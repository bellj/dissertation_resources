package org.thoughtcrime.securesms.video.videoconverter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.media.MediaInput;
import org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor;

/* loaded from: classes5.dex */
public class VideoThumbnailsView extends View {
    private static final String TAG = Log.tag(VideoThumbnailsView.class);
    private final Rect drawRect = new Rect();
    private long duration = 0;
    private OnDurationListener durationListener;
    private MediaInput input;
    private final Paint paint = new Paint(1);
    private final Rect tempDrawRect = new Rect();
    private final RectF tempRect = new RectF();
    private volatile ArrayList<Bitmap> thumbnails;
    private AsyncTask<Void, Bitmap, Void> thumbnailsTask;

    /* loaded from: classes5.dex */
    public interface OnDurationListener {
        void onDurationKnown(long j);
    }

    public void afterDurationChange(long j) {
    }

    public VideoThumbnailsView(Context context) {
        super(context);
    }

    public VideoThumbnailsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public VideoThumbnailsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setInput(MediaInput mediaInput) {
        this.input = mediaInput;
        this.thumbnails = null;
        AsyncTask<Void, Bitmap, Void> asyncTask = this.thumbnailsTask;
        if (asyncTask != null) {
            asyncTask.cancel(true);
            this.thumbnailsTask = null;
        }
        invalidate();
    }

    @Override // android.view.View
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.thumbnails = null;
        AsyncTask<Void, Bitmap, Void> asyncTask = this.thumbnailsTask;
        if (asyncTask != null) {
            asyncTask.cancel(true);
            this.thumbnailsTask = null;
        }
        MediaInput mediaInput = this.input;
        if (mediaInput != null) {
            try {
                mediaInput.close();
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.input != null) {
            this.tempDrawRect.set(getPaddingLeft(), getPaddingTop(), getWidth() - getPaddingRight(), getHeight() - getPaddingBottom());
            if (!this.drawRect.equals(this.tempDrawRect)) {
                this.drawRect.set(this.tempDrawRect);
                this.thumbnails = null;
                AsyncTask<Void, Bitmap, Void> asyncTask = this.thumbnailsTask;
                if (asyncTask != null) {
                    asyncTask.cancel(true);
                    this.thumbnailsTask = null;
                }
            }
            if (this.thumbnails != null) {
                float width = ((float) this.drawRect.width()) / ((float) (this.drawRect.width() / this.drawRect.height()));
                float height = (float) this.drawRect.height();
                RectF rectF = this.tempRect;
                Rect rect = this.drawRect;
                rectF.top = (float) rect.top;
                rectF.bottom = (float) rect.bottom;
                for (int i = 0; i < this.thumbnails.size(); i++) {
                    RectF rectF2 = this.tempRect;
                    float f = ((float) this.drawRect.left) + (((float) i) * width);
                    rectF2.left = f;
                    rectF2.right = f + width;
                    Bitmap bitmap = this.thumbnails.get(i);
                    if (bitmap != null) {
                        canvas.save();
                        canvas.rotate(180.0f, this.tempRect.centerX(), this.tempRect.centerY());
                        this.tempDrawRect.set(0, 0, bitmap.getWidth(), bitmap.getHeight());
                        if (((float) this.tempDrawRect.width()) * height > ((float) this.tempDrawRect.height()) * width) {
                            float height2 = (((float) this.tempDrawRect.height()) * width) / height;
                            Rect rect2 = this.tempDrawRect;
                            rect2.left = rect2.centerX() - ((int) (height2 / 2.0f));
                            Rect rect3 = this.tempDrawRect;
                            rect3.right = rect3.left + ((int) height2);
                        } else {
                            float width2 = (((float) this.tempDrawRect.width()) * height) / width;
                            Rect rect4 = this.tempDrawRect;
                            rect4.top = rect4.centerY() - ((int) (width2 / 2.0f));
                            Rect rect5 = this.tempDrawRect;
                            rect5.bottom = rect5.top + ((int) width2);
                        }
                        canvas.drawBitmap(bitmap, this.tempDrawRect, this.tempRect, this.paint);
                        canvas.restore();
                    }
                }
            } else if (this.thumbnailsTask == null) {
                int width3 = this.drawRect.width() / this.drawRect.height();
                float width4 = ((float) this.drawRect.width()) / ((float) width3);
                float height3 = (float) this.drawRect.height();
                this.thumbnails = new ArrayList<>(width3);
                ThumbnailsTask thumbnailsTask = new ThumbnailsTask(this, this.input, width4, height3, width3);
                this.thumbnailsTask = thumbnailsTask;
                thumbnailsTask.execute(new Void[0]);
            }
        }
    }

    public void setDurationListener(OnDurationListener onDurationListener) {
        this.durationListener = onDurationListener;
    }

    public void setDuration(long j) {
        OnDurationListener onDurationListener = this.durationListener;
        if (onDurationListener != null) {
            onDurationListener.onDurationKnown(j);
        }
        if (this.duration != j) {
            this.duration = j;
            afterDurationChange(j);
        }
    }

    public long getDuration() {
        return this.duration;
    }

    /* loaded from: classes5.dex */
    public static class ThumbnailsTask extends AsyncTask<Void, Bitmap, Void> {
        long duration;
        final MediaInput input;
        final int thumbnailCount;
        final float thumbnailHeight;
        final float thumbnailWidth;
        final WeakReference<VideoThumbnailsView> viewReference;

        ThumbnailsTask(VideoThumbnailsView videoThumbnailsView, MediaInput mediaInput, float f, float f2, int i) {
            this.viewReference = new WeakReference<>(videoThumbnailsView);
            this.input = mediaInput;
            this.thumbnailWidth = f;
            this.thumbnailHeight = f2;
            this.thumbnailCount = i;
        }

        public Void doInBackground(Void... voidArr) {
            String str = VideoThumbnailsView.TAG;
            Log.i(str, "generate " + this.thumbnailCount + " thumbnails " + this.thumbnailWidth + "x" + this.thumbnailHeight);
            VideoThumbnailsExtractor.extractThumbnails(this.input, this.thumbnailCount, (int) this.thumbnailHeight, new VideoThumbnailsExtractor.Callback() { // from class: org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsView.ThumbnailsTask.1
                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.Callback
                public void durationKnown(long j) {
                    ThumbnailsTask.this.duration = j;
                }

                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.Callback
                public boolean publishProgress(int i, Bitmap bitmap) {
                    boolean z = !ThumbnailsTask.this.isCancelled();
                    if (z) {
                        ThumbnailsTask.this.publishProgress(bitmap);
                    }
                    return z;
                }

                @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.Callback
                public void failed() {
                    Log.w(VideoThumbnailsView.TAG, "Thumbnail extraction failed");
                }
            });
            return null;
        }

        public void onProgressUpdate(Bitmap... bitmapArr) {
            ArrayList arrayList;
            if (!isCancelled()) {
                VideoThumbnailsView videoThumbnailsView = this.viewReference.get();
                if (videoThumbnailsView != null) {
                    arrayList = videoThumbnailsView.thumbnails;
                } else {
                    arrayList = null;
                }
                if (arrayList != null) {
                    arrayList.addAll(Arrays.asList(bitmapArr));
                    videoThumbnailsView.invalidate();
                }
            }
        }

        public void onPostExecute(Void r4) {
            ArrayList arrayList;
            VideoThumbnailsView videoThumbnailsView = this.viewReference.get();
            if (videoThumbnailsView != null) {
                arrayList = videoThumbnailsView.thumbnails;
            } else {
                arrayList = null;
            }
            if (videoThumbnailsView != null) {
                videoThumbnailsView.setDuration(this.duration);
                videoThumbnailsView.invalidate();
                String str = VideoThumbnailsView.TAG;
                StringBuilder sb = new StringBuilder();
                sb.append("onPostExecute, we have ");
                sb.append(arrayList != null ? Integer.valueOf(arrayList.size()) : "null");
                sb.append(" thumbs");
                Log.i(str, sb.toString());
            }
        }
    }
}
