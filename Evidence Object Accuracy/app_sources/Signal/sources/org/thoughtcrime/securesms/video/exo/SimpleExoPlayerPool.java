package org.thoughtcrime.securesms.video.exo;

import android.content.Context;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.mediacodec.MediaCodecInfo;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.DataSource;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.OkHttpClient;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.net.ContentProxySelector;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.video.exo.SignalDataSource;

/* compiled from: SimpleExoPlayerPool.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\r\u001a\u00020\u0002H\u0015J\b\u0010\u000e\u001a\u00020\u000fH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\n \f*\u0004\u0018\u00010\u000b0\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/SimpleExoPlayerPool;", "Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool;", "Lcom/google/android/exoplayer2/SimpleExoPlayer;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "dataSourceFactory", "Lcom/google/android/exoplayer2/upstream/DataSource$Factory;", "mediaSourceFactory", "Lcom/google/android/exoplayer2/source/MediaSourceFactory;", "okHttpClient", "Lokhttp3/OkHttpClient;", "kotlin.jvm.PlatformType", "createPlayer", "getMaxSimultaneousPlayback", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public final class SimpleExoPlayerPool extends ExoPlayerPool<SimpleExoPlayer> {
    public static final Companion Companion = new Companion(null);
    private static final int MAXIMUM_RESERVED_PLAYERS;
    private static final int MAXIMUM_SUPPORTED_PLAYBACK_PRE_23;
    private static final int MAXIMUM_SUPPORTED_PLAYBACK_PRE_23_LOW_MEM;
    private final Context context;
    private final DataSource.Factory dataSourceFactory;
    private final MediaSourceFactory mediaSourceFactory;
    private final OkHttpClient okHttpClient;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SimpleExoPlayerPool(Context context) {
        super(1);
        Intrinsics.checkNotNullParameter(context, "context");
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
        this.context = applicationContext;
        OkHttpClient build = ApplicationDependencies.getOkHttpClient().newBuilder().proxySelector(new ContentProxySelector()).build();
        this.okHttpClient = build;
        SignalDataSource.Factory factory = new SignalDataSource.Factory(ApplicationDependencies.getApplication(), build, null);
        this.dataSourceFactory = factory;
        this.mediaSourceFactory = new ProgressiveMediaSource.Factory(factory);
        ApplicationDependencies.getAppForegroundObserver().addListener(this);
    }

    @Override // org.thoughtcrime.securesms.video.exo.ExoPlayerPool
    protected int getMaxSimultaneousPlayback() {
        int i = 0;
        try {
            MediaCodecInfo decoderInfo = MediaCodecUtil.getDecoderInfo("video/avc", false, false);
            if (decoderInfo != null && decoderInfo.getMaxSupportedInstances() > 0) {
                i = decoderInfo.getMaxSupportedInstances();
            }
        } catch (MediaCodecUtil.DecoderQueryException unused) {
        }
        if (i > 0) {
            return i;
        }
        return DeviceProperties.isLowMemoryDevice(ApplicationDependencies.getApplication()) ? 3 : 6;
    }

    @Override // org.thoughtcrime.securesms.video.exo.ExoPlayerPool
    public SimpleExoPlayer createPlayer() {
        SimpleExoPlayer build = new SimpleExoPlayer.Builder(this.context).setMediaSourceFactory(this.mediaSourceFactory).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder(context)\n      .…ceFactory)\n      .build()");
        return build;
    }

    /* compiled from: SimpleExoPlayerPool.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/SimpleExoPlayerPool$Companion;", "", "()V", "MAXIMUM_RESERVED_PLAYERS", "", "MAXIMUM_SUPPORTED_PLAYBACK_PRE_23", "MAXIMUM_SUPPORTED_PLAYBACK_PRE_23_LOW_MEM", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
