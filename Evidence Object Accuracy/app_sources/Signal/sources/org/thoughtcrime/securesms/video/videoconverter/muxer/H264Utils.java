package org.thoughtcrime.securesms.video.videoconverter.muxer;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

/* loaded from: classes5.dex */
public final class H264Utils {
    private H264Utils() {
    }

    public static List<ByteBuffer> getNals(ByteBuffer byteBuffer) {
        ArrayList arrayList = new ArrayList();
        while (true) {
            ByteBuffer nextNALUnit = nextNALUnit(byteBuffer);
            if (nextNALUnit == null) {
                return arrayList;
            }
            arrayList.add(nextNALUnit);
        }
    }

    static ByteBuffer nextNALUnit(ByteBuffer byteBuffer) {
        skipToNALUnit(byteBuffer);
        return gotoNALUnit(byteBuffer);
    }

    static void skipToNALUnit(ByteBuffer byteBuffer) {
        if (byteBuffer.hasRemaining()) {
            int i = -1;
            while (byteBuffer.hasRemaining()) {
                i = (i << 8) | (byteBuffer.get() & 255);
                if ((16777215 & i) == 1) {
                    byteBuffer.position(byteBuffer.position());
                    return;
                }
            }
        }
    }

    static ByteBuffer gotoNALUnit(ByteBuffer byteBuffer) {
        if (!byteBuffer.hasRemaining()) {
            return null;
        }
        int position = byteBuffer.position();
        ByteBuffer slice = byteBuffer.slice();
        slice.order(ByteOrder.BIG_ENDIAN);
        int i = -1;
        while (true) {
            if (!byteBuffer.hasRemaining()) {
                break;
            }
            i = (i << 8) | (byteBuffer.get() & 255);
            if ((16777215 & i) == 1) {
                byteBuffer.position(byteBuffer.position() - (i == 1 ? 4 : 3));
                slice.limit(byteBuffer.position() - position);
            }
        }
        return slice;
    }
}
