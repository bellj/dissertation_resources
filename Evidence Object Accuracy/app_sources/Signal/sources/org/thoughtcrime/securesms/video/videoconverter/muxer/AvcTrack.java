package org.thoughtcrime.securesms.video.videoconverter.muxer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import org.mp4parser.boxes.iso14496.part12.SampleDescriptionBox;
import org.mp4parser.boxes.iso14496.part15.AvcConfigurationBox;
import org.mp4parser.boxes.sampleentry.VisualSampleEntry;
import org.mp4parser.streaming.SampleExtension;
import org.mp4parser.streaming.StreamingSample;
import org.mp4parser.streaming.extensions.CompositionTimeSampleExtension;
import org.mp4parser.streaming.extensions.CompositionTimeTrackExtension;
import org.mp4parser.streaming.extensions.DimensionTrackExtension;
import org.mp4parser.streaming.extensions.SampleFlagsSampleExtension;
import org.mp4parser.streaming.input.AbstractStreamingTrack;
import org.mp4parser.streaming.input.StreamingSampleImpl;
import org.mp4parser.streaming.input.h264.H264NalUnitHeader;
import org.mp4parser.streaming.input.h264.spspps.PictureParameterSet;
import org.mp4parser.streaming.input.h264.spspps.SeqParameterSet;
import org.mp4parser.streaming.input.h264.spspps.SliceHeader;
import org.mp4parser.streaming.input.h264.spspps.VUIParameters;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.badges.gifts.Gifts;

/* loaded from: classes5.dex */
public abstract class AvcTrack extends AbstractStreamingTrack {
    private static final String TAG;
    private final List<ByteBuffer> bufferedNals = new ArrayList();
    private long currentPresentationTimeUs;
    private final List<StreamingSample> decFrameBuffer = new ArrayList();
    private final List<StreamingSample> decFrameBuffer2 = new ArrayList();
    private int frametick = Gifts.GOOGLE_PAY_REQUEST_CODE;
    private FirstVclNalDetector fvnd;
    private int maxDecFrameBuffering = 16;
    private final LinkedHashMap<Integer, PictureParameterSet> ppsIdToPps = new LinkedHashMap<>();
    private final LinkedHashMap<Integer, ByteBuffer> ppsIdToPpsBytes = new LinkedHashMap<>();
    private H264NalUnitHeader sliceNalUnitHeader;
    private final LinkedHashMap<Integer, SeqParameterSet> spsIdToSps = new LinkedHashMap<>();
    private final LinkedHashMap<Integer, ByteBuffer> spsIdToSpsBytes = new LinkedHashMap<>();
    private final SampleDescriptionBox stsd;
    private int timescale = 90000;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public String getHandler() {
        return "vide";
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public String getLanguage() {
        return "```";
    }

    public AvcTrack(ByteBuffer byteBuffer, ByteBuffer byteBuffer2) {
        int i;
        int i2;
        int i3 = 16;
        handlePPS(byteBuffer2);
        SeqParameterSet handleSPS = handleSPS(byteBuffer);
        int i4 = (handleSPS.pic_width_in_mbs_minus1 + 1) * 16;
        int i5 = handleSPS.frame_mbs_only_flag ? 1 : 2;
        int i6 = (handleSPS.pic_height_in_map_units_minus1 + 1) * 16 * i5;
        int i7 = 0;
        if (handleSPS.frame_cropping_flag) {
            if ((!handleSPS.residual_color_transform_flag ? handleSPS.chroma_format_idc.getId() : 0) != 0) {
                i2 = handleSPS.chroma_format_idc.getSubWidth();
                i5 *= handleSPS.chroma_format_idc.getSubHeight();
            } else {
                i2 = 1;
            }
            i4 -= i2 * (handleSPS.frame_crop_left_offset + handleSPS.frame_crop_right_offset);
            i6 -= i5 * (handleSPS.frame_crop_top_offset + handleSPS.frame_crop_bottom_offset);
        }
        VisualSampleEntry visualSampleEntry = new VisualSampleEntry("avc1");
        visualSampleEntry.setDataReferenceIndex(1);
        visualSampleEntry.setDepth(24);
        visualSampleEntry.setFrameCount(1);
        visualSampleEntry.setHorizresolution(72.0d);
        visualSampleEntry.setVertresolution(72.0d);
        if (((DimensionTrackExtension) getTrackExtension(DimensionTrackExtension.class)) == null) {
            addTrackExtension(new DimensionTrackExtension(i4, i6));
        }
        visualSampleEntry.setWidth(i4);
        visualSampleEntry.setHeight(i6);
        visualSampleEntry.setCompressorname("AVC Coding");
        AvcConfigurationBox avcConfigurationBox = new AvcConfigurationBox();
        avcConfigurationBox.setSequenceParameterSets(Collections.singletonList(byteBuffer));
        avcConfigurationBox.setPictureParameterSets(Collections.singletonList(byteBuffer2));
        avcConfigurationBox.setAvcLevelIndication(handleSPS.level_idc);
        avcConfigurationBox.setAvcProfileIndication(handleSPS.profile_idc);
        avcConfigurationBox.setBitDepthLumaMinus8(handleSPS.bit_depth_luma_minus8);
        avcConfigurationBox.setBitDepthChromaMinus8(handleSPS.bit_depth_chroma_minus8);
        avcConfigurationBox.setChromaFormat(handleSPS.chroma_format_idc.getId());
        avcConfigurationBox.setConfigurationVersion(1);
        avcConfigurationBox.setLengthSizeMinusOne(3);
        avcConfigurationBox.setProfileCompatibility((handleSPS.constraint_set_0_flag ? 128 : 0) + (handleSPS.constraint_set_1_flag ? 64 : 0) + (handleSPS.constraint_set_2_flag ? 32 : 0) + (!handleSPS.constraint_set_3_flag ? 0 : i3) + (handleSPS.constraint_set_4_flag ? 8 : 0) + ((int) (handleSPS.reserved_zero_2bits & 3)));
        visualSampleEntry.addBox(avcConfigurationBox);
        SampleDescriptionBox sampleDescriptionBox = new SampleDescriptionBox();
        this.stsd = sampleDescriptionBox;
        sampleDescriptionBox.addBox(visualSampleEntry);
        VUIParameters vUIParameters = handleSPS.vuiParams;
        if (vUIParameters != null) {
            int i8 = vUIParameters.time_scale >> 1;
            int i9 = vUIParameters.num_units_in_tick;
            if (i8 == 0 || i9 == 0) {
                Log.w(TAG, "vuiParams contain invalid values: time_scale: " + i8 + " and frame_tick: " + i9 + ". Setting frame rate to 30fps");
                i8 = 0;
            } else {
                i7 = i9;
            }
            if (i7 > 0) {
                int i10 = i8 / i7;
                if (i10 > 100) {
                    Log.w(TAG, "Framerate is " + i10 + ". That is suspicious.");
                }
            } else {
                Log.w(TAG, "Frametick is " + i7 + ". That is suspicious.");
            }
            VUIParameters.BitstreamRestriction bitstreamRestriction = handleSPS.vuiParams.bitstreamRestriction;
            if (bitstreamRestriction != null) {
                this.maxDecFrameBuffering = bitstreamRestriction.max_dec_frame_buffering;
            }
            i = i7;
            i7 = i8;
        } else {
            Log.w(TAG, "Can't determine frame rate as SPS does not contain vuiParama");
            i = 0;
        }
        if (!(i7 == 0 || i == 0)) {
            this.timescale = i7;
            this.frametick = i;
        }
        int i11 = handleSPS.pic_order_cnt_type;
        if (i11 == 0) {
            addTrackExtension(new CompositionTimeTrackExtension());
        } else if (i11 == 1) {
            throw new MuxingException("Have not yet imlemented pic_order_cnt_type 1");
        }
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public long getTimescale() {
        return (long) this.timescale;
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.stsd;
    }

    private static H264NalUnitHeader getNalUnitHeader(ByteBuffer byteBuffer) {
        H264NalUnitHeader h264NalUnitHeader = new H264NalUnitHeader();
        byte b = byteBuffer.get(0);
        h264NalUnitHeader.nal_ref_idc = (b >> 5) & 3;
        h264NalUnitHeader.nal_unit_type = b & 31;
        return h264NalUnitHeader;
    }

    public void consumeNal(ByteBuffer byteBuffer, long j) throws IOException {
        H264NalUnitHeader nalUnitHeader = getNalUnitHeader(byteBuffer);
        int i = nalUnitHeader.nal_unit_type;
        switch (i) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
                FirstVclNalDetector firstVclNalDetector = new FirstVclNalDetector(byteBuffer, nalUnitHeader.nal_ref_idc, i);
                FirstVclNalDetector firstVclNalDetector2 = this.fvnd;
                if (firstVclNalDetector2 != null && firstVclNalDetector2.isFirstInNew(firstVclNalDetector)) {
                    pushSample(createSample(this.bufferedNals, this.fvnd.sliceHeader, this.sliceNalUnitHeader, j - this.currentPresentationTimeUs), false, false);
                    this.bufferedNals.clear();
                }
                this.currentPresentationTimeUs = Math.max(this.currentPresentationTimeUs, j);
                this.sliceNalUnitHeader = nalUnitHeader;
                this.fvnd = firstVclNalDetector;
                this.bufferedNals.add(byteBuffer);
                return;
            case 6:
            case 9:
                FirstVclNalDetector firstVclNalDetector3 = this.fvnd;
                if (firstVclNalDetector3 != null) {
                    pushSample(createSample(this.bufferedNals, firstVclNalDetector3.sliceHeader, this.sliceNalUnitHeader, j - this.currentPresentationTimeUs), false, false);
                    this.bufferedNals.clear();
                    this.fvnd = null;
                }
                this.bufferedNals.add(byteBuffer);
                return;
            case 7:
                FirstVclNalDetector firstVclNalDetector4 = this.fvnd;
                if (firstVclNalDetector4 != null) {
                    pushSample(createSample(this.bufferedNals, firstVclNalDetector4.sliceHeader, this.sliceNalUnitHeader, j - this.currentPresentationTimeUs), false, false);
                    this.bufferedNals.clear();
                    this.fvnd = null;
                }
                handleSPS(byteBuffer);
                return;
            case 8:
                FirstVclNalDetector firstVclNalDetector5 = this.fvnd;
                if (firstVclNalDetector5 != null) {
                    pushSample(createSample(this.bufferedNals, firstVclNalDetector5.sliceHeader, this.sliceNalUnitHeader, j - this.currentPresentationTimeUs), false, false);
                    this.bufferedNals.clear();
                    this.fvnd = null;
                }
                handlePPS(byteBuffer);
                return;
            case 10:
            case 11:
                return;
            case 12:
            default:
                Log.w(TAG, "Unknown NAL unit type: " + nalUnitHeader.nal_unit_type);
                return;
            case 13:
                throw new IOException("Sequence parameter set extension is not yet handled. Needs TLC.");
        }
    }

    public void consumeLastNal() throws IOException {
        pushSample(createSample(this.bufferedNals, this.fvnd.sliceHeader, this.sliceNalUnitHeader, 0), true, true);
    }

    private void pushSample(StreamingSample streamingSample, boolean z, boolean z2) throws IOException {
        if (streamingSample != null) {
            this.decFrameBuffer.add(streamingSample);
        }
        if (z) {
            while (this.decFrameBuffer.size() > 0) {
                pushSample(null, false, true);
            }
        } else if (this.decFrameBuffer.size() - 1 > this.maxDecFrameBuffering || z2) {
            StreamingSample remove = this.decFrameBuffer.remove(0);
            PictureOrderCountType0SampleExtension pictureOrderCountType0SampleExtension = (PictureOrderCountType0SampleExtension) remove.getSampleExtension(PictureOrderCountType0SampleExtension.class);
            if (pictureOrderCountType0SampleExtension == null) {
                this.sampleSink.acceptSample(remove, this);
                return;
            }
            int i = 0;
            for (StreamingSample streamingSample2 : this.decFrameBuffer) {
                if (pictureOrderCountType0SampleExtension.getPoc() > ((PictureOrderCountType0SampleExtension) streamingSample2.getSampleExtension(PictureOrderCountType0SampleExtension.class)).getPoc()) {
                    i++;
                }
            }
            for (StreamingSample streamingSample3 : this.decFrameBuffer2) {
                if (pictureOrderCountType0SampleExtension.getPoc() < ((PictureOrderCountType0SampleExtension) streamingSample3.getSampleExtension(PictureOrderCountType0SampleExtension.class)).getPoc()) {
                    i--;
                }
            }
            this.decFrameBuffer2.add(remove);
            if (this.decFrameBuffer2.size() > this.maxDecFrameBuffering) {
                this.decFrameBuffer2.remove(0).removeSampleExtension(PictureOrderCountType0SampleExtension.class);
            }
            remove.addSampleExtension(CompositionTimeSampleExtension.create((long) (i * this.frametick)));
            this.sampleSink.acceptSample(remove, this);
        }
    }

    private SampleFlagsSampleExtension createSampleFlagsSampleExtension(H264NalUnitHeader h264NalUnitHeader, SliceHeader sliceHeader) {
        SampleFlagsSampleExtension sampleFlagsSampleExtension = new SampleFlagsSampleExtension();
        boolean z = true;
        if (h264NalUnitHeader.nal_ref_idc == 0) {
            sampleFlagsSampleExtension.setSampleIsDependedOn(2);
        } else {
            sampleFlagsSampleExtension.setSampleIsDependedOn(1);
        }
        SliceHeader.SliceType sliceType = sliceHeader.slice_type;
        if (sliceType == SliceHeader.SliceType.I || sliceType == SliceHeader.SliceType.SI) {
            sampleFlagsSampleExtension.setSampleDependsOn(2);
        } else {
            sampleFlagsSampleExtension.setSampleDependsOn(1);
        }
        if (5 == h264NalUnitHeader.nal_unit_type) {
            z = false;
        }
        sampleFlagsSampleExtension.setSampleIsNonSyncSample(z);
        return sampleFlagsSampleExtension;
    }

    private PictureOrderCountType0SampleExtension createPictureOrderCountType0SampleExtension(SliceHeader sliceHeader) {
        int i = sliceHeader.sps.pic_order_cnt_type;
        PictureOrderCountType0SampleExtension pictureOrderCountType0SampleExtension = null;
        if (i == 0) {
            if (this.decFrameBuffer.size() > 0) {
                List<StreamingSample> list = this.decFrameBuffer;
                pictureOrderCountType0SampleExtension = (PictureOrderCountType0SampleExtension) list.get(list.size() - 1).getSampleExtension(PictureOrderCountType0SampleExtension.class);
            }
            return new PictureOrderCountType0SampleExtension(sliceHeader, pictureOrderCountType0SampleExtension);
        } else if (i == 1) {
            throw new MuxingException("pic_order_cnt_type == 1 needs to be implemented");
        } else if (i == 2) {
            return null;
        } else {
            throw new MuxingException("I don't know sliceHeader.sps.pic_order_cnt_type of " + sliceHeader.sps.pic_order_cnt_type);
        }
    }

    private StreamingSample createSample(List<ByteBuffer> list, SliceHeader sliceHeader, H264NalUnitHeader h264NalUnitHeader, long j) {
        StreamingSampleImpl streamingSampleImpl = new StreamingSampleImpl(list, (getTimescale() * Math.max(0L, j)) / 1000000);
        streamingSampleImpl.addSampleExtension(createSampleFlagsSampleExtension(h264NalUnitHeader, sliceHeader));
        PictureOrderCountType0SampleExtension createPictureOrderCountType0SampleExtension = createPictureOrderCountType0SampleExtension(sliceHeader);
        if (createPictureOrderCountType0SampleExtension != null) {
            streamingSampleImpl.addSampleExtension(createPictureOrderCountType0SampleExtension);
        }
        return streamingSampleImpl;
    }

    private void handlePPS(ByteBuffer byteBuffer) {
        byteBuffer.position(1);
        try {
            PictureParameterSet read = PictureParameterSet.read(byteBuffer);
            ByteBuffer byteBuffer2 = this.ppsIdToPpsBytes.get(Integer.valueOf(read.pic_parameter_set_id));
            if (byteBuffer2 != null && !byteBuffer2.equals(byteBuffer)) {
                throw new MuxingException("OMG - I got two SPS with same ID but different settings! (AVC3 is the solution)");
            }
            this.ppsIdToPpsBytes.put(Integer.valueOf(read.pic_parameter_set_id), byteBuffer);
            this.ppsIdToPps.put(Integer.valueOf(read.pic_parameter_set_id), read);
        } catch (IOException e) {
            throw new MuxingException("That's surprising to get IOException when working on ByteArrayInputStream", e);
        }
    }

    private SeqParameterSet handleSPS(ByteBuffer byteBuffer) {
        byteBuffer.position(1);
        try {
            SeqParameterSet read = SeqParameterSet.read(byteBuffer);
            ByteBuffer byteBuffer2 = this.spsIdToSpsBytes.get(Integer.valueOf(read.seq_parameter_set_id));
            if (byteBuffer2 != null && !byteBuffer2.equals(byteBuffer)) {
                throw new MuxingException("OMG - I got two SPS with same ID but different settings!");
            }
            this.spsIdToSpsBytes.put(Integer.valueOf(read.seq_parameter_set_id), byteBuffer);
            this.spsIdToSps.put(Integer.valueOf(read.seq_parameter_set_id), read);
            return read;
        } catch (IOException e) {
            throw new MuxingException("That's surprising to get IOException when working on ByteArrayInputStream", e);
        }
    }

    /* loaded from: classes5.dex */
    public class FirstVclNalDetector {
        final boolean bottom_field_flag;
        final int delta_pic_order_cnt_0;
        final int delta_pic_order_cnt_1;
        final int delta_pic_order_cnt_bottom;
        final boolean field_pic_flag;
        final int frame_num;
        final int idr_pic_id;
        final int nal_ref_idc;
        final int pic_order_cnt_lsb;
        final int pic_order_cnt_type;
        final int pic_parameter_set_id;
        final SliceHeader sliceHeader;

        FirstVclNalDetector(ByteBuffer byteBuffer, int i, int i2) {
            AvcTrack.this = r5;
            SliceHeader sliceHeader = new SliceHeader(byteBuffer, r5.spsIdToSps, r5.ppsIdToPps, i2 == 5);
            this.sliceHeader = sliceHeader;
            this.frame_num = sliceHeader.frame_num;
            this.pic_parameter_set_id = sliceHeader.pic_parameter_set_id;
            this.field_pic_flag = sliceHeader.field_pic_flag;
            this.bottom_field_flag = sliceHeader.bottom_field_flag;
            this.nal_ref_idc = i;
            this.pic_order_cnt_type = ((SeqParameterSet) r5.spsIdToSps.get(Integer.valueOf(((PictureParameterSet) r5.ppsIdToPps.get(Integer.valueOf(sliceHeader.pic_parameter_set_id))).seq_parameter_set_id))).pic_order_cnt_type;
            this.delta_pic_order_cnt_bottom = sliceHeader.delta_pic_order_cnt_bottom;
            this.pic_order_cnt_lsb = sliceHeader.pic_order_cnt_lsb;
            this.delta_pic_order_cnt_0 = sliceHeader.delta_pic_order_cnt_0;
            this.delta_pic_order_cnt_1 = sliceHeader.delta_pic_order_cnt_1;
            this.idr_pic_id = sliceHeader.idr_pic_id;
        }

        boolean isFirstInNew(FirstVclNalDetector firstVclNalDetector) {
            boolean z;
            if (firstVclNalDetector.frame_num != this.frame_num || firstVclNalDetector.pic_parameter_set_id != this.pic_parameter_set_id || (z = firstVclNalDetector.field_pic_flag) != this.field_pic_flag) {
                return true;
            }
            if ((z && firstVclNalDetector.bottom_field_flag != this.bottom_field_flag) || firstVclNalDetector.nal_ref_idc != this.nal_ref_idc) {
                return true;
            }
            int i = firstVclNalDetector.pic_order_cnt_type;
            if (i == 0 && this.pic_order_cnt_type == 0 && (firstVclNalDetector.pic_order_cnt_lsb != this.pic_order_cnt_lsb || firstVclNalDetector.delta_pic_order_cnt_bottom != this.delta_pic_order_cnt_bottom)) {
                return true;
            }
            if (i != 1 || this.pic_order_cnt_type != 1) {
                return false;
            }
            if (firstVclNalDetector.delta_pic_order_cnt_0 == this.delta_pic_order_cnt_0 && firstVclNalDetector.delta_pic_order_cnt_1 == this.delta_pic_order_cnt_1) {
                return false;
            }
            return true;
        }
    }

    /* loaded from: classes5.dex */
    public static class PictureOrderCountType0SampleExtension implements SampleExtension {
        int picOrderCntMsb;
        int picOrderCountLsb;

        PictureOrderCountType0SampleExtension(SliceHeader sliceHeader, PictureOrderCountType0SampleExtension pictureOrderCountType0SampleExtension) {
            int i;
            int i2;
            if (pictureOrderCountType0SampleExtension != null) {
                i2 = pictureOrderCountType0SampleExtension.picOrderCountLsb;
                i = pictureOrderCountType0SampleExtension.picOrderCntMsb;
            } else {
                i = 0;
                i2 = 0;
            }
            int i3 = 1 << (sliceHeader.sps.log2_max_pic_order_cnt_lsb_minus4 + 4);
            int i4 = sliceHeader.pic_order_cnt_lsb;
            this.picOrderCountLsb = i4;
            this.picOrderCntMsb = 0;
            if (i4 < i2 && i2 - i4 >= i3 / 2) {
                this.picOrderCntMsb = i + i3;
            } else if (i4 <= i2 || i4 - i2 <= i3 / 2) {
                this.picOrderCntMsb = i;
            } else {
                this.picOrderCntMsb = i - i3;
            }
        }

        int getPoc() {
            return this.picOrderCntMsb + this.picOrderCountLsb;
        }

        public String toString() {
            return "picOrderCntMsb=" + this.picOrderCntMsb + ", picOrderCountLsb=" + this.picOrderCountLsb;
        }
    }
}
