package org.thoughtcrime.securesms.video.exo;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.util.storage.FileStorage;
import org.thoughtcrime.securesms.video.exo.GiphyMp4Cache;

/* compiled from: GiphyMp4Cache.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001a2\u00020\u0001:\u0004\u001a\u001b\u001c\u001dB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001c\u0010\u000b\u001a\u00020\u00032\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\rH\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0007J\u0018\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0007H\u0002J\u0018\u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0007J\u0018\u0010\u0016\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0007J\u001e\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u00072\u0006\u0010\u0018\u001a\u00020\u0019R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache;", "", "maxSize", "", "(J)V", "lockedUris", "", "Landroid/net/Uri;", "uriToEntry", "", "Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Entry;", "calculateTotalSize", "data", "", "onAppStart", "", "context", "Landroid/content/Context;", "onEntryReleased", "uri", "read", "Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$ReadData;", "readFromStorage", "write", "inputStream", "Ljava/io/InputStream;", "Companion", "Entry", "Lease", "ReadData", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class GiphyMp4Cache {
    public static final Companion Companion = new Companion(null);
    private static final Object DATA_LOCK = new Object();
    private static final String DIRECTORY;
    private static final String EXTENSION;
    private static final String PREFIX;
    private static final String TAG = Log.tag(GiphyMp4Cache.class);
    private final Set<Uri> lockedUris = new LinkedHashSet();
    private final long maxSize;
    private final Map<Uri, Entry> uriToEntry = new LinkedHashMap();

    /* compiled from: GiphyMp4Cache.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bæ\u0001\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Lease;", "", "release", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public interface Lease {
        void release();
    }

    public GiphyMp4Cache(long j) {
        this.maxSize = j;
    }

    /* compiled from: GiphyMp4Cache.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Companion;", "", "()V", "DATA_LOCK", "Ljava/lang/Object;", "DIRECTORY", "", "EXTENSION", "PREFIX", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final void onAppStart(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        synchronized (DATA_LOCK) {
            this.lockedUris.clear();
            for (File file : FileStorage.getAllFiles(context, DIRECTORY, PREFIX)) {
                if (!file.delete()) {
                    String str = TAG;
                    Log.w(str, "Failed to delete: " + file.getName());
                }
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    public final ReadData write(Context context, Uri uri, InputStream inputStream) throws IOException {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(uri, "uri");
        Intrinsics.checkNotNullParameter(inputStream, "inputStream");
        Object obj = DATA_LOCK;
        synchronized (obj) {
            this.lockedUris.add(uri);
        }
        String save = FileStorage.save(context, inputStream, DIRECTORY, PREFIX, EXTENSION);
        Intrinsics.checkNotNullExpressionValue(save, "save(context, inputStrea…CTORY, PREFIX, EXTENSION)");
        long length = FileStorage.getFile(context, DIRECTORY, save).length();
        synchronized (obj) {
            this.uriToEntry.put(uri, new Entry(uri, save, length, System.currentTimeMillis()));
            Unit unit = Unit.INSTANCE;
        }
        ReadData readFromStorage = readFromStorage(context, uri);
        if (readFromStorage != null) {
            return readFromStorage;
        }
        throw new IOException("Could not find file immediately after writing!");
    }

    public final ReadData read(Context context, Uri uri) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(uri, "uri");
        synchronized (DATA_LOCK) {
            this.lockedUris.add(uri);
        }
        try {
            return readFromStorage(context, uri);
        } catch (IOException unused) {
            return null;
        }
    }

    public final ReadData readFromStorage(Context context, Uri uri) throws IOException {
        Entry entry;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(uri, "uri");
        synchronized (DATA_LOCK) {
            entry = this.uriToEntry.get(uri);
        }
        if (entry == null) {
            return null;
        }
        long length = FileStorage.getFile(context, DIRECTORY, entry.getFilename()).length();
        InputStream read = FileStorage.read(context, DIRECTORY, entry.getFilename());
        Intrinsics.checkNotNullExpressionValue(read, "read(context, DIRECTORY, entry.filename)");
        return new ReadData(read, length, new Lease(context, uri) { // from class: org.thoughtcrime.securesms.video.exo.GiphyMp4Cache$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Uri f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.video.exo.GiphyMp4Cache.Lease
            public final void release() {
                GiphyMp4Cache.m3302readFromStorage$lambda5(GiphyMp4Cache.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: readFromStorage$lambda-5 */
    public static final void m3302readFromStorage$lambda5(GiphyMp4Cache giphyMp4Cache, Context context, Uri uri) {
        Intrinsics.checkNotNullParameter(giphyMp4Cache, "this$0");
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(uri, "$uri");
        giphyMp4Cache.onEntryReleased(context, uri);
    }

    private final void onEntryReleased(Context context, Uri uri) {
        synchronized (DATA_LOCK) {
            this.lockedUris.remove(uri);
            long calculateTotalSize = calculateTotalSize(this.uriToEntry);
            if (calculateTotalSize > this.maxSize) {
                Set<Map.Entry<Uri, Entry>> entrySet = this.uriToEntry.entrySet();
                ArrayList<Map.Entry> arrayList = new ArrayList();
                for (Object obj : entrySet) {
                    if (!this.lockedUris.contains(((Map.Entry) obj).getKey())) {
                        arrayList.add(obj);
                    }
                }
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
                for (Map.Entry entry : arrayList) {
                    arrayList2.add((Entry) entry.getValue());
                }
                ArrayList arrayList3 = new ArrayList(CollectionsKt___CollectionsKt.sortedWith(arrayList2, new Comparator() { // from class: org.thoughtcrime.securesms.video.exo.GiphyMp4Cache$onEntryReleased$lambda-9$$inlined$sortedBy$1
                    @Override // java.util.Comparator
                    public final int compare(T t, T t2) {
                        return ComparisonsKt__ComparisonsKt.compareValues(Long.valueOf(((GiphyMp4Cache.Entry) t).getLastAccessed()), Long.valueOf(((GiphyMp4Cache.Entry) t2).getLastAccessed()));
                    }
                }));
                while (calculateTotalSize > this.maxSize && (!arrayList3.isEmpty())) {
                    Entry entry2 = (Entry) arrayList3.remove(0);
                    if (!FileStorage.getFile(context, DIRECTORY, entry2.getFilename()).delete()) {
                        String str = TAG;
                        Log.w(str, "Failed to delete " + entry2.getFilename());
                    }
                    this.uriToEntry.remove(entry2.getUri());
                    calculateTotalSize = calculateTotalSize(this.uriToEntry);
                }
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    private final long calculateTotalSize(Map<Uri, Entry> map) {
        Object obj;
        Collection<Entry> values = map.values();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(values, 10));
        for (Entry entry : values) {
            arrayList.add(Long.valueOf(entry.getSize()));
        }
        Iterator it = arrayList.iterator();
        if (!it.hasNext()) {
            obj = null;
        } else {
            Object next = it.next();
            while (it.hasNext()) {
                next = Long.valueOf(((Number) next).longValue() + ((Number) it.next()).longValue());
            }
            obj = next;
        }
        Long l = (Long) obj;
        if (l != null) {
            return l.longValue();
        }
        return 0;
    }

    /* compiled from: GiphyMp4Cache.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0007HÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Entry;", "", "uri", "Landroid/net/Uri;", "filename", "", MediaPreviewActivity.SIZE_EXTRA, "", "lastAccessed", "(Landroid/net/Uri;Ljava/lang/String;JJ)V", "getFilename", "()Ljava/lang/String;", "getLastAccessed", "()J", "getSize", "getUri", "()Landroid/net/Uri;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Entry {
        private final String filename;
        private final long lastAccessed;
        private final long size;
        private final Uri uri;

        public static /* synthetic */ Entry copy$default(Entry entry, Uri uri, String str, long j, long j2, int i, Object obj) {
            if ((i & 1) != 0) {
                uri = entry.uri;
            }
            if ((i & 2) != 0) {
                str = entry.filename;
            }
            if ((i & 4) != 0) {
                j = entry.size;
            }
            if ((i & 8) != 0) {
                j2 = entry.lastAccessed;
            }
            return entry.copy(uri, str, j, j2);
        }

        public final Uri component1() {
            return this.uri;
        }

        public final String component2() {
            return this.filename;
        }

        public final long component3() {
            return this.size;
        }

        public final long component4() {
            return this.lastAccessed;
        }

        public final Entry copy(Uri uri, String str, long j, long j2) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(str, "filename");
            return new Entry(uri, str, j, j2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Entry)) {
                return false;
            }
            Entry entry = (Entry) obj;
            return Intrinsics.areEqual(this.uri, entry.uri) && Intrinsics.areEqual(this.filename, entry.filename) && this.size == entry.size && this.lastAccessed == entry.lastAccessed;
        }

        public int hashCode() {
            return (((((this.uri.hashCode() * 31) + this.filename.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.size)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastAccessed);
        }

        public String toString() {
            return "Entry(uri=" + this.uri + ", filename=" + this.filename + ", size=" + this.size + ", lastAccessed=" + this.lastAccessed + ')';
        }

        public Entry(Uri uri, String str, long j, long j2) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(str, "filename");
            this.uri = uri;
            this.filename = str;
            this.size = j;
            this.lastAccessed = j2;
        }

        public final String getFilename() {
            return this.filename;
        }

        public final long getLastAccessed() {
            return this.lastAccessed;
        }

        public final long getSize() {
            return this.size;
        }

        public final Uri getUri() {
            return this.uri;
        }
    }

    /* compiled from: GiphyMp4Cache.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\u0006\u0010\u0018\u001a\u00020\u0019J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$ReadData;", "", "inputStream", "Ljava/io/InputStream;", "length", "", "lease", "Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Lease;", "(Ljava/io/InputStream;JLorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Lease;)V", "getInputStream", "()Ljava/io/InputStream;", "getLease", "()Lorg/thoughtcrime/securesms/video/exo/GiphyMp4Cache$Lease;", "getLength", "()J", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "release", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class ReadData {
        private final InputStream inputStream;
        private final Lease lease;
        private final long length;

        public static /* synthetic */ ReadData copy$default(ReadData readData, InputStream inputStream, long j, Lease lease, int i, Object obj) {
            if ((i & 1) != 0) {
                inputStream = readData.inputStream;
            }
            if ((i & 2) != 0) {
                j = readData.length;
            }
            if ((i & 4) != 0) {
                lease = readData.lease;
            }
            return readData.copy(inputStream, j, lease);
        }

        public final InputStream component1() {
            return this.inputStream;
        }

        public final long component2() {
            return this.length;
        }

        public final Lease component3() {
            return this.lease;
        }

        public final ReadData copy(InputStream inputStream, long j, Lease lease) {
            Intrinsics.checkNotNullParameter(inputStream, "inputStream");
            Intrinsics.checkNotNullParameter(lease, "lease");
            return new ReadData(inputStream, j, lease);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ReadData)) {
                return false;
            }
            ReadData readData = (ReadData) obj;
            return Intrinsics.areEqual(this.inputStream, readData.inputStream) && this.length == readData.length && Intrinsics.areEqual(this.lease, readData.lease);
        }

        public int hashCode() {
            return (((this.inputStream.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.length)) * 31) + this.lease.hashCode();
        }

        public String toString() {
            return "ReadData(inputStream=" + this.inputStream + ", length=" + this.length + ", lease=" + this.lease + ')';
        }

        public ReadData(InputStream inputStream, long j, Lease lease) {
            Intrinsics.checkNotNullParameter(inputStream, "inputStream");
            Intrinsics.checkNotNullParameter(lease, "lease");
            this.inputStream = inputStream;
            this.length = j;
            this.lease = lease;
        }

        public final InputStream getInputStream() {
            return this.inputStream;
        }

        public final Lease getLease() {
            return this.lease;
        }

        public final long getLength() {
            return this.length;
        }

        public final void release() {
            StreamUtil.close(this.inputStream);
            this.lease.release();
        }
    }
}
