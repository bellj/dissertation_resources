package org.thoughtcrime.securesms.video.videoconverter;

import android.media.MediaCodec;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
final class AndroidMuxer implements Muxer {
    private final MediaMuxer muxer;

    public AndroidMuxer(File file) throws IOException {
        this.muxer = new MediaMuxer(file.getAbsolutePath(), 0);
    }

    public AndroidMuxer(FileDescriptor fileDescriptor) throws IOException {
        this.muxer = new MediaMuxer(fileDescriptor, 0);
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void start() {
        this.muxer.start();
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void stop() {
        this.muxer.stop();
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public int addTrack(MediaFormat mediaFormat) {
        return this.muxer.addTrack(mediaFormat);
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void writeSampleData(int i, ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) {
        this.muxer.writeSampleData(i, byteBuffer, bufferInfo);
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void release() {
        this.muxer.release();
    }
}
