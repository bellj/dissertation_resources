package org.thoughtcrime.securesms.video.videoconverter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.MemoryUnitFormat;

/* loaded from: classes5.dex */
public final class VideoThumbnailsRangeSelectorView extends VideoThumbnailsView {
    private static final int ANIMATION_DURATION_MS;
    private static final long MINIMUM_SELECTABLE_RANGE = TimeUnit.MILLISECONDS.toMicros(500);
    private static final String TAG = Log.tag(VideoThumbnailsRangeSelectorView.class);
    private long actualPosition;
    private Drawable chevronLeft;
    private Drawable chevronRight;
    private int cursor;
    private int cursorColor;
    private int cursorPixels;
    private long downCursor;
    private long downMax;
    private long downMin;
    private long dragEndTimeMs;
    private long dragPosition;
    private long dragStartTimeMs;
    private Thumb dragThumb;
    private Long externalMaxValue;
    private Long externalMinValue;
    private Thumb lastDragThumb;
    private int left;
    private Long maxValue;
    private long maximumSelectableRangeMicros;
    private Long minValue;
    private OnRangeChangeListener onRangeChangeListener;
    private Quality outputQuality;
    private final Paint paint = new Paint(1);
    private final Paint paintGrey = new Paint(1);
    private long qualityAvailableTimeMs;
    private int right;
    private final Rect tempDrawRect = new Rect();
    private int thumbColor;
    private int thumbColorEdited;
    private int thumbHintBackgroundColor;
    private int thumbHintTextColor;
    private int thumbHintTextSize;
    private int thumbSizePixels;
    private final Paint thumbTimeBackgroundPaint = new Paint(1);
    private final Paint thumbTimeTextPaint = new Paint(1);
    private int thumbTouchRadius;
    private final RectF timePillRect = new RectF();
    private float xDown;

    /* loaded from: classes5.dex */
    public interface OnRangeChangeListener {
        Quality getQuality(long j, long j2);

        void onEndPositionDrag(long j);

        void onPositionDrag(long j);

        void onRangeDrag(long j, long j2, long j3, Thumb thumb);

        void onRangeDragEnd(long j, long j2, long j3, Thumb thumb);
    }

    /* loaded from: classes5.dex */
    public enum Thumb {
        MIN,
        MAX,
        POSITION
    }

    public VideoThumbnailsRangeSelectorView(Context context) {
        super(context);
        init(null);
    }

    public VideoThumbnailsRangeSelectorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public VideoThumbnailsRangeSelectorView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.VideoThumbnailsRangeSelectorView, 0, 0);
            try {
                this.thumbSizePixels = obtainStyledAttributes.getDimensionPixelSize(8, 1);
                this.cursorPixels = obtainStyledAttributes.getDimensionPixelSize(1, 1);
                int color = obtainStyledAttributes.getColor(2, -65536);
                this.thumbColor = color;
                this.thumbColorEdited = obtainStyledAttributes.getColor(3, color);
                this.cursorColor = obtainStyledAttributes.getColor(0, this.thumbColor);
                this.thumbTouchRadius = obtainStyledAttributes.getDimensionPixelSize(7, 50);
                this.thumbHintTextSize = obtainStyledAttributes.getDimensionPixelSize(6, 0);
                this.thumbHintTextColor = obtainStyledAttributes.getColor(5, -65536);
                this.thumbHintBackgroundColor = obtainStyledAttributes.getColor(4, -16711936);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
        this.chevronLeft = VectorDrawableCompat.create(getResources(), R.drawable.ic_chevron_left_black_8dp, null);
        this.chevronRight = VectorDrawableCompat.create(getResources(), R.drawable.ic_chevron_right_black_8dp, null);
        this.paintGrey.setColor(2130706432);
        this.paintGrey.setStyle(Paint.Style.FILL_AND_STROKE);
        this.paintGrey.setStrokeWidth(1.0f);
        this.paint.setStrokeWidth(2.0f);
        this.thumbTimeTextPaint.setTextSize((float) this.thumbHintTextSize);
        this.thumbTimeTextPaint.setColor(this.thumbHintTextColor);
        this.thumbTimeBackgroundPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        this.thumbTimeBackgroundPaint.setColor(this.thumbHintBackgroundColor);
    }

    /* access modifiers changed from: protected */
    @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsView
    public void afterDurationChange(long j) {
        super.afterDurationChange(j);
        Long l = this.maxValue;
        if (l != null && j < l.longValue()) {
            this.maxValue = Long.valueOf(j);
        }
        Long l2 = this.minValue;
        if (l2 != null && j < l2.longValue()) {
            this.minValue = Long.valueOf(j);
        }
        if (j > 0) {
            Long l3 = this.externalMinValue;
            if (l3 != null) {
                setMinMax(l3.longValue(), getMaxValue(), Thumb.MIN);
                this.externalMinValue = null;
            }
            if (this.externalMaxValue != null) {
                setMinMax(getMinValue(), this.externalMaxValue.longValue(), Thumb.MAX);
                this.externalMaxValue = null;
            }
        }
        if (setMinValue(getMinValue())) {
            String str = TAG;
            Log.d(str, "Clamped video duration to " + getMaxValue());
            OnRangeChangeListener onRangeChangeListener = this.onRangeChangeListener;
            if (onRangeChangeListener != null) {
                onRangeChangeListener.onRangeDragEnd(getMinValue(), getMaxValue(), getDuration(), Thumb.MAX);
            }
        }
        OnRangeChangeListener onRangeChangeListener2 = this.onRangeChangeListener;
        if (onRangeChangeListener2 != null) {
            onRangeChangeListener2.onRangeDragEnd(getMinValue(), getMaxValue(), getDuration(), Thumb.MIN);
            setOutputQuality(this.onRangeChangeListener.getQuality(getClipDuration(), getDuration()));
        }
        invalidate();
    }

    public void setOnRangeChangeListener(OnRangeChangeListener onRangeChangeListener) {
        this.onRangeChangeListener = onRangeChangeListener;
    }

    public void setActualPosition(long j) {
        if (this.actualPosition != j) {
            this.actualPosition = j;
            invalidate();
        }
    }

    private void setDragPosition(long j) {
        if (this.dragPosition != j) {
            this.dragPosition = Math.max(getMinValue(), Math.min(getMaxValue(), j));
            invalidate();
        }
    }

    /* access modifiers changed from: protected */
    @Override // org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsView, android.view.View
    public void onDraw(Canvas canvas) {
        boolean z;
        int i;
        Thumb thumb;
        Thumb thumb2;
        Thumb thumb3;
        super.onDraw(canvas);
        canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
        int drawableWidth = getDrawableWidth();
        int drawableHeight = getDrawableHeight();
        long duration = getDuration();
        long minValue = getMinValue();
        long maxValue = getMaxValue();
        boolean z2 = (minValue == 0 && maxValue == duration) ? false : true;
        long j = this.dragThumb == Thumb.POSITION ? this.dragPosition : this.actualPosition;
        if (duration != 0) {
            z = z2;
            i = (int) ((minValue * ((long) drawableWidth)) / duration);
        } else {
            z = z2;
            i = 0;
        }
        this.left = i;
        this.right = duration != 0 ? (int) ((maxValue * ((long) drawableWidth)) / duration) : drawableWidth;
        this.cursor = duration != 0 ? (int) ((j * ((long) drawableWidth)) / duration) : drawableWidth;
        this.tempDrawRect.set(0, 0, i - 1, drawableHeight);
        canvas.drawRect(this.tempDrawRect, this.paintGrey);
        this.tempDrawRect.set(this.right + 1, 0, drawableWidth, drawableHeight);
        canvas.drawRect(this.tempDrawRect, this.paintGrey);
        this.paint.setStyle(Paint.Style.STROKE);
        this.tempDrawRect.set(this.left, 0, this.right, drawableHeight);
        this.paint.setColor(z ? this.thumbColorEdited : this.thumbColor);
        canvas.drawRect(this.tempDrawRect, this.paint);
        this.paint.setStyle(Paint.Style.FILL_AND_STROKE);
        Rect rect = this.tempDrawRect;
        int i2 = this.left;
        rect.set(i2, 0, this.thumbSizePixels + i2, drawableHeight);
        canvas.drawRect(this.tempDrawRect, this.paint);
        Rect rect2 = this.tempDrawRect;
        int i3 = this.right;
        rect2.set(i3 - this.thumbSizePixels, 0, i3, drawableHeight);
        canvas.drawRect(this.tempDrawRect, this.paint);
        int min = Math.min(drawableHeight, this.thumbSizePixels * 2);
        this.chevronLeft.setBounds(0, 0, min, min);
        this.chevronRight.setBounds(0, 0, min, min);
        float f = ((float) (drawableHeight - min)) / 2.0f;
        float f2 = ((float) (this.thumbSizePixels - min)) / 2.0f;
        canvas.save();
        canvas.translate(((float) this.left) + f2, f);
        this.chevronLeft.draw(canvas);
        canvas.restore();
        canvas.save();
        canvas.translate(((float) (this.right - this.thumbSizePixels)) + f2, f);
        this.chevronRight.draw(canvas);
        canvas.restore();
        if (this.thumbHintTextSize > 0) {
            if (this.dragStartTimeMs > 0 && ((thumb3 = this.dragThumb) == Thumb.MIN || thumb3 == Thumb.MAX)) {
                drawTimeHint(canvas, drawableWidth, drawableHeight, thumb3, false);
            }
            if (this.dragEndTimeMs > 0 && ((thumb2 = this.lastDragThumb) == Thumb.MIN || thumb2 == Thumb.MAX)) {
                drawTimeHint(canvas, drawableWidth, drawableHeight, thumb2, true);
            }
            canvas.save();
            canvas.translate(0.0f, (float) (drawableHeight * 2));
            drawDurationAndSizeHint(canvas, drawableWidth);
            canvas.restore();
        }
        int i4 = this.left;
        int i5 = this.cursor;
        if (i4 <= i5 && i5 <= this.right && (thumb = this.dragThumb) != Thumb.MIN && thumb != Thumb.MAX) {
            canvas.translate((float) (this.cursorPixels / 2), 0.0f);
            Rect rect3 = this.tempDrawRect;
            int i6 = this.cursor;
            rect3.set(i6, 0, this.cursorPixels + i6, drawableHeight);
            this.paint.setColor(this.cursorColor);
            canvas.drawRect(this.tempDrawRect, this.paint);
        }
    }

    private void drawTimeHint(Canvas canvas, int i, int i2, Thumb thumb, boolean z) {
        long j;
        canvas.save();
        long seconds = TimeUnit.MICROSECONDS.toSeconds(thumb == Thumb.MIN ? getMinValue() : getMaxValue());
        String format = String.format(Locale.getDefault(), "%d:%02d", Long.valueOf(seconds / 60), Long.valueOf(seconds % 60));
        int i3 = this.thumbHintTextSize;
        float f = ((float) i3) * 0.5f;
        float f2 = ((float) i3) * 0.75f;
        this.thumbTimeTextPaint.getTextBounds(format, 0, format.length(), this.tempDrawRect);
        RectF rectF = this.timePillRect;
        Rect rect = this.tempDrawRect;
        rectF.set(((float) rect.left) - f2, ((float) rect.top) - f, ((float) rect.right) + f2, ((float) rect.bottom) + f);
        float width = this.timePillRect.width() / 2.0f;
        float height = this.timePillRect.height() / 2.0f;
        if (z) {
            j = 100 - Math.min(100L, System.currentTimeMillis() - this.dragEndTimeMs);
        } else {
            j = Math.min(100L, System.currentTimeMillis() - this.dragStartTimeMs);
        }
        float f3 = ((float) j) / 100.0f;
        float f4 = (0.2f * f3) + 0.8f;
        int i4 = (int) (f3 * 255.0f);
        if (thumb == Thumb.MAX) {
            canvas.translate(Math.min((float) this.right, ((float) i) - width), 0.0f);
        } else {
            canvas.translate(Math.max((float) this.left, width), 0.0f);
        }
        canvas.translate(0.0f, ((float) i2) + height);
        canvas.scale(f4, f4);
        this.thumbTimeBackgroundPaint.setAlpha(Math.round(((float) i4) * 0.6f));
        this.thumbTimeTextPaint.setAlpha(i4);
        canvas.translate(f2 - width, height);
        canvas.drawRoundRect(this.timePillRect, height, height, this.thumbTimeBackgroundPaint);
        canvas.drawText(format, 0.0f, 0.0f, this.thumbTimeTextPaint);
        canvas.restore();
        if ((z && j > 0) || (!z && j < 100)) {
            invalidate();
        } else if (z) {
            this.lastDragThumb = null;
        }
    }

    private void drawDurationAndSizeHint(Canvas canvas, int i) {
        if (this.outputQuality != null) {
            canvas.save();
            long seconds = TimeUnit.MICROSECONDS.toSeconds(getMaxValue() - getMinValue());
            String format = String.format(Locale.getDefault(), "%d:%02d • %s", Long.valueOf(seconds / 60), Long.valueOf(seconds % 60), MemoryUnitFormat.formatBytes(this.outputQuality.fileSize, MemoryUnitFormat.MEGA_BYTES, true));
            int i2 = this.thumbHintTextSize;
            float f = ((float) i2) * 0.5f;
            float f2 = ((float) i2) * 0.75f;
            this.thumbTimeTextPaint.getTextBounds(format, 0, format.length(), this.tempDrawRect);
            RectF rectF = this.timePillRect;
            Rect rect = this.tempDrawRect;
            rectF.set(((float) rect.left) - f2, ((float) rect.top) - f, ((float) rect.right) + f2, ((float) rect.bottom) + f);
            float width = this.timePillRect.width() / 2.0f;
            float height = this.timePillRect.height() / 2.0f;
            long min = Math.min(100L, System.currentTimeMillis() - this.qualityAvailableTimeMs);
            float f3 = ((float) min) / 100.0f;
            float f4 = (0.2f * f3) + 0.8f;
            int i3 = (int) (f3 * 255.0f);
            canvas.translate(Math.max(width, Math.min(((float) (this.right + this.left)) / 2.0f, ((float) i) - width)), -2.0f * height);
            canvas.scale(f4, f4);
            this.thumbTimeBackgroundPaint.setAlpha(Math.round(((float) i3) * 0.6f));
            this.thumbTimeTextPaint.setAlpha(i3);
            canvas.translate(f2 - width, height);
            canvas.drawRoundRect(this.timePillRect, height, height, this.thumbTimeBackgroundPaint);
            canvas.drawText(format, 0.0f, 0.0f, this.thumbTimeTextPaint);
            canvas.restore();
            if (min < 100) {
                invalidate();
            }
        }
    }

    public long getMinValue() {
        Long l = this.minValue;
        if (l == null) {
            return 0;
        }
        return l.longValue();
    }

    public long getMaxValue() {
        Long l = this.maxValue;
        return l == null ? getDuration() : l.longValue();
    }

    public long getClipDuration() {
        return getMaxValue() - getMinValue();
    }

    private boolean setMinValue(long j) {
        Long l = this.minValue;
        if (l == null || l.longValue() != j) {
            return setMinMax(j, getMaxValue(), Thumb.MIN);
        }
        return false;
    }

    public boolean setMaxValue(long j) {
        Long l = this.maxValue;
        if (l == null || l.longValue() != j) {
            return setMinMax(getMinValue(), j, Thumb.MAX);
        }
        return false;
    }

    private boolean setMinMax(long j, long j2, Thumb thumb) {
        long j3;
        long j4;
        long j5;
        long minValue = getMinValue();
        long maxValue = getMaxValue();
        long duration = getDuration();
        long j6 = MINIMUM_SELECTABLE_RANGE;
        long max = Math.max(j6, pixelToDuration(((float) this.thumbSizePixels) * 2.5f));
        long j7 = this.maximumSelectableRangeMicros;
        long max2 = j7 <= j6 ? 0 : Math.max(j7, pixelToDuration(((float) this.thumbSizePixels) * 2.5f));
        if (thumb == Thumb.MIN) {
            j5 = clamp(j, 0, maxValue - max);
            j4 = max2 > 0 ? clamp(j2, j5 + max, Math.min(max2 + j5, duration)) : j2;
            j3 = maxValue;
        } else {
            j3 = maxValue;
            long clamp = clamp(j2, minValue + max, duration);
            if (max2 > 0) {
                j5 = clamp(j, Math.max(0L, clamp - max2), clamp - max);
                j4 = clamp;
            } else {
                j4 = clamp;
                j5 = j;
            }
        }
        if (j5 == minValue && j4 == j3) {
            return false;
        }
        this.minValue = Long.valueOf(j5);
        this.maxValue = Long.valueOf(j4);
        invalidate();
        return true;
    }

    private static long clamp(long j, long j2, long j3) {
        return Math.min(Math.max(j2, j), j3);
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        OnRangeChangeListener onRangeChangeListener;
        int actionMasked = motionEvent.getActionMasked();
        boolean z = false;
        if (actionMasked == 0) {
            this.xDown = motionEvent.getX();
            this.downCursor = this.actualPosition;
            this.downMin = getMinValue();
            this.downMax = getMaxValue();
            this.dragThumb = closestThumb(motionEvent.getX());
            this.dragStartTimeMs = System.currentTimeMillis();
            invalidate();
            if (this.dragThumb != null) {
                return true;
            }
            return false;
        } else if (actionMasked == 2) {
            long pixelToDuration = pixelToDuration(motionEvent.getX() - this.xDown);
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$video$videoconverter$VideoThumbnailsRangeSelectorView$Thumb[this.dragThumb.ordinal()];
            if (i == 1) {
                setDragPosition(this.downCursor + pixelToDuration);
                z = true;
            } else if (i == 2) {
                z = setMinValue(this.downMin + pixelToDuration);
            } else if (i == 3) {
                z = setMaxValue(this.downMax + pixelToDuration);
            }
            if (z && (onRangeChangeListener = this.onRangeChangeListener) != null) {
                if (this.dragThumb == Thumb.POSITION) {
                    onRangeChangeListener.onPositionDrag(this.dragPosition);
                } else {
                    onRangeChangeListener.onRangeDrag(getMinValue(), getMaxValue(), getDuration(), this.dragThumb);
                    setOutputQuality(this.onRangeChangeListener.getQuality(getClipDuration(), getDuration()));
                }
            }
            return true;
        } else if (actionMasked == 1) {
            OnRangeChangeListener onRangeChangeListener2 = this.onRangeChangeListener;
            if (onRangeChangeListener2 != null) {
                if (this.dragThumb == Thumb.POSITION) {
                    onRangeChangeListener2.onEndPositionDrag(this.dragPosition);
                } else {
                    onRangeChangeListener2.onRangeDragEnd(getMinValue(), getMaxValue(), getDuration(), this.dragThumb);
                    setOutputQuality(this.onRangeChangeListener.getQuality(getClipDuration(), getDuration()));
                }
                this.lastDragThumb = this.dragThumb;
                this.dragEndTimeMs = System.currentTimeMillis();
                this.dragThumb = null;
                invalidate();
            }
            return true;
        } else {
            if (actionMasked == 3) {
                this.dragThumb = null;
            }
            return true;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsRangeSelectorView$1 */
    /* loaded from: classes5.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$video$videoconverter$VideoThumbnailsRangeSelectorView$Thumb;

        static {
            int[] iArr = new int[Thumb.values().length];
            $SwitchMap$org$thoughtcrime$securesms$video$videoconverter$VideoThumbnailsRangeSelectorView$Thumb = iArr;
            try {
                iArr[Thumb.POSITION.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$video$videoconverter$VideoThumbnailsRangeSelectorView$Thumb[Thumb.MIN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$video$videoconverter$VideoThumbnailsRangeSelectorView$Thumb[Thumb.MAX.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private void setOutputQuality(Quality quality) {
        if (!Objects.equals(this.outputQuality, quality)) {
            if (this.outputQuality == null) {
                this.qualityAvailableTimeMs = System.currentTimeMillis();
            }
            this.outputQuality = quality;
            invalidate();
        }
    }

    private Thumb closestThumb(float f) {
        int i = this.right;
        int i2 = this.left;
        float f2 = ((float) (i + i2)) / 2.0f;
        Thumb thumb = f < f2 ? Thumb.MIN : Thumb.MAX;
        if (f < f2) {
            i = i2;
        }
        if (Math.abs(f - ((float) i)) < ((float) this.thumbTouchRadius)) {
            return thumb;
        }
        return null;
    }

    private long pixelToDuration(float f) {
        return (long) ((f / ((float) getDrawableWidth())) * ((float) getDuration()));
    }

    private int getDrawableWidth() {
        return (getWidth() - getPaddingLeft()) - getPaddingRight();
    }

    private int getDrawableHeight() {
        return (getHeight() - getPaddingBottom()) - getPaddingTop();
    }

    public void setRange(long j, long j2) {
        if (getDuration() > 0) {
            setMinMax(j, j2, Thumb.MIN);
            setMinMax(j, j2, Thumb.MAX);
            return;
        }
        this.externalMinValue = Long.valueOf(j);
        this.externalMaxValue = Long.valueOf(j2);
    }

    public void setTimeLimit(int i, TimeUnit timeUnit) {
        this.maximumSelectableRangeMicros = timeUnit.toMicros((long) i);
    }

    /* loaded from: classes5.dex */
    public static final class Quality {
        private final long fileSize;
        private final int qualityRange;

        public Quality(long j, int i) {
            this.fileSize = j;
            this.qualityRange = i;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Quality)) {
                return false;
            }
            Quality quality = (Quality) obj;
            if (this.fileSize == quality.fileSize && this.qualityRange == quality.qualityRange) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            long j = this.fileSize;
            return (((int) (j ^ (j >>> 32))) * 31) + this.qualityRange;
        }
    }
}
