package org.thoughtcrime.securesms.video.exo;

import android.app.Application;
import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.net.ChunkedDataFetcher;
import org.thoughtcrime.securesms.video.exo.GiphyMp4Cache;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public class ChunkedDataSource implements DataSource {
    private GiphyMp4Cache.ReadData cacheEntry;
    private DataSpec dataSpec;
    private volatile Exception exception;
    private final OkHttpClient okHttpClient;
    private final TransferListener transferListener;

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void addTransferListener(TransferListener transferListener) {
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public /* bridge */ /* synthetic */ Map getResponseHeaders() {
        return DataSource.CC.$default$getResponseHeaders(this);
    }

    public ChunkedDataSource(OkHttpClient okHttpClient, TransferListener transferListener) {
        this.okHttpClient = okHttpClient;
        this.transferListener = transferListener;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public long open(final DataSpec dataSpec) throws IOException {
        this.dataSpec = dataSpec;
        this.exception = null;
        GiphyMp4Cache.ReadData readData = this.cacheEntry;
        if (readData != null) {
            readData.release();
        }
        try {
            Thread.sleep((long) (Math.random() * 750.0d));
        } catch (InterruptedException unused) {
        }
        final Application application = ApplicationDependencies.getApplication();
        final GiphyMp4Cache giphyMp4Cache = ApplicationDependencies.getGiphyMp4Cache();
        GiphyMp4Cache.ReadData read = giphyMp4Cache.read(application, dataSpec.uri);
        this.cacheEntry = read;
        if (read == null) {
            final CountDownLatch countDownLatch = new CountDownLatch(1);
            new ChunkedDataFetcher(this.okHttpClient).fetch(this.dataSpec.uri.toString(), dataSpec.length, new ChunkedDataFetcher.Callback() { // from class: org.thoughtcrime.securesms.video.exo.ChunkedDataSource.1
                @Override // org.thoughtcrime.securesms.net.ChunkedDataFetcher.Callback
                public void onSuccess(InputStream inputStream) {
                    try {
                        ChunkedDataSource.this.cacheEntry = giphyMp4Cache.write(application, dataSpec.uri, inputStream);
                    } catch (IOException e) {
                        ChunkedDataSource.this.exception = e;
                    }
                    countDownLatch.countDown();
                }

                @Override // org.thoughtcrime.securesms.net.ChunkedDataFetcher.Callback
                public void onFailure(Exception exc) {
                    ChunkedDataSource.this.exception = exc;
                    countDownLatch.countDown();
                }
            });
            try {
                countDownLatch.await(30, TimeUnit.SECONDS);
                if (this.exception != null) {
                    throw new IOException(this.exception);
                } else if (this.cacheEntry != null) {
                    TransferListener transferListener = this.transferListener;
                    if (transferListener != null) {
                        transferListener.onTransferStart(this, dataSpec, false);
                    }
                    long j = dataSpec.length;
                    if (j != -1 && j - dataSpec.position <= 0) {
                        throw new EOFException("No more data");
                    }
                } else {
                    throw new IOException("Timed out waiting for download.");
                }
            } catch (InterruptedException e) {
                throw new IOException(e);
            }
        }
        return this.cacheEntry.getLength();
    }

    @Override // com.google.android.exoplayer2.upstream.DataReader
    public int read(byte[] bArr, int i, int i2) throws IOException {
        TransferListener transferListener;
        int read = this.cacheEntry.getInputStream().read(bArr, i, i2);
        if (read > 0 && (transferListener = this.transferListener) != null) {
            transferListener.onBytesTransferred(this, this.dataSpec, false, read);
        }
        return read;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Uri getUri() {
        return this.dataSpec.uri;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void close() throws IOException {
        GiphyMp4Cache.ReadData readData = this.cacheEntry;
        if (readData != null) {
            readData.release();
        }
        this.cacheEntry = null;
    }
}
