package org.thoughtcrime.securesms.video.videoconverter.muxer;

import j$.util.concurrent.ConcurrentHashMap;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import org.mp4parser.Box;
import org.mp4parser.boxes.iso14496.part12.ChunkOffsetBox;
import org.mp4parser.boxes.iso14496.part12.CompositionTimeToSample;
import org.mp4parser.boxes.iso14496.part12.FileTypeBox;
import org.mp4parser.boxes.iso14496.part12.MediaHeaderBox;
import org.mp4parser.boxes.iso14496.part12.MovieBox;
import org.mp4parser.boxes.iso14496.part12.MovieHeaderBox;
import org.mp4parser.boxes.iso14496.part12.SampleSizeBox;
import org.mp4parser.boxes.iso14496.part12.SampleTableBox;
import org.mp4parser.boxes.iso14496.part12.SampleToChunkBox;
import org.mp4parser.boxes.iso14496.part12.SyncSampleBox;
import org.mp4parser.boxes.iso14496.part12.TimeToSampleBox;
import org.mp4parser.boxes.iso14496.part12.TrackBox;
import org.mp4parser.boxes.iso14496.part12.TrackHeaderBox;
import org.mp4parser.streaming.StreamingSample;
import org.mp4parser.streaming.StreamingTrack;
import org.mp4parser.streaming.extensions.CompositionTimeSampleExtension;
import org.mp4parser.streaming.extensions.CompositionTimeTrackExtension;
import org.mp4parser.streaming.extensions.SampleFlagsSampleExtension;
import org.mp4parser.streaming.extensions.TrackIdTrackExtension;
import org.mp4parser.streaming.output.SampleSink;
import org.mp4parser.streaming.output.mp4.DefaultBoxes;
import org.mp4parser.tools.CastUtils;
import org.mp4parser.tools.Mp4Arrays;
import org.mp4parser.tools.Mp4Math;
import org.mp4parser.tools.Path;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MmsSmsColumns;

/* loaded from: classes5.dex */
public final class Mp4Writer extends DefaultBoxes implements SampleSink {
    private static final String TAG;
    private long bytesWritten = 0;
    private final Map<StreamingTrack, Queue<ChunkContainer>> chunkBuffers = new ConcurrentHashMap();
    private final Map<StreamingTrack, Long> chunkNumbers = new HashMap();
    private final Date creationTime = new Date();
    private final Map<StreamingTrack, Long> nextChunkCreateStartTime = new ConcurrentHashMap();
    private final Map<StreamingTrack, Long> nextChunkWriteStartTime = new ConcurrentHashMap();
    private final Map<StreamingTrack, Long> nextSampleStartTime = new HashMap();
    private final Map<StreamingTrack, List<StreamingSample>> sampleBuffers = new HashMap();
    private final Map<StreamingTrack, Long> sampleNumbers = new HashMap();
    private final WritableByteChannel sink;
    private final List<StreamingTrack> source;
    private final Map<StreamingTrack, TrackBox> trackBoxes = new HashMap();

    public Mp4Writer(List<StreamingTrack> list, WritableByteChannel writableByteChannel) throws IOException {
        this.source = new ArrayList(list);
        this.sink = writableByteChannel;
        HashSet hashSet = new HashSet();
        for (StreamingTrack streamingTrack : list) {
            streamingTrack.setSampleSink(this);
            this.chunkNumbers.put(streamingTrack, 1L);
            this.sampleNumbers.put(streamingTrack, 1L);
            this.nextSampleStartTime.put(streamingTrack, 0L);
            this.nextChunkCreateStartTime.put(streamingTrack, 0L);
            this.nextChunkWriteStartTime.put(streamingTrack, 0L);
            this.sampleBuffers.put(streamingTrack, new ArrayList());
            this.chunkBuffers.put(streamingTrack, new LinkedList());
            if (streamingTrack.getTrackExtension(TrackIdTrackExtension.class) != null) {
                TrackIdTrackExtension trackIdTrackExtension = (TrackIdTrackExtension) streamingTrack.getTrackExtension(TrackIdTrackExtension.class);
                if (!hashSet.contains(Long.valueOf(trackIdTrackExtension.getTrackId()))) {
                    hashSet.add(Long.valueOf(trackIdTrackExtension.getTrackId()));
                } else {
                    throw new MuxingException("There may not be two tracks with the same trackID within one file");
                }
            }
        }
        for (StreamingTrack streamingTrack2 : list) {
            if (streamingTrack2.getTrackExtension(TrackIdTrackExtension.class) == null) {
                Iterator it = hashSet.iterator();
                long j = 0;
                while (it.hasNext()) {
                    j = Math.max(((Long) it.next()).longValue(), j);
                }
                TrackIdTrackExtension trackIdTrackExtension2 = new TrackIdTrackExtension(j + 1);
                hashSet.add(Long.valueOf(trackIdTrackExtension2.getTrackId()));
                streamingTrack2.addTrackExtension(trackIdTrackExtension2);
            }
        }
        LinkedList linkedList = new LinkedList();
        linkedList.add("isom");
        linkedList.add("mp42");
        write(writableByteChannel, new FileTypeBox("mp42", 0, linkedList));
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        for (StreamingTrack streamingTrack : this.source) {
            writeChunkContainer(createChunkContainer(streamingTrack));
            streamingTrack.close();
        }
        write(this.sink, createMoov());
    }

    private Box createMoov() {
        MovieBox movieBox = new MovieBox();
        MovieHeaderBox createMvhd = createMvhd();
        movieBox.addBox(createMvhd);
        for (StreamingTrack streamingTrack : this.source) {
            TrackBox trackBox = this.trackBoxes.get(streamingTrack);
            MediaHeaderBox mediaHeaderBox = (MediaHeaderBox) Path.getPath(trackBox, "mdia[0]/mdhd[0]");
            mediaHeaderBox.setCreationTime(this.creationTime);
            mediaHeaderBox.setModificationTime(this.creationTime);
            Long l = this.nextSampleStartTime.get(streamingTrack);
            Objects.requireNonNull(l);
            mediaHeaderBox.setDuration(l.longValue());
            mediaHeaderBox.setTimescale(streamingTrack.getTimescale());
            mediaHeaderBox.setLanguage(streamingTrack.getLanguage());
            movieBox.addBox(trackBox);
            Long l2 = this.nextSampleStartTime.get(streamingTrack);
            Objects.requireNonNull(l2);
            double longValue = (double) l2.longValue();
            double timescale = (double) streamingTrack.getTimescale();
            Double.isNaN(longValue);
            Double.isNaN(timescale);
            double d = longValue / timescale;
            double timescale2 = (double) createMvhd.getTimescale();
            Double.isNaN(timescale2);
            ((TrackHeaderBox) Path.getPath(trackBox, "tkhd[0]")).setDuration((long) (timescale2 * d));
        }
        return movieBox;
    }

    private void sortTracks() {
        Collections.sort(this.source, new Comparator() { // from class: org.thoughtcrime.securesms.video.videoconverter.muxer.Mp4Writer$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return Mp4Writer.m3303$r8$lambda$y9KYsN4fR1BCaJgqqkkJr73kzE(Mp4Writer.this, (StreamingTrack) obj, (StreamingTrack) obj2);
            }
        });
    }

    public /* synthetic */ int lambda$sortTracks$0(StreamingTrack streamingTrack, StreamingTrack streamingTrack2) {
        Long l = this.nextChunkWriteStartTime.get(streamingTrack);
        Objects.requireNonNull(l);
        long longValue = l.longValue() * streamingTrack2.getTimescale();
        Long l2 = this.nextChunkWriteStartTime.get(streamingTrack2);
        Objects.requireNonNull(l2);
        return (int) Math.signum((float) (longValue - (l2.longValue() * streamingTrack.getTimescale())));
    }

    @Override // org.mp4parser.streaming.output.mp4.DefaultBoxes
    public MovieHeaderBox createMvhd() {
        MovieHeaderBox movieHeaderBox = new MovieHeaderBox();
        movieHeaderBox.setVersion(1);
        movieHeaderBox.setCreationTime(this.creationTime);
        movieHeaderBox.setModificationTime(this.creationTime);
        long[] jArr = new long[0];
        long j = 0;
        double d = 0.0d;
        for (StreamingTrack streamingTrack : this.source) {
            Long l = this.nextSampleStartTime.get(streamingTrack);
            Objects.requireNonNull(l);
            double longValue = (double) l.longValue();
            double timescale = (double) streamingTrack.getTimescale();
            Double.isNaN(longValue);
            Double.isNaN(timescale);
            d = Math.max(longValue / timescale, d);
            jArr = Mp4Arrays.copyOfAndAppend(jArr, streamingTrack.getTimescale());
            j = Math.max(((TrackIdTrackExtension) streamingTrack.getTrackExtension(TrackIdTrackExtension.class)).getTrackId(), j);
        }
        movieHeaderBox.setTimescale(Mp4Math.lcm(jArr));
        double lcm = (double) Mp4Math.lcm(jArr);
        Double.isNaN(lcm);
        movieHeaderBox.setDuration((long) (lcm * d));
        movieHeaderBox.setNextTrackId(j + 1);
        return movieHeaderBox;
    }

    private void write(WritableByteChannel writableByteChannel, Box... boxArr) throws IOException {
        for (Box box : boxArr) {
            box.getBox(writableByteChannel);
            this.bytesWritten += box.getSize();
        }
    }

    private boolean isChunkReady(StreamingTrack streamingTrack, StreamingSample streamingSample) {
        Long l = this.nextSampleStartTime.get(streamingTrack);
        Objects.requireNonNull(l);
        long longValue = l.longValue();
        Long l2 = this.nextChunkCreateStartTime.get(streamingTrack);
        Objects.requireNonNull(l2);
        return longValue >= l2.longValue() + (streamingTrack.getTimescale() * 2);
    }

    private void writeChunkContainer(ChunkContainer chunkContainer) throws IOException {
        ChunkOffsetBox chunkOffsetBox = (ChunkOffsetBox) Path.getPath(this.trackBoxes.get(chunkContainer.streamingTrack), "mdia[0]/minf[0]/stbl[0]/stco[0]");
        Objects.requireNonNull(chunkOffsetBox);
        chunkOffsetBox.setChunkOffsets(Mp4Arrays.copyOfAndAppend(chunkOffsetBox.getChunkOffsets(), this.bytesWritten + 8));
        write(this.sink, chunkContainer.mdat);
    }

    @Override // org.mp4parser.streaming.output.SampleSink
    public void acceptSample(StreamingSample streamingSample, StreamingTrack streamingTrack) throws IOException {
        if (this.trackBoxes.get(streamingTrack) == null) {
            TrackBox trackBox = new TrackBox();
            trackBox.addBox(createTkhd(streamingTrack));
            trackBox.addBox(createMdia(streamingTrack));
            this.trackBoxes.put(streamingTrack, trackBox);
        }
        if (isChunkReady(streamingTrack, streamingSample)) {
            ChunkContainer createChunkContainer = createChunkContainer(streamingTrack);
            List<StreamingSample> list = this.sampleBuffers.get(streamingTrack);
            Objects.requireNonNull(list);
            list.clear();
            Map<StreamingTrack, Long> map = this.nextChunkCreateStartTime;
            Long l = map.get(streamingTrack);
            Objects.requireNonNull(l);
            map.put(streamingTrack, Long.valueOf(l.longValue() + createChunkContainer.duration));
            Queue<ChunkContainer> queue = this.chunkBuffers.get(streamingTrack);
            Objects.requireNonNull(queue);
            Queue<ChunkContainer> queue2 = queue;
            queue2.add(createChunkContainer);
            if (this.source.get(0) == streamingTrack) {
                while (true) {
                    Map<StreamingTrack, Queue<ChunkContainer>> map2 = this.chunkBuffers;
                    StreamingTrack streamingTrack2 = this.source.get(0);
                    Queue<ChunkContainer> queue3 = map2.get(streamingTrack2);
                    if (queue3.isEmpty()) {
                        break;
                    }
                    ChunkContainer remove = queue3.remove();
                    writeChunkContainer(remove);
                    StringBuilder sb = new StringBuilder();
                    sb.append("write chunk ");
                    sb.append(streamingTrack2.getHandler());
                    sb.append(". duration ");
                    double d = (double) remove.duration;
                    double timescale = (double) streamingTrack2.getTimescale();
                    Double.isNaN(d);
                    Double.isNaN(timescale);
                    sb.append(d / timescale);
                    Log.d(TAG, sb.toString());
                    Long l2 = this.nextChunkWriteStartTime.get(streamingTrack2);
                    Objects.requireNonNull(l2);
                    long longValue = l2.longValue() + remove.duration;
                    this.nextChunkWriteStartTime.put(streamingTrack2, Long.valueOf(longValue));
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append(streamingTrack2.getHandler());
                    sb2.append(" track advanced to ");
                    double d2 = (double) longValue;
                    double timescale2 = (double) streamingTrack2.getTimescale();
                    Double.isNaN(d2);
                    Double.isNaN(timescale2);
                    sb2.append(d2 / timescale2);
                    Log.d(TAG, sb2.toString());
                    sortTracks();
                }
            } else {
                Log.d(TAG, streamingTrack.getHandler() + " track delayed, queue size is " + queue2.size());
            }
        }
        List<StreamingSample> list2 = this.sampleBuffers.get(streamingTrack);
        Objects.requireNonNull(list2);
        list2.add(streamingSample);
        Map<StreamingTrack, Long> map3 = this.nextSampleStartTime;
        Long l3 = map3.get(streamingTrack);
        Objects.requireNonNull(l3);
        map3.put(streamingTrack, Long.valueOf(l3.longValue() + streamingSample.getDuration()));
    }

    private ChunkContainer createChunkContainer(StreamingTrack streamingTrack) {
        List<StreamingSample> list = this.sampleBuffers.get(streamingTrack);
        Objects.requireNonNull(list);
        List<StreamingSample> list2 = list;
        Long l = this.chunkNumbers.get(streamingTrack);
        Objects.requireNonNull(l);
        long longValue = l.longValue();
        this.chunkNumbers.put(streamingTrack, Long.valueOf(longValue + 1));
        ChunkContainer chunkContainer = new ChunkContainer();
        chunkContainer.streamingTrack = streamingTrack;
        chunkContainer.mdat = new Mdat(list2);
        Long l2 = this.nextSampleStartTime.get(streamingTrack);
        Objects.requireNonNull(l2);
        long longValue2 = l2.longValue();
        Long l3 = this.nextChunkCreateStartTime.get(streamingTrack);
        Objects.requireNonNull(l3);
        chunkContainer.duration = longValue2 - l3.longValue();
        SampleTableBox sampleTableBox = (SampleTableBox) Path.getPath(this.trackBoxes.get(streamingTrack), "mdia[0]/minf[0]/stbl[0]");
        Objects.requireNonNull(sampleTableBox);
        SampleToChunkBox sampleToChunkBox = (SampleToChunkBox) Path.getPath(sampleTableBox, "stsc[0]");
        Objects.requireNonNull(sampleToChunkBox);
        if (sampleToChunkBox.getEntries().isEmpty()) {
            ArrayList arrayList = new ArrayList();
            sampleToChunkBox.setEntries(arrayList);
            arrayList.add(new SampleToChunkBox.Entry(longValue, (long) list2.size(), 1));
        } else if (sampleToChunkBox.getEntries().get(sampleToChunkBox.getEntries().size() - 1).getSamplesPerChunk() != ((long) list2.size())) {
            sampleToChunkBox.getEntries().add(new SampleToChunkBox.Entry(longValue, (long) list2.size(), 1));
        }
        Long l4 = this.sampleNumbers.get(streamingTrack);
        Objects.requireNonNull(l4);
        long longValue3 = l4.longValue();
        SampleSizeBox sampleSizeBox = (SampleSizeBox) Path.getPath(sampleTableBox, "stsz[0]");
        Objects.requireNonNull(sampleSizeBox);
        TimeToSampleBox timeToSampleBox = (TimeToSampleBox) Path.getPath(sampleTableBox, "stts[0]");
        Objects.requireNonNull(timeToSampleBox);
        SyncSampleBox syncSampleBox = (SyncSampleBox) Path.getPath(sampleTableBox, "stss[0]");
        CompositionTimeToSample compositionTimeToSample = (CompositionTimeToSample) Path.getPath(sampleTableBox, "ctts[0]");
        if (streamingTrack.getTrackExtension(CompositionTimeTrackExtension.class) != null && compositionTimeToSample == null) {
            compositionTimeToSample = new CompositionTimeToSample();
            compositionTimeToSample.setEntries(new ArrayList());
            ArrayList arrayList2 = new ArrayList(sampleTableBox.getBoxes());
            arrayList2.add(arrayList2.indexOf(timeToSampleBox), compositionTimeToSample);
        }
        long[] jArr = new long[list2.size()];
        int i = 0;
        for (StreamingSample streamingSample : list2) {
            int i2 = i + 1;
            jArr[i] = (long) streamingSample.getContent().limit();
            if (compositionTimeToSample != null) {
                compositionTimeToSample.getEntries().add(new CompositionTimeToSample.Entry(1, CastUtils.l2i(((CompositionTimeSampleExtension) streamingSample.getSampleExtension(CompositionTimeSampleExtension.class)).getCompositionTimeOffset())));
            }
            if (timeToSampleBox.getEntries().isEmpty()) {
                ArrayList arrayList3 = new ArrayList(timeToSampleBox.getEntries());
                arrayList3.add(new TimeToSampleBox.Entry(1, streamingSample.getDuration()));
                timeToSampleBox.setEntries(arrayList3);
            } else {
                TimeToSampleBox.Entry entry = timeToSampleBox.getEntries().get(timeToSampleBox.getEntries().size() - 1);
                if (entry.getDelta() == streamingSample.getDuration()) {
                    entry.setCount(entry.getCount() + 1);
                } else {
                    timeToSampleBox.getEntries().add(new TimeToSampleBox.Entry(1, streamingSample.getDuration()));
                }
            }
            SampleFlagsSampleExtension sampleFlagsSampleExtension = (SampleFlagsSampleExtension) streamingSample.getSampleExtension(SampleFlagsSampleExtension.class);
            if (sampleFlagsSampleExtension != null && sampleFlagsSampleExtension.isSyncSample()) {
                if (syncSampleBox == null) {
                    syncSampleBox = new SyncSampleBox();
                    sampleTableBox.addBox(syncSampleBox);
                }
                syncSampleBox.setSampleNumber(Mp4Arrays.copyOfAndAppend(syncSampleBox.getSampleNumber(), longValue3));
            }
            longValue3++;
            i = i2;
            list2 = list2;
            chunkContainer = chunkContainer;
        }
        sampleSizeBox.setSampleSizes(Mp4Arrays.copyOfAndAppend(sampleSizeBox.getSampleSizes(), jArr));
        this.sampleNumbers.put(streamingTrack, Long.valueOf(longValue3));
        list2.clear();
        StringBuilder sb = new StringBuilder();
        sb.append("chunk container created for ");
        sb.append(streamingTrack.getHandler());
        sb.append(". mdat size: ");
        sb.append(chunkContainer.mdat.size);
        sb.append(". chunk duration is ");
        double d = (double) chunkContainer.duration;
        double timescale = (double) streamingTrack.getTimescale();
        Double.isNaN(d);
        Double.isNaN(timescale);
        sb.append(d / timescale);
        Log.d(TAG, sb.toString());
        return chunkContainer;
    }

    @Override // org.mp4parser.streaming.output.mp4.DefaultBoxes
    protected Box createMdhd(StreamingTrack streamingTrack) {
        MediaHeaderBox mediaHeaderBox = new MediaHeaderBox();
        mediaHeaderBox.setCreationTime(this.creationTime);
        mediaHeaderBox.setModificationTime(this.creationTime);
        mediaHeaderBox.setTimescale(streamingTrack.getTimescale());
        mediaHeaderBox.setLanguage(streamingTrack.getLanguage());
        return mediaHeaderBox;
    }

    /* loaded from: classes5.dex */
    public class Mdat implements Box {
        final ArrayList<StreamingSample> samples;
        long size = 8;

        @Override // org.mp4parser.Box
        public String getType() {
            return "mdat";
        }

        Mdat(List<StreamingSample> list) {
            Mp4Writer.this = r5;
            this.samples = new ArrayList<>(list);
            for (StreamingSample streamingSample : list) {
                this.size += (long) streamingSample.getContent().limit();
            }
        }

        @Override // org.mp4parser.Box
        public long getSize() {
            return this.size;
        }

        @Override // org.mp4parser.Box
        public void getBox(WritableByteChannel writableByteChannel) throws IOException {
            long j = this.size;
            writableByteChannel.write(ByteBuffer.wrap(new byte[]{(byte) ((int) ((MmsSmsColumns.Types.ENCRYPTION_MASK & j) >> 24)), (byte) ((int) ((16711680 & j) >> 16)), (byte) ((int) ((65280 & j) >> 8)), (byte) ((int) (j & 255)), 109, 100, 97, 116}));
            Iterator<StreamingSample> it = this.samples.iterator();
            while (it.hasNext()) {
                writableByteChannel.write((ByteBuffer) it.next().getContent().rewind());
            }
        }
    }

    /* loaded from: classes5.dex */
    public class ChunkContainer {
        long duration;
        Mdat mdat;
        StreamingTrack streamingTrack;

        private ChunkContainer() {
            Mp4Writer.this = r1;
        }
    }
}
