package org.thoughtcrime.securesms.video.exo;

import android.content.Context;
import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public class BlobDataSource implements DataSource {
    private final Context context;
    private DataSpec dataSpec;
    private InputStream inputStream;
    private final TransferListener listener;

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void addTransferListener(TransferListener transferListener) {
    }

    public BlobDataSource(Context context, TransferListener transferListener) {
        this.context = context.getApplicationContext();
        this.listener = transferListener;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public long open(DataSpec dataSpec) throws IOException {
        this.dataSpec = dataSpec;
        this.inputStream = BlobProvider.getInstance().getStream(this.context, dataSpec.uri, dataSpec.position);
        TransferListener transferListener = this.listener;
        if (transferListener != null) {
            transferListener.onTransferStart(this, dataSpec, false);
        }
        long unwrapLong = unwrapLong(BlobProvider.getFileSize(dataSpec.uri));
        if (unwrapLong == 0) {
            unwrapLong = BlobProvider.getInstance().calculateFileSize(this.context, dataSpec.uri);
        }
        long j = dataSpec.position;
        if (unwrapLong - j > 0) {
            return unwrapLong - j;
        }
        throw new EOFException("No more data");
    }

    private long unwrapLong(Long l) {
        if (l == null) {
            return 0;
        }
        return l.longValue();
    }

    @Override // com.google.android.exoplayer2.upstream.DataReader
    public int read(byte[] bArr, int i, int i2) throws IOException {
        TransferListener transferListener;
        int read = this.inputStream.read(bArr, i, i2);
        if (read > 0 && (transferListener = this.listener) != null) {
            transferListener.onBytesTransferred(this, this.dataSpec, false, read);
        }
        return read;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Uri getUri() {
        return this.dataSpec.uri;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Map<String, List<String>> getResponseHeaders() {
        return Collections.emptyMap();
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void close() throws IOException {
        this.inputStream.close();
    }
}
