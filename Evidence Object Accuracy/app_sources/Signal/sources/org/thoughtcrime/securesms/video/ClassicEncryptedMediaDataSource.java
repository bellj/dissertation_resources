package org.thoughtcrime.securesms.video;

import android.media.MediaDataSource;
import androidx.recyclerview.widget.RecyclerView;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.ClassicDecryptingPartInputStream;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes5.dex */
public final class ClassicEncryptedMediaDataSource extends MediaDataSource {
    private final AttachmentSecret attachmentSecret;
    private final long length;
    private final File mediaFile;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public ClassicEncryptedMediaDataSource(AttachmentSecret attachmentSecret, File file, long j) {
        this.attachmentSecret = attachmentSecret;
        this.mediaFile = file;
        this.length = j;
    }

    @Override // android.media.MediaDataSource
    public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
        InputStream createFor = ClassicDecryptingPartInputStream.createFor(this.attachmentSecret, this.mediaFile);
        try {
            byte[] bArr2 = new byte[RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT];
            while (j > 0) {
                int read = createFor.read(bArr2, 0, Util.toIntExact(Math.min((long) RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT, j)));
                if (read == -1) {
                    createFor.close();
                    return -1;
                }
                j -= (long) read;
            }
            int read2 = createFor.read(bArr, i, i2);
            createFor.close();
            return read2;
        } catch (Throwable th) {
            if (createFor != null) {
                try {
                    createFor.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // android.media.MediaDataSource
    public long getSize() {
        return this.length;
    }
}
