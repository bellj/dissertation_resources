package org.thoughtcrime.securesms.video.videoconverter;

/* loaded from: classes5.dex */
public final class Preconditions {
    private Preconditions() {
    }

    public static void checkState(String str, boolean z) {
        if (!z) {
            throw new IllegalStateException(str);
        }
    }
}
