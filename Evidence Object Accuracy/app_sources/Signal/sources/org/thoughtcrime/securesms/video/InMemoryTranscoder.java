package org.thoughtcrime.securesms.video;

import android.content.Context;
import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import java.io.Closeable;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.MemoryFileDescriptor;
import org.thoughtcrime.securesms.video.VideoBitRateCalculator;

/* loaded from: classes5.dex */
public final class InMemoryTranscoder implements Closeable {
    private static final String TAG = Log.tag(InMemoryTranscoder.class);
    private final Context context;
    private final MediaDataSource dataSource;
    private final long duration;
    private final long fileSizeEstimate;
    private final long inSize;
    private final int inputBitRate;
    private MemoryFileDescriptor memoryFile;
    private final long memoryFileEstimate;
    private final TranscoderOptions options;
    private final VideoBitRateCalculator.Quality targetQuality;
    private final boolean transcodeRequired;
    private final long upperSizeLimit;

    /* loaded from: classes5.dex */
    public interface Progress {
        void onProgress(int i);
    }

    public InMemoryTranscoder(Context context, MediaDataSource mediaDataSource, TranscoderOptions transcoderOptions, long j) throws IOException, VideoSourceException {
        this.context = context;
        this.dataSource = mediaDataSource;
        this.options = transcoderOptions;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(mediaDataSource);
            long size = mediaDataSource.getSize();
            this.inSize = size;
            long duration = getDuration(mediaMetadataRetriever);
            this.duration = duration;
            int bitRate = VideoBitRateCalculator.bitRate(size, duration);
            this.inputBitRate = bitRate;
            VideoBitRateCalculator.Quality targetQuality = new VideoBitRateCalculator(j).getTargetQuality(duration, bitRate);
            this.targetQuality = targetQuality;
            this.upperSizeLimit = j;
            double d = (double) bitRate;
            double targetTotalBitRate = (double) targetQuality.getTargetTotalBitRate();
            Double.isNaN(targetTotalBitRate);
            boolean z = d >= targetTotalBitRate * 1.2d || size > j || containsLocation(mediaMetadataRetriever) || transcoderOptions != null;
            this.transcodeRequired = z;
            if (!z) {
                Log.i(TAG, "Video is within 20% of target bitrate, below the size limit, contained no location metadata or custom options.");
            }
            long fileSizeEstimate = targetQuality.getFileSizeEstimate();
            this.fileSizeEstimate = fileSizeEstimate;
            double d2 = (double) fileSizeEstimate;
            Double.isNaN(d2);
            this.memoryFileEstimate = (long) (d2 * 1.1d);
        } catch (RuntimeException e) {
            Log.w(TAG, "Unable to read datasource", e);
            throw new VideoSourceException("Unable to read datasource", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x01b0  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x01c4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.thoughtcrime.securesms.mms.MediaStream transcode(org.thoughtcrime.securesms.video.InMemoryTranscoder.Progress r24, org.thoughtcrime.securesms.video.TranscoderCancelationSignal r25) throws java.io.IOException, org.thoughtcrime.securesms.video.videoconverter.EncodingException, org.thoughtcrime.securesms.video.VideoSizeException {
        /*
        // Method dump skipped, instructions count: 475
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.InMemoryTranscoder.transcode(org.thoughtcrime.securesms.video.InMemoryTranscoder$Progress, org.thoughtcrime.securesms.video.TranscoderCancelationSignal):org.thoughtcrime.securesms.mms.MediaStream");
    }

    public static /* synthetic */ boolean lambda$transcode$0(Progress progress, TranscoderCancelationSignal transcoderCancelationSignal, int i) {
        progress.onProgress(i);
        return transcoderCancelationSignal != null && transcoderCancelationSignal.isCanceled();
    }

    public boolean isTranscodeRequired() {
        return this.transcodeRequired;
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        MemoryFileDescriptor memoryFileDescriptor = this.memoryFile;
        if (memoryFileDescriptor != null) {
            memoryFileDescriptor.close();
        }
    }

    private static long getDuration(MediaMetadataRetriever mediaMetadataRetriever) throws VideoSourceException {
        String extractMetadata = mediaMetadataRetriever.extractMetadata(9);
        if (extractMetadata != null) {
            try {
                long parseLong = Long.parseLong(extractMetadata);
                if (parseLong > 0) {
                    return parseLong;
                }
                throw new VideoSourceException("Cannot determine duration of video, meta data: " + extractMetadata);
            } catch (NumberFormatException e) {
                throw new VideoSourceException("Cannot determine duration of video, meta data: " + extractMetadata, e);
            }
        } else {
            throw new VideoSourceException("Cannot determine duration of video, null meta data");
        }
    }

    private static boolean containsLocation(MediaMetadataRetriever mediaMetadataRetriever) {
        return mediaMetadataRetriever.extractMetadata(23) != null;
    }
}
