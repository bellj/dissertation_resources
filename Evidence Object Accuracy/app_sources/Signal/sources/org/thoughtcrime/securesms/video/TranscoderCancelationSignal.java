package org.thoughtcrime.securesms.video;

/* loaded from: classes5.dex */
public interface TranscoderCancelationSignal {
    boolean isCanceled();
}
