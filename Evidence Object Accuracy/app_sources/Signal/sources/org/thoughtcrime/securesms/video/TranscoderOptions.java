package org.thoughtcrime.securesms.video;

/* loaded from: classes5.dex */
public final class TranscoderOptions {
    final long endTimeUs;
    final long startTimeUs;

    public TranscoderOptions(long j, long j2) {
        this.startTimeUs = j;
        this.endTimeUs = j2;
    }
}
