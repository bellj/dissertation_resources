package org.thoughtcrime.securesms.video;

import android.media.MediaDataSource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;

/* loaded from: classes5.dex */
public final class ModernEncryptedMediaDataSource extends MediaDataSource {
    private final AttachmentSecret attachmentSecret;
    private final long length;
    private final File mediaFile;
    private final byte[] random;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    public ModernEncryptedMediaDataSource(AttachmentSecret attachmentSecret, File file, byte[] bArr, long j) {
        this.attachmentSecret = attachmentSecret;
        this.mediaFile = file;
        this.random = bArr;
        this.length = j;
    }

    @Override // android.media.MediaDataSource
    public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
        if (j >= this.length) {
            return -1;
        }
        InputStream createInputStream = createInputStream(j);
        int i3 = 0;
        while (i2 > 0) {
            try {
                int read = createInputStream.read(bArr, i, i2);
                if (read != -1) {
                    i2 -= read;
                    i += read;
                    i3 += read;
                } else if (i3 == 0) {
                    createInputStream.close();
                    return -1;
                } else {
                    createInputStream.close();
                    return i3;
                }
            } catch (Throwable th) {
                if (createInputStream != null) {
                    try {
                        createInputStream.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        if (createInputStream != null) {
            createInputStream.close();
        }
        return i3;
    }

    @Override // android.media.MediaDataSource
    public long getSize() {
        return this.length;
    }

    private InputStream createInputStream(long j) throws IOException {
        byte[] bArr = this.random;
        if (bArr == null) {
            return ModernDecryptingPartInputStream.createFor(this.attachmentSecret, this.mediaFile, j);
        }
        return ModernDecryptingPartInputStream.createFor(this.attachmentSecret, bArr, this.mediaFile, j);
    }
}
