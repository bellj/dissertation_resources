package org.thoughtcrime.securesms.video.videoconverter;

/* loaded from: classes5.dex */
public final class EncodingException extends Exception {
    public EncodingException(String str) {
        super(str);
    }

    public EncodingException(String str, Exception exc) {
        super(str, exc);
    }
}
