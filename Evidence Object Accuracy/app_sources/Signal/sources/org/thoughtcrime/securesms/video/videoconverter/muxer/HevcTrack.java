package org.thoughtcrime.securesms.video.videoconverter.muxer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.boxes.iso14496.part12.SampleDescriptionBox;
import org.mp4parser.boxes.iso14496.part15.HevcConfigurationBox;
import org.mp4parser.boxes.iso14496.part15.HevcDecoderConfigurationRecord;
import org.mp4parser.boxes.sampleentry.VisualSampleEntry;
import org.mp4parser.muxer.tracks.CleanInputStream;
import org.mp4parser.muxer.tracks.h265.H265NalUnitHeader;
import org.mp4parser.muxer.tracks.h265.SequenceParameterSetRbsp;
import org.mp4parser.streaming.StreamingSample;
import org.mp4parser.streaming.extensions.DimensionTrackExtension;
import org.mp4parser.streaming.extensions.SampleFlagsSampleExtension;
import org.mp4parser.streaming.input.AbstractStreamingTrack;
import org.mp4parser.streaming.input.StreamingSampleImpl;
import org.mp4parser.tools.ByteBufferByteChannel;
import org.mp4parser.tools.IsoTypeReader;

/* loaded from: classes5.dex */
public abstract class HevcTrack extends AbstractStreamingTrack {
    private final ArrayList<ByteBuffer> bufferedNals = new ArrayList<>();
    private long currentPresentationTimeUs;
    private boolean isIdr = true;
    private final SampleDescriptionBox stsd;
    private boolean vclNalUnitSeenInAU;

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() {
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public String getHandler() {
        return "vide";
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public String getLanguage() {
        return "```";
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public long getTimescale() {
        return 90000;
    }

    public HevcTrack(List<ByteBuffer> list) throws IOException {
        ArrayList<ByteBuffer> arrayList = new ArrayList<>();
        ArrayList<ByteBuffer> arrayList2 = new ArrayList<>();
        ArrayList<ByteBuffer> arrayList3 = new ArrayList<>();
        SequenceParameterSetRbsp sequenceParameterSetRbsp = null;
        for (ByteBuffer byteBuffer : list) {
            H265NalUnitHeader nalUnitHeader = getNalUnitHeader(byteBuffer);
            byteBuffer.position(0);
            switch (nalUnitHeader.nalUnitType) {
                case 32:
                    arrayList3.add(byteBuffer.duplicate());
                    break;
                case 33:
                    arrayList.add(byteBuffer.duplicate());
                    byteBuffer.position(2);
                    sequenceParameterSetRbsp = new SequenceParameterSetRbsp(new CleanInputStream(Channels.newInputStream(new ByteBufferByteChannel(byteBuffer.slice()))));
                    break;
                case 34:
                    arrayList2.add(byteBuffer.duplicate());
                    break;
            }
        }
        SampleDescriptionBox sampleDescriptionBox = new SampleDescriptionBox();
        this.stsd = sampleDescriptionBox;
        sampleDescriptionBox.addBox(createSampleEntry(arrayList, arrayList2, arrayList3, sequenceParameterSetRbsp));
    }

    @Override // org.mp4parser.streaming.StreamingTrack
    public SampleDescriptionBox getSampleDescriptionBox() {
        return this.stsd;
    }

    public void consumeLastNal() throws IOException {
        wrapUp(this.bufferedNals, this.currentPresentationTimeUs);
    }

    public void consumeNal(ByteBuffer byteBuffer, long j) throws IOException {
        H265NalUnitHeader nalUnitHeader = getNalUnitHeader(byteBuffer);
        boolean isVcl = isVcl(nalUnitHeader);
        if (this.vclNalUnitSeenInAU) {
            if (!isVcl) {
                switch (nalUnitHeader.nalUnitType) {
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 39:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 48:
                    case 49:
                    case 50:
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                        wrapUp(this.bufferedNals, j);
                        break;
                }
            } else if ((byteBuffer.get(2) & Byte.MIN_VALUE) != 0) {
                wrapUp(this.bufferedNals, j);
            }
        }
        switch (nalUnitHeader.nalUnitType) {
            case 32:
            case 33:
            case 34:
            case 35:
            case 36:
            case 37:
            case 38:
                break;
            default:
                this.bufferedNals.add(byteBuffer);
                break;
        }
        if (isVcl) {
            int i = nalUnitHeader.nalUnitType;
            this.isIdr = i == 19 || i == 20;
            this.vclNalUnitSeenInAU = true;
        }
    }

    private void wrapUp(List<ByteBuffer> list, long j) throws IOException {
        this.currentPresentationTimeUs = j;
        StreamingSample streamingSampleImpl = new StreamingSampleImpl(list, (getTimescale() * Math.max(0L, j - this.currentPresentationTimeUs)) / 1000000);
        SampleFlagsSampleExtension sampleFlagsSampleExtension = new SampleFlagsSampleExtension();
        sampleFlagsSampleExtension.setSampleIsNonSyncSample(!this.isIdr);
        streamingSampleImpl.addSampleExtension(sampleFlagsSampleExtension);
        this.sampleSink.acceptSample(streamingSampleImpl, this);
        this.vclNalUnitSeenInAU = false;
        this.isIdr = true;
        list.clear();
    }

    private static H265NalUnitHeader getNalUnitHeader(ByteBuffer byteBuffer) {
        byteBuffer.position(0);
        int readUInt16 = IsoTypeReader.readUInt16(byteBuffer);
        H265NalUnitHeader h265NalUnitHeader = new H265NalUnitHeader();
        h265NalUnitHeader.forbiddenZeroFlag = (32768 & readUInt16) >> 15;
        h265NalUnitHeader.nalUnitType = (readUInt16 & 32256) >> 9;
        h265NalUnitHeader.nuhLayerId = (readUInt16 & 504) >> 3;
        h265NalUnitHeader.nuhTemporalIdPlusOne = readUInt16 & 7;
        return h265NalUnitHeader;
    }

    private VisualSampleEntry createSampleEntry(ArrayList<ByteBuffer> arrayList, ArrayList<ByteBuffer> arrayList2, ArrayList<ByteBuffer> arrayList3, SequenceParameterSetRbsp sequenceParameterSetRbsp) {
        VisualSampleEntry visualSampleEntry = new VisualSampleEntry("hvc1");
        visualSampleEntry.setDataReferenceIndex(1);
        visualSampleEntry.setDepth(24);
        visualSampleEntry.setFrameCount(1);
        visualSampleEntry.setHorizresolution(72.0d);
        visualSampleEntry.setVertresolution(72.0d);
        visualSampleEntry.setCompressorname("HEVC Coding");
        HevcConfigurationBox hevcConfigurationBox = new HevcConfigurationBox();
        hevcConfigurationBox.getHevcDecoderConfigurationRecord().setConfigurationVersion(1);
        if (sequenceParameterSetRbsp != null) {
            visualSampleEntry.setWidth(sequenceParameterSetRbsp.pic_width_in_luma_samples);
            visualSampleEntry.setHeight(sequenceParameterSetRbsp.pic_height_in_luma_samples);
            if (((DimensionTrackExtension) getTrackExtension(DimensionTrackExtension.class)) == null) {
                addTrackExtension(new DimensionTrackExtension(sequenceParameterSetRbsp.pic_width_in_luma_samples, sequenceParameterSetRbsp.pic_height_in_luma_samples));
            }
            HevcDecoderConfigurationRecord hevcDecoderConfigurationRecord = hevcConfigurationBox.getHevcDecoderConfigurationRecord();
            hevcDecoderConfigurationRecord.setChromaFormat(sequenceParameterSetRbsp.chroma_format_idc);
            hevcDecoderConfigurationRecord.setGeneral_profile_idc(sequenceParameterSetRbsp.general_profile_idc);
            hevcDecoderConfigurationRecord.setGeneral_profile_compatibility_flags(sequenceParameterSetRbsp.general_profile_compatibility_flags);
            hevcDecoderConfigurationRecord.setGeneral_constraint_indicator_flags(sequenceParameterSetRbsp.general_constraint_indicator_flags);
            hevcDecoderConfigurationRecord.setGeneral_level_idc(sequenceParameterSetRbsp.general_level_idc);
            hevcDecoderConfigurationRecord.setGeneral_tier_flag(sequenceParameterSetRbsp.general_tier_flag);
            hevcDecoderConfigurationRecord.setGeneral_profile_space(sequenceParameterSetRbsp.general_profile_space);
            hevcDecoderConfigurationRecord.setBitDepthChromaMinus8(sequenceParameterSetRbsp.bit_depth_chroma_minus8);
            hevcDecoderConfigurationRecord.setBitDepthLumaMinus8(sequenceParameterSetRbsp.bit_depth_luma_minus8);
            hevcDecoderConfigurationRecord.setTemporalIdNested(sequenceParameterSetRbsp.sps_temporal_id_nesting_flag);
        }
        hevcConfigurationBox.getHevcDecoderConfigurationRecord().setLengthSizeMinusOne(3);
        HevcDecoderConfigurationRecord.Array array = new HevcDecoderConfigurationRecord.Array();
        array.array_completeness = false;
        array.nal_unit_type = 32;
        array.nalUnits = new ArrayList();
        Iterator<ByteBuffer> it = arrayList3.iterator();
        while (it.hasNext()) {
            array.nalUnits.add(Utils.toArray(it.next()));
        }
        HevcDecoderConfigurationRecord.Array array2 = new HevcDecoderConfigurationRecord.Array();
        array2.array_completeness = false;
        array2.nal_unit_type = 33;
        array2.nalUnits = new ArrayList();
        Iterator<ByteBuffer> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            array2.nalUnits.add(Utils.toArray(it2.next()));
        }
        HevcDecoderConfigurationRecord.Array array3 = new HevcDecoderConfigurationRecord.Array();
        array3.array_completeness = false;
        array3.nal_unit_type = 34;
        array3.nalUnits = new ArrayList();
        Iterator<ByteBuffer> it3 = arrayList2.iterator();
        while (it3.hasNext()) {
            array3.nalUnits.add(Utils.toArray(it3.next()));
        }
        hevcConfigurationBox.getArrays().addAll(Arrays.asList(array2, array, array3));
        visualSampleEntry.addBox(hevcConfigurationBox);
        return visualSampleEntry;
    }

    private boolean isVcl(H265NalUnitHeader h265NalUnitHeader) {
        int i = h265NalUnitHeader.nalUnitType;
        return i >= 0 && i <= 31;
    }
}
