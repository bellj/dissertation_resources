package org.thoughtcrime.securesms.video.videoconverter.muxer;

import android.media.MediaCodec;
import android.media.MediaFormat;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.mp4parser.streaming.StreamingTrack;
import org.thoughtcrime.securesms.video.VideoUtil;
import org.thoughtcrime.securesms.video.videoconverter.MediaConverter;
import org.thoughtcrime.securesms.video.videoconverter.Muxer;

/* loaded from: classes5.dex */
public final class StreamingMuxer implements Muxer {
    private Mp4Writer mp4Writer;
    private final OutputStream outputStream;
    private final List<MediaCodecTrack> tracks = new ArrayList();

    /* access modifiers changed from: package-private */
    /* loaded from: classes5.dex */
    public interface MediaCodecTrack {
        void finish() throws IOException;

        void writeSampleData(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) throws IOException;
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void release() {
    }

    public StreamingMuxer(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void start() throws IOException {
        ArrayList arrayList = new ArrayList();
        Iterator<MediaCodecTrack> it = this.tracks.iterator();
        while (it.hasNext()) {
            arrayList.add((StreamingTrack) it.next());
        }
        this.mp4Writer = new Mp4Writer(arrayList, Channels.newChannel(this.outputStream));
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void stop() throws IOException {
        if (this.mp4Writer != null) {
            for (MediaCodecTrack mediaCodecTrack : this.tracks) {
                mediaCodecTrack.finish();
            }
            this.mp4Writer.close();
            this.mp4Writer = null;
            return;
        }
        throw new IllegalStateException("calling stop prior to start");
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public int addTrack(MediaFormat mediaFormat) throws IOException {
        String string = mediaFormat.getString("mime");
        string.hashCode();
        char c = 65535;
        switch (string.hashCode()) {
            case -1662541442:
                if (string.equals(MediaConverter.VIDEO_CODEC_H265)) {
                    c = 0;
                    break;
                }
                break;
            case -53558318:
                if (string.equals(VideoUtil.AUDIO_MIME_TYPE)) {
                    c = 1;
                    break;
                }
                break;
            case 1331836730:
                if (string.equals("video/avc")) {
                    c = 2;
                    break;
                }
                break;
        }
        switch (c) {
            case 0:
                this.tracks.add(new MediaCodecHevcTrack(mediaFormat));
                break;
            case 1:
                this.tracks.add(new MediaCodecAacTrack(mediaFormat));
                break;
            case 2:
                this.tracks.add(new MediaCodecAvcTrack(mediaFormat));
                break;
            default:
                throw new IllegalArgumentException("unknown track format");
        }
        return this.tracks.size() - 1;
    }

    @Override // org.thoughtcrime.securesms.video.videoconverter.Muxer
    public void writeSampleData(int i, ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) throws IOException {
        this.tracks.get(i).writeSampleData(byteBuffer, bufferInfo);
    }

    /* loaded from: classes5.dex */
    static class MediaCodecAvcTrack extends AvcTrack implements MediaCodecTrack {
        MediaCodecAvcTrack(MediaFormat mediaFormat) {
            super(Utils.subBuffer(mediaFormat.getByteBuffer("csd-0"), 4), Utils.subBuffer(mediaFormat.getByteBuffer("csd-1"), 4));
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer.MediaCodecTrack
        public void writeSampleData(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) throws IOException {
            for (ByteBuffer byteBuffer2 : H264Utils.getNals(byteBuffer)) {
                consumeNal(Utils.clone(byteBuffer2), bufferInfo.presentationTimeUs);
            }
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer.MediaCodecTrack
        public void finish() throws IOException {
            consumeLastNal();
        }
    }

    /* loaded from: classes5.dex */
    static class MediaCodecHevcTrack extends HevcTrack implements MediaCodecTrack {
        MediaCodecHevcTrack(MediaFormat mediaFormat) throws IOException {
            super(H264Utils.getNals(mediaFormat.getByteBuffer("csd-0")));
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer.MediaCodecTrack
        public void writeSampleData(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) throws IOException {
            for (ByteBuffer byteBuffer2 : H264Utils.getNals(byteBuffer)) {
                consumeNal(Utils.clone(byteBuffer2), bufferInfo.presentationTimeUs);
            }
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer.MediaCodecTrack
        public void finish() throws IOException {
            consumeLastNal();
        }
    }

    /* loaded from: classes5.dex */
    static class MediaCodecAacTrack extends AacTrack implements MediaCodecTrack {
        @Override // org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer.MediaCodecTrack
        public void finish() {
        }

        MediaCodecAacTrack(MediaFormat mediaFormat) {
            super((long) mediaFormat.getInteger("bitrate"), (long) mediaFormat.getInteger("bitrate"), mediaFormat.getInteger("sample-rate"), mediaFormat.getInteger("channel-count"), mediaFormat.getInteger("aac-profile"));
        }

        @Override // org.thoughtcrime.securesms.video.videoconverter.muxer.StreamingMuxer.MediaCodecTrack
        public void writeSampleData(ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) throws IOException {
            byte[] bArr = new byte[bufferInfo.size];
            byteBuffer.position(bufferInfo.offset);
            byteBuffer.get(bArr, 0, bufferInfo.size);
            processSample(ByteBuffer.wrap(bArr));
        }
    }
}
