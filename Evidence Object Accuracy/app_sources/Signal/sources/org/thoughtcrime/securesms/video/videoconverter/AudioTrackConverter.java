package org.thoughtcrime.securesms.video.videoconverter;

import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCrypto;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.view.Surface;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.media.MediaInput;

/* loaded from: classes5.dex */
public final class AudioTrackConverter {
    private static final int OUTPUT_AUDIO_AAC_PROFILE;
    private static final String OUTPUT_AUDIO_MIME_TYPE;
    private static final String TAG;
    private static final int TIMEOUT_USEC;
    private static final boolean VERBOSE;
    private final int mAudioBitrate;
    private int mAudioDecodedFrameCount;
    private final MediaCodec mAudioDecoder;
    private boolean mAudioDecoderDone;
    private final ByteBuffer[] mAudioDecoderInputBuffers;
    private final MediaCodec.BufferInfo mAudioDecoderOutputBufferInfo;
    private ByteBuffer[] mAudioDecoderOutputBuffers;
    private int mAudioEncodedFrameCount;
    private final MediaCodec mAudioEncoder;
    boolean mAudioEncoderDone;
    private final ByteBuffer[] mAudioEncoderInputBuffers;
    private final MediaCodec.BufferInfo mAudioEncoderOutputBufferInfo;
    private ByteBuffer[] mAudioEncoderOutputBuffers;
    private int mAudioExtractedFrameCount;
    private final MediaExtractor mAudioExtractor;
    boolean mAudioExtractorDone;
    MediaFormat mEncoderOutputAudioFormat;
    final long mInputDuration;
    private Muxer mMuxer;
    long mMuxingAudioPresentationTime;
    private int mOutputAudioTrack = -1;
    private int mPendingAudioDecoderOutputBufferIndex = -1;
    private final long mTimeFrom;
    private final long mTimeTo;

    public static AudioTrackConverter create(MediaInput mediaInput, long j, long j2, int i) throws IOException {
        MediaExtractor createExtractor = mediaInput.createExtractor();
        int andSelectAudioTrackIndex = getAndSelectAudioTrackIndex(createExtractor);
        if (andSelectAudioTrackIndex != -1) {
            return new AudioTrackConverter(createExtractor, andSelectAudioTrackIndex, j, j2, i);
        }
        createExtractor.release();
        return null;
    }

    private AudioTrackConverter(MediaExtractor mediaExtractor, int i, long j, long j2, int i2) throws IOException {
        this.mTimeFrom = j;
        this.mTimeTo = j2;
        this.mAudioExtractor = mediaExtractor;
        this.mAudioBitrate = i2;
        MediaCodecInfo selectCodec = MediaConverter.selectCodec("audio/mp4a-latm");
        if (selectCodec != null) {
            MediaFormat trackFormat = mediaExtractor.getTrackFormat(i);
            this.mInputDuration = trackFormat.containsKey("durationUs") ? trackFormat.getLong("durationUs") : 0;
            MediaFormat createAudioFormat = MediaFormat.createAudioFormat("audio/mp4a-latm", trackFormat.getInteger("sample-rate"), trackFormat.getInteger("channel-count"));
            createAudioFormat.setInteger("bitrate", i2);
            createAudioFormat.setInteger("aac-profile", 2);
            createAudioFormat.setInteger("max-input-size", 16384);
            MediaCodec createAudioEncoder = createAudioEncoder(selectCodec, createAudioFormat);
            this.mAudioEncoder = createAudioEncoder;
            MediaCodec createAudioDecoder = createAudioDecoder(trackFormat);
            this.mAudioDecoder = createAudioDecoder;
            this.mAudioDecoderInputBuffers = createAudioDecoder.getInputBuffers();
            this.mAudioDecoderOutputBuffers = createAudioDecoder.getOutputBuffers();
            this.mAudioEncoderInputBuffers = createAudioEncoder.getInputBuffers();
            this.mAudioEncoderOutputBuffers = createAudioEncoder.getOutputBuffers();
            this.mAudioDecoderOutputBufferInfo = new MediaCodec.BufferInfo();
            this.mAudioEncoderOutputBufferInfo = new MediaCodec.BufferInfo();
            if (j > 0) {
                mediaExtractor.seekTo(1000 * j, 0);
                Log.i(TAG, "Seek audio:" + j + " " + mediaExtractor.getSampleTime());
                return;
            }
            return;
        }
        Log.e(TAG, "Unable to find an appropriate codec for audio/mp4a-latm");
        throw new FileNotFoundException();
    }

    public void setMuxer(Muxer muxer) throws IOException {
        this.mMuxer = muxer;
        if (this.mEncoderOutputAudioFormat != null) {
            Log.d(TAG, "muxer: adding audio track.");
            if (!this.mEncoderOutputAudioFormat.containsKey("bitrate")) {
                this.mEncoderOutputAudioFormat.setInteger("bitrate", this.mAudioBitrate);
            }
            if (!this.mEncoderOutputAudioFormat.containsKey("aac-profile")) {
                this.mEncoderOutputAudioFormat.setInteger("aac-profile", 2);
            }
            this.mOutputAudioTrack = muxer.addTrack(this.mEncoderOutputAudioFormat);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0051  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void step() throws java.io.IOException {
        /*
        // Method dump skipped, instructions count: 422
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.videoconverter.AudioTrackConverter.step():void");
    }

    public void release() throws Exception {
        Exception e;
        try {
            MediaExtractor mediaExtractor = this.mAudioExtractor;
            if (mediaExtractor != null) {
                mediaExtractor.release();
            }
            e = null;
        } catch (Exception e2) {
            e = e2;
            Log.e(TAG, "error while releasing mAudioExtractor", e);
        }
        try {
            MediaCodec mediaCodec = this.mAudioDecoder;
            if (mediaCodec != null) {
                mediaCodec.stop();
                this.mAudioDecoder.release();
            }
        } catch (Exception e3) {
            Log.e(TAG, "error while releasing mAudioDecoder", e3);
            if (e == null) {
                e = e3;
            }
        }
        try {
            MediaCodec mediaCodec2 = this.mAudioEncoder;
            if (mediaCodec2 != null) {
                mediaCodec2.stop();
                this.mAudioEncoder.release();
            }
        } catch (Exception e4) {
            Log.e(TAG, "error while releasing mAudioEncoder", e4);
            if (e == null) {
                e = e4;
            }
        }
        if (e != null) {
            throw e;
        }
    }

    String dumpState() {
        Locale locale = Locale.US;
        Object[] objArr = new Object[9];
        boolean z = false;
        objArr[0] = Integer.valueOf(this.mAudioExtractedFrameCount);
        objArr[1] = Boolean.valueOf(this.mAudioExtractorDone);
        objArr[2] = Integer.valueOf(this.mAudioDecodedFrameCount);
        objArr[3] = Boolean.valueOf(this.mAudioDecoderDone);
        objArr[4] = Integer.valueOf(this.mAudioEncodedFrameCount);
        objArr[5] = Boolean.valueOf(this.mAudioEncoderDone);
        objArr[6] = Integer.valueOf(this.mPendingAudioDecoderOutputBufferIndex);
        if (this.mMuxer != null) {
            z = true;
        }
        objArr[7] = Boolean.valueOf(z);
        objArr[8] = Integer.valueOf(this.mOutputAudioTrack);
        return String.format(locale, "A{extracted:%d(done:%b) decoded:%d(done:%b) encoded:%d(done:%b) pending:%d muxing:%b(track:%d} )", objArr);
    }

    public void verifyEndState() {
        Preconditions.checkState("no frame should be pending", -1 == this.mPendingAudioDecoderOutputBufferIndex);
    }

    private static MediaCodec createAudioDecoder(MediaFormat mediaFormat) throws IOException {
        MediaCodec createDecoderByType = MediaCodec.createDecoderByType(MediaConverter.getMimeTypeFor(mediaFormat));
        createDecoderByType.configure(mediaFormat, (Surface) null, (MediaCrypto) null, 0);
        createDecoderByType.start();
        return createDecoderByType;
    }

    private static MediaCodec createAudioEncoder(MediaCodecInfo mediaCodecInfo, MediaFormat mediaFormat) throws IOException {
        MediaCodec createByCodecName = MediaCodec.createByCodecName(mediaCodecInfo.getName());
        createByCodecName.configure(mediaFormat, (Surface) null, (MediaCrypto) null, 1);
        createByCodecName.start();
        return createByCodecName;
    }

    private static int getAndSelectAudioTrackIndex(MediaExtractor mediaExtractor) {
        for (int i = 0; i < mediaExtractor.getTrackCount(); i++) {
            if (isAudioFormat(mediaExtractor.getTrackFormat(i))) {
                mediaExtractor.selectTrack(i);
                return i;
            }
        }
        return -1;
    }

    private static boolean isAudioFormat(MediaFormat mediaFormat) {
        return MediaConverter.getMimeTypeFor(mediaFormat).startsWith("audio/");
    }
}
