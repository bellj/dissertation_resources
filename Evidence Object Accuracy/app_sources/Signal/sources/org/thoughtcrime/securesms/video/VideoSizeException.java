package org.thoughtcrime.securesms.video;

import java.io.IOException;

/* loaded from: classes5.dex */
public final class VideoSizeException extends IOException {
    public VideoSizeException(String str) {
        super(str);
    }
}
