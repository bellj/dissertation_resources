package org.thoughtcrime.securesms.video.exo;

import android.content.Context;
import android.net.Uri;
import android.support.v4.media.MediaDescriptionCompat;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManagerProvider;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceFactory;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.LoadErrorHandlingPolicy;
import java.util.List;
import org.thoughtcrime.securesms.video.exo.SignalDataSource;

/* loaded from: classes5.dex */
public final class SignalMediaSourceFactory implements MediaSourceFactory {
    private final ProgressiveMediaSource.Factory progressiveMediaSourceFactory;

    @Override // com.google.android.exoplayer2.source.MediaSourceFactory
    public int[] getSupportedTypes() {
        return new int[]{4};
    }

    public SignalMediaSourceFactory(Context context) {
        this.progressiveMediaSourceFactory = new ProgressiveMediaSource.Factory(new SignalDataSource.Factory(context, null, null), new DefaultExtractorsFactory().setConstantBitrateSeekingEnabled(true));
    }

    public MediaSource createMediaSource(MediaDescriptionCompat mediaDescriptionCompat) {
        return this.progressiveMediaSourceFactory.createMediaSource(new MediaItem.Builder().setUri(mediaDescriptionCompat.getMediaUri()).setTag(mediaDescriptionCompat).build());
    }

    public MediaSourceFactory setStreamKeys(List<Object> list) {
        return this.progressiveMediaSourceFactory.setStreamKeys(list);
    }

    public MediaSourceFactory setDrmSessionManagerProvider(DrmSessionManagerProvider drmSessionManagerProvider) {
        return this.progressiveMediaSourceFactory.setDrmSessionManagerProvider(drmSessionManagerProvider);
    }

    public MediaSourceFactory setDrmSessionManager(DrmSessionManager drmSessionManager) {
        return this.progressiveMediaSourceFactory.setDrmSessionManager(drmSessionManager);
    }

    public MediaSourceFactory setDrmHttpDataSourceFactory(HttpDataSource.Factory factory) {
        return this.progressiveMediaSourceFactory.setDrmHttpDataSourceFactory(factory);
    }

    public MediaSourceFactory setDrmUserAgent(String str) {
        return this.progressiveMediaSourceFactory.setDrmUserAgent(str);
    }

    public MediaSourceFactory setLoadErrorHandlingPolicy(LoadErrorHandlingPolicy loadErrorHandlingPolicy) {
        return this.progressiveMediaSourceFactory.setLoadErrorHandlingPolicy(loadErrorHandlingPolicy);
    }

    @Override // com.google.android.exoplayer2.source.MediaSourceFactory
    public MediaSource createMediaSource(MediaItem mediaItem) {
        return this.progressiveMediaSourceFactory.createMediaSource(mediaItem);
    }

    public MediaSource createMediaSource(Uri uri) {
        return this.progressiveMediaSourceFactory.createMediaSource(uri);
    }
}
