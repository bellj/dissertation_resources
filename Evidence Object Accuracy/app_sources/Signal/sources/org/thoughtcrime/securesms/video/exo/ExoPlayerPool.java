package org.thoughtcrime.securesms.video.exo;

import com.google.android.exoplayer2.ExoPlayer;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.AppForegroundObserver;

/* compiled from: SimpleExoPlayerPool.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\b&\u0018\u0000 \"*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003:\u0004\"#$%B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\r\u0010\n\u001a\u00028\u0000H$¢\u0006\u0002\u0010\u000bJ\u001a\u0010\f\u001a\u00020\t2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0002J\u0017\u0010\u0011\u001a\u0004\u0018\u00018\u00002\u0006\u0010\r\u001a\u00020\u000eH\u0002¢\u0006\u0002\u0010\u0012J\u000f\u0010\u0013\u001a\u0004\u0018\u00018\u0000H\u0002¢\u0006\u0002\u0010\u000bJ\u000f\u0010\u0014\u001a\u0004\u0018\u00018\u0000H\u0002¢\u0006\u0002\u0010\u000bJ\u001f\u0010\u0015\u001a\u0004\u0018\u00018\u00002\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0003¢\u0006\u0002\u0010\u0016J\u0017\u0010\u0015\u001a\u0004\u0018\u00018\u00002\u0006\u0010\u000f\u001a\u00020\u0010H\u0007¢\u0006\u0002\u0010\u0017J\b\u0010\u0018\u001a\u00020\u0005H$J\u0010\u0010\u0019\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0006\u0010\u001a\u001a\u00020\u001bJ\b\u0010\u001c\u001a\u00020\u001dH\u0017J\u0015\u0010\u0007\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00028\u0000H\u0007¢\u0006\u0002\u0010\u001fJ\b\u0010 \u001a\u00020\u0010H\u0002J\u0015\u0010!\u001a\u00028\u00002\u0006\u0010\u000f\u001a\u00020\u0010H\u0007¢\u0006\u0002\u0010\u0017R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool;", "T", "Lcom/google/android/exoplayer2/ExoPlayer;", "Lorg/thoughtcrime/securesms/util/AppForegroundObserver$Listener;", "maximumReservedPlayers", "", "(I)V", "pool", "", "Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$PoolState;", "createPlayer", "()Lcom/google/android/exoplayer2/ExoPlayer;", "createPoolStateForNewEntry", "allowReserved", "", "tag", "", "findAvailablePlayer", "(Z)Lcom/google/android/exoplayer2/ExoPlayer;", "findFirstReservedAndAvailablePlayer", "findFirstUnreservedAndAvailablePlayer", "get", "(ZLjava/lang/String;)Lcom/google/android/exoplayer2/ExoPlayer;", "(Ljava/lang/String;)Lcom/google/android/exoplayer2/ExoPlayer;", "getMaxSimultaneousPlayback", "getMaximumAllowed", "getPoolStats", "Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$PoolStats;", "onBackground", "", "exoPlayer", "(Lcom/google/android/exoplayer2/ExoPlayer;)V", "poolStats", "require", "Companion", "OwnershipInfo", "PoolState", "PoolStats", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes5.dex */
public abstract class ExoPlayerPool<T extends ExoPlayer> implements AppForegroundObserver.Listener {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ExoPlayerPool.class);
    private final int maximumReservedPlayers;
    private final Map<T, PoolState> pool = new LinkedHashMap();

    protected abstract T createPlayer();

    protected abstract int getMaxSimultaneousPlayback();

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public /* synthetic */ void onForeground() {
        AppForegroundObserver.Listener.CC.$default$onForeground(this);
    }

    public ExoPlayerPool(int i) {
        this.maximumReservedPlayers = i;
    }

    /* compiled from: SimpleExoPlayerPool.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final T get(String str) {
        Intrinsics.checkNotNullParameter(str, "tag");
        return get(false, str);
    }

    public final T require(String str) {
        Intrinsics.checkNotNullParameter(str, "tag");
        T t = get(true, str);
        if (t != null) {
            return t;
        }
        throw new IllegalStateException(("Required exoPlayer could not be acquired for " + str + "! :: " + poolStats()).toString());
    }

    public final void pool(T t) {
        Intrinsics.checkNotNullParameter(t, "exoPlayer");
        PoolState poolState = this.pool.get(t);
        if (poolState != null) {
            this.pool.put(t, PoolState.copy$default(poolState, true, false, null, 2, null));
            return;
        }
        throw new IllegalArgumentException("Tried to return unknown ExoPlayer to pool :: " + poolStats());
    }

    private final T get(boolean z, String str) {
        T findAvailablePlayer = findAvailablePlayer(z);
        if (findAvailablePlayer != null || this.pool.size() >= getMaximumAllowed(z)) {
            if (findAvailablePlayer != null) {
                PoolState poolState = this.pool.get(findAvailablePlayer);
                Intrinsics.checkNotNull(poolState);
                this.pool.put(findAvailablePlayer, PoolState.copy$default(poolState, false, false, str, 2, null));
            } else {
                String str2 = TAG;
                Log.d(str2, "Failed to get an ExoPlayer instance for tag: " + str);
                findAvailablePlayer = null;
            }
            if (findAvailablePlayer == null) {
                return null;
            }
            ExoPlayerKt.configureForVideoPlayback(findAvailablePlayer);
            return findAvailablePlayer;
        }
        T createPlayer = createPlayer();
        this.pool.put(createPlayer, createPoolStateForNewEntry(z, str));
        return createPlayer;
    }

    private final int getMaximumAllowed(boolean z) {
        return z ? getMaxSimultaneousPlayback() : getMaxSimultaneousPlayback() - this.maximumReservedPlayers;
    }

    private final PoolState createPoolStateForNewEntry(boolean z, String str) {
        boolean z2;
        if (z) {
            Map<T, PoolState> map = this.pool;
            if (!map.isEmpty()) {
                for (Map.Entry<T, PoolState> entry : map.entrySet()) {
                    if (entry.getValue().getReserved()) {
                        z2 = false;
                        break;
                    }
                }
            }
            z2 = true;
            if (z2) {
                return new PoolState(false, true, str);
            }
        }
        return new PoolState(false, false, str);
    }

    private final T findAvailablePlayer(boolean z) {
        if (!z) {
            return findFirstUnreservedAndAvailablePlayer();
        }
        T findFirstReservedAndAvailablePlayer = findFirstReservedAndAvailablePlayer();
        if (findFirstReservedAndAvailablePlayer == null) {
            return findFirstUnreservedAndAvailablePlayer();
        }
        return findFirstReservedAndAvailablePlayer;
    }

    private final T findFirstReservedAndAvailablePlayer() {
        Map<T, PoolState> map = this.pool;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<T, PoolState> entry : map.entrySet()) {
            if (entry.getValue().getReservedAndAvailable()) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return (T) ((ExoPlayer) CollectionsKt___CollectionsKt.firstOrNull(linkedHashMap.keySet()));
    }

    private final T findFirstUnreservedAndAvailablePlayer() {
        Map<T, PoolState> map = this.pool;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<T, PoolState> entry : map.entrySet()) {
            if (entry.getValue().getUnreservedAndAvailable()) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        return (T) ((ExoPlayer) CollectionsKt___CollectionsKt.firstOrNull(linkedHashMap.keySet()));
    }

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public void onBackground() {
        Map<T, PoolState> map = this.pool;
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry<T, PoolState> entry : map.entrySet()) {
            if (entry.getValue().getAvailable()) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        Set<ExoPlayer> keySet = linkedHashMap.keySet();
        boolean unused = CollectionsKt__MutableCollectionsKt.removeAll(this.pool.keySet(), keySet);
        for (ExoPlayer exoPlayer : keySet) {
            exoPlayer.release();
        }
    }

    private final String poolStats() {
        return getPoolStats().toString();
    }

    public final PoolStats getPoolStats() {
        List<OwnershipInfo> list;
        int size = this.pool.size();
        int maxSimultaneousPlayback = getMaxSimultaneousPlayback();
        int i = this.maximumReservedPlayers;
        PoolStats poolStats = new PoolStats(size, maxSimultaneousPlayback - i, i, 0, 0, 0, 0, CollectionsKt__CollectionsKt.emptyList(), 120, null);
        PoolStats poolStats2 = poolStats;
        for (PoolState poolState : this.pool.values()) {
            Log.d(TAG, String.valueOf(poolState));
            int unreservedAndAvailable = poolStats2.getUnreservedAndAvailable() + (poolState.getUnreservedAndAvailable() ? 1 : 0);
            int reservedAndAvailable = poolStats2.getReservedAndAvailable() + (poolState.getReservedAndAvailable() ? 1 : 0);
            int unreserved = poolStats2.getUnreserved() + (!poolState.getReserved());
            int reserved = poolStats2.getReserved() + (poolState.getReserved() ? 1 : 0);
            if (!poolState.getAvailable()) {
                List<OwnershipInfo> owners = poolStats2.getOwners();
                String tag = poolState.getTag();
                Intrinsics.checkNotNull(tag);
                list = CollectionsKt___CollectionsKt.plus((Collection<? extends OwnershipInfo>) ((Collection<? extends Object>) owners), new OwnershipInfo(tag, poolState.getReserved()));
            } else {
                list = poolStats2.getOwners();
            }
            poolStats2 = PoolStats.copy$default(poolStats2, 0, 0, 0, unreservedAndAvailable, reservedAndAvailable, unreserved, reserved, list, 7, null);
        }
        return poolStats2;
    }

    /* compiled from: SimpleExoPlayerPool.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BY\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0002\u0010\rJ\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003J_\u0010 \u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0001J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020\u0003HÖ\u0001J\t\u0010%\u001a\u00020&HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000fR\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000fR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u000f¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$PoolStats;", "", "created", "", "maxUnreserved", "maxReserved", "unreservedAndAvailable", "reservedAndAvailable", "unreserved", "reserved", "owners", "", "Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$OwnershipInfo;", "(IIIIIIILjava/util/List;)V", "getCreated", "()I", "getMaxReserved", "getMaxUnreserved", "getOwners", "()Ljava/util/List;", "getReserved", "getReservedAndAvailable", "getUnreserved", "getUnreservedAndAvailable", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class PoolStats {
        private final int created;
        private final int maxReserved;
        private final int maxUnreserved;
        private final List<OwnershipInfo> owners;
        private final int reserved;
        private final int reservedAndAvailable;
        private final int unreserved;
        private final int unreservedAndAvailable;

        public static /* synthetic */ PoolStats copy$default(PoolStats poolStats, int i, int i2, int i3, int i4, int i5, int i6, int i7, List list, int i8, Object obj) {
            return poolStats.copy((i8 & 1) != 0 ? poolStats.created : i, (i8 & 2) != 0 ? poolStats.maxUnreserved : i2, (i8 & 4) != 0 ? poolStats.maxReserved : i3, (i8 & 8) != 0 ? poolStats.unreservedAndAvailable : i4, (i8 & 16) != 0 ? poolStats.reservedAndAvailable : i5, (i8 & 32) != 0 ? poolStats.unreserved : i6, (i8 & 64) != 0 ? poolStats.reserved : i7, (i8 & 128) != 0 ? poolStats.owners : list);
        }

        public final int component1() {
            return this.created;
        }

        public final int component2() {
            return this.maxUnreserved;
        }

        public final int component3() {
            return this.maxReserved;
        }

        public final int component4() {
            return this.unreservedAndAvailable;
        }

        public final int component5() {
            return this.reservedAndAvailable;
        }

        public final int component6() {
            return this.unreserved;
        }

        public final int component7() {
            return this.reserved;
        }

        public final List<OwnershipInfo> component8() {
            return this.owners;
        }

        public final PoolStats copy(int i, int i2, int i3, int i4, int i5, int i6, int i7, List<OwnershipInfo> list) {
            Intrinsics.checkNotNullParameter(list, "owners");
            return new PoolStats(i, i2, i3, i4, i5, i6, i7, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PoolStats)) {
                return false;
            }
            PoolStats poolStats = (PoolStats) obj;
            return this.created == poolStats.created && this.maxUnreserved == poolStats.maxUnreserved && this.maxReserved == poolStats.maxReserved && this.unreservedAndAvailable == poolStats.unreservedAndAvailable && this.reservedAndAvailable == poolStats.reservedAndAvailable && this.unreserved == poolStats.unreserved && this.reserved == poolStats.reserved && Intrinsics.areEqual(this.owners, poolStats.owners);
        }

        public int hashCode() {
            return (((((((((((((this.created * 31) + this.maxUnreserved) * 31) + this.maxReserved) * 31) + this.unreservedAndAvailable) * 31) + this.reservedAndAvailable) * 31) + this.unreserved) * 31) + this.reserved) * 31) + this.owners.hashCode();
        }

        public String toString() {
            return "PoolStats(created=" + this.created + ", maxUnreserved=" + this.maxUnreserved + ", maxReserved=" + this.maxReserved + ", unreservedAndAvailable=" + this.unreservedAndAvailable + ", reservedAndAvailable=" + this.reservedAndAvailable + ", unreserved=" + this.unreserved + ", reserved=" + this.reserved + ", owners=" + this.owners + ')';
        }

        public PoolStats(int i, int i2, int i3, int i4, int i5, int i6, int i7, List<OwnershipInfo> list) {
            Intrinsics.checkNotNullParameter(list, "owners");
            this.created = i;
            this.maxUnreserved = i2;
            this.maxReserved = i3;
            this.unreservedAndAvailable = i4;
            this.reservedAndAvailable = i5;
            this.unreserved = i6;
            this.reserved = i7;
            this.owners = list;
        }

        public /* synthetic */ PoolStats(int i, int i2, int i3, int i4, int i5, int i6, int i7, List list, int i8, DefaultConstructorMarker defaultConstructorMarker) {
            this((i8 & 1) != 0 ? 0 : i, (i8 & 2) != 0 ? 0 : i2, (i8 & 4) != 0 ? 0 : i3, (i8 & 8) != 0 ? 0 : i4, (i8 & 16) != 0 ? 0 : i5, (i8 & 32) != 0 ? 0 : i6, (i8 & 64) != 0 ? 0 : i7, list);
        }

        public final int getCreated() {
            return this.created;
        }

        public final int getMaxUnreserved() {
            return this.maxUnreserved;
        }

        public final int getMaxReserved() {
            return this.maxReserved;
        }

        public final int getUnreservedAndAvailable() {
            return this.unreservedAndAvailable;
        }

        public final int getReservedAndAvailable() {
            return this.reservedAndAvailable;
        }

        public final int getUnreserved() {
            return this.unreserved;
        }

        public final int getReserved() {
            return this.reserved;
        }

        public final List<OwnershipInfo> getOwners() {
            return this.owners;
        }
    }

    /* compiled from: SimpleExoPlayerPool.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000b\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u00052\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$OwnershipInfo;", "", "tag", "", "isReserved", "", "(Ljava/lang/String;Z)V", "()Z", "getTag", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class OwnershipInfo {
        private final boolean isReserved;
        private final String tag;

        public static /* synthetic */ OwnershipInfo copy$default(OwnershipInfo ownershipInfo, String str, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                str = ownershipInfo.tag;
            }
            if ((i & 2) != 0) {
                z = ownershipInfo.isReserved;
            }
            return ownershipInfo.copy(str, z);
        }

        public final String component1() {
            return this.tag;
        }

        public final boolean component2() {
            return this.isReserved;
        }

        public final OwnershipInfo copy(String str, boolean z) {
            Intrinsics.checkNotNullParameter(str, "tag");
            return new OwnershipInfo(str, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof OwnershipInfo)) {
                return false;
            }
            OwnershipInfo ownershipInfo = (OwnershipInfo) obj;
            return Intrinsics.areEqual(this.tag, ownershipInfo.tag) && this.isReserved == ownershipInfo.isReserved;
        }

        public int hashCode() {
            int hashCode = this.tag.hashCode() * 31;
            boolean z = this.isReserved;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            return "OwnershipInfo(tag=" + this.tag + ", isReserved=" + this.isReserved + ')';
        }

        public OwnershipInfo(String str, boolean z) {
            Intrinsics.checkNotNullParameter(str, "tag");
            this.tag = str;
            this.isReserved = z;
        }

        public final String getTag() {
            return this.tag;
        }

        public final boolean isReserved() {
            return this.isReserved;
        }
    }

    /* compiled from: SimpleExoPlayerPool.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0006HÆ\u0003J)\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00032\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\tR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\t¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/video/exo/ExoPlayerPool$PoolState;", "", "available", "", "reserved", "tag", "", "(ZZLjava/lang/String;)V", "getAvailable", "()Z", "getReserved", "reservedAndAvailable", "getReservedAndAvailable", "getTag", "()Ljava/lang/String;", "unreservedAndAvailable", "getUnreservedAndAvailable", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes5.dex */
    public static final class PoolState {
        private final boolean available;
        private final boolean reserved;
        private final boolean reservedAndAvailable;
        private final String tag;
        private final boolean unreservedAndAvailable;

        public static /* synthetic */ PoolState copy$default(PoolState poolState, boolean z, boolean z2, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                z = poolState.available;
            }
            if ((i & 2) != 0) {
                z2 = poolState.reserved;
            }
            if ((i & 4) != 0) {
                str = poolState.tag;
            }
            return poolState.copy(z, z2, str);
        }

        public final boolean component1() {
            return this.available;
        }

        public final boolean component2() {
            return this.reserved;
        }

        public final String component3() {
            return this.tag;
        }

        public final PoolState copy(boolean z, boolean z2, String str) {
            return new PoolState(z, z2, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof PoolState)) {
                return false;
            }
            PoolState poolState = (PoolState) obj;
            return this.available == poolState.available && this.reserved == poolState.reserved && Intrinsics.areEqual(this.tag, poolState.tag);
        }

        public int hashCode() {
            boolean z = this.available;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = i2 * 31;
            boolean z2 = this.reserved;
            if (!z2) {
                i = z2 ? 1 : 0;
            }
            int i6 = (i5 + i) * 31;
            String str = this.tag;
            return i6 + (str == null ? 0 : str.hashCode());
        }

        public String toString() {
            return "PoolState(available=" + this.available + ", reserved=" + this.reserved + ", tag=" + this.tag + ')';
        }

        public PoolState(boolean z, boolean z2, String str) {
            this.available = z;
            this.reserved = z2;
            this.tag = str;
            boolean z3 = true;
            this.unreservedAndAvailable = z && !z2;
            this.reservedAndAvailable = (!z || !z2) ? false : z3;
        }

        public final boolean getAvailable() {
            return this.available;
        }

        public final boolean getReserved() {
            return this.reserved;
        }

        public final String getTag() {
            return this.tag;
        }

        public final boolean getUnreservedAndAvailable() {
            return this.unreservedAndAvailable;
        }

        public final boolean getReservedAndAvailable() {
            return this.reservedAndAvailable;
        }
    }
}
