package org.thoughtcrime.securesms.video;

/* loaded from: classes5.dex */
public final class VideoSourceException extends Exception {
    public VideoSourceException(String str) {
        super(str);
    }

    public VideoSourceException(String str, Exception exc) {
        super(str, exc);
    }
}
