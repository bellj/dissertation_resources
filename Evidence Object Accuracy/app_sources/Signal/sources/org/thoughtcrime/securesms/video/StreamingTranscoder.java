package org.thoughtcrime.securesms.video;

import android.media.MediaDataSource;
import android.media.MediaMetadataRetriever;
import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.video.VideoBitRateCalculator;

/* loaded from: classes5.dex */
public final class StreamingTranscoder {
    private static final String TAG = Log.tag(StreamingTranscoder.class);
    private final MediaDataSource dataSource;
    private final long duration;
    private final long fileSizeEstimate;
    private final long inSize;
    private final int inputBitRate;
    private final long memoryFileEstimate;
    private final TranscoderOptions options;
    private final VideoBitRateCalculator.Quality targetQuality;
    private final boolean transcodeRequired;
    private final long upperSizeLimit;

    /* loaded from: classes5.dex */
    public interface Progress {
        void onProgress(int i);
    }

    public StreamingTranscoder(MediaDataSource mediaDataSource, TranscoderOptions transcoderOptions, long j) throws IOException, VideoSourceException {
        this.dataSource = mediaDataSource;
        this.options = transcoderOptions;
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        try {
            mediaMetadataRetriever.setDataSource(mediaDataSource);
            long size = mediaDataSource.getSize();
            this.inSize = size;
            long duration = getDuration(mediaMetadataRetriever);
            this.duration = duration;
            int bitRate = VideoBitRateCalculator.bitRate(size, duration);
            this.inputBitRate = bitRate;
            VideoBitRateCalculator.Quality targetQuality = new VideoBitRateCalculator(j).getTargetQuality(duration, bitRate);
            this.targetQuality = targetQuality;
            this.upperSizeLimit = j;
            double d = (double) bitRate;
            double targetTotalBitRate = (double) targetQuality.getTargetTotalBitRate();
            Double.isNaN(targetTotalBitRate);
            boolean z = d >= targetTotalBitRate * 1.2d || size > j || containsLocation(mediaMetadataRetriever) || transcoderOptions != null;
            this.transcodeRequired = z;
            if (!z) {
                Log.i(TAG, "Video is within 20% of target bitrate, below the size limit, contained no location metadata or custom options.");
            }
            long fileSizeEstimate = targetQuality.getFileSizeEstimate();
            this.fileSizeEstimate = fileSizeEstimate;
            double d2 = (double) fileSizeEstimate;
            Double.isNaN(d2);
            this.memoryFileEstimate = (long) (d2 * 1.1d);
        } catch (RuntimeException e) {
            Log.w(TAG, "Unable to read datasource", e);
            throw new VideoSourceException("Unable to read datasource", e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x01a3  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x01a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void transcode(org.thoughtcrime.securesms.video.StreamingTranscoder.Progress r25, java.io.OutputStream r26, org.thoughtcrime.securesms.video.TranscoderCancelationSignal r27) throws java.io.IOException, org.thoughtcrime.securesms.video.videoconverter.EncodingException {
        /*
        // Method dump skipped, instructions count: 435
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.StreamingTranscoder.transcode(org.thoughtcrime.securesms.video.StreamingTranscoder$Progress, java.io.OutputStream, org.thoughtcrime.securesms.video.TranscoderCancelationSignal):void");
    }

    public static /* synthetic */ boolean lambda$transcode$0(Progress progress, TranscoderCancelationSignal transcoderCancelationSignal, int i) {
        progress.onProgress(i);
        return transcoderCancelationSignal != null && transcoderCancelationSignal.isCanceled();
    }

    public boolean isTranscodeRequired() {
        return this.transcodeRequired;
    }

    private static long getDuration(MediaMetadataRetriever mediaMetadataRetriever) throws VideoSourceException {
        String extractMetadata = mediaMetadataRetriever.extractMetadata(9);
        if (extractMetadata != null) {
            try {
                long parseLong = Long.parseLong(extractMetadata);
                if (parseLong > 0) {
                    return parseLong;
                }
                throw new VideoSourceException("Cannot determine duration of video, meta data: " + extractMetadata);
            } catch (NumberFormatException e) {
                throw new VideoSourceException("Cannot determine duration of video, meta data: " + extractMetadata, e);
            }
        } else {
            throw new VideoSourceException("Cannot determine duration of video, null meta data");
        }
    }

    private static boolean containsLocation(MediaMetadataRetriever mediaMetadataRetriever) {
        return mediaMetadataRetriever.extractMetadata(23) != null;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes5.dex */
    public static class LimitedSizeOutputStream extends FilterOutputStream {
        private final long sizeLimit;
        private long written;

        LimitedSizeOutputStream(OutputStream outputStream, long j) {
            super(outputStream);
            this.sizeLimit = j;
        }

        @Override // java.io.FilterOutputStream, java.io.OutputStream
        public void write(int i) throws IOException {
            incWritten(1);
            ((FilterOutputStream) this).out.write(i);
        }

        @Override // java.io.FilterOutputStream, java.io.OutputStream
        public void write(byte[] bArr, int i, int i2) throws IOException {
            incWritten(i2);
            ((FilterOutputStream) this).out.write(bArr, i, i2);
        }

        private void incWritten(int i) throws IOException {
            long j = this.written + ((long) i);
            if (j <= this.sizeLimit) {
                this.written = j;
            } else {
                Log.w(StreamingTranscoder.TAG, String.format(Locale.US, "File size limit hit. Wrote %d, tried to write %d more. Limit is %d", Long.valueOf(this.written), Integer.valueOf(i), Long.valueOf(this.sizeLimit)));
                throw new VideoSizeException("File size limit hit");
            }
        }
    }
}
