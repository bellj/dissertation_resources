package org.thoughtcrime.securesms.video.videoconverter.muxer;

import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public final class Utils {
    private Utils() {
    }

    public static byte[] toArray(ByteBuffer byteBuffer) {
        ByteBuffer duplicate = byteBuffer.duplicate();
        int remaining = duplicate.remaining();
        byte[] bArr = new byte[remaining];
        duplicate.get(bArr, 0, remaining);
        return bArr;
    }

    public static ByteBuffer clone(ByteBuffer byteBuffer) {
        ByteBuffer allocate = ByteBuffer.allocate(byteBuffer.capacity());
        byteBuffer.rewind();
        allocate.put(byteBuffer);
        byteBuffer.rewind();
        allocate.flip();
        return allocate;
    }

    public static ByteBuffer subBuffer(ByteBuffer byteBuffer, int i) {
        return subBuffer(byteBuffer, i, byteBuffer.remaining() - i);
    }

    static ByteBuffer subBuffer(ByteBuffer byteBuffer, int i, int i2) {
        ByteBuffer duplicate = byteBuffer.duplicate();
        byte[] bArr = new byte[i2];
        duplicate.position(i);
        duplicate.get(bArr, 0, i2);
        return ByteBuffer.wrap(bArr);
    }
}
