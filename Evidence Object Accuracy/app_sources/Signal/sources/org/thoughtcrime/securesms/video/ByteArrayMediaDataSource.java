package org.thoughtcrime.securesms.video;

import android.media.MediaDataSource;
import java.io.IOException;

/* loaded from: classes5.dex */
public class ByteArrayMediaDataSource extends MediaDataSource {
    private byte[] data;

    public ByteArrayMediaDataSource(byte[] bArr) {
        this.data = bArr;
    }

    @Override // android.media.MediaDataSource
    public int readAt(long j, byte[] bArr, int i, int i2) throws IOException {
        if (this.data != null) {
            int min = Math.min(i2, (int) (getSize() - j));
            if (min <= 0) {
                return -1;
            }
            if (bArr != null) {
                System.arraycopy(this.data, (int) j, bArr, i, min);
            }
            return min;
        }
        throw new IOException("ByteArrayMediaDataSource is closed");
    }

    @Override // android.media.MediaDataSource
    public long getSize() throws IOException {
        byte[] bArr = this.data;
        if (bArr != null) {
            return (long) bArr.length;
        }
        throw new IOException("ByteArrayMediaDataSource is closed");
    }

    @Override // java.io.Closeable, java.lang.AutoCloseable
    public void close() throws IOException {
        this.data = null;
    }
}
