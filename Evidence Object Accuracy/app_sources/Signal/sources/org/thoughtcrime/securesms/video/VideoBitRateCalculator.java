package org.thoughtcrime.securesms.video;

/* loaded from: classes5.dex */
public final class VideoBitRateCalculator {
    private static final int AUDIO_BITRATE;
    private static final int LOW_RES_OUTPUT_FORMAT;
    private static final int LOW_RES_TARGET_VIDEO_BITRATE;
    private static final int MAXIMUM_TARGET_VIDEO_BITRATE;
    private static final int MINIMUM_TARGET_VIDEO_BITRATE;
    private static final int OUTPUT_FORMAT;
    private final long upperFileSizeLimitWithMargin;

    public static int bitRate(long j, long j2) {
        return (int) (((float) (j * 8)) / (((float) j2) / 1000.0f));
    }

    public VideoBitRateCalculator(long j) {
        double d = (double) j;
        Double.isNaN(d);
        this.upperFileSizeLimitWithMargin = (long) (d / 1.1d);
    }

    public Quality getTargetQuality(long j, int i) {
        double d;
        int min = Math.min(2000000, i - 192000);
        int min2 = Math.min((int) MINIMUM_TARGET_VIDEO_BITRATE, min);
        int max = Math.max(min2, Math.min(getTargetVideoBitRate(this.upperFileSizeLimitWithMargin, j), min));
        int i2 = min - min2;
        if (i2 == 0) {
            d = 1.0d;
        } else {
            double d2 = (double) (max - min2);
            double d3 = (double) i2;
            Double.isNaN(d2);
            Double.isNaN(d3);
            d = d2 / d3;
        }
        return new Quality(max, 192000, d, j);
    }

    private int getTargetVideoBitRate(long j, long j2) {
        double d = (double) j2;
        Double.isNaN(d);
        double d2 = d / 1000.0d;
        double d3 = (double) j;
        Double.isNaN(d3);
        double d4 = (double) (((long) (d3 - ((192000.0d * d2) / 8.0d))) * 8);
        Double.isNaN(d4);
        return (int) (d4 / d2);
    }

    /* loaded from: classes5.dex */
    public static class Quality {
        private final long duration;
        private final double quality;
        private final int targetAudioBitRate;
        private final int targetVideoBitRate;

        private Quality(int i, int i2, double d, long j) {
            this.targetVideoBitRate = i;
            this.targetAudioBitRate = i2;
            this.quality = Math.max(0.0d, Math.min(d, 1.0d));
            this.duration = j;
        }

        public double getQuality() {
            return this.quality;
        }

        public int getTargetVideoBitRate() {
            return this.targetVideoBitRate;
        }

        public int getTargetAudioBitRate() {
            return this.targetAudioBitRate;
        }

        public int getTargetTotalBitRate() {
            return this.targetVideoBitRate + this.targetAudioBitRate;
        }

        public boolean useLowRes() {
            return this.targetVideoBitRate < VideoBitRateCalculator.LOW_RES_TARGET_VIDEO_BITRATE;
        }

        public int getOutputResolution() {
            return useLowRes() ? VideoBitRateCalculator.LOW_RES_OUTPUT_FORMAT : VideoBitRateCalculator.OUTPUT_FORMAT;
        }

        public long getFileSizeEstimate() {
            return (((long) getTargetTotalBitRate()) * this.duration) / 8000;
        }

        public String toString() {
            return "Quality{targetVideoBitRate=" + this.targetVideoBitRate + ", targetAudioBitRate=" + this.targetAudioBitRate + ", quality=" + this.quality + ", duration=" + this.duration + ", filesize=" + getFileSizeEstimate() + '}';
        }
    }
}
