package org.thoughtcrime.securesms.video.videoconverter;

import android.media.MediaCodec;
import android.media.MediaFormat;
import java.io.IOException;
import java.nio.ByteBuffer;

/* loaded from: classes5.dex */
public interface Muxer {
    int addTrack(MediaFormat mediaFormat) throws IOException;

    void release();

    void start() throws IOException;

    void stop() throws IOException;

    void writeSampleData(int i, ByteBuffer byteBuffer, MediaCodec.BufferInfo bufferInfo) throws IOException;
}
