package org.thoughtcrime.securesms.video.videoconverter;

import android.graphics.Bitmap;
import org.signal.core.util.logging.Log;

/* loaded from: classes5.dex */
public final class VideoThumbnailsExtractor {
    private static final String TAG = Log.tag(VideoThumbnailsExtractor.class);

    /* loaded from: classes5.dex */
    public interface Callback {
        void durationKnown(long j);

        void failed();

        boolean publishProgress(int i, Bitmap bitmap);
    }

    VideoThumbnailsExtractor() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x01b9 A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x017f A[EXC_TOP_SPLITTER, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:129:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x017a  */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01b4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void extractThumbnails(org.thoughtcrime.securesms.media.MediaInput r17, int r18, int r19, org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.Callback r20) {
        /*
        // Method dump skipped, instructions count: 490
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.extractThumbnails(org.thoughtcrime.securesms.media.MediaInput, int, int, org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor$Callback):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x00b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void doExtract(android.media.MediaExtractor r23, android.media.MediaCodec r24, org.thoughtcrime.securesms.video.videoconverter.OutputSurface r25, int r26, int r27, long r28, int r30, org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.Callback r31) throws org.thoughtcrime.securesms.video.videoconverter.TranscodingException {
        /*
        // Method dump skipped, instructions count: 330
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor.doExtract(android.media.MediaExtractor, android.media.MediaCodec, org.thoughtcrime.securesms.video.videoconverter.OutputSurface, int, int, long, int, org.thoughtcrime.securesms.video.videoconverter.VideoThumbnailsExtractor$Callback):void");
    }
}
