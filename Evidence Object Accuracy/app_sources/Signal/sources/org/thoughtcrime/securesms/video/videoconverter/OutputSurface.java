package org.thoughtcrime.securesms.video.videoconverter;

import android.graphics.SurfaceTexture;
import android.view.Surface;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;
import javax.microedition.khronos.egl.EGLSurface;
import org.signal.core.util.logging.Log;

/* loaded from: classes5.dex */
public final class OutputSurface implements SurfaceTexture.OnFrameAvailableListener {
    private static final int EGL_OPENGL_ES2_BIT;
    private static final String TAG;
    private static final boolean VERBOSE;
    private EGL10 mEGL;
    private EGLContext mEGLContext;
    private EGLDisplay mEGLDisplay;
    private EGLSurface mEGLSurface;
    private boolean mFrameAvailable;
    private final Object mFrameSyncObject = new Object();
    private Surface mSurface;
    private SurfaceTexture mSurfaceTexture;
    private TextureRender mTextureRender;

    public OutputSurface(int i, int i2, boolean z) throws TranscodingException {
        if (i <= 0 || i2 <= 0) {
            throw new IllegalArgumentException();
        }
        eglSetup(i, i2);
        makeCurrent();
        setup(z);
    }

    public OutputSurface() throws TranscodingException {
        setup(false);
    }

    private void setup(boolean z) throws TranscodingException {
        TextureRender textureRender = new TextureRender(z);
        this.mTextureRender = textureRender;
        textureRender.surfaceCreated();
        SurfaceTexture surfaceTexture = new SurfaceTexture(this.mTextureRender.getTextureId());
        this.mSurfaceTexture = surfaceTexture;
        surfaceTexture.setOnFrameAvailableListener(this);
        this.mSurface = new Surface(this.mSurfaceTexture);
    }

    private void eglSetup(int i, int i2) throws TranscodingException {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        this.mEGL = egl10;
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        this.mEGLDisplay = eglGetDisplay;
        if (this.mEGL.eglInitialize(eglGetDisplay, null)) {
            EGLConfig[] eGLConfigArr = new EGLConfig[1];
            if (this.mEGL.eglChooseConfig(this.mEGLDisplay, new int[]{12324, 8, 12323, 8, 12322, 8, 12339, 1, 12352, 4, 12344}, eGLConfigArr, 1, new int[1])) {
                this.mEGLContext = this.mEGL.eglCreateContext(this.mEGLDisplay, eGLConfigArr[0], EGL10.EGL_NO_CONTEXT, new int[]{12440, 2, 12344});
                checkEglError("eglCreateContext");
                if (this.mEGLContext != null) {
                    this.mEGLSurface = this.mEGL.eglCreatePbufferSurface(this.mEGLDisplay, eGLConfigArr[0], new int[]{12375, i, 12374, i2, 12344});
                    checkEglError("eglCreatePbufferSurface");
                    if (this.mEGLSurface == null) {
                        throw new TranscodingException("surface was null");
                    }
                    return;
                }
                throw new TranscodingException("null context");
            }
            throw new TranscodingException("unable to find RGB888+pbuffer EGL config");
        }
        throw new TranscodingException("unable to initialize EGL10");
    }

    public void release() {
        EGL10 egl10 = this.mEGL;
        if (egl10 != null) {
            if (egl10.eglGetCurrentContext().equals(this.mEGLContext)) {
                EGL10 egl102 = this.mEGL;
                EGLDisplay eGLDisplay = this.mEGLDisplay;
                EGLSurface eGLSurface = EGL10.EGL_NO_SURFACE;
                egl102.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, EGL10.EGL_NO_CONTEXT);
            }
            this.mEGL.eglDestroySurface(this.mEGLDisplay, this.mEGLSurface);
            this.mEGL.eglDestroyContext(this.mEGLDisplay, this.mEGLContext);
        }
        this.mSurface.release();
        this.mEGLDisplay = null;
        this.mEGLContext = null;
        this.mEGLSurface = null;
        this.mEGL = null;
        this.mTextureRender = null;
        this.mSurface = null;
        this.mSurfaceTexture = null;
    }

    private void makeCurrent() throws TranscodingException {
        if (this.mEGL != null) {
            checkEglError("before makeCurrent");
            EGL10 egl10 = this.mEGL;
            EGLDisplay eGLDisplay = this.mEGLDisplay;
            EGLSurface eGLSurface = this.mEGLSurface;
            if (!egl10.eglMakeCurrent(eGLDisplay, eGLSurface, eGLSurface, this.mEGLContext)) {
                throw new TranscodingException("eglMakeCurrent failed");
            }
            return;
        }
        throw new TranscodingException("not configured for makeCurrent");
    }

    public Surface getSurface() {
        return this.mSurface;
    }

    public void changeFragmentShader(String str) throws TranscodingException {
        this.mTextureRender.changeFragmentShader(str);
    }

    public void awaitNewImage() throws TranscodingException {
        synchronized (this.mFrameSyncObject) {
            long currentTimeMillis = System.currentTimeMillis() + 750;
            while (!this.mFrameAvailable) {
                try {
                    this.mFrameSyncObject.wait(750);
                    if (!this.mFrameAvailable && System.currentTimeMillis() > currentTimeMillis) {
                        throw new TranscodingException("Surface frame wait timed out");
                    }
                } catch (InterruptedException e) {
                    throw new TranscodingException(e);
                }
            }
            this.mFrameAvailable = false;
        }
        TextureRender.checkGlError("before updateTexImage");
        this.mSurfaceTexture.updateTexImage();
    }

    public void drawImage() throws TranscodingException {
        this.mTextureRender.drawFrame(this.mSurfaceTexture);
    }

    @Override // android.graphics.SurfaceTexture.OnFrameAvailableListener
    public void onFrameAvailable(SurfaceTexture surfaceTexture) {
        synchronized (this.mFrameSyncObject) {
            if (this.mFrameAvailable) {
                try {
                    throw new TranscodingException("mFrameAvailable already set, frame could be dropped");
                } catch (TranscodingException e) {
                    e.printStackTrace();
                }
            }
            this.mFrameAvailable = true;
            this.mFrameSyncObject.notifyAll();
        }
    }

    private void checkEglError(String str) throws TranscodingException {
        boolean z = false;
        while (true) {
            int eglGetError = this.mEGL.eglGetError();
            if (eglGetError == 12288) {
                break;
            }
            Log.e(TAG, str + ": EGL error: 0x" + Integer.toHexString(eglGetError));
            z = true;
        }
        if (z) {
            throw new TranscodingException("EGL error encountered (see log)");
        }
    }
}
