package org.thoughtcrime.securesms.video.exo;

import android.content.Context;
import android.net.Uri;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.TransferListener;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.mms.PartUriParser;

/* access modifiers changed from: package-private */
/* loaded from: classes5.dex */
public class PartDataSource implements DataSource {
    private final Context context;
    private InputStream inputSteam;
    private final TransferListener listener;
    private Uri uri;

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void addTransferListener(TransferListener transferListener) {
    }

    public PartDataSource(Context context, TransferListener transferListener) {
        this.context = context.getApplicationContext();
        this.listener = transferListener;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public long open(DataSpec dataSpec) throws IOException {
        this.uri = dataSpec.uri;
        AttachmentDatabase attachments = SignalDatabase.attachments();
        PartUriParser partUriParser = new PartUriParser(this.uri);
        DatabaseAttachment attachment = attachments.getAttachment(partUriParser.getPartId());
        if (attachment != null) {
            this.inputSteam = attachments.getAttachmentStream(partUriParser.getPartId(), dataSpec.position);
            TransferListener transferListener = this.listener;
            if (transferListener != null) {
                transferListener.onTransferStart(this, dataSpec, false);
            }
            if (attachment.getSize() - dataSpec.position > 0) {
                return attachment.getSize() - dataSpec.position;
            }
            throw new EOFException("No more data");
        }
        throw new IOException("Attachment not found");
    }

    @Override // com.google.android.exoplayer2.upstream.DataReader
    public int read(byte[] bArr, int i, int i2) throws IOException {
        TransferListener transferListener;
        int read = this.inputSteam.read(bArr, i, i2);
        if (read > 0 && (transferListener = this.listener) != null) {
            transferListener.onBytesTransferred(this, null, false, read);
        }
        return read;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Uri getUri() {
        return this.uri;
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public Map<String, List<String>> getResponseHeaders() {
        return Collections.emptyMap();
    }

    @Override // com.google.android.exoplayer2.upstream.DataSource
    public void close() throws IOException {
        this.inputSteam.close();
    }
}
