package org.thoughtcrime.securesms.contacts;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: SelectedContacts.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J$\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\bH\u0007¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/SelectedContacts;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onCloseIconClicked", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/contacts/SelectedContacts$Model;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SelectedContacts {
    public static final SelectedContacts INSTANCE = new SelectedContacts();

    private SelectedContacts() {
    }

    @JvmStatic
    public static final void register(MappingAdapter mappingAdapter, Function1<? super Model, Unit> function1) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        Intrinsics.checkNotNullParameter(function1, "onCloseIconClicked");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.contacts.SelectedContacts$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SelectedContacts.m1302register$lambda0(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.contact_selection_list_chip));
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m1302register$lambda0(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$onCloseIconClicked");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function1);
    }

    /* compiled from: SelectedContacts.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/SelectedContacts$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "selectedContact", "Lorg/thoughtcrime/securesms/contacts/SelectedContact;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/contacts/SelectedContact;Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getSelectedContact", "()Lorg/thoughtcrime/securesms/contacts/SelectedContact;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final Recipient recipient;
        private final SelectedContact selectedContact;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(SelectedContact selectedContact, Recipient recipient) {
            Intrinsics.checkNotNullParameter(selectedContact, "selectedContact");
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.selectedContact = selectedContact;
            this.recipient = recipient;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final SelectedContact getSelectedContact() {
            return this.selectedContact;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return model.selectedContact.matches(this.selectedContact) && Intrinsics.areEqual(this.recipient, model.recipient);
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return areItemsTheSame(model) && this.recipient.hasSameContent(model.recipient);
        }
    }

    /* compiled from: SelectedContacts.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\u000b\u001a\u00020\u00072\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/SelectedContacts$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/contacts/SelectedContacts$Model;", "itemView", "Landroid/view/View;", "onCloseIconClicked", "Lkotlin/Function1;", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "chip", "Lorg/thoughtcrime/securesms/contacts/ContactChip;", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ContactChip chip;
        private final Function1<Model, Unit> onCloseIconClicked;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.contacts.SelectedContacts$Model, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function1<? super Model, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "onCloseIconClicked");
            this.onCloseIconClicked = function1;
            View findViewById = view.findViewById(R.id.contact_chip);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.contact_chip)");
            this.chip = (ContactChip) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.chip.setText(model.getRecipient().getShortDisplayName(this.context));
            this.chip.setContact(model.getSelectedContact());
            this.chip.setCloseIconVisible(true);
            this.chip.setOnCloseIconClickListener(new SelectedContacts$ViewHolder$$ExternalSyntheticLambda0(this, model));
            this.chip.setAvatar(GlideApp.with(this.itemView), model.getRecipient(), null);
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1303bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.onCloseIconClicked.invoke(model);
        }
    }
}
