package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.core.content.ContextCompat;
import com.makeramen.roundedimageview.RoundedDrawable;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;

/* loaded from: classes4.dex */
public class TransparentContactPhoto implements FallbackContactPhoto {
    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor) {
        return asDrawable(context, avatarColor, false);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return RoundedDrawable.fromDrawable(context.getResources().getDrawable(17170445));
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asSmallDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return asDrawable(context, avatarColor, z);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asCallCard(Context context) {
        return ContextCompat.getDrawable(context, R.drawable.ic_contact_picture_large);
    }
}
