package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.widget.ImageView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import com.makeramen.roundedimageview.RoundedDrawable;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;

/* loaded from: classes4.dex */
public class ResourceContactPhoto implements FallbackContactPhoto {
    private final int callCardResourceId;
    private final int resourceId;
    private ImageView.ScaleType scaleType;
    private final int smallResourceId;

    public ResourceContactPhoto(int i) {
        this(i, i, i);
    }

    public ResourceContactPhoto(int i, int i2) {
        this(i, i2, i);
    }

    public ResourceContactPhoto(int i, int i2, int i3) {
        this.scaleType = ImageView.ScaleType.CENTER;
        this.resourceId = i;
        this.callCardResourceId = i3;
        this.smallResourceId = i2;
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor) {
        return asDrawable(context, avatarColor, false);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return buildDrawable(context, this.resourceId, avatarColor, z);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asSmallDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return buildDrawable(context, this.smallResourceId, avatarColor, z);
    }

    private Drawable buildDrawable(Context context, int i, AvatarColor avatarColor, boolean z) {
        Avatars.ForegroundColor foregroundColor = Avatars.getForegroundColor(avatarColor);
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.circle_tintable);
        Objects.requireNonNull(drawable);
        RoundedDrawable roundedDrawable = (RoundedDrawable) RoundedDrawable.fromDrawable(AppCompatResources.getDrawable(context, i));
        roundedDrawable.setScaleType(this.scaleType);
        drawable.setColorFilter(z ? foregroundColor.getColorInt() : avatarColor.colorInt(), PorterDuff.Mode.SRC_IN);
        roundedDrawable.setColorFilter(z ? avatarColor.colorInt() : foregroundColor.getColorInt(), PorterDuff.Mode.SRC_ATOP);
        return new ExpandingLayerDrawable(new Drawable[]{drawable, roundedDrawable});
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asCallCard(Context context) {
        return AppCompatResources.getDrawable(context, this.callCardResourceId);
    }

    /* loaded from: classes4.dex */
    public static class ExpandingLayerDrawable extends LayerDrawable {
        @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
        public int getIntrinsicHeight() {
            return -1;
        }

        @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
        public int getIntrinsicWidth() {
            return -1;
        }

        public ExpandingLayerDrawable(Drawable[] drawableArr) {
            super(drawableArr);
        }
    }
}
