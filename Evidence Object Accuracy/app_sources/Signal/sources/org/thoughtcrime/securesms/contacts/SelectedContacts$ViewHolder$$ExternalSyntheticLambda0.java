package org.thoughtcrime.securesms.contacts;

import android.view.View;
import org.thoughtcrime.securesms.contacts.SelectedContacts;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SelectedContacts$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ SelectedContacts.ViewHolder f$0;
    public final /* synthetic */ SelectedContacts.Model f$1;

    public /* synthetic */ SelectedContacts$ViewHolder$$ExternalSyntheticLambda0(SelectedContacts.ViewHolder viewHolder, SelectedContacts.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        SelectedContacts.ViewHolder.m1303bind$lambda0(this.f$0, this.f$1, view);
    }
}
