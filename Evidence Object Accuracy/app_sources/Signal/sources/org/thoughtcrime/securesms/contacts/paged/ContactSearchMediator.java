package org.thoughtcrime.securesms.contacts.paged;

import android.content.DialogInterface;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.stories.settings.custom.PrivateStorySettingsFragment;
import org.thoughtcrime.securesms.stories.settings.my.MyStorySettingsFragment;
import org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.PagingMappingAdapter;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: ContactSearchMediator.kt */
@Metadata(d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001+Bc\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\r0\u000b\u0012(\b\u0002\u0010\u000e\u001a\"\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u0011\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u000f¢\u0006\u0002\u0010\u0013J\u0014\u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\u0011J\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\u0012\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u001cJ\u0010\u0010\u001d\u001a\u00020\u00172\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fJ\u0006\u0010 \u001a\u00020\u0017J\u0014\u0010!\u001a\u00020\u00172\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\u0014\u0010#\u001a\u00020\u00172\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J \u0010$\u001a\u00020\u00172\u0006\u0010%\u001a\u00020\u00102\u0006\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020\tH\u0002J \u0010)\u001a\u00020\u00172\u0006\u0010%\u001a\u00020\u00102\u0006\u0010&\u001a\u00020*2\u0006\u0010(\u001a\u00020\tH\u0002R.\u0010\u000e\u001a\"\u0012\u0006\u0012\u0004\u0018\u00010\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u0011\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u00110\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchMediator;", "", "fragment", "Landroidx/fragment/app/Fragment;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "selectionLimits", "Lorg/thoughtcrime/securesms/groups/SelectionLimits;", "displayCheckBox", "", "mapStateToConfiguration", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "contactSelectionPreFilter", "Lkotlin/Function2;", "Landroid/view/View;", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "(Landroidx/fragment/app/Fragment;Landroidx/recyclerview/widget/RecyclerView;Lorg/thoughtcrime/securesms/groups/SelectionLimits;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;)V", "viewModel", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchViewModel;", "addToVisibleGroupStories", "", "groupStories", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey$Story;", "getSelectedContacts", "getSelectionState", "Landroidx/lifecycle/LiveData;", "onFilterChanged", "filter", "", "refresh", "setKeysNotSelected", "keys", "setKeysSelected", "toggleSelection", "view", "contactSearchData", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "isSelected", "toggleStorySelection", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "StoryContextMenuCallbacks", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchMediator {
    private final Function2<View, Set<? extends ContactSearchKey>, Set<ContactSearchKey>> contactSelectionPreFilter;
    private final Fragment fragment;
    private final ContactSearchViewModel viewModel;

    /* JADX DEBUG: Multi-variable search result rejected for r13v0, resolved type: kotlin.jvm.functions.Function2<? super android.view.View, ? super java.util.Set<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey>, ? extends java.util.Set<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey>> */
    /* JADX WARN: Multi-variable type inference failed */
    public ContactSearchMediator(Fragment fragment, RecyclerView recyclerView, SelectionLimits selectionLimits, boolean z, Function1<? super ContactSearchState, ContactSearchConfiguration> function1, Function2<? super View, ? super Set<? extends ContactSearchKey>, ? extends Set<? extends ContactSearchKey>> function2) {
        Intrinsics.checkNotNullParameter(fragment, "fragment");
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        Intrinsics.checkNotNullParameter(selectionLimits, "selectionLimits");
        Intrinsics.checkNotNullParameter(function1, "mapStateToConfiguration");
        Intrinsics.checkNotNullParameter(function2, "contactSelectionPreFilter");
        this.fragment = fragment;
        this.contactSelectionPreFilter = function2;
        ViewModel viewModel = new ViewModelProvider(fragment, new ContactSearchViewModel.Factory(selectionLimits, new ContactSearchRepository())).get(ContactSearchViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(fragme…rchViewModel::class.java)");
        ContactSearchViewModel contactSearchViewModel = (ContactSearchViewModel) viewModel;
        this.viewModel = contactSearchViewModel;
        PagingMappingAdapter pagingMappingAdapter = new PagingMappingAdapter();
        recyclerView.setAdapter(pagingMappingAdapter);
        ContactSearchItems.INSTANCE.register(pagingMappingAdapter, z, new Function3<View, ContactSearchData, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator.2
            @Override // kotlin.jvm.functions.Function3
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2, Object obj3) {
                invoke((View) obj, (ContactSearchData) obj2, ((Boolean) obj3).booleanValue());
                return Unit.INSTANCE;
            }

            public final void invoke(View view, ContactSearchData contactSearchData, boolean z2) {
                Intrinsics.checkNotNullParameter(view, "p0");
                Intrinsics.checkNotNullParameter(contactSearchData, "p1");
                ((ContactSearchMediator) this.receiver).toggleSelection(view, contactSearchData, z2);
            }
        }, new Function3<View, ContactSearchData.Story, Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator.3
            @Override // kotlin.jvm.functions.Function3
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2, Object obj3) {
                invoke((View) obj, (ContactSearchData.Story) obj2, ((Boolean) obj3).booleanValue());
                return Unit.INSTANCE;
            }

            public final void invoke(View view, ContactSearchData.Story story, boolean z2) {
                Intrinsics.checkNotNullParameter(view, "p0");
                Intrinsics.checkNotNullParameter(story, "p1");
                ((ContactSearchMediator) this.receiver).toggleStorySelection(view, story, z2);
            }
        }, new StoryContextMenuCallbacks(), new Function1<ContactSearchData.Expand, Unit>(this) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator.4
            final /* synthetic */ ContactSearchMediator this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ContactSearchData.Expand expand) {
                invoke(expand);
                return Unit.INSTANCE;
            }

            public final void invoke(ContactSearchData.Expand expand) {
                Intrinsics.checkNotNullParameter(expand, "it");
                this.this$0.viewModel.expandSection(expand.getSectionKey());
            }
        });
        LiveData combineLatest = LiveDataUtil.combineLatest(contactSearchViewModel.getData(), contactSearchViewModel.getSelectionState(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return new Pair((List) obj, (Set) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(\n      vie…State,\n      ::Pair\n    )");
        combineLatest.observe(fragment.getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ContactSearchMediator.$r8$lambda$yRjB8c20rjIajcrxYe8H6nbTMGk(PagingMappingAdapter.this, (Pair) obj);
            }
        });
        contactSearchViewModel.getController().observe(fragment.getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ContactSearchMediator.$r8$lambda$7kAzXxoGTJXJj22pN42SLH2AFrI(PagingMappingAdapter.this, (PagingController) obj);
            }
        });
        contactSearchViewModel.getConfigurationState().observe(fragment.getViewLifecycleOwner(), new Observer(function1) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator$$ExternalSyntheticLambda3
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ContactSearchMediator.m1325$r8$lambda$teXCbvk_dmZj0zDV9GWGkP8KwE(ContactSearchMediator.this, this.f$1, (ContactSearchState) obj);
            }
        });
    }

    public /* synthetic */ ContactSearchMediator(Fragment fragment, RecyclerView recyclerView, SelectionLimits selectionLimits, boolean z, Function1 function1, Function2 function2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(fragment, recyclerView, selectionLimits, z, function1, (i & 32) != 0 ? AnonymousClass1.INSTANCE : function2);
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1326_init_$lambda0(PagingMappingAdapter pagingMappingAdapter, Pair pair) {
        Intrinsics.checkNotNullParameter(pagingMappingAdapter, "$adapter");
        pagingMappingAdapter.submitList(ContactSearchItems.INSTANCE.toMappingModelList((List) pair.component1(), (Set) pair.component2()));
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m1327_init_$lambda1(PagingMappingAdapter pagingMappingAdapter, PagingController pagingController) {
        Intrinsics.checkNotNullParameter(pagingMappingAdapter, "$adapter");
        pagingMappingAdapter.setPagingController(pagingController);
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m1328_init_$lambda2(ContactSearchMediator contactSearchMediator, Function1 function1, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(contactSearchMediator, "this$0");
        Intrinsics.checkNotNullParameter(function1, "$mapStateToConfiguration");
        ContactSearchViewModel contactSearchViewModel = contactSearchMediator.viewModel;
        Intrinsics.checkNotNullExpressionValue(contactSearchState, "it");
        contactSearchViewModel.setConfiguration((ContactSearchConfiguration) function1.invoke(contactSearchState));
    }

    public final void onFilterChanged(String str) {
        this.viewModel.setQuery(str);
    }

    public final void setKeysSelected(Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(set, "keys");
        this.viewModel.setKeysSelected(this.contactSelectionPreFilter.invoke(null, set));
    }

    public final void setKeysNotSelected(Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(set, "keys");
        this.viewModel.setKeysNotSelected(set);
    }

    public final Set<ContactSearchKey> getSelectedContacts() {
        return this.viewModel.getSelectedContacts();
    }

    public final LiveData<Set<ContactSearchKey>> getSelectionState() {
        return this.viewModel.getSelectionState();
    }

    public final void addToVisibleGroupStories(Set<ContactSearchKey.RecipientSearchKey.Story> set) {
        Intrinsics.checkNotNullParameter(set, "groupStories");
        this.viewModel.addToVisibleGroupStories(set);
    }

    public final void refresh() {
        this.viewModel.refresh();
    }

    public final void toggleStorySelection(View view, ContactSearchData.Story story, boolean z) {
        if (!story.getRecipient().isMyStory() || SignalStore.storyValues().getUserHasBeenNotifiedAboutStories()) {
            toggleSelection(view, story, z);
            return;
        }
        ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Companion companion = ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Companion;
        FragmentManager childFragmentManager = this.fragment.getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "fragment.childFragmentManager");
        companion.show(childFragmentManager);
    }

    public final void toggleSelection(View view, ContactSearchData contactSearchData, boolean z) {
        if (z) {
            this.viewModel.setKeysNotSelected(SetsKt__SetsJVMKt.setOf(contactSearchData.getContactSearchKey()));
        } else {
            this.viewModel.setKeysSelected(this.contactSelectionPreFilter.invoke(view, SetsKt__SetsJVMKt.setOf(contactSearchData.getContactSearchKey())));
        }
    }

    /* compiled from: ContactSearchMediator.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0018\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchMediator$StoryContextMenuCallbacks;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryContextMenuCallbacks;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchMediator;)V", "onDeletePrivateStory", "", "story", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "isSelected", "", "onOpenStorySettings", "onRemoveGroupStory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class StoryContextMenuCallbacks implements ContactSearchItems.StoryContextMenuCallbacks {
        /* renamed from: onDeletePrivateStory$lambda-3 */
        public static final void m1330onDeletePrivateStory$lambda3(DialogInterface dialogInterface, int i) {
        }

        /* renamed from: onRemoveGroupStory$lambda-1 */
        public static final void m1332onRemoveGroupStory$lambda1(DialogInterface dialogInterface, int i) {
        }

        public StoryContextMenuCallbacks() {
            ContactSearchMediator.this = r1;
        }

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchItems.StoryContextMenuCallbacks
        public void onOpenStorySettings(ContactSearchData.Story story) {
            Intrinsics.checkNotNullParameter(story, "story");
            if (story.getRecipient().isMyStory()) {
                MyStorySettingsFragment.Companion.createAsDialog().show(ContactSearchMediator.this.fragment.getChildFragmentManager(), (String) null);
                return;
            }
            PrivateStorySettingsFragment.Companion companion = PrivateStorySettingsFragment.Companion;
            DistributionListId requireDistributionListId = story.getRecipient().requireDistributionListId();
            Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "story.recipient.requireDistributionListId()");
            companion.createAsDialog(requireDistributionListId).show(ContactSearchMediator.this.fragment.getChildFragmentManager(), (String) null);
        }

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchItems.StoryContextMenuCallbacks
        public void onRemoveGroupStory(ContactSearchData.Story story, boolean z) {
            Intrinsics.checkNotNullParameter(story, "story");
            new MaterialAlertDialogBuilder(ContactSearchMediator.this.fragment.requireContext()).setTitle(R.string.ContactSearchMediator__remove_group_story).setMessage(R.string.ContactSearchMediator__this_will_remove).setPositiveButton(R.string.ContactSearchMediator__remove, (DialogInterface.OnClickListener) new ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda2(ContactSearchMediator.this, story)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda3()).show();
        }

        /* renamed from: onRemoveGroupStory$lambda-0 */
        public static final void m1331onRemoveGroupStory$lambda0(ContactSearchMediator contactSearchMediator, ContactSearchData.Story story, DialogInterface dialogInterface, int i) {
            Intrinsics.checkNotNullParameter(contactSearchMediator, "this$0");
            Intrinsics.checkNotNullParameter(story, "$story");
            contactSearchMediator.viewModel.removeGroupStory(story);
        }

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchItems.StoryContextMenuCallbacks
        public void onDeletePrivateStory(ContactSearchData.Story story, boolean z) {
            Intrinsics.checkNotNullParameter(story, "story");
            new MaterialAlertDialogBuilder(ContactSearchMediator.this.fragment.requireContext()).setTitle(R.string.ContactSearchMediator__delete_story).setMessage((CharSequence) ContactSearchMediator.this.fragment.getString(R.string.ContactSearchMediator__delete_the_private, story.getRecipient().getDisplayName(ContactSearchMediator.this.fragment.requireContext()))).setPositiveButton(SpanUtil.color(ContextCompat.getColor(ContactSearchMediator.this.fragment.requireContext(), R.color.signal_colorError), ContactSearchMediator.this.fragment.getString(R.string.ContactSearchMediator__delete)), (DialogInterface.OnClickListener) new ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda0(ContactSearchMediator.this, story)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda1()).show();
        }

        /* renamed from: onDeletePrivateStory$lambda-2 */
        public static final void m1329onDeletePrivateStory$lambda2(ContactSearchMediator contactSearchMediator, ContactSearchData.Story story, DialogInterface dialogInterface, int i) {
            Intrinsics.checkNotNullParameter(contactSearchMediator, "this$0");
            Intrinsics.checkNotNullParameter(story, "$story");
            contactSearchMediator.viewModel.deletePrivateStory(story);
        }
    }
}
