package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.graphics.drawable.Drawable;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;

/* loaded from: classes4.dex */
public interface FallbackContactPhoto {
    Drawable asCallCard(Context context);

    Drawable asDrawable(Context context, AvatarColor avatarColor);

    Drawable asDrawable(Context context, AvatarColor avatarColor, boolean z);

    Drawable asSmallDrawable(Context context, AvatarColor avatarColor, boolean z);
}
