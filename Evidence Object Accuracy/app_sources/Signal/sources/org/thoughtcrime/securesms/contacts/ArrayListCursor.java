package org.thoughtcrime.securesms.contacts;

import android.database.AbstractCursor;
import android.database.CursorWindow;
import java.util.ArrayList;

/* loaded from: classes4.dex */
public class ArrayListCursor extends AbstractCursor {
    private String[] mColumnNames;
    private ArrayList<Object>[] mRows;

    public boolean deleteRow() {
        return false;
    }

    public ArrayListCursor(String[] strArr, ArrayList<ArrayList> arrayList) {
        boolean z;
        int length = strArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (strArr[i].compareToIgnoreCase("_id") == 0) {
                this.mColumnNames = strArr;
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            String[] strArr2 = new String[length + 1];
            this.mColumnNames = strArr2;
            System.arraycopy(strArr, 0, strArr2, 0, strArr.length);
            this.mColumnNames[length] = "_id";
        }
        int size = arrayList.size();
        this.mRows = new ArrayList[size];
        for (int i2 = 0; i2 < size; i2++) {
            this.mRows[i2] = arrayList.get(i2);
            if (!z) {
                this.mRows[i2].add(Integer.valueOf(i2));
            }
        }
    }

    @Override // android.database.AbstractCursor, android.database.CrossProcessCursor
    public void fillWindow(int i, CursorWindow cursorWindow) {
        if (i >= 0 && i <= getCount()) {
            cursorWindow.acquireReference();
            try {
                int i2 = ((AbstractCursor) this).mPos;
                ((AbstractCursor) this).mPos = i - 1;
                cursorWindow.clear();
                cursorWindow.setStartPosition(i);
                int columnCount = getColumnCount();
                cursorWindow.setNumColumns(columnCount);
                while (moveToNext() && cursorWindow.allocRow()) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= columnCount) {
                            break;
                        }
                        Object obj = this.mRows[((AbstractCursor) this).mPos].get(i3);
                        if (obj != null) {
                            if (obj instanceof byte[]) {
                                if (!cursorWindow.putBlob((byte[]) obj, ((AbstractCursor) this).mPos, i3)) {
                                    cursorWindow.freeLastRow();
                                    break;
                                }
                                i3++;
                            } else if (!cursorWindow.putString(obj.toString(), ((AbstractCursor) this).mPos, i3)) {
                                cursorWindow.freeLastRow();
                                break;
                            } else {
                                i3++;
                            }
                        } else if (!cursorWindow.putNull(((AbstractCursor) this).mPos, i3)) {
                            cursorWindow.freeLastRow();
                            break;
                        } else {
                            i3++;
                        }
                    }
                }
                ((AbstractCursor) this).mPos = i2;
            } catch (IllegalStateException unused) {
            } catch (Throwable th) {
                cursorWindow.releaseReference();
                throw th;
            }
            cursorWindow.releaseReference();
        }
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getCount() {
        return this.mRows.length;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String[] getColumnNames() {
        return this.mColumnNames;
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public byte[] getBlob(int i) {
        return (byte[]) this.mRows[((AbstractCursor) this).mPos].get(i);
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public String getString(int i) {
        Object obj = this.mRows[((AbstractCursor) this).mPos].get(i);
        if (obj == null) {
            return null;
        }
        return obj.toString();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public short getShort(int i) {
        return ((Number) this.mRows[((AbstractCursor) this).mPos].get(i)).shortValue();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public int getInt(int i) {
        return ((Number) this.mRows[((AbstractCursor) this).mPos].get(i)).intValue();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public long getLong(int i) {
        return ((Number) this.mRows[((AbstractCursor) this).mPos].get(i)).longValue();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public float getFloat(int i) {
        return ((Number) this.mRows[((AbstractCursor) this).mPos].get(i)).floatValue();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public double getDouble(int i) {
        return ((Number) this.mRows[((AbstractCursor) this).mPos].get(i)).doubleValue();
    }

    @Override // android.database.AbstractCursor, android.database.Cursor
    public boolean isNull(int i) {
        return this.mRows[((AbstractCursor) this).mPos].get(i) == null;
    }
}
