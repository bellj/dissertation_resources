package org.thoughtcrime.securesms.contacts.paged;

import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import kotlin.text.StringsKt__StringsKt;
import org.signal.core.util.CursorUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.keyvalue.StorySend;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ContactSearchPagedDataSourceRepository.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\u001c\u0010\u000b\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u000e\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0016J\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\f2\u0006\u0010\r\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\fH\u0016J\u0010\u0010\u001d\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\fH\u0016J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\fH\u0016J\u0010\u0010\u001f\u001a\u00020\n2\u0006\u0010\u001c\u001a\u00020\fH\u0016J\u0014\u0010 \u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u0010\u0010!\u001a\u00020\"2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u001c\u0010#\u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010$\u001a\u00020\"H\u0016J\u0014\u0010%\u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016J\u001c\u0010&\u001a\u0004\u0018\u00010\f2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010$\u001a\u00020\"H\u0016J\u001a\u0010'\u001a\u00020\"2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSourceRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "contactRepository", "Lorg/thoughtcrime/securesms/contacts/ContactRepository;", "getDistributionListMembershipCount", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getGroupContacts", "Landroid/database/Cursor;", "section", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Groups;", "query", "", "getGroupStories", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "getLatestStorySends", "", "Lorg/thoughtcrime/securesms/keyvalue/StorySend;", "activeStoryCutoffDuration", "", "getRecents", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents;", "getRecipientFromDistributionListCursor", "cursor", "getRecipientFromGroupCursor", "getRecipientFromRecipientCursor", "getRecipientFromThreadCursor", "getStories", "myStoryContainsQuery", "", "queryNonGroupContacts", "includeSelf", "queryNonSignalContacts", "querySignalContacts", "recipientNameContainsQuery", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class ContactSearchPagedDataSourceRepository {
    private final ContactRepository contactRepository;
    private final Context context;

    public ContactSearchPagedDataSourceRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
        this.contactRepository = new ContactRepository(context, context.getString(R.string.note_to_self));
    }

    public List<StorySend> getLatestStorySends(long j) {
        return SignalStore.storyValues().getLatestActiveStorySendTimestamps(System.currentTimeMillis() - j);
    }

    public Cursor querySignalContacts(String str, boolean z) {
        ContactRepository contactRepository = this.contactRepository;
        if (str == null) {
            str = "";
        }
        return contactRepository.querySignalContacts(str, z);
    }

    public Cursor queryNonSignalContacts(String str) {
        ContactRepository contactRepository = this.contactRepository;
        if (str == null) {
            str = "";
        }
        return contactRepository.queryNonSignalContacts(str);
    }

    public Cursor queryNonGroupContacts(String str, boolean z) {
        ContactRepository contactRepository = this.contactRepository;
        if (str == null) {
            str = "";
        }
        return contactRepository.queryNonGroupContacts(str, z);
    }

    public Cursor getGroupContacts(ContactSearchConfiguration.Section.Groups groups, String str) {
        Intrinsics.checkNotNullParameter(groups, "section");
        GroupDatabase groups2 = SignalDatabase.Companion.groups();
        if (str == null) {
            str = "";
        }
        return groups2.queryGroupsByTitle(str, groups.getIncludeInactive(), !groups.getIncludeV1(), !groups.getIncludeMms()).cursor;
    }

    public Cursor getRecents(ContactSearchConfiguration.Section.Recents recents) {
        Intrinsics.checkNotNullParameter(recents, "section");
        ThreadDatabase threads = SignalDatabase.Companion.threads();
        int limit = recents.getLimit();
        boolean includeInactiveGroups = recents.getIncludeInactiveGroups();
        boolean z = false;
        boolean z2 = recents.getMode() == ContactSearchConfiguration.Section.Recents.Mode.INDIVIDUALS;
        if (recents.getMode() == ContactSearchConfiguration.Section.Recents.Mode.GROUPS) {
            z = true;
        }
        return threads.getRecentConversationList(limit, includeInactiveGroups, z2, z, !recents.getIncludeGroupsV1(), !recents.getIncludeSms(), !recents.getIncludeSelf());
    }

    public Cursor getStories(String str) {
        return SignalDatabase.Companion.distributionLists().getAllListsForContactSelectionUiCursor(str, myStoryContainsQuery(str == null ? "" : str));
    }

    public Recipient getRecipientFromDistributionListCursor(Cursor cursor) {
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        Recipient resolved = Recipient.resolved(RecipientId.from(CursorUtil.requireLong(cursor, "recipient_id")));
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(RecipientId.fro…tDatabase.RECIPIENT_ID)))");
        return resolved;
    }

    public Recipient getRecipientFromThreadCursor(Cursor cursor) {
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        Recipient resolved = Recipient.resolved(RecipientId.from(CursorUtil.requireLong(cursor, ThreadDatabase.RECIPIENT_ID)));
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(RecipientId.fro…dDatabase.RECIPIENT_ID)))");
        return resolved;
    }

    public Recipient getRecipientFromRecipientCursor(Cursor cursor) {
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        Recipient resolved = Recipient.resolved(RecipientId.from(CursorUtil.requireLong(cursor, ContactRepository.ID_COLUMN)));
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(RecipientId.fro…ctRepository.ID_COLUMN)))");
        return resolved;
    }

    public Recipient getRecipientFromGroupCursor(Cursor cursor) {
        Intrinsics.checkNotNullParameter(cursor, "cursor");
        Recipient resolved = Recipient.resolved(RecipientId.from(CursorUtil.requireLong(cursor, "recipient_id")));
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(RecipientId.fro…pDatabase.RECIPIENT_ID)))");
        return resolved;
    }

    public int getDistributionListMembershipCount(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId requireDistributionListId = recipient.requireDistributionListId();
        Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "recipient.requireDistributionListId()");
        return distributionLists.getMemberCount(requireDistributionListId);
    }

    public Set<ContactSearchData.Story> getGroupStories() {
        List<GroupId> groupsToDisplayAsStories = SignalDatabase.Companion.groups().getGroupsToDisplayAsStories();
        Intrinsics.checkNotNullExpressionValue(groupsToDisplayAsStories, "SignalDatabase.groups.groupsToDisplayAsStories");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(groupsToDisplayAsStories, 10));
        for (GroupId groupId : groupsToDisplayAsStories) {
            RecipientDatabase recipients = SignalDatabase.Companion.recipients();
            Intrinsics.checkNotNullExpressionValue(groupId, "it");
            Recipient resolved = Recipient.resolved(recipients.getOrInsertFromGroupId(groupId));
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(SignalDatabase.…tOrInsertFromGroupId(it))");
            arrayList.add(new ContactSearchData.Story(resolved, resolved.getParticipantIds().size()));
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }

    public boolean recipientNameContainsQuery(Recipient recipient, String str) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        if (str == null || (StringsKt__StringsJVMKt.isBlank(str))) {
            return true;
        }
        String displayName = recipient.getDisplayName(this.context);
        Intrinsics.checkNotNullExpressionValue(displayName, "recipient.getDisplayName(context)");
        return StringsKt__StringsKt.contains$default((CharSequence) displayName, (CharSequence) str, false, 2, (Object) null);
    }

    public boolean myStoryContainsQuery(String str) {
        Intrinsics.checkNotNullParameter(str, "query");
        if (str.length() == 0) {
            return true;
        }
        String string = this.context.getString(R.string.Recipient_my_story);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.Recipient_my_story)");
        return StringsKt__StringsKt.contains$default((CharSequence) string, (CharSequence) str, false, 2, (Object) null);
    }
}
