package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class SelectedContact {
    private final String number;
    private final RecipientId recipientId;
    private final String username;

    public static SelectedContact forPhone(RecipientId recipientId, String str) {
        return new SelectedContact(recipientId, str, null);
    }

    public static SelectedContact forUsername(RecipientId recipientId, String str) {
        return new SelectedContact(recipientId, null, str);
    }

    public static SelectedContact forRecipientId(RecipientId recipientId) {
        return new SelectedContact(recipientId, null, null);
    }

    private SelectedContact(RecipientId recipientId, String str, String str2) {
        this.recipientId = recipientId;
        this.number = str;
        this.username = str2;
    }

    public RecipientId getOrCreateRecipientId(Context context) {
        RecipientId recipientId = this.recipientId;
        if (recipientId != null) {
            return recipientId;
        }
        String str = this.number;
        if (str != null) {
            return Recipient.external(context, str).getId();
        }
        throw new AssertionError();
    }

    public boolean matches(SelectedContact selectedContact) {
        String str;
        RecipientId recipientId;
        if (selectedContact == null) {
            return false;
        }
        RecipientId recipientId2 = this.recipientId;
        if (recipientId2 != null && (recipientId = selectedContact.recipientId) != null) {
            return recipientId2.equals(recipientId);
        }
        String str2 = this.number;
        if ((str2 == null || !str2.equals(selectedContact.number)) && ((str = this.username) == null || !str.equals(selectedContact.username))) {
            return false;
        }
        return true;
    }
}
