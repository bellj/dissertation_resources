package org.thoughtcrime.securesms.contacts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes4.dex */
public final class SelectedContactSet {
    private final List<SelectedContact> contacts = new LinkedList();

    public boolean add(SelectedContact selectedContact) {
        if (contains(selectedContact)) {
            return false;
        }
        this.contacts.add(selectedContact);
        return true;
    }

    public boolean contains(SelectedContact selectedContact) {
        for (SelectedContact selectedContact2 : this.contacts) {
            if (selectedContact.matches(selectedContact2)) {
                return true;
            }
        }
        return false;
    }

    public List<SelectedContact> getContacts() {
        return new ArrayList(this.contacts);
    }

    public int size() {
        return this.contacts.size();
    }

    public void clear() {
        this.contacts.clear();
    }

    public int remove(SelectedContact selectedContact) {
        Iterator<SelectedContact> it = this.contacts.iterator();
        int i = 0;
        while (it.hasNext()) {
            if (it.next().matches(selectedContact)) {
                it.remove();
                i++;
            }
        }
        return i;
    }
}
