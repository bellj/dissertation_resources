package org.thoughtcrime.securesms.contacts;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import org.signal.contacts.SystemContactsRepository;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ContactsSyncAdapter extends AbstractThreadedSyncAdapter {
    private static final int FULL_SYNC_THRESHOLD;
    private static final String TAG = Log.tag(ContactsSyncAdapter.class);

    public ContactsSyncAdapter(Context context, boolean z) {
        super(context, z);
    }

    @Override // android.content.AbstractThreadedSyncAdapter
    public void onPerformSync(Account account, Bundle bundle, String str, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        String str2 = TAG;
        Log.i(str2, "onPerformSync(" + str + ")");
        Context context = getContext();
        if (SignalStore.account().getE164() == null) {
            Log.i(str2, "No local number set, skipping all sync operations.");
        } else if (!SignalStore.account().isRegistered()) {
            Log.i(str2, "Not push registered. Just syncing contact info.");
            ContactDiscovery.syncRecipientInfoWithSystemContacts(context);
        } else {
            Set difference = SetUtil.difference((Set) Collection$EL.stream(SystemContactsRepository.getAllDisplayNumbers(context)).map(new Function(context) { // from class: org.thoughtcrime.securesms.contacts.ContactsSyncAdapter$$ExternalSyntheticLambda0
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ContactsSyncAdapter.$r8$lambda$ggSCuLzhAs9D71FxlnaPZ6mNqus(this.f$0, (String) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toSet()), SignalDatabase.recipients().getAllE164s());
            if (difference.size() > 10) {
                Log.i(str2, "There are " + difference.size() + " unknown contacts. Doing a full sync.");
                try {
                    ContactDiscovery.refreshAll(context, true);
                } catch (IOException e) {
                    Log.w(TAG, e);
                }
            } else if (difference.size() > 0) {
                List list = Stream.of(difference).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contacts.ContactsSyncAdapter$$ExternalSyntheticLambda1
                    @Override // com.annimon.stream.function.Predicate
                    public final boolean test(Object obj) {
                        return ContactsSyncAdapter.m1301$r8$lambda$jODjobMdI4TRJyj9JB_ykGfWFc((String) obj);
                    }
                }).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.contacts.ContactsSyncAdapter$$ExternalSyntheticLambda2
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return ContactsSyncAdapter.$r8$lambda$lWV7NXWhSTmjzkZY6n6Se9cizcA(ContactsSyncAdapter.this, (String) obj);
                    }
                }).toList();
                Log.i(str2, "There are " + difference.size() + " unknown E164s, which are now " + list.size() + " recipients. Only syncing these specific contacts.");
                try {
                    ContactDiscovery.refresh(context, (List<? extends Recipient>) list, true);
                    if (Util.isDefaultSmsProvider(context)) {
                        ContactDiscovery.syncRecipientInfoWithSystemContacts(context);
                    }
                } catch (IOException e2) {
                    Log.w(TAG, "Failed to refresh! Scheduling for later.", e2);
                    ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(true));
                }
            } else {
                Log.i(str2, "No new contacts. Just syncing system contact data.");
                ContactDiscovery.syncRecipientInfoWithSystemContacts(context);
            }
        }
    }

    public static /* synthetic */ String lambda$onPerformSync$0(Context context, String str) {
        return PhoneNumberFormatter.get(context).format(str);
    }

    public /* synthetic */ Recipient lambda$onPerformSync$2(String str) {
        return Recipient.external(getContext(), str);
    }

    @Override // android.content.AbstractThreadedSyncAdapter
    public void onSyncCanceled() {
        Log.w(TAG, "onSyncCanceled()");
    }

    @Override // android.content.AbstractThreadedSyncAdapter
    public void onSyncCanceled(Thread thread) {
        String str = TAG;
        Log.w(str, "onSyncCanceled(" + thread + ")");
    }
}
