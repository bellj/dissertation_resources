package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.CharacterIterable;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.RecyclerViewFastScroller;
import org.thoughtcrime.securesms.contacts.LetterHeaderDecoration;
import org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ContactSelectionListAdapter extends CursorRecyclerViewAdapter<ViewHolder> implements RecyclerViewFastScroller.FastScrollAdapter, StickyHeaderDecoration.StickyHeaderAdapter<HeaderViewHolder> {
    public static final int PAYLOAD_SELECTION_CHANGE;
    private static final String TAG = Log.tag(ContactSelectionListAdapter.class);
    private static final int VIEW_TYPE_CONTACT;
    private static final int VIEW_TYPE_DIVIDER;
    private final int checkboxResource;
    private final ItemClickListener clickListener;
    private final Set<RecipientId> currentContacts;
    private final GlideRequests glideRequests;
    private final LayoutInflater layoutInflater;
    private final boolean multiSelect;
    private final SelectedContactSet selectedContacts = new SelectedContactSet();

    /* loaded from: classes4.dex */
    public interface ItemClickListener {
        void onItemClick(ContactSelectionListItem contactSelectionListItem);
    }

    private boolean isContactRow(int i) {
        return (i & 44) == 0;
    }

    protected /* bridge */ /* synthetic */ void onBindItemViewHolder(RecyclerView.ViewHolder viewHolder, Cursor cursor, List list) {
        onBindItemViewHolder((ViewHolder) viewHolder, cursor, (List<Object>) list);
    }

    public void clearSelectedContacts() {
        this.selectedContacts.clear();
    }

    public boolean isSelectedContact(SelectedContact selectedContact) {
        return this.selectedContacts.contains(selectedContact);
    }

    public void addSelectedContact(SelectedContact selectedContact) {
        if (!this.selectedContacts.add(selectedContact)) {
            Log.i(TAG, "Contact was already selected, possibly by another identifier");
        }
    }

    public void removeFromSelectedContacts(SelectedContact selectedContact) {
        Log.i(TAG, String.format(Locale.US, "Removed %d selected contacts that matched", Integer.valueOf(this.selectedContacts.remove(selectedContact))));
    }

    /* loaded from: classes4.dex */
    public static abstract class ViewHolder extends RecyclerView.ViewHolder {
        public void animateChecked(boolean z) {
        }

        public abstract void bind(GlideRequests glideRequests, RecipientId recipientId, int i, String str, String str2, String str3, String str4, boolean z);

        public abstract void setChecked(boolean z);

        public abstract void setEnabled(boolean z);

        public void setLetterHeaderCharacter(String str) {
        }

        public abstract void unbind(GlideRequests glideRequests);

        public ViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static class ContactViewHolder extends ViewHolder implements LetterHeaderDecoration.LetterHeaderItem {
        private String letterHeader;

        ContactViewHolder(View view, ItemClickListener itemClickListener) {
            super(view);
            view.setOnClickListener(new ContactSelectionListAdapter$ContactViewHolder$$ExternalSyntheticLambda0(this, itemClickListener));
        }

        public /* synthetic */ void lambda$new$0(ItemClickListener itemClickListener, View view) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(getView());
            }
        }

        public ContactSelectionListItem getView() {
            return (ContactSelectionListItem) this.itemView;
        }

        public void bind(GlideRequests glideRequests, RecipientId recipientId, int i, String str, String str2, String str3, String str4, boolean z) {
            getView().set(glideRequests, recipientId, i, str, str2, str3, str4, z);
        }

        public void unbind(GlideRequests glideRequests) {
            getView().unbind();
        }

        public void setChecked(boolean z) {
            getView().setChecked(z, false);
        }

        public void animateChecked(boolean z) {
            getView().setChecked(z, true);
        }

        public void setEnabled(boolean z) {
            getView().setEnabled(z);
        }

        public String getHeaderLetter() {
            return this.letterHeader;
        }

        public void setLetterHeaderCharacter(String str) {
            this.letterHeader = str;
        }
    }

    /* loaded from: classes4.dex */
    public static class DividerViewHolder extends ViewHolder {
        private final TextView label;

        public void setChecked(boolean z) {
        }

        public void setEnabled(boolean z) {
        }

        public void unbind(GlideRequests glideRequests) {
        }

        DividerViewHolder(View view) {
            super(view);
            this.label = (TextView) view.findViewById(R.id.label);
        }

        public void bind(GlideRequests glideRequests, RecipientId recipientId, int i, String str, String str2, String str3, String str4, boolean z) {
            this.label.setText(str);
        }
    }

    /* loaded from: classes4.dex */
    public static class HeaderViewHolder extends RecyclerView.ViewHolder {
        HeaderViewHolder(View view) {
            super(view);
        }
    }

    public ContactSelectionListAdapter(Context context, GlideRequests glideRequests, Cursor cursor, ItemClickListener itemClickListener, boolean z, Set<RecipientId> set, int i) {
        super(context, cursor);
        this.layoutInflater = LayoutInflater.from(context);
        this.glideRequests = glideRequests;
        this.multiSelect = z;
        this.clickListener = itemClickListener;
        this.currentContacts = set;
        this.checkboxResource = i;
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration.StickyHeaderAdapter
    public long getHeaderId(int i) {
        if (!isActiveCursor() || i == -1 || getContactType(i) == 32) {
            return -1;
        }
        return (long) Util.hashCode(getHeaderString(i), Integer.valueOf(getContactType(i)));
    }

    public ViewHolder onCreateItemViewHolder(ViewGroup viewGroup, int i) {
        if (i != 0) {
            return new DividerViewHolder(this.layoutInflater.inflate(R.layout.contact_selection_list_divider, viewGroup, false));
        }
        View inflate = this.layoutInflater.inflate(R.layout.contact_selection_list_item, viewGroup, false);
        inflate.findViewById(R.id.check_box).setBackgroundResource(this.checkboxResource);
        return new ContactViewHolder(inflate, this.clickListener);
    }

    public void onBindItemViewHolder(ViewHolder viewHolder, Cursor cursor) {
        String requireString = CursorUtil.requireString(cursor, ContactRepository.ID_COLUMN);
        RecipientId from = requireString != null ? RecipientId.from(requireString) : null;
        int requireInt = CursorUtil.requireInt(cursor, "contact_type");
        String requireString2 = CursorUtil.requireString(cursor, "name");
        String requireString3 = CursorUtil.requireString(cursor, "number");
        int requireInt2 = CursorUtil.requireInt(cursor, "number_type");
        String requireString4 = CursorUtil.requireString(cursor, RecipientDatabase.ABOUT);
        String charSequence = ContactsContract.CommonDataKinds.Phone.getTypeLabel(getContext().getResources(), requireInt2, CursorUtil.requireString(cursor, EmojiSearchDatabase.LABEL)).toString();
        boolean contains = this.currentContacts.contains(from);
        viewHolder.unbind(this.glideRequests);
        viewHolder.bind(this.glideRequests, from, requireInt, requireString2, requireString3, charSequence, requireString4, this.multiSelect || contains);
        viewHolder.setEnabled(true);
        if (contains) {
            viewHolder.setChecked(true);
            viewHolder.setEnabled(false);
        } else if (requireInt2 == 8) {
            viewHolder.setChecked(this.selectedContacts.contains(SelectedContact.forUsername(from, requireString3)));
        } else {
            viewHolder.setChecked(this.selectedContacts.contains(SelectedContact.forPhone(from, requireString3)));
        }
        if (!isContactRow(requireInt)) {
            return;
        }
        if (cursor.getPosition() == 0) {
            viewHolder.setLetterHeaderCharacter(getHeaderLetterForDisplayName(cursor));
            return;
        }
        cursor.moveToPrevious();
        if (!isContactRow(CursorUtil.requireInt(cursor, "contact_type"))) {
            cursor.moveToNext();
            viewHolder.setLetterHeaderCharacter(getHeaderLetterForDisplayName(cursor));
            return;
        }
        String headerLetterForDisplayName = getHeaderLetterForDisplayName(cursor);
        cursor.moveToNext();
        String headerLetterForDisplayName2 = getHeaderLetterForDisplayName(cursor);
        if (Objects.equals(headerLetterForDisplayName, headerLetterForDisplayName2)) {
            viewHolder.setLetterHeaderCharacter(null);
        } else {
            viewHolder.setLetterHeaderCharacter(headerLetterForDisplayName2);
        }
    }

    private String getHeaderLetterForDisplayName(Cursor cursor) {
        String requireString = CursorUtil.requireString(cursor, "name");
        Iterator<String> it = new CharacterIterable(requireString).iterator();
        if (TextUtils.isEmpty(requireString) || !it.hasNext()) {
            return null;
        }
        String next = it.next();
        return Character.isLetter(next.codePointAt(0)) ? next.toUpperCase() : "#";
    }

    protected void onBindItemViewHolder(ViewHolder viewHolder, Cursor cursor, List<Object> list) {
        if (arePayloadsValid(list)) {
            String requireString = CursorUtil.requireString(cursor, ContactRepository.ID_COLUMN);
            RecipientId from = requireString != null ? RecipientId.from(requireString) : null;
            int requireInt = CursorUtil.requireInt(cursor, "number_type");
            String requireString2 = CursorUtil.requireString(cursor, "number");
            viewHolder.setEnabled(true);
            if (this.currentContacts.contains(from)) {
                viewHolder.animateChecked(true);
                viewHolder.setEnabled(false);
            } else if (requireInt == 8) {
                viewHolder.animateChecked(this.selectedContacts.contains(SelectedContact.forUsername(from, requireString2)));
            } else {
                viewHolder.animateChecked(this.selectedContacts.contains(SelectedContact.forPhone(from, requireString2)));
            }
        } else {
            throw new AssertionError();
        }
    }

    @Override // org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter
    public int getItemViewType(Cursor cursor) {
        return CursorUtil.requireInt(cursor, "contact_type") == 32 ? 1 : 0;
    }

    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i, int i2) {
        return new HeaderViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.contact_selection_recyclerview_header, viewGroup, false));
    }

    public void onBindHeaderViewHolder(HeaderViewHolder headerViewHolder, int i, int i2) {
        ((TextView) headerViewHolder.itemView).setText(getSpannedHeaderString(i));
    }

    @Override // org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter
    protected boolean arePayloadsValid(List<Object> list) {
        return list.size() == 1 && list.get(0).equals(1);
    }

    public void onItemViewRecycled(ViewHolder viewHolder) {
        viewHolder.unbind(this.glideRequests);
    }

    @Override // org.thoughtcrime.securesms.components.RecyclerViewFastScroller.FastScrollAdapter
    public CharSequence getBubbleText(int i) {
        return getHeaderString(i);
    }

    public List<SelectedContact> getSelectedContacts() {
        return this.selectedContacts.getContacts();
    }

    public int getSelectedContactsCount() {
        return this.selectedContacts.size();
    }

    public int getCurrentContactsCount() {
        return this.currentContacts.size();
    }

    private CharSequence getSpannedHeaderString(int i) {
        String headerString = getHeaderString(i);
        if (!isPush(i)) {
            return headerString;
        }
        SpannableString spannableString = new SpannableString(headerString);
        spannableString.setSpan(new ForegroundColorSpan(getContext().getResources().getColor(R.color.core_ultramarine)), 0, headerString.length(), 33);
        return spannableString;
    }

    private String getHeaderString(int i) {
        int contactType = getContactType(i);
        if ((contactType & 16) > 0 || contactType == 32) {
            return " ";
        }
        String requireString = CursorUtil.requireString(getCursorAtPositionOrThrow(i), "name");
        if (requireString == null) {
            return "#";
        }
        String trim = requireString.trim();
        if (trim.length() <= 0) {
            return "#";
        }
        char charAt = trim.charAt(0);
        return Character.isLetterOrDigit(charAt) ? String.valueOf(Character.toUpperCase(charAt)) : "#";
    }

    private int getContactType(int i) {
        Cursor cursorAtPositionOrThrow = getCursorAtPositionOrThrow(i);
        return cursorAtPositionOrThrow.getInt(cursorAtPositionOrThrow.getColumnIndexOrThrow("contact_type"));
    }

    private boolean isPush(int i) {
        return getContactType(i) == 1;
    }
}
