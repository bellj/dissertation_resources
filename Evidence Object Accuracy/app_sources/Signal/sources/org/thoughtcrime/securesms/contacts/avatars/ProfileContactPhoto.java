package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.net.Uri;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Objects;
import org.thoughtcrime.securesms.database.model.ProfileAvatarFileDetails;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class ProfileContactPhoto implements ContactPhoto {
    private final String avatarObject;
    private final ProfileAvatarFileDetails profileAvatarFileDetails;
    private final Recipient recipient;

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public Uri getUri(Context context) {
        return null;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public boolean isProfilePhoto() {
        return true;
    }

    public ProfileContactPhoto(Recipient recipient) {
        this.recipient = recipient;
        this.avatarObject = recipient.getProfileAvatar() == null ? "" : recipient.getProfileAvatar();
        this.profileAvatarFileDetails = recipient.getProfileAvatarFileDetails();
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public InputStream openInputStream(Context context) throws IOException {
        return AvatarHelper.getAvatar(context, this.recipient.getId());
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto, com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(this.recipient.getId().serialize().getBytes());
        messageDigest.update(this.avatarObject.getBytes());
        messageDigest.update(this.profileAvatarFileDetails.getDiskCacheKeyBytes());
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ProfileContactPhoto profileContactPhoto = (ProfileContactPhoto) obj;
        if (!this.recipient.equals(profileContactPhoto.recipient) || !this.avatarObject.equals(profileContactPhoto.avatarObject) || !this.profileAvatarFileDetails.equals(profileContactPhoto.profileAvatarFileDetails)) {
            return false;
        }
        return true;
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return Objects.hash(this.recipient, this.avatarObject, this.profileAvatarFileDetails);
    }
}
