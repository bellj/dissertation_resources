package org.thoughtcrime.securesms.contacts.avatars;

import org.thoughtcrime.securesms.color.MaterialColor;

/* loaded from: classes4.dex */
public class ContactColorsLegacy {
    private static final String[] LEGACY_PALETTE = {"red", "pink", "purple", "deep_purple", "indigo", "blue", "light_blue", "cyan", "teal", "green", "light_green", "orange", "deep_orange", "amber", "blue_grey"};
    private static final String[] LEGACY_PALETTE_2 = {"pink", "red", "orange", "purple", "blue", "indigo", "green", "light_green", "teal", "brown", "blue_grey"};

    public static MaterialColor generateFor(String str) {
        String[] strArr = LEGACY_PALETTE;
        try {
            return MaterialColor.fromSerialized(strArr[Math.abs(str.hashCode()) % strArr.length]);
        } catch (MaterialColor.UnknownColorException unused) {
            return ContactColors.generateFor(str);
        }
    }

    public static MaterialColor generateForV2(String str) {
        String[] strArr = LEGACY_PALETTE_2;
        try {
            return MaterialColor.fromSerialized(strArr[Math.abs(str.hashCode()) % strArr.length]);
        } catch (MaterialColor.UnknownColorException unused) {
            return ContactColors.generateFor(str);
        }
    }
}
