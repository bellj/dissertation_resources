package org.thoughtcrime.securesms.contacts;

/* loaded from: classes4.dex */
public class NameAndNumber {
    public String name;
    public String number;

    public NameAndNumber(String str, String str2) {
        this.name = str;
        this.number = str2;
    }

    public NameAndNumber() {
    }
}
