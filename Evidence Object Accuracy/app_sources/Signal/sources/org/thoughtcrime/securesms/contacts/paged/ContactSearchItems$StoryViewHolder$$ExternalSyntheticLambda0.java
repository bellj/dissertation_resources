package org.thoughtcrime.securesms.contacts.paged;

import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ContactSearchItems.StoryViewHolder f$0;
    public final /* synthetic */ ContactSearchItems.StoryModel f$1;

    public /* synthetic */ ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda0(ContactSearchItems.StoryViewHolder storyViewHolder, ContactSearchItems.StoryModel storyModel) {
        this.f$0 = storyViewHolder;
        this.f$1 = storyModel;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ContactSearchItems.StoryViewHolder.m1322getMyStoryContextMenuActions$lambda1(this.f$0, this.f$1);
    }
}
