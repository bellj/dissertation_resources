package org.thoughtcrime.securesms.contacts.paged;

import android.view.View;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchItems$BaseRecipientViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContactSearchItems.BaseRecipientViewHolder f$0;
    public final /* synthetic */ MappingModel f$1;

    public /* synthetic */ ContactSearchItems$BaseRecipientViewHolder$$ExternalSyntheticLambda0(ContactSearchItems.BaseRecipientViewHolder baseRecipientViewHolder, MappingModel mappingModel) {
        this.f$0 = baseRecipientViewHolder;
        this.f$1 = mappingModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ContactSearchItems.BaseRecipientViewHolder.m1313bind$lambda0(this.f$0, this.f$1, view);
    }
}
