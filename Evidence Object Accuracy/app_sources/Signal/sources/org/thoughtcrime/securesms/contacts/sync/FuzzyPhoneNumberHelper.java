package org.thoughtcrime.securesms.contacts.sync;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.whispersystems.signalservice.api.push.ACI;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class FuzzyPhoneNumberHelper {
    private static final List<FuzzyMatcher> FUZZY_MATCHERS = Arrays.asList(new MexicoFuzzyMatcher(), new ArgentinaFuzzyMatcher());

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public interface FuzzyMatcher {
        String getVariant(String str);

        boolean isPreferredVariant(String str);

        boolean matches(String str);
    }

    FuzzyPhoneNumberHelper() {
    }

    public static InputResult generateInput(Collection<String> collection, Collection<String> collection2) {
        String variant;
        HashSet hashSet = new HashSet(collection);
        HashMap hashMap = new HashMap();
        for (String str : collection) {
            for (FuzzyMatcher fuzzyMatcher : FUZZY_MATCHERS) {
                if (fuzzyMatcher.matches(str) && (variant = fuzzyMatcher.getVariant(str)) != null && !collection2.contains(variant) && hashSet.add(variant)) {
                    hashMap.put(str, variant);
                }
            }
        }
        return new InputResult(hashSet, hashMap);
    }

    public static OutputResult generateOutput(Map<String, ACI> map, InputResult inputResult) {
        HashMap hashMap = new HashMap(map);
        HashMap hashMap2 = new HashMap();
        for (Map.Entry<String, String> entry : inputResult.getMapOfOriginalToVariant().entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (map.containsKey(key) && map.containsKey(value)) {
                for (FuzzyMatcher fuzzyMatcher : FUZZY_MATCHERS) {
                    if (fuzzyMatcher.matches(key)) {
                        if (fuzzyMatcher.isPreferredVariant(key)) {
                            hashMap.remove(value);
                        } else {
                            hashMap2.put(key, value);
                            hashMap.remove(key);
                        }
                    }
                }
            } else if (map.containsKey(value)) {
                hashMap2.put(key, value);
                hashMap.remove(key);
            }
        }
        return new OutputResult(hashMap, hashMap2);
    }

    /* loaded from: classes4.dex */
    static class MexicoFuzzyMatcher implements FuzzyMatcher {
        MexicoFuzzyMatcher() {
        }

        @Override // org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper.FuzzyMatcher
        public boolean matches(String str) {
            return str.startsWith("+52") && (str.length() == 13 || str.length() == 14);
        }

        @Override // org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper.FuzzyMatcher
        public String getVariant(String str) {
            if (str.startsWith("+521") && str.length() == 14) {
                return "+52" + str.substring(4);
            } else if (!str.startsWith("+52") || str.startsWith("+521") || str.length() != 13) {
                return null;
            } else {
                return "+521" + str.substring(3);
            }
        }

        @Override // org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper.FuzzyMatcher
        public boolean isPreferredVariant(String str) {
            return str.startsWith("+52") && !str.startsWith("+521") && str.length() == 13;
        }
    }

    /* loaded from: classes4.dex */
    static class ArgentinaFuzzyMatcher implements FuzzyMatcher {
        ArgentinaFuzzyMatcher() {
        }

        @Override // org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper.FuzzyMatcher
        public boolean matches(String str) {
            return str.startsWith("+54") && (str.length() == 13 || str.length() == 14);
        }

        @Override // org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper.FuzzyMatcher
        public String getVariant(String str) {
            if (str.startsWith("+549") && str.length() == 14) {
                return "+54" + str.substring(4);
            } else if (!str.startsWith("+54") || str.startsWith("+549") || str.length() != 13) {
                return null;
            } else {
                return "+549" + str.substring(3);
            }
        }

        @Override // org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper.FuzzyMatcher
        public boolean isPreferredVariant(String str) {
            return str.startsWith("+549") && str.length() == 14;
        }
    }

    /* loaded from: classes4.dex */
    public static class InputResult {
        private final Set<String> numbers;
        private final Map<String, String> originalToVariant;

        InputResult(Set<String> set, Map<String, String> map) {
            this.numbers = set;
            this.originalToVariant = map;
        }

        public Set<String> getNumbers() {
            return this.numbers;
        }

        public Map<String, String> getMapOfOriginalToVariant() {
            return this.originalToVariant;
        }
    }

    /* loaded from: classes4.dex */
    public static class OutputResult {
        private final Map<String, ACI> numbers;
        private final Map<String, String> rewrites;

        private OutputResult(Map<String, ACI> map, Map<String, String> map2) {
            this.numbers = map;
            this.rewrites = map2;
        }

        public Map<String, ACI> getNumbers() {
            return this.numbers;
        }

        public Map<String, String> getRewrites() {
            return this.rewrites;
        }
    }
}
