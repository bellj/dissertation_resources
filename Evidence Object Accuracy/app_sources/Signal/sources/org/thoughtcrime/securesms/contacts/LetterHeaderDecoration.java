package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: LetterHeaderDecoration.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J(\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\b\u0010\u001a\u001a\u00020\u000bH\u0002J\b\u0010\u001b\u001a\u00020\u000bH\u0002J \u0010\u001c\u001a\u00020\u00122\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/contacts/LetterHeaderDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "context", "Landroid/content/Context;", "hideDecoration", "Lkotlin/Function0;", "", "(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V", "bounds", "Landroid/graphics/Rect;", "dividerHeight", "", "padStart", "padTop", "textBounds", "textPaint", "Landroid/graphics/Paint;", "getItemOffsets", "", "outRect", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "getLayoutBoundsLTR", "getLayoutBoundsRTL", "onDrawOver", "canvas", "Landroid/graphics/Canvas;", "LetterHeaderItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LetterHeaderDecoration extends RecyclerView.ItemDecoration {
    private final Rect bounds = new Rect();
    private final Context context;
    private int dividerHeight;
    private final Function0<Boolean> hideDecoration;
    private final int padStart;
    private final int padTop = ViewUtil.dpToPx(16);
    private final Rect textBounds = new Rect();
    private final Paint textPaint;

    /* compiled from: LetterHeaderDecoration.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\n\u0010\u0002\u001a\u0004\u0018\u00010\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/LetterHeaderDecoration$LetterHeaderItem;", "", "getHeaderLetter", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface LetterHeaderItem {
        String getHeaderLetter();
    }

    public LetterHeaderDecoration(Context context, Function0<Boolean> function0) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(function0, "hideDecoration");
        this.context = context;
        this.hideDecoration = function0;
        this.padStart = context.getResources().getDimensionPixelSize(R.dimen.dsl_settings_gutter);
        this.dividerHeight = -1;
        Paint paint = new Paint();
        paint.setColor(ContextCompat.getColor(context, R.color.signal_text_primary));
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.FILL);
        paint.setTypeface(Typeface.create("sans-serif-medium", 0));
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize((float) ViewUtil.spToPx(16.0f));
        this.textPaint = paint;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(view);
        if (this.hideDecoration.invoke().booleanValue() || !(childViewHolder instanceof LetterHeaderItem) || ((LetterHeaderItem) childViewHolder).getHeaderLetter() == null) {
            rect.set(0, 0, 0, 0);
            return;
        }
        if (this.dividerHeight == -1) {
            View inflate = LayoutInflater.from(this.context).inflate(R.layout.dsl_section_header, (ViewGroup) recyclerView, false);
            inflate.measure(0, 0);
            this.dividerHeight = inflate.getMeasuredHeight();
        }
        rect.set(0, this.dividerHeight, 0, 0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        if (!this.hideDecoration.invoke().booleanValue()) {
            int childCount = recyclerView.getChildCount();
            boolean z = true;
            if (recyclerView.getLayoutDirection() != 1) {
                z = false;
            }
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                RecyclerView.ViewHolder childViewHolder = recyclerView.getChildViewHolder(childAt);
                String headerLetter = childViewHolder instanceof LetterHeaderItem ? ((LetterHeaderItem) childViewHolder).getHeaderLetter() : null;
                if (headerLetter != null) {
                    recyclerView.getDecoratedBoundsWithMargins(childAt, this.bounds);
                    this.textPaint.getTextBounds(headerLetter, 0, headerLetter.length(), this.textBounds);
                    int layoutBoundsRTL = z ? getLayoutBoundsRTL() : getLayoutBoundsLTR();
                    canvas.save();
                    canvas.drawText(headerLetter, (float) layoutBoundsRTL, (float) ((this.bounds.top + this.padTop) - this.textBounds.top), this.textPaint);
                    canvas.restore();
                }
            }
        }
    }

    private final int getLayoutBoundsLTR() {
        return this.bounds.left + this.padStart;
    }

    private final int getLayoutBoundsRTL() {
        return (this.bounds.right - this.padStart) - this.textBounds.width();
    }
}
