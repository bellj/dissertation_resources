package org.thoughtcrime.securesms.contacts;

import android.database.Cursor;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactRepository$1$$ExternalSyntheticLambda6 implements ContactRepository.ValueMapper {
    @Override // org.thoughtcrime.securesms.contacts.ContactRepository.ValueMapper
    public final Object get(Cursor cursor) {
        return ContactRepository.AnonymousClass1.lambda$new$6(cursor);
    }
}
