package org.thoughtcrime.securesms.contacts.paged;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.signal.paging.PagedDataSource;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.keyvalue.StorySend;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ContactSearchPagedDataSource.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 :2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0005:;<=>B\u0017\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ:\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00132\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0012\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00030\u0016H\u0002J$\u0010\u0017\u001a\u00020\u00182\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\n0\u001a2\u0006\u0010\u001b\u001a\u00020\nH\u0002J \u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0010\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J0\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0010\u001a\u00020!2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020\nH\u0002J\u0010\u0010$\u001a\u00020\u00022\u0006\u0010%\u001a\u00020\u0003H\u0016J\u001c\u0010&\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0010\u001a\u00020'2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J0\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0010\u001a\u00020'2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020\nH\u0002J0\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0010\u001a\u00020*2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020\nH\u0002J\u001c\u0010+\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0010\u001a\u00020*2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J0\u0010,\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020\nH\u0002J\u001a\u0010-\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J0\u0010.\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0010\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001f2\u0006\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020\nH\u0002J\u0014\u0010/\u001a\u0004\u0018\u00010\u00132\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0002J&\u00100\u001a\b\u0012\u0004\u0012\u00020\u0003012\u0006\u00102\u001a\u00020\n2\u0006\u00103\u001a\u00020\n2\u0006\u00104\u001a\u000205H\u0016J\u0014\u00100\u001a\u0004\u0018\u00010\u00032\b\u00106\u001a\u0004\u0018\u00010\u0002H\u0016JR\u00107\u001a\b\u0012\u0004\u0012\u00020\u00030\f2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020\n2\u0012\u00108\u001a\u000e\u0012\u0004\u0012\u00020\u0013\u0012\u0004\u0012\u00020\u00030\u00162\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\fH\u0002J\b\u00109\u001a\u00020\nH\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fX\u0004¢\u0006\u0002\n\u0000¨\u0006?"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource;", "Lorg/signal/paging/PagedDataSource;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "contactConfiguration", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "contactSearchPagedDataSourceRepository", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSourceRepository;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSourceRepository;)V", "activeStoryCount", "", "latestStorySends", "", "Lorg/thoughtcrime/securesms/keyvalue/StorySend;", "createResultsCollection", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$ResultsCollection;", "section", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "cursor", "Landroid/database/Cursor;", "extraData", "cursorMapper", "Lkotlin/Function1;", "findIndex", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$Index;", "sizeMap", "", "target", "getFilteredGroupStories", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Stories;", "query", "", "getGroupContactsData", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Groups;", "startIndex", "endIndex", "getKey", "data", "getNonGroupContactsCursor", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Individuals;", "getNonGroupContactsData", "getRecentsContactData", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents;", "getRecentsCursor", "getSectionData", "getSectionSize", "getStoriesContactData", "getStoriesCursor", "load", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "length", "cancellationSignal", "Lorg/signal/paging/PagedDataSource$CancellationSignal;", "key", "readContactDataFromCursor", "cursorRowToData", MediaPreviewActivity.SIZE_EXTRA, "Companion", "Index", "ResultsCollection", "StoriesCollection", "StoryComparator", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchPagedDataSource implements PagedDataSource<ContactSearchKey, ContactSearchData> {
    private static final long ACTIVE_STORY_CUTOFF_DURATION = TimeUnit.DAYS.toMillis(1);
    public static final Companion Companion = new Companion(null);
    private final int activeStoryCount;
    private final ContactSearchConfiguration contactConfiguration;
    private final ContactSearchPagedDataSourceRepository contactSearchPagedDataSourceRepository;
    private final List<StorySend> latestStorySends;

    /* compiled from: ContactSearchPagedDataSource.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[ContactSearchConfiguration.TransportType.values().length];
            iArr[ContactSearchConfiguration.TransportType.PUSH.ordinal()] = 1;
            iArr[ContactSearchConfiguration.TransportType.SMS.ordinal()] = 2;
            iArr[ContactSearchConfiguration.TransportType.ALL.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public ContactSearchPagedDataSource(ContactSearchConfiguration contactSearchConfiguration, ContactSearchPagedDataSourceRepository contactSearchPagedDataSourceRepository) {
        Intrinsics.checkNotNullParameter(contactSearchConfiguration, "contactConfiguration");
        Intrinsics.checkNotNullParameter(contactSearchPagedDataSourceRepository, "contactSearchPagedDataSourceRepository");
        this.contactConfiguration = contactSearchConfiguration;
        this.contactSearchPagedDataSourceRepository = contactSearchPagedDataSourceRepository;
        List<StorySend> latestStorySends = contactSearchPagedDataSourceRepository.getLatestStorySends(ACTIVE_STORY_CUTOFF_DURATION);
        this.latestStorySends = latestStorySends;
        this.activeStoryCount = latestStorySends.size();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ContactSearchPagedDataSource(org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration r1, org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSourceRepository r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
        /*
            r0 = this;
            r3 = r3 & 2
            if (r3 == 0) goto L_0x0012
            org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSourceRepository r2 = new org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSourceRepository
            android.app.Application r3 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getApplication()
            java.lang.String r4 = "getApplication()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r4)
            r2.<init>(r3)
        L_0x0012:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource.<init>(org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration, org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSourceRepository, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    /* compiled from: ContactSearchPagedDataSource.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$Companion;", "", "()V", "ACTIVE_STORY_CUTOFF_DURATION", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.signal.paging.PagedDataSource
    public int size() {
        int i = 0;
        for (ContactSearchConfiguration.Section section : this.contactConfiguration.getSections()) {
            i += getSectionSize(section, this.contactConfiguration.getQuery());
        }
        return i;
    }

    @Override // org.signal.paging.PagedDataSource
    public List<ContactSearchData> load(int i, int i2, PagedDataSource.CancellationSignal cancellationSignal) {
        List<ContactSearchData> list;
        int i3;
        Intrinsics.checkNotNullParameter(cancellationSignal, "cancellationSignal");
        List<ContactSearchConfiguration.Section> sections = this.contactConfiguration.getSections();
        Map<ContactSearchConfiguration.Section, Integer> linkedHashMap = new LinkedHashMap<>(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(sections, 10)), 16));
        for (Object obj : sections) {
            linkedHashMap.put(obj, Integer.valueOf(getSectionSize((ContactSearchConfiguration.Section) obj, this.contactConfiguration.getQuery())));
        }
        Index findIndex = findIndex(linkedHashMap, i);
        Index findIndex2 = findIndex(linkedHashMap, i + i2);
        int indexOf = this.contactConfiguration.getSections().indexOf(findIndex.getCategory());
        int indexOf2 = this.contactConfiguration.getSections().indexOf(findIndex2.getCategory());
        List<ContactSearchConfiguration.Section> sections2 = this.contactConfiguration.getSections();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(sections2, 10));
        int i4 = 0;
        for (Object obj2 : sections2) {
            int i5 = i4 + 1;
            if (i4 < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            ContactSearchConfiguration.Section section = (ContactSearchConfiguration.Section) obj2;
            if (indexOf <= i4 && i4 <= indexOf2) {
                String query = this.contactConfiguration.getQuery();
                int offset = i4 == indexOf ? findIndex.getOffset() : 0;
                if (i4 == indexOf2) {
                    i3 = findIndex2.getOffset();
                } else {
                    Integer num = linkedHashMap.get(section);
                    if (num != null) {
                        i3 = num.intValue();
                    } else {
                        throw new IllegalStateException("Unknown section".toString());
                    }
                }
                list = getSectionData(section, query, offset, i3);
            } else {
                list = CollectionsKt__CollectionsKt.emptyList();
            }
            arrayList.add(list);
            i4 = i5;
        }
        return CollectionsKt___CollectionsKt.toMutableList((Collection) CollectionsKt__IterablesKt.flatten(arrayList));
    }

    /* compiled from: ContactSearchPagedDataSource.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$Index;", "", "category", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "offset", "", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;I)V", "getCategory", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "getOffset", "()I", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Index {
        private final ContactSearchConfiguration.Section category;
        private final int offset;

        public static /* synthetic */ Index copy$default(Index index, ContactSearchConfiguration.Section section, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                section = index.category;
            }
            if ((i2 & 2) != 0) {
                i = index.offset;
            }
            return index.copy(section, i);
        }

        public final ContactSearchConfiguration.Section component1() {
            return this.category;
        }

        public final int component2() {
            return this.offset;
        }

        public final Index copy(ContactSearchConfiguration.Section section, int i) {
            Intrinsics.checkNotNullParameter(section, "category");
            return new Index(section, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Index)) {
                return false;
            }
            Index index = (Index) obj;
            return Intrinsics.areEqual(this.category, index.category) && this.offset == index.offset;
        }

        public int hashCode() {
            return (this.category.hashCode() * 31) + this.offset;
        }

        public String toString() {
            return "Index(category=" + this.category + ", offset=" + this.offset + ')';
        }

        public Index(ContactSearchConfiguration.Section section, int i) {
            Intrinsics.checkNotNullParameter(section, "category");
            this.category = section;
            this.offset = i;
        }

        public final ContactSearchConfiguration.Section getCategory() {
            return this.category;
        }

        public final int getOffset() {
            return this.offset;
        }
    }

    public ContactSearchData load(ContactSearchKey contactSearchKey) {
        throw new UnsupportedOperationException();
    }

    public ContactSearchKey getKey(ContactSearchData contactSearchData) {
        Intrinsics.checkNotNullParameter(contactSearchData, "data");
        return contactSearchData.getContactSearchKey();
    }

    private final int getSectionSize(ContactSearchConfiguration.Section section, String str) {
        Cursor cursor;
        List<ContactSearchData> list;
        if (section instanceof ContactSearchConfiguration.Section.Individuals) {
            cursor = getNonGroupContactsCursor((ContactSearchConfiguration.Section.Individuals) section, str);
        } else if (section instanceof ContactSearchConfiguration.Section.Groups) {
            cursor = this.contactSearchPagedDataSourceRepository.getGroupContacts((ContactSearchConfiguration.Section.Groups) section, str);
        } else if (section instanceof ContactSearchConfiguration.Section.Recents) {
            cursor = getRecentsCursor((ContactSearchConfiguration.Section.Recents) section, str);
        } else if (section instanceof ContactSearchConfiguration.Section.Stories) {
            cursor = getStoriesCursor(str);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        Intrinsics.checkNotNull(cursor);
        th = null;
        try {
            if (section instanceof ContactSearchConfiguration.Section.Stories) {
                list = getFilteredGroupStories((ContactSearchConfiguration.Section.Stories) section, str);
            } else {
                list = CollectionsKt__CollectionsKt.emptyList();
            }
            return createResultsCollection(section, cursor, list, ContactSearchPagedDataSource$getSectionSize$1$collection$1.INSTANCE).getSize();
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    private final List<ContactSearchData> getFilteredGroupStories(ContactSearchConfiguration.Section.Stories stories, String str) {
        Set set = SetsKt___SetsKt.plus((Set) this.contactSearchPagedDataSourceRepository.getGroupStories(), (Iterable) stories.getGroupStories());
        ArrayList arrayList = new ArrayList();
        for (Object obj : set) {
            if (this.contactSearchPagedDataSourceRepository.recipientNameContainsQuery(((ContactSearchData.Story) obj).getRecipient(), str)) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    private final List<ContactSearchData> getSectionData(ContactSearchConfiguration.Section section, String str, int i, int i2) {
        if (section instanceof ContactSearchConfiguration.Section.Groups) {
            return getGroupContactsData((ContactSearchConfiguration.Section.Groups) section, str, i, i2);
        }
        if (section instanceof ContactSearchConfiguration.Section.Individuals) {
            return getNonGroupContactsData((ContactSearchConfiguration.Section.Individuals) section, str, i, i2);
        }
        if (section instanceof ContactSearchConfiguration.Section.Recents) {
            return getRecentsContactData((ContactSearchConfiguration.Section.Recents) section, str, i, i2);
        }
        if (section instanceof ContactSearchConfiguration.Section.Stories) {
            return getStoriesContactData((ContactSearchConfiguration.Section.Stories) section, str, i, i2);
        }
        throw new NoWhenBranchMatchedException();
    }

    private final Cursor getNonGroupContactsCursor(ContactSearchConfiguration.Section.Individuals individuals, String str) {
        int i = WhenMappings.$EnumSwitchMapping$0[individuals.getTransportType().ordinal()];
        if (i == 1) {
            return this.contactSearchPagedDataSourceRepository.querySignalContacts(str, individuals.getIncludeSelf());
        }
        if (i == 2) {
            return this.contactSearchPagedDataSourceRepository.queryNonSignalContacts(str);
        }
        if (i == 3) {
            return this.contactSearchPagedDataSourceRepository.queryNonGroupContacts(str, individuals.getIncludeSelf());
        }
        throw new NoWhenBranchMatchedException();
    }

    private final Cursor getStoriesCursor(String str) {
        return this.contactSearchPagedDataSourceRepository.getStories(str);
    }

    private final Cursor getRecentsCursor(ContactSearchConfiguration.Section.Recents recents, String str) {
        if (str == null || str.length() == 0) {
            return this.contactSearchPagedDataSourceRepository.getRecents(recents);
        }
        throw new IllegalArgumentException("Searching Recents is not supported");
    }

    /* JADX DEBUG: Multi-variable search result rejected for r7v0, resolved type: org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource */
    /* JADX WARN: Multi-variable type inference failed */
    static /* synthetic */ List readContactDataFromCursor$default(ContactSearchPagedDataSource contactSearchPagedDataSource, Cursor cursor, ContactSearchConfiguration.Section section, int i, int i2, Function1 function1, List list, int i3, Object obj) {
        if ((i3 & 32) != 0) {
            list = CollectionsKt__CollectionsKt.emptyList();
        }
        return contactSearchPagedDataSource.readContactDataFromCursor(cursor, section, i, i2, function1, list);
    }

    private final List<ContactSearchData> readContactDataFromCursor(Cursor cursor, ContactSearchConfiguration.Section section, int i, int i2, Function1<? super Cursor, ? extends ContactSearchData> function1, List<? extends ContactSearchData> list) {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(createResultsCollection(section, cursor, list, function1).getSublist(i, i2));
        return arrayList;
    }

    private final List<ContactSearchData> getStoriesContactData(ContactSearchConfiguration.Section.Stories stories, String str, int i, int i2) {
        Cursor storiesCursor = getStoriesCursor(str);
        if (storiesCursor != null) {
            th = null;
            try {
                List<ContactSearchData> readContactDataFromCursor = readContactDataFromCursor(storiesCursor, stories, i, i2, new Function1<Cursor, ContactSearchData>(this) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource$getStoriesContactData$1$1
                    final /* synthetic */ ContactSearchPagedDataSource this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                    }

                    public final ContactSearchData invoke(Cursor cursor) {
                        Intrinsics.checkNotNullParameter(cursor, "it");
                        Recipient recipientFromDistributionListCursor = ContactSearchPagedDataSource.access$getContactSearchPagedDataSourceRepository$p(this.this$0).getRecipientFromDistributionListCursor(cursor);
                        return new ContactSearchData.Story(recipientFromDistributionListCursor, ContactSearchPagedDataSource.access$getContactSearchPagedDataSourceRepository$p(this.this$0).getDistributionListMembershipCount(recipientFromDistributionListCursor));
                    }
                }, getFilteredGroupStories(stories, str));
                if (readContactDataFromCursor != null) {
                    return readContactDataFromCursor;
                }
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    CloseableKt.closeFinally(storiesCursor, th);
                }
            }
        }
        return CollectionsKt__CollectionsKt.emptyList();
    }

    private final List<ContactSearchData> getRecentsContactData(ContactSearchConfiguration.Section.Recents recents, String str, int i, int i2) {
        Cursor recentsCursor = getRecentsCursor(recents, str);
        if (recentsCursor != null) {
            th = null;
            try {
                List<ContactSearchData> readContactDataFromCursor$default = readContactDataFromCursor$default(this, recentsCursor, recents, i, i2, new Function1<Cursor, ContactSearchData>(this, recentsCursor) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource$getRecentsContactData$1$1
                    final /* synthetic */ Cursor $cursor;
                    final /* synthetic */ ContactSearchPagedDataSource this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                        this.$cursor = r2;
                    }

                    public final ContactSearchData invoke(Cursor cursor) {
                        Intrinsics.checkNotNullParameter(cursor, "it");
                        return new ContactSearchData.KnownRecipient(ContactSearchPagedDataSource.access$getContactSearchPagedDataSourceRepository$p(this.this$0).getRecipientFromThreadCursor(this.$cursor));
                    }
                }, null, 32, null);
                if (readContactDataFromCursor$default != null) {
                    return readContactDataFromCursor$default;
                }
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    CloseableKt.closeFinally(recentsCursor, th);
                }
            }
        }
        return CollectionsKt__CollectionsKt.emptyList();
    }

    private final List<ContactSearchData> getNonGroupContactsData(ContactSearchConfiguration.Section.Individuals individuals, String str, int i, int i2) {
        Cursor nonGroupContactsCursor = getNonGroupContactsCursor(individuals, str);
        if (nonGroupContactsCursor != null) {
            th = null;
            try {
                List<ContactSearchData> readContactDataFromCursor$default = readContactDataFromCursor$default(this, nonGroupContactsCursor, individuals, i, i2, new Function1<Cursor, ContactSearchData>(this, nonGroupContactsCursor) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource$getNonGroupContactsData$1$1
                    final /* synthetic */ Cursor $cursor;
                    final /* synthetic */ ContactSearchPagedDataSource this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.this$0 = r1;
                        this.$cursor = r2;
                    }

                    public final ContactSearchData invoke(Cursor cursor) {
                        Intrinsics.checkNotNullParameter(cursor, "it");
                        return new ContactSearchData.KnownRecipient(ContactSearchPagedDataSource.access$getContactSearchPagedDataSourceRepository$p(this.this$0).getRecipientFromRecipientCursor(this.$cursor));
                    }
                }, null, 32, null);
                if (readContactDataFromCursor$default != null) {
                    return readContactDataFromCursor$default;
                }
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    CloseableKt.closeFinally(nonGroupContactsCursor, th);
                }
            }
        }
        return CollectionsKt__CollectionsKt.emptyList();
    }

    private final List<ContactSearchData> getGroupContactsData(ContactSearchConfiguration.Section.Groups groups, String str, int i, int i2) {
        Cursor groupContacts = this.contactSearchPagedDataSourceRepository.getGroupContacts(groups, str);
        if (groupContacts != null) {
            th = null;
            try {
                List<ContactSearchData> readContactDataFromCursor$default = readContactDataFromCursor$default(this, groupContacts, groups, i, i2, new Function1<Cursor, ContactSearchData>(groups, this, groupContacts) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource$getGroupContactsData$1$1
                    final /* synthetic */ Cursor $cursor;
                    final /* synthetic */ ContactSearchConfiguration.Section.Groups $section;
                    final /* synthetic */ ContactSearchPagedDataSource this$0;

                    /* access modifiers changed from: package-private */
                    {
                        this.$section = r1;
                        this.this$0 = r2;
                        this.$cursor = r3;
                    }

                    public final ContactSearchData invoke(Cursor cursor) {
                        Intrinsics.checkNotNullParameter(cursor, "it");
                        if (this.$section.getReturnAsGroupStories()) {
                            return new ContactSearchData.Story(ContactSearchPagedDataSource.access$getContactSearchPagedDataSourceRepository$p(this.this$0).getRecipientFromGroupCursor(this.$cursor), 0);
                        }
                        return new ContactSearchData.KnownRecipient(ContactSearchPagedDataSource.access$getContactSearchPagedDataSourceRepository$p(this.this$0).getRecipientFromGroupCursor(this.$cursor));
                    }
                }, null, 32, null);
                if (readContactDataFromCursor$default != null) {
                    return readContactDataFromCursor$default;
                }
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                    CloseableKt.closeFinally(groupContacts, th);
                }
            }
        }
        return CollectionsKt__CollectionsKt.emptyList();
    }

    private final Index findIndex(Map<ContactSearchConfiguration.Section, Integer> map, int i) {
        int i2 = 0;
        for (Map.Entry<ContactSearchConfiguration.Section, Integer> entry : map.entrySet()) {
            ContactSearchConfiguration.Section key = entry.getKey();
            int intValue = entry.getValue().intValue() + i2;
            if (intValue > i) {
                return new Index(key, i - i2);
            }
            i2 = intValue;
        }
        return new Index((ContactSearchConfiguration.Section) CollectionsKt___CollectionsKt.last(map.keySet()), ((Number) CollectionsKt___CollectionsKt.last(map.values())).intValue());
    }

    private final ResultsCollection createResultsCollection(ContactSearchConfiguration.Section section, Cursor cursor, List<? extends ContactSearchData> list, Function1<? super Cursor, ? extends ContactSearchData> function1) {
        if (section instanceof ContactSearchConfiguration.Section.Stories) {
            return new StoriesCollection(section, cursor, list, function1, this.activeStoryCount, new StoryComparator(this.latestStorySends));
        }
        return new ResultsCollection(section, cursor, list, function1, 0);
    }

    /* compiled from: ContactSearchPagedDataSource.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0000\b\u0012\u0018\u00002\u00020\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b0\n\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\u0010\u0010\u0019\u001a\u00020\b2\u0006\u0010\u001a\u001a\u00020\fH\u0002J\u0010\u0010\u001b\u001a\u00020\b2\u0006\u0010\u001c\u001a\u00020\fH\u0014J\u0006\u0010\u001d\u001a\u00020\fJ\u001c\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\u001f\u001a\u00020\f2\u0006\u0010 \u001a\u00020\fJ\b\u0010!\u001a\u00020\"H\u0002R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$ResultsCollection;", "", "section", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "cursor", "Landroid/database/Cursor;", "extraData", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "cursorMapper", "Lkotlin/Function1;", "activeContactCount", "", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;Landroid/database/Cursor;Ljava/util/List;Lkotlin/jvm/functions/Function1;I)V", "getActiveContactCount", "()I", "contentSize", "getCursor", "()Landroid/database/Cursor;", "getCursorMapper", "()Lkotlin/jvm/functions/Function1;", "getExtraData", "()Ljava/util/List;", "getSection", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "getItemAt", "index", "getItemAtCorrectedIndex", "correctedIndex", "getSize", "getSublist", NotificationProfileDatabase.NotificationProfileScheduleTable.START, NotificationProfileDatabase.NotificationProfileScheduleTable.END, "shouldDisplayExpandRow", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static class ResultsCollection {
        private final int activeContactCount;
        private final int contentSize;
        private final Cursor cursor;
        private final Function1<Cursor, ContactSearchData> cursorMapper;
        private final List<ContactSearchData> extraData;
        private final ContactSearchConfiguration.Section section;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData> */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super android.database.Cursor, ? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData> */
        /* JADX WARN: Multi-variable type inference failed */
        public ResultsCollection(ContactSearchConfiguration.Section section, Cursor cursor, List<? extends ContactSearchData> list, Function1<? super Cursor, ? extends ContactSearchData> function1, int i) {
            Intrinsics.checkNotNullParameter(section, "section");
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            Intrinsics.checkNotNullParameter(list, "extraData");
            Intrinsics.checkNotNullParameter(function1, "cursorMapper");
            this.section = section;
            this.cursor = cursor;
            this.extraData = list;
            this.cursorMapper = function1;
            this.activeContactCount = i;
            this.contentSize = cursor.getCount() + list.size();
        }

        public final ContactSearchConfiguration.Section getSection() {
            return this.section;
        }

        public final Cursor getCursor() {
            return this.cursor;
        }

        public final List<ContactSearchData> getExtraData() {
            return this.extraData;
        }

        public final Function1<Cursor, ContactSearchData> getCursorMapper() {
            return this.cursorMapper;
        }

        public final int getActiveContactCount() {
            return this.activeContactCount;
        }

        public final int getSize() {
            ContactSearchConfiguration.ExpandConfig expandConfig = this.section.getExpandConfig();
            int i = 1;
            int i2 = Integer.MAX_VALUE;
            if (expandConfig != null && !expandConfig.isExpanded()) {
                i2 = expandConfig.getMaxCountWhenNotExpanded().invoke(Integer.valueOf(this.activeContactCount)).intValue() + 1;
            }
            int min = Math.min(i2, this.contentSize);
            if (min <= 0 || !this.section.getIncludeHeader()) {
                i = 0;
            }
            return min + i;
        }

        public final List<ContactSearchData> getSublist(int i, int i2) {
            ArrayList arrayList = new ArrayList();
            while (i < i2) {
                arrayList.add(getItemAt(i));
                i++;
            }
            return arrayList;
        }

        private final ContactSearchData getItemAt(int i) {
            if (i == 0 && this.section.getIncludeHeader()) {
                return new ContactSearchData.Header(this.section.getSectionKey(), this.section.getHeaderAction());
            }
            if (i == getSize() - 1 && shouldDisplayExpandRow()) {
                return new ContactSearchData.Expand(this.section.getSectionKey());
            }
            if (this.section.getIncludeHeader()) {
                i--;
            }
            return getItemAtCorrectedIndex(i);
        }

        protected ContactSearchData getItemAtCorrectedIndex(int i) {
            if (i < this.cursor.getCount()) {
                this.cursor.moveToPosition(i);
                return this.cursorMapper.invoke(this.cursor);
            }
            return this.extraData.get(i - this.cursor.getCount());
        }

        private final boolean shouldDisplayExpandRow() {
            ContactSearchConfiguration.ExpandConfig expandConfig = this.section.getExpandConfig();
            if (expandConfig == null || expandConfig.isExpanded() || this.contentSize <= expandConfig.getMaxCountWhenNotExpanded().invoke(Integer.valueOf(this.activeContactCount)).intValue() + 1) {
                return false;
            }
            return true;
        }
    }

    /* compiled from: ContactSearchPagedDataSource.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0002\u0018\u00002\u00020\u0001BG\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\b0\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u0018\u001a\u00020\b2\u0006\u0010\u0019\u001a\u00020\fH\u0014R!\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u00078BX\u0002¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$StoriesCollection;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$ResultsCollection;", "section", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "cursor", "Landroid/database/Cursor;", "extraData", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "cursorMapper", "Lkotlin/Function1;", "activeContactCount", "", "storyComparator", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$StoryComparator;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;Landroid/database/Cursor;Ljava/util/List;Lkotlin/jvm/functions/Function1;ILorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$StoryComparator;)V", "aggregateStoryData", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "getAggregateStoryData", "()Ljava/util/List;", "aggregateStoryData$delegate", "Lkotlin/Lazy;", "getStoryComparator", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$StoryComparator;", "getItemAtCorrectedIndex", "correctedIndex", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StoriesCollection extends ResultsCollection {
        private final Lazy aggregateStoryData$delegate;
        private final StoryComparator storyComparator;

        public final StoryComparator getStoryComparator() {
            return this.storyComparator;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StoriesCollection(ContactSearchConfiguration.Section section, Cursor cursor, List<? extends ContactSearchData> list, Function1<? super Cursor, ? extends ContactSearchData> function1, int i, StoryComparator storyComparator) {
            super(section, cursor, list, function1, i);
            Intrinsics.checkNotNullParameter(section, "section");
            Intrinsics.checkNotNullParameter(cursor, "cursor");
            Intrinsics.checkNotNullParameter(list, "extraData");
            Intrinsics.checkNotNullParameter(function1, "cursorMapper");
            Intrinsics.checkNotNullParameter(storyComparator, "storyComparator");
            this.storyComparator = storyComparator;
            this.aggregateStoryData$delegate = LazyKt__LazyJVMKt.lazy(new ContactSearchPagedDataSource$StoriesCollection$aggregateStoryData$2(section, cursor, list, this, function1));
        }

        private final List<ContactSearchData.Story> getAggregateStoryData() {
            return (List) this.aggregateStoryData$delegate.getValue();
        }

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource.ResultsCollection
        protected ContactSearchData getItemAtCorrectedIndex(int i) {
            return getAggregateStoryData().get(i);
        }
    }

    /* compiled from: ContactSearchPagedDataSource.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0002\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003B\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchPagedDataSource$StoryComparator;", "Ljava/util/Comparator;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "Lkotlin/Comparator;", "latestStorySends", "", "Lorg/thoughtcrime/securesms/keyvalue/StorySend;", "(Ljava/util/List;)V", "compare", "", "lhs", "rhs", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StoryComparator implements Comparator<ContactSearchData.Story> {
        private final List<StorySend> latestStorySends;

        public StoryComparator(List<StorySend> list) {
            Intrinsics.checkNotNullParameter(list, "latestStorySends");
            this.latestStorySends = list;
        }

        public int compare(ContactSearchData.Story story, ContactSearchData.Story story2) {
            Intrinsics.checkNotNullParameter(story, "lhs");
            Intrinsics.checkNotNullParameter(story2, "rhs");
            Iterator<StorySend> it = this.latestStorySends.iterator();
            int i = 0;
            while (true) {
                if (!it.hasNext()) {
                    i = -1;
                    break;
                } else if (it.next().getIdentifier().matches(story.getRecipient())) {
                    break;
                } else {
                    i++;
                }
            }
            int i2 = Integer.MAX_VALUE;
            if (i == -1) {
                i = Integer.MAX_VALUE;
            }
            Iterator<StorySend> it2 = this.latestStorySends.iterator();
            int i3 = 0;
            while (true) {
                if (!it2.hasNext()) {
                    i3 = -1;
                    break;
                } else if (it2.next().getIdentifier().matches(story2.getRecipient())) {
                    break;
                } else {
                    i3++;
                }
            }
            if (i3 != -1) {
                i2 = i3;
            }
            if (story.getRecipient().isMyStory() && story2.getRecipient().isMyStory()) {
                return 0;
            }
            if (!story.getRecipient().isMyStory()) {
                if (!story2.getRecipient().isMyStory()) {
                    if (i >= i2) {
                        if (i <= i2) {
                            if (i != i2) {
                                return 0;
                            }
                        }
                    }
                }
                return 1;
            }
            return -1;
        }
    }
}
