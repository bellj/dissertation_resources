package org.thoughtcrime.securesms.contacts.paged;

import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.contacts.HeaderAction;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: ContactSearchItems.kt */
@Metadata(d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\f\bÆ\u0002\u0018\u00002\u00020\u0001:\u000b\u001e\u001f !\"#$%&'(B\u0007\b\u0002¢\u0006\u0002\u0010\u0002Jz\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\"\u0010\t\u001a\u001e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\nj\u0002`\r2\"\u0010\u000e\u001a\u001e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00040\nj\u0002`\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0012\u0010\u0013\u001a\u000e\u0012\u0004\u0012\u00020\u0015\u0012\u0004\u0012\u00020\u00040\u0014J$\u0010\u0016\u001a\u00020\u00172\u000e\u0010\u0018\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u001a0\u00192\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001d0\u001c¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "displayCheckBox", "", "recipientListener", "Lkotlin/Function3;", "Landroid/view/View;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;", "Lorg/thoughtcrime/securesms/contacts/paged/RecipientClickListener;", "storyListener", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "Lorg/thoughtcrime/securesms/contacts/paged/StoryClickListener;", "storyContextMenuCallbacks", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryContextMenuCallbacks;", "expandListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;", "toMappingModelList", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "contactSearchData", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "selection", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "BaseRecipientViewHolder", "ExpandModel", "ExpandViewHolder", "HeaderModel", "HeaderViewHolder", "IsSelfComparator", "KnownRecipientViewHolder", "RecipientModel", "StoryContextMenuCallbacks", "StoryModel", "StoryViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchItems {
    public static final ContactSearchItems INSTANCE = new ContactSearchItems();

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\t\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryContextMenuCallbacks;", "", "onDeletePrivateStory", "", "story", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "isSelected", "", "onOpenStorySettings", "onRemoveGroupStory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface StoryContextMenuCallbacks {
        void onDeletePrivateStory(ContactSearchData.Story story, boolean z);

        void onOpenStorySettings(ContactSearchData.Story story);

        void onRemoveGroupStory(ContactSearchData.Story story, boolean z);
    }

    private ContactSearchItems() {
    }

    public final void register(MappingAdapter mappingAdapter, boolean z, Function3<? super View, ? super ContactSearchData.KnownRecipient, ? super Boolean, Unit> function3, Function3<? super View, ? super ContactSearchData.Story, ? super Boolean, Unit> function32, StoryContextMenuCallbacks storyContextMenuCallbacks, Function1<? super ContactSearchData.Expand, Unit> function1) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        Intrinsics.checkNotNullParameter(function3, "recipientListener");
        Intrinsics.checkNotNullParameter(function32, "storyListener");
        Intrinsics.checkNotNullParameter(storyContextMenuCallbacks, "storyContextMenuCallbacks");
        Intrinsics.checkNotNullParameter(function1, "expandListener");
        mappingAdapter.registerFactory(StoryModel.class, new LayoutFactory(new Function(z, function32, storyContextMenuCallbacks) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchItems$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;
            public final /* synthetic */ Function3 f$1;
            public final /* synthetic */ ContactSearchItems.StoryContextMenuCallbacks f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContactSearchItems.m1305$r8$lambda$LbqRTOINvy2qBaypbUX21iCUhg(this.f$0, this.f$1, this.f$2, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.contact_search_item));
        mappingAdapter.registerFactory(RecipientModel.class, new LayoutFactory(new Function(z, function3) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchItems$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;
            public final /* synthetic */ Function3 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContactSearchItems.m1306$r8$lambda$Sxi0LfNTv3wBAUOtivdSa8Sxz4(this.f$0, this.f$1, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.contact_search_item));
        mappingAdapter.registerFactory(HeaderModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchItems$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContactSearchItems.m1307$r8$lambda$xTjD36zE17kDvWZTuC1jKN3Mq0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.contact_search_section_header));
        mappingAdapter.registerFactory(ExpandModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchItems$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContactSearchItems.$r8$lambda$bDpAtSof0pq_uVnZ4tFDqs3jzrw(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.contacts_expand_item));
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m1308register$lambda0(boolean z, Function3 function3, StoryContextMenuCallbacks storyContextMenuCallbacks, View view) {
        Intrinsics.checkNotNullParameter(function3, "$storyListener");
        Intrinsics.checkNotNullParameter(storyContextMenuCallbacks, "$storyContextMenuCallbacks");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new StoryViewHolder(view, z, function3, storyContextMenuCallbacks);
    }

    /* renamed from: register$lambda-1 */
    public static final MappingViewHolder m1309register$lambda1(boolean z, Function3 function3, View view) {
        Intrinsics.checkNotNullParameter(function3, "$recipientListener");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new KnownRecipientViewHolder(view, z, function3);
    }

    /* renamed from: register$lambda-2 */
    public static final MappingViewHolder m1310register$lambda2(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new HeaderViewHolder(view);
    }

    /* renamed from: register$lambda-3 */
    public static final MappingViewHolder m1311register$lambda3(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$expandListener");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ExpandViewHolder(view, function1);
    }

    public final MappingModelList toMappingModelList(List<? extends ContactSearchData> list, Set<? extends ContactSearchKey> set) {
        Object obj;
        Intrinsics.checkNotNullParameter(list, "contactSearchData");
        Intrinsics.checkNotNullParameter(set, "selection");
        List<ContactSearchData> list2 = CollectionsKt___CollectionsKt.filterNotNull(list);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10));
        for (ContactSearchData contactSearchData : list2) {
            if (contactSearchData instanceof ContactSearchData.Story) {
                obj = new StoryModel((ContactSearchData.Story) contactSearchData, set.contains(contactSearchData.getContactSearchKey()), SignalStore.storyValues().getUserHasBeenNotifiedAboutStories());
            } else if (contactSearchData instanceof ContactSearchData.KnownRecipient) {
                obj = new RecipientModel((ContactSearchData.KnownRecipient) contactSearchData, set.contains(contactSearchData.getContactSearchKey()));
            } else if (contactSearchData instanceof ContactSearchData.Expand) {
                obj = new ExpandModel((ContactSearchData.Expand) contactSearchData);
            } else if (contactSearchData instanceof ContactSearchData.Header) {
                obj = new HeaderModel((ContactSearchData.Header) contactSearchData);
            } else {
                throw new NoWhenBranchMatchedException();
            }
            arrayList.add(obj);
        }
        return new MappingModelList(arrayList);
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "story", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "isSelected", "", "hasBeenNotified", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;ZZ)V", "getHasBeenNotified", "()Z", "getStory", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "areContentsTheSame", "newItem", "areItemsTheSame", "getChangePayload", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StoryModel implements MappingModel<StoryModel> {
        private final boolean hasBeenNotified;
        private final boolean isSelected;
        private final ContactSearchData.Story story;

        public StoryModel(ContactSearchData.Story story, boolean z, boolean z2) {
            Intrinsics.checkNotNullParameter(story, "story");
            this.story = story;
            this.isSelected = z;
            this.hasBeenNotified = z2;
        }

        public final boolean getHasBeenNotified() {
            return this.hasBeenNotified;
        }

        public final ContactSearchData.Story getStory() {
            return this.story;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public boolean areItemsTheSame(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "newItem");
            return Intrinsics.areEqual(storyModel.story, this.story);
        }

        public boolean areContentsTheSame(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "newItem");
            return this.story.getRecipient().hasSameContent(storyModel.story.getRecipient()) && this.isSelected == storyModel.isSelected && this.hasBeenNotified == storyModel.hasBeenNotified;
        }

        public Object getChangePayload(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "newItem");
            return (!this.story.getRecipient().hasSameContent(storyModel.story.getRecipient()) || this.hasBeenNotified != storyModel.hasBeenNotified || storyModel.isSelected == this.isSelected) ? null : 0;
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001BA\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\"\u0010\b\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\tj\u0002`\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0002H\u0014J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0002H\u0014J\u0010\u0010\u0012\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0002H\u0016J\u0016\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0010\u001a\u00020\u0002H\u0002J\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0010\u001a\u00020\u0002H\u0002J\u0016\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0010\u001a\u00020\u0002H\u0002J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u0010\u001a\u00020\u0002H\u0016J\u0010\u0010\u001a\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryViewHolder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$BaseRecipientViewHolder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryModel;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "itemView", "Landroid/view/View;", "displayCheckBox", "", "onClick", "Lkotlin/Function3;", "", "Lorg/thoughtcrime/securesms/contacts/paged/StoryClickListener;", "storyContextMenuCallbacks", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryContextMenuCallbacks;", "(Landroid/view/View;ZLkotlin/jvm/functions/Function3;Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$StoryContextMenuCallbacks;)V", "bindLongPress", "model", "bindNumberField", "getData", "getGroupStoryContextMenuActions", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "getMyStoryContextMenuActions", "getPrivateStoryContextMenuActions", "getRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isSelected", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StoryViewHolder extends BaseRecipientViewHolder<StoryModel, ContactSearchData.Story> {
        private final StoryContextMenuCallbacks storyContextMenuCallbacks;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StoryViewHolder(View view, boolean z, Function3<? super View, ? super ContactSearchData.Story, ? super Boolean, Unit> function3, StoryContextMenuCallbacks storyContextMenuCallbacks) {
            super(view, z, function3);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function3, "onClick");
            Intrinsics.checkNotNullParameter(storyContextMenuCallbacks, "storyContextMenuCallbacks");
            this.storyContextMenuCallbacks = storyContextMenuCallbacks;
        }

        public boolean isSelected(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "model");
            return storyModel.isSelected();
        }

        public ContactSearchData.Story getData(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "model");
            return storyModel.getStory();
        }

        public Recipient getRecipient(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "model");
            return storyModel.getStory().getRecipient();
        }

        public void bindNumberField(StoryModel storyModel) {
            int i;
            int i2;
            Intrinsics.checkNotNullParameter(storyModel, "model");
            ViewExtensionsKt.setVisible(getNumber(), true);
            if (storyModel.getStory().getRecipient().isGroup()) {
                i = storyModel.getStory().getRecipient().getParticipantIds().size();
            } else {
                i = storyModel.getStory().getViewerCount();
            }
            if (!storyModel.getStory().getRecipient().isMyStory() || storyModel.getHasBeenNotified()) {
                if (storyModel.getStory().getRecipient().isGroup()) {
                    i2 = R.plurals.ContactSearchItems__group_story_d_viewers;
                } else {
                    i2 = storyModel.getStory().getRecipient().isMyStory() ? R.plurals.SelectViewersFragment__d_viewers : R.plurals.ContactSearchItems__private_story_d_viewers;
                }
                getNumber().setText(this.context.getResources().getQuantityString(i2, i, Integer.valueOf(i)));
                return;
            }
            getNumber().setText(R.string.ContactSearchItems__tap_to_choose_your_viewers);
        }

        public void bindLongPress(StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyModel, "model");
            this.itemView.setOnLongClickListener(new ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda1(storyModel, this));
        }

        /* renamed from: bindLongPress$lambda-0 */
        public static final boolean m1320bindLongPress$lambda0(StoryModel storyModel, StoryViewHolder storyViewHolder, View view) {
            List<ActionItem> list;
            Intrinsics.checkNotNullParameter(storyModel, "$model");
            Intrinsics.checkNotNullParameter(storyViewHolder, "this$0");
            if (storyModel.getStory().getRecipient().isMyStory()) {
                list = storyViewHolder.getMyStoryContextMenuActions(storyModel);
            } else if (storyModel.getStory().getRecipient().isGroup()) {
                list = storyViewHolder.getGroupStoryContextMenuActions(storyModel);
            } else if (storyModel.getStory().getRecipient().isDistributionList()) {
                list = storyViewHolder.getPrivateStoryContextMenuActions(storyModel);
            } else {
                throw new IllegalStateException("Unsupported story target. Not a group or distribution list.".toString());
            }
            View view2 = storyViewHolder.itemView;
            Intrinsics.checkNotNullExpressionValue(view2, "itemView");
            View rootView = storyViewHolder.itemView.getRootView();
            if (rootView != null) {
                new SignalContextMenu.Builder(view2, (ViewGroup) rootView).offsetX(storyViewHolder.context.getResources().getDimensionPixelSize(R.dimen.dsl_settings_gutter)).show(list);
                return true;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }

        private final List<ActionItem> getMyStoryContextMenuActions(StoryModel storyModel) {
            String string = this.context.getString(R.string.ContactSearchItems__story_settings);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…rchItems__story_settings)");
            return CollectionsKt__CollectionsJVMKt.listOf(new ActionItem(R.drawable.ic_settings_24, string, 0, new ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda0(this, storyModel), 4, null));
        }

        /* renamed from: getMyStoryContextMenuActions$lambda-1 */
        public static final void m1322getMyStoryContextMenuActions$lambda1(StoryViewHolder storyViewHolder, StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(storyModel, "$model");
            storyViewHolder.storyContextMenuCallbacks.onOpenStorySettings(storyModel.getStory());
        }

        private final List<ActionItem> getGroupStoryContextMenuActions(StoryModel storyModel) {
            String string = this.context.getString(R.string.ContactSearchItems__remove_story);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…earchItems__remove_story)");
            return CollectionsKt__CollectionsJVMKt.listOf(new ActionItem(R.drawable.ic_minus_circle_20, string, 0, new ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda4(this, storyModel), 4, null));
        }

        /* renamed from: getGroupStoryContextMenuActions$lambda-2 */
        public static final void m1321getGroupStoryContextMenuActions$lambda2(StoryViewHolder storyViewHolder, StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(storyModel, "$model");
            storyViewHolder.storyContextMenuCallbacks.onRemoveGroupStory(storyModel.getStory(), storyModel.isSelected());
        }

        private final List<ActionItem> getPrivateStoryContextMenuActions(StoryModel storyModel) {
            String string = this.context.getString(R.string.ContactSearchItems__story_settings);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…rchItems__story_settings)");
            String string2 = this.context.getString(R.string.ContactSearchItems__delete_story);
            Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…earchItems__delete_story)");
            return CollectionsKt__CollectionsKt.listOf((Object[]) new ActionItem[]{new ActionItem(R.drawable.ic_settings_24, string, 0, new ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda2(this, storyModel), 4, null), new ActionItem(R.drawable.ic_delete_24, string2, R.color.signal_colorError, new ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda3(this, storyModel))});
        }

        /* renamed from: getPrivateStoryContextMenuActions$lambda-3 */
        public static final void m1323getPrivateStoryContextMenuActions$lambda3(StoryViewHolder storyViewHolder, StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(storyModel, "$model");
            storyViewHolder.storyContextMenuCallbacks.onOpenStorySettings(storyModel.getStory());
        }

        /* renamed from: getPrivateStoryContextMenuActions$lambda-4 */
        public static final void m1324getPrivateStoryContextMenuActions$lambda4(StoryViewHolder storyViewHolder, StoryModel storyModel) {
            Intrinsics.checkNotNullParameter(storyViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(storyModel, "$model");
            storyViewHolder.storyContextMenuCallbacks.onDeletePrivateStory(storyModel.getStory(), storyModel.isSelected());
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0000\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\n\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000b\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$RecipientModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "knownRecipient", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;", "isSelected", "", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;Z)V", "()Z", "getKnownRecipient", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;", "areContentsTheSame", "newItem", "areItemsTheSame", "getChangePayload", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientModel implements MappingModel<RecipientModel> {
        private final boolean isSelected;
        private final ContactSearchData.KnownRecipient knownRecipient;

        public RecipientModel(ContactSearchData.KnownRecipient knownRecipient, boolean z) {
            Intrinsics.checkNotNullParameter(knownRecipient, "knownRecipient");
            this.knownRecipient = knownRecipient;
            this.isSelected = z;
        }

        public final ContactSearchData.KnownRecipient getKnownRecipient() {
            return this.knownRecipient;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public boolean areItemsTheSame(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return Intrinsics.areEqual(recipientModel.knownRecipient, this.knownRecipient);
        }

        public boolean areContentsTheSame(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return this.knownRecipient.getRecipient().hasSameContent(recipientModel.knownRecipient.getRecipient()) && this.isSelected == recipientModel.isSelected;
        }

        public Object getChangePayload(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return (!this.knownRecipient.getRecipient().hasSameContent(recipientModel.knownRecipient.getRecipient()) || recipientModel.isSelected == this.isSelected) ? null : 0;
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B9\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\"\u0010\b\u001a\u001e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\n0\tj\u0002`\u000b¢\u0006\u0002\u0010\fJ\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0002H\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000e\u001a\u00020\u0002H\u0016J\u0010\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u0002H\u0016¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$KnownRecipientViewHolder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$BaseRecipientViewHolder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$RecipientModel;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;", "itemView", "Landroid/view/View;", "displayCheckBox", "", "onClick", "Lkotlin/Function3;", "", "Lorg/thoughtcrime/securesms/contacts/paged/RecipientClickListener;", "(Landroid/view/View;ZLkotlin/jvm/functions/Function3;)V", "getData", "model", "getRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isSelected", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class KnownRecipientViewHolder extends BaseRecipientViewHolder<RecipientModel, ContactSearchData.KnownRecipient> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public KnownRecipientViewHolder(View view, boolean z, Function3<? super View, ? super ContactSearchData.KnownRecipient, ? super Boolean, Unit> function3) {
            super(view, z, function3);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function3, "onClick");
        }

        public boolean isSelected(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "model");
            return recipientModel.isSelected();
        }

        public ContactSearchData.KnownRecipient getData(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "model");
            return recipientModel.getKnownRecipient();
        }

        public Recipient getRecipient(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "model");
            return recipientModel.getKnownRecipient().getRecipient();
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0005\b\"\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u0002*\b\b\u0001\u0010\u0003*\u00020\u00042\b\u0012\u0004\u0012\u0002H\u00010\u0005B5\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u001e\u0010\n\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0002\u0010\rJ\u0015\u0010)\u001a\u00020\f2\u0006\u0010*\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010+J\u0015\u0010,\u001a\u00020\f2\u0006\u0010*\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010+J\u0015\u0010-\u001a\u00020\f2\u0006\u0010*\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010+J\u0015\u0010.\u001a\u00020\f2\u0006\u0010*\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010+J\u0015\u0010/\u001a\u00020\f2\u0006\u0010*\u001a\u00028\u0000H\u0014¢\u0006\u0002\u0010+J\u0015\u00100\u001a\u00028\u00012\u0006\u0010*\u001a\u00028\u0000H&¢\u0006\u0002\u00101J\u0015\u00102\u001a\u0002032\u0006\u0010*\u001a\u00028\u0000H&¢\u0006\u0002\u00104J\u0015\u00105\u001a\u00020\t2\u0006\u0010*\u001a\u00028\u0000H&¢\u0006\u0002\u00106J\u0015\u00107\u001a\u00020\t2\u0006\u0010*\u001a\u00028\u0000H\u0002¢\u0006\u0002\u00106R\u0014\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\u00020\u001bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0014\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0014\u0010\"\u001a\u00020\u001bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001dR)\u0010\n\u001a\u001a\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0014\u0010&\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(¨\u00068"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$BaseRecipientViewHolder;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "D", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "displayCheckBox", "", "onClick", "Lkotlin/Function3;", "", "(Landroid/view/View;ZLkotlin/jvm/functions/Function3;)V", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "getAvatar", "()Lorg/thoughtcrime/securesms/components/AvatarImageView;", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "getBadge", "()Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "checkbox", "Landroid/widget/CheckBox;", "getCheckbox", "()Landroid/widget/CheckBox;", EmojiSearchDatabase.LABEL, "Landroid/widget/TextView;", "getLabel", "()Landroid/widget/TextView;", "name", "Lorg/thoughtcrime/securesms/components/FromTextView;", "getName", "()Lorg/thoughtcrime/securesms/components/FromTextView;", "number", "getNumber", "getOnClick", "()Lkotlin/jvm/functions/Function3;", "smsTag", "getSmsTag", "()Landroid/view/View;", "bind", "model", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;)V", "bindLabelField", "bindLongPress", "bindNumberField", "bindSmsTagField", "getData", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;)Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "getRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;)Lorg/thoughtcrime/securesms/recipients/Recipient;", "isSelected", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;)Z", "isSmsContact", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class BaseRecipientViewHolder<T extends MappingModel<T>, D extends ContactSearchData> extends MappingViewHolder<T> {
        private final AvatarImageView avatar;
        private final BadgeImageView badge;
        private final CheckBox checkbox;
        private final boolean displayCheckBox;
        private final TextView label;
        private final FromTextView name;
        private final TextView number;
        private final Function3<View, D, Boolean, Unit> onClick;
        private final View smsTag;

        protected void bindLongPress(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
        }

        public abstract D getData(T t);

        public abstract Recipient getRecipient(T t);

        public abstract boolean isSelected(T t);

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function3<? super android.view.View, ? super D extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData, ? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BaseRecipientViewHolder(View view, boolean z, Function3<? super View, ? super D, ? super Boolean, Unit> function3) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function3, "onClick");
            this.displayCheckBox = z;
            this.onClick = function3;
            View findViewById = view.findViewById(R.id.contact_photo_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.contact_photo_image)");
            this.avatar = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.contact_badge);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.contact_badge)");
            this.badge = (BadgeImageView) findViewById2;
            View findViewById3 = view.findViewById(R.id.check_box);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.check_box)");
            this.checkbox = (CheckBox) findViewById3;
            View findViewById4 = view.findViewById(R.id.name);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.id.name)");
            this.name = (FromTextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.number);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.number)");
            this.number = (TextView) findViewById5;
            View findViewById6 = view.findViewById(R.id.label);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "itemView.findViewById(R.id.label)");
            this.label = (TextView) findViewById6;
            View findViewById7 = view.findViewById(R.id.sms_tag);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "itemView.findViewById(R.id.sms_tag)");
            this.smsTag = findViewById7;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.contacts.paged.ContactSearchItems$BaseRecipientViewHolder<T extends org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<T>, D extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public /* bridge */ /* synthetic */ void bind(Object obj) {
            bind((BaseRecipientViewHolder<T, D>) ((MappingModel) obj));
        }

        public final Function3<View, D, Boolean, Unit> getOnClick() {
            return this.onClick;
        }

        protected final AvatarImageView getAvatar() {
            return this.avatar;
        }

        protected final BadgeImageView getBadge() {
            return this.badge;
        }

        protected final CheckBox getCheckbox() {
            return this.checkbox;
        }

        protected final FromTextView getName() {
            return this.name;
        }

        protected final TextView getNumber() {
            return this.number;
        }

        protected final TextView getLabel() {
            return this.label;
        }

        protected final View getSmsTag() {
            return this.smsTag;
        }

        public void bind(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            ViewExtensionsKt.setVisible(this.checkbox, this.displayCheckBox);
            this.checkbox.setChecked(isSelected(t));
            this.itemView.setOnClickListener(new ContactSearchItems$BaseRecipientViewHolder$$ExternalSyntheticLambda0(this, t));
            bindLongPress(t);
            List<Object> list = this.payload;
            Intrinsics.checkNotNullExpressionValue(list, "payload");
            if (!(!list.isEmpty())) {
                this.name.setText(getRecipient(t));
                this.avatar.setAvatar(getRecipient(t));
                this.badge.setBadgeFromRecipient(getRecipient(t));
                bindNumberField(t);
                bindLabelField(t);
                bindSmsTagField(t);
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v3, resolved type: kotlin.jvm.functions.Function3<android.view.View, D extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData, java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* renamed from: bind$lambda-0 */
        public static final void m1313bind$lambda0(BaseRecipientViewHolder baseRecipientViewHolder, MappingModel mappingModel, View view) {
            Intrinsics.checkNotNullParameter(baseRecipientViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(mappingModel, "$model");
            View view2 = baseRecipientViewHolder.itemView;
            Intrinsics.checkNotNullExpressionValue(view2, "itemView");
            baseRecipientViewHolder.onClick.invoke(view2, baseRecipientViewHolder.getData(mappingModel), Boolean.valueOf(baseRecipientViewHolder.isSelected(mappingModel)));
        }

        protected void bindNumberField(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            ViewExtensionsKt.setVisible(this.number, getRecipient(t).isGroup());
            if (getRecipient(t).isGroup()) {
                TextView textView = this.number;
                List<RecipientId> participantIds = getRecipient(t).getParticipantIds();
                Intrinsics.checkNotNullExpressionValue(participantIds, "getRecipient(model).participantIds");
                List<RecipientId> list = CollectionsKt___CollectionsKt.take(participantIds, 10);
                ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                for (RecipientId recipientId : list) {
                    arrayList.add(Recipient.resolved(recipientId));
                }
                textView.setText(CollectionsKt___CollectionsKt.joinToString$default(CollectionsKt___CollectionsKt.sortedWith(arrayList, new IsSelfComparator()), ", ", null, null, 0, null, new ContactSearchItems$BaseRecipientViewHolder$bindNumberField$2(this), 30, null));
            }
        }

        protected void bindLabelField(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            ViewExtensionsKt.setVisible(this.label, false);
        }

        protected void bindSmsTagField(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            ViewExtensionsKt.setVisible(this.smsTag, isSmsContact(t));
        }

        private final boolean isSmsContact(T t) {
            return (getRecipient(t).isForceSmsSelection() || getRecipient(t).isUnregistered()) && !getRecipient(t).isDistributionList();
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$HeaderModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "header", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Header;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Header;)V", "getHeader", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Header;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class HeaderModel implements MappingModel<HeaderModel> {
        private final ContactSearchData.Header header;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(HeaderModel headerModel) {
            return MappingModel.CC.$default$getChangePayload(this, headerModel);
        }

        public HeaderModel(ContactSearchData.Header header) {
            Intrinsics.checkNotNullParameter(header, "header");
            this.header = header;
        }

        public final ContactSearchData.Header getHeader() {
            return this.header;
        }

        public boolean areItemsTheSame(HeaderModel headerModel) {
            Intrinsics.checkNotNullParameter(headerModel, "newItem");
            return this.header.getSectionKey() == headerModel.header.getSectionKey();
        }

        public boolean areContentsTheSame(HeaderModel headerModel) {
            Intrinsics.checkNotNullParameter(headerModel, "newItem");
            if (areItemsTheSame(headerModel)) {
                HeaderAction action = this.header.getAction();
                Integer num = null;
                Integer valueOf = action != null ? Integer.valueOf(action.getIcon()) : null;
                HeaderAction action2 = headerModel.header.getAction();
                if (Intrinsics.areEqual(valueOf, action2 != null ? Integer.valueOf(action2.getIcon()) : null)) {
                    HeaderAction action3 = this.header.getAction();
                    Integer valueOf2 = action3 != null ? Integer.valueOf(action3.getLabel()) : null;
                    HeaderAction action4 = headerModel.header.getAction();
                    if (action4 != null) {
                        num = Integer.valueOf(action4.getLabel());
                    }
                    if (Intrinsics.areEqual(valueOf2, num)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$HeaderViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$HeaderModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "headerActionView", "Landroid/widget/TextView;", "headerTextView", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class HeaderViewHolder extends MappingViewHolder<HeaderModel> {
        private final TextView headerActionView;
        private final TextView headerTextView;

        /* compiled from: ContactSearchItems.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[ContactSearchConfiguration.SectionKey.values().length];
                iArr[ContactSearchConfiguration.SectionKey.STORIES.ordinal()] = 1;
                iArr[ContactSearchConfiguration.SectionKey.RECENTS.ordinal()] = 2;
                iArr[ContactSearchConfiguration.SectionKey.INDIVIDUALS.ordinal()] = 3;
                iArr[ContactSearchConfiguration.SectionKey.GROUPS.ordinal()] = 4;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public HeaderViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.section_header);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.section_header)");
            this.headerTextView = (TextView) findViewById;
            View findViewById2 = view.findViewById(R.id.section_header_action);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.section_header_action)");
            this.headerActionView = (TextView) findViewById2;
        }

        public void bind(HeaderModel headerModel) {
            int i;
            Intrinsics.checkNotNullParameter(headerModel, "model");
            TextView textView = this.headerTextView;
            int i2 = WhenMappings.$EnumSwitchMapping$0[headerModel.getHeader().getSectionKey().ordinal()];
            if (i2 == 1) {
                i = R.string.ContactsCursorLoader_my_stories;
            } else if (i2 == 2) {
                i = R.string.ContactsCursorLoader_recent_chats;
            } else if (i2 == 3) {
                i = R.string.ContactsCursorLoader_contacts;
            } else if (i2 == 4) {
                i = R.string.ContactsCursorLoader_groups;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            textView.setText(i);
            if (headerModel.getHeader().getAction() != null) {
                ViewExtensionsKt.setVisible(this.headerActionView, true);
                this.headerActionView.setCompoundDrawablesRelativeWithIntrinsicBounds(headerModel.getHeader().getAction().getIcon(), 0, 0, 0);
                this.headerActionView.setText(headerModel.getHeader().getAction().getLabel());
                this.headerActionView.setOnClickListener(new ContactSearchItems$HeaderViewHolder$$ExternalSyntheticLambda0(headerModel));
                return;
            }
            ViewExtensionsKt.setVisible(this.headerActionView, false);
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1317bind$lambda0(HeaderModel headerModel, View view) {
            Intrinsics.checkNotNullParameter(headerModel, "$model");
            headerModel.getHeader().getAction().getAction().run();
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$ExpandModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "expand", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;)V", "getExpand", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExpandModel implements MappingModel<ExpandModel> {
        private final ContactSearchData.Expand expand;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(ExpandModel expandModel) {
            return MappingModel.CC.$default$getChangePayload(this, expandModel);
        }

        public ExpandModel(ContactSearchData.Expand expand) {
            Intrinsics.checkNotNullParameter(expand, "expand");
            this.expand = expand;
        }

        public final ContactSearchData.Expand getExpand() {
            return this.expand;
        }

        public boolean areItemsTheSame(ExpandModel expandModel) {
            Intrinsics.checkNotNullParameter(expandModel, "newItem");
            return Intrinsics.areEqual(this.expand.getContactSearchKey(), expandModel.expand.getContactSearchKey());
        }

        public boolean areContentsTheSame(ExpandModel expandModel) {
            Intrinsics.checkNotNullParameter(expandModel, "newItem");
            return areItemsTheSame(expandModel);
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$ExpandViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$ExpandModel;", "itemView", "Landroid/view/View;", "expandListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExpandViewHolder extends MappingViewHolder<ExpandModel> {
        private final Function1<ContactSearchData.Expand, Unit> expandListener;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.contacts.paged.ContactSearchData$Expand, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ExpandViewHolder(View view, Function1<? super ContactSearchData.Expand, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "expandListener");
            this.expandListener = function1;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1315bind$lambda0(ExpandViewHolder expandViewHolder, ExpandModel expandModel, View view) {
            Intrinsics.checkNotNullParameter(expandViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(expandModel, "$model");
            expandViewHolder.expandListener.invoke(expandModel.getExpand());
        }

        public void bind(ExpandModel expandModel) {
            Intrinsics.checkNotNullParameter(expandModel, "model");
            this.itemView.setOnClickListener(new ContactSearchItems$ExpandViewHolder$$ExternalSyntheticLambda0(this, expandModel));
        }
    }

    /* compiled from: ContactSearchItems.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0002\u0018\u00002\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003B\u0005¢\u0006\u0002\u0010\u0004J\u001c\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u00022\b\u0010\b\u001a\u0004\u0018\u00010\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchItems$IsSelfComparator;", "Ljava/util/Comparator;", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Lkotlin/Comparator;", "()V", "compare", "", "lhs", "rhs", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class IsSelfComparator implements Comparator<Recipient> {
        public int compare(Recipient recipient, Recipient recipient2) {
            boolean z = recipient != null && recipient.isSelf();
            if (z == (recipient2 != null && recipient2.isSelf())) {
                return 0;
            }
            if (z) {
                return 1;
            }
            return -1;
        }
    }
}
