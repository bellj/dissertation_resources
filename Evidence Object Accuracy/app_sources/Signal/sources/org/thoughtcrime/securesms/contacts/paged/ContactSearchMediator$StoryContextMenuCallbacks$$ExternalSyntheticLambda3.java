package org.thoughtcrime.securesms.contacts.paged;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda3 implements DialogInterface.OnClickListener {
    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        ContactSearchMediator.StoryContextMenuCallbacks.m1332onRemoveGroupStory$lambda1(dialogInterface, i);
    }
}
