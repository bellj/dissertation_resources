package org.thoughtcrime.securesms.contacts.sync;

import android.content.Context;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import kotlin.text.StringsKt__StringsJVMKt;
import org.signal.contacts.SystemContactsRepository;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.services.CdsiV2Service;

/* compiled from: ContactDiscoveryRefreshV2.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002JJ\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00100\u000e2\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\u001e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aH\u0007J\u0010\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J\u0018\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b*\b\u0012\u0004\u0012\u00020\u00060\u000bH\u0002J \u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b*\b\u0012\u0004\u0012\u00020\u00060\u000b2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/sync/ContactDiscoveryRefreshV2;", "", "()V", "MAXIMUM_ONE_OFF_REQUEST_SIZE", "", "TAG", "", "kotlin.jvm.PlatformType", "makeRequest", "Lorg/whispersystems/signalservice/api/services/CdsiV2Service$Response;", "previousE164s", "", "newE164s", "serviceIds", "", "Lorg/whispersystems/signalservice/api/push/ServiceId;", "Lorg/signal/libsignal/zkgroup/profiles/ProfileKey;", "token", "", "saveToken", "", "refresh", "Lorg/thoughtcrime/securesms/contacts/sync/ContactDiscovery$RefreshResult;", "context", "Landroid/content/Context;", "inputRecipients", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "refreshAll", "sanitize", "toE164s", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactDiscoveryRefreshV2 {
    public static final ContactDiscoveryRefreshV2 INSTANCE = new ContactDiscoveryRefreshV2();
    private static final int MAXIMUM_ONE_OFF_REQUEST_SIZE;
    private static final String TAG = Log.tag(ContactDiscoveryRefreshV2.class);

    private ContactDiscoveryRefreshV2() {
    }

    @JvmStatic
    public static final synchronized ContactDiscovery.RefreshResult refreshAll(Context context) throws IOException {
        Set<String> set;
        ContactDiscovery.RefreshResult refreshResult;
        synchronized (ContactDiscoveryRefreshV2.class) {
            Intrinsics.checkNotNullParameter(context, "context");
            Stopwatch stopwatch = new Stopwatch("refresh-all");
            if (SignalStore.misc().getCdsToken() != null) {
                set = SignalDatabase.Companion.cds().getAllE164s();
            } else {
                Log.w(TAG, "No token set! Cannot provide previousE164s.");
                set = SetsKt__SetsKt.emptySet();
            }
            stopwatch.split("previous");
            ContactDiscoveryRefreshV2 contactDiscoveryRefreshV2 = INSTANCE;
            SignalDatabase.Companion companion = SignalDatabase.Companion;
            Set<String> sanitize = contactDiscoveryRefreshV2.sanitize(companion.recipients().getAllE164s());
            Set set2 = SetsKt___SetsKt.minus((Set) sanitize, (Iterable) set);
            stopwatch.split(RecipientDatabase.TABLE_NAME);
            Set<String> sanitize2 = contactDiscoveryRefreshV2.sanitize(contactDiscoveryRefreshV2.toE164s(SystemContactsRepository.getAllDisplayNumbers(context), context));
            Set set3 = SetsKt___SetsKt.minus((Set) sanitize2, (Iterable) set);
            stopwatch.split("system");
            Set<String> set4 = SetsKt___SetsKt.plus(set2, (Iterable) set3);
            CdsiV2Service.Response makeRequest = contactDiscoveryRefreshV2.makeRequest(set, set4, companion.recipients().getAllServiceIdProfileKeyPairs(), SignalStore.misc().getCdsToken(), true);
            stopwatch.split("network");
            companion.cds().updateAfterCdsQuery(set4, SetsKt___SetsKt.plus((Set) sanitize, (Iterable) sanitize2));
            stopwatch.split("cds-db");
            RecipientDatabase recipients = companion.recipients();
            Map<String, CdsiV2Service.ResponseItem> results = makeRequest.getResults();
            Intrinsics.checkNotNullExpressionValue(results, "response.results");
            LinkedHashMap linkedHashMap = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(results.size()));
            for (Object obj : results.entrySet()) {
                Object key = ((Map.Entry) obj).getKey();
                Map.Entry entry = (Map.Entry) obj;
                PNI pni = ((CdsiV2Service.ResponseItem) entry.getValue()).getPni();
                Intrinsics.checkNotNullExpressionValue(pni, "entry.value.pni");
                linkedHashMap.put(key, new RecipientDatabase.CdsV2Result(pni, ((CdsiV2Service.ResponseItem) entry.getValue()).getAci().orElse(null)));
            }
            Set<RecipientId> bulkProcessCdsV2Result = recipients.bulkProcessCdsV2Result(linkedHashMap);
            stopwatch.split("recipient-db");
            RecipientDatabase recipients2 = SignalDatabase.Companion.recipients();
            LinkedHashMap linkedHashMap2 = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(bulkProcessCdsV2Result, 10)), 16));
            for (Object obj2 : bulkProcessCdsV2Result) {
                RecipientId recipientId = (RecipientId) obj2;
                linkedHashMap2.put(obj2, null);
            }
            recipients2.bulkUpdatedRegisteredStatus(linkedHashMap2, CollectionsKt__CollectionsKt.emptyList());
            stopwatch.split("update-registered");
            stopwatch.stop(TAG);
            refreshResult = new ContactDiscovery.RefreshResult(bulkProcessCdsV2Result, MapsKt__MapsKt.emptyMap());
        }
        return refreshResult;
    }

    @JvmStatic
    public static final synchronized ContactDiscovery.RefreshResult refresh(Context context, List<? extends Recipient> list) throws IOException {
        synchronized (ContactDiscoveryRefreshV2.class) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(list, "inputRecipients");
            Stopwatch stopwatch = new Stopwatch("refresh-some");
            ArrayList<Recipient> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (Recipient recipient : list) {
                arrayList.add(recipient.resolve());
            }
            stopwatch.split("resolve");
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (Recipient recipient2 : arrayList) {
                arrayList2.add(recipient2.getId());
            }
            Set set = CollectionsKt___CollectionsKt.toSet(arrayList2);
            ArrayList arrayList3 = new ArrayList();
            for (Recipient recipient3 : arrayList) {
                String orElse = recipient3.getE164().orElse(null);
                if (orElse != null) {
                    arrayList3.add(orElse);
                }
            }
            Set<String> set2 = CollectionsKt___CollectionsKt.toSet(arrayList3);
            if (set2.size() > 3) {
                String str = TAG;
                Log.i(str, "List of specific recipients to refresh is too large! (Size: " + arrayList.size() + "). Doing a full refresh instead.");
                ContactDiscovery.RefreshResult refreshAll = refreshAll(context);
                Set set3 = CollectionsKt___CollectionsKt.intersect(refreshAll.getRegisteredIds(), set);
                Map<String, String> rewrites = refreshAll.getRewrites();
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                for (Map.Entry<String, String> entry : rewrites.entrySet()) {
                    if (set2.contains(entry.getKey())) {
                        linkedHashMap.put(entry.getKey(), entry.getValue());
                    }
                }
                return new ContactDiscovery.RefreshResult(set3, linkedHashMap);
            } else if (set2.isEmpty()) {
                Log.w(TAG, "No numbers to refresh!");
                return new ContactDiscovery.RefreshResult(SetsKt__SetsKt.emptySet(), MapsKt__MapsKt.emptyMap());
            } else {
                String str2 = TAG;
                Log.i(str2, "Doing a one-off request for " + set2.size() + " recipients.");
                ContactDiscoveryRefreshV2 contactDiscoveryRefreshV2 = INSTANCE;
                Set<String> set4 = SetsKt__SetsKt.emptySet();
                SignalDatabase.Companion companion = SignalDatabase.Companion;
                CdsiV2Service.Response makeRequest = contactDiscoveryRefreshV2.makeRequest(set4, set2, companion.recipients().getAllServiceIdProfileKeyPairs(), null, false);
                stopwatch.split("network");
                RecipientDatabase recipients = companion.recipients();
                Map<String, CdsiV2Service.ResponseItem> results = makeRequest.getResults();
                Intrinsics.checkNotNullExpressionValue(results, "response.results");
                LinkedHashMap linkedHashMap2 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(results.size()));
                for (Object obj : results.entrySet()) {
                    Object key = ((Map.Entry) obj).getKey();
                    Map.Entry entry2 = (Map.Entry) obj;
                    PNI pni = ((CdsiV2Service.ResponseItem) entry2.getValue()).getPni();
                    Intrinsics.checkNotNullExpressionValue(pni, "entry.value.pni");
                    linkedHashMap2.put(key, new RecipientDatabase.CdsV2Result(pni, ((CdsiV2Service.ResponseItem) entry2.getValue()).getAci().orElse(null)));
                }
                Set<RecipientId> bulkProcessCdsV2Result = recipients.bulkProcessCdsV2Result(linkedHashMap2);
                stopwatch.split("recipient-db");
                RecipientDatabase recipients2 = SignalDatabase.Companion.recipients();
                LinkedHashMap linkedHashMap3 = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(CollectionsKt__IterablesKt.collectionSizeOrDefault(bulkProcessCdsV2Result, 10)), 16));
                for (Object obj2 : bulkProcessCdsV2Result) {
                    RecipientId recipientId = (RecipientId) obj2;
                    linkedHashMap3.put(obj2, null);
                }
                recipients2.bulkUpdatedRegisteredStatus(linkedHashMap3, CollectionsKt__CollectionsKt.emptyList());
                stopwatch.split("update-registered");
                stopwatch.stop(TAG);
                return new ContactDiscovery.RefreshResult(bulkProcessCdsV2Result, MapsKt__MapsKt.emptyMap());
            }
        }
    }

    private final CdsiV2Service.Response makeRequest(Set<String> set, Set<String> set2, Map<ServiceId, ProfileKey> map, byte[] bArr, boolean z) throws IOException {
        CdsiV2Service.Response registeredUsersWithCdsi = ApplicationDependencies.getSignalServiceAccountManager().getRegisteredUsersWithCdsi(set, set2, map, Optional.ofNullable(bArr), BuildConfig.CDSI_MRENCLAVE, new Consumer(z) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV2$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ContactDiscoveryRefreshV2.$r8$lambda$purMrc9CiMxZy7rGGZiBg7HXXyE(this.f$0, (byte[]) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        Intrinsics.checkNotNullExpressionValue(registeredUsersWithCdsi, "getSignalServiceAccountM…ken = token\n      }\n    }");
        return registeredUsersWithCdsi;
    }

    /* renamed from: makeRequest$lambda-8 */
    public static final void m1355makeRequest$lambda8(boolean z, byte[] bArr) {
        if (z) {
            SignalStore.misc().setCdsToken(bArr);
        }
    }

    private final Set<String> sanitize(Set<String> set) {
        ArrayList arrayList = new ArrayList();
        for (Object obj : set) {
            String str = (String) obj;
            boolean z = false;
            try {
                if ((StringsKt__StringsJVMKt.startsWith$default(str, "+", false, 2, null)) && str.length() > 1 && str.charAt(1) != '0' && Long.parseLong(str) > 0) {
                    z = true;
                }
            } catch (NumberFormatException unused) {
            }
            if (z) {
                arrayList.add(obj);
            }
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }

    private final Set<String> toE164s(Set<String> set, Context context) {
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        for (String str : set) {
            arrayList.add(PhoneNumberFormatter.get(context).format(str));
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }
}
