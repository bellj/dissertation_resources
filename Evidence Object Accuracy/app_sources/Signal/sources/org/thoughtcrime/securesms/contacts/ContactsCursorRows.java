package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blocked.BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public final class ContactsCursorRows {
    private static final String[] CONTACT_PROJECTION = {ContactRepository.ID_COLUMN, "name", "number", "number_type", EmojiSearchDatabase.LABEL, "contact_type", RecipientDatabase.ABOUT};

    public static MatrixCursor createMatrixCursor() {
        return new MatrixCursor(CONTACT_PROJECTION);
    }

    public static MatrixCursor createMatrixCursor(int i) {
        return new MatrixCursor(CONTACT_PROJECTION, i);
    }

    public static Object[] forRecipient(Context context, Recipient recipient) {
        String str;
        int i = 1;
        if (recipient.isGroup()) {
            str = recipient.requireGroupId().toString();
        } else {
            str = (String) OptionalUtil.or(recipient.getE164().map(new BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1()), recipient.getEmail()).orElse("");
        }
        Object[] objArr = new Object[7];
        objArr[0] = recipient.getId().serialize();
        objArr[1] = recipient.getDisplayName(context);
        objArr[2] = str;
        objArr[3] = 2;
        objArr[4] = "";
        if (!recipient.isRegistered() || recipient.isForceSmsSelection()) {
            i = 0;
        }
        objArr[5] = Integer.valueOf(16 | i);
        objArr[6] = recipient.getCombinedAboutAndEmoji();
        return objArr;
    }

    public static Object[] forNonPushContact(Cursor cursor) {
        return new Object[]{Long.valueOf(cursor.getLong(cursor.getColumnIndexOrThrow(ContactRepository.ID_COLUMN))), cursor.getString(cursor.getColumnIndexOrThrow("name")), cursor.getString(cursor.getColumnIndexOrThrow("number")), cursor.getString(cursor.getColumnIndexOrThrow("number_type")), cursor.getString(cursor.getColumnIndexOrThrow(EmojiSearchDatabase.LABEL)), 0, ""};
    }

    public static Object[] forGroup(GroupDatabase.GroupRecord groupRecord) {
        return new Object[]{groupRecord.getRecipientId().serialize(), groupRecord.getTitle(), groupRecord.getId(), 0, "", 0, ""};
    }

    public static MatrixCursor forNewNumber(String str, String str2) {
        MatrixCursor createMatrixCursor = createMatrixCursor(1);
        createMatrixCursor.addRow(new Object[]{null, str, str2, 0, "⇢", 4, ""});
        return createMatrixCursor;
    }

    public static MatrixCursor forUsernameSearch(String str, String str2) {
        MatrixCursor createMatrixCursor = createMatrixCursor(1);
        createMatrixCursor.addRow(new Object[]{null, str, str2, 0, "⇢", 8, ""});
        return createMatrixCursor;
    }

    public static MatrixCursor forUsernameSearchHeader(Context context) {
        return forHeader(context.getString(R.string.ContactsCursorLoader_username_search));
    }

    public static MatrixCursor forPhoneNumberSearchHeader(Context context) {
        return forHeader(context.getString(R.string.ContactsCursorLoader_phone_number_search));
    }

    public static MatrixCursor forGroupsHeader(Context context) {
        return forHeader(context.getString(R.string.ContactsCursorLoader_groups));
    }

    public static MatrixCursor forRecentsHeader(Context context) {
        return forHeader(context.getString(R.string.ContactsCursorLoader_recent_chats));
    }

    public static MatrixCursor forContactsHeader(Context context) {
        return forHeader(context.getString(R.string.ContactsCursorLoader_contacts));
    }

    public static MatrixCursor forHeader(String str) {
        MatrixCursor createMatrixCursor = createMatrixCursor(1);
        createMatrixCursor.addRow(new Object[]{null, str, "", 2, "", 32, ""});
        return createMatrixCursor;
    }
}
