package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.chip.Chip;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class ContactChip extends Chip {
    private SelectedContact contact;

    public ContactChip(Context context) {
        super(context);
    }

    public ContactChip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ContactChip(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setContact(SelectedContact selectedContact) {
        this.contact = selectedContact;
    }

    public SelectedContact getContact() {
        return this.contact;
    }

    public void setAvatar(GlideRequests glideRequests, Recipient recipient, final Runnable runnable) {
        if (recipient != null) {
            glideRequests.clear(this);
            HalfScaleDrawable halfScaleDrawable = new HalfScaleDrawable(recipient.getFallbackContactPhotoDrawable(getContext(), false));
            ContactPhoto contactPhoto = recipient.getContactPhoto();
            if (contactPhoto == null) {
                setChipIcon(halfScaleDrawable);
                if (runnable != null) {
                    runnable.run();
                    return;
                }
                return;
            }
            glideRequests.load((Object) contactPhoto).placeholder((Drawable) halfScaleDrawable).fallback((Drawable) halfScaleDrawable).error((Drawable) halfScaleDrawable).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into((GlideRequest<Drawable>) new CustomTarget<Drawable>() { // from class: org.thoughtcrime.securesms.contacts.ContactChip.1
                @Override // com.bumptech.glide.request.target.Target
                public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                    onResourceReady((Drawable) obj, (Transition<? super Drawable>) transition);
                }

                public void onResourceReady(Drawable drawable, Transition<? super Drawable> transition) {
                    ContactChip.this.setChipIcon(drawable);
                    Runnable runnable2 = runnable;
                    if (runnable2 != null) {
                        runnable2.run();
                    }
                }

                @Override // com.bumptech.glide.request.target.Target
                public void onLoadCleared(Drawable drawable) {
                    ContactChip.this.setChipIcon(drawable);
                }
            });
        }
    }

    /* loaded from: classes4.dex */
    public static class HalfScaleDrawable extends Drawable {
        private final Drawable fallbackContactPhotoDrawable;

        @Override // android.graphics.drawable.Drawable
        public int getOpacity() {
            return -1;
        }

        @Override // android.graphics.drawable.Drawable
        public void setAlpha(int i) {
        }

        @Override // android.graphics.drawable.Drawable
        public void setColorFilter(ColorFilter colorFilter) {
        }

        HalfScaleDrawable(Drawable drawable) {
            this.fallbackContactPhotoDrawable = drawable;
        }

        @Override // android.graphics.drawable.Drawable
        public void setBounds(int i, int i2, int i3, int i4) {
            super.setBounds(i, i2, i3, i4);
            this.fallbackContactPhotoDrawable.setBounds(i, i2, (i3 * 2) - i, (i4 * 2) - i2);
        }

        @Override // android.graphics.drawable.Drawable
        public void setBounds(Rect rect) {
            super.setBounds(rect);
        }

        @Override // android.graphics.drawable.Drawable
        public void draw(Canvas canvas) {
            canvas.save();
            canvas.scale(0.5f, 0.5f);
            this.fallbackContactPhotoDrawable.draw(canvas);
            canvas.restore();
        }
    }
}
