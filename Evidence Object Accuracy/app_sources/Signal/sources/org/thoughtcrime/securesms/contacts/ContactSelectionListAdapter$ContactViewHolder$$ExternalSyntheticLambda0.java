package org.thoughtcrime.securesms.contacts;

import android.view.View;
import org.thoughtcrime.securesms.contacts.ContactSelectionListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSelectionListAdapter$ContactViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContactSelectionListAdapter.ContactViewHolder f$0;
    public final /* synthetic */ ContactSelectionListAdapter.ItemClickListener f$1;

    public /* synthetic */ ContactSelectionListAdapter$ContactViewHolder$$ExternalSyntheticLambda0(ContactSelectionListAdapter.ContactViewHolder contactViewHolder, ContactSelectionListAdapter.ItemClickListener itemClickListener) {
        this.f$0 = contactViewHolder;
        this.f$1 = itemClickListener;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$new$0(this.f$1, view);
    }
}
