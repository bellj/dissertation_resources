package org.thoughtcrime.securesms.contacts.paged;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Lambda;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchPagedDataSource;

/* compiled from: ContactSearchPagedDataSource.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\f\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchPagedDataSource$StoriesCollection$aggregateStoryData$2 extends Lambda implements Function0<List<? extends ContactSearchData.Story>> {
    final /* synthetic */ Cursor $cursor;
    final /* synthetic */ Function1<Cursor, ContactSearchData> $cursorMapper;
    final /* synthetic */ List<ContactSearchData> $extraData;
    final /* synthetic */ ContactSearchConfiguration.Section $section;
    final /* synthetic */ ContactSearchPagedDataSource.StoriesCollection this$0;

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData> */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super android.database.Cursor, ? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchData> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ContactSearchPagedDataSource$StoriesCollection$aggregateStoryData$2(ContactSearchConfiguration.Section section, Cursor cursor, List<? extends ContactSearchData> list, ContactSearchPagedDataSource.StoriesCollection storiesCollection, Function1<? super Cursor, ? extends ContactSearchData> function1) {
        super(0);
        this.$section = section;
        this.$cursor = cursor;
        this.$extraData = list;
        this.this$0 = storiesCollection;
        this.$cursorMapper = function1;
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends ContactSearchData.Story> invoke() {
        if (this.$section instanceof ContactSearchConfiguration.Section.Stories) {
            IntRange intRange = RangesKt___RangesKt.until(0, this.$cursor.getCount());
            Cursor cursor = this.$cursor;
            Function1<Cursor, ContactSearchData> function1 = this.$cursorMapper;
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange, 10));
            Iterator<Integer> it = intRange.iterator();
            while (it.hasNext()) {
                cursor.moveToPosition(((IntIterator) it).nextInt());
                arrayList.add(function1.invoke(cursor));
            }
            return CollectionsKt___CollectionsKt.sortedWith(CollectionsKt___CollectionsJvmKt.filterIsInstance(CollectionsKt___CollectionsKt.plus((Collection) arrayList, (Iterable) this.$extraData), ContactSearchData.Story.class), this.this$0.getStoryComparator());
        }
        throw new IllegalStateException("Aggregate data creation is only necessary for stories.".toString());
    }
}
