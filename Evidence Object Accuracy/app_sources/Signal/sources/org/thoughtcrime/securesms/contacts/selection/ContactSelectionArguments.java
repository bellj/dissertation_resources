package org.thoughtcrime.securesms.contacts.selection;

import android.os.Bundle;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ContactSelectionArguments.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u0000 42\u00020\u0001:\u00014B{\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\b\b\u0002\u0010\f\u001a\u00020\u0005\u0012\b\b\u0002\u0010\r\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0003¢\u0006\u0002\u0010\u0012J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0005HÆ\u0003J\t\u0010#\u001a\u00020\u0003HÆ\u0003J\t\u0010$\u001a\u00020\u0005HÆ\u0003J\t\u0010%\u001a\u00020\u0005HÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000f\u0010'\u001a\b\u0012\u0004\u0012\u00020\u000b0\nHÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003J\t\u0010)\u001a\u00020\u0005HÆ\u0003J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\t\u0010+\u001a\u00020\u0003HÆ\u0003J\u0010,\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00032\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u0003HÆ\u0001J\u0013\u0010-\u001a\u00020\u00052\b\u0010.\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010/\u001a\u00020\u0003HÖ\u0001J\u0006\u00100\u001a\u000201J\t\u00102\u001a\u000203HÖ\u0001R\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0011\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u000e\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0014R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0014R\u0011\u0010\u0010\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0014R\u0011\u0010\u000f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0016R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 ¨\u00065"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/selection/ContactSelectionArguments;", "", "displayMode", "", "isRefreshable", "", "displayRecents", "selectionLimits", "Lorg/thoughtcrime/securesms/groups/SelectionLimits;", "currentSelection", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "displaySelectionCount", "canSelectSelf", "displayChips", "recyclerPadBottom", "recyclerChildClipping", "checkboxResource", "(IZZLorg/thoughtcrime/securesms/groups/SelectionLimits;Ljava/util/List;ZZZIZI)V", "getCanSelectSelf", "()Z", "getCheckboxResource", "()I", "getCurrentSelection", "()Ljava/util/List;", "getDisplayChips", "getDisplayMode", "getDisplayRecents", "getDisplaySelectionCount", "getRecyclerChildClipping", "getRecyclerPadBottom", "getSelectionLimits", "()Lorg/thoughtcrime/securesms/groups/SelectionLimits;", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toArgumentBundle", "Landroid/os/Bundle;", "toString", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSelectionArguments {
    public static final String CAN_SELECT_SELF;
    public static final String CHECKBOX_RESOURCE;
    public static final String CURRENT_SELECTION;
    public static final Companion Companion = new Companion(null);
    public static final String DISPLAY_CHIPS;
    public static final String DISPLAY_MODE;
    public static final String HIDE_COUNT;
    public static final String RECENTS;
    public static final String REFRESHABLE;
    public static final String RV_CLIP;
    public static final String RV_PADDING_BOTTOM;
    public static final String SELECTION_LIMITS;
    private final boolean canSelectSelf;
    private final int checkboxResource;
    private final List<RecipientId> currentSelection;
    private final boolean displayChips;
    private final int displayMode;
    private final boolean displayRecents;
    private final boolean displaySelectionCount;
    private final boolean isRefreshable;
    private final boolean recyclerChildClipping;
    private final int recyclerPadBottom;
    private final SelectionLimits selectionLimits;

    public ContactSelectionArguments() {
        this(0, false, false, null, null, false, false, false, 0, false, 0, 2047, null);
    }

    public final int component1() {
        return this.displayMode;
    }

    public final boolean component10() {
        return this.recyclerChildClipping;
    }

    public final int component11() {
        return this.checkboxResource;
    }

    public final boolean component2() {
        return this.isRefreshable;
    }

    public final boolean component3() {
        return this.displayRecents;
    }

    public final SelectionLimits component4() {
        return this.selectionLimits;
    }

    public final List<RecipientId> component5() {
        return this.currentSelection;
    }

    public final boolean component6() {
        return this.displaySelectionCount;
    }

    public final boolean component7() {
        return this.canSelectSelf;
    }

    public final boolean component8() {
        return this.displayChips;
    }

    public final int component9() {
        return this.recyclerPadBottom;
    }

    public final ContactSelectionArguments copy(int i, boolean z, boolean z2, SelectionLimits selectionLimits, List<? extends RecipientId> list, boolean z3, boolean z4, boolean z5, int i2, boolean z6, int i3) {
        Intrinsics.checkNotNullParameter(list, "currentSelection");
        return new ContactSelectionArguments(i, z, z2, selectionLimits, list, z3, z4, z5, i2, z6, i3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ContactSelectionArguments)) {
            return false;
        }
        ContactSelectionArguments contactSelectionArguments = (ContactSelectionArguments) obj;
        return this.displayMode == contactSelectionArguments.displayMode && this.isRefreshable == contactSelectionArguments.isRefreshable && this.displayRecents == contactSelectionArguments.displayRecents && Intrinsics.areEqual(this.selectionLimits, contactSelectionArguments.selectionLimits) && Intrinsics.areEqual(this.currentSelection, contactSelectionArguments.currentSelection) && this.displaySelectionCount == contactSelectionArguments.displaySelectionCount && this.canSelectSelf == contactSelectionArguments.canSelectSelf && this.displayChips == contactSelectionArguments.displayChips && this.recyclerPadBottom == contactSelectionArguments.recyclerPadBottom && this.recyclerChildClipping == contactSelectionArguments.recyclerChildClipping && this.checkboxResource == contactSelectionArguments.checkboxResource;
    }

    public int hashCode() {
        int i = this.displayMode * 31;
        boolean z = this.isRefreshable;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (i + i3) * 31;
        boolean z2 = this.displayRecents;
        if (z2) {
            z2 = true;
        }
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = z2 ? 1 : 0;
        int i10 = (i6 + i7) * 31;
        SelectionLimits selectionLimits = this.selectionLimits;
        int hashCode = (((i10 + (selectionLimits == null ? 0 : selectionLimits.hashCode())) * 31) + this.currentSelection.hashCode()) * 31;
        boolean z3 = this.displaySelectionCount;
        if (z3) {
            z3 = true;
        }
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = z3 ? 1 : 0;
        int i14 = (hashCode + i11) * 31;
        boolean z4 = this.canSelectSelf;
        if (z4) {
            z4 = true;
        }
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = z4 ? 1 : 0;
        int i18 = (i14 + i15) * 31;
        boolean z5 = this.displayChips;
        if (z5) {
            z5 = true;
        }
        int i19 = z5 ? 1 : 0;
        int i20 = z5 ? 1 : 0;
        int i21 = z5 ? 1 : 0;
        int i22 = (((i18 + i19) * 31) + this.recyclerPadBottom) * 31;
        boolean z6 = this.recyclerChildClipping;
        if (!z6) {
            i2 = z6 ? 1 : 0;
        }
        return ((i22 + i2) * 31) + this.checkboxResource;
    }

    public String toString() {
        return "ContactSelectionArguments(displayMode=" + this.displayMode + ", isRefreshable=" + this.isRefreshable + ", displayRecents=" + this.displayRecents + ", selectionLimits=" + this.selectionLimits + ", currentSelection=" + this.currentSelection + ", displaySelectionCount=" + this.displaySelectionCount + ", canSelectSelf=" + this.canSelectSelf + ", displayChips=" + this.displayChips + ", recyclerPadBottom=" + this.recyclerPadBottom + ", recyclerChildClipping=" + this.recyclerChildClipping + ", checkboxResource=" + this.checkboxResource + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public ContactSelectionArguments(int i, boolean z, boolean z2, SelectionLimits selectionLimits, List<? extends RecipientId> list, boolean z3, boolean z4, boolean z5, int i2, boolean z6, int i3) {
        Intrinsics.checkNotNullParameter(list, "currentSelection");
        this.displayMode = i;
        this.isRefreshable = z;
        this.displayRecents = z2;
        this.selectionLimits = selectionLimits;
        this.currentSelection = list;
        this.displaySelectionCount = z3;
        this.canSelectSelf = z4;
        this.displayChips = z5;
        this.recyclerPadBottom = i2;
        this.recyclerChildClipping = z6;
        this.checkboxResource = i3;
    }

    public final int getDisplayMode() {
        return this.displayMode;
    }

    public final boolean isRefreshable() {
        return this.isRefreshable;
    }

    public final boolean getDisplayRecents() {
        return this.displayRecents;
    }

    public final SelectionLimits getSelectionLimits() {
        return this.selectionLimits;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ContactSelectionArguments(int r13, boolean r14, boolean r15, org.thoughtcrime.securesms.groups.SelectionLimits r16, java.util.List r17, boolean r18, boolean r19, boolean r20, int r21, boolean r22, int r23, int r24, kotlin.jvm.internal.DefaultConstructorMarker r25) {
        /*
            r12 = this;
            r0 = r24
            r1 = r0 & 1
            if (r1 == 0) goto L_0x0009
            r1 = 31
            goto L_0x000a
        L_0x0009:
            r1 = r13
        L_0x000a:
            r2 = r0 & 2
            r3 = 1
            if (r2 == 0) goto L_0x0011
            r2 = 1
            goto L_0x0012
        L_0x0011:
            r2 = r14
        L_0x0012:
            r4 = r0 & 4
            r5 = 0
            if (r4 == 0) goto L_0x0019
            r4 = 0
            goto L_0x001a
        L_0x0019:
            r4 = r15
        L_0x001a:
            r6 = r0 & 8
            if (r6 == 0) goto L_0x0020
            r6 = 0
            goto L_0x0022
        L_0x0020:
            r6 = r16
        L_0x0022:
            r7 = r0 & 16
            if (r7 == 0) goto L_0x002b
            java.util.List r7 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x002d
        L_0x002b:
            r7 = r17
        L_0x002d:
            r8 = r0 & 32
            if (r8 == 0) goto L_0x0033
            r8 = 1
            goto L_0x0035
        L_0x0033:
            r8 = r18
        L_0x0035:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x003d
            if (r6 != 0) goto L_0x003f
            r5 = 1
            goto L_0x003f
        L_0x003d:
            r5 = r19
        L_0x003f:
            r9 = r0 & 128(0x80, float:1.794E-43)
            if (r9 == 0) goto L_0x0045
            r9 = 1
            goto L_0x0047
        L_0x0045:
            r9 = r20
        L_0x0047:
            r10 = r0 & 256(0x100, float:3.59E-43)
            if (r10 == 0) goto L_0x004d
            r10 = -1
            goto L_0x004f
        L_0x004d:
            r10 = r21
        L_0x004f:
            r11 = r0 & 512(0x200, float:7.175E-43)
            if (r11 == 0) goto L_0x0054
            goto L_0x0056
        L_0x0054:
            r3 = r22
        L_0x0056:
            r0 = r0 & 1024(0x400, float:1.435E-42)
            if (r0 == 0) goto L_0x005e
            r0 = 2131230911(0x7f0800bf, float:1.8077888E38)
            goto L_0x0060
        L_0x005e:
            r0 = r23
        L_0x0060:
            r13 = r12
            r14 = r1
            r15 = r2
            r16 = r4
            r17 = r6
            r18 = r7
            r19 = r8
            r20 = r5
            r21 = r9
            r22 = r10
            r23 = r3
            r24 = r0
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.contacts.selection.ContactSelectionArguments.<init>(int, boolean, boolean, org.thoughtcrime.securesms.groups.SelectionLimits, java.util.List, boolean, boolean, boolean, int, boolean, int, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<RecipientId> getCurrentSelection() {
        return this.currentSelection;
    }

    public final boolean getDisplaySelectionCount() {
        return this.displaySelectionCount;
    }

    public final boolean getCanSelectSelf() {
        return this.canSelectSelf;
    }

    public final boolean getDisplayChips() {
        return this.displayChips;
    }

    public final int getRecyclerPadBottom() {
        return this.recyclerPadBottom;
    }

    public final boolean getRecyclerChildClipping() {
        return this.recyclerChildClipping;
    }

    public final int getCheckboxResource() {
        return this.checkboxResource;
    }

    public final Bundle toArgumentBundle() {
        Bundle bundle = new Bundle();
        bundle.putInt("display_mode", this.displayMode);
        bundle.putBoolean("refreshable", this.isRefreshable);
        bundle.putBoolean("recents", this.displayRecents);
        bundle.putParcelable("selection_limits", this.selectionLimits);
        bundle.putBoolean("hide_count", !this.displaySelectionCount);
        bundle.putBoolean("can_select_self", this.canSelectSelf);
        bundle.putBoolean("display_chips", this.displayChips);
        bundle.putInt("recycler_view_padding_bottom", this.recyclerPadBottom);
        bundle.putBoolean("recycler_view_clipping", this.recyclerChildClipping);
        bundle.putInt(CHECKBOX_RESOURCE, this.checkboxResource);
        bundle.putParcelableArrayList("current_selection", new ArrayList<>(this.currentSelection));
        return bundle;
    }

    /* compiled from: ContactSelectionArguments.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/selection/ContactSelectionArguments$Companion;", "", "()V", "CAN_SELECT_SELF", "", "CHECKBOX_RESOURCE", "CURRENT_SELECTION", "DISPLAY_CHIPS", "DISPLAY_MODE", "HIDE_COUNT", "RECENTS", "REFRESHABLE", "RV_CLIP", "RV_PADDING_BOTTOM", "SELECTION_LIMITS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
