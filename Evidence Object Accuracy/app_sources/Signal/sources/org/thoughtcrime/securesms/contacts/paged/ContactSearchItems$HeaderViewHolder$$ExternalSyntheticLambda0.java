package org.thoughtcrime.securesms.contacts.paged;

import android.view.View;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchItems$HeaderViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContactSearchItems.HeaderModel f$0;

    public /* synthetic */ ContactSearchItems$HeaderViewHolder$$ExternalSyntheticLambda0(ContactSearchItems.HeaderModel headerModel) {
        this.f$0 = headerModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ContactSearchItems.HeaderViewHolder.m1317bind$lambda0(this.f$0, view);
    }
}
