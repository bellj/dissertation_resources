package org.thoughtcrime.securesms.contacts.paged;

import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;

/* compiled from: ContactSearchState.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B1\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\u0002\u0010\tJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0003J5\u0010\u0012\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0003HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "", "query", "", "expandedSections", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "groupStories", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V", "getExpandedSections", "()Ljava/util/Set;", "getGroupStories", "getQuery", "()Ljava/lang/String;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchState {
    private final Set<ContactSearchConfiguration.SectionKey> expandedSections;
    private final Set<ContactSearchData.Story> groupStories;
    private final String query;

    public ContactSearchState() {
        this(null, null, null, 7, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.contacts.paged.ContactSearchState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ContactSearchState copy$default(ContactSearchState contactSearchState, String str, Set set, Set set2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = contactSearchState.query;
        }
        if ((i & 2) != 0) {
            set = contactSearchState.expandedSections;
        }
        if ((i & 4) != 0) {
            set2 = contactSearchState.groupStories;
        }
        return contactSearchState.copy(str, set, set2);
    }

    public final String component1() {
        return this.query;
    }

    public final Set<ContactSearchConfiguration.SectionKey> component2() {
        return this.expandedSections;
    }

    public final Set<ContactSearchData.Story> component3() {
        return this.groupStories;
    }

    public final ContactSearchState copy(String str, Set<? extends ContactSearchConfiguration.SectionKey> set, Set<ContactSearchData.Story> set2) {
        Intrinsics.checkNotNullParameter(set, "expandedSections");
        Intrinsics.checkNotNullParameter(set2, "groupStories");
        return new ContactSearchState(str, set, set2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ContactSearchState)) {
            return false;
        }
        ContactSearchState contactSearchState = (ContactSearchState) obj;
        return Intrinsics.areEqual(this.query, contactSearchState.query) && Intrinsics.areEqual(this.expandedSections, contactSearchState.expandedSections) && Intrinsics.areEqual(this.groupStories, contactSearchState.groupStories);
    }

    public int hashCode() {
        String str = this.query;
        return ((((str == null ? 0 : str.hashCode()) * 31) + this.expandedSections.hashCode()) * 31) + this.groupStories.hashCode();
    }

    public String toString() {
        return "ContactSearchState(query=" + this.query + ", expandedSections=" + this.expandedSections + ", groupStories=" + this.groupStories + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration$SectionKey> */
    /* JADX WARN: Multi-variable type inference failed */
    public ContactSearchState(String str, Set<? extends ContactSearchConfiguration.SectionKey> set, Set<ContactSearchData.Story> set2) {
        Intrinsics.checkNotNullParameter(set, "expandedSections");
        Intrinsics.checkNotNullParameter(set2, "groupStories");
        this.query = str;
        this.expandedSections = set;
        this.groupStories = set2;
    }

    public final String getQuery() {
        return this.query;
    }

    public /* synthetic */ ContactSearchState(String str, Set set, Set set2, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : str, (i & 2) != 0 ? SetsKt__SetsKt.emptySet() : set, (i & 4) != 0 ? SetsKt__SetsKt.emptySet() : set2);
    }

    public final Set<ContactSearchConfiguration.SectionKey> getExpandedSections() {
        return this.expandedSections;
    }

    public final Set<ContactSearchData.Story> getGroupStories() {
        return this.groupStories;
    }
}
