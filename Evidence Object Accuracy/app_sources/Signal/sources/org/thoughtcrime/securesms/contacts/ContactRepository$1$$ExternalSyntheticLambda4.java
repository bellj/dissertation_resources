package org.thoughtcrime.securesms.contacts;

import android.database.Cursor;
import org.signal.core.util.CursorUtil;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.RecipientDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactRepository$1$$ExternalSyntheticLambda4 implements ContactRepository.ValueMapper {
    @Override // org.thoughtcrime.securesms.contacts.ContactRepository.ValueMapper
    public final Object get(Cursor cursor) {
        return CursorUtil.requireString(cursor, RecipientDatabase.SYSTEM_PHONE_LABEL);
    }
}
