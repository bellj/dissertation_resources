package org.thoughtcrime.securesms.contacts.paged;

import android.database.Cursor;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: ContactSearchPagedDataSource.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "it", "Landroid/database/Cursor;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchPagedDataSource$getSectionSize$1$collection$1 extends Lambda implements Function1<Cursor, ContactSearchData> {
    public static final ContactSearchPagedDataSource$getSectionSize$1$collection$1 INSTANCE = new ContactSearchPagedDataSource$getSectionSize$1$collection$1();

    ContactSearchPagedDataSource$getSectionSize$1$collection$1() {
        super(1);
    }

    public final ContactSearchData invoke(Cursor cursor) {
        Intrinsics.checkNotNullParameter(cursor, "it");
        throw new IllegalStateException("Unsupported".toString());
    }
}
