package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class FallbackPhoto80dp implements FallbackContactPhoto {
    private final AvatarColor color;
    private final int drawable80dp;

    public FallbackPhoto80dp(int i, AvatarColor avatarColor) {
        this.drawable80dp = i;
        this.color = avatarColor;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor) {
        return buildDrawable(context);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return buildDrawable(context);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asSmallDrawable(Context context, AvatarColor avatarColor, boolean z) {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asCallCard(Context context) {
        ColorDrawable colorDrawable = new ColorDrawable(this.color.colorInt());
        Drawable drawable = AppCompatResources.getDrawable(context, this.drawable80dp);
        Objects.requireNonNull(drawable);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{colorDrawable, drawable});
        int dpToPx = ViewUtil.dpToPx(24);
        DrawableCompat.setTint(drawable, Avatars.getForegroundColor(this.color).getColorInt());
        layerDrawable.setLayerInset(1, dpToPx, dpToPx, dpToPx, dpToPx);
        return layerDrawable;
    }

    private Drawable buildDrawable(Context context) {
        Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.circle_tintable);
        Objects.requireNonNull(drawable);
        Drawable mutate = DrawableCompat.wrap(drawable).mutate();
        Drawable drawable2 = AppCompatResources.getDrawable(context, this.drawable80dp);
        Objects.requireNonNull(drawable2);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{mutate, drawable2});
        int dpToPx = ViewUtil.dpToPx(24);
        DrawableCompat.setTint(mutate, this.color.colorInt());
        DrawableCompat.setTint(drawable2, Avatars.getForegroundColor(this.color).getColorInt());
        layerDrawable.setLayerInset(1, dpToPx, dpToPx, dpToPx, dpToPx);
        return layerDrawable;
    }
}
