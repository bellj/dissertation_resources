package org.thoughtcrime.securesms.contacts.paged;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.contacts.HeaderAction;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;

/* compiled from: ContactSearchConfiguration.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 \r2\u00020\u0001:\u0007\f\r\u000e\u000f\u0010\u0011\u0012B\u001f\b\u0002\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "", "query", "", "sections", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "(Ljava/lang/String;Ljava/util/List;)V", "getQuery", "()Ljava/lang/String;", "getSections", "()Ljava/util/List;", "Builder", "Companion", "ConfigurationBuilder", "ExpandConfig", "Section", "SectionKey", "TransportType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchConfiguration {
    public static final Companion Companion = new Companion(null);
    private final String query;
    private final List<Section> sections;

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&R\u001a\u0010\u0002\u001a\u0004\u0018\u00010\u0003X¦\u000e¢\u0006\f\u001a\u0004\b\u0004\u0010\u0005\"\u0004\b\u0006\u0010\u0007¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Builder;", "", "query", "", "getQuery", "()Ljava/lang/String;", "setQuery", "(Ljava/lang/String;)V", "addSection", "", "section", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Builder {
        void addSection(Section section);

        String getQuery();

        void setQuery(String str);
    }

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "", "(Ljava/lang/String;I)V", "STORIES", "RECENTS", "INDIVIDUALS", "GROUPS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum SectionKey {
        STORIES,
        RECENTS,
        INDIVIDUALS,
        GROUPS
    }

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$TransportType;", "", "(Ljava/lang/String;I)V", "PUSH", "SMS", "ALL", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum TransportType {
        PUSH,
        SMS,
        ALL
    }

    public /* synthetic */ ContactSearchConfiguration(String str, List list, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, list);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration$Section> */
    /* JADX WARN: Multi-variable type inference failed */
    private ContactSearchConfiguration(String str, List<? extends Section> list) {
        this.query = str;
        this.sections = list;
    }

    public final String getQuery() {
        return this.query;
    }

    public final List<Section> getSections() {
        return this.sections;
    }

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0013\u0014\u0015\u0016B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0005\u001a\u0004\u0018\u00010\u0006X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0012\u0010\r\u001a\u00020\u000eX¦\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u0001\u0004\u0017\u0018\u0019\u001a¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "", "sectionKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;)V", "expandConfig", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "getExpandConfig", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "headerAction", "Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "getHeaderAction", "()Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "includeHeader", "", "getIncludeHeader", "()Z", "getSectionKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "Groups", "Individuals", RecentEmojiPageModel.KEY, "Stories", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Stories;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Individuals;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Groups;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Section {
        private final HeaderAction headerAction;
        private final SectionKey sectionKey;

        public /* synthetic */ Section(SectionKey sectionKey, DefaultConstructorMarker defaultConstructorMarker) {
            this(sectionKey);
        }

        public abstract ExpandConfig getExpandConfig();

        public abstract boolean getIncludeHeader();

        private Section(SectionKey sectionKey) {
            this.sectionKey = sectionKey;
        }

        public final SectionKey getSectionKey() {
            return this.sectionKey;
        }

        public HeaderAction getHeaderAction() {
            return this.headerAction;
        }

        /* compiled from: ContactSearchConfiguration.kt */
        @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B5\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n¢\u0006\u0002\u0010\u000bJ\u000f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\bHÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\nHÆ\u0003J;\u0010\u0018\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\nHÆ\u0001J\u0013\u0010\u0019\u001a\u00020\u00062\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bHÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001R\u0016\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Stories;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "groupStories", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "includeHeader", "", "headerAction", "Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "expandConfig", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "(Ljava/util/Set;ZLorg/thoughtcrime/securesms/contacts/HeaderAction;Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;)V", "getExpandConfig", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "getGroupStories", "()Ljava/util/Set;", "getHeaderAction", "()Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "getIncludeHeader", "()Z", "component1", "component2", "component3", "component4", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Stories extends Section {
            private final ExpandConfig expandConfig;
            private final Set<ContactSearchData.Story> groupStories;
            private final HeaderAction headerAction;
            private final boolean includeHeader;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration$Section$Stories */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Stories copy$default(Stories stories, Set set, boolean z, HeaderAction headerAction, ExpandConfig expandConfig, int i, Object obj) {
                if ((i & 1) != 0) {
                    set = stories.groupStories;
                }
                if ((i & 2) != 0) {
                    z = stories.getIncludeHeader();
                }
                if ((i & 4) != 0) {
                    headerAction = stories.getHeaderAction();
                }
                if ((i & 8) != 0) {
                    expandConfig = stories.getExpandConfig();
                }
                return stories.copy(set, z, headerAction, expandConfig);
            }

            public final Set<ContactSearchData.Story> component1() {
                return this.groupStories;
            }

            public final boolean component2() {
                return getIncludeHeader();
            }

            public final HeaderAction component3() {
                return getHeaderAction();
            }

            public final ExpandConfig component4() {
                return getExpandConfig();
            }

            public final Stories copy(Set<ContactSearchData.Story> set, boolean z, HeaderAction headerAction, ExpandConfig expandConfig) {
                Intrinsics.checkNotNullParameter(set, "groupStories");
                return new Stories(set, z, headerAction, expandConfig);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Stories)) {
                    return false;
                }
                Stories stories = (Stories) obj;
                return Intrinsics.areEqual(this.groupStories, stories.groupStories) && getIncludeHeader() == stories.getIncludeHeader() && Intrinsics.areEqual(getHeaderAction(), stories.getHeaderAction()) && Intrinsics.areEqual(getExpandConfig(), stories.getExpandConfig());
            }

            public int hashCode() {
                int hashCode = this.groupStories.hashCode() * 31;
                boolean includeHeader = getIncludeHeader();
                if (includeHeader) {
                    includeHeader = true;
                }
                int i = includeHeader ? 1 : 0;
                int i2 = includeHeader ? 1 : 0;
                int i3 = includeHeader ? 1 : 0;
                int i4 = 0;
                int hashCode2 = (((hashCode + i) * 31) + (getHeaderAction() == null ? 0 : getHeaderAction().hashCode())) * 31;
                if (getExpandConfig() != null) {
                    i4 = getExpandConfig().hashCode();
                }
                return hashCode2 + i4;
            }

            public String toString() {
                return "Stories(groupStories=" + this.groupStories + ", includeHeader=" + getIncludeHeader() + ", headerAction=" + getHeaderAction() + ", expandConfig=" + getExpandConfig() + ')';
            }

            public /* synthetic */ Stories(Set set, boolean z, HeaderAction headerAction, ExpandConfig expandConfig, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? SetsKt__SetsKt.emptySet() : set, z, (i & 4) != 0 ? null : headerAction, (i & 8) != 0 ? null : expandConfig);
            }

            public final Set<ContactSearchData.Story> getGroupStories() {
                return this.groupStories;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public boolean getIncludeHeader() {
                return this.includeHeader;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public HeaderAction getHeaderAction() {
                return this.headerAction;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public ExpandConfig getExpandConfig() {
                return this.expandConfig;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Stories(Set<ContactSearchData.Story> set, boolean z, HeaderAction headerAction, ExpandConfig expandConfig) {
                super(SectionKey.STORIES, null);
                Intrinsics.checkNotNullParameter(set, "groupStories");
                this.groupStories = set;
                this.includeHeader = z;
                this.headerAction = headerAction;
                this.expandConfig = expandConfig;
            }
        }

        /* compiled from: ContactSearchConfiguration.kt */
        @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001*BU\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\u0007\u0012\b\b\u0002\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0007\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r¢\u0006\u0002\u0010\u000eJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0007HÆ\u0003J\t\u0010 \u001a\u00020\u0007HÆ\u0003J\t\u0010!\u001a\u00020\u0007HÆ\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\rHÆ\u0003J[\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00072\b\b\u0002\u0010\t\u001a\u00020\u00072\b\b\u0002\u0010\n\u001a\u00020\u00072\b\b\u0002\u0010\u000b\u001a\u00020\u00072\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\rHÆ\u0001J\u0013\u0010$\u001a\u00020\u00072\b\u0010%\u001a\u0004\u0018\u00010&HÖ\u0003J\t\u0010'\u001a\u00020\u0003HÖ\u0001J\t\u0010(\u001a\u00020)HÖ\u0001R\u0016\u0010\f\u001a\u0004\u0018\u00010\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u000b\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0012R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0012R\u0011\u0010\n\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0012R\u0011\u0010\t\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001a¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "limit", "", "mode", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents$Mode;", "includeInactiveGroups", "", "includeGroupsV1", "includeSms", "includeSelf", "includeHeader", "expandConfig", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "(ILorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents$Mode;ZZZZZLorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;)V", "getExpandConfig", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "getIncludeGroupsV1", "()Z", "getIncludeHeader", "getIncludeInactiveGroups", "getIncludeSelf", "getIncludeSms", "getLimit", "()I", "getMode", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents$Mode;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "other", "", "hashCode", "toString", "", "Mode", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Recents extends Section {
            private final ExpandConfig expandConfig;
            private final boolean includeGroupsV1;
            private final boolean includeHeader;
            private final boolean includeInactiveGroups;
            private final boolean includeSelf;
            private final boolean includeSms;
            private final int limit;
            private final Mode mode;

            /* compiled from: ContactSearchConfiguration.kt */
            @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Recents$Mode;", "", "(Ljava/lang/String;I)V", "INDIVIDUALS", "GROUPS", "ALL", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes4.dex */
            public enum Mode {
                INDIVIDUALS,
                GROUPS,
                ALL
            }

            public final int component1() {
                return this.limit;
            }

            public final Mode component2() {
                return this.mode;
            }

            public final boolean component3() {
                return this.includeInactiveGroups;
            }

            public final boolean component4() {
                return this.includeGroupsV1;
            }

            public final boolean component5() {
                return this.includeSms;
            }

            public final boolean component6() {
                return this.includeSelf;
            }

            public final boolean component7() {
                return getIncludeHeader();
            }

            public final ExpandConfig component8() {
                return getExpandConfig();
            }

            public final Recents copy(int i, Mode mode, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig) {
                Intrinsics.checkNotNullParameter(mode, "mode");
                return new Recents(i, mode, z, z2, z3, z4, z5, expandConfig);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Recents)) {
                    return false;
                }
                Recents recents = (Recents) obj;
                return this.limit == recents.limit && this.mode == recents.mode && this.includeInactiveGroups == recents.includeInactiveGroups && this.includeGroupsV1 == recents.includeGroupsV1 && this.includeSms == recents.includeSms && this.includeSelf == recents.includeSelf && getIncludeHeader() == recents.getIncludeHeader() && Intrinsics.areEqual(getExpandConfig(), recents.getExpandConfig());
            }

            public int hashCode() {
                int hashCode = ((this.limit * 31) + this.mode.hashCode()) * 31;
                boolean z = this.includeInactiveGroups;
                int i = 1;
                if (z) {
                    z = true;
                }
                int i2 = z ? 1 : 0;
                int i3 = z ? 1 : 0;
                int i4 = z ? 1 : 0;
                int i5 = (hashCode + i2) * 31;
                boolean z2 = this.includeGroupsV1;
                if (z2) {
                    z2 = true;
                }
                int i6 = z2 ? 1 : 0;
                int i7 = z2 ? 1 : 0;
                int i8 = z2 ? 1 : 0;
                int i9 = (i5 + i6) * 31;
                boolean z3 = this.includeSms;
                if (z3) {
                    z3 = true;
                }
                int i10 = z3 ? 1 : 0;
                int i11 = z3 ? 1 : 0;
                int i12 = z3 ? 1 : 0;
                int i13 = (i9 + i10) * 31;
                boolean z4 = this.includeSelf;
                if (z4) {
                    z4 = true;
                }
                int i14 = z4 ? 1 : 0;
                int i15 = z4 ? 1 : 0;
                int i16 = z4 ? 1 : 0;
                int i17 = (i13 + i14) * 31;
                boolean includeHeader = getIncludeHeader();
                if (!includeHeader) {
                    i = includeHeader;
                }
                return ((i17 + i) * 31) + (getExpandConfig() == null ? 0 : getExpandConfig().hashCode());
            }

            public String toString() {
                return "Recents(limit=" + this.limit + ", mode=" + this.mode + ", includeInactiveGroups=" + this.includeInactiveGroups + ", includeGroupsV1=" + this.includeGroupsV1 + ", includeSms=" + this.includeSms + ", includeSelf=" + this.includeSelf + ", includeHeader=" + getIncludeHeader() + ", expandConfig=" + getExpandConfig() + ')';
            }

            public final int getLimit() {
                return this.limit;
            }

            public /* synthetic */ Recents(int i, Mode mode, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig, int i2, DefaultConstructorMarker defaultConstructorMarker) {
                this((i2 & 1) != 0 ? 25 : i, (i2 & 2) != 0 ? Mode.ALL : mode, (i2 & 4) != 0 ? false : z, (i2 & 8) != 0 ? false : z2, (i2 & 16) != 0 ? false : z3, (i2 & 32) != 0 ? false : z4, z5, (i2 & 128) != 0 ? null : expandConfig);
            }

            public final Mode getMode() {
                return this.mode;
            }

            public final boolean getIncludeInactiveGroups() {
                return this.includeInactiveGroups;
            }

            public final boolean getIncludeGroupsV1() {
                return this.includeGroupsV1;
            }

            public final boolean getIncludeSms() {
                return this.includeSms;
            }

            public final boolean getIncludeSelf() {
                return this.includeSelf;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public boolean getIncludeHeader() {
                return this.includeHeader;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public ExpandConfig getExpandConfig() {
                return this.expandConfig;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Recents(int i, Mode mode, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig) {
                super(SectionKey.RECENTS, null);
                Intrinsics.checkNotNullParameter(mode, "mode");
                this.limit = i;
                this.mode = mode;
                this.includeInactiveGroups = z;
                this.includeGroupsV1 = z2;
                this.includeSms = z3;
                this.includeSelf = z4;
                this.includeHeader = z5;
                this.expandConfig = expandConfig;
            }
        }

        /* compiled from: ContactSearchConfiguration.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\bHÆ\u0003J3\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00032\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\u0006\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Individuals;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "includeSelf", "", "transportType", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$TransportType;", "includeHeader", "expandConfig", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "(ZLorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$TransportType;ZLorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;)V", "getExpandConfig", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "getIncludeHeader", "()Z", "getIncludeSelf", "getTransportType", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$TransportType;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Individuals extends Section {
            private final ExpandConfig expandConfig;
            private final boolean includeHeader;
            private final boolean includeSelf;
            private final TransportType transportType;

            public static /* synthetic */ Individuals copy$default(Individuals individuals, boolean z, TransportType transportType, boolean z2, ExpandConfig expandConfig, int i, Object obj) {
                if ((i & 1) != 0) {
                    z = individuals.includeSelf;
                }
                if ((i & 2) != 0) {
                    transportType = individuals.transportType;
                }
                if ((i & 4) != 0) {
                    z2 = individuals.getIncludeHeader();
                }
                if ((i & 8) != 0) {
                    expandConfig = individuals.getExpandConfig();
                }
                return individuals.copy(z, transportType, z2, expandConfig);
            }

            public final boolean component1() {
                return this.includeSelf;
            }

            public final TransportType component2() {
                return this.transportType;
            }

            public final boolean component3() {
                return getIncludeHeader();
            }

            public final ExpandConfig component4() {
                return getExpandConfig();
            }

            public final Individuals copy(boolean z, TransportType transportType, boolean z2, ExpandConfig expandConfig) {
                Intrinsics.checkNotNullParameter(transportType, "transportType");
                return new Individuals(z, transportType, z2, expandConfig);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Individuals)) {
                    return false;
                }
                Individuals individuals = (Individuals) obj;
                return this.includeSelf == individuals.includeSelf && this.transportType == individuals.transportType && getIncludeHeader() == individuals.getIncludeHeader() && Intrinsics.areEqual(getExpandConfig(), individuals.getExpandConfig());
            }

            public int hashCode() {
                boolean z = this.includeSelf;
                int i = 1;
                if (z) {
                    z = true;
                }
                int i2 = z ? 1 : 0;
                int i3 = z ? 1 : 0;
                int i4 = z ? 1 : 0;
                int hashCode = ((i2 * 31) + this.transportType.hashCode()) * 31;
                boolean includeHeader = getIncludeHeader();
                if (!includeHeader) {
                    i = includeHeader;
                }
                return ((hashCode + i) * 31) + (getExpandConfig() == null ? 0 : getExpandConfig().hashCode());
            }

            public String toString() {
                return "Individuals(includeSelf=" + this.includeSelf + ", transportType=" + this.transportType + ", includeHeader=" + getIncludeHeader() + ", expandConfig=" + getExpandConfig() + ')';
            }

            public /* synthetic */ Individuals(boolean z, TransportType transportType, boolean z2, ExpandConfig expandConfig, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(z, transportType, z2, (i & 8) != 0 ? null : expandConfig);
            }

            public final boolean getIncludeSelf() {
                return this.includeSelf;
            }

            public final TransportType getTransportType() {
                return this.transportType;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public boolean getIncludeHeader() {
                return this.includeHeader;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public ExpandConfig getExpandConfig() {
                return this.expandConfig;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Individuals(boolean z, TransportType transportType, boolean z2, ExpandConfig expandConfig) {
                super(SectionKey.INDIVIDUALS, null);
                Intrinsics.checkNotNullParameter(transportType, "transportType");
                this.includeSelf = z;
                this.transportType = transportType;
                this.includeHeader = z2;
                this.expandConfig = expandConfig;
            }
        }

        /* compiled from: ContactSearchConfiguration.kt */
        @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\tHÆ\u0003JG\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tHÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u00032\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cHÖ\u0003J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001R\u0016\u0010\b\u001a\u0004\u0018\u00010\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0007\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000e¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section$Groups;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "includeMms", "", "includeV1", "includeInactive", "returnAsGroupStories", "includeHeader", "expandConfig", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "(ZZZZZLorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;)V", "getExpandConfig", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "getIncludeHeader", "()Z", "getIncludeInactive", "getIncludeMms", "getIncludeV1", "getReturnAsGroupStories", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Groups extends Section {
            private final ExpandConfig expandConfig;
            private final boolean includeHeader;
            private final boolean includeInactive;
            private final boolean includeMms;
            private final boolean includeV1;
            private final boolean returnAsGroupStories;

            public static /* synthetic */ Groups copy$default(Groups groups, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig, int i, Object obj) {
                if ((i & 1) != 0) {
                    z = groups.includeMms;
                }
                if ((i & 2) != 0) {
                    z2 = groups.includeV1;
                }
                if ((i & 4) != 0) {
                    z3 = groups.includeInactive;
                }
                if ((i & 8) != 0) {
                    z4 = groups.returnAsGroupStories;
                }
                if ((i & 16) != 0) {
                    z5 = groups.getIncludeHeader();
                }
                if ((i & 32) != 0) {
                    expandConfig = groups.getExpandConfig();
                }
                return groups.copy(z, z2, z3, z4, z5, expandConfig);
            }

            public final boolean component1() {
                return this.includeMms;
            }

            public final boolean component2() {
                return this.includeV1;
            }

            public final boolean component3() {
                return this.includeInactive;
            }

            public final boolean component4() {
                return this.returnAsGroupStories;
            }

            public final boolean component5() {
                return getIncludeHeader();
            }

            public final ExpandConfig component6() {
                return getExpandConfig();
            }

            public final Groups copy(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig) {
                return new Groups(z, z2, z3, z4, z5, expandConfig);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Groups)) {
                    return false;
                }
                Groups groups = (Groups) obj;
                return this.includeMms == groups.includeMms && this.includeV1 == groups.includeV1 && this.includeInactive == groups.includeInactive && this.returnAsGroupStories == groups.returnAsGroupStories && getIncludeHeader() == groups.getIncludeHeader() && Intrinsics.areEqual(getExpandConfig(), groups.getExpandConfig());
            }

            public int hashCode() {
                boolean z = this.includeMms;
                int i = 1;
                if (z) {
                    z = true;
                }
                int i2 = z ? 1 : 0;
                int i3 = z ? 1 : 0;
                int i4 = z ? 1 : 0;
                int i5 = i2 * 31;
                boolean z2 = this.includeV1;
                if (z2) {
                    z2 = true;
                }
                int i6 = z2 ? 1 : 0;
                int i7 = z2 ? 1 : 0;
                int i8 = z2 ? 1 : 0;
                int i9 = (i5 + i6) * 31;
                boolean z3 = this.includeInactive;
                if (z3) {
                    z3 = true;
                }
                int i10 = z3 ? 1 : 0;
                int i11 = z3 ? 1 : 0;
                int i12 = z3 ? 1 : 0;
                int i13 = (i9 + i10) * 31;
                boolean z4 = this.returnAsGroupStories;
                if (z4) {
                    z4 = true;
                }
                int i14 = z4 ? 1 : 0;
                int i15 = z4 ? 1 : 0;
                int i16 = z4 ? 1 : 0;
                int i17 = (i13 + i14) * 31;
                boolean includeHeader = getIncludeHeader();
                if (!includeHeader) {
                    i = includeHeader;
                }
                return ((i17 + i) * 31) + (getExpandConfig() == null ? 0 : getExpandConfig().hashCode());
            }

            public String toString() {
                return "Groups(includeMms=" + this.includeMms + ", includeV1=" + this.includeV1 + ", includeInactive=" + this.includeInactive + ", returnAsGroupStories=" + this.returnAsGroupStories + ", includeHeader=" + getIncludeHeader() + ", expandConfig=" + getExpandConfig() + ')';
            }

            public /* synthetic */ Groups(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? false : z4, z5, (i & 32) != 0 ? null : expandConfig);
            }

            public final boolean getIncludeMms() {
                return this.includeMms;
            }

            public final boolean getIncludeV1() {
                return this.includeV1;
            }

            public final boolean getIncludeInactive() {
                return this.includeInactive;
            }

            public final boolean getReturnAsGroupStories() {
                return this.returnAsGroupStories;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public boolean getIncludeHeader() {
                return this.includeHeader;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Section
            public ExpandConfig getExpandConfig() {
                return this.expandConfig;
            }

            public Groups(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, ExpandConfig expandConfig) {
                super(SectionKey.GROUPS, null);
                this.includeMms = z;
                this.includeV1 = z2;
                this.includeInactive = z3;
                this.returnAsGroupStories = z4;
                this.includeHeader = z5;
                this.expandConfig = expandConfig;
            }
        }
    }

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\bJ\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u0019\u0010\r\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J-\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00032\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0006HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\tR!\u0010\u0004\u001a\u0012\u0012\b\u0012\u00060\u0006j\u0002`\u0007\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ExpandConfig;", "", "isExpanded", "", "maxCountWhenNotExpanded", "Lkotlin/Function1;", "", "Lorg/thoughtcrime/securesms/contacts/paged/ActiveContactCount;", "(ZLkotlin/jvm/functions/Function1;)V", "()Z", "getMaxCountWhenNotExpanded", "()Lkotlin/jvm/functions/Function1;", "component1", "component2", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ExpandConfig {
        private final boolean isExpanded;
        private final Function1<Integer, Integer> maxCountWhenNotExpanded;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration$ExpandConfig */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ExpandConfig copy$default(ExpandConfig expandConfig, boolean z, Function1 function1, int i, Object obj) {
            if ((i & 1) != 0) {
                z = expandConfig.isExpanded;
            }
            if ((i & 2) != 0) {
                function1 = expandConfig.maxCountWhenNotExpanded;
            }
            return expandConfig.copy(z, function1);
        }

        public final boolean component1() {
            return this.isExpanded;
        }

        public final Function1<Integer, Integer> component2() {
            return this.maxCountWhenNotExpanded;
        }

        public final ExpandConfig copy(boolean z, Function1<? super Integer, Integer> function1) {
            Intrinsics.checkNotNullParameter(function1, "maxCountWhenNotExpanded");
            return new ExpandConfig(z, function1);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ExpandConfig)) {
                return false;
            }
            ExpandConfig expandConfig = (ExpandConfig) obj;
            return this.isExpanded == expandConfig.isExpanded && Intrinsics.areEqual(this.maxCountWhenNotExpanded, expandConfig.maxCountWhenNotExpanded);
        }

        public int hashCode() {
            boolean z = this.isExpanded;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (i * 31) + this.maxCountWhenNotExpanded.hashCode();
        }

        public String toString() {
            return "ExpandConfig(isExpanded=" + this.isExpanded + ", maxCountWhenNotExpanded=" + this.maxCountWhenNotExpanded + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Integer, java.lang.Integer> */
        /* JADX WARN: Multi-variable type inference failed */
        public ExpandConfig(boolean z, Function1<? super Integer, Integer> function1) {
            Intrinsics.checkNotNullParameter(function1, "maxCountWhenNotExpanded");
            this.isExpanded = z;
            this.maxCountWhenNotExpanded = function1;
        }

        public final boolean isExpanded() {
            return this.isExpanded;
        }

        public /* synthetic */ ExpandConfig(boolean z, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(z, (i & 2) != 0 ? AnonymousClass1.INSTANCE : function1);
        }

        public final Function1<Integer, Integer> getMaxCountWhenNotExpanded() {
            return this.maxCountWhenNotExpanded;
        }
    }

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(bv = {}, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\b\u0010\tJ\u001a\u0010\u0007\u001a\u00020\u00062\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u00040\u0002¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Companion;", "", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Builder;", "", "builderFunction", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "build", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final ContactSearchConfiguration build(Function1<? super Builder, Unit> function1) {
            Intrinsics.checkNotNullParameter(function1, "builderFunction");
            ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
            function1.invoke(configurationBuilder);
            return configurationBuilder.build();
        }
    }

    /* compiled from: ContactSearchConfiguration.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000bH\u0016J\u0006\u0010\u000f\u001a\u00020\u0010R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$ConfigurationBuilder;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Builder;", "()V", "query", "", "getQuery", "()Ljava/lang/String;", "setQuery", "(Ljava/lang/String;)V", "sections", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$Section;", "addSection", "", "section", "build", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ConfigurationBuilder implements Builder {
        private String query;
        private final List<Section> sections = new ArrayList();

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Builder
        public String getQuery() {
            return this.query;
        }

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Builder
        public void setQuery(String str) {
            this.query = str;
        }

        @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Builder
        public void addSection(Section section) {
            Intrinsics.checkNotNullParameter(section, "section");
            this.sections.add(section);
        }

        public final ContactSearchConfiguration build() {
            return new ContactSearchConfiguration(getQuery(), this.sections, null);
        }
    }
}
