package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.AbstractContactsCursorLoader;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.phonenumbers.NumberUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.UsernameUtil;

/* loaded from: classes4.dex */
public class ContactsCursorLoader extends AbstractContactsCursorLoader {
    private static final int RECENT_CONVERSATION_MAX;
    private static final String TAG = Log.tag(ContactsCursorLoader.class);
    private final ContactRepository contactRepository;
    private final int mode;
    private final boolean recents;

    /* loaded from: classes4.dex */
    public static final class DisplayMode {
        public static final int FLAG_ACTIVE_GROUPS;
        public static final int FLAG_ALL;
        public static final int FLAG_BLOCK;
        public static final int FLAG_GROUPS_AFTER_CONTACTS;
        public static final int FLAG_HIDE_GROUPS_V1;
        public static final int FLAG_HIDE_NEW;
        public static final int FLAG_HIDE_RECENT_HEADER;
        public static final int FLAG_INACTIVE_GROUPS;
        public static final int FLAG_PUSH;
        public static final int FLAG_SELF;
        public static final int FLAG_SMS;
    }

    private static boolean flagSet(int i, int i2) {
        return (i & i2) > 0;
    }

    private static boolean groupsOnly(int i) {
        return i == 4;
    }

    private ContactsCursorLoader(Context context, int i, String str, boolean z) {
        super(context, str);
        if (!flagSet(i, 8) || flagSet(i, 4)) {
            this.mode = i;
            this.recents = z;
            this.contactRepository = new ContactRepository(context, context.getString(R.string.note_to_self));
            return;
        }
        throw new AssertionError("Inactive group flag set, but the active group flag isn't!");
    }

    @Override // org.thoughtcrime.securesms.contacts.AbstractContactsCursorLoader
    protected final List<Cursor> getUnfilteredResults() {
        ArrayList arrayList = new ArrayList();
        if (groupsOnly(this.mode)) {
            addRecentGroupsSection(arrayList);
            addGroupsSection(arrayList);
        } else {
            addRecentsSection(arrayList);
            addContactsSection(arrayList);
            if (addGroupsAfterContacts(this.mode)) {
                addGroupsSection(arrayList);
            }
        }
        return arrayList;
    }

    @Override // org.thoughtcrime.securesms.contacts.AbstractContactsCursorLoader
    protected final List<Cursor> getFilteredResults() {
        ArrayList arrayList = new ArrayList();
        addContactsSection(arrayList);
        addGroupsSection(arrayList);
        if (!hideNewNumberOrUsername(this.mode)) {
            addNewNumberSection(arrayList);
            addUsernameSearchSection(arrayList);
        }
        return arrayList;
    }

    private void addRecentsSection(List<Cursor> list) {
        if (this.recents) {
            Cursor recentConversationsCursor = getRecentConversationsCursor();
            if (recentConversationsCursor.getCount() > 0) {
                if (!hideRecentsHeader(this.mode)) {
                    list.add(ContactsCursorRows.forRecentsHeader(getContext()));
                }
                list.add(recentConversationsCursor);
            }
        }
    }

    private void addContactsSection(List<Cursor> list) {
        List<Cursor> contactsCursors = getContactsCursors();
        if (!isCursorListEmpty(contactsCursors)) {
            if (!getFilter().isEmpty() || this.recents) {
                list.add(ContactsCursorRows.forContactsHeader(getContext()));
            }
            list.addAll(contactsCursors);
        }
    }

    private void addRecentGroupsSection(List<Cursor> list) {
        if (groupsEnabled(this.mode) && this.recents) {
            Cursor recentConversationsCursor = getRecentConversationsCursor(true);
            if (recentConversationsCursor.getCount() > 0) {
                if (!hideRecentsHeader(this.mode)) {
                    list.add(ContactsCursorRows.forRecentsHeader(getContext()));
                }
                list.add(recentConversationsCursor);
            }
        }
    }

    private void addGroupsSection(List<Cursor> list) {
        if (groupsEnabled(this.mode)) {
            Cursor groupsCursor = getGroupsCursor();
            if (groupsCursor.getCount() > 0) {
                list.add(ContactsCursorRows.forGroupsHeader(getContext()));
                list.add(groupsCursor);
            }
        }
    }

    private void addNewNumberSection(List<Cursor> list) {
        if (FeatureFlags.usernames() && NumberUtil.isVisuallyValidNumberOrEmail(getFilter())) {
            list.add(ContactsCursorRows.forPhoneNumberSearchHeader(getContext()));
            list.add(getNewNumberCursor());
        } else if (!FeatureFlags.usernames() && NumberUtil.isValidSmsOrEmail(getFilter())) {
            list.add(ContactsCursorRows.forPhoneNumberSearchHeader(getContext()));
            list.add(getNewNumberCursor());
        }
    }

    private void addUsernameSearchSection(List<Cursor> list) {
        if (FeatureFlags.usernames() && UsernameUtil.isValidUsernameForSearch(getFilter())) {
            list.add(ContactsCursorRows.forUsernameSearchHeader(getContext()));
            list.add(getUsernameSearchCursor());
        }
    }

    private Cursor getRecentConversationsCursor() {
        return getRecentConversationsCursor(false);
    }

    private Cursor getRecentConversationsCursor(boolean z) {
        ThreadDatabase threads = SignalDatabase.threads();
        MatrixCursor createMatrixCursor = ContactsCursorRows.createMatrixCursor(25);
        Cursor recentConversationList = threads.getRecentConversationList(25, flagSet(this.mode, 8), false, z, hideGroupsV1(this.mode), !smsEnabled(this.mode), false);
        try {
            ThreadDatabase.Reader readerFor = threads.readerFor(recentConversationList);
            while (true) {
                ThreadRecord next = readerFor.getNext();
                if (next == null) {
                    break;
                }
                createMatrixCursor.addRow(ContactsCursorRows.forRecipient(getContext(), next.getRecipient()));
            }
            if (recentConversationList != null) {
                recentConversationList.close();
            }
            return createMatrixCursor;
        } catch (Throwable th) {
            if (recentConversationList != null) {
                try {
                    recentConversationList.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private List<Cursor> getContactsCursors() {
        ArrayList arrayList = new ArrayList(2);
        if (pushEnabled(this.mode) && smsEnabled(this.mode)) {
            arrayList.add(this.contactRepository.queryNonGroupContacts(getFilter(), selfEnabled(this.mode)));
        } else if (pushEnabled(this.mode)) {
            arrayList.add(this.contactRepository.querySignalContacts(getFilter(), selfEnabled(this.mode)));
        } else if (smsEnabled(this.mode)) {
            arrayList.add(this.contactRepository.queryNonSignalContacts(getFilter()));
        }
        return arrayList;
    }

    private Cursor getGroupsCursor() {
        MatrixCursor createMatrixCursor = ContactsCursorRows.createMatrixCursor();
        GroupDatabase.Reader queryGroupsByTitle = SignalDatabase.groups().queryGroupsByTitle(getFilter(), flagSet(this.mode, 8), hideGroupsV1(this.mode), !smsEnabled(this.mode));
        while (true) {
            try {
                GroupDatabase.GroupRecord next = queryGroupsByTitle.getNext();
                if (next != null) {
                    createMatrixCursor.addRow(ContactsCursorRows.forGroup(next));
                } else {
                    queryGroupsByTitle.close();
                    return createMatrixCursor;
                }
            } catch (Throwable th) {
                if (queryGroupsByTitle != null) {
                    try {
                        queryGroupsByTitle.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
    }

    private Cursor getNewNumberCursor() {
        return ContactsCursorRows.forNewNumber(getUnknownContactTitle(), getFilter());
    }

    private Cursor getUsernameSearchCursor() {
        return ContactsCursorRows.forUsernameSearch(getUnknownContactTitle(), getFilter());
    }

    private String getUnknownContactTitle() {
        if (blockUser(this.mode)) {
            return getContext().getString(R.string.contact_selection_list__unknown_contact_block);
        }
        if (newConversation(this.mode)) {
            return getContext().getString(R.string.contact_selection_list__unknown_contact);
        }
        return getContext().getString(R.string.contact_selection_list__unknown_contact_add_to_group);
    }

    private static boolean isCursorListEmpty(List<Cursor> list) {
        int i = 0;
        for (Cursor cursor : list) {
            i += cursor.getCount();
        }
        if (i == 0) {
            return true;
        }
        return false;
    }

    private static boolean selfEnabled(int i) {
        return flagSet(i, 16);
    }

    private static boolean blockUser(int i) {
        return flagSet(i, 32);
    }

    private static boolean newConversation(int i) {
        return groupsEnabled(i);
    }

    private static boolean pushEnabled(int i) {
        return flagSet(i, 1);
    }

    private static boolean smsEnabled(int i) {
        return flagSet(i, 2);
    }

    private static boolean groupsEnabled(int i) {
        return flagSet(i, 4);
    }

    private static boolean hideGroupsV1(int i) {
        return flagSet(i, 32);
    }

    private static boolean hideNewNumberOrUsername(int i) {
        return flagSet(i, 64);
    }

    private static boolean hideRecentsHeader(int i) {
        return flagSet(i, 128);
    }

    private static boolean addGroupsAfterContacts(int i) {
        return flagSet(i, 256);
    }

    /* loaded from: classes4.dex */
    public static class Factory implements AbstractContactsCursorLoader.Factory {
        private final Context context;
        private final String cursorFilter;
        private final int displayMode;
        private final boolean displayRecents;

        public Factory(Context context, int i, String str, boolean z) {
            this.context = context;
            this.displayMode = i;
            this.cursorFilter = str;
            this.displayRecents = z;
        }

        @Override // org.thoughtcrime.securesms.contacts.AbstractContactsCursorLoader.Factory
        public AbstractContactsCursorLoader create() {
            return new ContactsCursorLoader(this.context, this.displayMode, this.cursorFilter, this.displayRecents);
        }
    }
}
