package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.MergeCursor;
import android.text.TextUtils;
import androidx.loader.content.CursorLoader;
import java.util.List;

/* loaded from: classes4.dex */
public abstract class AbstractContactsCursorLoader extends CursorLoader {
    private final String filter;

    /* loaded from: classes4.dex */
    public interface Factory {
        AbstractContactsCursorLoader create();
    }

    protected abstract List<Cursor> getFilteredResults();

    protected abstract List<Cursor> getUnfilteredResults();

    public AbstractContactsCursorLoader(Context context, String str) {
        super(context);
        this.filter = sanitizeFilter(str);
    }

    @Override // androidx.loader.content.CursorLoader, androidx.loader.content.AsyncTaskLoader
    public final Cursor loadInBackground() {
        List<Cursor> list;
        if (TextUtils.isEmpty(this.filter)) {
            list = getUnfilteredResults();
        } else {
            list = getFilteredResults();
        }
        if (list.size() > 0) {
            return new MergeCursor((Cursor[]) list.toArray(new Cursor[0]));
        }
        return null;
    }

    public final String getFilter() {
        return this.filter;
    }

    private static String sanitizeFilter(String str) {
        if (str == null) {
            return "";
        }
        return str.startsWith("@") ? str.substring(1) : str;
    }
}
