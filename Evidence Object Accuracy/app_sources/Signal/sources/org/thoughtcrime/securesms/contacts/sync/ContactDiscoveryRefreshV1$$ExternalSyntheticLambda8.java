package org.thoughtcrime.securesms.contacts.sync;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return Recipient.resolved((RecipientId) obj);
    }
}
