package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.net.Uri;
import com.bumptech.glide.load.Key;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;

/* loaded from: classes4.dex */
public interface ContactPhoto extends Key {
    Uri getUri(Context context);

    boolean isProfilePhoto();

    InputStream openInputStream(Context context) throws IOException;

    @Override // com.bumptech.glide.load.Key
    /* synthetic */ void updateDiskCacheKey(MessageDigest messageDigest);
}
