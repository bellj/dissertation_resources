package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes4.dex */
class ContactIdentityManagerICS extends ContactIdentityManager {
    @Override // org.thoughtcrime.securesms.contacts.ContactIdentityManager
    public boolean isSelfIdentityAutoDetected() {
        return true;
    }

    public ContactIdentityManagerICS(Context context) {
        super(context);
    }

    @Override // org.thoughtcrime.securesms.contacts.ContactIdentityManager
    public Uri getSelfIdentityUri() {
        Cursor cursor;
        Throwable th;
        try {
            cursor = this.context.getContentResolver().query(ContactsContract.Profile.CONTENT_URI, new String[]{"display_name", "lookup", "_id"}, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        Uri lookupUri = ContactsContract.Contacts.getLookupUri(cursor.getLong(2), cursor.getString(1));
                        cursor.close();
                        return lookupUri;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    @Override // org.thoughtcrime.securesms.contacts.ContactIdentityManager
    public List<Long> getSelfIdentityRawContactIds() {
        Throwable th;
        LinkedList linkedList = new LinkedList();
        Cursor cursor = null;
        try {
            Cursor query = this.context.getContentResolver().query(ContactsContract.Profile.CONTENT_RAW_CONTACTS_URI, new String[]{"_id"}, null, null, null);
            if (query != null) {
                try {
                    if (query.getCount() != 0) {
                        while (query.moveToNext()) {
                            linkedList.add(Long.valueOf(query.getLong(0)));
                        }
                        query.close();
                        return linkedList;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
        }
    }
}
