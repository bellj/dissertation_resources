package org.thoughtcrime.securesms.contacts;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.ContactsContract;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes4.dex */
public class ContactAccessor {
    private static final ContactAccessor instance = new ContactAccessor();

    public static ContactAccessor getInstance() {
        return instance;
    }

    public ContactData getContactData(Context context, Uri uri) {
        ContactData contactData = new ContactData(Long.parseLong(uri.getLastPathSegment()), getNameFromContact(context, uri));
        ContentResolver contentResolver = context.getContentResolver();
        Uri uri2 = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        Cursor query = contentResolver.query(uri2, null, "contact_id = ?", new String[]{contactData.id + ""}, null);
        while (query != null) {
            try {
                if (!query.moveToNext()) {
                    break;
                }
                int i = query.getInt(query.getColumnIndexOrThrow("data2"));
                String string = query.getString(query.getColumnIndexOrThrow("data3"));
                contactData.numbers.add(new NumberData(ContactsContract.CommonDataKinds.Phone.getTypeLabel(context.getResources(), i, string).toString(), query.getString(query.getColumnIndexOrThrow("data1"))));
            } catch (Throwable th) {
                try {
                    query.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
                throw th;
            }
        }
        if (query != null) {
            query.close();
        }
        return contactData;
    }

    private String getNameFromContact(Context context, Uri uri) {
        Throwable th;
        Cursor cursor = null;
        try {
            Cursor query = context.getContentResolver().query(uri, new String[]{"display_name"}, null, null, null);
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        String string = query.getString(0);
                        query.close();
                        return string;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    cursor = query;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            if (query != null) {
                query.close();
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
        }
    }

    /* loaded from: classes4.dex */
    public static class NumberData implements Parcelable {
        public static final Parcelable.Creator<NumberData> CREATOR = new Parcelable.Creator<NumberData>() { // from class: org.thoughtcrime.securesms.contacts.ContactAccessor.NumberData.1
            @Override // android.os.Parcelable.Creator
            public NumberData createFromParcel(Parcel parcel) {
                return new NumberData(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public NumberData[] newArray(int i) {
                return new NumberData[i];
            }
        };
        public final String number;
        public final String type;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public NumberData(String str, String str2) {
            this.type = str;
            this.number = str2;
        }

        public NumberData(Parcel parcel) {
            this.number = parcel.readString();
            this.type = parcel.readString();
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.number);
            parcel.writeString(this.type);
        }
    }

    /* loaded from: classes4.dex */
    public static class ContactData implements Parcelable {
        public static final Parcelable.Creator<ContactData> CREATOR = new Parcelable.Creator<ContactData>() { // from class: org.thoughtcrime.securesms.contacts.ContactAccessor.ContactData.1
            @Override // android.os.Parcelable.Creator
            public ContactData createFromParcel(Parcel parcel) {
                return new ContactData(parcel);
            }

            @Override // android.os.Parcelable.Creator
            public ContactData[] newArray(int i) {
                return new ContactData[i];
            }
        };
        public final long id;
        public final String name;
        public final List<NumberData> numbers;

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        public ContactData(long j, String str) {
            this.id = j;
            this.name = str;
            this.numbers = new LinkedList();
        }

        public ContactData(Parcel parcel) {
            this.id = parcel.readLong();
            this.name = parcel.readString();
            LinkedList linkedList = new LinkedList();
            this.numbers = linkedList;
            parcel.readTypedList(linkedList, NumberData.CREATOR);
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeLong(this.id);
            parcel.writeString(this.name);
            parcel.writeTypedList(this.numbers);
        }
    }
}
