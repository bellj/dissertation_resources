package org.thoughtcrime.securesms.contacts.paged;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: ContactSearchRepository.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ \u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\r0\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\rJ\u000e\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u0013¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchRepository;", "", "()V", "canSelectRecipient", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "deletePrivateStory", "Lio/reactivex/rxjava3/core/Completable;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "filterOutUnselectableContactSearchKeys", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchSelectionResult;", "contactSearchKeys", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "unmarkDisplayAsStory", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchRepository {
    public final Single<Set<ContactSearchSelectionResult>> filterOutUnselectableContactSearchKeys(Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKeys");
        Single<Set<ContactSearchSelectionResult>> fromCallable = Single.fromCallable(new Callable(set, this) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ ContactSearchRepository f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ContactSearchRepository.m1335filterOutUnselectableContactSearchKeys$lambda1(this.f$0, this.f$1);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n      con…le)\n      }.toSet()\n    }");
        return fromCallable;
    }

    private final boolean canSelectRecipient(RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (!resolved.isPushV2Group()) {
            return true;
        }
        Optional<GroupDatabase.GroupRecord> group = SignalDatabase.Companion.groups().getGroup(resolved.requireGroupId());
        if (!group.isPresent() || !group.get().isAnnouncementGroup() || group.get().isAdmin(Recipient.self())) {
            return true;
        }
        return false;
    }

    public final Completable unmarkDisplayAsStory(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Completable subscribeOn = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                ContactSearchRepository.m1336unmarkDisplayAsStory$lambda2(GroupId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: unmarkDisplayAsStory$lambda-2 */
    public static final void m1336unmarkDisplayAsStory$lambda2(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        SignalDatabase.Companion.groups().markDisplayAsStory(groupId, false);
    }

    public final Completable deletePrivateStory(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
        Completable subscribeOn = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                ContactSearchRepository.m1334deletePrivateStory$lambda3(DistributionListId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: deletePrivateStory$lambda-3 */
    public static final void m1334deletePrivateStory$lambda3(DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionListId");
        DistributionListDatabase.deleteList$default(SignalDatabase.Companion.distributionLists(), distributionListId, 0, 2, null);
        Stories.INSTANCE.onStorySettingsChanged(distributionListId);
    }

    /* renamed from: filterOutUnselectableContactSearchKeys$lambda-1 */
    public static final Set m1335filterOutUnselectableContactSearchKeys$lambda1(Set set, ContactSearchRepository contactSearchRepository) {
        Intrinsics.checkNotNullParameter(set, "$contactSearchKeys");
        Intrinsics.checkNotNullParameter(contactSearchRepository, "this$0");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        Iterator it = set.iterator();
        while (it.hasNext()) {
            ContactSearchKey contactSearchKey = (ContactSearchKey) it.next();
            boolean z = false;
            if (!(contactSearchKey instanceof ContactSearchKey.Expand) && !(contactSearchKey instanceof ContactSearchKey.Header)) {
                if (contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.KnownRecipient) {
                    z = contactSearchRepository.canSelectRecipient(((ContactSearchKey.RecipientSearchKey.KnownRecipient) contactSearchKey).getRecipientId());
                } else if (contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.Story) {
                    z = contactSearchRepository.canSelectRecipient(((ContactSearchKey.RecipientSearchKey.Story) contactSearchKey).getRecipientId());
                } else {
                    throw new NoWhenBranchMatchedException();
                }
            }
            arrayList.add(new ContactSearchSelectionResult(contactSearchKey, z));
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }
}
