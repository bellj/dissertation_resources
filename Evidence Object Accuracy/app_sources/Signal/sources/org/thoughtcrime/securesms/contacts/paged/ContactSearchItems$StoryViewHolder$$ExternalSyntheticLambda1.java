package org.thoughtcrime.securesms.contacts.paged;

import android.view.View;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ ContactSearchItems.StoryModel f$0;
    public final /* synthetic */ ContactSearchItems.StoryViewHolder f$1;

    public /* synthetic */ ContactSearchItems$StoryViewHolder$$ExternalSyntheticLambda1(ContactSearchItems.StoryModel storyModel, ContactSearchItems.StoryViewHolder storyViewHolder) {
        this.f$0 = storyModel;
        this.f$1 = storyViewHolder;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return ContactSearchItems.StoryViewHolder.m1320bindLongPress$lambda0(this.f$0, this.f$1, view);
    }
}
