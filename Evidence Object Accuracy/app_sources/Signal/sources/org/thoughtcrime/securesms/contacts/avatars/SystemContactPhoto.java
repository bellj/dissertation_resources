package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.net.Uri;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.security.MessageDigest;
import org.signal.core.util.Conversions;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class SystemContactPhoto implements ContactPhoto {
    private final Uri contactPhotoUri;
    private final long lastModifiedTime;
    private final RecipientId recipientId;

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public boolean isProfilePhoto() {
        return false;
    }

    public SystemContactPhoto(RecipientId recipientId, Uri uri, long j) {
        this.recipientId = recipientId;
        this.contactPhotoUri = uri;
        this.lastModifiedTime = j;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public InputStream openInputStream(Context context) throws FileNotFoundException {
        return context.getContentResolver().openInputStream(this.contactPhotoUri);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public Uri getUri(Context context) {
        return this.contactPhotoUri;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto, com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(this.recipientId.serialize().getBytes());
        messageDigest.update(this.contactPhotoUri.toString().getBytes());
        messageDigest.update(Conversions.longToByteArray(this.lastModifiedTime));
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof SystemContactPhoto)) {
            return false;
        }
        SystemContactPhoto systemContactPhoto = (SystemContactPhoto) obj;
        if (!this.recipientId.equals(systemContactPhoto.recipientId) || !this.contactPhotoUri.equals(systemContactPhoto.contactPhotoUri) || this.lastModifiedTime != systemContactPhoto.lastModifiedTime) {
            return false;
        }
        return true;
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return (this.recipientId.hashCode() ^ this.contactPhotoUri.hashCode()) ^ ((int) this.lastModifiedTime);
    }
}
