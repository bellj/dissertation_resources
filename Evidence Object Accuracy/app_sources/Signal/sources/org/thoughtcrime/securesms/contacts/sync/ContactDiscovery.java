package org.thoughtcrime.securesms.contacts.sync;

import android.accounts.Account;
import android.content.Context;
import android.content.OperationApplicationException;
import android.os.RemoteException;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.ContactLinkConfiguration;
import org.signal.contacts.SystemContactsRepository;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.profiles.ProfileName;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* compiled from: ContactDiscovery.kt */
@Metadata(d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u00013B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00132\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u000e\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u001a\u001a\u00020\u0011J\u001e\u0010\u001b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010H\u0002J\u001c\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u001e2\u0006\u0010\r\u001a\u00020\u000eH\u0002J&\u0010\u001f\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\"0!2\u0006\u0010#\u001a\u00020\u0013H\u0007J \u0010\u001f\u001a\u00020$2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0013H\u0007J\u0018\u0010&\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010#\u001a\u00020\u0013H\u0007J6\u0010'\u001a\u00020(2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010)\u001a\u00020\u00042\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020(0*2\u0006\u0010+\u001a\u00020\u00132\u0006\u0010#\u001a\u00020\u0013H\u0002J\u0010\u0010,\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007J<\u0010-\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0012\u0010.\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040/2\u000e\b\u0002\u00100\u001a\b\u0012\u0004\u0012\u0002010*2\u0006\u00102\u001a\u00020\u0013H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u00064"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/sync/ContactDiscovery;", "", "()V", "CALL_MIMETYPE", "", "CONTACT_TAG", "FULL_SYSTEM_CONTACT_SYNC_THRESHOLD", "", "MESSAGE_MIMETYPE", "TAG", "kotlin.jvm.PlatformType", "addSystemContactLinks", "", "context", "Landroid/content/Context;", "registeredIds", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "removeIfMissing", "", "buildContactLinkConfiguration", "Lorg/signal/contacts/ContactLinkConfiguration;", "account", "Landroid/accounts/Account;", "hasContactsPermissions", "hasSession", ContactRepository.ID_COLUMN, "notifyNewUsers", "newUserIds", "phoneNumberFormatter", "Lkotlin/Function1;", "refresh", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "notifyOfNewUsers", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$RegisteredState;", RecipientDatabase.TABLE_NAME, "refreshAll", "refreshRecipients", "Lorg/thoughtcrime/securesms/contacts/sync/ContactDiscovery$RefreshResult;", "descriptor", "Lkotlin/Function0;", "removeSystemContactLinksIfMissing", "syncRecipientInfoWithSystemContacts", "syncRecipientsWithSystemContacts", "rewrites", "", "contactsProvider", "Lorg/signal/contacts/SystemContactsRepository$ContactIterator;", "clearInfoForMissingContacts", "RefreshResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactDiscovery {
    private static final String CALL_MIMETYPE;
    private static final String CONTACT_TAG;
    private static final int FULL_SYSTEM_CONTACT_SYNC_THRESHOLD;
    public static final ContactDiscovery INSTANCE = new ContactDiscovery();
    private static final String MESSAGE_MIMETYPE;
    private static final String TAG = Log.tag(ContactDiscovery.class);

    private ContactDiscovery() {
    }

    @JvmStatic
    public static final void refreshAll(Context context, boolean z) throws IOException {
        Intrinsics.checkNotNullParameter(context, "context");
        if (TextUtils.isEmpty(SignalStore.account().getE164())) {
            Log.w(TAG, "Have not yet set our own local number. Skipping.");
            return;
        }
        ContactDiscovery contactDiscovery = INSTANCE;
        if (!contactDiscovery.hasContactsPermissions(context)) {
            Log.w(TAG, "No contact permissions. Skipping.");
        } else if (!SignalStore.registrationValues().isRegistrationComplete()) {
            Log.w(TAG, "Registration is not yet complete. Skipping, but running a routine to possibly mark it complete.");
            RegistrationUtil.maybeMarkRegistrationComplete(context);
        } else {
            contactDiscovery.refreshRecipients(context, "refresh-all", new Function0<RefreshResult>(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$refreshAll$1
                final /* synthetic */ Context $context;

                /* access modifiers changed from: package-private */
                {
                    this.$context = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final ContactDiscovery.RefreshResult invoke() {
                    if (FeatureFlags.phoneNumberPrivacy()) {
                        return ContactDiscoveryRefreshV2.refreshAll(this.$context);
                    }
                    ContactDiscovery.RefreshResult refreshAll = ContactDiscoveryRefreshV1.refreshAll(this.$context);
                    Intrinsics.checkNotNullExpressionValue(refreshAll, "{\n          ContactDisco…eshAll(context)\n        }");
                    return refreshAll;
                }
            }, true, z);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    @JvmStatic
    public static final void refresh(Context context, List<? extends Recipient> list, boolean z) throws IOException {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        INSTANCE.refreshRecipients(context, "refresh-multiple", new Function0<RefreshResult>(context, list) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$refresh$1
            final /* synthetic */ Context $context;
            final /* synthetic */ List<Recipient> $recipients;

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
                this.$recipients = r2;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ContactDiscovery.RefreshResult invoke() {
                if (FeatureFlags.phoneNumberPrivacy()) {
                    return ContactDiscoveryRefreshV2.refresh(this.$context, this.$recipients);
                }
                ContactDiscovery.RefreshResult refresh = ContactDiscoveryRefreshV1.refresh(this.$context, this.$recipients);
                Intrinsics.checkNotNullExpressionValue(refresh, "{\n          ContactDisco…xt, recipients)\n        }");
                return refresh;
            }
        }, false, z);
    }

    @JvmStatic
    public static final RecipientDatabase.RegisteredState refresh(Context context, Recipient recipient, boolean z) throws IOException {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        if (INSTANCE.refreshRecipients(context, "refresh-single", new Function0<RefreshResult>(context, recipient) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$refresh$result$1
            final /* synthetic */ Context $context;
            final /* synthetic */ Recipient $recipient;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
                this.$recipient = r2;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ContactDiscovery.RefreshResult invoke() {
                if (FeatureFlags.phoneNumberPrivacy()) {
                    return ContactDiscoveryRefreshV2.refresh(this.$context, CollectionsKt__CollectionsJVMKt.listOf(this.$recipient));
                }
                ContactDiscovery.RefreshResult refresh = ContactDiscoveryRefreshV1.refresh(this.$context, CollectionsKt__CollectionsJVMKt.listOf(this.$recipient));
                Intrinsics.checkNotNullExpressionValue(refresh, "{\n          ContactDisco…tOf(recipient))\n        }");
                return refresh;
            }
        }, false, z).getRegisteredIds().contains(recipient.getId())) {
            return RecipientDatabase.RegisteredState.REGISTERED;
        }
        return RecipientDatabase.RegisteredState.NOT_REGISTERED;
    }

    @JvmStatic
    public static final void syncRecipientInfoWithSystemContacts(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        ContactDiscovery contactDiscovery = INSTANCE;
        if (!contactDiscovery.hasContactsPermissions(context)) {
            Log.w(TAG, "[syncRecipientInfoWithSystemContacts] No contacts permission, skipping.");
        } else {
            syncRecipientsWithSystemContacts$default(contactDiscovery, context, MapsKt__MapsKt.emptyMap(), null, true, 4, null);
        }
    }

    public final Function1<String, String> phoneNumberFormatter(Context context) {
        return new Function1<String, String>(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$phoneNumberFormatter$1
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
            }

            public final String invoke(String str) {
                Intrinsics.checkNotNullParameter(str, "it");
                String format = PhoneNumberFormatter.get(this.$context).format(str);
                Intrinsics.checkNotNullExpressionValue(format, "get(context).format(it)");
                return format;
            }
        };
    }

    private final RefreshResult refreshRecipients(Context context, String str, Function0<RefreshResult> function0, boolean z, boolean z2) {
        Stopwatch stopwatch = new Stopwatch(str);
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        Set set = CollectionsKt___CollectionsKt.toSet(companion.recipients().getRegistered());
        stopwatch.split("pre-existing");
        RefreshResult invoke = function0.invoke();
        stopwatch.split(CdsDatabase.TABLE_NAME);
        if (hasContactsPermissions(context)) {
            addSystemContactLinks(context, invoke.getRegisteredIds(), z);
            stopwatch.split("contact-links");
            boolean z3 = z && invoke.getRegisteredIds().size() > 3;
            syncRecipientsWithSystemContacts(context, invoke.getRewrites(), new Function0<SystemContactsRepository.ContactIterator>(z3, invoke, context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$refreshRecipients$1
                final /* synthetic */ Context $context;
                final /* synthetic */ ContactDiscovery.RefreshResult $result;
                final /* synthetic */ boolean $useFullSync;

                /* access modifiers changed from: package-private */
                {
                    this.$useFullSync = r1;
                    this.$result = r2;
                    this.$context = r3;
                }

                @Override // kotlin.jvm.functions.Function0
                public final SystemContactsRepository.ContactIterator invoke() {
                    if (this.$useFullSync) {
                        String str2 = ContactDiscovery.TAG;
                        Log.d(str2, "Doing a full system contact sync. There are " + this.$result.getRegisteredIds().size() + " contacts to get info for.");
                        Context context2 = this.$context;
                        return SystemContactsRepository.getAllSystemContacts(context2, ContactDiscovery.INSTANCE.phoneNumberFormatter(context2));
                    }
                    String str3 = ContactDiscovery.TAG;
                    Log.d(str3, "Doing a partial system contact sync. There are " + this.$result.getRegisteredIds().size() + " contacts to get info for.");
                    Context context3 = this.$context;
                    List<Recipient> resolvedList = Recipient.resolvedList(this.$result.getRegisteredIds());
                    Intrinsics.checkNotNullExpressionValue(resolvedList, "resolvedList(result.registeredIds)");
                    ArrayList arrayList = new ArrayList();
                    for (Recipient recipient : resolvedList) {
                        String orElse = recipient.getE164().orElse(null);
                        if (orElse != null) {
                            arrayList.add(orElse);
                        }
                    }
                    return SystemContactsRepository.getContactDetailsByQueries(context3, arrayList, ContactDiscovery.INSTANCE.phoneNumberFormatter(this.$context));
                }
            }, z3);
            stopwatch.split("contact-sync");
            if (!TextSecurePreferences.hasSuccessfullyRetrievedDirectory(context) || !z2) {
                TextSecurePreferences.setHasSuccessfullyRetrievedDirectory(context, true);
            } else {
                notifyNewUsers(context, CollectionsKt___CollectionsKt.intersect(SetsKt___SetsKt.minus((Set) invoke.getRegisteredIds(), (Iterable) set), CollectionsKt___CollectionsKt.toSet(companion.recipients().getSystemContacts())));
            }
            stopwatch.split("notify");
        } else {
            Log.w(TAG, "No contacts permission, can't sync with system contacts.");
        }
        stopwatch.stop(TAG);
        return invoke;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0047, code lost:
        if (r5.hasSession(r4) == false) goto L_0x004b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void notifyNewUsers(android.content.Context r8, java.util.Collection<? extends org.thoughtcrime.securesms.recipients.RecipientId> r9) {
        /*
        // Method dump skipped, instructions count: 313
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery.notifyNewUsers(android.content.Context, java.util.Collection):void");
    }

    private final ContactLinkConfiguration buildContactLinkConfiguration(Context context, Account account) {
        String string = context.getString(R.string.app_name);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.app_name)");
        return new ContactLinkConfiguration(account, string, new Function1<String, String>(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$buildContactLinkConfiguration$1
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
            }

            public final String invoke(String str) {
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                String string2 = this.$context.getString(R.string.ContactsDatabase_message_s, str);
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…Database_message_s, e164)");
                return string2;
            }
        }, new Function1<String, String>(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$buildContactLinkConfiguration$2
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
            }

            public final String invoke(String str) {
                Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
                String string2 = this.$context.getString(R.string.ContactsDatabase_signal_call_s, str);
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…base_signal_call_s, e164)");
                return string2;
            }
        }, new Function1<String, String>(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$buildContactLinkConfiguration$3
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
            }

            public final String invoke(String str) {
                Intrinsics.checkNotNullParameter(str, "number");
                String format = PhoneNumberFormatter.get(this.$context).format(str);
                Intrinsics.checkNotNullExpressionValue(format, "get(context).format(number)");
                return format;
            }
        }, MESSAGE_MIMETYPE, CALL_MIMETYPE, CONTACT_TAG);
    }

    private final boolean hasContactsPermissions(Context context) {
        return Permissions.hasAll(context, "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS");
    }

    private final void addSystemContactLinks(Context context, Collection<? extends RecipientId> collection, boolean z) {
        if (!Permissions.hasAll(context, "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS")) {
            Log.w(TAG, "[addSystemContactLinks] No contact permissions. Skipping.");
        } else if (collection.isEmpty()) {
            Log.w(TAG, "[addSystemContactLinks] No registeredIds. Skipping.");
        } else {
            Stopwatch stopwatch = new Stopwatch("contact-links");
            String string = context.getString(R.string.app_name);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.string.app_name)");
            Account orCreateSystemAccount = SystemContactsRepository.getOrCreateSystemAccount(context, "org.thoughtcrime.securesms", string);
            if (orCreateSystemAccount == null) {
                Log.w(TAG, "[addSystemContactLinks] Failed to create an account!");
                return;
            }
            try {
                Set<String> e164sForIds = SignalDatabase.Companion.recipients().getE164sForIds(collection);
                stopwatch.split("fetch-e164s");
                SystemContactsRepository.removeDeletedRawContactsForAccount(context, orCreateSystemAccount);
                stopwatch.split("delete-stragglers");
                SystemContactsRepository.addMessageAndCallLinksToContacts(context, buildContactLinkConfiguration(context, orCreateSystemAccount), e164sForIds, z);
                stopwatch.split("add-links");
                stopwatch.stop(TAG);
            } catch (OperationApplicationException e) {
                Log.w(TAG, "[addSystemContactLinks] Failed to add links to contacts.", e);
            } catch (RemoteException e2) {
                Log.w(TAG, "[addSystemContactLinks] Failed to add links to contacts.", e2);
            }
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery */
    /* JADX WARN: Multi-variable type inference failed */
    static /* synthetic */ void syncRecipientsWithSystemContacts$default(ContactDiscovery contactDiscovery, Context context, Map map, Function0 function0, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            function0 = new Function0<SystemContactsRepository.ContactIterator>(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscovery$syncRecipientsWithSystemContacts$1
                final /* synthetic */ Context $context;

                /* access modifiers changed from: package-private */
                {
                    this.$context = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final SystemContactsRepository.ContactIterator invoke() {
                    Context context2 = this.$context;
                    return SystemContactsRepository.getAllSystemContacts(context2, ContactDiscovery.INSTANCE.phoneNumberFormatter(context2));
                }
            };
        }
        contactDiscovery.syncRecipientsWithSystemContacts(context, map, function0, z);
    }

    /* JADX INFO: finally extract failed */
    private final void syncRecipientsWithSystemContacts(Context context, Map<String, String> map, Function0<? extends SystemContactsRepository.ContactIterator> function0, boolean z) {
        RecipientDatabase.BulkOperationsHandle beginBulkSystemContactUpdate;
        Closeable recipientsWithNotificationChannels;
        ProfileName profileName;
        String e164 = SignalStore.account().getE164();
        if (e164 == null) {
            e164 = "";
        }
        try {
            beginBulkSystemContactUpdate = SignalDatabase.Companion.recipients().beginBulkSystemContactUpdate(z);
            th = null;
            try {
                recipientsWithNotificationChannels = (Closeable) function0.invoke();
            } catch (IllegalStateException e) {
                Log.w(TAG, "Hit an issue with the cursor while reading!", e);
            }
            try {
                SystemContactsRepository.ContactIterator contactIterator = (SystemContactsRepository.ContactIterator) recipientsWithNotificationChannels;
                while (contactIterator.hasNext()) {
                    SystemContactsRepository.ContactDetails next = contactIterator.next();
                    List<SystemContactsRepository.ContactPhoneDetails> numbers = next.getNumbers();
                    ArrayList arrayList = new ArrayList();
                    for (Object obj : numbers) {
                        if (!Intrinsics.areEqual(((SystemContactsRepository.ContactPhoneDetails) obj).getNumber(), e164)) {
                            arrayList.add(obj);
                        }
                    }
                    ArrayList<SystemContactsRepository.ContactPhoneDetails> arrayList2 = new ArrayList();
                    for (Object obj2 : arrayList) {
                        if (!UuidUtil.isUuid(((SystemContactsRepository.ContactPhoneDetails) obj2).getNumber())) {
                            arrayList2.add(obj2);
                        }
                    }
                    for (SystemContactsRepository.ContactPhoneDetails contactPhoneDetails : arrayList2) {
                        String firstNonEmpty = Util.getFirstNonEmpty(map.get(contactPhoneDetails.getNumber()), contactPhoneDetails.getNumber());
                        Intrinsics.checkNotNullExpressionValue(firstNonEmpty, "getFirstNonEmpty(rewrite…er], phoneDetails.number)");
                        if (!StringUtil.isEmpty(next.getGivenName())) {
                            profileName = ProfileName.fromParts(next.getGivenName(), next.getFamilyName());
                            Intrinsics.checkNotNullExpressionValue(profileName, "{\n              ProfileN…familyName)\n            }");
                        } else if (!StringUtil.isEmpty(contactPhoneDetails.getDisplayName())) {
                            profileName = ProfileName.asGiven(contactPhoneDetails.getDisplayName());
                            Intrinsics.checkNotNullExpressionValue(profileName, "{\n              ProfileN…isplayName)\n            }");
                        } else {
                            profileName = ProfileName.EMPTY;
                            Intrinsics.checkNotNullExpressionValue(profileName, "{\n              ProfileName.EMPTY\n            }");
                        }
                        RecipientId id = Recipient.externalContact(firstNonEmpty).getId();
                        Intrinsics.checkNotNullExpressionValue(id, "externalContact(realNumber).id");
                        beginBulkSystemContactUpdate.setSystemContactInfo(id, profileName, contactPhoneDetails.getDisplayName(), contactPhoneDetails.getPhotoUri(), contactPhoneDetails.getLabel(), contactPhoneDetails.getType(), contactPhoneDetails.getContactUri().toString());
                    }
                }
                Unit unit = Unit.INSTANCE;
                beginBulkSystemContactUpdate.finish();
                if (NotificationChannels.supported()) {
                    recipientsWithNotificationChannels = SignalDatabase.Companion.recipients().getRecipientsWithNotificationChannels();
                    try {
                        for (Recipient next2 = recipientsWithNotificationChannels.getNext(); next2 != null; next2 = recipientsWithNotificationChannels.getNext()) {
                            NotificationChannels.updateContactChannelName(context, next2);
                        }
                        Unit unit2 = Unit.INSTANCE;
                    } catch (Throwable th) {
                        try {
                            throw th;
                        } finally {
                        }
                    }
                }
            } catch (Throwable th) {
                try {
                    throw th;
                } finally {
                }
            }
        } catch (Throwable th2) {
            beginBulkSystemContactUpdate.finish();
            throw th2;
        }
    }

    public final boolean hasSession(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(id)");
        if (!resolved.hasServiceId()) {
            return false;
        }
        SignalProtocolAddress protocolAddress = Recipient.resolved(recipientId).requireServiceId().toProtocolAddress(1);
        if (ApplicationDependencies.getProtocolStore().aci().containsSession(protocolAddress) || ApplicationDependencies.getProtocolStore().pni().containsSession(protocolAddress)) {
            return true;
        }
        return false;
    }

    /* compiled from: ContactDiscovery.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B'\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001d\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/sync/ContactDiscovery$RefreshResult;", "", "registeredIds", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "rewrites", "", "", "(Ljava/util/Set;Ljava/util/Map;)V", "getRegisteredIds", "()Ljava/util/Set;", "getRewrites", "()Ljava/util/Map;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RefreshResult {
        private final Set<RecipientId> registeredIds;
        private final Map<String, String> rewrites;

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
        /* JADX WARN: Multi-variable type inference failed */
        public RefreshResult(Set<? extends RecipientId> set, Map<String, String> map) {
            Intrinsics.checkNotNullParameter(set, "registeredIds");
            Intrinsics.checkNotNullParameter(map, "rewrites");
            this.registeredIds = set;
            this.rewrites = map;
        }

        public final Set<RecipientId> getRegisteredIds() {
            return this.registeredIds;
        }

        public final Map<String, String> getRewrites() {
            return this.rewrites;
        }
    }
}
