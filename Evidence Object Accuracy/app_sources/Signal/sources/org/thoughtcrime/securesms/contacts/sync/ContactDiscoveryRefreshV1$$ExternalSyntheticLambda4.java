package org.thoughtcrime.securesms.contacts.sync;

import com.annimon.stream.function.Predicate;
import java.util.Set;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactDiscoveryRefreshV1$$ExternalSyntheticLambda4 implements Predicate {
    public final /* synthetic */ Set f$0;

    public /* synthetic */ ContactDiscoveryRefreshV1$$ExternalSyntheticLambda4(Set set) {
        this.f$0 = set;
    }

    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.contains((String) obj);
    }
}
