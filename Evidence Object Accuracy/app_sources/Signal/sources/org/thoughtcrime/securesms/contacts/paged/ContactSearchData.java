package org.thoughtcrime.securesms.contacts.paged;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.HeaderAction;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ContactSearchData.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0007\b\t\nB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0004\u000b\f\r\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "", "contactSearchKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;)V", "getContactSearchKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "Expand", "Header", "KnownRecipient", "Story", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Header;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ContactSearchData {
    private final ContactSearchKey contactSearchKey;

    public /* synthetic */ ContactSearchData(ContactSearchKey contactSearchKey, DefaultConstructorMarker defaultConstructorMarker) {
        this(contactSearchKey);
    }

    private ContactSearchData(ContactSearchKey contactSearchKey) {
        this.contactSearchKey = contactSearchKey;
    }

    public final ContactSearchKey getContactSearchKey() {
        return this.contactSearchKey;
    }

    /* compiled from: ContactSearchData.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "viewerCount", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;I)V", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getViewerCount", "()I", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Story extends ContactSearchData {
        private final Recipient recipient;
        private final int viewerCount;

        public static /* synthetic */ Story copy$default(Story story, Recipient recipient, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                recipient = story.recipient;
            }
            if ((i2 & 2) != 0) {
                i = story.viewerCount;
            }
            return story.copy(recipient, i);
        }

        public final Recipient component1() {
            return this.recipient;
        }

        public final int component2() {
            return this.viewerCount;
        }

        public final Story copy(Recipient recipient, int i) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            return new Story(recipient, i);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Story)) {
                return false;
            }
            Story story = (Story) obj;
            return Intrinsics.areEqual(this.recipient, story.recipient) && this.viewerCount == story.viewerCount;
        }

        public int hashCode() {
            return (this.recipient.hashCode() * 31) + this.viewerCount;
        }

        public String toString() {
            return "Story(recipient=" + this.recipient + ", viewerCount=" + this.viewerCount + ')';
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public Story(org.thoughtcrime.securesms.recipients.Recipient r4, int r5) {
            /*
                r3 = this;
                java.lang.String r0 = "recipient"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
                org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey$Story r0 = new org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey$Story
                org.thoughtcrime.securesms.recipients.RecipientId r1 = r4.getId()
                java.lang.String r2 = "recipient.id"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
                r0.<init>(r1)
                r1 = 0
                r3.<init>(r0, r1)
                r3.recipient = r4
                r3.viewerCount = r5
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.contacts.paged.ContactSearchData.Story.<init>(org.thoughtcrime.securesms.recipients.Recipient, int):void");
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final int getViewerCount() {
            return this.viewerCount;
        }
    }

    /* compiled from: ContactSearchData.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$KnownRecipient;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class KnownRecipient extends ContactSearchData {
        private final Recipient recipient;

        public static /* synthetic */ KnownRecipient copy$default(KnownRecipient knownRecipient, Recipient recipient, int i, Object obj) {
            if ((i & 1) != 0) {
                recipient = knownRecipient.recipient;
            }
            return knownRecipient.copy(recipient);
        }

        public final Recipient component1() {
            return this.recipient;
        }

        public final KnownRecipient copy(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            return new KnownRecipient(recipient);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof KnownRecipient) && Intrinsics.areEqual(this.recipient, ((KnownRecipient) obj).recipient);
        }

        public int hashCode() {
            return this.recipient.hashCode();
        }

        public String toString() {
            return "KnownRecipient(recipient=" + this.recipient + ')';
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public KnownRecipient(org.thoughtcrime.securesms.recipients.Recipient r4) {
            /*
                r3 = this;
                java.lang.String r0 = "recipient"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r0)
                org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey$KnownRecipient r0 = new org.thoughtcrime.securesms.contacts.paged.ContactSearchKey$RecipientSearchKey$KnownRecipient
                org.thoughtcrime.securesms.recipients.RecipientId r1 = r4.getId()
                java.lang.String r2 = "recipient.id"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
                r0.<init>(r1)
                r1 = 0
                r3.<init>(r0, r1)
                r3.recipient = r4
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.contacts.paged.ContactSearchData.KnownRecipient.<init>(org.thoughtcrime.securesms.recipients.Recipient):void");
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }
    }

    /* compiled from: ContactSearchData.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Header;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "sectionKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "action", "Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;Lorg/thoughtcrime/securesms/contacts/HeaderAction;)V", "getAction", "()Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "getSectionKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Header extends ContactSearchData {
        private final HeaderAction action;
        private final ContactSearchConfiguration.SectionKey sectionKey;

        public final ContactSearchConfiguration.SectionKey getSectionKey() {
            return this.sectionKey;
        }

        public final HeaderAction getAction() {
            return this.action;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Header(ContactSearchConfiguration.SectionKey sectionKey, HeaderAction headerAction) {
            super(new ContactSearchKey.Header(sectionKey), null);
            Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
            this.sectionKey = sectionKey;
            this.action = headerAction;
        }
    }

    /* compiled from: ContactSearchData.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Expand;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "sectionKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;)V", "getSectionKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Expand extends ContactSearchData {
        private final ContactSearchConfiguration.SectionKey sectionKey;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Expand(ContactSearchConfiguration.SectionKey sectionKey) {
            super(new ContactSearchKey.Expand(sectionKey), null);
            Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
            this.sectionKey = sectionKey;
        }

        public final ContactSearchConfiguration.SectionKey getSectionKey() {
            return this.sectionKey;
        }
    }
}
