package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorWrapper;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.signal.core.util.CursorUtil;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ContactRepository {
    static final String ABOUT_COLUMN;
    static final String CONTACT_TYPE_COLUMN;
    static final int DIVIDER_TYPE;
    public static final String ID_COLUMN;
    static final String LABEL_COLUMN;
    static final String NAME_COLUMN;
    static final int NEW_PHONE_TYPE;
    static final int NEW_USERNAME_TYPE;
    static final int NORMAL_TYPE;
    static final String NUMBER_COLUMN;
    static final String NUMBER_TYPE_COLUMN;
    static final int PUSH_TYPE;
    static final int RECENT_TYPE;
    private static final List<Pair<String, ValueMapper>> SEARCH_CURSOR_MAPPERS = new ArrayList<Pair<String, ValueMapper>>() { // from class: org.thoughtcrime.securesms.contacts.ContactRepository.1
        {
            add(new Pair(ContactRepository.ID_COLUMN, new ContactRepository$1$$ExternalSyntheticLambda0()));
            add(new Pair("name", new ContactRepository$1$$ExternalSyntheticLambda1()));
            add(new Pair(ContactRepository.NUMBER_COLUMN, new ContactRepository$1$$ExternalSyntheticLambda2()));
            add(new Pair(ContactRepository.NUMBER_TYPE_COLUMN, new ContactRepository$1$$ExternalSyntheticLambda3()));
            add(new Pair("label", new ContactRepository$1$$ExternalSyntheticLambda4()));
            add(new Pair(ContactRepository.CONTACT_TYPE_COLUMN, new ContactRepository$1$$ExternalSyntheticLambda5()));
            add(new Pair("about", new ContactRepository$1$$ExternalSyntheticLambda6()));
        }

        public static /* synthetic */ Object lambda$new$0(Cursor cursor) {
            return Long.valueOf(CursorUtil.requireLong(cursor, "_id"));
        }

        public static /* synthetic */ Object lambda$new$1(Cursor cursor) {
            return Util.getFirstNonEmpty(CursorUtil.requireString(cursor, RecipientDatabase.SYSTEM_JOINED_NAME), CursorUtil.requireString(cursor, RecipientDatabase.SEARCH_PROFILE_NAME));
        }

        public static /* synthetic */ Object lambda$new$2(Cursor cursor) {
            String requireString = CursorUtil.requireString(cursor, RecipientDatabase.PHONE);
            String requireString2 = CursorUtil.requireString(cursor, RecipientDatabase.EMAIL);
            if (requireString != null) {
                requireString = PhoneNumberFormatter.prettyPrint(requireString);
            }
            return Util.getFirstNonEmpty(requireString, requireString2);
        }

        public static /* synthetic */ Object lambda$new$3(Cursor cursor) {
            return Integer.valueOf(CursorUtil.requireInt(cursor, RecipientDatabase.SYSTEM_PHONE_TYPE));
        }

        public static /* synthetic */ Object lambda$new$5(Cursor cursor) {
            return Integer.valueOf(CursorUtil.requireInt(cursor, RecipientDatabase.REGISTERED) == RecipientDatabase.RegisteredState.REGISTERED.getId() ? 1 : 0);
        }

        public static /* synthetic */ Object lambda$new$6(Cursor cursor) {
            String requireString = CursorUtil.requireString(cursor, RecipientDatabase.ABOUT_EMOJI);
            String requireString2 = CursorUtil.requireString(cursor, "about");
            if (Util.isEmpty(requireString)) {
                return !Util.isEmpty(requireString2) ? requireString2 : "";
            }
            if (Util.isEmpty(requireString2)) {
                return requireString;
            }
            return requireString + " " + requireString2;
        }
    };
    private final Context context;
    private final String noteToSelfTitle;
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();

    /* loaded from: classes4.dex */
    public interface ValueMapper<T> {
        T get(Cursor cursor);
    }

    public ContactRepository(Context context, String str) {
        this.noteToSelfTitle = str;
        this.context = context.getApplicationContext();
    }

    public Cursor querySignalContacts(String str) {
        return querySignalContacts(str, true);
    }

    public Cursor querySignalContacts(String str, boolean z) {
        Cursor cursor;
        if (TextUtils.isEmpty(str)) {
            cursor = this.recipientDatabase.getSignalContacts(z);
        } else {
            cursor = this.recipientDatabase.querySignalContacts(str, z);
        }
        return new SearchCursorWrapper(handleNoteToSelfQuery(str, z, cursor), SEARCH_CURSOR_MAPPERS);
    }

    public Cursor queryNonGroupContacts(String str, boolean z) {
        Cursor cursor;
        if (TextUtils.isEmpty(str)) {
            cursor = this.recipientDatabase.getNonGroupContacts(z);
        } else {
            cursor = this.recipientDatabase.queryNonGroupContacts(str, z);
        }
        return new SearchCursorWrapper(handleNoteToSelfQuery(str, z, cursor), SEARCH_CURSOR_MAPPERS);
    }

    private Cursor handleNoteToSelfQuery(String str, boolean z, Cursor cursor) {
        if (!z || !this.noteToSelfTitle.toLowerCase().contains(str.toLowerCase())) {
            return cursor;
        }
        Recipient self = Recipient.self();
        if (!(!self.getDisplayName(this.context).toLowerCase().contains(str.toLowerCase()) && !(self.getE164().isPresent() && self.requireE164().contains(str)))) {
            return cursor;
        }
        MatrixCursor matrixCursor = new MatrixCursor(RecipientDatabase.SEARCH_PROJECTION_NAMES);
        String str2 = this.noteToSelfTitle;
        matrixCursor.addRow(new Object[]{self.getId().serialize(), this.noteToSelfTitle, self.getE164().orElse(""), self.getEmail().orElse(null), null, -1, Integer.valueOf(RecipientDatabase.RegisteredState.REGISTERED.getId()), self.getAbout(), self.getAboutEmoji(), null, Boolean.TRUE, str2, str2});
        if (cursor == null) {
            return matrixCursor;
        }
        return new MergeCursor(new Cursor[]{cursor, matrixCursor});
    }

    public Cursor queryNonSignalContacts(String str) {
        Cursor cursor;
        if (TextUtils.isEmpty(str)) {
            cursor = this.recipientDatabase.getNonSignalContacts();
        } else {
            cursor = this.recipientDatabase.queryNonSignalContacts(str);
        }
        return new SearchCursorWrapper(cursor, SEARCH_CURSOR_MAPPERS);
    }

    /* loaded from: classes4.dex */
    public static class SearchCursorWrapper extends CursorWrapper {
        private final String[] columnNames;
        private final List<Pair<String, ValueMapper>> mappers;
        private final Map<String, Integer> positions = new HashMap();
        private final Cursor wrapped;

        SearchCursorWrapper(Cursor cursor, List<Pair<String, ValueMapper>> list) {
            super(cursor);
            this.wrapped = cursor;
            this.mappers = list;
            this.columnNames = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                Pair<String, ValueMapper> pair = list.get(i);
                this.positions.put(pair.first(), Integer.valueOf(i));
                this.columnNames[i] = pair.first();
            }
        }

        @Override // android.database.CursorWrapper, android.database.Cursor
        public int getColumnCount() {
            return this.mappers.size();
        }

        @Override // android.database.CursorWrapper, android.database.Cursor
        public String[] getColumnNames() {
            return this.columnNames;
        }

        @Override // android.database.CursorWrapper, android.database.Cursor
        public int getColumnIndexOrThrow(String str) throws IllegalArgumentException {
            Integer num = this.positions.get(str);
            if (num != null) {
                return num.intValue();
            }
            throw new IllegalArgumentException();
        }

        @Override // android.database.CursorWrapper, android.database.Cursor
        public String getString(int i) {
            return String.valueOf(this.mappers.get(i).second().get(this.wrapped));
        }

        @Override // android.database.CursorWrapper, android.database.Cursor
        public int getInt(int i) {
            return ((Integer) this.mappers.get(i).second().get(this.wrapped)).intValue();
        }

        @Override // android.database.CursorWrapper, android.database.Cursor
        public long getLong(int i) {
            return ((Long) this.mappers.get(i).second().get(this.wrapped)).longValue();
        }
    }
}
