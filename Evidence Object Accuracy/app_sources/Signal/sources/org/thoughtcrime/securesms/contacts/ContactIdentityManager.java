package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.net.Uri;
import java.util.List;

/* loaded from: classes4.dex */
public abstract class ContactIdentityManager {
    protected final Context context;

    public abstract List<Long> getSelfIdentityRawContactIds();

    public abstract Uri getSelfIdentityUri();

    public abstract boolean isSelfIdentityAutoDetected();

    public static ContactIdentityManager getInstance(Context context) {
        return new ContactIdentityManagerICS(context);
    }

    public ContactIdentityManager(Context context) {
        this.context = context.getApplicationContext();
    }
}
