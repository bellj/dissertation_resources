package org.thoughtcrime.securesms.contacts.sync;

import android.content.Context;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.contacts.SystemContactsRepository;
import org.signal.core.util.SetUtil;
import org.signal.core.util.concurrent.RxExtensions;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidKeyException;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1;
import org.thoughtcrime.securesms.contacts.sync.FuzzyPhoneNumberHelper;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.push.IasTrustStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.services.ProfileService;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.contacts.crypto.Quote;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedQuoteException;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* loaded from: classes4.dex */
public class ContactDiscoveryRefreshV1 {
    private static final int MAX_NUMBERS;
    private static final String TAG = Log.tag(ContactDiscoveryRefreshV1.class);

    ContactDiscoveryRefreshV1() {
    }

    public static ContactDiscovery.RefreshResult refreshAll(Context context) throws IOException {
        return refreshNumbers(context, sanitizeNumbers(SignalDatabase.recipients().getAllE164s()), sanitizeNumbers((Set) Stream.of(SystemContactsRepository.getAllDisplayNumbers(context)).map(new Function(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactDiscoveryRefreshV1.m1352$r8$lambda$pDtukNie6VOnx3XczBvKvsTR4A(this.f$0, (String) obj);
            }
        }).collect(Collectors.toSet())));
    }

    public static /* synthetic */ String lambda$refreshAll$0(Context context, String str) {
        return PhoneNumberFormatter.get(context).format(str);
    }

    public static ContactDiscovery.RefreshResult refresh(Context context, List<Recipient> list) throws IOException {
        RecipientDatabase recipients = SignalDatabase.recipients();
        for (Recipient recipient : list) {
            if (recipient.hasServiceId() && !recipient.hasE164()) {
                if (ApplicationDependencies.getSignalServiceAccountManager().isIdentifierRegistered(recipient.requireServiceId())) {
                    recipients.markRegistered(recipient.getId(), recipient.requireServiceId());
                } else {
                    recipients.markUnregistered(recipient.getId());
                }
            }
        }
        Set set = (Set) Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((Recipient) obj).hasE164();
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((Recipient) obj).requireE164();
            }
        }).collect(Collectors.toSet());
        if (set.size() < list.size()) {
            String str = TAG;
            Log.w(str, "We were asked to refresh " + list.size() + " numbers, but filtered that down to " + set.size());
        }
        return refreshNumbers(context, set, set);
    }

    private static ContactDiscovery.RefreshResult refreshNumbers(Context context, Set<String> set, Set<String> set2) throws IOException {
        RecipientDatabase recipients = SignalDatabase.recipients();
        Set union = SetUtil.union(set, set2);
        if (union.isEmpty()) {
            Log.w(TAG, "No numbers to refresh!");
            return new ContactDiscovery.RefreshResult(Collections.emptySet(), Collections.emptyMap());
        }
        Stopwatch stopwatch = new Stopwatch("refresh");
        ContactIntersection intersection = getIntersection(context, set, set2);
        stopwatch.split("network");
        if (intersection.getNumberRewrites().size() > 0) {
            Log.i(TAG, "[getDirectoryResult] Need to rewrite some numbers.");
            recipients.updatePhoneNumbers(intersection.getNumberRewrites());
        }
        Map<RecipientId, ACI> bulkProcessCdsResult = recipients.bulkProcessCdsResult(intersection.getRegisteredNumbers());
        Set<String> keySet = intersection.getRegisteredNumbers().keySet();
        Set<RecipientId> keySet2 = bulkProcessCdsResult.keySet();
        Stream of = Stream.of(union);
        Objects.requireNonNull(keySet);
        Set set3 = (Set) of.filterNot(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda4(keySet)).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ContactDiscoveryRefreshV1.m1354$r8$lambda$v9k2bwH2L0859LSI_sovP5WpCY(ContactDiscoveryRefreshV1.ContactIntersection.this, (String) obj);
            }
        }).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ContactDiscoveryRefreshV1.$r8$lambda$1mcXl7JgEsLcJQVGJ0cFDEPFWks(ContactDiscoveryRefreshV1.ContactIntersection.this, (String) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return RecipientDatabase.this.getOrInsertFromE164((String) obj);
            }
        }).collect(Collectors.toSet());
        stopwatch.split("process-cds");
        UnlistedResult filterForUnlistedUsers = filterForUnlistedUsers(context, set3);
        set3.removeAll(filterForUnlistedUsers.getPossiblyActive());
        if (filterForUnlistedUsers.getRetries().size() > 0) {
            Log.i(TAG, "Some profile fetches failed to resolve. Assuming not-inactive for now and scheduling a retry.");
            RetrieveProfileJob.enqueue(filterForUnlistedUsers.getRetries());
        }
        stopwatch.split("handle-unlisted");
        recipients.bulkUpdatedRegisteredStatus(bulkProcessCdsResult, set3);
        stopwatch.split("update-registered");
        if (TextSecurePreferences.isMultiDevice(context)) {
            ApplicationDependencies.getJobManager().add(new MultiDeviceContactUpdateJob());
        }
        stopwatch.stop(TAG);
        return new ContactDiscovery.RefreshResult(keySet2, intersection.getNumberRewrites());
    }

    public static /* synthetic */ boolean lambda$refreshNumbers$1(ContactIntersection contactIntersection, String str) {
        return contactIntersection.getNumberRewrites().containsKey(str);
    }

    public static /* synthetic */ boolean lambda$refreshNumbers$2(ContactIntersection contactIntersection, String str) {
        return contactIntersection.getIgnoredNumbers().contains(str);
    }

    private static Set<String> sanitizeNumbers(Set<String> set) {
        return (Set) Stream.of(set).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ContactDiscoveryRefreshV1.$r8$lambda$nWKxIxJvh7HJkZUUJSAhD5tauCU((String) obj);
            }
        }).collect(Collectors.toSet());
    }

    public static /* synthetic */ boolean lambda$sanitizeNumbers$3(String str) {
        try {
            if (!str.startsWith("+") || str.length() <= 1 || str.charAt(1) == '0') {
                return false;
            }
            return Long.parseLong(str.substring(1)) > 0;
        } catch (NumberFormatException unused) {
            return false;
        }
    }

    private static UnlistedResult filterForUnlistedUsers(Context context, Set<RecipientId> set) {
        List<Recipient> list = Stream.of(set).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).filter(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda9()).filter(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda10()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ContactDiscoveryRefreshV1.m1351$r8$lambda$JDkM_gr2z8CNczgzu3LKr5fvaw((Recipient) obj);
            }
        }).toList();
        try {
            return (UnlistedResult) RxExtensions.safeBlockingGet(Observable.mergeDelayError(Stream.of(list).map(new Function(context) { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda12
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ContactDiscoveryRefreshV1.m1353$r8$lambda$ucITC_ZFX96_2WsTyir9rI7TPw(this.f$0, (Recipient) obj);
                }
            }).toList()).observeOn(Schedulers.io(), true).scan(new UnlistedResult.Builder(), new BiFunction() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda13
                @Override // io.reactivex.rxjava3.functions.BiFunction
                public final Object apply(Object obj, Object obj2) {
                    return ContactDiscoveryRefreshV1.$r8$lambda$SFF4VNQXvHfdm4R6EbsHbHtOwJs((ContactDiscoveryRefreshV1.UnlistedResult.Builder) obj, (Pair) obj2);
                }
            }).lastOrError().map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda14
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return ((ContactDiscoveryRefreshV1.UnlistedResult.Builder) obj).build();
                }
            }));
        } catch (InterruptedException unused) {
            Log.i(TAG, "Filter for unlisted profile fetches interrupted, fetch via job instead");
            UnlistedResult.Builder builder = new UnlistedResult.Builder();
            for (Recipient recipient : list) {
                builder.retries.add(recipient.getId());
            }
            return builder.build();
        }
    }

    public static /* synthetic */ Observable lambda$filterForUnlistedUsers$5(Context context, Recipient recipient) {
        return ProfileUtil.retrieveProfile(context, recipient, SignalServiceProfile.RequestType.PROFILE).toObservable().timeout(5, TimeUnit.SECONDS).onErrorReturn(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ContactDiscoveryRefreshV1.$r8$lambda$jbBiVHT_lg7KZxQzaAsg1WVOO4E(Recipient.this, (Throwable) obj);
            }
        });
    }

    public static /* synthetic */ Pair lambda$filterForUnlistedUsers$4(Recipient recipient, Throwable th) throws Throwable {
        return new Pair(recipient, ServiceResponse.forUnknownError(th));
    }

    public static /* synthetic */ UnlistedResult.Builder lambda$filterForUnlistedUsers$6(UnlistedResult.Builder builder, Pair pair) throws Throwable {
        Recipient recipient = (Recipient) pair.first();
        ProfileService.ProfileResponseProcessor profileResponseProcessor = new ProfileService.ProfileResponseProcessor((ServiceResponse) pair.second());
        if (profileResponseProcessor.hasResult()) {
            builder.potentiallyActiveIds.add(recipient.getId());
        } else if (profileResponseProcessor.genericIoError() || !profileResponseProcessor.notFound()) {
            builder.retries.add(recipient.getId());
            builder.potentiallyActiveIds.add(recipient.getId());
        }
        return builder;
    }

    public static boolean hasCommunicatedWith(Recipient recipient) {
        return SignalDatabase.threads().hasThread(recipient.getId()) || (recipient.hasServiceId() && SignalDatabase.sessions().hasSessionFor(SignalStore.account().requireAci(), recipient.requireServiceId().toString()));
    }

    private static ContactIntersection getIntersection(Context context, Set<String> set, Set<String> set2) throws IOException {
        FuzzyPhoneNumberHelper.InputResult generateInput = FuzzyPhoneNumberHelper.generateInput(SetUtil.union(set, set2), set);
        Set<String> sanitizeNumbers = sanitizeNumbers(generateInput.getNumbers());
        Set hashSet = new HashSet();
        if (sanitizeNumbers.size() > MAX_NUMBERS) {
            Set<String> randomlySelect = randomlySelect(sanitizeNumbers, MAX_NUMBERS);
            hashSet = SetUtil.difference(sanitizeNumbers, randomlySelect);
            sanitizeNumbers = randomlySelect;
        }
        try {
            FuzzyPhoneNumberHelper.OutputResult generateOutput = FuzzyPhoneNumberHelper.generateOutput(ApplicationDependencies.getSignalServiceAccountManager().getRegisteredUsers(getIasKeyStore(context), sanitizeNumbers, BuildConfig.CDS_MRENCLAVE), generateInput);
            return new ContactIntersection(generateOutput.getNumbers(), generateOutput.getRewrites(), hashSet);
        } catch (SignatureException | InvalidKeyException | Quote.InvalidQuoteFormatException | UnauthenticatedQuoteException | UnauthenticatedResponseException e) {
            Log.w(TAG, "Attestation error.", e);
            throw new IOException(e);
        }
    }

    private static Set<String> randomlySelect(Set<String> set, int i) {
        ArrayList arrayList = new ArrayList(set);
        Collections.shuffle(arrayList);
        return new HashSet(arrayList.subList(0, i));
    }

    private static KeyStore getIasKeyStore(Context context) {
        try {
            IasTrustStore iasTrustStore = new IasTrustStore(context);
            KeyStore instance = KeyStore.getInstance("BKS");
            instance.load(iasTrustStore.getKeyStoreInputStream(), iasTrustStore.getKeyStorePassword().toCharArray());
            return instance;
        } catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            throw new AssertionError(e);
        }
    }

    /* loaded from: classes4.dex */
    public static class ContactIntersection {
        private final Set<String> ignoredNumbers;
        private final Map<String, String> numberRewrites;
        private final Map<String, ACI> registeredNumbers;

        ContactIntersection(Map<String, ACI> map, Map<String, String> map2, Set<String> set) {
            this.registeredNumbers = map;
            this.numberRewrites = map2;
            this.ignoredNumbers = set;
        }

        Map<String, ACI> getRegisteredNumbers() {
            return this.registeredNumbers;
        }

        Map<String, String> getNumberRewrites() {
            return this.numberRewrites;
        }

        Set<String> getIgnoredNumbers() {
            return this.ignoredNumbers;
        }
    }

    /* loaded from: classes4.dex */
    public static class UnlistedResult {
        private final Set<RecipientId> possiblyActive;
        private final Set<RecipientId> retries;

        private UnlistedResult(Set<RecipientId> set, Set<RecipientId> set2) {
            this.possiblyActive = set;
            this.retries = set2;
        }

        Set<RecipientId> getPossiblyActive() {
            return this.possiblyActive;
        }

        Set<RecipientId> getRetries() {
            return this.retries;
        }

        /* loaded from: classes4.dex */
        public static class Builder {
            final Set<RecipientId> potentiallyActiveIds;
            final Set<RecipientId> retries;

            private Builder() {
                this.potentiallyActiveIds = new HashSet();
                this.retries = new HashSet();
            }

            public UnlistedResult build() {
                return new UnlistedResult(this.potentiallyActiveIds, this.retries);
            }
        }
    }
}
