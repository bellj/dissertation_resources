package org.thoughtcrime.securesms.contacts;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;

/* compiled from: HeaderAction.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001B\u0019\b\u0016\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B!\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\f¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/HeaderAction;", "", EmojiSearchDatabase.LABEL, "", "action", "Ljava/lang/Runnable;", "(ILjava/lang/Runnable;)V", "icon", "(IILjava/lang/Runnable;)V", "getAction", "()Ljava/lang/Runnable;", "getIcon", "()I", "getLabel", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class HeaderAction {
    private final Runnable action;
    private final int icon;
    private final int label;

    public HeaderAction(int i, int i2, Runnable runnable) {
        Intrinsics.checkNotNullParameter(runnable, "action");
        this.label = i;
        this.icon = i2;
        this.action = runnable;
    }

    public final Runnable getAction() {
        return this.action;
    }

    public final int getIcon() {
        return this.icon;
    }

    public final int getLabel() {
        return this.label;
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public HeaderAction(int i, Runnable runnable) {
        this(i, 0, runnable);
        Intrinsics.checkNotNullParameter(runnable, "action");
    }
}
