package org.thoughtcrime.securesms.contacts.sync;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactDiscoveryRefreshV1$$ExternalSyntheticLambda10 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((Recipient) obj).hasServiceId();
    }
}
