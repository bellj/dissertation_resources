package org.thoughtcrime.securesms.contacts.paged;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.paging.LivePagedData;
import org.signal.paging.PagedData;
import org.signal.paging.PagingConfig;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.util.Preconditions;

/* compiled from: ContactSearchViewModel.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u00018B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0014\u0010!\u001a\u00020\"2\f\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\u001eJ\u000e\u0010%\u001a\u00020\"2\u0006\u0010&\u001a\u00020'J\u000e\u0010(\u001a\u00020\"2\u0006\u0010)\u001a\u00020*J\f\u0010+\u001a\b\u0012\u0004\u0012\u00020\u00100\u001eJ\b\u0010,\u001a\u00020\"H\u0014J\u0006\u0010-\u001a\u00020\"J\u000e\u0010.\u001a\u00020\"2\u0006\u0010&\u001a\u00020'J\u000e\u0010/\u001a\u00020\"2\u0006\u00100\u001a\u000201J\u0014\u00102\u001a\u00020\"2\f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00100\u001eJ\u0014\u00104\u001a\u00020\"2\f\u00103\u001a\b\u0012\u0004\u0012\u00020\u00100\u001eJ\u0010\u00105\u001a\u00020\"2\b\u00106\u001a\u0004\u0018\u000107R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000bR\u001d\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00130\b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u000bR\u000e\u0010\u0016\u001a\u00020\u0017X\u0004¢\u0006\u0002\n\u0000R \u0010\u0018\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u00140\u001a0\u0019X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u001e0\b¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u000bR\u001a\u0010 \u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u001e0\rX\u0004¢\u0006\u0002\n\u0000¨\u00069"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchViewModel;", "Landroidx/lifecycle/ViewModel;", "selectionLimits", "Lorg/thoughtcrime/securesms/groups/SelectionLimits;", "contactSearchRepository", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchRepository;", "(Lorg/thoughtcrime/securesms/groups/SelectionLimits;Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchRepository;)V", "configurationState", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "getConfigurationState", "()Landroidx/lifecycle/LiveData;", "configurationStore", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "controller", "Lorg/signal/paging/PagingController;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "getController", "data", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "getData", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "pagedData", "Landroidx/lifecycle/MutableLiveData;", "Lorg/signal/paging/LivePagedData;", "pagingConfig", "Lorg/signal/paging/PagingConfig;", "selectionState", "", "getSelectionState", "selectionStore", "addToVisibleGroupStories", "", "groupStories", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey$Story;", "deletePrivateStory", "story", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData$Story;", "expandSection", "sectionKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "getSelectedContacts", "onCleared", "refresh", "removeGroupStory", "setConfiguration", "contactSearchConfiguration", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "setKeysNotSelected", "contactSearchKeys", "setKeysSelected", "setQuery", "query", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchViewModel extends ViewModel {
    private final LiveData<ContactSearchState> configurationState;
    private final Store<ContactSearchState> configurationStore;
    private final ContactSearchRepository contactSearchRepository;
    private final LiveData<PagingController<ContactSearchKey>> controller;
    private final LiveData<List<ContactSearchData>> data;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<LivePagedData<ContactSearchKey, ContactSearchData>> pagedData;
    private final PagingConfig pagingConfig;
    private final SelectionLimits selectionLimits;
    private final LiveData<Set<ContactSearchKey>> selectionState;
    private final Store<Set<ContactSearchKey>> selectionStore;

    public ContactSearchViewModel(SelectionLimits selectionLimits, ContactSearchRepository contactSearchRepository) {
        Intrinsics.checkNotNullParameter(selectionLimits, "selectionLimits");
        Intrinsics.checkNotNullParameter(contactSearchRepository, "contactSearchRepository");
        this.selectionLimits = selectionLimits;
        this.contactSearchRepository = contactSearchRepository;
        PagingConfig build = new PagingConfig.Builder().setBufferPages(1).setPageSize(20).setStartIndex(0).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n    .setBuffer…tartIndex(0)\n    .build()");
        this.pagingConfig = build;
        MutableLiveData<LivePagedData<ContactSearchKey, ContactSearchData>> mutableLiveData = new MutableLiveData<>();
        this.pagedData = mutableLiveData;
        Store<ContactSearchState> store = new Store<>(new ContactSearchState(null, null, null, 7, null));
        this.configurationStore = store;
        Store<Set<ContactSearchKey>> store2 = new Store<>(SetsKt__SetsKt.emptySet());
        this.selectionStore = store2;
        LiveData<PagingController<ContactSearchKey>> map = Transformations.map(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1341controller$lambda0((LivePagedData) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "map(pagedData) { it.controller }");
        this.controller = map;
        LiveData<List<ContactSearchData>> switchMap = Transformations.switchMap(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda4
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1342data$lambda1((LivePagedData) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(switchMap, "switchMap(pagedData) { it.data }");
        this.data = switchMap;
        LiveData<ContactSearchState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "configurationStore.stateLiveData");
        this.configurationState = stateLiveData;
        LiveData<Set<ContactSearchKey>> stateLiveData2 = store2.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData2, "selectionStore.stateLiveData");
        this.selectionState = stateLiveData2;
    }

    /* renamed from: controller$lambda-0 */
    public static final PagingController m1341controller$lambda0(LivePagedData livePagedData) {
        return livePagedData.getController();
    }

    public final LiveData<PagingController<ContactSearchKey>> getController() {
        return this.controller;
    }

    /* renamed from: data$lambda-1 */
    public static final LiveData m1342data$lambda1(LivePagedData livePagedData) {
        return livePagedData.getData();
    }

    public final LiveData<List<ContactSearchData>> getData() {
        return this.data;
    }

    public final LiveData<ContactSearchState> getConfigurationState() {
        return this.configurationState;
    }

    public final LiveData<Set<ContactSearchKey>> getSelectionState() {
        return this.selectionState;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void setConfiguration(ContactSearchConfiguration contactSearchConfiguration) {
        Intrinsics.checkNotNullParameter(contactSearchConfiguration, "contactSearchConfiguration");
        this.pagedData.setValue(PagedData.createForLiveData(new ContactSearchPagedDataSource(contactSearchConfiguration, null, 2, null), this.pagingConfig));
    }

    /* renamed from: setQuery$lambda-2 */
    public static final ContactSearchState m1350setQuery$lambda2(String str, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullExpressionValue(contactSearchState, "it");
        return ContactSearchState.copy$default(contactSearchState, str, null, null, 6, null);
    }

    public final void setQuery(String str) {
        this.configurationStore.update(new com.annimon.stream.function.Function(str) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda8
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1350setQuery$lambda2(this.f$0, (ContactSearchState) obj);
            }
        });
    }

    /* renamed from: expandSection$lambda-3 */
    public static final ContactSearchState m1344expandSection$lambda3(ContactSearchConfiguration.SectionKey sectionKey, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(sectionKey, "$sectionKey");
        Intrinsics.checkNotNullExpressionValue(contactSearchState, "it");
        return ContactSearchState.copy$default(contactSearchState, null, SetsKt___SetsKt.plus(contactSearchState.getExpandedSections(), sectionKey), null, 5, null);
    }

    public final void expandSection(ContactSearchConfiguration.SectionKey sectionKey) {
        Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
        this.configurationStore.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1344expandSection$lambda3(ContactSearchConfiguration.SectionKey.this, (ContactSearchState) obj);
            }
        });
    }

    public final void setKeysSelected(Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKeys");
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.contactSearchRepository.filterOutUnselectableContactSearchKeys(set).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda10
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ContactSearchViewModel.m1348setKeysSelected$lambda8(ContactSearchViewModel.this, (Set) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "contactSearchRepository.…wSelectionEntries }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: setKeysSelected$lambda-8 */
    public static final void m1348setKeysSelected$lambda8(ContactSearchViewModel contactSearchViewModel, Set set) {
        Intrinsics.checkNotNullParameter(contactSearchViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(set, "results");
        boolean z = true;
        if (!(set instanceof Collection) || !set.isEmpty()) {
            Iterator it = set.iterator();
            while (it.hasNext()) {
                if (!((ContactSearchSelectionResult) it.next()).isSelectable()) {
                    break;
                }
            }
        }
        z = false;
        if (!z) {
            ArrayList<ContactSearchSelectionResult> arrayList = new ArrayList();
            for (Object obj : set) {
                if (((ContactSearchSelectionResult) obj).isSelectable()) {
                    arrayList.add(obj);
                }
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (ContactSearchSelectionResult contactSearchSelectionResult : arrayList) {
                arrayList2.add(contactSearchSelectionResult.getKey());
            }
            List list = CollectionsKt___CollectionsKt.minus((Iterable) arrayList2, (Iterable) contactSearchViewModel.getSelectedContacts());
            int size = list.size() + contactSearchViewModel.getSelectedContacts().size();
            if ((contactSearchViewModel.selectionLimits.hasRecommendedLimit() && contactSearchViewModel.getSelectedContacts().size() < contactSearchViewModel.selectionLimits.getRecommendedLimit() && size >= contactSearchViewModel.selectionLimits.getRecommendedLimit()) || !contactSearchViewModel.selectionLimits.hasHardLimit() || size <= contactSearchViewModel.selectionLimits.getHardLimit()) {
                contactSearchViewModel.selectionStore.update(new com.annimon.stream.function.Function(list) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda7
                    public final /* synthetic */ List f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj2) {
                        return ContactSearchViewModel.m1349setKeysSelected$lambda8$lambda7(this.f$0, (Set) obj2);
                    }
                });
            }
        }
    }

    /* renamed from: setKeysSelected$lambda-8$lambda-7 */
    public static final Set m1349setKeysSelected$lambda8$lambda7(List list, Set set) {
        Intrinsics.checkNotNullParameter(list, "$newSelectionEntries");
        Intrinsics.checkNotNullExpressionValue(set, "state");
        return SetsKt___SetsKt.plus(set, (Iterable) list);
    }

    /* renamed from: setKeysNotSelected$lambda-9 */
    public static final Set m1347setKeysNotSelected$lambda9(Set set, Set set2) {
        Intrinsics.checkNotNullParameter(set, "$contactSearchKeys");
        Intrinsics.checkNotNullExpressionValue(set2, "it");
        return SetsKt___SetsKt.minus(set2, (Iterable) set);
    }

    public final void setKeysNotSelected(Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(set, "contactSearchKeys");
        this.selectionStore.update(new com.annimon.stream.function.Function(set) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda6
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1347setKeysNotSelected$lambda9(this.f$0, (Set) obj);
            }
        });
    }

    public final Set<ContactSearchKey> getSelectedContacts() {
        Set<ContactSearchKey> state = this.selectionStore.getState();
        Intrinsics.checkNotNullExpressionValue(state, "selectionStore.state");
        return state;
    }

    public final void addToVisibleGroupStories(Set<ContactSearchKey.RecipientSearchKey.Story> set) {
        Intrinsics.checkNotNullParameter(set, "groupStories");
        this.configurationStore.update(new com.annimon.stream.function.Function(set) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1340addToVisibleGroupStories$lambda11(this.f$0, (ContactSearchState) obj);
            }
        });
    }

    /* renamed from: addToVisibleGroupStories$lambda-11 */
    public static final ContactSearchState m1340addToVisibleGroupStories$lambda11(Set set, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(set, "$groupStories");
        Intrinsics.checkNotNullExpressionValue(contactSearchState, "state");
        Set<ContactSearchData.Story> groupStories = contactSearchState.getGroupStories();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        Iterator it = set.iterator();
        while (it.hasNext()) {
            Recipient resolved = Recipient.resolved(((ContactSearchKey.RecipientSearchKey.Story) it.next()).getRecipientId());
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(it.recipientId)");
            arrayList.add(new ContactSearchData.Story(resolved, resolved.getParticipantIds().size()));
        }
        return ContactSearchState.copy$default(contactSearchState, null, null, SetsKt___SetsKt.plus((Set) groupStories, (Iterable) arrayList), 3, null);
    }

    public final void removeGroupStory(ContactSearchData.Story story) {
        Intrinsics.checkNotNullParameter(story, "story");
        Preconditions.checkArgument(story.getRecipient().isGroup());
        setKeysNotSelected(SetsKt__SetsJVMKt.setOf(story.getContactSearchKey()));
        CompositeDisposable compositeDisposable = this.disposables;
        ContactSearchRepository contactSearchRepository = this.contactSearchRepository;
        GroupId requireGroupId = story.getRecipient().requireGroupId();
        Intrinsics.checkNotNullExpressionValue(requireGroupId, "story.recipient.requireGroupId()");
        Disposable subscribe = contactSearchRepository.unmarkDisplayAsStory(requireGroupId).subscribe(new Action(story) { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ ContactSearchData.Story f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                ContactSearchViewModel.m1345removeGroupStory$lambda14(ContactSearchViewModel.this, this.f$1);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "contactSearchRepository.…  }\n      refresh()\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: removeGroupStory$lambda-14 */
    public static final void m1345removeGroupStory$lambda14(ContactSearchViewModel contactSearchViewModel, ContactSearchData.Story story) {
        Intrinsics.checkNotNullParameter(contactSearchViewModel, "this$0");
        Intrinsics.checkNotNullParameter(story, "$story");
        contactSearchViewModel.configurationStore.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ContactSearchViewModel.m1346removeGroupStory$lambda14$lambda13(ContactSearchData.Story.this, (ContactSearchState) obj);
            }
        });
        contactSearchViewModel.refresh();
    }

    /* renamed from: removeGroupStory$lambda-14$lambda-13 */
    public static final ContactSearchState m1346removeGroupStory$lambda14$lambda13(ContactSearchData.Story story, ContactSearchState contactSearchState) {
        Intrinsics.checkNotNullParameter(story, "$story");
        Intrinsics.checkNotNullExpressionValue(contactSearchState, "state");
        Set<ContactSearchData.Story> groupStories = contactSearchState.getGroupStories();
        ArrayList arrayList = new ArrayList();
        for (Object obj : groupStories) {
            if (Intrinsics.areEqual(((ContactSearchData.Story) obj).getRecipient().getId(), story.getRecipient().getId())) {
                arrayList.add(obj);
            }
        }
        return ContactSearchState.copy$default(contactSearchState, null, null, CollectionsKt___CollectionsKt.toSet(arrayList), 3, null);
    }

    public final void deletePrivateStory(ContactSearchData.Story story) {
        Intrinsics.checkNotNullParameter(story, "story");
        Preconditions.checkArgument(story.getRecipient().isDistributionList() && !story.getRecipient().isMyStory());
        setKeysNotSelected(SetsKt__SetsJVMKt.setOf(story.getContactSearchKey()));
        CompositeDisposable compositeDisposable = this.disposables;
        ContactSearchRepository contactSearchRepository = this.contactSearchRepository;
        DistributionListId requireDistributionListId = story.getRecipient().requireDistributionListId();
        Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "story.recipient.requireDistributionListId()");
        Disposable subscribe = contactSearchRepository.deletePrivateStory(requireDistributionListId).subscribe(new Action() { // from class: org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                ContactSearchViewModel.m1343deletePrivateStory$lambda15(ContactSearchViewModel.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "contactSearchRepository.…e {\n      refresh()\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: deletePrivateStory$lambda-15 */
    public static final void m1343deletePrivateStory$lambda15(ContactSearchViewModel contactSearchViewModel) {
        Intrinsics.checkNotNullParameter(contactSearchViewModel, "this$0");
        contactSearchViewModel.refresh();
    }

    public final void refresh() {
        PagingController<ContactSearchKey> value = this.controller.getValue();
        if (value != null) {
            value.onDataInvalidated();
        }
    }

    /* compiled from: ContactSearchViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "selectionLimits", "Lorg/thoughtcrime/securesms/groups/SelectionLimits;", "repository", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchRepository;", "(Lorg/thoughtcrime/securesms/groups/SelectionLimits;Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final ContactSearchRepository repository;
        private final SelectionLimits selectionLimits;

        public Factory(SelectionLimits selectionLimits, ContactSearchRepository contactSearchRepository) {
            Intrinsics.checkNotNullParameter(selectionLimits, "selectionLimits");
            Intrinsics.checkNotNullParameter(contactSearchRepository, "repository");
            this.selectionLimits = selectionLimits;
            this.repository = contactSearchRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ContactSearchViewModel(this.selectionLimits, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.contacts.paged.ContactSearchViewModel.Factory.create");
        }
    }
}
