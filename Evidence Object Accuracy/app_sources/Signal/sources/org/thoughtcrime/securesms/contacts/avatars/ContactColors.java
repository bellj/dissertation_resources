package org.thoughtcrime.securesms.contacts.avatars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.thoughtcrime.securesms.color.MaterialColor;

/* loaded from: classes4.dex */
public class ContactColors {
    private static final List<MaterialColor> CONVERSATION_PALETTE = new ArrayList(Arrays.asList(MaterialColor.PLUM, MaterialColor.CRIMSON, MaterialColor.VERMILLION, MaterialColor.VIOLET, MaterialColor.BLUE, MaterialColor.INDIGO, MaterialColor.FOREST, MaterialColor.WINTERGREEN, MaterialColor.TEAL, MaterialColor.BURLAP, MaterialColor.TAUPE, MaterialColor.ULTRAMARINE));
    public static final MaterialColor UNKNOWN_COLOR = MaterialColor.STEEL;

    public static MaterialColor generateFor(String str) {
        List<MaterialColor> list = CONVERSATION_PALETTE;
        return list.get(Math.abs(str.hashCode()) % list.size());
    }
}
