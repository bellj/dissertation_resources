package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import com.airbnb.lottie.SimpleColorFilter;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatar;
import org.thoughtcrime.securesms.avatar.AvatarRenderer;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.util.NameUtil;

/* loaded from: classes4.dex */
public class GeneratedContactPhoto implements FallbackContactPhoto {
    private final int fallbackResId;
    private final String name;
    private final int targetSize;

    public GeneratedContactPhoto(String str, int i) {
        this(str, i, -1);
    }

    public GeneratedContactPhoto(String str, int i, int i2) {
        this.name = str;
        this.fallbackResId = i;
        this.targetSize = i2;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor) {
        return asDrawable(context, avatarColor, false);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor, boolean z) {
        int i = this.targetSize;
        if (i <= 0) {
            i = context.getResources().getDimensionPixelSize(R.dimen.contact_photo_target_size);
        }
        String abbreviation = NameUtil.getAbbreviation(this.name);
        if (TextUtils.isEmpty(abbreviation)) {
            return newFallbackDrawable(context, avatarColor, z);
        }
        Avatars.ForegroundColor foregroundColor = Avatars.getForegroundColor(avatarColor);
        Drawable createTextDrawable = AvatarRenderer.createTextDrawable(context, new Avatar.Text(abbreviation, new Avatars.ColorPair(avatarColor, foregroundColor), Avatar.DatabaseId.DoNotPersist.INSTANCE), z, i, false);
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.circle_tintable);
        Objects.requireNonNull(drawable);
        drawable.setColorFilter(new SimpleColorFilter(z ? foregroundColor.getColorInt() : avatarColor.colorInt()));
        return new LayerDrawable(new Drawable[]{drawable, createTextDrawable});
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asSmallDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return asDrawable(context, avatarColor, z);
    }

    public int getFallbackResId() {
        return this.fallbackResId;
    }

    protected Drawable newFallbackDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return new ResourceContactPhoto(this.fallbackResId).asDrawable(context, avatarColor, z);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asCallCard(Context context) {
        return AppCompatResources.getDrawable(context, R.drawable.ic_person_large);
    }
}
