package org.thoughtcrime.securesms.contacts.paged;

import android.view.View;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchItems$ExpandViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContactSearchItems.ExpandViewHolder f$0;
    public final /* synthetic */ ContactSearchItems.ExpandModel f$1;

    public /* synthetic */ ContactSearchItems$ExpandViewHolder$$ExternalSyntheticLambda0(ContactSearchItems.ExpandViewHolder expandViewHolder, ContactSearchItems.ExpandModel expandModel) {
        this.f$0 = expandViewHolder;
        this.f$1 = expandModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ContactSearchItems.ExpandViewHolder.m1315bind$lambda0(this.f$0, this.f$1, view);
    }
}
