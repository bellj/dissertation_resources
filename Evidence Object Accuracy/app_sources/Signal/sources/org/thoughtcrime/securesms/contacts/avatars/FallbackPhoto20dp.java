package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.graphics.drawable.DrawableCompat;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.Avatars;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class FallbackPhoto20dp implements FallbackContactPhoto {
    private final int drawable20dp;

    public FallbackPhoto20dp(int i) {
        this.drawable20dp = i;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor) {
        return buildDrawable(context, avatarColor);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return buildDrawable(context, avatarColor);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asSmallDrawable(Context context, AvatarColor avatarColor, boolean z) {
        return buildDrawable(context, avatarColor);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto
    public Drawable asCallCard(Context context) {
        throw new UnsupportedOperationException();
    }

    private Drawable buildDrawable(Context context, AvatarColor avatarColor) {
        Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.circle_tintable);
        Objects.requireNonNull(drawable);
        Drawable mutate = DrawableCompat.wrap(drawable).mutate();
        Drawable drawable2 = AppCompatResources.getDrawable(context, this.drawable20dp);
        Objects.requireNonNull(drawable2);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{mutate, drawable2});
        int dpToPx = ViewUtil.dpToPx(2);
        DrawableCompat.setTint(mutate, avatarColor.colorInt());
        DrawableCompat.setTint(drawable2, Avatars.getForegroundColor(avatarColor).getColorInt());
        layerDrawable.setLayerInset(1, dpToPx, dpToPx, dpToPx, dpToPx);
        return layerDrawable;
    }
}
