package org.thoughtcrime.securesms.contacts.paged;

import android.os.Parcel;
import android.os.Parcelable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.ShareContact;

/* compiled from: ContactSearchKey.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0005\u0007\b\t\n\u000bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\b\u0010\u0005\u001a\u00020\u0006H\u0016\u0001\u0003\f\r\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "", "()V", "requireParcelable", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "requireShareContact", "Lorg/thoughtcrime/securesms/sharing/ShareContact;", "Expand", "Header", "ParcelableRecipientSearchKey", "ParcelableType", "RecipientSearchKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$Header;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$Expand;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ContactSearchKey {

    /* compiled from: ContactSearchKey.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableType;", "", "(Ljava/lang/String;I)V", "STORY", "KNOWN_RECIPIENT", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum ParcelableType {
        STORY,
        KNOWN_RECIPIENT
    }

    public /* synthetic */ ContactSearchKey(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    private ContactSearchKey() {
    }

    public ShareContact requireShareContact() {
        throw new IllegalStateException("This key cannot be converted into a ShareContact".toString());
    }

    public ParcelableRecipientSearchKey requireParcelable() {
        throw new IllegalStateException("This key cannot be parcelized".toString());
    }

    /* compiled from: ContactSearchKey.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005R\u0012\u0010\u0006\u001a\u00020\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\b\u0010\t\u0001\u0002\f\r¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "()V", "isStory", "", "()Z", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "KnownRecipient", "Story", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey$Story;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey$KnownRecipient;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class RecipientSearchKey extends ContactSearchKey {
        public /* synthetic */ RecipientSearchKey(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public abstract RecipientId getRecipientId();

        public abstract boolean isStory();

        private RecipientSearchKey() {
            super(null);
        }

        /* compiled from: ContactSearchKey.kt */
        @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\u00062\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey$Story;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "isStory", "", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "other", "", "hashCode", "", "requireParcelable", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "requireShareContact", "Lorg/thoughtcrime/securesms/sharing/ShareContact;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Story extends RecipientSearchKey {
            private final boolean isStory = true;
            private final RecipientId recipientId;

            public static /* synthetic */ Story copy$default(Story story, RecipientId recipientId, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = story.getRecipientId();
                }
                return story.copy(recipientId);
            }

            public final RecipientId component1() {
                return getRecipientId();
            }

            public final Story copy(RecipientId recipientId) {
                Intrinsics.checkNotNullParameter(recipientId, "recipientId");
                return new Story(recipientId);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Story) && Intrinsics.areEqual(getRecipientId(), ((Story) obj).getRecipientId());
            }

            public int hashCode() {
                return getRecipientId().hashCode();
            }

            public String toString() {
                return "Story(recipientId=" + getRecipientId() + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Story(RecipientId recipientId) {
                super(null);
                Intrinsics.checkNotNullParameter(recipientId, "recipientId");
                this.recipientId = recipientId;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey.RecipientSearchKey
            public RecipientId getRecipientId() {
                return this.recipientId;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey
            public ShareContact requireShareContact() {
                return new ShareContact(getRecipientId());
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey
            public ParcelableRecipientSearchKey requireParcelable() {
                return new ParcelableRecipientSearchKey(ParcelableType.STORY, getRecipientId());
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey.RecipientSearchKey
            public boolean isStory() {
                return this.isStory;
            }
        }

        /* compiled from: ContactSearchKey.kt */
        @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\u00062\b\u0010\r\u001a\u0004\u0018\u00010\u000eHÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0014H\u0016J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0014\u0010\u0005\u001a\u00020\u0006XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0007R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey$KnownRecipient;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "isStory", "", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "copy", "equals", "other", "", "hashCode", "", "requireParcelable", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "requireShareContact", "Lorg/thoughtcrime/securesms/sharing/ShareContact;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class KnownRecipient extends RecipientSearchKey {
            private final boolean isStory;
            private final RecipientId recipientId;

            public static /* synthetic */ KnownRecipient copy$default(KnownRecipient knownRecipient, RecipientId recipientId, int i, Object obj) {
                if ((i & 1) != 0) {
                    recipientId = knownRecipient.getRecipientId();
                }
                return knownRecipient.copy(recipientId);
            }

            public final RecipientId component1() {
                return getRecipientId();
            }

            public final KnownRecipient copy(RecipientId recipientId) {
                Intrinsics.checkNotNullParameter(recipientId, "recipientId");
                return new KnownRecipient(recipientId);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof KnownRecipient) && Intrinsics.areEqual(getRecipientId(), ((KnownRecipient) obj).getRecipientId());
            }

            public int hashCode() {
                return getRecipientId().hashCode();
            }

            public String toString() {
                return "KnownRecipient(recipientId=" + getRecipientId() + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public KnownRecipient(RecipientId recipientId) {
                super(null);
                Intrinsics.checkNotNullParameter(recipientId, "recipientId");
                this.recipientId = recipientId;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey.RecipientSearchKey
            public RecipientId getRecipientId() {
                return this.recipientId;
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey
            public ShareContact requireShareContact() {
                return new ShareContact(getRecipientId());
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey
            public ParcelableRecipientSearchKey requireParcelable() {
                return new ParcelableRecipientSearchKey(ParcelableType.KNOWN_RECIPIENT, getRecipientId());
            }

            @Override // org.thoughtcrime.securesms.contacts.paged.ContactSearchKey.RecipientSearchKey
            public boolean isStory() {
                return this.isStory;
            }
        }
    }

    /* compiled from: ContactSearchKey.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$Header;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "sectionKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;)V", "getSectionKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Header extends ContactSearchKey {
        private final ContactSearchConfiguration.SectionKey sectionKey;

        public static /* synthetic */ Header copy$default(Header header, ContactSearchConfiguration.SectionKey sectionKey, int i, Object obj) {
            if ((i & 1) != 0) {
                sectionKey = header.sectionKey;
            }
            return header.copy(sectionKey);
        }

        public final ContactSearchConfiguration.SectionKey component1() {
            return this.sectionKey;
        }

        public final Header copy(ContactSearchConfiguration.SectionKey sectionKey) {
            Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
            return new Header(sectionKey);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Header) && this.sectionKey == ((Header) obj).sectionKey;
        }

        public int hashCode() {
            return this.sectionKey.hashCode();
        }

        public String toString() {
            return "Header(sectionKey=" + this.sectionKey + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Header(ContactSearchConfiguration.SectionKey sectionKey) {
            super(null);
            Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
            this.sectionKey = sectionKey;
        }

        public final ContactSearchConfiguration.SectionKey getSectionKey() {
            return this.sectionKey;
        }
    }

    /* compiled from: ContactSearchKey.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$Expand;", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "sectionKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;)V", "getSectionKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration$SectionKey;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Expand extends ContactSearchKey {
        private final ContactSearchConfiguration.SectionKey sectionKey;

        public static /* synthetic */ Expand copy$default(Expand expand, ContactSearchConfiguration.SectionKey sectionKey, int i, Object obj) {
            if ((i & 1) != 0) {
                sectionKey = expand.sectionKey;
            }
            return expand.copy(sectionKey);
        }

        public final ContactSearchConfiguration.SectionKey component1() {
            return this.sectionKey;
        }

        public final Expand copy(ContactSearchConfiguration.SectionKey sectionKey) {
            Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
            return new Expand(sectionKey);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Expand) && this.sectionKey == ((Expand) obj).sectionKey;
        }

        public int hashCode() {
            return this.sectionKey.hashCode();
        }

        public String toString() {
            return "Expand(sectionKey=" + this.sectionKey + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Expand(ContactSearchConfiguration.SectionKey sectionKey) {
            super(null);
            Intrinsics.checkNotNullParameter(sectionKey, "sectionKey");
            this.sectionKey = sectionKey;
        }

        public final ContactSearchConfiguration.SectionKey getSectionKey() {
            return this.sectionKey;
        }
    }

    /* compiled from: ContactSearchKey.kt */
    @Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\fJ\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u000f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\u0013\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\u0019\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0011HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "Landroid/os/Parcelable;", "type", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableType;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableType;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getType", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableType;", "asRecipientSearchKey", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "component1", "component2", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ParcelableRecipientSearchKey implements Parcelable {
        public static final Parcelable.Creator<ParcelableRecipientSearchKey> CREATOR = new Creator();
        private final RecipientId recipientId;
        private final ParcelableType type;

        /* compiled from: ContactSearchKey.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Creator implements Parcelable.Creator<ParcelableRecipientSearchKey> {
            @Override // android.os.Parcelable.Creator
            public final ParcelableRecipientSearchKey createFromParcel(Parcel parcel) {
                Intrinsics.checkNotNullParameter(parcel, "parcel");
                return new ParcelableRecipientSearchKey(ParcelableType.valueOf(parcel.readString()), (RecipientId) parcel.readParcelable(ParcelableRecipientSearchKey.class.getClassLoader()));
            }

            @Override // android.os.Parcelable.Creator
            public final ParcelableRecipientSearchKey[] newArray(int i) {
                return new ParcelableRecipientSearchKey[i];
            }
        }

        /* compiled from: ContactSearchKey.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[ParcelableType.values().length];
                iArr[ParcelableType.STORY.ordinal()] = 1;
                iArr[ParcelableType.KNOWN_RECIPIENT.ordinal()] = 2;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        public static /* synthetic */ ParcelableRecipientSearchKey copy$default(ParcelableRecipientSearchKey parcelableRecipientSearchKey, ParcelableType parcelableType, RecipientId recipientId, int i, Object obj) {
            if ((i & 1) != 0) {
                parcelableType = parcelableRecipientSearchKey.type;
            }
            if ((i & 2) != 0) {
                recipientId = parcelableRecipientSearchKey.recipientId;
            }
            return parcelableRecipientSearchKey.copy(parcelableType, recipientId);
        }

        public final ParcelableType component1() {
            return this.type;
        }

        public final RecipientId component2() {
            return this.recipientId;
        }

        public final ParcelableRecipientSearchKey copy(ParcelableType parcelableType, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(parcelableType, "type");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            return new ParcelableRecipientSearchKey(parcelableType, recipientId);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // java.lang.Object
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ParcelableRecipientSearchKey)) {
                return false;
            }
            ParcelableRecipientSearchKey parcelableRecipientSearchKey = (ParcelableRecipientSearchKey) obj;
            return this.type == parcelableRecipientSearchKey.type && Intrinsics.areEqual(this.recipientId, parcelableRecipientSearchKey.recipientId);
        }

        @Override // java.lang.Object
        public int hashCode() {
            return (this.type.hashCode() * 31) + this.recipientId.hashCode();
        }

        @Override // java.lang.Object
        public String toString() {
            return "ParcelableRecipientSearchKey(type=" + this.type + ", recipientId=" + this.recipientId + ')';
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "out");
            parcel.writeString(this.type.name());
            parcel.writeParcelable(this.recipientId, i);
        }

        public ParcelableRecipientSearchKey(ParcelableType parcelableType, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(parcelableType, "type");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.type = parcelableType;
            this.recipientId = recipientId;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final ParcelableType getType() {
            return this.type;
        }

        public final RecipientSearchKey asRecipientSearchKey() {
            int i = WhenMappings.$EnumSwitchMapping$0[this.type.ordinal()];
            if (i == 1) {
                return new RecipientSearchKey.Story(this.recipientId);
            }
            if (i == 2) {
                return new RecipientSearchKey.KnownRecipient(this.recipientId);
            }
            throw new NoWhenBranchMatchedException();
        }
    }
}
