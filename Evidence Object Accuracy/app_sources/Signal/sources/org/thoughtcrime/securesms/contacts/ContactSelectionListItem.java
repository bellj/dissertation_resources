package org.thoughtcrime.securesms.contacts;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import j$.util.Optional;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ContactSelectionListItem extends ConstraintLayout implements RecipientForeverObserver {
    private static final String TAG = Log.tag(ContactSelectionListItem.class);
    private BadgeImageView badge;
    private CheckBox checkBox;
    private String chipName;
    private String contactAbout;
    private String contactLabel;
    private String contactName;
    private String contactNumber;
    private AvatarImageView contactPhotoImage;
    private int contactType;
    private GlideRequests glideRequests;
    private TextView labelView;
    private FromTextView nameView;
    private String number;
    private TextView numberView;
    private LiveRecipient recipient;
    private View smsTag;

    public ContactSelectionListItem(Context context) {
        super(context);
    }

    public ContactSelectionListItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.contactPhotoImage = (AvatarImageView) findViewById(R.id.contact_photo_image);
        this.numberView = (TextView) findViewById(R.id.number);
        this.labelView = (TextView) findViewById(R.id.label);
        this.nameView = (FromTextView) findViewById(R.id.name);
        this.checkBox = (CheckBox) findViewById(R.id.check_box);
        this.smsTag = findViewById(R.id.sms_tag);
        this.badge = (BadgeImageView) findViewById(R.id.contact_badge);
        ViewUtil.setTextViewGravityStart(this.nameView, getContext());
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient != null) {
            liveRecipient.observeForever(this);
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        unbind();
    }

    public void set(GlideRequests glideRequests, RecipientId recipientId, int i, String str, String str2, String str3, String str4, boolean z) {
        this.glideRequests = glideRequests;
        this.number = str2;
        this.contactType = i;
        String str5 = str;
        this.contactName = str5;
        this.contactNumber = str2;
        this.contactLabel = str3;
        this.contactAbout = str4;
        int i2 = 8;
        if (i == 4 || i == 8) {
            this.recipient = null;
            this.contactPhotoImage.setAvatar(glideRequests, (Recipient) null, false);
        } else if (recipientId != null) {
            LiveRecipient liveRecipient = this.recipient;
            if (liveRecipient != null) {
                liveRecipient.lambda$asObservable$6(this);
            }
            LiveRecipient live = Recipient.live(recipientId);
            this.recipient = live;
            live.observeForever(this);
        }
        LiveRecipient liveRecipient2 = this.recipient;
        Recipient recipient = liveRecipient2 != null ? liveRecipient2.get() : null;
        if (recipient != null && !recipient.isResolving() && !recipient.isMyStory()) {
            str5 = recipient.getDisplayName(getContext());
            this.contactName = str5;
        } else if (this.recipient != null) {
            str5 = "";
        }
        if (recipient == null || recipient.isResolving() || recipient.isRegistered() || recipient.isDistributionList()) {
            this.smsTag.setVisibility(8);
        } else {
            this.smsTag.setVisibility(0);
        }
        if (recipient == null || recipient.isResolving()) {
            this.contactPhotoImage.setAvatar(glideRequests, (Recipient) null, false);
            setText(null, i, str5, str2, str3, str4);
        } else if (recipient.isMyStory()) {
            this.contactPhotoImage.setRecipient(Recipient.self(), false);
            setText(recipient, i, str5, str2, str3, str4);
        } else {
            this.contactPhotoImage.setAvatar(glideRequests, recipient, false);
            setText(recipient, i, str5, str2, str3, str4);
        }
        CheckBox checkBox = this.checkBox;
        if (z) {
            i2 = 0;
        }
        checkBox.setVisibility(i2);
        if (recipient == null || recipient.isSelf()) {
            this.badge.setBadge(null);
        } else {
            this.badge.setBadgeFromRecipient(recipient);
        }
    }

    public void setChecked(boolean z, boolean z2) {
        this.checkBox.setChecked(z);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.checkBox.setEnabled(z);
    }

    public void unbind() {
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this);
        }
    }

    private void setText(Recipient recipient, int i, String str, String str2, String str3, String str4) {
        String str5 = "";
        if (str2 == null || str2.isEmpty()) {
            this.nameView.setEnabled(false);
            this.numberView.setText(str5);
            this.labelView.setVisibility(8);
        } else if (recipient != null && recipient.isGroup()) {
            this.nameView.setEnabled(false);
            this.numberView.setText(getGroupMemberCount(recipient));
            this.labelView.setVisibility(8);
        } else if (i == 1) {
            TextView textView = this.numberView;
            if (!Util.isEmpty(str4)) {
                str2 = str4;
            }
            textView.setText(str2);
            this.nameView.setEnabled(true);
            this.labelView.setVisibility(8);
        } else if (i == 8) {
            TextView textView2 = this.numberView;
            textView2.setText("@" + str2);
            this.nameView.setEnabled(true);
            this.labelView.setText(str3);
            this.labelView.setVisibility(0);
        } else if (recipient == null || !recipient.isDistributionList()) {
            TextView textView3 = this.numberView;
            if (!Util.isEmpty(str4)) {
                str2 = str4;
            }
            textView3.setText(str2);
            this.nameView.setEnabled(true);
            TextView textView4 = this.labelView;
            if (str3 != null && !str3.equals("null")) {
                str5 = getResources().getString(R.string.ContactSelectionListItem__dot_s, str3);
            }
            textView4.setText(str5);
            this.labelView.setVisibility(0);
        } else {
            this.numberView.setText(getViewerCount(str2));
            this.labelView.setVisibility(8);
        }
        if (recipient != null) {
            this.nameView.setText(recipient);
            this.chipName = recipient.getShortDisplayName(getContext());
            return;
        }
        this.nameView.setText(str);
        this.chipName = str;
    }

    public String getNumber() {
        return this.number;
    }

    public String getChipName() {
        return this.chipName;
    }

    private String getGroupMemberCount(Recipient recipient) {
        if (recipient.isGroup()) {
            int size = recipient.getParticipantIds().size();
            return getContext().getResources().getQuantityString(R.plurals.contact_selection_list_item__number_of_members, size, Integer.valueOf(size));
        }
        throw new AssertionError();
    }

    private String getViewerCount(String str) {
        int parseInt = Integer.parseInt(str);
        return getContext().getResources().getQuantityString(R.plurals.contact_selection_list_item__number_of_viewers, parseInt, Integer.valueOf(parseInt));
    }

    public LiveRecipient getRecipient() {
        return this.recipient;
    }

    public boolean isUsernameType() {
        return this.contactType == 8;
    }

    public Optional<RecipientId> getRecipientId() {
        LiveRecipient liveRecipient = this.recipient;
        return liveRecipient != null ? Optional.of(liveRecipient.getId()) : Optional.empty();
    }

    @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
    public void onRecipientChanged(Recipient recipient) {
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient == null || !liveRecipient.getId().equals(recipient.getId())) {
            String str = TAG;
            StringBuilder sb = new StringBuilder();
            sb.append("Bad change! Local recipient doesn't match. Ignoring. Local: ");
            LiveRecipient liveRecipient2 = this.recipient;
            sb.append(liveRecipient2 == null ? "null" : liveRecipient2.getId());
            sb.append(", Changed: ");
            sb.append(recipient.getId());
            Log.w(str, sb.toString());
            return;
        }
        this.contactName = recipient.getDisplayName(getContext());
        this.contactAbout = recipient.getCombinedAboutAndEmoji();
        if (recipient.isGroup() && recipient.getGroupId().isPresent()) {
            this.contactNumber = recipient.getGroupId().get().toString();
        } else if (recipient.hasE164()) {
            this.contactNumber = PhoneNumberFormatter.prettyPrint(recipient.getE164().orElse(""));
        } else if (!recipient.isDistributionList()) {
            this.contactNumber = recipient.getEmail().orElse("");
        }
        int i = 0;
        if (recipient.isMyStory()) {
            this.contactPhotoImage.setRecipient(Recipient.self(), false);
        } else {
            this.contactPhotoImage.setAvatar(this.glideRequests, recipient, false);
        }
        setText(recipient, this.contactType, this.contactName, this.contactNumber, this.contactLabel, this.contactAbout);
        View view = this.smsTag;
        if (recipient.isRegistered() || recipient.isDistributionList()) {
            i = 8;
        }
        view.setVisibility(i);
        this.badge.setBadgeFromRecipient(recipient);
    }
}
