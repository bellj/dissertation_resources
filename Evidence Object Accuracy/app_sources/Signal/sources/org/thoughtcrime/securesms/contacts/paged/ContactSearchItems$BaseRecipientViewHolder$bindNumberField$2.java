package org.thoughtcrime.securesms.contacts.paged;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchItems;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ContactSearchItems.kt */
/* access modifiers changed from: package-private */
@Metadata(d1 = {"\u0000\u001a\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001\"\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u0002H\u00020\u0003\"\b\b\u0001\u0010\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "D", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchData;", "it", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchItems$BaseRecipientViewHolder$bindNumberField$2 extends Lambda implements Function1<Recipient, CharSequence> {
    final /* synthetic */ ContactSearchItems.BaseRecipientViewHolder<T, D> this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ContactSearchItems$BaseRecipientViewHolder$bindNumberField$2(ContactSearchItems.BaseRecipientViewHolder<T, D> baseRecipientViewHolder) {
        super(1);
        this.this$0 = baseRecipientViewHolder;
    }

    public final CharSequence invoke(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "it");
        if (recipient.isSelf()) {
            String string = this.this$0.context.getString(R.string.ConversationTitleView_you);
            Intrinsics.checkNotNullExpressionValue(string, "{\n              context.…leView_you)\n            }");
            return string;
        }
        String shortDisplayName = recipient.getShortDisplayName(this.this$0.context);
        Intrinsics.checkNotNullExpressionValue(shortDisplayName, "{\n              it.getSh…me(context)\n            }");
        return shortDisplayName;
    }
}
