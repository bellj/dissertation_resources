package org.thoughtcrime.securesms.contacts.paged;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ContactSearchSelectionResult.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000b\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u00052\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchSelectionResult;", "", "key", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "isSelectable", "", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;Z)V", "()Z", "getKey", "()Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactSearchSelectionResult {
    private final boolean isSelectable;
    private final ContactSearchKey key;

    public static /* synthetic */ ContactSearchSelectionResult copy$default(ContactSearchSelectionResult contactSearchSelectionResult, ContactSearchKey contactSearchKey, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            contactSearchKey = contactSearchSelectionResult.key;
        }
        if ((i & 2) != 0) {
            z = contactSearchSelectionResult.isSelectable;
        }
        return contactSearchSelectionResult.copy(contactSearchKey, z);
    }

    public final ContactSearchKey component1() {
        return this.key;
    }

    public final boolean component2() {
        return this.isSelectable;
    }

    public final ContactSearchSelectionResult copy(ContactSearchKey contactSearchKey, boolean z) {
        Intrinsics.checkNotNullParameter(contactSearchKey, "key");
        return new ContactSearchSelectionResult(contactSearchKey, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ContactSearchSelectionResult)) {
            return false;
        }
        ContactSearchSelectionResult contactSearchSelectionResult = (ContactSearchSelectionResult) obj;
        return Intrinsics.areEqual(this.key, contactSearchSelectionResult.key) && this.isSelectable == contactSearchSelectionResult.isSelectable;
    }

    public int hashCode() {
        int hashCode = this.key.hashCode() * 31;
        boolean z = this.isSelectable;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "ContactSearchSelectionResult(key=" + this.key + ", isSelectable=" + this.isSelectable + ')';
    }

    public ContactSearchSelectionResult(ContactSearchKey contactSearchKey, boolean z) {
        Intrinsics.checkNotNullParameter(contactSearchKey, "key");
        this.key = contactSearchKey;
        this.isSelectable = z;
    }

    public final ContactSearchKey getKey() {
        return this.key;
    }

    public final boolean isSelectable() {
        return this.isSelectable;
    }
}
