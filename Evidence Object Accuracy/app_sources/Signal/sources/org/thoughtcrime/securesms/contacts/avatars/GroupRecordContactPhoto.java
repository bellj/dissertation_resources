package org.thoughtcrime.securesms.contacts.avatars;

import android.content.Context;
import android.net.Uri;
import j$.util.Optional;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import org.signal.core.util.Conversions;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.profiles.AvatarHelper;

/* loaded from: classes4.dex */
public final class GroupRecordContactPhoto implements ContactPhoto {
    private final long avatarId;
    private final GroupId groupId;

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public Uri getUri(Context context) {
        return null;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public boolean isProfilePhoto() {
        return false;
    }

    public GroupRecordContactPhoto(GroupId groupId, long j) {
        this.groupId = groupId;
        this.avatarId = j;
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto
    public InputStream openInputStream(Context context) throws IOException {
        Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(this.groupId);
        if (group.isPresent() && AvatarHelper.hasAvatar(context, group.get().getRecipientId())) {
            return AvatarHelper.getAvatar(context, group.get().getRecipientId());
        }
        throw new IOException("No avatar for group: " + this.groupId);
    }

    @Override // org.thoughtcrime.securesms.contacts.avatars.ContactPhoto, com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        messageDigest.update(this.groupId.toString().getBytes());
        messageDigest.update(Conversions.longToByteArray(this.avatarId));
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (!(obj instanceof GroupRecordContactPhoto)) {
            return false;
        }
        GroupRecordContactPhoto groupRecordContactPhoto = (GroupRecordContactPhoto) obj;
        if (!this.groupId.equals(groupRecordContactPhoto.groupId) || this.avatarId != groupRecordContactPhoto.avatarId) {
            return false;
        }
        return true;
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return this.groupId.hashCode() ^ ((int) this.avatarId);
    }
}
