package org.thoughtcrime.securesms.contacts;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;

/* loaded from: classes4.dex */
public class TurnOffContactJoinedNotificationsActivity extends AppCompatActivity {
    private static final String EXTRA_THREAD_ID;

    public static Intent newIntent(Context context, long j) {
        Intent intent = new Intent(context, TurnOffContactJoinedNotificationsActivity.class);
        intent.putExtra("thread_id", j);
        return intent;
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        new AlertDialog.Builder(this).setMessage(R.string.TurnOffContactJoinedNotificationsActivity__turn_off_contact_joined_signal).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.contacts.TurnOffContactJoinedNotificationsActivity$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                TurnOffContactJoinedNotificationsActivity.$r8$lambda$5ZZ8hdfE5ckvXTSbmC_7hUPtqnA(TurnOffContactJoinedNotificationsActivity.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.contacts.TurnOffContactJoinedNotificationsActivity$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                TurnOffContactJoinedNotificationsActivity.$r8$lambda$sXlXMuFcVR3X2f133G56C7ONODk(TurnOffContactJoinedNotificationsActivity.this, dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onResume$0(DialogInterface dialogInterface, int i) {
        handlePositiveAction(dialogInterface);
    }

    public /* synthetic */ void lambda$onResume$1(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        finish();
    }

    private void handlePositiveAction(DialogInterface dialogInterface) {
        SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.contacts.TurnOffContactJoinedNotificationsActivity$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return TurnOffContactJoinedNotificationsActivity.$r8$lambda$hzObn_7kZROYyGr0XoNebOVVemU(TurnOffContactJoinedNotificationsActivity.this);
            }
        }, new SimpleTask.ForegroundTask(dialogInterface) { // from class: org.thoughtcrime.securesms.contacts.TurnOffContactJoinedNotificationsActivity$$ExternalSyntheticLambda1
            public final /* synthetic */ DialogInterface f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                TurnOffContactJoinedNotificationsActivity.m1304$r8$lambda$IrWLpnPGVK2IW_b45ZzvmMbZs(TurnOffContactJoinedNotificationsActivity.this, this.f$1, obj);
            }
        });
    }

    public /* synthetic */ Object lambda$handlePositiveAction$2() {
        MarkReadReceiver.process(this, SignalDatabase.threads().setRead(getIntent().getLongExtra("thread_id", -1), false));
        SignalStore.settings().setNotifyWhenContactJoinsSignal(false);
        ApplicationDependencies.getMessageNotifier().updateNotification(this);
        return null;
    }

    public /* synthetic */ void lambda$handlePositiveAction$3(DialogInterface dialogInterface, Object obj) {
        dialogInterface.dismiss();
        finish();
    }
}
