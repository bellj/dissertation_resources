package org.thoughtcrime.securesms.contacts.paged;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchData;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ContactSearchMediator f$0;
    public final /* synthetic */ ContactSearchData.Story f$1;

    public /* synthetic */ ContactSearchMediator$StoryContextMenuCallbacks$$ExternalSyntheticLambda0(ContactSearchMediator contactSearchMediator, ContactSearchData.Story story) {
        this.f$0 = contactSearchMediator;
        this.f$1 = story;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        ContactSearchMediator.StoryContextMenuCallbacks.m1329onDeletePrivateStory$lambda2(this.f$0, this.f$1, dialogInterface, i);
    }
}
