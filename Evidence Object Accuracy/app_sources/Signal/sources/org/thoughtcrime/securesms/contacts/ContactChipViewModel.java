package org.thoughtcrime.securesms.contacts;

import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.thoughtcrime.securesms.contacts.SelectedContacts;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: ContactChipViewModel.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018J\u0016\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\t0\u001a2\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0014J\u000e\u0010\u001c\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u001a\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u0014X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/contacts/ContactChipViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", NewHtcHomeBadger.COUNT, "", "getCount", "()I", "disposableMap", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lio/reactivex/rxjava3/disposables/Disposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "", "Lorg/thoughtcrime/securesms/contacts/SelectedContacts$Model;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "add", "", "selectedContact", "Lorg/thoughtcrime/securesms/contacts/SelectedContact;", "getOrCreateRecipientId", "Lio/reactivex/rxjava3/core/Single;", "onCleared", "remove", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContactChipViewModel extends ViewModel {
    private final int count;
    private final Map<RecipientId, Disposable> disposableMap = new LinkedHashMap();
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Flowable<List<SelectedContacts.Model>> state;
    private final RxStore<List<SelectedContacts.Model>> store;

    public ContactChipViewModel() {
        RxStore<List<SelectedContacts.Model>> rxStore = new RxStore<>(CollectionsKt__CollectionsKt.emptyList(), null, 2, null);
        this.store = rxStore;
        Flowable<List<SelectedContacts.Model>> observeOn = rxStore.getStateFlowable().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "store.stateFlowable\n    …dSchedulers.mainThread())");
        this.state = observeOn;
        this.count = rxStore.getState().size();
    }

    public final Flowable<List<SelectedContacts.Model>> getState() {
        return this.state;
    }

    public final int getCount() {
        return this.count;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
        for (Disposable disposable : this.disposableMap.values()) {
            disposable.dispose();
        }
        this.disposableMap.clear();
    }

    /* renamed from: add$lambda-1 */
    public static final Recipient m1296add$lambda1(RecipientId recipientId) {
        return Recipient.resolved(recipientId);
    }

    public final void add(SelectedContact selectedContact) {
        Intrinsics.checkNotNullParameter(selectedContact, "selectedContact");
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = getOrCreateRecipientId(selectedContact).map(new Function() { // from class: org.thoughtcrime.securesms.contacts.ContactChipViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ContactChipViewModel.m1296add$lambda1((RecipientId) obj);
            }
        }).observeOn(Schedulers.io()).subscribe(new Consumer(selectedContact) { // from class: org.thoughtcrime.securesms.contacts.ContactChipViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ SelectedContact f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ContactChipViewModel.m1297add$lambda2(ContactChipViewModel.this, this.f$1, (Recipient) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "getOrCreateRecipientId(s…}\n        }\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: add$lambda-2 */
    public static final void m1297add$lambda2(ContactChipViewModel contactChipViewModel, SelectedContact selectedContact, Recipient recipient) {
        Intrinsics.checkNotNullParameter(contactChipViewModel, "this$0");
        Intrinsics.checkNotNullParameter(selectedContact, "$selectedContact");
        contactChipViewModel.store.update(new Function1<List<? extends SelectedContacts.Model>, List<? extends SelectedContacts.Model>>(selectedContact, recipient) { // from class: org.thoughtcrime.securesms.contacts.ContactChipViewModel$add$2$1
            final /* synthetic */ Recipient $recipient;
            final /* synthetic */ SelectedContact $selectedContact;

            /* access modifiers changed from: package-private */
            {
                this.$selectedContact = r1;
                this.$recipient = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ List<? extends SelectedContacts.Model> invoke(List<? extends SelectedContacts.Model> list) {
                return invoke((List<SelectedContacts.Model>) list);
            }

            public final List<SelectedContacts.Model> invoke(List<SelectedContacts.Model> list) {
                Intrinsics.checkNotNullParameter(list, "it");
                SelectedContact selectedContact2 = this.$selectedContact;
                Recipient recipient2 = this.$recipient;
                Intrinsics.checkNotNullExpressionValue(recipient2, RecipientDatabase.TABLE_NAME);
                return CollectionsKt___CollectionsKt.plus((Collection<? extends SelectedContacts.Model>) ((Collection<? extends Object>) list), new SelectedContacts.Model(selectedContact2, recipient2));
            }
        });
        Disposable disposable = contactChipViewModel.disposableMap.get(recipient.getId());
        if (disposable != null) {
            disposable.dispose();
        }
        Map<RecipientId, Disposable> map = contactChipViewModel.disposableMap;
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        RxStore<List<SelectedContacts.Model>> rxStore = contactChipViewModel.store;
        Flowable<Recipient> flowable = recipient.live().asObservable().toFlowable(BackpressureStrategy.LATEST);
        Intrinsics.checkNotNullExpressionValue(flowable, "recipient.live().asObser…kpressureStrategy.LATEST)");
        map.put(id, rxStore.update(flowable, new Function2<Recipient, List<? extends SelectedContacts.Model>, List<? extends SelectedContacts.Model>>(selectedContact) { // from class: org.thoughtcrime.securesms.contacts.ContactChipViewModel$add$2$2
            final /* synthetic */ SelectedContact $selectedContact;

            /* access modifiers changed from: package-private */
            {
                this.$selectedContact = r1;
            }

            public final List<SelectedContacts.Model> invoke(Recipient recipient2, List<SelectedContacts.Model> list) {
                Intrinsics.checkNotNullParameter(list, "state");
                SelectedContact selectedContact2 = this.$selectedContact;
                Iterator<SelectedContacts.Model> it = list.iterator();
                int i = 0;
                while (true) {
                    if (!it.hasNext()) {
                        i = -1;
                        break;
                    } else if (it.next().getSelectedContact().matches(selectedContact2)) {
                        break;
                    } else {
                        i++;
                    }
                }
                if (i == 0) {
                    SelectedContact selectedContact3 = this.$selectedContact;
                    Intrinsics.checkNotNullExpressionValue(recipient2, "changedRecipient");
                    return CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new SelectedContacts.Model(selectedContact3, recipient2)), (Iterable) CollectionsKt___CollectionsKt.drop(list, i + 1));
                } else if (i <= 0) {
                    return list;
                } else {
                    List list2 = CollectionsKt___CollectionsKt.take(list, i);
                    SelectedContact selectedContact4 = this.$selectedContact;
                    Intrinsics.checkNotNullExpressionValue(recipient2, "changedRecipient");
                    return CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt___CollectionsKt.plus((Collection<? extends SelectedContacts.Model>) ((Collection<? extends Object>) list2), new SelectedContacts.Model(selectedContact4, recipient2)), (Iterable) CollectionsKt___CollectionsKt.drop(list, i + 1));
                }
            }
        }));
    }

    public final void remove(SelectedContact selectedContact) {
        Intrinsics.checkNotNullParameter(selectedContact, "selectedContact");
        this.store.update(new Function1<List<? extends SelectedContacts.Model>, List<? extends SelectedContacts.Model>>(selectedContact) { // from class: org.thoughtcrime.securesms.contacts.ContactChipViewModel$remove$1
            final /* synthetic */ SelectedContact $selectedContact;

            /* access modifiers changed from: package-private */
            {
                this.$selectedContact = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ List<? extends SelectedContacts.Model> invoke(List<? extends SelectedContacts.Model> list) {
                return invoke((List<SelectedContacts.Model>) list);
            }

            public final List<SelectedContacts.Model> invoke(List<SelectedContacts.Model> list) {
                Intrinsics.checkNotNullParameter(list, "list");
                SelectedContact selectedContact2 = this.$selectedContact;
                ArrayList arrayList = new ArrayList();
                for (Object obj : list) {
                    if (!((SelectedContacts.Model) obj).getSelectedContact().matches(selectedContact2)) {
                        arrayList.add(obj);
                    }
                }
                return arrayList;
            }
        });
    }

    private final Single<RecipientId> getOrCreateRecipientId(SelectedContact selectedContact) {
        Single<RecipientId> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.contacts.ContactChipViewModel$$ExternalSyntheticLambda2
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ContactChipViewModel.m1298getOrCreateRecipientId$lambda3(SelectedContact.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n      sel…s.getApplication())\n    }");
        return fromCallable;
    }

    /* renamed from: getOrCreateRecipientId$lambda-3 */
    public static final RecipientId m1298getOrCreateRecipientId$lambda3(SelectedContact selectedContact) {
        Intrinsics.checkNotNullParameter(selectedContact, "$selectedContact");
        return selectedContact.getOrCreateRecipientId(ApplicationDependencies.getApplication());
    }
}
