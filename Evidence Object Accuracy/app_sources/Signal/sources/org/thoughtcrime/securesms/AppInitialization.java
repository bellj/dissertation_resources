package org.thoughtcrime.securesms;

import android.content.Context;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.insights.InsightsOptOut;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobs.EmojiSearchIndexDownloadJob;
import org.thoughtcrime.securesms.jobs.StickerPackDownloadJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.stickers.BlessedPacks;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes.dex */
public final class AppInitialization {
    private static final String TAG = Log.tag(AppInitialization.class);

    private AppInitialization() {
    }

    public static void onFirstEverAppLaunch(Context context) {
        Log.i(TAG, "onFirstEverAppLaunch()");
        InsightsOptOut.userRequestedOptOut(context);
        TextSecurePreferences.setAppMigrationVersion(context, 62);
        TextSecurePreferences.setJobManagerVersion(context, 8);
        TextSecurePreferences.setLastVersionCode(context, Util.getCanonicalVersionCode());
        TextSecurePreferences.setHasSeenStickerIntroTooltip(context, true);
        TextSecurePreferences.setPasswordDisabled(context, true);
        TextSecurePreferences.setReadReceiptsEnabled(context, true);
        TextSecurePreferences.setTypingIndicatorsEnabled(context, true);
        TextSecurePreferences.setHasSeenWelcomeScreen(context, false);
        ApplicationDependencies.getMegaphoneRepository().onFirstEverAppLaunch();
        SignalStore.onFirstEverAppLaunch();
        JobManager jobManager = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack = BlessedPacks.ZOZO;
        jobManager.add(StickerPackDownloadJob.forInstall(pack.getPackId(), pack.getPackKey(), false));
        JobManager jobManager2 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack2 = BlessedPacks.BANDIT;
        jobManager2.add(StickerPackDownloadJob.forInstall(pack2.getPackId(), pack2.getPackKey(), false));
        JobManager jobManager3 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack3 = BlessedPacks.DAY_BY_DAY;
        jobManager3.add(StickerPackDownloadJob.forInstall(pack3.getPackId(), pack3.getPackKey(), false));
        JobManager jobManager4 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack4 = BlessedPacks.SWOON_HANDS;
        jobManager4.add(StickerPackDownloadJob.forReference(pack4.getPackId(), pack4.getPackKey()));
        JobManager jobManager5 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack5 = BlessedPacks.SWOON_FACES;
        jobManager5.add(StickerPackDownloadJob.forReference(pack5.getPackId(), pack5.getPackKey()));
    }

    public static void onPostBackupRestore(Context context) {
        Log.i(TAG, "onPostBackupRestore()");
        ApplicationDependencies.getMegaphoneRepository().onFirstEverAppLaunch();
        SignalStore.onPostBackupRestore();
        SignalStore.onFirstEverAppLaunch();
        SignalStore.onboarding().clearAll();
        TextSecurePreferences.onPostBackupRestore(context);
        TextSecurePreferences.setPasswordDisabled(context, true);
        JobManager jobManager = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack = BlessedPacks.ZOZO;
        jobManager.add(StickerPackDownloadJob.forInstall(pack.getPackId(), pack.getPackKey(), false));
        JobManager jobManager2 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack2 = BlessedPacks.BANDIT;
        jobManager2.add(StickerPackDownloadJob.forInstall(pack2.getPackId(), pack2.getPackKey(), false));
        JobManager jobManager3 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack3 = BlessedPacks.DAY_BY_DAY;
        jobManager3.add(StickerPackDownloadJob.forInstall(pack3.getPackId(), pack3.getPackKey(), false));
        JobManager jobManager4 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack4 = BlessedPacks.SWOON_HANDS;
        jobManager4.add(StickerPackDownloadJob.forReference(pack4.getPackId(), pack4.getPackKey()));
        JobManager jobManager5 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack5 = BlessedPacks.SWOON_FACES;
        jobManager5.add(StickerPackDownloadJob.forReference(pack5.getPackId(), pack5.getPackKey()));
        EmojiSearchIndexDownloadJob.scheduleImmediately();
    }

    public static void onRepairFirstEverAppLaunch(Context context) {
        Log.w(TAG, "onRepairFirstEverAppLaunch()");
        InsightsOptOut.userRequestedOptOut(context);
        TextSecurePreferences.setAppMigrationVersion(context, 62);
        TextSecurePreferences.setJobManagerVersion(context, 8);
        TextSecurePreferences.setLastVersionCode(context, Util.getCanonicalVersionCode());
        TextSecurePreferences.setHasSeenStickerIntroTooltip(context, true);
        TextSecurePreferences.setPasswordDisabled(context, true);
        ApplicationDependencies.getMegaphoneRepository().onFirstEverAppLaunch();
        SignalStore.onFirstEverAppLaunch();
        JobManager jobManager = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack = BlessedPacks.ZOZO;
        jobManager.add(StickerPackDownloadJob.forInstall(pack.getPackId(), pack.getPackKey(), false));
        JobManager jobManager2 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack2 = BlessedPacks.BANDIT;
        jobManager2.add(StickerPackDownloadJob.forInstall(pack2.getPackId(), pack2.getPackKey(), false));
        JobManager jobManager3 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack3 = BlessedPacks.DAY_BY_DAY;
        jobManager3.add(StickerPackDownloadJob.forInstall(pack3.getPackId(), pack3.getPackKey(), false));
        JobManager jobManager4 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack4 = BlessedPacks.SWOON_HANDS;
        jobManager4.add(StickerPackDownloadJob.forReference(pack4.getPackId(), pack4.getPackKey()));
        JobManager jobManager5 = ApplicationDependencies.getJobManager();
        BlessedPacks.Pack pack5 = BlessedPacks.SWOON_FACES;
        jobManager5.add(StickerPackDownloadJob.forReference(pack5.getPackId(), pack5.getPackKey()));
    }
}
