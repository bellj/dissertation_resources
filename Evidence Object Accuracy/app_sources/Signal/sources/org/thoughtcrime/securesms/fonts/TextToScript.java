package org.thoughtcrime.securesms.fonts;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: TextToScript.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010J\u001a\u0010\u0011\u001a\u00020\u0012*\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R \u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TextToScript;", "", "()V", "ARABIC_RANGES", "", "Lkotlin/ranges/IntRange;", "CJK_JAPANESE_RANGES", "CJK_RANGES", "CYRILLIC_RANGES", "DEVANAGARI_RANGES", "LATIN_RANGES", "allRanges", "", "Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "guessScript", DraftDatabase.Draft.TEXT, "", "contains", "", "x", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextToScript {
    private static final List<IntRange> ARABIC_RANGES;
    private static final List<IntRange> CJK_JAPANESE_RANGES;
    private static final List<IntRange> CJK_RANGES;
    private static final List<IntRange> CYRILLIC_RANGES;
    private static final List<IntRange> DEVANAGARI_RANGES;
    public static final TextToScript INSTANCE = new TextToScript();
    private static final List<IntRange> LATIN_RANGES;
    private static final Map<SupportedScript, List<IntRange>> allRanges;

    private TextToScript() {
    }

    static {
        INSTANCE = new TextToScript();
        List<IntRange> list = CollectionsKt__CollectionsKt.listOf((Object[]) new IntRange[]{new IntRange(0, 591), new IntRange(7680, 7935), new IntRange(11360, 11391), new IntRange(42784, 43007), new IntRange(43824, 43887)});
        LATIN_RANGES = list;
        List<IntRange> list2 = CollectionsKt__CollectionsKt.listOf((Object[]) new IntRange[]{new IntRange(1024, 1279), new IntRange(1280, 1327), new IntRange(7296, 7311), new IntRange(11744, 11775), new IntRange(42560, 42655)});
        CYRILLIC_RANGES = list2;
        List<IntRange> list3 = CollectionsKt__CollectionsKt.listOf((Object[]) new IntRange[]{new IntRange(2304, 2431), new IntRange(43232, 43263), new IntRange(12448, 12543)});
        DEVANAGARI_RANGES = list3;
        List<IntRange> list4 = CollectionsKt__CollectionsKt.listOf((Object[]) new IntRange[]{new IntRange(12736, 12783), new IntRange(13056, 13311), new IntRange(19968, 40959), new IntRange(63744, 64255), new IntRange(65072, 65103), new IntRange(131072, 191471), new IntRange(194560, 195103)});
        CJK_RANGES = list4;
        List<IntRange> list5 = CollectionsKt__CollectionsKt.listOf((Object[]) new IntRange[]{new IntRange(12352, 12447), new IntRange(12448, 12543), new IntRange(12688, 12703)});
        CJK_JAPANESE_RANGES = list5;
        List<IntRange> list6 = CollectionsKt__CollectionsKt.listOf((Object[]) new IntRange[]{new IntRange(1536, 1791), new IntRange(1872, 1919), new IntRange(2160, 2207), new IntRange(2208, 2303)});
        ARABIC_RANGES = list6;
        allRanges = MapsKt__MapsKt.mapOf(TuplesKt.to(SupportedScript.LATIN, list), TuplesKt.to(SupportedScript.CYRILLIC, list2), TuplesKt.to(SupportedScript.DEVANAGARI, list3), TuplesKt.to(SupportedScript.UNKNOWN_CJK, list4), TuplesKt.to(SupportedScript.JAPANESE, list5), TuplesKt.to(SupportedScript.ARABIC, list6));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v13 */
    public final SupportedScript guessScript(CharSequence charSequence) {
        Map.Entry entry;
        SupportedScript supportedScript;
        Intrinsics.checkNotNullParameter(charSequence, DraftDatabase.Draft.TEXT);
        SupportedScript[] values = SupportedScript.values();
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(values.length), 16));
        for (SupportedScript supportedScript2 : values) {
            Pair pair = TuplesKt.to(supportedScript2, 0);
            linkedHashMap.put(pair.getFirst(), pair.getSecond());
        }
        Map map = MapsKt__MapsKt.toMutableMap(linkedHashMap);
        String obj = charSequence.toString();
        int codePointCount = obj.codePointCount(0, obj.length());
        for (int i = 0; i < codePointCount; i++) {
            int codePointAt = obj.codePointAt(i);
            for (Map.Entry<SupportedScript, List<IntRange>> entry2 : allRanges.entrySet()) {
                SupportedScript key = entry2.getKey();
                if (contains(entry2.getValue(), codePointAt)) {
                    Object obj2 = map.get(key);
                    Intrinsics.checkNotNull(obj2);
                    map.put(key, Integer.valueOf(((Number) obj2).intValue() + 1));
                }
            }
        }
        Iterator it = map.entrySet().iterator();
        if (!it.hasNext()) {
            entry = null;
        } else {
            Object next = it.next();
            if (!it.hasNext()) {
                entry = next;
            } else {
                int intValue = ((Number) ((Map.Entry) next).getValue()).intValue();
                do {
                    Object next2 = it.next();
                    int intValue2 = ((Number) ((Map.Entry) next2).getValue()).intValue();
                    if (intValue < intValue2) {
                        next = next2;
                        intValue = intValue2;
                    }
                } while (it.hasNext());
                entry = next;
            }
        }
        Map.Entry entry3 = entry;
        if (entry3 == null || (supportedScript = (SupportedScript) entry3.getKey()) == null) {
            supportedScript = SupportedScript.UNKNOWN;
        }
        if (supportedScript != SupportedScript.UNKNOWN_CJK) {
            return supportedScript;
        }
        SupportedScript supportedScript3 = SupportedScript.JAPANESE;
        Object obj3 = map.get(supportedScript3);
        Intrinsics.checkNotNull(obj3);
        return ((Number) obj3).intValue() > 0 ? supportedScript3 : supportedScript;
    }

    private final boolean contains(List<IntRange> list, int i) {
        boolean z;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            for (IntRange intRange : list) {
                int first = intRange.getFirst();
                if (i > intRange.getLast() || first > i) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    return true;
                }
            }
        }
        return false;
    }
}
