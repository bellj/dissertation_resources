package org.thoughtcrime.securesms.fonts;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.imageeditor.core.Renderer;
import org.signal.imageeditor.core.RendererContext;

/* compiled from: FontTypefaceProvider.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u000e\u001a\u00020\u0006H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontTypefaceProvider;", "Lorg/signal/imageeditor/core/RendererContext$TypefaceProvider;", "()V", "cachedLocale", "Ljava/util/Locale;", "cachedTypeface", "Landroid/graphics/Typeface;", "getSelectedTypeface", "context", "Landroid/content/Context;", "renderer", "Lorg/signal/imageeditor/core/Renderer;", "invalidate", "Lorg/signal/imageeditor/core/RendererContext$Invalidate;", "getTypeface", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FontTypefaceProvider implements RendererContext.TypefaceProvider {
    private Locale cachedLocale;
    private Typeface cachedTypeface;

    @Override // org.signal.imageeditor.core.RendererContext.TypefaceProvider
    public Typeface getSelectedTypeface(Context context, Renderer renderer, RendererContext.Invalidate invalidate) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(renderer, "renderer");
        Intrinsics.checkNotNullParameter(invalidate, "invalidate");
        return getTypeface();
    }

    private final Typeface getTypeface() {
        if (Build.VERSION.SDK_INT < 26) {
            Typeface create = Typeface.create(Typeface.DEFAULT, 1);
            Intrinsics.checkNotNullExpressionValue(create, "{\n      Typeface.create(…ULT, Typeface.BOLD)\n    }");
            return create;
        }
        Typeface build = new Typeface.Builder("").setFallback("sans-serif").setWeight(900).build();
        Intrinsics.checkNotNullExpressionValue(build, "{\n      Typeface.Builder…0)\n        .build()\n    }");
        return build;
    }
}
