package org.thoughtcrime.securesms.fonts;

import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.databaseprotos.StoryTextPost;

/* compiled from: TextFont.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u000e\b\u0001\u0018\u0000 \u00152\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0015B)\b\u0002\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013j\u0002\b\u0014¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TextFont;", "", "icon", "", "fallbackFamily", "", "fallbackStyle", "isAllCaps", "", "(Ljava/lang/String;IILjava/lang/String;IZ)V", "getFallbackFamily", "()Ljava/lang/String;", "getFallbackStyle", "()I", "getIcon", "()Z", "REGULAR", "BOLD", "SERIF", "SCRIPT", "CONDENSED", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum TextFont {
    REGULAR(R.drawable.ic_font_regular, "sans-serif", 0, false),
    BOLD(R.drawable.ic_font_bold, "sans-serif", 1, false),
    SERIF(R.drawable.ic_font_serif, "serif", 0, false),
    SCRIPT(R.drawable.ic_font_script, "serif", 1, false),
    CONDENSED(R.drawable.ic_font_condensed, "sans-serif", 1, true);
    
    public static final Companion Companion = new Companion(null);
    private final String fallbackFamily;
    private final int fallbackStyle;
    private final int icon;
    private final boolean isAllCaps;

    TextFont(int i, String str, int i2, boolean z) {
        this.icon = i;
        this.fallbackFamily = str;
        this.fallbackStyle = i2;
        this.isAllCaps = z;
    }

    public final String getFallbackFamily() {
        return this.fallbackFamily;
    }

    public final int getFallbackStyle() {
        return this.fallbackStyle;
    }

    public final int getIcon() {
        return this.icon;
    }

    public final boolean isAllCaps() {
        return this.isAllCaps;
    }

    /* compiled from: TextFont.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TextFont$Companion;", "", "()V", "fromStyle", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "style", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/StoryTextPost$Style;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {

        /* compiled from: TextFont.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[StoryTextPost.Style.values().length];
                iArr[StoryTextPost.Style.DEFAULT.ordinal()] = 1;
                iArr[StoryTextPost.Style.REGULAR.ordinal()] = 2;
                iArr[StoryTextPost.Style.BOLD.ordinal()] = 3;
                iArr[StoryTextPost.Style.SERIF.ordinal()] = 4;
                iArr[StoryTextPost.Style.SCRIPT.ordinal()] = 5;
                iArr[StoryTextPost.Style.CONDENSED.ordinal()] = 6;
                iArr[StoryTextPost.Style.UNRECOGNIZED.ordinal()] = 7;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final TextFont fromStyle(StoryTextPost.Style style) {
            Intrinsics.checkNotNullParameter(style, "style");
            switch (WhenMappings.$EnumSwitchMapping$0[style.ordinal()]) {
                case 1:
                    return TextFont.REGULAR;
                case 2:
                    return TextFont.REGULAR;
                case 3:
                    return TextFont.BOLD;
                case 4:
                    return TextFont.SERIF;
                case 5:
                    return TextFont.SCRIPT;
                case 6:
                    return TextFont.CONDENSED;
                case 7:
                    return TextFont.REGULAR;
                default:
                    throw new NoWhenBranchMatchedException();
            }
        }
    }
}
