package org.thoughtcrime.securesms.fonts;

import android.graphics.Typeface;
import android.os.Build;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: TypefaceHelper.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u000b\fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TypefaceHelper;", "", "()V", "typefaceFor", "Landroid/graphics/Typeface;", "family", "Lorg/thoughtcrime/securesms/fonts/TypefaceHelper$Family;", "weightName", "", "weight", "Lorg/thoughtcrime/securesms/fonts/TypefaceHelper$Weight;", "Family", "Weight", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TypefaceHelper {
    public static final TypefaceHelper INSTANCE = new TypefaceHelper();

    private TypefaceHelper() {
    }

    public final Typeface typefaceFor(Family family, String str, Weight weight) {
        Intrinsics.checkNotNullParameter(family, "family");
        Intrinsics.checkNotNullParameter(str, "weightName");
        Intrinsics.checkNotNullParameter(weight, "weight");
        int i = Build.VERSION.SDK_INT;
        int i2 = 0;
        if (i >= 28) {
            Typeface create = Typeface.create(Typeface.create(family.getFamilyName(), 0), weight.getValue(), false);
            Intrinsics.checkNotNullExpressionValue(create, "create(\n        Typeface…ue,\n        false\n      )");
            return create;
        } else if (i >= 21) {
            Typeface create2 = Typeface.create(family.getFamilyName() + '-' + str, 0);
            Intrinsics.checkNotNullExpressionValue(create2, "create(\"${family.familyN…htName\", Typeface.NORMAL)");
            return create2;
        } else {
            String familyName = family.getFamilyName();
            if (weight.getValue() > Weight.MEDIUM.getValue()) {
                i2 = 1;
            }
            Typeface create3 = Typeface.create(familyName, i2);
            Intrinsics.checkNotNullExpressionValue(create3, "create(family.familyName…OLD else Typeface.NORMAL)");
            return create3;
        }
    }

    /* compiled from: TypefaceHelper.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TypefaceHelper$Family;", "", "familyName", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getFamilyName", "()Ljava/lang/String;", "SANS_SERIF", "SERIF", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Family {
        SANS_SERIF("sans-serif"),
        SERIF("serif");
        
        private final String familyName;

        Family(String str) {
            this.familyName = str;
        }

        public final String getFamilyName() {
            return this.familyName;
        }
    }

    /* compiled from: TypefaceHelper.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u000e\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TypefaceHelper$Weight;", "", DraftDatabase.DRAFT_VALUE, "", "(Ljava/lang/String;II)V", "getValue", "()I", "THIN", "EXTRA_LIGHT", "DEMI_LIGHT", "LIGHT", "NORMAL", "MEDIUM", "SEMI_BOLD", "BOLD", "EXTRA_BOLD", "BLACK", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Weight {
        THIN(100),
        EXTRA_LIGHT(200),
        DEMI_LIGHT(200),
        LIGHT(WebRtcCallView.PIP_RESIZE_DURATION),
        NORMAL(400),
        MEDIUM(500),
        SEMI_BOLD(600),
        BOLD(Stories.MAX_BODY_SIZE),
        EXTRA_BOLD(800),
        BLACK(900);
        
        private final int value;

        Weight(int i) {
            this.value = i;
        }

        public final int getValue() {
            return this.value;
        }
    }
}
