package org.thoughtcrime.securesms.fonts;

import java.io.File;
import java.io.FilenameFilter;
import org.thoughtcrime.securesms.fonts.FontVersion;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class FontVersion$Companion$$ExternalSyntheticLambda0 implements FilenameFilter {
    public final /* synthetic */ String f$0;

    public /* synthetic */ FontVersion$Companion$$ExternalSyntheticLambda0(String str) {
        this.f$0 = str;
    }

    @Override // java.io.FilenameFilter
    public final boolean accept(File file, String str) {
        return FontVersion.Companion.m1793cleanOldVersions$lambda3(this.f$0, file, str);
    }
}
