package org.thoughtcrime.securesms.fonts;

import android.content.Context;
import android.graphics.Typeface;
import j$.util.DesugarCollections;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.UUID;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.fonts.FontManifest;
import org.thoughtcrime.securesms.fonts.FontVersion;
import org.thoughtcrime.securesms.fonts.Fonts;
import org.thoughtcrime.securesms.fonts.TypefaceHelper;
import org.thoughtcrime.securesms.s3.S3;
import org.thoughtcrime.securesms.util.ListenableFutureTask;

/* compiled from: Fonts.kt */
@Metadata(d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u000201B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0004H\u0007J4\u0010\u0016\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u00142\u0006\u0010\u001c\u001a\u00020\u001dH\u0003J\b\u0010\u001e\u001a\u00020\u001fH\u0007J\u0018\u0010 \u001a\u00020\r2\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u000e\u0010!\u001a\u00020\"2\u0006\u0010\u0011\u001a\u00020\u0012J\u001a\u0010#\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010$\u001a\u00020%H\u0002J\u001c\u0010&\u001a\u00020\u00182\f\u0010'\u001a\b\u0012\u0004\u0012\u00020)0(2\u0006\u0010*\u001a\u00020\u0018J\"\u0010+\u001a\u0004\u0018\u00010\r2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u00142\u0006\u0010,\u001a\u00020\u0004H\u0003J \u0010-\u001a\u00020.2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J\u001c\u0010/\u001a\u0004\u0018\u00010%2\b\u0010\u0017\u001a\u0004\u0018\u00010\u00182\u0006\u0010\u001c\u001a\u00020\u001dH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n \u0007*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000Rf\u0010\t\u001aZ\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u000b0\u000b\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\r \u0007*\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f0\f \u0007*,\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u000b0\u000b\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\r \u0007*\n\u0012\u0004\u0012\u00020\r\u0018\u00010\f0\f\u0018\u00010\u000e0\nX\u0004¢\u0006\u0002\n\u0000¨\u00062"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/Fonts;", "", "()V", "BASE_STATIC_BUCKET_URI", "", "MANIFEST", "TAG", "kotlin.jvm.PlatformType", "VERSION_URI", "taskCache", "", "Lorg/thoughtcrime/securesms/fonts/Fonts$FontDownloadKey;", "Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "Landroid/graphics/Typeface;", "", "downloadAndVerifyLatestManifest", "", "context", "Landroid/content/Context;", "version", "Lorg/thoughtcrime/securesms/fonts/FontVersion;", "manifestPath", "downloadFont", "supportedScript", "Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "font", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "fontVersion", "fontManifest", "Lorg/thoughtcrime/securesms/fonts/FontManifest;", "downloadLatestVersionLong", "", "getDefaultFontForScriptAndStyle", "getDirectory", "Ljava/io/File;", "getScriptPath", "script", "Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;", "getSupportedScript", "locales", "", "Ljava/util/Locale;", "guessedScript", "loadFontIntoTypeface", "fontLocalPath", "resolveFont", "Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult;", "resolveFontScriptFromScriptName", "FontDownloadKey", "FontResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Fonts {
    private static final String BASE_STATIC_BUCKET_URI;
    public static final Fonts INSTANCE = new Fonts();
    private static final String MANIFEST;
    private static final String TAG = Log.tag(Fonts.class);
    private static final String VERSION_URI;
    private static final Map<FontDownloadKey, ListenableFutureTask<Typeface>> taskCache = DesugarCollections.synchronizedMap(new LinkedHashMap());

    /* compiled from: Fonts.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[TextFont.values().length];
            iArr[TextFont.REGULAR.ordinal()] = 1;
            iArr[TextFont.BOLD.ordinal()] = 2;
            iArr[TextFont.SERIF.ordinal()] = 3;
            iArr[TextFont.SCRIPT.ordinal()] = 4;
            iArr[TextFont.CONDENSED.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
            int[] iArr2 = new int[SupportedScript.values().length];
            iArr2[SupportedScript.CYRILLIC.ordinal()] = 1;
            iArr2[SupportedScript.DEVANAGARI.ordinal()] = 2;
            iArr2[SupportedScript.CHINESE_TRADITIONAL_HK.ordinal()] = 3;
            iArr2[SupportedScript.CHINESE_TRADITIONAL.ordinal()] = 4;
            iArr2[SupportedScript.CHINESE_SIMPLIFIED.ordinal()] = 5;
            iArr2[SupportedScript.UNKNOWN_CJK.ordinal()] = 6;
            iArr2[SupportedScript.ARABIC.ordinal()] = 7;
            iArr2[SupportedScript.JAPANESE.ordinal()] = 8;
            iArr2[SupportedScript.LATIN.ordinal()] = 9;
            iArr2[SupportedScript.UNKNOWN.ordinal()] = 10;
            $EnumSwitchMapping$1 = iArr2;
        }
    }

    private Fonts() {
    }

    public final File getDirectory(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        File dir = context.getDir("story-fonts", 0);
        Intrinsics.checkNotNullExpressionValue(dir, "context.getDir(\"story-fo…s\", Context.MODE_PRIVATE)");
        return dir;
    }

    public final FontResult resolveFont(Context context, TextFont textFont, SupportedScript supportedScript) {
        FontResult.Async async;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(textFont, "font");
        Intrinsics.checkNotNullParameter(supportedScript, "supportedScript");
        ThreadUtil.assertNotMainThread();
        synchronized (this) {
            Typeface create = Typeface.create(textFont.getFallbackFamily(), textFont.getFallbackStyle());
            Intrinsics.checkNotNullExpressionValue(create, "create(font.fallbackFamily, font.fallbackStyle)");
            FontResult.Immediate immediate = new FontResult.Immediate(create);
            FontVersion.Companion companion = FontVersion.Companion;
            FontVersion fontVersion = companion.get(context);
            if (Intrinsics.areEqual(fontVersion, companion.getNONE())) {
                return immediate;
            }
            FontManifest fontManifest = FontManifest.Companion.get(context, fontVersion);
            if (fontManifest == null) {
                return immediate;
            }
            String str = TAG;
            Log.d(str, "Loaded manifest.");
            Fonts fonts = INSTANCE;
            FontManifest.FontScript resolveFontScriptFromScriptName = fonts.resolveFontScriptFromScriptName(supportedScript, fontManifest);
            if (resolveFontScriptFromScriptName == null) {
                Log.d(str, "Manifest does not have an entry for " + supportedScript + ". Using default.");
                return new FontResult.Immediate(fonts.getDefaultFontForScriptAndStyle(supportedScript, textFont));
            }
            Log.d(str, "Loaded script for locale.");
            String scriptPath = fonts.getScriptPath(textFont, resolveFontScriptFromScriptName);
            if (scriptPath == null) {
                Log.d(str, "Manifest does not contain a network path for " + supportedScript + ". Using default.");
                return new FontResult.Immediate(fonts.getDefaultFontForScriptAndStyle(supportedScript, textFont));
            }
            String nameOnDisk = FontFileMap.Companion.getNameOnDisk(context, fontVersion, scriptPath);
            if (nameOnDisk != null) {
                Log.d(str, "Local font version found, returning immediate.");
                Typeface loadFontIntoTypeface = fonts.loadFontIntoTypeface(context, fontVersion, nameOnDisk);
                if (loadFontIntoTypeface == null) {
                    loadFontIntoTypeface = immediate.getTypeface();
                }
                return new FontResult.Immediate(loadFontIntoTypeface);
            }
            FontDownloadKey fontDownloadKey = new FontDownloadKey(fontVersion, supportedScript, textFont);
            Map<FontDownloadKey, ListenableFutureTask<Typeface>> map = taskCache;
            ListenableFutureTask<Typeface> listenableFutureTask = map.get(fontDownloadKey);
            if (listenableFutureTask != null) {
                Log.d(str, "Found a task in progress. Returning in-progress async.");
                async = new FontResult.Async(listenableFutureTask, immediate.getTypeface());
            } else {
                Log.d(str, "Could not find a task in progress. Returning new async.");
                ListenableFutureTask<Typeface> listenableFutureTask2 = new ListenableFutureTask<>(new Callable(context, supportedScript, textFont, fontVersion, fontManifest, immediate, fontDownloadKey) { // from class: org.thoughtcrime.securesms.fonts.Fonts$$ExternalSyntheticLambda0
                    public final /* synthetic */ Context f$0;
                    public final /* synthetic */ SupportedScript f$1;
                    public final /* synthetic */ TextFont f$2;
                    public final /* synthetic */ FontVersion f$3;
                    public final /* synthetic */ FontManifest f$4;
                    public final /* synthetic */ Fonts.FontResult.Immediate f$5;
                    public final /* synthetic */ Fonts.FontDownloadKey f$6;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                        this.f$5 = r6;
                        this.f$6 = r7;
                    }

                    @Override // java.util.concurrent.Callable
                    public final Object call() {
                        return Fonts.m1794resolveFont$lambda2$lambda1(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
                    }
                });
                Intrinsics.checkNotNullExpressionValue(map, "taskCache");
                map.put(fontDownloadKey, listenableFutureTask2);
                SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.fonts.Fonts$$ExternalSyntheticLambda1
                    @Override // java.lang.Runnable
                    public final void run() {
                        ListenableFutureTask.this.run();
                    }
                });
                async = new FontResult.Async(listenableFutureTask2, immediate.getTypeface());
            }
            return async;
        }
    }

    /* renamed from: resolveFont$lambda-2$lambda-1 */
    public static final Typeface m1794resolveFont$lambda2$lambda1(Context context, SupportedScript supportedScript, TextFont textFont, FontVersion fontVersion, FontManifest fontManifest, FontResult.Immediate immediate, FontDownloadKey fontDownloadKey) {
        Typeface typeface;
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(supportedScript, "$supportedScript");
        Intrinsics.checkNotNullParameter(textFont, "$font");
        Intrinsics.checkNotNullParameter(fontVersion, "$version");
        Intrinsics.checkNotNullParameter(fontManifest, "$manifest");
        Intrinsics.checkNotNullParameter(immediate, "$errorFallback");
        Intrinsics.checkNotNullParameter(fontDownloadKey, "$fontDownloadKey");
        Fonts fonts = INSTANCE;
        String downloadFont = fonts.downloadFont(context, supportedScript, textFont, fontVersion, fontManifest);
        String str = TAG;
        Log.d(str, "Finished download, " + downloadFont);
        if (downloadFont == null || (typeface = fonts.loadFontIntoTypeface(context, fontVersion, downloadFont)) == null) {
            typeface = immediate.getTypeface();
        }
        taskCache.remove(fontDownloadKey);
        return typeface;
    }

    private final Typeface getDefaultFontForScriptAndStyle(SupportedScript supportedScript, TextFont textFont) {
        Typeface typeface;
        switch (WhenMappings.$EnumSwitchMapping$1[supportedScript.ordinal()]) {
            case 1:
                int i = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
                if (i == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i == 2) {
                    typeface = Typeface.create(Typeface.SANS_SERIF, 1);
                } else if (i == 3) {
                    typeface = Typeface.SERIF;
                } else if (i == 4) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SERIF, "semibold", TypefaceHelper.Weight.SEMI_BOLD);
                } else if (i == 5) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "light", TypefaceHelper.Weight.LIGHT);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(typeface, "{\n        when (font) {\n….LIGHT)\n        }\n      }");
                break;
            case 2:
                int i2 = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
                if (i2 == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i2 == 2) {
                    typeface = Typeface.create(Typeface.SANS_SERIF, 1);
                } else if (i2 == 3) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i2 == 4) {
                    typeface = Typeface.create(Typeface.SANS_SERIF, 1);
                } else if (i2 == 5) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "light", TypefaceHelper.Weight.LIGHT);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(typeface, "{\n        when (font) {\n….LIGHT)\n        }\n      }");
                break;
            case 3:
            case 4:
            case 5:
            case 6:
                int i3 = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
                if (i3 == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i3 == 2) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "semibold", TypefaceHelper.Weight.SEMI_BOLD);
                } else if (i3 == 3) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "thin", TypefaceHelper.Weight.THIN);
                } else if (i3 == 4) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "light", TypefaceHelper.Weight.LIGHT);
                } else if (i3 == 5) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "demilight", TypefaceHelper.Weight.DEMI_LIGHT);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(typeface, "{\n        when (font) {\n…_LIGHT)\n        }\n      }");
                break;
            case 7:
                int i4 = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
                if (i4 == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i4 == 2) {
                    typeface = Typeface.create(Typeface.SANS_SERIF, 1);
                } else if (i4 == 3) {
                    typeface = Typeface.SERIF;
                } else if (i4 == 4) {
                    typeface = Typeface.create(Typeface.SERIF, 1);
                } else if (i4 == 5) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "black", TypefaceHelper.Weight.BLACK);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(typeface, "{\n        when (font) {\n….BLACK)\n        }\n      }");
                break;
            case 8:
                int i5 = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
                if (i5 == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i5 == 2) {
                    typeface = Typeface.create(Typeface.SANS_SERIF, 1);
                } else if (i5 == 3) {
                    typeface = Typeface.SERIF;
                } else if (i5 == 4) {
                    typeface = Typeface.create(Typeface.SERIF, 1);
                } else if (i5 == 5) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "medium", TypefaceHelper.Weight.MEDIUM);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(typeface, "{\n        when (font) {\n…MEDIUM)\n        }\n      }");
                break;
            case 9:
            case 10:
                int i6 = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
                if (i6 == 1) {
                    typeface = Typeface.SANS_SERIF;
                } else if (i6 == 2) {
                    typeface = Typeface.create(Typeface.SANS_SERIF, 1);
                } else if (i6 == 3) {
                    typeface = Typeface.SERIF;
                } else if (i6 == 4) {
                    typeface = Typeface.create(Typeface.SERIF, 1);
                } else if (i6 == 5) {
                    typeface = TypefaceHelper.INSTANCE.typefaceFor(TypefaceHelper.Family.SANS_SERIF, "black", TypefaceHelper.Weight.BLACK);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(typeface, "{\n        when (font) {\n….BLACK)\n        }\n      }");
                break;
            default:
                throw new NoWhenBranchMatchedException();
        }
        return typeface;
    }

    private final Typeface loadFontIntoTypeface(Context context, FontVersion fontVersion, String str) {
        try {
            File directory = getDirectory(context);
            return Typeface.createFromFile(new File(directory, fontVersion.getPath() + '/' + str));
        } catch (Exception unused) {
            Log.w(TAG, "Could not load typeface from disk.");
            return null;
        }
    }

    public final long downloadLatestVersionLong() {
        return S3.INSTANCE.getLong(VERSION_URI);
    }

    public final boolean downloadAndVerifyLatestManifest(Context context, FontVersion fontVersion, String str) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(fontVersion, "version");
        Intrinsics.checkNotNullParameter(str, "manifestPath");
        S3 s3 = S3.INSTANCE;
        return S3.verifyAndWriteToDisk$default(s3, context, "/static/story-fonts/" + fontVersion.getId() + "/manifest.json", new File(getDirectory(context), str), false, 8, null);
    }

    private final String downloadFont(Context context, SupportedScript supportedScript, TextFont textFont, FontVersion fontVersion, FontManifest fontManifest) {
        FontManifest.FontScript resolveFontScriptFromScriptName;
        String scriptPath;
        if (supportedScript == null || (resolveFontScriptFromScriptName = resolveFontScriptFromScriptName(supportedScript, fontManifest)) == null || (scriptPath = getScriptPath(textFont, resolveFontScriptFromScriptName)) == null) {
            return null;
        }
        String str = "/static/story-fonts/" + fontVersion.getId() + '/' + scriptPath;
        String uuid = UUID.randomUUID().toString();
        Intrinsics.checkNotNullExpressionValue(uuid, "randomUUID().toString()");
        if (S3.INSTANCE.verifyAndWriteToDisk(context, str, new File(getDirectory(context), fontVersion.getPath() + '/' + uuid), true)) {
            FontFileMap.Companion.put(context, fontVersion, uuid, scriptPath);
            return uuid;
        }
        Log.w(TAG, "Failed to download and verify font.");
        return null;
    }

    private final String getScriptPath(TextFont textFont, FontManifest.FontScript fontScript) {
        int i = WhenMappings.$EnumSwitchMapping$0[textFont.ordinal()];
        if (i == 1) {
            return fontScript.getRegular();
        }
        if (i == 2) {
            return fontScript.getBold();
        }
        if (i == 3) {
            return fontScript.getSerif();
        }
        if (i == 4) {
            return fontScript.getScript();
        }
        if (i == 5) {
            return fontScript.getCondensed();
        }
        throw new NoWhenBranchMatchedException();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final SupportedScript getSupportedScript(List<Locale> list, SupportedScript supportedScript) {
        SupportedScript supportedScript2;
        SupportedScript supportedScript3;
        Intrinsics.checkNotNullParameter(list, "locales");
        Intrinsics.checkNotNullParameter(supportedScript, "guessedScript");
        if (!(supportedScript == SupportedScript.UNKNOWN || supportedScript == SupportedScript.UNKNOWN_CJK)) {
            return supportedScript;
        }
        if (supportedScript == SupportedScript.UNKNOWN_CJK) {
            ArrayList arrayList = new ArrayList();
            for (Locale locale : list) {
                SupportedScript supportedScript4 = null;
                try {
                    String iSO3Country = locale.getISO3Country();
                    if (iSO3Country != null) {
                        switch (iSO3Country.hashCode()) {
                            case 66697:
                                if (!iSO3Country.equals("CHN")) {
                                    break;
                                } else {
                                    supportedScript3 = SupportedScript.CHINESE_SIMPLIFIED;
                                    supportedScript4 = supportedScript3;
                                    break;
                                }
                            case 71588:
                                if (!iSO3Country.equals("HKG")) {
                                    break;
                                } else {
                                    supportedScript3 = SupportedScript.CHINESE_TRADITIONAL_HK;
                                    supportedScript4 = supportedScript3;
                                    break;
                                }
                            case 73672:
                                if (!iSO3Country.equals("JPN")) {
                                    break;
                                } else {
                                    supportedScript3 = SupportedScript.JAPANESE;
                                    supportedScript4 = supportedScript3;
                                    break;
                                }
                            case 83499:
                                if (!iSO3Country.equals("TWN")) {
                                    break;
                                } else {
                                    supportedScript3 = SupportedScript.CHINESE_TRADITIONAL;
                                    supportedScript4 = supportedScript3;
                                    break;
                                }
                        }
                    }
                } catch (MissingResourceException unused) {
                    String str = TAG;
                    Log.w(str, "Unable to get ISO-3 country code for: " + locale);
                }
                if (supportedScript4 != null) {
                    arrayList.add(supportedScript4);
                }
            }
            SupportedScript supportedScript5 = (SupportedScript) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) arrayList));
            if (supportedScript5 != null) {
                return supportedScript5;
            }
        }
        Locale locale2 = (Locale) CollectionsKt___CollectionsKt.first((List) ((List<? extends Object>) list));
        String script = ScriptUtil.getScript(locale2);
        String str2 = TAG;
        Log.d(str2, "Getting Script for " + script);
        if (script != null) {
            switch (script.hashCode()) {
                case 2049074:
                    if (script.equals("Arab")) {
                        supportedScript2 = SupportedScript.ARABIC;
                        break;
                    }
                    break;
                case 2115920:
                    if (script.equals("Cyrl")) {
                        supportedScript2 = SupportedScript.CYRILLIC;
                        break;
                    }
                    break;
                case 2126604:
                    if (script.equals("Deva")) {
                        supportedScript2 = SupportedScript.DEVANAGARI;
                        break;
                    }
                    break;
                case 2241694:
                    if (script.equals("Hans")) {
                        supportedScript2 = SupportedScript.CHINESE_SIMPLIFIED;
                        break;
                    }
                    break;
                case 2241695:
                    if (script.equals("Hant")) {
                        supportedScript2 = SupportedScript.CHINESE_TRADITIONAL;
                        break;
                    }
                    break;
                case 2315283:
                    if (script.equals("Jpan")) {
                        supportedScript2 = SupportedScript.JAPANESE;
                        break;
                    }
                    break;
                case 2361039:
                    if (script.equals("Latn")) {
                        supportedScript2 = SupportedScript.LATIN;
                        break;
                    }
                    break;
            }
            return (supportedScript2 != SupportedScript.CHINESE_SIMPLIFIED || !Intrinsics.areEqual(locale2.getISO3Country(), "HKG")) ? supportedScript2 : SupportedScript.CHINESE_TRADITIONAL_HK;
        }
        supportedScript2 = SupportedScript.UNKNOWN;
        if (supportedScript2 != SupportedScript.CHINESE_SIMPLIFIED) {
            return supportedScript2;
        }
    }

    private final FontManifest.FontScript resolveFontScriptFromScriptName(SupportedScript supportedScript, FontManifest fontManifest) {
        String str = TAG;
        Log.d(str, "Getting Script for " + supportedScript);
        switch (supportedScript == null ? -1 : WhenMappings.$EnumSwitchMapping$1[supportedScript.ordinal()]) {
            case -1:
            case 6:
            case 10:
                return null;
            case 0:
            default:
                throw new NoWhenBranchMatchedException();
            case 1:
                return fontManifest.getScripts().getCyrillicExtended();
            case 2:
                return fontManifest.getScripts().getDevanagari();
            case 3:
                return fontManifest.getScripts().getChineseTraditionalHk();
            case 4:
                return fontManifest.getScripts().getChineseTraditional();
            case 5:
                return fontManifest.getScripts().getChineseSimplified();
            case 7:
                return fontManifest.getScripts().getArabic();
            case 8:
                return fontManifest.getScripts().getJapanese();
            case 9:
                return fontManifest.getScripts().getLatinExtended();
        }
    }

    /* compiled from: Fonts.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult;", "", "()V", "Async", "Immediate", "Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult$Immediate;", "Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult$Async;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class FontResult {
        public /* synthetic */ FontResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: Fonts.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult$Immediate;", "Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult;", "typeface", "Landroid/graphics/Typeface;", "(Landroid/graphics/Typeface;)V", "getTypeface", "()Landroid/graphics/Typeface;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Immediate extends FontResult {
            private final Typeface typeface;

            public static /* synthetic */ Immediate copy$default(Immediate immediate, Typeface typeface, int i, Object obj) {
                if ((i & 1) != 0) {
                    typeface = immediate.typeface;
                }
                return immediate.copy(typeface);
            }

            public final Typeface component1() {
                return this.typeface;
            }

            public final Immediate copy(Typeface typeface) {
                Intrinsics.checkNotNullParameter(typeface, "typeface");
                return new Immediate(typeface);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Immediate) && Intrinsics.areEqual(this.typeface, ((Immediate) obj).typeface);
            }

            public int hashCode() {
                return this.typeface.hashCode();
            }

            public String toString() {
                return "Immediate(typeface=" + this.typeface + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Immediate(Typeface typeface) {
                super(null);
                Intrinsics.checkNotNullParameter(typeface, "typeface");
                this.typeface = typeface;
            }

            public final Typeface getTypeface() {
                return this.typeface;
            }
        }

        private FontResult() {
        }

        /* compiled from: Fonts.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0004¢\u0006\u0002\u0010\u0006J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0004HÆ\u0003J#\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0004HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult$Async;", "Lorg/thoughtcrime/securesms/fonts/Fonts$FontResult;", "future", "Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "Landroid/graphics/Typeface;", "placeholder", "(Lorg/thoughtcrime/securesms/util/ListenableFutureTask;Landroid/graphics/Typeface;)V", "getFuture", "()Lorg/thoughtcrime/securesms/util/ListenableFutureTask;", "getPlaceholder", "()Landroid/graphics/Typeface;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Async extends FontResult {
            private final ListenableFutureTask<Typeface> future;
            private final Typeface placeholder;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.fonts.Fonts$FontResult$Async */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Async copy$default(Async async, ListenableFutureTask listenableFutureTask, Typeface typeface, int i, Object obj) {
                if ((i & 1) != 0) {
                    listenableFutureTask = async.future;
                }
                if ((i & 2) != 0) {
                    typeface = async.placeholder;
                }
                return async.copy(listenableFutureTask, typeface);
            }

            public final ListenableFutureTask<Typeface> component1() {
                return this.future;
            }

            public final Typeface component2() {
                return this.placeholder;
            }

            public final Async copy(ListenableFutureTask<Typeface> listenableFutureTask, Typeface typeface) {
                Intrinsics.checkNotNullParameter(listenableFutureTask, "future");
                Intrinsics.checkNotNullParameter(typeface, "placeholder");
                return new Async(listenableFutureTask, typeface);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Async)) {
                    return false;
                }
                Async async = (Async) obj;
                return Intrinsics.areEqual(this.future, async.future) && Intrinsics.areEqual(this.placeholder, async.placeholder);
            }

            public int hashCode() {
                return (this.future.hashCode() * 31) + this.placeholder.hashCode();
            }

            public String toString() {
                return "Async(future=" + this.future + ", placeholder=" + this.placeholder + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Async(ListenableFutureTask<Typeface> listenableFutureTask, Typeface typeface) {
                super(null);
                Intrinsics.checkNotNullParameter(listenableFutureTask, "future");
                Intrinsics.checkNotNullParameter(typeface, "placeholder");
                this.future = listenableFutureTask;
                this.placeholder = typeface;
            }

            public final ListenableFutureTask<Typeface> getFuture() {
                return this.future;
            }

            public final Typeface getPlaceholder() {
                return this.placeholder;
            }
        }
    }

    /* compiled from: Fonts.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/Fonts$FontDownloadKey;", "", "version", "Lorg/thoughtcrime/securesms/fonts/FontVersion;", "script", "Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "font", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "(Lorg/thoughtcrime/securesms/fonts/FontVersion;Lorg/thoughtcrime/securesms/fonts/SupportedScript;Lorg/thoughtcrime/securesms/fonts/TextFont;)V", "getFont", "()Lorg/thoughtcrime/securesms/fonts/TextFont;", "getScript", "()Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "getVersion", "()Lorg/thoughtcrime/securesms/fonts/FontVersion;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FontDownloadKey {
        private final TextFont font;
        private final SupportedScript script;
        private final FontVersion version;

        public static /* synthetic */ FontDownloadKey copy$default(FontDownloadKey fontDownloadKey, FontVersion fontVersion, SupportedScript supportedScript, TextFont textFont, int i, Object obj) {
            if ((i & 1) != 0) {
                fontVersion = fontDownloadKey.version;
            }
            if ((i & 2) != 0) {
                supportedScript = fontDownloadKey.script;
            }
            if ((i & 4) != 0) {
                textFont = fontDownloadKey.font;
            }
            return fontDownloadKey.copy(fontVersion, supportedScript, textFont);
        }

        public final FontVersion component1() {
            return this.version;
        }

        public final SupportedScript component2() {
            return this.script;
        }

        public final TextFont component3() {
            return this.font;
        }

        public final FontDownloadKey copy(FontVersion fontVersion, SupportedScript supportedScript, TextFont textFont) {
            Intrinsics.checkNotNullParameter(fontVersion, "version");
            Intrinsics.checkNotNullParameter(supportedScript, "script");
            Intrinsics.checkNotNullParameter(textFont, "font");
            return new FontDownloadKey(fontVersion, supportedScript, textFont);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FontDownloadKey)) {
                return false;
            }
            FontDownloadKey fontDownloadKey = (FontDownloadKey) obj;
            return Intrinsics.areEqual(this.version, fontDownloadKey.version) && this.script == fontDownloadKey.script && this.font == fontDownloadKey.font;
        }

        public int hashCode() {
            return (((this.version.hashCode() * 31) + this.script.hashCode()) * 31) + this.font.hashCode();
        }

        public String toString() {
            return "FontDownloadKey(version=" + this.version + ", script=" + this.script + ", font=" + this.font + ')';
        }

        public FontDownloadKey(FontVersion fontVersion, SupportedScript supportedScript, TextFont textFont) {
            Intrinsics.checkNotNullParameter(fontVersion, "version");
            Intrinsics.checkNotNullParameter(supportedScript, "script");
            Intrinsics.checkNotNullParameter(textFont, "font");
            this.version = fontVersion;
            this.script = supportedScript;
            this.font = textFont;
        }

        public final FontVersion getVersion() {
            return this.version;
        }

        public final SupportedScript getScript() {
            return this.script;
        }

        public final TextFont getFont() {
            return this.font;
        }
    }
}
