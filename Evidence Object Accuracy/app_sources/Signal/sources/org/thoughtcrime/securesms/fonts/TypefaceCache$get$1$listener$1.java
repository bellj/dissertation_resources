package org.thoughtcrime.securesms.fonts;

import android.graphics.Typeface;
import io.reactivex.rxjava3.core.SingleEmitter;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.fonts.Fonts;
import org.thoughtcrime.securesms.fonts.TypefaceCache;
import org.thoughtcrime.securesms.util.FutureTaskListener;

/* compiled from: TypefaceCache.kt */
@Metadata(d1 = {"\u0000\u001d\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003*\u0001\u0000\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"org/thoughtcrime/securesms/fonts/TypefaceCache$get$1$listener$1", "Lorg/thoughtcrime/securesms/util/FutureTaskListener;", "Landroid/graphics/Typeface;", "onFailure", "", "exception", "Ljava/util/concurrent/ExecutionException;", "onSuccess", "typeface", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TypefaceCache$get$1$listener$1 implements FutureTaskListener<Typeface> {
    final /* synthetic */ TypefaceCache.CacheKey $cacheKey;
    final /* synthetic */ SingleEmitter<Typeface> $emitter;
    final /* synthetic */ Fonts.FontResult $result;

    public TypefaceCache$get$1$listener$1(TypefaceCache.CacheKey cacheKey, SingleEmitter<Typeface> singleEmitter, Fonts.FontResult fontResult) {
        this.$cacheKey = cacheKey;
        this.$emitter = singleEmitter;
        this.$result = fontResult;
    }

    public void onSuccess(Typeface typeface) {
        Intrinsics.checkNotNullParameter(typeface, "typeface");
        Map map = TypefaceCache.cache;
        Intrinsics.checkNotNullExpressionValue(map, "cache");
        map.put(this.$cacheKey, typeface);
        this.$emitter.onSuccess(typeface);
    }

    @Override // org.thoughtcrime.securesms.util.FutureTaskListener
    public void onFailure(ExecutionException executionException) {
        Intrinsics.checkNotNullParameter(executionException, "exception");
        this.$emitter.onSuccess(((Fonts.FontResult.Async) this.$result).getPlaceholder());
    }
}
