package org.thoughtcrime.securesms.fonts;

import android.content.Context;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.ExtensionsKt;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.EncryptedStreamUtils;

/* compiled from: FontFileMap.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u0000 \u00102\u00020\u0001:\u0001\u0010B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0015\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u001f\u0010\t\u001a\u00020\u00002\u0014\b\u0002\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0004HÖ\u0001R\u001d\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontFileMap;", "", "map", "", "", "(Ljava/util/Map;)V", "getMap", "()Ljava/util/Map;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FontFileMap {
    public static final Companion Companion = new Companion(null);
    private static final String PATH;
    private static final String TAG = Log.tag(FontFileMap.class);
    private static final ObjectMapper objectMapper = ExtensionsKt.registerKotlinModule(new ObjectMapper());
    private final Map<String, String> map;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.fonts.FontFileMap */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ FontFileMap copy$default(FontFileMap fontFileMap, Map map, int i, Object obj) {
        if ((i & 1) != 0) {
            map = fontFileMap.map;
        }
        return fontFileMap.copy(map);
    }

    public final Map<String, String> component1() {
        return this.map;
    }

    public final FontFileMap copy(Map<String, String> map) {
        Intrinsics.checkNotNullParameter(map, "map");
        return new FontFileMap(map);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof FontFileMap) && Intrinsics.areEqual(this.map, ((FontFileMap) obj).map);
    }

    public int hashCode() {
        return this.map.hashCode();
    }

    public String toString() {
        return "FontFileMap(map=" + this.map + ')';
    }

    public FontFileMap(Map<String, String> map) {
        Intrinsics.checkNotNullParameter(map, "map");
        this.map = map;
    }

    public final Map<String, String> getMap() {
        return this.map;
    }

    /* compiled from: FontFileMap.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0003J\"\u0010\u000f\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0010\u001a\u00020\u0004H\u0007J(\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0004H\u0007J \u0010\u0014\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u0015\u001a\u00020\nH\u0003R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontFileMap$Companion;", "", "()V", "PATH", "", "TAG", "kotlin.jvm.PlatformType", "objectMapper", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "getMap", "Lorg/thoughtcrime/securesms/fonts/FontFileMap;", "context", "Landroid/content/Context;", "fontVersion", "Lorg/thoughtcrime/securesms/fonts/FontVersion;", "getNameOnDisk", "nameOnNetwork", "put", "", "nameOnDisk", "setMap", "fontFileMap", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void put(Context context, FontVersion fontVersion, String str, String str2) {
            FontFileMap fontFileMap;
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(fontVersion, "fontVersion");
            Intrinsics.checkNotNullParameter(str, "nameOnDisk");
            Intrinsics.checkNotNullParameter(str2, "nameOnNetwork");
            FontFileMap map = getMap(context, fontVersion);
            if (map == null) {
                Log.d(FontFileMap.TAG, "Creating a new font file map.");
                fontFileMap = new FontFileMap(MapsKt__MapsJVMKt.mapOf(TuplesKt.to(str2, str)));
            } else {
                Log.d(FontFileMap.TAG, "Modifying existing font file map.");
                fontFileMap = map.copy(MapsKt__MapsKt.plus(map.getMap(), TuplesKt.to(str2, str)));
            }
            setMap(context, fontVersion, fontFileMap);
        }

        public final String getNameOnDisk(Context context, FontVersion fontVersion, String str) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(fontVersion, "fontVersion");
            Intrinsics.checkNotNullParameter(str, "nameOnNetwork");
            FontFileMap map = getMap(context, fontVersion);
            if (map == null) {
                return null;
            }
            return map.getMap().get(str);
        }

        private final FontFileMap getMap(Context context, FontVersion fontVersion) {
            try {
                EncryptedStreamUtils encryptedStreamUtils = EncryptedStreamUtils.INSTANCE;
                File directory = Fonts.INSTANCE.getDirectory(context);
                InputStream inputStream = encryptedStreamUtils.getInputStream(context, new File(directory, fontVersion.getPath() + "/.map"));
                FontFileMap fontFileMap = (FontFileMap) FontFileMap.objectMapper.readValue(inputStream, FontFileMap.class);
                CloseableKt.closeFinally(inputStream, null);
                return fontFileMap;
            } catch (Exception unused) {
                Log.w(FontFileMap.TAG, "Couldn't read names file.");
                return null;
            }
        }

        private final void setMap(Context context, FontVersion fontVersion, FontFileMap fontFileMap) {
            try {
                EncryptedStreamUtils encryptedStreamUtils = EncryptedStreamUtils.INSTANCE;
                File directory = Fonts.INSTANCE.getDirectory(context);
                OutputStream outputStream = encryptedStreamUtils.getOutputStream(context, new File(directory, fontVersion.getPath() + "/.map"));
                FontFileMap.objectMapper.writeValue(outputStream, fontFileMap);
                Unit unit = Unit.INSTANCE;
                CloseableKt.closeFinally(outputStream, null);
            } catch (Exception unused) {
                Log.w(FontFileMap.TAG, "Couldn't write names file.");
            }
        }
    }
}
