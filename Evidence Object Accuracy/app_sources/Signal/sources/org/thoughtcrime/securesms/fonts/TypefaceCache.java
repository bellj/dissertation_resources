package org.thoughtcrime.securesms.fonts;

import android.content.Context;
import android.graphics.Typeface;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleEmitter;
import io.reactivex.rxjava3.core.SingleOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.DesugarCollections;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.fonts.Fonts;
import org.thoughtcrime.securesms.fonts.TypefaceCache;
import org.thoughtcrime.securesms.util.LocaleUtil;

/* compiled from: TypefaceCache.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J&\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00070\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u0010J\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u0010RN\u0010\u0003\u001aB\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007 \u0006* \u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\b0\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TypefaceCache;", "", "()V", "cache", "", "Lorg/thoughtcrime/securesms/fonts/TypefaceCache$CacheKey;", "kotlin.jvm.PlatformType", "Landroid/graphics/Typeface;", "", "get", "Lio/reactivex/rxjava3/core/Single;", "context", "Landroid/content/Context;", "font", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "guessedScript", "Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "warm", "", "script", "CacheKey", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TypefaceCache {
    public static final TypefaceCache INSTANCE = new TypefaceCache();
    private static final Map<CacheKey, Typeface> cache = DesugarCollections.synchronizedMap(new LinkedHashMap());

    private TypefaceCache() {
    }

    public final void warm(Context context, SupportedScript supportedScript) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(supportedScript, "script");
        Context applicationContext = context.getApplicationContext();
        TextFont[] values = TextFont.values();
        for (TextFont textFont : values) {
            TypefaceCache typefaceCache = INSTANCE;
            Intrinsics.checkNotNullExpressionValue(applicationContext, "appContext");
            typefaceCache.get(applicationContext, textFont, supportedScript).subscribe();
        }
    }

    public static /* synthetic */ Single get$default(TypefaceCache typefaceCache, Context context, TextFont textFont, SupportedScript supportedScript, int i, Object obj) {
        if ((i & 4) != 0) {
            supportedScript = SupportedScript.UNKNOWN;
        }
        return typefaceCache.get(context, textFont, supportedScript);
    }

    public final Single<Typeface> get(Context context, TextFont textFont, SupportedScript supportedScript) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(textFont, "font");
        Intrinsics.checkNotNullParameter(supportedScript, "guessedScript");
        SupportedScript supportedScript2 = Fonts.INSTANCE.getSupportedScript(LocaleUtil.INSTANCE.getLocaleDefaults(), supportedScript);
        CacheKey cacheKey = new CacheKey(supportedScript2, textFont);
        Typeface typeface = cache.get(cacheKey);
        Context applicationContext = context.getApplicationContext();
        if (typeface != null) {
            Single<Typeface> just = Single.just(typeface);
            Intrinsics.checkNotNullExpressionValue(just, "just(cachedValue)");
            return just;
        }
        Single<Typeface> subscribeOn = Single.create(new SingleOnSubscribe(applicationContext, textFont, supportedScript2, cacheKey) { // from class: org.thoughtcrime.securesms.fonts.TypefaceCache$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ TextFont f$1;
            public final /* synthetic */ SupportedScript f$2;
            public final /* synthetic */ TypefaceCache.CacheKey f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // io.reactivex.rxjava3.core.SingleOnSubscribe
            public final void subscribe(SingleEmitter singleEmitter) {
                TypefaceCache.m1795$r8$lambda$Y1ESv2sXx7mXHZ4yLu5FDEDukk(this.f$0, this.f$1, this.f$2, this.f$3, singleEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create<Typeface> { emitt…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: get$lambda-2 */
    public static final void m1796get$lambda2(Context context, TextFont textFont, SupportedScript supportedScript, CacheKey cacheKey, SingleEmitter singleEmitter) {
        Intrinsics.checkNotNullParameter(textFont, "$font");
        Intrinsics.checkNotNullParameter(supportedScript, "$supportedScript");
        Intrinsics.checkNotNullParameter(cacheKey, "$cacheKey");
        Fonts fonts = Fonts.INSTANCE;
        Intrinsics.checkNotNullExpressionValue(context, "appContext");
        Fonts.FontResult resolveFont = fonts.resolveFont(context, textFont, supportedScript);
        if (resolveFont instanceof Fonts.FontResult.Immediate) {
            Map<CacheKey, Typeface> map = cache;
            Intrinsics.checkNotNullExpressionValue(map, "cache");
            Fonts.FontResult.Immediate immediate = (Fonts.FontResult.Immediate) resolveFont;
            map.put(cacheKey, immediate.getTypeface());
            singleEmitter.onSuccess(immediate.getTypeface());
        } else if (resolveFont instanceof Fonts.FontResult.Async) {
            TypefaceCache$get$1$listener$1 typefaceCache$get$1$listener$1 = new TypefaceCache$get$1$listener$1(cacheKey, singleEmitter, resolveFont);
            ((Fonts.FontResult.Async) resolveFont).getFuture().addListener(typefaceCache$get$1$listener$1);
            singleEmitter.setCancellable(new Cancellable(typefaceCache$get$1$listener$1) { // from class: org.thoughtcrime.securesms.fonts.TypefaceCache$$ExternalSyntheticLambda0
                public final /* synthetic */ TypefaceCache$get$1$listener$1 f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Cancellable
                public final void cancel() {
                    TypefaceCache.$r8$lambda$vQdfgonw9LHxJq7bhAxkEAhLy8U(Fonts.FontResult.this, this.f$1);
                }
            });
        }
    }

    /* renamed from: get$lambda-2$lambda-1 */
    public static final void m1797get$lambda2$lambda1(Fonts.FontResult fontResult, TypefaceCache$get$1$listener$1 typefaceCache$get$1$listener$1) {
        Intrinsics.checkNotNullParameter(fontResult, "$result");
        Intrinsics.checkNotNullParameter(typefaceCache$get$1$listener$1, "$listener");
        ((Fonts.FontResult.Async) fontResult).getFuture().removeListener(typefaceCache$get$1$listener$1);
    }

    /* compiled from: TypefaceCache.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/TypefaceCache$CacheKey;", "", "script", "Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "font", "Lorg/thoughtcrime/securesms/fonts/TextFont;", "(Lorg/thoughtcrime/securesms/fonts/SupportedScript;Lorg/thoughtcrime/securesms/fonts/TextFont;)V", "getFont", "()Lorg/thoughtcrime/securesms/fonts/TextFont;", "getScript", "()Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CacheKey {
        private final TextFont font;
        private final SupportedScript script;

        public static /* synthetic */ CacheKey copy$default(CacheKey cacheKey, SupportedScript supportedScript, TextFont textFont, int i, Object obj) {
            if ((i & 1) != 0) {
                supportedScript = cacheKey.script;
            }
            if ((i & 2) != 0) {
                textFont = cacheKey.font;
            }
            return cacheKey.copy(supportedScript, textFont);
        }

        public final SupportedScript component1() {
            return this.script;
        }

        public final TextFont component2() {
            return this.font;
        }

        public final CacheKey copy(SupportedScript supportedScript, TextFont textFont) {
            Intrinsics.checkNotNullParameter(supportedScript, "script");
            Intrinsics.checkNotNullParameter(textFont, "font");
            return new CacheKey(supportedScript, textFont);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CacheKey)) {
                return false;
            }
            CacheKey cacheKey = (CacheKey) obj;
            return this.script == cacheKey.script && this.font == cacheKey.font;
        }

        public int hashCode() {
            return (this.script.hashCode() * 31) + this.font.hashCode();
        }

        public String toString() {
            return "CacheKey(script=" + this.script + ", font=" + this.font + ')';
        }

        public CacheKey(SupportedScript supportedScript, TextFont textFont) {
            Intrinsics.checkNotNullParameter(supportedScript, "script");
            Intrinsics.checkNotNullParameter(textFont, "font");
            this.script = supportedScript;
            this.font = textFont;
        }

        public final SupportedScript getScript() {
            return this.script;
        }

        public final TextFont getFont() {
            return this.font;
        }
    }
}
