package org.thoughtcrime.securesms.fonts;

import android.content.Context;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.ExtensionsKt;
import java.io.File;
import java.io.InputStream;
import kotlin.Metadata;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.EncryptedStreamUtils;

/* compiled from: FontManifest.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\b\u0018\u0000 \u00102\u00020\u0001:\u0003\u0010\u0011\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontManifest;", "", "scripts", "Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScripts;", "(Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScripts;)V", "getScripts", "()Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScripts;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "FontScript", "FontScripts", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FontManifest {
    public static final Companion Companion = new Companion(null);
    private static final String PATH;
    private static final String TAG = Log.tag(FontManifest.class);
    private static final ObjectMapper objectMapper = ExtensionsKt.registerKotlinModule(new ObjectMapper());
    private final FontScripts scripts;

    public static /* synthetic */ FontManifest copy$default(FontManifest fontManifest, FontScripts fontScripts, int i, Object obj) {
        if ((i & 1) != 0) {
            fontScripts = fontManifest.scripts;
        }
        return fontManifest.copy(fontScripts);
    }

    public final FontScripts component1() {
        return this.scripts;
    }

    public final FontManifest copy(FontScripts fontScripts) {
        Intrinsics.checkNotNullParameter(fontScripts, "scripts");
        return new FontManifest(fontScripts);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof FontManifest) && Intrinsics.areEqual(this.scripts, ((FontManifest) obj).scripts);
    }

    public int hashCode() {
        return this.scripts.hashCode();
    }

    public String toString() {
        return "FontManifest(scripts=" + this.scripts + ')';
    }

    public FontManifest(FontScripts fontScripts) {
        Intrinsics.checkNotNullParameter(fontScripts, "scripts");
        this.scripts = fontScripts;
    }

    public final FontScripts getScripts() {
        return this.scripts;
    }

    /* compiled from: FontManifest.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B_\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000bJ\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0017\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0018\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0003HÆ\u0003Ji\u0010\u001d\u001a\u00020\u00002\n\b\u0003\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\u0007\u001a\u0004\u0018\u00010\u00032\n\b\u0003\u0010\b\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010!\u001a\u00020\"HÖ\u0001J\t\u0010#\u001a\u00020$HÖ\u0001R\u0013\u0010\t\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\rR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\rR\u0013\u0010\n\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\rR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\r¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScripts;", "", "latinExtended", "Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;", "cyrillicExtended", "devanagari", "chineseTraditionalHk", "chineseTraditional", "chineseSimplified", "arabic", "japanese", "(Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;)V", "getArabic", "()Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;", "getChineseSimplified", "getChineseTraditional", "getChineseTraditionalHk", "getCyrillicExtended", "getDevanagari", "getJapanese", "getLatinExtended", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes.dex */
    public static final class FontScripts {
        private final FontScript arabic;
        private final FontScript chineseSimplified;
        private final FontScript chineseTraditional;
        private final FontScript chineseTraditionalHk;
        private final FontScript cyrillicExtended;
        private final FontScript devanagari;
        private final FontScript japanese;
        private final FontScript latinExtended;

        public final FontScript component1() {
            return this.latinExtended;
        }

        public final FontScript component2() {
            return this.cyrillicExtended;
        }

        public final FontScript component3() {
            return this.devanagari;
        }

        public final FontScript component4() {
            return this.chineseTraditionalHk;
        }

        public final FontScript component5() {
            return this.chineseTraditional;
        }

        public final FontScript component6() {
            return this.chineseSimplified;
        }

        public final FontScript component7() {
            return this.arabic;
        }

        public final FontScript component8() {
            return this.japanese;
        }

        public final FontScripts copy(@JsonProperty("latin-extended") FontScript fontScript, @JsonProperty("cyrillic-extended") FontScript fontScript2, FontScript fontScript3, @JsonProperty("chinese-traditional-hk") FontScript fontScript4, @JsonProperty("chinese-traditional") FontScript fontScript5, @JsonProperty("chinese-simplified") FontScript fontScript6, FontScript fontScript7, FontScript fontScript8) {
            return new FontScripts(fontScript, fontScript2, fontScript3, fontScript4, fontScript5, fontScript6, fontScript7, fontScript8);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FontScripts)) {
                return false;
            }
            FontScripts fontScripts = (FontScripts) obj;
            return Intrinsics.areEqual(this.latinExtended, fontScripts.latinExtended) && Intrinsics.areEqual(this.cyrillicExtended, fontScripts.cyrillicExtended) && Intrinsics.areEqual(this.devanagari, fontScripts.devanagari) && Intrinsics.areEqual(this.chineseTraditionalHk, fontScripts.chineseTraditionalHk) && Intrinsics.areEqual(this.chineseTraditional, fontScripts.chineseTraditional) && Intrinsics.areEqual(this.chineseSimplified, fontScripts.chineseSimplified) && Intrinsics.areEqual(this.arabic, fontScripts.arabic) && Intrinsics.areEqual(this.japanese, fontScripts.japanese);
        }

        public int hashCode() {
            FontScript fontScript = this.latinExtended;
            int i = 0;
            int hashCode = (fontScript == null ? 0 : fontScript.hashCode()) * 31;
            FontScript fontScript2 = this.cyrillicExtended;
            int hashCode2 = (hashCode + (fontScript2 == null ? 0 : fontScript2.hashCode())) * 31;
            FontScript fontScript3 = this.devanagari;
            int hashCode3 = (hashCode2 + (fontScript3 == null ? 0 : fontScript3.hashCode())) * 31;
            FontScript fontScript4 = this.chineseTraditionalHk;
            int hashCode4 = (hashCode3 + (fontScript4 == null ? 0 : fontScript4.hashCode())) * 31;
            FontScript fontScript5 = this.chineseTraditional;
            int hashCode5 = (hashCode4 + (fontScript5 == null ? 0 : fontScript5.hashCode())) * 31;
            FontScript fontScript6 = this.chineseSimplified;
            int hashCode6 = (hashCode5 + (fontScript6 == null ? 0 : fontScript6.hashCode())) * 31;
            FontScript fontScript7 = this.arabic;
            int hashCode7 = (hashCode6 + (fontScript7 == null ? 0 : fontScript7.hashCode())) * 31;
            FontScript fontScript8 = this.japanese;
            if (fontScript8 != null) {
                i = fontScript8.hashCode();
            }
            return hashCode7 + i;
        }

        public String toString() {
            return "FontScripts(latinExtended=" + this.latinExtended + ", cyrillicExtended=" + this.cyrillicExtended + ", devanagari=" + this.devanagari + ", chineseTraditionalHk=" + this.chineseTraditionalHk + ", chineseTraditional=" + this.chineseTraditional + ", chineseSimplified=" + this.chineseSimplified + ", arabic=" + this.arabic + ", japanese=" + this.japanese + ')';
        }

        public FontScripts(@JsonProperty("latin-extended") FontScript fontScript, @JsonProperty("cyrillic-extended") FontScript fontScript2, FontScript fontScript3, @JsonProperty("chinese-traditional-hk") FontScript fontScript4, @JsonProperty("chinese-traditional") FontScript fontScript5, @JsonProperty("chinese-simplified") FontScript fontScript6, FontScript fontScript7, FontScript fontScript8) {
            this.latinExtended = fontScript;
            this.cyrillicExtended = fontScript2;
            this.devanagari = fontScript3;
            this.chineseTraditionalHk = fontScript4;
            this.chineseTraditional = fontScript5;
            this.chineseSimplified = fontScript6;
            this.arabic = fontScript7;
            this.japanese = fontScript8;
        }

        public final FontScript getLatinExtended() {
            return this.latinExtended;
        }

        public final FontScript getCyrillicExtended() {
            return this.cyrillicExtended;
        }

        public final FontScript getDevanagari() {
            return this.devanagari;
        }

        public final FontScript getChineseTraditionalHk() {
            return this.chineseTraditionalHk;
        }

        public final FontScript getChineseTraditional() {
            return this.chineseTraditional;
        }

        public final FontScript getChineseSimplified() {
            return this.chineseSimplified;
        }

        public final FontScript getArabic() {
            return this.arabic;
        }

        public final FontScript getJapanese() {
            return this.japanese;
        }
    }

    /* compiled from: FontManifest.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B7\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\bJ\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0013\u001a\u0004\u0018\u00010\u0003HÆ\u0003JE\u0010\u0014\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0003HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0003HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontManifest$FontScript;", "", "regular", "", "bold", "serif", "script", "condensed", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getBold", "()Ljava/lang/String;", "getCondensed", "getRegular", "getScript", "getSerif", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FontScript {
        private final String bold;
        private final String condensed;
        private final String regular;
        private final String script;
        private final String serif;

        public static /* synthetic */ FontScript copy$default(FontScript fontScript, String str, String str2, String str3, String str4, String str5, int i, Object obj) {
            if ((i & 1) != 0) {
                str = fontScript.regular;
            }
            if ((i & 2) != 0) {
                str2 = fontScript.bold;
            }
            if ((i & 4) != 0) {
                str3 = fontScript.serif;
            }
            if ((i & 8) != 0) {
                str4 = fontScript.script;
            }
            if ((i & 16) != 0) {
                str5 = fontScript.condensed;
            }
            return fontScript.copy(str, str2, str3, str4, str5);
        }

        public final String component1() {
            return this.regular;
        }

        public final String component2() {
            return this.bold;
        }

        public final String component3() {
            return this.serif;
        }

        public final String component4() {
            return this.script;
        }

        public final String component5() {
            return this.condensed;
        }

        public final FontScript copy(String str, String str2, String str3, String str4, String str5) {
            return new FontScript(str, str2, str3, str4, str5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FontScript)) {
                return false;
            }
            FontScript fontScript = (FontScript) obj;
            return Intrinsics.areEqual(this.regular, fontScript.regular) && Intrinsics.areEqual(this.bold, fontScript.bold) && Intrinsics.areEqual(this.serif, fontScript.serif) && Intrinsics.areEqual(this.script, fontScript.script) && Intrinsics.areEqual(this.condensed, fontScript.condensed);
        }

        public int hashCode() {
            String str = this.regular;
            int i = 0;
            int hashCode = (str == null ? 0 : str.hashCode()) * 31;
            String str2 = this.bold;
            int hashCode2 = (hashCode + (str2 == null ? 0 : str2.hashCode())) * 31;
            String str3 = this.serif;
            int hashCode3 = (hashCode2 + (str3 == null ? 0 : str3.hashCode())) * 31;
            String str4 = this.script;
            int hashCode4 = (hashCode3 + (str4 == null ? 0 : str4.hashCode())) * 31;
            String str5 = this.condensed;
            if (str5 != null) {
                i = str5.hashCode();
            }
            return hashCode4 + i;
        }

        public String toString() {
            return "FontScript(regular=" + this.regular + ", bold=" + this.bold + ", serif=" + this.serif + ", script=" + this.script + ", condensed=" + this.condensed + ')';
        }

        public FontScript(String str, String str2, String str3, String str4, String str5) {
            this.regular = str;
            this.bold = str2;
            this.serif = str3;
            this.script = str4;
            this.condensed = str5;
        }

        public final String getRegular() {
            return this.regular;
        }

        public final String getBold() {
            return this.bold;
        }

        public final String getSerif() {
            return this.serif;
        }

        public final String getScript() {
            return this.script;
        }

        public final String getCondensed() {
            return this.condensed;
        }
    }

    /* compiled from: FontManifest.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0003J\u001a\u0010\u000f\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0003J\u001a\u0010\u0010\u001a\u0004\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0007J\f\u0010\u0011\u001a\u00020\u0004*\u00020\u000eH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0006*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontManifest$Companion;", "", "()V", "PATH", "", "TAG", "kotlin.jvm.PlatformType", "objectMapper", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "fromDisk", "Lorg/thoughtcrime/securesms/fonts/FontManifest;", "context", "Landroid/content/Context;", "fontVersion", "Lorg/thoughtcrime/securesms/fonts/FontVersion;", "fromNetwork", "get", "manifestPath", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final FontManifest get(Context context, FontVersion fontVersion) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(fontVersion, "fontVersion");
            FontManifest fromDisk = fromDisk(context, fontVersion);
            return fromDisk == null ? fromNetwork(context, fontVersion) : fromDisk;
        }

        private final FontManifest fromDisk(Context context, FontVersion fontVersion) {
            if (!(fontVersion.getPath().length() == 0)) {
                try {
                    InputStream inputStream = EncryptedStreamUtils.INSTANCE.getInputStream(context, new File(Fonts.INSTANCE.getDirectory(context), manifestPath(fontVersion)));
                    FontManifest fontManifest = (FontManifest) FontManifest.objectMapper.readValue(inputStream, FontManifest.class);
                    CloseableKt.closeFinally(inputStream, null);
                    return fontManifest;
                } catch (Exception e) {
                    Log.w(FontManifest.TAG, "Failed to load manifest from disk", e);
                    return null;
                }
            } else {
                throw new AssertionError();
            }
        }

        private final FontManifest fromNetwork(Context context, FontVersion fontVersion) {
            if (Fonts.INSTANCE.downloadAndVerifyLatestManifest(context, fontVersion, manifestPath(fontVersion))) {
                return fromDisk(context, fontVersion);
            }
            return null;
        }

        private final String manifestPath(FontVersion fontVersion) {
            return fontVersion.getPath() + "/.manifest";
        }
    }
}
