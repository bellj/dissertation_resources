package org.thoughtcrime.securesms.fonts;

import kotlin.Metadata;

/* compiled from: SupportedScript.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\f\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/SupportedScript;", "", "(Ljava/lang/String;I)V", "LATIN", "CYRILLIC", "DEVANAGARI", "CHINESE_TRADITIONAL_HK", "CHINESE_TRADITIONAL", "CHINESE_SIMPLIFIED", "UNKNOWN_CJK", "ARABIC", "JAPANESE", "UNKNOWN", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum SupportedScript {
    LATIN,
    CYRILLIC,
    DEVANAGARI,
    CHINESE_TRADITIONAL_HK,
    CHINESE_TRADITIONAL,
    CHINESE_SIMPLIFIED,
    UNKNOWN_CJK,
    ARABIC,
    JAPANESE,
    UNKNOWN
}
