package org.thoughtcrime.securesms.fonts;

import android.content.Context;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.kotlin.ExtensionsKt;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.EncryptedStreamUtils;

/* compiled from: FontVersion.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontVersion;", "", ContactRepository.ID_COLUMN, "", "path", "", "(JLjava/lang/String;)V", "getId", "()J", "getPath", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FontVersion {
    public static final Companion Companion = new Companion(null);
    private static final FontVersion NONE = new FontVersion(-1, "");
    private static final String PATH;
    private static final String TAG = Log.tag(FontVersion.class);
    private static final long VERSION_CHECK_INTERVAL = TimeUnit.DAYS.toMillis(7);
    private static final ObjectMapper objectMapper = ExtensionsKt.registerKotlinModule(new ObjectMapper());
    private final long id;
    private final String path;

    public static /* synthetic */ FontVersion copy$default(FontVersion fontVersion, long j, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            j = fontVersion.id;
        }
        if ((i & 2) != 0) {
            str = fontVersion.path;
        }
        return fontVersion.copy(j, str);
    }

    public final long component1() {
        return this.id;
    }

    public final String component2() {
        return this.path;
    }

    public final FontVersion copy(long j, String str) {
        Intrinsics.checkNotNullParameter(str, "path");
        return new FontVersion(j, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FontVersion)) {
            return false;
        }
        FontVersion fontVersion = (FontVersion) obj;
        return this.id == fontVersion.id && Intrinsics.areEqual(this.path, fontVersion.path);
    }

    public int hashCode() {
        return (SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.id) * 31) + this.path.hashCode();
    }

    public String toString() {
        return "FontVersion(id=" + this.id + ", path=" + this.path + ')';
    }

    public FontVersion(long j, String str) {
        Intrinsics.checkNotNullParameter(str, "path");
        this.id = j;
        this.path = str;
    }

    public final long getId() {
        return this.id;
    }

    public final String getPath() {
        return this.path;
    }

    /* compiled from: FontVersion.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\bH\u0003J\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0011\u001a\u00020\u0012H\u0003J\n\u0010\u0015\u001a\u0004\u0018\u00010\u0004H\u0003J\u0010\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012H\u0007J\u001a\u0010\u0017\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0004H\u0003R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bXT¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\n \n*\u0004\u0018\u00010\b0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/fonts/FontVersion$Companion;", "", "()V", "NONE", "Lorg/thoughtcrime/securesms/fonts/FontVersion;", "getNONE", "()Lorg/thoughtcrime/securesms/fonts/FontVersion;", "PATH", "", "TAG", "kotlin.jvm.PlatformType", "VERSION_CHECK_INTERVAL", "", "objectMapper", "Lcom/fasterxml/jackson/databind/ObjectMapper;", "cleanOldVersions", "", "context", "Landroid/content/Context;", "path", "fromDisk", "fromNetwork", "get", "writeVersionToDisk", "fontVersion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final FontVersion getNONE() {
            return FontVersion.NONE;
        }

        public final FontVersion get(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            FontVersion fromDisk = fromDisk(context);
            if (System.currentTimeMillis() - SignalStore.storyValues().getLastFontVersionCheck() > FontVersion.VERSION_CHECK_INTERVAL) {
                Log.i(FontVersion.TAG, "Timeout interval exceeded, checking network for new font version.");
                FontVersion fromNetwork = fromNetwork();
                if (fromDisk == null && fromNetwork == null) {
                    Log.i(FontVersion.TAG, "Couldn't download font version and none present on disk.");
                    return getNONE();
                } else if (fromDisk == null && fromNetwork != null) {
                    Log.i(FontVersion.TAG, "Found initial font version.");
                    FontVersion writeVersionToDisk = writeVersionToDisk(context, fromNetwork);
                    return writeVersionToDisk == null ? getNONE() : writeVersionToDisk;
                } else if (fromDisk == null || fromNetwork == null) {
                    Log.i(FontVersion.TAG, "Couldn't download font version, using what we have.");
                    if (fromDisk == null) {
                        fromDisk = getNONE();
                    }
                } else if (fromDisk.getId() < fromNetwork.getId()) {
                    Log.i(FontVersion.TAG, "Found a new font version. Replacing old version");
                    fromDisk = writeVersionToDisk(context, fromNetwork);
                    if (fromDisk == null) {
                        fromDisk = getNONE();
                    }
                } else {
                    Log.i(FontVersion.TAG, "Network version is the same as our local version.");
                    SignalStore.storyValues().setLastFontVersionCheck(System.currentTimeMillis());
                }
            } else {
                Log.i(FontVersion.TAG, "Timeout interval not exceeded, using what we have.");
                if (fromDisk == null) {
                    fromDisk = getNONE();
                }
            }
            cleanOldVersions(context, fromDisk.getPath());
            return fromDisk;
        }

        private final FontVersion writeVersionToDisk(Context context, FontVersion fontVersion) {
            try {
                Fonts fonts = Fonts.INSTANCE;
                File file = new File(fonts.getDirectory(context), FontVersion.PATH);
                if (file.exists()) {
                    file.delete();
                }
                OutputStream outputStream = EncryptedStreamUtils.INSTANCE.getOutputStream(context, file);
                FontVersion.objectMapper.writeValue(outputStream, fontVersion);
                Unit unit = Unit.INSTANCE;
                CloseableKt.closeFinally(outputStream, null);
                new File(fonts.getDirectory(context), fontVersion.getPath()).mkdir();
                String str = FontVersion.TAG;
                Log.i(str, "Wrote version " + fontVersion.getId() + " to disk.");
                SignalStore.storyValues().setLastFontVersionCheck(System.currentTimeMillis());
                return fontVersion;
            } catch (Exception e) {
                Log.e(FontVersion.TAG, "Failed to write new font version to disk", e);
                return null;
            }
        }

        private final FontVersion fromDisk(Context context) {
            try {
                InputStream inputStream = EncryptedStreamUtils.INSTANCE.getInputStream(context, new File(Fonts.INSTANCE.getDirectory(context), FontVersion.PATH));
                FontVersion fontVersion = (FontVersion) FontVersion.objectMapper.readValue(inputStream, FontVersion.class);
                CloseableKt.closeFinally(inputStream, null);
                return fontVersion;
            } catch (Exception unused) {
                Log.w(FontVersion.TAG, "Could not read font version from disk.");
                return null;
            }
        }

        private final FontVersion fromNetwork() {
            try {
                long downloadLatestVersionLong = Fonts.INSTANCE.downloadLatestVersionLong();
                String uuid = UUID.randomUUID().toString();
                Intrinsics.checkNotNullExpressionValue(uuid, "randomUUID().toString()");
                FontVersion fontVersion = new FontVersion(downloadLatestVersionLong, uuid);
                String str = FontVersion.TAG;
                Log.i(str, "Downloaded version " + fontVersion.getId());
                return fontVersion;
            } catch (Exception e) {
                Log.w(FontVersion.TAG, "Could not read font version from network.", e);
                return null;
            }
        }

        private final void cleanOldVersions(Context context, String str) {
            if (str.length() == 0) {
                Log.i(FontVersion.TAG, "No versions downloaded. Skipping cleanup.");
                return;
            }
            File[] listFiles = Fonts.INSTANCE.getDirectory(context).listFiles(new FontVersion$Companion$$ExternalSyntheticLambda0(str));
            if (listFiles != null) {
                Log.i(FontVersion.TAG, "Deleting " + listFiles.length + " files");
                for (File file : listFiles) {
                    file.delete();
                }
            }
        }

        /* renamed from: cleanOldVersions$lambda-3 */
        public static final boolean m1793cleanOldVersions$lambda3(String str, File file, String str2) {
            Intrinsics.checkNotNullParameter(str, "$path");
            return !Intrinsics.areEqual(str2, str) && !Intrinsics.areEqual(str2, FontVersion.PATH);
        }
    }
}
