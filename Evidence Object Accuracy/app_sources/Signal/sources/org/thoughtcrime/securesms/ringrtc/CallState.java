package org.thoughtcrime.securesms.ringrtc;

/* loaded from: classes4.dex */
public enum CallState {
    IDLE,
    DIALING,
    ANSWERING,
    REMOTE_RINGING,
    LOCAL_RINGING,
    CONNECTED,
    TERMINATED,
    RECEIVED_BUSY
}
