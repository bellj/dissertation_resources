package org.thoughtcrime.securesms.ringrtc;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes4.dex */
public class CameraState implements Parcelable {
    public static final Parcelable.Creator<CameraState> CREATOR = new Parcelable.Creator<CameraState>() { // from class: org.thoughtcrime.securesms.ringrtc.CameraState.1
        @Override // android.os.Parcelable.Creator
        public CameraState createFromParcel(Parcel parcel) {
            return new CameraState(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public CameraState[] newArray(int i) {
            return new CameraState[i];
        }
    };
    public static final CameraState UNKNOWN = new CameraState(Direction.NONE, 0);
    private final Direction activeDirection;
    private final int cameraCount;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public CameraState(Direction direction, int i) {
        this.activeDirection = direction;
        this.cameraCount = i;
    }

    private CameraState(Parcel parcel) {
        this(Direction.valueOf(parcel.readString()), parcel.readInt());
    }

    public int getCameraCount() {
        return this.cameraCount;
    }

    public Direction getActiveDirection() {
        return this.activeDirection;
    }

    public boolean isEnabled() {
        return this.activeDirection != Direction.NONE;
    }

    @Override // java.lang.Object
    public String toString() {
        return "count: " + this.cameraCount + ", activeDirection: " + this.activeDirection;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.activeDirection.name());
        parcel.writeInt(this.cameraCount);
    }

    /* loaded from: classes4.dex */
    public enum Direction {
        FRONT,
        BACK,
        NONE,
        PENDING;

        public boolean isUsable() {
            return this == FRONT || this == BACK;
        }

        public Direction switchDirection() {
            int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$ringrtc$CameraState$Direction[ordinal()];
            if (i == 1) {
                return BACK;
            }
            if (i != 2) {
                return this;
            }
            return FRONT;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.ringrtc.CameraState$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$ringrtc$CameraState$Direction;

        static {
            int[] iArr = new int[Direction.values().length];
            $SwitchMap$org$thoughtcrime$securesms$ringrtc$CameraState$Direction = iArr;
            try {
                iArr[Direction.FRONT.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$ringrtc$CameraState$Direction[Direction.BACK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }
}
