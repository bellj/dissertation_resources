package org.thoughtcrime.securesms.ringrtc;

/* loaded from: classes4.dex */
public interface CameraEventListener {
    void onCameraSwitchCompleted(CameraState cameraState);

    void onFullyInitialized();
}
