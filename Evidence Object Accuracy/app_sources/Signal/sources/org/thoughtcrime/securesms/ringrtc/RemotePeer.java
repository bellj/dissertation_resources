package org.thoughtcrime.securesms.ringrtc;

import android.os.Parcel;
import android.os.Parcelable;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CallId;
import org.signal.ringrtc.Remote;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class RemotePeer implements Remote, Parcelable {
    public static final Parcelable.Creator<RemotePeer> CREATOR = new Parcelable.Creator<RemotePeer>() { // from class: org.thoughtcrime.securesms.ringrtc.RemotePeer.1
        @Override // android.os.Parcelable.Creator
        public RemotePeer createFromParcel(Parcel parcel) {
            return new RemotePeer(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public RemotePeer[] newArray(int i) {
            return new RemotePeer[i];
        }
    };
    public static final CallId GROUP_CALL_ID = new CallId(-2L);
    public static final CallId NO_CALL_ID = new CallId(-1L);
    private static final String TAG = Log.tag(RemotePeer.class);
    private CallId callId;
    private long callStartTimestamp;
    private CallState callState;
    private final RecipientId recipientId;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public RemotePeer(RecipientId recipientId) {
        this(recipientId, new CallId(-1L));
    }

    public RemotePeer(RecipientId recipientId, CallId callId) {
        this.recipientId = recipientId;
        this.callState = CallState.IDLE;
        this.callId = callId;
        this.callStartTimestamp = 0;
    }

    private RemotePeer(Parcel parcel) {
        this.recipientId = RecipientId.CREATOR.createFromParcel(parcel);
        this.callState = CallState.values()[parcel.readInt()];
        this.callId = new CallId(Long.valueOf(parcel.readLong()));
        this.callStartTimestamp = parcel.readLong();
    }

    public CallId getCallId() {
        return this.callId;
    }

    public void setCallId(CallId callId) {
        this.callId = callId;
    }

    public void setCallStartTimestamp(long j) {
        this.callStartTimestamp = j;
    }

    public long getCallStartTimestamp() {
        return this.callStartTimestamp;
    }

    public CallState getState() {
        return this.callState;
    }

    public RecipientId getId() {
        return this.recipientId;
    }

    public Recipient getRecipient() {
        return Recipient.resolved(this.recipientId);
    }

    @Override // java.lang.Object
    public String toString() {
        return "recipientId: " + this.recipientId + ", callId: " + this.callId + ", state: " + this.callState;
    }

    @Override // org.signal.ringrtc.Remote
    public boolean recipientEquals(Remote remote) {
        if (remote == null || RemotePeer.class != remote.getClass()) {
            return false;
        }
        return this.recipientId.equals(((RemotePeer) remote).recipientId);
    }

    public boolean callIdEquals(RemotePeer remotePeer) {
        return remotePeer != null && this.callId.equals(remotePeer.callId);
    }

    public void dialing() {
        if (this.callState == CallState.IDLE) {
            this.callState = CallState.DIALING;
            return;
        }
        throw new IllegalStateException("Cannot transition to DIALING from state: " + this.callState);
    }

    public void answering() {
        if (this.callState == CallState.IDLE) {
            this.callState = CallState.ANSWERING;
            return;
        }
        throw new IllegalStateException("Cannot transition to ANSWERING from state: " + this.callState);
    }

    public void remoteRinging() {
        if (this.callState == CallState.DIALING) {
            this.callState = CallState.REMOTE_RINGING;
            return;
        }
        throw new IllegalStateException("Cannot transition to REMOTE_RINGING from state: " + this.callState);
    }

    public void localRinging() {
        if (this.callState == CallState.ANSWERING) {
            this.callState = CallState.LOCAL_RINGING;
            return;
        }
        throw new IllegalStateException("Cannot transition to LOCAL_RINGING from state: " + this.callState);
    }

    public void connected() {
        CallState callState = this.callState;
        if (callState == CallState.REMOTE_RINGING || callState == CallState.LOCAL_RINGING) {
            this.callState = CallState.CONNECTED;
            return;
        }
        throw new IllegalStateException("Cannot transition outgoing call to CONNECTED from state: " + this.callState);
    }

    public void receivedBusy() {
        if (this.callState != CallState.DIALING) {
            String str = TAG;
            Log.w(str, "RECEIVED_BUSY from unexpected state: " + this.callState);
        }
        this.callState = CallState.RECEIVED_BUSY;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        this.recipientId.writeToParcel(parcel, i);
        parcel.writeInt(this.callState.ordinal());
        parcel.writeLong(this.callId.longValue().longValue());
        parcel.writeLong(this.callStartTimestamp);
    }
}
