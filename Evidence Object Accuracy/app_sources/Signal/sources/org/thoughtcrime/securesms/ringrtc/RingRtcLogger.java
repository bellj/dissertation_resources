package org.thoughtcrime.securesms.ringrtc;

import org.signal.ringrtc.Log;

/* loaded from: classes.dex */
public class RingRtcLogger implements Log.Logger {
    @Override // org.signal.ringrtc.Log.Logger
    public void v(String str, String str2, Throwable th) {
        org.signal.core.util.logging.Log.v(str, str2, th);
    }

    @Override // org.signal.ringrtc.Log.Logger
    public void d(String str, String str2, Throwable th) {
        org.signal.core.util.logging.Log.d(str, str2, th);
    }

    @Override // org.signal.ringrtc.Log.Logger
    public void i(String str, String str2, Throwable th) {
        org.signal.core.util.logging.Log.i(str, str2, th);
    }

    @Override // org.signal.ringrtc.Log.Logger
    public void w(String str, String str2, Throwable th) {
        org.signal.core.util.logging.Log.w(str, str2, th);
    }

    @Override // org.signal.ringrtc.Log.Logger
    public void e(String str, String str2, Throwable th) {
        org.signal.core.util.logging.Log.e(str, str2, th);
    }
}
