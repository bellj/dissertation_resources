package org.thoughtcrime.securesms.ringrtc;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraManager;
import com.annimon.stream.Stream;
import j$.util.function.Consumer;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.ringrtc.CameraControl;
import org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.webrtc.Camera1Enumerator;
import org.webrtc.Camera2Capturer;
import org.webrtc.Camera2Enumerator;
import org.webrtc.CameraEnumerator;
import org.webrtc.CameraVideoCapturer;
import org.webrtc.CapturerObserver;
import org.webrtc.EglBase;
import org.webrtc.SurfaceTextureHelper;
import org.webrtc.VideoFrame;

/* loaded from: classes4.dex */
public class Camera implements CameraControl, CameraVideoCapturer.CameraSwitchHandler {
    private static final String TAG = Log.tag(Camera.class);
    private CameraState.Direction activeDirection;
    private final int cameraCount;
    private final CameraEventListener cameraEventListener;
    private final CameraVideoCapturer capturer;
    private final Context context;
    private final EglBaseWrapper eglBase;
    private boolean enabled;
    private boolean isInitialized;
    private int orientation;

    public Camera(Context context, CameraEventListener cameraEventListener, EglBaseWrapper eglBaseWrapper, CameraState.Direction direction) {
        this.context = context;
        this.cameraEventListener = cameraEventListener;
        this.eglBase = eglBaseWrapper;
        CameraEnumerator cameraEnumerator = getCameraEnumerator(context);
        this.cameraCount = cameraEnumerator.getDeviceNames().length;
        direction = !direction.isUsable() ? CameraState.Direction.FRONT : direction;
        CameraVideoCapturer createVideoCapturer = createVideoCapturer(cameraEnumerator, direction);
        if (createVideoCapturer != null) {
            this.activeDirection = direction;
        } else {
            CameraState.Direction switchDirection = direction.switchDirection();
            CameraVideoCapturer createVideoCapturer2 = createVideoCapturer(cameraEnumerator, switchDirection);
            if (createVideoCapturer2 != null) {
                this.activeDirection = switchDirection;
            } else {
                this.activeDirection = CameraState.Direction.NONE;
            }
            createVideoCapturer = createVideoCapturer2;
        }
        this.capturer = createVideoCapturer;
    }

    @Override // org.signal.ringrtc.CameraControl
    public void initCapturer(CapturerObserver capturerObserver) {
        if (this.capturer != null) {
            this.eglBase.performWithValidEglBase(new Consumer(capturerObserver) { // from class: org.thoughtcrime.securesms.ringrtc.Camera$$ExternalSyntheticLambda0
                public final /* synthetic */ CapturerObserver f$1;

                {
                    this.f$1 = r2;
                }

                @Override // j$.util.function.Consumer
                public final void accept(Object obj) {
                    Camera.$r8$lambda$GrfWTy0kPVToS41d0P1sDdnPjfM(Camera.this, this.f$1, (EglBase) obj);
                }

                @Override // j$.util.function.Consumer
                public /* synthetic */ Consumer andThen(Consumer consumer) {
                    return Consumer.CC.$default$andThen(this, consumer);
                }
            });
        }
    }

    public /* synthetic */ void lambda$initCapturer$0(CapturerObserver capturerObserver, EglBase eglBase) {
        this.capturer.initialize(SurfaceTextureHelper.create("WebRTC-SurfaceTextureHelper", eglBase.getEglBaseContext()), this.context, new CameraCapturerWrapper(capturerObserver));
        this.capturer.setOrientation(Integer.valueOf(this.orientation));
        this.isInitialized = true;
    }

    @Override // org.signal.ringrtc.CameraControl
    public boolean hasCapturer() {
        return this.capturer != null;
    }

    @Override // org.signal.ringrtc.CameraControl
    public void flip() {
        CameraVideoCapturer cameraVideoCapturer = this.capturer;
        if (cameraVideoCapturer == null || this.cameraCount < 2) {
            throw new AssertionError("Tried to flip the camera, but we only have " + this.cameraCount + " of them.");
        }
        this.activeDirection = CameraState.Direction.PENDING;
        cameraVideoCapturer.switchCamera(this);
    }

    @Override // org.signal.ringrtc.CameraControl
    public void setOrientation(Integer num) {
        CameraVideoCapturer cameraVideoCapturer;
        this.orientation = num.intValue();
        if (this.isInitialized && (cameraVideoCapturer = this.capturer) != null) {
            cameraVideoCapturer.setOrientation(num);
        }
    }

    @Override // org.signal.ringrtc.CameraControl
    public void setEnabled(boolean z) {
        String str = TAG;
        Log.i(str, "setEnabled(): " + z);
        this.enabled = z;
        if (this.capturer != null) {
            try {
                if (z) {
                    Log.i(str, "setEnabled(): starting capture");
                    this.capturer.startCapture(1280, 720, 30);
                    return;
                }
                Log.i(str, "setEnabled(): stopping capture");
                this.capturer.stopCapture();
            } catch (InterruptedException e) {
                while (true) {
                    Log.w(TAG, "Got interrupted while trying to stop video capture", e);
                    return;
                }
            }
        }
    }

    public void dispose() {
        CameraVideoCapturer cameraVideoCapturer = this.capturer;
        if (cameraVideoCapturer != null) {
            cameraVideoCapturer.dispose();
            this.isInitialized = false;
        }
    }

    int getCount() {
        return this.cameraCount;
    }

    CameraState.Direction getActiveDirection() {
        return this.enabled ? this.activeDirection : CameraState.Direction.NONE;
    }

    public CameraState getCameraState() {
        return new CameraState(getActiveDirection(), getCount());
    }

    CameraVideoCapturer getCapturer() {
        return this.capturer;
    }

    public boolean isInitialized() {
        return this.isInitialized;
    }

    private CameraVideoCapturer createVideoCapturer(CameraEnumerator cameraEnumerator, CameraState.Direction direction) {
        String[] deviceNames = cameraEnumerator.getDeviceNames();
        for (String str : deviceNames) {
            if ((direction == CameraState.Direction.FRONT && cameraEnumerator.isFrontFacing(str)) || (direction == CameraState.Direction.BACK && cameraEnumerator.isBackFacing(str))) {
                return cameraEnumerator.createCapturer(str, null);
            }
        }
        return null;
    }

    private CameraEnumerator getCameraEnumerator(Context context) {
        boolean z;
        try {
            z = Camera2Enumerator.isSupported(context);
        } catch (Throwable th) {
            Log.w(TAG, "Camera2Enumator.isSupport() threw.", th);
            z = false;
        }
        String str = TAG;
        Log.i(str, "Camera2 enumerator supported: " + z);
        if (z) {
            return new FilteredCamera2Enumerator(context);
        }
        return new Camera1Enumerator(true);
    }

    @Override // org.webrtc.CameraVideoCapturer.CameraSwitchHandler
    public void onCameraSwitchDone(boolean z) {
        this.activeDirection = z ? CameraState.Direction.FRONT : CameraState.Direction.BACK;
        this.cameraEventListener.onCameraSwitchCompleted(new CameraState(getActiveDirection(), getCount()));
    }

    @Override // org.webrtc.CameraVideoCapturer.CameraSwitchHandler
    public void onCameraSwitchError(String str) {
        String str2 = TAG;
        Log.e(str2, "onCameraSwitchError: " + str);
        this.cameraEventListener.onCameraSwitchCompleted(new CameraState(getActiveDirection(), getCount()));
    }

    /* loaded from: classes4.dex */
    public static class FilteredCamera2Enumerator extends Camera2Enumerator {
        private static final String TAG = Log.tag(Camera2Enumerator.class);
        private final CameraManager cameraManager;
        private final Context context;
        private String[] deviceNames = null;

        FilteredCamera2Enumerator(Context context) {
            super(context);
            this.context = context;
            this.cameraManager = (CameraManager) context.getSystemService("camera");
        }

        private static boolean isMonochrome(String str, CameraManager cameraManager) {
            try {
                int[] iArr = (int[]) cameraManager.getCameraCharacteristics(str).get(CameraCharacteristics.REQUEST_AVAILABLE_CAPABILITIES);
                if (iArr != null) {
                    for (int i : iArr) {
                        if (i == 12) {
                            return true;
                        }
                    }
                }
            } catch (CameraAccessException unused) {
            }
            return false;
        }

        private static boolean isLensFacing(String str, CameraManager cameraManager, Integer num) {
            try {
                return num.equals((Integer) cameraManager.getCameraCharacteristics(str).get(CameraCharacteristics.LENS_FACING));
            } catch (CameraAccessException unused) {
                return false;
            }
        }

        @Override // org.webrtc.Camera2Enumerator, org.webrtc.CameraEnumerator
        public String[] getDeviceNames() {
            String[] strArr = this.deviceNames;
            if (strArr != null) {
                return strArr;
            }
            try {
                LinkedList linkedList = new LinkedList();
                CameraManager cameraManager = this.cameraManager;
                if (cameraManager != null) {
                    List list = Stream.of(cameraManager.getCameraIdList()).filterNot(new Camera$FilteredCamera2Enumerator$$ExternalSyntheticLambda0(this)).toList();
                    String str = (String) Stream.of(list).filter(new Camera$FilteredCamera2Enumerator$$ExternalSyntheticLambda1(this)).findFirst().orElse(null);
                    if (str != null) {
                        linkedList.add(str);
                    }
                    String str2 = (String) Stream.of(list).filter(new Camera$FilteredCamera2Enumerator$$ExternalSyntheticLambda2(this)).findFirst().orElse(null);
                    if (str2 != null) {
                        linkedList.add(str2);
                    }
                }
                this.deviceNames = (String[]) linkedList.toArray(new String[0]);
            } catch (CameraAccessException e) {
                String str3 = TAG;
                Log.e(str3, "Camera access exception: " + e);
                this.deviceNames = new String[0];
            }
            return this.deviceNames;
        }

        public /* synthetic */ boolean lambda$getDeviceNames$0(String str) {
            return isMonochrome(str, this.cameraManager);
        }

        public /* synthetic */ boolean lambda$getDeviceNames$1(String str) {
            return isLensFacing(str, this.cameraManager, 0);
        }

        public /* synthetic */ boolean lambda$getDeviceNames$2(String str) {
            return isLensFacing(str, this.cameraManager, 1);
        }

        @Override // org.webrtc.Camera2Enumerator, org.webrtc.CameraEnumerator
        public CameraVideoCapturer createCapturer(String str, CameraVideoCapturer.CameraEventsHandler cameraEventsHandler) {
            Context context = this.context;
            return new Camera2Capturer(context, str, cameraEventsHandler, new FilteredCamera2Enumerator(context));
        }
    }

    /* loaded from: classes4.dex */
    public class CameraCapturerWrapper implements CapturerObserver {
        private final CapturerObserver observer;

        public CameraCapturerWrapper(CapturerObserver capturerObserver) {
            Camera.this = r1;
            this.observer = capturerObserver;
        }

        @Override // org.webrtc.CapturerObserver
        public void onCapturerStarted(boolean z) {
            this.observer.onCapturerStarted(z);
            if (z) {
                Camera.this.cameraEventListener.onFullyInitialized();
            }
        }

        @Override // org.webrtc.CapturerObserver
        public void onCapturerStopped() {
            this.observer.onCapturerStopped();
        }

        @Override // org.webrtc.CapturerObserver
        public void onFrameCaptured(VideoFrame videoFrame) {
            this.observer.onFrameCaptured(videoFrame);
        }
    }
}
