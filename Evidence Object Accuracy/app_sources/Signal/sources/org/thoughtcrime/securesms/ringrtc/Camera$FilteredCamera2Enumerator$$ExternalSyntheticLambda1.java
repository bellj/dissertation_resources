package org.thoughtcrime.securesms.ringrtc;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.ringrtc.Camera;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Camera$FilteredCamera2Enumerator$$ExternalSyntheticLambda1 implements Predicate {
    public final /* synthetic */ Camera.FilteredCamera2Enumerator f$0;

    public /* synthetic */ Camera$FilteredCamera2Enumerator$$ExternalSyntheticLambda1(Camera.FilteredCamera2Enumerator filteredCamera2Enumerator) {
        this.f$0 = filteredCamera2Enumerator;
    }

    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.lambda$getDeviceNames$1((String) obj);
    }
}
