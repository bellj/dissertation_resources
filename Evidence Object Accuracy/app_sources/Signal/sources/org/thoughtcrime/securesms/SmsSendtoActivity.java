package org.thoughtcrime.securesms;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import java.net.URISyntaxException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Rfc5724Uri;

/* loaded from: classes.dex */
public class SmsSendtoActivity extends Activity {
    private static final String TAG = Log.tag(SmsSendtoActivity.class);

    @Override // android.app.Activity
    protected void onCreate(Bundle bundle) {
        startActivity(getNextIntent(getIntent()));
        finish();
        super.onCreate(bundle);
    }

    private Intent getNextIntent(Intent intent) {
        DestinationAndBody destinationAndBody;
        if (intent.getAction().equals("android.intent.action.SENDTO")) {
            destinationAndBody = getDestinationForSendTo(intent);
        } else if (intent.getData() == null || !"content".equals(intent.getData().getScheme())) {
            destinationAndBody = getDestinationForView(intent);
        } else {
            destinationAndBody = getDestinationForSyncAdapter(intent);
        }
        if (TextUtils.isEmpty(destinationAndBody.destination)) {
            Intent intent2 = new Intent(this, NewConversationActivity.class);
            intent2.putExtra("android.intent.extra.TEXT", destinationAndBody.getBody());
            Toast.makeText(this, (int) R.string.ConversationActivity_specify_recipient, 1).show();
            return intent2;
        }
        Recipient external = Recipient.external(this, destinationAndBody.getDestination());
        return ConversationIntents.createBuilder(this, external.getId(), SignalDatabase.threads().getThreadIdIfExistsFor(external.getId())).withDraftText(destinationAndBody.getBody()).build();
    }

    private DestinationAndBody getDestinationForSendTo(Intent intent) {
        return new DestinationAndBody(intent.getData().getSchemeSpecificPart(), intent.getStringExtra("sms_body"));
    }

    private DestinationAndBody getDestinationForView(Intent intent) {
        try {
            Rfc5724Uri rfc5724Uri = new Rfc5724Uri(intent.getData().toString());
            return new DestinationAndBody(rfc5724Uri.getPath(), rfc5724Uri.getQueryParams().get("body"));
        } catch (URISyntaxException e) {
            Log.w(TAG, "unable to parse RFC5724 URI from intent", e);
            return new DestinationAndBody("", "");
        }
    }

    private DestinationAndBody getDestinationForSyncAdapter(Intent intent) {
        Cursor cursor;
        Throwable th;
        try {
            cursor = getContentResolver().query(intent.getData(), null, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToNext()) {
                        DestinationAndBody destinationAndBody = new DestinationAndBody(cursor.getString(cursor.getColumnIndexOrThrow("data1")), "");
                        cursor.close();
                        return destinationAndBody;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (cursor != null) {
                        cursor.close();
                    }
                    throw th;
                }
            }
            DestinationAndBody destinationAndBody2 = new DestinationAndBody("", "");
            if (cursor != null) {
                cursor.close();
            }
            return destinationAndBody2;
        } catch (Throwable th3) {
            th = th3;
            cursor = null;
        }
    }

    /* loaded from: classes.dex */
    public static class DestinationAndBody {
        private final String body;
        private final String destination;

        private DestinationAndBody(String str, String str2) {
            this.destination = str;
            this.body = str2;
        }

        public String getDestination() {
            return this.destination;
        }

        public String getBody() {
            return this.body;
        }
    }
}
