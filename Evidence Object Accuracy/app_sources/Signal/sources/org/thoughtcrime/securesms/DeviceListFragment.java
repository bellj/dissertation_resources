package org.thoughtcrime.securesms;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;
import androidx.fragment.app.ListFragment;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.loaders.DeviceListLoader;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.devicelist.Device;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;

/* loaded from: classes.dex */
public class DeviceListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<List<Device>>, AdapterView.OnItemClickListener, View.OnClickListener {
    private static final String TAG = Log.tag(DeviceListFragment.class);
    private SignalServiceAccountManager accountManager;
    private FloatingActionButton addDeviceButton;
    private View.OnClickListener addDeviceButtonListener;
    private View empty;
    private Locale locale;
    private View progressContainer;

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<List<Device>>) loader, (List) obj);
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.locale = (Locale) requireArguments().getSerializable(PassphraseRequiredActivity.LOCALE_EXTRA);
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        this.accountManager = ApplicationDependencies.getSignalServiceAccountManager();
    }

    @Override // androidx.fragment.app.ListFragment, androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.device_list_fragment, viewGroup, false);
        this.empty = inflate.findViewById(R.id.empty);
        this.progressContainer = inflate.findViewById(R.id.progress_container);
        FloatingActionButton floatingActionButton = (FloatingActionButton) inflate.findViewById(R.id.add_device);
        this.addDeviceButton = floatingActionButton;
        floatingActionButton.setOnClickListener(this);
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getLoaderManager().initLoader(0, null, this);
        getListView().setOnItemClickListener(this);
    }

    public void setAddDeviceButtonListener(View.OnClickListener onClickListener) {
        this.addDeviceButtonListener = onClickListener;
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public Loader<List<Device>> onCreateLoader(int i, Bundle bundle) {
        this.empty.setVisibility(8);
        this.progressContainer.setVisibility(0);
        return new DeviceListLoader(getActivity(), this.accountManager);
    }

    public void onLoadFinished(Loader<List<Device>> loader, List<Device> list) {
        this.progressContainer.setVisibility(8);
        if (list == null) {
            handleLoaderFailed();
            return;
        }
        setListAdapter(new DeviceListAdapter(getActivity(), R.layout.device_list_item_view, list, this.locale));
        if (list.isEmpty()) {
            this.empty.setVisibility(0);
            TextSecurePreferences.setMultiDevice(getActivity(), false);
            return;
        }
        this.empty.setVisibility(8);
    }

    public void onLoaderReset(Loader<List<Device>> loader) {
        setListAdapter(null);
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
        DeviceListItem deviceListItem = (DeviceListItem) view;
        String deviceName = deviceListItem.getDeviceName();
        long deviceId = deviceListItem.getDeviceId();
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
        materialAlertDialogBuilder.setTitle((CharSequence) getString(R.string.DeviceListActivity_unlink_s, deviceName));
        materialAlertDialogBuilder.setMessage(R.string.DeviceListActivity_by_unlinking_this_device_it_will_no_longer_be_able_to_send_or_receive);
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        materialAlertDialogBuilder.setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(deviceId) { // from class: org.thoughtcrime.securesms.DeviceListFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                DeviceListFragment.this.lambda$onItemClick$0(this.f$1, dialogInterface, i2);
            }
        });
        materialAlertDialogBuilder.show();
    }

    public /* synthetic */ void lambda$onItemClick$0(long j, DialogInterface dialogInterface, int i) {
        handleDisconnectDevice(j);
    }

    private void handleLoaderFailed() {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
        materialAlertDialogBuilder.setMessage(R.string.DeviceListActivity_network_connection_failed);
        materialAlertDialogBuilder.setPositiveButton(R.string.DeviceListActivity_try_again, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.DeviceListFragment$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeviceListFragment.this.lambda$handleLoaderFailed$1(dialogInterface, i);
            }
        });
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.DeviceListFragment$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeviceListFragment.this.lambda$handleLoaderFailed$2(dialogInterface, i);
            }
        });
        materialAlertDialogBuilder.setOnCancelListener((DialogInterface.OnCancelListener) new DialogInterface.OnCancelListener() { // from class: org.thoughtcrime.securesms.DeviceListFragment$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnCancelListener
            public final void onCancel(DialogInterface dialogInterface) {
                DeviceListFragment.this.lambda$handleLoaderFailed$3(dialogInterface);
            }
        });
        materialAlertDialogBuilder.show();
    }

    public /* synthetic */ void lambda$handleLoaderFailed$1(DialogInterface dialogInterface, int i) {
        getLoaderManager().restartLoader(0, null, this);
    }

    public /* synthetic */ void lambda$handleLoaderFailed$2(DialogInterface dialogInterface, int i) {
        requireActivity().onBackPressed();
    }

    public /* synthetic */ void lambda$handleLoaderFailed$3(DialogInterface dialogInterface) {
        requireActivity().onBackPressed();
    }

    private void handleDisconnectDevice(final long j) {
        new ProgressDialogAsyncTask<Void, Void, Boolean>(getActivity(), R.string.DeviceListActivity_unlinking_device_no_ellipsis, R.string.DeviceListActivity_unlinking_device) { // from class: org.thoughtcrime.securesms.DeviceListFragment.1
            public Boolean doInBackground(Void... voidArr) {
                try {
                    DeviceListFragment.this.accountManager.removeDevice(j);
                    return Boolean.TRUE;
                } catch (IOException e) {
                    Log.w(DeviceListFragment.TAG, e);
                    return Boolean.FALSE;
                }
            }

            public void onPostExecute(Boolean bool) {
                super.onPostExecute((AnonymousClass1) bool);
                if (bool.booleanValue()) {
                    DeviceListFragment.this.getLoaderManager().restartLoader(0, null, DeviceListFragment.this);
                } else {
                    Toast.makeText(DeviceListFragment.this.getActivity(), (int) R.string.DeviceListActivity_network_failed, 1).show();
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        View.OnClickListener onClickListener = this.addDeviceButtonListener;
        if (onClickListener != null) {
            onClickListener.onClick(view);
        }
    }

    /* loaded from: classes.dex */
    public static class DeviceListAdapter extends ArrayAdapter<Device> {
        private final Locale locale;
        private final int resource;

        public DeviceListAdapter(Context context, int i, List<Device> list, Locale locale) {
            super(context, i, list);
            this.resource = i;
            this.locale = locale;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ((Activity) getContext()).getLayoutInflater().inflate(this.resource, viewGroup, false);
            }
            ((DeviceListItem) view).set(getItem(i), this.locale);
            return view;
        }
    }
}
