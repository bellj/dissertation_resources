package org.thoughtcrime.securesms.providers;

import android.content.Context;
import android.content.UriMatcher;
import android.media.MediaDataSource;
import android.net.Uri;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.voice.VoiceNoteDraft;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernEncryptingPartOutputStream;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.IOFunction;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.video.ByteArrayMediaDataSource;
import org.thoughtcrime.securesms.video.EncryptedMediaDataSource;

/* loaded from: classes.dex */
public class BlobProvider {
    public static final String AUTHORITY;
    public static final Uri CONTENT_URI = Uri.parse("content://org.thoughtcrime.securesms.blob/blob");
    private static final String DRAFT_ATTACHMENTS_DIRECTORY;
    private static final int FILENAME_PATH_SEGMENT;
    private static final int FILESIZE_PATH_SEGMENT;
    private static final int ID_PATH_SEGMENT;
    private static final BlobProvider INSTANCE = new BlobProvider();
    private static final int MATCH;
    private static final int MIMETYPE_PATH_SEGMENT;
    private static final String MULTI_SESSION_DIRECTORY;
    public static final String PATH;
    private static final String SINGLE_SESSION_DIRECTORY;
    private static final int STORAGE_TYPE_PATH_SEGMENT;
    private static final String TAG = Log.tag(BlobProvider.class);
    private static final UriMatcher URI_MATCHER = new UriMatcher(-1) { // from class: org.thoughtcrime.securesms.providers.BlobProvider.1
        {
            addURI(BlobProvider.AUTHORITY, BlobProvider.PATH, 1);
        }
    };
    private volatile boolean initialized = false;
    private final Map<Uri, byte[]> memoryBlobs = new HashMap();

    /* loaded from: classes4.dex */
    public interface ErrorListener {
        void onError(IOException iOException);
    }

    /* loaded from: classes4.dex */
    public interface SuccessListener {
        void onSuccess();
    }

    public static BlobProvider getInstance() {
        return INSTANCE;
    }

    public MemoryBlobBuilder forData(byte[] bArr) {
        return new MemoryBlobBuilder(bArr);
    }

    public BlobBuilder forData(InputStream inputStream, long j) {
        return new BlobBuilder(inputStream, j);
    }

    public synchronized InputStream getStream(Context context, Uri uri) throws IOException {
        waitUntilInitialized();
        return getStream(context, uri, 0);
    }

    public synchronized InputStream getStream(Context context, Uri uri, long j) throws IOException {
        waitUntilInitialized();
        return (InputStream) getBlobRepresentation(context, uri, new IOFunction(j, uri) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda9
            public final /* synthetic */ long f$0;
            public final /* synthetic */ Uri f$1;

            {
                this.f$0 = r1;
                this.f$1 = r3;
            }

            @Override // org.thoughtcrime.securesms.util.IOFunction
            public final Object apply(Object obj) {
                return BlobProvider.lambda$getStream$0(this.f$0, this.f$1, (byte[]) obj);
            }
        }, new IOFunction(context, j) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda10
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.thoughtcrime.securesms.util.IOFunction
            public final Object apply(Object obj) {
                return BlobProvider.this.lambda$getStream$1(this.f$1, this.f$2, (File) obj);
            }
        });
    }

    public static /* synthetic */ InputStream lambda$getStream$0(long j, Uri uri, byte[] bArr) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        if (byteArrayInputStream.skip(j) == j) {
            return byteArrayInputStream;
        }
        throw new IOException("Failed to skip to position " + j + " for: " + uri);
    }

    public /* synthetic */ InputStream lambda$getStream$1(Context context, long j, File file) throws IOException {
        return ModernDecryptingPartInputStream.createFor(getAttachmentSecret(context), file, j);
    }

    public synchronized MediaDataSource getMediaDataSource(Context context, Uri uri) throws IOException {
        waitUntilInitialized();
        return (MediaDataSource) getBlobRepresentation(context, uri, new IOFunction() { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.util.IOFunction
            public final Object apply(Object obj) {
                return new ByteArrayMediaDataSource((byte[]) obj);
            }
        }, new IOFunction(context) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda8
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.util.IOFunction
            public final Object apply(Object obj) {
                return BlobProvider.this.lambda$getMediaDataSource$2(this.f$1, (File) obj);
            }
        });
    }

    public /* synthetic */ MediaDataSource lambda$getMediaDataSource$2(Context context, File file) throws IOException {
        return EncryptedMediaDataSource.createForDiskBlob(getAttachmentSecret(context), file);
    }

    private synchronized <T> T getBlobRepresentation(Context context, Uri uri, IOFunction<byte[], T> iOFunction, IOFunction<File, T> iOFunction2) throws IOException {
        if (isAuthority(uri)) {
            StorageType decode = StorageType.decode(uri.getPathSegments().get(1));
            if (decode.isMemory()) {
                byte[] bArr = this.memoryBlobs.get(uri);
                if (bArr != null) {
                    if (decode == StorageType.SINGLE_USE_MEMORY) {
                        this.memoryBlobs.remove(uri);
                    }
                    return iOFunction.apply(bArr);
                }
                throw new IOException("Failed to find in-memory blob for: " + uri);
            }
            return iOFunction2.apply(new File(getOrCreateDirectory(context, getDirectory(decode)), buildFileName(uri.getPathSegments().get(5))));
        }
        throw new IOException("Provided URI does not match this spec. Uri: " + uri);
    }

    private synchronized AttachmentSecret getAttachmentSecret(Context context) {
        return AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret();
    }

    public synchronized void delete(Context context, Uri uri) {
        waitUntilInitialized();
        if (!isAuthority(uri)) {
            String str = TAG;
            Log.d(str, "Can't delete. Not the authority for uri: " + uri);
            return;
        }
        String str2 = TAG;
        Log.d(str2, "Deleting " + getId(uri));
        try {
            StorageType decode = StorageType.decode(uri.getPathSegments().get(1));
            if (decode.isMemory()) {
                this.memoryBlobs.remove(uri);
            } else if (new File(getOrCreateDirectory(context, getDirectory(decode)), buildFileName(uri.getPathSegments().get(5))).delete()) {
                Log.d(str2, "Successfully deleted " + getId(uri));
            } else {
                throw new IOException("File wasn't deleted.");
            }
        } catch (IOException e) {
            String str3 = TAG;
            Log.w(str3, "Failed to delete uri: " + getId(uri), e);
        }
    }

    public synchronized void initialize(Context context) {
        SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda11
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlobProvider.this.lambda$initialize$3(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$initialize$3(Context context) {
        synchronized (this) {
            File[] listFiles = getOrCreateDirectory(context, SINGLE_SESSION_DIRECTORY).listFiles();
            if (listFiles != null) {
                for (File file : listFiles) {
                    if (file.delete()) {
                        Log.d(TAG, "Deleted single-session file: " + file.getName());
                    } else {
                        Log.w(TAG, "Failed to delete single-session file! " + file.getName());
                    }
                }
            } else {
                Log.w(TAG, "Null directory listing!");
            }
            deleteOrphanedDraftFiles(context);
            Log.i(TAG, "Initialized.");
            this.initialized = true;
            notifyAll();
        }
    }

    private static void deleteOrphanedDraftFiles(Context context) {
        File[] listFiles = getOrCreateDirectory(context, DRAFT_ATTACHMENTS_DIRECTORY).listFiles();
        if (listFiles == null || listFiles.length == 0) {
            Log.d(TAG, "No attachment drafts exist. Skipping.");
            return;
        }
        List list = (List) Collection$EL.stream(SignalDatabase.drafts().getAllVoiceNoteDrafts()).map(new Function() { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return VoiceNoteDraft.fromDraft((DraftDatabase.Draft) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((VoiceNoteDraft) obj).getUri();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BlobProvider.getId((Uri) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda5
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((String) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BlobProvider.buildFileName((String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        for (File file : listFiles) {
            if (!list.contains(file.getName())) {
                if (file.delete()) {
                    Log.d(TAG, "Deleted orphaned attachment draft: " + file.getName());
                } else {
                    Log.d(TAG, "Failed to delete orphaned attachment draft: " + file.getName());
                }
            }
        }
    }

    public static String getMimeType(Uri uri) {
        if (isAuthority(uri)) {
            return uri.getPathSegments().get(2);
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (isAuthority(uri)) {
            return uri.getPathSegments().get(3);
        }
        return null;
    }

    public static Long getFileSize(Uri uri) {
        if (isAuthority(uri)) {
            try {
                return Long.valueOf(Long.parseLong(uri.getPathSegments().get(4)));
            } catch (NumberFormatException unused) {
            }
        }
        return null;
    }

    public static String getId(Uri uri) {
        if (isAuthority(uri)) {
            return uri.getPathSegments().get(5);
        }
        return null;
    }

    public long calculateFileSize(Context context, Uri uri) {
        if (!isAuthority(uri)) {
            return 0;
        }
        try {
            InputStream stream = getStream(context, uri);
            long streamLength = StreamUtil.getStreamLength(stream);
            if (stream != null) {
                stream.close();
            }
            return streamLength;
        } catch (IOException e) {
            Log.w(TAG, e);
            return 0;
        }
    }

    public static boolean isAuthority(Uri uri) {
        return URI_MATCHER.match(uri) == 1;
    }

    public synchronized Uri writeBlobSpecToDisk(Context context, BlobSpec blobSpec) throws IOException {
        Uri writeBlobSpecToDiskAsync;
        waitUntilInitialized();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        AtomicReference atomicReference = new AtomicReference(null);
        writeBlobSpecToDiskAsync = writeBlobSpecToDiskAsync(context, blobSpec, new SuccessListener(countDownLatch) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda0
            public final /* synthetic */ CountDownLatch f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.providers.BlobProvider.SuccessListener
            public final void onSuccess() {
                this.f$0.countDown();
            }
        }, new ErrorListener(atomicReference, countDownLatch) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda1
            public final /* synthetic */ AtomicReference f$0;
            public final /* synthetic */ CountDownLatch f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.providers.BlobProvider.ErrorListener
            public final void onError(IOException iOException) {
                BlobProvider.lambda$writeBlobSpecToDisk$4(this.f$0, this.f$1, iOException);
            }
        });
        try {
            countDownLatch.await();
            if (atomicReference.get() != null) {
                throw ((IOException) atomicReference.get());
            }
        } catch (InterruptedException e) {
            throw new IOException(e);
        }
        return writeBlobSpecToDiskAsync;
    }

    public static /* synthetic */ void lambda$writeBlobSpecToDisk$4(AtomicReference atomicReference, CountDownLatch countDownLatch, IOException iOException) {
        atomicReference.set(iOException);
        countDownLatch.countDown();
    }

    public synchronized Uri writeBlobSpecToDiskAsync(Context context, BlobSpec blobSpec, SuccessListener successListener, ErrorListener errorListener) throws IOException {
        SignalExecutors.UNBOUNDED.execute(new Runnable((OutputStream) ModernEncryptingPartOutputStream.createFor(AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), new File(getOrCreateDirectory(context, getDirectory(blobSpec.getStorageType())), buildFileName(blobSpec.id)), true).second, successListener, errorListener) { // from class: org.thoughtcrime.securesms.providers.BlobProvider$$ExternalSyntheticLambda12
            public final /* synthetic */ OutputStream f$1;
            public final /* synthetic */ BlobProvider.SuccessListener f$2;
            public final /* synthetic */ BlobProvider.ErrorListener f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                BlobProvider.lambda$writeBlobSpecToDiskAsync$5(BlobProvider.BlobSpec.this, this.f$1, this.f$2, this.f$3);
            }
        });
        return buildUri(blobSpec);
    }

    public static /* synthetic */ void lambda$writeBlobSpecToDiskAsync$5(BlobSpec blobSpec, OutputStream outputStream, SuccessListener successListener, ErrorListener errorListener) {
        try {
            StreamUtil.copy(blobSpec.getData(), outputStream);
            if (successListener != null) {
                successListener.onSuccess();
            }
        } catch (IOException e) {
            Log.w(TAG, "Error during write!", e);
            if (errorListener != null) {
                errorListener.onError(e);
            }
        }
    }

    public synchronized Uri writeBlobSpecToMemory(BlobSpec blobSpec, byte[] bArr) {
        Uri buildUri;
        buildUri = buildUri(blobSpec);
        this.memoryBlobs.put(buildUri, bArr);
        return buildUri;
    }

    public static String buildFileName(String str) {
        return str + ".blob";
    }

    /* renamed from: org.thoughtcrime.securesms.providers.BlobProvider$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType;

        static {
            int[] iArr = new int[StorageType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType = iArr;
            try {
                iArr[StorageType.SINGLE_USE_MEMORY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType[StorageType.SINGLE_SESSION_MEMORY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType[StorageType.SINGLE_SESSION_DISK.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType[StorageType.MULTI_SESSION_DISK.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType[StorageType.ATTACHMENT_DRAFT.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
        }
    }

    private static String getDirectory(StorageType storageType) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$providers$BlobProvider$StorageType[storageType.ordinal()];
        if (i == 1 || i == 2) {
            throw new IllegalArgumentException("In-Memory Blobs do not have directories.");
        } else if (i == 3) {
            return SINGLE_SESSION_DIRECTORY;
        } else {
            if (i == 4) {
                return MULTI_SESSION_DIRECTORY;
            }
            if (i != 5) {
                return storageType == StorageType.MULTI_SESSION_DISK ? MULTI_SESSION_DIRECTORY : SINGLE_SESSION_DIRECTORY;
            }
            return DRAFT_ATTACHMENTS_DIRECTORY;
        }
    }

    private static Uri buildUri(BlobSpec blobSpec) {
        return CONTENT_URI.buildUpon().appendPath(blobSpec.getStorageType().encode()).appendPath(blobSpec.getMimeType()).appendPath(blobSpec.getFileName()).appendEncodedPath(String.valueOf(blobSpec.getFileSize())).appendPath(blobSpec.getId()).build();
    }

    private static File getOrCreateDirectory(Context context, String str) {
        return context.getDir(str, 0);
    }

    public File forNonAutoEncryptingSingleSessionOnDisk(Context context) {
        return new File(getOrCreateDirectory(context, getDirectory(StorageType.SINGLE_SESSION_DISK)), buildFileName(UUID.randomUUID().toString()));
    }

    /* loaded from: classes4.dex */
    public class BlobBuilder {
        private InputStream data;
        private String fileName;
        private long fileSize;
        private String id;
        private String mimeType;

        private BlobBuilder(InputStream inputStream, long j) {
            BlobProvider.this = r1;
            this.id = UUID.randomUUID().toString();
            this.data = inputStream;
            this.fileSize = j;
        }

        public BlobBuilder withMimeType(String str) {
            this.mimeType = str;
            return this;
        }

        public BlobBuilder withFileName(String str) {
            this.fileName = str;
            return this;
        }

        protected BlobSpec buildBlobSpec(StorageType storageType) {
            return new BlobSpec(this.data, this.id, storageType, this.mimeType, this.fileName, this.fileSize);
        }

        public Uri createForSingleSessionOnDisk(Context context) throws IOException {
            return BlobProvider.this.writeBlobSpecToDisk(context, buildBlobSpec(StorageType.SINGLE_SESSION_DISK));
        }

        public Uri createForSingleSessionOnDiskAsync(Context context, SuccessListener successListener, ErrorListener errorListener) throws IOException {
            return BlobProvider.this.writeBlobSpecToDiskAsync(context, buildBlobSpec(StorageType.SINGLE_SESSION_DISK), successListener, errorListener);
        }

        public Uri createForMultipleSessionsOnDisk(Context context) throws IOException {
            return BlobProvider.this.writeBlobSpecToDisk(context, buildBlobSpec(StorageType.MULTI_SESSION_DISK));
        }

        public Uri createForDraftAttachmentAsync(Context context, SuccessListener successListener, ErrorListener errorListener) throws IOException {
            return BlobProvider.this.writeBlobSpecToDiskAsync(context, buildBlobSpec(StorageType.ATTACHMENT_DRAFT), successListener, errorListener);
        }
    }

    private synchronized void waitUntilInitialized() {
        if (!this.initialized) {
            Log.i(TAG, "Waiting for initialization...");
            synchronized (this) {
                while (!this.initialized) {
                    Util.wait(this, 0);
                }
                Log.i(TAG, "Initialization complete.");
            }
        }
    }

    /* loaded from: classes4.dex */
    public class MemoryBlobBuilder extends BlobBuilder {
        private byte[] data;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        private MemoryBlobBuilder(byte[] bArr) {
            super(new ByteArrayInputStream(bArr), (long) bArr.length);
            BlobProvider.this = r7;
            this.data = bArr;
        }

        @Override // org.thoughtcrime.securesms.providers.BlobProvider.BlobBuilder
        public MemoryBlobBuilder withMimeType(String str) {
            super.withMimeType(str);
            return this;
        }

        @Override // org.thoughtcrime.securesms.providers.BlobProvider.BlobBuilder
        public MemoryBlobBuilder withFileName(String str) {
            super.withFileName(str);
            return this;
        }

        public Uri createForSingleUseInMemory() {
            return BlobProvider.this.writeBlobSpecToMemory(buildBlobSpec(StorageType.SINGLE_USE_MEMORY), this.data);
        }

        public Uri createForSingleSessionInMemory() {
            return BlobProvider.this.writeBlobSpecToMemory(buildBlobSpec(StorageType.SINGLE_SESSION_MEMORY), this.data);
        }
    }

    /* loaded from: classes4.dex */
    public static class BlobSpec {
        private final InputStream data;
        private final String fileName;
        private final long fileSize;
        private final String id;
        private final String mimeType;
        private final StorageType storageType;

        private BlobSpec(InputStream inputStream, String str, StorageType storageType, String str2, String str3, long j) {
            this.data = inputStream;
            this.id = str;
            this.storageType = storageType;
            this.mimeType = str2;
            this.fileName = str3;
            this.fileSize = j;
        }

        public InputStream getData() {
            return this.data;
        }

        public String getId() {
            return this.id;
        }

        public StorageType getStorageType() {
            return this.storageType;
        }

        public String getMimeType() {
            return this.mimeType;
        }

        public String getFileName() {
            return this.fileName;
        }

        public long getFileSize() {
            return this.fileSize;
        }
    }

    /* loaded from: classes4.dex */
    public enum StorageType {
        SINGLE_USE_MEMORY("single-use-memory", true),
        SINGLE_SESSION_MEMORY("single-session-memory", true),
        SINGLE_SESSION_DISK("single-session-disk", false),
        MULTI_SESSION_DISK("multi-session-disk", false),
        ATTACHMENT_DRAFT("attachment-draft", false);
        
        private final String encoded;
        private final boolean inMemory;

        StorageType(String str, boolean z) {
            this.encoded = str;
            this.inMemory = z;
        }

        public String encode() {
            return this.encoded;
        }

        public boolean isMemory() {
            return this.inMemory;
        }

        public static StorageType decode(String str) throws IOException {
            StorageType[] values = values();
            for (StorageType storageType : values) {
                if (storageType.encoded.equals(str)) {
                    return storageType;
                }
            }
            throw new IOException("Failed to decode lifespan.");
        }
    }
}
