package org.thoughtcrime.securesms.providers;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.MemoryFileUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class BlobContentProvider extends BaseContentProvider {
    private static final String TAG = Log.tag(BlobContentProvider.class);

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.providers.BaseContentProvider, android.content.ContentProvider
    public /* bridge */ /* synthetic */ void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
    }

    @Override // android.content.ContentProvider
    public boolean onCreate() {
        Log.i(TAG, "onCreate()");
        return true;
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        String str2 = TAG;
        Log.i(str2, "openFile() called: " + uri);
        try {
            InputStream stream = BlobProvider.getInstance().getStream(ApplicationDependencies.getApplication(), uri);
            Long fileSize = BlobProvider.getFileSize(uri);
            if (fileSize != null) {
                ParcelFileDescriptor parcelStreamForStream = getParcelStreamForStream(stream, Util.toIntExact(fileSize.longValue()));
                if (stream != null) {
                    stream.close();
                }
                return parcelStreamForStream;
            }
            Log.w(str2, "No file size available");
            throw new FileNotFoundException();
        } catch (IOException unused) {
            throw new FileNotFoundException();
        }
    }

    private static ParcelFileDescriptor getParcelStreamForStream(InputStream inputStream, int i) throws IOException {
        MemoryFile memoryFile = new MemoryFile(null, i);
        OutputStream outputStream = memoryFile.getOutputStream();
        try {
            StreamUtil.copy(inputStream, outputStream);
            if (outputStream != null) {
                outputStream.close();
            }
            return MemoryFileUtil.getParcelFileDescriptor(memoryFile);
        } catch (Throwable th) {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3 = TAG;
        Log.i(str3, "query() called: " + uri);
        if (strArr == null || strArr.length <= 0) {
            return null;
        }
        String mimeType = BlobProvider.getMimeType(uri);
        String fileName = BlobProvider.getFileName(uri);
        Long fileSize = BlobProvider.getFileSize(uri);
        if (fileSize == null) {
            Log.w(str3, "No file size");
            return null;
        } else if (mimeType == null) {
            Log.w(str3, "No mime type");
            return null;
        } else {
            if (fileName == null) {
                fileName = BaseContentProvider.createFileNameForMimeType(mimeType);
            }
            return BaseContentProvider.createCursor(strArr, fileName, fileSize.longValue());
        }
    }

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        return BlobProvider.getMimeType(uri);
    }
}
