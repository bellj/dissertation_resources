package org.thoughtcrime.securesms.providers;

import android.content.ContentUris;
import android.content.Context;
import android.content.UriMatcher;
import android.net.Uri;
import android.webkit.MimeTypeMap;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.AttachmentSecret;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.ClassicDecryptingPartInputStream;
import org.thoughtcrime.securesms.crypto.ModernDecryptingPartInputStream;
import org.thoughtcrime.securesms.util.FileProviderUtil;
import org.thoughtcrime.securesms.util.MediaUtil;

@Deprecated
/* loaded from: classes4.dex */
public class DeprecatedPersistentBlobProvider {
    public static final String AUTHORITY;
    private static final String BLOB_EXTENSION;
    public static final Uri CONTENT_URI = Uri.parse(URI_STRING);
    public static final String EXPECTED_PATH_NEW;
    public static final String EXPECTED_PATH_OLD;
    private static final int FILENAME_PATH_SEGMENT;
    private static final int FILESIZE_PATH_SEGMENT;
    private static final UriMatcher MATCHER = new UriMatcher(-1) { // from class: org.thoughtcrime.securesms.providers.DeprecatedPersistentBlobProvider.1
        {
            addURI("org.thoughtcrime.securesms", DeprecatedPersistentBlobProvider.EXPECTED_PATH_OLD, 1);
            addURI("org.thoughtcrime.securesms", DeprecatedPersistentBlobProvider.EXPECTED_PATH_NEW, 2);
        }
    };
    private static final int MATCH_NEW;
    private static final int MATCH_OLD;
    private static final int MIMETYPE_PATH_SEGMENT;
    private static final String TAG = Log.tag(DeprecatedPersistentBlobProvider.class);
    private static final String URI_STRING;
    private static volatile DeprecatedPersistentBlobProvider instance;
    private final AttachmentSecret attachmentSecret;

    @Deprecated
    public static DeprecatedPersistentBlobProvider getInstance(Context context) {
        if (instance == null) {
            synchronized (DeprecatedPersistentBlobProvider.class) {
                if (instance == null) {
                    instance = new DeprecatedPersistentBlobProvider(context);
                }
            }
        }
        return instance;
    }

    private DeprecatedPersistentBlobProvider(Context context) {
        this.attachmentSecret = AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret();
    }

    public Uri createForExternal(Context context, String str) throws IOException {
        File externalDir = getExternalDir(context);
        return FileProviderUtil.getUriFor(context, new File(externalDir, String.valueOf(System.currentTimeMillis()) + "." + getExtensionFromMimeType(str)));
    }

    public boolean delete(Context context, Uri uri) {
        int match = MATCHER.match(uri);
        if (match == 1 || match == 2) {
            ContentUris.parseId(uri);
            return getFile(context, ContentUris.parseId(uri)).file.delete();
        } else if (isExternalBlobUri(context, uri)) {
            return FileProviderUtil.delete(context, uri);
        } else {
            return false;
        }
    }

    public InputStream getStream(Context context, long j) throws IOException {
        FileData file = getFile(context, j);
        if (file.modern) {
            return ModernDecryptingPartInputStream.createFor(this.attachmentSecret, file.file, 0);
        }
        return ClassicDecryptingPartInputStream.createFor(this.attachmentSecret, file.file);
    }

    private FileData getFile(Context context, long j) {
        File legacyFile = getLegacyFile(context, j);
        File cacheFile = getCacheFile(context, j);
        File modernCacheFile = getModernCacheFile(context, j);
        if (legacyFile.exists()) {
            return new FileData(legacyFile, false);
        }
        if (cacheFile.exists()) {
            return new FileData(cacheFile, false);
        }
        return new FileData(modernCacheFile, true);
    }

    private File getLegacyFile(Context context, long j) {
        File dir = context.getDir("captures", 0);
        return new File(dir, j + "." + BLOB_EXTENSION);
    }

    private File getCacheFile(Context context, long j) {
        File cacheDir = context.getCacheDir();
        return new File(cacheDir, "capture-" + j + "." + BLOB_EXTENSION);
    }

    private File getModernCacheFile(Context context, long j) {
        File cacheDir = context.getCacheDir();
        return new File(cacheDir, "capture-m-" + j + "." + BLOB_EXTENSION);
    }

    public static String getMimeType(Context context, Uri uri) {
        if (!isAuthority(context, uri)) {
            return null;
        }
        if (isExternalBlobUri(context, uri)) {
            return getMimeTypeFromExtension(uri);
        }
        return uri.getPathSegments().get(1);
    }

    public static String getFileName(Context context, Uri uri) {
        if (isAuthority(context, uri) && !isExternalBlobUri(context, uri) && MATCHER.match(uri) != 1) {
            return uri.getPathSegments().get(2);
        }
        return null;
    }

    public static Long getFileSize(Context context, Uri uri) {
        if (!isAuthority(context, uri) || isExternalBlobUri(context, uri) || MATCHER.match(uri) == 1) {
            return null;
        }
        try {
            return Long.valueOf(uri.getPathSegments().get(3));
        } catch (NumberFormatException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    private static String getExtensionFromMimeType(String str) {
        String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(str);
        return extensionFromMimeType != null ? extensionFromMimeType : BLOB_EXTENSION;
    }

    private static String getMimeTypeFromExtension(Uri uri) {
        String mimeTypeFromExtension = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(uri.toString()));
        return mimeTypeFromExtension != null ? mimeTypeFromExtension : MediaUtil.OCTET;
    }

    private static File getExternalDir(Context context) throws IOException {
        File externalCacheDir = context.getExternalCacheDir();
        if (externalCacheDir != null) {
            return externalCacheDir;
        }
        throw new IOException("no external files directory");
    }

    public static boolean isAuthority(Context context, Uri uri) {
        int match = MATCHER.match(uri);
        if (match == 2 || match == 1 || isExternalBlobUri(context, uri)) {
            return true;
        }
        return false;
    }

    private static boolean isExternalBlobUri(Context context, Uri uri) {
        try {
            if (!uri.getPath().startsWith(getExternalDir(context).getAbsolutePath())) {
                if (!FileProviderUtil.isAuthority(uri)) {
                    return false;
                }
            }
            return true;
        } catch (IOException e) {
            Log.w(TAG, "Failed to determine if it's an external blob URI.", e);
            return false;
        }
    }

    /* loaded from: classes4.dex */
    public static class FileData {
        private final File file;
        private final boolean modern;

        private FileData(File file, boolean z) {
            this.file = file;
            this.modern = z;
        }
    }
}
