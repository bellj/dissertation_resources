package org.thoughtcrime.securesms.providers;

import android.content.ContentProvider;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import java.util.ArrayList;

/* loaded from: classes4.dex */
abstract class BaseContentProvider extends ContentProvider {
    private static final String[] COLUMNS = {"_display_name", "_size"};

    @Override // android.content.ContentProvider
    public void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
        if (providerInfo.exported) {
            throw new SecurityException("Provider must not be exported");
        } else if (!providerInfo.grantUriPermissions) {
            throw new SecurityException("Provider must grant uri permissions");
        }
    }

    public static Cursor createCursor(String[] strArr, String str, long j) {
        if (strArr == null || strArr.length == 0) {
            strArr = COLUMNS;
        }
        ArrayList arrayList = new ArrayList(strArr.length);
        ArrayList arrayList2 = new ArrayList(strArr.length);
        for (String str2 : strArr) {
            if ("_display_name".equals(str2)) {
                arrayList.add("_display_name");
                arrayList2.add(str);
            } else if ("_size".equals(str2)) {
                arrayList.add("_size");
                arrayList2.add(Long.valueOf(j));
            }
        }
        MatrixCursor matrixCursor = new MatrixCursor((String[]) arrayList.toArray(new String[0]), 1);
        matrixCursor.addRow(arrayList2.toArray(new Object[0]));
        return matrixCursor;
    }

    public static String createFileNameForMimeType(String str) {
        return str.replace('/', '.');
    }
}
