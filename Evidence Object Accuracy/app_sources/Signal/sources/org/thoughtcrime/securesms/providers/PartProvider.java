package org.thoughtcrime.securesms.providers;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;
import android.os.ProxyFileDescriptorCallback;
import android.os.storage.StorageManager;
import android.system.ErrnoException;
import android.system.OsConstants;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.mms.PartUriParser;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.MemoryFileUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class PartProvider extends BaseContentProvider {
    private static final String CONTENT_AUTHORITY;
    private static final Uri CONTENT_URI = Uri.parse(CONTENT_URI_STRING);
    private static final String CONTENT_URI_STRING;
    private static final int SINGLE_ROW;
    private static final String TAG = Log.tag(PartProvider.class);
    private static final UriMatcher uriMatcher;

    @Override // org.thoughtcrime.securesms.providers.BaseContentProvider, android.content.ContentProvider
    public /* bridge */ /* synthetic */ void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
    }

    static {
        TAG = Log.tag(PartProvider.class);
        CONTENT_URI = Uri.parse(CONTENT_URI_STRING);
        UriMatcher uriMatcher2 = new UriMatcher(-1);
        uriMatcher = uriMatcher2;
        uriMatcher2.addURI(CONTENT_AUTHORITY, "part/*/#", 1);
    }

    @Override // android.content.ContentProvider
    public boolean onCreate() {
        Log.i(TAG, "onCreate()");
        return true;
    }

    public static Uri getContentUri(AttachmentId attachmentId) {
        return ContentUris.withAppendedId(Uri.withAppendedPath(CONTENT_URI, String.valueOf(attachmentId.getUniqueId())), attachmentId.getRowId());
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        String str2 = TAG;
        Log.i(str2, "openFile() called!");
        if (KeyCachingService.isLocked(getContext())) {
            Log.w(str2, "masterSecret was null, abandoning.");
            return null;
        } else if (SignalDatabase.getInstance() == null) {
            Log.w(str2, "SignalDatabase unavailable");
            return null;
        } else if (uriMatcher.match(uri) == 1) {
            Log.i(str2, "Parting out a single row...");
            try {
                PartUriParser partUriParser = new PartUriParser(uri);
                if (Build.VERSION.SDK_INT >= 26) {
                    return getParcelStreamProxyForAttachment(partUriParser.getPartId());
                }
                return getParcelStreamForAttachment(partUriParser.getPartId());
            } catch (IOException e) {
                Log.w(TAG, e);
                throw new FileNotFoundException("Error opening file");
            }
        } else {
            throw new FileNotFoundException("Request for bad part.");
        }
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        Log.i(TAG, "delete() called");
        return 0;
    }

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        String str = TAG;
        Log.i(str, "getType() called: " + uri);
        if (SignalDatabase.getInstance() == null) {
            Log.w(str, "SignalDatabase unavailable");
            return null;
        }
        if (uriMatcher.match(uri) == 1) {
            DatabaseAttachment attachment = SignalDatabase.attachments().getAttachment(new PartUriParser(uri).getPartId());
            if (attachment != null) {
                Log.i(str, "getType() called: " + uri + " It's " + attachment.getContentType());
                return attachment.getContentType();
            }
        }
        return null;
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        Log.i(TAG, "insert() called");
        return null;
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        String str3;
        String str4 = TAG;
        Log.i(str4, "query() called: " + uri);
        if (SignalDatabase.getInstance() == null) {
            Log.w(str4, "SignalDatabase unavailable");
            return null;
        } else if (uriMatcher.match(uri) != 1) {
            return null;
        } else {
            DatabaseAttachment attachment = SignalDatabase.attachments().getAttachment(new PartUriParser(uri).getPartId());
            if (attachment == null) {
                return null;
            }
            long size = attachment.getSize();
            if (size <= 0) {
                Log.w(str4, "Empty file " + size);
                return null;
            }
            if (attachment.getFileName() != null) {
                str3 = attachment.getFileName();
            } else {
                str3 = BaseContentProvider.createFileNameForMimeType(attachment.getContentType());
            }
            return BaseContentProvider.createCursor(strArr, str3, size);
        }
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        Log.i(TAG, "update() called");
        return 0;
    }

    private ParcelFileDescriptor getParcelStreamForAttachment(AttachmentId attachmentId) throws IOException {
        MemoryFile memoryFile = new MemoryFile(attachmentId.toString(), Util.toIntExact(StreamUtil.getStreamLength(SignalDatabase.attachments().getAttachmentStream(attachmentId, 0))));
        InputStream attachmentStream = SignalDatabase.attachments().getAttachmentStream(attachmentId, 0);
        OutputStream outputStream = memoryFile.getOutputStream();
        StreamUtil.copy(attachmentStream, outputStream);
        StreamUtil.close(outputStream);
        StreamUtil.close(attachmentStream);
        return MemoryFileUtil.getParcelFileDescriptor(memoryFile);
    }

    private ParcelFileDescriptor getParcelStreamProxyForAttachment(AttachmentId attachmentId) throws IOException {
        StorageManager storageManager = (StorageManager) getContext().getSystemService(StorageManager.class);
        Objects.requireNonNull(storageManager);
        ParcelFileDescriptor openProxyFileDescriptor = storageManager.openProxyFileDescriptor(SQLiteDatabase.CREATE_IF_NECESSARY, new ProxyCallback(SignalDatabase.attachments(), attachmentId), new Handler(SignalExecutors.getAndStartHandlerThread("storageservice-proxy").getLooper()));
        String str = TAG;
        Log.i(str, attachmentId + ":createdProxy");
        return openProxyFileDescriptor;
    }

    /* loaded from: classes4.dex */
    public static final class ProxyCallback extends ProxyFileDescriptorCallback {
        private AttachmentId attachmentId;
        private AttachmentDatabase attachments;

        public ProxyCallback(AttachmentDatabase attachmentDatabase, AttachmentId attachmentId) {
            this.attachments = attachmentDatabase;
            this.attachmentId = attachmentId;
        }

        @Override // android.os.ProxyFileDescriptorCallback
        public long onGetSize() throws ErrnoException {
            DatabaseAttachment attachment = this.attachments.getAttachment(this.attachmentId);
            if (attachment == null || attachment.getSize() <= 0) {
                String str = PartProvider.TAG;
                Log.w(str, this.attachmentId + ":getSize:attachment is null or size is 0");
                throw new ErrnoException("Attachment is invalid", OsConstants.ENOENT);
            }
            String str2 = PartProvider.TAG;
            Log.i(str2, this.attachmentId + ":getSize");
            return attachment.getSize();
        }

        @Override // android.os.ProxyFileDescriptorCallback
        public int onRead(long j, int i, byte[] bArr) throws ErrnoException {
            try {
                DatabaseAttachment attachment = this.attachments.getAttachment(this.attachmentId);
                if (attachment == null || attachment.getSize() <= 0) {
                    String str = PartProvider.TAG;
                    Log.w(str, this.attachmentId + ":onRead:attachment is null or size is 0");
                    throw new ErrnoException("Attachment is invalid", OsConstants.ENOENT);
                }
                String str2 = PartProvider.TAG;
                Log.i(str2, this.attachmentId + ":onRead");
                InputStream attachmentStream = this.attachments.getAttachmentStream(this.attachmentId, j);
                int i2 = 0;
                while (i2 < i) {
                    int read = attachmentStream.read(bArr, i2, Math.max(0, i - i2));
                    if (read < 0) {
                        break;
                    }
                    i2 += read;
                }
                if (attachmentStream != null) {
                    attachmentStream.close();
                }
                return i2;
            } catch (IOException e) {
                String str3 = PartProvider.TAG;
                Log.w(str3, this.attachmentId + ":onRead:attachment read failed", e);
                throw new ErrnoException("Error reading", OsConstants.EIO, e);
            }
        }

        @Override // android.os.ProxyFileDescriptorCallback
        public void onRelease() {
            String str = PartProvider.TAG;
            Log.i(str, this.attachmentId + ":onRelease");
            this.attachments = null;
            this.attachmentId = null;
        }
    }
}
