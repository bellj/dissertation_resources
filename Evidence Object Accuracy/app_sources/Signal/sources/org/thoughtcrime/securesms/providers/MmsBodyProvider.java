package org.thoughtcrime.securesms.providers;

import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class MmsBodyProvider extends BaseContentProvider {
    private static final String CONTENT_AUTHORITY;
    public static final Uri CONTENT_URI = Uri.parse(CONTENT_URI_STRING);
    private static final String CONTENT_URI_STRING;
    private static final int SINGLE_ROW;
    private static final String TAG = Log.tag(MmsBodyProvider.class);
    private static final UriMatcher uriMatcher;

    @Override // android.content.ContentProvider
    public String getType(Uri uri) {
        return null;
    }

    @Override // android.content.ContentProvider
    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    @Override // android.content.ContentProvider
    public boolean onCreate() {
        return true;
    }

    @Override // android.content.ContentProvider
    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    @Override // android.content.ContentProvider
    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    @Override // org.thoughtcrime.securesms.providers.BaseContentProvider, android.content.ContentProvider
    public /* bridge */ /* synthetic */ void attachInfo(Context context, ProviderInfo providerInfo) {
        super.attachInfo(context, providerInfo);
    }

    static {
        TAG = Log.tag(MmsBodyProvider.class);
        CONTENT_URI = Uri.parse(CONTENT_URI_STRING);
        UriMatcher uriMatcher2 = new UriMatcher(-1);
        uriMatcher = uriMatcher2;
        uriMatcher2.addURI(CONTENT_AUTHORITY, "mms/#", 1);
    }

    private File getFile(Uri uri) {
        long parseLong = Long.parseLong(uri.getPathSegments().get(1));
        File cacheDir = getContext().getCacheDir();
        return new File(cacheDir, parseLong + ".mmsbody");
    }

    @Override // android.content.ContentProvider
    public ParcelFileDescriptor openFile(Uri uri, String str) throws FileNotFoundException {
        int i;
        String str2 = TAG;
        Log.i(str2, "openFile(" + uri + ", " + str + ")");
        if (uriMatcher.match(uri) == 1) {
            Log.i(str2, "Fetching message body for a single row...");
            File file = getFile(uri);
            str.hashCode();
            if (str.equals("r")) {
                i = SQLiteDatabase.CREATE_IF_NECESSARY;
            } else if (str.equals("w")) {
                i = 738197504;
            } else {
                throw new IllegalArgumentException("requested file mode unsupported");
            }
            Log.i(str2, "returning file " + file.getAbsolutePath());
            return ParcelFileDescriptor.open(file, i);
        }
        throw new FileNotFoundException("Request for bad message.");
    }

    @Override // android.content.ContentProvider
    public int delete(Uri uri, String str, String[] strArr) {
        if (uriMatcher.match(uri) != 1) {
            return 0;
        }
        return getFile(uri).delete() ? 1 : 0;
    }

    public static Pointer makeTemporaryPointer(Context context) {
        return new Pointer(context, ContentUris.withAppendedId(CONTENT_URI, System.currentTimeMillis()));
    }

    /* loaded from: classes4.dex */
    public static class Pointer {
        private final Context context;
        private final Uri uri;

        public Pointer(Context context, Uri uri) {
            this.context = context;
            this.uri = uri;
        }

        public Uri getUri() {
            return this.uri;
        }

        public OutputStream getOutputStream() throws FileNotFoundException {
            return this.context.getContentResolver().openOutputStream(this.uri, "w");
        }

        public InputStream getInputStream() throws FileNotFoundException {
            return this.context.getContentResolver().openInputStream(this.uri);
        }

        public void close() {
            this.context.getContentResolver().delete(this.uri, null, null);
        }
    }
}
