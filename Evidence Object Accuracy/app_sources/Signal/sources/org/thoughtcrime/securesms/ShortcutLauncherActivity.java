package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.TaskStackBuilder;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.CommunicationActions;

/* loaded from: classes.dex */
public class ShortcutLauncherActivity extends AppCompatActivity {
    private static final String KEY_RECIPIENT;

    public static Intent createIntent(Context context, RecipientId recipientId) {
        Intent intent = new Intent(context, ShortcutLauncherActivity.class);
        intent.setAction("android.intent.action.MAIN");
        intent.putExtra("recipient_id", recipientId.serialize());
        return intent;
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra("recipient_id");
        if (stringExtra == null) {
            Toast.makeText(this, (int) R.string.ShortcutLauncherActivity_invalid_shortcut, 0).show();
            startActivity(MainActivity.clearTop(this));
            finish();
            return;
        }
        CommunicationActions.startConversation(this, Recipient.live(RecipientId.from(stringExtra)).get(), null, TaskStackBuilder.create(this).addNextIntent(MainActivity.clearTop(this)));
        finish();
    }
}
