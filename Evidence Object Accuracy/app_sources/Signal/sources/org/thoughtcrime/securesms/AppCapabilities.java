package org.thoughtcrime.securesms;

import org.thoughtcrime.securesms.util.FeatureFlags;
import org.whispersystems.signalservice.api.account.AccountAttributes;

/* loaded from: classes.dex */
public final class AppCapabilities {
    private static final boolean ANNOUNCEMENT_GROUPS;
    private static final boolean CHANGE_NUMBER;
    private static final boolean GV1_MIGRATION;
    private static final boolean GV2_CAPABLE;
    private static final boolean SENDER_KEY;
    private static final boolean UUID_CAPABLE;

    private AppCapabilities() {
    }

    public static AccountAttributes.Capabilities getCapabilities(boolean z) {
        return new AccountAttributes.Capabilities(false, true, z, true, true, true, true, FeatureFlags.stories(), FeatureFlags.giftBadgeReceiveSupport());
    }
}
