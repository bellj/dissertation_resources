package org.thoughtcrime.securesms;

import android.app.Activity;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity;
import org.thoughtcrime.securesms.insights.InsightsLauncher;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes.dex */
public class MainNavigator {
    public static final int REQUEST_CONFIG_CHANGES;
    private final MainActivity activity;

    /* loaded from: classes.dex */
    public interface BackHandler {
        boolean onBackPressed();
    }

    public MainNavigator(MainActivity mainActivity) {
        this.activity = mainActivity;
    }

    public static MainNavigator get(Activity activity) {
        if (activity instanceof MainActivity) {
            return ((MainActivity) activity).getNavigator();
        }
        throw new IllegalArgumentException("Activity must be an instance of MainActivity!");
    }

    public boolean onBackPressed() {
        Fragment findFragmentById = getFragmentManager().findFragmentById(R.id.fragment_container);
        if (findFragmentById instanceof BackHandler) {
            return ((BackHandler) findFragmentById).onBackPressed();
        }
        return false;
    }

    public void goToConversation(RecipientId recipientId, long j, int i, int i2) {
        this.activity.startActivity(ConversationIntents.createBuilder(this.activity, recipientId, j).withDistributionType(i).withStartingPosition(i2).build());
        this.activity.overridePendingTransition(R.anim.slide_from_end, R.anim.fade_scale_out);
    }

    public void goToAppSettings() {
        MainActivity mainActivity = this.activity;
        mainActivity.startActivityForResult(AppSettingsActivity.home(mainActivity), REQUEST_CONFIG_CHANGES);
    }

    public void goToGroupCreation() {
        MainActivity mainActivity = this.activity;
        mainActivity.startActivity(CreateGroupActivity.newIntent(mainActivity));
    }

    public void goToInvite() {
        this.activity.startActivity(new Intent(this.activity, InviteActivity.class));
    }

    public void goToInsights() {
        InsightsLauncher.showInsightsDashboard(this.activity.getSupportFragmentManager());
    }

    private FragmentManager getFragmentManager() {
        return this.activity.getSupportFragmentManager();
    }
}
