package org.thoughtcrime.securesms;

/* loaded from: classes.dex */
public interface Unbindable {
    void unbind();
}
