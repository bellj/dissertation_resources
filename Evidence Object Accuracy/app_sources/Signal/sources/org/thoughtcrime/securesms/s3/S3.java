package org.thoughtcrime.securesms.s3;

import android.content.Context;
import j$.util.function.Function;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringNumberConversionsKt;
import kotlin.text.StringsKt__StringsKt;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSource;
import okio.HashingSink;
import okio.Okio;
import okio.Sink;
import org.signal.core.util.Hex;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.EncryptedStreamUtils;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.whispersystems.signalservice.api.push.exceptions.NonSuccessfulResponseCodeException;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.websocket.DefaultErrorMapper;
import org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper;
import org.whispersystems.signalservice.internal.websocket.ResponseMapper;

/* compiled from: S3.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001#B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J6\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\r0\f\"\u0004\b\u0000\u0010\r2\u0006\u0010\u000e\u001a\u00020\u00042\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\r0\u00102\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0007J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u000e\u001a\u00020\u0004H\u0007J\u0012\u0010\u0015\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u000e\u001a\u00020\u0004H\u0007J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u000e\u001a\u00020\u0004H\u0007J\u0010\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u0004H\u0007J*\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020!2\b\b\u0002\u0010\"\u001a\u00020\u001cH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0007\u001a\n \b*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/s3/S3;", "", "()V", "DYNAMIC_PATH", "", "S3_BASE", "STATIC_PATH", "TAG", "kotlin.jvm.PlatformType", "okHttpClient", "Lokhttp3/OkHttpClient;", "getAndVerifyObject", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "T", "endpoint", "clazz", "Ljava/lang/Class;", "md5", "", "getLong", "", "getMD5FromResponse", "response", "Lokhttp3/Response;", "getObject", "getObjectMD5", "getString", "verifyAndWriteToDisk", "", "context", "Landroid/content/Context;", "objectPathOnNetwork", "objectFileOnDisk", "Ljava/io/File;", "doNotEncrypt", "Md5FailureException", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class S3 {
    public static final String DYNAMIC_PATH;
    public static final S3 INSTANCE = new S3();
    private static final String S3_BASE;
    public static final String STATIC_PATH;
    private static final String TAG = Log.tag(S3.class);
    private static final OkHttpClient okHttpClient;

    /* renamed from: getAndVerifyObject$lambda-4$lambda-3 */
    public static final String m2608getAndVerifyObject$lambda4$lambda3(String str) {
        return "";
    }

    private S3() {
    }

    static {
        INSTANCE = new S3();
        TAG = Log.tag(S3.class);
        OkHttpClient signalOkHttpClient = ApplicationDependencies.getSignalOkHttpClient();
        Intrinsics.checkNotNullExpressionValue(signalOkHttpClient, "getSignalOkHttpClient()");
        okHttpClient = signalOkHttpClient;
    }

    @JvmStatic
    public static final String getString(String str) throws IOException {
        String string;
        Intrinsics.checkNotNullParameter(str, "endpoint");
        Response object = getObject(str);
        try {
            if (object.isSuccessful()) {
                ResponseBody body = object.body();
                if (!(body == null || (string = body.string()) == null)) {
                    Intrinsics.checkNotNullExpressionValue(string, "string()");
                    String obj = StringsKt__StringsKt.trim(string).toString();
                    if (obj != null) {
                        th = null;
                        return obj;
                    }
                }
                throw new IOException();
            }
            throw new NonSuccessfulResponseCodeException(object.code());
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }

    public final long getLong(String str) throws IOException {
        Intrinsics.checkNotNullParameter(str, "endpoint");
        Long l = StringsKt__StringNumberConversionsKt.toLongOrNull(getString(str));
        if (l != null) {
            return l.longValue();
        }
        Log.w(TAG, "Failed to retrieve long value from S3");
        throw new IOException("Unable to parse");
    }

    @JvmStatic
    public static final Response getObject(String str) throws IOException {
        Intrinsics.checkNotNullParameter(str, "endpoint");
        Request.Builder builder = new Request.Builder().get();
        Response execute = okHttpClient.newCall(builder.url(S3_BASE + str).build()).execute();
        Intrinsics.checkNotNullExpressionValue(execute, "okHttpClient.newCall(request).execute()");
        return execute;
    }

    public static /* synthetic */ ServiceResponse getAndVerifyObject$default(S3 s3, String str, Class cls, byte[] bArr, int i, Object obj) {
        if ((i & 4) != 0) {
            bArr = s3.getObjectMD5(str);
        }
        return s3.getAndVerifyObject(str, cls, bArr);
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Throwable] */
    public final <T> ServiceResponse<T> getAndVerifyObject(String str, Class<T> cls, byte[] bArr) {
        Intrinsics.checkNotNullParameter(str, "endpoint");
        Intrinsics.checkNotNullParameter(cls, "clazz");
        if (bArr == null) {
            Log.w(TAG, "Failed to download s3 object MD5.");
            ServiceResponse<T> forExecutionError = ServiceResponse.forExecutionError(new Md5FailureException());
            Intrinsics.checkNotNullExpressionValue(forExecutionError, "forExecutionError(Md5FailureException())");
            return forExecutionError;
        }
        try {
            Sink object = getObject(str);
            th = 0;
            if (!object.isSuccessful()) {
                ServiceResponse<T> forApplicationError = ServiceResponse.forApplicationError(DefaultErrorMapper.getDefault().parseError(object.code()), object.code(), "");
                Intrinsics.checkNotNullExpressionValue(forApplicationError, "forApplicationError(\n   …           \"\"\n          )");
                return forApplicationError;
            }
            ResponseBody body = object.body();
            BufferedSource source = body != null ? body.source() : th;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            object = Okio.sink(byteArrayOutputStream);
            try {
                HashingSink md5 = HashingSink.Companion.md5(object);
                if (source != null) {
                    source.readAll(md5);
                }
                byte[] byteArray = md5.hash().toByteArray();
                CloseableKt.closeFinally(object, th);
                if (!MessageDigest.isEqual(bArr, byteArray)) {
                    Log.w(TAG, "Content mismatch when downloading s3 object. Deleting.");
                    ServiceResponse<T> forExecutionError2 = ServiceResponse.forExecutionError(new Md5FailureException());
                    Intrinsics.checkNotNullExpressionValue(forExecutionError2, "forExecutionError(Md5FailureException())");
                    return forExecutionError2;
                }
                ResponseMapper build = DefaultResponseMapper.extend(cls).withResponseMapper(new DefaultResponseMapper.CustomResponseMapper(cls) { // from class: org.thoughtcrime.securesms.s3.S3$$ExternalSyntheticLambda0
                    public final /* synthetic */ Class f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // org.whispersystems.signalservice.internal.websocket.DefaultResponseMapper.CustomResponseMapper
                    public final ServiceResponse map(int i, String str2, Function function, boolean z) {
                        return S3.m2607getAndVerifyObject$lambda4$lambda2(this.f$0, i, str2, function, z);
                    }
                }).build();
                byte[] byteArray2 = byteArrayOutputStream.toByteArray();
                Intrinsics.checkNotNullExpressionValue(byteArray2, "outputStream.toByteArray()");
                Charset forName = Charset.forName("UTF-8");
                Intrinsics.checkNotNullExpressionValue(forName, "forName(\"UTF-8\")");
                ServiceResponse<T> map = build.map(200, new String(byteArray2, forName), new Function() { // from class: org.thoughtcrime.securesms.s3.S3$$ExternalSyntheticLambda1
                    @Override // j$.util.function.Function
                    public /* synthetic */ Function andThen(Function function) {
                        return Function.CC.$default$andThen(this, function);
                    }

                    @Override // j$.util.function.Function
                    public final Object apply(Object obj) {
                        return S3.m2608getAndVerifyObject$lambda4$lambda3((String) obj);
                    }

                    @Override // j$.util.function.Function
                    public /* synthetic */ Function compose(Function function) {
                        return Function.CC.$default$compose(this, function);
                    }
                }, false);
                Intrinsics.checkNotNullExpressionValue(map, "extend(clazz)\n          …\"UTF-8\")), { \"\" }, false)");
                return map;
            } finally {
                try {
                    throw th;
                } finally {
                }
            }
        } catch (IOException e) {
            Log.w(TAG, "Unable to get and verify", e);
            ServiceResponse<T> forUnknownError = ServiceResponse.forUnknownError(e);
            Intrinsics.checkNotNullExpressionValue(forUnknownError, "forUnknownError(e)");
            return forUnknownError;
        }
    }

    /* renamed from: getAndVerifyObject$lambda-4$lambda-2 */
    public static final ServiceResponse m2607getAndVerifyObject$lambda4$lambda2(Class cls, int i, String str, Function function, boolean z) {
        Intrinsics.checkNotNullParameter(cls, "$clazz");
        return ServiceResponse.forResult(JsonUtils.fromJson(str, cls), i, str);
    }

    public static /* synthetic */ boolean verifyAndWriteToDisk$default(S3 s3, Context context, String str, File file, boolean z, int i, Object obj) {
        if ((i & 8) != 0) {
            z = false;
        }
        return s3.verifyAndWriteToDisk(context, str, file, z);
    }

    public final boolean verifyAndWriteToDisk(Context context, String str, File file, boolean z) {
        OutputStream outputStream;
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(str, "objectPathOnNetwork");
        Intrinsics.checkNotNullParameter(file, "objectFileOnDisk");
        byte[] objectMD5 = getObjectMD5(str);
        if (objectMD5 == null) {
            Log.w(TAG, "Failed to download s3 object MD5.");
            return false;
        }
        try {
            if (file.exists()) {
                file.delete();
            }
            Response object = getObject(str);
            ResponseBody body = object.body();
            BufferedSource source = body != null ? body.source() : null;
            if (z) {
                outputStream = new FileOutputStream(file);
            } else {
                outputStream = EncryptedStreamUtils.INSTANCE.getOutputStream(context, file);
            }
            Sink sink = Okio.sink(outputStream);
            HashingSink md5 = HashingSink.Companion.md5(sink);
            if (source != null) {
                source.readAll(md5);
            }
            byte[] byteArray = md5.hash().toByteArray();
            CloseableKt.closeFinally(sink, null);
            if (!Arrays.equals(objectMD5, byteArray)) {
                Log.w(TAG, "Content mismatch when downloading s3 object. Deleting.");
                file.delete();
                CloseableKt.closeFinally(object, null);
                return false;
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(object, null);
            return true;
        } catch (Exception e) {
            Log.w(TAG, "Failed to download s3 object", e);
            return false;
        }
    }

    public final byte[] getObjectMD5(String str) {
        Intrinsics.checkNotNullParameter(str, "endpoint");
        Request.Builder head = new Request.Builder().head();
        try {
            Response execute = okHttpClient.newCall(head.url(S3_BASE + str).build()).execute();
            if (!execute.isSuccessful()) {
                CloseableKt.closeFinally(execute, null);
                return null;
            }
            S3 s3 = INSTANCE;
            Intrinsics.checkNotNullExpressionValue(execute, "response");
            String mD5FromResponse = s3.getMD5FromResponse(execute);
            byte[] fromStringCondensed = mD5FromResponse != null ? Hex.fromStringCondensed(mD5FromResponse) : null;
            CloseableKt.closeFinally(execute, null);
            return fromStringCondensed;
        } catch (IOException e) {
            Log.w(TAG, "Could not retrieve md5", e);
            return null;
        }
    }

    private final String getMD5FromResponse(Response response) {
        Pattern compile = Pattern.compile(".*([a-f0-9]{32}).*");
        Intrinsics.checkNotNullExpressionValue(compile, "compile(\".*([a-f0-9]{32}).*\")");
        String header = response.header("etag");
        if (header == null) {
            return null;
        }
        Matcher matcher = compile.matcher(header);
        Intrinsics.checkNotNullExpressionValue(matcher, "pattern.matcher(header)");
        if (matcher.find()) {
            return matcher.group(1);
        }
        return null;
    }

    /* compiled from: S3.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/s3/S3$Md5FailureException;", "Ljava/io/IOException;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Md5FailureException extends IOException {
        public Md5FailureException() {
            super("Failed to getting or comparing MD5");
        }
    }
}
