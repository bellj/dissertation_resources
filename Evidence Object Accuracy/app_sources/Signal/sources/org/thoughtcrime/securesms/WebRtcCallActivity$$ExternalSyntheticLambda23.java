package org.thoughtcrime.securesms;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class WebRtcCallActivity$$ExternalSyntheticLambda23 implements Runnable {
    public final /* synthetic */ WebRtcCallActivity f$0;

    public /* synthetic */ WebRtcCallActivity$$ExternalSyntheticLambda23(WebRtcCallActivity webRtcCallActivity) {
        this.f$0 = webRtcCallActivity;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.handleDenyCall();
    }
}
