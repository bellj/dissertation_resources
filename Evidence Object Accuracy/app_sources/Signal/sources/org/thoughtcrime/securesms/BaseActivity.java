package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.AppStartup;
import org.thoughtcrime.securesms.util.ConfigurationUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.dynamiclanguage.DynamicLanguageContextWrapper;

/* loaded from: classes.dex */
public abstract class BaseActivity extends AppCompatActivity {
    private static final String TAG = Log.tag(BaseActivity.class);

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        AppStartup.getInstance().onCriticalRenderEventStart();
        logEvent("onCreate()");
        super.onCreate(bundle);
        AppStartup.getInstance().onCriticalRenderEventEnd();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        initializeScreenshotSecurity();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        logEvent("onStart()");
        ApplicationDependencies.getShakeToReport().registerActivity(this);
        super.onStart();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        logEvent("onStop()");
        super.onStop();
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        logEvent("onDestroy()");
        super.onDestroy();
    }

    private void initializeScreenshotSecurity() {
        if (TextSecurePreferences.isScreenSecurityEnabled(this)) {
            getWindow().addFlags(8192);
        } else {
            getWindow().clearFlags(8192);
        }
    }

    protected void startActivitySceneTransition(Intent intent, View view, String str) {
        ContextCompat.startActivity(this, intent, ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, str).toBundle());
    }

    @Override // androidx.appcompat.app.AppCompatActivity, android.app.Activity, android.view.ContextThemeWrapper, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        int i;
        super.attachBaseContext(context);
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        if (getDelegate().getLocalNightMode() != -100) {
            i = getDelegate().getLocalNightMode();
        } else {
            i = AppCompatDelegate.getDefaultNightMode();
        }
        configuration.uiMode = mapNightModeToConfigurationUiMode(context, i) | (configuration.uiMode & -49);
        configuration.orientation = 0;
        applyOverrideConfiguration(configuration);
    }

    @Override // android.view.ContextThemeWrapper
    public void applyOverrideConfiguration(Configuration configuration) {
        DynamicLanguageContextWrapper.prepareOverrideConfiguration(this, configuration);
        super.applyOverrideConfiguration(configuration);
    }

    private void logEvent(String str) {
        String str2 = TAG;
        Log.d(str2, "[" + Log.tag(getClass()) + "] " + str);
    }

    public final ActionBar requireSupportActionBar() {
        ActionBar supportActionBar = getSupportActionBar();
        Objects.requireNonNull(supportActionBar);
        return supportActionBar;
    }

    private static int mapNightModeToConfigurationUiMode(Context context, int i) {
        if (i == 2) {
            return 32;
        }
        if (i == 1) {
            return 16;
        }
        return ConfigurationUtil.getNightModeConfiguration(context.getApplicationContext());
    }
}
