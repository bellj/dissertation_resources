package org.thoughtcrime.securesms.safety.review;

import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetState;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetViewModel;
import org.thoughtcrime.securesms.safety.SafetyNumberBucket;
import org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem;
import org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: SafetyNumberReviewConnectionsFragment.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00182\u00020\u0001:\u0002\u0018\u0019B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/safety/review/SafetyNumberReviewConnectionsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getActionItemsForBucket", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "bucket", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState;", "Companion", "Dialog", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberReviewConnectionsFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SafetyNumberBottomSheetViewModel.class), new Function0<ViewModelStore>(new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$viewModel$2
        final /* synthetic */ SafetyNumberReviewConnectionsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStoreOwner invoke() {
            Fragment requireParentFragment = this.this$0.requireParentFragment().requireParentFragment();
            Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment().requireParentFragment()");
            return requireParentFragment;
        }
    }) { // from class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public SafetyNumberReviewConnectionsFragment() {
        super(R.string.SafetyNumberReviewConnectionsFragment__safety_number_changes, 0, R.layout.safety_number_review_fragment, null, 10, null);
    }

    public final SafetyNumberBottomSheetViewModel getViewModel() {
        return (SafetyNumberBottomSheetViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        SafetyNumberBucketRowItem.INSTANCE.register(dSLSettingsAdapter);
        SafetyNumberRecipientRowItem.INSTANCE.register(dSLSettingsAdapter);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        requireView().findViewById(R.id.done).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SafetyNumberReviewConnectionsFragment.m2646bindAdapter$lambda0(SafetyNumberReviewConnectionsFragment.this, view);
            }
        });
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ SafetyNumberReviewConnectionsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SafetyNumberReviewConnectionsFragment.m2647bindAdapter$lambda1(DSLSettingsAdapter.this, this.f$1, (SafetyNumberBottomSheetState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.subscrib…MappingModelList())\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m2646bindAdapter$lambda0(SafetyNumberReviewConnectionsFragment safetyNumberReviewConnectionsFragment, View view) {
        Intrinsics.checkNotNullParameter(safetyNumberReviewConnectionsFragment, "this$0");
        safetyNumberReviewConnectionsFragment.requireActivity().onBackPressed();
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m2647bindAdapter$lambda1(DSLSettingsAdapter dSLSettingsAdapter, SafetyNumberReviewConnectionsFragment safetyNumberReviewConnectionsFragment, SafetyNumberBottomSheetState safetyNumberBottomSheetState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(safetyNumberReviewConnectionsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(safetyNumberBottomSheetState, "state");
        dSLSettingsAdapter.submitList(safetyNumberReviewConnectionsFragment.getConfiguration(safetyNumberBottomSheetState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(SafetyNumberBottomSheetState safetyNumberBottomSheetState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(safetyNumberBottomSheetState, this) { // from class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1
            final /* synthetic */ SafetyNumberBottomSheetState $state;
            final /* synthetic */ SafetyNumberReviewConnectionsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0091: INVOKE  
                  (r14v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<?> : 0x008d: INVOKE  (r4v4 org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<?> A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem : 0x0086: SGET  (r5v4 org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem A[REMOVE]) =  org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem.INSTANCE org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem)
                  (wrap: org.thoughtcrime.securesms.safety.SafetyNumberBucket : 0x007a: INVOKE  (r4v2 org.thoughtcrime.securesms.safety.SafetyNumberBucket A[REMOVE]) = 
                  (r2v6 'entry' java.util.Map$Entry<org.thoughtcrime.securesms.safety.SafetyNumberBucket, java.util.List<org.thoughtcrime.securesms.safety.SafetyNumberRecipient>>)
                 type: INTERFACE call: java.util.Map.Entry.getKey():java.lang.Object)
                  (wrap: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1 : 0x008a: CONSTRUCTOR  (r7v4 org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r1v2 'safetyNumberReviewConnectionsFragment' org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment)
                 call: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1.<init>(java.lang.Object):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem.createModel(org.thoughtcrime.securesms.safety.SafetyNumberBucket, kotlin.jvm.functions.Function1):org.thoughtcrime.securesms.util.adapter.mapping.MappingModel)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x008d: INVOKE  (r4v4 org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<?> A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem : 0x0086: SGET  (r5v4 org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem A[REMOVE]) =  org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem.INSTANCE org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem)
                  (wrap: org.thoughtcrime.securesms.safety.SafetyNumberBucket : 0x007a: INVOKE  (r4v2 org.thoughtcrime.securesms.safety.SafetyNumberBucket A[REMOVE]) = 
                  (r2v6 'entry' java.util.Map$Entry<org.thoughtcrime.securesms.safety.SafetyNumberBucket, java.util.List<org.thoughtcrime.securesms.safety.SafetyNumberRecipient>>)
                 type: INTERFACE call: java.util.Map.Entry.getKey():java.lang.Object)
                  (wrap: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1 : 0x008a: CONSTRUCTOR  (r7v4 org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r1v2 'safetyNumberReviewConnectionsFragment' org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment)
                 call: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1.<init>(java.lang.Object):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem.createModel(org.thoughtcrime.securesms.safety.SafetyNumberBucket, kotlin.jvm.functions.Function1):org.thoughtcrime.securesms.util.adapter.mapping.MappingModel in method: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 16 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x008a: CONSTRUCTOR  (r7v4 org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r1v2 'safetyNumberReviewConnectionsFragment' org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment)
                 call: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1.<init>(java.lang.Object):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 22 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 28 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r14) {
                /*
                    r13 = this;
                    java.lang.String r0 = "$this$configure"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r14, r0)
                    org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetState r0 = r13.$state
                    java.util.Map r0 = r0.getDestinationToRecipientMap()
                    java.util.Collection r0 = r0.values()
                    java.util.List r0 = kotlin.collections.CollectionsKt.flatten(r0)
                    int r0 = r0.size()
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r1 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment r2 = r13.this$0
                    android.content.res.Resources r2 = r2.getResources()
                    r3 = 1
                    java.lang.Object[] r4 = new java.lang.Object[r3]
                    java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
                    r6 = 0
                    r4[r6] = r5
                    r5 = 2131820640(0x7f110060, float:1.9274E38)
                    java.lang.String r0 = r2.getQuantityString(r5, r0, r4)
                    java.lang.String r2 = "resources.getQuantityStr…entCount, recipientCount)"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r2)
                    r2 = 2
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r4 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r2]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$TextAppearanceModifier r5 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText$TextAppearanceModifier
                    r7 = 2132017644(0x7f1401ec, float:1.9673572E38)
                    r5.<init>(r7)
                    r4[r6] = r5
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$ColorModifier r5 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText$ColorModifier
                    org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment r7 = r13.this$0
                    android.content.Context r7 = r7.requireContext()
                    r8 = 2131100761(0x7f060459, float:1.7813913E38)
                    int r7 = androidx.core.content.ContextCompat.getColor(r7, r8)
                    r5.<init>(r7)
                    r4[r3] = r5
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r0 = r1.from(r0, r4)
                    r1 = 0
                    org.thoughtcrime.securesms.components.settings.DSLConfiguration.textPref$default(r14, r0, r1, r2, r1)
                    org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetState r0 = r13.$state
                    java.util.Map r0 = r0.getDestinationToRecipientMap()
                    org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment r1 = r13.this$0
                    java.util.Set r0 = r0.entrySet()
                    java.util.Iterator r0 = r0.iterator()
                L_0x006e:
                    boolean r2 = r0.hasNext()
                    if (r2 == 0) goto L_0x00ce
                    java.lang.Object r2 = r0.next()
                    java.util.Map$Entry r2 = (java.util.Map.Entry) r2
                    java.lang.Object r4 = r2.getKey()
                    org.thoughtcrime.securesms.safety.SafetyNumberBucket r4 = (org.thoughtcrime.securesms.safety.SafetyNumberBucket) r4
                    java.lang.Object r2 = r2.getValue()
                    java.util.List r2 = (java.util.List) r2
                    org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem r5 = org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem.INSTANCE
                    org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1 r7 = new org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$1
                    r7.<init>(r1)
                    org.thoughtcrime.securesms.util.adapter.mapping.MappingModel r4 = r5.createModel(r4, r7)
                    r14.customPref(r4)
                    java.util.Iterator r2 = r2.iterator()
                L_0x0098:
                    boolean r4 = r2.hasNext()
                    if (r4 == 0) goto L_0x006e
                    java.lang.Object r4 = r2.next()
                    org.thoughtcrime.securesms.safety.SafetyNumberRecipient r4 = (org.thoughtcrime.securesms.safety.SafetyNumberRecipient) r4
                    org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem$Model r5 = new org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem$Model
                    org.thoughtcrime.securesms.recipients.Recipient r8 = r4.getRecipient()
                    org.thoughtcrime.securesms.database.model.IdentityRecord r7 = r4.getIdentityRecord()
                    org.thoughtcrime.securesms.database.IdentityDatabase$VerifiedStatus r7 = r7.getVerifiedStatus()
                    org.thoughtcrime.securesms.database.IdentityDatabase$VerifiedStatus r9 = org.thoughtcrime.securesms.database.IdentityDatabase.VerifiedStatus.VERIFIED
                    if (r7 != r9) goto L_0x00b8
                    r9 = 1
                    goto L_0x00b9
                L_0x00b8:
                    r9 = 0
                L_0x00b9:
                    int r10 = r4.getDistributionListMembershipCount()
                    int r11 = r4.getGroupMembershipCount()
                    org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$2$1 r12 = new org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1$1$2$1
                    r12.<init>(r1)
                    r7 = r5
                    r7.<init>(r8, r9, r10, r11, r12)
                    r14.customPref(r5)
                    goto L_0x0098
                L_0x00ce:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final List<ActionItem> getActionItemsForBucket(SafetyNumberBucket safetyNumberBucket) {
        if (!(safetyNumberBucket instanceof SafetyNumberBucket.DistributionListBucket)) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        String string = getString(R.string.SafetyNumberReviewConnectionsFragment__remove_all);
        Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.Safet…ionsFragment__remove_all)");
        return CollectionsKt__CollectionsJVMKt.listOf(new ActionItem(R.drawable.ic_circle_x_24, string, R.color.signal_colorOnSurface, new Runnable(safetyNumberBucket) { // from class: org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ SafetyNumberBucket f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SafetyNumberReviewConnectionsFragment.m2648getActionItemsForBucket$lambda2(SafetyNumberReviewConnectionsFragment.this, this.f$1);
            }
        }));
    }

    /* renamed from: getActionItemsForBucket$lambda-2 */
    public static final void m2648getActionItemsForBucket$lambda2(SafetyNumberReviewConnectionsFragment safetyNumberReviewConnectionsFragment, SafetyNumberBucket safetyNumberBucket) {
        Intrinsics.checkNotNullParameter(safetyNumberReviewConnectionsFragment, "this$0");
        Intrinsics.checkNotNullParameter(safetyNumberBucket, "$bucket");
        safetyNumberReviewConnectionsFragment.getViewModel().removeAll((SafetyNumberBucket.DistributionListBucket) safetyNumberBucket);
    }

    /* compiled from: SafetyNumberReviewConnectionsFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/safety/review/SafetyNumberReviewConnectionsFragment$Dialog;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment;", "()V", "getWrappedFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Dialog extends WrapperDialogFragment {
        @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment
        public Fragment getWrappedFragment() {
            return new SafetyNumberReviewConnectionsFragment();
        }
    }

    /* compiled from: SafetyNumberReviewConnectionsFragment.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/safety/review/SafetyNumberReviewConnectionsFragment$Companion;", "", "()V", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            new Dialog().show(fragmentManager, (String) null);
        }
    }
}
