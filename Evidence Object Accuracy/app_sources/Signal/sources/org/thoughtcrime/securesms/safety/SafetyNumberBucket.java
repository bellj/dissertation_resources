package org.thoughtcrime.securesms.safety;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: SafetyNumberBucket.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "", "()V", "ContactsBucket", "DistributionListBucket", "GroupBucket", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$DistributionListBucket;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$GroupBucket;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$ContactsBucket;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class SafetyNumberBucket {
    public /* synthetic */ SafetyNumberBucket(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: SafetyNumberBucket.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$DistributionListBucket;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "name", "", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;Ljava/lang/String;)V", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "getName", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DistributionListBucket extends SafetyNumberBucket {
        private final DistributionListId distributionListId;
        private final String name;

        public static /* synthetic */ DistributionListBucket copy$default(DistributionListBucket distributionListBucket, DistributionListId distributionListId, String str, int i, Object obj) {
            if ((i & 1) != 0) {
                distributionListId = distributionListBucket.distributionListId;
            }
            if ((i & 2) != 0) {
                str = distributionListBucket.name;
            }
            return distributionListBucket.copy(distributionListId, str);
        }

        public final DistributionListId component1() {
            return this.distributionListId;
        }

        public final String component2() {
            return this.name;
        }

        public final DistributionListBucket copy(DistributionListId distributionListId, String str) {
            Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
            Intrinsics.checkNotNullParameter(str, "name");
            return new DistributionListBucket(distributionListId, str);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DistributionListBucket)) {
                return false;
            }
            DistributionListBucket distributionListBucket = (DistributionListBucket) obj;
            return Intrinsics.areEqual(this.distributionListId, distributionListBucket.distributionListId) && Intrinsics.areEqual(this.name, distributionListBucket.name);
        }

        public int hashCode() {
            return (this.distributionListId.hashCode() * 31) + this.name.hashCode();
        }

        public String toString() {
            return "DistributionListBucket(distributionListId=" + this.distributionListId + ", name=" + this.name + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DistributionListBucket(DistributionListId distributionListId, String str) {
            super(null);
            Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
            Intrinsics.checkNotNullParameter(str, "name");
            this.distributionListId = distributionListId;
            this.name = str;
        }

        public final DistributionListId getDistributionListId() {
            return this.distributionListId;
        }

        public final String getName() {
            return this.name;
        }
    }

    private SafetyNumberBucket() {
    }

    /* compiled from: SafetyNumberBucket.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$GroupBucket;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupBucket extends SafetyNumberBucket {
        private final Recipient recipient;

        public static /* synthetic */ GroupBucket copy$default(GroupBucket groupBucket, Recipient recipient, int i, Object obj) {
            if ((i & 1) != 0) {
                recipient = groupBucket.recipient;
            }
            return groupBucket.copy(recipient);
        }

        public final Recipient component1() {
            return this.recipient;
        }

        public final GroupBucket copy(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            return new GroupBucket(recipient);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof GroupBucket) && Intrinsics.areEqual(this.recipient, ((GroupBucket) obj).recipient);
        }

        public int hashCode() {
            return this.recipient.hashCode();
        }

        public String toString() {
            return "GroupBucket(recipient=" + this.recipient + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupBucket(Recipient recipient) {
            super(null);
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.recipient = recipient;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }
    }

    /* compiled from: SafetyNumberBucket.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$ContactsBucket;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ContactsBucket extends SafetyNumberBucket {
        public static final ContactsBucket INSTANCE = new ContactsBucket();

        private ContactsBucket() {
            super(null);
        }
    }
}
