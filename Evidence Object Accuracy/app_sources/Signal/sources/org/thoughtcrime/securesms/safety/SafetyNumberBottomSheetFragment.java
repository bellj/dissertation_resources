package org.thoughtcrime.securesms.safety;

import android.os.Bundle;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.button.MaterialButton;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.LazyThreadSafetyMode;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.models.SplashImage;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository;
import org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetViewModel;
import org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem;
import org.thoughtcrime.securesms.safety.review.SafetyNumberReviewConnectionsFragment;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: SafetyNumberBottomSheetFragment.kt */
@Metadata(d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 #2\u00020\u00012\u00020\u0002:\u0001#B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020!H\u0016J\b\u0010\"\u001a\u00020\u0018H\u0016R\u001b\u0010\u0004\u001a\u00020\u00058CX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\rXD¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0012\u001a\u00020\u00138BX\u0002¢\u0006\f\n\u0004\b\u0016\u0010\t\u001a\u0004\b\u0014\u0010\u0015¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment$WrapperDialogFragmentCallback;", "()V", MultiselectForwardFragment.ARGS, "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "getArgs", "()Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "args$delegate", "Lkotlin/Lazy;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "sendAnyway", "Lcom/google/android/material/button/MaterialButton;", "viewModel", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState;", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onWrapperDialogFragmentDismissed", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetFragment extends DSLSettingsBottomSheetFragment implements WrapperDialogFragment.WrapperDialogFragmentCallback {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(SafetyNumberBottomSheetFragment.class);
    private final Lazy args$delegate = LazyKt__LazyJVMKt.lazy(LazyThreadSafetyMode.NONE, new Function0<SafetyNumberBottomSheetArgs>(this) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$args$2
        final /* synthetic */ SafetyNumberBottomSheetFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final SafetyNumberBottomSheetArgs invoke() {
            SafetyNumberBottomSheet safetyNumberBottomSheet = SafetyNumberBottomSheet.INSTANCE;
            Bundle requireArguments = this.this$0.requireArguments();
            Intrinsics.checkNotNullExpressionValue(requireArguments, "requireArguments()");
            return safetyNumberBottomSheet.getArgsFromBundle(requireArguments);
        }
    });
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final float peekHeightPercentage = 1.0f;
    private MaterialButton sendAnyway;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SafetyNumberBottomSheetViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$viewModel$2
        final /* synthetic */ SafetyNumberBottomSheetFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new SafetyNumberBottomSheetViewModel.Factory(this.this$0.getArgs(), new SafetyNumberChangeRepository(this.this$0.requireContext()));
        }
    });

    /* compiled from: SafetyNumberBottomSheetFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[TrustAndVerifyResult.Result.values().length];
            iArr[TrustAndVerifyResult.Result.TRUST_AND_VERIFY.ordinal()] = 1;
            iArr[TrustAndVerifyResult.Result.TRUST_VERIFY_AND_RESEND.ordinal()] = 2;
            iArr[TrustAndVerifyResult.Result.UNKNOWN.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback
    public void onWrapperDialogFragmentDismissed() {
    }

    public SafetyNumberBottomSheetFragment() {
        super(R.layout.safety_number_bottom_sheet, null, 0.0f, 6, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment, org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    public final SafetyNumberBottomSheetArgs getArgs() {
        return (SafetyNumberBottomSheetArgs) this.args$delegate.getValue();
    }

    public final SafetyNumberBottomSheetViewModel getViewModel() {
        return (SafetyNumberBottomSheetViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        View findViewById = requireView().findViewById(R.id.review_connections);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewBy…(R.id.review_connections)");
        View findViewById2 = requireView().findViewById(R.id.send_anyway);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "requireView().findViewById(R.id.send_anyway)");
        this.sendAnyway = (MaterialButton) findViewById2;
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SafetyNumberBottomSheetFragment.m2611bindAdapter$lambda0(SafetyNumberBottomSheetFragment.this, view);
            }
        });
        MaterialButton materialButton = this.sendAnyway;
        if (materialButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("sendAnyway");
            materialButton = null;
        }
        materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SafetyNumberBottomSheetFragment.m2612bindAdapter$lambda2(SafetyNumberBottomSheetFragment.this, view);
            }
        });
        SplashImage.INSTANCE.register(dSLSettingsAdapter);
        SafetyNumberRecipientRowItem.INSTANCE.register(dSLSettingsAdapter);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().subscribe(new Consumer(findViewById, this, dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ View f$0;
            public final /* synthetic */ SafetyNumberBottomSheetFragment f$1;
            public final /* synthetic */ DSLSettingsAdapter f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SafetyNumberBottomSheetFragment.m2614bindAdapter$lambda3(this.f$0, this.f$1, this.f$2, (SafetyNumberBottomSheetState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.subscrib…MappingModelList())\n    }");
        lifecycleDisposable2.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m2611bindAdapter$lambda0(SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, View view) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        safetyNumberBottomSheetFragment.getViewModel().setDone();
        SafetyNumberReviewConnectionsFragment.Companion companion = SafetyNumberReviewConnectionsFragment.Companion;
        FragmentManager childFragmentManager = safetyNumberBottomSheetFragment.getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        companion.show(childFragmentManager);
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m2612bindAdapter$lambda2(SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, View view) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        MaterialButton materialButton = safetyNumberBottomSheetFragment.sendAnyway;
        if (materialButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("sendAnyway");
            materialButton = null;
        }
        materialButton.setEnabled(false);
        LifecycleDisposable lifecycleDisposable = safetyNumberBottomSheetFragment.lifecycleDisposable;
        Disposable subscribe = safetyNumberBottomSheetFragment.getViewModel().trustAndVerify().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SafetyNumberBottomSheetFragment.m2613bindAdapter$lambda2$lambda1(SafetyNumberBottomSheetFragment.this, (TrustAndVerifyResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.trustAndVerify…lowingStateLoss()\n      }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:36:0x0041 */
    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:37:0x0066 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v5, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r3v8, types: [org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks] */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v13, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r3v16, types: [org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks] */
    /* JADX WARN: Type inference failed for: r3v17 */
    /* JADX WARN: Type inference failed for: r3v21 */
    /* JADX WARN: Type inference failed for: r3v22 */
    /* JADX WARN: Type inference failed for: r3v23 */
    /* JADX WARN: Type inference failed for: r3v24 */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* renamed from: bindAdapter$lambda-2$lambda-1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m2613bindAdapter$lambda2$lambda1(org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment r2, org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult r3) {
        /*
            java.lang.String r0 = "this$0"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult$Result r3 = r3.getResult()
            int[] r0 = org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment.WhenMappings.$EnumSwitchMapping$0
            int r3 = r3.ordinal()
            r3 = r0[r3]
            r0 = 1
            r1 = 0
            if (r3 == r0) goto L_0x0049
            r0 = 2
            if (r3 == r0) goto L_0x0024
            r0 = 3
            if (r3 == r0) goto L_0x001c
            goto L_0x0075
        L_0x001c:
            java.lang.String r3 = org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment.TAG
            java.lang.String r0 = "Unknown Result"
            org.signal.core.util.logging.Log.w(r3, r0)
            goto L_0x0075
        L_0x0024:
            androidx.fragment.app.Fragment r3 = r2.getParentFragment()
        L_0x0028:
            if (r3 == 0) goto L_0x0034
            boolean r0 = r3 instanceof org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
            if (r0 == 0) goto L_0x002f
            goto L_0x0041
        L_0x002f:
            androidx.fragment.app.Fragment r3 = r3.getParentFragment()
            goto L_0x0028
        L_0x0034:
            androidx.fragment.app.FragmentActivity r3 = r2.requireActivity()
            boolean r0 = r3 instanceof org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
            if (r0 != 0) goto L_0x003d
            goto L_0x003e
        L_0x003d:
            r1 = r3
        L_0x003e:
            r3 = r1
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks r3 = (org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks) r3
        L_0x0041:
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks r3 = (org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks) r3
            if (r3 == 0) goto L_0x0075
            r3.onMessageResentAfterSafetyNumberChangeInBottomSheet()
            goto L_0x0075
        L_0x0049:
            androidx.fragment.app.Fragment r3 = r2.getParentFragment()
        L_0x004d:
            if (r3 == 0) goto L_0x0059
            boolean r0 = r3 instanceof org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
            if (r0 == 0) goto L_0x0054
            goto L_0x0066
        L_0x0054:
            androidx.fragment.app.Fragment r3 = r3.getParentFragment()
            goto L_0x004d
        L_0x0059:
            androidx.fragment.app.FragmentActivity r3 = r2.requireActivity()
            boolean r0 = r3 instanceof org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
            if (r0 != 0) goto L_0x0062
            goto L_0x0063
        L_0x0062:
            r1 = r3
        L_0x0063:
            r3 = r1
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks r3 = (org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks) r3
        L_0x0066:
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks r3 = (org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks) r3
            if (r3 == 0) goto L_0x0075
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetViewModel r0 = r2.getViewModel()
            java.util.List r0 = r0.getDestinationSnapshot()
            r3.sendAnywayAfterSafetyNumberChangedInBottomSheet(r0)
        L_0x0075:
            r2.dismissAllowingStateLoss()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment.m2613bindAdapter$lambda2$lambda1(org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment, org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult):void");
    }

    /* renamed from: bindAdapter$lambda-3 */
    public static final void m2614bindAdapter$lambda3(View view, SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, DSLSettingsAdapter dSLSettingsAdapter, SafetyNumberBottomSheetState safetyNumberBottomSheetState) {
        Intrinsics.checkNotNullParameter(view, "$reviewConnections");
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        ViewExtensionsKt.setVisible(view, safetyNumberBottomSheetState.getHasLargeNumberOfUntrustedRecipients());
        if (safetyNumberBottomSheetState.isCheckupComplete()) {
            MaterialButton materialButton = safetyNumberBottomSheetFragment.sendAnyway;
            if (materialButton == null) {
                Intrinsics.throwUninitializedPropertyAccessException("sendAnyway");
                materialButton = null;
            }
            materialButton.setText(R.string.conversation_activity__send);
        }
        Intrinsics.checkNotNullExpressionValue(safetyNumberBottomSheetState, "state");
        dSLSettingsAdapter.submitList(safetyNumberBottomSheetFragment.getConfiguration(safetyNumberBottomSheetState).toMappingModelList());
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:20:0x0036 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r3v5, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r3v8, types: [org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks] */
    /* JADX WARN: Type inference failed for: r3v9 */
    /* JADX WARN: Type inference failed for: r3v14 */
    /* JADX WARN: Type inference failed for: r3v15 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDismiss(android.content.DialogInterface r3) {
        /*
            r2 = this;
            java.lang.String r0 = "dialog"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
            super.onDismiss(r3)
            com.google.android.material.button.MaterialButton r3 = r2.sendAnyway
            r0 = 0
            if (r3 != 0) goto L_0x0013
            java.lang.String r3 = "sendAnyway"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r3)
            r3 = r0
        L_0x0013:
            boolean r3 = r3.isEnabled()
            if (r3 == 0) goto L_0x003d
            androidx.fragment.app.Fragment r3 = r2.getParentFragment()
        L_0x001d:
            if (r3 == 0) goto L_0x0029
            boolean r1 = r3 instanceof org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
            if (r1 == 0) goto L_0x0024
            goto L_0x0036
        L_0x0024:
            androidx.fragment.app.Fragment r3 = r3.getParentFragment()
            goto L_0x001d
        L_0x0029:
            androidx.fragment.app.FragmentActivity r3 = r2.requireActivity()
            boolean r1 = r3 instanceof org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
            if (r1 != 0) goto L_0x0032
            goto L_0x0033
        L_0x0032:
            r0 = r3
        L_0x0033:
            r3 = r0
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks r3 = (org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks) r3
        L_0x0036:
            org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$Callbacks r3 = (org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks) r3
            if (r3 == 0) goto L_0x003d
            r3.onCanceled()
        L_0x003d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment.onDismiss(android.content.DialogInterface):void");
    }

    private final DSLConfiguration getConfiguration(SafetyNumberBottomSheetState safetyNumberBottomSheetState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(safetyNumberBottomSheetState, this) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$getConfiguration$1
            final /* synthetic */ SafetyNumberBottomSheetState $state;
            final /* synthetic */ SafetyNumberBottomSheetFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                int i;
                String str;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new SplashImage.Model(R.drawable.ic_safety_number_24, R.color.signal_colorOnSurface));
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                if (!this.$state.isCheckupComplete() || !this.$state.getHasLargeNumberOfUntrustedRecipients()) {
                    i = this.$state.getHasLargeNumberOfUntrustedRecipients() ? R.string.SafetyNumberBottomSheetFragment__safety_number_checkup : R.string.SafetyNumberBottomSheetFragment__safety_number_changes;
                } else {
                    i = R.string.SafetyNumberBottomSheetFragment__safety_number_checkup_complete;
                }
                DSLSettingsText.CenterModifier centerModifier = DSLSettingsText.CenterModifier.INSTANCE;
                DSLConfiguration.textPref$default(dSLConfiguration, companion.from(i, new DSLSettingsText.TextAppearanceModifier(R.style.Signal_Text_TitleLarge), centerModifier), null, 2, null);
                if (this.$state.isCheckupComplete() && this.$state.getHasLargeNumberOfUntrustedRecipients()) {
                    str = this.this$0.getString(R.string.SafetyNumberBottomSheetFragment__all_connections_have_been_reviewed);
                } else if (this.$state.getHasLargeNumberOfUntrustedRecipients()) {
                    SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment = this.this$0;
                    str = safetyNumberBottomSheetFragment.getString(R.string.SafetyNumberBottomSheetFragment__you_have_d_connections, Integer.valueOf(safetyNumberBottomSheetFragment.getArgs().getUntrustedRecipients().size()));
                } else {
                    str = this.this$0.getString(R.string.SafetyNumberBottomSheetFragment__the_following_people);
                }
                Intrinsics.checkNotNullExpressionValue(str, "when {\n            state…owing_people)\n          }");
                DSLConfiguration.textPref$default(dSLConfiguration, companion.from(str, new DSLSettingsText.TextAppearanceModifier(R.style.Signal_Text_BodyLarge), centerModifier), null, 2, null);
                if (this.$state.isEmpty()) {
                    DimensionUnit dimensionUnit = DimensionUnit.DP;
                    dSLConfiguration.space((int) dimensionUnit.toPixels(48.0f));
                    dSLConfiguration.noPadTextPref(companion.from(R.string.SafetyNumberBottomSheetFragment__no_more_recipients_to_show, new DSLSettingsText.TextAppearanceModifier(R.style.Signal_Text_BodyLarge), centerModifier, new DSLSettingsText.ColorModifier(ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_colorOnSurfaceVariant))));
                    dSLConfiguration.space((int) dimensionUnit.toPixels(48.0f));
                }
                if (!this.$state.getHasLargeNumberOfUntrustedRecipients()) {
                    List<SafetyNumberRecipient> list = CollectionsKt___CollectionsKt.distinct(CollectionsKt__IterablesKt.flatten(this.$state.getDestinationToRecipientMap().values()));
                    SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment2 = this.this$0;
                    for (SafetyNumberRecipient safetyNumberRecipient : list) {
                        dSLConfiguration.customPref(new SafetyNumberRecipientRowItem.Model(safetyNumberRecipient.getRecipient(), safetyNumberRecipient.getIdentityRecord().getVerifiedStatus() == IdentityDatabase.VerifiedStatus.VERIFIED, safetyNumberRecipient.getDistributionListMembershipCount(), safetyNumberRecipient.getGroupMembershipCount(), new SafetyNumberBottomSheetFragment$getConfiguration$1$1$1(safetyNumberBottomSheetFragment2)));
                    }
                }
            }
        });
    }

    /* compiled from: SafetyNumberBottomSheetFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetFragment$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
