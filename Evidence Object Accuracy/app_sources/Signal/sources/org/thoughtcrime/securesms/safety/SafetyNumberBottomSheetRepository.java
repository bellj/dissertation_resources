package org.thoughtcrime.securesms.safety;

import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.MaybeSource;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Function3;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Map;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.DistributionListDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBucket;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: SafetyNumberBottomSheetRepository.kt */
@Metadata(d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010%\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001-B\u0005¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005H\u0002J\"\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005H\u0002J:\u0010\n\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u00050\f0\u000b2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u00052\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005J\"\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u00050\u00122\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005H\u0002J$\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u00052\u0006\u0010\u0015\u001a\u00020\u00062\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00130\u0005H\u0002J$\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u0006\u0010\u0015\u001a\u00020\u00062\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u0002J\"\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00122\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005H\u0002J\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u001c0\u001b2\u0006\u0010\u001d\u001a\u00020\u0010J\"\u0010\u001e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u001f0\u00050\u00122\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\u0005H\u0002J2\u0010 \u001a\u00020!2\u0018\u0010\"\u001a\u0014\u0012\u0004\u0012\u00020\r\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u00050#2\u0006\u0010$\u001a\u00020\r2\u0006\u0010%\u001a\u00020\u000eH\u0002J\u0016\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00130\u00122\u0006\u0010\u0015\u001a\u00020\u0006H\u0002J\u001c\u0010'\u001a\u00020(2\f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00100\u00052\u0006\u0010*\u001a\u00020+J\u001c\u0010,\u001a\u00020(2\u0006\u0010\u001d\u001a\u00020\u00102\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0005¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetRepository;", "", "()V", "filterForDistributionLists", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "destinations", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "filterForGroups", "getBuckets", "Lio/reactivex/rxjava3/core/Flowable;", "", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipient;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getDistributionLists", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/database/model/DistributionListRecord;", "getDistributionMemberships", RecipientDatabase.TABLE_NAME, "distributionLists", "getGroupMemberships", ChooseGroupStoryBottomSheet.RESULT_SET, "getGroups", "getIdentityRecord", "Lio/reactivex/rxjava3/core/Maybe;", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "recipientId", "getResolvedIdentities", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetRepository$ResolvedIdentity;", "insert", "", "map", "", "bucket", "safetyNumberRecipient", "observeDistributionList", "removeAllFromStory", "Lio/reactivex/rxjava3/core/Completable;", "recipientIds", "distributionList", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "removeFromStories", "ResolvedIdentity", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetRepository {
    public final Maybe<IdentityRecord> getIdentityRecord(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Maybe<IdentityRecord> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda6
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return SafetyNumberBottomSheetRepository.$r8$lambda$VhB8s94Vxj0N09Az2VolQwyxi3c(RecipientId.this);
            }
        }).flatMapMaybe(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.m2621$r8$lambda$OPehUWeN0zmZ9npIhpNwQeFJo((Optional) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      App…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getIdentityRecord$lambda-0 */
    public static final Optional m2630getIdentityRecord$lambda0(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        return ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipientId);
    }

    /* renamed from: getIdentityRecord$lambda-1 */
    public static final MaybeSource m2631getIdentityRecord$lambda1(Optional optional) {
        if (optional.isPresent()) {
            return Maybe.just(optional.get());
        }
        return Maybe.empty();
    }

    public final Flowable<Map<SafetyNumberBucket, List<SafetyNumberRecipient>>> getBuckets(List<? extends RecipientId> list, List<? extends ContactSearchKey.RecipientSearchKey> list2) {
        Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        Intrinsics.checkNotNullParameter(list2, "destinations");
        Observable<List<ResolvedIdentity>> resolvedIdentities = getResolvedIdentities(list);
        Observable<List<DistributionListRecord>> distributionLists = getDistributionLists(list2);
        Observable<List<Recipient>> groups = getGroups(list2);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10));
        for (ContactSearchKey.RecipientSearchKey recipientSearchKey : list2) {
            arrayList.add(recipientSearchKey.getRecipientId());
        }
        Flowable<Map<SafetyNumberBucket, List<SafetyNumberRecipient>>> subscribeOn = Observable.combineLatest(resolvedIdentities, distributionLists, groups, new Function3(CollectionsKt___CollectionsKt.toSet(arrayList)) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function3
            public final Object apply(Object obj, Object obj2, Object obj3) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$EmnwYfV7AMqpq0ie6CorLo69MhE(SafetyNumberBottomSheetRepository.this, this.f$1, (List) obj, (List) obj2, (List) obj3);
            }
        }).toFlowable(BackpressureStrategy.LATEST).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "combineLatest(recipientO…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getBuckets$lambda-6 */
    public static final Map m2625getBuckets$lambda6(SafetyNumberBottomSheetRepository safetyNumberBottomSheetRepository, Set set, List list, List list2, List list3) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetRepository, "this$0");
        Intrinsics.checkNotNullParameter(set, "$destinationRecipientIds");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        Intrinsics.checkNotNullExpressionValue(list, "recipientList");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ResolvedIdentity resolvedIdentity = (ResolvedIdentity) it.next();
            Recipient recipient = resolvedIdentity.getRecipient();
            Intrinsics.checkNotNullExpressionValue(list2, "distributionLists");
            List<DistributionListRecord> distributionMemberships = safetyNumberBottomSheetRepository.getDistributionMemberships(recipient, list2);
            Recipient recipient2 = resolvedIdentity.getRecipient();
            Intrinsics.checkNotNullExpressionValue(list3, ChooseGroupStoryBottomSheet.RESULT_SET);
            List<Recipient> groupMemberships = safetyNumberBottomSheetRepository.getGroupMemberships(recipient2, list3);
            boolean contains = set.contains(resolvedIdentity.getRecipient().getId());
            Recipient recipient3 = resolvedIdentity.getRecipient();
            IdentityRecord identityRecord = resolvedIdentity.getIdentityRecord().get();
            Intrinsics.checkNotNullExpressionValue(identityRecord, "it.identityRecord.get()");
            SafetyNumberRecipient safetyNumberRecipient = new SafetyNumberRecipient(recipient3, identityRecord, distributionMemberships.size(), groupMemberships.size());
            for (DistributionListRecord distributionListRecord : distributionMemberships) {
                safetyNumberBottomSheetRepository.insert(linkedHashMap, new SafetyNumberBucket.DistributionListBucket(distributionListRecord.getId(), distributionListRecord.getName()), safetyNumberRecipient);
            }
            for (Recipient recipient4 : groupMemberships) {
                safetyNumberBottomSheetRepository.insert(linkedHashMap, new SafetyNumberBucket.GroupBucket(recipient4), safetyNumberRecipient);
            }
            if (contains) {
                safetyNumberBottomSheetRepository.insert(linkedHashMap, SafetyNumberBucket.ContactsBucket.INSTANCE, safetyNumberRecipient);
            }
        }
        return MapsKt__MapsKt.toMap(linkedHashMap);
    }

    public final Completable removeFromStories(RecipientId recipientId, List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(list, "destinations");
        Completable subscribeOn = filterForDistributionLists(list).flatMapCompletable(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$gONkgzWiM3nw8qd8CYPlX7VTjYE(RecipientId.this, (List) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "filterForDistributionLis…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: removeFromStories$lambda-11 */
    public static final CompletableSource m2637removeFromStories$lambda11(RecipientId recipientId, List list) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        return Completable.fromAction(new Action(list, recipientId) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda13
            public final /* synthetic */ List f$0;
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                SafetyNumberBottomSheetRepository.$r8$lambda$hwtebf7bR9FmUrEwuaK9YtLic50(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: removeFromStories$lambda-11$lambda-10 */
    public static final void m2638removeFromStories$lambda11$lambda10(List list, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullExpressionValue(list, "distributionRecipients");
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
            DistributionListId requireDistributionListId = ((Recipient) it.next()).requireDistributionListId();
            Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "it.requireDistributionListId()");
            DistributionListRecord list2 = distributionLists.getList(requireDistributionListId);
            if (list2 != null) {
                arrayList.add(list2);
            }
        }
        ArrayList<DistributionListRecord> arrayList2 = new ArrayList();
        for (Object obj : arrayList) {
            if (((DistributionListRecord) obj).getMembers().contains(recipientId)) {
                arrayList2.add(obj);
            }
        }
        for (DistributionListRecord distributionListRecord : arrayList2) {
            SignalDatabase.Companion.distributionLists().excludeFromStory(recipientId, distributionListRecord);
            Stories.INSTANCE.onStorySettingsChanged(distributionListRecord.getId());
        }
    }

    public final Completable removeAllFromStory(List<? extends RecipientId> list, DistributionListId distributionListId) {
        Intrinsics.checkNotNullParameter(list, "recipientIds");
        Intrinsics.checkNotNullParameter(distributionListId, "distributionList");
        Completable subscribeOn = Completable.fromAction(new Action(list) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                SafetyNumberBottomSheetRepository.$r8$lambda$jPhNfbPjYpIAGzylMR7Vh3Js39Q(DistributionListId.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      val r…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: removeAllFromStory$lambda-12 */
    public static final void m2636removeAllFromStory$lambda12(DistributionListId distributionListId, List list) {
        Intrinsics.checkNotNullParameter(distributionListId, "$distributionList");
        Intrinsics.checkNotNullParameter(list, "$recipientIds");
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        DistributionListRecord list2 = companion.distributionLists().getList(distributionListId);
        if (list2 != null) {
            companion.distributionLists().excludeAllFromStory(list, list2);
            Stories.INSTANCE.onStorySettingsChanged(distributionListId);
        }
    }

    private final void insert(Map<SafetyNumberBucket, List<SafetyNumberRecipient>> map, SafetyNumberBucket safetyNumberBucket, SafetyNumberRecipient safetyNumberRecipient) {
        map.put(safetyNumberBucket, CollectionsKt___CollectionsKt.plus((Collection<? extends SafetyNumberRecipient>) ((Collection<? extends Object>) ((List) Map.EL.getOrDefault(map, safetyNumberBucket, CollectionsKt__CollectionsKt.emptyList()))), safetyNumberRecipient));
    }

    private final Single<List<Recipient>> filterForDistributionLists(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Single<List<Recipient>> fromCallable = Single.fromCallable(new Callable(list) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return SafetyNumberBottomSheetRepository.$r8$lambda$OaibRSB4M0jujBu9CpFTeYRujO4(this.f$0);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n      val…sDistributionList }\n    }");
        return fromCallable;
    }

    private final Single<List<Recipient>> filterForGroups(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Single<List<Recipient>> fromCallable = Single.fromCallable(new Callable(list) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return SafetyNumberBottomSheetRepository.m2622$r8$lambda$xLK97IhfflcCynCM83BUSaeWug(this.f$0);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n      val…lter { it.isGroup }\n    }");
        return fromCallable;
    }

    /* JADX DEBUG: Type inference failed for r2v3. Raw type applied. Possible types: io.reactivex.rxjava3.core.Observable<R>, java.lang.Object, io.reactivex.rxjava3.core.Observable<org.thoughtcrime.securesms.database.model.DistributionListRecord> */
    private final Observable<DistributionListRecord> observeDistributionList(Recipient recipient) {
        Observable map = Recipient.observable(recipient.getId()).map(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$sOJ8S0axKPlNBSeWs1WyDdKo1DU((Recipient) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "observable(recipient.id)…DistributionListId())!! }");
        return map;
    }

    /* renamed from: observeDistributionList$lambda-19 */
    public static final DistributionListRecord m2635observeDistributionList$lambda19(Recipient recipient) {
        DistributionListDatabase distributionLists = SignalDatabase.Companion.distributionLists();
        DistributionListId requireDistributionListId = recipient.requireDistributionListId();
        Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "it.requireDistributionListId()");
        DistributionListRecord list = distributionLists.getList(requireDistributionListId);
        Intrinsics.checkNotNull(list);
        return list;
    }

    /* JADX DEBUG: Type inference failed for r2v2. Raw type applied. Possible types: io.reactivex.rxjava3.core.Observable<R>, java.lang.Object, io.reactivex.rxjava3.core.Observable<java.util.List<org.thoughtcrime.securesms.database.model.DistributionListRecord>> */
    private final Observable<List<DistributionListRecord>> getDistributionLists(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Observable flatMapObservable = filterForDistributionLists(list).flatMapObservable(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$ctrEGhjuWxC_FwKaYgbqr_YAyvg(SafetyNumberBottomSheetRepository.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapObservable, "distributionListRecipien…)\n        }\n      }\n    }");
        return flatMapObservable;
    }

    /* renamed from: getDistributionLists$lambda-22 */
    public static final ObservableSource m2626getDistributionLists$lambda22(SafetyNumberBottomSheetRepository safetyNumberBottomSheetRepository, List list) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetRepository, "this$0");
        if (list.isEmpty()) {
            return Observable.just(CollectionsKt__CollectionsKt.emptyList());
        }
        Intrinsics.checkNotNullExpressionValue(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(safetyNumberBottomSheetRepository.observeDistributionList((Recipient) it.next()));
        }
        return Observable.combineLatest(arrayList, new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda14
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$16d6iCJLLPytVRcmJg7yjk_WhFk((Object[]) obj);
            }
        });
    }

    /* renamed from: getDistributionLists$lambda-22$lambda-21 */
    public static final List m2627getDistributionLists$lambda22$lambda21(Object[] objArr) {
        Intrinsics.checkNotNullExpressionValue(objArr, "it");
        ArrayList arrayList = new ArrayList();
        for (Object obj : objArr) {
            if (obj instanceof DistributionListRecord) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Type inference failed for r2v2. Raw type applied. Possible types: io.reactivex.rxjava3.core.Observable<R>, java.lang.Object, io.reactivex.rxjava3.core.Observable<java.util.List<org.thoughtcrime.securesms.recipients.Recipient>> */
    private final Observable<List<Recipient>> getGroups(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Observable flatMapObservable = filterForGroups(list).flatMapObservable(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$FZlc5h9VRTIhZo6ErW6V1ewwc_M((List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapObservable, "groupRecipients.flatMapO…)\n        }\n      }\n    }");
        return flatMapObservable;
    }

    /* renamed from: getGroups$lambda-25 */
    public static final ObservableSource m2628getGroups$lambda25(List list) {
        if (list.isEmpty()) {
            return Observable.just(CollectionsKt__CollectionsKt.emptyList());
        }
        Intrinsics.checkNotNullExpressionValue(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(Recipient.observable(((Recipient) it.next()).getId()));
        }
        return Observable.combineLatest(arrayList, new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda15
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$00eAB5Nroj5V_dA5IGu86EVipB4((Object[]) obj);
            }
        });
    }

    /* renamed from: getGroups$lambda-25$lambda-24 */
    public static final List m2629getGroups$lambda25$lambda24(Object[] objArr) {
        Intrinsics.checkNotNullExpressionValue(objArr, "it");
        ArrayList arrayList = new ArrayList();
        for (Object obj : objArr) {
            if (obj instanceof Recipient) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* renamed from: getResolvedIdentities$lambda-28$lambda-27 */
    public static final ObservableSource m2632getResolvedIdentities$lambda28$lambda27(Recipient recipient) {
        return Observable.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda4
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return SafetyNumberBottomSheetRepository.$r8$lambda$a_rMAUr8ezJPyxc_lDdWhBQ4I9E(Recipient.this);
            }
        });
    }

    /* renamed from: getResolvedIdentities$lambda-28$lambda-27$lambda-26 */
    public static final ResolvedIdentity m2633getResolvedIdentities$lambda28$lambda27$lambda26(Recipient recipient) {
        Optional<IdentityRecord> identityRecord = ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipient.getId());
        Intrinsics.checkNotNullExpressionValue(identityRecord, "getProtocolStore().aci()…ntityRecord(recipient.id)");
        Intrinsics.checkNotNullExpressionValue(recipient, RecipientDatabase.TABLE_NAME);
        return new ResolvedIdentity(recipient, identityRecord);
    }

    /* renamed from: getResolvedIdentities$lambda-30 */
    public static final List m2634getResolvedIdentities$lambda30(Object[] objArr) {
        Intrinsics.checkNotNullExpressionValue(objArr, "identities");
        ArrayList arrayList = new ArrayList();
        for (Object obj : objArr) {
            if (obj instanceof ResolvedIdentity) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList();
        for (Object obj2 : arrayList) {
            if (((ResolvedIdentity) obj2).getIdentityRecord().isPresent()) {
                arrayList2.add(obj2);
            }
        }
        return arrayList2;
    }

    /* compiled from: SafetyNumberBottomSheetRepository.kt */
    @Metadata(bv = {}, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0007\u001a\u00020\u0002\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\u0004\b\u0017\u0010\u0018J\t\u0010\u0003\u001a\u00020\u0002HÆ\u0003J\u000f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004HÆ\u0003J#\u0010\t\u001a\u00020\u00002\b\b\u0002\u0010\u0007\u001a\u00020\u00022\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004HÆ\u0001J\t\u0010\u000b\u001a\u00020\nHÖ\u0001J\t\u0010\r\u001a\u00020\fHÖ\u0001J\u0013\u0010\u0010\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u0017\u0010\u0007\u001a\u00020\u00028\u0006¢\u0006\f\n\u0004\b\u0007\u0010\u0011\u001a\u0004\b\u0012\u0010\u0013R\u001d\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006¢\u0006\f\n\u0004\b\b\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetRepository$ResolvedIdentity;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "j$/util/Optional", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "component2", RecipientDatabase.TABLE_NAME, "identityRecord", "copy", "", "toString", "", "hashCode", "other", "", "equals", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "Lj$/util/Optional;", "getIdentityRecord", "()Lj$/util/Optional;", "<init>", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lj$/util/Optional;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes4.dex */
    public static final class ResolvedIdentity {
        private final Optional<IdentityRecord> identityRecord;
        private final Recipient recipient;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$ResolvedIdentity */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ ResolvedIdentity copy$default(ResolvedIdentity resolvedIdentity, Recipient recipient, Optional optional, int i, Object obj) {
            if ((i & 1) != 0) {
                recipient = resolvedIdentity.recipient;
            }
            if ((i & 2) != 0) {
                optional = resolvedIdentity.identityRecord;
            }
            return resolvedIdentity.copy(recipient, optional);
        }

        public final Recipient component1() {
            return this.recipient;
        }

        public final Optional<IdentityRecord> component2() {
            return this.identityRecord;
        }

        public final ResolvedIdentity copy(Recipient recipient, Optional<IdentityRecord> optional) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(optional, "identityRecord");
            return new ResolvedIdentity(recipient, optional);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ResolvedIdentity)) {
                return false;
            }
            ResolvedIdentity resolvedIdentity = (ResolvedIdentity) obj;
            return Intrinsics.areEqual(this.recipient, resolvedIdentity.recipient) && Intrinsics.areEqual(this.identityRecord, resolvedIdentity.identityRecord);
        }

        public int hashCode() {
            return (this.recipient.hashCode() * 31) + this.identityRecord.hashCode();
        }

        public String toString() {
            return "ResolvedIdentity(recipient=" + this.recipient + ", identityRecord=" + this.identityRecord + ')';
        }

        public ResolvedIdentity(Recipient recipient, Optional<IdentityRecord> optional) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(optional, "identityRecord");
            this.recipient = recipient;
            this.identityRecord = optional;
        }

        public final Optional<IdentityRecord> getIdentityRecord() {
            return this.identityRecord;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }
    }

    private final List<DistributionListRecord> getDistributionMemberships(Recipient recipient, List<DistributionListRecord> list) {
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (((DistributionListRecord) obj).getMembers().contains(recipient.getId())) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    private final List<Recipient> getGroupMemberships(Recipient recipient, List<? extends Recipient> list) {
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (((Recipient) obj).getParticipantIds().contains(recipient.getId())) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* renamed from: filterForDistributionLists$lambda-15 */
    public static final List m2623filterForDistributionLists$lambda15(List list) {
        Intrinsics.checkNotNullParameter(list, "$destinations");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((ContactSearchKey.RecipientSearchKey) it.next()).getRecipientId());
        }
        List<Recipient> resolvedList = Recipient.resolvedList(arrayList);
        Intrinsics.checkNotNullExpressionValue(resolvedList, "resolvedList(destinations.map { it.recipientId })");
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : resolvedList) {
            if (((Recipient) obj).isDistributionList()) {
                arrayList2.add(obj);
            }
        }
        return arrayList2;
    }

    /* renamed from: filterForGroups$lambda-18 */
    public static final List m2624filterForGroups$lambda18(List list) {
        Intrinsics.checkNotNullParameter(list, "$destinations");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((ContactSearchKey.RecipientSearchKey) it.next()).getRecipientId());
        }
        List<Recipient> resolvedList = Recipient.resolvedList(arrayList);
        Intrinsics.checkNotNullExpressionValue(resolvedList, "resolvedList(destinations.map { it.recipientId })");
        ArrayList arrayList2 = new ArrayList();
        for (Object obj : resolvedList) {
            if (((Recipient) obj).isGroup()) {
                arrayList2.add(obj);
            }
        }
        return arrayList2;
    }

    private final Observable<List<ResolvedIdentity>> getResolvedIdentities(List<? extends RecipientId> list) {
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (RecipientId recipientId : list) {
            arrayList.add(Recipient.observable(recipientId).switchMap(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda11
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return SafetyNumberBottomSheetRepository.m2620$r8$lambda$HcsEDvnVkJkiiQ9s0s0pXsCZSk((Recipient) obj);
                }
            }));
        }
        Observable<List<ResolvedIdentity>> combineLatest = Observable.combineLatest(arrayList, new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetRepository$$ExternalSyntheticLambda12
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetRepository.$r8$lambda$0vcqsg2o5hFnqgyvcWHA0e6KxVc((Object[]) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(recipientO…yRecord.isPresent }\n    }");
        return combineLatest;
    }
}
