package org.thoughtcrime.securesms.safety;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.IdentityDatabase;

/* compiled from: SafetyNumberBottomSheetState.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\"B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u001a\b\u0002\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0007\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0005HÆ\u0003J\u001b\u0010\u0018\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0007HÆ\u0003J\t\u0010\u0019\u001a\u00020\fHÆ\u0003JC\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u001a\b\u0002\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u00072\b\b\u0002\u0010\u000b\u001a\u00020\fHÆ\u0001J\u0013\u0010\u001b\u001a\u00020\u00052\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001d\u001a\u00020\u0003HÖ\u0001J\u0006\u0010\u001e\u001a\u00020\u0005J\u0006\u0010\u001f\u001a\u00020\u0005J\t\u0010 \u001a\u00020!HÖ\u0001R#\u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState;", "", "untrustedRecipientCount", "", "hasLargeNumberOfUntrustedRecipients", "", "destinationToRecipientMap", "", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "", "Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipient;", "loadState", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState$LoadState;", "(IZLjava/util/Map;Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState$LoadState;)V", "getDestinationToRecipientMap", "()Ljava/util/Map;", "getHasLargeNumberOfUntrustedRecipients", "()Z", "getLoadState", "()Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState$LoadState;", "getUntrustedRecipientCount", "()I", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "isCheckupComplete", "isEmpty", "toString", "", "LoadState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetState {
    private final Map<SafetyNumberBucket, List<SafetyNumberRecipient>> destinationToRecipientMap;
    private final boolean hasLargeNumberOfUntrustedRecipients;
    private final LoadState loadState;
    private final int untrustedRecipientCount;

    /* compiled from: SafetyNumberBottomSheetState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState$LoadState;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "DONE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum LoadState {
        INIT,
        READY,
        DONE
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SafetyNumberBottomSheetState copy$default(SafetyNumberBottomSheetState safetyNumberBottomSheetState, int i, boolean z, Map map, LoadState loadState, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = safetyNumberBottomSheetState.untrustedRecipientCount;
        }
        if ((i2 & 2) != 0) {
            z = safetyNumberBottomSheetState.hasLargeNumberOfUntrustedRecipients;
        }
        if ((i2 & 4) != 0) {
            map = safetyNumberBottomSheetState.destinationToRecipientMap;
        }
        if ((i2 & 8) != 0) {
            loadState = safetyNumberBottomSheetState.loadState;
        }
        return safetyNumberBottomSheetState.copy(i, z, map, loadState);
    }

    public final int component1() {
        return this.untrustedRecipientCount;
    }

    public final boolean component2() {
        return this.hasLargeNumberOfUntrustedRecipients;
    }

    public final Map<SafetyNumberBucket, List<SafetyNumberRecipient>> component3() {
        return this.destinationToRecipientMap;
    }

    public final LoadState component4() {
        return this.loadState;
    }

    public final SafetyNumberBottomSheetState copy(int i, boolean z, Map<SafetyNumberBucket, ? extends List<SafetyNumberRecipient>> map, LoadState loadState) {
        Intrinsics.checkNotNullParameter(map, "destinationToRecipientMap");
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        return new SafetyNumberBottomSheetState(i, z, map, loadState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SafetyNumberBottomSheetState)) {
            return false;
        }
        SafetyNumberBottomSheetState safetyNumberBottomSheetState = (SafetyNumberBottomSheetState) obj;
        return this.untrustedRecipientCount == safetyNumberBottomSheetState.untrustedRecipientCount && this.hasLargeNumberOfUntrustedRecipients == safetyNumberBottomSheetState.hasLargeNumberOfUntrustedRecipients && Intrinsics.areEqual(this.destinationToRecipientMap, safetyNumberBottomSheetState.destinationToRecipientMap) && this.loadState == safetyNumberBottomSheetState.loadState;
    }

    public int hashCode() {
        int i = this.untrustedRecipientCount * 31;
        boolean z = this.hasLargeNumberOfUntrustedRecipients;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        return ((((i + i2) * 31) + this.destinationToRecipientMap.hashCode()) * 31) + this.loadState.hashCode();
    }

    public String toString() {
        return "SafetyNumberBottomSheetState(untrustedRecipientCount=" + this.untrustedRecipientCount + ", hasLargeNumberOfUntrustedRecipients=" + this.hasLargeNumberOfUntrustedRecipients + ", destinationToRecipientMap=" + this.destinationToRecipientMap + ", loadState=" + this.loadState + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Map<org.thoughtcrime.securesms.safety.SafetyNumberBucket, ? extends java.util.List<org.thoughtcrime.securesms.safety.SafetyNumberRecipient>> */
    /* JADX WARN: Multi-variable type inference failed */
    public SafetyNumberBottomSheetState(int i, boolean z, Map<SafetyNumberBucket, ? extends List<SafetyNumberRecipient>> map, LoadState loadState) {
        Intrinsics.checkNotNullParameter(map, "destinationToRecipientMap");
        Intrinsics.checkNotNullParameter(loadState, "loadState");
        this.untrustedRecipientCount = i;
        this.hasLargeNumberOfUntrustedRecipients = z;
        this.destinationToRecipientMap = map;
        this.loadState = loadState;
    }

    public final int getUntrustedRecipientCount() {
        return this.untrustedRecipientCount;
    }

    public final boolean getHasLargeNumberOfUntrustedRecipients() {
        return this.hasLargeNumberOfUntrustedRecipients;
    }

    public /* synthetic */ SafetyNumberBottomSheetState(int i, boolean z, Map map, LoadState loadState, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, z, (i2 & 4) != 0 ? MapsKt__MapsKt.emptyMap() : map, (i2 & 8) != 0 ? LoadState.INIT : loadState);
    }

    public final Map<SafetyNumberBucket, List<SafetyNumberRecipient>> getDestinationToRecipientMap() {
        return this.destinationToRecipientMap;
    }

    public final LoadState getLoadState() {
        return this.loadState;
    }

    public final boolean isEmpty() {
        return !this.hasLargeNumberOfUntrustedRecipients && CollectionsKt__IterablesKt.flatten(this.destinationToRecipientMap.values()).isEmpty() && this.loadState == LoadState.READY;
    }

    public final boolean isCheckupComplete() {
        boolean z;
        boolean z2;
        if (this.loadState != LoadState.DONE && !isEmpty()) {
            List<SafetyNumberRecipient> list = CollectionsKt__IterablesKt.flatten(this.destinationToRecipientMap.values());
            if (!(list instanceof Collection) || !list.isEmpty()) {
                for (SafetyNumberRecipient safetyNumberRecipient : list) {
                    if (safetyNumberRecipient.getIdentityRecord().getVerifiedStatus() == IdentityDatabase.VerifiedStatus.VERIFIED) {
                        z2 = true;
                        continue;
                    } else {
                        z2 = false;
                        continue;
                    }
                    if (!z2) {
                        z = false;
                        break;
                    }
                }
            }
            z = true;
            if (!z) {
                return false;
            }
        }
        return true;
    }
}
