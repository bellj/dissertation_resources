package org.thoughtcrime.securesms.safety;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetState;

/* compiled from: SafetyNumberBottomSheetViewModel.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetViewModel$setDone$1 extends Lambda implements Function1<SafetyNumberBottomSheetState, SafetyNumberBottomSheetState> {
    public static final SafetyNumberBottomSheetViewModel$setDone$1 INSTANCE = new SafetyNumberBottomSheetViewModel$setDone$1();

    SafetyNumberBottomSheetViewModel$setDone$1() {
        super(1);
    }

    public final SafetyNumberBottomSheetState invoke(SafetyNumberBottomSheetState safetyNumberBottomSheetState) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetState, "it");
        return SafetyNumberBottomSheetState.copy$default(safetyNumberBottomSheetState, 0, false, null, SafetyNumberBottomSheetState.LoadState.DONE, 7, null);
    }
}
