package org.thoughtcrime.securesms.safety;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: SafetyNumberBottomSheetArgs.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B-\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tJ\u000f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\bHÆ\u0003J5\u0010\u0012\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\bHÆ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\t\u0010\u0019\u001a\u00020\u0014HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0014HÖ\u0001R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000b¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "Landroid/os/Parcelable;", "untrustedRecipients", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "destinations", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "(Ljava/util/List;Ljava/util/List;Lorg/thoughtcrime/securesms/database/model/MessageId;)V", "getDestinations", "()Ljava/util/List;", "getMessageId", "()Lorg/thoughtcrime/securesms/database/model/MessageId;", "getUntrustedRecipients", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetArgs implements Parcelable {
    public static final Parcelable.Creator<SafetyNumberBottomSheetArgs> CREATOR = new Creator();
    private final List<ContactSearchKey.ParcelableRecipientSearchKey> destinations;
    private final MessageId messageId;
    private final List<RecipientId> untrustedRecipients;

    /* compiled from: SafetyNumberBottomSheetArgs.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Creator implements Parcelable.Creator<SafetyNumberBottomSheetArgs> {
        @Override // android.os.Parcelable.Creator
        public final SafetyNumberBottomSheetArgs createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            for (int i = 0; i != readInt; i++) {
                arrayList.add(parcel.readParcelable(SafetyNumberBottomSheetArgs.class.getClassLoader()));
            }
            int readInt2 = parcel.readInt();
            ArrayList arrayList2 = new ArrayList(readInt2);
            for (int i2 = 0; i2 != readInt2; i2++) {
                arrayList2.add(ContactSearchKey.ParcelableRecipientSearchKey.CREATOR.createFromParcel(parcel));
            }
            return new SafetyNumberBottomSheetArgs(arrayList, arrayList2, parcel.readInt() == 0 ? null : MessageId.CREATOR.createFromParcel(parcel));
        }

        @Override // android.os.Parcelable.Creator
        public final SafetyNumberBottomSheetArgs[] newArray(int i) {
            return new SafetyNumberBottomSheetArgs[i];
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetArgs */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SafetyNumberBottomSheetArgs copy$default(SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs, List list, List list2, MessageId messageId, int i, Object obj) {
        if ((i & 1) != 0) {
            list = safetyNumberBottomSheetArgs.untrustedRecipients;
        }
        if ((i & 2) != 0) {
            list2 = safetyNumberBottomSheetArgs.destinations;
        }
        if ((i & 4) != 0) {
            messageId = safetyNumberBottomSheetArgs.messageId;
        }
        return safetyNumberBottomSheetArgs.copy(list, list2, messageId);
    }

    public final List<RecipientId> component1() {
        return this.untrustedRecipients;
    }

    public final List<ContactSearchKey.ParcelableRecipientSearchKey> component2() {
        return this.destinations;
    }

    public final MessageId component3() {
        return this.messageId;
    }

    public final SafetyNumberBottomSheetArgs copy(List<? extends RecipientId> list, List<ContactSearchKey.ParcelableRecipientSearchKey> list2, MessageId messageId) {
        Intrinsics.checkNotNullParameter(list, "untrustedRecipients");
        Intrinsics.checkNotNullParameter(list2, "destinations");
        return new SafetyNumberBottomSheetArgs(list, list2, messageId);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SafetyNumberBottomSheetArgs)) {
            return false;
        }
        SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs = (SafetyNumberBottomSheetArgs) obj;
        return Intrinsics.areEqual(this.untrustedRecipients, safetyNumberBottomSheetArgs.untrustedRecipients) && Intrinsics.areEqual(this.destinations, safetyNumberBottomSheetArgs.destinations) && Intrinsics.areEqual(this.messageId, safetyNumberBottomSheetArgs.messageId);
    }

    @Override // java.lang.Object
    public int hashCode() {
        int hashCode = ((this.untrustedRecipients.hashCode() * 31) + this.destinations.hashCode()) * 31;
        MessageId messageId = this.messageId;
        return hashCode + (messageId == null ? 0 : messageId.hashCode());
    }

    @Override // java.lang.Object
    public String toString() {
        return "SafetyNumberBottomSheetArgs(untrustedRecipients=" + this.untrustedRecipients + ", destinations=" + this.destinations + ", messageId=" + this.messageId + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        List<RecipientId> list = this.untrustedRecipients;
        parcel.writeInt(list.size());
        for (RecipientId recipientId : list) {
            parcel.writeParcelable(recipientId, i);
        }
        List<ContactSearchKey.ParcelableRecipientSearchKey> list2 = this.destinations;
        parcel.writeInt(list2.size());
        for (ContactSearchKey.ParcelableRecipientSearchKey parcelableRecipientSearchKey : list2) {
            parcelableRecipientSearchKey.writeToParcel(parcel, i);
        }
        MessageId messageId = this.messageId;
        if (messageId == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        messageId.writeToParcel(parcel, i);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public SafetyNumberBottomSheetArgs(List<? extends RecipientId> list, List<ContactSearchKey.ParcelableRecipientSearchKey> list2, MessageId messageId) {
        Intrinsics.checkNotNullParameter(list, "untrustedRecipients");
        Intrinsics.checkNotNullParameter(list2, "destinations");
        this.untrustedRecipients = list;
        this.destinations = list2;
        this.messageId = messageId;
    }

    public /* synthetic */ SafetyNumberBottomSheetArgs(List list, List list2, MessageId messageId, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(list, list2, (i & 4) != 0 ? null : messageId);
    }

    public final List<RecipientId> getUntrustedRecipients() {
        return this.untrustedRecipients;
    }

    public final List<ContactSearchKey.ParcelableRecipientSearchKey> getDestinations() {
        return this.destinations;
    }

    public final MessageId getMessageId() {
        return this.messageId;
    }
}
