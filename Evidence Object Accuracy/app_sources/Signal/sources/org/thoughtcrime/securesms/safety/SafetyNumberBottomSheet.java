package org.thoughtcrime.securesms.safety;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog;
import org.thoughtcrime.securesms.database.documents.IdentityKeyMismatch;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.util.Preconditions;

/* compiled from: SafetyNumberBottomSheet.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u001e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003\"#$B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007J\u0016\u0010\n\u001a\u00020\u00072\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\t0\fH\u0007J\u0016\u0010\r\u001a\u00020\u00072\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fH\u0007J\u001e\u0010\u0011\u001a\u00020\u00072\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0012\u001a\u00020\u0013H\u0007J$\u0010\u0014\u001a\u00020\u00072\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u000fH\u0007J\u0018\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0007J\u0010\u0010\u001b\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0007J\u000e\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fJ\u0016\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\u000f2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet;", "", "()V", "ARGS", "", "TAG", "forCall", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Factory;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "forDuringGroupCall", "recipientIds", "", "forGroupCall", "identityRecords", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "forIdentityRecordsAndDestination", "destination", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "forIdentityRecordsAndDestinations", "destinations", "forMessageRecord", "context", "Landroid/content/Context;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "forRecipientId", "getArgsFromBundle", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "bundle", "Landroid/os/Bundle;", "getDestinationFromRecord", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$ParcelableRecipientSearchKey;", "Callbacks", "Factory", "SheetFactory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheet {
    private static final String ARGS;
    public static final SafetyNumberBottomSheet INSTANCE = new SafetyNumberBottomSheet();
    private static final String TAG;

    /* compiled from: SafetyNumberBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\u0016\u0010\u0005\u001a\u00020\u00032\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007H&¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Callbacks;", "", "onCanceled", "", "onMessageResentAfterSafetyNumberChangeInBottomSheet", "sendAnywayAfterSafetyNumberChangedInBottomSheet", "destinations", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onCanceled();

        void onMessageResentAfterSafetyNumberChangeInBottomSheet();

        void sendAnywayAfterSafetyNumberChangedInBottomSheet(List<? extends ContactSearchKey.RecipientSearchKey> list);
    }

    /* compiled from: SafetyNumberBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Factory;", "", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Factory {
        void show(FragmentManager fragmentManager);
    }

    private SafetyNumberBottomSheet() {
    }

    @JvmStatic
    public static final Factory forRecipientId(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        return new Factory(recipientId) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$forRecipientId$1
            final /* synthetic */ RecipientId $recipientId;

            /* access modifiers changed from: package-private */
            {
                this.$recipientId = r1;
            }

            @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Factory
            public void show(FragmentManager fragmentManager) {
                Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
                SafetyNumberChangeDialog.show(fragmentManager, this.$recipientId);
            }
        };
    }

    @JvmStatic
    public static final Factory forCall(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        return new Factory(recipientId) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$forCall$1
            final /* synthetic */ RecipientId $recipientId;

            /* access modifiers changed from: package-private */
            {
                this.$recipientId = r1;
            }

            @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Factory
            public void show(FragmentManager fragmentManager) {
                Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
                SafetyNumberChangeDialog.showForCall(fragmentManager, this.$recipientId);
            }
        };
    }

    @JvmStatic
    public static final Factory forGroupCall(List<IdentityRecord> list) {
        Intrinsics.checkNotNullParameter(list, "identityRecords");
        return new Factory(list) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$forGroupCall$1
            final /* synthetic */ List<IdentityRecord> $identityRecords;

            /* access modifiers changed from: package-private */
            {
                this.$identityRecords = r1;
            }

            @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Factory
            public void show(FragmentManager fragmentManager) {
                Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
                SafetyNumberChangeDialog.showForGroupCall(fragmentManager, this.$identityRecords);
            }
        };
    }

    @JvmStatic
    public static final Factory forDuringGroupCall(Collection<? extends RecipientId> collection) {
        Intrinsics.checkNotNullParameter(collection, "recipientIds");
        return new Factory(collection) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet$forDuringGroupCall$1
            final /* synthetic */ Collection<RecipientId> $recipientIds;

            /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: java.util.Collection<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$recipientIds = r1;
            }

            @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Factory
            public void show(FragmentManager fragmentManager) {
                Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
                SafetyNumberChangeDialog.showForDuringGroupCall(fragmentManager, this.$recipientIds);
            }
        };
    }

    @JvmStatic
    public static final Factory forMessageRecord(Context context, MessageRecord messageRecord) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
        Set<IdentityKeyMismatch> identityKeyMismatches = messageRecord.getIdentityKeyMismatches();
        Intrinsics.checkNotNullExpressionValue(identityKeyMismatches, "messageRecord.identityKeyMismatches");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(identityKeyMismatches, 10));
        for (IdentityKeyMismatch identityKeyMismatch : identityKeyMismatches) {
            arrayList.add(identityKeyMismatch.getRecipientId(context));
        }
        return new SheetFactory(new SafetyNumberBottomSheetArgs(arrayList, INSTANCE.getDestinationFromRecord(messageRecord), new MessageId(messageRecord.getId(), messageRecord.isMms())));
    }

    public final SafetyNumberBottomSheetArgs getArgsFromBundle(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs = (SafetyNumberBottomSheetArgs) bundle.getParcelable("args");
        Preconditions.checkArgument(safetyNumberBottomSheetArgs != null);
        Intrinsics.checkNotNull(safetyNumberBottomSheetArgs);
        return safetyNumberBottomSheetArgs;
    }

    private final List<ContactSearchKey.ParcelableRecipientSearchKey> getDestinationFromRecord(MessageRecord messageRecord) {
        ContactSearchKey contactSearchKey;
        StoryType storyType;
        MmsMessageRecord mmsMessageRecord = messageRecord instanceof MmsMessageRecord ? (MmsMessageRecord) messageRecord : null;
        boolean z = true;
        if (mmsMessageRecord == null || (storyType = mmsMessageRecord.getStoryType()) == null || !storyType.isStory()) {
            z = false;
        }
        if (z) {
            RecipientId id = ((MmsMessageRecord) messageRecord).getRecipient().getId();
            Intrinsics.checkNotNullExpressionValue(id, "messageRecord.recipient.id");
            contactSearchKey = new ContactSearchKey.RecipientSearchKey.Story(id);
        } else {
            RecipientId id2 = messageRecord.getRecipient().getId();
            Intrinsics.checkNotNullExpressionValue(id2, "messageRecord.recipient.id");
            contactSearchKey = new ContactSearchKey.RecipientSearchKey.KnownRecipient(id2);
        }
        return CollectionsKt__CollectionsJVMKt.listOf(contactSearchKey.requireParcelable());
    }

    /* compiled from: SafetyNumberBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$SheetFactory;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Factory;", "args", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "(Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;)V", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SheetFactory implements Factory {
        private final SafetyNumberBottomSheetArgs args;

        public SheetFactory(SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs) {
            Intrinsics.checkNotNullParameter(safetyNumberBottomSheetArgs, "args");
            this.args = safetyNumberBottomSheetArgs;
        }

        @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Factory
        public void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment = new SafetyNumberBottomSheetFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("args", this.args);
            safetyNumberBottomSheetFragment.setArguments(bundle);
            safetyNumberBottomSheetFragment.show(fragmentManager, SafetyNumberBottomSheet.TAG);
        }
    }

    @JvmStatic
    public static final Factory forIdentityRecordsAndDestination(List<IdentityRecord> list, ContactSearchKey contactSearchKey) {
        Intrinsics.checkNotNullParameter(list, "identityRecords");
        Intrinsics.checkNotNullParameter(contactSearchKey, "destination");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (IdentityRecord identityRecord : list) {
            arrayList.add(identityRecord.getRecipientId());
        }
        List list2 = CollectionsKt__CollectionsJVMKt.listOf(contactSearchKey);
        ArrayList<ContactSearchKey.RecipientSearchKey> arrayList2 = new ArrayList();
        for (Object obj : list2) {
            if (obj instanceof ContactSearchKey.RecipientSearchKey) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (ContactSearchKey.RecipientSearchKey recipientSearchKey : arrayList2) {
            arrayList3.add(recipientSearchKey.requireParcelable());
        }
        return new SheetFactory(new SafetyNumberBottomSheetArgs(arrayList, arrayList3, null, 4, null));
    }

    @JvmStatic
    public static final Factory forIdentityRecordsAndDestinations(List<IdentityRecord> list, List<? extends ContactSearchKey> list2) {
        Intrinsics.checkNotNullParameter(list, "identityRecords");
        Intrinsics.checkNotNullParameter(list2, "destinations");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (IdentityRecord identityRecord : list) {
            arrayList.add(identityRecord.getRecipientId());
        }
        ArrayList<ContactSearchKey.RecipientSearchKey> arrayList2 = new ArrayList();
        for (Object obj : list2) {
            if (obj instanceof ContactSearchKey.RecipientSearchKey) {
                arrayList2.add(obj);
            }
        }
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
        for (ContactSearchKey.RecipientSearchKey recipientSearchKey : arrayList2) {
            arrayList3.add(recipientSearchKey.requireParcelable());
        }
        return new SheetFactory(new SafetyNumberBottomSheetArgs(arrayList, arrayList3, null, 4, null));
    }
}
