package org.thoughtcrime.securesms.safety;

import org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ SafetyNumberRecipientRowItem.ViewHolder f$0;

    public /* synthetic */ SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda1(SafetyNumberRecipientRowItem.ViewHolder viewHolder) {
        this.f$0 = viewHolder;
    }

    @Override // java.lang.Runnable
    public final void run() {
        SafetyNumberRecipientRowItem.ViewHolder.m2644bind$lambda1$lambda0(this.f$0);
    }
}
