package org.thoughtcrime.securesms.safety;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.reactivestreams.Publisher;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository;
import org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBucket;
import org.thoughtcrime.securesms.util.rx.RxStore;

/* compiled from: SafetyNumberBottomSheetViewModel.kt */
@Metadata(d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 )2\u00020\u0001:\u0002)*B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u001b\u001a\u00020\u001cJ\b\u0010\u001d\u001a\u00020\u001eH\u0014J\u000e\u0010\u001f\u001a\u00020\u001e2\u0006\u0010 \u001a\u00020!J\u000e\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020\u001cJ\u000e\u0010$\u001a\u00020\u001e2\u0006\u0010\u001b\u001a\u00020\u001cJ\u0006\u0010%\u001a\u00020\u001eJ\f\u0010&\u001a\b\u0012\u0004\u0012\u00020(0'R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n8F¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00140\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel;", "Landroidx/lifecycle/ViewModel;", MultiselectForwardFragment.ARGS, "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "repository", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetRepository;", "trustAndVerifyRepository", "Lorg/thoughtcrime/securesms/conversation/ui/error/SafetyNumberChangeRepository;", "(Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetRepository;Lorg/thoughtcrime/securesms/conversation/ui/error/SafetyNumberChangeRepository;)V", "destinationSnapshot", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "getDestinationSnapshot", "()Ljava/util/List;", "destinationStore", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "getIdentityRecord", "Lio/reactivex/rxjava3/core/Maybe;", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onCleared", "", "removeAll", "distributionListBucket", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$DistributionListBucket;", "removeDestination", "destination", "removeRecipientFromSelectedStories", "setDone", "trustAndVerify", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/conversation/ui/error/TrustAndVerifyResult;", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final int MAX_RECIPIENTS_TO_DISPLAY;
    private final SafetyNumberBottomSheetArgs args;
    private final RxStore<List<ContactSearchKey.RecipientSearchKey>> destinationStore;
    private final CompositeDisposable disposables;
    private final SafetyNumberBottomSheetRepository repository;
    private final Flowable<SafetyNumberBottomSheetState> state;
    private final RxStore<SafetyNumberBottomSheetState> store;
    private final SafetyNumberChangeRepository trustAndVerifyRepository;

    public /* synthetic */ SafetyNumberBottomSheetViewModel(SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs, SafetyNumberBottomSheetRepository safetyNumberBottomSheetRepository, SafetyNumberChangeRepository safetyNumberChangeRepository, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(safetyNumberBottomSheetArgs, (i & 2) != 0 ? new SafetyNumberBottomSheetRepository() : safetyNumberBottomSheetRepository, safetyNumberChangeRepository);
    }

    /* JADX DEBUG: Type inference failed for r10v5. Raw type applied. Possible types: io.reactivex.rxjava3.core.Flowable<R>, java.lang.Object, io.reactivex.rxjava3.core.Flowable<U> */
    public SafetyNumberBottomSheetViewModel(SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs, SafetyNumberBottomSheetRepository safetyNumberBottomSheetRepository, SafetyNumberChangeRepository safetyNumberChangeRepository) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetArgs, MultiselectForwardFragment.ARGS);
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetRepository, "repository");
        Intrinsics.checkNotNullParameter(safetyNumberChangeRepository, "trustAndVerifyRepository");
        this.args = safetyNumberBottomSheetArgs;
        this.repository = safetyNumberBottomSheetRepository;
        this.trustAndVerifyRepository = safetyNumberChangeRepository;
        List<ContactSearchKey.ParcelableRecipientSearchKey> destinations = safetyNumberBottomSheetArgs.getDestinations();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(destinations, 10));
        for (ContactSearchKey.ParcelableRecipientSearchKey parcelableRecipientSearchKey : destinations) {
            arrayList.add(parcelableRecipientSearchKey.asRecipientSearchKey());
        }
        RxStore<List<ContactSearchKey.RecipientSearchKey>> rxStore = new RxStore<>(arrayList, null, 2, null);
        this.destinationStore = rxStore;
        RxStore<SafetyNumberBottomSheetState> rxStore2 = new RxStore<>(new SafetyNumberBottomSheetState(this.args.getUntrustedRecipients().size(), this.args.getUntrustedRecipients().size() > 5, null, null, 12, null), null, 2, null);
        this.store = rxStore2;
        Flowable<SafetyNumberBottomSheetState> observeOn = rxStore2.getStateFlowable().observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "store.stateFlowable.obse…dSchedulers.mainThread())");
        this.state = observeOn;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        Flowable switchMap = rxStore.getStateFlowable().switchMap(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SafetyNumberBottomSheetViewModel.m2639_init_$lambda1(SafetyNumberBottomSheetViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(switchMap, "destinationStore.stateFl…ntrustedRecipients, it) }");
        DisposableKt.plusAssign(compositeDisposable, rxStore2.update(switchMap, AnonymousClass1.INSTANCE));
    }

    /* compiled from: SafetyNumberBottomSheetViewModel.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel$Companion;", "", "()V", "MAX_RECIPIENTS_TO_DISPLAY", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final List<ContactSearchKey.RecipientSearchKey> getDestinationSnapshot() {
        return this.destinationStore.getState();
    }

    public final Flowable<SafetyNumberBottomSheetState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-1 */
    public static final Publisher m2639_init_$lambda1(SafetyNumberBottomSheetViewModel safetyNumberBottomSheetViewModel, List list) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetViewModel, "this$0");
        SafetyNumberBottomSheetRepository safetyNumberBottomSheetRepository = safetyNumberBottomSheetViewModel.repository;
        List<RecipientId> untrustedRecipients = safetyNumberBottomSheetViewModel.args.getUntrustedRecipients();
        Intrinsics.checkNotNullExpressionValue(list, "it");
        return safetyNumberBottomSheetRepository.getBuckets(untrustedRecipients, list);
    }

    public final void setDone() {
        this.store.update(SafetyNumberBottomSheetViewModel$setDone$1.INSTANCE);
    }

    public final Single<TrustAndVerifyResult> trustAndVerify() {
        List<SafetyNumberRecipient> list = CollectionsKt___CollectionsKt.distinct(CollectionsKt__IterablesKt.flatten(this.store.getState().getDestinationToRecipientMap().values()));
        if (this.args.getMessageId() != null) {
            Single<TrustAndVerifyResult> trustOrVerifyChangedRecipientsAndResendRx = this.trustAndVerifyRepository.trustOrVerifyChangedRecipientsAndResendRx(list, this.args.getMessageId());
            Intrinsics.checkNotNullExpressionValue(trustOrVerifyChangedRecipientsAndResendRx, "{\n      trustAndVerifyRe…ts, args.messageId)\n    }");
            return trustOrVerifyChangedRecipientsAndResendRx;
        }
        Single<TrustAndVerifyResult> observeOn = this.trustAndVerifyRepository.trustOrVerifyChangedRecipientsRx(list).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "{\n      trustAndVerifyRe…ulers.mainThread())\n    }");
        return observeOn;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final Maybe<IdentityRecord> getIdentityRecord(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Maybe<IdentityRecord> observeOn = this.repository.getIdentityRecord(recipientId).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.getIdentityRe…dSchedulers.mainThread())");
        return observeOn;
    }

    public final void removeRecipientFromSelectedStories(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.removeFromStories(recipientId, this.destinationStore.getState()).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.removeFromSto…nStore.state).subscribe()");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final void removeDestination(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "destination");
        this.destinationStore.update(new Function1<List<? extends ContactSearchKey.RecipientSearchKey>, List<? extends ContactSearchKey.RecipientSearchKey>>(recipientId) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetViewModel$removeDestination$1
            final /* synthetic */ RecipientId $destination;

            /* access modifiers changed from: package-private */
            {
                this.$destination = r1;
            }

            public final List<ContactSearchKey.RecipientSearchKey> invoke(List<? extends ContactSearchKey.RecipientSearchKey> list) {
                Intrinsics.checkNotNullParameter(list, "list");
                RecipientId recipientId2 = this.$destination;
                ArrayList arrayList = new ArrayList();
                for (Object obj : list) {
                    if (!Intrinsics.areEqual(((ContactSearchKey.RecipientSearchKey) obj).getRecipientId(), recipientId2)) {
                        arrayList.add(obj);
                    }
                }
                return arrayList;
            }
        });
    }

    public final void removeAll(SafetyNumberBucket.DistributionListBucket distributionListBucket) {
        Intrinsics.checkNotNullParameter(distributionListBucket, "distributionListBucket");
        List<SafetyNumberRecipient> list = this.store.getState().getDestinationToRecipientMap().get(distributionListBucket);
        if (list != null) {
            CompositeDisposable compositeDisposable = this.disposables;
            SafetyNumberBottomSheetRepository safetyNumberBottomSheetRepository = this.repository;
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (SafetyNumberRecipient safetyNumberRecipient : list) {
                RecipientId id = safetyNumberRecipient.getRecipient().getId();
                Intrinsics.checkNotNullExpressionValue(id, "it.recipient.id");
                arrayList.add(id);
            }
            Disposable subscribe = safetyNumberBottomSheetRepository.removeAllFromStory(arrayList, distributionListBucket.getDistributionListId()).subscribe();
            Intrinsics.checkNotNullExpressionValue(subscribe, "repository.removeAllFrom…butionListId).subscribe()");
            DisposableKt.plusAssign(compositeDisposable, subscribe);
        }
    }

    /* compiled from: SafetyNumberBottomSheetViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", MultiselectForwardFragment.ARGS, "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;", "trustAndVerifyRepository", "Lorg/thoughtcrime/securesms/conversation/ui/error/SafetyNumberChangeRepository;", "(Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheetArgs;Lorg/thoughtcrime/securesms/conversation/ui/error/SafetyNumberChangeRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final SafetyNumberBottomSheetArgs args;
        private final SafetyNumberChangeRepository trustAndVerifyRepository;

        public Factory(SafetyNumberBottomSheetArgs safetyNumberBottomSheetArgs, SafetyNumberChangeRepository safetyNumberChangeRepository) {
            Intrinsics.checkNotNullParameter(safetyNumberBottomSheetArgs, MultiselectForwardFragment.ARGS);
            Intrinsics.checkNotNullParameter(safetyNumberChangeRepository, "trustAndVerifyRepository");
            this.args = safetyNumberBottomSheetArgs;
            this.trustAndVerifyRepository = safetyNumberChangeRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new SafetyNumberBottomSheetViewModel(this.args, null, this.trustAndVerifyRepository, 2, null));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetViewModel.Factory.create");
        }
    }
}
