package org.thoughtcrime.securesms.safety;

import android.view.View;
import org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ SafetyNumberRecipientRowItem.ViewHolder f$0;
    public final /* synthetic */ SafetyNumberRecipientRowItem.Model f$1;

    public /* synthetic */ SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda0(SafetyNumberRecipientRowItem.ViewHolder viewHolder, SafetyNumberRecipientRowItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        SafetyNumberRecipientRowItem.ViewHolder.m2643bind$lambda1(this.f$0, this.f$1, view);
    }
}
