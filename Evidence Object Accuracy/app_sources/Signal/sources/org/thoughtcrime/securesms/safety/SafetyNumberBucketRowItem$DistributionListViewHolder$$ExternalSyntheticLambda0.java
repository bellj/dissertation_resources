package org.thoughtcrime.securesms.safety;

import android.view.View;
import org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SafetyNumberBucketRowItem$DistributionListViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ View f$0;
    public final /* synthetic */ SafetyNumberBucketRowItem.DistributionListModel f$1;

    public /* synthetic */ SafetyNumberBucketRowItem$DistributionListViewHolder$$ExternalSyntheticLambda0(View view, SafetyNumberBucketRowItem.DistributionListModel distributionListModel) {
        this.f$0 = view;
        this.f$1 = distributionListModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        SafetyNumberBucketRowItem.DistributionListViewHolder.m2641bindMenuListener$lambda0(this.f$0, this.f$1, view);
    }
}
