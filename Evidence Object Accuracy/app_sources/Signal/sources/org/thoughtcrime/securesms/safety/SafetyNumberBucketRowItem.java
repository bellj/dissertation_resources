package org.thoughtcrime.securesms.safety;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.safety.SafetyNumberBucket;
import org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: SafetyNumberBucketRowItem.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\bÆ\u0002\u0018\u00002\u00020\u0001:\u0007\u000f\u0010\u0011\u0012\u0013\u0014\u0015B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J,\u0010\u0003\u001a\u0006\u0012\u0002\b\u00030\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bJ\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem;", "", "()V", "createModel", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "safetyNumberBucket", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "actionItemsProvider", "Lkotlin/Function1;", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "BaseViewHolder", "ContactsModel", "ContactsViewHolder", "DistributionListModel", "DistributionListViewHolder", "GroupModel", "GroupViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBucketRowItem {
    public static final SafetyNumberBucketRowItem INSTANCE = new SafetyNumberBucketRowItem();

    /* compiled from: SafetyNumberBucketRowItem.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$ContactsModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "()V", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ContactsModel implements MappingModel<ContactsModel> {
        public boolean areContentsTheSame(ContactsModel contactsModel) {
            Intrinsics.checkNotNullParameter(contactsModel, "newItem");
            return true;
        }

        public boolean areItemsTheSame(ContactsModel contactsModel) {
            Intrinsics.checkNotNullParameter(contactsModel, "newItem");
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(ContactsModel contactsModel) {
            return MappingModel.CC.$default$getChangePayload(this, contactsModel);
        }
    }

    private SafetyNumberBucketRowItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(DistributionListModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SafetyNumberBucketRowItem.DistributionListViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.safety_number_bucket_row_item));
        mappingAdapter.registerFactory(GroupModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SafetyNumberBucketRowItem.GroupViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.safety_number_bucket_row_item));
        mappingAdapter.registerFactory(ContactsModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SafetyNumberBucketRowItem.ContactsViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.safety_number_bucket_row_item));
    }

    public final MappingModel<?> createModel(SafetyNumberBucket safetyNumberBucket, Function1<? super SafetyNumberBucket, ? extends List<ActionItem>> function1) {
        Intrinsics.checkNotNullParameter(safetyNumberBucket, "safetyNumberBucket");
        Intrinsics.checkNotNullParameter(function1, "actionItemsProvider");
        if (Intrinsics.areEqual(safetyNumberBucket, SafetyNumberBucket.ContactsBucket.INSTANCE)) {
            return new ContactsModel();
        }
        if (safetyNumberBucket instanceof SafetyNumberBucket.DistributionListBucket) {
            return new DistributionListModel((SafetyNumberBucket.DistributionListBucket) safetyNumberBucket, function1);
        }
        if (safetyNumberBucket instanceof SafetyNumberBucket.GroupBucket) {
            return new GroupModel((SafetyNumberBucket.GroupBucket) safetyNumberBucket);
        }
        throw new NoWhenBranchMatchedException();
    }

    /* compiled from: SafetyNumberBucketRowItem.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0018\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0005¢\u0006\u0002\u0010\tJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0000H\u0016J\u0010\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0000H\u0016R#\u0010\u0004\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$DistributionListModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "distributionListBucket", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$DistributionListBucket;", "actionItemsProvider", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket;", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "(Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$DistributionListBucket;Lkotlin/jvm/functions/Function1;)V", "getActionItemsProvider", "()Lkotlin/jvm/functions/Function1;", "getDistributionListBucket", "()Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$DistributionListBucket;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DistributionListModel implements MappingModel<DistributionListModel> {
        private final Function1<SafetyNumberBucket, List<ActionItem>> actionItemsProvider;
        private final SafetyNumberBucket.DistributionListBucket distributionListBucket;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(DistributionListModel distributionListModel) {
            return MappingModel.CC.$default$getChangePayload(this, distributionListModel);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.safety.SafetyNumberBucket, ? extends java.util.List<org.thoughtcrime.securesms.components.menu.ActionItem>> */
        /* JADX WARN: Multi-variable type inference failed */
        public DistributionListModel(SafetyNumberBucket.DistributionListBucket distributionListBucket, Function1<? super SafetyNumberBucket, ? extends List<ActionItem>> function1) {
            Intrinsics.checkNotNullParameter(distributionListBucket, "distributionListBucket");
            Intrinsics.checkNotNullParameter(function1, "actionItemsProvider");
            this.distributionListBucket = distributionListBucket;
            this.actionItemsProvider = function1;
        }

        public final SafetyNumberBucket.DistributionListBucket getDistributionListBucket() {
            return this.distributionListBucket;
        }

        public final Function1<SafetyNumberBucket, List<ActionItem>> getActionItemsProvider() {
            return this.actionItemsProvider;
        }

        public boolean areItemsTheSame(DistributionListModel distributionListModel) {
            Intrinsics.checkNotNullParameter(distributionListModel, "newItem");
            return Intrinsics.areEqual(this.distributionListBucket.getDistributionListId(), distributionListModel.distributionListBucket.getDistributionListId());
        }

        public boolean areContentsTheSame(DistributionListModel distributionListModel) {
            Intrinsics.checkNotNullParameter(distributionListModel, "newItem");
            return Intrinsics.areEqual(this.distributionListBucket, distributionListModel.distributionListBucket);
        }
    }

    /* compiled from: SafetyNumberBucketRowItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$GroupModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "groupBucket", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$GroupBucket;", "(Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$GroupBucket;)V", "getGroupBucket", "()Lorg/thoughtcrime/securesms/safety/SafetyNumberBucket$GroupBucket;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupModel implements MappingModel<GroupModel> {
        private final SafetyNumberBucket.GroupBucket groupBucket;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(GroupModel groupModel) {
            return MappingModel.CC.$default$getChangePayload(this, groupModel);
        }

        public GroupModel(SafetyNumberBucket.GroupBucket groupBucket) {
            Intrinsics.checkNotNullParameter(groupBucket, "groupBucket");
            this.groupBucket = groupBucket;
        }

        public final SafetyNumberBucket.GroupBucket getGroupBucket() {
            return this.groupBucket;
        }

        public boolean areItemsTheSame(GroupModel groupModel) {
            Intrinsics.checkNotNullParameter(groupModel, "newItem");
            return Intrinsics.areEqual(this.groupBucket.getRecipient().getId(), groupModel.groupBucket.getRecipient().getId());
        }

        public boolean areContentsTheSame(GroupModel groupModel) {
            Intrinsics.checkNotNullParameter(groupModel, "newItem");
            return this.groupBucket.getRecipient().hasSameContent(groupModel.groupBucket.getRecipient());
        }
    }

    /* compiled from: SafetyNumberBucketRowItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0004H\u0016J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$DistributionListViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$DistributionListModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindMenuListener", "", "model", "menuView", "getTitle", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DistributionListViewHolder extends BaseViewHolder<DistributionListModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public DistributionListViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public String getTitle(DistributionListModel distributionListModel) {
            Intrinsics.checkNotNullParameter(distributionListModel, "model");
            if (!Intrinsics.areEqual(distributionListModel.getDistributionListBucket().getDistributionListId(), DistributionListId.MY_STORY)) {
                return distributionListModel.getDistributionListBucket().getName();
            }
            String string = this.context.getString(R.string.Recipient_my_story);
            Intrinsics.checkNotNullExpressionValue(string, "{\n        context.getStr…cipient_my_story)\n      }");
            return string;
        }

        public void bindMenuListener(DistributionListModel distributionListModel, View view) {
            Intrinsics.checkNotNullParameter(distributionListModel, "model");
            Intrinsics.checkNotNullParameter(view, "menuView");
            view.setOnClickListener(new SafetyNumberBucketRowItem$DistributionListViewHolder$$ExternalSyntheticLambda0(view, distributionListModel));
        }

        /* renamed from: bindMenuListener$lambda-0 */
        public static final void m2641bindMenuListener$lambda0(View view, DistributionListModel distributionListModel, View view2) {
            Intrinsics.checkNotNullParameter(view, "$menuView");
            Intrinsics.checkNotNullParameter(distributionListModel, "$model");
            View rootView = view.getRootView();
            if (rootView != null) {
                SignalContextMenu.Builder builder = new SignalContextMenu.Builder(view, (ViewGroup) rootView);
                DimensionUnit dimensionUnit = DimensionUnit.DP;
                builder.offsetX((int) dimensionUnit.toPixels(16.0f)).offsetY((int) dimensionUnit.toPixels(16.0f)).show(distributionListModel.getActionItemsProvider().invoke(distributionListModel.getDistributionListBucket()));
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }
    }

    /* compiled from: SafetyNumberBucketRowItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0004H\u0016J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$GroupViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$GroupModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindMenuListener", "", "model", "menuView", "getTitle", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupViewHolder extends BaseViewHolder<GroupModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public String getTitle(GroupModel groupModel) {
            Intrinsics.checkNotNullParameter(groupModel, "model");
            String displayName = groupModel.getGroupBucket().getRecipient().getDisplayName(this.context);
            Intrinsics.checkNotNullExpressionValue(displayName, "model.groupBucket.recipi…t.getDisplayName(context)");
            return displayName;
        }

        public void bindMenuListener(GroupModel groupModel, View view) {
            Intrinsics.checkNotNullParameter(groupModel, "model");
            Intrinsics.checkNotNullParameter(view, "menuView");
            ViewExtensionsKt.setVisible(view, false);
        }
    }

    /* compiled from: SafetyNumberBucketRowItem.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u00022\u0006\u0010\t\u001a\u00020\u0004H\u0016J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$ContactsViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$BaseViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$ContactsModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bindMenuListener", "", "model", "menuView", "getTitle", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ContactsViewHolder extends BaseViewHolder<ContactsModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ContactsViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public String getTitle(ContactsModel contactsModel) {
            Intrinsics.checkNotNullParameter(contactsModel, "model");
            String string = this.context.getString(R.string.SafetyNumberBucketRowItem__contacts);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…rBucketRowItem__contacts)");
            return string;
        }

        public void bindMenuListener(ContactsModel contactsModel, View view) {
            Intrinsics.checkNotNullParameter(contactsModel, "model");
            Intrinsics.checkNotNullParameter(view, "menuView");
            ViewExtensionsKt.setVisible(view, false);
        }
    }

    /* compiled from: SafetyNumberBucketRowItem.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0002\b\"\u0018\u0000*\f\b\u0000\u0010\u0001*\u0006\u0012\u0002\b\u00030\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\rJ\u001d\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00028\u00002\u0006\u0010\u0007\u001a\u00020\u0005H&¢\u0006\u0002\u0010\u000fJ\u0015\u0010\u0010\u001a\u00020\u00112\u0006\u0010\f\u001a\u00028\u0000H&¢\u0006\u0002\u0010\u0012R\u000e\u0010\u0007\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberBucketRowItem$BaseViewHolder;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "menuView", "titleView", "Landroid/widget/TextView;", "bind", "", "model", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;)V", "bindMenuListener", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;Landroid/view/View;)V", "getTitle", "", "(Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;)Ljava/lang/String;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class BaseViewHolder<T extends MappingModel<?>> extends MappingViewHolder<T> {
        private final View menuView;
        private final TextView titleView;

        public abstract void bindMenuListener(T t, View view);

        public abstract String getTitle(T t);

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BaseViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.safety_number_bucket_header);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.safety_number_bucket_header)");
            this.titleView = (TextView) findViewById;
            View findViewById2 = findViewById(R.id.safety_number_bucket_menu);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.safety_number_bucket_menu)");
            this.menuView = findViewById2;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.safety.SafetyNumberBucketRowItem$BaseViewHolder<T extends org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<?>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public /* bridge */ /* synthetic */ void bind(Object obj) {
            bind((BaseViewHolder<T>) ((MappingModel) obj));
        }

        public void bind(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            this.titleView.setText(getTitle(t));
            bindMenuListener(t, this.menuView);
        }
    }
}
