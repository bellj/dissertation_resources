package org.thoughtcrime.securesms.safety;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.OptionalExtensionsKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: SafetyNumberRecipientRowItem.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipientRowItem;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberRecipientRowItem {
    public static final SafetyNumberRecipientRowItem INSTANCE = new SafetyNumberRecipientRowItem();

    private SafetyNumberRecipientRowItem() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SafetyNumberRecipientRowItem.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.safety_number_recipient_row_item));
    }

    /* compiled from: SafetyNumberRecipientRowItem.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0018\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n¢\u0006\u0002\u0010\rJ\u0010\u0010\u0016\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0000H\u0016J\u0010\u0010\u0018\u001a\u00020\u00052\u0006\u0010\u0017\u001a\u00020\u0000H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR#\u0010\t\u001a\u0014\u0012\u0004\u0012\u00020\u0000\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipientRowItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isVerified", "", "distributionListMembershipCount", "", "groupMembershipCount", "getContextMenuActions", "Lkotlin/Function1;", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;ZIILkotlin/jvm/functions/Function1;)V", "getDistributionListMembershipCount", "()I", "getGetContextMenuActions", "()Lkotlin/jvm/functions/Function1;", "getGroupMembershipCount", "()Z", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final int distributionListMembershipCount;
        private final Function1<Model, List<ActionItem>> getContextMenuActions;
        private final int groupMembershipCount;
        private final boolean isVerified;
        private final Recipient recipient;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem$Model, ? extends java.util.List<org.thoughtcrime.securesms.components.menu.ActionItem>> */
        /* JADX WARN: Multi-variable type inference failed */
        public Model(Recipient recipient, boolean z, int i, int i2, Function1<? super Model, ? extends List<ActionItem>> function1) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(function1, "getContextMenuActions");
            this.recipient = recipient;
            this.isVerified = z;
            this.distributionListMembershipCount = i;
            this.groupMembershipCount = i2;
            this.getContextMenuActions = function1;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final boolean isVerified() {
            return this.isVerified;
        }

        public final int getDistributionListMembershipCount() {
            return this.distributionListMembershipCount;
        }

        public final int getGroupMembershipCount() {
            return this.groupMembershipCount;
        }

        public final Function1<Model, List<ActionItem>> getGetContextMenuActions() {
            return this.getContextMenuActions;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.recipient.getId(), model.recipient.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.recipient.hasSameContent(model.recipient) && this.isVerified == model.isVerified && this.distributionListMembershipCount == model.distributionListMembershipCount && this.groupMembershipCount == model.groupMembershipCount;
        }
    }

    /* compiled from: SafetyNumberRecipientRowItem.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipientRowItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipientRowItem$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "identifier", "Landroid/widget/TextView;", "name", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final AvatarImageView avatar;
        private final TextView identifier;
        private final TextView name;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.safety_number_recipient_avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.…_number_recipient_avatar)");
            this.avatar = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.safety_number_recipient_name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…ty_number_recipient_name)");
            this.name = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.safety_number_recipient_identifier);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.…ber_recipient_identifier)");
            this.identifier = (TextView) findViewById3;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.avatar.setRecipient(model.getRecipient());
            this.name.setText(model.getRecipient().getDisplayName(this.context));
            Optional<String> e164 = model.getRecipient().getE164();
            Intrinsics.checkNotNullExpressionValue(e164, "model.recipient.e164");
            Optional<String> username = model.getRecipient().getUsername();
            Intrinsics.checkNotNullExpressionValue(username, "model.recipient.username");
            String str = (String) OptionalExtensionsKt.or(e164, username).orElse(null);
            boolean z = false;
            if (model.isVerified()) {
                if (str == null || (StringsKt__StringsJVMKt.isBlank(str))) {
                    str = this.context.getString(R.string.SafetyNumberRecipientRowItem__verified);
                    this.identifier.setText(str);
                    TextView textView = this.identifier;
                    if (str != null || (StringsKt__StringsJVMKt.isBlank(str))) {
                        z = true;
                    }
                    ViewExtensionsKt.setVisible(textView, !z);
                    this.itemView.setOnClickListener(new SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda0(this, model));
                }
            }
            if (model.isVerified()) {
                str = this.context.getString(R.string.SafetyNumberRecipientRowItem__s_dot_verified, str);
            }
            this.identifier.setText(str);
            TextView textView = this.identifier;
            if (str != null) {
            }
            z = true;
            ViewExtensionsKt.setVisible(textView, !z);
            this.itemView.setOnClickListener(new SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda0(this, model));
        }

        /* renamed from: bind$lambda-1 */
        public static final void m2643bind$lambda1(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.itemView.setSelected(true);
            View view2 = viewHolder.itemView;
            Intrinsics.checkNotNullExpressionValue(view2, "itemView");
            View rootView = viewHolder.itemView.getRootView();
            if (rootView != null) {
                new SignalContextMenu.Builder(view2, (ViewGroup) rootView).offsetY((int) DimensionUnit.DP.toPixels(12.0f)).onDismiss(new SafetyNumberRecipientRowItem$ViewHolder$$ExternalSyntheticLambda1(viewHolder)).show(model.getGetContextMenuActions().invoke(model));
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }

        /* renamed from: bind$lambda-1$lambda-0 */
        public static final void m2644bind$lambda1$lambda0(ViewHolder viewHolder) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            viewHolder.itemView.setSelected(false);
        }
    }
}
