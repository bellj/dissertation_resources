package org.thoughtcrime.securesms.safety;

import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.crypto.IdentityKeyParcelable;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberRecipientRowItem;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.verify.VerifyIdentityFragment;

/* compiled from: SafetyNumberBottomSheetFragment.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0004H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "model", "Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipientRowItem$Model;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberBottomSheetFragment$getConfiguration$1$1$1 extends Lambda implements Function1<SafetyNumberRecipientRowItem.Model, List<? extends ActionItem>> {
    final /* synthetic */ SafetyNumberBottomSheetFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SafetyNumberBottomSheetFragment$getConfiguration$1$1$1(SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment) {
        super(1);
        this.this$0 = safetyNumberBottomSheetFragment;
    }

    public final List<ActionItem> invoke(SafetyNumberRecipientRowItem.Model model) {
        Intrinsics.checkNotNullParameter(model, "model");
        ArrayList arrayList = new ArrayList();
        String string = this.this$0.getString(R.string.SafetyNumberBottomSheetFragment__verify_safety_number);
        Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.Safet…nt__verify_safety_number)");
        arrayList.add(new ActionItem(R.drawable.ic_safety_number_24, string, R.color.signal_colorOnSurface, new Runnable(model) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$getConfiguration$1$1$1$$ExternalSyntheticLambda1
            public final /* synthetic */ SafetyNumberRecipientRowItem.Model f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SafetyNumberBottomSheetFragment$getConfiguration$1$1$1.$r8$lambda$OvLOnO53tQPB27cMbIeJWykiza4(SafetyNumberBottomSheetFragment.this, this.f$1);
            }
        }));
        if (model.getDistributionListMembershipCount() > 0) {
            String string2 = this.this$0.getString(R.string.SafetyNumberBottomSheetFragment__remove_from_story);
            Intrinsics.checkNotNullExpressionValue(string2, "getString(R.string.Safet…gment__remove_from_story)");
            arrayList.add(new ActionItem(R.drawable.ic_circle_x_24, string2, R.color.signal_colorOnSurface, new Runnable(model) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$getConfiguration$1$1$1$$ExternalSyntheticLambda2
                public final /* synthetic */ SafetyNumberRecipientRowItem.Model f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SafetyNumberBottomSheetFragment$getConfiguration$1$1$1.$r8$lambda$rrbCnYm536PJyhLzheXfa8AlpJg(SafetyNumberBottomSheetFragment.this, this.f$1);
                }
            }));
        }
        if (model.getDistributionListMembershipCount() == 0 && model.getGroupMembershipCount() == 0) {
            String string3 = this.this$0.getString(R.string.SafetyNumberReviewConnectionsFragment__remove);
            Intrinsics.checkNotNullExpressionValue(string3, "getString(R.string.Safet…nectionsFragment__remove)");
            arrayList.add(new ActionItem(R.drawable.ic_circle_x_24, string3, R.color.signal_colorOnSurface, new Runnable(model) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$getConfiguration$1$1$1$$ExternalSyntheticLambda3
                public final /* synthetic */ SafetyNumberRecipientRowItem.Model f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SafetyNumberBottomSheetFragment$getConfiguration$1$1$1.$r8$lambda$q4fBV4x44V0jjPNdW6N6xpyj5sE(SafetyNumberBottomSheetFragment.this, this.f$1);
                }
            }));
        }
        return arrayList;
    }

    /* renamed from: invoke$lambda-1 */
    public static final void m2616invoke$lambda1(SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, SafetyNumberRecipientRowItem.Model model) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        Intrinsics.checkNotNullParameter(model, "$model");
        LifecycleDisposable lifecycleDisposable = safetyNumberBottomSheetFragment.lifecycleDisposable;
        SafetyNumberBottomSheetViewModel safetyNumberBottomSheetViewModel = safetyNumberBottomSheetFragment.getViewModel();
        RecipientId id = model.getRecipient().getId();
        Intrinsics.checkNotNullExpressionValue(id, "model.recipient.id");
        Disposable subscribe = safetyNumberBottomSheetViewModel.getIdentityRecord(id).subscribe(new Consumer(safetyNumberBottomSheetFragment) { // from class: org.thoughtcrime.securesms.safety.SafetyNumberBottomSheetFragment$getConfiguration$1$1$1$$ExternalSyntheticLambda0
            public final /* synthetic */ SafetyNumberBottomSheetFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SafetyNumberBottomSheetFragment$getConfiguration$1$1$1.m2615$r8$lambda$qIzuqlmRjeBXbaxPrKOEUE4mUg(SafetyNumberRecipientRowItem.Model.this, this.f$1, (IdentityRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.getIdentityRec…)\n                      }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: invoke$lambda-1$lambda-0 */
    public static final void m2617invoke$lambda1$lambda0(SafetyNumberRecipientRowItem.Model model, SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, IdentityRecord identityRecord) {
        Intrinsics.checkNotNullParameter(model, "$model");
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        VerifyIdentityFragment.Companion companion = VerifyIdentityFragment.Companion;
        RecipientId id = model.getRecipient().getId();
        Intrinsics.checkNotNullExpressionValue(id, "model.recipient.id");
        companion.createDialog(id, new IdentityKeyParcelable(identityRecord.getIdentityKey()), false).show(safetyNumberBottomSheetFragment.getChildFragmentManager(), (String) null);
    }

    /* renamed from: invoke$lambda-2 */
    public static final void m2618invoke$lambda2(SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, SafetyNumberRecipientRowItem.Model model) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        Intrinsics.checkNotNullParameter(model, "$model");
        SafetyNumberBottomSheetViewModel safetyNumberBottomSheetViewModel = safetyNumberBottomSheetFragment.getViewModel();
        RecipientId id = model.getRecipient().getId();
        Intrinsics.checkNotNullExpressionValue(id, "model.recipient.id");
        safetyNumberBottomSheetViewModel.removeRecipientFromSelectedStories(id);
    }

    /* renamed from: invoke$lambda-3 */
    public static final void m2619invoke$lambda3(SafetyNumberBottomSheetFragment safetyNumberBottomSheetFragment, SafetyNumberRecipientRowItem.Model model) {
        Intrinsics.checkNotNullParameter(safetyNumberBottomSheetFragment, "this$0");
        Intrinsics.checkNotNullParameter(model, "$model");
        SafetyNumberBottomSheetViewModel safetyNumberBottomSheetViewModel = safetyNumberBottomSheetFragment.getViewModel();
        RecipientId id = model.getRecipient().getId();
        Intrinsics.checkNotNullExpressionValue(id, "model.recipient.id");
        safetyNumberBottomSheetViewModel.removeDestination(id);
    }
}
