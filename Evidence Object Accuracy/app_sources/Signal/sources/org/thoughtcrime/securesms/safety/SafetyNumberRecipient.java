package org.thoughtcrime.securesms.safety;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: SafetyNumberRecipient.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0007HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0007HÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u0007HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/safety/SafetyNumberRecipient;", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "identityRecord", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "distributionListMembershipCount", "", "groupMembershipCount", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/IdentityRecord;II)V", "getDistributionListMembershipCount", "()I", "getGroupMembershipCount", "getIdentityRecord", "()Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SafetyNumberRecipient {
    private final int distributionListMembershipCount;
    private final int groupMembershipCount;
    private final IdentityRecord identityRecord;
    private final Recipient recipient;

    public static /* synthetic */ SafetyNumberRecipient copy$default(SafetyNumberRecipient safetyNumberRecipient, Recipient recipient, IdentityRecord identityRecord, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            recipient = safetyNumberRecipient.recipient;
        }
        if ((i3 & 2) != 0) {
            identityRecord = safetyNumberRecipient.identityRecord;
        }
        if ((i3 & 4) != 0) {
            i = safetyNumberRecipient.distributionListMembershipCount;
        }
        if ((i3 & 8) != 0) {
            i2 = safetyNumberRecipient.groupMembershipCount;
        }
        return safetyNumberRecipient.copy(recipient, identityRecord, i, i2);
    }

    public final Recipient component1() {
        return this.recipient;
    }

    public final IdentityRecord component2() {
        return this.identityRecord;
    }

    public final int component3() {
        return this.distributionListMembershipCount;
    }

    public final int component4() {
        return this.groupMembershipCount;
    }

    public final SafetyNumberRecipient copy(Recipient recipient, IdentityRecord identityRecord, int i, int i2) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(identityRecord, "identityRecord");
        return new SafetyNumberRecipient(recipient, identityRecord, i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SafetyNumberRecipient)) {
            return false;
        }
        SafetyNumberRecipient safetyNumberRecipient = (SafetyNumberRecipient) obj;
        return Intrinsics.areEqual(this.recipient, safetyNumberRecipient.recipient) && Intrinsics.areEqual(this.identityRecord, safetyNumberRecipient.identityRecord) && this.distributionListMembershipCount == safetyNumberRecipient.distributionListMembershipCount && this.groupMembershipCount == safetyNumberRecipient.groupMembershipCount;
    }

    public int hashCode() {
        return (((((this.recipient.hashCode() * 31) + this.identityRecord.hashCode()) * 31) + this.distributionListMembershipCount) * 31) + this.groupMembershipCount;
    }

    public String toString() {
        return "SafetyNumberRecipient(recipient=" + this.recipient + ", identityRecord=" + this.identityRecord + ", distributionListMembershipCount=" + this.distributionListMembershipCount + ", groupMembershipCount=" + this.groupMembershipCount + ')';
    }

    public SafetyNumberRecipient(Recipient recipient, IdentityRecord identityRecord, int i, int i2) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(identityRecord, "identityRecord");
        this.recipient = recipient;
        this.identityRecord = identityRecord;
        this.distributionListMembershipCount = i;
        this.groupMembershipCount = i2;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final IdentityRecord getIdentityRecord() {
        return this.identityRecord;
    }

    public final int getDistributionListMembershipCount() {
        return this.distributionListMembershipCount;
    }

    public final int getGroupMembershipCount() {
        return this.groupMembershipCount;
    }
}
