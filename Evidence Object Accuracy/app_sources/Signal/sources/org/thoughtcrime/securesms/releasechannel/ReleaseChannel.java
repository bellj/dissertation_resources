package org.thoughtcrime.securesms.releasechannel;

import j$.util.Optional;
import java.util.List;
import java.util.UUID;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.PointerAttachment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.mms.IncomingMediaMessage;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentRemoteId;

/* compiled from: ReleaseChannel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002Jb\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000e\u001a\u00020\u00042\b\b\u0002\u0010\u000f\u001a\u00020\u00042\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u0014R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/releasechannel/ReleaseChannel;", "", "()V", "CDN_NUMBER", "", "insertReleaseChannelMessage", "Lorg/thoughtcrime/securesms/database/MessageDatabase$InsertResult;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "body", "", "threadId", "", DraftDatabase.Draft.IMAGE, "imageWidth", "imageHeight", "serverUuid", "messageRanges", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList;", "storyType", "Lorg/thoughtcrime/securesms/database/model/StoryType;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReleaseChannel {
    public static final int CDN_NUMBER;
    public static final ReleaseChannel INSTANCE = new ReleaseChannel();

    private ReleaseChannel() {
    }

    public static /* synthetic */ MessageDatabase.InsertResult insertReleaseChannelMessage$default(ReleaseChannel releaseChannel, RecipientId recipientId, String str, long j, String str2, int i, int i2, String str3, BodyRangeList bodyRangeList, StoryType storyType, int i3, Object obj) {
        StoryType storyType2;
        String str4 = (i3 & 8) != 0 ? null : str2;
        int i4 = (i3 & 16) != 0 ? 0 : i;
        int i5 = (i3 & 32) != 0 ? 0 : i2;
        String uuid = (i3 & 64) != 0 ? UUID.randomUUID().toString() : str3;
        BodyRangeList bodyRangeList2 = (i3 & 128) != 0 ? null : bodyRangeList;
        if ((i3 & 256) != 0) {
            storyType2 = StoryType.NONE;
        } else {
            storyType2 = storyType;
        }
        return releaseChannel.insertReleaseChannelMessage(recipientId, str, j, str4, i4, i5, uuid, bodyRangeList2, storyType2);
    }

    public final MessageDatabase.InsertResult insertReleaseChannelMessage(RecipientId recipientId, String str, long j, String str2, int i, int i2, String str3, BodyRangeList bodyRangeList, StoryType storyType) {
        Optional optional;
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(str, "body");
        Intrinsics.checkNotNullParameter(storyType, "storyType");
        if (str2 != null) {
            optional = Optional.of(CollectionsKt__CollectionsJVMKt.listOf(new SignalServiceAttachmentPointer(-1, SignalServiceAttachmentRemoteId.from(""), MediaUtil.IMAGE_WEBP, null, Optional.empty(), Optional.empty(), i, i2, Optional.empty(), Optional.of(str2), false, false, false, Optional.empty(), Optional.empty(), System.currentTimeMillis())));
            Intrinsics.checkNotNullExpressionValue(optional, "{\n      val attachment =…listOf(attachment))\n    }");
        } else {
            optional = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(optional, "{\n      Optional.empty()\n    }");
        }
        long currentTimeMillis = System.currentTimeMillis();
        long currentTimeMillis2 = System.currentTimeMillis();
        long currentTimeMillis3 = System.currentTimeMillis();
        List<Attachment> forPointers = PointerAttachment.forPointers(optional);
        Intrinsics.checkNotNullExpressionValue(forPointers, "forPointers(attachments)");
        return SignalDatabase.Companion.mms().insertSecureDecryptedMessageInbox(new IncomingMediaMessage(recipientId, null, str, false, storyType, null, false, currentTimeMillis, currentTimeMillis2, currentTimeMillis3, 0, 0, false, null, false, false, str3, bodyRangeList, forPointers, null, null, null, null, 7928938, null), j).orElse(null);
    }
}
