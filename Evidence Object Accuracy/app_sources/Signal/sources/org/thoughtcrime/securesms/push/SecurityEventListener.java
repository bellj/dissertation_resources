package org.thoughtcrime.securesms.push;

import android.content.Context;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.SecurityEvent;
import org.whispersystems.signalservice.api.SignalServiceMessageSender;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;

/* loaded from: classes4.dex */
public class SecurityEventListener implements SignalServiceMessageSender.EventListener {
    private static final String TAG = Log.tag(SecurityEventListener.class);
    private final Context context;

    public SecurityEventListener(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override // org.whispersystems.signalservice.api.SignalServiceMessageSender.EventListener
    public void onSecurityEvent(SignalServiceAddress signalServiceAddress) {
        SecurityEvent.broadcastSecurityUpdateEvent(this.context);
    }
}
