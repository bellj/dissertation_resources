package org.thoughtcrime.securesms.push;

import android.content.Context;
import j$.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import okhttp3.CipherSuite;
import okhttp3.ConnectionSpec;
import okhttp3.Dns;
import okhttp3.Interceptor;
import okhttp3.TlsVersion;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.CustomDns;
import org.thoughtcrime.securesms.net.DeprecatedClientPreventionInterceptor;
import org.thoughtcrime.securesms.net.DeviceTransferBlockingInterceptor;
import org.thoughtcrime.securesms.net.RemoteDeprecationDetectorInterceptor;
import org.thoughtcrime.securesms.net.SequentialDns;
import org.thoughtcrime.securesms.net.StandardUserAgentInterceptor;
import org.thoughtcrime.securesms.net.StaticDns;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.push.TrustStore;
import org.whispersystems.signalservice.internal.configuration.SignalCdnUrl;
import org.whispersystems.signalservice.internal.configuration.SignalCdsiUrl;
import org.whispersystems.signalservice.internal.configuration.SignalContactDiscoveryUrl;
import org.whispersystems.signalservice.internal.configuration.SignalKeyBackupServiceUrl;
import org.whispersystems.signalservice.internal.configuration.SignalServiceConfiguration;
import org.whispersystems.signalservice.internal.configuration.SignalServiceUrl;
import org.whispersystems.signalservice.internal.configuration.SignalStorageUrl;

/* compiled from: SignalServiceNetworkAccess.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0012\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u0000 (2\u00020\u0001:\u0002()B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0016\u0010\u001f\u001a\u00020\u000b2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0002J\u0006\u0010!\u001a\u00020\u000bJ\u0010\u0010!\u001a\u00020\u000b2\b\u0010\"\u001a\u0004\u0018\u00010\u0014J\u0006\u0010#\u001a\u00020$J\u0010\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010\u0014J\u000e\u0010&\u001a\u00020$2\u0006\u0010'\u001a\u00020\nR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0004¢\u0006\u0004\n\u0002\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00180\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u001a\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u001eX\u0004¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/push/SignalServiceNetworkAccess;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "baseGHostConfigs", "", "Lorg/thoughtcrime/securesms/push/SignalServiceNetworkAccess$HostConfig;", "censorshipConfiguration", "", "", "Lorg/whispersystems/signalservice/internal/configuration/SignalServiceConfiguration;", "defaultCensoredConfiguration", "defaultCensoredCountryCodes", "", "fConfig", "fTrustStore", "Lorg/whispersystems/signalservice/api/push/TrustStore;", "fUrls", "", "", "[Ljava/lang/String;", "gTrustStore", "interceptors", "Lokhttp3/Interceptor;", "serviceTrustStore", "uncensoredConfiguration", "getUncensoredConfiguration", "()Lorg/whispersystems/signalservice/internal/configuration/SignalServiceConfiguration;", "zkGroupServerPublicParams", "", "buildGConfiguration", "hostConfigs", "getConfiguration", "localNumber", "isCensored", "", "number", "isCountryCodeCensoredByDefault", "countryCode", "Companion", "HostConfig", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class SignalServiceNetworkAccess {
    private static final ConnectionSpec APP_CONNECTION_SPEC;
    private static final int COUNTRY_CODE_CUBA;
    private static final int COUNTRY_CODE_EGYPT;
    private static final int COUNTRY_CODE_IRAN;
    private static final int COUNTRY_CODE_OMAN;
    private static final int COUNTRY_CODE_QATAR;
    private static final int COUNTRY_CODE_UAE;
    private static final int COUNTRY_CODE_UKRAINE;
    private static final int COUNTRY_CODE_UZBEKISTAN;
    public static final Companion Companion;
    public static final Dns DNS;
    private static final String F_CDN2_HOST;
    private static final String F_CDN_HOST;
    private static final String F_DIRECTORY_HOST;
    private static final String F_KBS_HOST;
    private static final String F_SERVICE_HOST;
    private static final String F_STORAGE_HOST;
    private static final ConnectionSpec GMAIL_CONNECTION_SPEC;
    private static final ConnectionSpec GMAPS_CONNECTION_SPEC;
    private static final String G_HOST;
    private static final ConnectionSpec PLAY_CONNECTION_SPEC;
    private static final String TAG = Log.tag(SignalServiceNetworkAccess.class);
    private final List<HostConfig> baseGHostConfigs;
    private final Map<Integer, SignalServiceConfiguration> censorshipConfiguration;
    private final SignalServiceConfiguration defaultCensoredConfiguration;
    private final Set<Integer> defaultCensoredCountryCodes;
    private final SignalServiceConfiguration fConfig;
    private final TrustStore fTrustStore;
    private final String[] fUrls;
    private final TrustStore gTrustStore;
    private final List<Interceptor> interceptors = CollectionsKt__CollectionsKt.listOf((Object[]) new Interceptor[]{new StandardUserAgentInterceptor(), new RemoteDeprecationDetectorInterceptor(), new DeprecatedClientPreventionInterceptor(), DeviceTransferBlockingInterceptor.getInstance()});
    private final TrustStore serviceTrustStore;
    private final SignalServiceConfiguration uncensoredConfiguration;
    private final byte[] zkGroupServerPublicParams;

    /* compiled from: SignalServiceNetworkAccess.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[SettingsValues.CensorshipCircumventionEnabled.values().length];
            iArr[SettingsValues.CensorshipCircumventionEnabled.ENABLED.ordinal()] = 1;
            iArr[SettingsValues.CensorshipCircumventionEnabled.DISABLED.ordinal()] = 2;
            iArr[SettingsValues.CensorshipCircumventionEnabled.DEFAULT.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* compiled from: SignalServiceNetworkAccess.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\f\u0010\u001d\u001a\u00020\u0012*\u00020\u0012H\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007XT¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u00020\u00108\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0018\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0019\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0012XT¢\u0006\u0002\n\u0000R\u0016\u0010\u001b\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u001c\u001a\n \u0005*\u0004\u0018\u00010\u00120\u0012X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/push/SignalServiceNetworkAccess$Companion;", "", "()V", "APP_CONNECTION_SPEC", "Lokhttp3/ConnectionSpec;", "kotlin.jvm.PlatformType", "COUNTRY_CODE_CUBA", "", "COUNTRY_CODE_EGYPT", "COUNTRY_CODE_IRAN", "COUNTRY_CODE_OMAN", "COUNTRY_CODE_QATAR", "COUNTRY_CODE_UAE", "COUNTRY_CODE_UKRAINE", "COUNTRY_CODE_UZBEKISTAN", "DNS", "Lokhttp3/Dns;", "F_CDN2_HOST", "", "F_CDN_HOST", "F_DIRECTORY_HOST", "F_KBS_HOST", "F_SERVICE_HOST", "F_STORAGE_HOST", "GMAIL_CONNECTION_SPEC", "GMAPS_CONNECTION_SPEC", "G_HOST", "PLAY_CONNECTION_SPEC", "TAG", "stripProtocol", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String stripProtocol(String str) {
            return StringsKt__StringsKt.removePrefix(str, (CharSequence) "https://");
        }
    }

    public SignalServiceNetworkAccess(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.serviceTrustStore = new SignalServiceTrustStore(context);
        this.gTrustStore = new DomainFrontingTrustStore(context);
        this.fTrustStore = new DomainFrontingDigicertTrustStore(context);
        int i = 0;
        try {
            byte[] decode = Base64.decode(BuildConfig.ZKGROUP_SERVER_PUBLIC_PARAMS);
            Intrinsics.checkNotNullExpressionValue(decode, "{\n    Base64.decode(Buil…SERVER_PUBLIC_PARAMS)\n  }");
            this.zkGroupServerPublicParams = decode;
            ConnectionSpec connectionSpec = GMAIL_CONNECTION_SPEC;
            Intrinsics.checkNotNullExpressionValue(connectionSpec, "GMAIL_CONNECTION_SPEC");
            ConnectionSpec connectionSpec2 = PLAY_CONNECTION_SPEC;
            Intrinsics.checkNotNullExpressionValue(connectionSpec2, "PLAY_CONNECTION_SPEC");
            ConnectionSpec connectionSpec3 = GMAPS_CONNECTION_SPEC;
            Intrinsics.checkNotNullExpressionValue(connectionSpec3, "GMAPS_CONNECTION_SPEC");
            Intrinsics.checkNotNullExpressionValue(connectionSpec3, "GMAPS_CONNECTION_SPEC");
            Intrinsics.checkNotNullExpressionValue(connectionSpec, "GMAIL_CONNECTION_SPEC");
            this.baseGHostConfigs = CollectionsKt__CollectionsKt.listOf((Object[]) new HostConfig[]{new HostConfig("https://www.google.com", G_HOST, connectionSpec), new HostConfig("https://android.clients.google.com", G_HOST, connectionSpec2), new HostConfig("https://clients3.google.com", G_HOST, connectionSpec3), new HostConfig("https://clients4.google.com", G_HOST, connectionSpec3), new HostConfig("https://inbox.google.com", G_HOST, connectionSpec)});
            String[] strArr = {"https://cdn.sstatic.net", "https://github.githubassets.com", "https://pinterest.com", "https://open.scdn.co", "https://www.redditstatic.com"};
            this.fUrls = strArr;
            ArrayList arrayList = new ArrayList(strArr.length);
            for (String str : strArr) {
                arrayList.add(new SignalServiceUrl(str, F_SERVICE_HOST, this.fTrustStore, APP_CONNECTION_SPEC));
            }
            Object[] array = arrayList.toArray(new SignalServiceUrl[0]);
            if (array != null) {
                SignalServiceUrl[] signalServiceUrlArr = (SignalServiceUrl[]) array;
                Pair[] pairArr = new Pair[2];
                String[] strArr2 = this.fUrls;
                ArrayList arrayList2 = new ArrayList(strArr2.length);
                int length = strArr2.length;
                int i2 = 0;
                while (i2 < length) {
                    arrayList2.add(new SignalCdnUrl(strArr2[i2], F_CDN_HOST, this.fTrustStore, APP_CONNECTION_SPEC));
                    i2++;
                    i = 0;
                }
                Object[] array2 = arrayList2.toArray(new SignalCdnUrl[i]);
                if (array2 != null) {
                    pairArr[i] = TuplesKt.to(0, array2);
                    String[] strArr3 = this.fUrls;
                    ArrayList arrayList3 = new ArrayList(strArr3.length);
                    for (String str2 : strArr3) {
                        arrayList3.add(new SignalCdnUrl(str2, F_CDN2_HOST, this.fTrustStore, APP_CONNECTION_SPEC));
                    }
                    Object[] array3 = arrayList3.toArray(new SignalCdnUrl[0]);
                    if (array3 != null) {
                        pairArr[1] = TuplesKt.to(2, array3);
                        Map map = MapsKt__MapsKt.mapOf(pairArr);
                        String[] strArr4 = this.fUrls;
                        ArrayList arrayList4 = new ArrayList(strArr4.length);
                        for (String str3 : strArr4) {
                            arrayList4.add(new SignalContactDiscoveryUrl(str3, F_DIRECTORY_HOST, this.fTrustStore, APP_CONNECTION_SPEC));
                        }
                        Object[] array4 = arrayList4.toArray(new SignalContactDiscoveryUrl[0]);
                        if (array4 != null) {
                            SignalContactDiscoveryUrl[] signalContactDiscoveryUrlArr = (SignalContactDiscoveryUrl[]) array4;
                            String[] strArr5 = this.fUrls;
                            ArrayList arrayList5 = new ArrayList(strArr5.length);
                            for (String str4 : strArr5) {
                                arrayList5.add(new SignalKeyBackupServiceUrl(str4, F_KBS_HOST, this.fTrustStore, APP_CONNECTION_SPEC));
                            }
                            Object[] array5 = arrayList5.toArray(new SignalKeyBackupServiceUrl[0]);
                            if (array5 != null) {
                                SignalKeyBackupServiceUrl[] signalKeyBackupServiceUrlArr = (SignalKeyBackupServiceUrl[]) array5;
                                String[] strArr6 = this.fUrls;
                                ArrayList arrayList6 = new ArrayList(strArr6.length);
                                for (String str5 : strArr6) {
                                    arrayList6.add(new SignalStorageUrl(str5, F_STORAGE_HOST, this.fTrustStore, APP_CONNECTION_SPEC));
                                }
                                Object[] array6 = arrayList6.toArray(new SignalStorageUrl[0]);
                                if (array6 != null) {
                                    SignalStorageUrl[] signalStorageUrlArr = (SignalStorageUrl[]) array6;
                                    SignalCdsiUrl[] signalCdsiUrlArr = {new SignalCdsiUrl(BuildConfig.SIGNAL_CDSI_URL, this.serviceTrustStore)};
                                    List<Interceptor> list = this.interceptors;
                                    Dns dns = DNS;
                                    SignalServiceConfiguration signalServiceConfiguration = new SignalServiceConfiguration(signalServiceUrlArr, map, signalContactDiscoveryUrlArr, signalKeyBackupServiceUrlArr, signalStorageUrlArr, signalCdsiUrlArr, list, Optional.of(dns), Optional.empty(), this.zkGroupServerPublicParams);
                                    this.fConfig = signalServiceConfiguration;
                                    ConnectionSpec connectionSpec4 = GMAIL_CONNECTION_SPEC;
                                    Intrinsics.checkNotNullExpressionValue(connectionSpec4, "GMAIL_CONNECTION_SPEC");
                                    Integer valueOf = Integer.valueOf((int) COUNTRY_CODE_UAE);
                                    Intrinsics.checkNotNullExpressionValue(connectionSpec4, "GMAIL_CONNECTION_SPEC");
                                    Integer valueOf2 = Integer.valueOf((int) COUNTRY_CODE_OMAN);
                                    Intrinsics.checkNotNullExpressionValue(connectionSpec4, "GMAIL_CONNECTION_SPEC");
                                    Integer valueOf3 = Integer.valueOf((int) COUNTRY_CODE_QATAR);
                                    Intrinsics.checkNotNullExpressionValue(connectionSpec4, "GMAIL_CONNECTION_SPEC");
                                    Integer valueOf4 = Integer.valueOf((int) COUNTRY_CODE_UZBEKISTAN);
                                    Intrinsics.checkNotNullExpressionValue(connectionSpec4, "GMAIL_CONNECTION_SPEC");
                                    Integer valueOf5 = Integer.valueOf((int) COUNTRY_CODE_UKRAINE);
                                    Intrinsics.checkNotNullExpressionValue(connectionSpec4, "GMAIL_CONNECTION_SPEC");
                                    this.censorshipConfiguration = MapsKt__MapsKt.mapOf(TuplesKt.to(20, buildGConfiguration(CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new HostConfig("https://www.google.com.eg", G_HOST, connectionSpec4)), (Iterable) this.baseGHostConfigs))), TuplesKt.to(valueOf, buildGConfiguration(CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new HostConfig("https://www.google.ae", G_HOST, connectionSpec4)), (Iterable) this.baseGHostConfigs))), TuplesKt.to(valueOf2, buildGConfiguration(CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new HostConfig("https://www.google.com.om", G_HOST, connectionSpec4)), (Iterable) this.baseGHostConfigs))), TuplesKt.to(valueOf3, buildGConfiguration(CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new HostConfig("https://www.google.com.qa", G_HOST, connectionSpec4)), (Iterable) this.baseGHostConfigs))), TuplesKt.to(valueOf4, buildGConfiguration(CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new HostConfig("https://www.google.co.uz", G_HOST, connectionSpec4)), (Iterable) this.baseGHostConfigs))), TuplesKt.to(valueOf5, buildGConfiguration(CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(new HostConfig("https://www.google.com.ua", G_HOST, connectionSpec4)), (Iterable) this.baseGHostConfigs))), TuplesKt.to(98, signalServiceConfiguration), TuplesKt.to(53, signalServiceConfiguration));
                                    this.defaultCensoredConfiguration = buildGConfiguration(this.baseGHostConfigs);
                                    this.defaultCensoredCountryCodes = SetsKt__SetsKt.setOf((Object[]) new Integer[]{20, Integer.valueOf((int) COUNTRY_CODE_UAE), Integer.valueOf((int) COUNTRY_CODE_OMAN), Integer.valueOf((int) COUNTRY_CODE_QATAR), 98, 53, Integer.valueOf((int) COUNTRY_CODE_UZBEKISTAN)});
                                    this.uncensoredConfiguration = new SignalServiceConfiguration(new SignalServiceUrl[]{new SignalServiceUrl(BuildConfig.SIGNAL_URL, this.serviceTrustStore)}, MapsKt__MapsKt.mapOf(TuplesKt.to(0, new SignalCdnUrl[]{new SignalCdnUrl(BuildConfig.SIGNAL_CDN_URL, this.serviceTrustStore)}), TuplesKt.to(2, new SignalCdnUrl[]{new SignalCdnUrl(BuildConfig.SIGNAL_CDN2_URL, this.serviceTrustStore)})), new SignalContactDiscoveryUrl[]{new SignalContactDiscoveryUrl(BuildConfig.SIGNAL_CONTACT_DISCOVERY_URL, this.serviceTrustStore)}, new SignalKeyBackupServiceUrl[]{new SignalKeyBackupServiceUrl(BuildConfig.SIGNAL_KEY_BACKUP_URL, this.serviceTrustStore)}, new SignalStorageUrl[]{new SignalStorageUrl(BuildConfig.STORAGE_URL, this.serviceTrustStore)}, new SignalCdsiUrl[]{new SignalCdsiUrl(BuildConfig.SIGNAL_CDSI_URL, this.serviceTrustStore)}, this.interceptors, Optional.of(dns), SignalStore.proxy().isProxyEnabled() ? Optional.ofNullable(SignalStore.proxy().getProxy()) : Optional.empty(), this.zkGroupServerPublicParams);
                                    return;
                                }
                                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                            }
                            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                }
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    static {
        Companion companion = new Companion(null);
        Companion = companion;
        TAG = Log.tag(SignalServiceNetworkAccess.class);
        String stripProtocol = companion.stripProtocol(BuildConfig.SIGNAL_URL);
        String[] strArr = BuildConfig.SIGNAL_SERVICE_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr, "SIGNAL_SERVICE_IPS");
        String stripProtocol2 = companion.stripProtocol(BuildConfig.STORAGE_URL);
        String[] strArr2 = BuildConfig.SIGNAL_STORAGE_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr2, "SIGNAL_STORAGE_IPS");
        String stripProtocol3 = companion.stripProtocol(BuildConfig.SIGNAL_CDN_URL);
        String[] strArr3 = BuildConfig.SIGNAL_CDN_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr3, "SIGNAL_CDN_IPS");
        String stripProtocol4 = companion.stripProtocol(BuildConfig.SIGNAL_CDN2_URL);
        String[] strArr4 = BuildConfig.SIGNAL_CDN2_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr4, "SIGNAL_CDN2_IPS");
        String stripProtocol5 = companion.stripProtocol(BuildConfig.SIGNAL_CONTACT_DISCOVERY_URL);
        String[] strArr5 = BuildConfig.SIGNAL_CDS_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr5, "SIGNAL_CDS_IPS");
        String stripProtocol6 = companion.stripProtocol(BuildConfig.SIGNAL_KEY_BACKUP_URL);
        String[] strArr6 = BuildConfig.SIGNAL_KBS_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr6, "SIGNAL_KBS_IPS");
        String stripProtocol7 = companion.stripProtocol(BuildConfig.SIGNAL_SFU_URL);
        String[] strArr7 = BuildConfig.SIGNAL_SFU_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr7, "SIGNAL_SFU_IPS");
        String stripProtocol8 = companion.stripProtocol(BuildConfig.CONTENT_PROXY_HOST);
        String[] strArr8 = BuildConfig.SIGNAL_CONTENT_PROXY_IPS;
        Intrinsics.checkNotNullExpressionValue(strArr8, "SIGNAL_CONTENT_PROXY_IPS");
        DNS = new SequentialDns(Dns.SYSTEM, new CustomDns("1.1.1.1"), new StaticDns(MapsKt__MapsKt.mapOf(TuplesKt.to(stripProtocol, ArraysKt___ArraysKt.toSet(strArr)), TuplesKt.to(stripProtocol2, ArraysKt___ArraysKt.toSet(strArr2)), TuplesKt.to(stripProtocol3, ArraysKt___ArraysKt.toSet(strArr3)), TuplesKt.to(stripProtocol4, ArraysKt___ArraysKt.toSet(strArr4)), TuplesKt.to(stripProtocol5, ArraysKt___ArraysKt.toSet(strArr5)), TuplesKt.to(stripProtocol6, ArraysKt___ArraysKt.toSet(strArr6)), TuplesKt.to(stripProtocol7, ArraysKt___ArraysKt.toSet(strArr7)), TuplesKt.to(stripProtocol8, ArraysKt___ArraysKt.toSet(strArr8)))));
        ConnectionSpec connectionSpec = ConnectionSpec.MODERN_TLS;
        ConnectionSpec.Builder builder = new ConnectionSpec.Builder(connectionSpec);
        TlsVersion tlsVersion = TlsVersion.TLS_1_2;
        ConnectionSpec.Builder tlsVersions = builder.tlsVersions(tlsVersion);
        CipherSuite cipherSuite = CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256;
        CipherSuite cipherSuite2 = CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256;
        CipherSuite cipherSuite3 = CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA;
        CipherSuite cipherSuite4 = CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA;
        CipherSuite cipherSuite5 = CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA;
        CipherSuite cipherSuite6 = CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA;
        CipherSuite cipherSuite7 = CipherSuite.TLS_RSA_WITH_AES_128_GCM_SHA256;
        CipherSuite cipherSuite8 = CipherSuite.TLS_RSA_WITH_AES_128_CBC_SHA;
        CipherSuite cipherSuite9 = CipherSuite.TLS_RSA_WITH_AES_256_CBC_SHA;
        GMAPS_CONNECTION_SPEC = tlsVersions.cipherSuites(CipherSuite.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256, cipherSuite, CipherSuite.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384, CipherSuite.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256, cipherSuite2, CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384, cipherSuite3, cipherSuite4, cipherSuite5, cipherSuite6, cipherSuite7, CipherSuite.TLS_RSA_WITH_AES_256_GCM_SHA384, cipherSuite8, cipherSuite9).supportsTlsExtensions(true).build();
        GMAIL_CONNECTION_SPEC = new ConnectionSpec.Builder(connectionSpec).tlsVersions(tlsVersion).cipherSuites(cipherSuite, cipherSuite2, cipherSuite4, cipherSuite3, cipherSuite5, cipherSuite6, cipherSuite7, cipherSuite8, cipherSuite9).supportsTlsExtensions(true).build();
        PLAY_CONNECTION_SPEC = new ConnectionSpec.Builder(connectionSpec).tlsVersions(tlsVersion).cipherSuites(cipherSuite, cipherSuite2, cipherSuite4, cipherSuite3, cipherSuite5, cipherSuite6, cipherSuite7, cipherSuite8, cipherSuite9).supportsTlsExtensions(true).build();
        APP_CONNECTION_SPEC = connectionSpec;
    }

    public final SignalServiceConfiguration getUncensoredConfiguration() {
        return this.uncensoredConfiguration;
    }

    public final SignalServiceConfiguration getConfiguration() {
        return getConfiguration(SignalStore.account().getE164());
    }

    public final SignalServiceConfiguration getConfiguration(String str) {
        if (str == null || SignalStore.proxy().isProxyEnabled()) {
            return this.uncensoredConfiguration;
        }
        int localCountryCode = PhoneNumberFormatter.getLocalCountryCode();
        int i = WhenMappings.$EnumSwitchMapping$0[SignalStore.settings().getCensorshipCircumventionEnabled().ordinal()];
        if (i == 1) {
            SignalServiceConfiguration signalServiceConfiguration = this.censorshipConfiguration.get(Integer.valueOf(localCountryCode));
            return signalServiceConfiguration == null ? this.defaultCensoredConfiguration : signalServiceConfiguration;
        } else if (i == 2) {
            return this.uncensoredConfiguration;
        } else {
            if (i != 3) {
                throw new NoWhenBranchMatchedException();
            } else if (!this.defaultCensoredCountryCodes.contains(Integer.valueOf(localCountryCode))) {
                return this.uncensoredConfiguration;
            } else {
                SignalServiceConfiguration signalServiceConfiguration2 = this.censorshipConfiguration.get(Integer.valueOf(localCountryCode));
                if (signalServiceConfiguration2 == null) {
                    return this.defaultCensoredConfiguration;
                }
                return signalServiceConfiguration2;
            }
        }
    }

    public final boolean isCensored() {
        return isCensored(SignalStore.account().getE164());
    }

    public final boolean isCensored(String str) {
        return !Intrinsics.areEqual(getConfiguration(str), this.uncensoredConfiguration);
    }

    public final boolean isCountryCodeCensoredByDefault(int i) {
        return this.defaultCensoredCountryCodes.contains(Integer.valueOf(i));
    }

    /* compiled from: SignalServiceNetworkAccess.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0006HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/push/SignalServiceNetworkAccess$HostConfig;", "", "baseUrl", "", "host", "connectionSpec", "Lokhttp3/ConnectionSpec;", "(Ljava/lang/String;Ljava/lang/String;Lokhttp3/ConnectionSpec;)V", "getBaseUrl", "()Ljava/lang/String;", "getConnectionSpec", "()Lokhttp3/ConnectionSpec;", "getHost", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class HostConfig {
        private final String baseUrl;
        private final ConnectionSpec connectionSpec;
        private final String host;

        public static /* synthetic */ HostConfig copy$default(HostConfig hostConfig, String str, String str2, ConnectionSpec connectionSpec, int i, Object obj) {
            if ((i & 1) != 0) {
                str = hostConfig.baseUrl;
            }
            if ((i & 2) != 0) {
                str2 = hostConfig.host;
            }
            if ((i & 4) != 0) {
                connectionSpec = hostConfig.connectionSpec;
            }
            return hostConfig.copy(str, str2, connectionSpec);
        }

        public final String component1() {
            return this.baseUrl;
        }

        public final String component2() {
            return this.host;
        }

        public final ConnectionSpec component3() {
            return this.connectionSpec;
        }

        public final HostConfig copy(String str, String str2, ConnectionSpec connectionSpec) {
            Intrinsics.checkNotNullParameter(str, "baseUrl");
            Intrinsics.checkNotNullParameter(str2, "host");
            Intrinsics.checkNotNullParameter(connectionSpec, "connectionSpec");
            return new HostConfig(str, str2, connectionSpec);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof HostConfig)) {
                return false;
            }
            HostConfig hostConfig = (HostConfig) obj;
            return Intrinsics.areEqual(this.baseUrl, hostConfig.baseUrl) && Intrinsics.areEqual(this.host, hostConfig.host) && Intrinsics.areEqual(this.connectionSpec, hostConfig.connectionSpec);
        }

        public int hashCode() {
            return (((this.baseUrl.hashCode() * 31) + this.host.hashCode()) * 31) + this.connectionSpec.hashCode();
        }

        public String toString() {
            return "HostConfig(baseUrl=" + this.baseUrl + ", host=" + this.host + ", connectionSpec=" + this.connectionSpec + ')';
        }

        public HostConfig(String str, String str2, ConnectionSpec connectionSpec) {
            Intrinsics.checkNotNullParameter(str, "baseUrl");
            Intrinsics.checkNotNullParameter(str2, "host");
            Intrinsics.checkNotNullParameter(connectionSpec, "connectionSpec");
            this.baseUrl = str;
            this.host = str2;
            this.connectionSpec = connectionSpec;
        }

        public final String getBaseUrl() {
            return this.baseUrl;
        }

        public final ConnectionSpec getConnectionSpec() {
            return this.connectionSpec;
        }

        public final String getHost() {
            return this.host;
        }
    }

    private final SignalServiceConfiguration buildGConfiguration(List<HostConfig> list) {
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (HostConfig hostConfig : list) {
            arrayList.add(new SignalServiceUrl(hostConfig.getBaseUrl() + "/service", hostConfig.getHost(), this.gTrustStore, hostConfig.getConnectionSpec()));
        }
        Object[] array = arrayList.toArray(new SignalServiceUrl[0]);
        if (array != null) {
            SignalServiceUrl[] signalServiceUrlArr = (SignalServiceUrl[]) array;
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (HostConfig hostConfig2 : list) {
                arrayList2.add(new SignalCdnUrl(hostConfig2.getBaseUrl() + "/cdn", hostConfig2.getHost(), this.gTrustStore, hostConfig2.getConnectionSpec()));
            }
            Object[] array2 = arrayList2.toArray(new SignalCdnUrl[0]);
            if (array2 != null) {
                SignalCdnUrl[] signalCdnUrlArr = (SignalCdnUrl[]) array2;
                ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                for (HostConfig hostConfig3 : list) {
                    arrayList3.add(new SignalCdnUrl(hostConfig3.getBaseUrl() + "/cdn2", hostConfig3.getHost(), this.gTrustStore, hostConfig3.getConnectionSpec()));
                }
                Object[] array3 = arrayList3.toArray(new SignalCdnUrl[0]);
                if (array3 != null) {
                    SignalCdnUrl[] signalCdnUrlArr2 = (SignalCdnUrl[]) array3;
                    ArrayList arrayList4 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                    for (HostConfig hostConfig4 : list) {
                        arrayList4.add(new SignalContactDiscoveryUrl(hostConfig4.getBaseUrl() + "/directory", hostConfig4.getHost(), this.gTrustStore, hostConfig4.getConnectionSpec()));
                    }
                    Object[] array4 = arrayList4.toArray(new SignalContactDiscoveryUrl[0]);
                    if (array4 != null) {
                        SignalContactDiscoveryUrl[] signalContactDiscoveryUrlArr = (SignalContactDiscoveryUrl[]) array4;
                        ArrayList arrayList5 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                        for (HostConfig hostConfig5 : list) {
                            arrayList5.add(new SignalKeyBackupServiceUrl(hostConfig5.getBaseUrl() + "/backup", hostConfig5.getHost(), this.gTrustStore, hostConfig5.getConnectionSpec()));
                        }
                        Object[] array5 = arrayList5.toArray(new SignalKeyBackupServiceUrl[0]);
                        if (array5 != null) {
                            SignalKeyBackupServiceUrl[] signalKeyBackupServiceUrlArr = (SignalKeyBackupServiceUrl[]) array5;
                            ArrayList arrayList6 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
                            for (HostConfig hostConfig6 : list) {
                                arrayList6.add(new SignalStorageUrl(hostConfig6.getBaseUrl() + "/storage", hostConfig6.getHost(), this.gTrustStore, hostConfig6.getConnectionSpec()));
                            }
                            Object[] array6 = arrayList6.toArray(new SignalStorageUrl[0]);
                            if (array6 != null) {
                                SignalStorageUrl[] signalStorageUrlArr = (SignalStorageUrl[]) array6;
                                Object[] array7 = CollectionsKt__CollectionsJVMKt.listOf(new SignalCdsiUrl(BuildConfig.SIGNAL_CDSI_URL, this.serviceTrustStore)).toArray(new SignalCdsiUrl[0]);
                                if (array7 != null) {
                                    return new SignalServiceConfiguration(signalServiceUrlArr, MapsKt__MapsKt.mapOf(TuplesKt.to(0, signalCdnUrlArr), TuplesKt.to(2, signalCdnUrlArr2)), signalContactDiscoveryUrlArr, signalKeyBackupServiceUrlArr, signalStorageUrlArr, (SignalCdsiUrl[]) array7, this.interceptors, Optional.of(DNS), Optional.empty(), this.zkGroupServerPublicParams);
                                }
                                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                            }
                            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                }
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }
}
