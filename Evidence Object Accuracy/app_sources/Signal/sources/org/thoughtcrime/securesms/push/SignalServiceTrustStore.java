package org.thoughtcrime.securesms.push;

import android.content.Context;
import java.io.InputStream;
import org.thoughtcrime.securesms.R;
import org.whispersystems.signalservice.api.push.TrustStore;

/* loaded from: classes4.dex */
public class SignalServiceTrustStore implements TrustStore {
    private final Context context;

    @Override // org.whispersystems.signalservice.api.push.TrustStore
    public String getKeyStorePassword() {
        return "whisper";
    }

    public SignalServiceTrustStore(Context context) {
        this.context = context.getApplicationContext();
    }

    @Override // org.whispersystems.signalservice.api.push.TrustStore
    public InputStream getKeyStoreInputStream() {
        return this.context.getResources().openRawResource(R.raw.whisper);
    }
}
