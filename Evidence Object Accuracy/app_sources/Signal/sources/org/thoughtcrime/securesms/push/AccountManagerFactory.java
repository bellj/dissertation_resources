package org.thoughtcrime.securesms.push;

import android.content.Context;
import com.google.android.gms.security.ProviderInstaller;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;

/* loaded from: classes4.dex */
public class AccountManagerFactory {
    private static final String TAG = Log.tag(AccountManagerFactory.class);

    public static SignalServiceAccountManager createAuthenticated(Context context, ACI aci, PNI pni, String str, int i, String str2) {
        if (ApplicationDependencies.getSignalServiceNetworkAccess().isCensored(str)) {
            SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.push.AccountManagerFactory$$ExternalSyntheticLambda1
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AccountManagerFactory.$r8$lambda$ob49L5DR1rPICaydN_ac_Th011Q(this.f$0);
                }
            });
        }
        return new SignalServiceAccountManager(ApplicationDependencies.getSignalServiceNetworkAccess().getConfiguration(str), aci, pni, str, i, str2, BuildConfig.SIGNAL_AGENT, FeatureFlags.okHttpAutomaticRetry(), FeatureFlags.groupLimits().getHardLimit());
    }

    public static /* synthetic */ void lambda$createAuthenticated$0(Context context) {
        try {
            ProviderInstaller.installIfNeeded(context);
        } catch (Throwable th) {
            Log.w(TAG, th);
        }
    }

    public static SignalServiceAccountManager createUnauthenticated(Context context, String str, int i, String str2) {
        if (new SignalServiceNetworkAccess(context).isCensored(str)) {
            SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.push.AccountManagerFactory$$ExternalSyntheticLambda0
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AccountManagerFactory.m2504$r8$lambda$FwiSsdYR4mrsSu7r2ekArlUyGI(this.f$0);
                }
            });
        }
        return new SignalServiceAccountManager(new SignalServiceNetworkAccess(context).getConfiguration(str), null, null, str, i, str2, BuildConfig.SIGNAL_AGENT, FeatureFlags.okHttpAutomaticRetry(), FeatureFlags.groupLimits().getHardLimit());
    }

    public static /* synthetic */ void lambda$createUnauthenticated$1(Context context) {
        try {
            ProviderInstaller.installIfNeeded(context);
        } catch (Throwable th) {
            Log.w(TAG, th);
        }
    }
}
