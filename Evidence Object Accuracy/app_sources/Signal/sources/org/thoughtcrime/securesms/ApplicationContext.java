package org.thoughtcrime.securesms;

import android.content.Context;
import android.os.Build;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.multidex.MultiDexApplication;
import com.google.android.gms.security.ProviderInstaller;
import io.reactivex.rxjava3.core.Scheduler;
import io.reactivex.rxjava3.exceptions.OnErrorNotImplementedException;
import io.reactivex.rxjava3.exceptions.UndeliverableException;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Supplier;
import io.reactivex.rxjava3.plugins.RxJavaPlugins;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.lang.Thread;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.Security;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.conscrypt.Conscrypt;
import org.greenrobot.eventbus.EventBus;
import org.signal.aesgcmprovider.AesGcmProvider;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.AndroidLogger;
import org.signal.core.util.logging.Log;
import org.signal.core.util.tracing.Tracer;
import org.signal.glide.Log$Provider;
import org.signal.glide.SignalGlideCodecs;
import org.signal.libsignal.protocol.logging.SignalProtocolLoggerProvider;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.avatar.AvatarPickerStorage;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.crypto.DatabaseSecretProvider;
import org.thoughtcrime.securesms.database.LogDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SqlCipherLibraryLoader;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencyProvider;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.emoji.JumboEmoji;
import org.thoughtcrime.securesms.gcm.FcmJobService;
import org.thoughtcrime.securesms.jobs.CheckServiceReachabilityJob;
import org.thoughtcrime.securesms.jobs.CreateSignedPreKeyJob;
import org.thoughtcrime.securesms.jobs.DownloadLatestEmojiDataJob;
import org.thoughtcrime.securesms.jobs.EmojiSearchIndexDownloadJob;
import org.thoughtcrime.securesms.jobs.FcmRefreshJob;
import org.thoughtcrime.securesms.jobs.FontDownloaderJob;
import org.thoughtcrime.securesms.jobs.GroupV2UpdateSelfProfileKeyJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.jobs.PushNotificationReceiveJob;
import org.thoughtcrime.securesms.jobs.RefreshPreKeysJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.jobs.RetrieveRemoteAnnouncementsJob;
import org.thoughtcrime.securesms.jobs.StoryOnboardingDownloadJob;
import org.thoughtcrime.securesms.jobs.SubscriptionKeepAliveJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logging.CustomSignalProtocolLogger;
import org.thoughtcrime.securesms.logging.PersistentLogger;
import org.thoughtcrime.securesms.messageprocessingalarm.MessageProcessReceiver;
import org.thoughtcrime.securesms.migrations.ApplicationMigrations;
import org.thoughtcrime.securesms.mms.SignalGlideComponents;
import org.thoughtcrime.securesms.mms.SignalGlideModule;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.ratelimit.RateLimitUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.registration.RegistrationUtil;
import org.thoughtcrime.securesms.ringrtc.RingRtcLogger;
import org.thoughtcrime.securesms.service.DirectoryRefreshListener;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.service.LocalBackupListener;
import org.thoughtcrime.securesms.service.RotateSenderCertificateListener;
import org.thoughtcrime.securesms.service.RotateSignedPreKeyListener;
import org.thoughtcrime.securesms.service.UpdateApkRefreshListener;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.AppForegroundObserver;
import org.thoughtcrime.securesms.util.AppStartup;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.SignalUncaughtExceptionHandler;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.VersionTracker;
import org.thoughtcrime.securesms.util.dynamiclanguage.DynamicLanguageContextWrapper;
import rxdogtag2.RxDogTag;

/* loaded from: classes.dex */
public class ApplicationContext extends MultiDexApplication implements AppForegroundObserver.Listener {
    private static final String TAG = Log.tag(ApplicationContext.class);
    private PersistentLogger persistentLogger;

    public static ApplicationContext getInstance(Context context) {
        return (ApplicationContext) context.getApplicationContext();
    }

    @Override // android.app.Application
    public void onCreate() {
        Tracer.getInstance().start("Application#onCreate()");
        AppStartup.getInstance().onApplicationCreate();
        SignalLocalMetrics.ColdStart.start();
        long currentTimeMillis = System.currentTimeMillis();
        if (FeatureFlags.internalUser()) {
            Tracer.getInstance().setMaxBufferSize(35000);
        }
        super.onCreate();
        AppStartup addPostRender = AppStartup.getInstance().addBlocking("security-provider", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m226$r8$lambda$Scq3ib706ZVzWqOAVxKHsk0ZoU(ApplicationContext.this);
            }
        }).addBlocking("sqlcipher-init", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda11
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m221$r8$lambda$FUG4OX_0WYia1ROK536avz_AeY(ApplicationContext.this);
            }
        }).addBlocking("logging", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda22
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$B9YbTGILmyFPaoBXOmNL6jOXsfE(ApplicationContext.this);
            }
        }).addBlocking("crash-handling", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda33
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$NVlmwOJyR5FD_DHoV8_J17Ia52c(ApplicationContext.this);
            }
        }).addBlocking("rx-init", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda44
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$rcTJiLjsUxlIZkDmzTMiXZsegjE(ApplicationContext.this);
            }
        }).addBlocking("event-bus", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda45
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$QTEjfsRccjaeTw0od8y5UGofLnw();
            }
        }).addBlocking("app-dependencies", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda46
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$qvh2Zoy_X6Q3iKBFfLxaDqc7Ezk(ApplicationContext.this);
            }
        }).addBlocking("notification-channels", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda47
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m232$r8$lambda$hmtEgWKgtJqnegkmcKGyW9S4lE(ApplicationContext.this);
            }
        }).addBlocking("first-launch", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda48
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$yQRCXGQDtNjbeugn3EXipLHH9FU(ApplicationContext.this);
            }
        }).addBlocking("app-migrations", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda49
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$gKE1IGqAoyc13fxTm6l3gJlrjAY(ApplicationContext.this);
            }
        }).addBlocking("ring-rtc", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$QaXC_yGYxWOey7BnguyExwrBRyw(ApplicationContext.this);
            }
        }).addBlocking("mark-registration", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$XFCWFblt7YUuZhHxhGYWaSgtLK4(ApplicationContext.this);
            }
        }).addBlocking("lifecycle-observer", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$o9M3Go_qjQ3txidH450LqHZ_4xI(ApplicationContext.this);
            }
        }).addBlocking("message-retriever", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.this.initializeMessageRetrieval();
            }
        }).addBlocking("dynamic-theme", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$iarTlmbogKr8dV9CUxT7EuUjjw4(ApplicationContext.this);
            }
        }).addBlocking("vector-compat", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda6
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m228$r8$lambda$cQnTp954Na4k9YavnTCgv5QUVQ();
            }
        }).addBlocking("proxy-init", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$idYYlPtuECvmRBdMPMIWbD5a3kU();
            }
        }).addBlocking("blob-provider", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda8
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m230$r8$lambda$fadH5ka8PbgB4jUw8I3Z_HPDRI(ApplicationContext.this);
            }
        }).addBlocking("feature-flags", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda9
            @Override // java.lang.Runnable
            public final void run() {
                FeatureFlags.init();
            }
        }).addBlocking("glide", new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda10
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$j9SYVZNI3aEbrOx6jxBWYL046uc();
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda12
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$ovJzPtEGSgvdzEfbBJQ_xX3JMa4(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda13
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m222$r8$lambda$KZRSoLfokd2ZTMSgOM5BhsFgbA(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda14
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$5poXrEJk3jfcZLSg8_BUV0hYvj4(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda15
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m217$r8$lambda$2tBnsS8FZWlorJhhkCuxRVYRSc(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda16
            @Override // java.lang.Runnable
            public final void run() {
                CreateSignedPreKeyJob.enqueueIfNeeded();
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda17
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m227$r8$lambda$WFI6T_In_uKmos4g8LacfmDn_c(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda18
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m216$r8$lambda$0nB3hoiKS15ZNfJuPtUUL0olRo(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda19
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m224$r8$lambda$QlPqPSy0sRT4z93quy19Cm_YJY(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda20
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$Siio_ve2xbB778LsiRf92wQaPrQ(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda21
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m219$r8$lambda$AR9LJ8q2aMajmfmRrECo7SJOl0(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda23
            @Override // java.lang.Runnable
            public final void run() {
                RefreshPreKeysJob.scheduleIfNecessary();
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda24
            @Override // java.lang.Runnable
            public final void run() {
                StorageSyncHelper.scheduleRoutineSync();
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda25
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$cg57OvxqUbhM0WGhH7ksLWOJYSw();
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda26
            @Override // java.lang.Runnable
            public final void run() {
                EmojiSource.refresh();
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda27
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$3VWVqr6SC_sFhv7gd_5DN2x2hwU(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda28
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$aqlwcePJYXBRACew6McVRGPWjgY(ApplicationContext.this);
            }
        }).addNonBlocking(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda29
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m218$r8$lambda$6C1lVL7dG_wAqmFY6Az_Qc88wU();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda30
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$qevxw1qFwLGXj9OcUX_KD3ckSw0(ApplicationContext.this);
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda31
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$N6AFqhOafdHeCKhjkTVk0zrFQFI(ApplicationContext.this);
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda32
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$MmwA9B3F_jF9sNdbk0mDvWIwKV0(ApplicationContext.this);
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda34
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m231$r8$lambda$hJfkj5QdkzUdZNqHj3zo7eJY4Q(ApplicationContext.this);
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda35
            @Override // java.lang.Runnable
            public final void run() {
                EmojiSearchIndexDownloadJob.scheduleIfNecessary();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda36
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$d1IiCBeJCM6wI7G1fz_gXkhXmmY();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda37
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$gaqvDgvvLmLInSMPxOieQ3YthkU(ApplicationContext.this);
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda38
            @Override // java.lang.Runnable
            public final void run() {
                RetrieveRemoteAnnouncementsJob.enqueue();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda39
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$XEBpgM6g0m715OxGVQde5BV4obs();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda40
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m225$r8$lambda$RIVst1BPIoPfHpYnrmNZ6n5IC4();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda41
            @Override // java.lang.Runnable
            public final void run() {
                CheckServiceReachabilityJob.enqueueIfNecessary();
            }
        }).addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda42
            @Override // java.lang.Runnable
            public final void run() {
                GroupV2UpdateSelfProfileKeyJob.enqueueForGroupsIfNecessary();
            }
        });
        StoryOnboardingDownloadJob.Companion companion = StoryOnboardingDownloadJob.Companion;
        Objects.requireNonNull(companion);
        addPostRender.addPostRender(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda43
            @Override // java.lang.Runnable
            public final void run() {
                StoryOnboardingDownloadJob.Companion.this.enqueueIfNeeded();
            }
        }).execute();
        String str = TAG;
        Log.d(str, "onCreate() took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        SignalLocalMetrics.ColdStart.onApplicationCreateFinished();
        Tracer.getInstance().end("Application#onCreate()");
    }

    public /* synthetic */ void lambda$onCreate$0() {
        SqlCipherLibraryLoader.load();
        SignalDatabase.init(this, DatabaseSecretProvider.getOrCreateDatabaseSecret(this), AttachmentSecretProvider.getInstance(this).getOrCreateAttachmentSecret());
    }

    public /* synthetic */ void lambda$onCreate$1() {
        initializeLogging();
        Log.i(TAG, "onCreate()");
    }

    public static /* synthetic */ void lambda$onCreate$2() {
        EventBus.builder().logNoSubscriberMessages(false).installDefaultEventBus();
    }

    public /* synthetic */ void lambda$onCreate$3() {
        NotificationChannels.create(this);
    }

    public /* synthetic */ void lambda$onCreate$4() {
        RegistrationUtil.maybeMarkRegistrationComplete(this);
    }

    public /* synthetic */ void lambda$onCreate$5() {
        ApplicationDependencies.getAppForegroundObserver().addListener(this);
    }

    public /* synthetic */ void lambda$onCreate$6() {
        DynamicTheme.setDefaultDayNightMode(this);
    }

    public static /* synthetic */ void lambda$onCreate$7() {
        if (Build.VERSION.SDK_INT < 21) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }

    public static /* synthetic */ void lambda$onCreate$8() {
        if (SignalStore.proxy().isProxyEnabled()) {
            Log.w(TAG, "Proxy detected. Enabling Conscrypt.setUseEngineSocketByDefault()");
            Conscrypt.setUseEngineSocketByDefault(true);
        }
    }

    public static /* synthetic */ void lambda$onCreate$9() {
        SignalGlideModule.setRegisterGlideComponents(new SignalGlideComponents());
    }

    public static /* synthetic */ void lambda$onCreate$10() {
        ApplicationDependencies.getJobManager().beginJobLoop();
    }

    public /* synthetic */ void lambda$onCreate$11() {
        ApplicationDependencies.getGiphyMp4Cache().onAppStart(this);
    }

    public static /* synthetic */ void lambda$onCreate$12() {
        ApplicationDependencies.getExpireStoriesManager().scheduleIfNecessary();
    }

    public /* synthetic */ void lambda$onCreate$13() {
        RateLimitUtil.retryAllRateLimitedMessages(this);
    }

    public /* synthetic */ void lambda$onCreate$14() {
        SignalStore.settings().setDefaultSms(Util.isDefaultSmsProvider(this));
    }

    public /* synthetic */ void lambda$onCreate$15() {
        DownloadLatestEmojiDataJob.scheduleIfNecessary(this);
    }

    public static /* synthetic */ void lambda$onCreate$16() {
        SignalDatabase.messageLog().trimOldMessages(System.currentTimeMillis(), FeatureFlags.retryRespondMaxAge());
    }

    public /* synthetic */ void lambda$onCreate$17() {
        JumboEmoji.updateCurrentVersion(this);
    }

    public static /* synthetic */ void lambda$onCreate$19() {
        ApplicationDependencies.getJobManager().add(new FontDownloaderJob());
    }

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public void onForeground() {
        long currentTimeMillis = System.currentTimeMillis();
        String str = TAG;
        Log.i(str, "App is now visible.");
        ApplicationDependencies.getFrameRateTracker().start();
        ApplicationDependencies.getMegaphoneRepository().onAppForegrounded();
        ApplicationDependencies.getDeadlockDetector().start();
        SubscriptionKeepAliveJob.enqueueAndTrackTimeIfNecessary();
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda52
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.$r8$lambda$eJ0g4kFMHqf27eVt7l3rykHQwKE(ApplicationContext.this);
            }
        });
        Log.d(str, "onStart() took " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
    }

    public /* synthetic */ void lambda$onForeground$20() {
        FeatureFlags.refreshIfNecessary();
        ApplicationDependencies.getRecipientCache().warmUp();
        RetrieveProfileJob.enqueueRoutineFetchIfNecessary(this);
        executePendingContactSync();
        KeyCachingService.onAppForegrounded(this);
        ApplicationDependencies.getShakeToReport().enable();
        checkBuildExpiration();
    }

    @Override // org.thoughtcrime.securesms.util.AppForegroundObserver.Listener
    public void onBackground() {
        Log.i(TAG, "App is no longer visible.");
        KeyCachingService.onAppBackgrounded(this);
        ApplicationDependencies.getMessageNotifier().clearVisibleThread();
        ApplicationDependencies.getFrameRateTracker().stop();
        ApplicationDependencies.getShakeToReport().disable();
        ApplicationDependencies.getDeadlockDetector().stop();
    }

    public PersistentLogger getPersistentLogger() {
        return this.persistentLogger;
    }

    public void checkBuildExpiration() {
        if (Util.getTimeUntilBuildExpiry() <= 0 && !SignalStore.misc().isClientDeprecated()) {
            Log.w(TAG, "Build expired!");
            SignalStore.misc().markClientDeprecated();
        }
    }

    public void initializeSecurityProvider() {
        try {
            Class.forName("org.signal.aesgcmprovider.AesGcmCipher");
            int insertProviderAt = Security.insertProviderAt(new AesGcmProvider(), 1);
            String str = TAG;
            Log.i(str, "Installed AesGcmProvider: " + insertProviderAt);
            if (insertProviderAt >= 0) {
                int insertProviderAt2 = Security.insertProviderAt(Conscrypt.newProvider(), 2);
                Log.i(str, "Installed Conscrypt provider: " + insertProviderAt2);
                if (insertProviderAt2 < 0) {
                    Log.w(str, "Did not install Conscrypt provider. May already be present.");
                    return;
                }
                return;
            }
            Log.e(str, "Failed to install AesGcmProvider()");
            throw new ProviderInitializationException();
        } catch (ClassNotFoundException unused) {
            Log.e(TAG, "Failed to find AesGcmCipher class");
            throw new ProviderInitializationException();
        }
    }

    private void initializeLogging() {
        this.persistentLogger = new PersistentLogger(this);
        Log.initialize(new Log.InternalCheck() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda50
            @Override // org.signal.core.util.logging.Log.InternalCheck
            public final boolean isInternal() {
                return FeatureFlags.internalUser();
            }
        }, new AndroidLogger(), this.persistentLogger);
        SignalProtocolLoggerProvider.setProvider(new CustomSignalProtocolLogger());
        SignalExecutors.UNBOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda51
            @Override // java.lang.Runnable
            public final void run() {
                ApplicationContext.m229$r8$lambda$fTV1FNC7iTGhQeuYgV6i4vFrhQ(ApplicationContext.this);
            }
        });
    }

    public /* synthetic */ void lambda$initializeLogging$21() {
        Log.blockUntilAllWritesFinished();
        LogDatabase.getInstance(this).trimToSize();
    }

    public void initializeCrashHandling() {
        Thread.setDefaultUncaughtExceptionHandler(new SignalUncaughtExceptionHandler(Thread.getDefaultUncaughtExceptionHandler()));
    }

    public void initializeRx() {
        RxDogTag.install();
        RxJavaPlugins.setInitIoSchedulerHandler(new Function() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda53
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ApplicationContext.m233$r8$lambda$sZkZdXUCZmytF5ZrAeZBo312VY((Supplier) obj);
            }
        });
        RxJavaPlugins.setInitComputationSchedulerHandler(new Function() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda54
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ApplicationContext.m223$r8$lambda$PknWdW5WO4zwWsWXF94cAh5G_k((Supplier) obj);
            }
        });
        RxJavaPlugins.setErrorHandler(new Consumer() { // from class: org.thoughtcrime.securesms.ApplicationContext$$ExternalSyntheticLambda55
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ApplicationContext.m220$r8$lambda$C78IQaABIFJnzng8XB8BhSQBOM((Throwable) obj);
            }
        });
    }

    public static /* synthetic */ Scheduler lambda$initializeRx$22(Supplier supplier) throws Throwable {
        return Schedulers.from(SignalExecutors.BOUNDED_IO, true, false);
    }

    public static /* synthetic */ Scheduler lambda$initializeRx$23(Supplier supplier) throws Throwable {
        return Schedulers.from(SignalExecutors.BOUNDED, true, false);
    }

    public static /* synthetic */ void lambda$initializeRx$24(Throwable th) throws Throwable {
        boolean z = false;
        while (true) {
            if (((th instanceof UndeliverableException) || (th instanceof AssertionError) || (th instanceof OnErrorNotImplementedException)) && th.getCause() != null) {
                z = true;
                th = th.getCause();
            }
        }
        if (!z || (!(th instanceof SocketException) && !(th instanceof SocketTimeoutException) && !(th instanceof InterruptedException))) {
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = Thread.currentThread().getUncaughtExceptionHandler();
            if (uncaughtExceptionHandler == null) {
                uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
            }
            uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), th);
        }
    }

    public void initializeApplicationMigrations() {
        ApplicationMigrations.onApplicationCreate(this, ApplicationDependencies.getJobManager());
    }

    public void initializeMessageRetrieval() {
        ApplicationDependencies.getIncomingMessageObserver();
    }

    public void initializeAppDependencies() {
        ApplicationDependencies.init(this, new ApplicationDependencyProvider(this));
    }

    public void initializeFirstEverAppLaunch() {
        if (TextSecurePreferences.getFirstInstallVersion(this) == -1) {
            if (!SignalDatabase.databaseFileExists(this) || VersionTracker.getDaysSinceFirstInstalled(this) < 365) {
                Log.i(TAG, "First ever app launch!");
                AppInitialization.onFirstEverAppLaunch(this);
            }
            Log.i(TAG, "Setting first install version to 1097");
            TextSecurePreferences.setFirstInstallVersion(this, BuildConfig.CANONICAL_VERSION_CODE);
        } else if (!TextSecurePreferences.isPasswordDisabled(this) && VersionTracker.getDaysSinceFirstInstalled(this) < 90) {
            Log.i(TAG, "Detected a new install that doesn't have passphrases disabled -- assuming bad initialization.");
            AppInitialization.onRepairFirstEverAppLaunch(this);
        } else if (!TextSecurePreferences.isPasswordDisabled(this) && VersionTracker.getDaysSinceFirstInstalled(this) < 912) {
            Log.i(TAG, "Detected a not-recent install that doesn't have passphrases disabled -- disabling now.");
            TextSecurePreferences.setPasswordDisabled(this, true);
        }
    }

    public void initializeFcmCheck() {
        if (SignalStore.account().isRegistered()) {
            long fcmTokenLastSetTime = SignalStore.account().getFcmTokenLastSetTime() + TimeUnit.HOURS.toMillis(6);
            if (SignalStore.account().getFcmToken() == null || fcmTokenLastSetTime <= System.currentTimeMillis()) {
                ApplicationDependencies.getJobManager().add(new FcmRefreshJob());
            }
        }
    }

    public void initializeExpiringMessageManager() {
        ApplicationDependencies.getExpiringMessageManager().checkSchedule();
    }

    public void initializeRevealableMessageManager() {
        ApplicationDependencies.getViewOnceMessageManager().scheduleIfNecessary();
    }

    public void initializePendingRetryReceiptManager() {
        ApplicationDependencies.getPendingRetryReceiptManager().scheduleIfNecessary();
    }

    public void initializePeriodicTasks() {
        RotateSignedPreKeyListener.schedule(this);
        DirectoryRefreshListener.schedule(this);
        LocalBackupListener.schedule(this);
        RotateSenderCertificateListener.schedule(this);
        MessageProcessReceiver.startOrUpdateAlarm(this);
        UpdateApkRefreshListener.schedule(this);
    }

    public void initializeRingRtc() {
        try {
            CallManager.initialize(this, new RingRtcLogger());
        } catch (UnsatisfiedLinkError e) {
            throw new AssertionError("Unable to load ringrtc library", e);
        }
    }

    public void initializeCircumvention() {
        if (ApplicationDependencies.getSignalServiceNetworkAccess().isCensored()) {
            try {
                ProviderInstaller.installIfNeeded(this);
            } catch (Throwable th) {
                Log.w(TAG, th);
            }
        }
    }

    public void ensureProfileUploaded() {
        if (SignalStore.account().isRegistered() && !SignalStore.registrationValues().hasUploadedProfile() && !Recipient.self().getProfileName().isEmpty()) {
            Log.w(TAG, "User has a profile, but has not uploaded one. Uploading now.");
            ApplicationDependencies.getJobManager().add(new ProfileUploadJob());
        }
    }

    private void executePendingContactSync() {
        if (TextSecurePreferences.needsFullContactSync(this)) {
            ApplicationDependencies.getJobManager().add(new MultiDeviceContactUpdateJob(true));
        }
    }

    public void initializePendingMessages() {
        if (TextSecurePreferences.getNeedsMessagePull(this)) {
            Log.i(TAG, "Scheduling a message fetch.");
            if (Build.VERSION.SDK_INT >= 26) {
                FcmJobService.schedule(this);
            } else {
                ApplicationDependencies.getJobManager().add(new PushNotificationReceiveJob());
            }
            TextSecurePreferences.setNeedsMessagePull(this, false);
        }
    }

    public void initializeBlobProvider() {
        BlobProvider.getInstance().initialize(this);
    }

    public void cleanAvatarStorage() {
        AvatarPickerStorage.cleanOrphans(this);
    }

    public void initializeCleanup() {
        int deleteAbandonedPreuploadedAttachments = SignalDatabase.attachments().deleteAbandonedPreuploadedAttachments();
        String str = TAG;
        Log.i(str, "Deleted " + deleteAbandonedPreuploadedAttachments + " abandoned attachments.");
    }

    public void initializeGlideCodecs() {
        SignalGlideCodecs.setLogProvider(new Log$Provider() { // from class: org.thoughtcrime.securesms.ApplicationContext.1
            public void v(String str, String str2) {
                Log.v(str, str2);
            }

            public void d(String str, String str2) {
                Log.d(str, str2);
            }

            public void i(String str, String str2) {
                Log.i(str, str2);
            }

            public void w(String str, String str2) {
                Log.w(str, str2);
            }

            public void e(String str, String str2, Throwable th) {
                Log.e(str, str2, th);
            }
        });
    }

    @Override // androidx.multidex.MultiDexApplication, android.content.ContextWrapper
    public void attachBaseContext(Context context) {
        DynamicLanguageContextWrapper.updateContext(context);
        super.attachBaseContext(context);
    }

    /* loaded from: classes.dex */
    public static class ProviderInitializationException extends RuntimeException {
        private ProviderInitializationException() {
        }
    }
}
