package org.thoughtcrime.securesms.storage;

import android.content.Context;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RetrieveProfileAvatarJob;
import org.thoughtcrime.securesms.jobs.StorageForcePushJob$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.jobs.StorageSyncJob;
import org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Entropy;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.SignalContactRecord;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.StorageId;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes4.dex */
public final class StorageSyncHelper {
    public static final StorageKeyGenerator KEY_GENERATOR;
    private static final long REFRESH_INTERVAL = TimeUnit.HOURS.toMillis(2);
    private static final String TAG = Log.tag(StorageSyncHelper.class);
    private static StorageKeyGenerator keyGenerator;

    public static /* synthetic */ StorageId lambda$findIdDifference$2(StorageId storageId) {
        return storageId;
    }

    public static /* synthetic */ StorageId lambda$findIdDifference$4(StorageId storageId) {
        return storageId;
    }

    static {
        TAG = Log.tag(StorageSyncHelper.class);
        StorageSyncHelper$$ExternalSyntheticLambda0 storageSyncHelper$$ExternalSyntheticLambda0 = new StorageKeyGenerator() { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.storage.StorageKeyGenerator
            public final byte[] generate() {
                return Util.getSecretBytes(16);
            }
        };
        KEY_GENERATOR = storageSyncHelper$$ExternalSyntheticLambda0;
        keyGenerator = storageSyncHelper$$ExternalSyntheticLambda0;
        REFRESH_INTERVAL = TimeUnit.HOURS.toMillis(2);
    }

    public static IdDifferenceResult findIdDifference(Collection<StorageId> collection, Collection<StorageId> collection2) {
        Map map = (Map) Stream.of(collection).collect(Collectors.toMap(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorageSyncHelper.lambda$findIdDifference$1((StorageId) obj);
            }
        }, new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorageSyncHelper.lambda$findIdDifference$2((StorageId) obj);
            }
        }));
        Map map2 = (Map) Stream.of(collection2).collect(Collectors.toMap(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorageSyncHelper.lambda$findIdDifference$3((StorageId) obj);
            }
        }, new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorageSyncHelper.lambda$findIdDifference$4((StorageId) obj);
            }
        }));
        boolean z = (map.size() == collection.size() && map2.size() == collection2.size()) ? false : true;
        Set difference = SetUtil.difference(map.keySet(), map2.keySet());
        Set difference2 = SetUtil.difference(map2.keySet(), map.keySet());
        for (String str : SetUtil.intersection(map2.keySet(), map.keySet())) {
            StorageId storageId = (StorageId) map.get(str);
            Objects.requireNonNull(storageId);
            StorageId storageId2 = (StorageId) map2.get(str);
            Objects.requireNonNull(storageId2);
            if (storageId.getType() != storageId2.getType()) {
                difference.remove(str);
                difference2.remove(str);
                String str2 = TAG;
                Log.w(str2, "Remote type " + storageId.getType() + " did not match local type " + storageId2.getType() + "!");
                z = true;
            }
        }
        return new IdDifferenceResult(Stream.of(difference).map(new Function(map) { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda6
            public final /* synthetic */ Map f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (StorageId) this.f$0.get((String) obj);
            }
        }).toList(), Stream.of(difference2).map(new Function(map2) { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda6
            public final /* synthetic */ Map f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (StorageId) this.f$0.get((String) obj);
            }
        }).toList(), z);
    }

    public static /* synthetic */ String lambda$findIdDifference$1(StorageId storageId) {
        return Base64.encodeBytes(storageId.getRaw());
    }

    public static /* synthetic */ String lambda$findIdDifference$3(StorageId storageId) {
        return Base64.encodeBytes(storageId.getRaw());
    }

    public static byte[] generateKey() {
        return keyGenerator.generate();
    }

    static void setTestKeyGenerator(StorageKeyGenerator storageKeyGenerator) {
        if (storageKeyGenerator == null) {
            storageKeyGenerator = KEY_GENERATOR;
        }
        keyGenerator = storageKeyGenerator;
    }

    public static boolean profileKeyChanged(StorageRecordUpdate<SignalContactRecord> storageRecordUpdate) {
        return !OptionalUtil.byteArrayEquals(storageRecordUpdate.getOld().getProfileKey(), storageRecordUpdate.getNew().getProfileKey());
    }

    public static SignalStorageRecord buildAccountRecord(Context context, Recipient recipient) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        RecipientRecord recordForSync = recipients.getRecordForSync(recipient.getId());
        List list = Stream.of(SignalDatabase.threads().getPinnedRecipientIds()).map(new StorageForcePushJob$$ExternalSyntheticLambda0(recipients)).toList();
        boolean z = true;
        SignalAccountRecord.Builder noteToSelfArchived = new SignalAccountRecord.Builder(recipient.getStorageServiceId(), recordForSync != null ? recordForSync.getSyncExtras().getStorageProto() : null).setProfileKey(recipient.getProfileKey()).setGivenName(recipient.getProfileName().getGivenName()).setFamilyName(recipient.getProfileName().getFamilyName()).setAvatarUrlPath(recipient.getProfileAvatar()).setNoteToSelfArchived(recordForSync != null && recordForSync.getSyncExtras().isArchived());
        if (recordForSync == null || !recordForSync.getSyncExtras().isForcedUnread()) {
            z = false;
        }
        return SignalStorageRecord.forAccount(noteToSelfArchived.setNoteToSelfForcedUnread(z).setTypingIndicatorsEnabled(TextSecurePreferences.isTypingIndicatorsEnabled(context)).setReadReceiptsEnabled(TextSecurePreferences.isReadReceiptsEnabled(context)).setSealedSenderIndicatorsEnabled(TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(context)).setLinkPreviewsEnabled(SignalStore.settings().isLinkPreviewsEnabled()).setUnlistedPhoneNumber(SignalStore.phoneNumberPrivacy().getPhoneNumberListingMode().isUnlisted()).setPhoneNumberSharingMode(StorageSyncModels.localToRemotePhoneNumberSharingMode(SignalStore.phoneNumberPrivacy().getPhoneNumberSharingMode())).setPinnedConversations(StorageSyncModels.localToRemotePinnedConversations(list)).setPreferContactAvatars(SignalStore.settings().isPreferSystemContactPhotos()).setPayments(SignalStore.paymentsValues().mobileCoinPaymentsEnabled(), (byte[]) Optional.ofNullable(SignalStore.paymentsValues().getPaymentsEntropy()).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncHelper$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((Entropy) obj).getBytes();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null)).setPrimarySendsSms(Util.isDefaultSmsProvider(context)).setUniversalExpireTimer(SignalStore.settings().getUniversalExpireTimer()).setE164(recipient.requireE164()).setDefaultReactions(SignalStore.emojiValues().getReactions()).setSubscriber(StorageSyncModels.localToRemoteSubscriber(SignalStore.donationsValues().getSubscriber())).setDisplayBadgesOnProfile(SignalStore.donationsValues().getDisplayBadgesOnProfile()).setSubscriptionManuallyCancelled(SignalStore.donationsValues().isUserManuallyCancelled()).build());
    }

    public static void applyAccountStorageSyncUpdates(Context context, Recipient recipient, SignalAccountRecord signalAccountRecord, boolean z) {
        applyAccountStorageSyncUpdates(context, recipient, new StorageRecordUpdate(buildAccountRecord(context, recipient).getAccount().get(), signalAccountRecord), z);
    }

    public static void applyAccountStorageSyncUpdates(Context context, Recipient recipient, StorageRecordUpdate<SignalAccountRecord> storageRecordUpdate, boolean z) {
        SignalDatabase.recipients().applyStorageSyncAccountUpdate(storageRecordUpdate);
        TextSecurePreferences.setReadReceiptsEnabled(context, storageRecordUpdate.getNew().isReadReceiptsEnabled());
        TextSecurePreferences.setTypingIndicatorsEnabled(context, storageRecordUpdate.getNew().isTypingIndicatorsEnabled());
        TextSecurePreferences.setShowUnidentifiedDeliveryIndicatorsEnabled(context, storageRecordUpdate.getNew().isSealedSenderIndicatorsEnabled());
        SignalStore.settings().setLinkPreviewsEnabled(storageRecordUpdate.getNew().isLinkPreviewsEnabled());
        SignalStore.phoneNumberPrivacy().setPhoneNumberListingMode(storageRecordUpdate.getNew().isPhoneNumberUnlisted() ? PhoneNumberPrivacyValues.PhoneNumberListingMode.UNLISTED : PhoneNumberPrivacyValues.PhoneNumberListingMode.LISTED);
        SignalStore.phoneNumberPrivacy().setPhoneNumberSharingMode(StorageSyncModels.remoteToLocalPhoneNumberSharingMode(storageRecordUpdate.getNew().getPhoneNumberSharingMode()));
        SignalStore.settings().setPreferSystemContactPhotos(storageRecordUpdate.getNew().isPreferContactAvatars());
        SignalStore.paymentsValues().setEnabledAndEntropy(storageRecordUpdate.getNew().getPayments().isEnabled(), Entropy.fromBytes(storageRecordUpdate.getNew().getPayments().getEntropy().orElse(null)));
        SignalStore.settings().setUniversalExpireTimer(storageRecordUpdate.getNew().getUniversalExpireTimer());
        SignalStore.emojiValues().setReactions(storageRecordUpdate.getNew().getDefaultReactions());
        SignalStore.donationsValues().setDisplayBadgesOnProfile(storageRecordUpdate.getNew().isDisplayBadgesOnProfile());
        if (storageRecordUpdate.getNew().isSubscriptionManuallyCancelled()) {
            SignalStore.donationsValues().updateLocalStateForManualCancellation();
        } else {
            SignalStore.donationsValues().clearUserManuallyCancelled();
        }
        Subscriber remoteToLocalSubscriber = StorageSyncModels.remoteToLocalSubscriber(storageRecordUpdate.getNew().getSubscriber());
        if (remoteToLocalSubscriber != null) {
            SignalStore.donationsValues().setSubscriber(remoteToLocalSubscriber);
        }
        if (z && storageRecordUpdate.getNew().getAvatarUrlPath().isPresent()) {
            ApplicationDependencies.getJobManager().add(new RetrieveProfileAvatarJob(recipient, storageRecordUpdate.getNew().getAvatarUrlPath().get()));
        }
    }

    public static void scheduleSyncForDataChange() {
        if (!SignalStore.registrationValues().isRegistrationComplete()) {
            Log.d(TAG, "Registration still ongoing. Ignore sync request.");
        } else {
            ApplicationDependencies.getJobManager().add(new StorageSyncJob());
        }
    }

    public static void scheduleRoutineSync() {
        long currentTimeMillis = System.currentTimeMillis() - SignalStore.storageService().getLastSyncTime();
        if (currentTimeMillis > REFRESH_INTERVAL) {
            String str = TAG;
            Log.d(str, "Scheduling a sync. Last sync was " + currentTimeMillis + " ms ago.");
            scheduleSyncForDataChange();
            return;
        }
        String str2 = TAG;
        Log.d(str2, "No need for sync. Last sync was " + currentTimeMillis + " ms ago.");
    }

    /* loaded from: classes4.dex */
    public static final class IdDifferenceResult {
        private final boolean hasTypeMismatches;
        private final List<StorageId> localOnlyIds;
        private final List<StorageId> remoteOnlyIds;

        private IdDifferenceResult(List<StorageId> list, List<StorageId> list2, boolean z) {
            this.remoteOnlyIds = list;
            this.localOnlyIds = list2;
            this.hasTypeMismatches = z;
        }

        public List<StorageId> getRemoteOnlyIds() {
            return this.remoteOnlyIds;
        }

        public List<StorageId> getLocalOnlyIds() {
            return this.localOnlyIds;
        }

        public boolean hasTypeMismatches() {
            return this.hasTypeMismatches;
        }

        public boolean isEmpty() {
            return this.remoteOnlyIds.isEmpty() && this.localOnlyIds.isEmpty();
        }

        public String toString() {
            return "remoteOnly: " + this.remoteOnlyIds.size() + ", localOnly: " + this.localOnlyIds.size() + ", hasTypeMismatches: " + this.hasTypeMismatches;
        }
    }

    /* loaded from: classes4.dex */
    public static final class WriteOperationResult {
        private final List<byte[]> deletes;
        private final List<SignalStorageRecord> inserts;
        private final SignalStorageManifest manifest;

        public WriteOperationResult(SignalStorageManifest signalStorageManifest, List<SignalStorageRecord> list, List<byte[]> list2) {
            this.manifest = signalStorageManifest;
            this.inserts = list;
            this.deletes = list2;
        }

        public SignalStorageManifest getManifest() {
            return this.manifest;
        }

        public List<SignalStorageRecord> getInserts() {
            return this.inserts;
        }

        public List<byte[]> getDeletes() {
            return this.deletes;
        }

        public boolean isEmpty() {
            return this.inserts.isEmpty() && this.deletes.isEmpty();
        }

        public String toString() {
            if (isEmpty()) {
                return "Empty";
            }
            return String.format(Locale.ENGLISH, "ManifestVersion: %d, Total Keys: %d, Inserts: %d, Deletes: %d", Long.valueOf(this.manifest.getVersion()), Integer.valueOf(this.manifest.getStorageIds().size()), Integer.valueOf(this.inserts.size()), Integer.valueOf(this.deletes.size()));
        }
    }
}
