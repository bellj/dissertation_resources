package org.thoughtcrime.securesms.storage;

import android.content.Context;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.storage.SignalGroupV1Record;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;

/* loaded from: classes4.dex */
public final class GroupV1RecordProcessor extends DefaultStorageRecordProcessor<SignalGroupV1Record> {
    private static final String TAG = Log.tag(GroupV1RecordProcessor.class);
    private final GroupDatabase groupDatabase;
    private final RecipientDatabase recipientDatabase;

    public /* bridge */ /* synthetic */ void process(Collection collection, StorageKeyGenerator storageKeyGenerator) throws IOException {
        super.process(collection, storageKeyGenerator);
    }

    public GroupV1RecordProcessor(Context context) {
        this(SignalDatabase.groups(), SignalDatabase.recipients());
    }

    GroupV1RecordProcessor(GroupDatabase groupDatabase, RecipientDatabase recipientDatabase) {
        this.groupDatabase = groupDatabase;
        this.recipientDatabase = recipientDatabase;
    }

    public boolean isInvalid(SignalGroupV1Record signalGroupV1Record) {
        try {
            if (!this.groupDatabase.getGroup(GroupId.v1(signalGroupV1Record.getGroupId()).deriveV2MigrationGroupId()).isPresent()) {
                return false;
            }
            Log.w(TAG, "We already have an upgraded V2 group for this V1 group -- marking as invalid.");
            return true;
        } catch (BadGroupIdException unused) {
            Log.w(TAG, "Bad Group ID -- marking as invalid.");
            return true;
        }
    }

    public Optional<SignalGroupV1Record> getMatching(SignalGroupV1Record signalGroupV1Record, StorageKeyGenerator storageKeyGenerator) {
        Optional<RecipientId> byGroupId = this.recipientDatabase.getByGroupId(GroupId.v1orThrow(signalGroupV1Record.getGroupId()));
        RecipientDatabase recipientDatabase = this.recipientDatabase;
        Objects.requireNonNull(recipientDatabase);
        return byGroupId.map(new ContactRecordProcessor$$ExternalSyntheticLambda0(recipientDatabase)).map(new Function() { // from class: org.thoughtcrime.securesms.storage.GroupV1RecordProcessor$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StorageSyncModels.localToRemoteRecord((RecipientRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.storage.GroupV1RecordProcessor$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GroupV1RecordProcessor.lambda$getMatching$0((SignalStorageRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public static /* synthetic */ SignalGroupV1Record lambda$getMatching$0(SignalStorageRecord signalStorageRecord) {
        return signalStorageRecord.getGroupV1().get();
    }

    public SignalGroupV1Record merge(SignalGroupV1Record signalGroupV1Record, SignalGroupV1Record signalGroupV1Record2, StorageKeyGenerator storageKeyGenerator) {
        byte[] serializeUnknownFields = signalGroupV1Record.serializeUnknownFields();
        boolean isBlocked = signalGroupV1Record.isBlocked();
        boolean isProfileSharingEnabled = signalGroupV1Record.isProfileSharingEnabled();
        boolean isArchived = signalGroupV1Record.isArchived();
        boolean isForcedUnread = signalGroupV1Record.isForcedUnread();
        long muteUntil = signalGroupV1Record.getMuteUntil();
        boolean doParamsMatch = doParamsMatch(signalGroupV1Record, serializeUnknownFields, isBlocked, isProfileSharingEnabled, isArchived, isForcedUnread, muteUntil);
        boolean doParamsMatch2 = doParamsMatch(signalGroupV1Record2, serializeUnknownFields, isBlocked, isProfileSharingEnabled, isArchived, isForcedUnread, muteUntil);
        if (doParamsMatch) {
            return signalGroupV1Record;
        }
        if (doParamsMatch2) {
            return signalGroupV1Record2;
        }
        return new SignalGroupV1Record.Builder(storageKeyGenerator.generate(), signalGroupV1Record.getGroupId(), serializeUnknownFields).setBlocked(isBlocked).setProfileSharingEnabled(isBlocked).setForcedUnread(isForcedUnread).setMuteUntil(muteUntil).build();
    }

    public void insertLocal(SignalGroupV1Record signalGroupV1Record) {
        this.recipientDatabase.applyStorageSyncGroupV1Insert(signalGroupV1Record);
    }

    void updateLocal(StorageRecordUpdate<SignalGroupV1Record> storageRecordUpdate) {
        this.recipientDatabase.applyStorageSyncGroupV1Update(storageRecordUpdate);
    }

    public int compare(SignalGroupV1Record signalGroupV1Record, SignalGroupV1Record signalGroupV1Record2) {
        return Arrays.equals(signalGroupV1Record.getGroupId(), signalGroupV1Record2.getGroupId()) ? 0 : 1;
    }

    private boolean doParamsMatch(SignalGroupV1Record signalGroupV1Record, byte[] bArr, boolean z, boolean z2, boolean z3, boolean z4, long j) {
        return Arrays.equals(bArr, signalGroupV1Record.serializeUnknownFields()) && z == signalGroupV1Record.isBlocked() && z2 == signalGroupV1Record.isProfileSharingEnabled() && z3 == signalGroupV1Record.isArchived() && z4 == signalGroupV1Record.isForcedUnread() && j == signalGroupV1Record.getMuteUntil();
    }
}
