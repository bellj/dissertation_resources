package org.thoughtcrime.securesms.storage;

import j$.util.Optional;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.DistributionId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public class StoryDistributionListRecordProcessor extends DefaultStorageRecordProcessor<SignalStoryDistributionListRecord> {
    private static final String TAG = Log.tag(StoryDistributionListRecordProcessor.class);
    private boolean haveSeenMyStory;

    public /* bridge */ /* synthetic */ void process(Collection collection, StorageKeyGenerator storageKeyGenerator) throws IOException {
        super.process(collection, storageKeyGenerator);
    }

    public boolean isInvalid(SignalStoryDistributionListRecord signalStoryDistributionListRecord) {
        UUID parseOrNull = UuidUtil.parseOrNull(signalStoryDistributionListRecord.getIdentifier());
        if (parseOrNull == null) {
            Log.d(TAG, "Bad distribution list identifier -- marking as invalid");
            return true;
        }
        boolean equals = parseOrNull.equals(DistributionId.MY_STORY.asUuid());
        boolean z = this.haveSeenMyStory;
        if (!z || !equals) {
            this.haveSeenMyStory = z | equals;
            if (signalStoryDistributionListRecord.getDeletedAtTimestamp() > 0) {
                if (!equals) {
                    return false;
                }
                Log.w(TAG, "Refusing to delete My Story -- marking as invalid");
                return true;
            } else if (!StringUtil.isVisuallyEmpty(signalStoryDistributionListRecord.getName())) {
                return false;
            } else {
                Log.d(TAG, "Bad distribution list name (visually empty) -- marking as invalid");
                return true;
            }
        } else {
            Log.w(TAG, "Found an additional MyStory record -- marking as invalid");
            return true;
        }
    }

    public Optional<SignalStoryDistributionListRecord> getMatching(SignalStoryDistributionListRecord signalStoryDistributionListRecord, StorageKeyGenerator storageKeyGenerator) {
        RecipientId recipientIdForSyncRecord = SignalDatabase.distributionLists().getRecipientIdForSyncRecord(signalStoryDistributionListRecord);
        if (recipientIdForSyncRecord == null) {
            return Optional.empty();
        }
        RecipientRecord recordForSync = SignalDatabase.recipients().getRecordForSync(recipientIdForSyncRecord);
        if (recordForSync != null) {
            return StorageSyncModels.localToRemoteRecord(recordForSync).getStoryDistributionList();
        }
        throw new IllegalStateException("Found matching recipient but couldn't generate record for sync.");
    }

    public SignalStoryDistributionListRecord merge(SignalStoryDistributionListRecord signalStoryDistributionListRecord, SignalStoryDistributionListRecord signalStoryDistributionListRecord2, StorageKeyGenerator storageKeyGenerator) {
        byte[] serializeUnknownFields = signalStoryDistributionListRecord.serializeUnknownFields();
        byte[] identifier = signalStoryDistributionListRecord.getIdentifier();
        String name = signalStoryDistributionListRecord.getName();
        List<SignalServiceAddress> recipients = signalStoryDistributionListRecord.getRecipients();
        long deletedAtTimestamp = signalStoryDistributionListRecord.getDeletedAtTimestamp();
        boolean allowsReplies = signalStoryDistributionListRecord.allowsReplies();
        boolean isBlockList = signalStoryDistributionListRecord.isBlockList();
        boolean doParamsMatch = doParamsMatch(signalStoryDistributionListRecord, serializeUnknownFields, identifier, name, recipients, deletedAtTimestamp, allowsReplies, isBlockList);
        boolean doParamsMatch2 = doParamsMatch(signalStoryDistributionListRecord2, serializeUnknownFields, identifier, name, recipients, deletedAtTimestamp, allowsReplies, isBlockList);
        if (doParamsMatch) {
            return signalStoryDistributionListRecord;
        }
        if (doParamsMatch2) {
            return signalStoryDistributionListRecord2;
        }
        return new SignalStoryDistributionListRecord.Builder(storageKeyGenerator.generate(), serializeUnknownFields).setIdentifier(identifier).setName(name).setRecipients(recipients).setDeletedAtTimestamp(deletedAtTimestamp).setAllowsReplies(allowsReplies).setIsBlockList(isBlockList).build();
    }

    public void insertLocal(SignalStoryDistributionListRecord signalStoryDistributionListRecord) throws IOException {
        SignalDatabase.distributionLists().applyStorageSyncStoryDistributionListInsert(signalStoryDistributionListRecord);
    }

    void updateLocal(StorageRecordUpdate<SignalStoryDistributionListRecord> storageRecordUpdate) {
        SignalDatabase.distributionLists().applyStorageSyncStoryDistributionListUpdate(storageRecordUpdate);
    }

    public int compare(SignalStoryDistributionListRecord signalStoryDistributionListRecord, SignalStoryDistributionListRecord signalStoryDistributionListRecord2) {
        return Arrays.equals(signalStoryDistributionListRecord.getIdentifier(), signalStoryDistributionListRecord2.getIdentifier()) ? 0 : 1;
    }

    private boolean doParamsMatch(SignalStoryDistributionListRecord signalStoryDistributionListRecord, byte[] bArr, byte[] bArr2, String str, List<SignalServiceAddress> list, long j, boolean z, boolean z2) {
        return Arrays.equals(bArr, signalStoryDistributionListRecord.serializeUnknownFields()) && Arrays.equals(bArr2, signalStoryDistributionListRecord.getIdentifier()) && Objects.equals(str, signalStoryDistributionListRecord.getName()) && Objects.equals(list, signalStoryDistributionListRecord.getRecipients()) && j == signalStoryDistributionListRecord.getDeletedAtTimestamp() && z == signalStoryDistributionListRecord.allowsReplies() && z2 == signalStoryDistributionListRecord.isBlockList();
    }
}
