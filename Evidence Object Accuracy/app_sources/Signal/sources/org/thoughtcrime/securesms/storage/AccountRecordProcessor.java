package org.thoughtcrime.securesms.storage;

import android.content.Context;
import j$.util.Optional;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.internal.storage.protos.AccountRecord;

/* loaded from: classes4.dex */
public class AccountRecordProcessor extends DefaultStorageRecordProcessor<SignalAccountRecord> {
    private static final String TAG = Log.tag(AccountRecordProcessor.class);
    private final Context context;
    private boolean foundAccountRecord;
    private final SignalAccountRecord localAccountRecord;
    private final Recipient self;

    public int compare(SignalAccountRecord signalAccountRecord, SignalAccountRecord signalAccountRecord2) {
        return 0;
    }

    public /* bridge */ /* synthetic */ void process(Collection collection, StorageKeyGenerator storageKeyGenerator) throws IOException {
        super.process(collection, storageKeyGenerator);
    }

    public AccountRecordProcessor(Context context, Recipient recipient) {
        this(context, recipient, StorageSyncHelper.buildAccountRecord(context, recipient).getAccount().get());
    }

    AccountRecordProcessor(Context context, Recipient recipient, SignalAccountRecord signalAccountRecord) {
        this.foundAccountRecord = false;
        this.context = context;
        this.self = recipient;
        this.localAccountRecord = signalAccountRecord;
    }

    public boolean isInvalid(SignalAccountRecord signalAccountRecord) {
        if (this.foundAccountRecord) {
            Log.w(TAG, "Found an additional account record! Considering it invalid.");
            return true;
        }
        this.foundAccountRecord = true;
        return false;
    }

    public Optional<SignalAccountRecord> getMatching(SignalAccountRecord signalAccountRecord, StorageKeyGenerator storageKeyGenerator) {
        return Optional.of(this.localAccountRecord);
    }

    public SignalAccountRecord merge(SignalAccountRecord signalAccountRecord, SignalAccountRecord signalAccountRecord2, StorageKeyGenerator storageKeyGenerator) {
        String str;
        String str2;
        SignalAccountRecord.Payments payments;
        SignalAccountRecord.Subscriber subscriber;
        if (signalAccountRecord.getGivenName().isPresent() || signalAccountRecord.getFamilyName().isPresent()) {
            str2 = signalAccountRecord.getGivenName().orElse("");
            str = signalAccountRecord.getFamilyName().orElse("");
        } else {
            str2 = signalAccountRecord2.getGivenName().orElse("");
            str = signalAccountRecord2.getFamilyName().orElse("");
        }
        if (signalAccountRecord.getPayments().getEntropy().isPresent()) {
            payments = signalAccountRecord.getPayments();
        } else {
            payments = signalAccountRecord2.getPayments();
        }
        if (signalAccountRecord.getSubscriber().getId().isPresent()) {
            subscriber = signalAccountRecord.getSubscriber();
        } else {
            subscriber = signalAccountRecord2.getSubscriber();
        }
        byte[] serializeUnknownFields = signalAccountRecord.serializeUnknownFields();
        String str3 = (String) OptionalUtil.or(signalAccountRecord.getAvatarUrlPath(), signalAccountRecord2.getAvatarUrlPath()).orElse("");
        byte[] bArr = (byte[]) OptionalUtil.or(signalAccountRecord.getProfileKey(), signalAccountRecord2.getProfileKey()).orElse(null);
        boolean isNoteToSelfArchived = signalAccountRecord.isNoteToSelfArchived();
        boolean isNoteToSelfForcedUnread = signalAccountRecord.isNoteToSelfForcedUnread();
        boolean isReadReceiptsEnabled = signalAccountRecord.isReadReceiptsEnabled();
        boolean isTypingIndicatorsEnabled = signalAccountRecord.isTypingIndicatorsEnabled();
        boolean isSealedSenderIndicatorsEnabled = signalAccountRecord.isSealedSenderIndicatorsEnabled();
        boolean isLinkPreviewsEnabled = signalAccountRecord.isLinkPreviewsEnabled();
        boolean isPhoneNumberUnlisted = signalAccountRecord.isPhoneNumberUnlisted();
        List<SignalAccountRecord.PinnedConversation> pinnedConversations = signalAccountRecord.getPinnedConversations();
        AccountRecord.PhoneNumberSharingMode phoneNumberSharingMode = signalAccountRecord.getPhoneNumberSharingMode();
        boolean isPreferContactAvatars = signalAccountRecord.isPreferContactAvatars();
        int universalExpireTimer = signalAccountRecord.getUniversalExpireTimer();
        boolean isPrimarySendsSms = SignalStore.account().isPrimaryDevice() ? signalAccountRecord2.isPrimarySendsSms() : signalAccountRecord.isPrimarySendsSms();
        String e164 = SignalStore.account().isPrimaryDevice() ? signalAccountRecord2.getE164() : signalAccountRecord.getE164();
        List<String> defaultReactions = signalAccountRecord.getDefaultReactions().size() > 0 ? signalAccountRecord.getDefaultReactions() : signalAccountRecord2.getDefaultReactions();
        boolean isDisplayBadgesOnProfile = signalAccountRecord.isDisplayBadgesOnProfile();
        boolean isSubscriptionManuallyCancelled = signalAccountRecord.isSubscriptionManuallyCancelled();
        boolean doParamsMatch = doParamsMatch(signalAccountRecord, serializeUnknownFields, str2, str, str3, bArr, isNoteToSelfArchived, isNoteToSelfForcedUnread, isReadReceiptsEnabled, isTypingIndicatorsEnabled, isSealedSenderIndicatorsEnabled, isLinkPreviewsEnabled, phoneNumberSharingMode, isPhoneNumberUnlisted, pinnedConversations, isPreferContactAvatars, payments, universalExpireTimer, isPrimarySendsSms, e164, defaultReactions, subscriber, isDisplayBadgesOnProfile, isSubscriptionManuallyCancelled);
        boolean doParamsMatch2 = doParamsMatch(signalAccountRecord2, serializeUnknownFields, str2, str, str3, bArr, isNoteToSelfArchived, isNoteToSelfForcedUnread, isReadReceiptsEnabled, isTypingIndicatorsEnabled, isSealedSenderIndicatorsEnabled, isLinkPreviewsEnabled, phoneNumberSharingMode, isPhoneNumberUnlisted, pinnedConversations, isPreferContactAvatars, payments, universalExpireTimer, isPrimarySendsSms, e164, defaultReactions, subscriber, isDisplayBadgesOnProfile, isSubscriptionManuallyCancelled);
        if (doParamsMatch) {
            return signalAccountRecord;
        }
        if (doParamsMatch2) {
            return signalAccountRecord2;
        }
        return new SignalAccountRecord.Builder(storageKeyGenerator.generate(), serializeUnknownFields).setGivenName(str2).setFamilyName(str).setAvatarUrlPath(str3).setProfileKey(bArr).setNoteToSelfArchived(isNoteToSelfArchived).setNoteToSelfForcedUnread(isNoteToSelfForcedUnread).setReadReceiptsEnabled(isReadReceiptsEnabled).setTypingIndicatorsEnabled(isTypingIndicatorsEnabled).setSealedSenderIndicatorsEnabled(isSealedSenderIndicatorsEnabled).setLinkPreviewsEnabled(isLinkPreviewsEnabled).setUnlistedPhoneNumber(isPhoneNumberUnlisted).setPhoneNumberSharingMode(phoneNumberSharingMode).setUnlistedPhoneNumber(isPhoneNumberUnlisted).setPinnedConversations(pinnedConversations).setPreferContactAvatars(isPreferContactAvatars).setPayments(payments.isEnabled(), payments.getEntropy().orElse(null)).setUniversalExpireTimer(universalExpireTimer).setPrimarySendsSms(isPrimarySendsSms).setE164(e164).setDefaultReactions(defaultReactions).setSubscriber(subscriber).setDisplayBadgesOnProfile(isDisplayBadgesOnProfile).setSubscriptionManuallyCancelled(isSubscriptionManuallyCancelled).build();
    }

    public void insertLocal(SignalAccountRecord signalAccountRecord) {
        throw new UnsupportedOperationException("We should always have a local AccountRecord, so we should never been inserting a new one.");
    }

    void updateLocal(StorageRecordUpdate<SignalAccountRecord> storageRecordUpdate) {
        StorageSyncHelper.applyAccountStorageSyncUpdates(this.context, this.self, storageRecordUpdate, true);
    }

    private static boolean doParamsMatch(SignalAccountRecord signalAccountRecord, byte[] bArr, String str, String str2, String str3, byte[] bArr2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, AccountRecord.PhoneNumberSharingMode phoneNumberSharingMode, boolean z7, List<SignalAccountRecord.PinnedConversation> list, boolean z8, SignalAccountRecord.Payments payments, int i, boolean z9, String str4, List<String> list2, SignalAccountRecord.Subscriber subscriber, boolean z10, boolean z11) {
        return Arrays.equals(signalAccountRecord.serializeUnknownFields(), bArr) && Objects.equals(signalAccountRecord.getGivenName().orElse(""), str) && Objects.equals(signalAccountRecord.getFamilyName().orElse(""), str2) && Objects.equals(signalAccountRecord.getAvatarUrlPath().orElse(""), str3) && Objects.equals(signalAccountRecord.getPayments(), payments) && Objects.equals(signalAccountRecord.getE164(), str4) && Objects.equals(signalAccountRecord.getDefaultReactions(), list2) && Arrays.equals(signalAccountRecord.getProfileKey().orElse(null), bArr2) && signalAccountRecord.isNoteToSelfArchived() == z && signalAccountRecord.isNoteToSelfForcedUnread() == z2 && signalAccountRecord.isReadReceiptsEnabled() == z3 && signalAccountRecord.isTypingIndicatorsEnabled() == z4 && signalAccountRecord.isSealedSenderIndicatorsEnabled() == z5 && signalAccountRecord.isLinkPreviewsEnabled() == z6 && signalAccountRecord.getPhoneNumberSharingMode() == phoneNumberSharingMode && signalAccountRecord.isPhoneNumberUnlisted() == z7 && signalAccountRecord.isPreferContactAvatars() == z8 && signalAccountRecord.getUniversalExpireTimer() == i && signalAccountRecord.isPrimarySendsSms() == z9 && Objects.equals(signalAccountRecord.getPinnedConversations(), list) && Objects.equals(signalAccountRecord.getSubscriber(), subscriber) && signalAccountRecord.isDisplayBadgesOnProfile() == z10 && signalAccountRecord.isSubscriptionManuallyCancelled() == z11;
    }
}
