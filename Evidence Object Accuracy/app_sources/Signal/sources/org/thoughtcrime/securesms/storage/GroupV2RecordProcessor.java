package org.thoughtcrime.securesms.storage;

import android.content.Context;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.storage.SignalGroupV2Record;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;

/* loaded from: classes4.dex */
public final class GroupV2RecordProcessor extends DefaultStorageRecordProcessor<SignalGroupV2Record> {
    private static final String TAG = Log.tag(GroupV2RecordProcessor.class);
    private final Context context;
    private final GroupDatabase groupDatabase;
    private final Map<GroupId.V2, GroupId.V1> gv1GroupsByExpectedGv2Id;
    private final RecipientDatabase recipientDatabase;

    public /* bridge */ /* synthetic */ void process(Collection collection, StorageKeyGenerator storageKeyGenerator) throws IOException {
        super.process(collection, storageKeyGenerator);
    }

    public GroupV2RecordProcessor(Context context) {
        this(context, SignalDatabase.recipients(), SignalDatabase.groups());
    }

    GroupV2RecordProcessor(Context context, RecipientDatabase recipientDatabase, GroupDatabase groupDatabase) {
        this.context = context;
        this.recipientDatabase = recipientDatabase;
        this.groupDatabase = groupDatabase;
        this.gv1GroupsByExpectedGv2Id = groupDatabase.getAllExpectedV2Ids();
    }

    public boolean isInvalid(SignalGroupV2Record signalGroupV2Record) {
        return signalGroupV2Record.getMasterKeyBytes().length != 32;
    }

    public Optional<SignalGroupV2Record> getMatching(SignalGroupV2Record signalGroupV2Record, StorageKeyGenerator storageKeyGenerator) {
        Optional<RecipientId> byGroupId = this.recipientDatabase.getByGroupId(GroupId.v2(signalGroupV2Record.getMasterKeyOrThrow()));
        RecipientDatabase recipientDatabase = this.recipientDatabase;
        Objects.requireNonNull(recipientDatabase);
        return byGroupId.map(new ContactRecordProcessor$$ExternalSyntheticLambda0(recipientDatabase)).map(new Function(signalGroupV2Record) { // from class: org.thoughtcrime.securesms.storage.GroupV2RecordProcessor$$ExternalSyntheticLambda0
            public final /* synthetic */ SignalGroupV2Record f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GroupV2RecordProcessor.this.lambda$getMatching$0(this.f$1, (RecipientRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.storage.GroupV2RecordProcessor$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GroupV2RecordProcessor.lambda$getMatching$1((SignalStorageRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public /* synthetic */ SignalStorageRecord lambda$getMatching$0(SignalGroupV2Record signalGroupV2Record, RecipientRecord recipientRecord) {
        if (recipientRecord.getSyncExtras().getGroupMasterKey() != null) {
            return StorageSyncModels.localToRemoteRecord(recipientRecord);
        }
        Log.w(TAG, "No local master key. Assuming it matches remote since the groupIds match. Enqueuing a fetch to fix the bad state.");
        this.groupDatabase.fixMissingMasterKey(null, signalGroupV2Record.getMasterKeyOrThrow());
        return StorageSyncModels.localToRemoteRecord(recipientRecord, signalGroupV2Record.getMasterKeyOrThrow());
    }

    public static /* synthetic */ SignalGroupV2Record lambda$getMatching$1(SignalStorageRecord signalStorageRecord) {
        return signalStorageRecord.getGroupV2().get();
    }

    public SignalGroupV2Record merge(SignalGroupV2Record signalGroupV2Record, SignalGroupV2Record signalGroupV2Record2, StorageKeyGenerator storageKeyGenerator) {
        byte[] serializeUnknownFields = signalGroupV2Record.serializeUnknownFields();
        boolean isBlocked = signalGroupV2Record.isBlocked();
        boolean isProfileSharingEnabled = signalGroupV2Record.isProfileSharingEnabled();
        boolean isArchived = signalGroupV2Record.isArchived();
        boolean isForcedUnread = signalGroupV2Record.isForcedUnread();
        long muteUntil = signalGroupV2Record.getMuteUntil();
        boolean notifyForMentionsWhenMuted = signalGroupV2Record.notifyForMentionsWhenMuted();
        boolean shouldHideStory = signalGroupV2Record.shouldHideStory();
        boolean doParamsMatch = doParamsMatch(signalGroupV2Record, serializeUnknownFields, isBlocked, isProfileSharingEnabled, isArchived, isForcedUnread, muteUntil, notifyForMentionsWhenMuted, shouldHideStory);
        boolean doParamsMatch2 = doParamsMatch(signalGroupV2Record2, serializeUnknownFields, isBlocked, isProfileSharingEnabled, isArchived, isForcedUnread, muteUntil, notifyForMentionsWhenMuted, shouldHideStory);
        if (doParamsMatch) {
            return signalGroupV2Record;
        }
        if (doParamsMatch2) {
            return signalGroupV2Record2;
        }
        return new SignalGroupV2Record.Builder(storageKeyGenerator.generate(), signalGroupV2Record.getMasterKeyBytes(), serializeUnknownFields).setBlocked(isBlocked).setProfileSharingEnabled(isBlocked).setArchived(isArchived).setForcedUnread(isForcedUnread).setMuteUntil(muteUntil).setNotifyForMentionsWhenMuted(notifyForMentionsWhenMuted).setHideStory(shouldHideStory).build();
    }

    public void insertLocal(SignalGroupV2Record signalGroupV2Record) throws IOException {
        GroupId.V1 v1 = this.gv1GroupsByExpectedGv2Id.get(GroupId.v2(signalGroupV2Record.getMasterKeyOrThrow()));
        if (v1 != null) {
            Log.i(TAG, "Discovered a new GV2 ID that is actually a migrated V1 group! Migrating now.");
            GroupsV1MigrationUtil.performLocalMigration(this.context, v1);
            return;
        }
        this.recipientDatabase.applyStorageSyncGroupV2Insert(signalGroupV2Record);
    }

    void updateLocal(StorageRecordUpdate<SignalGroupV2Record> storageRecordUpdate) {
        this.recipientDatabase.applyStorageSyncGroupV2Update(storageRecordUpdate);
    }

    public int compare(SignalGroupV2Record signalGroupV2Record, SignalGroupV2Record signalGroupV2Record2) {
        return Arrays.equals(signalGroupV2Record.getMasterKeyBytes(), signalGroupV2Record2.getMasterKeyBytes()) ? 0 : 1;
    }

    private boolean doParamsMatch(SignalGroupV2Record signalGroupV2Record, byte[] bArr, boolean z, boolean z2, boolean z3, boolean z4, long j, boolean z5, boolean z6) {
        return Arrays.equals(bArr, signalGroupV2Record.serializeUnknownFields()) && z == signalGroupV2Record.isBlocked() && z2 == signalGroupV2Record.isProfileSharingEnabled() && z3 == signalGroupV2Record.isArchived() && z4 == signalGroupV2Record.isForcedUnread() && j == signalGroupV2Record.getMuteUntil() && z5 == signalGroupV2Record.notifyForMentionsWhenMuted() && z6 == signalGroupV2Record.shouldHideStory();
    }
}
