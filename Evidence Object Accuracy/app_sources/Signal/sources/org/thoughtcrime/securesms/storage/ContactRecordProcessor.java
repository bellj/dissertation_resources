package org.thoughtcrime.securesms.storage;

import android.content.Context;
import j$.util.Optional;
import j$.util.function.Function;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.SignalContactRecord;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.util.OptionalUtil;
import org.whispersystems.signalservice.internal.storage.protos.ContactRecord;

/* loaded from: classes4.dex */
public class ContactRecordProcessor extends DefaultStorageRecordProcessor<SignalContactRecord> {
    private static final String TAG = Log.tag(ContactRecordProcessor.class);
    private final RecipientDatabase recipientDatabase;
    private final Recipient self;

    public /* bridge */ /* synthetic */ void process(Collection collection, StorageKeyGenerator storageKeyGenerator) throws IOException {
        super.process(collection, storageKeyGenerator);
    }

    public ContactRecordProcessor(Context context, Recipient recipient) {
        this(recipient, SignalDatabase.recipients());
    }

    ContactRecordProcessor(Recipient recipient, RecipientDatabase recipientDatabase) {
        this.self = recipient;
        this.recipientDatabase = recipientDatabase;
    }

    public boolean isInvalid(SignalContactRecord signalContactRecord) {
        SignalServiceAddress address = signalContactRecord.getAddress();
        if (address == null) {
            Log.w(TAG, "No address on the ContentRecord -- marking as invalid.");
            return true;
        } else if (!address.hasValidServiceId()) {
            Log.w(TAG, "Found a ContactRecord without a UUID -- marking as invalid.");
            return true;
        } else if ((!this.self.getServiceId().isPresent() || !address.getServiceId().equals(this.self.requireServiceId())) && (!this.self.getE164().isPresent() || !address.getNumber().equals(this.self.getE164()))) {
            return false;
        } else {
            Log.w(TAG, "Found a ContactRecord for ourselves -- marking as invalid.");
            return true;
        }
    }

    public Optional<SignalContactRecord> getMatching(SignalContactRecord signalContactRecord, StorageKeyGenerator storageKeyGenerator) {
        SignalServiceAddress address = signalContactRecord.getAddress();
        Optional or = OptionalUtil.or(this.recipientDatabase.getByServiceId(address.getServiceId()), address.getNumber().isPresent() ? this.recipientDatabase.getByE164(address.getNumber().get()) : Optional.empty());
        RecipientDatabase recipientDatabase = this.recipientDatabase;
        Objects.requireNonNull(recipientDatabase);
        return or.map(new ContactRecordProcessor$$ExternalSyntheticLambda0(recipientDatabase)).map(new Function(storageKeyGenerator) { // from class: org.thoughtcrime.securesms.storage.ContactRecordProcessor$$ExternalSyntheticLambda1
            public final /* synthetic */ StorageKeyGenerator f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContactRecordProcessor.this.lambda$getMatching$0(this.f$1, (RecipientRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.storage.ContactRecordProcessor$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContactRecordProcessor.lambda$getMatching$1((SignalStorageRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public /* synthetic */ SignalStorageRecord lambda$getMatching$0(StorageKeyGenerator storageKeyGenerator, RecipientRecord recipientRecord) {
        if (recipientRecord.getStorageId() != null) {
            return StorageSyncModels.localToRemoteRecord(recipientRecord);
        }
        Log.w(TAG, "Newly discovering a registered user via storage service. Saving a storageId for them.");
        this.recipientDatabase.updateStorageId(recipientRecord.getId(), storageKeyGenerator.generate());
        RecipientRecord recordForSync = this.recipientDatabase.getRecordForSync(recipientRecord.getId());
        Objects.requireNonNull(recordForSync);
        return StorageSyncModels.localToRemoteRecord(recordForSync);
    }

    public static /* synthetic */ SignalContactRecord lambda$getMatching$1(SignalStorageRecord signalStorageRecord) {
        return signalStorageRecord.getContact().get();
    }

    public SignalContactRecord merge(SignalContactRecord signalContactRecord, SignalContactRecord signalContactRecord2, StorageKeyGenerator storageKeyGenerator) {
        String str;
        String str2;
        byte[] bArr;
        ContactRecord.IdentityState identityState;
        if (signalContactRecord.getGivenName().isPresent() || signalContactRecord.getFamilyName().isPresent()) {
            str2 = signalContactRecord.getGivenName().orElse("");
            str = signalContactRecord.getFamilyName().orElse("");
        } else {
            str2 = signalContactRecord2.getGivenName().orElse("");
            str = signalContactRecord2.getFamilyName().orElse("");
        }
        if ((signalContactRecord.getIdentityState() == signalContactRecord2.getIdentityState() || !signalContactRecord.getIdentityKey().isPresent()) && (!signalContactRecord.getIdentityKey().isPresent() || signalContactRecord2.getIdentityKey().isPresent())) {
            identityState = signalContactRecord2.getIdentityState();
            bArr = signalContactRecord2.getIdentityKey().orElse(null);
        } else {
            identityState = signalContactRecord.getIdentityState();
            bArr = signalContactRecord.getIdentityKey().get();
        }
        if (bArr != null && signalContactRecord.getIdentityKey().isPresent() && !Arrays.equals(bArr, signalContactRecord.getIdentityKey().get())) {
            String str3 = TAG;
            Log.w(str3, "The local and remote identity keys do not match for " + signalContactRecord2.getAddress().getIdentifier() + ". Enqueueing a profile fetch.");
            RetrieveProfileJob.enqueue(Recipient.externalPush(signalContactRecord2.getAddress()).getId());
        }
        byte[] serializeUnknownFields = signalContactRecord.serializeUnknownFields();
        SignalServiceAddress signalServiceAddress = new SignalServiceAddress((signalContactRecord2.getAddress().getServiceId() == ServiceId.UNKNOWN ? signalContactRecord.getAddress() : signalContactRecord2.getAddress()).getServiceId(), (String) OptionalUtil.or(signalContactRecord.getAddress().getNumber(), signalContactRecord2.getAddress().getNumber()).orElse(null));
        byte[] bArr2 = (byte[]) OptionalUtil.or(signalContactRecord.getProfileKey(), signalContactRecord2.getProfileKey()).orElse(null);
        String str4 = (String) OptionalUtil.or(signalContactRecord.getUsername(), signalContactRecord2.getUsername()).orElse("");
        boolean isBlocked = signalContactRecord.isBlocked();
        boolean isProfileSharingEnabled = signalContactRecord.isProfileSharingEnabled();
        boolean isArchived = signalContactRecord.isArchived();
        boolean isForcedUnread = signalContactRecord.isForcedUnread();
        long muteUntil = signalContactRecord.getMuteUntil();
        boolean shouldHideStory = signalContactRecord.shouldHideStory();
        boolean doParamsMatch = doParamsMatch(signalContactRecord, serializeUnknownFields, signalServiceAddress, str2, str, bArr2, str4, identityState, bArr, isBlocked, isProfileSharingEnabled, isArchived, isForcedUnread, muteUntil, shouldHideStory);
        boolean doParamsMatch2 = doParamsMatch(signalContactRecord2, serializeUnknownFields, signalServiceAddress, str2, str, bArr2, str4, identityState, bArr, isBlocked, isProfileSharingEnabled, isArchived, isForcedUnread, muteUntil, shouldHideStory);
        if (doParamsMatch) {
            return signalContactRecord;
        }
        if (doParamsMatch2) {
            return signalContactRecord2;
        }
        return new SignalContactRecord.Builder(storageKeyGenerator.generate(), signalServiceAddress, serializeUnknownFields).setGivenName(str2).setFamilyName(str).setProfileKey(bArr2).setUsername(str4).setIdentityState(identityState).setIdentityKey(bArr).setBlocked(isBlocked).setProfileSharingEnabled(isProfileSharingEnabled).setArchived(isArchived).setForcedUnread(isForcedUnread).setMuteUntil(muteUntil).setHideStory(shouldHideStory).build();
    }

    public void insertLocal(SignalContactRecord signalContactRecord) {
        this.recipientDatabase.applyStorageSyncContactInsert(signalContactRecord);
    }

    void updateLocal(StorageRecordUpdate<SignalContactRecord> storageRecordUpdate) {
        this.recipientDatabase.applyStorageSyncContactUpdate(storageRecordUpdate);
    }

    public int compare(SignalContactRecord signalContactRecord, SignalContactRecord signalContactRecord2) {
        return (Objects.equals(signalContactRecord.getAddress().getServiceId(), signalContactRecord2.getAddress().getServiceId()) || Objects.equals(signalContactRecord.getAddress().getNumber(), signalContactRecord2.getAddress().getNumber())) ? 0 : 1;
    }

    private static boolean doParamsMatch(SignalContactRecord signalContactRecord, byte[] bArr, SignalServiceAddress signalServiceAddress, String str, String str2, byte[] bArr2, String str3, ContactRecord.IdentityState identityState, byte[] bArr3, boolean z, boolean z2, boolean z3, boolean z4, long j, boolean z5) {
        return Arrays.equals(signalContactRecord.serializeUnknownFields(), bArr) && Objects.equals(signalContactRecord.getAddress(), signalServiceAddress) && Objects.equals(signalContactRecord.getGivenName().orElse(""), str) && Objects.equals(signalContactRecord.getFamilyName().orElse(""), str2) && Arrays.equals(signalContactRecord.getProfileKey().orElse(null), bArr2) && Objects.equals(signalContactRecord.getUsername().orElse(""), str3) && Objects.equals(signalContactRecord.getIdentityState(), identityState) && Arrays.equals(signalContactRecord.getIdentityKey().orElse(null), bArr3) && signalContactRecord.isBlocked() == z && signalContactRecord.isProfileSharingEnabled() == z2 && signalContactRecord.isArchived() == z3 && signalContactRecord.isForcedUnread() == z4 && signalContactRecord.getMuteUntil() == j && signalContactRecord.shouldHideStory() == z5;
    }
}
