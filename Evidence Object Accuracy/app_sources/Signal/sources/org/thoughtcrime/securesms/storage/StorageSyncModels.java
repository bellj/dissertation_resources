package org.thoughtcrime.securesms.storage;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.stream.Collectors;
import java.util.List;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.database.GroupDatabase$GroupRecord$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.database.model.DistributionListRecord;
import org.thoughtcrime.securesms.database.model.RecipientRecord;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.SignalAccountRecord;
import org.whispersystems.signalservice.api.storage.SignalContactRecord;
import org.whispersystems.signalservice.api.storage.SignalGroupV1Record;
import org.whispersystems.signalservice.api.storage.SignalGroupV2Record;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.SignalStoryDistributionListRecord;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.storage.protos.AccountRecord;
import org.whispersystems.signalservice.internal.storage.protos.ContactRecord;

/* loaded from: classes4.dex */
public final class StorageSyncModels {
    private StorageSyncModels() {
    }

    public static SignalStorageRecord localToRemoteRecord(RecipientRecord recipientRecord) {
        if (recipientRecord.getStorageId() != null) {
            return localToRemoteRecord(recipientRecord, recipientRecord.getStorageId());
        }
        throw new AssertionError("Must have a storage key!");
    }

    public static SignalStorageRecord localToRemoteRecord(RecipientRecord recipientRecord, GroupMasterKey groupMasterKey) {
        if (recipientRecord.getStorageId() != null) {
            return SignalStorageRecord.forGroupV2(localToRemoteGroupV2(recipientRecord, recipientRecord.getStorageId(), groupMasterKey));
        }
        throw new AssertionError("Must have a storage key!");
    }

    public static SignalStorageRecord localToRemoteRecord(RecipientRecord recipientRecord, byte[] bArr) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType[recipientRecord.getGroupType().ordinal()];
        if (i == 1) {
            return SignalStorageRecord.forContact(localToRemoteContact(recipientRecord, bArr));
        }
        if (i == 2) {
            return SignalStorageRecord.forGroupV1(localToRemoteGroupV1(recipientRecord, bArr));
        }
        if (i == 3) {
            return SignalStorageRecord.forGroupV2(localToRemoteGroupV2(recipientRecord, bArr, recipientRecord.getSyncExtras().getGroupMasterKey()));
        }
        if (i == 4) {
            return SignalStorageRecord.forStoryDistributionList(localToRemoteStoryDistributionList(recipientRecord, bArr));
        }
        throw new AssertionError("Unsupported type!");
    }

    public static AccountRecord.PhoneNumberSharingMode localToRemotePhoneNumberSharingMode(PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[phoneNumberSharingMode.ordinal()];
        if (i == 1) {
            return AccountRecord.PhoneNumberSharingMode.EVERYBODY;
        }
        if (i == 2) {
            return AccountRecord.PhoneNumberSharingMode.CONTACTS_ONLY;
        }
        if (i == 3) {
            return AccountRecord.PhoneNumberSharingMode.NOBODY;
        }
        throw new AssertionError();
    }

    public static PhoneNumberPrivacyValues.PhoneNumberSharingMode remoteToLocalPhoneNumberSharingMode(AccountRecord.PhoneNumberSharingMode phoneNumberSharingMode) {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$storage$protos$AccountRecord$PhoneNumberSharingMode[phoneNumberSharingMode.ordinal()];
        if (i == 1) {
            return PhoneNumberPrivacyValues.PhoneNumberSharingMode.EVERYONE;
        }
        if (i == 2) {
            return PhoneNumberPrivacyValues.PhoneNumberSharingMode.CONTACTS;
        }
        if (i != 3) {
            return PhoneNumberPrivacyValues.PhoneNumberSharingMode.CONTACTS;
        }
        return PhoneNumberPrivacyValues.PhoneNumberSharingMode.NOBODY;
    }

    public static List<SignalAccountRecord.PinnedConversation> localToRemotePinnedConversations(List<RecipientRecord> list) {
        return Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.storage.StorageSyncModels$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return StorageSyncModels.lambda$localToRemotePinnedConversations$0((RecipientRecord) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncModels$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorageSyncModels.localToRemotePinnedConversation((RecipientRecord) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$localToRemotePinnedConversations$0(RecipientRecord recipientRecord) {
        return recipientRecord.getGroupType() == RecipientDatabase.GroupType.SIGNAL_V1 || recipientRecord.getGroupType() == RecipientDatabase.GroupType.SIGNAL_V2 || recipientRecord.getRegistered() == RecipientDatabase.RegisteredState.REGISTERED;
    }

    public static SignalAccountRecord.PinnedConversation localToRemotePinnedConversation(RecipientRecord recipientRecord) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType[recipientRecord.getGroupType().ordinal()];
        if (i == 1) {
            return SignalAccountRecord.PinnedConversation.forContact(new SignalServiceAddress(recipientRecord.getServiceId(), recipientRecord.getE164()));
        }
        if (i == 2) {
            return SignalAccountRecord.PinnedConversation.forGroupV1(recipientRecord.getGroupId().requireV1().getDecodedId());
        }
        if (i == 3) {
            return SignalAccountRecord.PinnedConversation.forGroupV2(recipientRecord.getSyncExtras().getGroupMasterKey().serialize());
        }
        throw new AssertionError("Unexpected group type!");
    }

    private static SignalContactRecord localToRemoteContact(RecipientRecord recipientRecord, byte[] bArr) {
        if (recipientRecord.getServiceId() == null && recipientRecord.getE164() == null) {
            throw new AssertionError("Must have either a UUID or a phone number!");
        }
        ServiceId serviceId = recipientRecord.getServiceId() != null ? recipientRecord.getServiceId() : ServiceId.UNKNOWN;
        boolean z = true;
        boolean z2 = recipientRecord.getExtras() != null && recipientRecord.getExtras().hideStory();
        SignalContactRecord.Builder blocked = new SignalContactRecord.Builder(bArr, new SignalServiceAddress(serviceId, recipientRecord.getE164()), recipientRecord.getSyncExtras().getStorageProto()).setProfileKey(recipientRecord.getProfileKey()).setGivenName(recipientRecord.getProfileName().getGivenName()).setFamilyName(recipientRecord.getProfileName().getFamilyName()).setBlocked(recipientRecord.isBlocked());
        if (!recipientRecord.isProfileSharing() && recipientRecord.getSystemContactUri() == null) {
            z = false;
        }
        return blocked.setProfileSharingEnabled(z).setIdentityKey(recipientRecord.getSyncExtras().getIdentityKey()).setIdentityState(localToRemoteIdentityState(recipientRecord.getSyncExtras().getIdentityStatus())).setArchived(recipientRecord.getSyncExtras().isArchived()).setForcedUnread(recipientRecord.getSyncExtras().isForcedUnread()).setMuteUntil(recipientRecord.getMuteUntil()).setHideStory(z2).build();
    }

    private static SignalGroupV1Record localToRemoteGroupV1(RecipientRecord recipientRecord, byte[] bArr) {
        GroupId groupId = recipientRecord.getGroupId();
        if (groupId == null) {
            throw new AssertionError("Must have a groupId!");
        } else if (groupId.isV1()) {
            return new SignalGroupV1Record.Builder(bArr, groupId.getDecodedId(), recipientRecord.getSyncExtras().getStorageProto()).setBlocked(recipientRecord.isBlocked()).setProfileSharingEnabled(recipientRecord.isProfileSharing()).setArchived(recipientRecord.getSyncExtras().isArchived()).setForcedUnread(recipientRecord.getSyncExtras().isForcedUnread()).setMuteUntil(recipientRecord.getMuteUntil()).build();
        } else {
            throw new AssertionError("Group is not V1");
        }
    }

    private static SignalGroupV2Record localToRemoteGroupV2(RecipientRecord recipientRecord, byte[] bArr, GroupMasterKey groupMasterKey) {
        GroupId groupId = recipientRecord.getGroupId();
        if (groupId == null) {
            throw new AssertionError("Must have a groupId!");
        } else if (!groupId.isV2()) {
            throw new AssertionError("Group is not V2");
        } else if (groupMasterKey != null) {
            boolean z = true;
            boolean z2 = recipientRecord.getExtras() != null && recipientRecord.getExtras().hideStory();
            SignalGroupV2Record.Builder muteUntil = new SignalGroupV2Record.Builder(bArr, groupMasterKey, recipientRecord.getSyncExtras().getStorageProto()).setBlocked(recipientRecord.isBlocked()).setProfileSharingEnabled(recipientRecord.isProfileSharing()).setArchived(recipientRecord.getSyncExtras().isArchived()).setForcedUnread(recipientRecord.getSyncExtras().isForcedUnread()).setMuteUntil(recipientRecord.getMuteUntil());
            if (recipientRecord.getMentionSetting() != RecipientDatabase.MentionSetting.ALWAYS_NOTIFY) {
                z = false;
            }
            return muteUntil.setNotifyForMentionsWhenMuted(z).setHideStory(z2).build();
        } else {
            throw new AssertionError("Group master key not on recipient record");
        }
    }

    private static SignalStoryDistributionListRecord localToRemoteStoryDistributionList(RecipientRecord recipientRecord, byte[] bArr) {
        DistributionListId distributionListId = recipientRecord.getDistributionListId();
        if (distributionListId != null) {
            DistributionListRecord listForStorageSync = SignalDatabase.distributionLists().getListForStorageSync(distributionListId);
            if (listForStorageSync == null) {
                throw new AssertionError("Must have a distribution list record!");
            } else if (listForStorageSync.getDeletedAtTimestamp() > 0) {
                return new SignalStoryDistributionListRecord.Builder(bArr, recipientRecord.getSyncExtras().getStorageProto()).setIdentifier(UuidUtil.toByteArray(listForStorageSync.getDistributionId().asUuid())).setDeletedAtTimestamp(listForStorageSync.getDeletedAtTimestamp()).build();
            } else {
                return new SignalStoryDistributionListRecord.Builder(bArr, recipientRecord.getSyncExtras().getStorageProto()).setIdentifier(UuidUtil.toByteArray(listForStorageSync.getDistributionId().asUuid())).setName(listForStorageSync.getName()).setRecipients((List) Collection$EL.stream(listForStorageSync.getMembersToSync()).map(new GroupDatabase$GroupRecord$$ExternalSyntheticLambda0()).filter(new SignalBaseIdentityKeyStore$$ExternalSyntheticLambda1()).map(new SignalBaseIdentityKeyStore$$ExternalSyntheticLambda2()).map(new StorageSyncModels$$ExternalSyntheticLambda0()).collect(Collectors.toList())).setAllowsReplies(listForStorageSync.getAllowsReplies()).setIsBlockList(listForStorageSync.getPrivacyMode().isBlockList()).build();
            }
        } else {
            throw new AssertionError("Must have a distributionListId!");
        }
    }

    public static IdentityDatabase.VerifiedStatus remoteToLocalIdentityStatus(ContactRecord.IdentityState identityState) {
        int i = AnonymousClass1.$SwitchMap$org$whispersystems$signalservice$internal$storage$protos$ContactRecord$IdentityState[identityState.ordinal()];
        if (i == 1) {
            return IdentityDatabase.VerifiedStatus.VERIFIED;
        }
        if (i != 2) {
            return IdentityDatabase.VerifiedStatus.DEFAULT;
        }
        return IdentityDatabase.VerifiedStatus.UNVERIFIED;
    }

    /* renamed from: org.thoughtcrime.securesms.storage.StorageSyncModels$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$AccountRecord$PhoneNumberSharingMode;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$ContactRecord$IdentityState;

        static {
            int[] iArr = new int[IdentityDatabase.VerifiedStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus = iArr;
            try {
                iArr[IdentityDatabase.VerifiedStatus.VERIFIED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[IdentityDatabase.VerifiedStatus.UNVERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            int[] iArr2 = new int[ContactRecord.IdentityState.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$ContactRecord$IdentityState = iArr2;
            try {
                iArr2[ContactRecord.IdentityState.VERIFIED.ordinal()] = 1;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$ContactRecord$IdentityState[ContactRecord.IdentityState.UNVERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError unused4) {
            }
            int[] iArr3 = new int[AccountRecord.PhoneNumberSharingMode.values().length];
            $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$AccountRecord$PhoneNumberSharingMode = iArr3;
            try {
                iArr3[AccountRecord.PhoneNumberSharingMode.EVERYBODY.ordinal()] = 1;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$AccountRecord$PhoneNumberSharingMode[AccountRecord.PhoneNumberSharingMode.CONTACTS_ONLY.ordinal()] = 2;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$internal$storage$protos$AccountRecord$PhoneNumberSharingMode[AccountRecord.PhoneNumberSharingMode.NOBODY.ordinal()] = 3;
            } catch (NoSuchFieldError unused7) {
            }
            int[] iArr4 = new int[PhoneNumberPrivacyValues.PhoneNumberSharingMode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode = iArr4;
            try {
                iArr4[PhoneNumberPrivacyValues.PhoneNumberSharingMode.EVERYONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[PhoneNumberPrivacyValues.PhoneNumberSharingMode.CONTACTS.ordinal()] = 2;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[PhoneNumberPrivacyValues.PhoneNumberSharingMode.NOBODY.ordinal()] = 3;
            } catch (NoSuchFieldError unused10) {
            }
            int[] iArr5 = new int[RecipientDatabase.GroupType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType = iArr5;
            try {
                iArr5[RecipientDatabase.GroupType.NONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType[RecipientDatabase.GroupType.SIGNAL_V1.ordinal()] = 2;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType[RecipientDatabase.GroupType.SIGNAL_V2.ordinal()] = 3;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$GroupType[RecipientDatabase.GroupType.DISTRIBUTION_LIST.ordinal()] = 4;
            } catch (NoSuchFieldError unused14) {
            }
        }
    }

    private static ContactRecord.IdentityState localToRemoteIdentityState(IdentityDatabase.VerifiedStatus verifiedStatus) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$IdentityDatabase$VerifiedStatus[verifiedStatus.ordinal()];
        if (i == 1) {
            return ContactRecord.IdentityState.VERIFIED;
        }
        if (i != 2) {
            return ContactRecord.IdentityState.DEFAULT;
        }
        return ContactRecord.IdentityState.UNVERIFIED;
    }

    public static SignalAccountRecord.Subscriber localToRemoteSubscriber(Subscriber subscriber) {
        if (subscriber == null) {
            return new SignalAccountRecord.Subscriber(null, null);
        }
        return new SignalAccountRecord.Subscriber(subscriber.getCurrencyCode(), subscriber.getSubscriberId().getBytes());
    }

    public static Subscriber remoteToLocalSubscriber(SignalAccountRecord.Subscriber subscriber) {
        if (subscriber.getId().isPresent()) {
            return new Subscriber(SubscriberId.fromBytes(subscriber.getId().get()), subscriber.getCurrencyCode().get());
        }
        return null;
    }
}
