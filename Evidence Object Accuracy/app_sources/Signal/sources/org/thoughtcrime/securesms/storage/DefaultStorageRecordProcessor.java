package org.thoughtcrime.securesms.storage;

import j$.util.Optional;
import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;
import org.signal.core.util.logging.Log;
import org.whispersystems.signalservice.api.storage.SignalRecord;

/* loaded from: classes4.dex */
public abstract class DefaultStorageRecordProcessor<E extends SignalRecord> implements StorageRecordProcessor<E>, Comparator<E> {
    private static final String TAG = Log.tag(DefaultStorageRecordProcessor.class);

    abstract Optional<E> getMatching(E e, StorageKeyGenerator storageKeyGenerator);

    abstract void insertLocal(E e) throws IOException;

    abstract boolean isInvalid(E e);

    abstract E merge(E e, E e2, StorageKeyGenerator storageKeyGenerator);

    abstract void updateLocal(StorageRecordUpdate<E> storageRecordUpdate);

    @Override // org.thoughtcrime.securesms.storage.StorageRecordProcessor
    public void process(Collection<E> collection, StorageKeyGenerator storageKeyGenerator) throws IOException {
        TreeSet treeSet = new TreeSet(this);
        int i = 0;
        for (E e : collection) {
            if (isInvalid(e)) {
                warn(i, e, "Found invalid key! Ignoring it.");
            } else {
                Optional<E> matching = getMatching(e, storageKeyGenerator);
                if (matching.isPresent()) {
                    E merge = merge(e, matching.get(), storageKeyGenerator);
                    if (treeSet.contains(matching.get())) {
                        warn(i, e, "Multiple remote records map to the same local record! Ignoring this one.");
                    } else {
                        treeSet.add(matching.get());
                        if (!merge.equals(e)) {
                            info(i, e, "[Remote Update] " + new StorageRecordUpdate(e, merge).toString());
                        }
                        if (!merge.equals(matching.get())) {
                            StorageRecordUpdate<E> storageRecordUpdate = new StorageRecordUpdate<>(matching.get(), merge);
                            info(i, e, "[Local Update] " + storageRecordUpdate.toString());
                            updateLocal(storageRecordUpdate);
                        }
                    }
                } else {
                    info(i, e, "No matching local record. Inserting.");
                    insertLocal(e);
                }
            }
            i++;
        }
    }

    private void info(int i, E e, String str) {
        String str2 = TAG;
        Log.i(str2, "[" + i + "][" + e.getClass().getSimpleName() + "] " + str);
    }

    private void warn(int i, E e, String str) {
        String str2 = TAG;
        Log.w(str2, "[" + i + "][" + e.getClass().getSimpleName() + "] " + str);
    }
}
