package org.thoughtcrime.securesms.storage;

import java.util.Objects;
import org.whispersystems.signalservice.api.storage.SignalRecord;

/* loaded from: classes4.dex */
public class StorageRecordUpdate<E extends SignalRecord> {
    private final E newRecord;
    private final E oldRecord;

    public StorageRecordUpdate(E e, E e2) {
        this.oldRecord = e;
        this.newRecord = e2;
    }

    public E getOld() {
        return this.oldRecord;
    }

    public E getNew() {
        return this.newRecord;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        StorageRecordUpdate storageRecordUpdate = (StorageRecordUpdate) obj;
        if (!this.oldRecord.equals(storageRecordUpdate.oldRecord) || !this.newRecord.equals(storageRecordUpdate.newRecord)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.oldRecord, this.newRecord);
    }

    public String toString() {
        return this.newRecord.describeDiff(this.oldRecord);
    }
}
