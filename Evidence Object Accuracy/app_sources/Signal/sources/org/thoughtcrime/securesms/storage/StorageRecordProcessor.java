package org.thoughtcrime.securesms.storage;

import java.io.IOException;
import java.util.Collection;
import org.whispersystems.signalservice.api.storage.SignalRecord;

/* loaded from: classes4.dex */
public interface StorageRecordProcessor<E extends SignalRecord> {
    void process(Collection<E> collection, StorageKeyGenerator storageKeyGenerator) throws IOException;
}
