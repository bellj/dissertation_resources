package org.thoughtcrime.securesms.storage;

import j$.util.function.Function;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContactRecordProcessor$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ RecipientDatabase f$0;

    public /* synthetic */ ContactRecordProcessor$$ExternalSyntheticLambda0(RecipientDatabase recipientDatabase) {
        this.f$0 = recipientDatabase;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return this.f$0.getRecordForSync((RecipientId) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
