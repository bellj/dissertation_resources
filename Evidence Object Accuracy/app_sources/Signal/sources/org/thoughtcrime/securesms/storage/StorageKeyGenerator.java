package org.thoughtcrime.securesms.storage;

/* loaded from: classes4.dex */
public interface StorageKeyGenerator {
    byte[] generate();
}
