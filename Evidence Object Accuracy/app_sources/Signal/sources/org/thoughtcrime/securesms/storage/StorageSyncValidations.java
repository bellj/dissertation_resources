package org.thoughtcrime.securesms.storage;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.signal.core.util.SetUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.jobs.StorageSyncJob$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.SignalStorageRecord;
import org.whispersystems.signalservice.api.storage.StorageId;

/* loaded from: classes4.dex */
public final class StorageSyncValidations {
    private static final String TAG = Log.tag(StorageSyncValidations.class);

    private StorageSyncValidations() {
    }

    public static void validate(StorageSyncHelper.WriteOperationResult writeOperationResult, SignalStorageManifest signalStorageManifest, boolean z, Recipient recipient) {
        validateManifestAndInserts(writeOperationResult.getManifest(), writeOperationResult.getInserts(), recipient);
        if (writeOperationResult.getDeletes().size() > 0) {
            Set set = (Set) Stream.of(writeOperationResult.getManifest().getStorageIds()).map(new StorageSyncJob$$ExternalSyntheticLambda1()).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return Base64.encodeBytes((byte[]) obj);
                }
            }).collect(Collectors.toSet());
            for (byte[] bArr : writeOperationResult.getDeletes()) {
                if (set.contains(Base64.encodeBytes(bArr))) {
                    throw new DeletePresentInFullIdSetError();
                }
            }
        }
        if (signalStorageManifest.getVersion() == 0) {
            Log.i(TAG, "Previous manifest is empty, not bothering with additional validations around the diffs between the two manifests.");
        } else if (writeOperationResult.getManifest().getVersion() != signalStorageManifest.getVersion() + 1) {
            throw new IncorrectManifestVersionError();
        } else if (z) {
            Log.i(TAG, "Force push pending, not bothering with additional validations around the diffs between the two manifests.");
        } else {
            Set set2 = (Set) Stream.of(signalStorageManifest.getStorageIds()).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return StorageSyncValidations.lambda$validate$0((StorageId) obj);
                }
            }).collect(Collectors.toSet());
            Set set3 = (Set) Stream.of(writeOperationResult.getManifest().getStorageIds()).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return StorageSyncValidations.lambda$validate$1((StorageId) obj);
                }
            }).collect(Collectors.toSet());
            Set difference = SetUtil.difference(set3, set2);
            Set difference2 = SetUtil.difference(set2, set3);
            Set set4 = (Set) Stream.of(writeOperationResult.getInserts()).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda3
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return StorageSyncValidations.lambda$validate$2((SignalStorageRecord) obj);
                }
            }).collect(Collectors.toSet());
            Set set5 = (Set) Stream.of(writeOperationResult.getDeletes()).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda4
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ByteBuffer.wrap((byte[]) obj);
                }
            }).collect(Collectors.toSet());
            if (set4.size() > difference.size()) {
                String str = TAG;
                Log.w(str, "DeclaredInserts: " + set4.size() + ", ManifestInserts: " + difference.size());
                throw new MoreInsertsThanExpectedError();
            } else if (set4.size() < difference.size()) {
                String str2 = TAG;
                Log.w(str2, "DeclaredInserts: " + set4.size() + ", ManifestInserts: " + difference.size());
                throw new LessInsertsThanExpectedError();
            } else if (!set4.containsAll(difference)) {
                throw new InsertMismatchError();
            } else if (set5.size() > difference2.size()) {
                String str3 = TAG;
                Log.w(str3, "DeclaredDeletes: " + set5.size() + ", ManifestDeletes: " + difference2.size());
                throw new MoreDeletesThanExpectedError();
            } else if (set5.size() < difference2.size()) {
                String str4 = TAG;
                Log.w(str4, "DeclaredDeletes: " + set5.size() + ", ManifestDeletes: " + difference2.size());
                throw new LessDeletesThanExpectedError();
            } else if (!set5.containsAll(difference2)) {
                throw new DeleteMismatchError();
            }
        }
    }

    public static /* synthetic */ ByteBuffer lambda$validate$0(StorageId storageId) {
        return ByteBuffer.wrap(storageId.getRaw());
    }

    public static /* synthetic */ ByteBuffer lambda$validate$1(StorageId storageId) {
        return ByteBuffer.wrap(storageId.getRaw());
    }

    public static /* synthetic */ ByteBuffer lambda$validate$2(SignalStorageRecord signalStorageRecord) {
        return ByteBuffer.wrap(signalStorageRecord.getId().getRaw());
    }

    public static void validateForcePush(SignalStorageManifest signalStorageManifest, List<SignalStorageRecord> list, Recipient recipient) {
        validateManifestAndInserts(signalStorageManifest, list, recipient);
    }

    private static void validateManifestAndInserts(SignalStorageManifest signalStorageManifest, List<SignalStorageRecord> list, Recipient recipient) {
        HashSet hashSet = new HashSet(signalStorageManifest.getStorageIds());
        HashSet hashSet2 = new HashSet(Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((SignalStorageRecord) obj).getId();
            }
        }).toList());
        Set set = (Set) Stream.of(hashSet).map(new Function() { // from class: org.thoughtcrime.securesms.storage.StorageSyncValidations$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return StorageSyncValidations.lambda$validateManifestAndInserts$3((StorageId) obj);
            }
        }).collect(Collectors.toSet());
        if (hashSet.size() != signalStorageManifest.getStorageIds().size()) {
            throw new DuplicateStorageIdError();
        } else if (set.size() != hashSet.size()) {
            throw new DuplicateRawIdError();
        } else if (list.size() <= hashSet2.size()) {
            Iterator<StorageId> it = signalStorageManifest.getStorageIds().iterator();
            int i = 0;
            while (true) {
                int i2 = 1;
                if (!it.hasNext()) {
                    break;
                }
                if (it.next().getType() != 4) {
                    i2 = 0;
                }
                i += i2;
            }
            if (i > 1) {
                throw new MultipleAccountError();
            } else if (i != 0) {
                for (SignalStorageRecord signalStorageRecord : list) {
                    if (!hashSet.contains(signalStorageRecord.getId())) {
                        throw new InsertNotPresentInFullIdSetError();
                    } else if (signalStorageRecord.isUnknown()) {
                        throw new UnknownInsertError();
                    } else if (signalStorageRecord.getContact().isPresent()) {
                        SignalServiceAddress address = signalStorageRecord.getContact().get().getAddress();
                        if (recipient.requireE164().equals(address.getNumber().orElse("")) || recipient.requireServiceId().equals(address.getServiceId())) {
                            throw new SelfAddedAsContactError();
                        }
                    }
                }
            } else {
                throw new MissingAccountError();
            }
        } else {
            throw new DuplicateInsertInWriteError();
        }
    }

    public static /* synthetic */ ByteBuffer lambda$validateManifestAndInserts$3(StorageId storageId) {
        return ByteBuffer.wrap(storageId.getRaw());
    }

    /* loaded from: classes4.dex */
    public static final class DuplicateStorageIdError extends Error {
        private DuplicateStorageIdError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class DuplicateRawIdError extends Error {
        private DuplicateRawIdError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class DuplicateInsertInWriteError extends Error {
        private DuplicateInsertInWriteError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class InsertNotPresentInFullIdSetError extends Error {
        private InsertNotPresentInFullIdSetError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class DeletePresentInFullIdSetError extends Error {
        private DeletePresentInFullIdSetError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class UnknownInsertError extends Error {
        private UnknownInsertError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class MultipleAccountError extends Error {
        private MultipleAccountError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class MissingAccountError extends Error {
        private MissingAccountError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class SelfAddedAsContactError extends Error {
        private SelfAddedAsContactError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class IncorrectManifestVersionError extends Error {
        private IncorrectManifestVersionError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class MoreInsertsThanExpectedError extends Error {
        private MoreInsertsThanExpectedError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class LessInsertsThanExpectedError extends Error {
        private LessInsertsThanExpectedError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class InsertMismatchError extends Error {
        private InsertMismatchError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class MoreDeletesThanExpectedError extends Error {
        private MoreDeletesThanExpectedError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class LessDeletesThanExpectedError extends Error {
        private LessDeletesThanExpectedError() {
        }
    }

    /* loaded from: classes4.dex */
    public static final class DeleteMismatchError extends Error {
        private DeleteMismatchError() {
        }
    }
}
