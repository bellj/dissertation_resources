package org.thoughtcrime.securesms.mediaoverview;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.recyclerview.GridDividerDecoration;

/* compiled from: MediaGridDividerDecoration.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J(\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/mediaoverview/MediaGridDividerDecoration;", "Lorg/thoughtcrime/securesms/components/recyclerview/GridDividerDecoration;", "spanCount", "", "space", "adapter", "Lorg/thoughtcrime/securesms/mediaoverview/MediaGalleryAllAdapter;", "(IILorg/thoughtcrime/securesms/mediaoverview/MediaGalleryAllAdapter;)V", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MediaGridDividerDecoration extends GridDividerDecoration {
    private final MediaGalleryAllAdapter adapter;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MediaGridDividerDecoration(int i, int i2, MediaGalleryAllAdapter mediaGalleryAllAdapter) {
        super(i, i2);
        Intrinsics.checkNotNullParameter(mediaGalleryAllAdapter, "adapter");
        this.adapter = mediaGalleryAllAdapter;
    }

    @Override // org.thoughtcrime.securesms.components.recyclerview.GridDividerDecoration, androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        int bindingAdapterPosition = recyclerView.getChildViewHolder(view).getBindingAdapterPosition();
        int adapterPositionSection = this.adapter.getAdapterPositionSection(bindingAdapterPosition);
        int itemSectionOffset = this.adapter.getItemSectionOffset(adapterPositionSection, bindingAdapterPosition);
        if (itemSectionOffset != -1 && this.adapter.getSectionItemViewType(adapterPositionSection, itemSectionOffset) == 2) {
            setItemOffsets(itemSectionOffset, view, rect);
        }
    }
}
