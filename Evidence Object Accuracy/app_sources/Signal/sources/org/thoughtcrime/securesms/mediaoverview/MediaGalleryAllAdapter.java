package org.thoughtcrime.securesms.mediaoverview;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.ToLongFunction;
import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;
import j$.util.Optional;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.AttachmentId;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.components.AudioView;
import org.thoughtcrime.securesms.components.ThumbnailView;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.livedata.LiveDataPair;

/* loaded from: classes4.dex */
public final class MediaGalleryAllAdapter extends StickyHeaderGridAdapter {
    private static final int AUDIO_DETAIL;
    private static final int DOCUMENT_DETAIL;
    public static final int GALLERY;
    private static final int GALLERY_DETAIL;
    private static final int PAYLOAD_SELECTED;
    private static final long SELECTION_ANIMATION_DURATION = TimeUnit.MILLISECONDS.toMillis(150);
    private final AudioItemListener audioItemListener;
    private final Context context;
    private boolean detailView;
    private final GlideRequests glideRequests;
    private final ItemClickListener itemClickListener;
    private GroupedThreadMediaLoader.GroupedThreadMedia media;
    private final Map<AttachmentId, MediaDatabase.MediaRecord> selected = new HashMap();
    private boolean showFileSizes;
    private final boolean showThread;

    /* loaded from: classes4.dex */
    public interface AudioItemListener {
        void onPause(Uri uri);

        void onPlay(Uri uri, double d, long j);

        void onSeekTo(Uri uri, double d);

        void onStopAndReset(Uri uri);

        void registerPlaybackStateObserver(Observer<VoiceNotePlaybackState> observer);

        void unregisterPlaybackStateObserver(Observer<VoiceNotePlaybackState> observer);
    }

    /* loaded from: classes4.dex */
    public interface ItemClickListener {
        void onMediaClicked(MediaDatabase.MediaRecord mediaRecord);

        void onMediaLongClicked(MediaDatabase.MediaRecord mediaRecord);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public /* bridge */ /* synthetic */ void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List list) {
        onBindViewHolder((StickyHeaderGridAdapter.ViewHolder) viewHolder, i, (List<Object>) list);
    }

    public void detach(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof SelectableViewHolder) {
            ((SelectableViewHolder) viewHolder).onDetached();
        }
    }

    /* loaded from: classes4.dex */
    private static class HeaderHolder extends StickyHeaderGridAdapter.HeaderViewHolder {
        TextView textView;

        HeaderHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(R.id.text);
        }
    }

    public MediaGalleryAllAdapter(Context context, GlideRequests glideRequests, GroupedThreadMediaLoader.GroupedThreadMedia groupedThreadMedia, ItemClickListener itemClickListener, AudioItemListener audioItemListener, boolean z, boolean z2) {
        this.context = context;
        this.glideRequests = glideRequests;
        this.media = groupedThreadMedia;
        this.itemClickListener = itemClickListener;
        this.audioItemListener = audioItemListener;
        this.showFileSizes = z;
        this.showThread = z2;
    }

    public void setMedia(GroupedThreadMediaLoader.GroupedThreadMedia groupedThreadMedia) {
        this.media = groupedThreadMedia;
    }

    public StickyHeaderGridAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i) {
        return new HeaderHolder(LayoutInflater.from(this.context).inflate(R.layout.media_overview_item_header, viewGroup, false));
    }

    public StickyHeaderGridAdapter.ItemViewHolder onCreateItemViewHolder(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new AudioDetailViewHolder(LayoutInflater.from(this.context).inflate(R.layout.media_overview_detail_item_audio, viewGroup, false));
        }
        if (i == 2) {
            return new GalleryViewHolder(LayoutInflater.from(this.context).inflate(R.layout.media_overview_gallery_item, viewGroup, false));
        }
        if (i != 3) {
            return new DocumentDetailViewHolder(LayoutInflater.from(this.context).inflate(R.layout.media_overview_detail_item_document, viewGroup, false));
        }
        return new GalleryDetailViewHolder(LayoutInflater.from(this.context).inflate(R.layout.media_overview_detail_item_media, viewGroup, false));
    }

    @Override // com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
    public int getSectionItemViewType(int i, int i2) {
        Slide slideForAttachment = MediaUtil.getSlideForAttachment(this.context, this.media.get(i, i2).getAttachment());
        if (slideForAttachment.hasAudio()) {
            return 1;
        }
        return (slideForAttachment.hasImage() || slideForAttachment.hasVideo()) ? this.detailView ? 3 : 2 : slideForAttachment.hasDocument() ? 4 : 0;
    }

    @Override // com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
    public void onBindHeaderViewHolder(StickyHeaderGridAdapter.HeaderViewHolder headerViewHolder, int i) {
        ((HeaderHolder) headerViewHolder).textView.setText(this.media.getName(i));
    }

    public void onBindViewHolder(StickyHeaderGridAdapter.ViewHolder viewHolder, int i, List<Object> list) {
        if (!(viewHolder instanceof SelectableViewHolder) || !list.contains(1)) {
            super.onBindViewHolder((MediaGalleryAllAdapter) viewHolder, i, list);
        } else {
            ((SelectableViewHolder) viewHolder).animateSelectedView();
        }
    }

    @Override // com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
    public void onBindItemViewHolder(StickyHeaderGridAdapter.ItemViewHolder itemViewHolder, int i, int i2) {
        MediaDatabase.MediaRecord mediaRecord = this.media.get(i, i2);
        ((SelectableViewHolder) itemViewHolder).bind(this.context, mediaRecord, MediaUtil.getSlideForAttachment(this.context, mediaRecord.getAttachment()));
    }

    public void onViewDetachedFromWindow(StickyHeaderGridAdapter.ViewHolder viewHolder) {
        super.onViewDetachedFromWindow((MediaGalleryAllAdapter) viewHolder);
        if (viewHolder instanceof SelectableViewHolder) {
            ((SelectableViewHolder) viewHolder).onDetached();
        }
    }

    @Override // com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
    public int getSectionCount() {
        return this.media.getSectionCount();
    }

    @Override // com.codewaves.stickyheadergrid.StickyHeaderGridAdapter
    public int getSectionItemCount(int i) {
        return this.media.getSectionItemCount(i);
    }

    public void toggleSelection(MediaDatabase.MediaRecord mediaRecord) {
        AttachmentId attachmentId = mediaRecord.getAttachment().getAttachmentId();
        if (this.selected.remove(attachmentId) == null) {
            this.selected.put(attachmentId, mediaRecord);
        }
        notifyItemRangeChanged(0, getItemCount(), 1);
    }

    public int getSelectedMediaCount() {
        return this.selected.size();
    }

    public long getSelectedMediaTotalFileSize() {
        return ((Long) Stream.of(this.selected.values()).collect(Collectors.summingLong(new ToLongFunction() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaGalleryAllAdapter$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.ToLongFunction
            public final long applyAsLong(Object obj) {
                return MediaGalleryAllAdapter.$r8$lambda$UP0cylC_Kk4uEUXcFHbossmeddw((MediaDatabase.MediaRecord) obj);
            }
        }))).longValue();
    }

    public static /* synthetic */ long lambda$getSelectedMediaTotalFileSize$0(MediaDatabase.MediaRecord mediaRecord) {
        return mediaRecord.getAttachment().getSize();
    }

    public Collection<MediaDatabase.MediaRecord> getSelectedMedia() {
        return new HashSet(this.selected.values());
    }

    public void clearSelection() {
        this.selected.clear();
        notifyItemRangeChanged(0, getItemCount(), 1);
    }

    public void selectAllMedia() {
        int sectionCount = this.media.getSectionCount();
        for (int i = 0; i < sectionCount; i++) {
            int sectionItemCount = this.media.getSectionItemCount(i);
            for (int i2 = 0; i2 < sectionItemCount; i2++) {
                MediaDatabase.MediaRecord mediaRecord = this.media.get(i, i2);
                this.selected.put(mediaRecord.getAttachment().getAttachmentId(), mediaRecord);
            }
        }
        notifyItemRangeChanged(0, getItemCount(), 1);
    }

    public void setShowFileSizes(boolean z) {
        this.showFileSizes = z;
    }

    public void setDetailView(boolean z) {
        this.detailView = z;
    }

    /* loaded from: classes4.dex */
    public class SelectableViewHolder extends StickyHeaderGridAdapter.ItemViewHolder {
        private boolean bound;
        private MediaDatabase.MediaRecord mediaRecord;
        protected final View selectedIndicator;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        SelectableViewHolder(View view) {
            super(view);
            MediaGalleryAllAdapter.this = r1;
            this.selectedIndicator = view.findViewById(R.id.selected_indicator);
        }

        public void bind(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            if (this.bound) {
                unbind();
            }
            this.mediaRecord = mediaRecord;
            updateSelectedView();
            this.bound = true;
        }

        void unbind() {
            this.bound = false;
        }

        protected boolean isSelected() {
            return MediaGalleryAllAdapter.this.selected.containsKey(this.mediaRecord.getAttachment().getAttachmentId());
        }

        protected void updateSelectedView() {
            View view = this.selectedIndicator;
            if (view != null) {
                view.animate().cancel();
                this.selectedIndicator.setAlpha(isSelected() ? 1.0f : 0.0f);
            }
        }

        protected void animateSelectedView() {
            View view = this.selectedIndicator;
            if (view != null) {
                view.animate().alpha(isSelected() ? 1.0f : 0.0f).setDuration(MediaGalleryAllAdapter.SELECTION_ANIMATION_DURATION);
            }
        }

        boolean onLongClick() {
            MediaGalleryAllAdapter.this.itemClickListener.onMediaLongClicked(this.mediaRecord);
            return true;
        }

        void onDetached() {
            if (this.bound) {
                unbind();
            }
        }
    }

    /* loaded from: classes4.dex */
    public class GalleryViewHolder extends SelectableViewHolder {
        private static final float SCALE_NORMAL;
        private static final float SCALE_SELECTED;
        private final TextView imageFileSize;
        private final ThumbnailView thumbnailView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        GalleryViewHolder(View view) {
            super(view);
            MediaGalleryAllAdapter.this = r1;
            this.thumbnailView = (ThumbnailView) view.findViewById(R.id.image);
            this.imageFileSize = (TextView) view.findViewById(R.id.image_file_size);
        }

        public void bind(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            super.bind(context, mediaRecord, slide);
            if (MediaGalleryAllAdapter.this.showFileSizes || MediaGalleryAllAdapter.this.detailView) {
                this.imageFileSize.setText(Util.getPrettyFileSize(slide.getFileSize()));
                this.imageFileSize.setVisibility(0);
            } else {
                this.imageFileSize.setVisibility(8);
            }
            this.thumbnailView.setImageResource(MediaGalleryAllAdapter.this.glideRequests, slide, false, false);
            this.thumbnailView.setOnClickListener(new MediaGalleryAllAdapter$GalleryViewHolder$$ExternalSyntheticLambda0(this, mediaRecord));
            this.thumbnailView.setOnLongClickListener(new MediaGalleryAllAdapter$GalleryViewHolder$$ExternalSyntheticLambda1(this));
        }

        public /* synthetic */ void lambda$bind$0(MediaDatabase.MediaRecord mediaRecord, View view) {
            MediaGalleryAllAdapter.this.itemClickListener.onMediaClicked(mediaRecord);
        }

        public /* synthetic */ boolean lambda$bind$1(View view) {
            return onLongClick();
        }

        protected void updateSelectedView() {
            super.updateSelectedView();
            this.thumbnailView.animate().cancel();
            float f = isSelected() ? SCALE_SELECTED : SCALE_NORMAL;
            this.thumbnailView.setScaleX(f);
            this.thumbnailView.setScaleY(f);
        }

        void unbind() {
            this.thumbnailView.clear(MediaGalleryAllAdapter.this.glideRequests);
            super.unbind();
        }

        public void animateSelectedView() {
            super.animateSelectedView();
            float f = isSelected() ? SCALE_SELECTED : SCALE_NORMAL;
            this.thumbnailView.animate().scaleX(f).scaleY(f).setDuration(MediaGalleryAllAdapter.SELECTION_ANIMATION_DURATION);
        }
    }

    /* loaded from: classes4.dex */
    public abstract class DetailViewHolder extends SelectableViewHolder implements Observer<Pair<Recipient, Recipient>> {
        private Optional<String> fileName;
        private String fileTypeDescription;
        private Handler handler;
        protected final View itemView;
        private final TextView line1;
        private final TextView line2;
        private LiveDataPair<Recipient, Recipient> liveDataPair;
        private Runnable selectForMarque;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        DetailViewHolder(View view) {
            super(view);
            MediaGalleryAllAdapter.this = r1;
            this.line1 = (TextView) view.findViewById(R.id.line1);
            this.line2 = (TextView) view.findViewById(R.id.line2);
            this.itemView = view;
        }

        public void bind(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            super.bind(context, mediaRecord, slide);
            this.fileName = slide.getFileName();
            String fileTypeDescription = getFileTypeDescription(context, slide);
            this.fileTypeDescription = fileTypeDescription;
            this.line1.setText(this.fileName.orElse(fileTypeDescription));
            this.line2.setText(getLine2(context, mediaRecord, slide));
            this.itemView.setOnClickListener(new MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda0(this, mediaRecord));
            this.itemView.setOnLongClickListener(new MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda1(this));
            this.selectForMarque = new MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda2(this);
            Handler handler = new Handler(Looper.getMainLooper());
            this.handler = handler;
            handler.postDelayed(this.selectForMarque, 2500);
            LiveRecipient live = mediaRecord.isOutgoing() ? Recipient.self().live() : Recipient.live(mediaRecord.getRecipientId());
            LiveRecipient live2 = Recipient.live(mediaRecord.getThreadRecipientId());
            LiveData<Recipient> liveData = live.getLiveData();
            LiveData<Recipient> liveData2 = live2.getLiveData();
            Recipient recipient = Recipient.UNKNOWN;
            LiveDataPair<Recipient, Recipient> liveDataPair = new LiveDataPair<>(liveData, liveData2, recipient, recipient);
            this.liveDataPair = liveDataPair;
            liveDataPair.observeForever(this);
        }

        public /* synthetic */ void lambda$bind$0(MediaDatabase.MediaRecord mediaRecord, View view) {
            MediaGalleryAllAdapter.this.itemClickListener.onMediaClicked(mediaRecord);
        }

        public /* synthetic */ boolean lambda$bind$1(View view) {
            return onLongClick();
        }

        public /* synthetic */ void lambda$bind$2() {
            this.line1.setSelected(true);
        }

        void unbind() {
            this.liveDataPair.removeObserver(this);
            this.handler.removeCallbacks(this.selectForMarque);
            this.line1.setSelected(false);
            super.unbind();
        }

        private String getLine2(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            return context.getString(R.string.MediaOverviewActivity_detail_line_3_part, Util.getPrettyFileSize(slide.getFileSize()), getFileTypeDescription(context, slide), DateUtils.formatDateWithoutDayOfWeek(Locale.getDefault(), mediaRecord.getDate()));
        }

        protected String getFileTypeDescription(Context context, Slide slide) {
            return context.getString(R.string.MediaOverviewActivity_file);
        }

        public void onChanged(Pair<Recipient, Recipient> pair) {
            this.line1.setText(describe(pair.first(), pair.second()));
        }

        protected String getMediaTitle() {
            return this.fileName.orElse(null);
        }

        private String describe(Recipient recipient, Recipient recipient2) {
            Recipient recipient3 = Recipient.UNKNOWN;
            if (recipient == recipient3 && recipient2 == recipient3) {
                return this.fileName.orElse(this.fileTypeDescription);
            }
            String sentFromToString = getSentFromToString(recipient, recipient2);
            String mediaTitle = getMediaTitle();
            return mediaTitle != null ? MediaGalleryAllAdapter.this.context.getString(R.string.MediaOverviewActivity_detail_line_2_part, mediaTitle, sentFromToString) : sentFromToString;
        }

        private String getSentFromToString(Recipient recipient, Recipient recipient2) {
            if (recipient.isSelf() && recipient == recipient2) {
                return MediaGalleryAllAdapter.this.context.getString(R.string.note_to_self);
            }
            if (!MediaGalleryAllAdapter.this.showThread || (!recipient.isSelf() && !recipient2.isGroup())) {
                if (recipient.isSelf()) {
                    return MediaGalleryAllAdapter.this.context.getString(R.string.MediaOverviewActivity_sent_by_you);
                }
                return MediaGalleryAllAdapter.this.context.getString(R.string.MediaOverviewActivity_sent_by_s, recipient.getDisplayName(MediaGalleryAllAdapter.this.context));
            } else if (recipient.isSelf()) {
                return MediaGalleryAllAdapter.this.context.getString(R.string.MediaOverviewActivity_sent_by_you_to_s, recipient2.getDisplayName(MediaGalleryAllAdapter.this.context));
            } else {
                return MediaGalleryAllAdapter.this.context.getString(R.string.MediaOverviewActivity_sent_by_s_to_s, recipient.getDisplayName(MediaGalleryAllAdapter.this.context), recipient2.getDisplayName(MediaGalleryAllAdapter.this.context));
            }
        }
    }

    /* loaded from: classes4.dex */
    private class DocumentDetailViewHolder extends DetailViewHolder {
        private final TextView documentType;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        DocumentDetailViewHolder(View view) {
            super(view);
            MediaGalleryAllAdapter.this = r1;
            this.documentType = (TextView) view.findViewById(R.id.document_extension);
        }

        public void bind(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            super.bind(context, mediaRecord, slide);
            this.documentType.setText(slide.getFileType(context).orElse("").toLowerCase());
        }
    }

    /* loaded from: classes4.dex */
    public class AudioDetailViewHolder extends DetailViewHolder {
        private final AudioView audioView;
        private boolean isVoiceNote;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        AudioDetailViewHolder(View view) {
            super(view);
            MediaGalleryAllAdapter.this = r1;
            this.audioView = (AudioView) view.findViewById(R.id.audio);
        }

        public void bind(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            if (slide.hasAudio()) {
                this.isVoiceNote = slide.asAttachment().isVoiceNote();
                super.bind(context, mediaRecord, slide);
                DatabaseAttachment attachment = mediaRecord.getAttachment();
                Objects.requireNonNull(attachment);
                long mmsId = attachment.getMmsId();
                MediaGalleryAllAdapter.this.audioItemListener.unregisterPlaybackStateObserver(this.audioView.getPlaybackStateObserver());
                this.audioView.setAudio((AudioSlide) slide, new AudioViewCallbacksAdapter(MediaGalleryAllAdapter.this.audioItemListener, mmsId), true, true);
                MediaGalleryAllAdapter.this.audioItemListener.registerPlaybackStateObserver(this.audioView.getPlaybackStateObserver());
                this.audioView.setOnClickListener(new MediaGalleryAllAdapter$AudioDetailViewHolder$$ExternalSyntheticLambda0(this, mediaRecord));
                ((DetailViewHolder) this).itemView.setOnClickListener(new MediaGalleryAllAdapter$AudioDetailViewHolder$$ExternalSyntheticLambda1(this, mediaRecord));
                return;
            }
            throw new AssertionError();
        }

        public /* synthetic */ void lambda$bind$0(MediaDatabase.MediaRecord mediaRecord, View view) {
            MediaGalleryAllAdapter.this.itemClickListener.onMediaClicked(mediaRecord);
        }

        public /* synthetic */ void lambda$bind$1(MediaDatabase.MediaRecord mediaRecord, View view) {
            MediaGalleryAllAdapter.this.itemClickListener.onMediaClicked(mediaRecord);
        }

        protected String getMediaTitle() {
            return MediaGalleryAllAdapter.this.context.getString(R.string.ThreadRecord_voice_message);
        }

        void unbind() {
            super.unbind();
            MediaGalleryAllAdapter.this.audioItemListener.unregisterPlaybackStateObserver(this.audioView.getPlaybackStateObserver());
        }

        protected String getFileTypeDescription(Context context, Slide slide) {
            return context.getString(R.string.MediaOverviewActivity_audio);
        }
    }

    /* loaded from: classes4.dex */
    public class GalleryDetailViewHolder extends DetailViewHolder {
        private final ThumbnailView thumbnailView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        GalleryDetailViewHolder(View view) {
            super(view);
            MediaGalleryAllAdapter.this = r1;
            this.thumbnailView = (ThumbnailView) view.findViewById(R.id.image);
        }

        public void bind(Context context, MediaDatabase.MediaRecord mediaRecord, Slide slide) {
            super.bind(context, mediaRecord, slide);
            this.thumbnailView.setImageResource(MediaGalleryAllAdapter.this.glideRequests, slide, false, false);
            this.thumbnailView.setOnClickListener(new MediaGalleryAllAdapter$GalleryDetailViewHolder$$ExternalSyntheticLambda0(this, mediaRecord));
            this.thumbnailView.setOnLongClickListener(new MediaGalleryAllAdapter$GalleryDetailViewHolder$$ExternalSyntheticLambda1(this));
        }

        public /* synthetic */ void lambda$bind$0(MediaDatabase.MediaRecord mediaRecord, View view) {
            MediaGalleryAllAdapter.this.itemClickListener.onMediaClicked(mediaRecord);
        }

        public /* synthetic */ boolean lambda$bind$1(View view) {
            return onLongClick();
        }

        protected String getFileTypeDescription(Context context, Slide slide) {
            if (slide.hasVideo()) {
                return context.getString(R.string.MediaOverviewActivity_video);
            }
            if (slide.hasImage()) {
                return context.getString(R.string.MediaOverviewActivity_image);
            }
            return super.getFileTypeDescription(context, slide);
        }

        void unbind() {
            this.thumbnailView.clear(MediaGalleryAllAdapter.this.glideRequests);
            super.unbind();
        }
    }

    /* loaded from: classes4.dex */
    private static final class AudioViewCallbacksAdapter implements AudioView.Callbacks {
        private final AudioItemListener audioItemListener;
        private final long messageId;

        public void onProgressUpdated(long j, long j2) {
        }

        public void onSpeedChanged(float f, boolean z) {
        }

        private AudioViewCallbacksAdapter(AudioItemListener audioItemListener, long j) {
            this.audioItemListener = audioItemListener;
            this.messageId = j;
        }

        public void onPlay(Uri uri, double d) {
            this.audioItemListener.onPlay(uri, d, this.messageId);
        }

        public void onPause(Uri uri) {
            this.audioItemListener.onPause(uri);
        }

        public void onSeekTo(Uri uri, double d) {
            this.audioItemListener.onSeekTo(uri, d);
        }

        public void onStopAndReset(Uri uri) {
            this.audioItemListener.onStopAndReset(uri);
        }
    }
}
