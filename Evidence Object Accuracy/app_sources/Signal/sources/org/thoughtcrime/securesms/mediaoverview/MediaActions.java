package org.thoughtcrime.securesms.mediaoverview;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.AttachmentUtil;
import org.thoughtcrime.securesms.util.SaveAttachmentTask;
import org.thoughtcrime.securesms.util.StorageUtil;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public final class MediaActions {
    private MediaActions() {
    }

    public static void handleSaveMedia(Fragment fragment, Collection<MediaDatabase.MediaRecord> collection, Runnable runnable) {
        Context requireContext = fragment.requireContext();
        if (StorageUtil.canWriteToMediaStore()) {
            performSaveToDisk(requireContext, collection, runnable);
        } else {
            SaveAttachmentTask.showWarningDialog(requireContext, new DialogInterface.OnClickListener(requireContext, collection, runnable) { // from class: org.thoughtcrime.securesms.mediaoverview.MediaActions$$ExternalSyntheticLambda0
                public final /* synthetic */ Context f$1;
                public final /* synthetic */ Collection f$2;
                public final /* synthetic */ Runnable f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    MediaActions.$r8$lambda$ghlxaX32_SuV3708IeX7L48CNSI(Fragment.this, this.f$1, this.f$2, this.f$3, dialogInterface, i);
                }
            }, collection.size());
        }
    }

    public static /* synthetic */ void lambda$handleSaveMedia$2(Fragment fragment, Context context, Collection collection, Runnable runnable, DialogInterface dialogInterface, int i) {
        Permissions.with(fragment).request("android.permission.WRITE_EXTERNAL_STORAGE").ifNecessary().withPermanentDenialDialog(fragment.getString(R.string.MediaPreviewActivity_signal_needs_the_storage_permission_in_order_to_write_to_external_storage_but_it_has_been_permanently_denied)).onAnyDenied(new Runnable(context) { // from class: org.thoughtcrime.securesms.mediaoverview.MediaActions$$ExternalSyntheticLambda2
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaActions.$r8$lambda$iTYRkP3NlUAQEFpvwHKyspFNMLk(this.f$0);
            }
        }).onAllGranted(new Runnable(context, collection, runnable) { // from class: org.thoughtcrime.securesms.mediaoverview.MediaActions$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Collection f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MediaActions.m2048$r8$lambda$HEhmzAilmsaOOPwgjOgDthnwh8(this.f$0, this.f$1, this.f$2);
            }
        }).execute();
    }

    public static /* synthetic */ void lambda$handleSaveMedia$0(Context context) {
        Toast.makeText(context, (int) R.string.MediaPreviewActivity_unable_to_write_to_external_storage_without_permission, 1).show();
    }

    public static void handleDeleteMedia(Context context, Collection<MediaDatabase.MediaRecord> collection) {
        int size = collection.size();
        Resources resources = context.getResources();
        MaterialAlertDialogBuilder cancelable = new MaterialAlertDialogBuilder(context).setTitle((CharSequence) resources.getQuantityString(R.plurals.MediaOverviewActivity_Media_delete_confirm_title, size, Integer.valueOf(size))).setMessage((CharSequence) resources.getQuantityString(R.plurals.MediaOverviewActivity_Media_delete_confirm_message, size, Integer.valueOf(size))).setCancelable(true);
        cancelable.setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(context, collection) { // from class: org.thoughtcrime.securesms.mediaoverview.MediaActions$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ Collection f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MediaActions.$r8$lambda$lCHvPvKXkuF7L2z53FeVymEm1TQ(this.f$0, this.f$1, dialogInterface, i);
            }
        });
        cancelable.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        cancelable.show();
    }

    public static /* synthetic */ void lambda$handleDeleteMedia$3(final Context context, Collection collection, DialogInterface dialogInterface, int i) {
        new ProgressDialogAsyncTask<MediaDatabase.MediaRecord, Void, Void>(R.string.MediaOverviewActivity_Media_delete_progress_title, R.string.MediaOverviewActivity_Media_delete_progress_message, context) { // from class: org.thoughtcrime.securesms.mediaoverview.MediaActions.1
            public Void doInBackground(MediaDatabase.MediaRecord... mediaRecordArr) {
                if (!(mediaRecordArr == null || mediaRecordArr.length == 0)) {
                    for (MediaDatabase.MediaRecord mediaRecord : mediaRecordArr) {
                        AttachmentUtil.deleteAttachment(context, mediaRecord.getAttachment());
                    }
                }
                return null;
            }
        }.execute((MediaDatabase.MediaRecord[]) collection.toArray(new MediaDatabase.MediaRecord[0]));
    }

    public static void performSaveToDisk(final Context context, final Collection<MediaDatabase.MediaRecord> collection, final Runnable runnable) {
        new ProgressDialogAsyncTask<Void, Void, List<SaveAttachmentTask.Attachment>>(R.string.MediaOverviewActivity_collecting_attachments, R.string.please_wait, context) { // from class: org.thoughtcrime.securesms.mediaoverview.MediaActions.2
            public List<SaveAttachmentTask.Attachment> doInBackground(Void... voidArr) {
                LinkedList linkedList = new LinkedList();
                for (MediaDatabase.MediaRecord mediaRecord : collection) {
                    if (mediaRecord.getAttachment().getUri() != null) {
                        linkedList.add(new SaveAttachmentTask.Attachment(mediaRecord.getAttachment().getUri(), mediaRecord.getContentType(), mediaRecord.getDate(), mediaRecord.getAttachment().getFileName()));
                    }
                }
                return linkedList;
            }

            public void onPostExecute(List<SaveAttachmentTask.Attachment> list) {
                super.onPostExecute((AnonymousClass2) list);
                new SaveAttachmentTask(context, list.size()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (SaveAttachmentTask.Attachment[]) list.toArray(new SaveAttachmentTask.Attachment[0]));
                Runnable runnable2 = runnable;
                if (runnable2 != null) {
                    runnable2.run();
                }
            }
        }.execute(new Void[0]);
    }
}
