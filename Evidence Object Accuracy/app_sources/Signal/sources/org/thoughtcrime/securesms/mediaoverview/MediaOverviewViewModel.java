package org.thoughtcrime.securesms.mediaoverview;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import org.thoughtcrime.securesms.database.MediaDatabase;

/* loaded from: classes4.dex */
public class MediaOverviewViewModel extends ViewModel {
    private final MutableLiveData<Boolean> detailLayout;
    private final MutableLiveData<MediaDatabase.Sorting> sortOrder;

    public MediaOverviewViewModel(SavedStateHandle savedStateHandle) {
        this.sortOrder = savedStateHandle.getLiveData("SORT_ORDER", MediaDatabase.Sorting.Newest);
        this.detailLayout = savedStateHandle.getLiveData("DETAIL_LAYOUT", Boolean.FALSE);
    }

    public LiveData<MediaDatabase.Sorting> getSortOrder() {
        return this.sortOrder;
    }

    public LiveData<Boolean> getDetailLayout() {
        return this.detailLayout;
    }

    public void setSortOrder(MediaDatabase.Sorting sorting) {
        this.sortOrder.setValue(sorting);
    }

    public void setDetailLayout(boolean z) {
        this.detailLayout.setValue(Boolean.valueOf(z));
    }

    public static MediaOverviewViewModel getMediaOverviewViewModel(FragmentActivity fragmentActivity) {
        return (MediaOverviewViewModel) ViewModelProviders.of(fragmentActivity, new SavedStateViewModelFactory(fragmentActivity.getApplication(), fragmentActivity)).get(MediaOverviewViewModel.class);
    }
}
