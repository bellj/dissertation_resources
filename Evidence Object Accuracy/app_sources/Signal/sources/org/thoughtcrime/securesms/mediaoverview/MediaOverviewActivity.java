package org.thoughtcrime.securesms.mediaoverview;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.Observer;
import androidx.viewpager.widget.ViewPager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AnimatingToggle;
import org.thoughtcrime.securesms.components.BoldSelectionTabItem;
import org.thoughtcrime.securesms.components.ControllableTabLayout;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.loaders.MediaLoader;
import org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public final class MediaOverviewActivity extends PassphraseRequiredActivity {
    private static final String THREAD_ID_EXTRA;
    private Boolean currentDetailLayout;
    private MediaDatabase.Sorting currentSorting;
    private AnimatingToggle displayToggle;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private MediaOverviewViewModel model;
    private TextView sortOrder;
    private View sortOrderArrow;
    private ControllableTabLayout tabLayout;
    private long threadId;
    private Toolbar toolbar;
    private View viewDetail;
    private View viewGrid;
    private ViewPager viewPager;

    public static boolean allowGridSelectionOnPage(int i) {
        return i == 0;
    }

    public static Intent forThread(Context context, long j) {
        Intent intent = new Intent(context, MediaOverviewActivity.class);
        intent.putExtra("thread_id", j);
        return intent;
    }

    public static Intent forAll(Context context) {
        return forThread(context, -1);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.media_overview_activity);
        initializeResources();
        initializeToolbar();
        int i = 0;
        boolean z2 = this.threadId == -1;
        BoldSelectionTabItem.registerListeners(this.tabLayout);
        fillTabLayoutIfFits(this.tabLayout);
        this.tabLayout.setupWithViewPager(this.viewPager);
        this.viewPager.setAdapter(new MediaOverviewPagerAdapter(getSupportFragmentManager()));
        MediaOverviewViewModel mediaOverviewViewModel = MediaOverviewViewModel.getMediaOverviewViewModel(this);
        this.model = mediaOverviewViewModel;
        mediaOverviewViewModel.setSortOrder(z2 ? MediaDatabase.Sorting.Largest : MediaDatabase.Sorting.Newest);
        this.model.setDetailLayout(z2);
        this.model.getSortOrder().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediaOverviewActivity.this.setSorting((MediaDatabase.Sorting) obj);
            }
        });
        this.model.getDetailLayout().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediaOverviewActivity.this.setDetailLayout((Boolean) obj);
            }
        });
        this.sortOrder.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MediaOverviewActivity.this.showSortOrderDialog(view);
            }
        });
        this.sortOrderArrow.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MediaOverviewActivity.this.showSortOrderDialog(view);
            }
        });
        this.displayToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MediaOverviewActivity.this.lambda$onCreate$0(view);
            }
        });
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity.1
            @Override // androidx.viewpager.widget.ViewPager.SimpleOnPageChangeListener, androidx.viewpager.widget.ViewPager.OnPageChangeListener
            public void onPageSelected(int i2) {
                boolean allowGridSelectionOnPage = MediaOverviewActivity.allowGridSelectionOnPage(i2);
                MediaOverviewActivity.this.displayToggle.animate().alpha(allowGridSelectionOnPage ? 1.0f : 0.0f).start();
                MediaOverviewActivity.this.displayToggle.setEnabled(allowGridSelectionOnPage);
            }
        });
        ViewPager viewPager = this.viewPager;
        if (z2) {
            i = 3;
        }
        viewPager.setCurrentItem(i);
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        setDetailLayout(Boolean.valueOf(!this.currentDetailLayout.booleanValue()));
    }

    public void setSorting(MediaDatabase.Sorting sorting) {
        if (this.currentSorting != sorting) {
            this.sortOrder.setText(sortingToString(sorting));
            this.currentSorting = sorting;
            this.model.setSortOrder(sorting);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$MediaDatabase$Sorting;

        static {
            int[] iArr = new int[MediaDatabase.Sorting.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$MediaDatabase$Sorting = iArr;
            try {
                iArr[MediaDatabase.Sorting.Oldest.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$MediaDatabase$Sorting[MediaDatabase.Sorting.Newest.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$MediaDatabase$Sorting[MediaDatabase.Sorting.Largest.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private static int sortingToString(MediaDatabase.Sorting sorting) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$database$MediaDatabase$Sorting[sorting.ordinal()];
        if (i == 1) {
            return R.string.MediaOverviewActivity_Oldest;
        }
        if (i == 2) {
            return R.string.MediaOverviewActivity_Newest;
        }
        if (i == 3) {
            return R.string.MediaOverviewActivity_Storage_used;
        }
        throw new AssertionError();
    }

    public void setDetailLayout(Boolean bool) {
        if (this.currentDetailLayout != bool) {
            this.currentDetailLayout = bool;
            this.model.setDetailLayout(bool.booleanValue());
            this.displayToggle.display(bool.booleanValue() ? this.viewGrid : this.viewDetail);
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        if (menuItem.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    private void initializeResources() {
        long longExtra = getIntent().getLongExtra("thread_id", Long.MIN_VALUE);
        if (longExtra != Long.MIN_VALUE) {
            this.viewPager = (ViewPager) findViewById(R.id.pager);
            this.toolbar = (Toolbar) findViewById(R.id.toolbar);
            this.tabLayout = (ControllableTabLayout) findViewById(R.id.tab_layout);
            this.sortOrder = (TextView) findViewById(R.id.sort_order);
            this.sortOrderArrow = findViewById(R.id.sort_order_arrow);
            this.displayToggle = (AnimatingToggle) findViewById(R.id.grid_list_toggle);
            this.viewDetail = findViewById(R.id.view_detail);
            this.viewGrid = findViewById(R.id.view_grid);
            this.threadId = longExtra;
            return;
        }
        throw new AssertionError();
    }

    private void initializeToolbar() {
        setSupportActionBar(this.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (this.threadId == -1) {
            getSupportActionBar().setTitle(R.string.MediaOverviewActivity_All_storage_use);
        } else {
            SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda4
                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return MediaOverviewActivity.this.lambda$initializeToolbar$1();
                }
            }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda5
                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    MediaOverviewActivity.this.lambda$initializeToolbar$3((Recipient) obj);
                }
            });
        }
    }

    public /* synthetic */ Recipient lambda$initializeToolbar$1() {
        return SignalDatabase.threads().getRecipientForThreadId(this.threadId);
    }

    public /* synthetic */ void lambda$initializeToolbar$3(Recipient recipient) {
        if (recipient != null) {
            getSupportActionBar().setTitle(recipient.getDisplayName(this));
            recipient.live().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda6
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    MediaOverviewActivity.this.lambda$initializeToolbar$2((Recipient) obj);
                }
            });
        }
    }

    public /* synthetic */ void lambda$initializeToolbar$2(Recipient recipient) {
        getSupportActionBar().setTitle(recipient.getDisplayName(this));
    }

    public void onEnterMultiSelect() {
        this.tabLayout.setEnabled(false);
        this.viewPager.setEnabled(false);
        this.toolbar.setVisibility(4);
    }

    public void onExitMultiSelect() {
        this.tabLayout.setEnabled(true);
        this.viewPager.setEnabled(true);
        this.toolbar.setVisibility(0);
    }

    public void showSortOrderDialog(View view) {
        new MaterialAlertDialogBuilder(this).setTitle(R.string.MediaOverviewActivity_Sort_by).setSingleChoiceItems(R.array.MediaOverviewActivity_Sort_by, this.currentSorting.ordinal(), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MediaOverviewActivity.this.lambda$showSortOrderDialog$4(dialogInterface, i);
            }
        }).create().show();
    }

    public /* synthetic */ void lambda$showSortOrderDialog$4(DialogInterface dialogInterface, int i) {
        setSorting(MediaDatabase.Sorting.values()[i]);
        dialogInterface.dismiss();
    }

    private static void fillTabLayoutIfFits(TabLayout tabLayout) {
        tabLayout.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity$$ExternalSyntheticLambda8
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                MediaOverviewActivity.lambda$fillTabLayoutIfFits$5(TabLayout.this, view, i, i2, i3, i4, i5, i6, i7, i8);
            }
        });
    }

    public static /* synthetic */ void lambda$fillTabLayoutIfFits$5(TabLayout tabLayout, View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int i9 = 0;
        int i10 = 0;
        for (int i11 = 0; i11 < tabLayout.getTabCount(); i11++) {
            int width = viewGroup.getChildAt(i11).getWidth();
            i9 += width;
            i10 = Math.max(i10, width);
        }
        if (i9 < i3 - i) {
            tabLayout.setTabMode(1);
        }
    }

    /* loaded from: classes4.dex */
    private class MediaOverviewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Pair<MediaLoader.MediaType, CharSequence>> pages;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        MediaOverviewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager, 1);
            MediaOverviewActivity.this = r4;
            ArrayList arrayList = new ArrayList(4);
            this.pages = arrayList;
            arrayList.add(new Pair(MediaLoader.MediaType.GALLERY, r4.getString(R.string.MediaOverviewActivity_Media)));
            arrayList.add(new Pair(MediaLoader.MediaType.DOCUMENT, r4.getString(R.string.MediaOverviewActivity_Files)));
            arrayList.add(new Pair(MediaLoader.MediaType.AUDIO, r4.getString(R.string.MediaOverviewActivity_Audio)));
            arrayList.add(new Pair(MediaLoader.MediaType.ALL, r4.getString(R.string.MediaOverviewActivity_All)));
        }

        @Override // androidx.fragment.app.FragmentStatePagerAdapter
        public Fragment getItem(int i) {
            MediaOverviewPageFragment.GridMode gridMode;
            if (MediaOverviewActivity.allowGridSelectionOnPage(i)) {
                gridMode = MediaOverviewPageFragment.GridMode.FOLLOW_MODEL;
            } else {
                gridMode = MediaOverviewPageFragment.GridMode.FIXED_DETAIL;
            }
            return MediaOverviewPageFragment.newInstance(MediaOverviewActivity.this.threadId, this.pages.get(i).first(), gridMode);
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public int getCount() {
            return this.pages.size();
        }

        @Override // androidx.viewpager.widget.PagerAdapter
        public CharSequence getPageTitle(int i) {
            return this.pages.get(i).second();
        }
    }
}
