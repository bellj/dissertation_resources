package org.thoughtcrime.securesms.mediaoverview;

import org.thoughtcrime.securesms.mediaoverview.MediaGalleryAllAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ MediaGalleryAllAdapter.DetailViewHolder f$0;

    public /* synthetic */ MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda2(MediaGalleryAllAdapter.DetailViewHolder detailViewHolder) {
        this.f$0 = detailViewHolder;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$bind$2();
    }
}
