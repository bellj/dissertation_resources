package org.thoughtcrime.securesms.mediaoverview;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.RecyclerView;
import com.codewaves.stickyheadergrid.StickyHeaderGridLayoutManager;
import java.util.Arrays;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalBottomActionBar;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.loaders.GroupedThreadMediaLoader;
import org.thoughtcrime.securesms.database.loaders.MediaLoader;
import org.thoughtcrime.securesms.mediaoverview.MediaGalleryAllAdapter;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class MediaOverviewPageFragment extends Fragment implements MediaGalleryAllAdapter.ItemClickListener, MediaGalleryAllAdapter.AudioItemListener, LoaderManager.LoaderCallbacks<GroupedThreadMediaLoader.GroupedThreadMedia> {
    private static final String GRID_MODE;
    private static final String MEDIA_TYPE_EXTRA;
    private static final String TAG = Log.tag(MediaOverviewPageFragment.class);
    private static final String THREAD_ID_EXTRA;
    private ActionMode actionMode;
    private final ActionModeCallback actionModeCallback = new ActionModeCallback();
    private MediaGalleryAllAdapter adapter;
    private SignalBottomActionBar bottomActionBar;
    private boolean detail;
    private StickyHeaderGridLayoutManager gridManager;
    private GridMode gridMode;
    private MediaLoader.MediaType mediaType = MediaLoader.MediaType.GALLERY;
    private TextView noMedia;
    private RecyclerView recyclerView;
    private MediaDatabase.Sorting sorting = MediaDatabase.Sorting.Newest;
    private long threadId;
    private VoiceNoteMediaController voiceNoteMediaController;

    /* loaded from: classes4.dex */
    public enum GridMode {
        FIXED_DETAIL,
        FOLLOW_MODEL
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<GroupedThreadMediaLoader.GroupedThreadMedia>) loader, (GroupedThreadMediaLoader.GroupedThreadMedia) obj);
    }

    public static Fragment newInstance(long j, MediaLoader.MediaType mediaType, GridMode gridMode) {
        MediaOverviewPageFragment mediaOverviewPageFragment = new MediaOverviewPageFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("thread_id", j);
        bundle.putInt(MEDIA_TYPE_EXTRA, mediaType.ordinal());
        bundle.putInt(GRID_MODE, gridMode.ordinal());
        mediaOverviewPageFragment.setArguments(bundle);
        return mediaOverviewPageFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle requireArguments = requireArguments();
        this.threadId = requireArguments.getLong("thread_id", Long.MIN_VALUE);
        this.mediaType = MediaLoader.MediaType.values()[requireArguments.getInt(MEDIA_TYPE_EXTRA)];
        this.gridMode = GridMode.values()[requireArguments.getInt(GRID_MODE)];
        if (this.threadId != Long.MIN_VALUE) {
            LoaderManager.getInstance(this).initLoader(0, null, this);
            return;
        }
        throw new AssertionError();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.voiceNoteMediaController = new VoiceNoteMediaController((AppCompatActivity) requireActivity());
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Context requireContext = requireContext();
        boolean z = false;
        View inflate = layoutInflater.inflate(R.layout.media_overview_page_fragment, viewGroup, false);
        int integer = getResources().getInteger(R.integer.media_overview_cols);
        this.recyclerView = (RecyclerView) inflate.findViewById(R.id.media_grid);
        this.noMedia = (TextView) inflate.findViewById(R.id.no_images);
        this.bottomActionBar = (SignalBottomActionBar) inflate.findViewById(R.id.media_overview_bottom_action_bar);
        this.gridManager = new StickyHeaderGridLayoutManager(integer);
        MediaGalleryAllAdapter mediaGalleryAllAdapter = new MediaGalleryAllAdapter(requireContext, GlideApp.with(this), new GroupedThreadMediaLoader.EmptyGroupedThreadMedia(), this, this, this.sorting.isRelatedToFileSize(), this.threadId == -1);
        this.adapter = mediaGalleryAllAdapter;
        this.recyclerView.setAdapter(mediaGalleryAllAdapter);
        this.recyclerView.setLayoutManager(this.gridManager);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.addItemDecoration(new MediaGridDividerDecoration(integer, ViewUtil.dpToPx(4), this.adapter));
        MediaOverviewViewModel mediaOverviewViewModel = MediaOverviewViewModel.getMediaOverviewViewModel(requireActivity());
        mediaOverviewViewModel.getSortOrder().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MediaOverviewPageFragment.this.lambda$onCreateView$0((MediaDatabase.Sorting) obj);
            }
        });
        GridMode gridMode = this.gridMode;
        if (gridMode == GridMode.FOLLOW_MODEL) {
            mediaOverviewViewModel.getDetailLayout().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment$$ExternalSyntheticLambda4
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    MediaOverviewPageFragment.this.setDetailView(((Boolean) obj).booleanValue());
                }
            });
        } else {
            if (gridMode == GridMode.FIXED_DETAIL) {
                z = true;
            }
            setDetailView(z);
        }
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(MediaDatabase.Sorting sorting) {
        if (sorting != null) {
            this.sorting = sorting;
            this.adapter.setShowFileSizes(sorting.isRelatedToFileSize());
            LoaderManager.getInstance(this).restartLoader(0, null, this);
            updateMultiSelect();
        }
    }

    public void setDetailView(boolean z) {
        this.detail = z;
        this.adapter.setDetailView(z);
        refreshLayoutManager();
        updateMultiSelect();
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.gridManager != null) {
            refreshLayoutManager();
        }
    }

    private void refreshLayoutManager() {
        StickyHeaderGridLayoutManager stickyHeaderGridLayoutManager = new StickyHeaderGridLayoutManager(this.detail ? 1 : getResources().getInteger(R.integer.media_overview_cols));
        this.gridManager = stickyHeaderGridLayoutManager;
        this.recyclerView.setLayoutManager(stickyHeaderGridLayoutManager);
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public Loader<GroupedThreadMediaLoader.GroupedThreadMedia> onCreateLoader(int i, Bundle bundle) {
        return new GroupedThreadMediaLoader(requireContext(), this.threadId, this.mediaType, this.sorting);
    }

    public void onLoadFinished(Loader<GroupedThreadMediaLoader.GroupedThreadMedia> loader, GroupedThreadMediaLoader.GroupedThreadMedia groupedThreadMedia) {
        ((MediaGalleryAllAdapter) this.recyclerView.getAdapter()).setMedia(groupedThreadMedia);
        ((MediaGalleryAllAdapter) this.recyclerView.getAdapter()).notifyAllSectionsDataSetChanged();
        this.noMedia.setVisibility(this.recyclerView.getAdapter().getItemCount() > 0 ? 8 : 0);
        getActivity().invalidateOptionsMenu();
    }

    public void onLoaderReset(Loader<GroupedThreadMediaLoader.GroupedThreadMedia> loader) {
        ((MediaGalleryAllAdapter) this.recyclerView.getAdapter()).setMedia(new GroupedThreadMediaLoader.EmptyGroupedThreadMedia());
    }

    public void onMediaClicked(MediaDatabase.MediaRecord mediaRecord) {
        if (this.actionMode != null) {
            handleMediaMultiSelectClick(mediaRecord);
        } else {
            handleMediaPreviewClick(mediaRecord);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        super.onDestroy();
        int childCount = this.recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            MediaGalleryAllAdapter mediaGalleryAllAdapter = this.adapter;
            RecyclerView recyclerView = this.recyclerView;
            mediaGalleryAllAdapter.detach(recyclerView.getChildViewHolder(recyclerView.getChildAt(i)));
        }
    }

    private void handleMediaMultiSelectClick(MediaDatabase.MediaRecord mediaRecord) {
        MediaGalleryAllAdapter listAdapter = getListAdapter();
        listAdapter.toggleSelection(mediaRecord);
        if (listAdapter.getSelectedMediaCount() == 0) {
            this.actionMode.finish();
        } else {
            updateMultiSelect();
        }
    }

    private void handleMediaPreviewClick(MediaDatabase.MediaRecord mediaRecord) {
        Context context;
        if (mediaRecord.getAttachment().getUri() != null && (context = getContext()) != null) {
            DatabaseAttachment attachment = mediaRecord.getAttachment();
            if (MediaUtil.isVideo(attachment) || MediaUtil.isImage(attachment)) {
                Intent intent = new Intent(context, MediaPreviewActivity.class);
                intent.putExtra("date", mediaRecord.getDate());
                intent.putExtra(MediaPreviewActivity.SIZE_EXTRA, mediaRecord.getAttachment().getSize());
                intent.putExtra("thread_id", this.threadId);
                boolean z = true;
                intent.putExtra(MediaPreviewActivity.LEFT_IS_RECENT_EXTRA, true);
                intent.putExtra(MediaPreviewActivity.HIDE_ALL_MEDIA_EXTRA, true);
                if (this.threadId != -1) {
                    z = false;
                }
                intent.putExtra(MediaPreviewActivity.SHOW_THREAD_EXTRA, z);
                intent.putExtra(MediaPreviewActivity.SORTING_EXTRA, this.sorting.ordinal());
                intent.putExtra(MediaPreviewActivity.IS_VIDEO_GIF, attachment.isVideoGif());
                intent.setDataAndType(mediaRecord.getAttachment().getUri(), mediaRecord.getContentType());
                context.startActivity(intent);
            } else if (!MediaUtil.isAudio(attachment)) {
                showFileExternally(context, mediaRecord);
            }
        }
    }

    private static void showFileExternally(Context context, MediaDatabase.MediaRecord mediaRecord) {
        Uri uri = mediaRecord.getAttachment().getUri();
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.addFlags(1);
        intent.setDataAndType(PartAuthority.getAttachmentPublicUri(uri), mediaRecord.getContentType());
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException unused) {
            Log.w(TAG, "No activity existed to view the media.");
            Toast.makeText(context, (int) R.string.ConversationItem_unable_to_open_media, 1).show();
        }
    }

    public void onMediaLongClicked(MediaDatabase.MediaRecord mediaRecord) {
        if (this.actionMode == null) {
            enterMultiSelect();
        }
        handleMediaMultiSelectClick(mediaRecord);
    }

    public void handleSelectAllMedia() {
        getListAdapter().selectAllMedia();
        updateMultiSelect();
    }

    public String getActionModeTitle() {
        MediaGalleryAllAdapter listAdapter = getListAdapter();
        int selectedMediaCount = listAdapter.getSelectedMediaCount();
        return getResources().getQuantityString(R.plurals.MediaOverviewActivity_d_selected_s, selectedMediaCount, Integer.valueOf(selectedMediaCount), Util.getPrettyFileSize(listAdapter.getSelectedMediaTotalFileSize()));
    }

    public MediaGalleryAllAdapter getListAdapter() {
        return (MediaGalleryAllAdapter) this.recyclerView.getAdapter();
    }

    private void enterMultiSelect() {
        FragmentActivity requireActivity = requireActivity();
        this.actionMode = ((AppCompatActivity) requireActivity).startSupportActionMode(this.actionModeCallback);
        ((MediaOverviewActivity) requireActivity).onEnterMultiSelect();
        SignalBottomActionBar signalBottomActionBar = this.bottomActionBar;
        ViewUtil.animateIn(signalBottomActionBar, signalBottomActionBar.getEnterAnimation());
        updateMultiSelect();
    }

    public void exitMultiSelect() {
        this.actionMode.finish();
        this.actionMode = null;
        SignalBottomActionBar signalBottomActionBar = this.bottomActionBar;
        ViewUtil.animateOut(signalBottomActionBar, signalBottomActionBar.getExitAnimation());
    }

    private void updateMultiSelect() {
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.setTitle(getActionModeTitle());
            int sectionCount = getListAdapter().getSectionCount();
            this.bottomActionBar.setItems(Arrays.asList(new ActionItem(R.drawable.ic_save_24, getResources().getQuantityString(R.plurals.MediaOverviewActivity_save_plural, sectionCount), new Runnable() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    MediaOverviewPageFragment.this.lambda$updateMultiSelect$1();
                }
            }), new ActionItem(R.drawable.ic_select_24, getString(R.string.MediaOverviewActivity_select_all), new Runnable() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    MediaOverviewPageFragment.this.handleSelectAllMedia();
                }
            }), new ActionItem(R.drawable.ic_delete_24, getResources().getQuantityString(R.plurals.MediaOverviewActivity_delete_plural, sectionCount), new Runnable() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    MediaOverviewPageFragment.this.lambda$updateMultiSelect$2();
                }
            })));
        }
    }

    public /* synthetic */ void lambda$updateMultiSelect$1() {
        MediaActions.handleSaveMedia(this, getListAdapter().getSelectedMedia(), new Runnable() { // from class: org.thoughtcrime.securesms.mediaoverview.MediaOverviewPageFragment$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                MediaOverviewPageFragment.this.exitMultiSelect();
            }
        });
    }

    public /* synthetic */ void lambda$updateMultiSelect$2() {
        MediaActions.handleDeleteMedia(requireContext(), getListAdapter().getSelectedMedia());
        exitMultiSelect();
    }

    public void onPlay(Uri uri, double d, long j) {
        this.voiceNoteMediaController.startSinglePlayback(uri, j, d);
    }

    public void onPause(Uri uri) {
        this.voiceNoteMediaController.pausePlayback(uri);
    }

    public void onSeekTo(Uri uri, double d) {
        this.voiceNoteMediaController.seekToPosition(uri, d);
    }

    public void onStopAndReset(Uri uri) {
        this.voiceNoteMediaController.stopPlaybackAndReset(uri);
    }

    public void registerPlaybackStateObserver(Observer<VoiceNotePlaybackState> observer) {
        this.voiceNoteMediaController.getVoiceNotePlaybackState().observe(getViewLifecycleOwner(), observer);
    }

    public void unregisterPlaybackStateObserver(Observer<VoiceNotePlaybackState> observer) {
        this.voiceNoteMediaController.getVoiceNotePlaybackState().removeObserver(observer);
    }

    /* loaded from: classes4.dex */
    public class ActionModeCallback implements ActionMode.Callback {
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            return false;
        }

        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        private ActionModeCallback() {
            MediaOverviewPageFragment.this = r1;
        }

        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.setTitle(MediaOverviewPageFragment.this.getActionModeTitle());
            return true;
        }

        public void onDestroyActionMode(ActionMode actionMode) {
            MediaOverviewPageFragment.this.getListAdapter().clearSelection();
            ((MediaOverviewActivity) MediaOverviewPageFragment.this.requireActivity()).onExitMultiSelect();
            MediaOverviewPageFragment.this.exitMultiSelect();
        }
    }
}
