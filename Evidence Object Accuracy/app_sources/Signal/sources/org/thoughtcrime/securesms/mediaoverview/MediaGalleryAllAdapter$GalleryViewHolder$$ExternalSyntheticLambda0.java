package org.thoughtcrime.securesms.mediaoverview;

import android.view.View;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.mediaoverview.MediaGalleryAllAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGalleryAllAdapter$GalleryViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ MediaGalleryAllAdapter.GalleryViewHolder f$0;
    public final /* synthetic */ MediaDatabase.MediaRecord f$1;

    public /* synthetic */ MediaGalleryAllAdapter$GalleryViewHolder$$ExternalSyntheticLambda0(MediaGalleryAllAdapter.GalleryViewHolder galleryViewHolder, MediaDatabase.MediaRecord mediaRecord) {
        this.f$0 = galleryViewHolder;
        this.f$1 = mediaRecord;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
