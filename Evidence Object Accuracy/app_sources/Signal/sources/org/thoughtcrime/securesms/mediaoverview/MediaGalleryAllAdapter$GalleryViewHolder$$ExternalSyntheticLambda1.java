package org.thoughtcrime.securesms.mediaoverview;

import android.view.View;
import org.thoughtcrime.securesms.mediaoverview.MediaGalleryAllAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGalleryAllAdapter$GalleryViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ MediaGalleryAllAdapter.GalleryViewHolder f$0;

    public /* synthetic */ MediaGalleryAllAdapter$GalleryViewHolder$$ExternalSyntheticLambda1(MediaGalleryAllAdapter.GalleryViewHolder galleryViewHolder) {
        this.f$0 = galleryViewHolder;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return this.f$0.lambda$bind$1(view);
    }
}
