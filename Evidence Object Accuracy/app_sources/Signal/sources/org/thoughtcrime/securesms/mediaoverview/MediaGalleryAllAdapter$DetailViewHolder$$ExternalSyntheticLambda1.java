package org.thoughtcrime.securesms.mediaoverview;

import android.view.View;
import org.thoughtcrime.securesms.mediaoverview.MediaGalleryAllAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ MediaGalleryAllAdapter.DetailViewHolder f$0;

    public /* synthetic */ MediaGalleryAllAdapter$DetailViewHolder$$ExternalSyntheticLambda1(MediaGalleryAllAdapter.DetailViewHolder detailViewHolder) {
        this.f$0 = detailViewHolder;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return this.f$0.lambda$bind$1(view);
    }
}
