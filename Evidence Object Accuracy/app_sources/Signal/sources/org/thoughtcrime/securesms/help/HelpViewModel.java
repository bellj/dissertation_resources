package org.thoughtcrime.securesms.help;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import j$.util.Optional;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class HelpViewModel extends ViewModel {
    private static final int MINIMUM_PROBLEM_CHARS;
    private final MutableLiveData<Integer> categoryIndex;
    private final LiveData<Boolean> isFormValid;
    private final MutableLiveData<Boolean> problemMeetsLengthRequirements;
    private final SubmitDebugLogRepository submitDebugLogRepository = new SubmitDebugLogRepository();

    public HelpViewModel() {
        MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>();
        this.problemMeetsLengthRequirements = mutableLiveData;
        MutableLiveData<Integer> mutableLiveData2 = new MutableLiveData<>(0);
        this.categoryIndex = mutableLiveData2;
        this.isFormValid = LiveDataUtil.combineLatest(mutableLiveData, mutableLiveData2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.help.HelpViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return HelpViewModel.lambda$new$0((Boolean) obj, (Integer) obj2);
            }
        });
    }

    public static /* synthetic */ Boolean lambda$new$0(Boolean bool, Integer num) {
        return Boolean.valueOf(bool == Boolean.TRUE && num.intValue() > 0);
    }

    public LiveData<Boolean> isFormValid() {
        return this.isFormValid;
    }

    public void onProblemChanged(String str) {
        this.problemMeetsLengthRequirements.setValue(Boolean.valueOf(str.length() >= 10));
    }

    public void onCategorySelected(int i) {
        this.categoryIndex.setValue(Integer.valueOf(i));
    }

    public int getCategoryIndex() {
        return ((Integer) Optional.ofNullable(this.categoryIndex.getValue()).orElse(0)).intValue();
    }

    public LiveData<SubmitResult> onSubmitClicked(boolean z) {
        MutableLiveData mutableLiveData = new MutableLiveData();
        if (z) {
            this.submitDebugLogRepository.buildAndSubmitLog(new SubmitDebugLogRepository.Callback() { // from class: org.thoughtcrime.securesms.help.HelpViewModel$$ExternalSyntheticLambda1
                @Override // org.thoughtcrime.securesms.logsubmit.SubmitDebugLogRepository.Callback
                public final void onResult(Object obj) {
                    HelpViewModel.lambda$onSubmitClicked$1(MutableLiveData.this, (Optional) obj);
                }
            });
        } else {
            mutableLiveData.postValue(new SubmitResult(Optional.empty(), false));
        }
        return mutableLiveData;
    }

    public static /* synthetic */ void lambda$onSubmitClicked$1(MutableLiveData mutableLiveData, Optional optional) {
        mutableLiveData.postValue(new SubmitResult(optional, optional.isPresent()));
    }

    /* loaded from: classes4.dex */
    public static class SubmitResult {
        private final Optional<String> debugLogUrl;
        private final boolean isError;

        private SubmitResult(Optional<String> optional, boolean z) {
            this.debugLogUrl = optional;
            this.isError = z;
        }

        public Optional<String> getDebugLogUrl() {
            return this.debugLogUrl;
        }

        public boolean isError() {
            return this.isError;
        }
    }
}
