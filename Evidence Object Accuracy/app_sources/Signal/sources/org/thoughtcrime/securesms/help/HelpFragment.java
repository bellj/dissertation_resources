package org.thoughtcrime.securesms.help;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import androidx.core.util.Consumer;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.ResourceUtil;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.help.HelpViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class HelpFragment extends LoggingFragment {
    public static final int DONATION_INDEX;
    public static final int PAYMENT_INDEX;
    public static final String START_CATEGORY_INDEX;
    private ArrayAdapter<CharSequence> categoryAdapter;
    private Spinner categorySpinner;
    private View debugLogInfo;
    private List<EmojiImageView> emoji;
    private View faq;
    private HelpViewModel helpViewModel;
    private CheckBox includeDebugLogs;
    private CircularProgressMaterialButton next;
    private EditText problem;
    private View toaster;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.help_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        initializeViewModels();
        initializeViews(view);
        initializeListeners();
        initializeObservers();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.next.cancelSpinning();
        this.problem.setEnabled(true);
    }

    private void initializeViewModels() {
        this.helpViewModel = (HelpViewModel) ViewModelProviders.of(this).get(HelpViewModel.class);
    }

    private void initializeViews(View view) {
        this.problem = (EditText) view.findViewById(R.id.help_fragment_problem);
        this.includeDebugLogs = (CheckBox) view.findViewById(R.id.help_fragment_debug);
        this.debugLogInfo = view.findViewById(R.id.help_fragment_debug_info);
        this.faq = view.findViewById(R.id.help_fragment_faq);
        this.next = (CircularProgressMaterialButton) view.findViewById(R.id.help_fragment_next);
        this.toaster = view.findViewById(R.id.help_fragment_next_toaster);
        this.categorySpinner = (Spinner) view.findViewById(R.id.help_fragment_category);
        this.emoji = new ArrayList(Feeling.values().length);
        Feeling[] values = Feeling.values();
        for (Feeling feeling : values) {
            ((EmojiImageView) view.findViewById(feeling.getViewId())).setImageEmoji(feeling.getEmojiCode());
            this.emoji.add((EmojiImageView) view.findViewById(feeling.getViewId()));
        }
        ArrayAdapter<CharSequence> createFromResource = ArrayAdapter.createFromResource(requireContext(), R.array.HelpFragment__categories_4, 17367048);
        this.categoryAdapter = createFromResource;
        createFromResource.setDropDownViewResource(17367049);
        this.categorySpinner.setAdapter((SpinnerAdapter) this.categoryAdapter);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.categorySpinner.setSelection(Util.clamp(arguments.getInt(START_CATEGORY_INDEX, 0), 0, this.categorySpinner.getCount() - 1));
        }
    }

    private void initializeListeners() {
        this.problem.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                HelpFragment.this.lambda$initializeListeners$0((Editable) obj);
            }
        }));
        Stream.of(this.emoji).forEach(new com.annimon.stream.function.Consumer() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                HelpFragment.this.lambda$initializeListeners$1((EmojiImageView) obj);
            }
        });
        this.faq.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                HelpFragment.this.lambda$initializeListeners$2(view);
            }
        });
        this.debugLogInfo.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                HelpFragment.this.lambda$initializeListeners$3(view);
            }
        });
        this.next.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                HelpFragment.this.lambda$initializeListeners$4(view);
            }
        });
        this.toaster.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                HelpFragment.this.lambda$initializeListeners$5(view);
            }
        });
        this.categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() { // from class: org.thoughtcrime.securesms.help.HelpFragment.1
            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                HelpFragment.this.helpViewModel.onCategorySelected(i);
            }
        });
    }

    public /* synthetic */ void lambda$initializeListeners$0(Editable editable) {
        this.helpViewModel.onProblemChanged(editable.toString());
    }

    public /* synthetic */ void lambda$initializeListeners$1(EmojiImageView emojiImageView) {
        emojiImageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                HelpFragment.this.handleEmojiClicked(view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeListeners$2(View view) {
        launchFaq();
    }

    public /* synthetic */ void lambda$initializeListeners$3(View view) {
        launchDebugLogInfo();
    }

    public /* synthetic */ void lambda$initializeListeners$4(View view) {
        submitForm();
    }

    public /* synthetic */ void lambda$initializeListeners$5(View view) {
        if (this.helpViewModel.getCategoryIndex() == 0) {
            this.categorySpinner.startAnimation(AnimationUtils.loadAnimation(requireContext(), R.anim.shake_horizontal));
        }
        Toast.makeText(requireContext(), (int) R.string.HelpFragment__please_be_as_descriptive_as_possible, 1).show();
    }

    private void initializeObservers() {
        this.helpViewModel.isFormValid().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                HelpFragment.this.lambda$initializeObservers$6((Boolean) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeObservers$6(Boolean bool) {
        this.next.setEnabled(bool.booleanValue());
        this.toaster.setVisibility(bool.booleanValue() ? 8 : 0);
    }

    public void handleEmojiClicked(View view) {
        if (view.isSelected()) {
            view.setSelected(false);
            return;
        }
        Stream.of(this.emoji).forEach(new com.annimon.stream.function.Consumer() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                ((EmojiImageView) obj).setSelected(false);
            }
        });
        view.setSelected(true);
    }

    private void launchFaq() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.HelpFragment__link__faq))));
    }

    private void launchDebugLogInfo() {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(getString(R.string.HelpFragment__link__debug_info))));
    }

    private void submitForm() {
        this.next.setSpinning();
        this.problem.setEnabled(false);
        this.helpViewModel.onSubmitClicked(this.includeDebugLogs.isChecked()).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                HelpFragment.this.lambda$submitForm$8((HelpViewModel.SubmitResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$submitForm$8(HelpViewModel.SubmitResult submitResult) {
        if (submitResult.getDebugLogUrl().isPresent()) {
            submitFormWithDebugLog(submitResult.getDebugLogUrl().get());
        } else if (submitResult.isError()) {
            submitFormWithDebugLog(getString(R.string.HelpFragment__could_not_upload_logs));
        } else {
            submitFormWithDebugLog(null);
        }
    }

    private void submitFormWithDebugLog(String str) {
        CommunicationActions.openEmail(requireContext(), SupportEmailUtil.getSupportEmailAddress(requireContext()), getEmailSubject(), getEmailBody(str, (Feeling) Stream.of(this.emoji).filter(new Predicate() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((EmojiImageView) obj).isSelected();
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.help.HelpFragment$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return HelpFragment.lambda$submitFormWithDebugLog$9((EmojiImageView) obj);
            }
        }).findFirst().orElse(null)));
    }

    public static /* synthetic */ Feeling lambda$submitFormWithDebugLog$9(EmojiImageView emojiImageView) {
        return Feeling.getByViewId(emojiImageView.getId());
    }

    private String getEmailSubject() {
        return getString(R.string.HelpFragment__signal_android_support_request);
    }

    private String getEmailBody(String str, Feeling feeling) {
        String str2;
        StringBuilder sb = new StringBuilder();
        if (str != null) {
            sb.append("\n");
            sb.append(getString(R.string.HelpFragment__debug_log));
            sb.append(" ");
            sb.append(str);
        }
        if (feeling != null) {
            sb.append("\n\n");
            sb.append(feeling.getEmojiCode());
            sb.append("\n");
            sb.append(getString(feeling.getStringId()));
        }
        String[] stringArray = ResourceUtil.getEnglishResources(requireContext()).getStringArray(R.array.HelpFragment__categories_4);
        if (this.helpViewModel.getCategoryIndex() < 0 || this.helpViewModel.getCategoryIndex() >= stringArray.length) {
            str2 = this.categoryAdapter.getItem(this.helpViewModel.getCategoryIndex()).toString();
        } else {
            str2 = stringArray[this.helpViewModel.getCategoryIndex()];
        }
        return SupportEmailUtil.generateSupportEmailBody(requireContext(), R.string.HelpFragment__signal_android_support_request, " - " + str2, this.problem.getText().toString() + "\n\n", sb.toString());
    }

    /* loaded from: classes4.dex */
    public enum Feeling {
        ECSTATIC(R.id.help_fragment_emoji_5, R.string.HelpFragment__emoji_5, "😀"),
        HAPPY(R.id.help_fragment_emoji_4, R.string.HelpFragment__emoji_4, "🙂"),
        AMBIVALENT(R.id.help_fragment_emoji_3, R.string.HelpFragment__emoji_3, "😐"),
        UNHAPPY(R.id.help_fragment_emoji_2, R.string.HelpFragment__emoji_2, "🙁"),
        ANGRY(R.id.help_fragment_emoji_1, R.string.HelpFragment__emoji_1, "😠");
        
        private final CharSequence emojiCode;
        private final int stringId;
        private final int viewId;

        Feeling(int i, int i2, CharSequence charSequence) {
            this.viewId = i;
            this.stringId = i2;
            this.emojiCode = charSequence;
        }

        public int getViewId() {
            return this.viewId;
        }

        public int getStringId() {
            return this.stringId;
        }

        public CharSequence getEmojiCode() {
            return this.emojiCode;
        }

        static Feeling getByViewId(int i) {
            Feeling[] values = values();
            for (Feeling feeling : values) {
                if (feeling.viewId == i) {
                    return feeling;
                }
            }
            throw new AssertionError();
        }
    }
}
