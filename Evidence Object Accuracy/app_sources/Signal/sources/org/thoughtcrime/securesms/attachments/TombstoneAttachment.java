package org.thoughtcrime.securesms.attachments;

import android.net.Uri;

/* loaded from: classes.dex */
public class TombstoneAttachment extends Attachment {
    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getPublicUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getUri() {
        return null;
    }

    public TombstoneAttachment(String str, boolean z) {
        super(str, 0, 0, null, 0, null, null, null, null, null, false, false, false, 0, 0, z, 0, null, null, null, null, null);
    }
}
