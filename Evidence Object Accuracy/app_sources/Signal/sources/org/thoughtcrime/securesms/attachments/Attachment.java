package org.thoughtcrime.securesms.attachments;

import android.net.Uri;
import org.thoughtcrime.securesms.audio.AudioHash;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.stickers.StickerLocator;

/* loaded from: classes.dex */
public abstract class Attachment {
    private final AudioHash audioHash;
    private final BlurHash blurHash;
    private final boolean borderless;
    private final String caption;
    private final int cdnNumber;
    private final String contentType;
    private final byte[] digest;
    private final String fastPreflightId;
    private final String fileName;
    private final int height;
    private final String key;
    private final String location;
    private final boolean quote;
    private final String relay;
    private final long size;
    private final StickerLocator stickerLocator;
    private final int transferState;
    private final AttachmentDatabase.TransformProperties transformProperties;
    private final long uploadTimestamp;
    private final boolean videoGif;
    private final boolean voiceNote;
    private final int width;

    public abstract Uri getPublicUri();

    public abstract Uri getUri();

    public Attachment(String str, int i, long j, String str2, int i2, String str3, String str4, String str5, byte[] bArr, String str6, boolean z, boolean z2, boolean z3, int i3, int i4, boolean z4, long j2, String str7, StickerLocator stickerLocator, BlurHash blurHash, AudioHash audioHash, AttachmentDatabase.TransformProperties transformProperties) {
        AttachmentDatabase.TransformProperties transformProperties2;
        this.contentType = str;
        this.transferState = i;
        this.size = j;
        this.fileName = str2;
        this.cdnNumber = i2;
        this.location = str3;
        this.key = str4;
        this.relay = str5;
        this.digest = bArr;
        this.fastPreflightId = str6;
        this.voiceNote = z;
        this.borderless = z2;
        this.videoGif = z3;
        this.width = i3;
        this.height = i4;
        this.quote = z4;
        this.uploadTimestamp = j2;
        this.stickerLocator = stickerLocator;
        this.caption = str7;
        this.blurHash = blurHash;
        this.audioHash = audioHash;
        if (transformProperties != null) {
            transformProperties2 = transformProperties;
        } else {
            transformProperties2 = AttachmentDatabase.TransformProperties.empty();
        }
        this.transformProperties = transformProperties2;
    }

    public int getTransferState() {
        return this.transferState;
    }

    public boolean isInProgress() {
        int i = this.transferState;
        return (i == 0 || i == 3) ? false : true;
    }

    public long getSize() {
        return this.size;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getContentType() {
        return this.contentType;
    }

    public int getCdnNumber() {
        return this.cdnNumber;
    }

    public String getLocation() {
        return this.location;
    }

    public String getKey() {
        return this.key;
    }

    public String getRelay() {
        return this.relay;
    }

    public byte[] getDigest() {
        return this.digest;
    }

    public String getFastPreflightId() {
        return this.fastPreflightId;
    }

    public boolean isVoiceNote() {
        return this.voiceNote;
    }

    public boolean isBorderless() {
        return this.borderless;
    }

    public boolean isVideoGif() {
        return this.videoGif;
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public boolean isQuote() {
        return this.quote;
    }

    public long getUploadTimestamp() {
        return this.uploadTimestamp;
    }

    public boolean isSticker() {
        return this.stickerLocator != null;
    }

    public StickerLocator getSticker() {
        return this.stickerLocator;
    }

    public BlurHash getBlurHash() {
        return this.blurHash;
    }

    public AudioHash getAudioHash() {
        return this.audioHash;
    }

    public String getCaption() {
        return this.caption;
    }

    public AttachmentDatabase.TransformProperties getTransformProperties() {
        return this.transformProperties;
    }
}
