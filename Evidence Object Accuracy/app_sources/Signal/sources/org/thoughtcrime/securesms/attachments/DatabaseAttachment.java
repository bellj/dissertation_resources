package org.thoughtcrime.securesms.attachments;

import android.net.Uri;
import java.util.Comparator;
import org.thoughtcrime.securesms.audio.AudioHash;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.stickers.StickerLocator;

/* loaded from: classes.dex */
public class DatabaseAttachment extends Attachment {
    private final AttachmentId attachmentId;
    private final int displayOrder;
    private final boolean hasData;
    private final boolean hasThumbnail;
    private final long mmsId;

    public DatabaseAttachment(AttachmentId attachmentId, long j, boolean z, boolean z2, String str, int i, long j2, String str2, int i2, String str3, String str4, String str5, byte[] bArr, String str6, boolean z3, boolean z4, boolean z5, int i3, int i4, boolean z6, String str7, StickerLocator stickerLocator, BlurHash blurHash, AudioHash audioHash, AttachmentDatabase.TransformProperties transformProperties, int i5, long j3) {
        super(str, i, j2, str2, i2, str3, str4, str5, bArr, str6, z3, z4, z5, i3, i4, z6, j3, str7, stickerLocator, blurHash, audioHash, transformProperties);
        this.attachmentId = attachmentId;
        this.hasData = z;
        this.hasThumbnail = z2;
        this.mmsId = j;
        this.displayOrder = i5;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getUri() {
        if (this.hasData) {
            return PartAuthority.getAttachmentDataUri(this.attachmentId);
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getPublicUri() {
        if (this.hasData) {
            return PartAuthority.getAttachmentPublicUri(getUri());
        }
        return null;
    }

    public AttachmentId getAttachmentId() {
        return this.attachmentId;
    }

    public int getDisplayOrder() {
        return this.displayOrder;
    }

    public boolean equals(Object obj) {
        return obj != null && (obj instanceof DatabaseAttachment) && ((DatabaseAttachment) obj).attachmentId.equals(this.attachmentId);
    }

    public int hashCode() {
        return this.attachmentId.hashCode();
    }

    public long getMmsId() {
        return this.mmsId;
    }

    public boolean hasData() {
        return this.hasData;
    }

    public boolean hasThumbnail() {
        return this.hasThumbnail;
    }

    /* loaded from: classes.dex */
    public static class DisplayOrderComparator implements Comparator<DatabaseAttachment> {
        public int compare(DatabaseAttachment databaseAttachment, DatabaseAttachment databaseAttachment2) {
            return Integer.compare(databaseAttachment.getDisplayOrder(), databaseAttachment2.getDisplayOrder());
        }
    }
}
