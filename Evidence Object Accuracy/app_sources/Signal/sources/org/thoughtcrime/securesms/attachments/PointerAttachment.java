package org.thoughtcrime.securesms.attachments;

import android.net.Uri;
import j$.util.Optional;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;

/* loaded from: classes.dex */
public class PointerAttachment extends Attachment {
    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getPublicUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getUri() {
        return null;
    }

    private PointerAttachment(String str, int i, long j, String str2, int i2, String str3, String str4, String str5, byte[] bArr, String str6, boolean z, boolean z2, boolean z3, int i3, int i4, long j2, String str7, StickerLocator stickerLocator, BlurHash blurHash) {
        super(str, i, j, str2, i2, str3, str4, str5, bArr, str6, z, z2, z3, i3, i4, false, j2, str7, stickerLocator, blurHash, null, null);
    }

    public static List<Attachment> forPointers(Optional<List<SignalServiceAttachment>> optional) {
        LinkedList linkedList = new LinkedList();
        if (optional.isPresent()) {
            for (SignalServiceAttachment signalServiceAttachment : optional.get()) {
                Optional<Attachment> forPointer = forPointer(Optional.of(signalServiceAttachment));
                if (forPointer.isPresent()) {
                    linkedList.add(forPointer.get());
                }
            }
        }
        return linkedList;
    }

    public static List<Attachment> forPointers(List<SignalServiceDataMessage.Quote.QuotedAttachment> list) {
        LinkedList linkedList = new LinkedList();
        if (list != null) {
            for (SignalServiceDataMessage.Quote.QuotedAttachment quotedAttachment : list) {
                Optional<Attachment> forPointer = forPointer(quotedAttachment);
                if (forPointer.isPresent()) {
                    linkedList.add(forPointer.get());
                }
            }
        }
        return linkedList;
    }

    public static Optional<Attachment> forPointer(Optional<SignalServiceAttachment> optional) {
        return forPointer(optional, null, null);
    }

    public static Optional<Attachment> forPointer(Optional<SignalServiceAttachment> optional, StickerLocator stickerLocator) {
        return forPointer(optional, stickerLocator, null);
    }

    public static Optional<Attachment> forPointer(Optional<SignalServiceAttachment> optional, StickerLocator stickerLocator, String str) {
        if (!optional.isPresent() || !optional.get().isPointer()) {
            return Optional.empty();
        }
        return Optional.of(new PointerAttachment(optional.get().getContentType(), 2, (long) optional.get().asPointer().getSize().orElse(0).intValue(), optional.get().asPointer().getFileName().orElse(null), optional.get().asPointer().getCdnNumber(), optional.get().asPointer().getRemoteId().toString(), optional.get().asPointer().getKey() != null ? Base64.encodeBytes(optional.get().asPointer().getKey()) : null, null, optional.get().asPointer().getDigest().orElse(null), str, optional.get().asPointer().getVoiceNote(), optional.get().asPointer().isBorderless(), optional.get().asPointer().isGif(), optional.get().asPointer().getWidth(), optional.get().asPointer().getHeight(), optional.get().asPointer().getUploadTimestamp(), optional.get().asPointer().getCaption().orElse(null), stickerLocator, BlurHash.parseOrNull(optional.get().asPointer().getBlurHash().orElse(null))));
    }

    public static Optional<Attachment> forPointer(SignalServiceDataMessage.Quote.QuotedAttachment quotedAttachment) {
        SignalServiceAttachment thumbnail = quotedAttachment.getThumbnail();
        String contentType = quotedAttachment.getContentType();
        long j = 0;
        long intValue = thumbnail != null ? (long) thumbnail.asPointer().getSize().orElse(0).intValue() : 0;
        String fileName = quotedAttachment.getFileName();
        int cdnNumber = thumbnail != null ? thumbnail.asPointer().getCdnNumber() : 0;
        String signalServiceAttachmentRemoteId = thumbnail != null ? thumbnail.asPointer().getRemoteId().toString() : "0";
        String encodeBytes = (thumbnail == null || thumbnail.asPointer().getKey() == null) ? null : Base64.encodeBytes(thumbnail.asPointer().getKey());
        byte[] orElse = thumbnail != null ? thumbnail.asPointer().getDigest().orElse(null) : null;
        int width = thumbnail != null ? thumbnail.asPointer().getWidth() : 0;
        int height = thumbnail != null ? thumbnail.asPointer().getHeight() : 0;
        if (thumbnail != null) {
            j = thumbnail.asPointer().getUploadTimestamp();
        }
        return Optional.of(new PointerAttachment(contentType, 2, intValue, fileName, cdnNumber, signalServiceAttachmentRemoteId, encodeBytes, null, orElse, null, false, false, false, width, height, j, thumbnail != null ? thumbnail.asPointer().getCaption().orElse(null) : null, null, null));
    }
}
