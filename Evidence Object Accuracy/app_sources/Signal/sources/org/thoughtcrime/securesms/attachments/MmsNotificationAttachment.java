package org.thoughtcrime.securesms.attachments;

import android.net.Uri;

/* loaded from: classes.dex */
public class MmsNotificationAttachment extends Attachment {
    private static int getTransferStateFromStatus(int i) {
        int i2 = 2;
        if (!(i == 1 || i == 2)) {
            i2 = 3;
            if (i == 3) {
                return 1;
            }
        }
        return i2;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getPublicUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getUri() {
        return null;
    }

    public MmsNotificationAttachment(int i, long j) {
        super("application/mms", getTransferStateFromStatus(i), j, null, 0, null, null, null, null, null, false, false, false, 0, 0, false, 0, null, null, null, null, null);
    }
}
