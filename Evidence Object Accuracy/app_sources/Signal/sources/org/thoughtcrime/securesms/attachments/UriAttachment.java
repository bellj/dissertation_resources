package org.thoughtcrime.securesms.attachments;

import android.net.Uri;
import org.thoughtcrime.securesms.audio.AudioHash;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.database.AttachmentDatabase;
import org.thoughtcrime.securesms.stickers.StickerLocator;

/* loaded from: classes.dex */
public class UriAttachment extends Attachment {
    private final Uri dataUri;

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getPublicUri() {
        return null;
    }

    public UriAttachment(Uri uri, String str, int i, long j, String str2, boolean z, boolean z2, boolean z3, boolean z4, String str3, StickerLocator stickerLocator, BlurHash blurHash, AudioHash audioHash, AttachmentDatabase.TransformProperties transformProperties) {
        this(uri, str, i, j, 0, 0, str2, null, z, z2, z3, z4, str3, stickerLocator, blurHash, audioHash, transformProperties);
    }

    public UriAttachment(Uri uri, String str, int i, long j, int i2, int i3, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, String str4, StickerLocator stickerLocator, BlurHash blurHash, AudioHash audioHash, AttachmentDatabase.TransformProperties transformProperties) {
        super(str, i, j, str2, 0, null, null, null, null, str3, z, z2, z3, i2, i3, z4, 0, str4, stickerLocator, blurHash, audioHash, transformProperties);
        this.dataUri = uri;
    }

    @Override // org.thoughtcrime.securesms.attachments.Attachment
    public Uri getUri() {
        return this.dataUri;
    }

    public boolean equals(Object obj) {
        return obj != null && (obj instanceof UriAttachment) && ((UriAttachment) obj).dataUri.equals(this.dataUri);
    }

    public int hashCode() {
        return this.dataUri.hashCode();
    }
}
