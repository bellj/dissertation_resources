package org.thoughtcrime.securesms.attachments;

import android.os.Parcel;
import android.os.Parcelable;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes.dex */
public class AttachmentId implements Parcelable {
    public static final Parcelable.Creator<AttachmentId> CREATOR = new Parcelable.Creator<AttachmentId>() { // from class: org.thoughtcrime.securesms.attachments.AttachmentId.1
        @Override // android.os.Parcelable.Creator
        public AttachmentId createFromParcel(Parcel parcel) {
            return new AttachmentId(parcel);
        }

        @Override // android.os.Parcelable.Creator
        public AttachmentId[] newArray(int i) {
            return new AttachmentId[i];
        }
    };
    @JsonProperty
    private final long rowId;
    @JsonProperty
    private final long uniqueId;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public AttachmentId(@JsonProperty("rowId") long j, @JsonProperty("uniqueId") long j2) {
        this.rowId = j;
        this.uniqueId = j2;
    }

    private AttachmentId(Parcel parcel) {
        this.rowId = parcel.readLong();
        this.uniqueId = parcel.readLong();
    }

    public long getRowId() {
        return this.rowId;
    }

    public long getUniqueId() {
        return this.uniqueId;
    }

    public String[] toStrings() {
        return new String[]{String.valueOf(this.rowId), String.valueOf(this.uniqueId)};
    }

    @Override // java.lang.Object
    public String toString() {
        return "AttachmentId::(" + this.rowId + ", " + this.uniqueId + ")";
    }

    public boolean isValid() {
        return this.rowId >= 0 && this.uniqueId >= 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AttachmentId attachmentId = (AttachmentId) obj;
        if (this.rowId == attachmentId.rowId && this.uniqueId == attachmentId.uniqueId) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Util.hashCode(Long.valueOf(this.rowId), Long.valueOf(this.uniqueId));
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.rowId);
        parcel.writeLong(this.uniqueId);
    }
}
