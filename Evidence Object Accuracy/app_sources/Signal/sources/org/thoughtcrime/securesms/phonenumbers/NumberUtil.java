package org.thoughtcrime.securesms.phonenumbers;

import android.telephony.PhoneNumberUtils;
import android.util.Patterns;
import java.util.regex.Pattern;

/* loaded from: classes4.dex */
public class NumberUtil {
    private static final Pattern EMAIL_PATTERN = Patterns.EMAIL_ADDRESS;
    private static final Pattern PHONE_PATTERN = Patterns.PHONE;

    public static boolean isValidEmail(String str) {
        return EMAIL_PATTERN.matcher(str).matches();
    }

    public static boolean isVisuallyValidNumber(String str) {
        return PHONE_PATTERN.matcher(str).matches();
    }

    public static boolean isVisuallyValidNumberOrEmail(String str) {
        return isVisuallyValidNumber(str) || isValidEmail(str);
    }

    public static boolean isValidSmsOrEmail(String str) {
        return PhoneNumberUtils.isWellFormedSmsAddress(str) || isValidEmail(str);
    }
}
