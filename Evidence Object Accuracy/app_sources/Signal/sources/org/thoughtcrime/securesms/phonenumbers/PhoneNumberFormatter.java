package org.thoughtcrime.securesms.phonenumbers;

import android.content.Context;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import com.google.i18n.phonenumbers.ShortNumberInfo;
import j$.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.signal.core.util.SetUtil;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class PhoneNumberFormatter {
    private static final Pattern BR_NO_AREACODE = Pattern.compile("^(9?\\d{8})$");
    private static final Set<String> EXCLUDE_FROM_MANUAL_SHORTCODE_4 = SetUtil.newHashSet("AC", "NC", "NU", "TK");
    private static final Set<String> MANUAL_SHORTCODE_6 = SetUtil.newHashSet("DE", "FI", "GB", "SK");
    private static final Set<Integer> NATIONAL_FORMAT_COUNTRY_CODES = SetUtil.newHashSet(1, 44);
    private static final String TAG = Log.tag(PhoneNumberFormatter.class);
    private static final Pattern US_NO_AREACODE = Pattern.compile("^(\\d{7})$");
    private static final AtomicReference<Pair<String, PhoneNumberFormatter>> cachedFormatter = new AtomicReference<>();
    private final Pattern ALPHA_PATTERN;
    private final String localCountryCode;
    private final Optional<PhoneNumber> localNumber;
    private final PhoneNumberUtil phoneNumberUtil;

    public static PhoneNumberFormatter get(Context context) {
        String e164 = SignalStore.account().getE164();
        if (Util.isEmpty(e164)) {
            return new PhoneNumberFormatter(Util.getSimCountryIso(context).orElse("US"), true);
        }
        AtomicReference<Pair<String, PhoneNumberFormatter>> atomicReference = cachedFormatter;
        Pair<String, PhoneNumberFormatter> pair = atomicReference.get();
        if (pair != null && pair.first().equals(e164)) {
            return pair.second();
        }
        PhoneNumberFormatter phoneNumberFormatter = new PhoneNumberFormatter(e164);
        atomicReference.set(new Pair<>(e164, phoneNumberFormatter));
        return phoneNumberFormatter;
    }

    PhoneNumberFormatter(String str) {
        PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
        this.phoneNumberUtil = instance;
        this.ALPHA_PATTERN = Pattern.compile("[a-zA-Z]");
        try {
            Phonenumber$PhoneNumber parse = instance.parse(str, null);
            int countryCode = parse.getCountryCode();
            this.localNumber = Optional.of(new PhoneNumber(str, countryCode, parseAreaCode(str, countryCode)));
            this.localCountryCode = instance.getRegionCodeForNumber(parse);
        } catch (NumberParseException e) {
            throw new AssertionError(e);
        }
    }

    PhoneNumberFormatter(String str, boolean z) {
        this.phoneNumberUtil = PhoneNumberUtil.getInstance();
        this.ALPHA_PATTERN = Pattern.compile("[a-zA-Z]");
        this.localNumber = Optional.empty();
        this.localCountryCode = str;
    }

    public static String prettyPrint(String str) {
        return get(ApplicationDependencies.getApplication()).prettyPrintFormat(str);
    }

    public String prettyPrintFormat(String str) {
        try {
            Phonenumber$PhoneNumber parse = this.phoneNumberUtil.parse(str, this.localCountryCode);
            if (!this.localNumber.isPresent() || this.localNumber.get().countryCode != parse.getCountryCode() || !NATIONAL_FORMAT_COUNTRY_CODES.contains(Integer.valueOf(this.localNumber.get().getCountryCode()))) {
                return StringUtil.isolateBidi(this.phoneNumberUtil.format(parse, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL));
            }
            return StringUtil.isolateBidi(this.phoneNumberUtil.format(parse, PhoneNumberUtil.PhoneNumberFormat.NATIONAL));
        } catch (NumberParseException unused) {
            Log.w(TAG, "Failed to format number.");
            return StringUtil.isolateBidi(str);
        }
    }

    public static int getLocalCountryCode() {
        Optional<PhoneNumber> optional = get(ApplicationDependencies.getApplication()).localNumber;
        if (optional == null || !optional.isPresent()) {
            return 0;
        }
        return optional.get().countryCode;
    }

    public String format(String str) {
        if (str == null) {
            return "Unknown";
        }
        if (GroupId.isEncodedGroup(str)) {
            return str;
        }
        if (this.ALPHA_PATTERN.matcher(str).find()) {
            return str.trim();
        }
        String replaceAll = str.replaceAll("[^0-9+]", "");
        if (replaceAll.length() == 0) {
            if (str.trim().length() == 0) {
                return "Unknown";
            }
            return str.trim();
        } else if (replaceAll.length() <= 6 && MANUAL_SHORTCODE_6.contains(this.localCountryCode)) {
            return replaceAll;
        } else {
            if (replaceAll.length() <= 4 && !EXCLUDE_FROM_MANUAL_SHORTCODE_4.contains(this.localCountryCode)) {
                return replaceAll;
            }
            if (isShortCode(replaceAll, this.localCountryCode)) {
                return replaceAll;
            }
            try {
                return this.phoneNumberUtil.format(this.phoneNumberUtil.parse(applyAreaCodeRules(this.localNumber, replaceAll), this.localCountryCode), PhoneNumberUtil.PhoneNumberFormat.E164);
            } catch (NumberParseException e) {
                Log.w(TAG, e);
                if (replaceAll.charAt(0) == '+') {
                    return replaceAll;
                }
                if (this.localNumber.isPresent()) {
                    String e164Number = this.localNumber.get().getE164Number();
                    if (e164Number.charAt(0) == '+') {
                        e164Number = e164Number.substring(1);
                    }
                    if (e164Number.length() == replaceAll.length() || replaceAll.length() > e164Number.length()) {
                        return "+" + str;
                    }
                    int length = e164Number.length() - replaceAll.length();
                    return "+" + e164Number.substring(0, length) + replaceAll;
                }
                String valueOf = String.valueOf(this.phoneNumberUtil.getCountryCodeForRegion(this.localCountryCode));
                StringBuilder sb = new StringBuilder();
                sb.append("+");
                if (!replaceAll.startsWith(valueOf)) {
                    replaceAll = valueOf + replaceAll;
                }
                sb.append(replaceAll);
                return sb.toString();
            }
        }
    }

    private boolean isShortCode(String str, String str2) {
        try {
            return ShortNumberInfo.getInstance().isPossibleShortNumberForRegion(this.phoneNumberUtil.parse(str, str2), str2);
        } catch (NumberParseException unused) {
            return false;
        }
    }

    private String parseAreaCode(String str, int i) {
        if (i == 1) {
            return str.substring(2, 5);
        }
        if (i != 55) {
            return null;
        }
        return str.substring(3, 5);
    }

    private String applyAreaCodeRules(Optional<PhoneNumber> optional, String str) {
        if (optional.isPresent() && optional.get().getAreaCode().isPresent()) {
            int countryCode = optional.get().getCountryCode();
            if (countryCode == 1) {
                Matcher matcher = US_NO_AREACODE.matcher(str);
                if (matcher.matches()) {
                    return optional.get().getAreaCode() + matcher.group();
                }
            } else if (countryCode == 55) {
                Matcher matcher2 = BR_NO_AREACODE.matcher(str);
                if (matcher2.matches()) {
                    return optional.get().getAreaCode() + matcher2.group();
                }
            }
        }
        return str;
    }

    /* loaded from: classes4.dex */
    public static class PhoneNumber {
        private final Optional<String> areaCode;
        private final int countryCode;
        private final String e164Number;

        PhoneNumber(String str, int i, String str2) {
            this.e164Number = str;
            this.countryCode = i;
            this.areaCode = Optional.ofNullable(str2);
        }

        String getE164Number() {
            return this.e164Number;
        }

        int getCountryCode() {
            return this.countryCode;
        }

        Optional<String> getAreaCode() {
            return this.areaCode;
        }
    }
}
