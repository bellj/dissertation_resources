package org.thoughtcrime.securesms;

import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;

/* loaded from: classes.dex */
public class PlayServicesProblemActivity extends FragmentActivity {
    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        new PlayServicesProblemFragment().show(getSupportFragmentManager(), "dialog");
    }
}
