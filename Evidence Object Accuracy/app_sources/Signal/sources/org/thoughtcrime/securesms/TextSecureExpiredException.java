package org.thoughtcrime.securesms;

/* loaded from: classes.dex */
public class TextSecureExpiredException extends Exception {
    public TextSecureExpiredException(String str) {
        super(str);
    }
}
