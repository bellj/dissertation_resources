package org.thoughtcrime.securesms.keyboard.emoji;

import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.components.emoji.EmojiPageView;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.keyboard.KeyboardPageSelected;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ThemedFragment;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: EmojiKeyboardPageFragment.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\t\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004:\u0003345B\u0005¢\u0006\u0002\u0010\u0005J\u0012\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J&\u0010\u001e\u001a\u0004\u0018\u00010\t2\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u0010\u0010#\u001a\u00020\u001b2\u0006\u0010$\u001a\u00020%H\u0016J\u0012\u0010&\u001a\u00020\u001b2\b\u0010'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010)\u001a\u00020\u001bH\u0016J\b\u0010*\u001a\u00020\u001bH\u0016J\u0010\u0010+\u001a\u00020\u001b2\u0006\u0010,\u001a\u00020-H\u0016J\u001a\u0010.\u001a\u00020\u001b2\u0006\u0010/\u001a\u00020\t2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u0010\u00100\u001a\u00020\u001b2\u0006\u00101\u001a\u00020%H\u0002J\u0010\u00102\u001a\u00020\u001b2\u0006\u00101\u001a\u00020%H\u0002R\u000e\u0010\u0006\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX.¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX.¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX.¢\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u00060\u000fR\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0002X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\tX.¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X.¢\u0006\u0002\n\u0000¨\u00066"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiEventListener;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageViewGridAdapter$VariationSelectorListener;", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPageSelected;", "()V", "appBarLayout", "Lcom/google/android/material/appbar/AppBarLayout;", "backspaceView", "Landroid/view/View;", "callback", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$Callback;", "categoriesAdapter", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoriesAdapter;", "categoryUpdateOnScroll", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$UpdateCategorySelectionOnScroll;", "emojiCategoriesRecycler", "Landroidx/recyclerview/widget/RecyclerView;", "emojiPageView", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageView;", "eventListener", "searchBar", "Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView;", "searchView", "viewModel", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageViewModel;", "onActivityCreated", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onEmojiSelected", "emoji", "", "onKeyEvent", "keyEvent", "Landroid/view/KeyEvent;", "onPageSelected", "onResume", "onVariationSelectorStateChanged", "open", "", "onViewCreated", "view", "scrollTo", "key", "updateCategoryTab", "Callback", "EmojiKeyboardPageSearchViewCallbacks", "UpdateCategorySelectionOnScroll", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiKeyboardPageFragment extends Fragment implements EmojiEventListener, EmojiPageViewGridAdapter.VariationSelectorListener, KeyboardPageSelected {
    private AppBarLayout appBarLayout;
    private View backspaceView;
    private Callback callback;
    private EmojiKeyboardPageCategoriesAdapter categoriesAdapter;
    private final UpdateCategorySelectionOnScroll categoryUpdateOnScroll = new UpdateCategorySelectionOnScroll();
    private RecyclerView emojiCategoriesRecycler;
    private EmojiPageView emojiPageView;
    private EmojiEventListener eventListener;
    private KeyboardPageSearchView searchBar;
    private View searchView;
    private EmojiKeyboardPageViewModel viewModel;

    /* compiled from: EmojiKeyboardPageFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$Callback;", "", "openEmojiSearch", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void openEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.VariationSelectorListener
    public void onVariationSelectorStateChanged(boolean z) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002a, code lost:
        r0 = (org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback) requireActivity();
     */
    @Override // androidx.fragment.app.Fragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewCreated(android.view.View r4, android.os.Bundle r5) {
        /*
            r3 = this;
            java.lang.String r5 = "view"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r4, r5)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            androidx.fragment.app.Fragment r0 = r3.getParentFragment()     // Catch: ClassCastException -> 0x00ba
        L_0x000e:
            if (r0 == 0) goto L_0x002a
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback     // Catch: ClassCastException -> 0x00ba
            if (r1 == 0) goto L_0x0015
            goto L_0x0030
        L_0x0015:
            java.lang.Class r1 = r0.getClass()     // Catch: ClassCastException -> 0x00ba
            java.lang.String r1 = r1.getName()     // Catch: ClassCastException -> 0x00ba
            java.lang.String r2 = "parent::class.java.name"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)     // Catch: ClassCastException -> 0x00ba
            r5.add(r1)     // Catch: ClassCastException -> 0x00ba
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()     // Catch: ClassCastException -> 0x00ba
            goto L_0x000e
        L_0x002a:
            androidx.fragment.app.FragmentActivity r0 = r3.requireActivity()     // Catch: ClassCastException -> 0x00ba
            org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$Callback r0 = (org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback) r0     // Catch: ClassCastException -> 0x00ba
        L_0x0030:
            if (r0 == 0) goto L_0x00ae
            org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$Callback r0 = (org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback) r0
            r3.callback = r0
            r5 = 2131363041(0x7f0a04e1, float:1.834588E38)
            android.view.View r5 = r4.findViewById(r5)
            java.lang.String r0 = "view.findViewById(R.id.emoji_page_view)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r0)
            org.thoughtcrime.securesms.components.emoji.EmojiPageView r5 = (org.thoughtcrime.securesms.components.emoji.EmojiPageView) r5
            r3.emojiPageView = r5
            r0 = 0
            java.lang.String r1 = "emojiPageView"
            if (r5 != 0) goto L_0x004f
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
            r5 = r0
        L_0x004f:
            r2 = 1
            r5.initialize(r3, r3, r2)
            org.thoughtcrime.securesms.components.emoji.EmojiPageView r5 = r3.emojiPageView
            if (r5 != 0) goto L_0x005b
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r1)
            goto L_0x005c
        L_0x005b:
            r0 = r5
        L_0x005c:
            org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$UpdateCategorySelectionOnScroll r5 = r3.categoryUpdateOnScroll
            r0.addOnScrollListener(r5)
            r5 = 2131363042(0x7f0a04e2, float:1.8345882E38)
            android.view.View r5 = r4.findViewById(r5)
            java.lang.String r0 = "view.findViewById(R.id.emoji_search)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r0)
            r3.searchView = r5
            r5 = 2131363040(0x7f0a04e0, float:1.8345878E38)
            android.view.View r5 = r4.findViewById(r5)
            java.lang.String r0 = "view.findViewById(R.id.emoji_keyboard_search_text)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r0)
            org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView r5 = (org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView) r5
            r3.searchBar = r5
            r5 = 2131363033(0x7f0a04d9, float:1.8345863E38)
            android.view.View r5 = r4.findViewById(r5)
            java.lang.String r0 = "view.findViewById(R.id.emoji_categories_recycler)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r0)
            androidx.recyclerview.widget.RecyclerView r5 = (androidx.recyclerview.widget.RecyclerView) r5
            r3.emojiCategoriesRecycler = r5
            r5 = 2131363032(0x7f0a04d8, float:1.8345861E38)
            android.view.View r5 = r4.findViewById(r5)
            java.lang.String r0 = "view.findViewById(R.id.emoji_backspace)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r5, r0)
            r3.backspaceView = r5
            r5 = 2131363039(0x7f0a04df, float:1.8345876E38)
            android.view.View r4 = r4.findViewById(r5)
            java.lang.String r5 = "view.findViewById(R.id.e…i_keyboard_search_appbar)"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r4, r5)
            com.google.android.material.appbar.AppBarLayout r4 = (com.google.android.material.appbar.AppBarLayout) r4
            r3.appBarLayout = r4
            return
        L_0x00ae:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            java.lang.String r5 = "Required value was null."
            java.lang.String r5 = r5.toString()
            r4.<init>(r5)
            throw r4
        L_0x00ba:
            r4 = move-exception
            androidx.fragment.app.FragmentActivity r0 = r3.requireActivity()
            java.lang.Class r0 = r0.getClass()
            java.lang.String r0 = r0.getName()
            java.lang.String r1 = "requireActivity()::class.java.name"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            r5.add(r0)
            org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException r0 = new org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException
            r0.<init>(r5, r4)
            goto L_0x00d6
        L_0x00d5:
            throw r0
        L_0x00d6:
            goto L_0x00d5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.onViewCreated(android.view.View, android.os.Bundle):void");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return ThemedFragment.themedInflate(this, R.layout.keyboard_pager_emoji_page_fragment, layoutInflater, viewGroup);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        EmojiEventListener emojiEventListener;
        super.onActivityCreated(bundle);
        FragmentActivity requireActivity = requireActivity();
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ViewModel viewModel = new ViewModelProvider(requireActivity, new EmojiKeyboardPageViewModel.Factory(requireContext)).get(EmojiKeyboardPageViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(requir…ageViewModel::class.java)");
        this.viewModel = (EmojiKeyboardPageViewModel) viewModel;
        this.categoriesAdapter = new EmojiKeyboardPageCategoriesAdapter(new Consumer() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda3
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                EmojiKeyboardPageFragment.m1955onActivityCreated$lambda0(EmojiKeyboardPageFragment.this, (String) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        RecyclerView recyclerView = this.emojiCategoriesRecycler;
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiCategoriesRecycler");
            recyclerView = null;
        }
        EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter = this.categoriesAdapter;
        if (emojiKeyboardPageCategoriesAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("categoriesAdapter");
            emojiKeyboardPageCategoriesAdapter = null;
        }
        recyclerView.setAdapter(emojiKeyboardPageCategoriesAdapter);
        KeyboardPageSearchView keyboardPageSearchView = this.searchBar;
        if (keyboardPageSearchView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("searchBar");
            keyboardPageSearchView = null;
        }
        keyboardPageSearchView.setCallbacks(new EmojiKeyboardPageSearchViewCallbacks());
        View view = this.searchView;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("searchView");
            view = null;
        }
        view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EmojiKeyboardPageFragment.m1956onActivityCreated$lambda1(EmojiKeyboardPageFragment.this, view2);
            }
        });
        View view2 = this.backspaceView;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("backspaceView");
            view2 = null;
        }
        view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view3) {
                EmojiKeyboardPageFragment.m1957onActivityCreated$lambda2(EmojiKeyboardPageFragment.this, view3);
            }
        });
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel2 = this.viewModel;
        if (emojiKeyboardPageViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            emojiKeyboardPageViewModel2 = null;
        }
        emojiKeyboardPageViewModel2.getCategories().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EmojiKeyboardPageFragment.m1958onActivityCreated$lambda4(EmojiKeyboardPageFragment.this, (MappingModelList) obj);
            }
        });
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel3 = this.viewModel;
        if (emojiKeyboardPageViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            emojiKeyboardPageViewModel3 = null;
        }
        emojiKeyboardPageViewModel3.getPages().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda7
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EmojiKeyboardPageFragment.m1960onActivityCreated$lambda6(EmojiKeyboardPageFragment.this, (MappingModelList) obj);
            }
        });
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel4 = this.viewModel;
        if (emojiKeyboardPageViewModel4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            emojiKeyboardPageViewModel = emojiKeyboardPageViewModel4;
        }
        emojiKeyboardPageViewModel.getSelectedKey().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                EmojiKeyboardPageFragment.m1962onActivityCreated$lambda7(EmojiKeyboardPageFragment.this, (String) obj);
            }
        });
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity2 = requireActivity();
                    if (requireActivity2 != null) {
                        emojiEventListener = (EmojiEventListener) requireActivity2;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.emoji.EmojiEventListener");
                    }
                } else if (fragment instanceof EmojiEventListener) {
                    emojiEventListener = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.eventListener = emojiEventListener;
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: onActivityCreated$lambda-0 */
    public static final void m1955onActivityCreated$lambda0(EmojiKeyboardPageFragment emojiKeyboardPageFragment, String str) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "key");
        emojiKeyboardPageFragment.scrollTo(str);
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel = emojiKeyboardPageFragment.viewModel;
        if (emojiKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            emojiKeyboardPageViewModel = null;
        }
        emojiKeyboardPageViewModel.onKeySelected(str);
    }

    /* renamed from: onActivityCreated$lambda-1 */
    public static final void m1956onActivityCreated$lambda1(EmojiKeyboardPageFragment emojiKeyboardPageFragment, View view) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        Callback callback = emojiKeyboardPageFragment.callback;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.openEmojiSearch();
    }

    /* renamed from: onActivityCreated$lambda-2 */
    public static final void m1957onActivityCreated$lambda2(EmojiKeyboardPageFragment emojiKeyboardPageFragment, View view) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        EmojiEventListener emojiEventListener = emojiKeyboardPageFragment.eventListener;
        if (emojiEventListener == null) {
            Intrinsics.throwUninitializedPropertyAccessException("eventListener");
            emojiEventListener = null;
        }
        emojiEventListener.onKeyEvent(EmojiKeyboardPageFragmentKt.DELETE_KEY_EVENT);
    }

    /* renamed from: onActivityCreated$lambda-4 */
    public static final void m1958onActivityCreated$lambda4(EmojiKeyboardPageFragment emojiKeyboardPageFragment, MappingModelList mappingModelList) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter = emojiKeyboardPageFragment.categoriesAdapter;
        if (emojiKeyboardPageCategoriesAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("categoriesAdapter");
            emojiKeyboardPageCategoriesAdapter = null;
        }
        emojiKeyboardPageCategoriesAdapter.submitList(mappingModelList, new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                EmojiKeyboardPageFragment.m1959onActivityCreated$lambda4$lambda3(EmojiKeyboardPageFragment.this);
            }
        });
    }

    /* renamed from: onActivityCreated$lambda-4$lambda-3 */
    public static final void m1959onActivityCreated$lambda4$lambda3(EmojiKeyboardPageFragment emojiKeyboardPageFragment) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        RecyclerView recyclerView = emojiKeyboardPageFragment.emojiCategoriesRecycler;
        RecyclerView recyclerView2 = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiCategoriesRecycler");
            recyclerView = null;
        }
        ViewParent parent = recyclerView.getParent();
        if (parent != null) {
            ((View) parent).invalidate();
            RecyclerView recyclerView3 = emojiKeyboardPageFragment.emojiCategoriesRecycler;
            if (recyclerView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiCategoriesRecycler");
            } else {
                recyclerView2 = recyclerView3;
            }
            recyclerView2.getParent().requestLayout();
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.View");
    }

    /* renamed from: onActivityCreated$lambda-6 */
    public static final void m1960onActivityCreated$lambda6(EmojiKeyboardPageFragment emojiKeyboardPageFragment, MappingModelList mappingModelList) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        EmojiPageView emojiPageView = emojiKeyboardPageFragment.emojiPageView;
        if (emojiPageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiPageView");
            emojiPageView = null;
        }
        emojiPageView.setList(mappingModelList, new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                EmojiKeyboardPageFragment.m1961onActivityCreated$lambda6$lambda5(EmojiKeyboardPageFragment.this);
            }
        });
    }

    /* renamed from: onActivityCreated$lambda-6$lambda-5 */
    public static final void m1961onActivityCreated$lambda6$lambda5(EmojiKeyboardPageFragment emojiKeyboardPageFragment) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        EmojiPageView emojiPageView = emojiKeyboardPageFragment.emojiPageView;
        LinearLayoutManager linearLayoutManager = null;
        if (emojiPageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiPageView");
            emojiPageView = null;
        }
        RecyclerView.LayoutManager layoutManager = emojiPageView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            linearLayoutManager = (LinearLayoutManager) layoutManager;
        }
        if (linearLayoutManager != null) {
            linearLayoutManager.scrollToPositionWithOffset(1, 0);
        }
    }

    /* renamed from: onActivityCreated$lambda-7 */
    public static final void m1962onActivityCreated$lambda7(EmojiKeyboardPageFragment emojiKeyboardPageFragment, String str) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(str, "it");
        emojiKeyboardPageFragment.updateCategoryTab(str);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel = this.viewModel;
        if (emojiKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            emojiKeyboardPageViewModel = null;
        }
        emojiKeyboardPageViewModel.refreshRecentEmoji();
    }

    @Override // org.thoughtcrime.securesms.keyboard.KeyboardPageSelected
    public void onPageSelected() {
        EmojiKeyboardPageViewModel emojiKeyboardPageViewModel = this.viewModel;
        if (emojiKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            emojiKeyboardPageViewModel = null;
        }
        emojiKeyboardPageViewModel.refreshRecentEmoji();
    }

    private final void updateCategoryTab(String str) {
        RecyclerView recyclerView = this.emojiCategoriesRecycler;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiCategoriesRecycler");
            recyclerView = null;
        }
        recyclerView.post(new Runnable(str) { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                EmojiKeyboardPageFragment.m1963updateCategoryTab$lambda8(EmojiKeyboardPageFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: updateCategoryTab$lambda-8 */
    public static final void m1963updateCategoryTab$lambda8(EmojiKeyboardPageFragment emojiKeyboardPageFragment, String str) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "$key");
        EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter = emojiKeyboardPageFragment.categoriesAdapter;
        RecyclerView recyclerView = null;
        if (emojiKeyboardPageCategoriesAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("categoriesAdapter");
            emojiKeyboardPageCategoriesAdapter = null;
        }
        int indexOfFirst = emojiKeyboardPageCategoriesAdapter.indexOfFirst(EmojiKeyboardPageCategoryMappingModel.class, new Function1<EmojiKeyboardPageCategoryMappingModel, Boolean>(str) { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$updateCategoryTab$1$index$1
            final /* synthetic */ String $key;

            /* access modifiers changed from: package-private */
            {
                this.$key = r1;
            }

            public final Boolean invoke(EmojiKeyboardPageCategoryMappingModel emojiKeyboardPageCategoryMappingModel) {
                return Boolean.valueOf(Intrinsics.areEqual(emojiKeyboardPageCategoryMappingModel.getKey(), this.$key));
            }
        });
        if (indexOfFirst != -1) {
            RecyclerView recyclerView2 = emojiKeyboardPageFragment.emojiCategoriesRecycler;
            if (recyclerView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiCategoriesRecycler");
            } else {
                recyclerView = recyclerView2;
            }
            recyclerView.smoothScrollToPosition(indexOfFirst);
        }
    }

    private final void scrollTo(String str) {
        int indexOfFirst;
        EmojiPageView emojiPageView = this.emojiPageView;
        EmojiPageView emojiPageView2 = null;
        if (emojiPageView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiPageView");
            emojiPageView = null;
        }
        EmojiPageViewGridAdapter adapter = emojiPageView.getAdapter();
        if (adapter != null && (indexOfFirst = adapter.indexOfFirst(EmojiPageViewGridAdapter.EmojiHeader.class, new Function1<EmojiPageViewGridAdapter.EmojiHeader, Boolean>(str) { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment$scrollTo$1$index$1
            final /* synthetic */ String $key;

            /* access modifiers changed from: package-private */
            {
                this.$key = r1;
            }

            public final Boolean invoke(EmojiPageViewGridAdapter.EmojiHeader emojiHeader) {
                return Boolean.valueOf(Intrinsics.areEqual(emojiHeader.getKey(), this.$key));
            }
        })) != -1) {
            AppBarLayout appBarLayout = this.appBarLayout;
            if (appBarLayout == null) {
                Intrinsics.throwUninitializedPropertyAccessException("appBarLayout");
                appBarLayout = null;
            }
            appBarLayout.setExpanded(false, true);
            this.categoryUpdateOnScroll.startAutoScrolling();
            EmojiPageView emojiPageView3 = this.emojiPageView;
            if (emojiPageView3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emojiPageView");
            } else {
                emojiPageView2 = emojiPageView3;
            }
            emojiPageView2.smoothScrollToPositionTop(indexOfFirst);
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        SignalStore.emojiValues().setPreferredVariation(str);
        EmojiEventListener emojiEventListener = this.eventListener;
        if (emojiEventListener == null) {
            Intrinsics.throwUninitializedPropertyAccessException("eventListener");
            emojiEventListener = null;
        }
        emojiEventListener.onEmojiSelected(str);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
        EmojiEventListener emojiEventListener = this.eventListener;
        if (emojiEventListener == null) {
            Intrinsics.throwUninitializedPropertyAccessException("eventListener");
            emojiEventListener = null;
        }
        emojiEventListener.onKeyEvent(keyEvent);
    }

    /* compiled from: EmojiKeyboardPageFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$EmojiKeyboardPageSearchViewCallbacks;", "Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$Callbacks;", "(Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment;)V", "onClicked", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private final class EmojiKeyboardPageSearchViewCallbacks implements KeyboardPageSearchView.Callbacks {
        public EmojiKeyboardPageSearchViewCallbacks() {
            EmojiKeyboardPageFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onFocusGained() {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusGained(this);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onFocusLost() {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusLost(this);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onNavigationClicked() {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onNavigationClicked(this);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onQueryChanged(String str) {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onQueryChanged(this, str);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onClicked() {
            Callback callback = EmojiKeyboardPageFragment.this.callback;
            if (callback == null) {
                Intrinsics.throwUninitializedPropertyAccessException("callback");
                callback = null;
            }
            callback.openEmojiSearch();
        }
    }

    /* compiled from: EmojiKeyboardPageFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J \u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\nH\u0016J\u0006\u0010\u000e\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$UpdateCategorySelectionOnScroll;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "(Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment;)V", "doneScrolling", "", "onScrollStateChanged", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "newState", "", "onScrolled", "dx", "dy", "startAutoScrolling", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class UpdateCategorySelectionOnScroll extends RecyclerView.OnScrollListener {
        private boolean doneScrolling = true;

        public UpdateCategorySelectionOnScroll() {
            EmojiKeyboardPageFragment.this = r1;
        }

        public final void startAutoScrolling() {
            this.doneScrolling = false;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            if (i == 0 && !this.doneScrolling) {
                this.doneScrolling = true;
                onScrolled(recyclerView, 0, 0);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            if (recyclerView.getLayoutManager() != null && this.doneScrolling) {
                EmojiPageView emojiPageView = EmojiKeyboardPageFragment.this.emojiPageView;
                EmojiKeyboardPageViewModel emojiKeyboardPageViewModel = null;
                if (emojiPageView == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("emojiPageView");
                    emojiPageView = null;
                }
                EmojiPageViewGridAdapter adapter = emojiPageView.getAdapter();
                if (adapter != null) {
                    EmojiKeyboardPageFragment emojiKeyboardPageFragment = EmojiKeyboardPageFragment.this;
                    RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                    if (layoutManager != null) {
                        Optional<MappingModel<?>> model = adapter.getModel(((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition());
                        Intrinsics.checkNotNullExpressionValue(model, "adapter.getModel(index)");
                        if (model.isPresent() && (model.get() instanceof EmojiPageViewGridAdapter.HasKey)) {
                            EmojiKeyboardPageViewModel emojiKeyboardPageViewModel2 = emojiKeyboardPageFragment.viewModel;
                            if (emojiKeyboardPageViewModel2 == null) {
                                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            } else {
                                emojiKeyboardPageViewModel = emojiKeyboardPageViewModel2;
                            }
                            String key = ((EmojiPageViewGridAdapter.HasKey) model.get()).getKey();
                            Intrinsics.checkNotNullExpressionValue(key, "item.get() as EmojiPageViewGridAdapter.HasKey).key");
                            emojiKeyboardPageViewModel.onKeySelected(key);
                            return;
                        }
                        return;
                    }
                    throw new NullPointerException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
                }
            }
        }
    }
}
