package org.thoughtcrime.securesms.keyboard.sticker;

import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;

/* compiled from: StickerSearchRepository.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0012\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004*\u00020\u0006H\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"EMOJI_SEARCH_RESULTS_LIMIT", "", "RECENT_LIMIT", "readAll", "", "Lorg/thoughtcrime/securesms/database/model/StickerRecord;", "Lorg/thoughtcrime/securesms/database/StickerDatabase$StickerRecordReader;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerSearchRepositoryKt {
    private static final int EMOJI_SEARCH_RESULTS_LIMIT;
    private static final int RECENT_LIMIT;

    public static final List<StickerRecord> readAll(StickerDatabase.StickerRecordReader stickerRecordReader) {
        ArrayList arrayList = new ArrayList();
        try {
            StickerRecord next = stickerRecordReader.getNext();
            while (next != null) {
                arrayList.add(next);
                next = stickerRecordReader.getNext();
            }
            Unit unit = Unit.INSTANCE;
            th = null;
            return arrayList;
        } finally {
            try {
                throw th;
            } finally {
            }
        }
    }
}
