package org.thoughtcrime.securesms.keyboard;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KClass;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ThemedFragment;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.WindowUtil;

/* compiled from: KeyboardPagerFragment.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\u000eH\u0002J#\u0010\u0010\u001a\u00020\u000e\"\n\b\u0000\u0010\u0011\u0018\u0001*\u00020\u00012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u0002H\u00110\u0013H\bJ\b\u0010\u0014\u001a\u00020\u000eH\u0002J\u0006\u0010\u0015\u001a\u00020\u000eJ\u0012\u0010\u0016\u001a\u00020\u000e2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J&\u0010\u0019\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0010\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0010\u0010!\u001a\u00020\u000e2\u0006\u0010\"\u001a\u00020#H\u0002J\u001a\u0010$\u001a\u00020\u000e2\u0006\u0010%\u001a\u00020\u00052\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0006\u0010&\u001a\u00020\u000eR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0001X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u001e\u0010\u0006\u001a\u0012\u0012\b\u0012\u0006\u0012\u0002\b\u00030\b\u0012\u0004\u0012\u00020\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerFragment;", "Landroidx/fragment/app/Fragment;", "()V", "currentFragment", "emojiButton", "Landroid/view/View;", "fragments", "", "Lkotlin/reflect/KClass;", "gifButton", "stickerButton", "viewModel", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "displayEmojiPage", "", "displayGifPage", "displayPage", "F", "fragmentFactory", "Lkotlin/Function0;", "displayStickerPage", "hide", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onHiddenChanged", "hidden", "", "onPageSelected", "page", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPage;", "onViewCreated", "view", "show", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class KeyboardPagerFragment extends Fragment {
    private Fragment currentFragment;
    private View emojiButton;
    private final Map<KClass<?>, Fragment> fragments = new LinkedHashMap();
    private View gifButton;
    private View stickerButton;
    private KeyboardPagerViewModel viewModel;

    /* compiled from: KeyboardPagerFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[KeyboardPage.values().length];
            iArr[KeyboardPage.EMOJI.ordinal()] = 1;
            iArr[KeyboardPage.GIF.ordinal()] = 2;
            iArr[KeyboardPage.STICKER.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return ThemedFragment.themedInflate(this, R.layout.keyboard_pager_fragment, layoutInflater, viewGroup);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.keyboard_pager_fragment_emoji);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.k…ard_pager_fragment_emoji)");
        this.emojiButton = findViewById;
        View findViewById2 = view.findViewById(R.id.keyboard_pager_fragment_sticker);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.k…d_pager_fragment_sticker)");
        this.stickerButton = findViewById2;
        View findViewById3 = view.findViewById(R.id.keyboard_pager_fragment_gif);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.k…board_pager_fragment_gif)");
        this.gifButton = findViewById3;
        ViewModel viewModel = new ViewModelProvider(requireActivity()).get(KeyboardPagerViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(requir…gerViewModel::class.java]");
        KeyboardPagerViewModel keyboardPagerViewModel = (KeyboardPagerViewModel) viewModel;
        this.viewModel = keyboardPagerViewModel;
        View view2 = null;
        if (keyboardPagerViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            keyboardPagerViewModel = null;
        }
        keyboardPagerViewModel.page().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                KeyboardPagerFragment.this.onPageSelected((KeyboardPage) obj);
            }
        });
        KeyboardPagerViewModel keyboardPagerViewModel2 = this.viewModel;
        if (keyboardPagerViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            keyboardPagerViewModel2 = null;
        }
        keyboardPagerViewModel2.pages().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                KeyboardPagerFragment.m1944onViewCreated$lambda0(KeyboardPagerFragment.this, (Set) obj);
            }
        });
        View view3 = this.emojiButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiButton");
            view3 = null;
        }
        view3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view4) {
                KeyboardPagerFragment.m1945onViewCreated$lambda1(KeyboardPagerFragment.this, view4);
            }
        });
        View view4 = this.stickerButton;
        if (view4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerButton");
            view4 = null;
        }
        view4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view5) {
                KeyboardPagerFragment.m1946onViewCreated$lambda2(KeyboardPagerFragment.this, view5);
            }
        });
        View view5 = this.gifButton;
        if (view5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("gifButton");
        } else {
            view2 = view5;
        }
        view2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view6) {
                KeyboardPagerFragment.m1947onViewCreated$lambda3(KeyboardPagerFragment.this, view6);
            }
        });
        onHiddenChanged(false);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1944onViewCreated$lambda0(KeyboardPagerFragment keyboardPagerFragment, Set set) {
        Intrinsics.checkNotNullParameter(keyboardPagerFragment, "this$0");
        View view = keyboardPagerFragment.emojiButton;
        View view2 = null;
        if (view == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emojiButton");
            view = null;
        }
        boolean z = false;
        ViewExtensionsKt.setVisible(view, set.contains(KeyboardPage.EMOJI) && set.size() > 1);
        View view3 = keyboardPagerFragment.stickerButton;
        if (view3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerButton");
            view3 = null;
        }
        ViewExtensionsKt.setVisible(view3, set.contains(KeyboardPage.STICKER) && set.size() > 1);
        View view4 = keyboardPagerFragment.gifButton;
        if (view4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("gifButton");
        } else {
            view2 = view4;
        }
        if (set.contains(KeyboardPage.GIF) && set.size() > 1) {
            z = true;
        }
        ViewExtensionsKt.setVisible(view2, z);
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1945onViewCreated$lambda1(KeyboardPagerFragment keyboardPagerFragment, View view) {
        Intrinsics.checkNotNullParameter(keyboardPagerFragment, "this$0");
        KeyboardPagerViewModel keyboardPagerViewModel = keyboardPagerFragment.viewModel;
        if (keyboardPagerViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            keyboardPagerViewModel = null;
        }
        keyboardPagerViewModel.switchToPage(KeyboardPage.EMOJI);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m1946onViewCreated$lambda2(KeyboardPagerFragment keyboardPagerFragment, View view) {
        Intrinsics.checkNotNullParameter(keyboardPagerFragment, "this$0");
        KeyboardPagerViewModel keyboardPagerViewModel = keyboardPagerFragment.viewModel;
        if (keyboardPagerViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            keyboardPagerViewModel = null;
        }
        keyboardPagerViewModel.switchToPage(KeyboardPage.STICKER);
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m1947onViewCreated$lambda3(KeyboardPagerFragment keyboardPagerFragment, View view) {
        Intrinsics.checkNotNullParameter(keyboardPagerFragment, "this$0");
        KeyboardPagerViewModel keyboardPagerViewModel = keyboardPagerFragment.viewModel;
        if (keyboardPagerViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            keyboardPagerViewModel = null;
        }
        keyboardPagerViewModel.switchToPage(KeyboardPage.GIF);
    }

    @Override // androidx.fragment.app.Fragment
    public void onHiddenChanged(boolean z) {
        if (Build.VERSION.SDK_INT >= 21) {
            if (z) {
                WindowUtil.setNavigationBarColor(requireActivity(), ThemeUtil.getThemedColor(requireContext(), 16843858));
            } else {
                WindowUtil.setNavigationBarColor(requireActivity(), ThemeUtil.getThemedColor(requireContext(), R.attr.mediaKeyboardBottomBarBackgroundColor));
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        KeyboardPagerViewModel keyboardPagerViewModel = this.viewModel;
        if (keyboardPagerViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            keyboardPagerViewModel = null;
        }
        KeyboardPage value = keyboardPagerViewModel.page().getValue();
        if (value != null) {
            onPageSelected(value);
        }
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:44:0x0076 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v9, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v12, types: [org.thoughtcrime.securesms.components.emoji.MediaKeyboard$MediaKeyboardListener] */
    /* JADX WARN: Type inference failed for: r0v13 */
    /* JADX WARN: Type inference failed for: r0v22 */
    /* JADX WARN: Type inference failed for: r0v23 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onPageSelected(org.thoughtcrime.securesms.keyboard.KeyboardPage r6) {
        /*
            r5 = this;
            android.view.View r0 = r5.emojiButton
            r1 = 0
            if (r0 != 0) goto L_0x000b
            java.lang.String r0 = "emojiButton"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r0 = r1
        L_0x000b:
            org.thoughtcrime.securesms.keyboard.KeyboardPage r2 = org.thoughtcrime.securesms.keyboard.KeyboardPage.EMOJI
            r3 = 0
            r4 = 1
            if (r6 != r2) goto L_0x0013
            r2 = 1
            goto L_0x0014
        L_0x0013:
            r2 = 0
        L_0x0014:
            r0.setSelected(r2)
            android.view.View r0 = r5.stickerButton
            if (r0 != 0) goto L_0x0021
            java.lang.String r0 = "stickerButton"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r0 = r1
        L_0x0021:
            org.thoughtcrime.securesms.keyboard.KeyboardPage r2 = org.thoughtcrime.securesms.keyboard.KeyboardPage.STICKER
            if (r6 != r2) goto L_0x0027
            r2 = 1
            goto L_0x0028
        L_0x0027:
            r2 = 0
        L_0x0028:
            r0.setSelected(r2)
            android.view.View r0 = r5.gifButton
            if (r0 != 0) goto L_0x0035
            java.lang.String r0 = "gifButton"
            kotlin.jvm.internal.Intrinsics.throwUninitializedPropertyAccessException(r0)
            r0 = r1
        L_0x0035:
            org.thoughtcrime.securesms.keyboard.KeyboardPage r2 = org.thoughtcrime.securesms.keyboard.KeyboardPage.GIF
            if (r6 != r2) goto L_0x003a
            r3 = 1
        L_0x003a:
            r0.setSelected(r3)
            int[] r0 = org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment.WhenMappings.$EnumSwitchMapping$0
            int r2 = r6.ordinal()
            r0 = r0[r2]
            if (r0 == r4) goto L_0x0056
            r2 = 2
            if (r0 == r2) goto L_0x0052
            r2 = 3
            if (r0 == r2) goto L_0x004e
            goto L_0x0059
        L_0x004e:
            r5.displayStickerPage()
            goto L_0x0059
        L_0x0052:
            r5.displayGifPage()
            goto L_0x0059
        L_0x0056:
            r5.displayEmojiPage()
        L_0x0059:
            androidx.fragment.app.Fragment r0 = r5.getParentFragment()
        L_0x005d:
            if (r0 == 0) goto L_0x0069
            boolean r2 = r0 instanceof org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
            if (r2 == 0) goto L_0x0064
            goto L_0x0076
        L_0x0064:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x005d
        L_0x0069:
            androidx.fragment.app.FragmentActivity r0 = r5.requireActivity()
            boolean r2 = r0 instanceof org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
            if (r2 != 0) goto L_0x0072
            goto L_0x0073
        L_0x0072:
            r1 = r0
        L_0x0073:
            r0 = r1
            org.thoughtcrime.securesms.components.emoji.MediaKeyboard$MediaKeyboardListener r0 = (org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener) r0
        L_0x0076:
            org.thoughtcrime.securesms.components.emoji.MediaKeyboard$MediaKeyboardListener r0 = (org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener) r0
            if (r0 == 0) goto L_0x007d
            r0.onKeyboardChanged(r6)
        L_0x007d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment.onPageSelected(org.thoughtcrime.securesms.keyboard.KeyboardPage):void");
    }

    private final void displayEmojiPage() {
        Fragment fragment = this.currentFragment;
        KeyboardPageSelected keyboardPageSelected = null;
        if (fragment instanceof EmojiKeyboardPageFragment) {
            if (fragment instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
                return;
            }
            return;
        }
        FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
        Intrinsics.checkNotNullExpressionValue(beginTransaction, "childFragmentManager.beginTransaction()");
        Fragment fragment2 = this.currentFragment;
        if (fragment2 != null) {
            beginTransaction.hide(fragment2);
        }
        Fragment fragment3 = this.fragments.get(Reflection.getOrCreateKotlinClass(EmojiKeyboardPageFragment.class));
        if (fragment3 == null) {
            fragment3 = ThemedFragment.withTheme(new EmojiKeyboardPageFragment(), ThemedFragment.getThemeResId(this));
            beginTransaction.add(R.id.fragment_container, fragment3);
            this.fragments.put(Reflection.getOrCreateKotlinClass(EmojiKeyboardPageFragment.class), fragment3);
        } else {
            if (fragment3 instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment3;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
            }
            beginTransaction.show(fragment3);
        }
        this.currentFragment = fragment3;
        beginTransaction.commitAllowingStateLoss();
    }

    private final void displayGifPage() {
        Fragment fragment = this.currentFragment;
        KeyboardPageSelected keyboardPageSelected = null;
        if (fragment instanceof GifKeyboardPageFragment) {
            if (fragment instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
                return;
            }
            return;
        }
        FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
        Intrinsics.checkNotNullExpressionValue(beginTransaction, "childFragmentManager.beginTransaction()");
        Fragment fragment2 = this.currentFragment;
        if (fragment2 != null) {
            beginTransaction.hide(fragment2);
        }
        Fragment fragment3 = this.fragments.get(Reflection.getOrCreateKotlinClass(GifKeyboardPageFragment.class));
        if (fragment3 == null) {
            fragment3 = ThemedFragment.withTheme(new GifKeyboardPageFragment(), ThemedFragment.getThemeResId(this));
            beginTransaction.add(R.id.fragment_container, fragment3);
            this.fragments.put(Reflection.getOrCreateKotlinClass(GifKeyboardPageFragment.class), fragment3);
        } else {
            if (fragment3 instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment3;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
            }
            beginTransaction.show(fragment3);
        }
        this.currentFragment = fragment3;
        beginTransaction.commitAllowingStateLoss();
    }

    private final /* synthetic */ <F extends Fragment> void displayPage(Function0<? extends F> function0) {
        Fragment fragment = this.currentFragment;
        Intrinsics.reifiedOperationMarker(3, "F");
        KeyboardPageSelected keyboardPageSelected = null;
        if (fragment instanceof Fragment) {
            Fragment fragment2 = this.currentFragment;
            if (fragment2 instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment2;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
                return;
            }
            return;
        }
        FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
        Intrinsics.checkNotNullExpressionValue(beginTransaction, "childFragmentManager.beginTransaction()");
        Fragment fragment3 = this.currentFragment;
        if (fragment3 != null) {
            beginTransaction.hide(fragment3);
        }
        Map<KClass<?>, Fragment> map = this.fragments;
        Intrinsics.reifiedOperationMarker(4, "F");
        Fragment fragment4 = map.get(Reflection.getOrCreateKotlinClass(Fragment.class));
        if (fragment4 == null) {
            fragment4 = ThemedFragment.withTheme((Fragment) function0.invoke(), ThemedFragment.getThemeResId(this));
            beginTransaction.add(R.id.fragment_container, fragment4);
            Map<KClass<?>, Fragment> map2 = this.fragments;
            Intrinsics.reifiedOperationMarker(4, "F");
            map2.put(Reflection.getOrCreateKotlinClass(Fragment.class), fragment4);
        } else {
            if (fragment4 instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment4;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
            }
            beginTransaction.show(fragment4);
        }
        this.currentFragment = fragment4;
        beginTransaction.commitAllowingStateLoss();
    }

    private final void displayStickerPage() {
        Fragment fragment = this.currentFragment;
        KeyboardPageSelected keyboardPageSelected = null;
        if (fragment instanceof StickerKeyboardPageFragment) {
            if (fragment instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
                return;
            }
            return;
        }
        FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
        Intrinsics.checkNotNullExpressionValue(beginTransaction, "childFragmentManager.beginTransaction()");
        Fragment fragment2 = this.currentFragment;
        if (fragment2 != null) {
            beginTransaction.hide(fragment2);
        }
        Fragment fragment3 = this.fragments.get(Reflection.getOrCreateKotlinClass(StickerKeyboardPageFragment.class));
        if (fragment3 == null) {
            fragment3 = ThemedFragment.withTheme(new StickerKeyboardPageFragment(), ThemedFragment.getThemeResId(this));
            beginTransaction.add(R.id.fragment_container, fragment3);
            this.fragments.put(Reflection.getOrCreateKotlinClass(StickerKeyboardPageFragment.class), fragment3);
        } else {
            if (fragment3 instanceof KeyboardPageSelected) {
                keyboardPageSelected = (KeyboardPageSelected) fragment3;
            }
            if (keyboardPageSelected != null) {
                keyboardPageSelected.onPageSelected();
            }
            beginTransaction.show(fragment3);
        }
        this.currentFragment = fragment3;
        beginTransaction.commitAllowingStateLoss();
    }

    public final void show() {
        if (isAdded() && getView() != null) {
            onHiddenChanged(false);
            KeyboardPagerViewModel keyboardPagerViewModel = this.viewModel;
            if (keyboardPagerViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                keyboardPagerViewModel = null;
            }
            KeyboardPage value = keyboardPagerViewModel.page().getValue();
            if (value != null) {
                onPageSelected(value);
            }
        }
    }

    public final void hide() {
        if (isAdded() && getView() != null) {
            onHiddenChanged(true);
            FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
            Intrinsics.checkNotNullExpressionValue(beginTransaction, "childFragmentManager.beginTransaction()");
            for (Fragment fragment : this.fragments.values()) {
                beginTransaction.remove(fragment);
            }
            beginTransaction.commitAllowingStateLoss();
            this.currentFragment = null;
            this.fragments.clear();
        }
    }
}
