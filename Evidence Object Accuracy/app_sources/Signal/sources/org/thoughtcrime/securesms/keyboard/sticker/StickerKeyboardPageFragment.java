package org.thoughtcrime.securesms.keyboard.sticker;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerPackListAdapter;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.util.InsetItemDecoration;
import org.thoughtcrime.securesms.util.Throttler;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: StickerKeyboardPageFragment.kt */
@Metadata(d1 = {"\u0000 \u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u00052\u00020\u0006:\u0002KLB\u0005¢\u0006\u0002\u0010\u0007J\u0012\u0010\u001e\u001a\u00020\u001f2\b\b\u0001\u0010 \u001a\u00020\u001fH\u0002J\u001e\u0010!\u001a\u0010\u0012\u0004\u0012\u00020#\u0012\u0004\u0012\u00020$\u0018\u00010\"2\u0006\u0010%\u001a\u00020&H\u0016J\u0012\u0010'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\b\u0010+\u001a\u00020(H\u0016J\b\u0010,\u001a\u00020(H\u0016JR\u0010-\u001a\u00020(2\b\u0010.\u001a\u0004\u0018\u00010&2\u0006\u0010/\u001a\u00020\u001f2\u0006\u00100\u001a\u00020\u001f2\u0006\u00101\u001a\u00020\u001f2\u0006\u00102\u001a\u00020\u001f2\u0006\u00103\u001a\u00020\u001f2\u0006\u00104\u001a\u00020\u001f2\u0006\u00105\u001a\u00020\u001f2\u0006\u00106\u001a\u00020\u001fH\u0016J\u0012\u00107\u001a\u00020(2\b\b\u0001\u00108\u001a\u00020\u001fH\u0002J\u0010\u00109\u001a\u00020(2\u0006\u0010:\u001a\u00020;H\u0016J\u0010\u0010<\u001a\u00020(2\u0006\u0010:\u001a\u00020;H\u0016J\b\u0010=\u001a\u00020(H\u0016J\b\u0010>\u001a\u00020(H\u0016J\u0010\u0010?\u001a\u00020(2\u0006\u0010@\u001a\u00020AH\u0002J\u001a\u0010B\u001a\u00020(2\u0006\u0010%\u001a\u00020&2\b\u0010)\u001a\u0004\u0018\u00010*H\u0016J\u0010\u0010C\u001a\u00020(2\u0006\u0010D\u001a\u00020$H\u0002J\u0010\u0010E\u001a\u00020(2\u0006\u0010F\u001a\u00020\u001fH\u0002J\u0010\u0010G\u001a\u00020(2\u0006\u0010D\u001a\u00020$H\u0002J\u0010\u0010H\u001a\u00020(2\u0006\u0010I\u001a\u00020JH\u0002R\u000e\u0010\b\u001a\u00020\tX.¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX.¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0012\u001a\u00060\u0013R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X.¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0015X.¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX.¢\u0006\u0002\n\u0000¨\u0006M"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$EventListener;", "Lorg/thoughtcrime/securesms/stickers/StickerRolloverTouchListener$RolloverEventListener;", "Lorg/thoughtcrime/securesms/stickers/StickerRolloverTouchListener$RolloverStickerRetriever;", "Lorg/thoughtcrime/securesms/database/DatabaseObserver$Observer;", "Landroid/view/View$OnLayoutChangeListener;", "()V", "appBarLayout", "Lcom/google/android/material/appbar/AppBarLayout;", "firstLoad", "", "layoutManager", "Landroidx/recyclerview/widget/GridLayoutManager;", "listTouchListener", "Lorg/thoughtcrime/securesms/stickers/StickerRolloverTouchListener;", "observerThrottler", "Lorg/thoughtcrime/securesms/util/Throttler;", "packIdSelectionOnScroll", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageFragment$UpdatePackSelectionOnScroll;", "stickerList", "Landroidx/recyclerview/widget/RecyclerView;", "stickerListAdapter", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter;", "stickerPacksAdapter", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter;", "stickerPacksRecycler", "stickerThrottler", "viewModel", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageViewModel;", "calculateColumnCount", "", "screenWidth", "getStickerDataFromView", "Lorg/signal/libsignal/protocol/util/Pair;", "", "", "view", "Landroid/view/View;", "onActivityCreated", "", "savedInstanceState", "Landroid/os/Bundle;", "onChanged", "onDestroyView", "onLayoutChange", "v", "left", "top", "right", "bottom", "oldLeft", "oldTop", "oldRight", "oldBottom", "onScreenWidthChanged", "newWidth", "onStickerClicked", StickerDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$Sticker;", "onStickerLongClicked", "onStickerPopupEnded", "onStickerPopupStarted", "onTabSelected", "stickerPack", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter$StickerPack;", "onViewCreated", "scrollTo", "packId", "smoothScrollToPositionTop", "position", "updateCategoryTab", "updateStickerList", StickerDatabase.DIRECTORY, "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "Callback", "UpdatePackSelectionOnScroll", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerKeyboardPageFragment extends LoggingFragment implements KeyboardStickerListAdapter.EventListener, StickerRolloverTouchListener.RolloverEventListener, StickerRolloverTouchListener.RolloverStickerRetriever, DatabaseObserver.Observer, View.OnLayoutChangeListener {
    private AppBarLayout appBarLayout;
    private boolean firstLoad = true;
    private GridLayoutManager layoutManager;
    private StickerRolloverTouchListener listTouchListener;
    private final Throttler observerThrottler = new Throttler(500);
    private final UpdatePackSelectionOnScroll packIdSelectionOnScroll = new UpdatePackSelectionOnScroll();
    private RecyclerView stickerList;
    private KeyboardStickerListAdapter stickerListAdapter;
    private KeyboardStickerPackListAdapter stickerPacksAdapter;
    private RecyclerView stickerPacksRecycler;
    private final Throttler stickerThrottler = new Throttler(100);
    private StickerKeyboardPageViewModel viewModel;

    /* compiled from: StickerKeyboardPageFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageFragment$Callback;", "", "openStickerSearch", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void openStickerSearch();
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener.RolloverEventListener
    public void onStickerPopupEnded() {
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener.RolloverEventListener
    public void onStickerPopupStarted() {
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:15:0x0026 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v3, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v6, types: [org.thoughtcrime.securesms.stickers.StickerEventListener] */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: onStickerClicked$lambda-5 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m1994onStickerClicked$lambda5(org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment r2, org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.Sticker r3) {
        /*
            java.lang.String r0 = "this$0"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            java.lang.String r0 = "$sticker"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x000e:
            if (r0 == 0) goto L_0x001a
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stickers.StickerEventListener
            if (r1 == 0) goto L_0x0015
            goto L_0x0026
        L_0x0015:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x000e
        L_0x001a:
            androidx.fragment.app.FragmentActivity r2 = r2.requireActivity()
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.stickers.StickerEventListener
            if (r0 != 0) goto L_0x0023
            r2 = 0
        L_0x0023:
            r0 = r2
            org.thoughtcrime.securesms.stickers.StickerEventListener r0 = (org.thoughtcrime.securesms.stickers.StickerEventListener) r0
        L_0x0026:
            org.thoughtcrime.securesms.stickers.StickerEventListener r0 = (org.thoughtcrime.securesms.stickers.StickerEventListener) r0
            if (r0 == 0) goto L_0x0031
            org.thoughtcrime.securesms.database.model.StickerRecord r2 = r3.getStickerRecord()
            r0.onStickerSelected(r2)
        L_0x0031:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment.m1994onStickerClicked$lambda5(org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment, org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter$Sticker):void");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0021 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v3, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r2v6, types: [org.thoughtcrime.securesms.stickers.StickerEventListener] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: onViewCreated$lambda-2 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m1996onViewCreated$lambda2(org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment r1, android.view.View r2) {
        /*
            java.lang.String r2 = "this$0"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r1, r2)
            androidx.fragment.app.Fragment r2 = r1.getParentFragment()
        L_0x0009:
            if (r2 == 0) goto L_0x0015
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.stickers.StickerEventListener
            if (r0 == 0) goto L_0x0010
            goto L_0x0021
        L_0x0010:
            androidx.fragment.app.Fragment r2 = r2.getParentFragment()
            goto L_0x0009
        L_0x0015:
            androidx.fragment.app.FragmentActivity r1 = r1.requireActivity()
            boolean r2 = r1 instanceof org.thoughtcrime.securesms.stickers.StickerEventListener
            if (r2 != 0) goto L_0x001e
            r1 = 0
        L_0x001e:
            r2 = r1
            org.thoughtcrime.securesms.stickers.StickerEventListener r2 = (org.thoughtcrime.securesms.stickers.StickerEventListener) r2
        L_0x0021:
            org.thoughtcrime.securesms.stickers.StickerEventListener r2 = (org.thoughtcrime.securesms.stickers.StickerEventListener) r2
            if (r2 == 0) goto L_0x0028
            r2.onStickerManagementClicked()
        L_0x0028:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment.m1996onViewCreated$lambda2(org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment, android.view.View):void");
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1995onViewCreated$lambda1(StickerKeyboardPageFragment stickerKeyboardPageFragment, View view) {
        Callback callback;
        Intrinsics.checkNotNullParameter(stickerKeyboardPageFragment, "this$0");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = stickerKeyboardPageFragment.getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = stickerKeyboardPageFragment.requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            callback.openStickerSearch();
        } catch (ClassCastException e) {
            String name2 = stickerKeyboardPageFragment.requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    public StickerKeyboardPageFragment() {
        super(R.layout.keyboard_pager_sticker_page_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        GlideRequests with = GlideApp.with(this);
        Intrinsics.checkNotNullExpressionValue(with, "with(this)");
        this.stickerListAdapter = new KeyboardStickerListAdapter(with, this, DeviceProperties.shouldAllowApngStickerAnimation(requireContext()));
        GridLayoutManager gridLayoutManager = new GridLayoutManager(requireContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup(this, gridLayoutManager) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$onViewCreated$1$1
            final /* synthetic */ GridLayoutManager $this_apply;
            final /* synthetic */ StickerKeyboardPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$this_apply = r2;
            }

            @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
            public int getSpanSize(int i) {
                KeyboardStickerListAdapter keyboardStickerListAdapter = this.this$0.stickerListAdapter;
                if (keyboardStickerListAdapter == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
                    keyboardStickerListAdapter = null;
                }
                Optional<MappingModel<?>> model = keyboardStickerListAdapter.getModel(i);
                Intrinsics.checkNotNullExpressionValue(model, "stickerListAdapter.getModel(position)");
                if (!model.isPresent() || !(model.get() instanceof KeyboardStickerListAdapter.StickerHeader)) {
                    return 1;
                }
                return this.$this_apply.getSpanCount();
            }
        });
        this.layoutManager = gridLayoutManager;
        this.listTouchListener = new StickerRolloverTouchListener(requireContext(), with, this, this);
        View findViewById = view.findViewById(R.id.sticker_keyboard_list);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.sticker_keyboard_list)");
        RecyclerView recyclerView = (RecyclerView) findViewById;
        this.stickerList = recyclerView;
        KeyboardStickerPackListAdapter keyboardStickerPackListAdapter = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
            recyclerView = null;
        }
        GridLayoutManager gridLayoutManager2 = this.layoutManager;
        if (gridLayoutManager2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            gridLayoutManager2 = null;
        }
        recyclerView.setLayoutManager(gridLayoutManager2);
        RecyclerView recyclerView2 = this.stickerList;
        if (recyclerView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
            recyclerView2 = null;
        }
        KeyboardStickerListAdapter keyboardStickerListAdapter = this.stickerListAdapter;
        if (keyboardStickerListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
            keyboardStickerListAdapter = null;
        }
        recyclerView2.setAdapter(keyboardStickerListAdapter);
        RecyclerView recyclerView3 = this.stickerList;
        if (recyclerView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
            recyclerView3 = null;
        }
        StickerRolloverTouchListener stickerRolloverTouchListener = this.listTouchListener;
        if (stickerRolloverTouchListener == null) {
            Intrinsics.throwUninitializedPropertyAccessException("listTouchListener");
            stickerRolloverTouchListener = null;
        }
        recyclerView3.addOnItemTouchListener(stickerRolloverTouchListener);
        RecyclerView recyclerView4 = this.stickerList;
        if (recyclerView4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
            recyclerView4 = null;
        }
        recyclerView4.addOnScrollListener(this.packIdSelectionOnScroll);
        RecyclerView recyclerView5 = this.stickerList;
        if (recyclerView5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
            recyclerView5 = null;
        }
        recyclerView5.addItemDecoration(new InsetItemDecoration(new StickerInsetSetter()));
        View findViewById2 = view.findViewById(R.id.sticker_packs_recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.sticker_packs_recycler)");
        this.stickerPacksRecycler = (RecyclerView) findViewById2;
        this.stickerPacksAdapter = new KeyboardStickerPackListAdapter(with, DeviceProperties.shouldAllowApngStickerAnimation(requireContext()), new Function1<KeyboardStickerPackListAdapter.StickerPack, Unit>(this) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$onViewCreated$2
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(KeyboardStickerPackListAdapter.StickerPack stickerPack) {
                invoke(stickerPack);
                return Unit.INSTANCE;
            }

            public final void invoke(KeyboardStickerPackListAdapter.StickerPack stickerPack) {
                Intrinsics.checkNotNullParameter(stickerPack, "p0");
                ((StickerKeyboardPageFragment) this.receiver).onTabSelected(stickerPack);
            }
        });
        RecyclerView recyclerView6 = this.stickerPacksRecycler;
        if (recyclerView6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerPacksRecycler");
            recyclerView6 = null;
        }
        KeyboardStickerPackListAdapter keyboardStickerPackListAdapter2 = this.stickerPacksAdapter;
        if (keyboardStickerPackListAdapter2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerPacksAdapter");
        } else {
            keyboardStickerPackListAdapter = keyboardStickerPackListAdapter2;
        }
        recyclerView6.setAdapter(keyboardStickerPackListAdapter);
        View findViewById3 = view.findViewById(R.id.sticker_keyboard_search_appbar);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.s…r_keyboard_search_appbar)");
        this.appBarLayout = (AppBarLayout) findViewById3;
        ((KeyboardPageSearchView) view.findViewById(R.id.sticker_keyboard_search_text)).setCallbacks(new KeyboardPageSearchView.Callbacks(this) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$onViewCreated$3
            final /* synthetic */ StickerKeyboardPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onFocusGained() {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusGained(this);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onFocusLost() {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusLost(this);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onNavigationClicked() {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onNavigationClicked(this);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onQueryChanged(String str) {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onQueryChanged(this, str);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onClicked() {
                StickerKeyboardPageFragment.Callback callback;
                StickerKeyboardPageFragment stickerKeyboardPageFragment = this.this$0;
                ArrayList arrayList = new ArrayList();
                try {
                    Fragment fragment = stickerKeyboardPageFragment.getParentFragment();
                    while (true) {
                        if (fragment == null) {
                            FragmentActivity requireActivity = stickerKeyboardPageFragment.requireActivity();
                            if (requireActivity != null) {
                                callback = (StickerKeyboardPageFragment.Callback) requireActivity;
                            } else {
                                throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment.Callback");
                            }
                        } else if (fragment instanceof StickerKeyboardPageFragment.Callback) {
                            callback = fragment;
                            break;
                        } else {
                            String name = fragment.getClass().getName();
                            Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                            arrayList.add(name);
                            fragment = fragment.getParentFragment();
                        }
                    }
                    callback.openStickerSearch();
                } catch (ClassCastException e) {
                    String name2 = stickerKeyboardPageFragment.requireActivity().getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
                    arrayList.add(name2);
                    throw new ListenerNotFoundException(arrayList, e);
                }
            }
        });
        view.findViewById(R.id.sticker_search).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StickerKeyboardPageFragment.m1995onViewCreated$lambda1(StickerKeyboardPageFragment.this, view2);
            }
        });
        view.findViewById(R.id.sticker_manage).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                StickerKeyboardPageFragment.m1996onViewCreated$lambda2(StickerKeyboardPageFragment.this, view2);
            }
        });
        ApplicationDependencies.getDatabaseObserver().registerStickerObserver(this);
        ApplicationDependencies.getDatabaseObserver().registerStickerPackObserver(this);
        view.addOnLayoutChangeListener(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this);
        requireView().removeOnLayoutChangeListener(this);
        super.onDestroyView();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ViewModel viewModel = new ViewModelProvider(requireActivity(), new StickerKeyboardPageViewModel.Factory()).get(StickerKeyboardPageViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(requir…ageViewModel::class.java)");
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel = (StickerKeyboardPageViewModel) viewModel;
        this.viewModel = stickerKeyboardPageViewModel;
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel2 = null;
        if (stickerKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            stickerKeyboardPageViewModel = null;
        }
        stickerKeyboardPageViewModel.getStickers().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StickerKeyboardPageFragment.this.updateStickerList((MappingModelList) obj);
            }
        });
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel3 = this.viewModel;
        if (stickerKeyboardPageViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            stickerKeyboardPageViewModel3 = null;
        }
        LiveData<List<KeyboardStickerPackListAdapter.StickerPack>> packs = stickerKeyboardPageViewModel3.getPacks();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        KeyboardStickerPackListAdapter keyboardStickerPackListAdapter = this.stickerPacksAdapter;
        if (keyboardStickerPackListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerPacksAdapter");
            keyboardStickerPackListAdapter = null;
        }
        packs.observe(viewLifecycleOwner, new Observer() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                KeyboardStickerPackListAdapter.this.submitList((List) obj);
            }
        });
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel4 = this.viewModel;
        if (stickerKeyboardPageViewModel4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            stickerKeyboardPageViewModel4 = null;
        }
        stickerKeyboardPageViewModel4.getSelectedPack().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StickerKeyboardPageFragment.this.updateCategoryTab((String) obj);
            }
        });
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel5 = this.viewModel;
        if (stickerKeyboardPageViewModel5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            stickerKeyboardPageViewModel2 = stickerKeyboardPageViewModel5;
        }
        stickerKeyboardPageViewModel2.refreshStickers();
    }

    public final void updateStickerList(MappingModelList mappingModelList) {
        KeyboardStickerListAdapter keyboardStickerListAdapter = null;
        if (this.firstLoad) {
            KeyboardStickerListAdapter keyboardStickerListAdapter2 = this.stickerListAdapter;
            if (keyboardStickerListAdapter2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
            } else {
                keyboardStickerListAdapter = keyboardStickerListAdapter2;
            }
            keyboardStickerListAdapter.submitList(mappingModelList, new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda8
                @Override // java.lang.Runnable
                public final void run() {
                    StickerKeyboardPageFragment.m1998updateStickerList$lambda3(StickerKeyboardPageFragment.this);
                }
            });
            this.firstLoad = false;
            return;
        }
        KeyboardStickerListAdapter keyboardStickerListAdapter3 = this.stickerListAdapter;
        if (keyboardStickerListAdapter3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
        } else {
            keyboardStickerListAdapter = keyboardStickerListAdapter3;
        }
        keyboardStickerListAdapter.submitList(mappingModelList);
    }

    /* renamed from: updateStickerList$lambda-3 */
    public static final void m1998updateStickerList$lambda3(StickerKeyboardPageFragment stickerKeyboardPageFragment) {
        Intrinsics.checkNotNullParameter(stickerKeyboardPageFragment, "this$0");
        GridLayoutManager gridLayoutManager = stickerKeyboardPageFragment.layoutManager;
        if (gridLayoutManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            gridLayoutManager = null;
        }
        gridLayoutManager.scrollToPositionWithOffset(1, 0);
    }

    public final void onTabSelected(KeyboardStickerPackListAdapter.StickerPack stickerPack) {
        scrollTo(stickerPack.getPackRecord().getPackId());
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel = this.viewModel;
        if (stickerKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            stickerKeyboardPageViewModel = null;
        }
        stickerKeyboardPageViewModel.selectPack(stickerPack.getPackRecord().getPackId());
    }

    public final void updateCategoryTab(String str) {
        RecyclerView recyclerView = this.stickerPacksRecycler;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerPacksRecycler");
            recyclerView = null;
        }
        recyclerView.post(new Runnable(str) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerKeyboardPageFragment.m1997updateCategoryTab$lambda4(StickerKeyboardPageFragment.this, this.f$1);
            }
        });
    }

    /* renamed from: updateCategoryTab$lambda-4 */
    public static final void m1997updateCategoryTab$lambda4(StickerKeyboardPageFragment stickerKeyboardPageFragment, String str) {
        Intrinsics.checkNotNullParameter(stickerKeyboardPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "$packId");
        KeyboardStickerPackListAdapter keyboardStickerPackListAdapter = stickerKeyboardPageFragment.stickerPacksAdapter;
        RecyclerView recyclerView = null;
        if (keyboardStickerPackListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerPacksAdapter");
            keyboardStickerPackListAdapter = null;
        }
        int indexOfFirst = keyboardStickerPackListAdapter.indexOfFirst(KeyboardStickerPackListAdapter.StickerPack.class, new Function1<KeyboardStickerPackListAdapter.StickerPack, Boolean>(str) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$updateCategoryTab$1$index$1
            final /* synthetic */ String $packId;

            /* access modifiers changed from: package-private */
            {
                this.$packId = r1;
            }

            public final Boolean invoke(KeyboardStickerPackListAdapter.StickerPack stickerPack) {
                return Boolean.valueOf(Intrinsics.areEqual(stickerPack.getPackRecord().getPackId(), this.$packId));
            }
        });
        if (indexOfFirst != -1) {
            RecyclerView recyclerView2 = stickerKeyboardPageFragment.stickerPacksRecycler;
            if (recyclerView2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("stickerPacksRecycler");
            } else {
                recyclerView = recyclerView2;
            }
            recyclerView.smoothScrollToPosition(indexOfFirst);
        }
    }

    private final void scrollTo(String str) {
        KeyboardStickerListAdapter keyboardStickerListAdapter = this.stickerListAdapter;
        AppBarLayout appBarLayout = null;
        if (keyboardStickerListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
            keyboardStickerListAdapter = null;
        }
        int indexOfFirst = keyboardStickerListAdapter.indexOfFirst(KeyboardStickerListAdapter.StickerHeader.class, new Function1<KeyboardStickerListAdapter.StickerHeader, Boolean>(str) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$scrollTo$index$1
            final /* synthetic */ String $packId;

            /* access modifiers changed from: package-private */
            {
                this.$packId = r1;
            }

            public final Boolean invoke(KeyboardStickerListAdapter.StickerHeader stickerHeader) {
                return Boolean.valueOf(Intrinsics.areEqual(stickerHeader.getPackId(), this.$packId));
            }
        });
        if (indexOfFirst != -1) {
            AppBarLayout appBarLayout2 = this.appBarLayout;
            if (appBarLayout2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("appBarLayout");
            } else {
                appBarLayout = appBarLayout2;
            }
            appBarLayout.setExpanded(false, true);
            this.packIdSelectionOnScroll.startAutoScrolling();
            smoothScrollToPositionTop(indexOfFirst);
        }
    }

    private final void smoothScrollToPositionTop(int i) {
        GridLayoutManager gridLayoutManager = this.layoutManager;
        GridLayoutManager gridLayoutManager2 = null;
        if (gridLayoutManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            gridLayoutManager = null;
        }
        if (Math.abs(gridLayoutManager.findFirstCompletelyVisibleItemPosition() - i) < 40) {
            StickerKeyboardPageFragment$smoothScrollToPositionTop$smoothScroller$1 stickerKeyboardPageFragment$smoothScrollToPositionTop$smoothScroller$1 = new LinearSmoothScroller(getContext()) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$smoothScrollToPositionTop$smoothScroller$1
                @Override // androidx.recyclerview.widget.LinearSmoothScroller
                protected int getVerticalSnapPreference() {
                    return -1;
                }
            };
            stickerKeyboardPageFragment$smoothScrollToPositionTop$smoothScroller$1.setTargetPosition(i);
            GridLayoutManager gridLayoutManager3 = this.layoutManager;
            if (gridLayoutManager3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            } else {
                gridLayoutManager2 = gridLayoutManager3;
            }
            gridLayoutManager2.startSmoothScroll(stickerKeyboardPageFragment$smoothScrollToPositionTop$smoothScroller$1);
            return;
        }
        GridLayoutManager gridLayoutManager4 = this.layoutManager;
        if (gridLayoutManager4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
        } else {
            gridLayoutManager2 = gridLayoutManager4;
        }
        gridLayoutManager2.scrollToPositionWithOffset(i, 0);
    }

    @Override // org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.EventListener
    public void onStickerClicked(KeyboardStickerListAdapter.Sticker sticker) {
        Intrinsics.checkNotNullParameter(sticker, StickerDatabase.TABLE_NAME);
        this.stickerThrottler.publish(new Runnable(sticker) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ KeyboardStickerListAdapter.Sticker f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerKeyboardPageFragment.m1994onStickerClicked$lambda5(StickerKeyboardPageFragment.this, this.f$1);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.EventListener
    public void onStickerLongClicked(KeyboardStickerListAdapter.Sticker sticker) {
        Intrinsics.checkNotNullParameter(sticker, StickerDatabase.TABLE_NAME);
        StickerRolloverTouchListener stickerRolloverTouchListener = this.listTouchListener;
        RecyclerView recyclerView = null;
        if (stickerRolloverTouchListener == null) {
            Intrinsics.throwUninitializedPropertyAccessException("listTouchListener");
            stickerRolloverTouchListener = null;
        }
        RecyclerView recyclerView2 = this.stickerList;
        if (recyclerView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
        } else {
            recyclerView = recyclerView2;
        }
        stickerRolloverTouchListener.enterHoverMode(recyclerView, sticker);
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerRolloverTouchListener.RolloverStickerRetriever
    public Pair<Object, String> getStickerDataFromView(View view) {
        Intrinsics.checkNotNullParameter(view, "view");
        RecyclerView recyclerView = this.stickerList;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerList");
            recyclerView = null;
        }
        int childAdapterPosition = recyclerView.getChildAdapterPosition(view);
        KeyboardStickerListAdapter keyboardStickerListAdapter = this.stickerListAdapter;
        if (keyboardStickerListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
            keyboardStickerListAdapter = null;
        }
        Optional<MappingModel<?>> model = keyboardStickerListAdapter.getModel(childAdapterPosition);
        Intrinsics.checkNotNullExpressionValue(model, "stickerListAdapter.getModel(position)");
        if (!model.isPresent() || !(model.get() instanceof KeyboardStickerListAdapter.Sticker)) {
            return null;
        }
        KeyboardStickerListAdapter.Sticker sticker = (KeyboardStickerListAdapter.Sticker) model.get();
        return new Pair<>(sticker.getUri(), sticker.getStickerRecord().getEmoji());
    }

    @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
    public void onChanged() {
        Throttler throttler = this.observerThrottler;
        StickerKeyboardPageViewModel stickerKeyboardPageViewModel = this.viewModel;
        if (stickerKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            stickerKeyboardPageViewModel = null;
        }
        throttler.publish(new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                StickerKeyboardPageViewModel.this.refreshStickers();
            }
        });
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        View view2 = getView();
        onScreenWidthChanged(view2 != null ? view2.getWidth() : 0);
    }

    private final void onScreenWidthChanged(int i) {
        GridLayoutManager gridLayoutManager = this.layoutManager;
        if (gridLayoutManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            gridLayoutManager = null;
        }
        gridLayoutManager.setSpanCount(calculateColumnCount(i));
    }

    private final int calculateColumnCount(int i) {
        return Math.max(1, (int) (((float) i) / (((float) getResources().getDimensionPixelOffset(R.dimen.sticker_page_item_width)) + ((float) getResources().getDimensionPixelOffset(R.dimen.sticker_page_item_padding)))));
    }

    /* compiled from: StickerKeyboardPageFragment.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016J \u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\nH\u0016J\u0006\u0010\u000e\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageFragment$UpdatePackSelectionOnScroll;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "(Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageFragment;)V", "doneScrolling", "", "onScrollStateChanged", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "newState", "", "onScrolled", "dx", "dy", "startAutoScrolling", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class UpdatePackSelectionOnScroll extends RecyclerView.OnScrollListener {
        private boolean doneScrolling = true;

        public UpdatePackSelectionOnScroll() {
            StickerKeyboardPageFragment.this = r1;
        }

        public final void startAutoScrolling() {
            this.doneScrolling = false;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            if (i == 0 && !this.doneScrolling) {
                this.doneScrolling = true;
                onScrolled(recyclerView, 0, 0);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            if (recyclerView.getLayoutManager() != null && this.doneScrolling) {
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                if (layoutManager != null) {
                    int findFirstCompletelyVisibleItemPosition = ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition();
                    KeyboardStickerListAdapter keyboardStickerListAdapter = StickerKeyboardPageFragment.this.stickerListAdapter;
                    StickerKeyboardPageViewModel stickerKeyboardPageViewModel = null;
                    if (keyboardStickerListAdapter == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("stickerListAdapter");
                        keyboardStickerListAdapter = null;
                    }
                    Optional<MappingModel<?>> model = keyboardStickerListAdapter.getModel(findFirstCompletelyVisibleItemPosition);
                    Intrinsics.checkNotNullExpressionValue(model, "stickerListAdapter.getModel(index)");
                    if (model.isPresent() && (model.get() instanceof KeyboardStickerListAdapter.HasPackId)) {
                        StickerKeyboardPageViewModel stickerKeyboardPageViewModel2 = StickerKeyboardPageFragment.this.viewModel;
                        if (stickerKeyboardPageViewModel2 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                        } else {
                            stickerKeyboardPageViewModel = stickerKeyboardPageViewModel2;
                        }
                        stickerKeyboardPageViewModel.selectPack(((KeyboardStickerListAdapter.HasPackId) model.get()).getPackId());
                        return;
                    }
                    return;
                }
                throw new NullPointerException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
            }
        }
    }
}
