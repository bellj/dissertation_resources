package org.thoughtcrime.securesms.keyboard.emoji;

import android.view.View;
import j$.util.function.Consumer;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconViewHolder;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoryMappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: EmojiKeyboardPageCategoriesAdapter.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0015\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002¢\u0006\u0004\b\u0006\u0010\u0007R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0004\u0010\u0005¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoriesAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "j$/util/function/Consumer", "", "onPageSelected", "Lj$/util/function/Consumer;", "<init>", "(Lj$/util/function/Consumer;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EmojiKeyboardPageCategoriesAdapter extends MappingAdapter {
    private final Consumer<String> onPageSelected;

    public EmojiKeyboardPageCategoriesAdapter(Consumer<String> consumer) {
        Intrinsics.checkNotNullParameter(consumer, "onPageSelected");
        this.onPageSelected = consumer;
        registerFactory(EmojiKeyboardPageCategoryMappingModel.RecentsMappingModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoriesAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiKeyboardPageCategoriesAdapter.m1949$r8$lambda$ufe3_nfIzuJE1iRSMVUOVHVpk(EmojiKeyboardPageCategoriesAdapter.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.keyboard_pager_category_icon));
        registerFactory(EmojiKeyboardPageCategoryMappingModel.EmojiCategoryMappingModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoriesAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiKeyboardPageCategoriesAdapter.$r8$lambda$q0Avb67N8jUlMf3uVmz0fprgw60(EmojiKeyboardPageCategoriesAdapter.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.keyboard_pager_category_icon));
    }

    /* renamed from: _init_$lambda-0 */
    public static final MappingViewHolder m1950_init_$lambda0(EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter, View view) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageCategoriesAdapter, "this$0");
        Intrinsics.checkNotNullExpressionValue(view, "v");
        return new KeyboardPageCategoryIconViewHolder(view, emojiKeyboardPageCategoriesAdapter.onPageSelected);
    }

    /* renamed from: _init_$lambda-1 */
    public static final MappingViewHolder m1951_init_$lambda1(EmojiKeyboardPageCategoriesAdapter emojiKeyboardPageCategoriesAdapter, View view) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageCategoriesAdapter, "this$0");
        Intrinsics.checkNotNullExpressionValue(view, "v");
        return new KeyboardPageCategoryIconViewHolder(view, emojiKeyboardPageCategoriesAdapter.onPageSelected);
    }
}
