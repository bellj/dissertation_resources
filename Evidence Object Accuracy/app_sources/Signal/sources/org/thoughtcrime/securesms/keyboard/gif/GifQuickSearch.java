package org.thoughtcrime.securesms.keyboard.gif;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: GifQuickSearch.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0000H\u0016J\u0010\u0010\r\u001a\u00020\u00052\u0006\u0010\f\u001a\u00020\u0000H\u0016J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013HÖ\u0003J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearch;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "gifQuickSearchOption", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "selected", "", "(Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;Z)V", "getGifQuickSearchOption", "()Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "getSelected", "()Z", "areContentsTheSame", "newItem", "areItemsTheSame", "component1", "component2", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GifQuickSearch implements MappingModel<GifQuickSearch> {
    private final GifQuickSearchOption gifQuickSearchOption;
    private final boolean selected;

    public static /* synthetic */ GifQuickSearch copy$default(GifQuickSearch gifQuickSearch, GifQuickSearchOption gifQuickSearchOption, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            gifQuickSearchOption = gifQuickSearch.gifQuickSearchOption;
        }
        if ((i & 2) != 0) {
            z = gifQuickSearch.selected;
        }
        return gifQuickSearch.copy(gifQuickSearchOption, z);
    }

    public final GifQuickSearchOption component1() {
        return this.gifQuickSearchOption;
    }

    public final boolean component2() {
        return this.selected;
    }

    public final GifQuickSearch copy(GifQuickSearchOption gifQuickSearchOption, boolean z) {
        Intrinsics.checkNotNullParameter(gifQuickSearchOption, "gifQuickSearchOption");
        return new GifQuickSearch(gifQuickSearchOption, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GifQuickSearch)) {
            return false;
        }
        GifQuickSearch gifQuickSearch = (GifQuickSearch) obj;
        return this.gifQuickSearchOption == gifQuickSearch.gifQuickSearchOption && this.selected == gifQuickSearch.selected;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(GifQuickSearch gifQuickSearch) {
        return MappingModel.CC.$default$getChangePayload(this, gifQuickSearch);
    }

    public int hashCode() {
        int hashCode = this.gifQuickSearchOption.hashCode() * 31;
        boolean z = this.selected;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "GifQuickSearch(gifQuickSearchOption=" + this.gifQuickSearchOption + ", selected=" + this.selected + ')';
    }

    public GifQuickSearch(GifQuickSearchOption gifQuickSearchOption, boolean z) {
        Intrinsics.checkNotNullParameter(gifQuickSearchOption, "gifQuickSearchOption");
        this.gifQuickSearchOption = gifQuickSearchOption;
        this.selected = z;
    }

    public final GifQuickSearchOption getGifQuickSearchOption() {
        return this.gifQuickSearchOption;
    }

    public final boolean getSelected() {
        return this.selected;
    }

    public boolean areItemsTheSame(GifQuickSearch gifQuickSearch) {
        Intrinsics.checkNotNullParameter(gifQuickSearch, "newItem");
        return this.gifQuickSearchOption == gifQuickSearch.gifQuickSearchOption;
    }

    public boolean areContentsTheSame(GifQuickSearch gifQuickSearch) {
        Intrinsics.checkNotNullParameter(gifQuickSearch, "newItem");
        return this.selected == gifQuickSearch.selected;
    }
}
