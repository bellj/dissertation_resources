package org.thoughtcrime.securesms.keyboard.emoji;

import android.content.Context;
import android.graphics.drawable.Drawable;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.emoji.EmojiCategory;
import org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: EmojiKeyboardPageCategoryMappingModel.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0002\u0016\u0017B!\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0000H\u0016J\u0010\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0000H\u0016J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u0001\u0002\u0018\u0019¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel;", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPageCategoryIconMappingModel;", "key", "", "iconId", "", "selected", "", "(Ljava/lang/String;IZ)V", "getIconId", "()I", "getKey", "()Ljava/lang/String;", "getSelected", "()Z", "areContentsTheSame", "newItem", "areItemsTheSame", "getIcon", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "EmojiCategoryMappingModel", "RecentsMappingModel", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel$RecentsMappingModel;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel$EmojiCategoryMappingModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class EmojiKeyboardPageCategoryMappingModel implements KeyboardPageCategoryIconMappingModel<EmojiKeyboardPageCategoryMappingModel> {
    private final int iconId;
    private final String key;
    private final boolean selected;

    public /* synthetic */ EmojiKeyboardPageCategoryMappingModel(String str, int i, boolean z, DefaultConstructorMarker defaultConstructorMarker) {
        this(str, i, z);
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(Object obj) {
        return MappingModel.CC.$default$getChangePayload(this, obj);
    }

    private EmojiKeyboardPageCategoryMappingModel(String str, int i, boolean z) {
        this.key = str;
        this.iconId = i;
        this.selected = z;
    }

    @Override // org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel
    public String getKey() {
        return this.key;
    }

    public final int getIconId() {
        return this.iconId;
    }

    @Override // org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel
    public boolean getSelected() {
        return this.selected;
    }

    @Override // org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel
    public Drawable getIcon(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        Drawable themedDrawable = ThemeUtil.getThemedDrawable(context, this.iconId);
        if (themedDrawable != null) {
            return themedDrawable;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public boolean areItemsTheSame(EmojiKeyboardPageCategoryMappingModel emojiKeyboardPageCategoryMappingModel) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageCategoryMappingModel, "newItem");
        return Intrinsics.areEqual(emojiKeyboardPageCategoryMappingModel.getKey(), getKey());
    }

    /* compiled from: EmojiKeyboardPageCategoryMappingModel.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0001H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel$RecentsMappingModel;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel;", "selected", "", "(Z)V", "areContentsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecentsMappingModel extends EmojiKeyboardPageCategoryMappingModel {
        public RecentsMappingModel(boolean z) {
            super(RecentEmojiPageModel.KEY, R.attr.emoji_category_recent, z, null);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoryMappingModel
        public boolean areContentsTheSame(EmojiKeyboardPageCategoryMappingModel emojiKeyboardPageCategoryMappingModel) {
            Intrinsics.checkNotNullParameter(emojiKeyboardPageCategoryMappingModel, "newItem");
            return (emojiKeyboardPageCategoryMappingModel instanceof RecentsMappingModel) && EmojiKeyboardPageCategoryMappingModel.super.areContentsTheSame(emojiKeyboardPageCategoryMappingModel);
        }
    }

    /* compiled from: EmojiKeyboardPageCategoryMappingModel.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\u0001H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel$EmojiCategoryMappingModel;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageCategoryMappingModel;", "emojiCategory", "Lorg/thoughtcrime/securesms/emoji/EmojiCategory;", "selected", "", "(Lorg/thoughtcrime/securesms/emoji/EmojiCategory;Z)V", "areContentsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EmojiCategoryMappingModel extends EmojiKeyboardPageCategoryMappingModel {
        private final EmojiCategory emojiCategory;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public EmojiCategoryMappingModel(EmojiCategory emojiCategory, boolean z) {
            super(emojiCategory.getKey(), emojiCategory.getIcon(), z, null);
            Intrinsics.checkNotNullParameter(emojiCategory, "emojiCategory");
            this.emojiCategory = emojiCategory;
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoryMappingModel
        public boolean areContentsTheSame(EmojiKeyboardPageCategoryMappingModel emojiKeyboardPageCategoryMappingModel) {
            Intrinsics.checkNotNullParameter(emojiKeyboardPageCategoryMappingModel, "newItem");
            return (emojiKeyboardPageCategoryMappingModel instanceof EmojiCategoryMappingModel) && EmojiKeyboardPageCategoryMappingModel.super.areContentsTheSame(emojiKeyboardPageCategoryMappingModel) && ((EmojiCategoryMappingModel) emojiKeyboardPageCategoryMappingModel).emojiCategory == this.emojiCategory;
        }
    }

    public boolean areContentsTheSame(EmojiKeyboardPageCategoryMappingModel emojiKeyboardPageCategoryMappingModel) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageCategoryMappingModel, "newItem");
        return areItemsTheSame(emojiKeyboardPageCategoryMappingModel) && getSelected() == emojiKeyboardPageCategoryMappingModel.getSelected();
    }
}
