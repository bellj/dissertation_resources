package org.thoughtcrime.securesms.keyboard.sticker;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;
import org.thoughtcrime.securesms.keyboard.sticker.StickerSearchViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.util.InsetItemDecoration;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: StickerSearchDialogFragment.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 +2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001+B\u0005¢\u0006\u0002\u0010\u0004J\u0012\u0010\u000f\u001a\u00020\u00102\b\b\u0001\u0010\u0011\u001a\u00020\u0010H\u0002J\u0012\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016J&\u0010\u0016\u001a\u0004\u0018\u00010\f2\u0006\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016JR\u0010\u001b\u001a\u00020\u00132\b\u0010\u001c\u001a\u0004\u0018\u00010\f2\u0006\u0010\u001d\u001a\u00020\u00102\u0006\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020\u00102\u0006\u0010 \u001a\u00020\u00102\u0006\u0010!\u001a\u00020\u00102\u0006\u0010\"\u001a\u00020\u00102\u0006\u0010#\u001a\u00020\u00102\u0006\u0010$\u001a\u00020\u0010H\u0016J\u0010\u0010%\u001a\u00020\u00132\u0006\u0010&\u001a\u00020'H\u0016J\u0010\u0010(\u001a\u00020\u00132\u0006\u0010&\u001a\u00020'H\u0016J\u001a\u0010)\u001a\u00020\u00132\u0006\u0010*\u001a\u00020\f2\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchDialogFragment;", "Landroidx/fragment/app/DialogFragment;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$EventListener;", "Landroid/view/View$OnLayoutChangeListener;", "()V", "adapter", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter;", "layoutManager", "Landroidx/recyclerview/widget/GridLayoutManager;", "list", "Landroidx/recyclerview/widget/RecyclerView;", "noResults", "Landroid/view/View;", "search", "Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView;", "calculateColumnCount", "", "screenWidth", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onLayoutChange", "v", "left", "top", "right", "bottom", "oldLeft", "oldTop", "oldRight", "oldBottom", "onStickerClicked", StickerDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$Sticker;", "onStickerLongClicked", "onViewCreated", "view", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerSearchDialogFragment extends DialogFragment implements KeyboardStickerListAdapter.EventListener, View.OnLayoutChangeListener {
    public static final Companion Companion = new Companion(null);
    private KeyboardStickerListAdapter adapter;
    private GridLayoutManager layoutManager;
    private RecyclerView list;
    private View noResults;
    private KeyboardPageSearchView search;

    @JvmStatic
    public static final void show(FragmentManager fragmentManager) {
        Companion.show(fragmentManager);
    }

    @Override // org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.EventListener
    public void onStickerLongClicked(KeyboardStickerListAdapter.Sticker sticker) {
        Intrinsics.checkNotNullParameter(sticker, StickerDatabase.TABLE_NAME);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.Signal_DayNight_Dialog_Animated_Bottom);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.sticker_search_dialog_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.sticker_search_text);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.sticker_search_text)");
        this.search = (KeyboardPageSearchView) findViewById;
        View findViewById2 = view.findViewById(R.id.sticker_search_list);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.sticker_search_list)");
        this.list = (RecyclerView) findViewById2;
        View findViewById3 = view.findViewById(R.id.sticker_search_no_results);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.sticker_search_no_results)");
        this.noResults = findViewById3;
        GlideRequests with = GlideApp.with(this);
        Intrinsics.checkNotNullExpressionValue(with, "with(this)");
        this.adapter = new KeyboardStickerListAdapter(with, this, DeviceProperties.shouldAllowApngStickerAnimation(requireContext()));
        this.layoutManager = new GridLayoutManager(requireContext(), 2);
        RecyclerView recyclerView = this.list;
        KeyboardPageSearchView keyboardPageSearchView = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("list");
            recyclerView = null;
        }
        GridLayoutManager gridLayoutManager = this.layoutManager;
        if (gridLayoutManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            gridLayoutManager = null;
        }
        recyclerView.setLayoutManager(gridLayoutManager);
        RecyclerView recyclerView2 = this.list;
        if (recyclerView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("list");
            recyclerView2 = null;
        }
        KeyboardStickerListAdapter keyboardStickerListAdapter = this.adapter;
        if (keyboardStickerListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("adapter");
            keyboardStickerListAdapter = null;
        }
        recyclerView2.setAdapter(keyboardStickerListAdapter);
        RecyclerView recyclerView3 = this.list;
        if (recyclerView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("list");
            recyclerView3 = null;
        }
        recyclerView3.addItemDecoration(new InsetItemDecoration(new StickerInsetSetter()));
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ViewModel viewModel = new ViewModelProvider(this, new StickerSearchViewModel.Factory(requireContext)).get(StickerSearchViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …rchViewModel::class.java)");
        StickerSearchViewModel stickerSearchViewModel = (StickerSearchViewModel) viewModel;
        stickerSearchViewModel.getSearchResults().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerSearchDialogFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                StickerSearchDialogFragment.m2007onViewCreated$lambda0(StickerSearchDialogFragment.this, (List) obj);
            }
        });
        KeyboardPageSearchView keyboardPageSearchView2 = this.search;
        if (keyboardPageSearchView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("search");
            keyboardPageSearchView2 = null;
        }
        KeyboardPageSearchView.enableBackNavigation$default(keyboardPageSearchView2, false, 1, null);
        KeyboardPageSearchView keyboardPageSearchView3 = this.search;
        if (keyboardPageSearchView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("search");
            keyboardPageSearchView3 = null;
        }
        keyboardPageSearchView3.setCallbacks(new KeyboardPageSearchView.Callbacks(stickerSearchViewModel, this, view) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerSearchDialogFragment$onViewCreated$2
            final /* synthetic */ View $view;
            final /* synthetic */ StickerSearchViewModel $viewModel;
            final /* synthetic */ StickerSearchDialogFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$viewModel = r1;
                this.this$0 = r2;
                this.$view = r3;
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onClicked() {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onClicked(this);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onFocusGained() {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusGained(this);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onFocusLost() {
                KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusLost(this);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onQueryChanged(String str) {
                Intrinsics.checkNotNullParameter(str, "query");
                this.$viewModel.query(str);
            }

            @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
            public void onNavigationClicked() {
                ViewUtil.hideKeyboard(this.this$0.requireContext(), this.$view);
                this.this$0.dismissAllowingStateLoss();
            }
        });
        KeyboardPageSearchView keyboardPageSearchView4 = this.search;
        if (keyboardPageSearchView4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("search");
        } else {
            keyboardPageSearchView = keyboardPageSearchView4;
        }
        keyboardPageSearchView.requestFocus();
        view.addOnLayoutChangeListener(this);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m2007onViewCreated$lambda0(StickerSearchDialogFragment stickerSearchDialogFragment, List list) {
        Intrinsics.checkNotNullParameter(stickerSearchDialogFragment, "this$0");
        KeyboardStickerListAdapter keyboardStickerListAdapter = stickerSearchDialogFragment.adapter;
        View view = null;
        if (keyboardStickerListAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("adapter");
            keyboardStickerListAdapter = null;
        }
        keyboardStickerListAdapter.submitList(list);
        View view2 = stickerSearchDialogFragment.noResults;
        if (view2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("noResults");
        } else {
            view = view2;
        }
        view.setVisibility(list.isEmpty() ? 0 : 8);
    }

    @Override // android.view.View.OnLayoutChangeListener
    public void onLayoutChange(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        GridLayoutManager gridLayoutManager = this.layoutManager;
        if (gridLayoutManager == null) {
            Intrinsics.throwUninitializedPropertyAccessException("layoutManager");
            gridLayoutManager = null;
        }
        View view2 = getView();
        gridLayoutManager.setSpanCount(calculateColumnCount(view2 != null ? view2.getWidth() : 0));
    }

    private final int calculateColumnCount(int i) {
        return Math.max(1, (int) (((float) i) / (((float) getResources().getDimensionPixelOffset(R.dimen.sticker_page_item_width)) + ((float) getResources().getDimensionPixelOffset(R.dimen.sticker_page_item_padding)))));
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x002b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v3, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v6, types: [org.thoughtcrime.securesms.stickers.StickerEventListener] */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARN: Type inference failed for: r0v12 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.EventListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onStickerClicked(org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.Sticker r3) {
        /*
            r2 = this;
            java.lang.String r0 = "sticker"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r3, r0)
            android.content.Context r0 = r2.requireContext()
            android.view.View r1 = r2.requireView()
            org.thoughtcrime.securesms.util.ViewUtil.hideKeyboard(r0, r1)
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x0014:
            if (r0 == 0) goto L_0x0020
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stickers.StickerEventListener
            if (r1 == 0) goto L_0x001b
            goto L_0x002b
        L_0x001b:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0014
        L_0x0020:
            androidx.fragment.app.FragmentActivity r0 = r2.requireActivity()
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.stickers.StickerEventListener
            if (r1 != 0) goto L_0x0029
            r0 = 0
        L_0x0029:
            org.thoughtcrime.securesms.stickers.StickerEventListener r0 = (org.thoughtcrime.securesms.stickers.StickerEventListener) r0
        L_0x002b:
            org.thoughtcrime.securesms.stickers.StickerEventListener r0 = (org.thoughtcrime.securesms.stickers.StickerEventListener) r0
            if (r0 == 0) goto L_0x0036
            org.thoughtcrime.securesms.database.model.StickerRecord r3 = r3.getStickerRecord()
            r0.onStickerSelected(r3)
        L_0x0036:
            r2.dismissAllowingStateLoss()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyboard.sticker.StickerSearchDialogFragment.onStickerClicked(org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter$Sticker):void");
    }

    /* compiled from: StickerSearchDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchDialogFragment$Companion;", "", "()V", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            new StickerSearchDialogFragment().show(fragmentManager, "TAG");
        }
    }
}
