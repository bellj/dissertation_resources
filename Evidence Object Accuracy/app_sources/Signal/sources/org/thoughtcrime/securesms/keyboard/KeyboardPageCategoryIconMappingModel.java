package org.thoughtcrime.securesms.keyboard;

import android.content.Context;
import android.graphics.drawable.Drawable;
import kotlin.Metadata;
import org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: KeyboardPageCategoryIconViewHolder.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006R\u0012\u0010\u0007\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/KeyboardPageCategoryIconMappingModel;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "key", "", "getKey", "()Ljava/lang/String;", "selected", "", "getSelected", "()Z", "getIcon", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface KeyboardPageCategoryIconMappingModel<T extends KeyboardPageCategoryIconMappingModel<T>> extends MappingModel<T> {
    Drawable getIcon(Context context);

    String getKey();

    boolean getSelected();
}
