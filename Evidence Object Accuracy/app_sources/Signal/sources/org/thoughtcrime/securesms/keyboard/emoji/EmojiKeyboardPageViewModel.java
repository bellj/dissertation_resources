package org.thoughtcrime.securesms.keyboard.emoji;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Consumer;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiCategory;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageCategoryMappingModel;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: EmojiKeyboardPageViewModel.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 \u001b2\u00020\u0001:\u0002\u001b\u001cB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0012J\u0006\u0010\u001a\u001a\u00020\u0018R\u001d\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00120\f8F¢\u0006\u0006\u001a\u0004\b\u0016\u0010\u000f¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageRepository;", "(Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageRepository;)V", "allEmojiModels", "Landroidx/lifecycle/MutableLiveData;", "", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "getAllEmojiModels", "()Landroidx/lifecycle/MutableLiveData;", "categories", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "getCategories", "()Landroidx/lifecycle/LiveData;", "internalSelectedKey", "Lorg/thoughtcrime/securesms/util/DefaultValueLiveData;", "", "pages", "getPages", "selectedKey", "getSelectedKey", "onKeySelected", "", "key", "refreshRecentEmoji", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiKeyboardPageViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private final MutableLiveData<List<EmojiPageModel>> allEmojiModels;
    private final LiveData<MappingModelList> categories;
    private final DefaultValueLiveData<String> internalSelectedKey;
    private final LiveData<MappingModelList> pages;
    private final EmojiKeyboardPageRepository repository;

    public EmojiKeyboardPageViewModel(EmojiKeyboardPageRepository emojiKeyboardPageRepository) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageRepository, "repository");
        this.repository = emojiKeyboardPageRepository;
        DefaultValueLiveData<String> defaultValueLiveData = new DefaultValueLiveData<>(Companion.getStartingTab());
        this.internalSelectedKey = defaultValueLiveData;
        MutableLiveData<List<EmojiPageModel>> mutableLiveData = new MutableLiveData<>();
        this.allEmojiModels = mutableLiveData;
        LiveData<MappingModelList> mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageViewModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiKeyboardPageViewModel.m1965_init_$lambda1((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapAsync, "mapAsync(allEmojiModels)…      }\n\n      list\n    }");
        this.pages = mapAsync;
        LiveData<MappingModelList> combineLatest = LiveDataUtil.combineLatest(mutableLiveData, defaultValueLiveData, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return EmojiKeyboardPageViewModel.m1966_init_$lambda3((List) obj, (String) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(allEmojiMo…\n      }\n      list\n    }");
        this.categories = combineLatest;
    }

    public final LiveData<String> getSelectedKey() {
        return this.internalSelectedKey;
    }

    public final MutableLiveData<List<EmojiPageModel>> getAllEmojiModels() {
        return this.allEmojiModels;
    }

    public final LiveData<MappingModelList> getPages() {
        return this.pages;
    }

    public final LiveData<MappingModelList> getCategories() {
        return this.categories;
    }

    /* renamed from: _init_$lambda-1 */
    public static final MappingModelList m1965_init_$lambda1(List list) {
        MappingModelList mappingModelList = new MappingModelList();
        Intrinsics.checkNotNullExpressionValue(list, "models");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            EmojiPageModel emojiPageModel = (EmojiPageModel) it.next();
            if (!Intrinsics.areEqual(RecentEmojiPageModel.KEY, emojiPageModel.getKey())) {
                EmojiCategory.Companion companion = EmojiCategory.Companion;
                String key = emojiPageModel.getKey();
                Intrinsics.checkNotNullExpressionValue(key, "pageModel.key");
                mappingModelList.add(new EmojiPageViewGridAdapter.EmojiHeader(emojiPageModel.getKey(), companion.forKey(key).getCategoryLabel()));
                boolean unused = CollectionsKt__MutableCollectionsKt.addAll(mappingModelList, EmojiPageModelExtensionsKt.toMappingModels(emojiPageModel));
            } else {
                List<Emoji> displayEmoji = emojiPageModel.getDisplayEmoji();
                Intrinsics.checkNotNullExpressionValue(displayEmoji, "pageModel.displayEmoji");
                if (!displayEmoji.isEmpty()) {
                    mappingModelList.add(new EmojiPageViewGridAdapter.EmojiHeader(emojiPageModel.getKey(), R.string.ReactWithAnyEmojiBottomSheetDialogFragment__recently_used));
                    boolean unused2 = CollectionsKt__MutableCollectionsKt.addAll(mappingModelList, EmojiPageModelExtensionsKt.toMappingModels(emojiPageModel));
                }
            }
        }
        return mappingModelList;
    }

    /* renamed from: _init_$lambda-3 */
    public static final MappingModelList m1966_init_$lambda3(List list, String str) {
        Object obj;
        Intrinsics.checkNotNullParameter(list, "models");
        Intrinsics.checkNotNullParameter(str, "selectedKey");
        MappingModelList mappingModelList = new MappingModelList();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            EmojiPageModel emojiPageModel = (EmojiPageModel) it.next();
            if (Intrinsics.areEqual(RecentEmojiPageModel.KEY, emojiPageModel.getKey())) {
                obj = new EmojiKeyboardPageCategoryMappingModel.RecentsMappingModel(Intrinsics.areEqual(emojiPageModel.getKey(), str));
            } else {
                EmojiCategory.Companion companion = EmojiCategory.Companion;
                String key = emojiPageModel.getKey();
                Intrinsics.checkNotNullExpressionValue(key, "m.key");
                EmojiCategory forKey = companion.forKey(key);
                obj = new EmojiKeyboardPageCategoryMappingModel.EmojiCategoryMappingModel(forKey, Intrinsics.areEqual(forKey.getKey(), str));
            }
            arrayList.add(obj);
        }
        boolean unused = CollectionsKt__MutableCollectionsKt.addAll(mappingModelList, arrayList);
        return mappingModelList;
    }

    public final void onKeySelected(String str) {
        Intrinsics.checkNotNullParameter(str, "key");
        this.internalSelectedKey.setValue(str);
    }

    public final void refreshRecentEmoji() {
        this.repository.getEmoji(new Consumer() { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((List) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    /* compiled from: EmojiKeyboardPageViewModel.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageViewModel$Companion;", "", "()V", "getStartingTab", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final String getStartingTab() {
            if (RecentEmojiPageModel.hasRecents(ApplicationDependencies.getApplication(), TextSecurePreferences.RECENT_STORAGE_KEY)) {
                return RecentEmojiPageModel.KEY;
            }
            return EmojiCategory.PEOPLE.getKey();
        }
    }

    /* compiled from: EmojiKeyboardPageViewModel.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "repository", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageRepository;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final EmojiKeyboardPageRepository repository;

        public Factory(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            this.repository = new EmojiKeyboardPageRepository(context);
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new EmojiKeyboardPageViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
