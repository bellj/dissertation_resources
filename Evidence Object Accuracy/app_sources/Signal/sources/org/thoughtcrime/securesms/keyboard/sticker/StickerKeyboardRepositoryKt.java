package org.thoughtcrime.securesms.keyboard.sticker;

import kotlin.Metadata;

/* compiled from: StickerKeyboardRepository.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000¨\u0006\u0004"}, d2 = {"RECENT_LIMIT", "", "RECENT_PACK_ID", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerKeyboardRepositoryKt {
    private static final int RECENT_LIMIT;
    private static final String RECENT_PACK_ID;
}
