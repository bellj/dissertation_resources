package org.thoughtcrime.securesms.keyboard.emoji.search;

import android.content.Context;
import android.net.Uri;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: EmojiSearchRepository.kt */
@Metadata(bv = {}, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0015B\u000f\u0012\u0006\u0010\u000e\u001a\u00020\r¢\u0006\u0004\b\u0013\u0010\u0014J.\u0010\f\u001a\u00020\u000b2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bR\u0014\u0010\u000e\u001a\u00020\r8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0011\u001a\u00020\u00108\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchRepository;", "", "", "query", "", "includeRecents", "", "limit", "j$/util/function/Consumer", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "consumer", "", "submitQuery", "Landroid/content/Context;", "context", "Landroid/content/Context;", "Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "emojiSearchDatabase", "Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "<init>", "(Landroid/content/Context;)V", "EmojiSearchResultsPageModel", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EmojiSearchRepository {
    private final Context context;
    private final EmojiSearchDatabase emojiSearchDatabase = SignalDatabase.Companion.emojiSearch();

    public EmojiSearchRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public static /* synthetic */ void submitQuery$default(EmojiSearchRepository emojiSearchRepository, String str, boolean z, int i, Consumer consumer, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 20;
        }
        emojiSearchRepository.submitQuery(str, z, i, consumer);
    }

    public final void submitQuery(String str, boolean z, int i, Consumer<EmojiPageModel> consumer) {
        Intrinsics.checkNotNullParameter(str, "query");
        Intrinsics.checkNotNullParameter(consumer, "consumer");
        if (str.length() >= 1 || !z) {
            SignalExecutors.SERIAL.execute(new Runnable(str, i, consumer) { // from class: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchRepository$$ExternalSyntheticLambda0
                public final /* synthetic */ String f$1;
                public final /* synthetic */ int f$2;
                public final /* synthetic */ Consumer f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    EmojiSearchRepository.m1980submitQuery$lambda2(EmojiSearchRepository.this, this.f$1, this.f$2, this.f$3);
                }
            });
        } else {
            consumer.accept(new RecentEmojiPageModel(this.context, TextSecurePreferences.RECENT_STORAGE_KEY));
        }
    }

    /* renamed from: submitQuery$lambda-2 */
    public static final void m1980submitQuery$lambda2(EmojiSearchRepository emojiSearchRepository, String str, int i, Consumer consumer) {
        Intrinsics.checkNotNullParameter(emojiSearchRepository, "this$0");
        Intrinsics.checkNotNullParameter(str, "$query");
        Intrinsics.checkNotNullParameter(consumer, "$consumer");
        List<String> query = emojiSearchRepository.emojiSearchDatabase.query(str, i);
        Intrinsics.checkNotNullExpressionValue(query, "emojiSearchDatabase.query(query, limit)");
        ArrayList<List> arrayList = new ArrayList();
        for (String str2 : query) {
            List<String> list = EmojiSource.Companion.getLatest().getCanonicalToVariations().get(str2);
            if (list != null) {
                arrayList.add(list);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (List list2 : arrayList) {
            arrayList2.add(new Emoji(list2));
        }
        consumer.accept(new EmojiSearchResultsPageModel(query, arrayList2));
    }

    /* compiled from: EmojiSearchRepository.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\b\u0002\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0002\u0010\u0007J\u000e\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003H\u0016J\u000e\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u0016J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u0004H\u0016J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchRepository$EmojiSearchResultsPageModel;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "emoji", "", "", "displayEmoji", "Lorg/thoughtcrime/securesms/components/emoji/Emoji;", "(Ljava/util/List;Ljava/util/List;)V", "getDisplayEmoji", "getEmoji", "getIconAttr", "", "getKey", "getSpriteUri", "Landroid/net/Uri;", "isDynamic", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EmojiSearchResultsPageModel implements EmojiPageModel {
        private final List<Emoji> displayEmoji;
        private final List<String> emoji;

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
        public int getIconAttr() {
            return -1;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
        public String getKey() {
            return "";
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
        public Uri getSpriteUri() {
            return null;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
        public boolean isDynamic() {
            return false;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.components.emoji.Emoji> */
        /* JADX WARN: Multi-variable type inference failed */
        public EmojiSearchResultsPageModel(List<String> list, List<? extends Emoji> list2) {
            Intrinsics.checkNotNullParameter(list, "emoji");
            Intrinsics.checkNotNullParameter(list2, "displayEmoji");
            this.emoji = list;
            this.displayEmoji = list2;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
        public List<String> getEmoji() {
            return this.emoji;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
        public List<Emoji> getDisplayEmoji() {
            return this.displayEmoji;
        }
    }
}
