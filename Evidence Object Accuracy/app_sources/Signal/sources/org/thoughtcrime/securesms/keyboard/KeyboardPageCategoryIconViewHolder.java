package org.thoughtcrime.securesms.keyboard;

import android.content.Context;
import android.view.View;
import androidx.appcompat.widget.AppCompatImageView;
import j$.util.function.Consumer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: KeyboardPageCategoryIconViewHolder.kt */
@Metadata(bv = {}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000*\u000e\b\u0000\u0010\u0002*\b\u0012\u0004\u0012\u00028\u00000\u00012\b\u0012\u0004\u0012\u00028\u00000\u0003B\u001d\u0012\u0006\u0010\u0012\u001a\u00020\u000f\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\u0004\b\u0013\u0010\u0014J\u0017\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00028\u0000H\u0016¢\u0006\u0004\b\u0006\u0010\u0007R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000bR\u0014\u0010\r\u001a\u00020\f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u0014\u0010\u0010\u001a\u00020\u000f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0010\u0010\u0011¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/KeyboardPageCategoryIconViewHolder;", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPageCategoryIconMappingModel;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "model", "", "bind", "(Lorg/thoughtcrime/securesms/keyboard/KeyboardPageCategoryIconMappingModel;)V", "j$/util/function/Consumer", "", "onPageSelected", "Lj$/util/function/Consumer;", "Landroidx/appcompat/widget/AppCompatImageView;", "iconView", "Landroidx/appcompat/widget/AppCompatImageView;", "Landroid/view/View;", "iconSelected", "Landroid/view/View;", "itemView", "<init>", "(Landroid/view/View;Lj$/util/function/Consumer;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class KeyboardPageCategoryIconViewHolder<T extends KeyboardPageCategoryIconMappingModel<T>> extends MappingViewHolder<T> {
    private final View iconSelected;
    private final AppCompatImageView iconView;
    private final Consumer<String> onPageSelected;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public KeyboardPageCategoryIconViewHolder(View view, Consumer<String> consumer) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
        Intrinsics.checkNotNullParameter(consumer, "onPageSelected");
        this.onPageSelected = consumer;
        View findViewById = view.findViewById(R.id.category_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.category_icon)");
        this.iconView = (AppCompatImageView) findViewById;
        View findViewById2 = view.findViewById(R.id.category_icon_selected);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.category_icon_selected)");
        this.iconSelected = findViewById2;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconViewHolder<T extends org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconMappingModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
    public /* bridge */ /* synthetic */ void bind(Object obj) {
        bind((KeyboardPageCategoryIconViewHolder<T>) ((KeyboardPageCategoryIconMappingModel) obj));
    }

    public void bind(T t) {
        Intrinsics.checkNotNullParameter(t, "model");
        this.itemView.setOnClickListener(new View.OnClickListener(t) { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPageCategoryIconViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ KeyboardPageCategoryIconMappingModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                KeyboardPageCategoryIconViewHolder.$r8$lambda$HHbdaluL2Ywc3OZWUT6uT2KZBlI(KeyboardPageCategoryIconViewHolder.this, this.f$1, view);
            }
        });
        AppCompatImageView appCompatImageView = this.iconView;
        Context context = this.context;
        Intrinsics.checkNotNullExpressionValue(context, "context");
        appCompatImageView.setImageDrawable(t.getIcon(context));
        this.iconView.setSelected(t.getSelected());
        this.iconSelected.setSelected(t.getSelected());
    }

    /* renamed from: bind$lambda-0 */
    public static final void m1942bind$lambda0(KeyboardPageCategoryIconViewHolder keyboardPageCategoryIconViewHolder, KeyboardPageCategoryIconMappingModel keyboardPageCategoryIconMappingModel, View view) {
        Intrinsics.checkNotNullParameter(keyboardPageCategoryIconViewHolder, "this$0");
        Intrinsics.checkNotNullParameter(keyboardPageCategoryIconMappingModel, "$model");
        keyboardPageCategoryIconViewHolder.onPageSelected.accept(keyboardPageCategoryIconMappingModel.getKey());
    }
}
