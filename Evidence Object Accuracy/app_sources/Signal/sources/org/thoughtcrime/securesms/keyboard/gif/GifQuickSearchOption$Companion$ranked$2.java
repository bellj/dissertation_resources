package org.thoughtcrime.securesms.keyboard.gif;

import java.util.Comparator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;

/* compiled from: GifQuickSearchOption.kt */
@Metadata(d1 = {"\u0000\f\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class GifQuickSearchOption$Companion$ranked$2 extends Lambda implements Function0<List<? extends GifQuickSearchOption>> {
    public static final GifQuickSearchOption$Companion$ranked$2 INSTANCE = new GifQuickSearchOption$Companion$ranked$2();

    GifQuickSearchOption$Companion$ranked$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final List<? extends GifQuickSearchOption> invoke() {
        return ArraysKt___ArraysKt.sortedWith(GifQuickSearchOption.values(), new Comparator() { // from class: org.thoughtcrime.securesms.keyboard.gif.GifQuickSearchOption$Companion$ranked$2$invoke$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return ComparisonsKt__ComparisonsKt.compareValues(Integer.valueOf(((GifQuickSearchOption) t).rank), Integer.valueOf(((GifQuickSearchOption) t2).rank));
            }
        });
    }
}
