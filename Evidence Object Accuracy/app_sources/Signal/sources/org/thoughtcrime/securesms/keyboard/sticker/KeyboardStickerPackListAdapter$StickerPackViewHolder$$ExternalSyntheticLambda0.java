package org.thoughtcrime.securesms.keyboard.sticker;

import android.view.View;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerPackListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class KeyboardStickerPackListAdapter$StickerPackViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ KeyboardStickerPackListAdapter f$0;
    public final /* synthetic */ KeyboardStickerPackListAdapter.StickerPack f$1;

    public /* synthetic */ KeyboardStickerPackListAdapter$StickerPackViewHolder$$ExternalSyntheticLambda0(KeyboardStickerPackListAdapter keyboardStickerPackListAdapter, KeyboardStickerPackListAdapter.StickerPack stickerPack) {
        this.f$0 = keyboardStickerPackListAdapter;
        this.f$1 = stickerPack;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        KeyboardStickerPackListAdapter.StickerPackViewHolder.m1991bind$lambda0(this.f$0, this.f$1, view);
    }
}
