package org.thoughtcrime.securesms.keyboard.gif;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Fragment;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4SaveResult;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* compiled from: GifKeyboardPageFragment.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001$B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0010\u0010\u0014\u001a\u00020\u00102\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0010H\u0002J\u0012\u0010\u0018\u001a\u00020\u00102\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00102\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u001a\u0010\u001e\u001a\u00020\u00102\u0006\u0010\u001f\u001a\u00020 2\b\u0010\u0019\u001a\u0004\u0018\u00010\u001aH\u0016J\b\u0010!\u001a\u00020\u0010H\u0002J\b\u0010\"\u001a\u00020\u0010H\u0002J\b\u0010#\u001a\u00020\u0010H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifKeyboardPageFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "giphyMp4ViewModel", "Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4ViewModel;", "host", "Lorg/thoughtcrime/securesms/keyboard/gif/GifKeyboardPageFragment$Host;", "progressDialog", "Landroidx/appcompat/app/AlertDialog;", "quickSearchAdapter", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchAdapter;", "quickSearchList", "Landroidx/recyclerview/widget/RecyclerView;", "viewModel", "Lorg/thoughtcrime/securesms/keyboard/gif/GifKeyboardPageViewModel;", "handleGiphyMp4ErrorResult", "", "handleGiphyMp4SaveResult", MediaSendActivityResult.EXTRA_RESULT, "Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4SaveResult;", "handleGiphyMp4SuccessfulResult", "success", "Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4SaveResult$Success;", "hideProgressDialog", "onActivityCreated", "savedInstanceState", "Landroid/os/Bundle;", "onQuickSearchSelected", "gifQuickSearchOption", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "onViewCreated", "view", "Landroid/view/View;", "openGifSearch", "scrollToTab", "updateQuickSearchTabs", "Host", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GifKeyboardPageFragment extends LoggingFragment {
    private GiphyMp4ViewModel giphyMp4ViewModel;
    private Host host;
    private AlertDialog progressDialog;
    private GifQuickSearchAdapter quickSearchAdapter;
    private RecyclerView quickSearchList;
    private GifKeyboardPageViewModel viewModel;

    /* compiled from: GifKeyboardPageFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J \u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\tH&J\b\u0010\u000b\u001a\u00020\u0005H&¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifKeyboardPageFragment$Host;", "", "isMms", "", "onGifSelectSuccess", "", "blobUri", "Landroid/net/Uri;", "width", "", "height", "openGifSearch", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Host {
        boolean isMms();

        void onGifSelectSuccess(Uri uri, int i, int i2);

        void openGifSearch();
    }

    public GifKeyboardPageFragment() {
        super(R.layout.gif_keyboard_page_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Host host;
        Intrinsics.checkNotNullParameter(view, "view");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        host = (Host) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment.Host");
                    }
                } else if (fragment instanceof Host) {
                    host = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.host = host;
            FragmentTransaction beginTransaction = getChildFragmentManager().beginTransaction();
            Host host2 = this.host;
            GiphyMp4ViewModel giphyMp4ViewModel = null;
            if (host2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("host");
                host2 = null;
            }
            beginTransaction.replace(R.id.gif_keyboard_giphy_frame, GiphyMp4Fragment.create(host2.isMms())).commitAllowingStateLoss();
            View findViewById = view.findViewById(R.id.gif_keyboard_search_text);
            Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.gif_keyboard_search_text)");
            ((KeyboardPageSearchView) findViewById).setCallbacks(new KeyboardPageSearchView.Callbacks(this) { // from class: org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment$onViewCreated$1
                final /* synthetic */ GifKeyboardPageFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
                public void onFocusGained() {
                    KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusGained(this);
                }

                @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
                public void onFocusLost() {
                    KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusLost(this);
                }

                @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
                public void onNavigationClicked() {
                    KeyboardPageSearchView.Callbacks.DefaultImpls.onNavigationClicked(this);
                }

                @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
                public void onQueryChanged(String str) {
                    KeyboardPageSearchView.Callbacks.DefaultImpls.onQueryChanged(this, str);
                }

                @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
                public void onClicked() {
                    this.this$0.openGifSearch();
                }
            });
            view.findViewById(R.id.gif_keyboard_search).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    GifKeyboardPageFragment.m1984onViewCreated$lambda0(GifKeyboardPageFragment.this, view2);
                }
            });
            View findViewById2 = view.findViewById(R.id.gif_keyboard_quick_search_recycler);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.g…rd_quick_search_recycler)");
            this.quickSearchList = (RecyclerView) findViewById2;
            this.quickSearchAdapter = new GifQuickSearchAdapter(new Function1<GifQuickSearchOption, Unit>(this) { // from class: org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment$onViewCreated$3
                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(GifQuickSearchOption gifQuickSearchOption) {
                    invoke(gifQuickSearchOption);
                    return Unit.INSTANCE;
                }

                public final void invoke(GifQuickSearchOption gifQuickSearchOption) {
                    Intrinsics.checkNotNullParameter(gifQuickSearchOption, "p0");
                    ((GifKeyboardPageFragment) this.receiver).onQuickSearchSelected(gifQuickSearchOption);
                }
            });
            RecyclerView recyclerView = this.quickSearchList;
            if (recyclerView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("quickSearchList");
                recyclerView = null;
            }
            GifQuickSearchAdapter gifQuickSearchAdapter = this.quickSearchAdapter;
            if (gifQuickSearchAdapter == null) {
                Intrinsics.throwUninitializedPropertyAccessException("quickSearchAdapter");
                gifQuickSearchAdapter = null;
            }
            recyclerView.setAdapter(gifQuickSearchAdapter);
            FragmentActivity requireActivity2 = requireActivity();
            Host host3 = this.host;
            if (host3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("host");
                host3 = null;
            }
            ViewModel viewModel = new ViewModelProvider(requireActivity2, new GiphyMp4ViewModel.Factory(host3.isMms())).get(GiphyMp4ViewModel.class);
            Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(requir…Mp4ViewModel::class.java)");
            GiphyMp4ViewModel giphyMp4ViewModel2 = (GiphyMp4ViewModel) viewModel;
            this.giphyMp4ViewModel = giphyMp4ViewModel2;
            if (giphyMp4ViewModel2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("giphyMp4ViewModel");
            } else {
                giphyMp4ViewModel = giphyMp4ViewModel2;
            }
            giphyMp4ViewModel.getSaveResultEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment$$ExternalSyntheticLambda2
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    GifKeyboardPageFragment.this.handleGiphyMp4SaveResult((GiphyMp4SaveResult) obj);
                }
            });
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1984onViewCreated$lambda0(GifKeyboardPageFragment gifKeyboardPageFragment, View view) {
        Intrinsics.checkNotNullParameter(gifKeyboardPageFragment, "this$0");
        gifKeyboardPageFragment.openGifSearch();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ViewModel viewModel = new ViewModelProvider(requireActivity()).get(GifKeyboardPageViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(requir…ageViewModel::class.java)");
        this.viewModel = (GifKeyboardPageViewModel) viewModel;
        updateQuickSearchTabs();
    }

    public final void onQuickSearchSelected(GifQuickSearchOption gifQuickSearchOption) {
        GifKeyboardPageViewModel gifKeyboardPageViewModel = this.viewModel;
        GiphyMp4ViewModel giphyMp4ViewModel = null;
        if (gifKeyboardPageViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            gifKeyboardPageViewModel = null;
        }
        if (gifKeyboardPageViewModel.getSelectedTab() != gifQuickSearchOption) {
            GifKeyboardPageViewModel gifKeyboardPageViewModel2 = this.viewModel;
            if (gifKeyboardPageViewModel2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                gifKeyboardPageViewModel2 = null;
            }
            gifKeyboardPageViewModel2.setSelectedTab(gifQuickSearchOption);
            GiphyMp4ViewModel giphyMp4ViewModel2 = this.giphyMp4ViewModel;
            if (giphyMp4ViewModel2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("giphyMp4ViewModel");
            } else {
                giphyMp4ViewModel = giphyMp4ViewModel2;
            }
            giphyMp4ViewModel.updateSearchQuery(gifQuickSearchOption.getQuery());
            updateQuickSearchTabs();
        }
    }

    private final void updateQuickSearchTabs() {
        GifQuickSearchAdapter gifQuickSearchAdapter;
        List<GifQuickSearchOption> ranked = GifQuickSearchOption.Companion.getRanked();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(ranked, 10));
        Iterator<T> it = ranked.iterator();
        while (true) {
            gifQuickSearchAdapter = null;
            GifKeyboardPageViewModel gifKeyboardPageViewModel = null;
            if (!it.hasNext()) {
                break;
            }
            GifQuickSearchOption gifQuickSearchOption = (GifQuickSearchOption) it.next();
            GifKeyboardPageViewModel gifKeyboardPageViewModel2 = this.viewModel;
            if (gifKeyboardPageViewModel2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            } else {
                gifKeyboardPageViewModel = gifKeyboardPageViewModel2;
            }
            arrayList.add(new GifQuickSearch(gifQuickSearchOption, gifQuickSearchOption == gifKeyboardPageViewModel.getSelectedTab()));
        }
        GifQuickSearchAdapter gifQuickSearchAdapter2 = this.quickSearchAdapter;
        if (gifQuickSearchAdapter2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("quickSearchAdapter");
        } else {
            gifQuickSearchAdapter = gifQuickSearchAdapter2;
        }
        gifQuickSearchAdapter.submitList(arrayList, new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                GifKeyboardPageFragment.this.scrollToTab();
            }
        });
    }

    public final void scrollToTab() {
        RecyclerView recyclerView = this.quickSearchList;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("quickSearchList");
            recyclerView = null;
        }
        recyclerView.post(new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                GifKeyboardPageFragment.m1985scrollToTab$lambda2(GifKeyboardPageFragment.this);
            }
        });
    }

    /* renamed from: scrollToTab$lambda-2 */
    public static final void m1985scrollToTab$lambda2(GifKeyboardPageFragment gifKeyboardPageFragment) {
        Intrinsics.checkNotNullParameter(gifKeyboardPageFragment, "this$0");
        RecyclerView recyclerView = gifKeyboardPageFragment.quickSearchList;
        GifKeyboardPageViewModel gifKeyboardPageViewModel = null;
        if (recyclerView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("quickSearchList");
            recyclerView = null;
        }
        List<GifQuickSearchOption> ranked = GifQuickSearchOption.Companion.getRanked();
        GifKeyboardPageViewModel gifKeyboardPageViewModel2 = gifKeyboardPageFragment.viewModel;
        if (gifKeyboardPageViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            gifKeyboardPageViewModel = gifKeyboardPageViewModel2;
        }
        recyclerView.smoothScrollToPosition(ranked.indexOf(gifKeyboardPageViewModel.getSelectedTab()));
    }

    public final void handleGiphyMp4SaveResult(GiphyMp4SaveResult giphyMp4SaveResult) {
        if (giphyMp4SaveResult instanceof GiphyMp4SaveResult.Success) {
            hideProgressDialog();
            handleGiphyMp4SuccessfulResult((GiphyMp4SaveResult.Success) giphyMp4SaveResult);
        } else if (giphyMp4SaveResult instanceof GiphyMp4SaveResult.Error) {
            hideProgressDialog();
            handleGiphyMp4ErrorResult();
        } else {
            this.progressDialog = SimpleProgressDialog.show(requireContext());
        }
    }

    private final void hideProgressDialog() {
        AlertDialog alertDialog = this.progressDialog;
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
    }

    private final void handleGiphyMp4SuccessfulResult(GiphyMp4SaveResult.Success success) {
        Host host = this.host;
        if (host == null) {
            Intrinsics.throwUninitializedPropertyAccessException("host");
            host = null;
        }
        Uri blobUri = success.getBlobUri();
        Intrinsics.checkNotNullExpressionValue(blobUri, "success.blobUri");
        host.onGifSelectSuccess(blobUri, success.getWidth(), success.getHeight());
    }

    private final void handleGiphyMp4ErrorResult() {
        Toast.makeText(requireContext(), (int) R.string.GiphyActivity_error_while_retrieving_full_resolution_gif, 1).show();
    }

    public final void openGifSearch() {
        Host host = this.host;
        if (host == null) {
            Intrinsics.throwUninitializedPropertyAccessException("host");
            host = null;
        }
        host.openGifSearch();
    }
}
