package org.thoughtcrime.securesms.keyboard.sticker;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: StickerSearchViewModel.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000e\u001a\u00020\u0007R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchViewModel;", "Landroidx/lifecycle/ViewModel;", "searchRepository", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchRepository;", "(Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchRepository;)V", "searchQuery", "Landroidx/lifecycle/MutableLiveData;", "", "searchResults", "Landroidx/lifecycle/LiveData;", "", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$Sticker;", "getSearchResults", "()Landroidx/lifecycle/LiveData;", "query", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerSearchViewModel extends ViewModel {
    private final MutableLiveData<String> searchQuery;
    private final StickerSearchRepository searchRepository;
    private final LiveData<List<KeyboardStickerListAdapter.Sticker>> searchResults;

    public StickerSearchViewModel(StickerSearchRepository stickerSearchRepository) {
        Intrinsics.checkNotNullParameter(stickerSearchRepository, "searchRepository");
        this.searchRepository = stickerSearchRepository;
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>("");
        this.searchQuery = mutableLiveData;
        LiveData<List<KeyboardStickerListAdapter.Sticker>> mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerSearchViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StickerSearchViewModel.m2008searchResults$lambda1(StickerSearchViewModel.this, (String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapAsync, "mapAsync(searchQuery) { …cker(it.packId, it) }\n  }");
        this.searchResults = mapAsync;
    }

    public final LiveData<List<KeyboardStickerListAdapter.Sticker>> getSearchResults() {
        return this.searchResults;
    }

    /* renamed from: searchResults$lambda-1 */
    public static final List m2008searchResults$lambda1(StickerSearchViewModel stickerSearchViewModel, String str) {
        Intrinsics.checkNotNullParameter(stickerSearchViewModel, "this$0");
        StickerSearchRepository stickerSearchRepository = stickerSearchViewModel.searchRepository;
        Intrinsics.checkNotNullExpressionValue(str, "q");
        List<StickerRecord> search = stickerSearchRepository.search(str);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(search, 10));
        for (StickerRecord stickerRecord : search) {
            String packId = stickerRecord.getPackId();
            Intrinsics.checkNotNullExpressionValue(packId, "it.packId");
            arrayList.add(new KeyboardStickerListAdapter.Sticker(packId, stickerRecord));
        }
        return arrayList;
    }

    public final void query(String str) {
        Intrinsics.checkNotNullParameter(str, "query");
        this.searchQuery.postValue(str);
    }

    /* compiled from: StickerSearchViewModel.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "repository", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchRepository;", "getRepository", "()Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchRepository;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StickerSearchRepository repository = new StickerSearchRepository();

        public Factory(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
        }

        public final StickerSearchRepository getRepository() {
            return this.repository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StickerSearchViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
