package org.thoughtcrime.securesms.keyboard.sticker;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Consumer;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerPackListAdapter;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardRepository;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: StickerKeyboardPageViewModel.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0018B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000f0\nJ\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u000fR\u001a\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\t\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u00070\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0006X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\n¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\r¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository;", "(Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository;)V", "keyboardStickerPacks", "Landroidx/lifecycle/MutableLiveData;", "", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;", "packs", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter$StickerPack;", "getPacks", "()Landroidx/lifecycle/LiveData;", "selectedPack", "", StickerDatabase.DIRECTORY, "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "getStickers", "getSelectedPack", "refreshStickers", "", "selectPack", "packId", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerKeyboardPageViewModel extends ViewModel {
    private final MutableLiveData<List<StickerKeyboardRepository.KeyboardStickerPack>> keyboardStickerPacks;
    private final LiveData<List<KeyboardStickerPackListAdapter.StickerPack>> packs;
    private final StickerKeyboardRepository repository;
    private final MutableLiveData<String> selectedPack;
    private final LiveData<MappingModelList> stickers;

    public StickerKeyboardPageViewModel(StickerKeyboardRepository stickerKeyboardRepository) {
        Intrinsics.checkNotNullParameter(stickerKeyboardRepository, "repository");
        this.repository = stickerKeyboardRepository;
        MutableLiveData<List<StickerKeyboardRepository.KeyboardStickerPack>> mutableLiveData = new MutableLiveData<>();
        this.keyboardStickerPacks = mutableLiveData;
        MutableLiveData<String> mutableLiveData2 = new MutableLiveData<>("no_selected_page");
        this.selectedPack = mutableLiveData2;
        LiveData mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StickerKeyboardPageViewModel.m2002_init_$lambda1((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapAsync, "mapAsync(keyboardSticker…{ StickerPack(it) }\n    }");
        LiveData<List<KeyboardStickerPackListAdapter.StickerPack>> combineLatest = LiveDataUtil.combineLatest(mutableLiveData2, mapAsync, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return StickerKeyboardPageViewModel.m2003_init_$lambda3((String) obj, (List) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(selectedPa…Selected) }\n      }\n    }");
        this.packs = combineLatest;
        LiveData<MappingModelList> mapAsync2 = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageViewModel$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StickerKeyboardPageViewModel.m2004_init_$lambda6((List) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapAsync2, "mapAsync(keyboardSticker…      }\n\n      list\n    }");
        this.stickers = mapAsync2;
    }

    public final LiveData<List<KeyboardStickerPackListAdapter.StickerPack>> getPacks() {
        return this.packs;
    }

    public final LiveData<MappingModelList> getStickers() {
        return this.stickers;
    }

    /* renamed from: _init_$lambda-1 */
    public static final List m2002_init_$lambda1(List list) {
        Intrinsics.checkNotNullExpressionValue(list, "packs");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(new KeyboardStickerPackListAdapter.StickerPack((StickerKeyboardRepository.KeyboardStickerPack) it.next(), false, 2, null));
        }
        return arrayList;
    }

    /* renamed from: _init_$lambda-3 */
    public static final List m2003_init_$lambda3(String str, List list) {
        Intrinsics.checkNotNullParameter(str, "selected");
        Intrinsics.checkNotNullParameter(list, "packs");
        if (list.isEmpty()) {
            return list;
        }
        if (Intrinsics.areEqual(str, "no_selected_page")) {
            str = ((KeyboardStickerPackListAdapter.StickerPack) list.get(0)).getPackRecord().getPackId();
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            KeyboardStickerPackListAdapter.StickerPack stickerPack = (KeyboardStickerPackListAdapter.StickerPack) it.next();
            arrayList.add(KeyboardStickerPackListAdapter.StickerPack.copy$default(stickerPack, null, Intrinsics.areEqual(stickerPack.getPackRecord().getPackId(), str), 1, null));
        }
        return arrayList;
    }

    /* renamed from: _init_$lambda-6 */
    public static final MappingModelList m2004_init_$lambda6(List list) {
        MappingModelList mappingModelList = new MappingModelList();
        Intrinsics.checkNotNullExpressionValue(list, "packs");
        Iterator it = list.iterator();
        while (it.hasNext()) {
            StickerKeyboardRepository.KeyboardStickerPack keyboardStickerPack = (StickerKeyboardRepository.KeyboardStickerPack) it.next();
            mappingModelList.add(new KeyboardStickerListAdapter.StickerHeader(keyboardStickerPack.getPackId(), keyboardStickerPack.getTitle(), keyboardStickerPack.getTitleResource()));
            List<StickerRecord> stickers = keyboardStickerPack.getStickers();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(stickers, 10));
            for (StickerRecord stickerRecord : stickers) {
                arrayList.add(new KeyboardStickerListAdapter.Sticker(keyboardStickerPack.getPackId(), stickerRecord));
            }
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(mappingModelList, arrayList);
        }
        return mappingModelList;
    }

    public final LiveData<String> getSelectedPack() {
        return this.selectedPack;
    }

    public final void selectPack(String str) {
        Intrinsics.checkNotNullParameter(str, "packId");
        this.selectedPack.setValue(str);
    }

    /* renamed from: refreshStickers$lambda-7 */
    public static final void m2005refreshStickers$lambda7(StickerKeyboardPageViewModel stickerKeyboardPageViewModel, List list) {
        Intrinsics.checkNotNullParameter(stickerKeyboardPageViewModel, "this$0");
        Intrinsics.checkNotNullParameter(list, "it");
        stickerKeyboardPageViewModel.keyboardStickerPacks.postValue(list);
    }

    public final void refreshStickers() {
        this.repository.getStickerPacks(new Consumer() { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageViewModel$$ExternalSyntheticLambda3
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                StickerKeyboardPageViewModel.m2005refreshStickers$lambda7(StickerKeyboardPageViewModel.this, (List) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    /* compiled from: StickerKeyboardPageViewModel.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardPageViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "()V", "repository", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final StickerKeyboardRepository repository = new StickerKeyboardRepository(SignalDatabase.Companion.stickers());

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new StickerKeyboardPageViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
