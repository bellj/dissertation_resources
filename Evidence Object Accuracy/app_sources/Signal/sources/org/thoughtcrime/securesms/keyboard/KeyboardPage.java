package org.thoughtcrime.securesms.keyboard;

import kotlin.Metadata;

/* compiled from: KeyboardPage.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/KeyboardPage;", "", "(Ljava/lang/String;I)V", "EMOJI", "STICKER", "GIF", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum KeyboardPage {
    EMOJI,
    STICKER,
    GIF
}
