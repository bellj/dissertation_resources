package org.thoughtcrime.securesms.keyboard.emoji;

import android.animation.Animator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.ImageViewCompat;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.animation.ResizeAnimation;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: KeyboardPageSearchView.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\f\u0018\u00002\u00020\u0001:\u0002&'B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0017\u001a\u00020\u0018H\u0016J\u0006\u0010\u0019\u001a\u00020\u0018J\u0010\u0010\u001a\u001a\u00020\u00182\b\b\u0002\u0010\u001b\u001a\u00020\u001cJ\u0006\u0010\u001d\u001a\u00020\u0018J(\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u0007H\u0014J\u0006\u0010#\u001a\u00020\u0018J\u0006\u0010$\u001a\u00020\u0018J\u0006\u0010%\u001a\u00020\u001cR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "callbacks", "Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$Callbacks;", "getCallbacks", "()Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$Callbacks;", "setCallbacks", "(Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$Callbacks;)V", "clearButton", "Landroidx/appcompat/widget/AppCompatImageView;", "input", "Landroid/widget/EditText;", "navButton", "state", "Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$State;", "targetInputWidth", "clearFocus", "", "clearQuery", "enableBackNavigation", "enable", "", "hide", "onSizeChanged", "w", "h", "oldw", "oldh", "presentForEmojiSearch", "show", "showRequested", "Callbacks", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class KeyboardPageSearchView extends ConstraintLayout {
    private Callbacks callbacks;
    private final AppCompatImageView clearButton;
    private final EditText input;
    private final AppCompatImageView navButton;
    private State state;
    private int targetInputWidth;

    /* compiled from: KeyboardPageSearchView.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\b\u0010\u0004\u001a\u00020\u0003H\u0016J\b\u0010\u0005\u001a\u00020\u0003H\u0016J\b\u0010\u0006\u001a\u00020\u0003H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$Callbacks;", "", "onClicked", "", "onFocusGained", "onFocusLost", "onNavigationClicked", "onQueryChanged", "query", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callbacks {

        /* compiled from: KeyboardPageSearchView.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DefaultImpls {
            public static void onClicked(Callbacks callbacks) {
            }

            public static void onFocusGained(Callbacks callbacks) {
            }

            public static void onFocusLost(Callbacks callbacks) {
            }

            public static void onNavigationClicked(Callbacks callbacks) {
            }

            public static void onQueryChanged(Callbacks callbacks, String str) {
                Intrinsics.checkNotNullParameter(str, "query");
            }
        }

        void onClicked();

        void onFocusGained();

        void onFocusLost();

        void onNavigationClicked();

        void onQueryChanged(String str);
    }

    /* compiled from: KeyboardPageSearchView.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$State;", "", "(Ljava/lang/String;I)V", "SHOW_REQUESTED", "HIDE_REQUESTED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum State {
        SHOW_REQUESTED,
        HIDE_REQUESTED
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public KeyboardPageSearchView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public KeyboardPageSearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ KeyboardPageSearchView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public KeyboardPageSearchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        State state = State.HIDE_REQUESTED;
        this.state = state;
        this.targetInputWidth = -1;
        ViewGroup.inflate(context, R.layout.keyboard_pager_search_bar, this);
        View findViewById = findViewById(R.id.emoji_search_nav_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.emoji_search_nav_icon)");
        AppCompatImageView appCompatImageView = (AppCompatImageView) findViewById;
        this.navButton = appCompatImageView;
        View findViewById2 = findViewById(R.id.emoji_search_clear_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.emoji_search_clear_icon)");
        AppCompatImageView appCompatImageView2 = (AppCompatImageView) findViewById2;
        this.clearButton = appCompatImageView2;
        View findViewById3 = findViewById(R.id.emoji_search_entry);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.emoji_search_entry)");
        EditText editText = (EditText) findViewById3;
        this.input = editText;
        editText.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$special$$inlined$doAfterTextChanged$1
            final /* synthetic */ KeyboardPageSearchView this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                boolean z = false;
                if (editable == null || editable.length() == 0) {
                    this.this$0.clearButton.setImageDrawable(null);
                    this.this$0.clearButton.setClickable(false);
                } else {
                    this.this$0.clearButton.setImageResource(R.drawable.ic_x);
                    this.this$0.clearButton.setClickable(true);
                }
                if (editable == null || editable.length() == 0) {
                    z = true;
                }
                if (z) {
                    KeyboardPageSearchView.Callbacks callbacks = this.this$0.getCallbacks();
                    if (callbacks != null) {
                        callbacks.onQueryChanged("");
                        return;
                    }
                    return;
                }
                KeyboardPageSearchView.Callbacks callbacks2 = this.this$0.getCallbacks();
                if (callbacks2 != null) {
                    callbacks2.onQueryChanged(editable.toString());
                }
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$$ExternalSyntheticLambda1
            @Override // android.view.View.OnFocusChangeListener
            public final void onFocusChange(View view, boolean z) {
                KeyboardPageSearchView.m1969_init_$lambda1(KeyboardPageSearchView.this, view, z);
            }
        });
        appCompatImageView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                KeyboardPageSearchView.m1970_init_$lambda2(KeyboardPageSearchView.this, view);
            }
        });
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.KeyboardPageSearchView, 0, 0);
        Intrinsics.checkNotNullExpressionValue(obtainStyledAttributes, "context.obtainStyledAttr…oardPageSearchView, 0, 0)");
        if (obtainStyledAttributes.getBoolean(4, false)) {
            setAlpha(1.0f);
            this.state = State.SHOW_REQUESTED;
        } else {
            setAlpha(0.0f);
            ViewGroup.LayoutParams layoutParams = editText.getLayoutParams();
            layoutParams.width = 1;
            editText.setLayoutParams(layoutParams);
            this.state = state;
        }
        String string = obtainStyledAttributes.getString(2);
        editText.setHint(string == null ? "" : string);
        int color = obtainStyledAttributes.getColor(1, ContextCompat.getColor(context, R.color.signal_background_primary));
        ColorStateList valueOf = ColorStateList.valueOf(color);
        Intrinsics.checkNotNullExpressionValue(valueOf, "valueOf(backgroundTint)");
        editText.setBackground(new ColorDrawable(color));
        ViewCompat.setBackgroundTintList(findViewById(R.id.emoji_search_nav), valueOf);
        ViewCompat.setBackgroundTintList(findViewById(R.id.emoji_search_clear), valueOf);
        ColorStateList colorStateList = obtainStyledAttributes.getColorStateList(3);
        colorStateList = colorStateList == null ? ContextCompat.getColorStateList(context, R.color.signal_icon_tint_primary) : colorStateList;
        ImageViewCompat.setImageTintList(appCompatImageView, colorStateList);
        ImageViewCompat.setImageTintList(appCompatImageView2, colorStateList);
        editText.setHintTextColor(colorStateList);
        if (obtainStyledAttributes.getBoolean(0, false)) {
            View findViewById4 = findViewById(R.id.keyboard_search_click_only);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.keyboard_search_click_only)");
            ViewExtensionsKt.setVisible(findViewById4, true);
            findViewById4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$$ExternalSyntheticLambda3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    KeyboardPageSearchView.m1973lambda5$lambda4(KeyboardPageSearchView.this, view);
                }
            });
        }
        Unit unit = Unit.INSTANCE;
        obtainStyledAttributes.recycle();
    }

    public final Callbacks getCallbacks() {
        return this.callbacks;
    }

    public final void setCallbacks(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m1969_init_$lambda1(KeyboardPageSearchView keyboardPageSearchView, View view, boolean z) {
        Intrinsics.checkNotNullParameter(keyboardPageSearchView, "this$0");
        if (z) {
            Callbacks callbacks = keyboardPageSearchView.callbacks;
            if (callbacks != null) {
                callbacks.onFocusGained();
                return;
            }
            return;
        }
        Callbacks callbacks2 = keyboardPageSearchView.callbacks;
        if (callbacks2 != null) {
            callbacks2.onFocusLost();
        }
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m1970_init_$lambda2(KeyboardPageSearchView keyboardPageSearchView, View view) {
        Intrinsics.checkNotNullParameter(keyboardPageSearchView, "this$0");
        keyboardPageSearchView.clearQuery();
    }

    /* renamed from: lambda-5$lambda-4 */
    public static final void m1973lambda5$lambda4(KeyboardPageSearchView keyboardPageSearchView, View view) {
        Intrinsics.checkNotNullParameter(keyboardPageSearchView, "this$0");
        Callbacks callbacks = keyboardPageSearchView.callbacks;
        if (callbacks != null) {
            callbacks.onClicked();
        }
    }

    public final boolean showRequested() {
        return this.state == State.SHOW_REQUESTED;
    }

    public static /* synthetic */ void enableBackNavigation$default(KeyboardPageSearchView keyboardPageSearchView, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = true;
        }
        keyboardPageSearchView.enableBackNavigation(z);
    }

    public final void enableBackNavigation(boolean z) {
        this.navButton.setImageResource(z ? R.drawable.ic_arrow_left_24 : R.drawable.ic_search_24);
        if (z) {
            this.navButton.setImageResource(R.drawable.ic_arrow_left_24);
            this.navButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$$ExternalSyntheticLambda5
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    KeyboardPageSearchView.m1971enableBackNavigation$lambda6(KeyboardPageSearchView.this, view);
                }
            });
            return;
        }
        this.navButton.setImageResource(R.drawable.ic_search_24);
        this.navButton.setOnClickListener(null);
    }

    /* renamed from: enableBackNavigation$lambda-6 */
    public static final void m1971enableBackNavigation$lambda6(KeyboardPageSearchView keyboardPageSearchView, View view) {
        Intrinsics.checkNotNullParameter(keyboardPageSearchView, "this$0");
        Callbacks callbacks = keyboardPageSearchView.callbacks;
        if (callbacks != null) {
            callbacks.onNavigationClicked();
        }
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        this.targetInputWidth = (i - ViewUtil.dpToPx(32)) - ViewUtil.dpToPx(90);
    }

    public final void show() {
        State state = this.state;
        State state2 = State.SHOW_REQUESTED;
        if (state != state2) {
            setVisibility(0);
            this.state = state2;
            post(new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    KeyboardPageSearchView.m1974show$lambda7(KeyboardPageSearchView.this);
                }
            });
        }
    }

    /* renamed from: show$lambda-7 */
    public static final void m1974show$lambda7(KeyboardPageSearchView keyboardPageSearchView) {
        Intrinsics.checkNotNullParameter(keyboardPageSearchView, "this$0");
        keyboardPageSearchView.animate().setDuration(250).alpha(1.0f).setListener(null);
        EditText editText = keyboardPageSearchView.input;
        ResizeAnimation resizeAnimation = new ResizeAnimation(editText, keyboardPageSearchView.targetInputWidth, editText.getMeasuredHeight());
        resizeAnimation.setDuration(250);
        keyboardPageSearchView.input.startAnimation(resizeAnimation);
    }

    public final void hide() {
        State state = this.state;
        State state2 = State.HIDE_REQUESTED;
        if (state != state2) {
            this.state = state2;
            post(new Runnable() { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    KeyboardPageSearchView.m1972hide$lambda8(KeyboardPageSearchView.this);
                }
            });
        }
    }

    /* renamed from: hide$lambda-8 */
    public static final void m1972hide$lambda8(KeyboardPageSearchView keyboardPageSearchView) {
        Intrinsics.checkNotNullParameter(keyboardPageSearchView, "this$0");
        keyboardPageSearchView.animate().setDuration(250).alpha(0.0f).setListener(new AnimationCompleteListener(keyboardPageSearchView) { // from class: org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView$hide$1$1
            final /* synthetic */ KeyboardPageSearchView this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                this.this$0.setVisibility(4);
            }
        });
        EditText editText = keyboardPageSearchView.input;
        ResizeAnimation resizeAnimation = new ResizeAnimation(editText, 1, editText.getMeasuredHeight());
        resizeAnimation.setDuration(250);
        keyboardPageSearchView.input.startAnimation(resizeAnimation);
    }

    public final void presentForEmojiSearch() {
        ViewUtil.focusAndShowKeyboard(this.input);
        enableBackNavigation$default(this, false, 1, null);
    }

    @Override // android.view.ViewGroup, android.view.View
    public void clearFocus() {
        super.clearFocus();
        clearChildFocus(this.input);
    }

    public final void clearQuery() {
        this.input.getText().clear();
    }
}
