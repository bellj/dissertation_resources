package org.thoughtcrime.securesms.keyboard.gif;

import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: GifQuickSearchOption.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\b\u0001\u0018\u0000 \u00142\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0014B\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000j\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "", "rank", "", DraftDatabase.Draft.IMAGE, "query", "", "(Ljava/lang/String;IIILjava/lang/String;)V", "getImage", "()I", "getQuery", "()Ljava/lang/String;", "TRENDING", "CELEBRATE", "LOVE", "THUMBS_UP", "SURPRISED", "EXCITED", "SAD", "ANGRY", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum GifQuickSearchOption {
    TRENDING(0, R.drawable.ic_gif_trending_24, ""),
    CELEBRATE(1, R.drawable.ic_gif_celebrate_24, "celebrate"),
    LOVE(2, R.drawable.ic_gif_love_24, "love"),
    THUMBS_UP(3, R.drawable.ic_gif_thumbsup_24, "thumbs up"),
    SURPRISED(4, R.drawable.ic_gif_surprised_24, "surprised"),
    EXCITED(5, R.drawable.ic_gif_excited_24, "excited"),
    SAD(6, R.drawable.ic_gif_sad_24, "sad"),
    ANGRY(7, R.drawable.ic_gif_angry_24, "angry");
    
    public static final Companion Companion = new Companion(null);
    private static final Lazy<List<GifQuickSearchOption>> ranked$delegate = LazyKt__LazyJVMKt.lazy(GifQuickSearchOption$Companion$ranked$2.INSTANCE);
    private final int image;
    private final String query;
    private final int rank;

    GifQuickSearchOption(int i, int i2, String str) {
        this.rank = i;
        this.image = i2;
        this.query = str;
    }

    public final int getImage() {
        return this.image;
    }

    public final String getQuery() {
        return this.query;
    }

    /* compiled from: GifQuickSearchOption.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R!\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048FX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption$Companion;", "", "()V", "ranked", "", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "getRanked", "()Ljava/util/List;", "ranked$delegate", "Lkotlin/Lazy;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final List<GifQuickSearchOption> getRanked() {
            return (List) GifQuickSearchOption.ranked$delegate.getValue();
        }
    }
}
