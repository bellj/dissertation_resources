package org.thoughtcrime.securesms.keyboard.sticker;

import android.view.View;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class KeyboardStickerListAdapter$StickerViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ KeyboardStickerListAdapter f$0;
    public final /* synthetic */ KeyboardStickerListAdapter.Sticker f$1;

    public /* synthetic */ KeyboardStickerListAdapter$StickerViewHolder$$ExternalSyntheticLambda1(KeyboardStickerListAdapter keyboardStickerListAdapter, KeyboardStickerListAdapter.Sticker sticker) {
        this.f$0 = keyboardStickerListAdapter;
        this.f$1 = sticker;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return KeyboardStickerListAdapter.StickerViewHolder.m1990bind$lambda1(this.f$0, this.f$1, view);
    }
}
