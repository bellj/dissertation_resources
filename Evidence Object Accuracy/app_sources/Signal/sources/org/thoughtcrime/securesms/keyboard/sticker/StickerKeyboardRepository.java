package org.thoughtcrime.securesms.keyboard.sticker;

import android.net.Uri;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerPackRecord;
import org.thoughtcrime.securesms.database.model.StickerRecord;

/* compiled from: StickerKeyboardRepository.kt */
@Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u000eB\u000f\u0012\u0006\u0010\n\u001a\u00020\t¢\u0006\u0004\b\f\u0010\rJ\b\u0010\u0003\u001a\u00020\u0002H\u0002J\u001a\u0010\b\u001a\u00020\u00072\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00020\u00050\u0004R\u0014\u0010\n\u001a\u00020\t8\u0002X\u0004¢\u0006\u0006\n\u0004\b\n\u0010\u000b¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository;", "", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;", "getRecentStickerPack", "j$/util/function/Consumer", "", "consumer", "", "getStickerPacks", "Lorg/thoughtcrime/securesms/database/StickerDatabase;", "stickerDatabase", "Lorg/thoughtcrime/securesms/database/StickerDatabase;", "<init>", "(Lorg/thoughtcrime/securesms/database/StickerDatabase;)V", "KeyboardStickerPack", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class StickerKeyboardRepository {
    private final StickerDatabase stickerDatabase;

    public StickerKeyboardRepository(StickerDatabase stickerDatabase) {
        Intrinsics.checkNotNullParameter(stickerDatabase, "stickerDatabase");
        this.stickerDatabase = stickerDatabase;
    }

    public final void getStickerPacks(Consumer<List<KeyboardStickerPack>> consumer) {
        Intrinsics.checkNotNullParameter(consumer, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                StickerKeyboardRepository.m2006getStickerPacks$lambda3(StickerKeyboardRepository.this, this.f$1);
            }
        });
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: getStickerPacks$lambda-3 */
    public static final void m2006getStickerPacks$lambda3(StickerKeyboardRepository stickerKeyboardRepository, Consumer consumer) {
        Intrinsics.checkNotNullParameter(stickerKeyboardRepository, "this$0");
        Intrinsics.checkNotNullParameter(consumer, "$consumer");
        ArrayList<KeyboardStickerPack> arrayList = new ArrayList();
        StickerDatabase.StickerRecordReader stickerPackRecordReader = new StickerDatabase.StickerPackRecordReader(stickerKeyboardRepository.stickerDatabase.getInstalledStickerPacks());
        try {
            for (StickerPackRecord next = stickerPackRecordReader.getNext(); next != null; next = stickerPackRecordReader.getNext()) {
                String packId = next.getPackId();
                Intrinsics.checkNotNullExpressionValue(packId, "pack.packId");
                arrayList.add(new KeyboardStickerPack(packId, next.getTitle().orElse(null), null, next.getCover().getUri(), null, null, 52, null));
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(stickerPackRecordReader, null);
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (KeyboardStickerPack keyboardStickerPack : arrayList) {
                ArrayList arrayList3 = new ArrayList();
                stickerPackRecordReader = new StickerDatabase.StickerRecordReader(stickerKeyboardRepository.stickerDatabase.getStickersForPack(keyboardStickerPack.getPackId()));
                try {
                    for (StickerRecord next2 = stickerPackRecordReader.getNext(); next2 != null; next2 = stickerPackRecordReader.getNext()) {
                        arrayList3.add(next2);
                    }
                    Unit unit2 = Unit.INSTANCE;
                    CloseableKt.closeFinally(stickerPackRecordReader, null);
                    arrayList2.add(KeyboardStickerPack.copy$default(keyboardStickerPack, null, null, null, null, null, arrayList3, 31, null));
                } finally {
                    try {
                        throw th;
                    } catch (Throwable th) {
                    }
                }
            }
            List list = CollectionsKt___CollectionsKt.toMutableList((Collection) arrayList2);
            KeyboardStickerPack recentStickerPack = stickerKeyboardRepository.getRecentStickerPack();
            if (!recentStickerPack.getStickers().isEmpty()) {
                list.add(0, recentStickerPack);
            }
            consumer.accept(list);
        } finally {
            try {
                throw th;
            } catch (Throwable th) {
                CloseableKt.closeFinally(stickerPackRecordReader, th);
            }
        }
    }

    /* JADX INFO: finally extract failed */
    private final KeyboardStickerPack getRecentStickerPack() {
        ArrayList arrayList = new ArrayList();
        StickerDatabase.StickerRecordReader stickerRecordReader = new StickerDatabase.StickerRecordReader(this.stickerDatabase.getRecentlyUsedStickers(24));
        try {
            for (StickerRecord next = stickerRecordReader.getNext(); next != null; next = stickerRecordReader.getNext()) {
                arrayList.add(next);
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(stickerRecordReader, null);
            return new KeyboardStickerPack("RECENT", null, Integer.valueOf((int) R.string.StickerKeyboard__recently_used), null, Integer.valueOf((int) R.drawable.ic_recent_20), arrayList);
        } finally {
            try {
                throw th;
            } catch (Throwable th) {
            }
        }
    }

    /* compiled from: StickerKeyboardRepository.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001BI\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0002\u0010\rJ\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0002\u0010\u000fJ\u000b\u0010\u001c\u001a\u0004\u0018\u00010\bHÆ\u0003J\u0010\u0010\u001d\u001a\u0004\u0018\u00010\u0006HÆ\u0003¢\u0006\u0002\u0010\u000fJ\u000f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0003JX\u0010\u001f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00062\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bHÆ\u0001¢\u0006\u0002\u0010 J\u0013\u0010!\u001a\u00020\"2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020\u0006HÖ\u0001J\t\u0010%\u001a\u00020\u0003HÖ\u0001R\u0015\u0010\t\u001a\u0004\u0018\u00010\u0006¢\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\n\n\u0002\u0010\u0010\u001a\u0004\b\u0018\u0010\u000f¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;", "", "packId", "", MultiselectForwardFragment.DIALOG_TITLE, "titleResource", "", "coverUri", "Landroid/net/Uri;", "coverResource", StickerDatabase.DIRECTORY, "", "Lorg/thoughtcrime/securesms/database/model/StickerRecord;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/net/Uri;Ljava/lang/Integer;Ljava/util/List;)V", "getCoverResource", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getCoverUri", "()Landroid/net/Uri;", "getPackId", "()Ljava/lang/String;", "getStickers", "()Ljava/util/List;", "getTitle", "getTitleResource", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Landroid/net/Uri;Ljava/lang/Integer;Ljava/util/List;)Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;", "equals", "", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class KeyboardStickerPack {
        private final Integer coverResource;
        private final Uri coverUri;
        private final String packId;
        private final List<StickerRecord> stickers;
        private final String title;
        private final Integer titleResource;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardRepository$KeyboardStickerPack */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ KeyboardStickerPack copy$default(KeyboardStickerPack keyboardStickerPack, String str, String str2, Integer num, Uri uri, Integer num2, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                str = keyboardStickerPack.packId;
            }
            if ((i & 2) != 0) {
                str2 = keyboardStickerPack.title;
            }
            if ((i & 4) != 0) {
                num = keyboardStickerPack.titleResource;
            }
            if ((i & 8) != 0) {
                uri = keyboardStickerPack.coverUri;
            }
            if ((i & 16) != 0) {
                num2 = keyboardStickerPack.coverResource;
            }
            if ((i & 32) != 0) {
                list = keyboardStickerPack.stickers;
            }
            return keyboardStickerPack.copy(str, str2, num, uri, num2, list);
        }

        public final String component1() {
            return this.packId;
        }

        public final String component2() {
            return this.title;
        }

        public final Integer component3() {
            return this.titleResource;
        }

        public final Uri component4() {
            return this.coverUri;
        }

        public final Integer component5() {
            return this.coverResource;
        }

        public final List<StickerRecord> component6() {
            return this.stickers;
        }

        public final KeyboardStickerPack copy(String str, String str2, Integer num, Uri uri, Integer num2, List<StickerRecord> list) {
            Intrinsics.checkNotNullParameter(str, "packId");
            Intrinsics.checkNotNullParameter(list, StickerDatabase.DIRECTORY);
            return new KeyboardStickerPack(str, str2, num, uri, num2, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof KeyboardStickerPack)) {
                return false;
            }
            KeyboardStickerPack keyboardStickerPack = (KeyboardStickerPack) obj;
            return Intrinsics.areEqual(this.packId, keyboardStickerPack.packId) && Intrinsics.areEqual(this.title, keyboardStickerPack.title) && Intrinsics.areEqual(this.titleResource, keyboardStickerPack.titleResource) && Intrinsics.areEqual(this.coverUri, keyboardStickerPack.coverUri) && Intrinsics.areEqual(this.coverResource, keyboardStickerPack.coverResource) && Intrinsics.areEqual(this.stickers, keyboardStickerPack.stickers);
        }

        public int hashCode() {
            int hashCode = this.packId.hashCode() * 31;
            String str = this.title;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            Integer num = this.titleResource;
            int hashCode3 = (hashCode2 + (num == null ? 0 : num.hashCode())) * 31;
            Uri uri = this.coverUri;
            int hashCode4 = (hashCode3 + (uri == null ? 0 : uri.hashCode())) * 31;
            Integer num2 = this.coverResource;
            if (num2 != null) {
                i = num2.hashCode();
            }
            return ((hashCode4 + i) * 31) + this.stickers.hashCode();
        }

        public String toString() {
            return "KeyboardStickerPack(packId=" + this.packId + ", title=" + this.title + ", titleResource=" + this.titleResource + ", coverUri=" + this.coverUri + ", coverResource=" + this.coverResource + ", stickers=" + this.stickers + ')';
        }

        public KeyboardStickerPack(String str, String str2, Integer num, Uri uri, Integer num2, List<StickerRecord> list) {
            Intrinsics.checkNotNullParameter(str, "packId");
            Intrinsics.checkNotNullParameter(list, StickerDatabase.DIRECTORY);
            this.packId = str;
            this.title = str2;
            this.titleResource = num;
            this.coverUri = uri;
            this.coverResource = num2;
            this.stickers = list;
        }

        public final String getPackId() {
            return this.packId;
        }

        public final String getTitle() {
            return this.title;
        }

        public final Integer getTitleResource() {
            return this.titleResource;
        }

        public final Uri getCoverUri() {
            return this.coverUri;
        }

        public final Integer getCoverResource() {
            return this.coverResource;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ KeyboardStickerPack(java.lang.String r10, java.lang.String r11, java.lang.Integer r12, android.net.Uri r13, java.lang.Integer r14, java.util.List r15, int r16, kotlin.jvm.internal.DefaultConstructorMarker r17) {
            /*
                r9 = this;
                r0 = r16 & 4
                r1 = 0
                if (r0 == 0) goto L_0x0007
                r5 = r1
                goto L_0x0008
            L_0x0007:
                r5 = r12
            L_0x0008:
                r0 = r16 & 16
                if (r0 == 0) goto L_0x000e
                r7 = r1
                goto L_0x000f
            L_0x000e:
                r7 = r14
            L_0x000f:
                r0 = r16 & 32
                if (r0 == 0) goto L_0x0019
                java.util.List r0 = kotlin.collections.CollectionsKt.emptyList()
                r8 = r0
                goto L_0x001a
            L_0x0019:
                r8 = r15
            L_0x001a:
                r2 = r9
                r3 = r10
                r4 = r11
                r6 = r13
                r2.<init>(r3, r4, r5, r6, r7, r8)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardRepository.KeyboardStickerPack.<init>(java.lang.String, java.lang.String, java.lang.Integer, android.net.Uri, java.lang.Integer, java.util.List, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final List<StickerRecord> getStickers() {
            return this.stickers;
        }
    }
}
