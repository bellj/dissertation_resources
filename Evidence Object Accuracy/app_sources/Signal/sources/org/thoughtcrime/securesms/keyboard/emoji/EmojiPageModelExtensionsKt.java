package org.thoughtcrime.securesms.keyboard.emoji;

import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.Emoji;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiTree;
import org.thoughtcrime.securesms.emoji.EmojiCategory;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: EmojiPageModelExtensions.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u0014\u0010\u0000\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001*\u00020\u0003¨\u0006\u0004"}, d2 = {"toMappingModels", "", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiPageModelExtensionsKt {
    public static final List<MappingModel<?>> toMappingModels(EmojiPageModel emojiPageModel) {
        Object obj;
        Intrinsics.checkNotNullParameter(emojiPageModel, "<this>");
        EmojiTree emojiTree = EmojiSource.Companion.getLatest().getEmojiTree();
        List<Emoji> displayEmoji = emojiPageModel.getDisplayEmoji();
        Intrinsics.checkNotNullExpressionValue(displayEmoji, "displayEmoji");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(displayEmoji, 10));
        for (Emoji emoji : displayEmoji) {
            boolean z = false;
            if (Intrinsics.areEqual(EmojiCategory.EMOTICONS.getKey(), emojiPageModel.getKey()) || (Intrinsics.areEqual(RecentEmojiPageModel.KEY, emojiPageModel.getKey()) && emojiTree.getEmoji(emoji.getValue(), 0, emoji.getValue().length()) == null)) {
                z = true;
            }
            if (z) {
                obj = new EmojiPageViewGridAdapter.EmojiTextModel(emojiPageModel.getKey(), emoji);
            } else {
                obj = new EmojiPageViewGridAdapter.EmojiModel(emojiPageModel.getKey(), emoji);
            }
            arrayList.add(obj);
        }
        return arrayList;
    }
}
