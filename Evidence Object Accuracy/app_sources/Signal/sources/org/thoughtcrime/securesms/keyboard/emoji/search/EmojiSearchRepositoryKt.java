package org.thoughtcrime.securesms.keyboard.emoji.search;

import kotlin.Metadata;

/* compiled from: EmojiSearchRepository.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0003"}, d2 = {"EMOJI_SEARCH_LIMIT", "", "MINIMUM_QUERY_THRESHOLD", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiSearchRepositoryKt {
    private static final int EMOJI_SEARCH_LIMIT;
    private static final int MINIMUM_QUERY_THRESHOLD;
}
