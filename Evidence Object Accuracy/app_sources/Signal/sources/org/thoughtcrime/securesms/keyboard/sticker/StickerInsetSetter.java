package org.thoughtcrime.securesms.keyboard.sticker;

import android.graphics.Rect;
import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.InsetItemDecoration;

/* compiled from: StickerInsetSetter.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerInsetSetter;", "Lorg/thoughtcrime/securesms/util/InsetItemDecoration$SetInset;", "()V", "setInset", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerInsetSetter extends InsetItemDecoration.SetInset {
    @Override // org.thoughtcrime.securesms.util.InsetItemDecoration.SetInset
    public void setInset(Rect rect, View view, RecyclerView recyclerView) {
        int i;
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        boolean areEqual = Intrinsics.areEqual(view.getClass(), AppCompatTextView.class);
        rect.left = StickerInsetSetterKt.horizontalInset;
        rect.right = StickerInsetSetterKt.horizontalInset;
        rect.top = StickerInsetSetterKt.verticalInset;
        if (areEqual) {
            i = 0;
        } else {
            i = StickerInsetSetterKt.verticalInset;
        }
        rect.bottom = i;
    }
}
