package org.thoughtcrime.securesms.keyboard;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class BottomShadowBehavior extends CoordinatorLayout.Behavior<View> {
    private int bottomBarId;
    private boolean shown = true;

    public BottomShadowBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.BottomShadowBehavior);
            this.bottomBarId = obtainStyledAttributes.getResourceId(0, 0);
            obtainStyledAttributes.recycle();
        }
        if (this.bottomBarId == 0) {
            throw new IllegalStateException();
        }
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
        return view2.getId() == this.bottomBarId;
    }

    public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, View view, View view2) {
        view.setAlpha(((float) (view2.getHeight() - ((int) view2.getTranslationY()))) / ((float) view2.getHeight()));
        float y = view2.getY() - ((float) view.getHeight());
        if (y == view.getY()) {
            return false;
        }
        view.setY(y);
        return true;
    }
}
