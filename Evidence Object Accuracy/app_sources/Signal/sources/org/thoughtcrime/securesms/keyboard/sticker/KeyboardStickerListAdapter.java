package org.thoughtcrime.securesms.keyboard.sticker;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.glide.cache.ApngOptions;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: KeyboardStickerListAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0006\t\n\u000b\f\r\u000eB\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "glideRequests", "Lorg/thoughtcrime/securesms/mms/GlideRequests;", "eventListener", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$EventListener;", "allowApngAnimation", "", "(Lorg/thoughtcrime/securesms/mms/GlideRequests;Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$EventListener;Z)V", "EventListener", "HasPackId", "Sticker", "StickerHeader", "StickerHeaderViewHolder", "StickerViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class KeyboardStickerListAdapter extends MappingAdapter {
    private final boolean allowApngAnimation;
    private final EventListener eventListener;
    private final GlideRequests glideRequests;

    /* compiled from: KeyboardStickerListAdapter.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$EventListener;", "", "onStickerClicked", "", StickerDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$Sticker;", "onStickerLongClicked", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface EventListener {
        void onStickerClicked(Sticker sticker);

        void onStickerLongClicked(Sticker sticker);
    }

    /* compiled from: KeyboardStickerListAdapter.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$HasPackId;", "", "packId", "", "getPackId", "()Ljava/lang/String;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface HasPackId {
        String getPackId();
    }

    public KeyboardStickerListAdapter(GlideRequests glideRequests, EventListener eventListener, boolean z) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        this.glideRequests = glideRequests;
        this.eventListener = eventListener;
        this.allowApngAnimation = z;
        registerFactory(Sticker.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new KeyboardStickerListAdapter.StickerViewHolder(KeyboardStickerListAdapter.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.sticker_keyboard_page_list_item));
        registerFactory(StickerHeader.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new KeyboardStickerListAdapter.StickerHeaderViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.sticker_grid_header));
    }

    /* compiled from: KeyboardStickerListAdapter.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0000H\u0016J\u0010\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0000H\u0016J\t\u0010\u0014\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0006HÆ\u0003J\u001d\u0010\u0016\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00112\b\u0010\u0018\u001a\u0004\u0018\u00010\u0019HÖ\u0003J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\t\u0010\u001c\u001a\u00020\u0004HÖ\u0001R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\f\u001a\u00020\r8F¢\u0006\u0006\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$Sticker;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$HasPackId;", "packId", "", "stickerRecord", "Lorg/thoughtcrime/securesms/database/model/StickerRecord;", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/database/model/StickerRecord;)V", "getPackId", "()Ljava/lang/String;", "getStickerRecord", "()Lorg/thoughtcrime/securesms/database/model/StickerRecord;", "uri", "Lorg/thoughtcrime/securesms/mms/DecryptableStreamUriLoader$DecryptableUri;", "getUri", "()Lorg/thoughtcrime/securesms/mms/DecryptableStreamUriLoader$DecryptableUri;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "component1", "component2", "copy", "equals", "other", "", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Sticker implements MappingModel<Sticker>, HasPackId {
        private final String packId;
        private final StickerRecord stickerRecord;

        public static /* synthetic */ Sticker copy$default(Sticker sticker, String str, StickerRecord stickerRecord, int i, Object obj) {
            if ((i & 1) != 0) {
                str = sticker.getPackId();
            }
            if ((i & 2) != 0) {
                stickerRecord = sticker.stickerRecord;
            }
            return sticker.copy(str, stickerRecord);
        }

        public final String component1() {
            return getPackId();
        }

        public final StickerRecord component2() {
            return this.stickerRecord;
        }

        public final Sticker copy(String str, StickerRecord stickerRecord) {
            Intrinsics.checkNotNullParameter(str, "packId");
            Intrinsics.checkNotNullParameter(stickerRecord, "stickerRecord");
            return new Sticker(str, stickerRecord);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Sticker)) {
                return false;
            }
            Sticker sticker = (Sticker) obj;
            return Intrinsics.areEqual(getPackId(), sticker.getPackId()) && Intrinsics.areEqual(this.stickerRecord, sticker.stickerRecord);
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Sticker sticker) {
            return MappingModel.CC.$default$getChangePayload(this, sticker);
        }

        public int hashCode() {
            return (getPackId().hashCode() * 31) + this.stickerRecord.hashCode();
        }

        public String toString() {
            return "Sticker(packId=" + getPackId() + ", stickerRecord=" + this.stickerRecord + ')';
        }

        public Sticker(String str, StickerRecord stickerRecord) {
            Intrinsics.checkNotNullParameter(str, "packId");
            Intrinsics.checkNotNullParameter(stickerRecord, "stickerRecord");
            this.packId = str;
            this.stickerRecord = stickerRecord;
        }

        @Override // org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.HasPackId
        public String getPackId() {
            return this.packId;
        }

        public final StickerRecord getStickerRecord() {
            return this.stickerRecord;
        }

        public final DecryptableStreamUriLoader.DecryptableUri getUri() {
            return new DecryptableStreamUriLoader.DecryptableUri(this.stickerRecord.getUri());
        }

        public boolean areItemsTheSame(Sticker sticker) {
            Intrinsics.checkNotNullParameter(sticker, "newItem");
            return Intrinsics.areEqual(getPackId(), sticker.getPackId()) && this.stickerRecord.getRowId() == sticker.stickerRecord.getRowId();
        }

        public boolean areContentsTheSame(Sticker sticker) {
            Intrinsics.checkNotNullParameter(sticker, "newItem");
            return areItemsTheSame(sticker);
        }
    }

    /* compiled from: KeyboardStickerListAdapter.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$StickerViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$Sticker;", "itemView", "Landroid/view/View;", "(Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter;Landroid/view/View;)V", DraftDatabase.Draft.IMAGE, "Landroid/widget/ImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class StickerViewHolder extends MappingViewHolder<Sticker> {
        private final ImageView image;
        final /* synthetic */ KeyboardStickerListAdapter this$0;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StickerViewHolder(KeyboardStickerListAdapter keyboardStickerListAdapter, View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.this$0 = keyboardStickerListAdapter;
            View findViewById = findViewById(R.id.sticker_keyboard_page_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.sticker_keyboard_page_image)");
            this.image = (ImageView) findViewById;
        }

        /* JADX DEBUG: Type inference failed for r1v1. Raw type applied. Possible types: com.bumptech.glide.load.Option<java.lang.Boolean>, com.bumptech.glide.load.Option<Y> */
        public void bind(Sticker sticker) {
            Intrinsics.checkNotNullParameter(sticker, "model");
            this.this$0.glideRequests.load((Object) sticker.getUri()).set((Option<Option>) ApngOptions.ANIMATE, (Option) Boolean.valueOf(this.this$0.allowApngAnimation)).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).into(this.image);
            if (this.this$0.eventListener != null) {
                this.itemView.setOnClickListener(new KeyboardStickerListAdapter$StickerViewHolder$$ExternalSyntheticLambda0(this.this$0, sticker));
                this.itemView.setOnLongClickListener(new KeyboardStickerListAdapter$StickerViewHolder$$ExternalSyntheticLambda1(this.this$0, sticker));
                return;
            }
            this.itemView.setOnClickListener(null);
            this.itemView.setOnLongClickListener(null);
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1989bind$lambda0(KeyboardStickerListAdapter keyboardStickerListAdapter, Sticker sticker, View view) {
            Intrinsics.checkNotNullParameter(keyboardStickerListAdapter, "this$0");
            Intrinsics.checkNotNullParameter(sticker, "$model");
            keyboardStickerListAdapter.eventListener.onStickerClicked(sticker);
        }

        /* renamed from: bind$lambda-1 */
        public static final boolean m1990bind$lambda1(KeyboardStickerListAdapter keyboardStickerListAdapter, Sticker sticker, View view) {
            Intrinsics.checkNotNullParameter(keyboardStickerListAdapter, "this$0");
            Intrinsics.checkNotNullParameter(sticker, "$model");
            keyboardStickerListAdapter.eventListener.onStickerLongClicked(sticker);
            return true;
        }
    }

    /* compiled from: KeyboardStickerListAdapter.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u00012\u00020\u0002B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\t\u0010\u0010\u001a\u00020\u0004HÆ\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0004HÂ\u0003J\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0007HÂ\u0003¢\u0006\u0002\u0010\u0013J0\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001¢\u0006\u0002\u0010\u0015J\u0013\u0010\u0016\u001a\u00020\r2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\u000e\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u001bJ\t\u0010\u001c\u001a\u00020\u0007HÖ\u0001J\t\u0010\u001d\u001a\u00020\u0004HÖ\u0001R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\u000b¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$StickerHeader;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$HasPackId;", "packId", "", MultiselectForwardFragment.DIALOG_TITLE, "titleResource", "", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V", "getPackId", "()Ljava/lang/String;", "Ljava/lang/Integer;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "component1", "component2", "component3", "()Ljava/lang/Integer;", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$StickerHeader;", "equals", "other", "", "getTitle", "context", "Landroid/content/Context;", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StickerHeader implements MappingModel<StickerHeader>, HasPackId {
        private final String packId;
        private final String title;
        private final Integer titleResource;

        private final String component2() {
            return this.title;
        }

        private final Integer component3() {
            return this.titleResource;
        }

        public static /* synthetic */ StickerHeader copy$default(StickerHeader stickerHeader, String str, String str2, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                str = stickerHeader.getPackId();
            }
            if ((i & 2) != 0) {
                str2 = stickerHeader.title;
            }
            if ((i & 4) != 0) {
                num = stickerHeader.titleResource;
            }
            return stickerHeader.copy(str, str2, num);
        }

        public final String component1() {
            return getPackId();
        }

        public final StickerHeader copy(String str, String str2, Integer num) {
            Intrinsics.checkNotNullParameter(str, "packId");
            return new StickerHeader(str, str2, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StickerHeader)) {
                return false;
            }
            StickerHeader stickerHeader = (StickerHeader) obj;
            return Intrinsics.areEqual(getPackId(), stickerHeader.getPackId()) && Intrinsics.areEqual(this.title, stickerHeader.title) && Intrinsics.areEqual(this.titleResource, stickerHeader.titleResource);
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(StickerHeader stickerHeader) {
            return MappingModel.CC.$default$getChangePayload(this, stickerHeader);
        }

        public int hashCode() {
            int hashCode = getPackId().hashCode() * 31;
            String str = this.title;
            int i = 0;
            int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
            Integer num = this.titleResource;
            if (num != null) {
                i = num.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "StickerHeader(packId=" + getPackId() + ", title=" + this.title + ", titleResource=" + this.titleResource + ')';
        }

        public StickerHeader(String str, String str2, Integer num) {
            Intrinsics.checkNotNullParameter(str, "packId");
            this.packId = str;
            this.title = str2;
            this.titleResource = num;
        }

        @Override // org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter.HasPackId
        public String getPackId() {
            return this.packId;
        }

        public final String getTitle(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            String str = this.title;
            if (str != null) {
                return str;
            }
            Resources resources = context.getResources();
            Integer num = this.titleResource;
            String string = resources.getString(num != null ? num.intValue() : R.string.StickerManagementAdapter_untitled);
            Intrinsics.checkNotNullExpressionValue(string, "context.resources.getStr…nagementAdapter_untitled)");
            return string;
        }

        public boolean areItemsTheSame(StickerHeader stickerHeader) {
            Intrinsics.checkNotNullParameter(stickerHeader, "newItem");
            return Intrinsics.areEqual(this.title, stickerHeader.title);
        }

        public boolean areContentsTheSame(StickerHeader stickerHeader) {
            Intrinsics.checkNotNullParameter(stickerHeader, "newItem");
            return areItemsTheSame(stickerHeader);
        }
    }

    /* compiled from: KeyboardStickerListAdapter.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$StickerHeaderViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerListAdapter$StickerHeader;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", MultiselectForwardFragment.DIALOG_TITLE, "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StickerHeaderViewHolder extends MappingViewHolder<StickerHeader> {
        private final TextView title;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StickerHeaderViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.sticker_grid_header_title);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.sticker_grid_header_title)");
            this.title = (TextView) findViewById;
        }

        public void bind(StickerHeader stickerHeader) {
            Intrinsics.checkNotNullParameter(stickerHeader, "model");
            TextView textView = this.title;
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setText(stickerHeader.getTitle(context));
        }
    }
}
