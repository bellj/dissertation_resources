package org.thoughtcrime.securesms.keyboard.emoji;

import kotlin.Metadata;

/* compiled from: KeyboardPageSearchView.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\t\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0002"}, d2 = {"REVEAL_DURATION", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class KeyboardPageSearchViewKt {
    private static final long REVEAL_DURATION;
}
