package org.thoughtcrime.securesms.keyboard.sticker;

import android.content.res.ColorStateList;
import android.view.View;
import android.widget.ImageView;
import androidx.core.widget.ImageViewCompat;
import com.bumptech.glide.load.Option;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.glide.cache.ApngOptions;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerPackListAdapter;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardRepository;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: KeyboardStickerPackListAdapter.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\u000b\fB)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0002\u0010\nR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "glideRequests", "Lorg/thoughtcrime/securesms/mms/GlideRequests;", "allowApngAnimation", "", "onTabSelected", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter$StickerPack;", "", "(Lorg/thoughtcrime/securesms/mms/GlideRequests;ZLkotlin/jvm/functions/Function1;)V", "StickerPack", "StickerPackViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class KeyboardStickerPackListAdapter extends MappingAdapter {
    private final boolean allowApngAnimation;
    private final GlideRequests glideRequests;
    private final Function1<StickerPack, Unit> onTabSelected;

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerPackListAdapter$StickerPack, kotlin.Unit> */
    /* JADX WARN: Multi-variable type inference failed */
    public KeyboardStickerPackListAdapter(GlideRequests glideRequests, boolean z, Function1<? super StickerPack, Unit> function1) {
        Intrinsics.checkNotNullParameter(glideRequests, "glideRequests");
        Intrinsics.checkNotNullParameter(function1, "onTabSelected");
        this.glideRequests = glideRequests;
        this.allowApngAnimation = z;
        this.onTabSelected = function1;
        registerFactory(StickerPack.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerPackListAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new KeyboardStickerPackListAdapter.StickerPackViewHolder(KeyboardStickerPackListAdapter.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.keyboard_pager_category_icon));
    }

    /* compiled from: KeyboardStickerPackListAdapter.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0000H\u0016J\u0010\u0010\u0017\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0000H\u0016J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u001b\u001a\u00020\u00052\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dHÖ\u0003J\t\u0010\u001e\u001a\u00020\bHÖ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\rR\u0013\u0010\u0011\u001a\u0004\u0018\u00010\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter$StickerPack;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "packRecord", "Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;", "selected", "", "(Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;Z)V", "iconResource", "", "getIconResource", "()I", "loadImage", "getLoadImage", "()Z", "getPackRecord", "()Lorg/thoughtcrime/securesms/keyboard/sticker/StickerKeyboardRepository$KeyboardStickerPack;", "getSelected", "uri", "Lorg/thoughtcrime/securesms/mms/DecryptableStreamUriLoader$DecryptableUri;", "getUri", "()Lorg/thoughtcrime/securesms/mms/DecryptableStreamUriLoader$DecryptableUri;", "areContentsTheSame", "newItem", "areItemsTheSame", "component1", "component2", "copy", "equals", "other", "", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StickerPack implements MappingModel<StickerPack> {
        private final int iconResource;
        private final boolean loadImage;
        private final StickerKeyboardRepository.KeyboardStickerPack packRecord;
        private final boolean selected;
        private final DecryptableStreamUriLoader.DecryptableUri uri;

        public static /* synthetic */ StickerPack copy$default(StickerPack stickerPack, StickerKeyboardRepository.KeyboardStickerPack keyboardStickerPack, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                keyboardStickerPack = stickerPack.packRecord;
            }
            if ((i & 2) != 0) {
                z = stickerPack.selected;
            }
            return stickerPack.copy(keyboardStickerPack, z);
        }

        public final StickerKeyboardRepository.KeyboardStickerPack component1() {
            return this.packRecord;
        }

        public final boolean component2() {
            return this.selected;
        }

        public final StickerPack copy(StickerKeyboardRepository.KeyboardStickerPack keyboardStickerPack, boolean z) {
            Intrinsics.checkNotNullParameter(keyboardStickerPack, "packRecord");
            return new StickerPack(keyboardStickerPack, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof StickerPack)) {
                return false;
            }
            StickerPack stickerPack = (StickerPack) obj;
            return Intrinsics.areEqual(this.packRecord, stickerPack.packRecord) && this.selected == stickerPack.selected;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(StickerPack stickerPack) {
            return MappingModel.CC.$default$getChangePayload(this, stickerPack);
        }

        public int hashCode() {
            int hashCode = this.packRecord.hashCode() * 31;
            boolean z = this.selected;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            return "StickerPack(packRecord=" + this.packRecord + ", selected=" + this.selected + ')';
        }

        public StickerPack(StickerKeyboardRepository.KeyboardStickerPack keyboardStickerPack, boolean z) {
            Intrinsics.checkNotNullParameter(keyboardStickerPack, "packRecord");
            this.packRecord = keyboardStickerPack;
            this.selected = z;
            int i = 0;
            this.loadImage = keyboardStickerPack.getCoverResource() == null;
            this.uri = keyboardStickerPack.getCoverUri() != null ? new DecryptableStreamUriLoader.DecryptableUri(keyboardStickerPack.getCoverUri()) : null;
            Integer coverResource = keyboardStickerPack.getCoverResource();
            this.iconResource = coverResource != null ? coverResource.intValue() : i;
        }

        public /* synthetic */ StickerPack(StickerKeyboardRepository.KeyboardStickerPack keyboardStickerPack, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(keyboardStickerPack, (i & 2) != 0 ? false : z);
        }

        public final StickerKeyboardRepository.KeyboardStickerPack getPackRecord() {
            return this.packRecord;
        }

        public final boolean getSelected() {
            return this.selected;
        }

        public final boolean getLoadImage() {
            return this.loadImage;
        }

        public final DecryptableStreamUriLoader.DecryptableUri getUri() {
            return this.uri;
        }

        public final int getIconResource() {
            return this.iconResource;
        }

        public boolean areItemsTheSame(StickerPack stickerPack) {
            Intrinsics.checkNotNullParameter(stickerPack, "newItem");
            return Intrinsics.areEqual(this.packRecord.getPackId(), stickerPack.packRecord.getPackId());
        }

        public boolean areContentsTheSame(StickerPack stickerPack) {
            Intrinsics.checkNotNullParameter(stickerPack, "newItem");
            return areItemsTheSame(stickerPack) && this.selected == stickerPack.selected;
        }
    }

    /* compiled from: KeyboardStickerPackListAdapter.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0002H\u0016R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter$StickerPackViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter$StickerPack;", "itemView", "Landroid/view/View;", "(Lorg/thoughtcrime/securesms/keyboard/sticker/KeyboardStickerPackListAdapter;Landroid/view/View;)V", "defaultTint", "Landroid/content/res/ColorStateList;", "icon", "Landroid/widget/ImageView;", "selected", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class StickerPackViewHolder extends MappingViewHolder<StickerPack> {
        private final ColorStateList defaultTint;
        private final ImageView icon;
        private final View selected;
        final /* synthetic */ KeyboardStickerPackListAdapter this$0;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public StickerPackViewHolder(KeyboardStickerPackListAdapter keyboardStickerPackListAdapter, View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.this$0 = keyboardStickerPackListAdapter;
            View findViewById = findViewById(R.id.category_icon_selected);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.category_icon_selected)");
            this.selected = findViewById;
            View findViewById2 = findViewById(R.id.category_icon);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.category_icon)");
            ImageView imageView = (ImageView) findViewById2;
            this.icon = imageView;
            this.defaultTint = ImageViewCompat.getImageTintList(imageView);
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1991bind$lambda0(KeyboardStickerPackListAdapter keyboardStickerPackListAdapter, StickerPack stickerPack, View view) {
            Intrinsics.checkNotNullParameter(keyboardStickerPackListAdapter, "this$0");
            Intrinsics.checkNotNullParameter(stickerPack, "$model");
            keyboardStickerPackListAdapter.onTabSelected.invoke(stickerPack);
        }

        /* JADX DEBUG: Type inference failed for r0v12. Raw type applied. Possible types: com.bumptech.glide.load.Option<java.lang.Boolean>, com.bumptech.glide.load.Option<Y> */
        public void bind(StickerPack stickerPack) {
            Intrinsics.checkNotNullParameter(stickerPack, "model");
            this.itemView.setOnClickListener(new KeyboardStickerPackListAdapter$StickerPackViewHolder$$ExternalSyntheticLambda0(this.this$0, stickerPack));
            this.selected.setSelected(stickerPack.getSelected());
            float f = 1.0f;
            if (stickerPack.getLoadImage()) {
                ImageViewCompat.setImageTintList(this.icon, null);
                ImageView imageView = this.icon;
                if (!stickerPack.getSelected()) {
                    f = 0.5f;
                }
                imageView.setAlpha(f);
                this.this$0.glideRequests.load((Object) stickerPack.getUri()).set((Option<Option>) ApngOptions.ANIMATE, (Option) Boolean.valueOf(this.this$0.allowApngAnimation)).into(this.icon);
                return;
            }
            ImageViewCompat.setImageTintList(this.icon, this.defaultTint);
            this.icon.setImageResource(stickerPack.getIconResource());
            this.icon.setAlpha(1.0f);
            this.icon.setSelected(stickerPack.getSelected());
        }
    }
}
