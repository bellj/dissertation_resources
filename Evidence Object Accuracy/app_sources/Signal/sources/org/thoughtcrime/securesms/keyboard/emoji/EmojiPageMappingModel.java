package org.thoughtcrime.securesms.keyboard.emoji;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: EmojiPageMappingModel.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiPageMappingModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "key", "", "emojiPageModel", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;)V", "getEmojiPageModel", "()Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "getKey", "()Ljava/lang/String;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiPageMappingModel implements MappingModel<EmojiPageMappingModel> {
    private final EmojiPageModel emojiPageModel;
    private final String key;

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(EmojiPageMappingModel emojiPageMappingModel) {
        return MappingModel.CC.$default$getChangePayload(this, emojiPageMappingModel);
    }

    public EmojiPageMappingModel(String str, EmojiPageModel emojiPageModel) {
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(emojiPageModel, "emojiPageModel");
        this.key = str;
        this.emojiPageModel = emojiPageModel;
    }

    public final EmojiPageModel getEmojiPageModel() {
        return this.emojiPageModel;
    }

    public final String getKey() {
        return this.key;
    }

    public boolean areItemsTheSame(EmojiPageMappingModel emojiPageMappingModel) {
        Intrinsics.checkNotNullParameter(emojiPageMappingModel, "newItem");
        return Intrinsics.areEqual(this.key, emojiPageMappingModel.key);
    }

    public boolean areContentsTheSame(EmojiPageMappingModel emojiPageMappingModel) {
        Intrinsics.checkNotNullParameter(emojiPageMappingModel, "newItem");
        return areItemsTheSame(emojiPageMappingModel) && Intrinsics.areEqual(emojiPageMappingModel.emojiPageModel.getSpriteUri(), this.emojiPageModel.getSpriteUri()) && emojiPageMappingModel.emojiPageModel.getIconAttr() == this.emojiPageModel.getIconAttr();
    }
}
