package org.thoughtcrime.securesms.keyboard;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.stickers.StickerSearchRepository;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;

/* compiled from: KeyboardPagerViewModel.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\bJ\u0012\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00070\bJ\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u0005H\u0007J\u000e\u0010\u000b\u001a\u00020\n2\u0006\u0010\u0003\u001a\u00020\u0005R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00070\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/KeyboardPagerViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "page", "Lorg/thoughtcrime/securesms/util/DefaultValueLiveData;", "Lorg/thoughtcrime/securesms/keyboard/KeyboardPage;", "pages", "", "Landroidx/lifecycle/LiveData;", "setOnlyPage", "", "switchToPage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class KeyboardPagerViewModel extends ViewModel {
    private final DefaultValueLiveData<KeyboardPage> page;
    private final DefaultValueLiveData<Set<KeyboardPage>> pages;

    public KeyboardPagerViewModel() {
        Set set = ArraysKt___ArraysKt.toMutableSet(KeyboardPage.values());
        if (SignalStore.settings().isPreferSystemEmoji()) {
            set.remove(KeyboardPage.EMOJI);
        }
        this.pages = new DefaultValueLiveData<>(set);
        this.page = new DefaultValueLiveData<>(CollectionsKt___CollectionsKt.first(set));
        new StickerSearchRepository(ApplicationDependencies.getApplication()).getStickerFeatureAvailability(new StickerSearchRepository.Callback() { // from class: org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.stickers.StickerSearchRepository.Callback
            public final void onResult(Object obj) {
                KeyboardPagerViewModel.m1948_init_$lambda1(KeyboardPagerViewModel.this, (Boolean) obj);
            }
        });
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m1948_init_$lambda1(KeyboardPagerViewModel keyboardPagerViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(keyboardPagerViewModel, "this$0");
        if (!bool.booleanValue()) {
            Set<KeyboardPage> value = keyboardPagerViewModel.pages.getValue();
            Intrinsics.checkNotNullExpressionValue(value, "pages.value");
            Set<KeyboardPage> set = CollectionsKt___CollectionsKt.toMutableSet(value);
            KeyboardPage keyboardPage = KeyboardPage.STICKER;
            set.remove(keyboardPage);
            keyboardPagerViewModel.pages.postValue(set);
            if (keyboardPagerViewModel.page.getValue() == keyboardPage) {
                keyboardPagerViewModel.switchToPage(KeyboardPage.GIF);
                keyboardPagerViewModel.switchToPage(KeyboardPage.EMOJI);
            }
        }
    }

    public final LiveData<KeyboardPage> page() {
        return this.page;
    }

    public final LiveData<Set<KeyboardPage>> pages() {
        return this.pages;
    }

    public final void setOnlyPage(KeyboardPage keyboardPage) {
        Intrinsics.checkNotNullParameter(keyboardPage, "page");
        this.pages.setValue(SetsKt__SetsJVMKt.setOf(keyboardPage));
        switchToPage(keyboardPage);
    }

    public final void switchToPage(KeyboardPage keyboardPage) {
        Intrinsics.checkNotNullParameter(keyboardPage, "page");
        if (this.pages.getValue().contains(keyboardPage) && this.page.getValue() != keyboardPage) {
            if (ThreadUtil.isMainThread()) {
                this.page.setValue(keyboardPage);
            } else {
                this.page.postValue(keyboardPage);
            }
        }
    }
}
