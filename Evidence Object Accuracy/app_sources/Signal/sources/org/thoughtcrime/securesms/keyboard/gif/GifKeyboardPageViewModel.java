package org.thoughtcrime.securesms.keyboard.gif;

import androidx.lifecycle.ViewModel;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: GifKeyboardPageViewModel.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifKeyboardPageViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "selectedTab", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "getSelectedTab", "()Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "setSelectedTab", "(Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GifKeyboardPageViewModel extends ViewModel {
    private GifQuickSearchOption selectedTab = GifQuickSearchOption.TRENDING;

    public final GifQuickSearchOption getSelectedTab() {
        return this.selectedTab;
    }

    public final void setSelectedTab(GifQuickSearchOption gifQuickSearchOption) {
        Intrinsics.checkNotNullParameter(gifQuickSearchOption, "<set-?>");
        this.selectedTab = gifQuickSearchOption;
    }
}
