package org.thoughtcrime.securesms.keyboard.emoji;

import android.view.KeyEvent;
import kotlin.Metadata;

/* compiled from: EmojiKeyboardPageFragment.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0002"}, d2 = {"DELETE_KEY_EVENT", "Landroid/view/KeyEvent;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiKeyboardPageFragmentKt {
    private static final KeyEvent DELETE_KEY_EVENT = new KeyEvent(0, 67);
}
