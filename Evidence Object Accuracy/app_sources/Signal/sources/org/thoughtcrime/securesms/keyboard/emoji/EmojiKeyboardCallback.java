package org.thoughtcrime.securesms.keyboard.emoji;

import kotlin.Metadata;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment;

/* compiled from: EmojiKeyboardCallback.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardCallback;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiEventListener;", "Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageFragment$Callback;", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$Callback;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface EmojiKeyboardCallback extends EmojiEventListener, EmojiKeyboardPageFragment.Callback, EmojiSearchFragment.Callback {
}
