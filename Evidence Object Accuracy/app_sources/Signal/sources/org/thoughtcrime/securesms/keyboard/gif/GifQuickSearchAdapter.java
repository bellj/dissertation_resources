package org.thoughtcrime.securesms.keyboard.gif;

import android.view.View;
import android.widget.ImageView;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: GifQuickSearchAdapter.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0007B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\u0010\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "clickListener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "", "(Lkotlin/jvm/functions/Function1;)V", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GifQuickSearchAdapter extends MappingAdapter {
    public GifQuickSearchAdapter(Function1<? super GifQuickSearchOption, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "clickListener");
        registerFactory(GifQuickSearch.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.keyboard.gif.GifQuickSearchAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GifQuickSearchAdapter.m1986_init_$lambda0(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.keyboard_pager_category_icon));
    }

    /* renamed from: _init_$lambda-0 */
    public static final MappingViewHolder m1986_init_$lambda0(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$clickListener");
        Intrinsics.checkNotNullExpressionValue(view, "v");
        return new ViewHolder(view, function1);
    }

    /* compiled from: GifQuickSearchAdapter.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006¢\u0006\u0002\u0010\tJ\u0010\u0010\r\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchAdapter$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearch;", "itemView", "Landroid/view/View;", "listener", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/keyboard/gif/GifQuickSearchOption;", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", DraftDatabase.Draft.IMAGE, "Landroid/widget/ImageView;", "imageSelected", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<GifQuickSearch> {
        private final ImageView image;
        private final View imageSelected;
        private final Function1<GifQuickSearchOption, Unit> listener;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.keyboard.gif.GifQuickSearchOption, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function1<? super GifQuickSearchOption, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "listener");
            this.listener = function1;
            View findViewById = findViewById(R.id.category_icon);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.category_icon)");
            this.image = (ImageView) findViewById;
            View findViewById2 = findViewById(R.id.category_icon_selected);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.category_icon_selected)");
            this.imageSelected = findViewById2;
        }

        public void bind(GifQuickSearch gifQuickSearch) {
            Intrinsics.checkNotNullParameter(gifQuickSearch, "model");
            this.image.setImageResource(gifQuickSearch.getGifQuickSearchOption().getImage());
            this.image.setSelected(gifQuickSearch.getSelected());
            this.imageSelected.setSelected(gifQuickSearch.getSelected());
            this.itemView.setOnClickListener(new GifQuickSearchAdapter$ViewHolder$$ExternalSyntheticLambda0(this, gifQuickSearch));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1988bind$lambda0(ViewHolder viewHolder, GifQuickSearch gifQuickSearch, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(gifQuickSearch, "$model");
            viewHolder.listener.invoke(gifQuickSearch.getGifQuickSearchOption());
        }
    }
}
