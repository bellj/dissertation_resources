package org.thoughtcrime.securesms.keyboard.sticker;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.StickerDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;

/* compiled from: StickerSearchRepository.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0003J\u0016\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\r\u001a\u00020\u000bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/sticker/StickerSearchRepository;", "", "()V", "emojiSearchDatabase", "Lorg/thoughtcrime/securesms/database/EmojiSearchDatabase;", "stickerDatabase", "Lorg/thoughtcrime/securesms/database/StickerDatabase;", "findStickersForEmoji", "", "Lorg/thoughtcrime/securesms/database/model/StickerRecord;", "emoji", "", "search", "query", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerSearchRepository {
    private final EmojiSearchDatabase emojiSearchDatabase;
    private final StickerDatabase stickerDatabase;

    public StickerSearchRepository() {
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        this.emojiSearchDatabase = companion.emojiSearch();
        this.stickerDatabase = companion.stickers();
    }

    public final List<StickerRecord> search(String str) {
        Intrinsics.checkNotNullParameter(str, "query");
        if (str.length() == 0) {
            return StickerSearchRepositoryKt.readAll(new StickerDatabase.StickerRecordReader(this.stickerDatabase.getRecentlyUsedStickers(24)));
        }
        List<StickerRecord> findStickersForEmoji = findStickersForEmoji(str);
        List<String> query = this.emojiSearchDatabase.query(str, 20);
        Intrinsics.checkNotNullExpressionValue(query, "emojiSearchDatabase.quer…OJI_SEARCH_RESULTS_LIMIT)");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(query, 10));
        for (String str2 : query) {
            Intrinsics.checkNotNullExpressionValue(str2, "it");
            arrayList.add(findStickersForEmoji(str2));
        }
        return CollectionsKt___CollectionsKt.plus((Collection) findStickersForEmoji, (Iterable) CollectionsKt__IterablesKt.flatten(arrayList));
    }

    private final List<StickerRecord> findStickersForEmoji(String str) {
        String canonicalRepresentation = EmojiUtil.getCanonicalRepresentation(str);
        Intrinsics.checkNotNullExpressionValue(canonicalRepresentation, "getCanonicalRepresentation(emoji)");
        Set<String> allRepresentations = EmojiUtil.getAllRepresentations(canonicalRepresentation);
        Intrinsics.checkNotNullExpressionValue(allRepresentations, "getAllRepresentations(searchEmoji)");
        List<String> list = CollectionsKt___CollectionsKt.filterNotNull(allRepresentations);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (String str2 : list) {
            arrayList.add(StickerSearchRepositoryKt.readAll(new StickerDatabase.StickerRecordReader(this.stickerDatabase.getStickersByEmoji(str2))));
        }
        return CollectionsKt__IterablesKt.flatten(arrayList);
    }
}
