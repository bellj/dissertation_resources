package org.thoughtcrime.securesms.keyboard.sticker;

import android.view.View;
import org.thoughtcrime.securesms.keyboard.sticker.KeyboardStickerListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class KeyboardStickerListAdapter$StickerViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ KeyboardStickerListAdapter f$0;
    public final /* synthetic */ KeyboardStickerListAdapter.Sticker f$1;

    public /* synthetic */ KeyboardStickerListAdapter$StickerViewHolder$$ExternalSyntheticLambda0(KeyboardStickerListAdapter keyboardStickerListAdapter, KeyboardStickerListAdapter.Sticker sticker) {
        this.f$0 = keyboardStickerListAdapter;
        this.f$1 = sticker;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        KeyboardStickerListAdapter.StickerViewHolder.m1989bind$lambda0(this.f$0, this.f$1, view);
    }
}
