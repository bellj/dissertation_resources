package org.thoughtcrime.securesms.keyboard.emoji.search;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Consumer;
import j$.util.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiPageModelExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: EmojiSearchViewModel.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchRepository;", "(Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchRepository;)V", "emojiList", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchViewModel$EmojiSearchResults;", "getEmojiList", "()Landroidx/lifecycle/LiveData;", "pageModel", "Landroidx/lifecycle/MutableLiveData;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "onQueryChanged", "", "query", "", "EmojiSearchResults", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiSearchViewModel extends ViewModel {
    private final LiveData<EmojiSearchResults> emojiList;
    private final MutableLiveData<EmojiPageModel> pageModel;
    private final EmojiSearchRepository repository;

    public EmojiSearchViewModel(EmojiSearchRepository emojiSearchRepository) {
        Intrinsics.checkNotNullParameter(emojiSearchRepository, "repository");
        this.repository = emojiSearchRepository;
        MutableLiveData<EmojiPageModel> mutableLiveData = new MutableLiveData<>();
        this.pageModel = mutableLiveData;
        LiveData<EmojiSearchResults> mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchViewModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiSearchViewModel.m1981emojiList$lambda0((EmojiPageModel) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapAsync, "mapAsync(pageModel) { pa…ntEmojiPageModel.KEY)\n  }");
        this.emojiList = mapAsync;
        onQueryChanged("");
    }

    public final LiveData<EmojiSearchResults> getEmojiList() {
        return this.emojiList;
    }

    /* renamed from: emojiList$lambda-0 */
    public static final EmojiSearchResults m1981emojiList$lambda0(EmojiPageModel emojiPageModel) {
        Intrinsics.checkNotNullExpressionValue(emojiPageModel, "page");
        return new EmojiSearchResults(EmojiPageModelExtensionsKt.toMappingModels(emojiPageModel), Intrinsics.areEqual(emojiPageModel.getKey(), RecentEmojiPageModel.KEY));
    }

    public final void onQueryChanged(String str) {
        Intrinsics.checkNotNullParameter(str, "query");
        EmojiSearchRepository.submitQuery$default(this.repository, str, true, 0, new Consumer() { // from class: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchViewModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((EmojiPageModel) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ Consumer andThen(Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        }, 4, null);
    }

    /* compiled from: EmojiSearchViewModel.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0010\u0010\u0002\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0013\u0010\u000b\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00040\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0006HÆ\u0003J'\u0010\r\u001a\u00020\u00002\u0012\b\u0002\u0010\u0002\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u001b\u0010\u0002\u001a\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchViewModel$EmojiSearchResults;", "", "emojiList", "", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "isRecents", "", "(Ljava/util/List;Z)V", "getEmojiList", "()Ljava/util/List;", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EmojiSearchResults {
        private final List<MappingModel<?>> emojiList;
        private final boolean isRecents;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchViewModel$EmojiSearchResults */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ EmojiSearchResults copy$default(EmojiSearchResults emojiSearchResults, List list, boolean z, int i, Object obj) {
            if ((i & 1) != 0) {
                list = emojiSearchResults.emojiList;
            }
            if ((i & 2) != 0) {
                z = emojiSearchResults.isRecents;
            }
            return emojiSearchResults.copy(list, z);
        }

        public final List<MappingModel<?>> component1() {
            return this.emojiList;
        }

        public final boolean component2() {
            return this.isRecents;
        }

        public final EmojiSearchResults copy(List<? extends MappingModel<?>> list, boolean z) {
            Intrinsics.checkNotNullParameter(list, "emojiList");
            return new EmojiSearchResults(list, z);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof EmojiSearchResults)) {
                return false;
            }
            EmojiSearchResults emojiSearchResults = (EmojiSearchResults) obj;
            return Intrinsics.areEqual(this.emojiList, emojiSearchResults.emojiList) && this.isRecents == emojiSearchResults.isRecents;
        }

        public int hashCode() {
            int hashCode = this.emojiList.hashCode() * 31;
            boolean z = this.isRecents;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return hashCode + i;
        }

        public String toString() {
            return "EmojiSearchResults(emojiList=" + this.emojiList + ", isRecents=" + this.isRecents + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.util.adapter.mapping.MappingModel<?>> */
        /* JADX WARN: Multi-variable type inference failed */
        public EmojiSearchResults(List<? extends MappingModel<?>> list, boolean z) {
            Intrinsics.checkNotNullParameter(list, "emojiList");
            this.emojiList = list;
            this.isRecents = z;
        }

        public final List<MappingModel<?>> getEmojiList() {
            return this.emojiList;
        }

        public final boolean isRecents() {
            return this.isRecents;
        }
    }

    /* compiled from: EmojiSearchViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchRepository;", "(Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final EmojiSearchRepository repository;

        public Factory(EmojiSearchRepository emojiSearchRepository) {
            Intrinsics.checkNotNullParameter(emojiSearchRepository, "repository");
            this.repository = emojiSearchRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new EmojiSearchViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
