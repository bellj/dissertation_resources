package org.thoughtcrime.securesms.keyboard;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class TopShadowBehavior extends CoordinatorLayout.Behavior<View> {
    private boolean shown = true;
    private int targetId;

    public TopShadowBehavior(int i) {
        this.targetId = i;
    }

    public TopShadowBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.TopShadowBehavior);
            this.targetId = obtainStyledAttributes.getResourceId(0, 0);
            obtainStyledAttributes.recycle();
        }
        if (this.targetId == 0) {
            throw new IllegalStateException();
        }
    }

    @Override // androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior
    public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, View view, View view2) {
        return view2 instanceof RecyclerView;
    }

    public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, View view, View view2) {
        boolean z = view2.getY() != ((float) coordinatorLayout.findViewById(this.targetId).getHeight());
        if (z != this.shown) {
            if (z) {
                show(view);
            } else {
                hide(view);
            }
            this.shown = z;
        }
        if (view.getY() == 0.0f) {
            return false;
        }
        view.setY(0.0f);
        return true;
    }

    private void show(View view) {
        view.animate().setDuration(250).alpha(1.0f);
    }

    private void hide(View view) {
        view.animate().setDuration(250).alpha(0.0f);
    }
}
