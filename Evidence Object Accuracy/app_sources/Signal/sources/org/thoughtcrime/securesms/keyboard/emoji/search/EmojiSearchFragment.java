package org.thoughtcrime.securesms.keyboard.emoji.search;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.components.emoji.EmojiPageView;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchViewModel;
import org.thoughtcrime.securesms.util.ThemedFragment;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: EmojiSearchFragment.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002:\u0002\u0019\u001aB\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0016J&\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u001a\u0010\u0017\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\r2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageViewGridAdapter$VariationSelectorListener;", "()V", "callback", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$Callback;", "viewModel", "Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchViewModel;", "onAttach", "", "context", "Landroid/content/Context;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onVariationSelectorStateChanged", "open", "", "onViewCreated", "view", "Callback", "SearchCallbacks", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiSearchFragment extends Fragment implements EmojiPageViewGridAdapter.VariationSelectorListener {
    private Callback callback;
    private EmojiSearchViewModel viewModel;

    /* compiled from: EmojiSearchFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$Callback;", "", "closeEmojiSearch", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void closeEmojiSearch();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.VariationSelectorListener
    public void onVariationSelectorStateChanged(boolean z) {
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        Callback callback;
        Intrinsics.checkNotNullParameter(context, "context");
        super.onAttach(context);
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.callback = callback;
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return ThemedFragment.themedInflate(this, R.layout.emoji_search_fragment, layoutInflater, viewGroup);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        EmojiEventListener emojiEventListener;
        Intrinsics.checkNotNullParameter(view, "view");
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ViewModel viewModel = new ViewModelProvider(this, new EmojiSearchViewModel.Factory(new EmojiSearchRepository(requireContext))).get(EmojiSearchViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …rchViewModel::class.java]");
        this.viewModel = (EmojiSearchViewModel) viewModel;
        View findViewById = view.findViewById(R.id.kb_aware_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.kb_aware_layout)");
        KeyboardAwareLinearLayout keyboardAwareLinearLayout = (KeyboardAwareLinearLayout) findViewById;
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        emojiEventListener = (EmojiEventListener) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.emoji.EmojiEventListener");
                    }
                } else if (fragment instanceof EmojiEventListener) {
                    emojiEventListener = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            EmojiEventListener emojiEventListener2 = emojiEventListener;
            View findViewById2 = view.findViewById(R.id.emoji_search_view);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.emoji_search_view)");
            KeyboardPageSearchView keyboardPageSearchView = (KeyboardPageSearchView) findViewById2;
            View findViewById3 = view.findViewById(R.id.emoji_search_results_container);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.e…search_results_container)");
            View findViewById4 = view.findViewById(R.id.emoji_search_empty);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.emoji_search_empty)");
            TextView textView = (TextView) findViewById4;
            EmojiPageView emojiPageView = new EmojiPageView(requireContext(), emojiEventListener2, this, true, new LinearLayoutManager(requireContext(), 0, false), R.layout.emoji_display_item_list, R.layout.emoji_text_display_item_list);
            ((FrameLayout) findViewById3).addView(emojiPageView);
            keyboardPageSearchView.presentForEmojiSearch();
            keyboardPageSearchView.setCallbacks(new SearchCallbacks());
            EmojiSearchViewModel emojiSearchViewModel = this.viewModel;
            if (emojiSearchViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                emojiSearchViewModel = null;
            }
            emojiSearchViewModel.getEmojiList().observe(getViewLifecycleOwner(), new Observer(textView) { // from class: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ TextView f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    EmojiSearchFragment.m1976onViewCreated$lambda0(EmojiPageView.this, this.f$1, (EmojiSearchViewModel.EmojiSearchResults) obj);
                }
            });
            view.post(new Runnable(this) { // from class: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ EmojiSearchFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    EmojiSearchFragment.m1977onViewCreated$lambda2(KeyboardAwareLinearLayout.this, this.f$1);
                }
            });
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1976onViewCreated$lambda0(EmojiPageView emojiPageView, TextView textView, EmojiSearchViewModel.EmojiSearchResults emojiSearchResults) {
        Intrinsics.checkNotNullParameter(emojiPageView, "$emojiPageView");
        Intrinsics.checkNotNullParameter(textView, "$noResults");
        emojiPageView.setList(emojiSearchResults.getEmojiList(), null);
        if ((!emojiSearchResults.getEmojiList().isEmpty()) || emojiSearchResults.isRecents()) {
            emojiPageView.setVisibility(0);
            textView.setVisibility(8);
            return;
        }
        emojiPageView.setVisibility(4);
        textView.setVisibility(0);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m1977onViewCreated$lambda2(KeyboardAwareLinearLayout keyboardAwareLinearLayout, EmojiSearchFragment emojiSearchFragment) {
        Intrinsics.checkNotNullParameter(keyboardAwareLinearLayout, "$keyboardAwareLinearLayout");
        Intrinsics.checkNotNullParameter(emojiSearchFragment, "this$0");
        keyboardAwareLinearLayout.addOnKeyboardHiddenListener(new KeyboardAwareLinearLayout.OnKeyboardHiddenListener() { // from class: org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardHiddenListener
            public final void onKeyboardHidden() {
                EmojiSearchFragment.m1978onViewCreated$lambda2$lambda1(EmojiSearchFragment.this);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-2$lambda-1 */
    public static final void m1978onViewCreated$lambda2$lambda1(EmojiSearchFragment emojiSearchFragment) {
        Intrinsics.checkNotNullParameter(emojiSearchFragment, "this$0");
        Callback callback = emojiSearchFragment.callback;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.closeEmojiSearch();
    }

    /* compiled from: EmojiSearchFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment$SearchCallbacks;", "Lorg/thoughtcrime/securesms/keyboard/emoji/KeyboardPageSearchView$Callbacks;", "(Lorg/thoughtcrime/securesms/keyboard/emoji/search/EmojiSearchFragment;)V", "onNavigationClicked", "", "onQueryChanged", "query", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private final class SearchCallbacks implements KeyboardPageSearchView.Callbacks {
        public SearchCallbacks() {
            EmojiSearchFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onClicked() {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onClicked(this);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onFocusGained() {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusGained(this);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onFocusLost() {
            KeyboardPageSearchView.Callbacks.DefaultImpls.onFocusLost(this);
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onNavigationClicked() {
            ViewUtil.hideKeyboard(EmojiSearchFragment.this.requireContext(), EmojiSearchFragment.this.requireView());
        }

        @Override // org.thoughtcrime.securesms.keyboard.emoji.KeyboardPageSearchView.Callbacks
        public void onQueryChanged(String str) {
            Intrinsics.checkNotNullParameter(str, "query");
            EmojiSearchViewModel emojiSearchViewModel = EmojiSearchFragment.this.viewModel;
            if (emojiSearchViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                emojiSearchViewModel = null;
            }
            emojiSearchViewModel.onQueryChanged(str);
        }
    }
}
