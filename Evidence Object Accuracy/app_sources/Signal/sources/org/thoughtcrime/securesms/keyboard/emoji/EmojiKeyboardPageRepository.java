package org.thoughtcrime.securesms.keyboard.emoji;

import android.content.Context;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.components.emoji.EmojiPageModel;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: EmojiKeyboardPageRepository.kt */
@Metadata(bv = {}, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u000f\u0012\u0006\u0010\t\u001a\u00020\b¢\u0006\u0004\b\u000b\u0010\fJ\u001a\u0010\u0007\u001a\u00020\u00062\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002R\u0014\u0010\t\u001a\u00020\b8\u0002X\u0004¢\u0006\u0006\n\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/keyboard/emoji/EmojiKeyboardPageRepository;", "", "j$/util/function/Consumer", "", "Lorg/thoughtcrime/securesms/components/emoji/EmojiPageModel;", "consumer", "", "getEmoji", "Landroid/content/Context;", "context", "Landroid/content/Context;", "<init>", "(Landroid/content/Context;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EmojiKeyboardPageRepository {
    private final Context context;

    public EmojiKeyboardPageRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final void getEmoji(Consumer<List<EmojiPageModel>> consumer) {
        Intrinsics.checkNotNullParameter(consumer, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                EmojiKeyboardPageRepository.$r8$lambda$9pyqbciU7nalsFZrN1JPdcoghJM(EmojiKeyboardPageRepository.this, this.f$1);
            }
        });
    }

    /* renamed from: getEmoji$lambda-0 */
    public static final void m1964getEmoji$lambda0(EmojiKeyboardPageRepository emojiKeyboardPageRepository, Consumer consumer) {
        Intrinsics.checkNotNullParameter(emojiKeyboardPageRepository, "this$0");
        Intrinsics.checkNotNullParameter(consumer, "$consumer");
        ArrayList arrayList = new ArrayList();
        arrayList.add(new RecentEmojiPageModel(emojiKeyboardPageRepository.context, TextSecurePreferences.RECENT_STORAGE_KEY));
        boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, EmojiSource.Companion.getLatest().getDisplayPages());
        consumer.accept(arrayList);
    }
}
