package org.thoughtcrime.securesms.keyboard.gif;

import android.view.View;
import org.thoughtcrime.securesms.keyboard.gif.GifQuickSearchAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GifQuickSearchAdapter$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ GifQuickSearchAdapter.ViewHolder f$0;
    public final /* synthetic */ GifQuickSearch f$1;

    public /* synthetic */ GifQuickSearchAdapter$ViewHolder$$ExternalSyntheticLambda0(GifQuickSearchAdapter.ViewHolder viewHolder, GifQuickSearch gifQuickSearch) {
        this.f$0 = viewHolder;
        this.f$1 = gifQuickSearch;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GifQuickSearchAdapter.ViewHolder.m1988bind$lambda0(this.f$0, this.f$1, view);
    }
}
