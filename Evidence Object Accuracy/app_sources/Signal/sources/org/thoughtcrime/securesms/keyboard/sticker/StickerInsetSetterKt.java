package org.thoughtcrime.securesms.keyboard.sticker;

import kotlin.Metadata;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: StickerInsetSetter.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0003"}, d2 = {"horizontalInset", "", "verticalInset", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StickerInsetSetterKt {
    private static final int horizontalInset = ViewUtil.dpToPx(8);
    private static final int verticalInset = ViewUtil.dpToPx(8);
}
