package org.thoughtcrime.securesms;

import androidx.appcompat.app.AlertDialog;
import j$.util.Optional;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.contacts.ContactSelectionListItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes.dex */
public final /* synthetic */ class ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda3 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ ContactSelectionListFragment.ListClickListener f$0;
    public final /* synthetic */ AlertDialog f$1;
    public final /* synthetic */ ContactSelectionListItem f$2;

    public /* synthetic */ ContactSelectionListFragment$ListClickListener$$ExternalSyntheticLambda3(ContactSelectionListFragment.ListClickListener listClickListener, AlertDialog alertDialog, ContactSelectionListItem contactSelectionListItem) {
        this.f$0 = listClickListener;
        this.f$1 = alertDialog;
        this.f$2 = contactSelectionListItem;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.lambda$onItemClick$3(this.f$1, this.f$2, (Optional) obj);
    }
}
