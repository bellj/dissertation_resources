package org.thoughtcrime.securesms.color;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* loaded from: classes3.dex */
public class MaterialColors {
    public static final MaterialColorList CONVERSATION_PALETTE = new MaterialColorList(new ArrayList(Arrays.asList(MaterialColor.PLUM, MaterialColor.CRIMSON, MaterialColor.VERMILLION, MaterialColor.VIOLET, MaterialColor.INDIGO, MaterialColor.TAUPE, MaterialColor.ULTRAMARINE, MaterialColor.BLUE, MaterialColor.TEAL, MaterialColor.FOREST, MaterialColor.WINTERGREEN, MaterialColor.BURLAP, MaterialColor.STEEL)));

    /* loaded from: classes3.dex */
    public static class MaterialColorList {
        private final List<MaterialColor> colors;

        private MaterialColorList(List<MaterialColor> list) {
            this.colors = list;
        }

        public MaterialColor get(int i) {
            return this.colors.get(i);
        }

        public int size() {
            return this.colors.size();
        }

        public MaterialColor getByColor(Context context, int i) {
            for (MaterialColor materialColor : this.colors) {
                if (materialColor.represents(context, i)) {
                    return materialColor;
                }
            }
            return null;
        }

        public int[] asConversationColorArray(Context context) {
            int[] iArr = new int[this.colors.size()];
            int i = 0;
            for (MaterialColor materialColor : this.colors) {
                iArr[i] = materialColor.toConversationColor(context);
                i++;
            }
            return iArr;
        }
    }
}
