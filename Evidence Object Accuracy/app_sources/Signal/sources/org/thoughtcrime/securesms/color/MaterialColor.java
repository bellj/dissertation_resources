package org.thoughtcrime.securesms.color;

import android.content.Context;
import android.graphics.Color;
import java.util.HashMap;
import java.util.Map;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes3.dex */
public enum MaterialColor {
    CRIMSON(R.color.conversation_crimson, R.color.conversation_crimson_tint, R.color.conversation_crimson_shade, "red"),
    VERMILLION(R.color.conversation_vermillion, R.color.conversation_vermillion_tint, R.color.conversation_vermillion_shade, "orange"),
    BURLAP(R.color.conversation_burlap, R.color.conversation_burlap_tint, R.color.conversation_burlap_shade, "brown"),
    FOREST(R.color.conversation_forest, R.color.conversation_forest_tint, R.color.conversation_forest_shade, "green"),
    WINTERGREEN(R.color.conversation_wintergreen, R.color.conversation_wintergreen_tint, R.color.conversation_wintergreen_shade, "light_green"),
    TEAL(R.color.conversation_teal, R.color.conversation_teal_tint, R.color.conversation_teal_shade, "teal"),
    BLUE(R.color.conversation_blue, R.color.conversation_blue_tint, R.color.conversation_blue_shade, "blue"),
    INDIGO(R.color.conversation_indigo, R.color.conversation_indigo_tint, R.color.conversation_indigo_shade, "indigo"),
    VIOLET(R.color.conversation_violet, R.color.conversation_violet_tint, R.color.conversation_violet_shade, "purple"),
    PLUM(R.color.conversation_plumb, R.color.conversation_plumb_tint, R.color.conversation_plumb_shade, "pink"),
    TAUPE(R.color.conversation_taupe, R.color.conversation_taupe_tint, R.color.conversation_taupe_shade, "blue_grey"),
    STEEL(R.color.conversation_steel, R.color.conversation_steel_tint, R.color.conversation_steel_shade, "grey"),
    ULTRAMARINE(R.color.conversation_ultramarine, R.color.conversation_ultramarine_tint, R.color.conversation_ultramarine_shade, "ultramarine"),
    GROUP(R.color.conversation_group, R.color.conversation_group_tint, R.color.conversation_group_shade, "blue");
    
    private static final Map<String, MaterialColor> COLOR_MATCHES = new HashMap<String, MaterialColor>() { // from class: org.thoughtcrime.securesms.color.MaterialColor.1
        {
            MaterialColor materialColor = MaterialColor.CRIMSON;
            put("red", materialColor);
            put("deep_orange", materialColor);
            MaterialColor materialColor2 = MaterialColor.VERMILLION;
            put("orange", materialColor2);
            put("amber", materialColor2);
            MaterialColor materialColor3 = MaterialColor.BURLAP;
            put("brown", materialColor3);
            put("yellow", materialColor3);
            put("pink", MaterialColor.PLUM);
            MaterialColor materialColor4 = MaterialColor.VIOLET;
            put("purple", materialColor4);
            put("deep_purple", materialColor4);
            put("indigo", MaterialColor.INDIGO);
            MaterialColor materialColor5 = MaterialColor.BLUE;
            put("blue", materialColor5);
            put("light_blue", materialColor5);
            MaterialColor materialColor6 = MaterialColor.TEAL;
            put("cyan", materialColor6);
            put("teal", materialColor6);
            put("green", MaterialColor.FOREST);
            MaterialColor materialColor7 = MaterialColor.WINTERGREEN;
            put("light_green", materialColor7);
            put("lime", materialColor7);
            put("blue_grey", MaterialColor.TAUPE);
            put("grey", MaterialColor.STEEL);
            put("ultramarine", MaterialColor.ULTRAMARINE);
            put("group_color", MaterialColor.GROUP);
        }
    };
    private final int mainColor;
    private final String serialized;
    private final int shadeColor;
    private final int tintColor;

    MaterialColor(int i, int i2, int i3, String str) {
        this.mainColor = i;
        this.tintColor = i2;
        this.shadeColor = i3;
        this.serialized = str;
    }

    public int toNotificationColor(Context context) {
        return context.getResources().getColor(ThemeUtil.isDarkNotificationTheme(context) ? this.shadeColor : this.mainColor);
    }

    public int toConversationColor(Context context) {
        return context.getResources().getColor(this.mainColor);
    }

    public int toAvatarColor(Context context) {
        return context.getResources().getColor(ThemeUtil.isDarkTheme(context) ? this.shadeColor : this.mainColor);
    }

    public int toActionBarColor(Context context) {
        return context.getResources().getColor(this.mainColor);
    }

    public int toStatusBarColor(Context context) {
        return context.getResources().getColor(this.mainColor);
    }

    public int toQuoteBarColorResource(Context context, boolean z) {
        if (!z) {
            return ThemeUtil.isDarkTheme(context) ? this.tintColor : this.shadeColor;
        }
        return R.color.core_white;
    }

    public int toQuoteBackgroundColor(Context context, boolean z) {
        if (!z) {
            int conversationColor = toConversationColor(context);
            return Color.argb(ThemeUtil.isDarkTheme(context) ? 51 : 102, Color.red(conversationColor), Color.green(conversationColor), Color.blue(conversationColor));
        }
        return context.getResources().getColor(ThemeUtil.isDarkTheme(context) ? R.color.transparent_black_40 : R.color.transparent_white_60);
    }

    public int toQuoteFooterColor(Context context, boolean z) {
        if (!z) {
            int conversationColor = toConversationColor(context);
            return Color.argb(ThemeUtil.isDarkTheme(context) ? 102 : 153, Color.red(conversationColor), Color.green(conversationColor), Color.blue(conversationColor));
        }
        return context.getResources().getColor(ThemeUtil.isDarkTheme(context) ? R.color.transparent_black_60 : R.color.transparent_white_80);
    }

    public boolean represents(Context context, int i) {
        return context.getResources().getColor(this.mainColor) == i || context.getResources().getColor(this.tintColor) == i || context.getResources().getColor(this.shadeColor) == i;
    }

    public String serialize() {
        return this.serialized;
    }

    public static MaterialColor fromSerialized(String str) throws UnknownColorException {
        Map<String, MaterialColor> map = COLOR_MATCHES;
        if (map.containsKey(str)) {
            return map.get(str);
        }
        throw new UnknownColorException("Unknown color: " + str);
    }

    /* loaded from: classes3.dex */
    public static class UnknownColorException extends Exception {
        public UnknownColorException(String str) {
            super(str);
        }
    }
}
