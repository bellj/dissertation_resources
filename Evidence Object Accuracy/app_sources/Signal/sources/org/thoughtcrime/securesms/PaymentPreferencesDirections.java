package org.thoughtcrime.securesms;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.payments.preferences.details.PaymentDetailsParcelable;
import org.thoughtcrime.securesms.payments.preferences.model.PayeeParcelable;

/* loaded from: classes.dex */
public class PaymentPreferencesDirections {
    private PaymentPreferencesDirections() {
    }

    public static ActionDirectlyToCreatePayment actionDirectlyToCreatePayment(PayeeParcelable payeeParcelable) {
        return new ActionDirectlyToCreatePayment(payeeParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsHome() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_paymentsHome);
    }

    public static ActionDirectlyToPaymentDetails actionDirectlyToPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
        return new ActionDirectlyToPaymentDetails(paymentDetailsParcelable);
    }

    public static NavDirections actionDirectlyToPaymentsTransfer() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_paymentsTransfer);
    }

    public static NavDirections actionDirectlyToPaymentsBackup() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_paymentsBackup);
    }

    /* loaded from: classes.dex */
    public static class ActionDirectlyToCreatePayment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_directly_to_createPayment;
        }

        private ActionDirectlyToCreatePayment(PayeeParcelable payeeParcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (payeeParcelable != null) {
                hashMap.put("payee", payeeParcelable);
                return;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public ActionDirectlyToCreatePayment setPayee(PayeeParcelable payeeParcelable) {
            if (payeeParcelable != null) {
                this.arguments.put("payee", payeeParcelable);
                return this;
            }
            throw new IllegalArgumentException("Argument \"payee\" is marked as non-null but was passed a null value.");
        }

        public ActionDirectlyToCreatePayment setNote(String str) {
            this.arguments.put("note", str);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("payee")) {
                PayeeParcelable payeeParcelable = (PayeeParcelable) this.arguments.get("payee");
                if (Parcelable.class.isAssignableFrom(PayeeParcelable.class) || payeeParcelable == null) {
                    bundle.putParcelable("payee", (Parcelable) Parcelable.class.cast(payeeParcelable));
                } else if (Serializable.class.isAssignableFrom(PayeeParcelable.class)) {
                    bundle.putSerializable("payee", (Serializable) Serializable.class.cast(payeeParcelable));
                } else {
                    throw new UnsupportedOperationException(PayeeParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("note")) {
                bundle.putString("note", (String) this.arguments.get("note"));
            } else {
                bundle.putString("note", null);
            }
            return bundle;
        }

        public PayeeParcelable getPayee() {
            return (PayeeParcelable) this.arguments.get("payee");
        }

        public String getNote() {
            return (String) this.arguments.get("note");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionDirectlyToCreatePayment actionDirectlyToCreatePayment = (ActionDirectlyToCreatePayment) obj;
            if (this.arguments.containsKey("payee") != actionDirectlyToCreatePayment.arguments.containsKey("payee")) {
                return false;
            }
            if (getPayee() == null ? actionDirectlyToCreatePayment.getPayee() != null : !getPayee().equals(actionDirectlyToCreatePayment.getPayee())) {
                return false;
            }
            if (this.arguments.containsKey("note") != actionDirectlyToCreatePayment.arguments.containsKey("note")) {
                return false;
            }
            if (getNote() == null ? actionDirectlyToCreatePayment.getNote() == null : getNote().equals(actionDirectlyToCreatePayment.getNote())) {
                return getActionId() == actionDirectlyToCreatePayment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((getPayee() != null ? getPayee().hashCode() : 0) + 31) * 31;
            if (getNote() != null) {
                i = getNote().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionDirectlyToCreatePayment(actionId=" + getActionId() + "){payee=" + getPayee() + ", note=" + getNote() + "}";
        }
    }

    /* loaded from: classes.dex */
    public static class ActionDirectlyToPaymentDetails implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_directly_to_paymentDetails;
        }

        private ActionDirectlyToPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (paymentDetailsParcelable != null) {
                hashMap.put("paymentDetails", paymentDetailsParcelable);
                return;
            }
            throw new IllegalArgumentException("Argument \"paymentDetails\" is marked as non-null but was passed a null value.");
        }

        public ActionDirectlyToPaymentDetails setPaymentDetails(PaymentDetailsParcelable paymentDetailsParcelable) {
            if (paymentDetailsParcelable != null) {
                this.arguments.put("paymentDetails", paymentDetailsParcelable);
                return this;
            }
            throw new IllegalArgumentException("Argument \"paymentDetails\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("paymentDetails")) {
                PaymentDetailsParcelable paymentDetailsParcelable = (PaymentDetailsParcelable) this.arguments.get("paymentDetails");
                if (Parcelable.class.isAssignableFrom(PaymentDetailsParcelable.class) || paymentDetailsParcelable == null) {
                    bundle.putParcelable("paymentDetails", (Parcelable) Parcelable.class.cast(paymentDetailsParcelable));
                } else if (Serializable.class.isAssignableFrom(PaymentDetailsParcelable.class)) {
                    bundle.putSerializable("paymentDetails", (Serializable) Serializable.class.cast(paymentDetailsParcelable));
                } else {
                    throw new UnsupportedOperationException(PaymentDetailsParcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public PaymentDetailsParcelable getPaymentDetails() {
            return (PaymentDetailsParcelable) this.arguments.get("paymentDetails");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionDirectlyToPaymentDetails actionDirectlyToPaymentDetails = (ActionDirectlyToPaymentDetails) obj;
            if (this.arguments.containsKey("paymentDetails") != actionDirectlyToPaymentDetails.arguments.containsKey("paymentDetails")) {
                return false;
            }
            if (getPaymentDetails() == null ? actionDirectlyToPaymentDetails.getPaymentDetails() == null : getPaymentDetails().equals(actionDirectlyToPaymentDetails.getPaymentDetails())) {
                return getActionId() == actionDirectlyToPaymentDetails.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getPaymentDetails() != null ? getPaymentDetails().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionDirectlyToPaymentDetails(actionId=" + getActionId() + "){paymentDetails=" + getPaymentDetails() + "}";
        }
    }
}
