package org.thoughtcrime.securesms;

import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;

/* loaded from: classes.dex */
public class DeviceLinkFragment extends Fragment implements View.OnClickListener {
    private LinearLayout container;
    private LinkClickedListener linkClickedListener;
    private Uri uri;

    /* loaded from: classes.dex */
    public interface LinkClickedListener {
        void onLink(Uri uri);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        LinearLayout linearLayout = (LinearLayout) layoutInflater.inflate(R.layout.device_link_fragment, (ViewGroup) this.container, false);
        this.container = linearLayout;
        linearLayout.findViewById(R.id.link_device).setOnClickListener(this);
        ViewCompat.setTransitionName(this.container.findViewById(R.id.devices), "devices");
        if (getResources().getConfiguration().orientation == 2) {
            this.container.setOrientation(0);
        } else {
            this.container.setOrientation(1);
        }
        return this.container;
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (configuration.orientation == 2) {
            this.container.setOrientation(0);
        } else {
            this.container.setOrientation(1);
        }
    }

    public void setLinkClickedListener(Uri uri, LinkClickedListener linkClickedListener) {
        this.uri = uri;
        this.linkClickedListener = linkClickedListener;
    }

    @Override // android.view.View.OnClickListener
    public void onClick(View view) {
        LinkClickedListener linkClickedListener = this.linkClickedListener;
        if (linkClickedListener != null) {
            linkClickedListener.onLink(this.uri);
        }
    }
}
