package org.thoughtcrime.securesms.delete;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Objects;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class DeleteAccountCountryPickerAdapter extends ListAdapter<Country, ViewHolder> {
    private final Callback callback;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onItemSelected(Country country);
    }

    public DeleteAccountCountryPickerAdapter(Callback callback) {
        super(new CountryDiffCallback());
        this.callback = callback;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.delete_account_country_adapter_item, viewGroup, false), new Consumer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerAdapter$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                DeleteAccountCountryPickerAdapter.$r8$lambda$Pfii1boBtIgLDp7oawppQHKXQ5I(DeleteAccountCountryPickerAdapter.this, (Integer) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onCreateViewHolder$0(Integer num) {
        this.callback.onItemSelected(getItem(num.intValue()));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.textView.setText(getItem(i).getDisplayName());
    }

    /* loaded from: classes4.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView;

        public ViewHolder(View view, Consumer<Integer> consumer) {
            super(view);
            this.textView = (TextView) view.findViewById(16908308);
            view.setOnClickListener(new DeleteAccountCountryPickerAdapter$ViewHolder$$ExternalSyntheticLambda0(this, consumer));
        }

        public /* synthetic */ void lambda$new$0(Consumer consumer, View view) {
            if (getAdapterPosition() != -1) {
                consumer.accept(Integer.valueOf(getAdapterPosition()));
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class CountryDiffCallback extends DiffUtil.ItemCallback<Country> {
        private CountryDiffCallback() {
        }

        public boolean areItemsTheSame(Country country, Country country2) {
            return Objects.equals(Integer.valueOf(country.getCode()), Integer.valueOf(country2.getCode()));
        }

        public boolean areContentsTheSame(Country country, Country country2) {
            return Objects.equals(country, country2);
        }
    }
}
