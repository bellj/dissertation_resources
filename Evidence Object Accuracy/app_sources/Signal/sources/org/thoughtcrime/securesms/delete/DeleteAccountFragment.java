package org.thoughtcrime.securesms.delete;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import com.google.i18n.phonenumbers.AsYouTypeFormatter;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import j$.util.Optional;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.LabeledEditText;
import org.thoughtcrime.securesms.delete.DeleteAccountEvent;
import org.thoughtcrime.securesms.delete.DeleteAccountViewModel;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* loaded from: classes4.dex */
public class DeleteAccountFragment extends Fragment {
    private TextView bullets;
    private LabeledEditText countryCode;
    private AsYouTypeFormatter countryFormatter;
    private ArrayAdapter<String> countrySpinnerAdapter;
    private DeleteAccountProgressDialog deletionProgressDialog;
    private LabeledEditText number;
    private DeleteAccountViewModel viewModel;

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.delete_account_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        View findViewById = view.findViewById(R.id.delete_account_fragment_delete);
        this.bullets = (TextView) view.findViewById(R.id.delete_account_fragment_bullets);
        this.countryCode = (LabeledEditText) view.findViewById(R.id.delete_account_fragment_country_code);
        this.number = (LabeledEditText) view.findViewById(R.id.delete_account_fragment_number);
        DeleteAccountViewModel deleteAccountViewModel = (DeleteAccountViewModel) new ViewModelProvider(requireActivity(), new DeleteAccountViewModel.Factory(new DeleteAccountRepository())).get(DeleteAccountViewModel.class);
        this.viewModel = deleteAccountViewModel;
        deleteAccountViewModel.getCountryDisplayName().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda8
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeleteAccountFragment.this.setCountryDisplay((String) obj);
            }
        });
        this.viewModel.getRegionCode().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeleteAccountFragment.this.handleRegionUpdated((String) obj);
            }
        });
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeleteAccountFragment.this.handleEvent((DeleteAccountEvent) obj);
            }
        });
        this.viewModel.getWalletBalance().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda11
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeleteAccountFragment.this.updateBullets((Optional) obj);
            }
        });
        initializeNumberInput();
        this.countryCode.getInput().addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda12
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                DeleteAccountFragment.this.afterCountryCodeChanged((Editable) obj);
            }
        }));
        this.countryCode.getInput().setImeOptions(5);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda13
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeleteAccountFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        initializeSpinner((Spinner) view.findViewById(R.id.delete_account_fragment_country_spinner));
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        this.viewModel.submit();
    }

    public void updateBullets(Optional<String> optional) {
        this.bullets.setText(buildBulletsText(optional));
    }

    private CharSequence buildBulletsText(Optional<String> optional) {
        SpannableStringBuilder append = new SpannableStringBuilder().append(SpanUtil.bullet(getString(R.string.DeleteAccountFragment__delete_your_account_info_and_profile_photo))).append((CharSequence) "\n").append(SpanUtil.bullet(getString(R.string.DeleteAccountFragment__delete_all_your_messages)));
        if (optional.isPresent()) {
            append.append((CharSequence) "\n");
            append.append(SpanUtil.bullet(getString(R.string.DeleteAccountFragment__delete_s_in_your_payments_account, optional.get())));
        }
        return append;
    }

    private void initializeSpinner(Spinner spinner) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(requireContext(), 17367048);
        this.countrySpinnerAdapter = arrayAdapter;
        arrayAdapter.setDropDownViewResource(17367049);
        spinner.setAdapter((SpinnerAdapter) this.countrySpinnerAdapter);
        spinner.setOnTouchListener(new View.OnTouchListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return DeleteAccountFragment.this.lambda$initializeSpinner$1(view, motionEvent);
            }
        });
        spinner.setOnKeyListener(new View.OnKeyListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnKeyListener
            public final boolean onKey(View view, int i, KeyEvent keyEvent) {
                return DeleteAccountFragment.this.lambda$initializeSpinner$2(view, i, keyEvent);
            }
        });
    }

    public /* synthetic */ boolean lambda$initializeSpinner$1(View view, MotionEvent motionEvent) {
        if (motionEvent.getAction() == 1) {
            pickCountry();
        }
        return true;
    }

    public /* synthetic */ boolean lambda$initializeSpinner$2(View view, int i, KeyEvent keyEvent) {
        if (i != 23 || keyEvent.getAction() != 1) {
            return false;
        }
        pickCountry();
        return true;
    }

    private void pickCountry() {
        this.countryCode.clearFocus();
        DeleteAccountCountryPickerFragment.show(requireFragmentManager());
    }

    public void setCountryDisplay(String str) {
        this.countrySpinnerAdapter.clear();
        if (TextUtils.isEmpty(str)) {
            this.countrySpinnerAdapter.add(requireContext().getString(R.string.RegistrationActivity_select_your_country));
        } else {
            this.countrySpinnerAdapter.add(str);
        }
    }

    public void handleRegionUpdated(String str) {
        PhoneNumberUtil instance = PhoneNumberUtil.getInstance();
        this.countryFormatter = str != null ? instance.getAsYouTypeFormatter(str) : null;
        reformatText(this.number.getText());
        if (!TextUtils.isEmpty(str) && !"ZZ".equals(str)) {
            this.number.requestFocus();
            int length = this.number.getText().length();
            this.number.getInput().setSelection(length, length);
            this.countryCode.setText(String.valueOf(instance.getCountryCodeForRegion(str)));
        }
    }

    private Long reformatText(Editable editable) {
        if (this.countryFormatter == null || TextUtils.isEmpty(editable)) {
            return null;
        }
        this.countryFormatter.clear();
        StringBuilder sb = new StringBuilder();
        String str = null;
        for (int i = 0; i < editable.length(); i++) {
            char charAt = editable.charAt(i);
            if (Character.isDigit(charAt)) {
                str = this.countryFormatter.inputDigit(charAt);
                sb.append(charAt);
            }
        }
        if (str != null && !editable.toString().equals(str)) {
            editable.replace(0, editable.length(), str);
        }
        if (sb.length() == 0) {
            return null;
        }
        return Long.valueOf(Long.parseLong(sb.toString()));
    }

    private void initializeNumberInput() {
        EditText input = this.number.getInput();
        Long nationalNumber = this.viewModel.getNationalNumber();
        if (nationalNumber != null) {
            this.number.setText(String.valueOf(nationalNumber));
        } else {
            this.number.setText("");
        }
        input.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                DeleteAccountFragment.this.afterNumberChanged((Editable) obj);
            }
        }));
        input.setImeOptions(6);
        input.setOnEditorActionListener(new TextView.OnEditorActionListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda4
            @Override // android.widget.TextView.OnEditorActionListener
            public final boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                return DeleteAccountFragment.this.lambda$initializeNumberInput$3(textView, i, keyEvent);
            }
        });
    }

    public /* synthetic */ boolean lambda$initializeNumberInput$3(TextView textView, int i, KeyEvent keyEvent) {
        if (i != 6) {
            return false;
        }
        ViewUtil.hideKeyboard(requireContext(), textView);
        this.viewModel.submit();
        return true;
    }

    public void afterCountryCodeChanged(Editable editable) {
        if (TextUtils.isEmpty(editable) || !TextUtils.isDigitsOnly(editable)) {
            this.viewModel.onCountrySelected(0);
        } else {
            this.viewModel.onCountrySelected(Integer.parseInt(editable.toString()));
        }
    }

    public void afterNumberChanged(Editable editable) {
        Long reformatText = reformatText(editable);
        if (reformatText != null) {
            this.viewModel.setNationalNumber(reformatText.longValue());
        }
    }

    /* renamed from: org.thoughtcrime.securesms.delete.DeleteAccountFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type;

        static {
            int[] iArr = new int[DeleteAccountEvent.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type = iArr;
            try {
                iArr[DeleteAccountEvent.Type.NO_COUNTRY_CODE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.NO_NATIONAL_NUMBER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.NOT_A_MATCH.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.CONFIRM_DELETION.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.LEAVE_GROUPS_FAILED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.PIN_DELETION_FAILED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.SERVER_DELETION_FAILED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.CANCEL_SUBSCRIPTION_FAILED.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.LOCAL_DATA_DELETION_FAILED.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.LEAVE_GROUPS_PROGRESS.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.LEAVE_GROUPS_FINISHED.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[DeleteAccountEvent.Type.CANCELING_SUBSCRIPTION.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public void handleEvent(DeleteAccountEvent deleteAccountEvent) {
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$delete$DeleteAccountEvent$Type[deleteAccountEvent.getType().ordinal()]) {
            case 1:
                Snackbar.make(requireView(), (int) R.string.DeleteAccountFragment__no_country_code, -1).show();
                return;
            case 2:
                Snackbar.make(requireView(), (int) R.string.DeleteAccountFragment__no_number, -1).show();
                return;
            case 3:
                new AlertDialog.Builder(requireContext()).setMessage(R.string.DeleteAccountFragment__the_phone_number).setPositiveButton(17039370, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda0
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setCancelable(true).show();
                return;
            case 4:
                new AlertDialog.Builder(requireContext()).setTitle(R.string.DeleteAccountFragment__are_you_sure).setMessage(R.string.DeleteAccountFragment__this_will_delete_your_signal_account).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda1
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).setPositiveButton(R.string.DeleteAccountFragment__delete_account, new DeleteAccountFragment$$ExternalSyntheticLambda2(this)).setCancelable(true).show();
                return;
            case 5:
            case 6:
            case 7:
            case 8:
                dismissDeletionProgressDialog();
                showNetworkDeletionFailedDialog();
                return;
            case 9:
                dismissDeletionProgressDialog();
                showLocalDataDeletionFailedDialog();
                return;
            case 10:
                ensureDeletionProgressDialog();
                this.deletionProgressDialog.presentLeavingGroups((DeleteAccountEvent.LeaveGroupsProgress) deleteAccountEvent);
                return;
            case 11:
                ensureDeletionProgressDialog();
                this.deletionProgressDialog.presentDeletingAccount();
                break;
            case 12:
                break;
            default:
                throw new IllegalStateException("Unknown error type: " + deleteAccountEvent);
        }
        ensureDeletionProgressDialog();
        this.deletionProgressDialog.presentCancelingSubscription();
    }

    private void dismissDeletionProgressDialog() {
        DeleteAccountProgressDialog deleteAccountProgressDialog = this.deletionProgressDialog;
        if (deleteAccountProgressDialog != null) {
            deleteAccountProgressDialog.dismiss();
            this.deletionProgressDialog = null;
        }
    }

    private void showNetworkDeletionFailedDialog() {
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.DeleteAccountFragment__account_not_deleted).setMessage(R.string.DeleteAccountFragment__there_was_a_problem).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DeleteAccountFragment$$ExternalSyntheticLambda2(this)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda5
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setCancelable(true).show();
    }

    private void showLocalDataDeletionFailedDialog() {
        new AlertDialog.Builder(requireContext()).setMessage(R.string.DeleteAccountFragment__failed_to_delete_local_data).setPositiveButton(R.string.DeleteAccountFragment__launch_app_settings, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountFragment$$ExternalSyntheticLambda14
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeleteAccountFragment.this.lambda$showLocalDataDeletionFailedDialog$7(dialogInterface, i);
            }
        }).setCancelable(false).show();
    }

    public /* synthetic */ void lambda$showLocalDataDeletionFailedDialog$7(DialogInterface dialogInterface, int i) {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.fromParts("package", requireActivity().getPackageName(), null));
        startActivity(intent);
    }

    public void handleDeleteAccountConfirmation(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        ensureDeletionProgressDialog();
        this.viewModel.deleteAccount();
    }

    private void ensureDeletionProgressDialog() {
        if (this.deletionProgressDialog == null) {
            this.deletionProgressDialog = DeleteAccountProgressDialog.show(requireContext());
        }
    }
}
