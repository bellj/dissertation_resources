package org.thoughtcrime.securesms.delete;

import androidx.core.util.Consumer;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import java.io.IOException;
import java.text.Collator;
import java.util.Comparator;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.delete.DeleteAccountEvent;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.pin.KbsEnclaves;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.whispersystems.signalservice.api.util.PhoneNumberFormatter;
import org.whispersystems.signalservice.internal.EmptyResponse;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.contacts.crypto.UnauthenticatedResponseException;

/* loaded from: classes4.dex */
public class DeleteAccountRepository {
    private static final String TAG = Log.tag(DeleteAccountRepository.class);

    public List<Country> getAllCountries() {
        return Stream.of(PhoneNumberUtil.getInstance().getSupportedRegions()).map(new Function() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountRepository$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeleteAccountRepository.getCountryForRegion((String) obj);
            }
        }).sorted(new RegionComparator()).toList();
    }

    public String getRegionDisplayName(String str) {
        return PhoneNumberFormatter.getRegionDisplayName(str).orElse("");
    }

    public int getRegionCountryCode(String str) {
        return PhoneNumberUtil.getInstance().getCountryCodeForRegion(str);
    }

    public void deleteAccount(Consumer<DeleteAccountEvent> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                DeleteAccountRepository.lambda$deleteAccount$0(Consumer.this);
            }
        });
    }

    public static /* synthetic */ void lambda$deleteAccount$0(Consumer consumer) {
        if (SignalStore.donationsValues().getSubscriber() != null) {
            String str = TAG;
            Log.i(str, "deleteAccount: attempting to cancel subscription");
            consumer.accept(DeleteAccountEvent.CancelingSubscription.INSTANCE);
            ServiceResponse<EmptyResponse> blockingGet = ApplicationDependencies.getDonationsService().cancelSubscription(SignalStore.donationsValues().requireSubscriber().getSubscriberId()).blockingGet();
            if (blockingGet.getExecutionError().isPresent()) {
                Log.w(str, "deleteAccount: failed attempt to cancel subscription");
                consumer.accept(DeleteAccountEvent.CancelSubscriptionFailed.INSTANCE);
                return;
            }
            int status = blockingGet.getStatus();
            if (status == 200) {
                Log.i(str, "deleteAccount: successfully cancelled subscription. Continuing deletion...");
            } else if (status != 404) {
                Log.w(str, "deleteAccount: an unexpected error occurred. " + blockingGet.getStatus());
                consumer.accept(DeleteAccountEvent.CancelSubscriptionFailed.INSTANCE);
                return;
            } else {
                Log.i(str, "deleteAccount: subscription does not exist. Continuing deletion...");
            }
        }
        String str2 = TAG;
        Log.i(str2, "deleteAccount: attempting to leave groups...");
        try {
            GroupDatabase.Reader groups = SignalDatabase.groups().getGroups();
            int i = 0;
            consumer.accept(new DeleteAccountEvent.LeaveGroupsProgress(groups.getCount(), 0));
            Log.i(str2, "deleteAccount: found " + groups.getCount() + " groups to leave.");
            for (GroupDatabase.GroupRecord next = groups.getNext(); next != null; next = groups.getNext()) {
                if (next.getId().isPush() && next.isActive()) {
                    GroupManager.leaveGroup(ApplicationDependencies.getApplication(), next.getId().requirePush());
                    i++;
                    consumer.accept(new DeleteAccountEvent.LeaveGroupsProgress(groups.getCount(), i));
                }
            }
            consumer.accept(DeleteAccountEvent.LeaveGroupsFinished.INSTANCE);
            groups.close();
            String str3 = TAG;
            Log.i(str3, "deleteAccount: successfully left all groups.");
            Log.i(str3, "deleteAccount: attempting to remove pin...");
            try {
                ApplicationDependencies.getKeyBackupService(KbsEnclaves.current()).newPinChangeSession().removePin();
                Log.i(str3, "deleteAccount: successfully removed pin.");
                Log.i(str3, "deleteAccount: attempting to delete account from server...");
                try {
                    ApplicationDependencies.getSignalServiceAccountManager().deleteAccount();
                    Log.i(str3, "deleteAccount: successfully removed account from server");
                    Log.i(str3, "deleteAccount: attempting to delete user data and close process...");
                    if (!ServiceUtil.getActivityManager(ApplicationDependencies.getApplication()).clearApplicationUserData()) {
                        Log.w(str3, "deleteAccount: failed to delete user data");
                        consumer.accept(DeleteAccountEvent.LocalDataDeletionFailed.INSTANCE);
                    }
                } catch (IOException e) {
                    Log.w(TAG, "deleteAccount: failed to delete account from signal service", e);
                    consumer.accept(DeleteAccountEvent.ServerDeletionFailed.INSTANCE);
                }
            } catch (IOException | UnauthenticatedResponseException e2) {
                Log.w(TAG, "deleteAccount: failed to remove PIN", e2);
                consumer.accept(DeleteAccountEvent.PinDeletionFailed.INSTANCE);
            }
        } catch (Exception e3) {
            Log.w(TAG, "deleteAccount: failed to leave groups", e3);
            consumer.accept(DeleteAccountEvent.LeaveGroupsFailed.INSTANCE);
        }
    }

    public static Country getCountryForRegion(String str) {
        return new Country(PhoneNumberFormatter.getRegionDisplayName(str).orElse(""), PhoneNumberUtil.getInstance().getCountryCodeForRegion(str), str);
    }

    /* loaded from: classes4.dex */
    public static class RegionComparator implements Comparator<Country> {
        private final Collator collator;

        RegionComparator() {
            Collator instance = Collator.getInstance();
            this.collator = instance;
            instance.setStrength(0);
        }

        public int compare(Country country, Country country2) {
            return this.collator.compare(country.getDisplayName(), country2.getDisplayName());
        }
    }
}
