package org.thoughtcrime.securesms.delete;

import java.util.Objects;

/* loaded from: classes4.dex */
public final class Country {
    private final int code;
    private final String displayName;
    private final String normalized;
    private final String region;

    public Country(String str, int i, String str2) {
        this.displayName = str;
        this.code = i;
        this.normalized = str.toLowerCase();
        this.region = str2;
    }

    public int getCode() {
        return this.code;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public String getNormalizedDisplayName() {
        return this.normalized;
    }

    public String getRegion() {
        return this.region;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Country.class != obj.getClass()) {
            return false;
        }
        Country country = (Country) obj;
        if (!this.displayName.equals(country.displayName) || this.code != country.code) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.displayName, Integer.valueOf(this.code));
    }
}
