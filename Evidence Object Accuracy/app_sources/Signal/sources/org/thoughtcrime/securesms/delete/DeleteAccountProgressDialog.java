package org.thoughtcrime.securesms.delete;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.delete.DeleteAccountEvent;

/* compiled from: DeleteAccountProgressDialog.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u0011\u001a\u00020\u0010J\u0006\u0010\u0012\u001a\u00020\u0010J\u000e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountProgressDialog;", "", "alertDialog", "Landroidx/appcompat/app/AlertDialog;", "(Landroidx/appcompat/app/AlertDialog;)V", "message", "Landroid/widget/TextView;", "getMessage", "()Landroid/widget/TextView;", "progressBar", "Landroid/widget/ProgressBar;", "getProgressBar", "()Landroid/widget/ProgressBar;", MultiselectForwardFragment.DIALOG_TITLE, "getTitle", "dismiss", "", "presentCancelingSubscription", "presentDeletingAccount", "presentLeavingGroups", "leaveGroupsProgress", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsProgress;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DeleteAccountProgressDialog {
    public static final Companion Companion = new Companion(null);
    private final AlertDialog alertDialog;
    private final TextView message;
    private final ProgressBar progressBar;
    private final TextView title;

    public /* synthetic */ DeleteAccountProgressDialog(AlertDialog alertDialog, DefaultConstructorMarker defaultConstructorMarker) {
        this(alertDialog);
    }

    @JvmStatic
    public static final DeleteAccountProgressDialog show(Context context) {
        return Companion.show(context);
    }

    private DeleteAccountProgressDialog(AlertDialog alertDialog) {
        this.alertDialog = alertDialog;
        View findViewById = alertDialog.findViewById(R.id.delete_account_progress_dialog_title);
        Intrinsics.checkNotNull(findViewById);
        this.title = (TextView) findViewById;
        View findViewById2 = alertDialog.findViewById(R.id.delete_account_progress_dialog_message);
        Intrinsics.checkNotNull(findViewById2);
        this.message = (TextView) findViewById2;
        View findViewById3 = alertDialog.findViewById(R.id.delete_account_progress_dialog_spinner);
        Intrinsics.checkNotNull(findViewById3);
        this.progressBar = (ProgressBar) findViewById3;
    }

    public final TextView getTitle() {
        return this.title;
    }

    public final TextView getMessage() {
        return this.message;
    }

    public final ProgressBar getProgressBar() {
        return this.progressBar;
    }

    public final void presentCancelingSubscription() {
        this.title.setText(R.string.DeleteAccountFragment__deleting_account);
        this.message.setText(R.string.DeleteAccountFragment__canceling_your_subscription);
        this.progressBar.setIndeterminate(true);
    }

    public final void presentLeavingGroups(DeleteAccountEvent.LeaveGroupsProgress leaveGroupsProgress) {
        Intrinsics.checkNotNullParameter(leaveGroupsProgress, "leaveGroupsProgress");
        this.title.setText(R.string.DeleteAccountFragment__leaving_groups);
        this.message.setText(R.string.DeleteAccountFragment__depending_on_the_number_of_groups);
        this.progressBar.setIndeterminate(false);
        this.progressBar.setMax(leaveGroupsProgress.getTotalCount());
        this.progressBar.setProgress(leaveGroupsProgress.getLeaveCount());
    }

    public final void presentDeletingAccount() {
        this.title.setText(R.string.DeleteAccountFragment__deleting_account);
        this.message.setText(R.string.DeleteAccountFragment__deleting_all_user_data_and_resetting);
        this.progressBar.setIndeterminate(true);
    }

    public final void dismiss() {
        this.alertDialog.dismiss();
    }

    /* compiled from: DeleteAccountProgressDialog.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountProgressDialog$Companion;", "", "()V", "show", "Lorg/thoughtcrime/securesms/delete/DeleteAccountProgressDialog;", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final DeleteAccountProgressDialog show(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            AlertDialog show = new MaterialAlertDialogBuilder(context).setView(R.layout.delete_account_progress_dialog).show();
            Intrinsics.checkNotNullExpressionValue(show, "MaterialAlertDialogBuild…dialog)\n          .show()");
            return new DeleteAccountProgressDialog(show, null);
        }
    }
}
