package org.thoughtcrime.securesms.delete;

import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerAdapter;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* loaded from: classes4.dex */
public class DeleteAccountCountryPickerFragment extends DialogFragment {
    private DeleteAccountViewModel viewModel;

    public static void show(FragmentManager fragmentManager) {
        new DeleteAccountCountryPickerFragment().show(fragmentManager, (String) null);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.Signal_DayNight_Dialog_FullScreen);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.delete_account_country_picker, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        DeleteAccountCountryPickerAdapter deleteAccountCountryPickerAdapter = new DeleteAccountCountryPickerAdapter(new DeleteAccountCountryPickerAdapter.Callback() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerAdapter.Callback
            public final void onItemSelected(Country country) {
                DeleteAccountCountryPickerFragment.$r8$lambda$K5B6klzCOI39Wnk9LbvQIaNGh9k(DeleteAccountCountryPickerFragment.this, country);
            }
        });
        ((RecyclerView) view.findViewById(R.id.delete_account_country_picker_recycler)).setAdapter(deleteAccountCountryPickerAdapter);
        ((Toolbar) view.findViewById(R.id.delete_account_country_picker_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeleteAccountCountryPickerFragment.$r8$lambda$_PuW2rn6muv1eO_Q6dbTpl1JTBo(DeleteAccountCountryPickerFragment.this, view2);
            }
        });
        DeleteAccountViewModel deleteAccountViewModel = (DeleteAccountViewModel) ViewModelProviders.of(requireActivity()).get(DeleteAccountViewModel.class);
        this.viewModel = deleteAccountViewModel;
        deleteAccountViewModel.getFilteredCountries().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeleteAccountCountryPickerAdapter.this.submitList((List) obj);
            }
        });
        ((EditText) view.findViewById(R.id.delete_account_country_picker_filter)).addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountCountryPickerFragment$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                DeleteAccountCountryPickerFragment.m1750$r8$lambda$MD_45G39hZe4fPw18IH6uRpUR8(DeleteAccountCountryPickerFragment.this, (Editable) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        dismiss();
    }

    public void onQueryChanged(Editable editable) {
        this.viewModel.onQueryChanged(editable.toString());
    }

    public void onCountryPicked(Country country) {
        this.viewModel.onRegionSelected(country.getRegion());
        dismissAllowingStateLoss();
    }
}
