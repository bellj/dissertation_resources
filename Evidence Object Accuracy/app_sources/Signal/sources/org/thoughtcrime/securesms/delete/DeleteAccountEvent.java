package org.thoughtcrime.securesms.delete;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: DeleteAccountEvent.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\r\u0007\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\f\u0014\u0015\u0016\u0017\u0018\u0019\u001a\u001b\u001c\u001d\u001e\u001f¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "", "type", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$Type;", "(Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$Type;)V", "getType", "()Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$Type;", "CancelSubscriptionFailed", "CancelingSubscription", "ConfirmDeletion", "LeaveGroupsFailed", "LeaveGroupsFinished", "LeaveGroupsProgress", "LocalDataDeletionFailed", "NoCountryCode", "NoNationalNumber", "NotAMatch", "PinDeletionFailed", "ServerDeletionFailed", "Type", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$NoCountryCode;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$NoNationalNumber;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$NotAMatch;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$ConfirmDeletion;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$PinDeletionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$CancelSubscriptionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$ServerDeletionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LocalDataDeletionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsFinished;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$CancelingSubscription;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsProgress;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DeleteAccountEvent {
    private final Type type;

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u000e\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$Type;", "", "(Ljava/lang/String;I)V", "NO_COUNTRY_CODE", "NO_NATIONAL_NUMBER", "NOT_A_MATCH", "CONFIRM_DELETION", "LEAVE_GROUPS_FAILED", "PIN_DELETION_FAILED", "CANCELING_SUBSCRIPTION", "CANCEL_SUBSCRIPTION_FAILED", "SERVER_DELETION_FAILED", "LOCAL_DATA_DELETION_FAILED", "LEAVE_GROUPS_PROGRESS", "LEAVE_GROUPS_FINISHED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Type {
        NO_COUNTRY_CODE,
        NO_NATIONAL_NUMBER,
        NOT_A_MATCH,
        CONFIRM_DELETION,
        LEAVE_GROUPS_FAILED,
        PIN_DELETION_FAILED,
        CANCELING_SUBSCRIPTION,
        CANCEL_SUBSCRIPTION_FAILED,
        SERVER_DELETION_FAILED,
        LOCAL_DATA_DELETION_FAILED,
        LEAVE_GROUPS_PROGRESS,
        LEAVE_GROUPS_FINISHED
    }

    public /* synthetic */ DeleteAccountEvent(Type type, DefaultConstructorMarker defaultConstructorMarker) {
        this(type);
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$NoCountryCode;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NoCountryCode extends DeleteAccountEvent {
        public static final NoCountryCode INSTANCE = new NoCountryCode();

        private NoCountryCode() {
            super(Type.NO_COUNTRY_CODE, null);
        }
    }

    private DeleteAccountEvent(Type type) {
        this.type = type;
    }

    public final Type getType() {
        return this.type;
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$NoNationalNumber;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NoNationalNumber extends DeleteAccountEvent {
        public static final NoNationalNumber INSTANCE = new NoNationalNumber();

        private NoNationalNumber() {
            super(Type.NO_NATIONAL_NUMBER, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$NotAMatch;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotAMatch extends DeleteAccountEvent {
        public static final NotAMatch INSTANCE = new NotAMatch();

        private NotAMatch() {
            super(Type.NOT_A_MATCH, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$ConfirmDeletion;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ConfirmDeletion extends DeleteAccountEvent {
        public static final ConfirmDeletion INSTANCE = new ConfirmDeletion();

        private ConfirmDeletion() {
            super(Type.CONFIRM_DELETION, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$PinDeletionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PinDeletionFailed extends DeleteAccountEvent {
        public static final PinDeletionFailed INSTANCE = new PinDeletionFailed();

        private PinDeletionFailed() {
            super(Type.PIN_DELETION_FAILED, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$CancelSubscriptionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CancelSubscriptionFailed extends DeleteAccountEvent {
        public static final CancelSubscriptionFailed INSTANCE = new CancelSubscriptionFailed();

        private CancelSubscriptionFailed() {
            super(Type.CANCEL_SUBSCRIPTION_FAILED, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LeaveGroupsFailed extends DeleteAccountEvent {
        public static final LeaveGroupsFailed INSTANCE = new LeaveGroupsFailed();

        private LeaveGroupsFailed() {
            super(Type.LEAVE_GROUPS_FAILED, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$ServerDeletionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ServerDeletionFailed extends DeleteAccountEvent {
        public static final ServerDeletionFailed INSTANCE = new ServerDeletionFailed();

        private ServerDeletionFailed() {
            super(Type.SERVER_DELETION_FAILED, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LocalDataDeletionFailed;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LocalDataDeletionFailed extends DeleteAccountEvent {
        public static final LocalDataDeletionFailed INSTANCE = new LocalDataDeletionFailed();

        private LocalDataDeletionFailed() {
            super(Type.LOCAL_DATA_DELETION_FAILED, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsFinished;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LeaveGroupsFinished extends DeleteAccountEvent {
        public static final LeaveGroupsFinished INSTANCE = new LeaveGroupsFinished();

        private LeaveGroupsFinished() {
            super(Type.LEAVE_GROUPS_FINISHED, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$CancelingSubscription;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CancelingSubscription extends DeleteAccountEvent {
        public static final CancelingSubscription INSTANCE = new CancelingSubscription();

        private CancelingSubscription() {
            super(Type.CANCELING_SUBSCRIPTION, null);
        }
    }

    /* compiled from: DeleteAccountEvent.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fHÖ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent$LeaveGroupsProgress;", "Lorg/thoughtcrime/securesms/delete/DeleteAccountEvent;", "totalCount", "", "leaveCount", "(II)V", "getLeaveCount", "()I", "getTotalCount", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LeaveGroupsProgress extends DeleteAccountEvent {
        private final int leaveCount;
        private final int totalCount;

        public static /* synthetic */ LeaveGroupsProgress copy$default(LeaveGroupsProgress leaveGroupsProgress, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = leaveGroupsProgress.totalCount;
            }
            if ((i3 & 2) != 0) {
                i2 = leaveGroupsProgress.leaveCount;
            }
            return leaveGroupsProgress.copy(i, i2);
        }

        public final int component1() {
            return this.totalCount;
        }

        public final int component2() {
            return this.leaveCount;
        }

        public final LeaveGroupsProgress copy(int i, int i2) {
            return new LeaveGroupsProgress(i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof LeaveGroupsProgress)) {
                return false;
            }
            LeaveGroupsProgress leaveGroupsProgress = (LeaveGroupsProgress) obj;
            return this.totalCount == leaveGroupsProgress.totalCount && this.leaveCount == leaveGroupsProgress.leaveCount;
        }

        public int hashCode() {
            return (this.totalCount * 31) + this.leaveCount;
        }

        public String toString() {
            return "LeaveGroupsProgress(totalCount=" + this.totalCount + ", leaveCount=" + this.leaveCount + ')';
        }

        public final int getTotalCount() {
            return this.totalCount;
        }

        public final int getLeaveCount() {
            return this.leaveCount;
        }

        public LeaveGroupsProgress(int i, int i2) {
            super(Type.LEAVE_GROUPS_PROGRESS, null);
            this.totalCount = i;
            this.leaveCount = i2;
        }
    }
}
