package org.thoughtcrime.securesms.delete;

import android.content.DialogInterface;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DeleteAccountFragment$$ExternalSyntheticLambda2 implements DialogInterface.OnClickListener {
    public final /* synthetic */ DeleteAccountFragment f$0;

    public /* synthetic */ DeleteAccountFragment$$ExternalSyntheticLambda2(DeleteAccountFragment deleteAccountFragment) {
        this.f$0 = deleteAccountFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.handleDeleteAccountConfirmation(dialogInterface, i);
    }
}
