package org.thoughtcrime.securesms.delete;

import android.text.TextUtils;
import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber$PhoneNumber;
import j$.util.Optional;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.delete.DeleteAccountEvent;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.Balance;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.whispersystems.signalservice.api.payments.FormatterOptions;
import org.whispersystems.signalservice.api.payments.Money;

/* loaded from: classes4.dex */
public class DeleteAccountViewModel extends ViewModel {
    private final List<Country> allCountries;
    private final LiveData<String> countryDisplayName;
    private final SingleLiveEvent<DeleteAccountEvent> events;
    private final LiveData<List<Country>> filteredCountries;
    private final MutableLiveData<Long> nationalNumber = new MutableLiveData<>();
    private final MutableLiveData<String> query;
    private final MutableLiveData<String> regionCode;
    private final DeleteAccountRepository repository;
    private final LiveData<Optional<String>> walletBalance;

    public DeleteAccountViewModel(DeleteAccountRepository deleteAccountRepository) {
        this.repository = deleteAccountRepository;
        this.allCountries = deleteAccountRepository.getAllCountries();
        DefaultValueLiveData defaultValueLiveData = new DefaultValueLiveData("ZZ");
        this.regionCode = defaultValueLiveData;
        DefaultValueLiveData defaultValueLiveData2 = new DefaultValueLiveData("");
        this.query = defaultValueLiveData2;
        this.countryDisplayName = Transformations.map(defaultValueLiveData, new Function() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return DeleteAccountRepository.this.getRegionDisplayName((String) obj);
            }
        });
        this.filteredCountries = Transformations.map(defaultValueLiveData2, new Function() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountViewModel$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return DeleteAccountViewModel.this.lambda$new$1((String) obj);
            }
        });
        this.events = new SingleLiveEvent<>();
        this.walletBalance = Transformations.map(SignalStore.paymentsValues().liveMobileCoinBalance(), new Function() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountViewModel$$ExternalSyntheticLambda4
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return DeleteAccountViewModel.getFormattedWalletBalance((Balance) obj);
            }
        });
    }

    public /* synthetic */ List lambda$new$1(String str) {
        return Stream.of(this.allCountries).filter(new Predicate(str) { // from class: org.thoughtcrime.securesms.delete.DeleteAccountViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return DeleteAccountViewModel.isMatch(this.f$0, (Country) obj);
            }
        }).toList();
    }

    public LiveData<Optional<String>> getWalletBalance() {
        return this.walletBalance;
    }

    public LiveData<List<Country>> getFilteredCountries() {
        return this.filteredCountries;
    }

    public LiveData<String> getCountryDisplayName() {
        return Transformations.distinctUntilChanged(this.countryDisplayName);
    }

    public LiveData<String> getRegionCode() {
        return Transformations.distinctUntilChanged(this.regionCode);
    }

    public SingleLiveEvent<DeleteAccountEvent> getEvents() {
        return this.events;
    }

    public Long getNationalNumber() {
        return this.nationalNumber.getValue();
    }

    public void onQueryChanged(String str) {
        this.query.setValue(str.toLowerCase());
    }

    public void deleteAccount() {
        DeleteAccountRepository deleteAccountRepository = this.repository;
        SingleLiveEvent<DeleteAccountEvent> singleLiveEvent = this.events;
        Objects.requireNonNull(singleLiveEvent);
        deleteAccountRepository.deleteAccount(new Consumer() { // from class: org.thoughtcrime.securesms.delete.DeleteAccountViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                SingleLiveEvent.this.postValue((DeleteAccountEvent) obj);
            }
        });
    }

    public void submit() {
        String value = this.regionCode.getValue();
        Integer valueOf = value != null ? Integer.valueOf(this.repository.getRegionCountryCode(value)) : null;
        Long value2 = this.nationalNumber.getValue();
        if (valueOf == null || valueOf.intValue() == 0) {
            this.events.setValue(DeleteAccountEvent.NoCountryCode.INSTANCE);
        } else if (value2 == null) {
            this.events.setValue(DeleteAccountEvent.NoNationalNumber.INSTANCE);
        } else {
            Phonenumber$PhoneNumber phonenumber$PhoneNumber = new Phonenumber$PhoneNumber();
            phonenumber$PhoneNumber.setCountryCode(valueOf.intValue());
            phonenumber$PhoneNumber.setNationalNumber(value2.longValue());
            if (PhoneNumberUtil.getInstance().isNumberMatch(phonenumber$PhoneNumber, Recipient.self().requireE164()) == PhoneNumberUtil.MatchType.EXACT_MATCH) {
                this.events.setValue(DeleteAccountEvent.ConfirmDeletion.INSTANCE);
            } else {
                this.events.setValue(DeleteAccountEvent.NotAMatch.INSTANCE);
            }
        }
    }

    public void onCountrySelected(int i) {
        if (!PhoneNumberUtil.getInstance().getRegionCodesForCountryCode(i).contains(this.regionCode.getValue())) {
            this.regionCode.setValue(PhoneNumberUtil.getInstance().getRegionCodeForCountryCode(i));
        }
    }

    public void onRegionSelected(String str) {
        this.regionCode.setValue(str);
    }

    public void setNationalNumber(long j) {
        this.nationalNumber.setValue(Long.valueOf(j));
        try {
            String regionCodeForNumber = PhoneNumberUtil.getInstance().getRegionCodeForNumber(PhoneNumberUtil.getInstance().parse(String.valueOf(j), this.regionCode.getValue()));
            if (regionCodeForNumber != null) {
                this.regionCode.setValue(regionCodeForNumber);
            }
        } catch (NumberParseException unused) {
        }
    }

    public static Optional<String> getFormattedWalletBalance(Balance balance) {
        Money fullAmount = balance.getFullAmount();
        if (fullAmount.isPositive()) {
            return Optional.of(fullAmount.toString(FormatterOptions.defaults()));
        }
        return Optional.empty();
    }

    public static boolean isMatch(String str, Country country) {
        if (TextUtils.isEmpty(str)) {
            return true;
        }
        return country.getNormalizedDisplayName().contains(str.toLowerCase());
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DeleteAccountRepository repository;

        public Factory(DeleteAccountRepository deleteAccountRepository) {
            this.repository = deleteAccountRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new DeleteAccountViewModel(this.repository));
        }
    }
}
