package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class CustomChatColorCreatorFragmentArgs {
    private final HashMap arguments;

    private CustomChatColorCreatorFragmentArgs() {
        this.arguments = new HashMap();
    }

    private CustomChatColorCreatorFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static CustomChatColorCreatorFragmentArgs fromBundle(Bundle bundle) {
        CustomChatColorCreatorFragmentArgs customChatColorCreatorFragmentArgs = new CustomChatColorCreatorFragmentArgs();
        bundle.setClassLoader(CustomChatColorCreatorFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("recipient_id")) {
            throw new IllegalArgumentException("Required argument \"recipient_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(RecipientId.class) || Serializable.class.isAssignableFrom(RecipientId.class)) {
            customChatColorCreatorFragmentArgs.arguments.put("recipient_id", (RecipientId) bundle.get("recipient_id"));
            if (bundle.containsKey("start_page")) {
                customChatColorCreatorFragmentArgs.arguments.put("start_page", Integer.valueOf(bundle.getInt("start_page")));
                if (bundle.containsKey("chat_color_id")) {
                    customChatColorCreatorFragmentArgs.arguments.put("chat_color_id", Long.valueOf(bundle.getLong("chat_color_id")));
                } else {
                    customChatColorCreatorFragmentArgs.arguments.put("chat_color_id", 0L);
                }
                return customChatColorCreatorFragmentArgs;
            }
            throw new IllegalArgumentException("Required argument \"start_page\" is missing and does not have an android:defaultValue");
        } else {
            throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public RecipientId getRecipientId() {
        return (RecipientId) this.arguments.get("recipient_id");
    }

    public int getStartPage() {
        return ((Integer) this.arguments.get("start_page")).intValue();
    }

    public long getChatColorId() {
        return ((Long) this.arguments.get("chat_color_id")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("recipient_id")) {
            RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
            if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
            } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
            } else {
                throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("start_page")) {
            bundle.putInt("start_page", ((Integer) this.arguments.get("start_page")).intValue());
        }
        if (this.arguments.containsKey("chat_color_id")) {
            bundle.putLong("chat_color_id", ((Long) this.arguments.get("chat_color_id")).longValue());
        } else {
            bundle.putLong("chat_color_id", 0);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        CustomChatColorCreatorFragmentArgs customChatColorCreatorFragmentArgs = (CustomChatColorCreatorFragmentArgs) obj;
        if (this.arguments.containsKey("recipient_id") != customChatColorCreatorFragmentArgs.arguments.containsKey("recipient_id")) {
            return false;
        }
        if (getRecipientId() == null ? customChatColorCreatorFragmentArgs.getRecipientId() == null : getRecipientId().equals(customChatColorCreatorFragmentArgs.getRecipientId())) {
            return this.arguments.containsKey("start_page") == customChatColorCreatorFragmentArgs.arguments.containsKey("start_page") && getStartPage() == customChatColorCreatorFragmentArgs.getStartPage() && this.arguments.containsKey("chat_color_id") == customChatColorCreatorFragmentArgs.arguments.containsKey("chat_color_id") && getChatColorId() == customChatColorCreatorFragmentArgs.getChatColorId();
        }
        return false;
    }

    public int hashCode() {
        return (((((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + getStartPage()) * 31) + ((int) (getChatColorId() ^ (getChatColorId() >>> 32)));
    }

    public String toString() {
        return "CustomChatColorCreatorFragmentArgs{recipientId=" + getRecipientId() + ", startPage=" + getStartPage() + ", chatColorId=" + getChatColorId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(CustomChatColorCreatorFragmentArgs customChatColorCreatorFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(customChatColorCreatorFragmentArgs.arguments);
        }

        public Builder(RecipientId recipientId, int i) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("recipient_id", recipientId);
            hashMap.put("start_page", Integer.valueOf(i));
        }

        public CustomChatColorCreatorFragmentArgs build() {
            return new CustomChatColorCreatorFragmentArgs(this.arguments);
        }

        public Builder setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        public Builder setStartPage(int i) {
            this.arguments.put("start_page", Integer.valueOf(i));
            return this;
        }

        public Builder setChatColorId(long j) {
            this.arguments.put("chat_color_id", Long.valueOf(j));
            return this;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public int getStartPage() {
            return ((Integer) this.arguments.get("start_page")).intValue();
        }

        public long getChatColorId() {
            return ((Long) this.arguments.get("chat_color_id")).longValue();
        }
    }
}
