package org.thoughtcrime.securesms.conversation.colors.ui;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class ChatColorSelectionFragmentDirections {
    private ChatColorSelectionFragmentDirections() {
    }

    public static ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment actionChatColorSelectionFragmentToCustomChatColorCreatorFragment(RecipientId recipientId, int i) {
        return new ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment(recipientId, i);
    }

    /* loaded from: classes4.dex */
    public static class ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_chatColorSelectionFragment_to_customChatColorCreatorFragment;
        }

        private ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment(RecipientId recipientId, int i) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("recipient_id", recipientId);
            hashMap.put("start_page", Integer.valueOf(i));
        }

        public ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        public ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment setStartPage(int i) {
            this.arguments.put("start_page", Integer.valueOf(i));
            return this;
        }

        public ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment setChatColorId(long j) {
            this.arguments.put("chat_color_id", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("recipient_id")) {
                RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
                if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                    bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
                } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                    bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
                } else {
                    throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("start_page")) {
                bundle.putInt("start_page", ((Integer) this.arguments.get("start_page")).intValue());
            }
            if (this.arguments.containsKey("chat_color_id")) {
                bundle.putLong("chat_color_id", ((Long) this.arguments.get("chat_color_id")).longValue());
            } else {
                bundle.putLong("chat_color_id", 0);
            }
            return bundle;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public int getStartPage() {
            return ((Integer) this.arguments.get("start_page")).intValue();
        }

        public long getChatColorId() {
            return ((Long) this.arguments.get("chat_color_id")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment actionChatColorSelectionFragmentToCustomChatColorCreatorFragment = (ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment) obj;
            if (this.arguments.containsKey("recipient_id") != actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.arguments.containsKey("recipient_id")) {
                return false;
            }
            if (getRecipientId() == null ? actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.getRecipientId() == null : getRecipientId().equals(actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.getRecipientId())) {
                return this.arguments.containsKey("start_page") == actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.arguments.containsKey("start_page") && getStartPage() == actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.getStartPage() && this.arguments.containsKey("chat_color_id") == actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.arguments.containsKey("chat_color_id") && getChatColorId() == actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.getChatColorId() && getActionId() == actionChatColorSelectionFragmentToCustomChatColorCreatorFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + getStartPage()) * 31) + ((int) (getChatColorId() ^ (getChatColorId() >>> 32)))) * 31) + getActionId();
        }

        public String toString() {
            return "ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment(actionId=" + getActionId() + "){recipientId=" + getRecipientId() + ", startPage=" + getStartPage() + ", chatColorId=" + getChatColorId() + "}";
        }
    }
}
