package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import java.util.EnumMap;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: CustomChatColorCreatorState.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B;\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007\u0012\u0006\u0010\n\u001a\u00020\b\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0019\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u0015\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007HÆ\u0003J\t\u0010\u001b\u001a\u00020\bHÆ\u0003J\t\u0010\u001c\u001a\u00020\fHÆ\u0003JI\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0014\b\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u00072\b\b\u0002\u0010\n\u001a\u00020\b2\b\b\u0002\u0010\u000b\u001a\u00020\fHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\u00032\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\t\u0010\"\u001a\u00020#HÖ\u0001R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\n\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorState;", "", "loading", "", "wallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "sliderStates", "Ljava/util/EnumMap;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/ColorSlidersState;", "selectedEdge", "degrees", "", "(ZLorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;Ljava/util/EnumMap;Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;F)V", "getDegrees", "()F", "getLoading", "()Z", "getSelectedEdge", "()Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;", "getSliderStates", "()Ljava/util/EnumMap;", "getWallpaper", "()Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorCreatorState {
    private final float degrees;
    private final boolean loading;
    private final CustomChatColorEdge selectedEdge;
    private final EnumMap<CustomChatColorEdge, ColorSlidersState> sliderStates;
    private final ChatWallpaper wallpaper;

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ CustomChatColorCreatorState copy$default(CustomChatColorCreatorState customChatColorCreatorState, boolean z, ChatWallpaper chatWallpaper, EnumMap enumMap, CustomChatColorEdge customChatColorEdge, float f, int i, Object obj) {
        if ((i & 1) != 0) {
            z = customChatColorCreatorState.loading;
        }
        if ((i & 2) != 0) {
            chatWallpaper = customChatColorCreatorState.wallpaper;
        }
        if ((i & 4) != 0) {
            enumMap = customChatColorCreatorState.sliderStates;
        }
        if ((i & 8) != 0) {
            customChatColorEdge = customChatColorCreatorState.selectedEdge;
        }
        if ((i & 16) != 0) {
            f = customChatColorCreatorState.degrees;
        }
        return customChatColorCreatorState.copy(z, chatWallpaper, enumMap, customChatColorEdge, f);
    }

    public final boolean component1() {
        return this.loading;
    }

    public final ChatWallpaper component2() {
        return this.wallpaper;
    }

    public final EnumMap<CustomChatColorEdge, ColorSlidersState> component3() {
        return this.sliderStates;
    }

    public final CustomChatColorEdge component4() {
        return this.selectedEdge;
    }

    public final float component5() {
        return this.degrees;
    }

    public final CustomChatColorCreatorState copy(boolean z, ChatWallpaper chatWallpaper, EnumMap<CustomChatColorEdge, ColorSlidersState> enumMap, CustomChatColorEdge customChatColorEdge, float f) {
        Intrinsics.checkNotNullParameter(enumMap, "sliderStates");
        Intrinsics.checkNotNullParameter(customChatColorEdge, "selectedEdge");
        return new CustomChatColorCreatorState(z, chatWallpaper, enumMap, customChatColorEdge, f);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CustomChatColorCreatorState)) {
            return false;
        }
        CustomChatColorCreatorState customChatColorCreatorState = (CustomChatColorCreatorState) obj;
        return this.loading == customChatColorCreatorState.loading && Intrinsics.areEqual(this.wallpaper, customChatColorCreatorState.wallpaper) && Intrinsics.areEqual(this.sliderStates, customChatColorCreatorState.sliderStates) && this.selectedEdge == customChatColorCreatorState.selectedEdge && Intrinsics.areEqual(Float.valueOf(this.degrees), Float.valueOf(customChatColorCreatorState.degrees));
    }

    public int hashCode() {
        boolean z = this.loading;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = i * 31;
        ChatWallpaper chatWallpaper = this.wallpaper;
        return ((((((i4 + (chatWallpaper == null ? 0 : chatWallpaper.hashCode())) * 31) + this.sliderStates.hashCode()) * 31) + this.selectedEdge.hashCode()) * 31) + Float.floatToIntBits(this.degrees);
    }

    public String toString() {
        return "CustomChatColorCreatorState(loading=" + this.loading + ", wallpaper=" + this.wallpaper + ", sliderStates=" + this.sliderStates + ", selectedEdge=" + this.selectedEdge + ", degrees=" + this.degrees + ')';
    }

    public CustomChatColorCreatorState(boolean z, ChatWallpaper chatWallpaper, EnumMap<CustomChatColorEdge, ColorSlidersState> enumMap, CustomChatColorEdge customChatColorEdge, float f) {
        Intrinsics.checkNotNullParameter(enumMap, "sliderStates");
        Intrinsics.checkNotNullParameter(customChatColorEdge, "selectedEdge");
        this.loading = z;
        this.wallpaper = chatWallpaper;
        this.sliderStates = enumMap;
        this.selectedEdge = customChatColorEdge;
        this.degrees = f;
    }

    public final boolean getLoading() {
        return this.loading;
    }

    public final ChatWallpaper getWallpaper() {
        return this.wallpaper;
    }

    public final EnumMap<CustomChatColorEdge, ColorSlidersState> getSliderStates() {
        return this.sliderStates;
    }

    public final CustomChatColorEdge getSelectedEdge() {
        return this.selectedEdge;
    }

    public final float getDegrees() {
        return this.degrees;
    }
}
