package org.thoughtcrime.securesms.conversation.colors;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Supplier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.MapUtil;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;

/* compiled from: NameColors.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0004J@\u0010\b\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\n0\t2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000b0\t2\u0018\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0004J6\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\t2\u0006\u0010\u0010\u001a\u00020\u00052\u0018\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0004H\u0002¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/NameColors;", "", "()V", "createSessionMembersCache", "", "Lorg/thoughtcrime/securesms/groups/GroupId;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getNameColorsMapLiveData", "Landroidx/lifecycle/LiveData;", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "recipientId", "sessionMemberCache", "getSessionGroupRecipients", "groupId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NameColors {
    public static final NameColors INSTANCE = new NameColors();

    private NameColors() {
    }

    public final Map<GroupId, Set<Recipient>> createSessionMembersCache() {
        return new LinkedHashMap();
    }

    /* renamed from: getNameColorsMapLiveData$lambda-0 */
    public static final LiveData m1479getNameColorsMapLiveData$lambda0(RecipientId recipientId) {
        Intrinsics.checkNotNull(recipientId);
        return Recipient.live(recipientId).getLiveData();
    }

    public final LiveData<Map<RecipientId, NameColor>> getNameColorsMapLiveData(LiveData<RecipientId> liveData, Map<GroupId, Set<Recipient>> map) {
        Intrinsics.checkNotNullParameter(liveData, "recipientId");
        Intrinsics.checkNotNullParameter(map, "sessionMemberCache");
        LiveData switchMap = Transformations.switchMap(liveData, new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$1heqXCD5kdepBTTaoOLPkzBC3WE((RecipientId) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(switchMap, "switchMap(recipientId) {…ient.live(r!!).liveData }");
        LiveData map2 = Transformations.map(switchMap, new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda4
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$XVnVnkpMqUE8BMpLyjJIxD1tAXw((Recipient) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map2, "map(recipient) { obj: Recipient -> obj.groupId }");
        LiveData switchMap2 = Transformations.switchMap(map2, new Function(map) { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda5
            public final /* synthetic */ Map f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return NameColors.m1477$r8$lambda$fCuJxuHxoGbKmApRKDRNtjvt88(NameColors.this, this.f$1, (Optional) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(switchMap2, "switchMap(group) { g: Op…eData(emptySet()) }\n    }");
        LiveData<Map<RecipientId, NameColor>> map3 = Transformations.map(switchMap2, new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda6
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$hN2hnjgGo3O_dvzoJlW2b9jX1ds((Set) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map3, "map(groupMembers) { memb…     }\n      colors\n    }");
        return map3;
    }

    /* renamed from: getNameColorsMapLiveData$lambda-1 */
    public static final Optional m1480getNameColorsMapLiveData$lambda1(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "obj");
        return recipient.getGroupId();
    }

    /* renamed from: getNameColorsMapLiveData$lambda-4 */
    public static final LiveData m1481getNameColorsMapLiveData$lambda4(NameColors nameColors, Map map, Optional optional) {
        Intrinsics.checkNotNullParameter(nameColors, "this$0");
        Intrinsics.checkNotNullParameter(map, "$sessionMemberCache");
        Intrinsics.checkNotNullParameter(optional, "g");
        return (LiveData) optional.map(new j$.util.function.Function(map) { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda1
            public final /* synthetic */ Map f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$AEssUzfLZ3N_sz_NE5uoEQ07mYg(NameColors.this, this.f$1, (GroupId) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElseGet(new Supplier() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda2
            @Override // j$.util.function.Supplier
            public final Object get() {
                return NameColors.$r8$lambda$JIXGqXFUZtHfnXynvjBc69CHaQE();
            }
        });
    }

    /* renamed from: getNameColorsMapLiveData$lambda-4$lambda-2 */
    public static final LiveData m1482getNameColorsMapLiveData$lambda4$lambda2(NameColors nameColors, Map map, GroupId groupId) {
        Intrinsics.checkNotNullParameter(nameColors, "this$0");
        Intrinsics.checkNotNullParameter(map, "$sessionMemberCache");
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        return nameColors.getSessionGroupRecipients(groupId, map);
    }

    /* renamed from: getNameColorsMapLiveData$lambda-4$lambda-3 */
    public static final LiveData m1483getNameColorsMapLiveData$lambda4$lambda3() {
        return new DefaultValueLiveData(SetsKt__SetsKt.emptySet());
    }

    /* renamed from: getNameColorsMapLiveData$lambda-7 */
    public static final Map m1484getNameColorsMapLiveData$lambda7(Set set) {
        List list = Stream.of(set).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return NameColors.m1476$r8$lambda$TZ_HtESQYlXCMeZNv762kQdzFI((Recipient) obj);
            }
        }).sortBy(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$oXtBWgICzkze3sBIdmplCunJwVs((Recipient) obj);
            }
        }).toList();
        List<NameColor> all = ChatColorsPalette.Names.getAll();
        HashMap hashMap = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            RecipientId id = ((Recipient) list.get(i)).getId();
            Intrinsics.checkNotNullExpressionValue(id, "sorted[i].id");
            hashMap.put(id, all.get(i % all.size()));
        }
        return hashMap;
    }

    /* renamed from: getNameColorsMapLiveData$lambda-7$lambda-5 */
    public static final boolean m1485getNameColorsMapLiveData$lambda7$lambda5(Recipient recipient) {
        return !Intrinsics.areEqual(recipient, Recipient.self());
    }

    /* renamed from: getNameColorsMapLiveData$lambda-7$lambda-6 */
    public static final String m1486getNameColorsMapLiveData$lambda7$lambda6(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, "obj");
        return recipient.requireStringId();
    }

    private final LiveData<Set<Recipient>> getSessionGroupRecipients(GroupId groupId, Map<GroupId, Set<Recipient>> map) {
        LiveData map2 = Transformations.map(new LiveGroup(groupId).getFullMembers(), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda9
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$LhqnI7mCmfyEuqqvXKx1jTv48co((List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map2, "map(\n      LiveGroup(gro…}\n        .toList()\n    }");
        LiveData<Set<Recipient>> map3 = Transformations.map(map2, new androidx.arch.core.util.Function(map, groupId) { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda10
            public final /* synthetic */ Map f$0;
            public final /* synthetic */ GroupId f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return NameColors.$r8$lambda$gvZdFs6VgJEewLwnfpNbURonDl0(this.f$0, this.f$1, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map3, "map(fullMembers) { curre…      cachedMembers\n    }");
        return map3;
    }

    /* renamed from: getSessionGroupRecipients$lambda-9 */
    public static final List m1488getSessionGroupRecipients$lambda9(List list) {
        return Stream.of(list).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.conversation.colors.NameColors$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NameColors.m1478$r8$lambda$gV666oOCKXM0QViSpoumZKe37M((GroupMemberEntry.FullMember) obj);
            }
        }).toList();
    }

    /* renamed from: getSessionGroupRecipients$lambda-9$lambda-8 */
    public static final Recipient m1489getSessionGroupRecipients$lambda9$lambda8(GroupMemberEntry.FullMember fullMember) {
        return fullMember.getMember();
    }

    /* renamed from: getSessionGroupRecipients$lambda-10 */
    public static final Set m1487getSessionGroupRecipients$lambda10(Map map, GroupId groupId, List list) {
        Intrinsics.checkNotNullParameter(map, "$sessionMemberCache");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Object orDefault = MapUtil.getOrDefault(map, groupId, new HashSet());
        Intrinsics.checkNotNullExpressionValue(orDefault, "getOrDefault(sessionMemb…ache, groupId, HashSet())");
        Set set = CollectionsKt___CollectionsKt.toMutableSet((Iterable) orDefault);
        Intrinsics.checkNotNull(list);
        set.addAll(list);
        map.put(groupId, set);
        return set;
    }
}
