package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.graphics.Bitmap;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$9$$ExternalSyntheticLambda1 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ Context f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ ConversationParentFragment$9$$ExternalSyntheticLambda1(Context context, Recipient recipient) {
        this.f$0 = context;
        this.f$1 = recipient;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        ConversationParentFragment.access$1200(this.f$0, (Bitmap) obj, this.f$1);
    }
}
