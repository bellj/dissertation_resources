package org.thoughtcrime.securesms.conversation.mutiselect;

import android.view.View;
import kotlin.Metadata;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.Colorizable;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable;

/* compiled from: Multiselectable.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u00012\u00020\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH&J\n\u0010\u000b\u001a\u0004\u0018\u00010\fH&J\b\u0010\r\u001a\u00020\nH&J\u0010\u0010\u000e\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH&J\b\u0010\u000f\u001a\u00020\u0010H&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/Multiselectable;", "Lorg/thoughtcrime/securesms/conversation/colors/Colorizable;", "Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4Playable;", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getBottomBoundaryOfMultiselectPart", "", "multiselectPart", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "getHorizontalTranslationTarget", "Landroid/view/View;", "getMultiselectPartForLatestTouch", "getTopBoundaryOfMultiselectPart", "hasNonSelectableMedia", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface Multiselectable extends Colorizable, GiphyMp4Playable {
    int getBottomBoundaryOfMultiselectPart(MultiselectPart multiselectPart);

    ConversationMessage getConversationMessage();

    View getHorizontalTranslationTarget();

    MultiselectPart getMultiselectPartForLatestTouch();

    int getTopBoundaryOfMultiselectPart(MultiselectPart multiselectPart);

    boolean hasNonSelectableMedia();
}
