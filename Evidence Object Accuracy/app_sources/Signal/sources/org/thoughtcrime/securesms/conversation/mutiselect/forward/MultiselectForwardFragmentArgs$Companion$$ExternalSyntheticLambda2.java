package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import j$.util.function.Consumer;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ Consumer f$0;
    public final /* synthetic */ MultiShareArgs f$1;
    public final /* synthetic */ boolean f$2;
    public final /* synthetic */ int f$3;

    public /* synthetic */ MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda2(Consumer consumer, MultiShareArgs multiShareArgs, boolean z, int i) {
        this.f$0 = consumer;
        this.f$1 = multiShareArgs;
        this.f$2 = z;
        this.f$3 = i;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MultiselectForwardFragmentArgs.Companion.m1579create$lambda3$lambda2(this.f$0, this.f$1, this.f$2, this.f$3);
    }
}
