package org.thoughtcrime.securesms.conversation.drafts;

import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.voice.VoiceNoteDraft;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: DraftViewModel.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001%B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014J\u000e\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u0017J\u000e\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u001e\u001a\u00020\u001fJ\u0016\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0010J\u0014\u0010$\u001a\u00020\u00172\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014R\u0011\u0010\u0005\u001a\u00020\u00068G¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0007R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u000eX\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u00108F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0013\u001a\n\u0012\u0004\u0012\u00020\u0015\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/drafts/DraftViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/conversation/drafts/DraftRepository;", "(Lorg/thoughtcrime/securesms/conversation/drafts/DraftRepository;)V", "hasVoiceNoteDraft", "", "()Z", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/conversation/drafts/DraftState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "voiceNoteDraft", "Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "getVoiceNoteDraft", "()Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "voiceNoteDraftFuture", "Lorg/thoughtcrime/securesms/util/concurrent/ListenableFuture;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteDraft;", "clearVoiceNoteDraft", "", "consumeVoiceNoteDraftFuture", "deleteBlob", "uri", "Landroid/net/Uri;", "deleteVoiceNoteDraft", "onRecipientChanged", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "setVoiceNoteDraft", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "draft", "setVoiceNoteDraftFuture", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DraftViewModel extends ViewModel {
    private final DraftRepository repository;
    private final LiveData<DraftState> state;
    private final Store<DraftState> store;
    private ListenableFuture<VoiceNoteDraft> voiceNoteDraftFuture;

    public DraftViewModel(DraftRepository draftRepository) {
        Intrinsics.checkNotNullParameter(draftRepository, "repository");
        this.repository = draftRepository;
        Store<DraftState> store = new Store<>(new DraftState(null, null, 3, null));
        this.store = store;
        LiveData<DraftState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<DraftState> getState() {
        return this.state;
    }

    public final DraftDatabase.Draft getVoiceNoteDraft() {
        return this.store.getState().getVoiceNoteDraft();
    }

    public final ListenableFuture<VoiceNoteDraft> consumeVoiceNoteDraftFuture() {
        ListenableFuture<VoiceNoteDraft> listenableFuture = this.voiceNoteDraftFuture;
        this.voiceNoteDraftFuture = null;
        return listenableFuture;
    }

    public final void setVoiceNoteDraftFuture(ListenableFuture<VoiceNoteDraft> listenableFuture) {
        Intrinsics.checkNotNullParameter(listenableFuture, "voiceNoteDraftFuture");
        this.voiceNoteDraftFuture = listenableFuture;
    }

    public final void setVoiceNoteDraft(RecipientId recipientId, DraftDatabase.Draft draft) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(draft, "draft");
        this.store.update(new Function(draft) { // from class: org.thoughtcrime.securesms.conversation.drafts.DraftViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ DraftDatabase.Draft f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DraftViewModel.m1558setVoiceNoteDraft$lambda0(RecipientId.this, this.f$1, (DraftState) obj);
            }
        });
    }

    /* renamed from: setVoiceNoteDraft$lambda-0 */
    public static final DraftState m1558setVoiceNoteDraft$lambda0(RecipientId recipientId, DraftDatabase.Draft draft, DraftState draftState) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(draft, "$draft");
        return draftState.copy(recipientId, draft);
    }

    public final boolean hasVoiceNoteDraft() {
        return this.store.getState().getVoiceNoteDraft() != null;
    }

    public final void clearVoiceNoteDraft() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.drafts.DraftViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DraftViewModel.m1556clearVoiceNoteDraft$lambda1((DraftState) obj);
            }
        });
    }

    /* renamed from: clearVoiceNoteDraft$lambda-1 */
    public static final DraftState m1556clearVoiceNoteDraft$lambda1(DraftState draftState) {
        Intrinsics.checkNotNullExpressionValue(draftState, "it");
        return DraftState.copy$default(draftState, null, null, 1, null);
    }

    public final void deleteVoiceNoteDraft() {
        DraftDatabase.Draft voiceNoteDraft = this.store.getState().getVoiceNoteDraft();
        if (voiceNoteDraft != null) {
            clearVoiceNoteDraft();
            this.repository.deleteVoiceNoteDraft(voiceNoteDraft);
        }
    }

    public final void onRecipientChanged(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.drafts.DraftViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DraftViewModel.m1557onRecipientChanged$lambda2(Recipient.this, (DraftState) obj);
            }
        });
    }

    /* renamed from: onRecipientChanged$lambda-2 */
    public static final DraftState m1557onRecipientChanged$lambda2(Recipient recipient, DraftState draftState) {
        Intrinsics.checkNotNullParameter(recipient, "$recipient");
        if (Intrinsics.areEqual(recipient.getId(), draftState.getRecipientId())) {
            return draftState;
        }
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        return draftState.copy(id, null);
    }

    public final void deleteBlob(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        this.repository.deleteBlob(uri);
    }

    /* compiled from: DraftViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/drafts/DraftViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/conversation/drafts/DraftRepository;", "(Lorg/thoughtcrime/securesms/conversation/drafts/DraftRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DraftRepository repository;

        public Factory(DraftRepository draftRepository) {
            Intrinsics.checkNotNullParameter(draftRepository, "repository");
            this.repository = draftRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new DraftViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
