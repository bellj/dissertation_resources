package org.thoughtcrime.securesms.conversation.ui.mentions;

import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* loaded from: classes4.dex */
public class MentionsPickerAdapter extends MappingAdapter {
    private final Runnable currentListChangedListener;

    public MentionsPickerAdapter(RecipientViewHolder.EventListener<MentionViewState> eventListener, Runnable runnable) {
        this.currentListChangedListener = runnable;
        registerFactory(MentionViewState.class, RecipientViewHolder.createFactory(R.layout.mentions_picker_recipient_list_item, eventListener));
    }

    @Override // androidx.recyclerview.widget.ListAdapter
    public void onCurrentListChanged(List<MappingModel<?>> list, List<MappingModel<?>> list2) {
        super.onCurrentListChanged(list, list2);
        this.currentListChangedListener.run();
    }
}
