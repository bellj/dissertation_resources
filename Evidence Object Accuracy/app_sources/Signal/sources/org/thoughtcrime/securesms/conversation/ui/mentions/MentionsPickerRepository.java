package org.thoughtcrime.securesms.conversation.ui.mentions;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class MentionsPickerRepository {
    private final GroupDatabase groupDatabase = SignalDatabase.groups();
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();

    public List<RecipientId> getMembers(Recipient recipient) {
        if (recipient == null || !recipient.isPushV2Group()) {
            return Collections.emptyList();
        }
        return this.groupDatabase.getGroupMemberIds(recipient.requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF);
    }

    public List<Recipient> search(MentionQuery mentionQuery) {
        if (mentionQuery.query == null || mentionQuery.members.isEmpty()) {
            return Collections.emptyList();
        }
        return this.recipientDatabase.queryRecipientsForMentions(mentionQuery.query, mentionQuery.members);
    }

    /* loaded from: classes4.dex */
    public static class MentionQuery {
        private final List<RecipientId> members;
        private final String query;

        public MentionQuery(String str, List<RecipientId> list) {
            this.query = str;
            this.members = list;
        }
    }
}
