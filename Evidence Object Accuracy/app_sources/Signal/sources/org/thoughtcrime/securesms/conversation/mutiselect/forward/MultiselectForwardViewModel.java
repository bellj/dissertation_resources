package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardRepository;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardState;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.v2.UntrustedRecords;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: MultiselectForwardViewModel.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001$B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0006\u0010\u0018\u001a\u00020\u0019J\u001c\u0010\u001a\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eJ\u001c\u0010 \u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eJ\b\u0010!\u001a\u00020\u0019H\u0014J\u001e\u0010\"\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eH\u0002J\u001c\u0010#\u001a\u00020\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eR\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u000e\u001a\u00020\u000f8F¢\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0014\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0017X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardViewModel;", "Landroidx/lifecycle/ViewModel;", "storySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "records", "", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "isSelectionOnly", "", "repository", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository;", "(Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;Ljava/util/List;ZLorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "snapshot", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState;", "getSnapshot", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "cancelSend", "", "confirmFirstSend", "additionalMessage", "", "selectedContacts", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "confirmSafetySend", "onCleared", "performSend", "send", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final boolean isSelectionOnly;
    private final List<MultiShareArgs> records;
    private final MultiselectForwardRepository repository;
    private final LiveData<MultiselectForwardState> state;
    private final Store<MultiselectForwardState> store;
    private final Stories.MediaTransform.SendRequirements storySendRequirements;

    public MultiselectForwardViewModel(Stories.MediaTransform.SendRequirements sendRequirements, List<MultiShareArgs> list, boolean z, MultiselectForwardRepository multiselectForwardRepository) {
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        Intrinsics.checkNotNullParameter(list, "records");
        Intrinsics.checkNotNullParameter(multiselectForwardRepository, "repository");
        this.storySendRequirements = sendRequirements;
        this.records = list;
        this.isSelectionOnly = z;
        this.repository = multiselectForwardRepository;
        Store<MultiselectForwardState> store = new Store<>(new MultiselectForwardState(null, sendRequirements, 1, null));
        this.store = store;
        LiveData<MultiselectForwardState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        if (!list.isEmpty()) {
            Disposable subscribe = multiselectForwardRepository.checkAllSelectedMediaCanBeSentToStories(list).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda5
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    MultiselectForwardViewModel.m1589_init_$lambda1(MultiselectForwardViewModel.this, (Stories.MediaTransform.SendRequirements) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "repository.checkAllSelec…ndRequirements) }\n      }");
            DisposableKt.plusAssign(compositeDisposable, subscribe);
        }
    }

    public final LiveData<MultiselectForwardState> getState() {
        return this.state;
    }

    public final MultiselectForwardState getSnapshot() {
        MultiselectForwardState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        return state;
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m1589_init_$lambda1(MultiselectForwardViewModel multiselectForwardViewModel, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(multiselectForwardViewModel, "this$0");
        multiselectForwardViewModel.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MultiselectForwardViewModel.m1591lambda1$lambda0(Stories.MediaTransform.SendRequirements.this, (MultiselectForwardState) obj);
            }
        });
    }

    /* renamed from: lambda-1$lambda-0 */
    public static final MultiselectForwardState m1591lambda1$lambda0(Stories.MediaTransform.SendRequirements sendRequirements, MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        Intrinsics.checkNotNullExpressionValue(sendRequirements, "sendRequirements");
        return MultiselectForwardState.copy$default(multiselectForwardState, null, sendRequirements, 1, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void send(String str, Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(str, "additionalMessage");
        Intrinsics.checkNotNullParameter(set, "selectedContacts");
        if (SignalStore.tooltips().showMultiForwardDialog()) {
            SignalStore.tooltips().markMultiForwardDialogSeen();
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda6
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MultiselectForwardViewModel.m1594send$lambda2((MultiselectForwardState) obj);
                }
            });
            return;
        }
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MultiselectForwardViewModel.m1595send$lambda3((MultiselectForwardState) obj);
            }
        });
        UntrustedRecords.INSTANCE.checkForBadIdentityRecords(CollectionsKt___CollectionsKt.toSet(CollectionsKt___CollectionsJvmKt.filterIsInstance(set, ContactSearchKey.RecipientSearchKey.class)), new androidx.core.util.Consumer(str, set) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda8
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Set f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                MultiselectForwardViewModel.m1596send$lambda5(MultiselectForwardViewModel.this, this.f$1, this.f$2, (List) obj);
            }
        });
    }

    /* renamed from: send$lambda-2 */
    public static final MultiselectForwardState m1594send$lambda2(MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        return MultiselectForwardState.copy$default(multiselectForwardState, MultiselectForwardState.Stage.FirstConfirmation.INSTANCE, null, 2, null);
    }

    /* renamed from: send$lambda-3 */
    public static final MultiselectForwardState m1595send$lambda3(MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        return MultiselectForwardState.copy$default(multiselectForwardState, MultiselectForwardState.Stage.LoadingIdentities.INSTANCE, null, 2, null);
    }

    /* renamed from: send$lambda-5 */
    public static final void m1596send$lambda5(MultiselectForwardViewModel multiselectForwardViewModel, String str, Set set, List list) {
        Intrinsics.checkNotNullParameter(multiselectForwardViewModel, "this$0");
        Intrinsics.checkNotNullParameter(str, "$additionalMessage");
        Intrinsics.checkNotNullParameter(set, "$selectedContacts");
        if (list.isEmpty()) {
            multiselectForwardViewModel.performSend(str, set);
        } else {
            multiselectForwardViewModel.store.update(new Function(list, set) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda0
                public final /* synthetic */ List f$0;
                public final /* synthetic */ Set f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MultiselectForwardViewModel.m1597send$lambda5$lambda4(this.f$0, this.f$1, (MultiselectForwardState) obj);
                }
            });
        }
    }

    /* renamed from: send$lambda-5$lambda-4 */
    public static final MultiselectForwardState m1597send$lambda5$lambda4(List list, Set set, MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullParameter(set, "$selectedContacts");
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "state");
        Intrinsics.checkNotNullExpressionValue(list, "identityRecords");
        ArrayList arrayList = new ArrayList();
        for (Object obj : set) {
            if (obj instanceof ContactSearchKey.RecipientSearchKey) {
                arrayList.add(obj);
            }
        }
        return MultiselectForwardState.copy$default(multiselectForwardState, new MultiselectForwardState.Stage.SafetyConfirmation(list, arrayList), null, 2, null);
    }

    public final void confirmFirstSend(String str, Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(str, "additionalMessage");
        Intrinsics.checkNotNullParameter(set, "selectedContacts");
        send(str, set);
    }

    public final void confirmSafetySend(String str, Set<? extends ContactSearchKey> set) {
        Intrinsics.checkNotNullParameter(str, "additionalMessage");
        Intrinsics.checkNotNullParameter(set, "selectedContacts");
        send(str, set);
    }

    /* renamed from: cancelSend$lambda-6 */
    public static final MultiselectForwardState m1590cancelSend$lambda6(MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        return MultiselectForwardState.copy$default(multiselectForwardState, MultiselectForwardState.Stage.Selection.INSTANCE, null, 2, null);
    }

    public final void cancelSend() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MultiselectForwardViewModel.m1590cancelSend$lambda6((MultiselectForwardState) obj);
            }
        });
    }

    private final void performSend(String str, Set<? extends ContactSearchKey> set) {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MultiselectForwardViewModel.m1592performSend$lambda7((MultiselectForwardState) obj);
            }
        });
        if (this.records.isEmpty() || this.isSelectionOnly) {
            this.store.update(new Function(set) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$$ExternalSyntheticLambda4
                public final /* synthetic */ Set f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return MultiselectForwardViewModel.m1593performSend$lambda8(this.f$0, (MultiselectForwardState) obj);
                }
            });
        } else {
            this.repository.send(str, this.records, set, new MultiselectForwardRepository.MultiselectForwardResultHandlers(new MultiselectForwardViewModel$performSend$3(this), new MultiselectForwardViewModel$performSend$4(this), new MultiselectForwardViewModel$performSend$5(this)));
        }
    }

    /* renamed from: performSend$lambda-7 */
    public static final MultiselectForwardState m1592performSend$lambda7(MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        return MultiselectForwardState.copy$default(multiselectForwardState, MultiselectForwardState.Stage.SendPending.INSTANCE, null, 2, null);
    }

    /* renamed from: performSend$lambda-8 */
    public static final MultiselectForwardState m1593performSend$lambda8(Set set, MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullParameter(set, "$selectedContacts");
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        return MultiselectForwardState.copy$default(multiselectForwardState, new MultiselectForwardState.Stage.SelectionConfirmed(set), null, 2, null);
    }

    /* compiled from: MultiselectForwardViewModel.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ%\u0010\f\u001a\u0002H\r\"\b\b\u0000\u0010\r*\u00020\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u0002H\r0\u0010H\u0016¢\u0006\u0002\u0010\u0011R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "storySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "records", "", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "isSelectionOnly", "", "repository", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository;", "(Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;Ljava/util/List;ZLorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final boolean isSelectionOnly;
        private final List<MultiShareArgs> records;
        private final MultiselectForwardRepository repository;
        private final Stories.MediaTransform.SendRequirements storySendRequirements;

        public Factory(Stories.MediaTransform.SendRequirements sendRequirements, List<MultiShareArgs> list, boolean z, MultiselectForwardRepository multiselectForwardRepository) {
            Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
            Intrinsics.checkNotNullParameter(list, "records");
            Intrinsics.checkNotNullParameter(multiselectForwardRepository, "repository");
            this.storySendRequirements = sendRequirements;
            this.records = list;
            this.isSelectionOnly = z;
            this.repository = multiselectForwardRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new MultiselectForwardViewModel(this.storySendRequirements, this.records, this.isSelectionOnly, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
