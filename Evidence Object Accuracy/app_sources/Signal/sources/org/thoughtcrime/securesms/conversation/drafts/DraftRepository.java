package org.thoughtcrime.securesms.conversation.drafts;

import android.content.Context;
import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* compiled from: DraftRepository.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/drafts/DraftRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "deleteBlob", "", "uri", "Landroid/net/Uri;", "deleteVoiceNoteDraft", "draft", "Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DraftRepository {
    private final Context context;

    public DraftRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final void deleteVoiceNoteDraft(DraftDatabase.Draft draft) {
        Intrinsics.checkNotNullParameter(draft, "draft");
        Uri build = Uri.parse(draft.getValue()).buildUpon().clearQuery().build();
        Intrinsics.checkNotNullExpressionValue(build, "parse(draft.value).build…on().clearQuery().build()");
        deleteBlob(build);
    }

    public final void deleteBlob(Uri uri) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        SignalExecutors.BOUNDED.execute(new Runnable(uri) { // from class: org.thoughtcrime.securesms.conversation.drafts.DraftRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Uri f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                DraftRepository.m1555deleteBlob$lambda0(DraftRepository.this, this.f$1);
            }
        });
    }

    /* renamed from: deleteBlob$lambda-0 */
    public static final void m1555deleteBlob$lambda0(DraftRepository draftRepository, Uri uri) {
        Intrinsics.checkNotNullParameter(draftRepository, "this$0");
        Intrinsics.checkNotNullParameter(uri, "$uri");
        BlobProvider.getInstance().delete(draftRepository.context, uri);
    }
}
