package org.thoughtcrime.securesms.conversation;

import android.content.DialogInterface;
import java.util.List;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$UnverifiedClickedListener$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ConversationParentFragment.UnverifiedClickedListener f$0;
    public final /* synthetic */ List f$1;

    public /* synthetic */ ConversationParentFragment$UnverifiedClickedListener$$ExternalSyntheticLambda0(ConversationParentFragment.UnverifiedClickedListener unverifiedClickedListener, List list) {
        this.f$0 = unverifiedClickedListener;
        this.f$1 = list;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onClicked$0(this.f$1, dialogInterface, i);
    }
}
