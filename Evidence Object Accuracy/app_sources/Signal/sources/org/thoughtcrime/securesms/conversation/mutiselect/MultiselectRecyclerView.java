package org.thoughtcrime.securesms.conversation.mutiselect;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: MultiselectRecyclerView.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u000b2\u00020\u0001:\u0001\u000bB\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectRecyclerView;", "Landroidx/recyclerview/widget/RecyclerView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "onInterceptTouchEvent", "", "e", "Landroid/view/MotionEvent;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectRecyclerView extends RecyclerView {
    public static final Companion Companion = new Companion(null);
    private static final Rect rect = new Rect();

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectRecyclerView(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ MultiselectRecyclerView(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MultiselectRecyclerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x003c A[EDGE_INSN: B:35:0x003c->B:16:0x003c ?: BREAK  , SYNTHETIC] */
    @Override // androidx.recyclerview.widget.RecyclerView, android.view.ViewGroup
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onInterceptTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            java.lang.String r0 = "e"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r8, r0)
            kotlin.sequences.Sequence r0 = androidx.core.view.ViewGroupKt.getChildren(r7)
            java.util.Iterator r0 = r0.iterator()
        L_0x000d:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x003b
            java.lang.Object r1 = r0.next()
            r2 = r1
            android.view.View r2 = (android.view.View) r2
            boolean r3 = r2 instanceof org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
            r4 = 1
            r5 = 0
            if (r3 == 0) goto L_0x0037
            int r3 = r2.getTop()
            int r2 = r2.getBottom()
            float r6 = r8.getY()
            int r6 = (int) r6
            if (r3 > r6) goto L_0x0033
            if (r6 > r2) goto L_0x0033
            r2 = 1
            goto L_0x0034
        L_0x0033:
            r2 = 0
        L_0x0034:
            if (r2 == 0) goto L_0x0037
            goto L_0x0038
        L_0x0037:
            r4 = 0
        L_0x0038:
            if (r4 == 0) goto L_0x000d
            goto L_0x003c
        L_0x003b:
            r1 = 0
        L_0x003c:
            android.view.View r1 = (android.view.View) r1
            if (r1 == 0) goto L_0x008a
            android.graphics.Rect r0 = org.thoughtcrime.securesms.conversation.mutiselect.MultiselectRecyclerView.rect
            r1.getHitRect(r0)
            boolean r2 = org.thoughtcrime.securesms.util.ViewUtil.isLtr(r1)
            r3 = 0
            if (r2 == 0) goto L_0x0065
            int r2 = r0.left
            if (r2 == 0) goto L_0x0065
            float r2 = r8.getX()
            int r4 = r0.left
            float r5 = (float) r4
            int r2 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r2 >= 0) goto L_0x0065
            float r0 = (float) r4
            float r1 = r8.getX()
            float r0 = r0 - r1
            r8.offsetLocation(r0, r3)
            goto L_0x008a
        L_0x0065:
            boolean r1 = org.thoughtcrime.securesms.util.ViewUtil.isRtl(r1)
            if (r1 == 0) goto L_0x008a
            int r1 = r0.right
            int r2 = r7.getRight()
            if (r1 >= r2) goto L_0x008a
            float r1 = r8.getX()
            int r2 = r0.right
            float r2 = (float) r2
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 <= 0) goto L_0x008a
            int r1 = r7.getRight()
            int r0 = r0.right
            int r1 = r1 - r0
            float r0 = (float) r1
            float r0 = -r0
            r8.offsetLocation(r0, r3)
        L_0x008a:
            boolean r8 = super.onInterceptTouchEvent(r8)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.MultiselectRecyclerView.onInterceptTouchEvent(android.view.MotionEvent):boolean");
    }

    /* compiled from: MultiselectRecyclerView.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectRecyclerView$Companion;", "", "()V", "rect", "Landroid/graphics/Rect;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
