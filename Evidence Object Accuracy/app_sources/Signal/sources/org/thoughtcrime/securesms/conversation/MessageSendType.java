package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CharacterCalculator;
import org.thoughtcrime.securesms.util.MmsCharacterCalculator;
import org.thoughtcrime.securesms.util.PushCharacterCalculator;
import org.thoughtcrime.securesms.util.SmsCharacterCalculator;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionInfoCompat;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionManagerCompat;

/* compiled from: MessageSendType.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b7\u0018\u0000 ,2\u00020\u0001:\u0005,-./0Ba\b\u0004\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u000fJ\u000e\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&J\u000e\u0010'\u001a\u00020\u00032\u0006\u0010(\u001a\u00020\u0003J\u0010\u0010)\u001a\u00020&2\u0006\u0010*\u001a\u00020+H\u0016R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0016\u0010\f\u001a\u0004\u0018\u00010\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0018\u0010\u000e\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\n\n\u0002\u0010\u001b\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u001f\u001a\u00020 8G¢\u0006\u0006\u001a\u0004\b\u001f\u0010!R\u0011\u0010\"\u001a\u00020 8G¢\u0006\u0006\u001a\u0004\b\"\u0010!\u0001\u0003123¨\u00064"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "Landroid/os/Parcelable;", "titleRes", "", "composeHintRes", "buttonDrawableRes", "menuDrawableRes", "backgroundColorRes", "transportType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType$TransportType;", "characterCalculator", "Lorg/thoughtcrime/securesms/util/CharacterCalculator;", "simName", "", "simSubscriptionId", "(IIIIILorg/thoughtcrime/securesms/conversation/MessageSendType$TransportType;Lorg/thoughtcrime/securesms/util/CharacterCalculator;Ljava/lang/CharSequence;Ljava/lang/Integer;)V", "getBackgroundColorRes", "()I", "getButtonDrawableRes", "getCharacterCalculator", "()Lorg/thoughtcrime/securesms/util/CharacterCalculator;", "getComposeHintRes", "getMenuDrawableRes", "getSimName", "()Ljava/lang/CharSequence;", "getSimSubscriptionId", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getTitleRes", "getTransportType", "()Lorg/thoughtcrime/securesms/conversation/MessageSendType$TransportType;", "usesSignalTransport", "", "()Z", "usesSmsTransport", "calculateCharacters", "Lorg/thoughtcrime/securesms/util/CharacterCalculator$CharacterState;", "body", "", "getSimSubscriptionIdOr", "fallback", "getTitle", "context", "Landroid/content/Context;", "Companion", "MmsMessageSendType", "SignalMessageSendType", "SmsMessageSendType", "TransportType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType$SmsMessageSendType;", "Lorg/thoughtcrime/securesms/conversation/MessageSendType$MmsMessageSendType;", "Lorg/thoughtcrime/securesms/conversation/MessageSendType$SignalMessageSendType;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class MessageSendType implements Parcelable {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(MessageSendType.class);
    private final int backgroundColorRes;
    private final int buttonDrawableRes;
    private final CharacterCalculator characterCalculator;
    private final int composeHintRes;
    private final int menuDrawableRes;
    private final CharSequence simName;
    private final Integer simSubscriptionId;
    private final int titleRes;
    private final TransportType transportType;

    /* compiled from: MessageSendType.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageSendType$TransportType;", "", "(Ljava/lang/String;I)V", "SIGNAL", "SMS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum TransportType {
        SIGNAL,
        SMS
    }

    public /* synthetic */ MessageSendType(int i, int i2, int i3, int i4, int i5, TransportType transportType, CharacterCalculator characterCalculator, CharSequence charSequence, Integer num, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, i2, i3, i4, i5, transportType, characterCalculator, charSequence, num);
    }

    @JvmStatic
    public static final List<MessageSendType> getAllAvailable(Context context, boolean z) {
        return Companion.getAllAvailable(context, z);
    }

    @JvmStatic
    public static final MessageSendType getFirstForTransport(Context context, boolean z, TransportType transportType) {
        return Companion.getFirstForTransport(context, z, transportType);
    }

    private MessageSendType(int i, int i2, int i3, int i4, int i5, TransportType transportType, CharacterCalculator characterCalculator, CharSequence charSequence, Integer num) {
        this.titleRes = i;
        this.composeHintRes = i2;
        this.buttonDrawableRes = i3;
        this.menuDrawableRes = i4;
        this.backgroundColorRes = i5;
        this.transportType = transportType;
        this.characterCalculator = characterCalculator;
        this.simName = charSequence;
        this.simSubscriptionId = num;
    }

    public /* synthetic */ MessageSendType(int i, int i2, int i3, int i4, int i5, TransportType transportType, CharacterCalculator characterCalculator, CharSequence charSequence, Integer num, int i6, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, i2, i3, i4, i5, transportType, characterCalculator, (i6 & 128) != 0 ? null : charSequence, (i6 & 256) != 0 ? null : num, null);
    }

    public final int getTitleRes() {
        return this.titleRes;
    }

    public final int getComposeHintRes() {
        return this.composeHintRes;
    }

    public final int getButtonDrawableRes() {
        return this.buttonDrawableRes;
    }

    public final int getMenuDrawableRes() {
        return this.menuDrawableRes;
    }

    public final int getBackgroundColorRes() {
        return this.backgroundColorRes;
    }

    public final TransportType getTransportType() {
        return this.transportType;
    }

    public final CharacterCalculator getCharacterCalculator() {
        return this.characterCalculator;
    }

    public CharSequence getSimName() {
        return this.simName;
    }

    public Integer getSimSubscriptionId() {
        return this.simSubscriptionId;
    }

    public final boolean usesSmsTransport() {
        return this.transportType == TransportType.SMS;
    }

    public final boolean usesSignalTransport() {
        return this.transportType == TransportType.SIGNAL;
    }

    public final CharacterCalculator.CharacterState calculateCharacters(String str) {
        Intrinsics.checkNotNullParameter(str, "body");
        CharacterCalculator.CharacterState calculateCharacters = this.characterCalculator.calculateCharacters(str);
        Intrinsics.checkNotNullExpressionValue(calculateCharacters, "characterCalculator.calculateCharacters(body)");
        return calculateCharacters;
    }

    public final int getSimSubscriptionIdOr(int i) {
        Integer simSubscriptionId = getSimSubscriptionId();
        return simSubscriptionId != null ? simSubscriptionId.intValue() : i;
    }

    public String getTitle(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        String string = context.getString(this.titleRes);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(titleRes)");
        return string;
    }

    /* compiled from: MessageSendType.kt */
    @Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\r\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\nJ&\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0002\u0010\u000fJ\t\u0010\u0010\u001a\u00020\u0005HÖ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\t\u0010\u0019\u001a\u00020\u0005HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0016HÖ\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0005HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\n¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageSendType$SmsMessageSendType;", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "simName", "", "simSubscriptionId", "", "(Ljava/lang/CharSequence;Ljava/lang/Integer;)V", "getSimName", "()Ljava/lang/CharSequence;", "getSimSubscriptionId", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component2", "copy", "(Ljava/lang/CharSequence;Ljava/lang/Integer;)Lorg/thoughtcrime/securesms/conversation/MessageSendType$SmsMessageSendType;", "describeContents", "equals", "", "other", "", "getTitle", "", "context", "Landroid/content/Context;", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SmsMessageSendType extends MessageSendType {
        public static final Parcelable.Creator<SmsMessageSendType> CREATOR = new Creator();
        private final CharSequence simName;
        private final Integer simSubscriptionId;

        /* compiled from: MessageSendType.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Creator implements Parcelable.Creator<SmsMessageSendType> {
            @Override // android.os.Parcelable.Creator
            public final SmsMessageSendType createFromParcel(Parcel parcel) {
                Intrinsics.checkNotNullParameter(parcel, "parcel");
                return new SmsMessageSendType((CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel), parcel.readInt() == 0 ? null : Integer.valueOf(parcel.readInt()));
            }

            @Override // android.os.Parcelable.Creator
            public final SmsMessageSendType[] newArray(int i) {
                return new SmsMessageSendType[i];
            }
        }

        public SmsMessageSendType() {
            this(null, null, 3, null);
        }

        public static /* synthetic */ SmsMessageSendType copy$default(SmsMessageSendType smsMessageSendType, CharSequence charSequence, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = smsMessageSendType.getSimName();
            }
            if ((i & 2) != 0) {
                num = smsMessageSendType.getSimSubscriptionId();
            }
            return smsMessageSendType.copy(charSequence, num);
        }

        public final CharSequence component1() {
            return getSimName();
        }

        public final Integer component2() {
            return getSimSubscriptionId();
        }

        public final SmsMessageSendType copy(CharSequence charSequence, Integer num) {
            return new SmsMessageSendType(charSequence, num);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // java.lang.Object
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof SmsMessageSendType)) {
                return false;
            }
            SmsMessageSendType smsMessageSendType = (SmsMessageSendType) obj;
            return Intrinsics.areEqual(getSimName(), smsMessageSendType.getSimName()) && Intrinsics.areEqual(getSimSubscriptionId(), smsMessageSendType.getSimSubscriptionId());
        }

        @Override // java.lang.Object
        public int hashCode() {
            int i = 0;
            int hashCode = (getSimName() == null ? 0 : getSimName().hashCode()) * 31;
            if (getSimSubscriptionId() != null) {
                i = getSimSubscriptionId().hashCode();
            }
            return hashCode + i;
        }

        @Override // java.lang.Object
        public String toString() {
            return "SmsMessageSendType(simName=" + ((Object) getSimName()) + ", simSubscriptionId=" + getSimSubscriptionId() + ')';
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            int i2;
            Intrinsics.checkNotNullParameter(parcel, "out");
            TextUtils.writeToParcel(this.simName, parcel, i);
            Integer num = this.simSubscriptionId;
            if (num == null) {
                i2 = 0;
            } else {
                parcel.writeInt(1);
                i2 = num.intValue();
            }
            parcel.writeInt(i2);
        }

        public /* synthetic */ SmsMessageSendType(CharSequence charSequence, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : charSequence, (i & 2) != 0 ? null : num);
        }

        @Override // org.thoughtcrime.securesms.conversation.MessageSendType
        public CharSequence getSimName() {
            return this.simName;
        }

        @Override // org.thoughtcrime.securesms.conversation.MessageSendType
        public Integer getSimSubscriptionId() {
            return this.simSubscriptionId;
        }

        public SmsMessageSendType(CharSequence charSequence, Integer num) {
            super(R.string.ConversationActivity_transport_insecure_sms, R.string.conversation_activity__type_message_sms_insecure, R.drawable.ic_send_unlock_24, R.drawable.ic_insecure_24, R.color.core_grey_50, TransportType.SMS, new SmsCharacterCalculator(), charSequence, num, null);
            this.simName = charSequence;
            this.simSubscriptionId = num;
        }

        @Override // org.thoughtcrime.securesms.conversation.MessageSendType
        public String getTitle(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            if (getSimName() == null) {
                return MessageSendType.super.getTitle(context);
            }
            String string = context.getString(R.string.ConversationActivity_transport_insecure_sms_with_sim, getSimName());
            Intrinsics.checkNotNullExpressionValue(string, "{\n        context.getStr…ith_sim, simName)\n      }");
            return string;
        }
    }

    /* compiled from: MessageSendType.kt */
    @Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u0010\u0010\r\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\nJ&\u0010\u000e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001¢\u0006\u0002\u0010\u000fJ\t\u0010\u0010\u001a\u00020\u0005HÖ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\t\u0010\u0019\u001a\u00020\u0005HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0016HÖ\u0001J\u0019\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020\u0005HÖ\u0001R\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0018\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\n¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageSendType$MmsMessageSendType;", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "simName", "", "simSubscriptionId", "", "(Ljava/lang/CharSequence;Ljava/lang/Integer;)V", "getSimName", "()Ljava/lang/CharSequence;", "getSimSubscriptionId", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component2", "copy", "(Ljava/lang/CharSequence;Ljava/lang/Integer;)Lorg/thoughtcrime/securesms/conversation/MessageSendType$MmsMessageSendType;", "describeContents", "equals", "", "other", "", "getTitle", "", "context", "Landroid/content/Context;", "hashCode", "toString", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MmsMessageSendType extends MessageSendType {
        public static final Parcelable.Creator<MmsMessageSendType> CREATOR = new Creator();
        private final CharSequence simName;
        private final Integer simSubscriptionId;

        /* compiled from: MessageSendType.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Creator implements Parcelable.Creator<MmsMessageSendType> {
            @Override // android.os.Parcelable.Creator
            public final MmsMessageSendType createFromParcel(Parcel parcel) {
                Intrinsics.checkNotNullParameter(parcel, "parcel");
                return new MmsMessageSendType((CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel), parcel.readInt() == 0 ? null : Integer.valueOf(parcel.readInt()));
            }

            @Override // android.os.Parcelable.Creator
            public final MmsMessageSendType[] newArray(int i) {
                return new MmsMessageSendType[i];
            }
        }

        public MmsMessageSendType() {
            this(null, null, 3, null);
        }

        public static /* synthetic */ MmsMessageSendType copy$default(MmsMessageSendType mmsMessageSendType, CharSequence charSequence, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = mmsMessageSendType.getSimName();
            }
            if ((i & 2) != 0) {
                num = mmsMessageSendType.getSimSubscriptionId();
            }
            return mmsMessageSendType.copy(charSequence, num);
        }

        public final CharSequence component1() {
            return getSimName();
        }

        public final Integer component2() {
            return getSimSubscriptionId();
        }

        public final MmsMessageSendType copy(CharSequence charSequence, Integer num) {
            return new MmsMessageSendType(charSequence, num);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // java.lang.Object
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MmsMessageSendType)) {
                return false;
            }
            MmsMessageSendType mmsMessageSendType = (MmsMessageSendType) obj;
            return Intrinsics.areEqual(getSimName(), mmsMessageSendType.getSimName()) && Intrinsics.areEqual(getSimSubscriptionId(), mmsMessageSendType.getSimSubscriptionId());
        }

        @Override // java.lang.Object
        public int hashCode() {
            int i = 0;
            int hashCode = (getSimName() == null ? 0 : getSimName().hashCode()) * 31;
            if (getSimSubscriptionId() != null) {
                i = getSimSubscriptionId().hashCode();
            }
            return hashCode + i;
        }

        @Override // java.lang.Object
        public String toString() {
            return "MmsMessageSendType(simName=" + ((Object) getSimName()) + ", simSubscriptionId=" + getSimSubscriptionId() + ')';
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            int i2;
            Intrinsics.checkNotNullParameter(parcel, "out");
            TextUtils.writeToParcel(this.simName, parcel, i);
            Integer num = this.simSubscriptionId;
            if (num == null) {
                i2 = 0;
            } else {
                parcel.writeInt(1);
                i2 = num.intValue();
            }
            parcel.writeInt(i2);
        }

        public /* synthetic */ MmsMessageSendType(CharSequence charSequence, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : charSequence, (i & 2) != 0 ? null : num);
        }

        @Override // org.thoughtcrime.securesms.conversation.MessageSendType
        public CharSequence getSimName() {
            return this.simName;
        }

        @Override // org.thoughtcrime.securesms.conversation.MessageSendType
        public Integer getSimSubscriptionId() {
            return this.simSubscriptionId;
        }

        public MmsMessageSendType(CharSequence charSequence, Integer num) {
            super(R.string.ConversationActivity_transport_insecure_mms, R.string.conversation_activity__type_message_mms_insecure, R.drawable.ic_send_unlock_24, R.drawable.ic_insecure_24, R.color.core_grey_50, TransportType.SMS, new MmsCharacterCalculator(), charSequence, num, null);
            this.simName = charSequence;
            this.simSubscriptionId = num;
        }

        @Override // org.thoughtcrime.securesms.conversation.MessageSendType
        public String getTitle(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            if (getSimName() == null) {
                return MessageSendType.super.getTitle(context);
            }
            String string = context.getString(R.string.ConversationActivity_transport_insecure_sms_with_sim, getSimName());
            Intrinsics.checkNotNullExpressionValue(string, "{\n        context.getStr…ith_sim, simName)\n      }");
            return string;
        }
    }

    /* compiled from: MessageSendType.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÇ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\t\u0010\u0003\u001a\u00020\u0004HÖ\u0001J\u0019\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0004HÖ\u0001¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageSendType$SignalMessageSendType;", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "()V", "describeContents", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SignalMessageSendType extends MessageSendType {
        public static final Parcelable.Creator<SignalMessageSendType> CREATOR = new Creator();
        public static final SignalMessageSendType INSTANCE = new SignalMessageSendType();

        /* compiled from: MessageSendType.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Creator implements Parcelable.Creator<SignalMessageSendType> {
            @Override // android.os.Parcelable.Creator
            public final SignalMessageSendType createFromParcel(Parcel parcel) {
                Intrinsics.checkNotNullParameter(parcel, "parcel");
                parcel.readInt();
                return SignalMessageSendType.INSTANCE;
            }

            @Override // android.os.Parcelable.Creator
            public final SignalMessageSendType[] newArray(int i) {
                return new SignalMessageSendType[i];
            }
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "out");
            parcel.writeInt(1);
        }

        private SignalMessageSendType() {
            super(R.string.ConversationActivity_transport_signal, R.string.conversation_activity__type_message_push, R.drawable.ic_send_lock_24, R.drawable.ic_secure_24, R.color.core_ultramarine, TransportType.SIGNAL, new PushCharacterCalculator(), null, null, 384, null);
        }
    }

    /* compiled from: MessageSendType.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\u0006\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\fH\u0007J \u0010\r\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageSendType$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "getAllAvailable", "", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "context", "Landroid/content/Context;", "isMedia", "", "getFirstForTransport", "transportType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType$TransportType;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public static /* synthetic */ List getAllAvailable$default(Companion companion, Context context, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            return companion.getAllAvailable(context, z);
        }

        @JvmStatic
        public final List<MessageSendType> getAllAvailable(Context context, boolean z) {
            Object obj;
            Intrinsics.checkNotNullParameter(context, "context");
            ArrayList arrayList = new ArrayList();
            arrayList.add(SignalMessageSendType.INSTANCE);
            try {
                Collection<SubscriptionInfoCompat> activeAndReadySubscriptionInfos = new SubscriptionManagerCompat(context).getActiveAndReadySubscriptionInfos();
                Intrinsics.checkNotNullExpressionValue(activeAndReadySubscriptionInfos, "SubscriptionManagerCompa…AndReadySubscriptionInfos");
                if (activeAndReadySubscriptionInfos.size() < 2) {
                    arrayList.add(z ? new MmsMessageSendType(null, null, 3, null) : new SmsMessageSendType(null, null, 3, null));
                } else {
                    ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(activeAndReadySubscriptionInfos, 10));
                    for (SubscriptionInfoCompat subscriptionInfoCompat : activeAndReadySubscriptionInfos) {
                        if (z) {
                            obj = new MmsMessageSendType(subscriptionInfoCompat.getDisplayName(), Integer.valueOf(subscriptionInfoCompat.getSubscriptionId()));
                        } else {
                            obj = new SmsMessageSendType(subscriptionInfoCompat.getDisplayName(), Integer.valueOf(subscriptionInfoCompat.getSubscriptionId()));
                        }
                        arrayList2.add(obj);
                    }
                    boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, arrayList2);
                }
            } catch (SecurityException unused2) {
                Log.w(MessageSendType.TAG, "Did not have permission to get SMS subscription details!");
            }
            return arrayList;
        }

        @JvmStatic
        public final MessageSendType getFirstForTransport(Context context, boolean z, TransportType transportType) {
            Object obj;
            boolean z2;
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(transportType, "transportType");
            Iterator<T> it = getAllAvailable(context, z).iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((MessageSendType) obj).getTransportType() == transportType) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            MessageSendType messageSendType = (MessageSendType) obj;
            if (messageSendType != null) {
                return messageSendType;
            }
            throw new IllegalArgumentException("No options available for desired type " + transportType + '!');
        }
    }
}
