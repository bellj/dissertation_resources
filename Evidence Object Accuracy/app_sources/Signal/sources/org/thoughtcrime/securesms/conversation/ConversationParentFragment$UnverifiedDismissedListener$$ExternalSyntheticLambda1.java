package org.thoughtcrime.securesms.conversation;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$UnverifiedDismissedListener$$ExternalSyntheticLambda1 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ ConversationParentFragment.UnverifiedDismissedListener f$0;

    public /* synthetic */ ConversationParentFragment$UnverifiedDismissedListener$$ExternalSyntheticLambda1(ConversationParentFragment.UnverifiedDismissedListener unverifiedDismissedListener) {
        this.f$0 = unverifiedDismissedListener;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.lambda$onDismissed$1(obj);
    }
}
