package org.thoughtcrime.securesms.conversation.colors.ui;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class ChatColorSelectionFragmentArgs {
    private final HashMap arguments;

    private ChatColorSelectionFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ChatColorSelectionFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ChatColorSelectionFragmentArgs fromBundle(Bundle bundle) {
        ChatColorSelectionFragmentArgs chatColorSelectionFragmentArgs = new ChatColorSelectionFragmentArgs();
        bundle.setClassLoader(ChatColorSelectionFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("recipient_id")) {
            throw new IllegalArgumentException("Required argument \"recipient_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(RecipientId.class) || Serializable.class.isAssignableFrom(RecipientId.class)) {
            chatColorSelectionFragmentArgs.arguments.put("recipient_id", (RecipientId) bundle.get("recipient_id"));
            return chatColorSelectionFragmentArgs;
        } else {
            throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public RecipientId getRecipientId() {
        return (RecipientId) this.arguments.get("recipient_id");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("recipient_id")) {
            RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
            if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
            } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
            } else {
                throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ChatColorSelectionFragmentArgs chatColorSelectionFragmentArgs = (ChatColorSelectionFragmentArgs) obj;
        if (this.arguments.containsKey("recipient_id") != chatColorSelectionFragmentArgs.arguments.containsKey("recipient_id")) {
            return false;
        }
        return getRecipientId() == null ? chatColorSelectionFragmentArgs.getRecipientId() == null : getRecipientId().equals(chatColorSelectionFragmentArgs.getRecipientId());
    }

    public int hashCode() {
        return 31 + (getRecipientId() != null ? getRecipientId().hashCode() : 0);
    }

    public String toString() {
        return "ChatColorSelectionFragmentArgs{recipientId=" + getRecipientId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ChatColorSelectionFragmentArgs chatColorSelectionFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(chatColorSelectionFragmentArgs.arguments);
        }

        public Builder(RecipientId recipientId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("recipient_id", recipientId);
        }

        public ChatColorSelectionFragmentArgs build() {
            return new ChatColorSelectionFragmentArgs(this.arguments);
        }

        public Builder setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }
    }
}
