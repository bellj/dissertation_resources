package org.thoughtcrime.securesms.conversation.colors.ui;

import kotlin.jvm.functions.Function0;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ChatColorSelectionRepository$Single$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ ChatColorSelectionRepository.Single f$0;
    public final /* synthetic */ ChatColors f$1;
    public final /* synthetic */ Function0 f$2;

    public /* synthetic */ ChatColorSelectionRepository$Single$$ExternalSyntheticLambda0(ChatColorSelectionRepository.Single single, ChatColors chatColors, Function0 function0) {
        this.f$0 = single;
        this.f$1 = chatColors;
        this.f$2 = function0;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ChatColorSelectionRepository.Single.m1514save$lambda2(this.f$0, this.f$1, this.f$2);
    }
}
