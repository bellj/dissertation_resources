package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.ContextMenuList;

/* compiled from: ConversationContextMenu.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fJ\u0016\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationContextMenu;", "Landroid/widget/PopupWindow;", "anchor", "Landroid/view/View;", "items", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "(Landroid/view/View;Ljava/util/List;)V", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "contextMenuList", "Lorg/thoughtcrime/securesms/components/menu/ContextMenuList;", "getMaxHeight", "", "getMaxWidth", "show", "", "offsetX", "offsetY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationContextMenu extends PopupWindow {
    private final View anchor;
    private final Context context;
    private final ContextMenuList contextMenuList;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ConversationContextMenu(View view, List<ActionItem> list) {
        super(LayoutInflater.from(view.getContext()).inflate(R.layout.signal_context_menu, (ViewGroup) null), -2, -2);
        Intrinsics.checkNotNullParameter(view, "anchor");
        Intrinsics.checkNotNullParameter(list, "items");
        this.anchor = view;
        Context context = view.getContext();
        Intrinsics.checkNotNullExpressionValue(context, "anchor.context");
        this.context = context;
        View findViewById = getContentView().findViewById(R.id.signal_context_menu_list);
        Intrinsics.checkNotNullExpressionValue(findViewById, "contentView.findViewById…signal_context_menu_list)");
        ContextMenuList contextMenuList = new ContextMenuList((RecyclerView) findViewById, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.conversation.ConversationContextMenu$contextMenuList$1
            final /* synthetic */ ConversationContextMenu this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.dismiss();
            }
        });
        this.contextMenuList = contextMenuList;
        setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.signal_context_menu_background));
        setAnimationStyle(R.style.ConversationContextMenuAnimation);
        setFocusable(false);
        setOutsideTouchable(true);
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation(20.0f);
        }
        setTouchInterceptor(new View.OnTouchListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationContextMenu$$ExternalSyntheticLambda0
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                return ConversationContextMenu.m1369$r8$lambda$wXg4_zwQ7uTIS6iZl77i5F1A(view2, motionEvent);
            }
        });
        contextMenuList.setItems(list);
        getContentView().measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    public final Context getContext() {
        return this.context;
    }

    /* renamed from: _init_$lambda-0 */
    public static final boolean m1370_init_$lambda0(View view, MotionEvent motionEvent) {
        return motionEvent.getAction() == 4;
    }

    public final int getMaxWidth() {
        return getContentView().getMeasuredWidth();
    }

    public final int getMaxHeight() {
        return getContentView().getMeasuredHeight();
    }

    public final void show(int i, int i2) {
        showAsDropDown(this.anchor, i, i2, 8388659);
    }
}
