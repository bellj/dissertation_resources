package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.SeekBar;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.ColorUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorPreviewView;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel;
import org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment;
import org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel;
import org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorGradientToolView;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.CustomDrawWrapperKt;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: CustomChatColorCreatorPageFragment.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0004\n\u0002\b\u0006\u0018\u0000 %2\u00020\u0001:\u0003%&'B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\tH\u0002J\b\u0010\f\u001a\u00020\rH\u0002J \u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\tH\u0002J\u001a\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\f\u0010\u0019\u001a\u00020\u001a*\u00020\u001aH\u0002J\f\u0010\u001b\u001a\u00020\u001c*\u00020\u001dH\u0003J\f\u0010\u001e\u001a\u00020\u001c*\u00020\u001dH\u0003J\f\u0010\u001f\u001a\u00020 *\u00020\u001dH\u0002J\u0014\u0010!\u001a\u00020\t*\u00020\"2\u0006\u0010#\u001a\u00020\"H\u0002J\u0014\u0010$\u001a\u00020\t*\u00020\"2\u0006\u0010#\u001a\u00020\"H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorPageFragment;", "Landroidx/fragment/app/Fragment;", "()V", "hueSlider", "Landroidx/appcompat/widget/AppCompatSeekBar;", "preview", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorPreviewView;", "saturationSlider", "calculateLightness", "", "hue", "valueFor60To80", "createRepository", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorRepository;", "interpolate", "point1", "Landroid/graphics/PointF;", "point2", "x", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "forSeekBar", "Landroid/graphics/drawable/Drawable;", "getColor", "", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/ColorSlidersState;", "getHueColor", "getSaturationColors", "", "toHue", "", "max", "toUnit", "Companion", "OnProgressChangedListener", "ThumbDrawable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorCreatorPageFragment extends Fragment {
    public static final Companion Companion = new Companion(null);
    private AppCompatSeekBar hueSlider;
    private ChatColorPreviewView preview;
    private AppCompatSeekBar saturationSlider;

    public CustomChatColorCreatorPageFragment() {
        super(R.layout.custom_chat_color_creator_fragment_page);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        AppCompatSeekBar appCompatSeekBar;
        Intrinsics.checkNotNullParameter(view, "view");
        CustomChatColorCreatorFragmentArgs fromBundle = CustomChatColorCreatorFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        ChatColorSelectionViewModel.Companion companion = ChatColorSelectionViewModel.Companion;
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        ChatColorSelectionViewModel orCreate = companion.getOrCreate(requireActivity, fromBundle.getRecipientId());
        int i = requireArguments().getInt("page");
        int i2 = 1023;
        ViewModel viewModel = new ViewModelProvider(requireParentFragment(), new CustomChatColorCreatorViewModel.Factory(1023, ChatColors.Id.Companion.forLongValue(fromBundle.getChatColorId()), fromBundle.getRecipientId(), createRepository())).get(CustomChatColorCreatorViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(\n     …torViewModel::class.java]");
        CustomChatColorCreatorViewModel customChatColorCreatorViewModel = (CustomChatColorCreatorViewModel) viewModel;
        View findViewById = view.findViewById(R.id.chat_color_preview);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.chat_color_preview)");
        this.preview = (ChatColorPreviewView) findViewById;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ThumbDrawable thumbDrawable = new ThumbDrawable(requireContext);
        Context requireContext2 = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
        ThumbDrawable thumbDrawable2 = new ThumbDrawable(requireContext2);
        View findViewById2 = view.findViewById(R.id.gradient_tool);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.gradient_tool)");
        CustomChatColorGradientToolView customChatColorGradientToolView = (CustomChatColorGradientToolView) findViewById2;
        View findViewById3 = view.findViewById(R.id.save);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.save)");
        View findViewById4 = view.findViewById(R.id.scroll_view);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.scroll_view)");
        ScrollView scrollView = (ScrollView) findViewById4;
        if (i == 0) {
            customChatColorGradientToolView.setVisibility(8);
        } else {
            customChatColorGradientToolView.setListener(new CustomChatColorGradientToolView.Listener(scrollView, customChatColorCreatorViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$onViewCreated$1
                final /* synthetic */ ScrollView $scrollView;
                final /* synthetic */ CustomChatColorCreatorViewModel $viewModel;

                /* access modifiers changed from: package-private */
                {
                    this.$scrollView = r1;
                    this.$viewModel = r2;
                }

                @Override // org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorGradientToolView.Listener
                public void onGestureStarted() {
                    this.$scrollView.requestDisallowInterceptTouchEvent(true);
                }

                @Override // org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorGradientToolView.Listener
                public void onGestureFinished() {
                    this.$scrollView.requestDisallowInterceptTouchEvent(false);
                }

                @Override // org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorGradientToolView.Listener
                public void onDegreesChanged(float f) {
                    this.$viewModel.setDegrees(f);
                }

                @Override // org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorGradientToolView.Listener
                public void onSelectedEdgeChanged(CustomChatColorEdge customChatColorEdge) {
                    Intrinsics.checkNotNullParameter(customChatColorEdge, "edge");
                    this.$viewModel.setSelectedEdge(customChatColorEdge);
                }
            });
        }
        View findViewById5 = view.findViewById(R.id.hue_slider);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.hue_slider)");
        this.hueSlider = (AppCompatSeekBar) findViewById5;
        View findViewById6 = view.findViewById(R.id.saturation_slider);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.saturation_slider)");
        this.saturationSlider = (AppCompatSeekBar) findViewById6;
        AppCompatSeekBar appCompatSeekBar2 = this.hueSlider;
        if (appCompatSeekBar2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hueSlider");
            appCompatSeekBar2 = null;
        }
        appCompatSeekBar2.setThumb(thumbDrawable);
        AppCompatSeekBar appCompatSeekBar3 = this.saturationSlider;
        if (appCompatSeekBar3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saturationSlider");
            appCompatSeekBar3 = null;
        }
        appCompatSeekBar3.setThumb(thumbDrawable2);
        AppCompatSeekBar appCompatSeekBar4 = this.hueSlider;
        if (appCompatSeekBar4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hueSlider");
            appCompatSeekBar4 = null;
        }
        appCompatSeekBar4.setMax(1023);
        AppCompatSeekBar appCompatSeekBar5 = this.saturationSlider;
        if (appCompatSeekBar5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saturationSlider");
            appCompatSeekBar5 = null;
        }
        appCompatSeekBar5.setMax(1023);
        IntRange intRange = new IntRange(0, 1023);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            int nextInt = ((IntIterator) it).nextInt();
            arrayList.add(Integer.valueOf(ColorUtils.HSLToColor(new float[]{toHue(Integer.valueOf(nextInt), Integer.valueOf(i2)), 1.0f, calculateLightness((float) nextInt, 0.4f)})));
            it = it;
            i2 = 1023;
        }
        Drawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, CollectionsKt___CollectionsKt.toIntArray(arrayList));
        AppCompatSeekBar appCompatSeekBar6 = this.hueSlider;
        if (appCompatSeekBar6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hueSlider");
            appCompatSeekBar6 = null;
        }
        appCompatSeekBar6.setProgressDrawable(forSeekBar(gradientDrawable));
        GradientDrawable gradientDrawable2 = new GradientDrawable();
        gradientDrawable2.setOrientation(GradientDrawable.Orientation.LEFT_RIGHT);
        AppCompatSeekBar appCompatSeekBar7 = this.saturationSlider;
        if (appCompatSeekBar7 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saturationSlider");
            appCompatSeekBar7 = null;
        }
        appCompatSeekBar7.setProgressDrawable(forSeekBar(gradientDrawable2));
        AppCompatSeekBar appCompatSeekBar8 = this.hueSlider;
        if (appCompatSeekBar8 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("hueSlider");
            appCompatSeekBar8 = null;
        }
        appCompatSeekBar8.setOnSeekBarChangeListener(new OnProgressChangedListener(new Function1<Integer, Unit>(customChatColorCreatorViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$onViewCreated$2
            final /* synthetic */ CustomChatColorCreatorViewModel $viewModel;

            /* access modifiers changed from: package-private */
            {
                this.$viewModel = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                invoke(num.intValue());
                return Unit.INSTANCE;
            }

            public final void invoke(int i3) {
                this.$viewModel.setHueProgress(i3);
            }
        }));
        AppCompatSeekBar appCompatSeekBar9 = this.saturationSlider;
        if (appCompatSeekBar9 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("saturationSlider");
            appCompatSeekBar = null;
        } else {
            appCompatSeekBar = appCompatSeekBar9;
        }
        appCompatSeekBar.setOnSeekBarChangeListener(new OnProgressChangedListener(new Function1<Integer, Unit>(customChatColorCreatorViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$onViewCreated$3
            final /* synthetic */ CustomChatColorCreatorViewModel $viewModel;

            /* access modifiers changed from: package-private */
            {
                this.$viewModel = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                invoke(num.intValue());
                return Unit.INSTANCE;
            }

            public final void invoke(int i3) {
                this.$viewModel.setSaturationProgress(i3);
            }
        }));
        customChatColorCreatorViewModel.getEvents().observe(getViewLifecycleOwner(), new Observer(this, orCreate) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ CustomChatColorCreatorPageFragment f$1;
            public final /* synthetic */ ChatColorSelectionViewModel f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CustomChatColorCreatorPageFragment.$r8$lambda$pDBl0HwTFR1oR55bop02YWyRYE0(CustomChatColorCreatorViewModel.this, this.f$1, this.f$2, (CustomChatColorCreatorViewModel.Event) obj);
            }
        });
        customChatColorCreatorViewModel.getState().observe(getViewLifecycleOwner(), new Observer(thumbDrawable, thumbDrawable2, gradientDrawable2, i, findViewById3, customChatColorGradientToolView, customChatColorCreatorViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ CustomChatColorCreatorPageFragment.ThumbDrawable f$1;
            public final /* synthetic */ CustomChatColorCreatorPageFragment.ThumbDrawable f$2;
            public final /* synthetic */ GradientDrawable f$3;
            public final /* synthetic */ int f$4;
            public final /* synthetic */ View f$5;
            public final /* synthetic */ CustomChatColorGradientToolView f$6;
            public final /* synthetic */ CustomChatColorCreatorViewModel f$7;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CustomChatColorCreatorPageFragment.$r8$lambda$J9F54mqK0tUbuZy2FGHSDMvzwpM(CustomChatColorCreatorPageFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, (CustomChatColorCreatorState) obj);
            }
        });
        if (i == 1 && SignalStore.chatColorsValues().getShouldShowGradientTooltip()) {
            view.post(new Runnable(view, customChatColorGradientToolView) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda6
                public final /* synthetic */ View f$1;
                public final /* synthetic */ CustomChatColorGradientToolView f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    CustomChatColorCreatorPageFragment.$r8$lambda$M_c2p4eeZ9l_toZxRqzfS1mguwc(CustomChatColorCreatorPageFragment.this, this.f$1, this.f$2);
                }
            });
        }
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m1532onViewCreated$lambda4(CustomChatColorCreatorViewModel customChatColorCreatorViewModel, CustomChatColorCreatorPageFragment customChatColorCreatorPageFragment, ChatColorSelectionViewModel chatColorSelectionViewModel, CustomChatColorCreatorViewModel.Event event) {
        Intrinsics.checkNotNullParameter(customChatColorCreatorViewModel, "$viewModel");
        Intrinsics.checkNotNullParameter(customChatColorCreatorPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(chatColorSelectionViewModel, "$chatColorSelectionViewModel");
        if (event instanceof CustomChatColorCreatorViewModel.Event.SaveNow) {
            customChatColorCreatorViewModel.saveNow(((CustomChatColorCreatorViewModel.Event.SaveNow) event).getChatColors(), new Function1<ChatColors, Unit>(chatColorSelectionViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$onViewCreated$4$1
                final /* synthetic */ ChatColorSelectionViewModel $chatColorSelectionViewModel;

                /* access modifiers changed from: package-private */
                {
                    this.$chatColorSelectionViewModel = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(ChatColors chatColors) {
                    invoke(chatColors);
                    return Unit.INSTANCE;
                }

                public final void invoke(ChatColors chatColors) {
                    Intrinsics.checkNotNullParameter(chatColors, "colors");
                    this.$chatColorSelectionViewModel.save(chatColors);
                }
            });
            Navigation.findNavController(customChatColorCreatorPageFragment.requireParentFragment().requireView()).popBackStack();
        } else if (event instanceof CustomChatColorCreatorViewModel.Event.Warn) {
            CustomChatColorCreatorViewModel.Event.Warn warn = (CustomChatColorCreatorViewModel.Event.Warn) event;
            new MaterialAlertDialogBuilder(customChatColorCreatorPageFragment.requireContext()).setTitle(R.string.CustomChatColorCreatorFragmentPage__edit_color).setMessage((CharSequence) customChatColorCreatorPageFragment.getResources().getQuantityString(R.plurals.CustomChatColorCreatorFragmentPage__this_color_is_used, warn.getUsageCount(), Integer.valueOf(warn.getUsageCount()))).setPositiveButton(R.string.save, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(event, customChatColorCreatorPageFragment, chatColorSelectionViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ CustomChatColorCreatorViewModel.Event f$1;
                public final /* synthetic */ CustomChatColorCreatorPageFragment f$2;
                public final /* synthetic */ ChatColorSelectionViewModel f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    CustomChatColorCreatorPageFragment.$r8$lambda$ljQTHeIA2X1ntEPiytkUdzVQ2fQ(CustomChatColorCreatorViewModel.this, this.f$1, this.f$2, this.f$3, dialogInterface, i);
                }
            }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda3
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    CustomChatColorCreatorPageFragment.$r8$lambda$Ta1DKdOKXLKFIHUohI1H4f5UBDY(dialogInterface, i);
                }
            }).show();
        }
    }

    /* renamed from: onViewCreated$lambda-4$lambda-2 */
    public static final void m1533onViewCreated$lambda4$lambda2(CustomChatColorCreatorViewModel customChatColorCreatorViewModel, CustomChatColorCreatorViewModel.Event event, CustomChatColorCreatorPageFragment customChatColorCreatorPageFragment, ChatColorSelectionViewModel chatColorSelectionViewModel, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(customChatColorCreatorViewModel, "$viewModel");
        Intrinsics.checkNotNullParameter(customChatColorCreatorPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(chatColorSelectionViewModel, "$chatColorSelectionViewModel");
        dialogInterface.dismiss();
        customChatColorCreatorViewModel.saveNow(((CustomChatColorCreatorViewModel.Event.Warn) event).getChatColors(), new Function1<ChatColors, Unit>(chatColorSelectionViewModel) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$onViewCreated$4$2$1
            final /* synthetic */ ChatColorSelectionViewModel $chatColorSelectionViewModel;

            /* access modifiers changed from: package-private */
            {
                this.$chatColorSelectionViewModel = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ChatColors chatColors) {
                invoke(chatColors);
                return Unit.INSTANCE;
            }

            public final void invoke(ChatColors chatColors) {
                Intrinsics.checkNotNullParameter(chatColors, "colors");
                this.$chatColorSelectionViewModel.save(chatColors);
            }
        });
        Navigation.findNavController(customChatColorCreatorPageFragment.requireParentFragment().requireView()).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-4$lambda-3 */
    public static final void m1534onViewCreated$lambda4$lambda3(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final void m1535onViewCreated$lambda7(CustomChatColorCreatorPageFragment customChatColorCreatorPageFragment, ThumbDrawable thumbDrawable, ThumbDrawable thumbDrawable2, GradientDrawable gradientDrawable, int i, View view, CustomChatColorGradientToolView customChatColorGradientToolView, CustomChatColorCreatorViewModel customChatColorCreatorViewModel, CustomChatColorCreatorState customChatColorCreatorState) {
        Intrinsics.checkNotNullParameter(customChatColorCreatorPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(thumbDrawable, "$hueThumb");
        Intrinsics.checkNotNullParameter(thumbDrawable2, "$saturationThumb");
        Intrinsics.checkNotNullParameter(gradientDrawable, "$saturationProgressDrawable");
        Intrinsics.checkNotNullParameter(view, "$save");
        Intrinsics.checkNotNullParameter(customChatColorGradientToolView, "$gradientTool");
        Intrinsics.checkNotNullParameter(customChatColorCreatorViewModel, "$viewModel");
        if (!customChatColorCreatorState.getLoading()) {
            ColorSlidersState colorSlidersState = customChatColorCreatorState.getSliderStates().get(customChatColorCreatorState.getSelectedEdge());
            if (colorSlidersState != null) {
                ColorSlidersState colorSlidersState2 = colorSlidersState;
                AppCompatSeekBar appCompatSeekBar = customChatColorCreatorPageFragment.hueSlider;
                ChatColorPreviewView chatColorPreviewView = null;
                if (appCompatSeekBar == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("hueSlider");
                    appCompatSeekBar = null;
                }
                appCompatSeekBar.setProgress(colorSlidersState2.getHuePosition());
                AppCompatSeekBar appCompatSeekBar2 = customChatColorCreatorPageFragment.saturationSlider;
                if (appCompatSeekBar2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("saturationSlider");
                    appCompatSeekBar2 = null;
                }
                appCompatSeekBar2.setProgress(colorSlidersState2.getSaturationPosition());
                int color = customChatColorCreatorPageFragment.getColor(colorSlidersState2);
                thumbDrawable.setColor(customChatColorCreatorPageFragment.getHueColor(colorSlidersState2));
                thumbDrawable2.setColor(color);
                gradientDrawable.setColors(customChatColorCreatorPageFragment.getSaturationColors(colorSlidersState2));
                ChatColorPreviewView chatColorPreviewView2 = customChatColorCreatorPageFragment.preview;
                if (chatColorPreviewView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("preview");
                    chatColorPreviewView2 = null;
                }
                chatColorPreviewView2.setWallpaper(customChatColorCreatorState.getWallpaper());
                if (i == 0) {
                    ChatColors forColor = ChatColors.Companion.forColor(ChatColors.Id.NotSet.INSTANCE, color);
                    ChatColorPreviewView chatColorPreviewView3 = customChatColorCreatorPageFragment.preview;
                    if (chatColorPreviewView3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("preview");
                    } else {
                        chatColorPreviewView = chatColorPreviewView3;
                    }
                    chatColorPreviewView.setChatColors(forColor);
                    view.setOnClickListener(new View.OnClickListener(forColor) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda0
                        public final /* synthetic */ ChatColors f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // android.view.View.OnClickListener
                        public final void onClick(View view2) {
                            CustomChatColorCreatorPageFragment.m1531$r8$lambda$5NjhqbgReRIFUhs5krnpwbZlc(CustomChatColorCreatorViewModel.this, this.f$1, view2);
                        }
                    });
                    return;
                }
                ColorSlidersState colorSlidersState3 = customChatColorCreatorState.getSliderStates().get(CustomChatColorEdge.TOP);
                if (colorSlidersState3 != null) {
                    ColorSlidersState colorSlidersState4 = colorSlidersState3;
                    ColorSlidersState colorSlidersState5 = customChatColorCreatorState.getSliderStates().get(CustomChatColorEdge.BOTTOM);
                    if (colorSlidersState5 != null) {
                        ColorSlidersState colorSlidersState6 = colorSlidersState5;
                        ChatColors forGradient = ChatColors.Companion.forGradient(ChatColors.Id.NotSet.INSTANCE, new ChatColors.LinearGradient(customChatColorCreatorState.getDegrees(), new int[]{customChatColorCreatorPageFragment.getColor(colorSlidersState4), customChatColorCreatorPageFragment.getColor(colorSlidersState6)}, new float[]{0.0f, 1.0f}));
                        ChatColorPreviewView chatColorPreviewView4 = customChatColorCreatorPageFragment.preview;
                        if (chatColorPreviewView4 == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("preview");
                        } else {
                            chatColorPreviewView = chatColorPreviewView4;
                        }
                        chatColorPreviewView.setChatColors(forGradient);
                        customChatColorGradientToolView.setSelectedEdge(customChatColorCreatorState.getSelectedEdge());
                        customChatColorGradientToolView.setDegrees(customChatColorCreatorState.getDegrees());
                        customChatColorGradientToolView.setTopColor(customChatColorCreatorPageFragment.getColor(colorSlidersState4));
                        customChatColorGradientToolView.setBottomColor(customChatColorCreatorPageFragment.getColor(colorSlidersState6));
                        view.setOnClickListener(new View.OnClickListener(forGradient) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$$ExternalSyntheticLambda1
                            public final /* synthetic */ ChatColors f$1;

                            {
                                this.f$1 = r2;
                            }

                            @Override // android.view.View.OnClickListener
                            public final void onClick(View view2) {
                                CustomChatColorCreatorPageFragment.$r8$lambda$cFlegyn3Y3AQPMRzqD6qtRDwbrw(CustomChatColorCreatorViewModel.this, this.f$1, view2);
                            }
                        });
                        return;
                    }
                    throw new IllegalArgumentException("Required value was null.".toString());
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* renamed from: onViewCreated$lambda-7$lambda-5 */
    public static final void m1536onViewCreated$lambda7$lambda5(CustomChatColorCreatorViewModel customChatColorCreatorViewModel, ChatColors chatColors, View view) {
        Intrinsics.checkNotNullParameter(customChatColorCreatorViewModel, "$viewModel");
        Intrinsics.checkNotNullParameter(chatColors, "$chatColor");
        customChatColorCreatorViewModel.startSave(chatColors);
    }

    /* renamed from: onViewCreated$lambda-7$lambda-6 */
    public static final void m1537onViewCreated$lambda7$lambda6(CustomChatColorCreatorViewModel customChatColorCreatorViewModel, ChatColors chatColors, View view) {
        Intrinsics.checkNotNullParameter(customChatColorCreatorViewModel, "$viewModel");
        Intrinsics.checkNotNullParameter(chatColors, "$chatColor");
        customChatColorCreatorViewModel.startSave(chatColors);
    }

    /* renamed from: onViewCreated$lambda-8 */
    public static final void m1538onViewCreated$lambda8(CustomChatColorCreatorPageFragment customChatColorCreatorPageFragment, View view, CustomChatColorGradientToolView customChatColorGradientToolView) {
        Intrinsics.checkNotNullParameter(customChatColorCreatorPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(view, "$view");
        Intrinsics.checkNotNullParameter(customChatColorGradientToolView, "$gradientTool");
        SignalStore.chatColorsValues().setShouldShowGradientTooltip(false);
        PopupWindow popupWindow = new PopupWindow(customChatColorCreatorPageFragment.getLayoutInflater().inflate(R.layout.gradient_tool_tooltip, (ViewGroup) view, false), -1, -2);
        popupWindow.setOutsideTouchable(false);
        popupWindow.setFocusable(true);
        if (Build.VERSION.SDK_INT > 21) {
            popupWindow.setElevation((float) ViewUtil.dpToPx(8));
        }
        popupWindow.showAsDropDown(customChatColorGradientToolView, 0, (-customChatColorGradientToolView.getMeasuredHeight()) + ViewUtil.dpToPx(48));
    }

    private final CustomChatColorCreatorRepository createRepository() {
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        return new CustomChatColorCreatorRepository(requireContext);
    }

    private final int getHueColor(ColorSlidersState colorSlidersState) {
        float hue = toHue(Integer.valueOf(colorSlidersState.getHuePosition()), 1023);
        return ColorUtils.HSLToColor(new float[]{hue, 1.0f, calculateLightness(hue, 0.4f)});
    }

    private final int getColor(ColorSlidersState colorSlidersState) {
        float hue = toHue(Integer.valueOf(colorSlidersState.getHuePosition()), 1023);
        return ColorUtils.HSLToColor(new float[]{hue, toUnit(Integer.valueOf(colorSlidersState.getSaturationPosition()), 1023), calculateLightness$default(this, hue, 0.0f, 2, null)});
    }

    private final int[] getSaturationColors(ColorSlidersState colorSlidersState) {
        float hue = toHue(Integer.valueOf(colorSlidersState.getHuePosition()), 1023);
        float calculateLightness$default = calculateLightness$default(this, hue, 0.0f, 2, null);
        List<Number> list = CollectionsKt__CollectionsKt.listOf((Object[]) new Float[]{Float.valueOf(0.0f), Float.valueOf(1.0f)});
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Number number : list) {
            arrayList.add(Integer.valueOf(ColorUtils.HSLToColor(new float[]{hue, number.floatValue(), calculateLightness$default})));
        }
        return CollectionsKt___CollectionsKt.toIntArray(arrayList);
    }

    static /* synthetic */ float calculateLightness$default(CustomChatColorCreatorPageFragment customChatColorCreatorPageFragment, float f, float f2, int i, Object obj) {
        if ((i & 2) != 0) {
            f2 = 0.3f;
        }
        return customChatColorCreatorPageFragment.calculateLightness(f, f2);
    }

    private final float calculateLightness(float f, float f2) {
        PointF pointF = new PointF();
        PointF pointF2 = new PointF();
        if (f >= 0.0f && f < 60.0f) {
            pointF.set(0.0f, 0.45f);
            pointF2.set(60.0f, f2);
        } else if (f >= 60.0f && f < 180.0f) {
            return f2;
        } else {
            if (f >= 180.0f && f < 240.0f) {
                pointF.set(180.0f, f2);
                pointF2.set(240.0f, 0.5f);
            } else if (f >= 240.0f && f < 300.0f) {
                pointF.set(240.0f, 0.5f);
                pointF2.set(300.0f, 0.4f);
            } else if (f < 300.0f || f >= 360.0f) {
                return 0.45f;
            } else {
                pointF.set(300.0f, 0.4f);
                pointF2.set(360.0f, 0.45f);
            }
        }
        return interpolate(pointF, pointF2, f);
    }

    private final float interpolate(PointF pointF, PointF pointF2, float f) {
        float f2 = pointF.y;
        float f3 = pointF2.x;
        float f4 = pointF2.y;
        float f5 = pointF.x;
        return ((f2 * (f3 - f)) + (f4 * (f - f5))) / (f3 - f5);
    }

    private final float toHue(Number number, Number number2) {
        return Util.clamp(number.floatValue() * (((float) 360) / number2.floatValue()), 0.0f, 360.0f);
    }

    private final float toUnit(Number number, Number number2) {
        return Util.clamp(number.floatValue() / number2.floatValue(), 0.0f, 1.0f);
    }

    private final Drawable forSeekBar(Drawable drawable) {
        int dpToPx = ViewUtil.dpToPx(8);
        IntRange intRange = new IntRange(1, 8);
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(intRange, 10));
        Iterator<Integer> it = intRange.iterator();
        while (it.hasNext()) {
            ((IntIterator) it).nextInt();
            arrayList.add(Float.valueOf(50.0f));
        }
        return CustomDrawWrapperKt.customizeOnDraw(drawable, new Function2<Drawable, Canvas, Unit>(new RectF(), drawable, dpToPx, new Path(), CollectionsKt___CollectionsKt.toFloatArray(arrayList)) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorPageFragment$forSeekBar$1
            final /* synthetic */ RectF $bounds;
            final /* synthetic */ Path $clipPath;
            final /* synthetic */ int $height;
            final /* synthetic */ float[] $radii;
            final /* synthetic */ Drawable $this_forSeekBar;

            /* access modifiers changed from: package-private */
            {
                this.$bounds = r1;
                this.$this_forSeekBar = r2;
                this.$height = r3;
                this.$clipPath = r4;
                this.$radii = r5;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            @Override // kotlin.jvm.functions.Function2
            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                invoke((Drawable) obj, (Canvas) obj2);
                return Unit.INSTANCE;
            }

            public final void invoke(Drawable drawable2, Canvas canvas) {
                Intrinsics.checkNotNullParameter(drawable2, "wrapped");
                Intrinsics.checkNotNullParameter(canvas, "canvas");
                canvas.save();
                this.$bounds.set(this.$this_forSeekBar.getBounds());
                this.$bounds.inset(0.0f, (((float) this.$height) / 2.0f) + ((float) 1));
                this.$clipPath.rewind();
                this.$clipPath.addRoundRect(this.$bounds, this.$radii, Path.Direction.CW);
                canvas.clipPath(this.$clipPath);
                drawable2.draw(canvas);
                canvas.restore();
            }
        });
    }

    /* compiled from: CustomChatColorCreatorPageFragment.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003¢\u0006\u0002\u0010\u0006J\"\u0010\u0007\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0012\u0010\r\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\u0012\u0010\u000e\u001a\u00020\u00052\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0016R\u001a\u0010\u0002\u001a\u000e\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\u00050\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorPageFragment$OnProgressChangedListener;", "Landroid/widget/SeekBar$OnSeekBarChangeListener;", "updateFn", "Lkotlin/Function1;", "", "", "(Lkotlin/jvm/functions/Function1;)V", "onProgressChanged", "seekBar", "Landroid/widget/SeekBar;", "progress", "fromUser", "", "onStartTrackingTouch", "onStopTrackingTouch", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class OnProgressChangedListener implements SeekBar.OnSeekBarChangeListener {
        private final Function1<Integer, Unit> updateFn;

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onStopTrackingTouch(SeekBar seekBar) {
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        public OnProgressChangedListener(Function1<? super Integer, Unit> function1) {
            Intrinsics.checkNotNullParameter(function1, "updateFn");
            this.updateFn = function1;
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
            this.updateFn.invoke(Integer.valueOf(i));
        }
    }

    /* compiled from: CustomChatColorCreatorPageFragment.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u0000 \u001d2\u00020\u0001:\u0001\u001dB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\bH\u0016J\b\u0010\u0014\u001a\u00020\bH\u0016J\b\u0010\u0015\u001a\u00020\bH\u0016J\u0010\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\bH\u0016J\u0010\u0010\u0018\u001a\u00020\u00102\b\b\u0001\u0010\u0019\u001a\u00020\bJ\u0012\u0010\u001a\u001a\u00020\u00102\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorPageFragment$ThumbDrawable;", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "borderPaint", "Landroid/graphics/Paint;", "borderWidth", "", "innerRadius", "", "paint", "thumbInnerSize", "thumbRadius", "thumbSize", "draw", "", "canvas", "Landroid/graphics/Canvas;", "getIntrinsicHeight", "getIntrinsicWidth", "getOpacity", "setAlpha", "alpha", "setColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setColorFilter", "colorFilter", "Landroid/graphics/ColorFilter;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ThumbDrawable extends Drawable {
        public static final Companion Companion = new Companion(null);
        private static final int THUMB_INNER_SIZE = 16;
        private static final int THUMB_MARGIN = 1;
        private final Paint borderPaint;
        private final int borderWidth;
        private final float innerRadius;
        private final Paint paint;
        private final int thumbInnerSize;
        private final float thumbRadius;
        private final float thumbSize;

        @Override // android.graphics.drawable.Drawable
        public int getOpacity() {
            return -2;
        }

        @Override // android.graphics.drawable.Drawable
        public void setAlpha(int i) {
        }

        @Override // android.graphics.drawable.Drawable
        public void setColorFilter(ColorFilter colorFilter) {
        }

        public ThumbDrawable(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            Paint paint = new Paint(1);
            paint.setColor(ContextCompat.getColor(context, R.color.signal_background_primary));
            this.borderPaint = paint;
            Paint paint2 = new Paint(1);
            paint2.setColor(0);
            this.paint = paint2;
            int dpToPx = ViewUtil.dpToPx(THUMB_MARGIN);
            this.borderWidth = dpToPx;
            int dpToPx2 = ViewUtil.dpToPx(THUMB_INNER_SIZE);
            this.thumbInnerSize = dpToPx2;
            this.innerRadius = ((float) dpToPx2) / 2.0f;
            float f = (float) (dpToPx2 + dpToPx);
            this.thumbSize = f;
            this.thumbRadius = f / 2.0f;
        }

        @Override // android.graphics.drawable.Drawable
        public int getIntrinsicHeight() {
            return ViewUtil.dpToPx(48);
        }

        @Override // android.graphics.drawable.Drawable
        public int getIntrinsicWidth() {
            return ViewUtil.dpToPx(48);
        }

        public final void setColor(int i) {
            this.paint.setColor(i);
            invalidateSelf();
        }

        @Override // android.graphics.drawable.Drawable
        public void draw(Canvas canvas) {
            Intrinsics.checkNotNullParameter(canvas, "canvas");
            canvas.drawCircle((((float) getBounds().width()) / 2.0f) + ((float) getBounds().left), (((float) getBounds().height()) / 2.0f) + ((float) getBounds().top), this.thumbRadius, this.borderPaint);
            canvas.drawCircle((((float) getBounds().width()) / 2.0f) + ((float) getBounds().left), (((float) getBounds().height()) / 2.0f) + ((float) getBounds().top), this.innerRadius, this.paint);
        }

        /* compiled from: CustomChatColorCreatorPageFragment.kt */
        @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0002XD¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorPageFragment$ThumbDrawable$Companion;", "", "()V", "THUMB_INNER_SIZE", "", "THUMB_MARGIN", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }
    }

    /* compiled from: CustomChatColorCreatorPageFragment.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0018\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u000e\u0010\n\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorPageFragment$Companion;", "", "()V", "forGradient", "Landroidx/fragment/app/Fragment;", "bundle", "Landroid/os/Bundle;", "forPage", "page", "", "forSingle", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment forSingle(Bundle bundle) {
            Intrinsics.checkNotNullParameter(bundle, "bundle");
            return forPage(0, bundle);
        }

        public final Fragment forGradient(Bundle bundle) {
            Intrinsics.checkNotNullParameter(bundle, "bundle");
            return forPage(1, bundle);
        }

        private final Fragment forPage(int i, Bundle bundle) {
            CustomChatColorCreatorPageFragment customChatColorCreatorPageFragment = new CustomChatColorCreatorPageFragment();
            Bundle bundle2 = new Bundle();
            bundle2.putInt("page", i);
            bundle2.putAll(bundle);
            customChatColorCreatorPageFragment.setArguments(bundle2);
            return customChatColorCreatorPageFragment;
        }
    }
}
