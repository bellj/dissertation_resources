package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes4.dex */
public class ConversationIntents {
    private static final String BUBBLE_AUTHORITY;
    private static final String EXTRA_BORDERLESS;
    private static final String EXTRA_DISTRIBUTION_TYPE;
    private static final String EXTRA_FIRST_TIME_IN_SELF_CREATED_GROUP;
    private static final String EXTRA_GIFT_BADGE;
    private static final String EXTRA_MEDIA;
    private static final String EXTRA_RECIPIENT;
    private static final String EXTRA_STARTING_POSITION;
    private static final String EXTRA_STICKER;
    private static final String EXTRA_TEXT;
    private static final String EXTRA_THREAD_ID;
    private static final String EXTRA_WITH_SEARCH_OPEN;
    private static final String INTENT_DATA;
    private static final String INTENT_TYPE;

    private ConversationIntents() {
    }

    public static Builder createBuilder(Context context, RecipientId recipientId, long j) {
        return new Builder(context, recipientId, j);
    }

    public static Builder createPopUpBuilder(Context context, RecipientId recipientId, long j) {
        return new Builder(context, ConversationPopupActivity.class, recipientId, j);
    }

    public static Intent createBubbleIntent(Context context, RecipientId recipientId, long j) {
        return new Builder(context, BubbleConversationActivity.class, recipientId, j).build();
    }

    public static boolean isInvalid(Bundle bundle) {
        Uri intentData = getIntentData(bundle);
        if (!isBubbleIntentUri(intentData)) {
            return !bundle.containsKey("recipient_id");
        }
        if (intentData.getQueryParameter("recipient_id") == null) {
            return true;
        }
        return false;
    }

    public static Uri getIntentData(Bundle bundle) {
        return (Uri) bundle.getParcelable(INTENT_DATA);
    }

    public static String getIntentType(Bundle bundle) {
        return bundle.getString(INTENT_TYPE);
    }

    public static Bundle createParentFragmentArguments(Intent intent) {
        Bundle bundle = new Bundle();
        if (intent.getExtras() != null) {
            bundle.putAll(intent.getExtras());
        }
        bundle.putParcelable(INTENT_DATA, intent.getData());
        bundle.putString(INTENT_TYPE, intent.getType());
        return bundle;
    }

    public static boolean isBubbleIntentUri(Uri uri) {
        return uri != null && Objects.equals(uri.getAuthority(), BUBBLE_AUTHORITY);
    }

    /* access modifiers changed from: package-private */
    /* loaded from: classes4.dex */
    public static final class Args {
        private final int distributionType;
        private final String draftText;
        private final boolean firstTimeInSelfCreatedGroup;
        private final Badge giftBadge;
        private final boolean isBorderless;
        private final ArrayList<Media> media;
        private final RecipientId recipientId;
        private final int startingPosition;
        private final StickerLocator stickerLocator;
        private final long threadId;
        private final boolean withSearchOpen;

        public static Args from(Bundle bundle) {
            Uri intentData = ConversationIntents.getIntentData(bundle);
            if (ConversationIntents.isBubbleIntentUri(intentData)) {
                return new Args(RecipientId.from(intentData.getQueryParameter("recipient_id")), Long.parseLong(intentData.getQueryParameter("thread_id")), null, null, null, false, 2, -1, false, false, null);
            }
            String string = bundle.getString("recipient_id");
            Objects.requireNonNull(string);
            return new Args(RecipientId.from(string), bundle.getLong("thread_id", -1), bundle.getString(ConversationIntents.EXTRA_TEXT), bundle.getParcelableArrayList(ConversationIntents.EXTRA_MEDIA), (StickerLocator) bundle.getParcelable(ConversationIntents.EXTRA_STICKER), bundle.getBoolean(ConversationIntents.EXTRA_BORDERLESS, false), bundle.getInt(ConversationIntents.EXTRA_DISTRIBUTION_TYPE, 2), bundle.getInt(ConversationIntents.EXTRA_STARTING_POSITION, -1), bundle.getBoolean(ConversationIntents.EXTRA_FIRST_TIME_IN_SELF_CREATED_GROUP, false), bundle.getBoolean(ConversationIntents.EXTRA_WITH_SEARCH_OPEN, false), (Badge) bundle.getParcelable(ConversationIntents.EXTRA_GIFT_BADGE));
        }

        private Args(RecipientId recipientId, long j, String str, ArrayList<Media> arrayList, StickerLocator stickerLocator, boolean z, int i, int i2, boolean z2, boolean z3, Badge badge) {
            this.recipientId = recipientId;
            this.threadId = j;
            this.draftText = str;
            this.media = arrayList;
            this.stickerLocator = stickerLocator;
            this.isBorderless = z;
            this.distributionType = i;
            this.startingPosition = i2;
            this.firstTimeInSelfCreatedGroup = z2;
            this.withSearchOpen = z3;
            this.giftBadge = badge;
        }

        public RecipientId getRecipientId() {
            return this.recipientId;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public String getDraftText() {
            return this.draftText;
        }

        public ArrayList<Media> getMedia() {
            return this.media;
        }

        public StickerLocator getStickerLocator() {
            return this.stickerLocator;
        }

        public int getDistributionType() {
            return this.distributionType;
        }

        public int getStartingPosition() {
            return this.startingPosition;
        }

        public boolean isBorderless() {
            return this.isBorderless;
        }

        public boolean isFirstTimeInSelfCreatedGroup() {
            return this.firstTimeInSelfCreatedGroup;
        }

        public ChatWallpaper getWallpaper() {
            return Recipient.resolved(this.recipientId).getWallpaper();
        }

        public ChatColors getChatColors() {
            return Recipient.resolved(this.recipientId).getChatColors();
        }

        public boolean isWithSearchOpen() {
            return this.withSearchOpen;
        }

        public Badge getGiftBadge() {
            return this.giftBadge;
        }
    }

    /* loaded from: classes4.dex */
    public static final class Builder {
        private final Context context;
        private final Class<? extends ConversationActivity> conversationActivityClass;
        private String dataType;
        private Uri dataUri;
        private int distributionType;
        private String draftText;
        private boolean firstTimeInSelfCreatedGroup;
        private Badge giftBadge;
        private boolean isBorderless;
        private List<Media> media;
        private final RecipientId recipientId;
        private int startingPosition;
        private StickerLocator stickerLocator;
        private final long threadId;
        private boolean withSearchOpen;

        private Builder(Context context, RecipientId recipientId, long j) {
            this(context, ConversationActivity.class, recipientId, j);
        }

        private Builder(Context context, Class<? extends ConversationActivity> cls, RecipientId recipientId, long j) {
            this.distributionType = 2;
            this.startingPosition = -1;
            this.context = context;
            this.conversationActivityClass = cls;
            this.recipientId = recipientId;
            this.threadId = j;
        }

        public Builder withDraftText(String str) {
            this.draftText = str;
            return this;
        }

        public Builder withMedia(Collection<Media> collection) {
            this.media = collection != null ? new ArrayList(collection) : null;
            return this;
        }

        public Builder withStickerLocator(StickerLocator stickerLocator) {
            this.stickerLocator = stickerLocator;
            return this;
        }

        public Builder asBorderless(boolean z) {
            this.isBorderless = z;
            return this;
        }

        public Builder withDistributionType(int i) {
            this.distributionType = i;
            return this;
        }

        public Builder withStartingPosition(int i) {
            this.startingPosition = i;
            return this;
        }

        public Builder withDataUri(Uri uri) {
            this.dataUri = uri;
            return this;
        }

        public Builder withDataType(String str) {
            this.dataType = str;
            return this;
        }

        public Builder withSearchOpen(boolean z) {
            this.withSearchOpen = z;
            return this;
        }

        public Builder firstTimeInSelfCreatedGroup() {
            this.firstTimeInSelfCreatedGroup = true;
            return this;
        }

        public Builder withGiftBadge(Badge badge) {
            this.giftBadge = badge;
            return this;
        }

        public Intent build() {
            String str;
            if (this.stickerLocator == null || this.media == null) {
                Intent intent = new Intent(this.context, this.conversationActivityClass);
                intent.setAction("android.intent.action.VIEW");
                if (Objects.equals(this.conversationActivityClass, BubbleConversationActivity.class)) {
                    intent.setData(new Uri.Builder().authority(ConversationIntents.BUBBLE_AUTHORITY).appendQueryParameter("recipient_id", this.recipientId.serialize()).appendQueryParameter("thread_id", String.valueOf(this.threadId)).build());
                    return intent;
                }
                intent.putExtra("recipient_id", this.recipientId.serialize());
                intent.putExtra("thread_id", this.threadId);
                intent.putExtra(ConversationIntents.EXTRA_DISTRIBUTION_TYPE, this.distributionType);
                intent.putExtra(ConversationIntents.EXTRA_STARTING_POSITION, this.startingPosition);
                intent.putExtra(ConversationIntents.EXTRA_BORDERLESS, this.isBorderless);
                intent.putExtra(ConversationIntents.EXTRA_FIRST_TIME_IN_SELF_CREATED_GROUP, this.firstTimeInSelfCreatedGroup);
                intent.putExtra(ConversationIntents.EXTRA_WITH_SEARCH_OPEN, this.withSearchOpen);
                intent.putExtra(ConversationIntents.EXTRA_GIFT_BADGE, this.giftBadge);
                String str2 = this.draftText;
                if (str2 != null) {
                    intent.putExtra(ConversationIntents.EXTRA_TEXT, str2);
                }
                if (this.media != null) {
                    intent.putParcelableArrayListExtra(ConversationIntents.EXTRA_MEDIA, new ArrayList<>(this.media));
                }
                StickerLocator stickerLocator = this.stickerLocator;
                if (stickerLocator != null) {
                    intent.putExtra(ConversationIntents.EXTRA_STICKER, stickerLocator);
                }
                Uri uri = this.dataUri;
                if (uri != null && (str = this.dataType) != null) {
                    intent.setDataAndType(uri, str);
                } else if (uri != null) {
                    intent.setData(uri);
                } else {
                    String str3 = this.dataType;
                    if (str3 != null) {
                        intent.setType(str3);
                    }
                }
                return intent;
            }
            throw new IllegalStateException("Cannot have both sticker and media array");
        }
    }
}
