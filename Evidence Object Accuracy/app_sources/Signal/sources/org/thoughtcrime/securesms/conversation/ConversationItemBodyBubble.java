package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import org.thoughtcrime.securesms.components.Outliner;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ConversationItemBodyBubble extends LinearLayout {
    private ClipProjectionDrawable clipProjectionDrawable;
    private List<Outliner> outliners = Collections.emptyList();
    private Projection quoteViewProjection;
    private OnSizeChangedListener sizeChangedListener;
    private Projection videoPlayerProjection;

    /* loaded from: classes4.dex */
    public interface OnSizeChangedListener {
        void onSizeChanged(int i, int i2);
    }

    public ConversationItemBodyBubble(Context context) {
        super(context);
        setLayoutTransition(new BodyBubbleLayoutTransition());
    }

    public ConversationItemBodyBubble(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setLayoutTransition(new BodyBubbleLayoutTransition());
    }

    public ConversationItemBodyBubble(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setLayoutTransition(new BodyBubbleLayoutTransition());
    }

    public void setOutliners(List<Outliner> list) {
        this.outliners = list;
    }

    public void setOnSizeChangedListener(OnSizeChangedListener onSizeChangedListener) {
        this.sizeChangedListener = onSizeChangedListener;
    }

    @Override // android.view.View
    public void setBackground(Drawable drawable) {
        ClipProjectionDrawable clipProjectionDrawable = new ClipProjectionDrawable(drawable);
        this.clipProjectionDrawable = clipProjectionDrawable;
        clipProjectionDrawable.setProjections(getProjections());
        super.setBackground(this.clipProjectionDrawable);
    }

    public void setQuoteViewProjection(Projection projection) {
        Projection projection2 = this.quoteViewProjection;
        if (projection2 != null) {
            projection2.release();
        }
        this.quoteViewProjection = projection;
        this.clipProjectionDrawable.setProjections(getProjections());
    }

    public void setVideoPlayerProjection(Projection projection) {
        Projection projection2 = this.videoPlayerProjection;
        if (projection2 != null) {
            projection2.release();
        }
        this.videoPlayerProjection = projection;
        this.clipProjectionDrawable.setProjections(getProjections());
    }

    public Projection getVideoPlayerProjection() {
        return this.videoPlayerProjection;
    }

    public Set<Projection> getProjections() {
        return (Set) Stream.of(this.quoteViewProjection, this.videoPlayerProjection).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationItemBodyBubble$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationItemBodyBubble$$ExternalSyntheticBackport0.m((Projection) obj);
            }
        }).collect(Collectors.toSet());
    }

    @Override // android.view.View
    public void onDrawForeground(Canvas canvas) {
        super.onDrawForeground(canvas);
        if (!Util.isEmpty(this.outliners)) {
            for (Outliner outliner : this.outliners) {
                outliner.draw(canvas, 0, getMeasuredWidth(), getMeasuredHeight(), 0);
            }
        }
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        if (this.sizeChangedListener != null) {
            post(new Runnable(i, i2) { // from class: org.thoughtcrime.securesms.conversation.ConversationItemBodyBubble$$ExternalSyntheticLambda1
                public final /* synthetic */ int f$1;
                public final /* synthetic */ int f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationItemBodyBubble.this.lambda$onSizeChanged$0(this.f$1, this.f$2);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onSizeChanged$0(int i, int i2) {
        OnSizeChangedListener onSizeChangedListener = this.sizeChangedListener;
        if (onSizeChangedListener != null) {
            onSizeChangedListener.onSizeChanged(i, i2);
        }
    }
}
