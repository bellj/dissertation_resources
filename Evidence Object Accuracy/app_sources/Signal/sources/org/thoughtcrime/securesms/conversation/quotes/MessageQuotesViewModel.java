package org.thoughtcrime.securesms.conversation.quotes;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.List;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.GroupAuthorNameColorHelper;
import org.thoughtcrime.securesms.conversation.colors.NameColor;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: MessageQuotesViewModel.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0014B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0012\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000eJ\u0018\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00130\u00120\u000eR\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesViewModel;", "Landroidx/lifecycle/AndroidViewModel;", "application", "Landroid/app/Application;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "conversationRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Landroid/app/Application;Lorg/thoughtcrime/securesms/database/model/MessageId;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "groupAuthorNameColorHelper", "Lorg/thoughtcrime/securesms/conversation/colors/GroupAuthorNameColorHelper;", "repository", "Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesRepository;", "getMessages", "Lio/reactivex/rxjava3/core/Observable;", "", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getNameColorsMap", "", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageQuotesViewModel extends AndroidViewModel {
    private final RecipientId conversationRecipientId;
    private final GroupAuthorNameColorHelper groupAuthorNameColorHelper = new GroupAuthorNameColorHelper();
    private final MessageId messageId;
    private final MessageQuotesRepository repository = new MessageQuotesRepository();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MessageQuotesViewModel(Application application, MessageId messageId, RecipientId recipientId) {
        super(application);
        Intrinsics.checkNotNullParameter(application, "application");
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        Intrinsics.checkNotNullParameter(recipientId, "conversationRecipientId");
        this.messageId = messageId;
        this.conversationRecipientId = recipientId;
    }

    public final Observable<List<ConversationMessage>> getMessages() {
        MessageQuotesRepository messageQuotesRepository = this.repository;
        Application application = getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        Observable<List<ConversationMessage>> observeOn = messageQuotesRepository.getMessagesInQuoteChain(application, this.messageId).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository\n      .getMes…dSchedulers.mainThread())");
        return observeOn;
    }

    public final Observable<Map<RecipientId, NameColor>> getNameColorsMap() {
        Observable<Map<RecipientId, NameColor>> observeOn = Observable.just(this.conversationRecipientId).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return MessageQuotesViewModel.m1610getNameColorsMap$lambda0(MessageQuotesViewModel.this, (RecipientId) obj);
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "just(conversationRecipie…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: getNameColorsMap$lambda-0 */
    public static final Map m1610getNameColorsMap$lambda0(MessageQuotesViewModel messageQuotesViewModel, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(messageQuotesViewModel, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(conversationRecipientId)");
        if (!resolved.getGroupId().isPresent()) {
            return MapsKt__MapsKt.emptyMap();
        }
        GroupAuthorNameColorHelper groupAuthorNameColorHelper = messageQuotesViewModel.groupAuthorNameColorHelper;
        GroupId groupId = resolved.getGroupId().get();
        Intrinsics.checkNotNullExpressionValue(groupId, "conversationRecipient.groupId.get()");
        return groupAuthorNameColorHelper.getColorMap(groupId);
    }

    /* compiled from: MessageQuotesViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;", "application", "Landroid/app/Application;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "conversationRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Landroid/app/Application;Lorg/thoughtcrime/securesms/database/model/MessageId;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final RecipientId conversationRecipientId;
        private final MessageId messageId;

        public Factory(Application application, MessageId messageId, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(application, "application");
            Intrinsics.checkNotNullParameter(messageId, "messageId");
            Intrinsics.checkNotNullParameter(recipientId, "conversationRecipientId");
            this.application = application;
            this.messageId = messageId;
            this.conversationRecipientId = recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new MessageQuotesViewModel(this.application, this.messageId, this.conversationRecipientId));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.conversation.quotes.MessageQuotesViewModel.Factory.create");
        }
    }
}
