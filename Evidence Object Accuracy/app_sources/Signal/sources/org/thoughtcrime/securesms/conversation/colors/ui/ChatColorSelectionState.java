package org.thoughtcrime.securesms.conversation.colors.ui;

import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: ChatColorSelectionState.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007¢\u0006\u0002\u0010\bJ\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u000f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007HÂ\u0003J1\u0010\u0014\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0007X\u0004¢\u0006\u0002\n\u0000R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionState;", "", "wallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "chatColorOptions", "", "(Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;Ljava/util/List;)V", "chatColorModels", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "getChatColorModels", "()Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getWallpaper", "()Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorSelectionState {
    private final MappingModelList chatColorModels;
    private final List<ChatColors> chatColorOptions;
    private final ChatColors chatColors;
    private final ChatWallpaper wallpaper;

    public ChatColorSelectionState() {
        this(null, null, null, 7, null);
    }

    private final List<ChatColors> component3() {
        return this.chatColorOptions;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ChatColorSelectionState copy$default(ChatColorSelectionState chatColorSelectionState, ChatWallpaper chatWallpaper, ChatColors chatColors, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            chatWallpaper = chatColorSelectionState.wallpaper;
        }
        if ((i & 2) != 0) {
            chatColors = chatColorSelectionState.chatColors;
        }
        if ((i & 4) != 0) {
            list = chatColorSelectionState.chatColorOptions;
        }
        return chatColorSelectionState.copy(chatWallpaper, chatColors, list);
    }

    public final ChatWallpaper component1() {
        return this.wallpaper;
    }

    public final ChatColors component2() {
        return this.chatColors;
    }

    public final ChatColorSelectionState copy(ChatWallpaper chatWallpaper, ChatColors chatColors, List<ChatColors> list) {
        Intrinsics.checkNotNullParameter(list, "chatColorOptions");
        return new ChatColorSelectionState(chatWallpaper, chatColors, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChatColorSelectionState)) {
            return false;
        }
        ChatColorSelectionState chatColorSelectionState = (ChatColorSelectionState) obj;
        return Intrinsics.areEqual(this.wallpaper, chatColorSelectionState.wallpaper) && Intrinsics.areEqual(this.chatColors, chatColorSelectionState.chatColors) && Intrinsics.areEqual(this.chatColorOptions, chatColorSelectionState.chatColorOptions);
    }

    public int hashCode() {
        ChatWallpaper chatWallpaper = this.wallpaper;
        int i = 0;
        int hashCode = (chatWallpaper == null ? 0 : chatWallpaper.hashCode()) * 31;
        ChatColors chatColors = this.chatColors;
        if (chatColors != null) {
            i = chatColors.hashCode();
        }
        return ((hashCode + i) * 31) + this.chatColorOptions.hashCode();
    }

    public String toString() {
        return "ChatColorSelectionState(wallpaper=" + this.wallpaper + ", chatColors=" + this.chatColors + ", chatColorOptions=" + this.chatColorOptions + ')';
    }

    public ChatColorSelectionState(ChatWallpaper chatWallpaper, ChatColors chatColors, List<ChatColors> list) {
        ChatColorMappingModel chatColorMappingModel;
        Intrinsics.checkNotNullParameter(list, "chatColorOptions");
        this.wallpaper = chatWallpaper;
        this.chatColors = chatColors;
        this.chatColorOptions = list;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (ChatColors chatColors2 : list) {
            arrayList.add(new ChatColorMappingModel(chatColors2, Intrinsics.areEqual(chatColors2, this.chatColors), false));
        }
        List list2 = CollectionsKt___CollectionsKt.toList(arrayList);
        ChatWallpaper chatWallpaper2 = this.wallpaper;
        ChatColors.Id id = null;
        if (chatWallpaper2 != null) {
            ChatColors autoChatColors = chatWallpaper2.getAutoChatColors();
            Intrinsics.checkNotNullExpressionValue(autoChatColors, "wallpaper.autoChatColors");
            ChatColors chatColors3 = this.chatColors;
            chatColorMappingModel = new ChatColorMappingModel(autoChatColors, Intrinsics.areEqual(chatColors3 != null ? chatColors3.getId() : id, ChatColors.Id.Auto.INSTANCE), true);
        } else {
            ChatColors chatColors4 = ChatColorsPalette.Bubbles.getDefault();
            ChatColors.Id.Auto auto = ChatColors.Id.Auto.INSTANCE;
            ChatColors withId = chatColors4.withId(auto);
            ChatColors chatColors5 = this.chatColors;
            chatColorMappingModel = new ChatColorMappingModel(withId, Intrinsics.areEqual(chatColors5 != null ? chatColors5.getId() : id, auto), true);
        }
        MappingModelList mappingModelList = new MappingModelList();
        mappingModelList.add(chatColorMappingModel);
        mappingModelList.addAll(list2);
        mappingModelList.add(new CustomColorMappingModel());
        this.chatColorModels = mappingModelList;
    }

    public final ChatWallpaper getWallpaper() {
        return this.wallpaper;
    }

    public final ChatColors getChatColors() {
        return this.chatColors;
    }

    public /* synthetic */ ChatColorSelectionState(ChatWallpaper chatWallpaper, ChatColors chatColors, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : chatWallpaper, (i & 2) != 0 ? null : chatColors, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final MappingModelList getChatColorModels() {
        return this.chatColorModels;
    }
}
