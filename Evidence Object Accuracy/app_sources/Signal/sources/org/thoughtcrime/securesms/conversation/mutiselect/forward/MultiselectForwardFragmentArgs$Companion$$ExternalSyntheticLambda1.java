package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import j$.util.function.Consumer;
import java.util.List;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ Consumer f$0;
    public final /* synthetic */ boolean f$1;
    public final /* synthetic */ List f$2;

    public /* synthetic */ MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda1(Consumer consumer, boolean z, List list) {
        this.f$0 = consumer;
        this.f$1 = z;
        this.f$2 = list;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MultiselectForwardFragmentArgs.Companion.m1581create$lambda9$lambda8(this.f$0, this.f$1, this.f$2);
    }
}
