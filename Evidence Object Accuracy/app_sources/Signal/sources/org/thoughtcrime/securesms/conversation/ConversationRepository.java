package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.os.Build;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.conversation.ConversationData;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceViewedUpdateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ConversationRepository {
    private static final String TAG = Log.tag(ConversationRepository.class);
    private final Context context = ApplicationDependencies.getApplication();

    public boolean canShowAsBubble(long j) {
        Recipient recipientForThreadId;
        if (Build.VERSION.SDK_INT < 30 || (recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(j)) == null || !BubbleUtil.canBubble(this.context, recipientForThreadId.getId(), Long.valueOf(j))) {
            return false;
        }
        return true;
    }

    public ConversationData getConversationData(long j, Recipient recipient, int i) {
        ConversationData.MessageRequestData messageRequestData;
        boolean z;
        boolean z2;
        ThreadDatabase.ConversationMetadata conversationMetadata = SignalDatabase.threads().getConversationMetadata(j);
        int conversationCount = SignalDatabase.mmsSms().getConversationCount(j);
        long lastSeen = conversationMetadata.getLastSeen();
        long lastScrolled = conversationMetadata.getLastScrolled();
        boolean isMessageRequestAccepted = RecipientUtil.isMessageRequestAccepted(this.context, j);
        ConversationData.MessageRequestData messageRequestData2 = new ConversationData.MessageRequestData(isMessageRequestAccepted);
        int messagePositionOnOrAfterTimestamp = lastSeen > 0 ? SignalDatabase.mmsSms().getMessagePositionOnOrAfterTimestamp(j, lastSeen) : 0;
        if (messagePositionOnOrAfterTimestamp <= 0) {
            lastSeen = 0;
        }
        int messagePositionOnOrAfterTimestamp2 = (lastSeen != 0 || lastScrolled <= 0) ? 0 : SignalDatabase.mmsSms().getMessagePositionOnOrAfterTimestamp(j, lastScrolled);
        if (!isMessageRequestAccepted) {
            if (recipient.isGroup()) {
                Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(recipient.getId());
                if (group.isPresent()) {
                    for (Recipient recipient2 : Recipient.resolvedList(group.get().getMembers())) {
                        if (recipient2.isProfileSharing() || recipient2.hasGroupsInCommon()) {
                            if (!recipient2.isSelf()) {
                                z2 = true;
                                break;
                            }
                        }
                    }
                }
                z2 = false;
                z = true;
            } else {
                z2 = recipient.hasGroupsInCommon();
                z = false;
            }
            messageRequestData = new ConversationData.MessageRequestData(isMessageRequestAccepted, z2, z);
        } else {
            messageRequestData = messageRequestData2;
        }
        return new ConversationData(j, lastSeen, messagePositionOnOrAfterTimestamp, messagePositionOnOrAfterTimestamp2, i, conversationCount, messageRequestData, SignalStore.settings().getUniversalExpireTimer() != 0 && recipient.getExpiresInSeconds() == 0 && !recipient.isGroup() && recipient.isRegistered() && (j == -1 || !SignalDatabase.mmsSms().hasMeaningfulMessage(j)));
    }

    public void markGiftBadgeRevealed(long j) {
        SignalExecutors.BOUNDED_IO.execute(new Runnable(j) { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationRepository.$r8$lambda$v0y1rQLjmQo_ydliMF_xh6itIvU(this.f$0);
            }
        });
    }

    public static /* synthetic */ void lambda$markGiftBadgeRevealed$0(long j) {
        List<MessageDatabase.MarkedMessageInfo> outgoingGiftsRevealed = SignalDatabase.mms().setOutgoingGiftsRevealed(Collections.singletonList(Long.valueOf(j)));
        if (!outgoingGiftsRevealed.isEmpty()) {
            Log.d(TAG, "Marked gift badge revealed. Sending view sync message.");
            MultiDeviceViewedUpdateJob.enqueue((List) Collection$EL.stream(outgoingGiftsRevealed).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda5
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ((MessageDatabase.MarkedMessageInfo) obj).getSyncMessageId();
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toList()));
        }
    }

    public /* synthetic */ Boolean lambda$checkIfMmsIsEnabled$1() throws Exception {
        return Boolean.valueOf(Util.isMmsCapable(this.context));
    }

    public Single<Boolean> checkIfMmsIsEnabled() {
        return Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda1
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ConversationRepository.$r8$lambda$S5eGsI3YzqUK6Ef8qFeIKvRWgfg(ConversationRepository.this);
            }
        }).subscribeOn(Schedulers.io());
    }

    public Observable<ConversationSecurityInfo> getSecurityInfo(RecipientId recipientId) {
        return Recipient.observable(recipientId).switchMapSingle(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationRepository.$r8$lambda$dZXof3rqG_PkT2lIxDYUtd5wkA0(ConversationRepository.this, (Recipient) obj);
            }
        }).subscribeOn(Schedulers.io());
    }

    public Single<ConversationSecurityInfo> getSecurityInfo(Recipient recipient) {
        return Single.fromCallable(new Callable(recipient) { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ConversationRepository.m1458$r8$lambda$SOP89aTjoJcnNcmS9p8A2nQSL8(ConversationRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
    }

    public /* synthetic */ ConversationSecurityInfo lambda$getSecurityInfo$2(Recipient recipient) throws Exception {
        RecipientDatabase.RegisteredState registeredState;
        String str = TAG;
        Log.i(str, "Resolving registered state...");
        if (recipient.isPushGroup()) {
            Log.i(str, "Push group recipient...");
            registeredState = RecipientDatabase.RegisteredState.REGISTERED;
        } else {
            Log.i(str, "Checking through resolved recipient");
            registeredState = recipient.resolve().getRegistered();
        }
        Log.i(str, "Resolved registered state: " + registeredState);
        boolean isRegistered = Recipient.self().isRegistered();
        boolean z = false;
        if (registeredState == RecipientDatabase.RegisteredState.UNKNOWN) {
            try {
                Log.i(str, "Refreshing directory for user: " + recipient.getId().serialize());
                registeredState = ContactDiscovery.refresh(this.context, recipient, false);
            } catch (IOException e) {
                Log.w(TAG, e);
            }
        }
        Log.i(TAG, "Returning registered state...");
        RecipientId id = recipient.getId();
        if (registeredState == RecipientDatabase.RegisteredState.REGISTERED && isRegistered) {
            z = true;
        }
        return new ConversationSecurityInfo(id, z, Util.isDefaultSmsProvider(this.context), true);
    }

    public Observable<Optional<ThreadRecord>> getThreadRecord(long j) {
        if (j == -1) {
            return Observable.just(Optional.empty());
        }
        return Observable.create(new ObservableOnSubscribe(j) { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                ConversationRepository.m1460$r8$lambda$vkJdrShnX8cdUWnDwbtVbHvJKE(this.f$0, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
    }

    public static /* synthetic */ void lambda$getThreadRecord$5(long j, ObservableEmitter observableEmitter) throws Throwable {
        ConversationRepository$$ExternalSyntheticLambda2 conversationRepository$$ExternalSyntheticLambda2 = new DatabaseObserver.Observer(j) { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ConversationRepository.m1459$r8$lambda$Sg3UaBC4dUJK8RE5Fi1kViaeHM(ObservableEmitter.this, this.f$1);
            }
        };
        ApplicationDependencies.getDatabaseObserver().registerConversationObserver(j, conversationRepository$$ExternalSyntheticLambda2);
        observableEmitter.setCancellable(new Cancellable() { // from class: org.thoughtcrime.securesms.conversation.ConversationRepository$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                ConversationRepository.$r8$lambda$Ul4pMcRCsifDbfQkZi0kBMgtA_4(DatabaseObserver.Observer.this);
            }
        });
        conversationRepository$$ExternalSyntheticLambda2.onChanged();
    }

    public static /* synthetic */ void lambda$getThreadRecord$3(ObservableEmitter observableEmitter, long j) {
        observableEmitter.onNext(Optional.ofNullable(SignalDatabase.threads().getThreadRecord(Long.valueOf(j))));
    }

    public static /* synthetic */ void lambda$getThreadRecord$4(DatabaseObserver.Observer observer) throws Throwable {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(observer);
    }
}
