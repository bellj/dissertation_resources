package org.thoughtcrime.securesms.conversation;

import android.graphics.Bitmap;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$9$$ExternalSyntheticLambda0 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ Bitmap f$0;

    public /* synthetic */ ConversationParentFragment$9$$ExternalSyntheticLambda0(Bitmap bitmap) {
        this.f$0 = bitmap;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return ConversationParentFragment.AnonymousClass9.lambda$onResourceReady$2(this.f$0);
    }
}
