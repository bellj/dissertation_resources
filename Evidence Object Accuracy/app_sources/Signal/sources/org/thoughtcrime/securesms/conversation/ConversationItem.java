package org.thoughtcrime.securesms.conversation;

import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.text.Annotation;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.BackgroundColorSpan;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.core.text.util.LinkifyCompat;
import androidx.lifecycle.LifecycleOwner;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.annimon.stream.function.Predicate;
import com.google.android.exoplayer2.MediaItem;
import com.google.common.collect.Sets;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BindableConversationItem;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.gifts.GiftMessageView;
import org.thoughtcrime.securesms.badges.gifts.OpenableGift;
import org.thoughtcrime.securesms.components.AlertView;
import org.thoughtcrime.securesms.components.AudioView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.BorderlessImageView;
import org.thoughtcrime.securesms.components.ConversationItemFooter;
import org.thoughtcrime.securesms.components.ConversationItemThumbnail;
import org.thoughtcrime.securesms.components.DocumentView;
import org.thoughtcrime.securesms.components.LinkPreviewView;
import org.thoughtcrime.securesms.components.Outliner;
import org.thoughtcrime.securesms.components.PlaybackSpeedToggleTextView;
import org.thoughtcrime.securesms.components.QuoteView;
import org.thoughtcrime.securesms.components.SharedContactView;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.conversation.ConversationItemBodyBubble;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.Quote;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicy;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicyEnforcer;
import org.thoughtcrime.securesms.jobs.AttachmentDownloadJob;
import org.thoughtcrime.securesms.jobs.MmsDownloadJob;
import org.thoughtcrime.securesms.jobs.MmsSendJob;
import org.thoughtcrime.securesms.jobs.SmsSendJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.mms.SlidesClickedListener;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.reactions.ReactionsConversationView;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageView;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.InterceptableLongClickCopyLinkSpan;
import org.thoughtcrime.securesms.util.LinkUtil;
import org.thoughtcrime.securesms.util.LongClickMovementMethod;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.PlaceholderURLSpan;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;
import org.thoughtcrime.securesms.util.SearchUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.UrlClickHandler;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.VibrateUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.NullableStub;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public final class ConversationItem extends RelativeLayout implements BindableConversationItem, RecipientForeverObserver, OpenableGift {
    private static final int CONDENSED_MODE_MAX_LINES;
    public static final float LONG_PRESS_SCALE_FACTOR;
    private static final long MAX_CLUSTERING_TIME_DIFF = TimeUnit.MINUTES.toMillis(3);
    private static final int MAX_MEASURE_CALLS;
    private static final int SHRINK_BUBBLE_DELAY_MILLIS;
    private static final Rect SWIPE_RECT = new Rect();
    private static final String TAG = Log.tag(ConversationItem.class);
    private AlertView alertView;
    private Stub<AudioView> audioViewStub;
    protected BadgeImageView badgeImageView;
    private Set<MultiselectPart> batchSelected;
    protected ConversationItemBodyBubble bodyBubble;
    private Projection.Corners bodyBubbleCorners;
    private EmojiTextView bodyText;
    private Stub<Button> callToActionStub;
    private boolean canPlayContent;
    private Colorizer colorizer;
    private ProjectionList colorizerProjections;
    private AvatarImageView contactPhoto;
    protected ViewGroup contactPhotoHolder;
    private final Context context;
    private ConversationMessage conversationMessage;
    private LiveRecipient conversationRecipient;
    private int defaultBubbleColor;
    private int defaultBubbleColorForWallpaper;
    private Stub<DocumentView> documentViewStub;
    private final AttachmentDownloadClickListener downloadClickListener;
    private BindableConversationItem.EventListener eventListener;
    private ConversationItemFooter footer;
    private final GiftMessageViewCallback giftMessageViewCallback;
    private Stub<GiftMessageView> giftViewStub;
    private GlideRequests glideRequests;
    private TextView groupSender;
    private View groupSenderHolder;
    private boolean groupThread;
    private boolean hasWallpaper;
    private boolean isCondensedMode;
    private float lastYDownRelativeToThis;
    private final LinkPreviewClickListener linkPreviewClickListener;
    private Stub<LinkPreviewView> linkPreviewStub;
    private Locale locale;
    private int measureCalls;
    private MediaItem mediaItem;
    private NullableStub<ConversationItemThumbnail> mediaThumbnailStub;
    private MessageRecord messageRecord;
    private Optional<MessageRecord> nextMessageRecord;
    private Outliner outliner;
    private List<Outliner> outliners;
    private final PassthroughClickListener passthroughClickListener;
    private Optional<MessageRecord> previousMessage;
    private Outliner pulseOutliner;
    private ValueAnimator pulseOutlinerAlphaAnimator;
    private QuoteView quoteView;
    protected View quotedIndicator;
    private final QuotedIndicatorClickListener quotedIndicatorClickListener;
    protected ReactionsConversationView reactionsView;
    private LiveRecipient recipient;
    protected View reply;
    protected View replyIcon;
    private final ViewOnceMessageClickListener revealableClickListener;
    private Stub<ViewOnceMessageView> revealableStub;
    private final SharedContactClickListener sharedContactClickListener;
    private final SharedContactEventListener sharedContactEventListener;
    private Stub<SharedContactView> sharedContactStub;
    private final Runnable shrinkBubble;
    private final SlideClickPassthroughListener singleDownloadClickListener;
    private ConversationItemFooter stickerFooter;
    private Stub<BorderlessImageView> stickerStub;
    private TextView storyReactionLabel;
    private View storyReactionLabelWrapper;
    private final Rect thumbnailMaskingRect;
    private final TouchDelegateChangedListener touchDelegateChangedListener;
    private boolean updatingFooter;
    private final UrlClickListener urlClickListener;

    public static /* synthetic */ void lambda$getLongMessageSpan$11() {
    }

    public ConversationItem(Context context) {
        this(context, null);
    }

    public ConversationItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.batchSelected = new HashSet();
        this.outliner = new Outliner();
        this.pulseOutliner = new Outliner();
        this.outliners = new ArrayList(2);
        this.passthroughClickListener = new PassthroughClickListener();
        AttachmentDownloadClickListener attachmentDownloadClickListener = new AttachmentDownloadClickListener();
        this.downloadClickListener = attachmentDownloadClickListener;
        this.singleDownloadClickListener = new SlideClickPassthroughListener(attachmentDownloadClickListener);
        this.sharedContactEventListener = new SharedContactEventListener();
        this.sharedContactClickListener = new SharedContactClickListener();
        this.linkPreviewClickListener = new LinkPreviewClickListener();
        this.revealableClickListener = new ViewOnceMessageClickListener();
        this.quotedIndicatorClickListener = new QuotedIndicatorClickListener();
        this.urlClickListener = new UrlClickListener();
        this.thumbnailMaskingRect = new Rect();
        this.touchDelegateChangedListener = new TouchDelegateChangedListener();
        this.giftMessageViewCallback = new GiftMessageViewCallback();
        this.colorizerProjections = new ProjectionList(3);
        this.shrinkBubble = new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem.1
            @Override // java.lang.Runnable
            public void run() {
                ConversationItem.this.bodyBubble.animate().scaleX(0.95f).scaleY(0.95f).setUpdateListener(new ConversationItem$1$$ExternalSyntheticLambda0(this));
                ConversationItem.this.reactionsView.animate().scaleX(0.95f).scaleY(0.95f);
                View view = ConversationItem.this.quotedIndicator;
                if (view != null) {
                    view.animate().scaleX(0.95f).scaleY(0.95f);
                }
            }

            public /* synthetic */ void lambda$run$0(ValueAnimator valueAnimator) {
                View view = (View) ConversationItem.this.getParent();
                if (view != null) {
                    view.invalidate();
                }
            }
        };
        this.context = context;
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(new ClickListener(onClickListener));
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        initializeAttributes();
        this.bodyText = (EmojiTextView) findViewById(R.id.conversation_item_body);
        this.footer = (ConversationItemFooter) findViewById(R.id.conversation_item_footer);
        this.stickerFooter = (ConversationItemFooter) findViewById(R.id.conversation_item_sticker_footer);
        this.groupSender = (TextView) findViewById(R.id.group_message_sender);
        this.alertView = (AlertView) findViewById(R.id.indicators_parent);
        this.contactPhoto = (AvatarImageView) findViewById(R.id.contact_photo);
        this.contactPhotoHolder = (ViewGroup) findViewById(R.id.contact_photo_container);
        this.bodyBubble = (ConversationItemBodyBubble) findViewById(R.id.body_bubble);
        this.mediaThumbnailStub = new NullableStub<>((ViewStub) findViewById(R.id.image_view_stub));
        this.audioViewStub = new Stub<>((ViewStub) findViewById(R.id.audio_view_stub));
        this.documentViewStub = new Stub<>((ViewStub) findViewById(R.id.document_view_stub));
        this.sharedContactStub = new Stub<>((ViewStub) findViewById(R.id.shared_contact_view_stub));
        this.linkPreviewStub = new Stub<>((ViewStub) findViewById(R.id.link_preview_stub));
        this.stickerStub = new Stub<>((ViewStub) findViewById(R.id.sticker_view_stub));
        this.revealableStub = new Stub<>((ViewStub) findViewById(R.id.revealable_view_stub));
        this.callToActionStub = ViewUtil.findStubById(this, (int) R.id.conversation_item_call_to_action_stub);
        this.groupSenderHolder = findViewById(R.id.group_sender_holder);
        this.quoteView = (QuoteView) findViewById(R.id.quote_view);
        this.reply = findViewById(R.id.reply_icon_wrapper);
        this.replyIcon = findViewById(R.id.reply_icon);
        this.reactionsView = (ReactionsConversationView) findViewById(R.id.reactions_view);
        this.badgeImageView = (BadgeImageView) findViewById(R.id.badge);
        this.storyReactionLabelWrapper = findViewById(R.id.story_reacted_label_holder);
        this.storyReactionLabel = (TextView) findViewById(R.id.story_reacted_label);
        this.giftViewStub = new Stub<>((ViewStub) findViewById(R.id.gift_view_stub));
        this.quotedIndicator = findViewById(R.id.quoted_indicator);
        setOnClickListener(new ClickListener(null));
        this.bodyText.setOnLongClickListener(this.passthroughClickListener);
        this.bodyText.setOnClickListener(this.passthroughClickListener);
        this.footer.setOnTouchDelegateChangedListener(this.touchDelegateChangedListener);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void bind(LifecycleOwner lifecycleOwner, ConversationMessage conversationMessage, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, GlideRequests glideRequests, Locale locale, Set<MultiselectPart> set, Recipient recipient, String str, boolean z, boolean z2, boolean z3, boolean z4, Colorizer colorizer, boolean z5) {
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this);
        }
        LiveRecipient liveRecipient2 = this.conversationRecipient;
        if (liveRecipient2 != null) {
            liveRecipient2.lambda$asObservable$6(this);
        }
        this.lastYDownRelativeToThis = 0.0f;
        Recipient resolve = recipient.resolve();
        this.conversationMessage = conversationMessage;
        this.messageRecord = conversationMessage.getMessageRecord();
        this.nextMessageRecord = optional2;
        this.locale = locale;
        this.glideRequests = glideRequests;
        this.batchSelected = set;
        this.conversationRecipient = resolve.live();
        this.groupThread = resolve.isGroup();
        LiveRecipient live = this.messageRecord.getIndividualRecipient().live();
        this.recipient = live;
        this.canPlayContent = false;
        this.mediaItem = null;
        this.colorizer = colorizer;
        this.isCondensedMode = z5;
        this.previousMessage = optional;
        live.observeForever(this);
        this.conversationRecipient.observeForever(this);
        setGutterSizes(this.messageRecord, this.groupThread);
        setMessageShape(this.messageRecord, optional, optional2, this.groupThread);
        setMediaAttributes(this.messageRecord, optional, optional2, this.groupThread, z2, z3, z4);
        setBodyText(this.messageRecord, str, z3);
        MessageRecord messageRecord = this.messageRecord;
        setBubbleState(messageRecord, messageRecord.getRecipient(), z2, colorizer);
        setInteractionState(conversationMessage, z);
        setStatusIcons(this.messageRecord, z2);
        setContactPhoto(this.recipient.get());
        setGroupMessageStatus(this.messageRecord, this.recipient.get());
        setGroupAuthorColor(this.messageRecord, z2, colorizer);
        setAuthor(this.messageRecord, optional, optional2, this.groupThread, z2);
        MessageRecord messageRecord2 = this.messageRecord;
        setQuote(messageRecord2, optional, optional2, this.groupThread, messageRecord2.getRecipient().getChatColors());
        setMessageSpacing(this.context, this.messageRecord, optional, optional2, this.groupThread);
        setReactions(this.messageRecord);
        setFooter(this.messageRecord, optional2, locale, this.groupThread, z2);
        setStoryReactionLabel(this.messageRecord);
        setHasBeenQuoted(conversationMessage);
        if (this.audioViewStub.resolved()) {
            this.audioViewStub.get().setOnLongClickListener(this.passthroughClickListener);
        }
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void updateSelectedState() {
        setHasBeenQuoted(this.conversationMessage);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void updateTimestamps() {
        getActiveFooter(this.messageRecord).setMessageRecord(this.messageRecord, this.locale);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void updateContactNameColor() {
        setGroupAuthorColor(this.messageRecord, this.hasWallpaper, this.colorizer);
    }

    @Override // android.view.View, android.view.ViewGroup
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        if (this.isCondensedMode) {
            return super.dispatchTouchEvent(motionEvent);
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            getHandler().postDelayed(this.shrinkBubble, 100);
        } else if (action == 1 || action == 3) {
            getHandler().removeCallbacks(this.shrinkBubble);
            this.bodyBubble.animate().scaleX(1.0f).scaleY(1.0f);
            this.reactionsView.animate().scaleX(1.0f).scaleY(1.0f);
            View view = this.quotedIndicator;
            if (view != null) {
                view.animate().scaleX(1.0f).scaleY(1.0f);
            }
        }
        return super.dispatchTouchEvent(motionEvent);
    }

    @Override // android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (motionEvent.getAction() == 0) {
            this.lastYDownRelativeToThis = motionEvent.getY();
        }
        if (this.batchSelected.isEmpty()) {
            return super.onInterceptTouchEvent(motionEvent);
        }
        return true;
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        ConversationSwipeAnimationHelper.update(this, 0.0f, 1.0f);
        unbind();
        super.onDetachedFromWindow();
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void setEventListener(BindableConversationItem.EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public boolean disallowSwipe(float f, float f2) {
        if (!hasAudio(this.messageRecord)) {
            return false;
        }
        Rect rect = SWIPE_RECT;
        this.audioViewStub.get().getSeekBarGlobalVisibleRect(rect);
        return rect.contains((int) f, (int) f2);
    }

    /* JADX WARNING: Removed duplicated region for block: B:73:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0237  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x024b  */
    @Override // android.widget.RelativeLayout, android.view.View
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onMeasure(int r17, int r18) {
        /*
        // Method dump skipped, instructions count: 592
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationItem.onMeasure(int, int):void");
    }

    private int getDefaultTopMarginForRecord(MessageRecord messageRecord, int i, int i2) {
        return (!isStoryReaction(messageRecord) || messageRecord.isRemoteDelete()) ? i : i2;
    }

    @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
    public void onRecipientChanged(Recipient recipient) {
        if (this.conversationRecipient.getId().equals(recipient.getId())) {
            setBubbleState(this.messageRecord, recipient, recipient.hasWallpaper(), this.colorizer);
            QuoteView quoteView = this.quoteView;
            if (quoteView != null) {
                quoteView.setWallpaperEnabled(recipient.hasWallpaper());
            }
            if (this.audioViewStub.resolved()) {
                setAudioViewTint(this.messageRecord);
            }
        }
        if (this.recipient.getId().equals(recipient.getId())) {
            setContactPhoto(recipient);
            setGroupMessageStatus(this.messageRecord, recipient);
        }
    }

    private int getAvailableMessageBubbleWidth(View view) {
        int i;
        if (hasAudio(this.messageRecord)) {
            i = this.audioViewStub.get().getMeasuredWidth() + ViewUtil.getLeftMargin(this.audioViewStub.get()) + ViewUtil.getRightMargin(this.audioViewStub.get());
        } else if (isViewOnceMessage(this.messageRecord) || (!hasThumbnail(this.messageRecord) && !hasBigImageLinkPreview(this.messageRecord))) {
            i = (this.bodyBubble.getMeasuredWidth() - this.bodyBubble.getPaddingLeft()) - this.bodyBubble.getPaddingRight();
        } else {
            i = this.mediaThumbnailStub.require().getMeasuredWidth();
        }
        return Math.min(i, getMaxBubbleWidth()) - (ViewUtil.getLeftMargin(view) + ViewUtil.getRightMargin(view));
    }

    private int getMaxBubbleWidth() {
        int paddingLeft = getPaddingLeft() + getPaddingRight() + ViewUtil.getLeftMargin(this.bodyBubble) + ViewUtil.getRightMargin(this.bodyBubble);
        if (this.groupThread && !this.messageRecord.isOutgoing() && !this.messageRecord.isRemoteDelete()) {
            paddingLeft += this.contactPhoto.getLayoutParams().width + ViewUtil.getLeftMargin(this.contactPhoto) + ViewUtil.getRightMargin(this.contactPhoto);
        }
        return getMeasuredWidth() - paddingLeft;
    }

    private void initializeAttributes() {
        this.defaultBubbleColor = ContextCompat.getColor(this.context, R.color.conversation_item_recv_bubble_color_normal);
        this.defaultBubbleColorForWallpaper = ContextCompat.getColor(this.context, R.color.conversation_item_recv_bubble_color_wallpaper);
    }

    private int getDefaultBubbleColor(boolean z) {
        return z ? this.defaultBubbleColorForWallpaper : this.defaultBubbleColor;
    }

    @Override // org.thoughtcrime.securesms.Unbindable
    public void unbind() {
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this);
        }
        LiveRecipient liveRecipient2 = this.conversationRecipient;
        if (liveRecipient2 != null) {
            liveRecipient2.lambda$asObservable$6(this);
        }
        this.bodyBubble.setVideoPlayerProjection(null);
        this.bodyBubble.setQuoteViewProjection(null);
        cancelPulseOutlinerAnimation();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public MultiselectPart getMultiselectPartForLatestTouch() {
        MultiselectCollection multiselectCollection = this.conversationMessage.getMultiselectCollection();
        if (multiselectCollection.isSingle()) {
            return multiselectCollection.asSingle().getSinglePart();
        }
        MultiselectPart topPart = multiselectCollection.asDouble().getTopPart();
        MultiselectPart bottomPart = multiselectCollection.asDouble().getBottomPart();
        if (hasThumbnail(this.messageRecord)) {
            return isTouchBelowBoundary(this.mediaThumbnailStub.require()) ? bottomPart : topPart;
        }
        if (hasDocument(this.messageRecord)) {
            return isTouchBelowBoundary(this.documentViewStub.get()) ? bottomPart : topPart;
        }
        if (hasAudio(this.messageRecord)) {
            return isTouchBelowBoundary(this.audioViewStub.get()) ? bottomPart : topPart;
        }
        throw new IllegalStateException("Found a situation where we have something other than a thumbnail or a document.");
    }

    private boolean isTouchBelowBoundary(View view) {
        Projection relativeToParent = Projection.relativeToParent(this, view, null);
        return this.lastYDownRelativeToThis > relativeToParent.getY() + ((float) relativeToParent.getHeight());
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public int getTopBoundaryOfMultiselectPart(MultiselectPart multiselectPart) {
        boolean z = multiselectPart instanceof MultiselectPart.Text;
        boolean z2 = multiselectPart instanceof MultiselectPart.Attachments;
        if (hasThumbnail(this.messageRecord) && z2) {
            return getProjectionTop(this.mediaThumbnailStub.require());
        }
        if (hasThumbnail(this.messageRecord) && z) {
            return getProjectionBottom(this.mediaThumbnailStub.require());
        }
        if (hasDocument(this.messageRecord) && z2) {
            return getProjectionTop(this.documentViewStub.get());
        }
        if (hasDocument(this.messageRecord) && z) {
            return getProjectionBottom(this.documentViewStub.get());
        }
        if (hasAudio(this.messageRecord) && z2) {
            return getProjectionTop(this.audioViewStub.get());
        }
        if (hasAudio(this.messageRecord) && z) {
            return getProjectionBottom(this.audioViewStub.get());
        }
        if (hasNoBubble(this.messageRecord)) {
            return getTop();
        }
        return getProjectionTop(this.bodyBubble);
    }

    private static int getProjectionTop(View view) {
        Projection relativeToViewRoot = Projection.relativeToViewRoot(view, null);
        int y = (int) relativeToViewRoot.getY();
        relativeToViewRoot.release();
        return y;
    }

    private static int getProjectionBottom(View view) {
        Projection relativeToViewRoot = Projection.relativeToViewRoot(view, null);
        int y = ((int) relativeToViewRoot.getY()) + relativeToViewRoot.getHeight();
        relativeToViewRoot.release();
        return y;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public int getBottomBoundaryOfMultiselectPart(MultiselectPart multiselectPart) {
        boolean z = multiselectPart instanceof MultiselectPart.Attachments;
        if (z && hasThumbnail(this.messageRecord)) {
            return getProjectionBottom(this.mediaThumbnailStub.require());
        }
        if (z && hasDocument(this.messageRecord)) {
            return getProjectionBottom(this.documentViewStub.get());
        }
        if (z && hasAudio(this.messageRecord)) {
            return getProjectionBottom(this.audioViewStub.get());
        }
        if (hasNoBubble(this.messageRecord)) {
            return getBottom();
        }
        return getProjectionBottom(this.bodyBubble);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public boolean hasNonSelectableMedia() {
        return hasQuote(this.messageRecord) || hasLinkPreview(this.messageRecord);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem, org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public ConversationMessage getConversationMessage() {
        return this.conversationMessage;
    }

    private void setBubbleState(MessageRecord messageRecord, Recipient recipient, boolean z, Colorizer colorizer) {
        this.hasWallpaper = z;
        ViewUtil.updateLayoutParams(this.bodyBubble, -2, -2);
        this.bodyText.setTextColor(colorizer.getIncomingBodyTextColor(this.context, z));
        this.bodyText.setLinkTextColor(colorizer.getIncomingBodyTextColor(this.context, z));
        if (messageRecord.isOutgoing() && !messageRecord.isRemoteDelete()) {
            this.bodyBubble.getBackground().setColorFilter(recipient.getChatColors().getChatBubbleColorFilter());
            this.bodyText.setTextColor(colorizer.getOutgoingBodyTextColor(this.context));
            this.bodyText.setLinkTextColor(colorizer.getOutgoingBodyTextColor(this.context));
            this.footer.setTextColor(colorizer.getOutgoingFooterTextColor(this.context));
            this.footer.setIconColor(colorizer.getOutgoingFooterIconColor(this.context));
            this.footer.setRevealDotColor(colorizer.getOutgoingFooterIconColor(this.context));
            this.footer.setOnlyShowSendingStatus(false, messageRecord);
        } else if (messageRecord.isRemoteDelete()) {
            if (z) {
                this.bodyBubble.getBackground().setColorFilter(ContextCompat.getColor(this.context, R.color.wallpaper_bubble_color), PorterDuff.Mode.SRC_IN);
            } else {
                this.bodyBubble.getBackground().setColorFilter(ContextCompat.getColor(this.context, R.color.signal_background_primary), PorterDuff.Mode.MULTIPLY);
                this.footer.setIconColor(ContextCompat.getColor(this.context, R.color.signal_icon_tint_secondary));
                this.footer.setRevealDotColor(ContextCompat.getColor(this.context, R.color.signal_icon_tint_secondary));
            }
            this.footer.setTextColor(ContextCompat.getColor(this.context, R.color.signal_text_secondary));
            this.footer.setOnlyShowSendingStatus(messageRecord.isRemoteDelete(), messageRecord);
        } else {
            this.bodyBubble.getBackground().setColorFilter(getDefaultBubbleColor(z), PorterDuff.Mode.SRC_IN);
            this.footer.setTextColor(colorizer.getIncomingFooterTextColor(this.context, z));
            this.footer.setIconColor(colorizer.getIncomingFooterIconColor(this.context, z));
            this.footer.setRevealDotColor(colorizer.getIncomingFooterIconColor(this.context, z));
            this.footer.setOnlyShowSendingStatus(false, messageRecord);
        }
        this.outliner.setColor(ContextCompat.getColor(this.context, R.color.signal_text_secondary));
        this.pulseOutliner.setColor(ContextCompat.getColor(getContext(), R.color.signal_inverse_transparent));
        this.pulseOutliner.setStrokeWidth((float) ViewUtil.dpToPx(4));
        this.outliners.clear();
        if (shouldDrawBodyBubbleOutline(messageRecord, z)) {
            this.outliners.add(this.outliner);
        }
        this.outliners.add(this.pulseOutliner);
        this.bodyBubble.setOutliners(this.outliners);
        if (this.audioViewStub.resolved()) {
            setAudioViewTint(messageRecord);
        }
        if (z) {
            this.replyIcon.setBackgroundResource(R.drawable.wallpaper_message_decoration_background);
        } else {
            this.replyIcon.setBackground(null);
        }
    }

    private void setAudioViewTint(MessageRecord messageRecord) {
        if (!hasAudio(messageRecord)) {
            return;
        }
        if (messageRecord.isOutgoing()) {
            this.audioViewStub.get().setTint(getContext().getResources().getColor(R.color.conversation_item_outgoing_audio_foreground_tint));
            this.audioViewStub.get().setProgressAndPlayBackgroundTint(getContext().getResources().getColor(R.color.signal_colorTransparent2));
        } else if (this.hasWallpaper) {
            this.audioViewStub.get().setTint(getContext().getResources().getColor(R.color.conversation_item_incoming_audio_foreground_tint_wallpaper));
            this.audioViewStub.get().setProgressAndPlayBackgroundTint(getContext().getResources().getColor(R.color.conversation_item_incoming_audio_play_pause_background_tint_wallpaper));
        } else {
            this.audioViewStub.get().setTint(getContext().getResources().getColor(R.color.conversation_item_incoming_audio_foreground_tint_normal));
            this.audioViewStub.get().setProgressAndPlayBackgroundTint(getContext().getResources().getColor(R.color.conversation_item_incoming_audio_play_pause_background_tint_normal));
        }
    }

    private void setInteractionState(ConversationMessage conversationMessage, boolean z) {
        boolean z2 = true;
        if (Util.hasItems(Sets.intersection(conversationMessage.getMultiselectCollection().toSet(), this.batchSelected))) {
            setSelected(true);
        } else if (z) {
            setSelected(false);
            startPulseOutlinerAnimation();
        } else {
            setSelected(false);
        }
        if (this.mediaThumbnailStub.resolved()) {
            this.mediaThumbnailStub.require().setFocusable(!shouldInterceptClicks(conversationMessage.getMessageRecord()) && this.batchSelected.isEmpty());
            this.mediaThumbnailStub.require().setClickable(!shouldInterceptClicks(conversationMessage.getMessageRecord()) && this.batchSelected.isEmpty());
            this.mediaThumbnailStub.require().setLongClickable(this.batchSelected.isEmpty());
        }
        if (this.audioViewStub.resolved()) {
            this.audioViewStub.get().setFocusable(!shouldInterceptClicks(conversationMessage.getMessageRecord()) && this.batchSelected.isEmpty());
            this.audioViewStub.get().setClickable(this.batchSelected.isEmpty());
            this.audioViewStub.get().setEnabled(this.batchSelected.isEmpty());
        }
        if (this.documentViewStub.resolved()) {
            DocumentView documentView = this.documentViewStub.get();
            if (shouldInterceptClicks(conversationMessage.getMessageRecord()) || !this.batchSelected.isEmpty()) {
                z2 = false;
            }
            documentView.setFocusable(z2);
            this.documentViewStub.get().setClickable(this.batchSelected.isEmpty());
        }
    }

    private void startPulseOutlinerAnimation() {
        ValueAnimator duration = ValueAnimator.ofInt(0, 102, 0).setDuration(600L);
        this.pulseOutlinerAlphaAnimator = duration;
        duration.setRepeatCount(1);
        this.pulseOutlinerAlphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda13
            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                ConversationItem.this.lambda$startPulseOutlinerAnimation$0(valueAnimator);
            }
        });
        this.pulseOutlinerAlphaAnimator.start();
    }

    public /* synthetic */ void lambda$startPulseOutlinerAnimation$0(ValueAnimator valueAnimator) {
        this.pulseOutliner.setAlpha(((Integer) valueAnimator.getAnimatedValue()).intValue());
        this.bodyBubble.invalidate();
        if (this.mediaThumbnailStub.resolved()) {
            this.mediaThumbnailStub.require().invalidate();
        }
    }

    private void cancelPulseOutlinerAnimation() {
        ValueAnimator valueAnimator = this.pulseOutlinerAlphaAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.pulseOutlinerAlphaAnimator = null;
        }
        this.pulseOutliner.setAlpha(0);
    }

    private boolean shouldDrawBodyBubbleOutline(MessageRecord messageRecord, boolean z) {
        if (z) {
            return false;
        }
        return messageRecord.isRemoteDelete();
    }

    private boolean isContentCondensed() {
        return this.isCondensedMode && !this.previousMessage.isPresent();
    }

    private boolean isStoryReaction(MessageRecord messageRecord) {
        return MessageRecordUtil.isStoryReaction(messageRecord);
    }

    private boolean isCaptionlessMms(MessageRecord messageRecord) {
        return MessageRecordUtil.isCaptionlessMms(messageRecord, this.context);
    }

    private boolean hasAudio(MessageRecord messageRecord) {
        return MessageRecordUtil.hasAudio(messageRecord);
    }

    private boolean hasThumbnail(MessageRecord messageRecord) {
        return MessageRecordUtil.hasThumbnail(messageRecord);
    }

    public boolean hasSticker(MessageRecord messageRecord) {
        return MessageRecordUtil.hasSticker(messageRecord);
    }

    private boolean isBorderless(MessageRecord messageRecord) {
        return MessageRecordUtil.isBorderless(messageRecord, this.context);
    }

    private boolean hasNoBubble(MessageRecord messageRecord) {
        return MessageRecordUtil.hasNoBubble(messageRecord, this.context);
    }

    private boolean hasOnlyThumbnail(MessageRecord messageRecord) {
        return MessageRecordUtil.hasOnlyThumbnail(messageRecord, this.context);
    }

    private boolean hasDocument(MessageRecord messageRecord) {
        return MessageRecordUtil.hasDocument(messageRecord);
    }

    private boolean hasExtraText(MessageRecord messageRecord) {
        return MessageRecordUtil.hasExtraText(messageRecord) || (!messageRecord.isDisplayBodyEmpty(this.context) && isContentCondensed());
    }

    private boolean hasQuote(MessageRecord messageRecord) {
        return MessageRecordUtil.hasQuote(messageRecord);
    }

    private boolean hasSharedContact(MessageRecord messageRecord) {
        return MessageRecordUtil.hasSharedContact(messageRecord);
    }

    private boolean hasLinkPreview(MessageRecord messageRecord) {
        return MessageRecordUtil.hasLinkPreview(messageRecord);
    }

    private boolean hasBigImageLinkPreview(MessageRecord messageRecord) {
        return MessageRecordUtil.hasBigImageLinkPreview(messageRecord, this.context) && !isContentCondensed();
    }

    private boolean isViewOnceMessage(MessageRecord messageRecord) {
        return MessageRecordUtil.isViewOnceMessage(messageRecord);
    }

    private boolean isGiftMessage(MessageRecord messageRecord) {
        return MessageRecordUtil.hasGiftBadge(messageRecord);
    }

    private void setBodyText(MessageRecord messageRecord, String str, boolean z) {
        this.bodyText.setClickable(false);
        this.bodyText.setFocusable(false);
        this.bodyText.setTextSize(2, (float) SignalStore.settings().getMessageFontSize());
        this.bodyText.setMovementMethod(LongClickMovementMethod.getInstance(getContext()));
        if (messageRecord.isRemoteDelete()) {
            String string = this.context.getString(messageRecord.isOutgoing() ? R.string.ConversationItem_you_deleted_this_message : R.string.ConversationItem_this_message_was_deleted);
            SpannableString spannableString = new SpannableString(string);
            spannableString.setSpan(new StyleSpan(2), 0, string.length(), 33);
            spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this.context, R.color.signal_text_primary)), 0, string.length(), 33);
            this.bodyText.setText(spannableString);
            this.bodyText.setVisibility(0);
            this.bodyText.setOverflowText(null);
        } else if (isCaptionlessMms(messageRecord) || isStoryReaction(messageRecord) || isGiftMessage(messageRecord)) {
            this.bodyText.setVisibility(8);
        } else {
            SpannableString displayBody = this.conversationMessage.getDisplayBody(getContext());
            if (z) {
                linkifyMessageBody(displayBody, this.batchSelected.isEmpty());
            }
            Spannable highlightedSpan = SearchUtil.getHighlightedSpan(this.locale, new SearchUtil.StyleFactory() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda8
                @Override // org.thoughtcrime.securesms.util.SearchUtil.StyleFactory
                public final CharacterStyle create() {
                    return ConversationItem.lambda$setBodyText$2();
                }
            }, SearchUtil.getHighlightedSpan(this.locale, new SearchUtil.StyleFactory() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda7
                @Override // org.thoughtcrime.securesms.util.SearchUtil.StyleFactory
                public final CharacterStyle create() {
                    return ConversationItem.lambda$setBodyText$1();
                }
            }, displayBody, str, 0), str, 0);
            if (hasExtraText(messageRecord)) {
                this.bodyText.setOverflowText(getLongMessageSpan(messageRecord));
            } else {
                this.bodyText.setOverflowText(null);
            }
            if (messageRecord.isOutgoing()) {
                this.bodyText.setMentionBackgroundTint(ContextCompat.getColor(this.context, R.color.transparent_black_25));
            } else {
                EmojiTextView emojiTextView = this.bodyText;
                Context context = this.context;
                emojiTextView.setMentionBackgroundTint(ContextCompat.getColor(context, ThemeUtil.isDarkTheme(context) ? R.color.core_grey_60 : R.color.core_grey_20));
            }
            if (isContentCondensed()) {
                this.bodyText.setMaxLines(3);
            } else {
                this.bodyText.setMaxLines(Integer.MAX_VALUE);
            }
            this.bodyText.setText(StringUtil.trim(highlightedSpan));
            this.bodyText.setVisibility(0);
            if (this.conversationMessage.getBottomButton() != null) {
                this.callToActionStub.get().setVisibility(0);
                this.callToActionStub.get().setText(this.conversationMessage.getBottomButton().getLabel());
                this.callToActionStub.get().setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda9
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ConversationItem.this.lambda$setBodyText$3(view);
                    }
                });
            } else if (this.callToActionStub.resolved()) {
                this.callToActionStub.get().setVisibility(8);
            }
        }
    }

    public static /* synthetic */ CharacterStyle lambda$setBodyText$1() {
        return new BackgroundColorSpan(-256);
    }

    public static /* synthetic */ CharacterStyle lambda$setBodyText$2() {
        return new ForegroundColorSpan(-16777216);
    }

    public /* synthetic */ void lambda$setBodyText$3(View view) {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onCallToAction(this.conversationMessage.getBottomButton().getAction());
        }
    }

    private void setMediaAttributes(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z, boolean z2, boolean z3, boolean z4) {
        int i;
        boolean z5 = true;
        boolean z6 = !messageRecord.isFailed();
        ViewUtil.setTopMargin(this.bodyText, readDimen(R.dimen.message_bubble_top_padding));
        this.bodyBubble.setQuoteViewProjection(null);
        this.bodyBubble.setVideoPlayerProjection(null);
        if (this.eventListener != null && this.audioViewStub.resolved()) {
            Log.d(TAG, "setMediaAttributes: unregistering voice note callbacks for audio slide " + this.audioViewStub.get().getAudioSlideUri());
            this.eventListener.onUnregisterVoiceNoteCallbacks(this.audioViewStub.get().getPlaybackStateObserver());
        }
        this.footer.setPlaybackSpeedListener(null);
        if (isViewOnceMessage(messageRecord) && !messageRecord.isRemoteDelete()) {
            this.revealableStub.get().setVisibility(0);
            if (this.mediaThumbnailStub.resolved()) {
                this.mediaThumbnailStub.require().setVisibility(8);
            }
            if (this.audioViewStub.resolved()) {
                this.audioViewStub.get().setVisibility(8);
            }
            if (this.documentViewStub.resolved()) {
                this.documentViewStub.get().setVisibility(8);
            }
            if (this.sharedContactStub.resolved()) {
                this.sharedContactStub.get().setVisibility(8);
            }
            if (this.linkPreviewStub.resolved()) {
                this.linkPreviewStub.get().setVisibility(8);
            }
            if (this.stickerStub.resolved()) {
                this.stickerStub.get().setVisibility(8);
            }
            if (this.giftViewStub.resolved()) {
                this.giftViewStub.get().setVisibility(8);
            }
            this.revealableStub.get().setMessage((MmsMessageRecord) messageRecord, z2);
            this.revealableStub.get().setOnClickListener(this.revealableClickListener);
            this.revealableStub.get().setOnLongClickListener(this.passthroughClickListener);
            updateRevealableMargins(messageRecord, optional, optional2, z);
            this.footer.setVisibility(0);
        } else if (hasSharedContact(messageRecord)) {
            this.sharedContactStub.get().setVisibility(0);
            if (this.audioViewStub.resolved()) {
                this.audioViewStub.get().setVisibility(8);
            }
            if (this.mediaThumbnailStub.resolved()) {
                this.mediaThumbnailStub.require().setVisibility(8);
            }
            if (this.documentViewStub.resolved()) {
                this.documentViewStub.get().setVisibility(8);
            }
            if (this.linkPreviewStub.resolved()) {
                this.linkPreviewStub.get().setVisibility(8);
            }
            if (this.stickerStub.resolved()) {
                this.stickerStub.get().setVisibility(8);
            }
            if (this.revealableStub.resolved()) {
                this.revealableStub.get().setVisibility(8);
            }
            if (this.giftViewStub.resolved()) {
                this.giftViewStub.get().setVisibility(8);
            }
            this.sharedContactStub.get().setContact(((MediaMmsMessageRecord) messageRecord).getSharedContacts().get(0), this.glideRequests, this.locale);
            this.sharedContactStub.get().setEventListener(this.sharedContactEventListener);
            this.sharedContactStub.get().setOnClickListener(this.sharedContactClickListener);
            this.sharedContactStub.get().setOnLongClickListener(this.passthroughClickListener);
            setSharedContactCorners(messageRecord, optional, optional2, z);
            ViewUtil.updateLayoutParams(this.bodyText, -2, -2);
            ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -2, -2);
            this.footer.setVisibility(8);
        } else {
            boolean hasLinkPreview = hasLinkPreview(messageRecord);
            int i2 = R.dimen.media_bubble_max_height;
            int i3 = R.dimen.media_bubble_min_width_with_content;
            if (hasLinkPreview && z3) {
                this.linkPreviewStub.get().setVisibility(0);
                if (this.audioViewStub.resolved()) {
                    this.audioViewStub.get().setVisibility(8);
                }
                if (this.mediaThumbnailStub.resolved()) {
                    this.mediaThumbnailStub.require().setVisibility(8);
                }
                if (this.documentViewStub.resolved()) {
                    this.documentViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.stickerStub.resolved()) {
                    this.stickerStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                if (this.giftViewStub.resolved()) {
                    this.giftViewStub.get().setVisibility(8);
                }
                LinkPreview linkPreview = ((MmsMessageRecord) messageRecord).getLinkPreviews().get(0);
                if (hasBigImageLinkPreview(messageRecord)) {
                    this.mediaThumbnailStub.require().setVisibility(0);
                    this.mediaThumbnailStub.require().setMinimumThumbnailWidth(readDimen(R.dimen.media_bubble_min_width_with_content));
                    this.mediaThumbnailStub.require().setMaximumThumbnailHeight(readDimen(R.dimen.media_bubble_max_height));
                    this.mediaThumbnailStub.require().setImageResource(this.glideRequests, Collections.singletonList(new ImageSlide(this.context, linkPreview.getThumbnail().get())), z6, false);
                    this.mediaThumbnailStub.require().setThumbnailClickListener(new LinkPreviewThumbnailClickListener());
                    this.mediaThumbnailStub.require().setDownloadClickListener(this.downloadClickListener);
                    this.mediaThumbnailStub.require().setOnLongClickListener(this.passthroughClickListener);
                    this.linkPreviewStub.get().setLinkPreview(this.glideRequests, linkPreview, false);
                    setThumbnailCorners(messageRecord, optional, optional2, z);
                    setLinkPreviewCorners(messageRecord, optional, optional2, z, true);
                    ViewUtil.updateLayoutParams(this.bodyText, -1, -2);
                    ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -1, -2);
                    ViewUtil.setTopMargin(this.linkPreviewStub.get(), 0);
                } else {
                    this.linkPreviewStub.get().setLinkPreview(this.glideRequests, linkPreview, true, !isContentCondensed());
                    this.linkPreviewStub.get().setDownloadClickedListener(this.downloadClickListener);
                    setLinkPreviewCorners(messageRecord, optional, optional2, z, false);
                    ViewUtil.updateLayoutParams(this.bodyText, -2, -2);
                    ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -2, -2);
                    ViewUtil.setTopMargin(this.linkPreviewStub.get(), (!z || !isStartOfMessageCluster(messageRecord, optional, z) || messageRecord.isOutgoing()) ? 0 : readDimen(R.dimen.message_bubble_top_padding));
                }
                this.linkPreviewStub.get().setOnClickListener(this.linkPreviewClickListener);
                this.linkPreviewStub.get().setOnLongClickListener(this.passthroughClickListener);
                this.linkPreviewStub.get().setBackgroundColor(getDefaultBubbleColor(z2));
                this.footer.setVisibility(0);
            } else if (hasAudio(messageRecord)) {
                this.audioViewStub.get().setVisibility(0);
                if (this.mediaThumbnailStub.resolved()) {
                    this.mediaThumbnailStub.require().setVisibility(8);
                }
                if (this.documentViewStub.resolved()) {
                    this.documentViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.linkPreviewStub.resolved()) {
                    this.linkPreviewStub.get().setVisibility(8);
                }
                if (this.stickerStub.resolved()) {
                    this.stickerStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                if (this.giftViewStub.resolved()) {
                    this.giftViewStub.get().setVisibility(8);
                }
                AudioSlide audioSlide = ((MediaMmsMessageRecord) messageRecord).getSlideDeck().getAudioSlide();
                Objects.requireNonNull(audioSlide);
                this.audioViewStub.get().setAudio(audioSlide, new AudioViewCallbacks(), z6, true);
                this.audioViewStub.get().setDownloadClickListener(this.singleDownloadClickListener);
                this.audioViewStub.get().setOnLongClickListener(this.passthroughClickListener);
                if (this.eventListener != null) {
                    Log.d(TAG, "setMediaAttributes: registered listener for audio slide " + this.audioViewStub.get().getAudioSlideUri());
                    this.eventListener.onRegisterVoiceNoteCallbacks(this.audioViewStub.get().getPlaybackStateObserver());
                } else {
                    Log.w(TAG, "setMediaAttributes: could not register listener for audio slide " + this.audioViewStub.get().getAudioSlideUri());
                }
                ViewUtil.updateLayoutParams(this.bodyText, -2, -2);
                ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -2, -2);
                this.footer.setPlaybackSpeedListener(new AudioPlaybackSpeedToggleListener());
                this.footer.setVisibility(0);
            } else if (hasDocument(messageRecord)) {
                this.documentViewStub.get().setVisibility(0);
                if (this.mediaThumbnailStub.resolved()) {
                    this.mediaThumbnailStub.require().setVisibility(8);
                }
                if (this.audioViewStub.resolved()) {
                    this.audioViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.linkPreviewStub.resolved()) {
                    this.linkPreviewStub.get().setVisibility(8);
                }
                if (this.stickerStub.resolved()) {
                    this.stickerStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                if (this.giftViewStub.resolved()) {
                    this.giftViewStub.get().setVisibility(8);
                }
                this.documentViewStub.get().setDocument(((MediaMmsMessageRecord) messageRecord).getSlideDeck().getDocumentSlide(), z6);
                this.documentViewStub.get().setDocumentClickListener(new ThumbnailClickListener());
                this.documentViewStub.get().setDownloadClickListener(this.singleDownloadClickListener);
                this.documentViewStub.get().setOnLongClickListener(this.passthroughClickListener);
                ViewUtil.updateLayoutParams(this.bodyText, -2, -2);
                ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -2, -2);
                ViewUtil.setTopMargin(this.bodyText, 0);
                this.footer.setVisibility(0);
            } else if ((hasSticker(messageRecord) && isCaptionlessMms(messageRecord)) || isBorderless(messageRecord)) {
                this.bodyBubble.setBackgroundColor(0);
                this.stickerStub.get().setVisibility(0);
                if (this.mediaThumbnailStub.resolved()) {
                    this.mediaThumbnailStub.require().setVisibility(8);
                }
                if (this.audioViewStub.resolved()) {
                    this.audioViewStub.get().setVisibility(8);
                }
                if (this.documentViewStub.resolved()) {
                    this.documentViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.linkPreviewStub.resolved()) {
                    this.linkPreviewStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                if (this.giftViewStub.resolved()) {
                    this.giftViewStub.get().setVisibility(8);
                }
                if (hasSticker(messageRecord)) {
                    this.stickerStub.get().setSlide(this.glideRequests, ((MmsMessageRecord) messageRecord).getSlideDeck().getStickerSlide());
                    this.stickerStub.get().setThumbnailClickListener(new StickerClickListener());
                } else {
                    this.stickerStub.get().setSlide(this.glideRequests, ((MmsMessageRecord) messageRecord).getSlideDeck().getThumbnailSlide());
                    this.stickerStub.get().setThumbnailClickListener(new SlideClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda12
                        @Override // org.thoughtcrime.securesms.mms.SlideClickListener
                        public final void onClick(View view, Slide slide) {
                            ConversationItem.this.lambda$setMediaAttributes$4(view, slide);
                        }
                    });
                }
                this.stickerStub.get().setDownloadClickListener(this.downloadClickListener);
                this.stickerStub.get().setOnLongClickListener(this.passthroughClickListener);
                this.stickerStub.get().setOnClickListener(this.passthroughClickListener);
                ViewUtil.updateLayoutParams(this.bodyText, -2, -2);
                ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -2, -2);
                this.footer.setVisibility(0);
            } else if (hasNoBubble(messageRecord)) {
                this.bodyBubble.setBackgroundColor(0);
            } else if (hasThumbnail(messageRecord)) {
                this.mediaThumbnailStub.require().setVisibility(0);
                if (this.audioViewStub.resolved()) {
                    this.audioViewStub.get().setVisibility(8);
                }
                if (this.documentViewStub.resolved()) {
                    this.documentViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.linkPreviewStub.resolved()) {
                    this.linkPreviewStub.get().setVisibility(8);
                }
                if (this.stickerStub.resolved()) {
                    this.stickerStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                if (this.giftViewStub.resolved()) {
                    this.giftViewStub.get().setVisibility(8);
                }
                List<Slide> thumbnailSlides = ((MmsMessageRecord) messageRecord).getSlideDeck().getThumbnailSlides();
                ConversationItemThumbnail require = this.mediaThumbnailStub.require();
                if (isCaptionlessMms(messageRecord)) {
                    i3 = R.dimen.media_bubble_min_width_solo;
                }
                require.setMinimumThumbnailWidth(readDimen(i3));
                ConversationItemThumbnail require2 = this.mediaThumbnailStub.require();
                if (isContentCondensed()) {
                    i2 = R.dimen.media_bubble_max_height_condensed;
                }
                require2.setMaximumThumbnailHeight(readDimen(i2));
                this.mediaThumbnailStub.require().setImageResource(this.glideRequests, thumbnailSlides, z6, false);
                this.mediaThumbnailStub.require().setThumbnailClickListener(new ThumbnailClickListener());
                this.mediaThumbnailStub.require().setDownloadClickListener(this.downloadClickListener);
                this.mediaThumbnailStub.require().setOnLongClickListener(this.passthroughClickListener);
                this.mediaThumbnailStub.require().setOnClickListener(this.passthroughClickListener);
                this.mediaThumbnailStub.require().showShade(messageRecord.isDisplayBodyEmpty(getContext()) && !hasExtraText(messageRecord));
                if (!messageRecord.isOutgoing()) {
                    this.mediaThumbnailStub.require().setConversationColor(getDefaultBubbleColor(z2));
                } else {
                    this.mediaThumbnailStub.require().setConversationColor(0);
                }
                this.mediaThumbnailStub.require().setBorderless(false);
                setThumbnailCorners(messageRecord, optional, optional2, z);
                ViewUtil.updateLayoutParams(this.bodyText, -1, -2);
                ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -1, -2);
                this.footer.setVisibility(0);
                if (thumbnailSlides.size() == 1 && thumbnailSlides.get(0).isVideoGif() && (thumbnailSlides.get(0) instanceof VideoSlide)) {
                    Uri uri = thumbnailSlides.get(0).getUri();
                    if (uri != null) {
                        this.mediaItem = MediaItem.fromUri(uri);
                    } else {
                        this.mediaItem = null;
                    }
                    if ((!GiphyMp4PlaybackPolicy.autoplay() && !z4) || this.mediaItem == null) {
                        z5 = false;
                    }
                    this.canPlayContent = z5;
                }
            } else if (isGiftMessage(messageRecord)) {
                if (this.mediaThumbnailStub.resolved()) {
                    this.mediaThumbnailStub.require().setVisibility(8);
                }
                if (this.audioViewStub.resolved()) {
                    this.audioViewStub.get().setVisibility(8);
                }
                if (this.documentViewStub.resolved()) {
                    this.documentViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.linkPreviewStub.resolved()) {
                    this.linkPreviewStub.get().setVisibility(8);
                }
                if (this.stickerStub.resolved()) {
                    this.stickerStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                GlideRequests glideRequests = this.glideRequests;
                GiftBadge giftBadge = ((MmsMessageRecord) messageRecord).getGiftBadge();
                Objects.requireNonNull(giftBadge);
                this.giftViewStub.get().setGiftBadge(glideRequests, giftBadge, messageRecord.isOutgoing(), this.giftMessageViewCallback);
                this.giftViewStub.get().setVisibility(0);
                this.footer.setVisibility(0);
            } else {
                if (this.mediaThumbnailStub.resolved()) {
                    this.mediaThumbnailStub.require().setVisibility(8);
                }
                if (this.audioViewStub.resolved()) {
                    this.audioViewStub.get().setVisibility(8);
                }
                if (this.documentViewStub.resolved()) {
                    this.documentViewStub.get().setVisibility(8);
                }
                if (this.sharedContactStub.resolved()) {
                    this.sharedContactStub.get().setVisibility(8);
                }
                if (this.linkPreviewStub.resolved()) {
                    this.linkPreviewStub.get().setVisibility(8);
                }
                if (this.stickerStub.resolved()) {
                    this.stickerStub.get().setVisibility(8);
                }
                if (this.revealableStub.resolved()) {
                    this.revealableStub.get().setVisibility(8);
                }
                if (this.giftViewStub.resolved()) {
                    this.giftViewStub.get().setVisibility(8);
                }
                ViewUtil.updateLayoutParams(this.bodyText, -2, -2);
                ViewUtil.updateLayoutParamsIfNonNull(this.groupSenderHolder, -2, -2);
                this.footer.setVisibility(0);
                if (messageRecord.isOutgoing() || !z || !isStartOfMessageCluster(messageRecord, optional, z)) {
                    i = readDimen(R.dimen.message_bubble_top_padding);
                } else {
                    i = readDimen(R.dimen.message_bubble_text_only_top_margin);
                }
                ViewUtil.setTopMargin(this.bodyText, i);
            }
        }
    }

    public /* synthetic */ void lambda$setMediaAttributes$4(View view, Slide slide) {
        performClick();
    }

    private void updateRevealableMargins(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z) {
        int readDimen = readDimen(R.dimen.message_bubble_revealable_padding);
        int readDimen2 = readDimen(R.dimen.message_bubble_top_padding);
        if (messageRecord.isOutgoing() || !isStartOfMessageCluster(messageRecord, optional, z)) {
            ViewUtil.setTopMargin(this.revealableStub.get(), readDimen);
        } else {
            ViewUtil.setTopMargin(this.revealableStub.get(), readDimen2);
        }
        if (isFooterVisible(messageRecord, optional2, z)) {
            ViewUtil.setBottomMargin(this.revealableStub.get(), readDimen2);
        } else {
            ViewUtil.setBottomMargin(this.revealableStub.get(), readDimen);
        }
    }

    private void setThumbnailCorners(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z) {
        int i;
        int i2;
        int readDimen = readDimen(R.dimen.message_corner_radius);
        int readDimen2 = readDimen(R.dimen.message_corner_collapse_radius);
        if (isSingularMessage(messageRecord, optional, optional2, z)) {
            i = readDimen;
            readDimen2 = i;
            i2 = readDimen2;
        } else if (isStartOfMessageCluster(messageRecord, optional, z)) {
            if (messageRecord.isOutgoing()) {
                i2 = readDimen;
                i = readDimen2;
                readDimen2 = i2;
            } else {
                i = readDimen;
                i2 = readDimen2;
                readDimen2 = i;
            }
        } else if (isEndOfMessageCluster(messageRecord, optional2, z)) {
            if (messageRecord.isOutgoing()) {
                i = readDimen;
                i2 = i;
            } else {
                i = readDimen;
                i2 = i;
                readDimen = readDimen2;
                readDimen2 = i2;
            }
        } else if (messageRecord.isOutgoing()) {
            i2 = readDimen;
            i = readDimen2;
        } else {
            i = readDimen;
            i2 = readDimen2;
            readDimen = i2;
            readDimen2 = i;
        }
        int i3 = 0;
        if (!messageRecord.isDisplayBodyEmpty(getContext())) {
            i = 0;
            i2 = 0;
        }
        if (isStartOfMessageCluster(messageRecord, optional, z) && !messageRecord.isOutgoing() && z) {
            readDimen = 0;
            readDimen2 = 0;
        }
        if (hasQuote(this.messageRecord)) {
            readDimen = 0;
            readDimen2 = 0;
        }
        if (hasLinkPreview(this.messageRecord) || hasExtraText(this.messageRecord)) {
            i2 = 0;
        } else {
            i3 = i;
        }
        if (ViewUtil.isRtl(this)) {
            this.mediaThumbnailStub.require().setCorners(readDimen2, readDimen, i2, i3);
        } else {
            this.mediaThumbnailStub.require().setCorners(readDimen, readDimen2, i3, i2);
        }
    }

    private void setSharedContactCorners(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z) {
        if (!this.messageRecord.isDisplayBodyEmpty(getContext())) {
            return;
        }
        if (isSingularMessage(messageRecord, optional, optional2, z) || isEndOfMessageCluster(messageRecord, optional2, z)) {
            this.sharedContactStub.get().setSingularStyle();
        } else if (messageRecord.isOutgoing()) {
            this.sharedContactStub.get().setClusteredOutgoingStyle();
        } else {
            this.sharedContactStub.get().setClusteredIncomingStyle();
        }
    }

    private void setLinkPreviewCorners(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z, boolean z2) {
        int readDimen = readDimen(R.dimen.message_corner_radius);
        int readDimen2 = readDimen(R.dimen.message_corner_collapse_radius);
        if (z2 || hasQuote(messageRecord)) {
            this.linkPreviewStub.get().setCorners(0, 0);
        } else if (isStartOfMessageCluster(messageRecord, optional, z) && !messageRecord.isOutgoing() && z) {
            this.linkPreviewStub.get().setCorners(0, 0);
        } else if (isSingularMessage(messageRecord, optional, optional2, z) || isStartOfMessageCluster(messageRecord, optional, z)) {
            this.linkPreviewStub.get().setCorners(readDimen, readDimen);
        } else if (messageRecord.isOutgoing()) {
            this.linkPreviewStub.get().setCorners(readDimen, readDimen2);
        } else {
            this.linkPreviewStub.get().setCorners(readDimen2, readDimen);
        }
    }

    private void setContactPhoto(Recipient recipient) {
        if (this.contactPhoto != null) {
            this.contactPhoto.setOnClickListener(new View.OnClickListener(recipient.getId()) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda11
                public final /* synthetic */ RecipientId f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationItem.this.lambda$setContactPhoto$5(this.f$1, view);
                }
            });
            this.contactPhoto.setAvatar(this.glideRequests, recipient, false);
            this.badgeImageView.setBadgeFromRecipient(recipient, this.glideRequests);
            this.badgeImageView.setClickable(false);
        }
    }

    public /* synthetic */ void lambda$setContactPhoto$5(RecipientId recipientId, View view) {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onGroupMemberClicked(recipientId, this.conversationRecipient.get().requireGroupId());
        }
    }

    private void linkifyMessageBody(Spannable spannable, boolean z) {
        linkifyUrlLinks(spannable, z, this.urlClickListener);
        if (this.conversationMessage.hasStyleLinks()) {
            PlaceholderURLSpan[] placeholderURLSpanArr = (PlaceholderURLSpan[]) spannable.getSpans(0, spannable.length(), PlaceholderURLSpan.class);
            for (PlaceholderURLSpan placeholderURLSpan : placeholderURLSpanArr) {
                spannable.setSpan(new InterceptableLongClickCopyLinkSpan(placeholderURLSpan.getValue(), this.urlClickListener, Integer.valueOf(ContextCompat.getColor(getContext(), R.color.signal_accent_primary)), false), spannable.getSpanStart(placeholderURLSpan), spannable.getSpanEnd(placeholderURLSpan), 33);
            }
        }
        for (Annotation annotation : MentionAnnotation.getMentionAnnotations(spannable)) {
            spannable.setSpan(new MentionClickableSpan(RecipientId.from(annotation.getValue())), spannable.getSpanStart(annotation), spannable.getSpanEnd(annotation), 33);
        }
    }

    static void linkifyUrlLinks(Spannable spannable, boolean z, UrlClickHandler urlClickHandler) {
        if (LinkifyCompat.addLinks(spannable, z ? 7 : 0)) {
            Stream.of((URLSpan[]) spannable.getSpans(0, spannable.length(), URLSpan.class)).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return ConversationItem.lambda$linkifyUrlLinks$6((URLSpan) obj);
                }
            }).forEach(new Consumer(spannable) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda2
                public final /* synthetic */ Spannable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Consumer
                public final void accept(Object obj) {
                    this.f$0.removeSpan((URLSpan) obj);
                }
            });
            URLSpan[] uRLSpanArr = (URLSpan[]) spannable.getSpans(0, spannable.length(), URLSpan.class);
            for (URLSpan uRLSpan : uRLSpanArr) {
                spannable.setSpan(new InterceptableLongClickCopyLinkSpan(uRLSpan.getURL(), urlClickHandler), spannable.getSpanStart(uRLSpan), spannable.getSpanEnd(uRLSpan), 33);
            }
        }
    }

    public static /* synthetic */ boolean lambda$linkifyUrlLinks$6(URLSpan uRLSpan) {
        return LinkUtil.isLegalUrl(uRLSpan.getURL());
    }

    private void setStatusIcons(MessageRecord messageRecord, boolean z) {
        this.bodyText.setCompoundDrawablesWithIntrinsicBounds(0, 0, messageRecord.isKeyExchange() ? R.drawable.ic_menu_login : 0, 0);
        if (messageRecord.isFailed()) {
            this.alertView.setFailed();
        } else if (messageRecord.isPendingInsecureSmsFallback()) {
            this.alertView.setPendingApproval();
        } else if (messageRecord.isRateLimited()) {
            this.alertView.setRateLimited();
        } else {
            this.alertView.setNone();
        }
        if (z) {
            this.alertView.setBackgroundResource(R.drawable.wallpaper_message_decoration_background);
        } else {
            this.alertView.setBackground(null);
        }
    }

    private void setQuote(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z, ChatColors chatColors) {
        boolean isStartOfMessageCluster = isStartOfMessageCluster(messageRecord, optional, z);
        int i = 0;
        if (!hasQuote(this.messageRecord)) {
            QuoteView quoteView = this.quoteView;
            if (quoteView != null) {
                quoteView.dismiss();
            }
            if (!messageRecord.isOutgoing() && isStartOfMessageCluster && this.groupThread) {
                i = readDimen(R.dimen.message_bubble_top_image_margin);
            }
            if (this.mediaThumbnailStub.resolved()) {
                ViewUtil.setTopMargin(this.mediaThumbnailStub.require(), i);
            }
        } else if (this.quoteView != null) {
            MediaMmsMessageRecord mediaMmsMessageRecord = (MediaMmsMessageRecord) messageRecord;
            Quote quote = mediaMmsMessageRecord.getQuote();
            if (mediaMmsMessageRecord.getParentStoryId() != null) {
                this.quoteView.setMessageType(messageRecord.isOutgoing() ? QuoteView.MessageType.STORY_REPLY_OUTGOING : QuoteView.MessageType.STORY_REPLY_INCOMING);
            } else {
                this.quoteView.setMessageType(messageRecord.isOutgoing() ? QuoteView.MessageType.OUTGOING : QuoteView.MessageType.INCOMING);
            }
            this.quoteView.setQuote(this.glideRequests, quote.getId(), Recipient.live(quote.getAuthor()).get(), quote.getDisplayText(), quote.isOriginalMissing(), quote.getAttachment(), isStoryReaction(messageRecord) ? messageRecord.getBody() : null, quote.getQuoteType());
            this.quoteView.setWallpaperEnabled(this.hasWallpaper);
            this.quoteView.setVisibility(0);
            this.quoteView.setTextSize(2, (float) SignalStore.settings().getMessageQuoteFontSize(this.context));
            this.quoteView.getLayoutParams().width = -2;
            this.quoteView.setOnClickListener(new View.OnClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda14
                public final /* synthetic */ MessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationItem.this.lambda$setQuote$7(this.f$1, view);
                }
            });
            this.quoteView.setOnLongClickListener(this.passthroughClickListener);
            if (isStartOfMessageCluster) {
                if (messageRecord.isOutgoing()) {
                    this.quoteView.setTopCornerSizes(true, true);
                } else if (z) {
                    this.quoteView.setTopCornerSizes(false, false);
                } else {
                    this.quoteView.setTopCornerSizes(true, true);
                }
            } else if (!isSingularMessage(messageRecord, optional, optional2, z)) {
                if (messageRecord.isOutgoing()) {
                    this.quoteView.setTopCornerSizes(true, false);
                } else {
                    this.quoteView.setTopCornerSizes(false, true);
                }
            }
            if (isFooterVisible(messageRecord, optional2, z) || !isStoryReaction(messageRecord)) {
                ViewUtil.setBottomMargin(this.quoteView, 0);
            } else {
                ViewUtil.setBottomMargin(this.quoteView, (int) DimensionUnit.DP.toPixels(8.0f));
            }
            if (this.mediaThumbnailStub.resolved()) {
                ViewUtil.setTopMargin(this.mediaThumbnailStub.require(), readDimen(R.dimen.message_bubble_top_padding));
            }
            if (this.linkPreviewStub.resolved() && !hasBigImageLinkPreview(messageRecord)) {
                ViewUtil.setTopMargin(this.linkPreviewStub.get(), readDimen(R.dimen.message_bubble_top_padding));
            }
        } else {
            throw new AssertionError();
        }
    }

    public /* synthetic */ void lambda$setQuote$7(MessageRecord messageRecord, View view) {
        if (this.eventListener == null || !this.batchSelected.isEmpty()) {
            this.passthroughClickListener.onClick(view);
        } else {
            this.eventListener.onQuoteClicked((MmsMessageRecord) messageRecord);
        }
    }

    private void setGutterSizes(MessageRecord messageRecord, boolean z) {
        if (z && messageRecord.isOutgoing()) {
            ViewUtil.setPaddingStart(this, readDimen(R.dimen.conversation_group_left_gutter));
            ViewUtil.setPaddingEnd(this, readDimen(R.dimen.conversation_individual_right_gutter));
        } else if (messageRecord.isOutgoing()) {
            ViewUtil.setPaddingStart(this, readDimen(R.dimen.conversation_individual_left_gutter));
            ViewUtil.setPaddingEnd(this, readDimen(R.dimen.conversation_individual_right_gutter));
        } else {
            ViewUtil.setPaddingStart(this, readDimen(R.dimen.conversation_individual_received_left_gutter));
            ViewUtil.setPaddingEnd(this, readDimen(R.dimen.conversation_individual_right_gutter));
        }
    }

    private void setReactions(MessageRecord messageRecord) {
        this.bodyBubble.setOnSizeChangedListener(null);
        if (messageRecord.getReactions().isEmpty()) {
            this.reactionsView.clear();
            return;
        }
        setReactionsWithWidth(messageRecord, this.bodyBubble.getWidth());
        this.bodyBubble.setOnSizeChangedListener(new ConversationItemBodyBubble.OnSizeChangedListener(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda10
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.conversation.ConversationItemBodyBubble.OnSizeChangedListener
            public final void onSizeChanged(int i, int i2) {
                ConversationItem.this.lambda$setReactions$8(this.f$1, i, i2);
            }
        });
    }

    public /* synthetic */ void lambda$setReactions$8(MessageRecord messageRecord, int i, int i2) {
        setReactionsWithWidth(messageRecord, i);
    }

    private void setReactionsWithWidth(MessageRecord messageRecord, int i) {
        this.reactionsView.setReactions(messageRecord.getReactions(), i);
        this.reactionsView.setOnClickListener(new View.OnClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda6
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationItem.this.lambda$setReactionsWithWidth$9(this.f$1, view);
            }
        });
    }

    public /* synthetic */ void lambda$setReactionsWithWidth$9(MessageRecord messageRecord, View view) {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onReactionClicked(new MultiselectPart.Message(this.conversationMessage), messageRecord.getId(), messageRecord.isMms());
        }
    }

    private void setFooter(MessageRecord messageRecord, Optional<MessageRecord> optional, Locale locale, boolean z, boolean z2) {
        ViewUtil.updateLayoutParams(this.footer, -2, -2);
        ViewUtil.setTopMargin(this.footer, readDimen(R.dimen.message_bubble_default_footer_bottom_margin));
        this.footer.setVisibility(8);
        ViewUtil.setVisibilityIfNonNull(this.stickerFooter, 8);
        if (this.sharedContactStub.resolved()) {
            this.sharedContactStub.get().getFooter().setVisibility(8);
        }
        if (this.mediaThumbnailStub.resolved()) {
            this.mediaThumbnailStub.require().getFooter().setVisibility(8);
        }
        if (isFooterVisible(messageRecord, optional, z)) {
            ConversationItemFooter activeFooter = getActiveFooter(messageRecord);
            activeFooter.setVisibility(0);
            activeFooter.setMessageRecord(messageRecord, locale);
            if (!z2 || !hasNoBubble(this.messageRecord)) {
                if (hasNoBubble(this.messageRecord)) {
                    activeFooter.disableBubbleBackground();
                    activeFooter.setTextColor(ContextCompat.getColor(this.context, R.color.signal_text_secondary));
                    activeFooter.setIconColor(ContextCompat.getColor(this.context, R.color.signal_icon_tint_secondary));
                    activeFooter.setRevealDotColor(ContextCompat.getColor(this.context, R.color.signal_icon_tint_secondary));
                    return;
                }
                activeFooter.disableBubbleBackground();
            } else if (this.messageRecord.isOutgoing()) {
                activeFooter.disableBubbleBackground();
                activeFooter.setTextColor(ContextCompat.getColor(this.context, R.color.conversation_item_sent_text_secondary_color));
                activeFooter.setIconColor(ContextCompat.getColor(this.context, R.color.conversation_item_sent_text_secondary_color));
                activeFooter.setRevealDotColor(ContextCompat.getColor(this.context, R.color.conversation_item_sent_text_secondary_color));
            } else {
                activeFooter.enableBubbleBackground(R.drawable.wallpaper_bubble_background_tintable_11, Integer.valueOf(getDefaultBubbleColor(z2)));
            }
        }
    }

    private void setStoryReactionLabel(MessageRecord messageRecord) {
        if (!isStoryReaction(messageRecord) || messageRecord.isRemoteDelete()) {
            View view = this.storyReactionLabelWrapper;
            if (view != null) {
                view.setVisibility(8);
                return;
            }
            return;
        }
        this.storyReactionLabelWrapper.setVisibility(0);
        this.storyReactionLabel.setTextColor(messageRecord.isOutgoing() ? this.colorizer.getOutgoingBodyTextColor(this.context) : ContextCompat.getColor(this.context, R.color.signal_text_primary));
        this.storyReactionLabel.setText(getStoryReactionLabelText(this.messageRecord));
    }

    private String getStoryReactionLabelText(MessageRecord messageRecord) {
        if (!hasQuote(messageRecord)) {
            return this.context.getString(R.string.ConversationItem__reacted_to_a_story);
        }
        RecipientId author = ((MmsMessageRecord) messageRecord).getQuote().getAuthor();
        if (author.equals(Recipient.self().getId())) {
            return this.context.getString(R.string.ConversationItem__reacted_to_your_story);
        }
        return this.context.getString(R.string.ConversationItem__you_reacted_to_s_story, Recipient.resolved(author).getShortDisplayName(this.context));
    }

    private void setHasBeenQuoted(ConversationMessage conversationMessage) {
        if (!conversationMessage.hasBeenQuoted() || this.isCondensedMode || this.quotedIndicator == null || !this.batchSelected.isEmpty()) {
            View view = this.quotedIndicator;
            if (view != null) {
                view.setVisibility(8);
                this.quotedIndicator.setOnClickListener(null);
                return;
            }
            return;
        }
        this.quotedIndicator.setVisibility(0);
        this.quotedIndicator.setOnClickListener(this.quotedIndicatorClickListener);
    }

    private boolean forceFooter(MessageRecord messageRecord) {
        return hasAudio(messageRecord);
    }

    private ConversationItemFooter getActiveFooter(MessageRecord messageRecord) {
        ConversationItemFooter conversationItemFooter;
        if (hasNoBubble(messageRecord) && (conversationItemFooter = this.stickerFooter) != null) {
            return conversationItemFooter;
        }
        if (hasSharedContact(messageRecord) && messageRecord.isDisplayBodyEmpty(getContext())) {
            return this.sharedContactStub.get().getFooter();
        }
        if (!hasOnlyThumbnail(messageRecord) || !messageRecord.isDisplayBodyEmpty(getContext())) {
            return this.footer;
        }
        return this.mediaThumbnailStub.require().getFooter();
    }

    private int readDimen(int i) {
        return this.context.getResources().getDimensionPixelOffset(i);
    }

    public boolean shouldInterceptClicks(MessageRecord messageRecord) {
        return this.batchSelected.isEmpty() && ((messageRecord.isFailed() && !messageRecord.isMmsNotification()) || ((messageRecord.isRateLimited() && SignalStore.rateLimit().needsRecaptcha()) || messageRecord.isPendingInsecureSmsFallback() || messageRecord.isBundleKeyExchange()));
    }

    private void setGroupMessageStatus(MessageRecord messageRecord, Recipient recipient) {
        TextView textView;
        if (this.groupThread && !messageRecord.isOutgoing() && (textView = this.groupSender) != null) {
            textView.setText(recipient.getDisplayName(getContext()));
        }
    }

    private void setGroupAuthorColor(MessageRecord messageRecord, boolean z, Colorizer colorizer) {
        TextView textView = this.groupSender;
        if (textView != null) {
            textView.setTextColor(colorizer.getIncomingGroupSenderColor(getContext(), messageRecord.getIndividualRecipient()));
        }
    }

    private void setAuthor(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z, boolean z2) {
        if (!z || messageRecord.isOutgoing()) {
            View view = this.groupSenderHolder;
            if (view != null) {
                view.setVisibility(8);
            }
            ViewGroup viewGroup = this.contactPhotoHolder;
            if (viewGroup != null) {
                viewGroup.setVisibility(8);
            }
            BadgeImageView badgeImageView = this.badgeImageView;
            if (badgeImageView != null) {
                badgeImageView.setVisibility(8);
                return;
            }
            return;
        }
        this.contactPhotoHolder.setVisibility(0);
        if (!optional.isPresent() || optional.get().isUpdate() || !messageRecord.getRecipient().equals(optional.get().getRecipient()) || !DateUtils.isSameDay(optional.get().getTimestamp(), messageRecord.getTimestamp()) || !isWithinClusteringTime(messageRecord, optional.get())) {
            this.groupSenderHolder.setVisibility(0);
            if (!z2 || !hasNoBubble(messageRecord)) {
                this.groupSenderHolder.setBackground(null);
            } else {
                this.groupSenderHolder.setBackgroundResource(R.drawable.wallpaper_bubble_background_tintable_11);
                this.groupSenderHolder.getBackground().setColorFilter(getDefaultBubbleColor(z2), PorterDuff.Mode.MULTIPLY);
            }
        } else {
            this.groupSenderHolder.setVisibility(8);
        }
        if (!optional2.isPresent() || optional2.get().isUpdate() || !messageRecord.getRecipient().equals(optional2.get().getRecipient()) || !isWithinClusteringTime(messageRecord, optional2.get())) {
            this.contactPhoto.setVisibility(0);
            this.badgeImageView.setVisibility(0);
            return;
        }
        this.contactPhoto.setVisibility(8);
        this.badgeImageView.setVisibility(8);
    }

    private void setOutlinerRadii(Outliner outliner, int i, int i2, int i3, int i4) {
        if (ViewUtil.isRtl(this)) {
            outliner.setRadii(i2, i, i4, i3);
        } else {
            outliner.setRadii(i, i2, i3, i4);
        }
    }

    private Projection.Corners getBodyBubbleCorners(int i, int i2, int i3, int i4) {
        if (ViewUtil.isRtl(this)) {
            return new Projection.Corners((float) i2, (float) i, (float) i4, (float) i3);
        }
        return new Projection.Corners((float) i, (float) i2, (float) i3, (float) i4);
    }

    private void setMessageShape(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z) {
        int i;
        int readDimen = readDimen(R.dimen.message_corner_radius);
        int readDimen2 = readDimen(R.dimen.message_corner_collapse_radius);
        if (isSingularMessage(messageRecord, optional, optional2, z)) {
            if (messageRecord.isOutgoing()) {
                i = R.drawable.message_bubble_background_sent_alone;
                this.outliner.setRadius(readDimen);
                this.pulseOutliner.setRadius(readDimen);
                this.bodyBubbleCorners = new Projection.Corners((float) readDimen);
            } else {
                i = R.drawable.message_bubble_background_received_alone;
                this.outliner.setRadius(readDimen);
                this.pulseOutliner.setRadius(readDimen);
                this.bodyBubbleCorners = new Projection.Corners((float) readDimen);
            }
        } else if (isStartOfMessageCluster(messageRecord, optional, z)) {
            if (messageRecord.isOutgoing()) {
                i = R.drawable.message_bubble_background_sent_start;
                setOutlinerRadii(this.outliner, readDimen, readDimen, readDimen2, readDimen);
                setOutlinerRadii(this.pulseOutliner, readDimen, readDimen, readDimen2, readDimen);
                this.bodyBubbleCorners = getBodyBubbleCorners(readDimen, readDimen, readDimen2, readDimen);
            } else {
                i = R.drawable.message_bubble_background_received_start;
                setOutlinerRadii(this.outliner, readDimen, readDimen, readDimen, readDimen2);
                setOutlinerRadii(this.pulseOutliner, readDimen, readDimen, readDimen, readDimen2);
                this.bodyBubbleCorners = getBodyBubbleCorners(readDimen, readDimen, readDimen, readDimen2);
            }
        } else if (isEndOfMessageCluster(messageRecord, optional2, z)) {
            if (messageRecord.isOutgoing()) {
                i = R.drawable.message_bubble_background_sent_end;
                setOutlinerRadii(this.outliner, readDimen, readDimen2, readDimen, readDimen);
                setOutlinerRadii(this.pulseOutliner, readDimen, readDimen2, readDimen, readDimen);
                this.bodyBubbleCorners = getBodyBubbleCorners(readDimen, readDimen2, readDimen, readDimen);
            } else {
                i = R.drawable.message_bubble_background_received_end;
                setOutlinerRadii(this.outliner, readDimen2, readDimen, readDimen, readDimen);
                setOutlinerRadii(this.pulseOutliner, readDimen2, readDimen, readDimen, readDimen);
                this.bodyBubbleCorners = getBodyBubbleCorners(readDimen2, readDimen, readDimen, readDimen);
            }
        } else if (messageRecord.isOutgoing()) {
            i = R.drawable.message_bubble_background_sent_middle;
            setOutlinerRadii(this.outliner, readDimen, readDimen2, readDimen2, readDimen);
            setOutlinerRadii(this.pulseOutliner, readDimen, readDimen2, readDimen2, readDimen);
            this.bodyBubbleCorners = getBodyBubbleCorners(readDimen, readDimen2, readDimen2, readDimen);
        } else {
            i = R.drawable.message_bubble_background_received_middle;
            setOutlinerRadii(this.outliner, readDimen2, readDimen, readDimen, readDimen2);
            setOutlinerRadii(this.pulseOutliner, readDimen2, readDimen, readDimen, readDimen2);
            this.bodyBubbleCorners = getBodyBubbleCorners(readDimen2, readDimen, readDimen, readDimen2);
        }
        this.bodyBubble.setBackgroundResource(i);
    }

    private boolean isStartOfMessageCluster(MessageRecord messageRecord, Optional<MessageRecord> optional, boolean z) {
        if (z) {
            if (!optional.isPresent() || optional.get().isUpdate() || !DateUtils.isSameDay(messageRecord.getTimestamp(), optional.get().getTimestamp()) || !messageRecord.getRecipient().equals(optional.get().getRecipient()) || !isWithinClusteringTime(messageRecord, optional.get())) {
                return true;
            }
            return false;
        } else if (!optional.isPresent() || optional.get().isUpdate() || !DateUtils.isSameDay(messageRecord.getTimestamp(), optional.get().getTimestamp()) || messageRecord.isOutgoing() != optional.get().isOutgoing() || optional.get().isSecure() != messageRecord.isSecure() || !isWithinClusteringTime(messageRecord, optional.get())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isEndOfMessageCluster(MessageRecord messageRecord, Optional<MessageRecord> optional, boolean z) {
        if (z) {
            if (!optional.isPresent() || optional.get().isUpdate() || !DateUtils.isSameDay(messageRecord.getTimestamp(), optional.get().getTimestamp()) || !messageRecord.getRecipient().equals(optional.get().getRecipient()) || !messageRecord.getReactions().isEmpty() || !isWithinClusteringTime(messageRecord, optional.get())) {
                return true;
            }
            return false;
        } else if (!optional.isPresent() || optional.get().isUpdate() || !DateUtils.isSameDay(messageRecord.getTimestamp(), optional.get().getTimestamp()) || messageRecord.isOutgoing() != optional.get().isOutgoing() || !messageRecord.getReactions().isEmpty() || optional.get().isSecure() != messageRecord.isSecure() || !isWithinClusteringTime(messageRecord, optional.get())) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isSingularMessage(MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z) {
        return isStartOfMessageCluster(messageRecord, optional, z) && isEndOfMessageCluster(messageRecord, optional2, z);
    }

    private boolean isFooterVisible(MessageRecord messageRecord, Optional<MessageRecord> optional, boolean z) {
        boolean z2 = optional.isPresent() && !DateUtils.isSameExtendedRelativeTimestamp(optional.get().getTimestamp(), messageRecord.getTimestamp());
        if (forceFooter(this.messageRecord) || messageRecord.getExpiresIn() > 0 || !messageRecord.isSecure() || messageRecord.isPending() || messageRecord.isPendingInsecureSmsFallback() || messageRecord.isFailed() || messageRecord.isRateLimited() || z2 || isEndOfMessageCluster(messageRecord, optional, z)) {
            return true;
        }
        return false;
    }

    private static boolean isWithinClusteringTime(MessageRecord messageRecord, MessageRecord messageRecord2) {
        return Math.abs(messageRecord.getDateSent() - messageRecord2.getDateSent()) <= MAX_CLUSTERING_TIME_DIFF;
    }

    private void setMessageSpacing(Context context, MessageRecord messageRecord, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, boolean z) {
        int readDimen = readDimen(context, R.dimen.conversation_vertical_message_spacing_collapse);
        int readDimen2 = isStartOfMessageCluster(messageRecord, optional, z) ? readDimen(context, R.dimen.conversation_vertical_message_spacing_default) : readDimen;
        if (isEndOfMessageCluster(messageRecord, optional2, z)) {
            readDimen = readDimen(context, R.dimen.conversation_vertical_message_spacing_default);
        }
        ViewUtil.setPaddingTop(this, readDimen2);
        ViewUtil.setPaddingBottom(this, readDimen);
    }

    private int readDimen(Context context, int i) {
        return context.getResources().getDimensionPixelOffset(i);
    }

    private Spannable getLongMessageSpan(MessageRecord messageRecord) {
        final Runnable runnable;
        String str;
        if (messageRecord.isMms()) {
            TextSlide textSlide = ((MmsMessageRecord) messageRecord).getSlideDeck().getTextSlide();
            if (textSlide != null && textSlide.asAttachment().getTransferState() == 0) {
                str = getResources().getString(R.string.ConversationItem_read_more);
                runnable = new Runnable(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda15
                    public final /* synthetic */ MessageRecord f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationItem.this.lambda$getLongMessageSpan$10(this.f$1);
                    }
                };
            } else if (textSlide != null && textSlide.asAttachment().getTransferState() == 1) {
                str = getResources().getString(R.string.ConversationItem_pending);
                runnable = new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda16
                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationItem.lambda$getLongMessageSpan$11();
                    }
                };
            } else if (textSlide != null) {
                String string = getResources().getString(R.string.ConversationItem_download_more);
                runnable = new Runnable(textSlide) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda17
                    public final /* synthetic */ TextSlide f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationItem.this.lambda$getLongMessageSpan$12(this.f$1);
                    }
                };
                str = string;
            } else {
                str = getResources().getString(R.string.ConversationItem_read_more);
                runnable = new Runnable(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda18
                    public final /* synthetic */ MessageRecord f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationItem.this.lambda$getLongMessageSpan$13(this.f$1);
                    }
                };
            }
        } else {
            str = getResources().getString(R.string.ConversationItem_read_more);
            runnable = new Runnable(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda19
                public final /* synthetic */ MessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationItem.this.lambda$getLongMessageSpan$14(this.f$1);
                }
            };
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
        spannableStringBuilder.setSpan(new ClickableSpan() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem.2
            @Override // android.text.style.ClickableSpan
            public void onClick(View view) {
                if (ConversationItem.this.eventListener != null && ConversationItem.this.batchSelected.isEmpty()) {
                    runnable.run();
                }
            }

            @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
            public void updateDrawState(TextPaint textPaint) {
                textPaint.setTypeface(Typeface.DEFAULT_BOLD);
            }
        }, 0, spannableStringBuilder.length(), 17);
        return spannableStringBuilder;
    }

    public /* synthetic */ void lambda$getLongMessageSpan$10(MessageRecord messageRecord) {
        this.eventListener.onMoreTextClicked(this.conversationRecipient.getId(), messageRecord.getId(), messageRecord.isMms());
    }

    public /* synthetic */ void lambda$getLongMessageSpan$12(TextSlide textSlide) {
        this.singleDownloadClickListener.onClick(this.bodyText, textSlide);
    }

    public /* synthetic */ void lambda$getLongMessageSpan$13(MessageRecord messageRecord) {
        this.eventListener.onMoreTextClicked(this.conversationRecipient.getId(), messageRecord.getId(), messageRecord.isMms());
    }

    public /* synthetic */ void lambda$getLongMessageSpan$14(MessageRecord messageRecord) {
        this.eventListener.onMoreTextClicked(this.conversationRecipient.getId(), messageRecord.getId(), messageRecord.isMms());
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void showProjectionArea() {
        NullableStub<ConversationItemThumbnail> nullableStub = this.mediaThumbnailStub;
        if (nullableStub != null && nullableStub.resolved()) {
            this.mediaThumbnailStub.require().showThumbnailView();
            this.bodyBubble.setVideoPlayerProjection(null);
        }
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void hideProjectionArea() {
        NullableStub<ConversationItemThumbnail> nullableStub = this.mediaThumbnailStub;
        if (nullableStub != null && nullableStub.resolved()) {
            this.mediaThumbnailStub.require().hideThumbnailView();
            this.mediaThumbnailStub.require().getDrawingRect(this.thumbnailMaskingRect);
            this.bodyBubble.setVideoPlayerProjection(Projection.relativeToViewWithCommonRoot(this.mediaThumbnailStub.require(), this.bodyBubble, null));
        }
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public MediaItem getMediaItem() {
        return this.mediaItem;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public GiphyMp4PlaybackPolicyEnforcer getPlaybackPolicyEnforcer() {
        if (GiphyMp4PlaybackPolicy.autoplay()) {
            return null;
        }
        return new GiphyMp4PlaybackPolicyEnforcer(new GiphyMp4PlaybackPolicyEnforcer.Callback() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicyEnforcer.Callback
            public final void onPlaybackWillEnd() {
                ConversationItem.this.lambda$getPlaybackPolicyEnforcer$15();
            }
        });
    }

    public /* synthetic */ void lambda$getPlaybackPolicyEnforcer$15() {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onPlayInlineContent(null);
        }
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public int getAdapterPosition() {
        throw new UnsupportedOperationException("Do not delegate to this method");
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public Projection getGiphyMp4PlayableProjection(ViewGroup viewGroup) {
        NullableStub<ConversationItemThumbnail> nullableStub = this.mediaThumbnailStub;
        if (nullableStub == null || !nullableStub.isResolvable()) {
            return Projection.relativeToParent(viewGroup, this.bodyBubble, this.bodyBubbleCorners).translateY(getTranslationY()).translateX(this.bodyBubble.getTranslationX()).translateX(getTranslationX());
        }
        ConversationItemThumbnail require = this.mediaThumbnailStub.require();
        return Projection.relativeToParent(viewGroup, require, require.getCorners()).scale(this.bodyBubble.getScaleX()).translateX(Util.halfOffsetFromScale(require.getWidth(), this.bodyBubble.getScaleX())).translateY(Util.halfOffsetFromScale(require.getHeight(), this.bodyBubble.getScaleY())).translateY(getTranslationY()).translateX(this.bodyBubble.getTranslationX()).translateX(getTranslationX());
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean canPlayContent() {
        NullableStub<ConversationItemThumbnail> nullableStub = this.mediaThumbnailStub;
        return nullableStub != null && nullableStub.isResolvable() && this.canPlayContent;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean shouldProjectContent() {
        return canPlayContent() && this.bodyBubble.getVisibility() == 0;
    }

    @Override // org.thoughtcrime.securesms.conversation.colors.Colorizable
    public ProjectionList getColorizerProjections(ViewGroup viewGroup) {
        return getSnapshotProjections(viewGroup, true);
    }

    public ProjectionList getSnapshotProjections(ViewGroup viewGroup, boolean z) {
        ConversationItemFooter activeFooter;
        Projection projection;
        this.colorizerProjections.clear();
        if (this.messageRecord.isOutgoing() && !hasNoBubble(this.messageRecord) && !this.messageRecord.isRemoteDelete() && this.bodyBubbleCorners != null && this.bodyBubble.getVisibility() == 0) {
            Projection translateX = Projection.relativeToParent(viewGroup, this.bodyBubble, this.bodyBubbleCorners).translateX(this.bodyBubble.getTranslationX());
            Projection videoPlayerProjection = this.bodyBubble.getVideoPlayerProjection();
            Projection projection2 = null;
            if (z && this.mediaThumbnailStub.resolved()) {
                projection2 = Projection.relativeToParent(viewGroup, this.mediaThumbnailStub.require(), null);
            }
            float halfOffsetFromScale = Util.halfOffsetFromScale(this.bodyBubble.getWidth(), this.bodyBubble.getScaleX());
            float halfOffsetFromScale2 = Util.halfOffsetFromScale(this.bodyBubble.getHeight(), this.bodyBubble.getScaleY());
            if (videoPlayerProjection != null) {
                List<Projection> capAndTail = Projection.getCapAndTail(translateX, Projection.translateFromDescendantToParentCoords(videoPlayerProjection, this.bodyBubble, viewGroup));
                if (!capAndTail.isEmpty()) {
                    capAndTail.get(0).scale(this.bodyBubble.getScaleX()).translateX(halfOffsetFromScale).translateY(halfOffsetFromScale2);
                    capAndTail.get(1).scale(this.bodyBubble.getScaleX()).translateX(halfOffsetFromScale).translateY(-halfOffsetFromScale2);
                }
                this.colorizerProjections.addAll(capAndTail);
            } else if (!hasThumbnail(this.messageRecord) || projection2 == null) {
                this.colorizerProjections.add(translateX.scale(this.bodyBubble.getScaleX()).translateX(halfOffsetFromScale).translateY(halfOffsetFromScale2));
            } else {
                if (hasQuote(this.messageRecord) && this.quoteView != null) {
                    Projection translateX2 = Projection.relativeToParent(viewGroup, this.bodyBubble, this.bodyBubbleCorners).translateX(this.bodyBubble.getTranslationX());
                    this.colorizerProjections.add(translateX2.insetBottom(translateX2.getHeight() - (((int) projection2.getY()) - ((int) translateX2.getY()))).scale(this.bodyBubble.getScaleX()).translateX(halfOffsetFromScale).translateY(halfOffsetFromScale2));
                }
                this.colorizerProjections.add(translateX.scale(this.bodyBubble.getScaleX()).insetTop((int) (((float) projection2.getHeight()) * this.bodyBubble.getScaleX())).translateX(halfOffsetFromScale).translateY(halfOffsetFromScale2));
            }
            if (projection2 != null) {
                projection2.release();
            }
        }
        if (this.messageRecord.isOutgoing() && hasNoBubble(this.messageRecord) && this.hasWallpaper && this.bodyBubble.getVisibility() == 0 && (projection = (activeFooter = getActiveFooter(this.messageRecord)).getProjection(viewGroup)) != null) {
            this.colorizerProjections.add(projection.translateX(this.bodyBubble.getTranslationX()).scale(this.bodyBubble.getScaleX()).translateX(Util.halfOffsetFromScale(activeFooter.getWidth(), this.bodyBubble.getScaleX())).translateY(-Util.halfOffsetFromScale(activeFooter.getHeight(), this.bodyBubble.getScaleY())));
        }
        for (int i = 0; i < this.colorizerProjections.size(); i++) {
            this.colorizerProjections.get(i).translateY(getTranslationY());
        }
        return this.colorizerProjections;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public View getHorizontalTranslationTarget() {
        if (this.messageRecord.isOutgoing()) {
            return null;
        }
        if (this.groupThread) {
            return this.contactPhotoHolder;
        }
        return this.bodyBubble;
    }

    @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGift
    public Projection getOpenableGiftProjection(boolean z) {
        if (!isGiftMessage(this.messageRecord) || this.messageRecord.isRemoteDelete()) {
            return null;
        }
        if (this.messageRecord.getViewedReceiptCount() <= 0 || z) {
            return Projection.relativeToViewRoot(this.bodyBubble, this.bodyBubbleCorners).translateX(this.bodyBubble.getTranslationX()).translateX(getTranslationX()).scale(this.bodyBubble.getScaleX());
        }
        return null;
    }

    @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGift
    public long getGiftId() {
        return this.messageRecord.getId();
    }

    @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGift
    public void setOpenGiftCallback(Function1<? super OpenableGift, Unit> function1) {
        if (this.giftViewStub.resolved()) {
            this.bodyBubble.setOnClickListener(new View.OnClickListener(function1) { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda3
                public final /* synthetic */ Function1 f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationItem.this.lambda$setOpenGiftCallback$16(this.f$1, view);
                }
            });
            this.giftViewStub.get().onGiftNotOpened();
        }
    }

    public /* synthetic */ void lambda$setOpenGiftCallback$16(Function1 function1, View view) {
        function1.invoke(this);
        this.eventListener.onGiftBadgeRevealed(this.messageRecord);
        this.bodyBubble.performHapticFeedback(Build.VERSION.SDK_INT >= 30 ? 16 : 3);
    }

    @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGift
    public void clearOpenGiftCallback() {
        if (this.giftViewStub.resolved()) {
            this.bodyBubble.setOnClickListener(null);
            this.bodyBubble.setClickable(false);
            this.giftViewStub.get().onGiftOpened();
        }
    }

    @Override // org.thoughtcrime.securesms.badges.gifts.OpenableGift
    public OpenableGift.AnimationSign getAnimationSign() {
        return OpenableGift.AnimationSign.get(ViewUtil.isLtr(this), this.messageRecord.isOutgoing());
    }

    /* loaded from: classes4.dex */
    public class SharedContactEventListener implements SharedContactView.EventListener {
        private SharedContactEventListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.SharedContactView.EventListener
        public void onAddToContactsClicked(Contact contact) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty()) {
                ConversationItem.this.passthroughClickListener.onClick((View) ConversationItem.this.sharedContactStub.get());
            } else {
                ConversationItem.this.eventListener.onAddToContactsClicked(contact);
            }
        }

        @Override // org.thoughtcrime.securesms.components.SharedContactView.EventListener
        public void onInviteClicked(List<Recipient> list) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty()) {
                ConversationItem.this.passthroughClickListener.onClick((View) ConversationItem.this.sharedContactStub.get());
            } else {
                ConversationItem.this.eventListener.onInviteSharedContactClicked(list);
            }
        }

        @Override // org.thoughtcrime.securesms.components.SharedContactView.EventListener
        public void onMessageClicked(List<Recipient> list) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty()) {
                ConversationItem.this.passthroughClickListener.onClick((View) ConversationItem.this.sharedContactStub.get());
            } else {
                ConversationItem.this.eventListener.onMessageSharedContactClicked(list);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class SharedContactClickListener implements View.OnClickListener {
        private SharedContactClickListener() {
            ConversationItem.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty() || !ConversationItem.this.messageRecord.isMms() || ((MmsMessageRecord) ConversationItem.this.messageRecord).getSharedContacts().isEmpty()) {
                ConversationItem.this.passthroughClickListener.onClick(view);
            } else {
                ConversationItem.this.eventListener.onSharedContactDetailsClicked(((MmsMessageRecord) ConversationItem.this.messageRecord).getSharedContacts().get(0), (View) ((SharedContactView) ConversationItem.this.sharedContactStub.get()).getAvatarView().getParent());
            }
        }
    }

    /* loaded from: classes4.dex */
    public class LinkPreviewClickListener implements View.OnClickListener {
        private LinkPreviewClickListener() {
            ConversationItem.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty() || !ConversationItem.this.messageRecord.isMms() || ((MmsMessageRecord) ConversationItem.this.messageRecord).getLinkPreviews().isEmpty()) {
                ConversationItem.this.passthroughClickListener.onClick(view);
            } else {
                ConversationItem.this.eventListener.onLinkPreviewClicked(((MmsMessageRecord) ConversationItem.this.messageRecord).getLinkPreviews().get(0));
            }
        }
    }

    /* loaded from: classes4.dex */
    public class ViewOnceMessageClickListener implements View.OnClickListener {
        private ViewOnceMessageClickListener() {
            ConversationItem.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ViewOnceMessageView viewOnceMessageView = (ViewOnceMessageView) view;
            if (ConversationItem.this.batchSelected.isEmpty() && ConversationItem.this.messageRecord.isMms() && viewOnceMessageView.requiresTapToDownload((MmsMessageRecord) ConversationItem.this.messageRecord)) {
                ConversationItem.this.singleDownloadClickListener.onClick(view, ((MmsMessageRecord) ConversationItem.this.messageRecord).getSlideDeck().getThumbnailSlide());
            } else if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty() || !ConversationItem.this.messageRecord.isMms()) {
                ConversationItem.this.passthroughClickListener.onClick(view);
            } else {
                ConversationItem.this.eventListener.onViewOnceMessageClicked((MmsMessageRecord) ConversationItem.this.messageRecord);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class LinkPreviewThumbnailClickListener implements SlideClickListener {
        private LinkPreviewThumbnailClickListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.mms.SlideClickListener
        public void onClick(View view, Slide slide) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty() || !ConversationItem.this.messageRecord.isMms() || ((MmsMessageRecord) ConversationItem.this.messageRecord).getLinkPreviews().isEmpty()) {
                ConversationItem.this.performClick();
            } else {
                ConversationItem.this.eventListener.onLinkPreviewClicked(((MmsMessageRecord) ConversationItem.this.messageRecord).getLinkPreviews().get(0));
            }
        }
    }

    /* loaded from: classes4.dex */
    public class QuotedIndicatorClickListener implements View.OnClickListener {
        private QuotedIndicatorClickListener() {
            ConversationItem.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (ConversationItem.this.eventListener == null || !ConversationItem.this.batchSelected.isEmpty() || !ConversationItem.this.conversationMessage.hasBeenQuoted()) {
                ConversationItem.this.passthroughClickListener.onClick(view);
            } else {
                ConversationItem.this.eventListener.onQuotedIndicatorClicked(ConversationItem.this.messageRecord);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class AttachmentDownloadClickListener implements SlidesClickedListener {
        private AttachmentDownloadClickListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.mms.SlidesClickedListener
        public void onClick(View view, List<Slide> list) {
            Log.i(ConversationItem.TAG, "onClick() for attachment download");
            if (ConversationItem.this.messageRecord.isMmsNotification()) {
                Log.i(ConversationItem.TAG, "Scheduling MMS attachment download");
                ApplicationDependencies.getJobManager().add(new MmsDownloadJob(ConversationItem.this.messageRecord.getId(), ConversationItem.this.messageRecord.getThreadId(), false));
                return;
            }
            String str = ConversationItem.TAG;
            Log.i(str, "Scheduling push attachment downloads for " + list.size() + " items");
            for (Slide slide : list) {
                ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(ConversationItem.this.messageRecord.getId(), ((DatabaseAttachment) slide.asAttachment()).getAttachmentId(), true));
            }
        }
    }

    /* loaded from: classes4.dex */
    public class SlideClickPassthroughListener implements SlideClickListener {
        private final SlidesClickedListener original;

        private SlideClickPassthroughListener(SlidesClickedListener slidesClickedListener) {
            ConversationItem.this = r1;
            this.original = slidesClickedListener;
        }

        @Override // org.thoughtcrime.securesms.mms.SlideClickListener
        public void onClick(View view, Slide slide) {
            this.original.onClick(view, Collections.singletonList(slide));
        }
    }

    /* loaded from: classes4.dex */
    public class StickerClickListener implements SlideClickListener {
        private StickerClickListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.mms.SlideClickListener
        public void onClick(View view, Slide slide) {
            ConversationItem conversationItem = ConversationItem.this;
            if (conversationItem.shouldInterceptClicks(conversationItem.messageRecord) || !ConversationItem.this.batchSelected.isEmpty()) {
                ConversationItem.this.performClick();
            } else if (ConversationItem.this.eventListener != null) {
                ConversationItem conversationItem2 = ConversationItem.this;
                if (conversationItem2.hasSticker(conversationItem2.messageRecord)) {
                    ConversationItem.this.eventListener.onStickerClicked(((MmsMessageRecord) ConversationItem.this.messageRecord).getSlideDeck().getStickerSlide().asAttachment().getSticker());
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public class ThumbnailClickListener implements SlideClickListener {
        private ThumbnailClickListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.mms.SlideClickListener
        public void onClick(View view, Slide slide) {
            ConversationItem conversationItem = ConversationItem.this;
            if (conversationItem.shouldInterceptClicks(conversationItem.messageRecord) || !ConversationItem.this.batchSelected.isEmpty() || ConversationItem.this.isCondensedMode) {
                ConversationItem.this.performClick();
            } else if (!ConversationItem.this.canPlayContent && ConversationItem.this.mediaItem != null && ConversationItem.this.eventListener != null) {
                ConversationItem.this.eventListener.onPlayInlineContent(ConversationItem.this.conversationMessage);
            } else if (MediaPreviewActivity.isContentTypeSupported(slide.getContentType()) && slide.getUri() != null) {
                Intent intent = new Intent(ConversationItem.this.context, MediaPreviewActivity.class);
                intent.addFlags(1);
                intent.setDataAndType(slide.getUri(), slide.getContentType());
                intent.putExtra("thread_id", ConversationItem.this.messageRecord.getThreadId());
                intent.putExtra("date", ConversationItem.this.messageRecord.getTimestamp());
                intent.putExtra(MediaPreviewActivity.SIZE_EXTRA, slide.asAttachment().getSize());
                intent.putExtra(MediaPreviewActivity.CAPTION_EXTRA, slide.getCaption().orElse(null));
                intent.putExtra(MediaPreviewActivity.IS_VIDEO_GIF, slide.isVideoGif());
                intent.putExtra(MediaPreviewActivity.LEFT_IS_RECENT_EXTRA, false);
                ConversationItem.this.context.startActivity(intent);
            } else if (slide.getUri() != null) {
                String str = ConversationItem.TAG;
                Log.i(str, "Clicked: " + slide.getUri() + " , " + slide.getContentType());
                Uri attachmentPublicUri = PartAuthority.getAttachmentPublicUri(slide.getUri());
                String str2 = ConversationItem.TAG;
                Log.i(str2, "Public URI: " + attachmentPublicUri);
                Intent intent2 = new Intent("android.intent.action.VIEW");
                intent2.addFlags(1);
                intent2.setDataAndType(PartAuthority.getAttachmentPublicUri(slide.getUri()), Intent.normalizeMimeType(slide.getContentType()));
                try {
                    ConversationItem.this.context.startActivity(intent2);
                } catch (ActivityNotFoundException unused) {
                    Log.w(ConversationItem.TAG, "No activity existed to view the media.");
                    Toast.makeText(ConversationItem.this.context, (int) R.string.ConversationItem_unable_to_open_media, 1).show();
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public class PassthroughClickListener implements View.OnLongClickListener, View.OnClickListener {
        private PassthroughClickListener() {
            ConversationItem.this = r1;
        }

        @Override // android.view.View.OnLongClickListener
        public boolean onLongClick(View view) {
            if (ConversationItem.this.bodyText.hasSelection()) {
                return false;
            }
            ConversationItem.this.performLongClick();
            return true;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ConversationItem.this.performClick();
        }
    }

    /* loaded from: classes4.dex */
    public class GiftMessageViewCallback implements GiftMessageView.Callback {
        private GiftMessageViewCallback() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.badges.gifts.GiftMessageView.Callback
        public void onViewGiftBadgeClicked() {
            ConversationItem.this.eventListener.onViewGiftBadgeClicked(ConversationItem.this.messageRecord);
        }
    }

    /* loaded from: classes4.dex */
    public class ClickListener implements View.OnClickListener {
        private View.OnClickListener parent;

        ClickListener(View.OnClickListener onClickListener) {
            ConversationItem.this = r1;
            this.parent = onClickListener;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            View.OnClickListener onClickListener;
            ConversationItem conversationItem = ConversationItem.this;
            if (!conversationItem.shouldInterceptClicks(conversationItem.messageRecord) && (onClickListener = this.parent) != null) {
                onClickListener.onClick(view);
            } else if (ConversationItem.this.messageRecord.isFailed()) {
                if (ConversationItem.this.eventListener != null) {
                    ConversationItem.this.eventListener.onMessageWithErrorClicked(ConversationItem.this.messageRecord);
                }
            } else if (!ConversationItem.this.messageRecord.isRateLimited() || !SignalStore.rateLimit().needsRecaptcha()) {
                if (ConversationItem.this.messageRecord.isOutgoing() || !ConversationItem.this.messageRecord.isIdentityMismatchFailure()) {
                    if (ConversationItem.this.messageRecord.isPendingInsecureSmsFallback()) {
                        ConversationItem.this.handleMessageApproval();
                    }
                } else if (ConversationItem.this.eventListener != null) {
                    ConversationItem.this.eventListener.onIncomingIdentityMismatchClicked(ConversationItem.this.messageRecord.getIndividualRecipient().getId());
                }
            } else if (ConversationItem.this.eventListener != null) {
                ConversationItem.this.eventListener.onMessageWithRecaptchaNeededClicked(ConversationItem.this.messageRecord);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public final class TouchDelegateChangedListener implements ConversationItemFooter.OnTouchDelegateChangedListener {
        private TouchDelegateChangedListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.ConversationItemFooter.OnTouchDelegateChangedListener
        public void onTouchDelegateChanged(Rect rect, View view) {
            ConversationItem conversationItem = ConversationItem.this;
            conversationItem.offsetDescendantRectToMyCoords(conversationItem.footer, rect);
            ConversationItem.this.setTouchDelegate(new TouchDelegate(rect, view));
        }
    }

    /* loaded from: classes4.dex */
    public final class UrlClickListener implements UrlClickHandler {
        private UrlClickListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.util.UrlClickHandler
        public boolean handleOnClick(String str) {
            return ConversationItem.this.eventListener != null && ConversationItem.this.eventListener.onUrlClicked(str);
        }
    }

    /* loaded from: classes4.dex */
    public class MentionClickableSpan extends ClickableSpan {
        private final RecipientId mentionedRecipientId;

        @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
        public void updateDrawState(TextPaint textPaint) {
        }

        MentionClickableSpan(RecipientId recipientId) {
            ConversationItem.this = r1;
            this.mentionedRecipientId = recipientId;
        }

        @Override // android.text.style.ClickableSpan
        public void onClick(View view) {
            if (ConversationItem.this.eventListener != null && ConversationItem.this.batchSelected.isEmpty()) {
                VibrateUtil.vibrateTick(ConversationItem.this.context);
                ConversationItem.this.eventListener.onGroupMemberClicked(this.mentionedRecipientId, ConversationItem.this.conversationRecipient.get().requireGroupId());
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class AudioPlaybackSpeedToggleListener implements PlaybackSpeedToggleTextView.PlaybackSpeedListener {
        private AudioPlaybackSpeedToggleListener() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.PlaybackSpeedToggleTextView.PlaybackSpeedListener
        public void onPlaybackSpeedChanged(float f) {
            Uri audioSlideUri;
            if (ConversationItem.this.eventListener != null && ConversationItem.this.audioViewStub.resolved() && (audioSlideUri = ((AudioView) ConversationItem.this.audioViewStub.get()).getAudioSlideUri()) != null) {
                ConversationItem.this.eventListener.onVoiceNotePlaybackSpeedChanged(audioSlideUri, f);
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class AudioViewCallbacks implements AudioView.Callbacks {
        private AudioViewCallbacks() {
            ConversationItem.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onPlay(Uri uri, double d) {
            if (ConversationItem.this.eventListener != null) {
                ConversationItem.this.eventListener.onVoiceNotePlay(uri, ConversationItem.this.messageRecord.getId(), d);
            }
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onPause(Uri uri) {
            if (ConversationItem.this.eventListener != null) {
                ConversationItem.this.eventListener.onVoiceNotePause(uri);
            }
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onSeekTo(Uri uri, double d) {
            if (ConversationItem.this.eventListener != null) {
                ConversationItem.this.eventListener.onVoiceNoteSeekTo(uri, d);
            }
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onStopAndReset(Uri uri) {
            throw new UnsupportedOperationException();
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onSpeedChanged(float f, boolean z) {
            ConversationItem.this.footer.setAudioPlaybackSpeed(f, z);
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onProgressUpdated(long j, long j2) {
            ConversationItem.this.footer.setAudioDuration(j, j2);
        }
    }

    public void handleMessageApproval() {
        int i = this.messageRecord.isMms() ? R.string.ConversationItem_click_to_approve_unencrypted_mms_dialog_title : R.string.ConversationItem_click_to_approve_unencrypted_sms_dialog_title;
        AlertDialog.Builder builder = new AlertDialog.Builder(this.context);
        builder.setTitle(i);
        builder.setMessage(R.string.ConversationItem_click_to_approve_unencrypted_dialog_message);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                ConversationItem.this.lambda$handleMessageApproval$17(dialogInterface, i2);
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationItem$$ExternalSyntheticLambda5
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                ConversationItem.this.lambda$handleMessageApproval$18(dialogInterface, i2);
            }
        });
        builder.show();
    }

    public /* synthetic */ void lambda$handleMessageApproval$17(DialogInterface dialogInterface, int i) {
        MessageDatabase messageDatabase;
        if (this.messageRecord.isMms()) {
            messageDatabase = SignalDatabase.mms();
        } else {
            messageDatabase = SignalDatabase.sms();
        }
        messageDatabase.markAsInsecure(this.messageRecord.getId());
        messageDatabase.markAsOutbox(this.messageRecord.getId());
        messageDatabase.markAsForcedSms(this.messageRecord.getId());
        if (this.messageRecord.isMms()) {
            MmsSendJob.enqueue(this.context, ApplicationDependencies.getJobManager(), this.messageRecord.getId());
        } else {
            ApplicationDependencies.getJobManager().add(new SmsSendJob(this.messageRecord.getId(), this.messageRecord.getIndividualRecipient()));
        }
    }

    public /* synthetic */ void lambda$handleMessageApproval$18(DialogInterface dialogInterface, int i) {
        if (this.messageRecord.isMms()) {
            SignalDatabase.mms().markAsSentFailed(this.messageRecord.getId());
        } else {
            SignalDatabase.sms().markAsSentFailed(this.messageRecord.getId());
        }
    }
}
