package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import androidx.core.graphics.ColorUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import java.util.EnumMap;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.math.MathKt__MathJVMKt;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: CustomChatColorCreatorViewModel.kt */
@Metadata(d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002()B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\b\u0010\u0017\u001a\u00020\u0013H\u0002J\"\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00190\u001dJ\u000e\u0010\u001e\u001a\u00020\u00192\u0006\u0010\u001f\u001a\u00020 J\u000e\u0010!\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020\u0003J\u000e\u0010#\u001a\u00020\u00192\u0006\u0010\"\u001a\u00020\u0003J\u000e\u0010$\u001a\u00020\u00192\u0006\u0010%\u001a\u00020&J\u000e\u0010'\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\f¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u0016X\u0004¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel;", "Landroidx/lifecycle/ViewModel;", "maxSliderValue", "", "chatColorsId", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorRepository;", "(ILorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorRepository;)V", "events", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event;", "getEvents", "()Landroidx/lifecycle/LiveData;", "internalEvents", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "state", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorState;", "getState", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getInitialState", "saveNow", "", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "onSaved", "Lkotlin/Function1;", "setDegrees", "degrees", "", "setHueProgress", "progress", "setSaturationProgress", "setSelectedEdge", "selectedEdge", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;", "startSave", "Event", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorCreatorViewModel extends ViewModel {
    private final ChatColors.Id chatColorsId;
    private final LiveData<Event> events;
    private final SingleLiveEvent<Event> internalEvents;
    private final int maxSliderValue;
    private final RecipientId recipientId;
    private final CustomChatColorCreatorRepository repository;
    private final LiveData<CustomChatColorCreatorState> state;
    private final Store<CustomChatColorCreatorState> store;

    public CustomChatColorCreatorViewModel(int i, ChatColors.Id id, RecipientId recipientId, CustomChatColorCreatorRepository customChatColorCreatorRepository) {
        Intrinsics.checkNotNullParameter(id, "chatColorsId");
        Intrinsics.checkNotNullParameter(customChatColorCreatorRepository, "repository");
        this.maxSliderValue = i;
        this.chatColorsId = id;
        this.recipientId = recipientId;
        this.repository = customChatColorCreatorRepository;
        Store<CustomChatColorCreatorState> store = new Store<>(getInitialState());
        this.store = store;
        SingleLiveEvent<Event> singleLiveEvent = new SingleLiveEvent<>();
        this.internalEvents = singleLiveEvent;
        LiveData<CustomChatColorCreatorState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        this.events = singleLiveEvent;
        customChatColorCreatorRepository.getWallpaper(recipientId, new Function1<ChatWallpaper, Unit>(this) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel.1
            final /* synthetic */ CustomChatColorCreatorViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ChatWallpaper chatWallpaper) {
                invoke(chatWallpaper);
                return Unit.INSTANCE;
            }

            /* renamed from: invoke$lambda-0 */
            public static final CustomChatColorCreatorState m1551invoke$lambda0(ChatWallpaper chatWallpaper, CustomChatColorCreatorState customChatColorCreatorState) {
                Intrinsics.checkNotNullExpressionValue(customChatColorCreatorState, "it");
                return CustomChatColorCreatorState.copy$default(customChatColorCreatorState, false, chatWallpaper, null, null, 0.0f, 29, null);
            }

            public final void invoke(ChatWallpaper chatWallpaper) {
                this.this$0.store.update(new CustomChatColorCreatorViewModel$1$$ExternalSyntheticLambda0(chatWallpaper));
            }
        });
        if (id instanceof ChatColors.Id.Custom) {
            customChatColorCreatorRepository.loadColors(id, new Function1<ChatColors, Unit>(this) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel.2
                final /* synthetic */ CustomChatColorCreatorViewModel this$0;

                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(ChatColors chatColors) {
                    invoke(chatColors);
                    return Unit.INSTANCE;
                }

                public final void invoke(ChatColors chatColors) {
                    Intrinsics.checkNotNullParameter(chatColors, "it");
                    int[] colors = chatColors.getColors();
                    int i2 = ArraysKt___ArraysKt.first(colors);
                    int i3 = ArraysKt___ArraysKt.last(colors);
                    float[] fArr = {0.0f, 0.0f, 0.0f};
                    float[] fArr2 = {0.0f, 0.0f, 0.0f};
                    ColorUtils.colorToHSL(i2, fArr);
                    ColorUtils.colorToHSL(i3, fArr2);
                    float f = fArr[0];
                    float f2 = fArr[1];
                    float f3 = fArr2[0];
                    float f4 = fArr2[1];
                    this.this$0.store.update(new CustomChatColorCreatorViewModel$2$$ExternalSyntheticLambda0(chatColors, new ColorSlidersState(MathKt__MathJVMKt.roundToInt((f / 360.0f) * ((float) this.this$0.maxSliderValue)), MathKt__MathJVMKt.roundToInt(f2 * ((float) this.this$0.maxSliderValue))), new ColorSlidersState(MathKt__MathJVMKt.roundToInt((f3 / 360.0f) * ((float) this.this$0.maxSliderValue)), MathKt__MathJVMKt.roundToInt(f4 * ((float) this.this$0.maxSliderValue)))));
                }

                /* renamed from: invoke$lambda-0 */
                public static final CustomChatColorCreatorState m1553invoke$lambda0(ChatColors chatColors, ColorSlidersState colorSlidersState, ColorSlidersState colorSlidersState2, CustomChatColorCreatorState customChatColorCreatorState) {
                    Intrinsics.checkNotNullParameter(chatColors, "$it");
                    Intrinsics.checkNotNullParameter(colorSlidersState, "$topEdge");
                    Intrinsics.checkNotNullParameter(colorSlidersState2, "$bottomEdge");
                    float degrees = chatColors.getDegrees();
                    EnumMap enumMap = new EnumMap(MapsKt__MapsKt.mapOf(TuplesKt.to(CustomChatColorEdge.TOP, colorSlidersState), TuplesKt.to(CustomChatColorEdge.BOTTOM, colorSlidersState2)));
                    Intrinsics.checkNotNullExpressionValue(customChatColorCreatorState, "state");
                    return CustomChatColorCreatorState.copy$default(customChatColorCreatorState, false, null, enumMap, null, degrees, 10, null);
                }
            });
        }
    }

    public final LiveData<CustomChatColorCreatorState> getState() {
        return this.state;
    }

    public final LiveData<Event> getEvents() {
        return this.events;
    }

    public final void setHueProgress(int i) {
        this.store.update(new Function(i) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CustomChatColorCreatorViewModel.m1548setHueProgress$lambda1(this.f$0, (CustomChatColorCreatorState) obj);
            }
        });
    }

    /* renamed from: setHueProgress$lambda-1 */
    public static final CustomChatColorCreatorState m1548setHueProgress$lambda1(int i, CustomChatColorCreatorState customChatColorCreatorState) {
        Intrinsics.checkNotNullExpressionValue(customChatColorCreatorState, "state");
        EnumMap<CustomChatColorEdge, ColorSlidersState> sliderStates = customChatColorCreatorState.getSliderStates();
        ColorSlidersState colorSlidersState = sliderStates.get(customChatColorCreatorState.getSelectedEdge());
        if (colorSlidersState != null) {
            sliderStates.put((EnumMap<CustomChatColorEdge, ColorSlidersState>) customChatColorCreatorState.getSelectedEdge(), (CustomChatColorEdge) ColorSlidersState.copy$default(colorSlidersState, i, 0, 2, null));
            Unit unit = Unit.INSTANCE;
            return CustomChatColorCreatorState.copy$default(customChatColorCreatorState, false, null, sliderStates, null, 0.0f, 27, null);
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public final void setSaturationProgress(int i) {
        this.store.update(new Function(i) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CustomChatColorCreatorViewModel.m1549setSaturationProgress$lambda3(this.f$0, (CustomChatColorCreatorState) obj);
            }
        });
    }

    /* renamed from: setSaturationProgress$lambda-3 */
    public static final CustomChatColorCreatorState m1549setSaturationProgress$lambda3(int i, CustomChatColorCreatorState customChatColorCreatorState) {
        Intrinsics.checkNotNullExpressionValue(customChatColorCreatorState, "state");
        EnumMap<CustomChatColorEdge, ColorSlidersState> sliderStates = customChatColorCreatorState.getSliderStates();
        ColorSlidersState colorSlidersState = sliderStates.get(customChatColorCreatorState.getSelectedEdge());
        if (colorSlidersState != null) {
            sliderStates.put((EnumMap<CustomChatColorEdge, ColorSlidersState>) customChatColorCreatorState.getSelectedEdge(), (CustomChatColorEdge) ColorSlidersState.copy$default(colorSlidersState, 0, i, 1, null));
            Unit unit = Unit.INSTANCE;
            return CustomChatColorCreatorState.copy$default(customChatColorCreatorState, false, null, sliderStates, null, 0.0f, 27, null);
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* renamed from: setDegrees$lambda-4 */
    public static final CustomChatColorCreatorState m1547setDegrees$lambda4(float f, CustomChatColorCreatorState customChatColorCreatorState) {
        Intrinsics.checkNotNullExpressionValue(customChatColorCreatorState, "it");
        return CustomChatColorCreatorState.copy$default(customChatColorCreatorState, false, null, null, null, f, 15, null);
    }

    public final void setDegrees(float f) {
        this.store.update(new Function(f) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ float f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CustomChatColorCreatorViewModel.m1547setDegrees$lambda4(this.f$0, (CustomChatColorCreatorState) obj);
            }
        });
    }

    /* renamed from: setSelectedEdge$lambda-5 */
    public static final CustomChatColorCreatorState m1550setSelectedEdge$lambda5(CustomChatColorEdge customChatColorEdge, CustomChatColorCreatorState customChatColorCreatorState) {
        Intrinsics.checkNotNullParameter(customChatColorEdge, "$selectedEdge");
        Intrinsics.checkNotNullExpressionValue(customChatColorCreatorState, "it");
        return CustomChatColorCreatorState.copy$default(customChatColorCreatorState, false, null, null, customChatColorEdge, 0.0f, 23, null);
    }

    public final void setSelectedEdge(CustomChatColorEdge customChatColorEdge) {
        Intrinsics.checkNotNullParameter(customChatColorEdge, "selectedEdge");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CustomChatColorCreatorViewModel.m1550setSelectedEdge$lambda5(CustomChatColorEdge.this, (CustomChatColorCreatorState) obj);
            }
        });
    }

    public final void startSave(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        ChatColors.Id id = this.chatColorsId;
        if (id instanceof ChatColors.Id.Custom) {
            this.repository.getUsageCount(id, new Function1<Integer, Unit>(this, chatColors) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel$startSave$1
                final /* synthetic */ ChatColors $chatColors;
                final /* synthetic */ CustomChatColorCreatorViewModel this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$chatColors = r2;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                    invoke(num.intValue());
                    return Unit.INSTANCE;
                }

                public final void invoke(int i) {
                    if (i > 0) {
                        this.this$0.internalEvents.postValue(new CustomChatColorCreatorViewModel.Event.Warn(i, this.$chatColors));
                    } else {
                        this.this$0.internalEvents.postValue(new CustomChatColorCreatorViewModel.Event.SaveNow(this.$chatColors));
                    }
                }
            });
        } else {
            this.internalEvents.postValue(new Event.SaveNow(chatColors));
        }
    }

    public final void saveNow(ChatColors chatColors, Function1<? super ChatColors, Unit> function1) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        Intrinsics.checkNotNullParameter(function1, "onSaved");
        this.repository.setChatColors(chatColors.withId(this.chatColorsId), function1);
    }

    private final CustomChatColorCreatorState getInitialState() {
        boolean z = this.chatColorsId instanceof ChatColors.Id.Custom;
        CustomChatColorEdge customChatColorEdge = CustomChatColorEdge.TOP;
        int i = this.maxSliderValue;
        CustomChatColorEdge customChatColorEdge2 = CustomChatColorEdge.BOTTOM;
        int i2 = this.maxSliderValue;
        return new CustomChatColorCreatorState(z, null, new EnumMap(MapsKt__MapsKt.mapOf(TuplesKt.to(customChatColorEdge, new ColorSlidersState(i / 2, i / 2)), TuplesKt.to(customChatColorEdge2, new ColorSlidersState(i2 / 2, i2 / 2)))), customChatColorEdge2, 180.0f);
    }

    /* compiled from: CustomChatColorCreatorViewModel.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ%\u0010\u000b\u001a\u0002H\f\"\b\b\u0000\u0010\f*\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\f0\u000fH\u0016¢\u0006\u0002\u0010\u0010R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "maxSliderValue", "", "chatColorsId", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "chatColorCreatorRepository", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorRepository;", "(ILorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final CustomChatColorCreatorRepository chatColorCreatorRepository;
        private final ChatColors.Id chatColorsId;
        private final int maxSliderValue;
        private final RecipientId recipientId;

        public Factory(int i, ChatColors.Id id, RecipientId recipientId, CustomChatColorCreatorRepository customChatColorCreatorRepository) {
            Intrinsics.checkNotNullParameter(id, "chatColorsId");
            Intrinsics.checkNotNullParameter(customChatColorCreatorRepository, "chatColorCreatorRepository");
            this.maxSliderValue = i;
            this.chatColorsId = id;
            this.recipientId = recipientId;
            this.chatColorCreatorRepository = customChatColorCreatorRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new CustomChatColorCreatorViewModel(this.maxSliderValue, this.chatColorsId, this.recipientId, this.chatColorCreatorRepository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* compiled from: CustomChatColorCreatorViewModel.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event;", "", "()V", "SaveNow", "Warn", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event$Warn;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event$SaveNow;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Event {
        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: CustomChatColorCreatorViewModel.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event$Warn;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event;", "usageCount", "", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "(ILorg/thoughtcrime/securesms/conversation/colors/ChatColors;)V", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getUsageCount", "()I", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Warn extends Event {
            private final ChatColors chatColors;
            private final int usageCount;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Warn(int i, ChatColors chatColors) {
                super(null);
                Intrinsics.checkNotNullParameter(chatColors, "chatColors");
                this.usageCount = i;
                this.chatColors = chatColors;
            }

            public final ChatColors getChatColors() {
                return this.chatColors;
            }

            public final int getUsageCount() {
                return this.usageCount;
            }
        }

        private Event() {
        }

        /* compiled from: CustomChatColorCreatorViewModel.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event$SaveNow;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorViewModel$Event;", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "(Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;)V", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SaveNow extends Event {
            private final ChatColors chatColors;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public SaveNow(ChatColors chatColors) {
                super(null);
                Intrinsics.checkNotNullParameter(chatColors, "chatColors");
                this.chatColors = chatColors;
            }

            public final ChatColors getChatColors() {
                return this.chatColors;
            }
        }
    }
}
