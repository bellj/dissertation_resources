package org.thoughtcrime.securesms.conversation.colors.ui;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: ChatSelectionContextMenu.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0011B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu;", "Landroid/widget/PopupWindow;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "callback", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu$Callback;", "getCallback", "()Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu$Callback;", "setCallback", "(Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu$Callback;)V", "getContext", "()Landroid/content/Context;", "show", "", "anchor", "Landroid/view/View;", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatSelectionContextMenu extends PopupWindow {
    private Callback callback;
    private final Context context;

    /* compiled from: ChatSelectionContextMenu.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0003H&J\b\u0010\u0005\u001a\u00020\u0003H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu$Callback;", "", "onDeletePressed", "", "onDuplicatePressed", "onEditPressed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void onDeletePressed();

        void onDuplicatePressed();

        void onEditPressed();
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatSelectionContextMenu(Context context) {
        super(context);
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
        setContentView(LayoutInflater.from(context).inflate(R.layout.chat_colors_fragment_context_menu, (ViewGroup) null, false));
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation((float) ViewUtil.dpToPx(8));
        }
        setOutsideTouchable(false);
        setFocusable(true);
        setWidth(ViewUtil.dpToPx(280));
        setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.round_background));
        View findViewById = getContentView().findViewById(R.id.context_menu_edit);
        Intrinsics.checkNotNullExpressionValue(findViewById, "contentView.findViewById(R.id.context_menu_edit)");
        View findViewById2 = getContentView().findViewById(R.id.context_menu_duplicate);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "contentView.findViewById…d.context_menu_duplicate)");
        View findViewById3 = getContentView().findViewById(R.id.context_menu_delete);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "contentView.findViewById(R.id.context_menu_delete)");
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatSelectionContextMenu.m1525_init_$lambda0(ChatSelectionContextMenu.this, view);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatSelectionContextMenu.m1526_init_$lambda1(ChatSelectionContextMenu.this, view);
            }
        });
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ChatSelectionContextMenu.m1527_init_$lambda2(ChatSelectionContextMenu.this, view);
            }
        });
    }

    public final Context getContext() {
        return this.context;
    }

    public final Callback getCallback() {
        return this.callback;
    }

    public final void setCallback(Callback callback) {
        this.callback = callback;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1525_init_$lambda0(ChatSelectionContextMenu chatSelectionContextMenu, View view) {
        Intrinsics.checkNotNullParameter(chatSelectionContextMenu, "this$0");
        chatSelectionContextMenu.dismiss();
        Callback callback = chatSelectionContextMenu.callback;
        if (callback != null) {
            callback.onEditPressed();
        }
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m1526_init_$lambda1(ChatSelectionContextMenu chatSelectionContextMenu, View view) {
        Intrinsics.checkNotNullParameter(chatSelectionContextMenu, "this$0");
        chatSelectionContextMenu.dismiss();
        Callback callback = chatSelectionContextMenu.callback;
        if (callback != null) {
            callback.onDuplicatePressed();
        }
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m1527_init_$lambda2(ChatSelectionContextMenu chatSelectionContextMenu, View view) {
        Intrinsics.checkNotNullParameter(chatSelectionContextMenu, "this$0");
        chatSelectionContextMenu.dismiss();
        Callback callback = chatSelectionContextMenu.callback;
        if (callback != null) {
            callback.onDeletePressed();
        }
    }

    public final void show(View view) {
        Intrinsics.checkNotNullParameter(view, "anchor");
        Rect rect = new Rect();
        View rootView = view.getRootView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView;
            view.getDrawingRect(rect);
            viewGroup.offsetDescendantRectToMyCoords(view, rect);
            getContentView().measure(0, 0);
            if (rect.bottom + getContentView().getMeasuredHeight() > viewGroup.getBottom()) {
                showAsDropDown(view, 0, -(getContentView().getMeasuredHeight() + view.getHeight()));
            } else {
                showAsDropDown(view, 0, 0);
            }
        } else {
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }
    }
}
