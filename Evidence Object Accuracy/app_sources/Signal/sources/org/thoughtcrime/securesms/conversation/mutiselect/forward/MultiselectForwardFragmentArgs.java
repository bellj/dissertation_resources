package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.content.Context;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.mutiselect.Multiselect;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.mms.StickerSlide;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.MediaUtil;

/* compiled from: MultiselectForwardFragmentArgs.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0019\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 22\u00020\u0001:\u00012B[\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0003\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003\u0012\b\b\u0002\u0010\n\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u000b\u001a\u00020\u0003\u0012\b\b\u0003\u0010\f\u001a\u00020\b\u0012\b\b\u0002\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010\u001e\u001a\u00020\bHÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\bHÆ\u0003J\t\u0010#\u001a\u00020\u000eHÆ\u0003J_\u0010$\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0003\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0003\u0010\f\u001a\u00020\b2\b\b\u0002\u0010\r\u001a\u00020\u000eHÆ\u0001J\t\u0010%\u001a\u00020\bHÖ\u0001J\u0013\u0010&\u001a\u00020\u00032\b\u0010'\u001a\u0004\u0018\u00010(HÖ\u0003J\t\u0010)\u001a\u00020\bHÖ\u0001J\t\u0010*\u001a\u00020+HÖ\u0001J\u0010\u0010,\u001a\u00020\u00002\b\b\u0001\u0010\f\u001a\u00020\bJ\u0019\u0010-\u001a\u00020.2\u0006\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\bHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0011\u0010\f\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0018¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "Landroid/os/Parcelable;", "canSendToNonPush", "", "multiShareArgs", "", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", MultiselectForwardFragment.DIALOG_TITLE, "", "forceDisableAddMessage", "forceSelectionOnly", "selectSingleRecipient", "sendButtonTint", "storySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "(ZLjava/util/List;IZZZILorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;)V", "getCanSendToNonPush", "()Z", "getForceDisableAddMessage", "getForceSelectionOnly", "getMultiShareArgs", "()Ljava/util/List;", "getSelectSingleRecipient", "getSendButtonTint", "()I", "getStorySendRequirements", "()Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "getTitle", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "describeContents", "equals", "other", "", "hashCode", "toString", "", "withSendButtonTint", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardFragmentArgs implements Parcelable {
    public static final Parcelable.Creator<MultiselectForwardFragmentArgs> CREATOR = new Creator();
    public static final Companion Companion = new Companion(null);
    private final boolean canSendToNonPush;
    private final boolean forceDisableAddMessage;
    private final boolean forceSelectionOnly;
    private final List<MultiShareArgs> multiShareArgs;
    private final boolean selectSingleRecipient;
    private final int sendButtonTint;
    private final Stories.MediaTransform.SendRequirements storySendRequirements;
    private final int title;

    /* compiled from: MultiselectForwardFragmentArgs.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Creator implements Parcelable.Creator<MultiselectForwardFragmentArgs> {
        @Override // android.os.Parcelable.Creator
        public final MultiselectForwardFragmentArgs createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            boolean z = parcel.readInt() != 0;
            int readInt = parcel.readInt();
            ArrayList arrayList = new ArrayList(readInt);
            for (int i = 0; i != readInt; i++) {
                arrayList.add(parcel.readParcelable(MultiselectForwardFragmentArgs.class.getClassLoader()));
            }
            return new MultiselectForwardFragmentArgs(z, arrayList, parcel.readInt(), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt() != 0, parcel.readInt(), Stories.MediaTransform.SendRequirements.valueOf(parcel.readString()));
        }

        @Override // android.os.Parcelable.Creator
        public final MultiselectForwardFragmentArgs[] newArray(int i) {
            return new MultiselectForwardFragmentArgs[i];
        }
    }

    public MultiselectForwardFragmentArgs(boolean z) {
        this(z, null, 0, false, false, false, 0, null, 254, null);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list) {
        this(z, list, 0, false, false, false, 0, null, 252, null);
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list, int i) {
        this(z, list, i, false, false, false, 0, null, 248, null);
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list, int i, boolean z2) {
        this(z, list, i, z2, false, false, 0, null, 240, null);
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list, int i, boolean z2, boolean z3) {
        this(z, list, i, z2, z3, false, 0, null, 224, null);
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list, int i, boolean z2, boolean z3, boolean z4) {
        this(z, list, i, z2, z3, z4, 0, null, 192, null);
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list, int i, boolean z2, boolean z3, boolean z4, int i2) {
        this(z, list, i, z2, z3, z4, i2, null, 128, null);
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
    }

    public static /* synthetic */ MultiselectForwardFragmentArgs copy$default(MultiselectForwardFragmentArgs multiselectForwardFragmentArgs, boolean z, List list, int i, boolean z2, boolean z3, boolean z4, int i2, Stories.MediaTransform.SendRequirements sendRequirements, int i3, Object obj) {
        return multiselectForwardFragmentArgs.copy((i3 & 1) != 0 ? multiselectForwardFragmentArgs.canSendToNonPush : z, (i3 & 2) != 0 ? multiselectForwardFragmentArgs.multiShareArgs : list, (i3 & 4) != 0 ? multiselectForwardFragmentArgs.title : i, (i3 & 8) != 0 ? multiselectForwardFragmentArgs.forceDisableAddMessage : z2, (i3 & 16) != 0 ? multiselectForwardFragmentArgs.forceSelectionOnly : z3, (i3 & 32) != 0 ? multiselectForwardFragmentArgs.selectSingleRecipient : z4, (i3 & 64) != 0 ? multiselectForwardFragmentArgs.sendButtonTint : i2, (i3 & 128) != 0 ? multiselectForwardFragmentArgs.storySendRequirements : sendRequirements);
    }

    @JvmStatic
    public static final void create(Context context, long j, Uri uri, String str, Consumer<MultiselectForwardFragmentArgs> consumer) {
        Companion.create(context, j, uri, str, consumer);
    }

    @JvmStatic
    public static final void create(Context context, Set<? extends MultiselectPart> set, Consumer<MultiselectForwardFragmentArgs> consumer) {
        Companion.create(context, set, consumer);
    }

    public final boolean component1() {
        return this.canSendToNonPush;
    }

    public final List<MultiShareArgs> component2() {
        return this.multiShareArgs;
    }

    public final int component3() {
        return this.title;
    }

    public final boolean component4() {
        return this.forceDisableAddMessage;
    }

    public final boolean component5() {
        return this.forceSelectionOnly;
    }

    public final boolean component6() {
        return this.selectSingleRecipient;
    }

    public final int component7() {
        return this.sendButtonTint;
    }

    public final Stories.MediaTransform.SendRequirements component8() {
        return this.storySendRequirements;
    }

    public final MultiselectForwardFragmentArgs copy(boolean z, List<MultiShareArgs> list, int i, boolean z2, boolean z3, boolean z4, int i2, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        return new MultiselectForwardFragmentArgs(z, list, i, z2, z3, z4, i2, sendRequirements);
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MultiselectForwardFragmentArgs)) {
            return false;
        }
        MultiselectForwardFragmentArgs multiselectForwardFragmentArgs = (MultiselectForwardFragmentArgs) obj;
        return this.canSendToNonPush == multiselectForwardFragmentArgs.canSendToNonPush && Intrinsics.areEqual(this.multiShareArgs, multiselectForwardFragmentArgs.multiShareArgs) && this.title == multiselectForwardFragmentArgs.title && this.forceDisableAddMessage == multiselectForwardFragmentArgs.forceDisableAddMessage && this.forceSelectionOnly == multiselectForwardFragmentArgs.forceSelectionOnly && this.selectSingleRecipient == multiselectForwardFragmentArgs.selectSingleRecipient && this.sendButtonTint == multiselectForwardFragmentArgs.sendButtonTint && this.storySendRequirements == multiselectForwardFragmentArgs.storySendRequirements;
    }

    @Override // java.lang.Object
    public int hashCode() {
        boolean z = this.canSendToNonPush;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode = ((((i2 * 31) + this.multiShareArgs.hashCode()) * 31) + this.title) * 31;
        boolean z2 = this.forceDisableAddMessage;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = (hashCode + i5) * 31;
        boolean z3 = this.forceSelectionOnly;
        if (z3) {
            z3 = true;
        }
        int i9 = z3 ? 1 : 0;
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = (i8 + i9) * 31;
        boolean z4 = this.selectSingleRecipient;
        if (!z4) {
            i = z4 ? 1 : 0;
        }
        return ((((i12 + i) * 31) + this.sendButtonTint) * 31) + this.storySendRequirements.hashCode();
    }

    @Override // java.lang.Object
    public String toString() {
        return "MultiselectForwardFragmentArgs(canSendToNonPush=" + this.canSendToNonPush + ", multiShareArgs=" + this.multiShareArgs + ", title=" + this.title + ", forceDisableAddMessage=" + this.forceDisableAddMessage + ", forceSelectionOnly=" + this.forceSelectionOnly + ", selectSingleRecipient=" + this.selectSingleRecipient + ", sendButtonTint=" + this.sendButtonTint + ", storySendRequirements=" + this.storySendRequirements + ')';
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        parcel.writeInt(this.canSendToNonPush ? 1 : 0);
        List<MultiShareArgs> list = this.multiShareArgs;
        parcel.writeInt(list.size());
        for (MultiShareArgs multiShareArgs : list) {
            parcel.writeParcelable(multiShareArgs, i);
        }
        parcel.writeInt(this.title);
        parcel.writeInt(this.forceDisableAddMessage ? 1 : 0);
        parcel.writeInt(this.forceSelectionOnly ? 1 : 0);
        parcel.writeInt(this.selectSingleRecipient ? 1 : 0);
        parcel.writeInt(this.sendButtonTint);
        parcel.writeString(this.storySendRequirements.name());
    }

    public MultiselectForwardFragmentArgs(boolean z, List<MultiShareArgs> list, int i, boolean z2, boolean z3, boolean z4, int i2, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        this.canSendToNonPush = z;
        this.multiShareArgs = list;
        this.title = i;
        this.forceDisableAddMessage = z2;
        this.forceSelectionOnly = z3;
        this.selectSingleRecipient = z4;
        this.sendButtonTint = i2;
        this.storySendRequirements = sendRequirements;
    }

    public final boolean getCanSendToNonPush() {
        return this.canSendToNonPush;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ MultiselectForwardFragmentArgs(boolean r8, java.util.List r9, int r10, boolean r11, boolean r12, boolean r13, int r14, org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements r15, int r16, kotlin.jvm.internal.DefaultConstructorMarker r17) {
        /*
            r7 = this;
            r0 = r16
            r1 = r0 & 2
            if (r1 == 0) goto L_0x000b
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x000c
        L_0x000b:
            r1 = r9
        L_0x000c:
            r2 = r0 & 4
            if (r2 == 0) goto L_0x0014
            r2 = 2131953199(0x7f13062f, float:1.9542862E38)
            goto L_0x0015
        L_0x0014:
            r2 = r10
        L_0x0015:
            r3 = r0 & 8
            r4 = 0
            if (r3 == 0) goto L_0x001c
            r3 = 0
            goto L_0x001d
        L_0x001c:
            r3 = r11
        L_0x001d:
            r5 = r0 & 16
            if (r5 == 0) goto L_0x0023
            r5 = 0
            goto L_0x0024
        L_0x0023:
            r5 = r12
        L_0x0024:
            r6 = r0 & 32
            if (r6 == 0) goto L_0x0029
            goto L_0x002a
        L_0x0029:
            r4 = r13
        L_0x002a:
            r6 = r0 & 64
            if (r6 == 0) goto L_0x0030
            r6 = -1
            goto L_0x0031
        L_0x0030:
            r6 = r14
        L_0x0031:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0038
            org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements r0 = org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements.CAN_NOT_SEND
            goto L_0x0039
        L_0x0038:
            r0 = r15
        L_0x0039:
            r9 = r7
            r10 = r8
            r11 = r1
            r12 = r2
            r13 = r3
            r14 = r5
            r15 = r4
            r16 = r6
            r17 = r0
            r9.<init>(r10, r11, r12, r13, r14, r15, r16, r17)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs.<init>(boolean, java.util.List, int, boolean, boolean, boolean, int, org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<MultiShareArgs> getMultiShareArgs() {
        return this.multiShareArgs;
    }

    public final int getTitle() {
        return this.title;
    }

    public final boolean getForceDisableAddMessage() {
        return this.forceDisableAddMessage;
    }

    public final boolean getForceSelectionOnly() {
        return this.forceSelectionOnly;
    }

    public final boolean getSelectSingleRecipient() {
        return this.selectSingleRecipient;
    }

    public final int getSendButtonTint() {
        return this.sendButtonTint;
    }

    public final Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return this.storySendRequirements;
    }

    public final MultiselectForwardFragmentArgs withSendButtonTint(int i) {
        return copy$default(this, false, null, 0, false, false, false, i, null, 191, null);
    }

    /* compiled from: MultiselectForwardFragmentArgs.kt */
    @Metadata(bv = {}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0019\u0010\u001aJ&\u0010\n\u001a\u00020\t2\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u00042\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0003J\u000e\u0010\r\u001a\u0004\u0018\u00010\f*\u00020\u000bH\u0002J6\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00122\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014H\u0007J,\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0003\u001a\u00020\u00022\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014H\u0007¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs$Companion;", "", "Landroid/content/Context;", "context", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "conversationMessage", "", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "selectedParts", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "buildMultiShareArgs", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "Lorg/thoughtcrime/securesms/mediasend/Media;", "toMedia", "", "threadId", "Landroid/net/Uri;", "mediaUri", "", "mediaType", "j$/util/function/Consumer", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "consumer", "", "create", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void create(Context context, long j, Uri uri, String str, Consumer<MultiselectForwardFragmentArgs> consumer) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(uri, "mediaUri");
            Intrinsics.checkNotNullParameter(str, "mediaType");
            Intrinsics.checkNotNullParameter(consumer, "consumer");
            SignalExecutors.BOUNDED.execute(new MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda0(context, uri, str, j, consumer));
        }

        /* renamed from: create$lambda-3 */
        public static final void m1578create$lambda3(Context context, Uri uri, String str, long j, Consumer consumer) {
            Recipient recipientForThreadId;
            ChatColors chatColors;
            Intrinsics.checkNotNullParameter(context, "$context");
            Intrinsics.checkNotNullParameter(uri, "$mediaUri");
            Intrinsics.checkNotNullParameter(str, "$mediaType");
            Intrinsics.checkNotNullParameter(consumer, "$consumer");
            boolean isMmsSupported = Multiselect.INSTANCE.isMmsSupported(context, uri, str, MediaUtil.getMediaSize(context, uri));
            MultiShareArgs build = new MultiShareArgs.Builder(SetsKt__SetsKt.emptySet()).withDataUri(uri).withDataType(str).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(setOf())\n       …aType)\n          .build()");
            Long valueOf = Long.valueOf(j);
            if (!(valueOf.longValue() > 0)) {
                valueOf = null;
            }
            ThreadUtil.runOnMain(new MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda2(consumer, build, isMmsSupported, (valueOf == null || (recipientForThreadId = SignalDatabase.Companion.threads().getRecipientForThreadId(valueOf.longValue())) == null || (chatColors = recipientForThreadId.getChatColors()) == null) ? -1 : chatColors.asSingleColor()));
        }

        /* renamed from: create$lambda-3$lambda-2 */
        public static final void m1579create$lambda3$lambda2(Consumer consumer, MultiShareArgs multiShareArgs, boolean z, int i) {
            Intrinsics.checkNotNullParameter(consumer, "$consumer");
            Intrinsics.checkNotNullParameter(multiShareArgs, "$multiShareArgs");
            consumer.accept(new MultiselectForwardFragmentArgs(z, CollectionsKt__CollectionsJVMKt.listOf(multiShareArgs), 0, false, false, false, i, Stories.MediaTransform.SendRequirements.CAN_NOT_SEND, 60, null));
        }

        @JvmStatic
        public final void create(Context context, Set<? extends MultiselectPart> set, Consumer<MultiselectForwardFragmentArgs> consumer) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(set, "selectedParts");
            Intrinsics.checkNotNullParameter(consumer, "consumer");
            SignalExecutors.BOUNDED.execute(new MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda3(set, context, consumer));
        }

        /* renamed from: create$lambda-9$lambda-8 */
        public static final void m1581create$lambda9$lambda8(Consumer consumer, boolean z, List list) {
            Intrinsics.checkNotNullParameter(consumer, "$consumer");
            Intrinsics.checkNotNullParameter(list, "$multiShareArgs");
            consumer.accept(new MultiselectForwardFragmentArgs(z, list, 0, false, false, false, 0, Stories.MediaTransform.SendRequirements.CAN_NOT_SEND, 124, null));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v3, resolved type: java.lang.String */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r3v2, types: [java.lang.Throwable] */
        /* JADX WARN: Type inference failed for: r3v8 */
        /* JADX WARN: Type inference failed for: r3v9 */
        private final MultiShareArgs buildMultiShareArgs(Context context, ConversationMessage conversationMessage, Set<? extends MultiselectPart> set) {
            Attachment asAttachment;
            Attachment asAttachment2;
            Attachment asAttachment3;
            StoryType storyType;
            List<LinkPreview> linkPreviews;
            SlideDeck slideDeck;
            TextSlide textSlide;
            MultiShareArgs.Builder withExpiration = new MultiShareArgs.Builder(SetsKt__SetsKt.emptySet()).withMentions(conversationMessage.getMentions()).withTimestamp(conversationMessage.getMessageRecord().getTimestamp()).withExpiration(conversationMessage.getMessageRecord().getExpireStarted() + conversationMessage.getMessageRecord().getExpiresIn());
            Intrinsics.checkNotNullExpressionValue(withExpiration, "Builder(setOf())\n       ….messageRecord.expiresIn)");
            String str = 0;
            str = 0;
            if (conversationMessage.getMultiselectCollection().isTextSelected(set)) {
                MessageRecord messageRecord = conversationMessage.getMessageRecord();
                MmsMessageRecord mmsMessageRecord = messageRecord instanceof MmsMessageRecord ? (MmsMessageRecord) messageRecord : null;
                Uri uri = (mmsMessageRecord == null || (slideDeck = mmsMessageRecord.getSlideDeck()) == null || (textSlide = slideDeck.getTextSlide()) == null) ? null : textSlide.getUri();
                if (uri != null) {
                    InputStream attachmentStream = PartAuthority.getAttachmentStream(context, uri);
                    try {
                        ConversationMessage createWithUnresolvedData = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(context, mmsMessageRecord, StreamUtil.readFullyAsString(attachmentStream));
                        Intrinsics.checkNotNullExpressionValue(createWithUnresolvedData, "createWithUnresolvedData…text, mediaMessage, body)");
                        withExpiration.withDraftText(createWithUnresolvedData.getDisplayBody(context).toString());
                    } finally {
                        try {
                            throw th;
                        } finally {
                        }
                    }
                } else {
                    withExpiration.withDraftText(conversationMessage.getDisplayBody(context).toString());
                }
                withExpiration.withLinkPreview((mmsMessageRecord == null || (linkPreviews = mmsMessageRecord.getLinkPreviews()) == null) ? null : (LinkPreview) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) linkPreviews)));
                withExpiration.asTextStory((mmsMessageRecord == null || (storyType = mmsMessageRecord.getStoryType()) == null) ? false : storyType.isTextStory());
            }
            if (conversationMessage.getMessageRecord().isMms() && conversationMessage.getMultiselectCollection().isMediaSelected(set)) {
                MmsMessageRecord mmsMessageRecord2 = (MmsMessageRecord) conversationMessage.getMessageRecord();
                if (mmsMessageRecord2.containsMediaSlide() && mmsMessageRecord2.getSlideDeck().getSlides().size() > 1 && mmsMessageRecord2.getSlideDeck().getAudioSlide() == null && mmsMessageRecord2.getSlideDeck().getDocumentSlide() == null && mmsMessageRecord2.getSlideDeck().getStickerSlide() == null) {
                    ArrayList arrayList = new ArrayList(mmsMessageRecord2.getSlideDeck().getSlides().size());
                    List<Slide> slides = mmsMessageRecord2.getSlideDeck().getSlides();
                    Intrinsics.checkNotNullExpressionValue(slides, "mediaMessage.slideDeck.slides");
                    ArrayList<Slide> arrayList2 = new ArrayList();
                    for (Object obj : slides) {
                        Slide slide = (Slide) obj;
                        if (slide.hasImage() || slide.hasVideo()) {
                            arrayList2.add(obj);
                        }
                    }
                    ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList2, 10));
                    for (Slide slide2 : arrayList2) {
                        arrayList3.add(slide2.asAttachment());
                    }
                    for (Attachment attachment : CollectionsKt___CollectionsKt.toList(arrayList3)) {
                        Companion companion = MultiselectForwardFragmentArgs.Companion;
                        Intrinsics.checkNotNullExpressionValue(attachment, "attachment");
                        Media media = companion.toMedia(attachment);
                        if (media != null) {
                            arrayList.add(media);
                        }
                    }
                    if (!arrayList.isEmpty()) {
                        withExpiration.withMedia(arrayList);
                    }
                } else if (mmsMessageRecord2.containsMediaSlide()) {
                    withExpiration.withMedia(CollectionsKt__CollectionsKt.emptyList());
                    if (mmsMessageRecord2.getSlideDeck().getStickerSlide() != null) {
                        StickerSlide stickerSlide = mmsMessageRecord2.getSlideDeck().getStickerSlide();
                        withExpiration.withDataUri((stickerSlide == null || (asAttachment3 = stickerSlide.asAttachment()) == null) ? null : asAttachment3.getUri());
                        StickerSlide stickerSlide2 = mmsMessageRecord2.getSlideDeck().getStickerSlide();
                        withExpiration.withStickerLocator((stickerSlide2 == null || (asAttachment2 = stickerSlide2.asAttachment()) == null) ? null : asAttachment2.getSticker());
                        StickerSlide stickerSlide3 = mmsMessageRecord2.getSlideDeck().getStickerSlide();
                        if (!(stickerSlide3 == null || (asAttachment = stickerSlide3.asAttachment()) == null)) {
                            str = asAttachment.getContentType();
                        }
                        withExpiration.withDataType(str);
                    }
                    Attachment asAttachment4 = mmsMessageRecord2.getSlideDeck().getSlides().get(0).asAttachment();
                    Intrinsics.checkNotNullExpressionValue(asAttachment4, "firstSlide.asAttachment()");
                    Media media2 = toMedia(asAttachment4);
                    if (media2 != null) {
                        withExpiration.asBorderless(media2.isBorderless());
                        withExpiration.withMedia(CollectionsKt__CollectionsJVMKt.listOf(media2));
                    }
                }
            }
            MultiShareArgs build = withExpiration.build();
            Intrinsics.checkNotNullExpressionValue(build, "builder.build()");
            return build;
        }

        private final Media toMedia(Attachment attachment) {
            Uri uri = attachment.getUri();
            if (uri == null) {
                return null;
            }
            return new Media(uri, attachment.getContentType(), System.currentTimeMillis(), attachment.getWidth(), attachment.getHeight(), attachment.getSize(), 0, attachment.isBorderless(), attachment.isVideoGif(), Optional.empty(), Optional.ofNullable(attachment.getCaption()), Optional.of(attachment.getTransformProperties()));
        }

        /* renamed from: create$lambda-9 */
        public static final void m1580create$lambda9(Set set, Context context, Consumer consumer) {
            boolean z;
            Intrinsics.checkNotNullParameter(set, "$selectedParts");
            Intrinsics.checkNotNullParameter(context, "$context");
            Intrinsics.checkNotNullParameter(consumer, "$consumer");
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
            Iterator it = set.iterator();
            while (it.hasNext()) {
                arrayList.add(((MultiselectPart) it.next()).getConversationMessage());
            }
            Set<ConversationMessage> set2 = CollectionsKt___CollectionsKt.toSet(arrayList);
            boolean z2 = true;
            if (!(set2 instanceof Collection) || !set2.isEmpty()) {
                for (ConversationMessage conversationMessage : set2) {
                    if (conversationMessage.getMessageRecord().isViewOnce()) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (!z) {
                if (!(set instanceof Collection) || !set.isEmpty()) {
                    Iterator it2 = set.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        }
                        if (!Multiselect.INSTANCE.canSendToNonPush(context, (MultiselectPart) it2.next())) {
                            z2 = false;
                            break;
                        }
                    }
                }
                ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set2, 10));
                for (ConversationMessage conversationMessage2 : set2) {
                    arrayList2.add(MultiselectForwardFragmentArgs.Companion.buildMultiShareArgs(context, conversationMessage2, set));
                }
                ThreadUtil.runOnMain(new MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda1(consumer, z2, arrayList2));
                return;
            }
            throw new AssertionError("Cannot forward view once media");
        }
    }
}
