package org.thoughtcrime.securesms.conversation;

import android.os.Handler;
import android.os.Looper;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ConversationUpdateTick.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u000f2\u00020\u0001:\u0002\u000f\u0010B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\u000e\u001a\u00020\nH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationUpdateTick;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "onTickListener", "Lorg/thoughtcrime/securesms/conversation/ConversationUpdateTick$OnTickListener;", "(Lorg/thoughtcrime/securesms/conversation/ConversationUpdateTick$OnTickListener;)V", "handler", "Landroid/os/Handler;", "isResumed", "", "onPause", "", "owner", "Landroidx/lifecycle/LifecycleOwner;", "onResume", "onTick", "Companion", "OnTickListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationUpdateTick implements DefaultLifecycleObserver {
    public static final Companion Companion = new Companion(null);
    private static final long TIMEOUT = TimeUnit.MINUTES.toMillis(1);
    private final Handler handler = new Handler(Looper.getMainLooper());
    private boolean isResumed;
    private final OnTickListener onTickListener;

    /* compiled from: ConversationUpdateTick.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationUpdateTick$OnTickListener;", "", "onTick", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface OnTickListener {
        void onTick();
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public ConversationUpdateTick(OnTickListener onTickListener) {
        Intrinsics.checkNotNullParameter(onTickListener, "onTickListener");
        this.onTickListener = onTickListener;
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onResume(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        this.isResumed = true;
        this.handler.removeCallbacksAndMessages(null);
        onTick();
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        this.isResumed = false;
        this.handler.removeCallbacksAndMessages(null);
    }

    public final void onTick() {
        if (this.isResumed) {
            this.onTickListener.onTick();
            this.handler.removeCallbacksAndMessages(null);
            this.handler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateTick$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationUpdateTick.this.onTick();
                }
            }, TIMEOUT);
        }
    }

    /* compiled from: ConversationUpdateTick.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationUpdateTick$Companion;", "", "()V", "TIMEOUT", "", "getTIMEOUT$annotations", "getTIMEOUT", "()J", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ void getTIMEOUT$annotations() {
        }

        private Companion() {
        }

        public final long getTIMEOUT() {
            return ConversationUpdateTick.TIMEOUT;
        }
    }
}
