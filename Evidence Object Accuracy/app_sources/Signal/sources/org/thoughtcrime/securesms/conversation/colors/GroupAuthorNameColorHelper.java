package org.thoughtcrime.securesms.conversation.colors;

import j$.util.Map;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: GroupAuthorNameColorHelper.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t2\b\b\u0001\u0010\f\u001a\u00020\u0005R \u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00060\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/GroupAuthorNameColorHelper;", "", "()V", "fullMemberCache", "", "Lorg/thoughtcrime/securesms/groups/GroupId;", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getColorMap", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "groupId", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupAuthorNameColorHelper {
    private final Map<GroupId, Set<Recipient>> fullMemberCache = new LinkedHashMap();

    public final Map<RecipientId, NameColor> getColorMap(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        List<Recipient> groupMembers = SignalDatabase.Companion.groups().getGroupMembers(groupId, GroupDatabase.MemberSet.FULL_MEMBERS_INCLUDING_SELF);
        Intrinsics.checkNotNullExpressionValue(groupMembers, "SignalDatabase\n      .gr…L_MEMBERS_INCLUDING_SELF)");
        Set<Recipient> set = SetsKt___SetsKt.plus((Set) Map.EL.getOrDefault(this.fullMemberCache, groupId, SetsKt__SetsKt.emptySet()), (Iterable) CollectionsKt___CollectionsKt.toSet(groupMembers));
        this.fullMemberCache.put(groupId, set);
        ArrayList arrayList = new ArrayList();
        for (Object obj : set) {
            if (!Intrinsics.areEqual((Recipient) obj, Recipient.self())) {
                arrayList.add(obj);
            }
        }
        List list = CollectionsKt___CollectionsKt.sortedWith(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.conversation.colors.GroupAuthorNameColorHelper$getColorMap$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return ComparisonsKt__ComparisonsKt.compareValues(((Recipient) t).requireStringId(), ((Recipient) t2).requireStringId());
            }
        });
        List<NameColor> all = ChatColorsPalette.Names.getAll();
        HashMap hashMap = new HashMap();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            RecipientId id = ((Recipient) list.get(i)).getId();
            Intrinsics.checkNotNullExpressionValue(id, "members[i].id");
            hashMap.put(id, all.get(i % all.size()));
        }
        return MapsKt__MapsKt.toMap(hashMap);
    }
}
