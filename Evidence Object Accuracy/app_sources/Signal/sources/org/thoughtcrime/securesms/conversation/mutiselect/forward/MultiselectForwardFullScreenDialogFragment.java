package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FullScreenDialogFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MultiselectForwardFullScreenDialogFragment.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\u0017B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0004\u001a\u00020\u0005H\u0016J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0014J\n\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\b\u0010\r\u001a\u00020\tH\u0014J\b\u0010\u000e\u001a\u00020\u0005H\u0016J\b\u0010\u000f\u001a\u00020\u0005H\u0016J\u001a\u0010\u0010\u001a\u00020\u00052\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J\u0010\u0010\u0015\u001a\u00020\u00052\u0006\u0010\u0016\u001a\u00020\u0014H\u0016¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFullScreenDialogFragment;", "Lorg/thoughtcrime/securesms/components/FullScreenDialogFragment;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "()V", "exitFlow", "", "getContainer", "Landroid/view/ViewGroup;", "getDialogBackgroundColor", "", "getDialogLayoutResource", "getStorySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "getTitle", "onFinishForwardAction", "onSearchInputFocused", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setResult", "bundle", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardFullScreenDialogFragment extends FullScreenDialogFragment implements MultiselectForwardFragment.Callback {

    /* compiled from: MultiselectForwardFullScreenDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\n\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u0016J\b\u0010\u0004\u001a\u00020\u0005H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFullScreenDialogFragment$Callback;", "", "getStorySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "onFinishForwardAction", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {

        /* compiled from: MultiselectForwardFullScreenDialogFragment.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DefaultImpls {
            public static Stories.MediaTransform.SendRequirements getStorySendRequirements(Callback callback) {
                return null;
            }

            public static void onFinishForwardAction(Callback callback) {
            }
        }

        Stories.MediaTransform.SendRequirements getStorySendRequirements();

        void onFinishForwardAction();
    }

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getDialogLayoutResource() {
        return R.layout.fragment_container;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onSearchInputFocused() {
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001c */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        /*
            r3 = this;
            androidx.fragment.app.Fragment r0 = r3.getParentFragment()
        L_0x0004:
            r1 = 0
            if (r0 == 0) goto L_0x0011
            boolean r2 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback
            if (r2 == 0) goto L_0x000c
            goto L_0x001c
        L_0x000c:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0011:
            androidx.fragment.app.FragmentActivity r0 = r3.requireActivity()
            boolean r2 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback
            if (r2 != 0) goto L_0x001a
            r0 = r1
        L_0x001a:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment$Callback r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback) r0
        L_0x001c:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment$Callback r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback) r0
            if (r0 == 0) goto L_0x0024
            org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements r1 = r0.getStorySendRequirements()
        L_0x0024:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.getStorySendRequirements():org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x001b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onFinishForwardAction() {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x0004:
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback
            if (r1 == 0) goto L_0x000b
            goto L_0x001b
        L_0x000b:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0010:
            androidx.fragment.app.FragmentActivity r0 = r2.requireActivity()
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback
            if (r1 != 0) goto L_0x0019
            r0 = 0
        L_0x0019:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment$Callback r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback) r0
        L_0x001b:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment$Callback r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.Callback) r0
            if (r0 == 0) goto L_0x0022
            r0.onFinishForwardAction()
        L_0x0022:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFullScreenDialogFragment.onFinishForwardAction():void");
    }

    @Override // org.thoughtcrime.securesms.components.FullScreenDialogFragment
    protected int getTitle() {
        return requireArguments().getInt(MultiselectForwardFragment.DIALOG_TITLE);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        if (bundle == null) {
            MultiselectForwardFragment multiselectForwardFragment = new MultiselectForwardFragment();
            multiselectForwardFragment.setArguments(requireArguments());
            getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, multiselectForwardFragment).commitAllowingStateLoss();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public int getDialogBackgroundColor() {
        return ContextCompat.getColor(requireContext(), R.color.signal_background_primary);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public ViewGroup getContainer() {
        View findViewById = requireView().findViewById(R.id.full_screen_dialog_content);
        if (findViewById != null) {
            return (ViewGroup) findViewById;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void setResult(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        FragmentKt.setFragmentResult(this, MultiselectForwardFragment.RESULT_KEY, bundle);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void exitFlow() {
        dismissAllowingStateLoss();
    }
}
