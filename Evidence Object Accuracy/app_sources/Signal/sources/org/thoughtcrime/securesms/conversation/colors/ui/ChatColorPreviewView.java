package org.thoughtcrime.securesms.conversation.colors.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.DeliveryStatusView;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.colors.ColorizerView;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: ChatColorPreviewView.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001'B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u0019\u001a\u00020\u001aH\u0014J0\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u00072\u0006\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u00072\u0006\u0010!\u001a\u00020\u0007H\u0014J\b\u0010\"\u001a\u00020\u001aH\u0002J\u000e\u0010#\u001a\u00020\u001a2\u0006\u0010\n\u001a\u00020\u000bJ\u0010\u0010$\u001a\u00020\u001a2\b\u0010%\u001a\u0004\u0018\u00010&R\u000e\u0010\t\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0011X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorPreviewView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bubbleCount", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "colorizer", "Lorg/thoughtcrime/securesms/conversation/colors/Colorizer;", "colorizerView", "Lorg/thoughtcrime/securesms/conversation/colors/ColorizerView;", "recv1", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorPreviewView$Bubble;", "recv2", "sent1", "sent2", "wallpaper", "Landroid/widget/ImageView;", "wallpaperDim", "Landroid/view/View;", "onAttachedToWindow", "", "onLayout", "changed", "", "left", "top", "right", "bottom", "redrawChatColors", "setChatColors", "setWallpaper", "chatWallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "Bubble", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorPreviewView extends ConstraintLayout {
    private final int bubbleCount;
    private ChatColors chatColors;
    private final Colorizer colorizer;
    private final ColorizerView colorizerView;
    private final Bubble recv1;
    private final Bubble recv2;
    private final Bubble sent1;
    private final Bubble sent2;
    private final ImageView wallpaper;
    private final View wallpaperDim;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ChatColorPreviewView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ChatColorPreviewView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ ChatColorPreviewView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatColorPreviewView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Throwable th;
        TypedArray obtainStyledAttributes;
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.chat_colors_preview_view, this);
        TypedArray typedArray = null;
        try {
            obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ChatColorPreviewView, 0, 0);
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            this.bubbleCount = obtainStyledAttributes.getInteger(0, 2);
            View findViewById = findViewById(R.id.bubble_1);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.bubble_1)");
            View findViewById2 = findViewById(R.id.bubble_1_text);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.bubble_1_text)");
            View findViewById3 = findViewById(R.id.bubble_1_time);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.bubble_1_time)");
            Bubble bubble = new Bubble(findViewById, (TextView) findViewById2, (TextView) findViewById3, null);
            this.recv1 = bubble;
            View findViewById4 = findViewById(R.id.bubble_2);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.bubble_2)");
            View findViewById5 = findViewById(R.id.bubble_2_text);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.bubble_2_text)");
            View findViewById6 = findViewById(R.id.bubble_2_time);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.bubble_2_time)");
            Bubble bubble2 = new Bubble(findViewById4, (TextView) findViewById5, (TextView) findViewById6, (DeliveryStatusView) findViewById(R.id.bubble_2_delivery));
            this.sent1 = bubble2;
            View findViewById7 = findViewById(R.id.bubble_3);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "findViewById(R.id.bubble_3)");
            View findViewById8 = findViewById(R.id.bubble_3_text);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "findViewById(R.id.bubble_3_text)");
            View findViewById9 = findViewById(R.id.bubble_3_time);
            Intrinsics.checkNotNullExpressionValue(findViewById9, "findViewById(R.id.bubble_3_time)");
            Bubble bubble3 = new Bubble(findViewById7, (TextView) findViewById8, (TextView) findViewById9, null);
            this.recv2 = bubble3;
            View findViewById10 = findViewById(R.id.bubble_4);
            Intrinsics.checkNotNullExpressionValue(findViewById10, "findViewById(R.id.bubble_4)");
            View findViewById11 = findViewById(R.id.bubble_4_text);
            Intrinsics.checkNotNullExpressionValue(findViewById11, "findViewById(R.id.bubble_4_text)");
            View findViewById12 = findViewById(R.id.bubble_4_time);
            Intrinsics.checkNotNullExpressionValue(findViewById12, "findViewById(R.id.bubble_4_time)");
            Bubble bubble4 = new Bubble(findViewById10, (TextView) findViewById11, (TextView) findViewById12, (DeliveryStatusView) findViewById(R.id.bubble_4_delivery));
            this.sent2 = bubble4;
            String extendedRelativeTimeSpanString = DateUtils.getExtendedRelativeTimeSpanString(context, Locale.getDefault(), System.currentTimeMillis());
            Intrinsics.checkNotNullExpressionValue(extendedRelativeTimeSpanString, "getExtendedRelativeTimeS…stem.currentTimeMillis())");
            for (Bubble bubble5 : CollectionsKt__CollectionsKt.listOf((Object[]) new Bubble[]{bubble2, bubble4, bubble, bubble3})) {
                bubble5.getTime().setText(extendedRelativeTimeSpanString);
                DeliveryStatusView delivery = bubble5.getDelivery();
                if (delivery != null) {
                    delivery.setRead();
                }
            }
            if (this.bubbleCount == 2) {
                this.recv2.getBubble().setVisibility(8);
                this.sent2.getBubble().setVisibility(8);
            }
            View findViewById13 = findViewById(R.id.wallpaper);
            Intrinsics.checkNotNullExpressionValue(findViewById13, "findViewById(R.id.wallpaper)");
            this.wallpaper = (ImageView) findViewById13;
            View findViewById14 = findViewById(R.id.wallpaper_dim);
            Intrinsics.checkNotNullExpressionValue(findViewById14, "findViewById(R.id.wallpaper_dim)");
            this.wallpaperDim = findViewById14;
            View findViewById15 = findViewById(R.id.colorizer);
            Intrinsics.checkNotNullExpressionValue(findViewById15, "findViewById(R.id.colorizer)");
            this.colorizerView = (ColorizerView) findViewById15;
            this.colorizer = new Colorizer();
            obtainStyledAttributes.recycle();
        } catch (Throwable th3) {
            th = th3;
            typedArray = obtainStyledAttributes;
            if (typedArray != null) {
                typedArray.recycle();
            }
            throw th;
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        redrawChatColors();
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.ViewGroup, android.view.View
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        redrawChatColors();
    }

    private final void redrawChatColors() {
        ChatColors chatColors = this.chatColors;
        if (chatColors == null) {
            return;
        }
        if (chatColors != null) {
            setChatColors(chatColors);
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public final void setWallpaper(ChatWallpaper chatWallpaper) {
        if (chatWallpaper != null) {
            chatWallpaper.loadInto(this.wallpaper);
            if (ThemeUtil.isDarkTheme(getContext())) {
                this.wallpaperDim.setAlpha(chatWallpaper.getDimLevelForDarkTheme());
            } else {
                this.wallpaperDim.setAlpha(0.0f);
            }
        } else {
            this.wallpaper.setBackground(null);
            this.wallpaperDim.setAlpha(0.0f);
        }
        int i = chatWallpaper != null ? R.color.conversation_item_recv_bubble_color_wallpaper : R.color.signal_background_secondary;
        for (Bubble bubble : CollectionsKt__CollectionsKt.listOf((Object[]) new Bubble[]{this.recv1, this.recv2})) {
            bubble.getBubble().getBackground().setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), i), PorterDuff.Mode.SRC_IN));
        }
    }

    public final void setChatColors(ChatColors chatColors) {
        List<Bubble> list;
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.chatColors = chatColors;
        List<Bubble> list2 = CollectionsKt__CollectionsKt.listOf((Object[]) new Bubble[]{this.sent1, this.sent2});
        for (Bubble bubble : list2) {
            bubble.getBubble().getBackground().setColorFilter(chatColors.getChatBubbleColorFilter());
        }
        Drawable chatBubbleMask = chatColors.getChatBubbleMask();
        if (this.bubbleCount == 4) {
            list = CollectionsKt__CollectionsKt.listOf((Object[]) new Bubble[]{this.sent1, this.sent2});
        } else {
            list = CollectionsKt__CollectionsJVMKt.listOf(this.sent1);
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Bubble bubble2 : list) {
            arrayList.add(Projection.relativeToViewWithCommonRoot(bubble2.getBubble(), this.colorizerView, new Projection.Corners((float) ViewUtil.dpToPx(10))));
        }
        this.colorizerView.setProjections(arrayList);
        this.colorizerView.setVisibility(0);
        this.colorizerView.setBackground(chatBubbleMask);
        for (Bubble bubble3 : list2) {
            TextView body = bubble3.getBody();
            Colorizer colorizer = this.colorizer;
            Context context = getContext();
            Intrinsics.checkNotNullExpressionValue(context, "context");
            body.setTextColor(colorizer.getOutgoingBodyTextColor(context));
            TextView time = bubble3.getTime();
            Colorizer colorizer2 = this.colorizer;
            Context context2 = getContext();
            Intrinsics.checkNotNullExpressionValue(context2, "context");
            time.setTextColor(colorizer2.getOutgoingFooterTextColor(context2));
            DeliveryStatusView delivery = bubble3.getDelivery();
            if (delivery != null) {
                Colorizer colorizer3 = this.colorizer;
                Context context3 = getContext();
                Intrinsics.checkNotNullExpressionValue(context3, "context");
                delivery.setTint(colorizer3.getOutgoingFooterIconColor(context3));
            }
        }
    }

    /* compiled from: ChatColorPreviewView.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\b\u0002\u0018\u00002\u00020\u0001B'\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\u0002\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0007\u001a\u0004\u0018\u00010\b¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000b¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorPreviewView$Bubble;", "", "bubble", "Landroid/view/View;", "body", "Landroid/widget/TextView;", "time", "delivery", "Lorg/thoughtcrime/securesms/components/DeliveryStatusView;", "(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lorg/thoughtcrime/securesms/components/DeliveryStatusView;)V", "getBody", "()Landroid/widget/TextView;", "getBubble", "()Landroid/view/View;", "getDelivery", "()Lorg/thoughtcrime/securesms/components/DeliveryStatusView;", "getTime", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Bubble {
        private final TextView body;
        private final View bubble;
        private final DeliveryStatusView delivery;
        private final TextView time;

        public Bubble(View view, TextView textView, TextView textView2, DeliveryStatusView deliveryStatusView) {
            Intrinsics.checkNotNullParameter(view, "bubble");
            Intrinsics.checkNotNullParameter(textView, "body");
            Intrinsics.checkNotNullParameter(textView2, "time");
            this.bubble = view;
            this.body = textView;
            this.time = textView2;
            this.delivery = deliveryStatusView;
        }

        public final TextView getBody() {
            return this.body;
        }

        public final View getBubble() {
            return this.bubble;
        }

        public final DeliveryStatusView getDelivery() {
            return this.delivery;
        }

        public final TextView getTime() {
            return this.time;
        }
    }
}
