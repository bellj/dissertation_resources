package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import kotlin.Metadata;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* compiled from: CustomChatColorEdge.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;", "", "(Ljava/lang/String;I)V", "TOP", BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum CustomChatColorEdge {
    TOP,
    BOTTOM
}
