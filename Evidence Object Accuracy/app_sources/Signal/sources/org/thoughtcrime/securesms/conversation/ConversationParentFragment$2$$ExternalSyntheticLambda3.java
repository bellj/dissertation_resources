package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.net.Uri;
import com.annimon.stream.function.Consumer;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$2$$ExternalSyntheticLambda3 implements Consumer {
    public final /* synthetic */ Context f$0;

    public /* synthetic */ ConversationParentFragment$2$$ExternalSyntheticLambda3(Context context) {
        this.f$0 = context;
    }

    @Override // com.annimon.stream.function.Consumer
    public final void accept(Object obj) {
        ConversationParentFragment.AnonymousClass2.lambda$onSuccess$0(this.f$0, (Uri) obj);
    }
}
