package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardRepository;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.sharing.MultiShareSender;
import org.thoughtcrime.securesms.stories.Stories;
import org.whispersystems.signalservice.api.util.Preconditions;

/* compiled from: MultiselectForwardRepository.kt */
@Metadata(bv = {}, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u001cB\u0007¢\u0006\u0004\b\u001a\u0010\u001bJ\u001e\u0010\b\u001a\u00020\u00072\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\u0006\u0010\u0006\u001a\u00020\u0005H\u0002J\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\f0\u000b2\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u0002J\u001a\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00110\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eJ2\u0010\u0019\u001a\u00020\u00072\u0006\u0010\u0014\u001a\u00020\u00132\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\t0\u00022\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00170\u00162\u0006\u0010\u0006\u001a\u00020\u0005¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository;", "", "", "Lorg/thoughtcrime/securesms/sharing/MultiShareSender$MultiShareSendResultCollection;", "results", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository$MultiselectForwardResultHandlers;", "resultHandlers", "", "handleResults", "Lorg/thoughtcrime/securesms/sharing/MultiShareArgs;", "records", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "checkAllSelectedMediaCanBeSentToStories", "j$/util/Optional", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "recipientId", "", "canSelectRecipient", "", "additionalMessage", "multiShareArgs", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "shareContacts", "send", "<init>", "()V", "MultiselectForwardResultHandlers", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class MultiselectForwardRepository {

    /* compiled from: MultiselectForwardRepository.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B/\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0007R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\t¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardRepository$MultiselectForwardResultHandlers;", "", "onAllMessageSentSuccessfully", "Lkotlin/Function0;", "", "onSomeMessagesFailed", "onAllMessagesFailed", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getOnAllMessageSentSuccessfully", "()Lkotlin/jvm/functions/Function0;", "getOnAllMessagesFailed", "getOnSomeMessagesFailed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MultiselectForwardResultHandlers {
        private final Function0<Unit> onAllMessageSentSuccessfully;
        private final Function0<Unit> onAllMessagesFailed;
        private final Function0<Unit> onSomeMessagesFailed;

        public MultiselectForwardResultHandlers(Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03) {
            Intrinsics.checkNotNullParameter(function0, "onAllMessageSentSuccessfully");
            Intrinsics.checkNotNullParameter(function02, "onSomeMessagesFailed");
            Intrinsics.checkNotNullParameter(function03, "onAllMessagesFailed");
            this.onAllMessageSentSuccessfully = function0;
            this.onSomeMessagesFailed = function02;
            this.onAllMessagesFailed = function03;
        }

        public final Function0<Unit> getOnAllMessageSentSuccessfully() {
            return this.onAllMessageSentSuccessfully;
        }

        public final Function0<Unit> getOnSomeMessagesFailed() {
            return this.onSomeMessagesFailed;
        }

        public final Function0<Unit> getOnAllMessagesFailed() {
            return this.onAllMessagesFailed;
        }
    }

    public final Single<Stories.MediaTransform.SendRequirements> checkAllSelectedMediaCanBeSentToStories(List<MultiShareArgs> list) {
        Intrinsics.checkNotNullParameter(list, "records");
        Preconditions.checkArgument(!list.isEmpty());
        if (!Stories.isFeatureEnabled()) {
            Single<Stories.MediaTransform.SendRequirements> just = Single.just(Stories.MediaTransform.SendRequirements.CAN_NOT_SEND);
            Intrinsics.checkNotNullExpressionValue(just, "just(Stories.MediaTransf…equirements.CAN_NOT_SEND)");
            return just;
        }
        Single<Stories.MediaTransform.SendRequirements> subscribeOn = Single.fromCallable(new Callable(list) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return MultiselectForwardRepository.m1585checkAllSelectedMediaCanBeSentToStories$lambda2(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      if …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    public final Single<Boolean> canSelectRecipient(Optional<RecipientId> optional) {
        Intrinsics.checkNotNullParameter(optional, "recipientId");
        if (!optional.isPresent()) {
            Single<Boolean> just = Single.just(Boolean.TRUE);
            Intrinsics.checkNotNullExpressionValue(just, "just(true)");
            return just;
        }
        Single<Boolean> fromCallable = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardRepository$$ExternalSyntheticLambda2
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return MultiselectForwardRepository.m1584canSelectRecipient$lambda3(Optional.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n      val…       true\n      }\n    }");
        return fromCallable;
    }

    /* renamed from: canSelectRecipient$lambda-3 */
    public static final Boolean m1584canSelectRecipient$lambda3(Optional optional) {
        Intrinsics.checkNotNullParameter(optional, "$recipientId");
        Recipient resolved = Recipient.resolved((RecipientId) optional.get());
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId.get())");
        boolean z = true;
        if (resolved.isPushV2Group()) {
            Optional<GroupDatabase.GroupRecord> group = SignalDatabase.Companion.groups().getGroup(resolved.requireGroupId());
            if (group.isPresent() && group.get().isAnnouncementGroup() && !group.get().isAdmin(Recipient.self())) {
                z = false;
            }
        }
        return Boolean.valueOf(z);
    }

    public final void send(String str, List<MultiShareArgs> list, Set<? extends ContactSearchKey> set, MultiselectForwardResultHandlers multiselectForwardResultHandlers) {
        Intrinsics.checkNotNullParameter(str, "additionalMessage");
        Intrinsics.checkNotNullParameter(list, "multiShareArgs");
        Intrinsics.checkNotNullParameter(set, "shareContacts");
        Intrinsics.checkNotNullParameter(multiselectForwardResultHandlers, "resultHandlers");
        SignalExecutors.BOUNDED.execute(new Runnable(set, list, str, this, multiselectForwardResultHandlers) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Set f$0;
            public final /* synthetic */ List f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ MultiselectForwardRepository f$3;
            public final /* synthetic */ MultiselectForwardRepository.MultiselectForwardResultHandlers f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MultiselectForwardRepository.m1586send$lambda8(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* renamed from: send$lambda-8 */
    public static final void m1586send$lambda8(Set set, List list, String str, MultiselectForwardRepository multiselectForwardRepository, MultiselectForwardResultHandlers multiselectForwardResultHandlers) {
        Intrinsics.checkNotNullParameter(set, "$shareContacts");
        Intrinsics.checkNotNullParameter(list, "$multiShareArgs");
        Intrinsics.checkNotNullParameter(str, "$additionalMessage");
        Intrinsics.checkNotNullParameter(multiselectForwardRepository, "this$0");
        Intrinsics.checkNotNullParameter(multiselectForwardResultHandlers, "$resultHandlers");
        Set<ContactSearchKey> set2 = SequencesKt___SequencesKt.toSet(SequencesKt___SequencesKt.filter(CollectionsKt___CollectionsKt.asSequence(set), MultiselectForwardRepository$send$1$filteredContacts$1.INSTANCE));
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(((MultiShareArgs) it.next()).buildUpon(set2).build());
        }
        List<MultiShareArgs> list2 = CollectionsKt___CollectionsKt.sortedWith(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardRepository$send$lambda-8$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return ComparisonsKt__ComparisonsKt.compareValues(Long.valueOf(((MultiShareArgs) t).getTimestamp()), Long.valueOf(((MultiShareArgs) t2).getTimestamp()));
            }
        });
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10));
        for (MultiShareArgs multiShareArgs : list2) {
            arrayList2.add(MultiShareSender.sendSync(multiShareArgs));
        }
        if (str.length() > 0) {
            ArrayList arrayList3 = new ArrayList();
            for (Object obj : set2) {
                if (!(((ContactSearchKey) obj) instanceof ContactSearchKey.RecipientSearchKey.Story)) {
                    arrayList3.add(obj);
                }
            }
            MultiShareArgs build = new MultiShareArgs.Builder(CollectionsKt___CollectionsKt.toSet(arrayList3)).withDraftText(str).build();
            Intrinsics.checkNotNullExpressionValue(build, "Builder(filteredContacts…ssage)\n          .build()");
            Set<ContactSearchKey> contactSearchKeys = build.getContactSearchKeys();
            Intrinsics.checkNotNullExpressionValue(contactSearchKeys, "additional.contactSearchKeys");
            if (!contactSearchKeys.isEmpty()) {
                MultiShareSender.MultiShareSendResultCollection sendSync = MultiShareSender.sendSync(build);
                Intrinsics.checkNotNullExpressionValue(sendSync, "sendSync(additional)");
                multiselectForwardRepository.handleResults(CollectionsKt___CollectionsKt.plus((Collection<? extends MultiShareSender.MultiShareSendResultCollection>) ((Collection<? extends Object>) arrayList2), sendSync), multiselectForwardResultHandlers);
                return;
            }
            multiselectForwardRepository.handleResults(arrayList2, multiselectForwardResultHandlers);
            return;
        }
        multiselectForwardRepository.handleResults(arrayList2, multiselectForwardResultHandlers);
    }

    /* renamed from: checkAllSelectedMediaCanBeSentToStories$lambda-2 */
    public static final Stories.MediaTransform.SendRequirements m1585checkAllSelectedMediaCanBeSentToStories$lambda2(List list) {
        Intrinsics.checkNotNullParameter(list, "$records");
        boolean z = true;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                if (!((MultiShareArgs) it.next()).isValidForStories()) {
                    break;
                }
            }
        }
        z = false;
        if (z) {
            return Stories.MediaTransform.SendRequirements.CAN_NOT_SEND;
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            arrayList.add(((MultiShareArgs) it2.next()).getMedia());
        }
        return Stories.MediaTransform.getSendRequirements(CollectionsKt__IterablesKt.flatten(arrayList));
    }

    private final void handleResults(List<MultiShareSender.MultiShareSendResultCollection> list, MultiselectForwardResultHandlers multiselectForwardResultHandlers) {
        boolean z;
        boolean z2 = list instanceof Collection;
        boolean z3 = true;
        if (!z2 || !list.isEmpty()) {
            for (MultiShareSender.MultiShareSendResultCollection multiShareSendResultCollection : list) {
                if (multiShareSendResultCollection.containsFailures()) {
                    z = true;
                    break;
                }
            }
        }
        z = false;
        if (z) {
            if (!z2 || !list.isEmpty()) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!((MultiShareSender.MultiShareSendResultCollection) it.next()).containsOnlyFailures()) {
                            z3 = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            if (z3) {
                multiselectForwardResultHandlers.getOnAllMessagesFailed().invoke();
            } else {
                multiselectForwardResultHandlers.getOnSomeMessagesFailed().invoke();
            }
        } else {
            multiselectForwardResultHandlers.getOnAllMessageSentSuccessfully().invoke();
        }
    }
}
