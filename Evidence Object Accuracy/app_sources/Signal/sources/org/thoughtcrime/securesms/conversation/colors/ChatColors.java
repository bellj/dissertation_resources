package org.thoughtcrime.securesms.conversation.colors;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.common.base.Objects;
import java.util.Arrays;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.RotatableGradientDrawable;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;
import org.thoughtcrime.securesms.util.CustomDrawWrapperKt;

/* compiled from: ChatColors.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u0000 +2\u00020\u0001:\u0003+,-B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\u0016\u001a\u00020\u0010J\b\u0010\u0017\u001a\u00020\u0007H\u0007J\t\u0010\u0018\u001a\u00020\u0007HÖ\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0002J\u0006\u0010\u001d\u001a\u00020\u001eJ\u0006\u0010\u001f\u001a\u00020 J\b\u0010!\u001a\u00020\u0007H\u0016J\u0006\u0010\"\u001a\u00020\u001aJ\u0006\u0010#\u001a\u00020$J\u000e\u0010%\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003J\u0019\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\u0007HÖ\u0001R\u0017\u0010\t\u001a\u00020\n¢\u0006\u000e\n\u0000\u0012\u0004\b\u000b\u0010\f\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\u00108F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0004\n\u0002\u0010\u0015¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "Landroid/os/Parcelable;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "linearGradient", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$LinearGradient;", "singleColor", "", "(Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$LinearGradient;Ljava/lang/Integer;)V", "chatBubbleColorFilter", "Landroid/graphics/ColorFilter;", "getChatBubbleColorFilter$annotations", "()V", "getChatBubbleColorFilter", "()Landroid/graphics/ColorFilter;", "chatBubbleMask", "Landroid/graphics/drawable/Drawable;", "getChatBubbleMask", "()Landroid/graphics/drawable/Drawable;", "getId", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "Ljava/lang/Integer;", "asCircle", "asSingleColor", "describeContents", "equals", "", "other", "", "getColors", "", "getDegrees", "", "hashCode", "isGradient", "serialize", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/ChatColor;", "withId", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Companion", "Id", "LinearGradient", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColors implements Parcelable {
    public static final Parcelable.Creator<ChatColors> CREATOR = new Creator();
    public static final Companion Companion = new Companion(null);
    private final ColorFilter chatBubbleColorFilter;
    private final Id id;
    private final LinearGradient linearGradient;
    private final Integer singleColor;

    /* compiled from: ChatColors.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Creator implements Parcelable.Creator<ChatColors> {
        @Override // android.os.Parcelable.Creator
        public final ChatColors createFromParcel(Parcel parcel) {
            Intrinsics.checkNotNullParameter(parcel, "parcel");
            Id id = (Id) parcel.readParcelable(ChatColors.class.getClassLoader());
            Integer num = null;
            LinearGradient createFromParcel = parcel.readInt() == 0 ? null : LinearGradient.CREATOR.createFromParcel(parcel);
            if (parcel.readInt() != 0) {
                num = Integer.valueOf(parcel.readInt());
            }
            return new ChatColors(id, createFromParcel, num);
        }

        @Override // android.os.Parcelable.Creator
        public final ChatColors[] newArray(int i) {
            return new ChatColors[i];
        }
    }

    @JvmStatic
    public static final ChatColors forChatColor(Id id, ChatColor chatColor) {
        return Companion.forChatColor(id, chatColor);
    }

    @JvmStatic
    public static final ChatColors forColor(Id id, int i) {
        return Companion.forColor(id, i);
    }

    @JvmStatic
    public static final ChatColors forGradient(Id id, LinearGradient linearGradient) {
        return Companion.forGradient(id, linearGradient);
    }

    public static /* synthetic */ void getChatBubbleColorFilter$annotations() {
    }

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        Intrinsics.checkNotNullParameter(parcel, "out");
        parcel.writeParcelable(this.id, i);
        LinearGradient linearGradient = this.linearGradient;
        if (linearGradient == null) {
            parcel.writeInt(0);
        } else {
            parcel.writeInt(1);
            linearGradient.writeToParcel(parcel, i);
        }
        Integer num = this.singleColor;
        if (num == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcel.writeInt(num.intValue());
    }

    public ChatColors(Id id, LinearGradient linearGradient, Integer num) {
        PorterDuffColorFilter porterDuffColorFilter;
        Intrinsics.checkNotNullParameter(id, ContactRepository.ID_COLUMN);
        this.id = id;
        this.linearGradient = linearGradient;
        this.singleColor = num;
        if (Build.VERSION.SDK_INT >= 21) {
            porterDuffColorFilter = new PorterDuffColorFilter(0, PorterDuff.Mode.SRC_IN);
        } else {
            porterDuffColorFilter = new PorterDuffColorFilter(asSingleColor(), PorterDuff.Mode.SRC_IN);
        }
        this.chatBubbleColorFilter = porterDuffColorFilter;
    }

    public final Id getId() {
        return this.id;
    }

    public final boolean isGradient() {
        return Build.VERSION.SDK_INT >= 21 && this.linearGradient != null;
    }

    public final Drawable getChatBubbleMask() {
        if (Build.VERSION.SDK_INT < 21) {
            return new ColorDrawable(0);
        }
        if (this.linearGradient != null) {
            return new RotatableGradientDrawable(this.linearGradient.getDegrees(), this.linearGradient.getColors(), this.linearGradient.getPositions());
        }
        return new ColorDrawable(asSingleColor());
    }

    public final ColorFilter getChatBubbleColorFilter() {
        return this.chatBubbleColorFilter;
    }

    public final int asSingleColor() {
        Integer num = this.singleColor;
        if (num != null) {
            return num.intValue();
        }
        LinearGradient linearGradient = this.linearGradient;
        if (linearGradient != null) {
            return ArraysKt___ArraysKt.last(linearGradient.getColors());
        }
        throw new AssertionError();
    }

    public final ChatColor serialize() {
        ChatColor.Builder newBuilder = ChatColor.newBuilder();
        Intrinsics.checkNotNullExpressionValue(newBuilder, "newBuilder()");
        if (this.linearGradient != null) {
            ChatColor.LinearGradient.Builder newBuilder2 = ChatColor.LinearGradient.newBuilder();
            newBuilder2.setRotation(this.linearGradient.getDegrees());
            for (int i : this.linearGradient.getColors()) {
                newBuilder2.addColors(i);
            }
            for (float f : this.linearGradient.getPositions()) {
                newBuilder2.addPositions(f);
            }
            newBuilder.setLinearGradient(newBuilder2);
        }
        if (this.singleColor != null) {
            newBuilder.setSingleColor(ChatColor.SingleColor.newBuilder().setColor(this.singleColor.intValue()));
        }
        ChatColor build = newBuilder.build();
        Intrinsics.checkNotNullExpressionValue(build, "builder.build()");
        return build;
    }

    public final int[] getColors() {
        int[] colors;
        LinearGradient linearGradient = this.linearGradient;
        if (linearGradient != null && (colors = linearGradient.getColors()) != null) {
            return colors;
        }
        Integer num = this.singleColor;
        if (num != null) {
            return new int[]{num.intValue()};
        }
        throw new AssertionError();
    }

    public final float getDegrees() {
        LinearGradient linearGradient = this.linearGradient;
        if (linearGradient != null) {
            return linearGradient.getDegrees();
        }
        return 180.0f;
    }

    public final Drawable asCircle() {
        if (Build.VERSION.SDK_INT >= 21) {
            return CustomDrawWrapperKt.customizeOnDraw(getChatBubbleMask(), new Function2<Drawable, Canvas, Unit>(new Path()) { // from class: org.thoughtcrime.securesms.conversation.colors.ChatColors$asCircle$2
                final /* synthetic */ Path $path;

                /* access modifiers changed from: package-private */
                {
                    this.$path = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                @Override // kotlin.jvm.functions.Function2
                public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                    invoke((Drawable) obj, (Canvas) obj2);
                    return Unit.INSTANCE;
                }

                public final void invoke(Drawable drawable, Canvas canvas) {
                    Intrinsics.checkNotNullParameter(drawable, "wrapped");
                    Intrinsics.checkNotNullParameter(canvas, "canvas");
                    canvas.save();
                    this.$path.rewind();
                    this.$path.addCircle(drawable.getBounds().exactCenterX(), drawable.getBounds().exactCenterY(), Math.min(drawable.getBounds().exactCenterX(), drawable.getBounds().exactCenterY()), Path.Direction.CW);
                    canvas.clipPath(this.$path);
                    drawable.draw(canvas);
                    canvas.restore();
                }
            });
        }
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        shapeDrawable.getPaint().setColor(asSingleColor());
        return shapeDrawable;
    }

    public final ChatColors withId(Id id) {
        Intrinsics.checkNotNullParameter(id, ContactRepository.ID_COLUMN);
        return new ChatColors(id, this.linearGradient, this.singleColor);
    }

    @Override // java.lang.Object
    public boolean equals(Object obj) {
        ChatColors chatColors = obj instanceof ChatColors ? (ChatColors) obj : null;
        if (chatColors != null && Intrinsics.areEqual(this.id, chatColors.id) && Intrinsics.areEqual(this.linearGradient, chatColors.linearGradient) && Intrinsics.areEqual(this.singleColor, chatColors.singleColor)) {
            return true;
        }
        return false;
    }

    @Override // java.lang.Object
    public int hashCode() {
        return Objects.hashCode(this.linearGradient, this.singleColor, this.id);
    }

    /* compiled from: ChatColors.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u001a\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\n\u001a\u00020\u000bH\u0007J\u0018\u0010\f\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0007¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Companion;", "", "()V", "forChatColor", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "chatColor", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/ChatColor;", "forColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "", "forGradient", "linearGradient", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$LinearGradient;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final ChatColors forChatColor(Id id, ChatColor chatColor) {
            Intrinsics.checkNotNullParameter(id, ContactRepository.ID_COLUMN);
            Intrinsics.checkNotNullParameter(chatColor, "chatColor");
            chatColor.hasSingleColor();
            chatColor.hasLinearGradient();
            if (!chatColor.hasLinearGradient()) {
                return forColor(id, chatColor.getSingleColor().getColor());
            }
            float rotation = chatColor.getLinearGradient().getRotation();
            List<Integer> colorsList = chatColor.getLinearGradient().getColorsList();
            Intrinsics.checkNotNullExpressionValue(colorsList, "chatColor.linearGradient.colorsList");
            int[] iArr = CollectionsKt___CollectionsKt.toIntArray(colorsList);
            List<Float> positionsList = chatColor.getLinearGradient().getPositionsList();
            Intrinsics.checkNotNullExpressionValue(positionsList, "chatColor.linearGradient.positionsList");
            return forGradient(id, new LinearGradient(rotation, iArr, CollectionsKt___CollectionsKt.toFloatArray(positionsList)));
        }

        @JvmStatic
        public final ChatColors forGradient(Id id, LinearGradient linearGradient) {
            Intrinsics.checkNotNullParameter(id, ContactRepository.ID_COLUMN);
            Intrinsics.checkNotNullParameter(linearGradient, "linearGradient");
            return new ChatColors(id, linearGradient, null);
        }

        @JvmStatic
        public final ChatColors forColor(Id id, int i) {
            Intrinsics.checkNotNullParameter(id, ContactRepository.ID_COLUMN);
            return new ChatColors(id, null, Integer.valueOf(i));
        }
    }

    /* compiled from: ChatColors.kt */
    @Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00152\u00020\u0001:\u0005\u0013\u0014\u0015\u0016\u0017B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0002J\b\u0010\r\u001a\u00020\bH\u0016J\u0018\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\bH\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0004\u0018\u0019\u001a\u001b¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "Landroid/os/Parcelable;", "longValue", "", "(J)V", "getLongValue", "()J", "describeContents", "", "equals", "", "other", "", "hashCode", "writeToParcel", "", "dest", "Landroid/os/Parcel;", "flags", "Auto", "BuiltIn", "Companion", "Custom", "NotSet", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$Auto;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$BuiltIn;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$NotSet;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$Custom;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Id implements Parcelable {
        public static final Parcelable.Creator<Id> CREATOR = new ChatColors$Id$Companion$CREATOR$1();
        public static final Companion Companion = new Companion(null);
        private final long longValue;

        public /* synthetic */ Id(long j, DefaultConstructorMarker defaultConstructorMarker) {
            this(j);
        }

        @JvmStatic
        public static final Id forLongValue(long j) {
            return Companion.forLongValue(j);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        private Id(long j) {
            this.longValue = j;
        }

        public final long getLongValue() {
            return this.longValue;
        }

        /* compiled from: ChatColors.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$Auto;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Auto extends Id {
            public static final Auto INSTANCE = new Auto();

            private Auto() {
                super(-2, null);
            }
        }

        /* compiled from: ChatColors.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$BuiltIn;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class BuiltIn extends Id {
            public static final BuiltIn INSTANCE = new BuiltIn();

            private BuiltIn() {
                super(-1, null);
            }
        }

        /* compiled from: ChatColors.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$NotSet;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class NotSet extends Id {
            public static final NotSet INSTANCE = new NotSet();

            private NotSet() {
                super(0, null);
            }
        }

        /* compiled from: ChatColors.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$Custom;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", ContactRepository.ID_COLUMN, "", "(J)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Custom extends Id {
            public Custom(long j) {
                super(j, null);
            }
        }

        @Override // java.lang.Object
        public boolean equals(Object obj) {
            Id id = obj instanceof Id ? (Id) obj : null;
            return id != null && this.longValue == id.longValue;
        }

        @Override // java.lang.Object
        public int hashCode() {
            return Objects.hashCode(Long.valueOf(this.longValue));
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "dest");
            parcel.writeLong(this.longValue);
        }

        /* compiled from: ChatColors.kt */
        @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u0016\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id$Companion;", "", "()V", "CREATOR", "Landroid/os/Parcelable$Creator;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "forLongValue", "longValue", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            @JvmStatic
            public final Id forLongValue(long j) {
                if (j == -2) {
                    return Auto.INSTANCE;
                }
                if (j == -1) {
                    return BuiltIn.INSTANCE;
                }
                if (j == 0) {
                    return NotSet.INSTANCE;
                }
                return new Custom(j);
            }
        }
    }

    /* compiled from: ChatColors.kt */
    @Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0014\n\u0002\b\f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u0014H\u0016J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001J\u0019\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$LinearGradient;", "Landroid/os/Parcelable;", "degrees", "", "colors", "", "positions", "", "(F[I[F)V", "getColors", "()[I", "getDegrees", "()F", "getPositions", "()[F", "component1", "component2", "component3", "copy", "describeContents", "", "equals", "", "other", "", "hashCode", "toString", "", "writeToParcel", "", "parcel", "Landroid/os/Parcel;", "flags", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LinearGradient implements Parcelable {
        public static final Parcelable.Creator<LinearGradient> CREATOR = new Creator();
        private final int[] colors;
        private final float degrees;
        private final float[] positions;

        /* compiled from: ChatColors.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Creator implements Parcelable.Creator<LinearGradient> {
            @Override // android.os.Parcelable.Creator
            public final LinearGradient createFromParcel(Parcel parcel) {
                Intrinsics.checkNotNullParameter(parcel, "parcel");
                return new LinearGradient(parcel.readFloat(), parcel.createIntArray(), parcel.createFloatArray());
            }

            @Override // android.os.Parcelable.Creator
            public final LinearGradient[] newArray(int i) {
                return new LinearGradient[i];
            }
        }

        public static /* synthetic */ LinearGradient copy$default(LinearGradient linearGradient, float f, int[] iArr, float[] fArr, int i, Object obj) {
            if ((i & 1) != 0) {
                f = linearGradient.degrees;
            }
            if ((i & 2) != 0) {
                iArr = linearGradient.colors;
            }
            if ((i & 4) != 0) {
                fArr = linearGradient.positions;
            }
            return linearGradient.copy(f, iArr, fArr);
        }

        public final float component1() {
            return this.degrees;
        }

        public final int[] component2() {
            return this.colors;
        }

        public final float[] component3() {
            return this.positions;
        }

        public final LinearGradient copy(float f, int[] iArr, float[] fArr) {
            Intrinsics.checkNotNullParameter(iArr, "colors");
            Intrinsics.checkNotNullParameter(fArr, "positions");
            return new LinearGradient(f, iArr, fArr);
        }

        @Override // android.os.Parcelable
        public int describeContents() {
            return 0;
        }

        @Override // java.lang.Object
        public String toString() {
            return "LinearGradient(degrees=" + this.degrees + ", colors=" + Arrays.toString(this.colors) + ", positions=" + Arrays.toString(this.positions) + ')';
        }

        @Override // android.os.Parcelable
        public void writeToParcel(Parcel parcel, int i) {
            Intrinsics.checkNotNullParameter(parcel, "out");
            parcel.writeFloat(this.degrees);
            parcel.writeIntArray(this.colors);
            parcel.writeFloatArray(this.positions);
        }

        public LinearGradient(float f, int[] iArr, float[] fArr) {
            Intrinsics.checkNotNullParameter(iArr, "colors");
            Intrinsics.checkNotNullParameter(fArr, "positions");
            this.degrees = f;
            this.colors = iArr;
            this.positions = fArr;
        }

        public final float getDegrees() {
            return this.degrees;
        }

        public final int[] getColors() {
            return this.colors;
        }

        public final float[] getPositions() {
            return this.positions;
        }

        @Override // java.lang.Object
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!Intrinsics.areEqual(LinearGradient.class, obj != null ? obj.getClass() : null)) {
                return false;
            }
            if (obj != null) {
                LinearGradient linearGradient = (LinearGradient) obj;
                return Arrays.equals(this.colors, linearGradient.colors) && Arrays.equals(this.positions, linearGradient.positions);
            }
            throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.colors.ChatColors.LinearGradient");
        }

        @Override // java.lang.Object
        public int hashCode() {
            return (Arrays.hashCode(this.colors) * 31) + Arrays.hashCode(this.positions);
        }
    }
}
