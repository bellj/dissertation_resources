package org.thoughtcrime.securesms.conversation.quotes;

import android.app.Application;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.recyclerview.SmoothScrollingLinearLayoutManager;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.conversation.ConversationAdapter;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.colors.RecyclerViewColorizer;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.conversation.quotes.MessageQuotesViewModel;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.InMemoryMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ItemDecoration;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicy;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionPlayerHolder;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionRecycler;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;

/* compiled from: MessageQuotesBottomSheet.kt */
@Metadata(d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 ,2\u00020\u0001:\u0003+,-B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\u0018\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J$\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010\u001e2\b\u0010&\u001a\u0004\u0018\u00010'H\u0016J\u001a\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020\"2\b\u0010&\u001a\u0004\u0018\u00010'H\u0017R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nXD¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\u00020\u000eXD¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0011\u001a\u00020\u00128BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0016\u001a\u0004\b\u0013\u0010\u0014¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesBottomSheet;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "()V", "disposables", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "firstRender", "", "messageAdapter", "Lorg/thoughtcrime/securesms/conversation/ConversationAdapter;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "themeResId", "", "getThemeResId", "()I", "viewModel", "Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getAdapterListener", "Lorg/thoughtcrime/securesms/conversation/ConversationAdapter$ItemClickListener;", "getCallback", "Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesBottomSheet$Callback;", "initializeGiphyMp4", "Lorg/thoughtcrime/securesms/giph/mp4/GiphyMp4ProjectionRecycler;", "videoContainer", "Landroid/view/ViewGroup;", "list", "Landroidx/recyclerview/widget/RecyclerView;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "", "view", "Callback", "Companion", "ConversationAdapterListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageQuotesBottomSheet extends FixedRoundedCornerBottomSheetDialogFragment {
    public static final Companion Companion = new Companion(null);
    private static final String KEY_CONVERSATION_RECIPIENT_ID;
    private static final String KEY_MESSAGE_ID;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private boolean firstRender = true;
    private ConversationAdapter messageAdapter;
    private final float peekHeightPercentage = 0.66f;
    private final int themeResId = R.style.Widget_Signal_FixedRoundedCorners_Messages;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MessageQuotesViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$viewModel$2
        final /* synthetic */ MessageQuotesBottomSheet this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            MessageId.Companion companion = MessageId.Companion;
            Bundle arguments = this.this$0.getArguments();
            String str = null;
            String string = arguments != null ? arguments.getString("message_id", null) : null;
            if (string != null) {
                MessageId deserialize = companion.deserialize(string);
                Bundle arguments2 = this.this$0.getArguments();
                if (arguments2 != null) {
                    str = arguments2.getString("conversation_recipient_id", null);
                }
                if (str != null) {
                    RecipientId from = RecipientId.from(str);
                    Application application = ApplicationDependencies.getApplication();
                    Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
                    Intrinsics.checkNotNullExpressionValue(from, "conversationRecipientId");
                    return new MessageQuotesViewModel.Factory(application, deserialize, from);
                }
                throw new IllegalArgumentException();
            }
            throw new IllegalArgumentException();
        }
    });

    /* compiled from: MessageQuotesBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesBottomSheet$Callback;", "", "getConversationAdapterListener", "Lorg/thoughtcrime/securesms/conversation/ConversationAdapter$ItemClickListener;", "jumpToMessage", "", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        ConversationAdapter.ItemClickListener getConversationAdapterListener();

        void jumpToMessage(MessageRecord messageRecord);
    }

    @JvmStatic
    public static final void show(FragmentManager fragmentManager, MessageId messageId, RecipientId recipientId) {
        Companion.show(fragmentManager, messageId, recipientId);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARN: Type inference failed for: r0v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback getCallback() {
        /*
            r2 = this;
            androidx.fragment.app.Fragment r0 = r2.getParentFragment()
        L_0x0004:
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback
            if (r1 == 0) goto L_0x000b
            goto L_0x001b
        L_0x000b:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0010:
            androidx.fragment.app.FragmentActivity r0 = r2.requireActivity()
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback
            if (r1 != 0) goto L_0x0019
            r0 = 0
        L_0x0019:
            org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$Callback r0 = (org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback) r0
        L_0x001b:
            org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$Callback r0 = (org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback) r0
            if (r0 == 0) goto L_0x0020
            return r0
        L_0x0020:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "Parent must implement callback interface!"
            r0.<init>(r1)
            goto L_0x0029
        L_0x0028:
            throw r0
        L_0x0029:
            goto L_0x0028
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.getCallback():org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$Callback");
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    public int getThemeResId() {
        return this.themeResId;
    }

    private final MessageQuotesViewModel getViewModel() {
        return (MessageQuotesViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        View inflate = layoutInflater.inflate(R.layout.message_quotes_bottom_sheet, viewGroup, false);
        LifecycleDisposable lifecycleDisposable = this.disposables;
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        Intrinsics.checkNotNullExpressionValue(viewLifecycleOwner, "viewLifecycleOwner");
        lifecycleDisposable.bindTo(viewLifecycleOwner);
        Intrinsics.checkNotNullExpressionValue(inflate, "view");
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        Bundle arguments = getArguments();
        String string = arguments != null ? arguments.getString(KEY_CONVERSATION_RECIPIENT_ID, null) : null;
        if (string != null) {
            Recipient resolved = Recipient.resolved(RecipientId.from(string));
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(conversationRecipientId)");
            Colorizer colorizer = new Colorizer();
            ConversationAdapter conversationAdapter = new ConversationAdapter(requireContext(), getViewLifecycleOwner(), GlideApp.with(this), Locale.getDefault(), new ConversationAdapterListener(), resolved, colorizer);
            conversationAdapter.setCondensedMode(true);
            this.messageAdapter = conversationAdapter;
            View findViewById = view.findViewById(R.id.quotes_list);
            RecyclerView recyclerView = (RecyclerView) findViewById;
            recyclerView.setLayoutManager(new SmoothScrollingLinearLayoutManager(requireContext(), true));
            ConversationAdapter conversationAdapter2 = this.messageAdapter;
            if (conversationAdapter2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("messageAdapter");
                conversationAdapter2 = null;
            }
            recyclerView.setAdapter(conversationAdapter2);
            recyclerView.setItemAnimator(null);
            Context context = recyclerView.getContext();
            Intrinsics.checkNotNullExpressionValue(context, "context");
            recyclerView.addItemDecoration(new MessageQuoteHeaderDecoration(context));
            Intrinsics.checkNotNullExpressionValue(recyclerView, "");
            recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener(recyclerView, this) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$onViewCreated$lambda-2$$inlined$doOnNextLayout$1
                final /* synthetic */ RecyclerView $this_apply$inlined;
                final /* synthetic */ MessageQuotesBottomSheet this$0;

                {
                    this.$this_apply$inlined = r1;
                    this.this$0 = r2;
                }

                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(View view2, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
                    Intrinsics.checkNotNullParameter(view2, "view");
                    view2.removeOnLayoutChangeListener(this);
                    RecyclerView recyclerView2 = this.$this_apply$inlined;
                    ConversationAdapter conversationAdapter3 = this.this$0.messageAdapter;
                    if (conversationAdapter3 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("messageAdapter");
                        conversationAdapter3 = null;
                    }
                    recyclerView2.addItemDecoration(new StickyHeaderDecoration(conversationAdapter3, false, false, 2));
                }
            });
            Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById<Recycl…LINE_DATE))\n      }\n    }");
            RecyclerViewColorizer recyclerViewColorizer = new RecyclerViewColorizer(recyclerView);
            LifecycleDisposable lifecycleDisposable = this.disposables;
            Disposable subscribe = getViewModel().getMessages().subscribe(new Consumer(recyclerViewColorizer, resolved, recyclerView) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$$ExternalSyntheticLambda0
                public final /* synthetic */ RecyclerViewColorizer f$1;
                public final /* synthetic */ Recipient f$2;
                public final /* synthetic */ RecyclerView f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    MessageQuotesBottomSheet.m1603onViewCreated$lambda4(MessageQuotesBottomSheet.this, this.f$1, this.f$2, this.f$3, (List) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.getMessages().…cipient.chatColors)\n    }");
            lifecycleDisposable.plusAssign(subscribe);
            LifecycleDisposable lifecycleDisposable2 = this.disposables;
            Disposable subscribe2 = getViewModel().getNameColorsMap().subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$$ExternalSyntheticLambda1
                public final /* synthetic */ MessageQuotesBottomSheet f$1;

                {
                    this.f$1 = r2;
                }

                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    MessageQuotesBottomSheet.m1605onViewCreated$lambda5(Colorizer.this, this.f$1, (Map) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe2, "viewModel.getNameColorsM…AYLOAD_NAME_COLORS)\n    }");
            lifecycleDisposable2.plusAssign(subscribe2);
            View findViewById2 = view.findViewById(R.id.video_container);
            if (findViewById2 != null) {
                initializeGiphyMp4((ViewGroup) findViewById2, recyclerView);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m1603onViewCreated$lambda4(MessageQuotesBottomSheet messageQuotesBottomSheet, RecyclerViewColorizer recyclerViewColorizer, Recipient recipient, RecyclerView recyclerView, List list) {
        Intrinsics.checkNotNullParameter(messageQuotesBottomSheet, "this$0");
        Intrinsics.checkNotNullParameter(recyclerViewColorizer, "$recyclerViewColorizer");
        Intrinsics.checkNotNullParameter(recipient, "$conversationRecipient");
        Intrinsics.checkNotNullParameter(recyclerView, "$list");
        if (list.isEmpty()) {
            messageQuotesBottomSheet.dismiss();
        }
        ConversationAdapter conversationAdapter = messageQuotesBottomSheet.messageAdapter;
        if (conversationAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("messageAdapter");
            conversationAdapter = null;
        }
        conversationAdapter.submitList(list, new Runnable(recyclerView, list) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet$$ExternalSyntheticLambda2
            public final /* synthetic */ RecyclerView f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MessageQuotesBottomSheet.m1604onViewCreated$lambda4$lambda3(MessageQuotesBottomSheet.this, this.f$1, this.f$2);
            }
        });
        ChatColors chatColors = recipient.getChatColors();
        Intrinsics.checkNotNullExpressionValue(chatColors, "conversationRecipient.chatColors");
        recyclerViewColorizer.setChatColors(chatColors);
    }

    /* renamed from: onViewCreated$lambda-4$lambda-3 */
    public static final void m1604onViewCreated$lambda4$lambda3(MessageQuotesBottomSheet messageQuotesBottomSheet, RecyclerView recyclerView, List list) {
        RecyclerView.LayoutManager layoutManager;
        Intrinsics.checkNotNullParameter(messageQuotesBottomSheet, "this$0");
        Intrinsics.checkNotNullParameter(recyclerView, "$list");
        if (messageQuotesBottomSheet.firstRender) {
            RecyclerView.LayoutManager layoutManager2 = recyclerView.getLayoutManager();
            if (layoutManager2 != null) {
                ((LinearLayoutManager) layoutManager2).scrollToPositionWithOffset(list.size() - 1, 100);
                messageQuotesBottomSheet.firstRender = false;
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
        } else if (!recyclerView.canScrollVertically(1) && (layoutManager = recyclerView.getLayoutManager()) != null) {
            layoutManager.scrollToPosition(0);
        }
    }

    /* renamed from: onViewCreated$lambda-5 */
    public static final void m1605onViewCreated$lambda5(Colorizer colorizer, MessageQuotesBottomSheet messageQuotesBottomSheet, Map map) {
        Intrinsics.checkNotNullParameter(colorizer, "$colorizer");
        Intrinsics.checkNotNullParameter(messageQuotesBottomSheet, "this$0");
        Intrinsics.checkNotNullExpressionValue(map, "map");
        colorizer.onNameColorsChanged(map);
        ConversationAdapter conversationAdapter = messageQuotesBottomSheet.messageAdapter;
        ConversationAdapter conversationAdapter2 = null;
        if (conversationAdapter == null) {
            Intrinsics.throwUninitializedPropertyAccessException("messageAdapter");
            conversationAdapter = null;
        }
        ConversationAdapter conversationAdapter3 = messageQuotesBottomSheet.messageAdapter;
        if (conversationAdapter3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("messageAdapter");
        } else {
            conversationAdapter2 = conversationAdapter3;
        }
        conversationAdapter.notifyItemRangeChanged(0, conversationAdapter2.getItemCount(), 1);
    }

    private final GiphyMp4ProjectionRecycler initializeGiphyMp4(ViewGroup viewGroup, RecyclerView recyclerView) {
        int maxSimultaneousPlaybackInConversation = GiphyMp4PlaybackPolicy.maxSimultaneousPlaybackInConversation();
        List<GiphyMp4ProjectionPlayerHolder> injectVideoViews = GiphyMp4ProjectionPlayerHolder.injectVideoViews(requireContext(), getViewLifecycleOwner().getLifecycle(), viewGroup, maxSimultaneousPlaybackInConversation);
        Intrinsics.checkNotNullExpressionValue(injectVideoViews, "injectVideoViews(\n      …,\n      maxPlayback\n    )");
        GiphyMp4ProjectionRecycler giphyMp4ProjectionRecycler = new GiphyMp4ProjectionRecycler(injectVideoViews);
        GiphyMp4PlaybackController.attach(recyclerView, giphyMp4ProjectionRecycler, maxSimultaneousPlaybackInConversation);
        recyclerView.addItemDecoration(new GiphyMp4ItemDecoration(giphyMp4ProjectionRecycler, MessageQuotesBottomSheet$initializeGiphyMp4$1.INSTANCE), 0);
        return giphyMp4ProjectionRecycler;
    }

    public final ConversationAdapter.ItemClickListener getAdapterListener() {
        return getCallback().getConversationAdapterListener();
    }

    /* compiled from: MessageQuotesBottomSheet.kt */
    @Metadata(d1 = {"\u0000Æ\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0000\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0013\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u0001J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\u0016J\u0013\u0010\n\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\fH\u0001J\u0010\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0013\u0010\u0010\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\fH\u0001J\b\u0010\u0011\u001a\u00020\u0004H\u0016J\b\u0010\u0012\u001a\u00020\u0004H\u0016J\b\u0010\u0013\u001a\u00020\u0004H\u0016J\u0013\u0010\u0014\u001a\u00020\u00042\b\b\u0001\u0010\u0015\u001a\u00020\u0016H\u0001J\u0018\u0010\u0017\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0010\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\u0013\u0010\u001e\u001a\u00020\u00042\b\b\u0001\u0010\u0015\u001a\u00020\u001fH\u0001J\u0013\u0010 \u001a\u00020\u00042\b\b\u0001\u0010\u0018\u001a\u00020\tH\u0001J\u0010\u0010!\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\"H\u0016J5\u0010#\u001a\u00020\u00042*\b\u0001\u0010$\u001a$\u0012\f\u0012\n &*\u0004\u0018\u00010\f0\f &*\u0010\u0012\f\u0012\n &*\u0004\u0018\u00010\f0\f0'0%H\u0001J\u0010\u0010(\u001a\u00020\u00042\u0006\u0010)\u001a\u00020*H\u0016J\u0018\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020-2\u0006\u0010)\u001a\u00020*H\u0016J\b\u0010.\u001a\u00020\u0004H\u0016J\u0010\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u000201H\u0016J5\u00102\u001a\u00020\u00042*\b\u0001\u0010$\u001a$\u0012\f\u0012\n &*\u0004\u0018\u00010\f0\f &*\u0010\u0012\f\u0012\n &*\u0004\u0018\u00010\f0\f0'0%H\u0001J\u0013\u00103\u001a\u00020\u00042\b\b\u0001\u0010\u0015\u001a\u00020\u0016H\u0001J\u0010\u00104\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J#\u00105\u001a\u00020\u00042\b\b\u0001\u00106\u001a\u00020\t2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0001J\u0019\u0010;\u001a\u00020\u00042\u000e\u0010<\u001a\n &*\u0004\u0018\u00010=0=H\u0001J\u0010\u0010>\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020?H\u0016J\u0010\u0010@\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J \u0010A\u001a\u00020\u00042\u0006\u0010B\u001a\u00020*2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:H\u0016J\u0010\u0010C\u001a\u00020\u00042\u0006\u0010D\u001a\u00020\tH\u0016J!\u0010E\u001a\u00020\u00042\u0016\b\u0001\u0010F\u001a\u0010\u0012\f\u0012\n &*\u0004\u0018\u00010H0H0GH\u0001J\u0010\u0010I\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u001d\u0010J\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010K\u001a\u00020-H\u0001J\u0013\u0010L\u001a\u00020\u00042\b\b\u0001\u0010M\u001a\u00020NH\u0001J!\u0010O\u001a\u00020\u00042\u0016\b\u0001\u0010F\u001a\u0010\u0012\f\u0012\n &*\u0004\u0018\u00010H0H0GH\u0001J\u0013\u0010P\u001a\u00020:2\b\b\u0001\u0010Q\u001a\u00020\u000fH\u0001J\u0010\u0010R\u001a\u00020\u00042\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J'\u0010S\u001a\u00020\u00042\n\b\u0001\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\b\b\u0001\u0010T\u001a\u00020\u000f2\u0006\u0010U\u001a\u00020:H\u0001J\u0013\u0010V\u001a\u00020\u00042\b\b\u0001\u0010\u0015\u001a\u00020?H\u0001J\u0013\u0010W\u001a\u00020\u00042\b\b\u0001\u0010X\u001a\u00020YH\u0001J#\u0010Z\u001a\u00020\u00042\b\b\u0001\u0010X\u001a\u00020Y2\u0006\u00107\u001a\u0002082\u0006\u0010[\u001a\u00020\\H\u0001J\u001b\u0010]\u001a\u00020\u00042\b\b\u0001\u0010X\u001a\u00020Y2\u0006\u0010^\u001a\u00020_H\u0001J\u001b\u0010`\u001a\u00020\u00042\b\b\u0001\u0010X\u001a\u00020Y2\u0006\u0010[\u001a\u00020\\H\u0001¨\u0006a"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesBottomSheet$ConversationAdapterListener;", "Lorg/thoughtcrime/securesms/conversation/ConversationAdapter$ItemClickListener;", "(Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesBottomSheet;)V", "onAddToContactsClicked", "", "contact", "Lorg/thoughtcrime/securesms/contactshare/Contact;", "onBadDecryptLearnMoreClicked", "author", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onBlockJoinRequest", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "onCallToAction", "action", "", "onChangeNumberUpdateContact", "onChatSessionRefreshLearnMoreClicked", "onDonateClicked", "onEnableCallNotificationsClicked", "onGiftBadgeRevealed", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "onGroupMemberClicked", "recipientId", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "onGroupMigrationLearnMoreClicked", "membershipChange", "Lorg/thoughtcrime/securesms/groups/GroupMigrationMembershipChange;", "onInMemoryMessageClicked", "Lorg/thoughtcrime/securesms/database/model/InMemoryMessageRecord;", "onIncomingIdentityMismatchClicked", "onInviteFriendsToGroupClicked", "Lorg/thoughtcrime/securesms/groups/GroupId$V2;", "onInviteSharedContactClicked", "choices", "", "kotlin.jvm.PlatformType", "", "onItemClick", "item", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "onItemLongClick", "itemView", "Landroid/view/View;", "onJoinGroupCallClicked", "onLinkPreviewClicked", "linkPreview", "Lorg/thoughtcrime/securesms/linkpreview/LinkPreview;", "onMessageSharedContactClicked", "onMessageWithErrorClicked", "onMessageWithRecaptchaNeededClicked", "onMoreTextClicked", "conversationRecipientId", "messageId", "", "isMms", "", "onPlayInlineContent", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "onQuoteClicked", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "onQuotedIndicatorClicked", "onReactionClicked", "multiselectPart", "onRecipientNameClicked", "target", "onRegisterVoiceNoteCallbacks", "onPlaybackStartObserver", "Landroidx/lifecycle/Observer;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState;", "onSafetyNumberLearnMoreClicked", "onSharedContactDetailsClicked", "avatarTransitionView", "onStickerClicked", "stickerLocator", "Lorg/thoughtcrime/securesms/stickers/StickerLocator;", "onUnregisterVoiceNoteCallbacks", "onUrlClicked", "url", "onViewGiftBadgeClicked", "onViewGroupDescriptionChange", "description", "isMessageRequestAccepted", "onViewOnceMessageClicked", "onVoiceNotePause", "uri", "Landroid/net/Uri;", "onVoiceNotePlay", "position", "", "onVoiceNotePlaybackSpeedChanged", "speed", "", "onVoiceNoteSeekTo", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private final class ConversationAdapterListener implements ConversationAdapter.ItemClickListener {
        private final /* synthetic */ ConversationAdapter.ItemClickListener $$delegate_0;

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onAddToContactsClicked(Contact contact) {
            Intrinsics.checkNotNullParameter(contact, "contact");
            this.$$delegate_0.onAddToContactsClicked(contact);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onBlockJoinRequest(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.$$delegate_0.onBlockJoinRequest(recipient);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onChangeNumberUpdateContact(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.$$delegate_0.onChangeNumberUpdateContact(recipient);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onGiftBadgeRevealed(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            this.$$delegate_0.onGiftBadgeRevealed(messageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onInMemoryMessageClicked(InMemoryMessageRecord inMemoryMessageRecord) {
            Intrinsics.checkNotNullParameter(inMemoryMessageRecord, "messageRecord");
            this.$$delegate_0.onInMemoryMessageClicked(inMemoryMessageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onIncomingIdentityMismatchClicked(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.$$delegate_0.onIncomingIdentityMismatchClicked(recipientId);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onInviteSharedContactClicked(List<Recipient> list) {
            Intrinsics.checkNotNullParameter(list, "choices");
            this.$$delegate_0.onInviteSharedContactClicked(list);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMessageSharedContactClicked(List<Recipient> list) {
            Intrinsics.checkNotNullParameter(list, "choices");
            this.$$delegate_0.onMessageSharedContactClicked(list);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMessageWithErrorClicked(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            this.$$delegate_0.onMessageWithErrorClicked(messageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMoreTextClicked(RecipientId recipientId, long j, boolean z) {
            Intrinsics.checkNotNullParameter(recipientId, "conversationRecipientId");
            this.$$delegate_0.onMoreTextClicked(recipientId, j, z);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onPlayInlineContent(ConversationMessage conversationMessage) {
            this.$$delegate_0.onPlayInlineContent(conversationMessage);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onRegisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer) {
            Intrinsics.checkNotNullParameter(observer, "onPlaybackStartObserver");
            this.$$delegate_0.onRegisterVoiceNoteCallbacks(observer);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onSharedContactDetailsClicked(Contact contact, View view) {
            Intrinsics.checkNotNullParameter(contact, "contact");
            Intrinsics.checkNotNullParameter(view, "avatarTransitionView");
            this.$$delegate_0.onSharedContactDetailsClicked(contact, view);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onStickerClicked(StickerLocator stickerLocator) {
            Intrinsics.checkNotNullParameter(stickerLocator, "stickerLocator");
            this.$$delegate_0.onStickerClicked(stickerLocator);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onUnregisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer) {
            Intrinsics.checkNotNullParameter(observer, "onPlaybackStartObserver");
            this.$$delegate_0.onUnregisterVoiceNoteCallbacks(observer);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public boolean onUrlClicked(String str) {
            Intrinsics.checkNotNullParameter(str, "url");
            return this.$$delegate_0.onUrlClicked(str);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onViewGroupDescriptionChange(GroupId groupId, String str, boolean z) {
            Intrinsics.checkNotNullParameter(str, "description");
            this.$$delegate_0.onViewGroupDescriptionChange(groupId, str, z);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onViewOnceMessageClicked(MmsMessageRecord mmsMessageRecord) {
            Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
            this.$$delegate_0.onViewOnceMessageClicked(mmsMessageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNotePause(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.$$delegate_0.onVoiceNotePause(uri);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNotePlay(Uri uri, long j, double d) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.$$delegate_0.onVoiceNotePlay(uri, j, d);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNotePlaybackSpeedChanged(Uri uri, float f) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.$$delegate_0.onVoiceNotePlaybackSpeedChanged(uri, f);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNoteSeekTo(Uri uri, double d) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            this.$$delegate_0.onVoiceNoteSeekTo(uri, d);
        }

        public ConversationAdapterListener() {
            MessageQuotesBottomSheet.this = r1;
            this.$$delegate_0 = r1.getAdapterListener();
        }

        @Override // org.thoughtcrime.securesms.conversation.ConversationAdapter.ItemClickListener
        public void onItemClick(MultiselectPart multiselectPart) {
            Intrinsics.checkNotNullParameter(multiselectPart, "item");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getCallback().jumpToMessage(multiselectPart.getMessageRecord());
        }

        @Override // org.thoughtcrime.securesms.conversation.ConversationAdapter.ItemClickListener
        public void onItemLongClick(View view, MultiselectPart multiselectPart) {
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(multiselectPart, "item");
            onItemClick(multiselectPart);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onQuoteClicked(MmsMessageRecord mmsMessageRecord) {
            Intrinsics.checkNotNullParameter(mmsMessageRecord, "messageRecord");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getCallback().jumpToMessage(mmsMessageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onLinkPreviewClicked(LinkPreview linkPreview) {
            Intrinsics.checkNotNullParameter(linkPreview, "linkPreview");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onLinkPreviewClicked(linkPreview);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onQuotedIndicatorClicked(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onQuotedIndicatorClicked(messageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onReactionClicked(MultiselectPart multiselectPart, long j, boolean z) {
            Intrinsics.checkNotNullParameter(multiselectPart, "multiselectPart");
            MessageQuotesBottomSheet.this.dismiss();
            Callback callback = MessageQuotesBottomSheet.this.getCallback();
            MessageRecord messageRecord = multiselectPart.getConversationMessage().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord, "multiselectPart.conversationMessage.messageRecord");
            callback.jumpToMessage(messageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onGroupMemberClicked(RecipientId recipientId, GroupId groupId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onGroupMemberClicked(recipientId, groupId);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMessageWithRecaptchaNeededClicked(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onMessageWithRecaptchaNeededClicked(messageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onGroupMigrationLearnMoreClicked(GroupMigrationMembershipChange groupMigrationMembershipChange) {
            Intrinsics.checkNotNullParameter(groupMigrationMembershipChange, "membershipChange");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onGroupMigrationLearnMoreClicked(groupMigrationMembershipChange);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onChatSessionRefreshLearnMoreClicked() {
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onChatSessionRefreshLearnMoreClicked();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onBadDecryptLearnMoreClicked(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "author");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onBadDecryptLearnMoreClicked(recipientId);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onSafetyNumberLearnMoreClicked(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onSafetyNumberLearnMoreClicked(recipient);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onJoinGroupCallClicked() {
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onJoinGroupCallClicked();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onInviteFriendsToGroupClicked(GroupId.V2 v2) {
            Intrinsics.checkNotNullParameter(v2, "groupId");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onInviteFriendsToGroupClicked(v2);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onEnableCallNotificationsClicked() {
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onEnableCallNotificationsClicked();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onCallToAction(String str) {
            Intrinsics.checkNotNullParameter(str, "action");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onCallToAction(str);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onDonateClicked() {
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onDonateClicked();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onRecipientNameClicked(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "target");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onRecipientNameClicked(recipientId);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onViewGiftBadgeClicked(MessageRecord messageRecord) {
            Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
            MessageQuotesBottomSheet.this.dismiss();
            MessageQuotesBottomSheet.this.getAdapterListener().onViewGiftBadgeClicked(messageRecord);
        }
    }

    /* compiled from: MessageQuotesBottomSheet.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesBottomSheet$Companion;", "", "()V", "KEY_CONVERSATION_RECIPIENT_ID", "", "KEY_MESSAGE_ID", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "conversationRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager, MessageId messageId, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(messageId, "messageId");
            Intrinsics.checkNotNullParameter(recipientId, "conversationRecipientId");
            Bundle bundle = new Bundle();
            bundle.putString("message_id", messageId.serialize());
            bundle.putString(MessageQuotesBottomSheet.KEY_CONVERSATION_RECIPIENT_ID, recipientId.serialize());
            MessageQuotesBottomSheet messageQuotesBottomSheet = new MessageQuotesBottomSheet();
            messageQuotesBottomSheet.setArguments(bundle);
            messageQuotesBottomSheet.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }
}
