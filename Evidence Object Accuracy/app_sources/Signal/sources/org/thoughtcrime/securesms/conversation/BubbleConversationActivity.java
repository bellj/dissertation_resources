package org.thoughtcrime.securesms.conversation;

import androidx.appcompat.widget.Toolbar;

/* loaded from: classes4.dex */
public class BubbleConversationActivity extends ConversationActivity {
    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public boolean isInBubble() {
        return true;
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public void onInitializeToolbar(Toolbar toolbar) {
    }
}
