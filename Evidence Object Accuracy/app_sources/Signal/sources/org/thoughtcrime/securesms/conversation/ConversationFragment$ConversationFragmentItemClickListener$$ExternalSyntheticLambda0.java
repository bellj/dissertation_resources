package org.thoughtcrime.securesms.conversation;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.conversation.ConversationFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda0(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener) {
        this.f$0 = conversationFragmentItemClickListener;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onInMemoryMessageClicked$17(dialogInterface, i);
    }
}
