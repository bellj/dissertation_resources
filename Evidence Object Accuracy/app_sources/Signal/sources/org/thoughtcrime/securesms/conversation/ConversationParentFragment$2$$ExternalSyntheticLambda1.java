package org.thoughtcrime.securesms.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.mms.Slide;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$2$$ExternalSyntheticLambda1 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((Slide) obj).getUri();
    }
}
