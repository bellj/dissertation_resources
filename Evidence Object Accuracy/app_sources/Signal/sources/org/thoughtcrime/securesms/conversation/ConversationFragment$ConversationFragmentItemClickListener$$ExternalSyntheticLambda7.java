package org.thoughtcrime.securesms.conversation;

import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda7 implements ContactUtil.RecipientSelectedCallback {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda7(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener) {
        this.f$0 = conversationFragmentItemClickListener;
    }

    @Override // org.thoughtcrime.securesms.contactshare.ContactUtil.RecipientSelectedCallback
    public final void onSelected(Recipient recipient) {
        this.f$0.lambda$onInviteSharedContactClicked$7(recipient);
    }
}
