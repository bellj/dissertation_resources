package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import j$.util.Collection$EL;
import j$.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.signal.paging.PagedDataSource;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.conversation.ConversationData;
import org.thoughtcrime.securesms.conversation.ConversationDataSource;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.MmsSmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.InMemoryMessageRecord;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.database.model.SmsMessageRecord;
import org.thoughtcrime.securesms.database.model.UpdateDescription;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.v2.MessageNotifierV2;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class ConversationDataSource implements PagedDataSource<MessageId, ConversationMessage> {
    private static final String TAG = Log.tag(ConversationDataSource.class);
    private int baseSize;
    private final Context context;
    private final ConversationData.MessageRequestData messageRequestData;
    private final boolean showUniversalExpireTimerUpdate;
    private final long threadId;

    public ConversationDataSource(Context context, long j, ConversationData.MessageRequestData messageRequestData, boolean z, int i) {
        this.context = context;
        this.threadId = j;
        this.messageRequestData = messageRequestData;
        this.showUniversalExpireTimerUpdate = z;
        this.baseSize = i;
    }

    @Override // org.signal.paging.PagedDataSource
    public int size() {
        long currentTimeMillis = System.currentTimeMillis();
        int sizeInternal = getSizeInternal() + (this.messageRequestData.includeWarningUpdateMessage() ? 1 : 0) + (this.showUniversalExpireTimerUpdate ? 1 : 0);
        String str = TAG;
        Log.d(str, "[size(), thread " + this.threadId + "] " + (System.currentTimeMillis() - currentTimeMillis) + " ms");
        return sizeInternal;
    }

    private int getSizeInternal() {
        synchronized (this) {
            int i = this.baseSize;
            if (i == -1) {
                return SignalDatabase.mmsSms().getConversationCount(this.threadId);
            }
            this.baseSize = -1;
            return i;
        }
    }

    @Override // org.signal.paging.PagedDataSource
    public List<ConversationMessage> load(int i, int i2, PagedDataSource.CancellationSignal cancellationSignal) {
        Stopwatch stopwatch = new Stopwatch("load(" + i + ", " + i2 + "), thread " + this.threadId);
        MmsSmsDatabase mmsSms = SignalDatabase.mmsSms();
        ArrayList arrayList = new ArrayList(i2);
        MentionHelper mentionHelper = new MentionHelper();
        AttachmentHelper attachmentHelper = new AttachmentHelper();
        ReactionHelper reactionHelper = new ReactionHelper();
        HashSet<ServiceId> hashSet = new HashSet();
        AttachmentHelper attachmentHelper2 = attachmentHelper;
        MmsSmsDatabase.Reader readerFor = MmsSmsDatabase.readerFor(mmsSms.getConversation(this.threadId, (long) i, (long) i2));
        while (true) {
            try {
                MessageRecord next = readerFor.getNext();
                if (next == null || cancellationSignal.isCanceled()) {
                    break;
                }
                arrayList.add(next);
                mentionHelper.add(next);
                reactionHelper.add(next);
                attachmentHelper2.add(next);
                UpdateDescription updateDisplayBody = next.getUpdateDisplayBody(this.context, null);
                if (updateDisplayBody != null) {
                    hashSet.addAll(updateDisplayBody.getMentioned());
                }
                attachmentHelper2 = attachmentHelper2;
            } catch (Throwable th) {
                if (readerFor != null) {
                    try {
                        readerFor.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }
        readerFor.close();
        if (this.messageRequestData.includeWarningUpdateMessage() && i + i2 >= size()) {
            arrayList.add(new InMemoryMessageRecord.NoGroupsInCommon(this.threadId, this.messageRequestData.isGroup()));
        }
        if (this.showUniversalExpireTimerUpdate) {
            arrayList.add(new InMemoryMessageRecord.UniversalExpireTimerUpdate(this.threadId));
        }
        stopwatch.split(MessageNotifierV2.NOTIFICATION_GROUP);
        mentionHelper.fetchMentions(this.context);
        stopwatch.split("mentions");
        reactionHelper.fetchReactions();
        stopwatch.split("reactions");
        List<MessageRecord> buildUpdatedModels = reactionHelper.buildUpdatedModels(arrayList);
        stopwatch.split("reaction-models");
        attachmentHelper2.fetchAttachments(this.context);
        stopwatch.split("attachments");
        List<MessageRecord> buildUpdatedModels2 = attachmentHelper2.buildUpdatedModels(this.context, buildUpdatedModels);
        stopwatch.split("attachment-models");
        for (ServiceId serviceId : hashSet) {
            Recipient.resolved(RecipientId.from(serviceId));
        }
        stopwatch.split("recipient-resolves");
        List<ConversationMessage> list = Stream.of(buildUpdatedModels2).map(new Function(mentionHelper) { // from class: org.thoughtcrime.securesms.conversation.ConversationDataSource$$ExternalSyntheticLambda0
            public final /* synthetic */ ConversationDataSource.MentionHelper f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationDataSource.$r8$lambda$Wm_jK146YMXC5B4xjbUNhWnPmoM(ConversationDataSource.this, this.f$1, (MessageRecord) obj);
            }
        }).toList();
        stopwatch.split("conversion");
        stopwatch.stop(TAG);
        return list;
    }

    public /* synthetic */ ConversationMessage lambda$load$0(MentionHelper mentionHelper, MessageRecord messageRecord) {
        return ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(this.context, messageRecord, mentionHelper.getMentions(messageRecord.getId()));
    }

    public ConversationMessage load(MessageId messageId) {
        List<Mention> list;
        ConversationMessage createWithUnresolvedData;
        Stopwatch stopwatch = new Stopwatch("load(" + messageId + "), thread " + this.threadId);
        MessageRecord messageRecordOrNull = (messageId.isMms() ? SignalDatabase.mms() : SignalDatabase.sms()).getMessageRecordOrNull(messageId.getId());
        stopwatch.split("message");
        if (messageRecordOrNull != null) {
            try {
                if (messageId.isMms()) {
                    list = SignalDatabase.mentions().getMentionsForMessage(messageId.getId());
                } else {
                    list = Collections.emptyList();
                }
                stopwatch.split("mentions");
                MessageRecord recordWithReactions = ReactionHelper.recordWithReactions(messageRecordOrNull, SignalDatabase.reactions().getReactions(messageId));
                stopwatch.split("reactions");
                if (messageId.isMms()) {
                    List<DatabaseAttachment> attachmentsForMessage = SignalDatabase.attachments().getAttachmentsForMessage(messageId.getId());
                    if (attachmentsForMessage.size() > 0) {
                        recordWithReactions = ((MediaMmsMessageRecord) recordWithReactions).withAttachments(this.context, attachmentsForMessage);
                    }
                }
                stopwatch.split("attachments");
                createWithUnresolvedData = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(ApplicationDependencies.getApplication(), recordWithReactions, list);
            } finally {
                stopwatch.stop(TAG);
            }
        } else {
            createWithUnresolvedData = null;
        }
        return createWithUnresolvedData;
    }

    public MessageId getKey(ConversationMessage conversationMessage) {
        return new MessageId(conversationMessage.getMessageRecord().getId(), conversationMessage.getMessageRecord().isMms());
    }

    /* loaded from: classes4.dex */
    public static class MentionHelper {
        private Map<Long, List<Mention>> messageIdToMentions;
        private Collection<Long> messageIds;

        private MentionHelper() {
            this.messageIds = new LinkedList();
            this.messageIdToMentions = new HashMap();
        }

        void add(MessageRecord messageRecord) {
            if (messageRecord.isMms()) {
                this.messageIds.add(Long.valueOf(messageRecord.getId()));
            }
        }

        void fetchMentions(Context context) {
            this.messageIdToMentions = SignalDatabase.mentions().getMentionsForMessages(this.messageIds);
        }

        List<Mention> getMentions(long j) {
            return this.messageIdToMentions.get(Long.valueOf(j));
        }
    }

    /* loaded from: classes4.dex */
    public static class AttachmentHelper {
        private Map<Long, List<DatabaseAttachment>> messageIdToAttachments;
        private Collection<Long> messageIds;

        private AttachmentHelper() {
            this.messageIds = new LinkedList();
            this.messageIdToAttachments = new HashMap();
        }

        void add(MessageRecord messageRecord) {
            if (messageRecord.isMms()) {
                this.messageIds.add(Long.valueOf(messageRecord.getId()));
            }
        }

        void fetchAttachments(Context context) {
            this.messageIdToAttachments = SignalDatabase.attachments().getAttachmentsForMessages(this.messageIds);
        }

        List<MessageRecord> buildUpdatedModels(Context context, List<MessageRecord> list) {
            return (List) Collection$EL.stream(list).map(new ConversationDataSource$AttachmentHelper$$ExternalSyntheticLambda0(this, context)).collect(Collectors.toList());
        }

        public /* synthetic */ MessageRecord lambda$buildUpdatedModels$0(Context context, MessageRecord messageRecord) {
            if (messageRecord instanceof MediaMmsMessageRecord) {
                List<DatabaseAttachment> list = this.messageIdToAttachments.get(Long.valueOf(messageRecord.getId()));
                if (Util.hasItems(list)) {
                    return ((MediaMmsMessageRecord) messageRecord).withAttachments(context, list);
                }
            }
            return messageRecord;
        }
    }

    /* loaded from: classes4.dex */
    public static class ReactionHelper {
        private Map<MessageId, List<ReactionRecord>> messageIdToReactions = new HashMap();
        private Collection<MessageId> messageIds = new LinkedList();

        public void add(MessageRecord messageRecord) {
            this.messageIds.add(new MessageId(messageRecord.getId(), messageRecord.isMms()));
        }

        public void addAll(List<MessageRecord> list) {
            for (MessageRecord messageRecord : list) {
                add(messageRecord);
            }
        }

        public void fetchReactions() {
            this.messageIdToReactions = SignalDatabase.reactions().getReactionsForMessages(this.messageIds);
        }

        public List<MessageRecord> buildUpdatedModels(List<MessageRecord> list) {
            return (List) Collection$EL.stream(list).map(new ConversationDataSource$ReactionHelper$$ExternalSyntheticLambda0(this)).collect(Collectors.toList());
        }

        public /* synthetic */ MessageRecord lambda$buildUpdatedModels$0(MessageRecord messageRecord) {
            return recordWithReactions(messageRecord, this.messageIdToReactions.get(new MessageId(messageRecord.getId(), messageRecord.isMms())));
        }

        public static MessageRecord recordWithReactions(MessageRecord messageRecord, List<ReactionRecord> list) {
            if (!Util.hasItems(list)) {
                return messageRecord;
            }
            if (messageRecord instanceof MediaMmsMessageRecord) {
                return ((MediaMmsMessageRecord) messageRecord).withReactions(list);
            }
            if (messageRecord instanceof SmsMessageRecord) {
                return ((SmsMessageRecord) messageRecord).withReactions(list);
            }
            throw new IllegalStateException("We have reactions for an unsupported record type: " + messageRecord.getClass().getName());
        }
    }
}
