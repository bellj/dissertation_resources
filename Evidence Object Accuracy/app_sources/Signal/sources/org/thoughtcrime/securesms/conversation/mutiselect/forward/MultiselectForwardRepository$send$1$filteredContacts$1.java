package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;

/* compiled from: MultiselectForwardRepository.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "invoke", "(Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;)Ljava/lang/Boolean;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardRepository$send$1$filteredContacts$1 extends Lambda implements Function1<ContactSearchKey, Boolean> {
    public static final MultiselectForwardRepository$send$1$filteredContacts$1 INSTANCE = new MultiselectForwardRepository$send$1$filteredContacts$1();

    MultiselectForwardRepository$send$1$filteredContacts$1() {
        super(1);
    }

    public final Boolean invoke(ContactSearchKey contactSearchKey) {
        Intrinsics.checkNotNullParameter(contactSearchKey, "it");
        return Boolean.valueOf((contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.Story) || (contactSearchKey instanceof ContactSearchKey.RecipientSearchKey.KnownRecipient));
    }
}
