package org.thoughtcrime.securesms.conversation;

import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public enum AttachmentKeyboardButton {
    GALLERY(R.string.AttachmentKeyboard_gallery, R.drawable.ic_gallery_outline_24),
    FILE(R.string.AttachmentKeyboard_file, R.drawable.ic_file_outline_24),
    PAYMENT(R.string.AttachmentKeyboard_payment, R.drawable.ic_payments_24),
    CONTACT(R.string.AttachmentKeyboard_contact, R.drawable.ic_contact_outline_24),
    LOCATION(R.string.AttachmentKeyboard_location, R.drawable.ic_location_outline_24);
    
    private final int iconRes;
    private final int titleRes;

    AttachmentKeyboardButton(int i, int i2) {
        this.titleRes = i;
        this.iconRes = i2;
    }

    public int getTitleRes() {
        return this.titleRes;
    }

    public int getIconRes() {
        return this.iconRes;
    }
}
