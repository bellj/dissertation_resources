package org.thoughtcrime.securesms.conversation.colors.ui;

import android.view.View;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ChatColorSelectionAdapter$ChatColorMappingViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ChatColorMappingModel f$0;
    public final /* synthetic */ ChatColorSelectionAdapter.ChatColorMappingViewHolder f$1;

    public /* synthetic */ ChatColorSelectionAdapter$ChatColorMappingViewHolder$$ExternalSyntheticLambda0(ChatColorMappingModel chatColorMappingModel, ChatColorSelectionAdapter.ChatColorMappingViewHolder chatColorMappingViewHolder) {
        this.f$0 = chatColorMappingModel;
        this.f$1 = chatColorMappingViewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ChatColorSelectionAdapter.ChatColorMappingViewHolder.m1493bind$lambda0(this.f$0, this.f$1, view);
    }
}
