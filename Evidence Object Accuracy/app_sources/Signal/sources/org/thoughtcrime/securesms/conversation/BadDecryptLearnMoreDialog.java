package org.thoughtcrime.securesms.conversation;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class BadDecryptLearnMoreDialog extends DialogFragment {
    private static final String FRAGMENT_TAG;
    private static final String KEY_DISPLAY_NAME;
    private static final String KEY_GROUP_CHAT;
    private static final String TAG = Log.tag(BadDecryptLearnMoreDialog.class);

    public static void show(FragmentManager fragmentManager, String str, boolean z) {
        if (fragmentManager.findFragmentByTag(FRAGMENT_TAG) != null) {
            Log.i(TAG, "Already shown!");
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(KEY_DISPLAY_NAME, str);
        bundle.putBoolean(KEY_GROUP_CHAT, z);
        BadDecryptLearnMoreDialog badDecryptLearnMoreDialog = new BadDecryptLearnMoreDialog();
        badDecryptLearnMoreDialog.setArguments(bundle);
        badDecryptLearnMoreDialog.show(fragmentManager, FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireContext());
        View inflate = LayoutInflater.from(requireContext()).inflate(R.layout.bad_decrypt_learn_more_dialog_fragment, (ViewGroup) null);
        TextView textView = (TextView) inflate.findViewById(R.id.bad_decrypt_dialog_body);
        String string = requireArguments().getString(KEY_DISPLAY_NAME);
        if (requireArguments().getBoolean(KEY_GROUP_CHAT)) {
            textView.setText(getString(R.string.BadDecryptLearnMoreDialog_couldnt_be_delivered_group, string));
        } else {
            textView.setText(getString(R.string.BadDecryptLearnMoreDialog_couldnt_be_delivered_individual, string));
        }
        materialAlertDialogBuilder.setView(inflate).setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        return materialAlertDialogBuilder.create();
    }
}
