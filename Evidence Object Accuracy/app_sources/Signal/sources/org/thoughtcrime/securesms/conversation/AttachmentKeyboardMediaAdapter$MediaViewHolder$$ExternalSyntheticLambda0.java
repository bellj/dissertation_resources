package org.thoughtcrime.securesms.conversation;

import android.view.View;
import org.thoughtcrime.securesms.conversation.AttachmentKeyboardMediaAdapter;
import org.thoughtcrime.securesms.mediasend.Media;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AttachmentKeyboardMediaAdapter$MediaViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AttachmentKeyboardMediaAdapter.Listener f$0;
    public final /* synthetic */ Media f$1;

    public /* synthetic */ AttachmentKeyboardMediaAdapter$MediaViewHolder$$ExternalSyntheticLambda0(AttachmentKeyboardMediaAdapter.Listener listener, Media media) {
        this.f$0 = listener;
        this.f$1 = media;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onMediaClicked(this.f$1);
    }
}
