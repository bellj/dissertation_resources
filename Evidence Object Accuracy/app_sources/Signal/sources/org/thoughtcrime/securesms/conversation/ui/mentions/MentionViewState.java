package org.thoughtcrime.securesms.conversation.ui.mentions;

import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;

/* loaded from: classes4.dex */
public final class MentionViewState extends RecipientMappingModel<MentionViewState> {
    private final Recipient recipient;

    public MentionViewState(Recipient recipient) {
        this.recipient = recipient;
    }

    @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel
    public Recipient getRecipient() {
        return this.recipient;
    }
}
