package org.thoughtcrime.securesms.conversation;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewKt;
import androidx.vectordrawable.graphics.drawable.AnimatorInflaterCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.IndexedFunction;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.WindowUtil;

/* loaded from: classes4.dex */
public final class ConversationReactionOverlay extends FrameLayout {
    private static final Interpolator INTERPOLATOR = new DecelerateInterpolator();
    private Activity activity;
    private int animationEmojiStartDelayFactor;
    private View backgroundView;
    private int bottomNavigationBarHeight;
    private ConversationContextMenu contextMenu;
    private View conversationItem;
    private Recipient conversationRecipient;
    private int customEmojiIndex;
    private final PointF deadzoneTouchPoint = new PointF();
    private float distanceFromTouchDownPointToBottomOfScrubberDeadZone;
    private boolean downIsOurs;
    private View dropdownAnchor;
    private final Rect emojiStripViewBounds = new Rect();
    private final Rect emojiViewGlobalRect = new Rect();
    private EmojiImageView[] emojiViews;
    private ConstraintLayout foregroundView;
    private AnimatorSet hideAnimatorSet = new AnimatorSet();
    private final Boundary horizontalEmojiBoundary = new Boundary();
    private View inputShade;
    private boolean isNonAdminInAnnouncementGroup;
    private MessageRecord messageRecord;
    private OnActionSelectedListener onActionSelectedListener;
    private OnHideListener onHideListener;
    private OnReactionSelectedListener onReactionSelectedListener;
    private int originalNavigationBarColor;
    private int originalStatusBarColor;
    private OverlayState overlayState = OverlayState.HIDDEN;
    private AnimatorSet revealAnimatorSet = new AnimatorSet();
    private int scrubberHorizontalMargin;
    private int scrubberWidth;
    private float segmentSize;
    private int selected = -1;
    private SelectedConversationModel selectedConversationModel;
    private int selectedVerticalTranslation;
    private View selectedView;
    private int statusBarHeight;
    private View toolbarShade;
    private float touchDownDeadZoneSize;
    private final Boundary verticalScrubBoundary = new Boundary();

    /* loaded from: classes4.dex */
    public enum Action {
        REPLY,
        FORWARD,
        RESEND,
        DOWNLOAD,
        COPY,
        MULTISELECT,
        VIEW_INFO,
        DELETE
    }

    /* loaded from: classes4.dex */
    public interface OnActionSelectedListener {
        void onActionSelected(Action action);
    }

    /* loaded from: classes4.dex */
    public interface OnHideListener {
        void onHide();

        void startHide();
    }

    /* loaded from: classes4.dex */
    public interface OnReactionSelectedListener {
        void onCustomReactionSelected(MessageRecord messageRecord, boolean z);

        void onReactionSelected(MessageRecord messageRecord, String str);
    }

    /* loaded from: classes4.dex */
    public enum OverlayState {
        HIDDEN,
        UNINITAILIZED,
        DEADZONE,
        SCRUB,
        TAP
    }

    public ConversationReactionOverlay(Context context) {
        super(context);
    }

    public ConversationReactionOverlay(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.dropdownAnchor = findViewById(R.id.dropdown_anchor);
        this.toolbarShade = findViewById(R.id.toolbar_shade);
        this.inputShade = findViewById(R.id.input_shade);
        this.conversationItem = findViewById(R.id.conversation_item);
        this.backgroundView = findViewById(R.id.conversation_reaction_scrubber_background);
        this.foregroundView = (ConstraintLayout) findViewById(R.id.conversation_reaction_scrubber_foreground);
        this.selectedView = findViewById(R.id.conversation_reaction_current_selection_indicator);
        EmojiImageView[] emojiImageViewArr = {(EmojiImageView) findViewById(R.id.reaction_1), (EmojiImageView) findViewById(R.id.reaction_2), (EmojiImageView) findViewById(R.id.reaction_3), (EmojiImageView) findViewById(R.id.reaction_4), (EmojiImageView) findViewById(R.id.reaction_5), (EmojiImageView) findViewById(R.id.reaction_6), (EmojiImageView) findViewById(R.id.reaction_7)};
        this.emojiViews = emojiImageViewArr;
        this.customEmojiIndex = emojiImageViewArr.length - 1;
        this.distanceFromTouchDownPointToBottomOfScrubberDeadZone = (float) getResources().getDimensionPixelSize(R.dimen.conversation_reaction_scrub_deadzone_distance_from_touch_bottom);
        this.touchDownDeadZoneSize = (float) getResources().getDimensionPixelSize(R.dimen.conversation_reaction_touch_deadzone_size);
        this.scrubberWidth = getResources().getDimensionPixelOffset(R.dimen.reaction_scrubber_width);
        this.selectedVerticalTranslation = getResources().getDimensionPixelOffset(R.dimen.conversation_reaction_scrub_vertical_translation);
        this.scrubberHorizontalMargin = getResources().getDimensionPixelOffset(R.dimen.conversation_reaction_scrub_horizontal_margin);
        this.animationEmojiStartDelayFactor = getResources().getInteger(R.integer.reaction_scrubber_emoji_reveal_duration_start_delay_factor);
        initAnimators();
    }

    public void show(Activity activity, Recipient recipient, ConversationMessage conversationMessage, PointF pointF, boolean z, SelectedConversationModel selectedConversationModel) {
        int i;
        int i2;
        if (this.overlayState == OverlayState.HIDDEN) {
            this.messageRecord = conversationMessage.getMessageRecord();
            this.conversationRecipient = recipient;
            this.selectedConversationModel = selectedConversationModel;
            this.isNonAdminInAnnouncementGroup = z;
            this.overlayState = OverlayState.UNINITAILIZED;
            this.selected = -1;
            setupSelectedEmoji();
            int i3 = Build.VERSION.SDK_INT;
            if (i3 >= 21) {
                View findViewById = activity.findViewById(16908335);
                if (findViewById == null) {
                    i = 0;
                } else {
                    i = findViewById.getHeight();
                }
                this.statusBarHeight = i;
                View findViewById2 = activity.findViewById(16908336);
                if (findViewById2 == null) {
                    i2 = 0;
                } else {
                    i2 = findViewById2.getHeight();
                }
                this.bottomNavigationBarHeight = i2;
            } else {
                this.statusBarHeight = ViewUtil.getStatusBarHeight(this);
                this.bottomNavigationBarHeight = ViewUtil.getNavigationBarHeight(this);
            }
            if (zeroNavigationBarHeightForConfiguration()) {
                this.bottomNavigationBarHeight = 0;
            }
            this.toolbarShade.setVisibility(0);
            this.toolbarShade.setAlpha(1.0f);
            this.inputShade.setVisibility(0);
            this.inputShade.setAlpha(1.0f);
            Bitmap bitmap = selectedConversationModel.getBitmap();
            this.conversationItem.setLayoutParams(new FrameLayout.LayoutParams(bitmap.getWidth(), bitmap.getHeight()));
            this.conversationItem.setBackground(new BitmapDrawable(getResources(), bitmap));
            boolean isOutgoing = selectedConversationModel.isOutgoing() ^ ViewUtil.isLtr(this);
            this.conversationItem.setScaleX(0.95f);
            this.conversationItem.setScaleY(0.95f);
            setVisibility(4);
            if (i3 >= 21) {
                this.activity = activity;
                updateSystemUiOnShow(activity);
            }
            ViewKt.doOnLayout(this, new Function1(activity, conversationMessage, pointF, isOutgoing) { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda1
                public final /* synthetic */ Activity f$1;
                public final /* synthetic */ ConversationMessage f$2;
                public final /* synthetic */ PointF f$3;
                public final /* synthetic */ boolean f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // kotlin.jvm.functions.Function1
                public final Object invoke(Object obj) {
                    return ConversationReactionOverlay.this.lambda$show$0(this.f$1, this.f$2, this.f$3, this.f$4, (View) obj);
                }
            });
        }
    }

    public /* synthetic */ Unit lambda$show$0(Activity activity, ConversationMessage conversationMessage, PointF pointF, boolean z, View view) {
        showAfterLayout(activity, conversationMessage, pointF, z);
        return Unit.INSTANCE;
    }

    /* JADX WARNING: Removed duplicated region for block: B:76:0x02ce  */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x02d1  */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0314  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0341  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void showAfterLayout(android.app.Activity r22, org.thoughtcrime.securesms.conversation.ConversationMessage r23, android.graphics.PointF r24, boolean r25) {
        /*
        // Method dump skipped, instructions count: 912
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.showAfterLayout(android.app.Activity, org.thoughtcrime.securesms.conversation.ConversationMessage, android.graphics.PointF, boolean):void");
    }

    private float getReactionBarOffsetForTouch(PointF pointF, float f, float f2, float f3, int i, float f4, float f5) {
        float min = Math.min(pointF.y - ((float) this.statusBarHeight), f);
        if (Math.abs(f5 - f) < DimensionUnit.DP.toPixels(150.0f)) {
            min = (f3 - f2) + f5;
        }
        return Math.max((min - f3) - ((float) i), f4);
    }

    private void updateToolbarShade(Activity activity) {
        View findViewById = activity.findViewById(R.id.toolbar);
        View findViewById2 = activity.findViewById(R.id.conversation_banner_container);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.toolbarShade.getLayoutParams();
        layoutParams.height = findViewById.getHeight() + findViewById2.getHeight();
        this.toolbarShade.setLayoutParams(layoutParams);
    }

    private void updateInputShade(Activity activity) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) this.inputShade.getLayoutParams();
        layoutParams.bottomMargin = this.bottomNavigationBarHeight;
        layoutParams.height = getInputPanelHeight(activity);
        this.inputShade.setLayoutParams(layoutParams);
    }

    private int getInputPanelHeight(Activity activity) {
        View findViewById = activity.findViewById(R.id.conversation_activity_panel_parent);
        View findViewById2 = activity.findViewById(R.id.emoji_drawer);
        return findViewById.getHeight() + ((findViewById2 == null || findViewById2.getVisibility() != 0) ? 0 : findViewById2.getHeight());
    }

    private boolean zeroNavigationBarHeightForConfiguration() {
        boolean z = getResources().getConfiguration().orientation == 2;
        if (Build.VERSION.SDK_INT < 29) {
            return z;
        }
        if (getRootWindowInsets().getSystemGestureInsets().bottom != 0 || !z) {
            return false;
        }
        return true;
    }

    private void updateSystemUiOnShow(Activity activity) {
        Window window = activity.getWindow();
        int color = ContextCompat.getColor(getContext(), R.color.conversation_item_selected_system_ui);
        this.originalStatusBarColor = window.getStatusBarColor();
        WindowUtil.setStatusBarColor(window, color);
        this.originalNavigationBarColor = window.getNavigationBarColor();
        WindowUtil.setNavigationBarColor(activity, color);
        if (!ThemeUtil.isDarkTheme(getContext())) {
            WindowUtil.clearLightStatusBar(window);
            WindowUtil.clearLightNavigationBar(window);
        }
    }

    public void hide() {
        hideInternal(this.onHideListener);
    }

    public void hideForReactWithAny() {
        hideInternal(this.onHideListener);
    }

    private void hideInternal(final OnHideListener onHideListener) {
        this.overlayState = OverlayState.HIDDEN;
        final AnimatorSet newHideAnimatorSet = newHideAnimatorSet();
        this.hideAnimatorSet = newHideAnimatorSet;
        this.revealAnimatorSet.end();
        newHideAnimatorSet.start();
        if (onHideListener != null) {
            onHideListener.startHide();
        }
        if (this.selectedConversationModel.getFocusedView() != null) {
            ViewUtil.focusAndShowKeyboard(this.selectedConversationModel.getFocusedView());
        }
        newHideAnimatorSet.addListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.1
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                newHideAnimatorSet.removeListener(this);
                ConversationReactionOverlay.this.toolbarShade.setVisibility(4);
                ConversationReactionOverlay.this.inputShade.setVisibility(4);
                OnHideListener onHideListener2 = onHideListener;
                if (onHideListener2 != null) {
                    onHideListener2.onHide();
                }
            }
        });
        ConversationContextMenu conversationContextMenu = this.contextMenu;
        if (conversationContextMenu != null) {
            conversationContextMenu.dismiss();
        }
    }

    public boolean isShowing() {
        return this.overlayState != OverlayState.HIDDEN;
    }

    public MessageRecord getMessageRecord() {
        return this.messageRecord;
    }

    @Override // android.widget.FrameLayout, android.view.View, android.view.ViewGroup
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        updateBoundsOnLayoutChanged();
    }

    private void updateBoundsOnLayoutChanged() {
        this.backgroundView.getGlobalVisibleRect(this.emojiStripViewBounds);
        this.emojiViews[0].getGlobalVisibleRect(this.emojiViewGlobalRect);
        this.emojiStripViewBounds.left = getStart(this.emojiViewGlobalRect);
        EmojiImageView[] emojiImageViewArr = this.emojiViews;
        emojiImageViewArr[emojiImageViewArr.length - 1].getGlobalVisibleRect(this.emojiViewGlobalRect);
        this.emojiStripViewBounds.right = getEnd(this.emojiViewGlobalRect);
        this.segmentSize = ((float) this.emojiStripViewBounds.width()) / ((float) this.emojiViews.length);
    }

    private int getStart(Rect rect) {
        if (ViewUtil.isLtr(this)) {
            return rect.left;
        }
        return rect.right;
    }

    private int getEnd(Rect rect) {
        if (ViewUtil.isLtr(this)) {
            return rect.right;
        }
        return rect.left;
    }

    public boolean applyTouchEvent(MotionEvent motionEvent) {
        if (!isShowing()) {
            throw new IllegalStateException("Touch events should only be propagated to this method if we are displaying the scrubber.");
        } else if ((motionEvent.getAction() & 65280) != 0) {
            return true;
        } else {
            if (this.overlayState == OverlayState.UNINITAILIZED) {
                this.downIsOurs = false;
                this.deadzoneTouchPoint.set(motionEvent.getX(), motionEvent.getY());
                this.overlayState = OverlayState.DEADZONE;
            }
            OverlayState overlayState = this.overlayState;
            OverlayState overlayState2 = OverlayState.DEADZONE;
            if (overlayState == overlayState2) {
                float abs = Math.abs(this.deadzoneTouchPoint.x - motionEvent.getX());
                float abs2 = Math.abs(this.deadzoneTouchPoint.y - motionEvent.getY());
                float f = this.touchDownDeadZoneSize;
                if (abs > f || abs2 > f) {
                    this.overlayState = OverlayState.SCRUB;
                } else {
                    if (motionEvent.getAction() == 1) {
                        this.overlayState = OverlayState.TAP;
                        if (this.downIsOurs) {
                            handleUpEvent();
                            return true;
                        }
                    }
                    if (2 == motionEvent.getAction()) {
                        return true;
                    }
                    return false;
                }
            }
            int action = motionEvent.getAction();
            if (action == 0) {
                this.selected = getSelectedIndexViaDownEvent(motionEvent);
                this.deadzoneTouchPoint.set(motionEvent.getX(), motionEvent.getY());
                this.overlayState = overlayState2;
                this.downIsOurs = true;
                return true;
            } else if (action == 1) {
                handleUpEvent();
                return this.downIsOurs;
            } else if (action == 2) {
                this.selected = getSelectedIndexViaMoveEvent(motionEvent);
                return true;
            } else if (action != 3) {
                return false;
            } else {
                hide();
                return this.downIsOurs;
            }
        }
    }

    private void setupSelectedEmoji() {
        List<String> reactions = SignalStore.emojiValues().getReactions();
        String oldEmoji = getOldEmoji(this.messageRecord);
        if (oldEmoji == null) {
            this.selectedView.setVisibility(8);
        }
        int i = 0;
        boolean z = false;
        while (true) {
            EmojiImageView[] emojiImageViewArr = this.emojiViews;
            if (i < emojiImageViewArr.length) {
                EmojiImageView emojiImageView = emojiImageViewArr[i];
                emojiImageView.setScaleX(1.0f);
                emojiImageView.setScaleY(1.0f);
                emojiImageView.setTranslationY(0.0f);
                boolean z2 = i == this.customEmojiIndex;
                boolean z3 = !z2 && oldEmoji != null && EmojiUtil.isCanonicallyEqual(reactions.get(i), oldEmoji);
                boolean z4 = z2 && oldEmoji != null;
                if (!z && (z3 || z4)) {
                    this.selectedView.setVisibility(0);
                    ConstraintSet constraintSet = new ConstraintSet();
                    constraintSet.clone(this.foregroundView);
                    constraintSet.clear(this.selectedView.getId(), 1);
                    constraintSet.clear(this.selectedView.getId(), 2);
                    constraintSet.connect(this.selectedView.getId(), 1, emojiImageView.getId(), 1);
                    constraintSet.connect(this.selectedView.getId(), 2, emojiImageView.getId(), 2);
                    constraintSet.applyTo(this.foregroundView);
                    if (z2) {
                        emojiImageView.setImageEmoji(oldEmoji);
                        emojiImageView.setTag(oldEmoji);
                    } else {
                        emojiImageView.setImageEmoji(SignalStore.emojiValues().getPreferredVariation(reactions.get(i)));
                    }
                    z = true;
                } else if (z2) {
                    emojiImageView.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_any_emoji_32));
                    emojiImageView.setTag(null);
                } else {
                    emojiImageView.setImageEmoji(SignalStore.emojiValues().getPreferredVariation(reactions.get(i)));
                }
                i++;
            } else {
                return;
            }
        }
    }

    private int getSelectedIndexViaDownEvent(MotionEvent motionEvent) {
        Rect rect = this.emojiStripViewBounds;
        return getSelectedIndexViaMotionEvent(motionEvent, new Boundary((float) rect.top, (float) rect.bottom));
    }

    private int getSelectedIndexViaMoveEvent(MotionEvent motionEvent) {
        return getSelectedIndexViaMotionEvent(motionEvent, this.verticalScrubBoundary);
    }

    private int getSelectedIndexViaMotionEvent(MotionEvent motionEvent, Boundary boundary) {
        EmojiImageView[] emojiImageViewArr;
        if (this.backgroundView.getVisibility() != 0) {
            return -1;
        }
        int i = 0;
        int i2 = -1;
        while (true) {
            emojiImageViewArr = this.emojiViews;
            if (i >= emojiImageViewArr.length) {
                break;
            }
            float f = this.segmentSize;
            float f2 = (((float) i) * f) + ((float) this.emojiStripViewBounds.left);
            this.horizontalEmojiBoundary.update(f2, f + f2);
            if (this.horizontalEmojiBoundary.contains(motionEvent.getX()) && boundary.contains(motionEvent.getY())) {
                i2 = i;
            }
            i++;
        }
        int i3 = this.selected;
        if (!(i3 == -1 || i3 == i2)) {
            shrinkView(emojiImageViewArr[i3]);
        }
        if (!(this.selected == i2 || i2 == -1)) {
            growView(this.emojiViews[i2]);
        }
        return i2;
    }

    private void growView(View view) {
        view.performHapticFeedback(3);
        view.animate().scaleY(1.5f).scaleX(1.5f).translationY((float) (-this.selectedVerticalTranslation)).setDuration(200).setInterpolator(INTERPOLATOR).start();
    }

    private void shrinkView(View view) {
        view.animate().scaleX(1.0f).scaleY(1.0f).translationY(0.0f).setDuration(200).setInterpolator(INTERPOLATOR).start();
    }

    private void handleUpEvent() {
        if (this.selected == -1 || this.onReactionSelectedListener == null || this.backgroundView.getVisibility() != 0) {
            hide();
            return;
        }
        int i = this.selected;
        if (i == this.customEmojiIndex) {
            this.onReactionSelectedListener.onCustomReactionSelected(this.messageRecord, this.emojiViews[i].getTag() != null);
        } else {
            this.onReactionSelectedListener.onReactionSelected(this.messageRecord, SignalStore.emojiValues().getPreferredVariation(SignalStore.emojiValues().getReactions().get(this.selected)));
        }
    }

    public void setOnReactionSelectedListener(OnReactionSelectedListener onReactionSelectedListener) {
        this.onReactionSelectedListener = onReactionSelectedListener;
    }

    public void setOnActionSelectedListener(OnActionSelectedListener onActionSelectedListener) {
        this.onActionSelectedListener = onActionSelectedListener;
    }

    public void setOnHideListener(OnHideListener onHideListener) {
        this.onHideListener = onHideListener;
    }

    private static String getOldEmoji(MessageRecord messageRecord) {
        return (String) Stream.of(messageRecord.getReactions()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationReactionOverlay.lambda$getOldEmoji$1((ReactionRecord) obj);
            }
        }).findFirst().map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ReactionRecord) obj).getEmoji();
            }
        }).orElse(null);
    }

    public static /* synthetic */ boolean lambda$getOldEmoji$1(ReactionRecord reactionRecord) {
        return reactionRecord.getAuthor().serialize().equals(Recipient.self().getId().serialize());
    }

    private List<ActionItem> getMenuActionItems(ConversationMessage conversationMessage) {
        int i = 0;
        MenuState menuState = MenuState.getMenuState(this.conversationRecipient, conversationMessage.getMultiselectCollection().toSet(), false, this.isNonAdminInAnnouncementGroup);
        ArrayList arrayList = new ArrayList();
        if (menuState.shouldShowReplyAction()) {
            arrayList.add(new ActionItem(R.drawable.ic_reply_24_tinted, getResources().getString(R.string.conversation_selection__menu_reply), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda7
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationReactionOverlay.this.lambda$getMenuActionItems$2();
                }
            }));
        }
        if (menuState.shouldShowForwardAction()) {
            arrayList.add(new ActionItem(R.drawable.ic_forward_24_tinted, getResources().getString(R.string.conversation_selection__menu_forward), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda8
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationReactionOverlay.this.lambda$getMenuActionItems$3();
                }
            }));
        }
        if (menuState.shouldShowResendAction()) {
            arrayList.add(new ActionItem(R.drawable.ic_retry_24, getResources().getString(R.string.conversation_selection__menu_resend_message), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda9
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationReactionOverlay.this.lambda$getMenuActionItems$4();
                }
            }));
        }
        if (menuState.shouldShowSaveAttachmentAction()) {
            arrayList.add(new ActionItem(R.drawable.ic_save_24_tinted, getResources().getString(R.string.conversation_selection__menu_save), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda10
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationReactionOverlay.this.lambda$getMenuActionItems$5();
                }
            }));
        }
        if (menuState.shouldShowCopyAction()) {
            arrayList.add(new ActionItem(R.drawable.ic_copy_24_tinted, getResources().getString(R.string.conversation_selection__menu_copy), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda11
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationReactionOverlay.this.lambda$getMenuActionItems$6();
                }
            }));
        }
        arrayList.add(new ActionItem(R.drawable.ic_select_24_tinted, getResources().getString(R.string.conversation_selection__menu_multi_select), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda12
            @Override // java.lang.Runnable
            public final void run() {
                ConversationReactionOverlay.this.lambda$getMenuActionItems$7();
            }
        }));
        if (menuState.shouldShowDetailsAction()) {
            arrayList.add(new ActionItem(R.drawable.ic_info_tinted_24, getResources().getString(R.string.conversation_selection__menu_message_details), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda13
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationReactionOverlay.this.lambda$getMenuActionItems$8();
                }
            }));
        }
        this.backgroundView.setVisibility(menuState.shouldShowReactions() ? 0 : 4);
        ConstraintLayout constraintLayout = this.foregroundView;
        if (!menuState.shouldShowReactions()) {
            i = 4;
        }
        constraintLayout.setVisibility(i);
        arrayList.add(new ActionItem(R.drawable.ic_delete_tinted_24, getResources().getString(R.string.conversation_selection__menu_delete), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda14
            @Override // java.lang.Runnable
            public final void run() {
                ConversationReactionOverlay.this.lambda$getMenuActionItems$9();
            }
        }));
        return arrayList;
    }

    public /* synthetic */ void lambda$getMenuActionItems$2() {
        handleActionItemClicked(Action.REPLY);
    }

    public /* synthetic */ void lambda$getMenuActionItems$3() {
        handleActionItemClicked(Action.FORWARD);
    }

    public /* synthetic */ void lambda$getMenuActionItems$4() {
        handleActionItemClicked(Action.RESEND);
    }

    public /* synthetic */ void lambda$getMenuActionItems$5() {
        handleActionItemClicked(Action.DOWNLOAD);
    }

    public /* synthetic */ void lambda$getMenuActionItems$6() {
        handleActionItemClicked(Action.COPY);
    }

    public /* synthetic */ void lambda$getMenuActionItems$7() {
        handleActionItemClicked(Action.MULTISELECT);
    }

    public /* synthetic */ void lambda$getMenuActionItems$8() {
        handleActionItemClicked(Action.VIEW_INFO);
    }

    public /* synthetic */ void lambda$getMenuActionItems$9() {
        handleActionItemClicked(Action.DELETE);
    }

    private void handleActionItemClicked(final Action action) {
        hideInternal(new OnHideListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.2
            @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnHideListener
            public void startHide() {
                if (ConversationReactionOverlay.this.onHideListener != null) {
                    ConversationReactionOverlay.this.onHideListener.startHide();
                }
            }

            @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnHideListener
            public void onHide() {
                if (ConversationReactionOverlay.this.onHideListener != null) {
                    ConversationReactionOverlay.this.onHideListener.onHide();
                }
                if (ConversationReactionOverlay.this.onActionSelectedListener != null) {
                    ConversationReactionOverlay.this.onActionSelectedListener.onActionSelected(action);
                }
            }
        });
    }

    private void initAnimators() {
        int integer = getContext().getResources().getInteger(R.integer.reaction_scrubber_reveal_duration);
        int integer2 = getContext().getResources().getInteger(R.integer.reaction_scrubber_reveal_offset);
        List list = Stream.of(this.emojiViews).mapIndexed(new IndexedFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.IndexedFunction
            public final Object apply(int i, Object obj) {
                return ConversationReactionOverlay.this.lambda$initAnimators$10(i, (EmojiImageView) obj);
            }
        }).toList();
        Animator loadAnimator = AnimatorInflaterCompat.loadAnimator(getContext(), 17498112);
        loadAnimator.setTarget(this.backgroundView);
        long j = (long) integer;
        loadAnimator.setDuration(j);
        long j2 = (long) integer2;
        loadAnimator.setStartDelay(j2);
        list.add(loadAnimator);
        Animator loadAnimator2 = AnimatorInflaterCompat.loadAnimator(getContext(), 17498112);
        loadAnimator2.setTarget(this.selectedView);
        loadAnimator.setDuration(j);
        loadAnimator.setStartDelay(j2);
        list.add(loadAnimator2);
        this.revealAnimatorSet.setInterpolator(INTERPOLATOR);
        this.revealAnimatorSet.playTogether(list);
    }

    public /* synthetic */ Animator lambda$initAnimators$10(int i, EmojiImageView emojiImageView) {
        Animator loadAnimator = AnimatorInflaterCompat.loadAnimator(getContext(), R.animator.reactions_scrubber_reveal);
        loadAnimator.setTarget(emojiImageView);
        loadAnimator.setStartDelay((long) (i * this.animationEmojiStartDelayFactor));
        return loadAnimator;
    }

    private AnimatorSet newHideAnimatorSet() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.addListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.3
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                ConversationReactionOverlay.this.setVisibility(8);
            }
        });
        animatorSet.setInterpolator(INTERPOLATOR);
        animatorSet.playTogether(newHideAnimators());
        return animatorSet;
    }

    private List<Animator> newHideAnimators() {
        Activity activity;
        int integer = getContext().getResources().getInteger(R.integer.reaction_scrubber_hide_duration);
        ArrayList arrayList = new ArrayList(Stream.of(this.emojiViews).mapIndexed(new IndexedFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.IndexedFunction
            public final Object apply(int i, Object obj) {
                return ConversationReactionOverlay.this.lambda$newHideAnimators$11(i, (EmojiImageView) obj);
            }
        }).toList());
        Animator loadAnimator = AnimatorInflaterCompat.loadAnimator(getContext(), 17498113);
        loadAnimator.setTarget(this.backgroundView);
        long j = (long) integer;
        loadAnimator.setDuration(j);
        arrayList.add(loadAnimator);
        Animator loadAnimator2 = AnimatorInflaterCompat.loadAnimator(getContext(), 17498113);
        loadAnimator2.setTarget(this.selectedView);
        loadAnimator2.setDuration(j);
        arrayList.add(loadAnimator2);
        ObjectAnimator objectAnimator = new ObjectAnimator();
        objectAnimator.setProperty(View.SCALE_X);
        objectAnimator.setFloatValues(1.0f);
        objectAnimator.setTarget(this.conversationItem);
        objectAnimator.setDuration(j);
        arrayList.add(objectAnimator);
        ObjectAnimator objectAnimator2 = new ObjectAnimator();
        objectAnimator2.setProperty(View.SCALE_Y);
        objectAnimator2.setFloatValues(1.0f);
        objectAnimator2.setTarget(this.conversationItem);
        objectAnimator2.setDuration(j);
        arrayList.add(objectAnimator2);
        ObjectAnimator objectAnimator3 = new ObjectAnimator();
        objectAnimator3.setProperty(View.X);
        objectAnimator3.setFloatValues(this.selectedConversationModel.getBubbleX());
        objectAnimator3.setTarget(this.conversationItem);
        objectAnimator3.setDuration(j);
        arrayList.add(objectAnimator3);
        ObjectAnimator objectAnimator4 = new ObjectAnimator();
        objectAnimator4.setProperty(View.Y);
        objectAnimator4.setFloatValues((this.selectedConversationModel.getItemY() + this.selectedConversationModel.getBubbleY()) - ((float) this.statusBarHeight));
        objectAnimator4.setTarget(this.conversationItem);
        objectAnimator4.setDuration(j);
        arrayList.add(objectAnimator4);
        ObjectAnimator objectAnimator5 = new ObjectAnimator();
        objectAnimator5.setProperty(View.ALPHA);
        objectAnimator5.setFloatValues(0.0f);
        objectAnimator5.setTarget(this.toolbarShade);
        objectAnimator5.setDuration(j);
        arrayList.add(objectAnimator5);
        ObjectAnimator objectAnimator6 = new ObjectAnimator();
        objectAnimator6.setProperty(View.ALPHA);
        objectAnimator6.setFloatValues(0.0f);
        objectAnimator6.setTarget(this.inputShade);
        objectAnimator6.setDuration(j);
        arrayList.add(objectAnimator6);
        if (Build.VERSION.SDK_INT >= 21 && (activity = this.activity) != null) {
            ValueAnimator ofArgb = ValueAnimator.ofArgb(activity.getWindow().getStatusBarColor(), this.originalStatusBarColor);
            ofArgb.setDuration(j);
            ofArgb.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda5
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ConversationReactionOverlay.this.lambda$newHideAnimators$12(valueAnimator);
                }
            });
            arrayList.add(ofArgb);
            ValueAnimator ofArgb2 = ValueAnimator.ofArgb(this.activity.getWindow().getStatusBarColor(), this.originalNavigationBarColor);
            ofArgb2.setDuration(j);
            ofArgb2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationReactionOverlay$$ExternalSyntheticLambda6
                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ConversationReactionOverlay.this.lambda$newHideAnimators$13(valueAnimator);
                }
            });
            arrayList.add(ofArgb2);
        }
        return arrayList;
    }

    public /* synthetic */ Animator lambda$newHideAnimators$11(int i, EmojiImageView emojiImageView) {
        Animator loadAnimator = AnimatorInflaterCompat.loadAnimator(getContext(), R.animator.reactions_scrubber_hide);
        loadAnimator.setTarget(emojiImageView);
        return loadAnimator;
    }

    public /* synthetic */ void lambda$newHideAnimators$12(ValueAnimator valueAnimator) {
        WindowUtil.setStatusBarColor(this.activity.getWindow(), ((Integer) valueAnimator.getAnimatedValue()).intValue());
    }

    public /* synthetic */ void lambda$newHideAnimators$13(ValueAnimator valueAnimator) {
        WindowUtil.setNavigationBarColor(this.activity, ((Integer) valueAnimator.getAnimatedValue()).intValue());
    }

    /* loaded from: classes4.dex */
    public static class Boundary {
        private float max;
        private float min;

        Boundary() {
        }

        Boundary(float f, float f2) {
            update(f, f2);
        }

        public void update(float f, float f2) {
            this.min = f;
            this.max = f2;
        }

        public boolean contains(float f) {
            float f2 = this.min;
            float f3 = this.max;
            return f2 < f3 ? f2 < f && f3 > f : f2 > f && f3 < f;
        }
    }
}
