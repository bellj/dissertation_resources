package org.thoughtcrime.securesms.conversation.colors;

import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;

/* compiled from: ChatColorsPalette.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColorsPalette;", "", "()V", "UNKNOWN_CONTACT", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "Bubbles", "Names", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorsPalette {
    public static final ChatColorsPalette INSTANCE = new ChatColorsPalette();
    public static final ChatColors UNKNOWN_CONTACT = Bubbles.STEEL;

    /* compiled from: ChatColorsPalette.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010 \n\u0002\b\u000b\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0015\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0018\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00040\u001b¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u001c\u0010\u001e\u001a\u00020\u00048\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u001f\u0010\u0002\u001a\u0004\b \u0010!R\u0017\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00040\u001b¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001dR\u0017\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00040\u001b¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001d¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColorsPalette$Bubbles;", "", "()V", "BASIL", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "BLUE", "BURLAP", "CRIMSON", "EMBER", "FLUORESCENT", "FOREST", "INDIGO", "INFRARED", "LAGOON", "MIDNIGHT", "PLUM", "SEA", "STEEL", "SUBLIME", "TANGERINE", "TAUPE", "TEAL", "ULTRAMARINE", "VERMILION", "VIOLET", "WINTERGREEN", "all", "", "getAll", "()Ljava/util/List;", "default", "getDefault$annotations", "getDefault", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "gradients", "getGradients", "solids", "getSolids", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Bubbles {
        public static final ChatColors BASIL;
        public static final ChatColors BLUE;
        public static final ChatColors BURLAP;
        public static final ChatColors CRIMSON;
        public static final ChatColors EMBER;
        public static final ChatColors FLUORESCENT;
        public static final ChatColors FOREST;
        public static final ChatColors INDIGO;
        public static final ChatColors INFRARED;
        public static final Bubbles INSTANCE = new Bubbles();
        public static final ChatColors LAGOON;
        public static final ChatColors MIDNIGHT;
        public static final ChatColors PLUM;
        public static final ChatColors SEA;
        public static final ChatColors STEEL;
        public static final ChatColors SUBLIME;
        public static final ChatColors TANGERINE;
        public static final ChatColors TAUPE;
        public static final ChatColors TEAL;
        public static final ChatColors ULTRAMARINE;
        public static final ChatColors VERMILION;
        public static final ChatColors VIOLET;
        public static final ChatColors WINTERGREEN;
        private static final List<ChatColors> all;

        /* renamed from: default */
        private static final ChatColors f3default;
        private static final List<ChatColors> gradients;
        private static final List<ChatColors> solids;

        @JvmStatic
        public static /* synthetic */ void getDefault$annotations() {
        }

        private Bubbles() {
        }

        static {
            INSTANCE = new Bubbles();
            ChatColors.Companion companion = ChatColors.Companion;
            ChatColors.Id.BuiltIn builtIn = ChatColors.Id.BuiltIn.INSTANCE;
            ChatColors forColor = companion.forColor(builtIn, -13541388);
            ULTRAMARINE = forColor;
            ChatColors forColor2 = companion.forColor(builtIn, -3205570);
            CRIMSON = forColor2;
            ChatColors forColor3 = companion.forColor(builtIn, -3719414);
            VERMILION = forColor3;
            ChatColors forColor4 = companion.forColor(builtIn, -9475496);
            BURLAP = forColor4;
            ChatColors forColor5 = companion.forColor(builtIn, -12879803);
            FOREST = forColor5;
            ChatColors forColor6 = companion.forColor(builtIn, -14842269);
            WINTERGREEN = forColor6;
            ChatColors forColor7 = companion.forColor(builtIn, -16286318);
            TEAL = forColor7;
            ChatColors forColor8 = companion.forColor(builtIn, -13407325);
            BLUE = forColor8;
            ChatColors forColor9 = companion.forColor(builtIn, -10463030);
            INDIGO = forColor9;
            ChatColors forColor10 = companion.forColor(builtIn, -6737205);
            VIOLET = forColor10;
            ChatColors forColor11 = companion.forColor(builtIn, -5621894);
            PLUM = forColor11;
            ChatColors forColor12 = companion.forColor(builtIn, -7380630);
            TAUPE = forColor12;
            ChatColors forColor13 = companion.forColor(builtIn, -9342593);
            STEEL = forColor13;
            ChatColors forGradient = companion.forGradient(builtIn, new ChatColors.LinearGradient(168.0f, new int[]{-1737728, -10616832}, new float[]{0.0f, 1.0f}));
            EMBER = forGradient;
            ChatColors forGradient2 = companion.forGradient(builtIn, new ChatColors.LinearGradient(180.0f, new int[]{-13882310, -8882031}, new float[]{0.0f, 1.0f}));
            MIDNIGHT = forGradient2;
            ChatColors forGradient3 = companion.forGradient(builtIn, new ChatColors.LinearGradient(192.0f, new int[]{-633504, -12309267}, new float[]{0.0f, 1.0f}));
            INFRARED = forGradient3;
            ChatColors forGradient4 = companion.forGradient(builtIn, new ChatColors.LinearGradient(180.0f, new int[]{-16760730, -13465987}, new float[]{0.0f, 1.0f}));
            LAGOON = forGradient4;
            ChatColors forGradient5 = companion.forGradient(builtIn, new ChatColors.LinearGradient(192.0f, new int[]{-1305635, -14993722}, new float[]{0.0f, 1.0f}));
            FLUORESCENT = forGradient5;
            ChatColors forGradient6 = companion.forGradient(builtIn, new ChatColors.LinearGradient(180.0f, new int[]{-13659277, -16288957}, new float[]{0.0f, 1.0f}));
            BASIL = forGradient6;
            ChatColors forGradient7 = companion.forGradient(builtIn, new ChatColors.LinearGradient(180.0f, new int[]{-10321451, -6863776}, new float[]{0.0f, 1.0f}));
            SUBLIME = forGradient7;
            ChatColors forGradient8 = companion.forGradient(builtIn, new ChatColors.LinearGradient(180.0f, new int[]{-11956268, -13867360}, new float[]{0.0f, 1.0f}));
            SEA = forGradient8;
            ChatColors forGradient9 = companion.forGradient(builtIn, new ChatColors.LinearGradient(192.0f, new int[]{-2395853, -7269839}, new float[]{0.0f, 1.0f}));
            TANGERINE = forGradient9;
            f3default = forColor;
            List<ChatColors> list = CollectionsKt__CollectionsKt.listOf((Object[]) new ChatColors[]{forColor2, forColor3, forColor4, forColor5, forColor6, forColor7, forColor8, forColor9, forColor10, forColor11, forColor12, forColor13});
            solids = list;
            List<ChatColors> list2 = CollectionsKt__CollectionsKt.listOf((Object[]) new ChatColors[]{forGradient, forGradient2, forGradient3, forGradient4, forGradient5, forGradient6, forGradient7, forGradient8, forGradient9});
            gradients = list2;
            all = CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(forColor), (Iterable) list), (Iterable) list2);
        }

        public static final ChatColors getDefault() {
            return f3default;
        }

        public final List<ChatColors> getSolids() {
            return solids;
        }

        public final List<ChatColors> getGradients() {
            return gradients;
        }

        public final List<ChatColors> getAll() {
            return all;
        }
    }

    private ChatColorsPalette() {
    }

    /* compiled from: ChatColorsPalette.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0006\u0010\u0002\u001a\u0004\b\u0007\u0010\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColorsPalette$Names;", "", "()V", "all", "", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "getAll$annotations", "getAll", "()Ljava/util/List;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Names {
        public static final Names INSTANCE = new Names();
        private static final List<NameColor> all = CollectionsKt__CollectionsKt.listOf((Object[]) new NameColor[]{new NameColor(-16749149, -16734214), new NameColor(-16353018, -16074742), new NameColor(-4713288, -632074), new NameColor(-4115947, -37038), new NameColor(-10786442, -7626314), new NameColor(-3407770, -561486), new NameColor(-13741569, -8021505), new NameColor(-16747147, -16731470), new NameColor(-6531311, -2780661), new NameColor(-3142835, -37988), new NameColor(-7394572, -4226817), new NameColor(-12749818, -10570999), new NameColor(-3142901, -36752), new NameColor(-16745923, -16730020), new NameColor(-11447818, -7039745), new NameColor(-7970536, -2715904), new NameColor(-16352941, -16729990), new NameColor(-6157075, -3179272), new NameColor(-11833344, -9130752), new NameColor(-3732856, -561463), new NameColor(-5029367, -689603), new NameColor(-16352979, -16074684), new NameColor(-8765963, -5272839), new NameColor(-9737436, -5987273), new NameColor(-3142868, -560247), new NameColor(-13797114, -12406007), new NameColor(-5305392, -2068745), new NameColor(-13470146, -11817124), new NameColor(-14261543, -8543768), new NameColor(-9017314, -4678902), new NameColor(-16354206, -16141417), new NameColor(-10205195, -6189063), new NameColor(-10588660, -7362039), new NameColor(-16289144, -16732463), new NameColor(-4060509, -565283), new NameColor(-13797858, -12340179)});

        @JvmStatic
        public static /* synthetic */ void getAll$annotations() {
        }

        private Names() {
        }

        public static final List<NameColor> getAll() {
            return all;
        }
    }
}
