package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Lambda;

/* compiled from: MultiselectForwardFragment.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\n\u0010\u0002\u001a\u00060\u0001j\u0002`\u0003H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/contacts/paged/ActiveContactCount;", "invoke", "(I)Ljava/lang/Integer;"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardFragment$getConfiguration$1$expandedConfig$1 extends Lambda implements Function1<Integer, Integer> {
    public static final MultiselectForwardFragment$getConfiguration$1$expandedConfig$1 INSTANCE = new MultiselectForwardFragment$getConfiguration$1$expandedConfig$1();

    MultiselectForwardFragment$getConfiguration$1$expandedConfig$1() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Integer invoke(Integer num) {
        return invoke(num.intValue());
    }

    public final Integer invoke(int i) {
        return Integer.valueOf(i + 1);
    }
}
