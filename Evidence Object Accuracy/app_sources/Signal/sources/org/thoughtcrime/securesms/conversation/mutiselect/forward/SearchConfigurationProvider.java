package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import androidx.fragment.app.FragmentManager;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchState;

/* compiled from: SearchConfigurationProvider.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u001a\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/SearchConfigurationProvider;", "", "getSearchConfiguration", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "contactSearchState", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface SearchConfigurationProvider {

    /* compiled from: SearchConfigurationProvider.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DefaultImpls {
        public static ContactSearchConfiguration getSearchConfiguration(SearchConfigurationProvider searchConfigurationProvider, FragmentManager fragmentManager, ContactSearchState contactSearchState) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            Intrinsics.checkNotNullParameter(contactSearchState, "contactSearchState");
            return null;
        }
    }

    ContactSearchConfiguration getSearchConfiguration(FragmentManager fragmentManager, ContactSearchState contactSearchState);
}
