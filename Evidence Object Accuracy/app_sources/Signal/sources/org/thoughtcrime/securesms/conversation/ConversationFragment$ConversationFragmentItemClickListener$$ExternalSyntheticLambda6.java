package org.thoughtcrime.securesms.conversation;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda6 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda6(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener) {
        this.f$0 = conversationFragmentItemClickListener;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.lambda$onQuoteClicked$2((Integer) obj);
    }
}
