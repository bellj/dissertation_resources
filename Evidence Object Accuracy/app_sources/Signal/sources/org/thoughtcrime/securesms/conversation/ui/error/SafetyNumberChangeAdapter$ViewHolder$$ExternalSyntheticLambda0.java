package org.thoughtcrime.securesms.conversation.ui.error;

import android.view.View;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SafetyNumberChangeAdapter$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ SafetyNumberChangeAdapter.ViewHolder f$0;
    public final /* synthetic */ ChangedRecipient f$1;

    public /* synthetic */ SafetyNumberChangeAdapter$ViewHolder$$ExternalSyntheticLambda0(SafetyNumberChangeAdapter.ViewHolder viewHolder, ChangedRecipient changedRecipient) {
        this.f$0 = viewHolder;
        this.f$1 = changedRecipient;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
