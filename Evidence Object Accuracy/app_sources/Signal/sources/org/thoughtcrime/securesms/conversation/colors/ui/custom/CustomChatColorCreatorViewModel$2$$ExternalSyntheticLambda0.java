package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CustomChatColorCreatorViewModel$2$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ ChatColors f$0;
    public final /* synthetic */ ColorSlidersState f$1;
    public final /* synthetic */ ColorSlidersState f$2;

    public /* synthetic */ CustomChatColorCreatorViewModel$2$$ExternalSyntheticLambda0(ChatColors chatColors, ColorSlidersState colorSlidersState, ColorSlidersState colorSlidersState2) {
        this.f$0 = chatColors;
        this.f$1 = colorSlidersState;
        this.f$2 = colorSlidersState2;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return CustomChatColorCreatorViewModel.AnonymousClass2.m1553invoke$lambda0(this.f$0, this.f$1, this.f$2, (CustomChatColorCreatorState) obj);
    }
}
