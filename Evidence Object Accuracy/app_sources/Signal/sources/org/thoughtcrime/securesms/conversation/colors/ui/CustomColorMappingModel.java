package org.thoughtcrime.securesms.conversation.colors.ui;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: CustomColorMappingModel.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/CustomColorMappingModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "()V", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomColorMappingModel implements MappingModel<CustomColorMappingModel> {
    public boolean areContentsTheSame(CustomColorMappingModel customColorMappingModel) {
        Intrinsics.checkNotNullParameter(customColorMappingModel, "newItem");
        return true;
    }

    public boolean areItemsTheSame(CustomColorMappingModel customColorMappingModel) {
        Intrinsics.checkNotNullParameter(customColorMappingModel, "newItem");
        return true;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(CustomColorMappingModel customColorMappingModel) {
        return MappingModel.CC.$default$getChangePayload(this, customColorMappingModel);
    }
}
