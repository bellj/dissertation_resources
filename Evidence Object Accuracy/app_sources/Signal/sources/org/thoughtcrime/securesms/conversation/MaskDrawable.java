package org.thoughtcrime.securesms.conversation;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;

/* loaded from: classes4.dex */
public final class MaskDrawable extends Drawable {
    private final RectF bounds = new RectF();
    private final Path clipPath = new Path();
    private float[] clipPathRadii;
    private Rect clipRect;
    private final Drawable wrapped;

    public MaskDrawable(Drawable drawable) {
        this.wrapped = drawable;
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        if (this.clipRect == null) {
            this.wrapped.draw(canvas);
            return;
        }
        canvas.save();
        if (this.clipPathRadii != null) {
            this.clipPath.reset();
            this.bounds.set(this.clipRect);
            this.clipPath.addRoundRect(this.bounds, this.clipPathRadii, Path.Direction.CW);
            canvas.clipPath(this.clipPath, Region.Op.DIFFERENCE);
        } else {
            canvas.clipRect(this.clipRect, Region.Op.DIFFERENCE);
        }
        this.wrapped.draw(canvas);
        canvas.restore();
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
        this.wrapped.setAlpha(i);
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
        this.wrapped.setColorFilter(colorFilter);
    }

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return this.wrapped.getOpacity();
    }

    @Override // android.graphics.drawable.Drawable
    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        this.wrapped.setBounds(i, i2, i3, i4);
    }

    @Override // android.graphics.drawable.Drawable
    public boolean getPadding(Rect rect) {
        return this.wrapped.getPadding(rect);
    }

    public void setMask(Rect rect) {
        this.clipRect = new Rect(rect);
        invalidateSelf();
    }

    public void setCorners(float[] fArr) {
        this.clipPathRadii = fArr;
        invalidateSelf();
    }
}
