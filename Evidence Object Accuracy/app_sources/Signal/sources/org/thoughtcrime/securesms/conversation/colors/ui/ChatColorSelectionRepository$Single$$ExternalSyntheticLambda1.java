package org.thoughtcrime.securesms.conversation.colors.ui;

import kotlin.jvm.functions.Function1;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ChatColorSelectionRepository$Single$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ChatColorSelectionRepository.Single f$0;
    public final /* synthetic */ Function1 f$1;

    public /* synthetic */ ChatColorSelectionRepository$Single$$ExternalSyntheticLambda1(ChatColorSelectionRepository.Single single, Function1 function1) {
        this.f$0 = single;
        this.f$1 = function1;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ChatColorSelectionRepository.Single.m1513getWallpaper$lambda0(this.f$0, this.f$1);
    }
}
