package org.thoughtcrime.securesms.conversation;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import j$.util.function.Function;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.v2.GroupBlockJoinRequestResult;
import org.thoughtcrime.securesms.groups.v2.GroupManagementRepository;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewRecipient;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewUtil;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public final class ConversationGroupViewModel extends ViewModel {
    private final LiveData<Integer> actionableRequestingMembers;
    private boolean firstTimeInviteFriendsTriggered;
    private final LiveData<GroupActiveState> groupActiveState;
    private final GroupManagementRepository groupManagementRepository;
    private final LiveData<List<RecipientId>> gv1MigrationSuggestions;
    private final MutableLiveData<Recipient> liveRecipient;
    private final LiveData<ReviewState> reviewState;
    private final LiveData<ConversationMemberLevel> selfMembershipLevel;

    private ConversationGroupViewModel() {
        MutableLiveData<Recipient> mutableLiveData = new MutableLiveData<>();
        this.liveRecipient = mutableLiveData;
        this.groupManagementRepository = new GroupManagementRepository();
        LiveData mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationGroupViewModel.getGroupRecordForRecipient((Recipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        LiveData mapAsync2 = LiveDataUtil.mapAsync(mapAsync, new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationGroupViewModel.lambda$new$0((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        this.groupActiveState = Transformations.distinctUntilChanged(Transformations.map(mapAsync, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda7
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConversationGroupViewModel.mapToGroupActiveState((GroupDatabase.GroupRecord) obj);
            }
        }));
        this.selfMembershipLevel = Transformations.distinctUntilChanged(Transformations.map(mapAsync, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda8
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConversationGroupViewModel.mapToSelfMembershipLevel((GroupDatabase.GroupRecord) obj);
            }
        }));
        this.actionableRequestingMembers = Transformations.distinctUntilChanged(Transformations.map(mapAsync, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda9
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Integer.valueOf(ConversationGroupViewModel.mapToActionableRequestingMemberCount((GroupDatabase.GroupRecord) obj));
            }
        }));
        this.gv1MigrationSuggestions = Transformations.distinctUntilChanged(LiveDataUtil.mapAsync(mapAsync, new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda10
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationGroupViewModel.mapToGroupV1MigrationSuggestions((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }));
        this.reviewState = LiveDataUtil.combineLatest(mapAsync, mapAsync2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda11
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ConversationGroupViewModel.lambda$new$1((GroupDatabase.GroupRecord) obj, (List) obj2);
            }
        });
    }

    public static /* synthetic */ List lambda$new$0(GroupDatabase.GroupRecord groupRecord) {
        if (groupRecord == null || !groupRecord.isV2Group()) {
            return Collections.emptyList();
        }
        return Stream.of(ReviewUtil.getDuplicatedRecipients(groupRecord.getId().requireV2())).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((ReviewRecipient) obj).getRecipient();
            }
        }).toList();
    }

    public static /* synthetic */ ReviewState lambda$new$1(GroupDatabase.GroupRecord groupRecord, List list) {
        if (list.isEmpty()) {
            return ReviewState.EMPTY;
        }
        return new ReviewState(groupRecord.getId().requireV2(), (Recipient) list.get(0), list.size());
    }

    public void onRecipientChange(Recipient recipient) {
        this.liveRecipient.setValue(recipient);
    }

    public void onSuggestedMembersBannerDismissed(GroupId groupId, List<RecipientId> list) {
        SignalExecutors.BOUNDED.execute(new Runnable(groupId, list) { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda12
            public final /* synthetic */ GroupId f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationGroupViewModel.this.lambda$onSuggestedMembersBannerDismissed$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onSuggestedMembersBannerDismissed$2(GroupId groupId, List list) {
        if (groupId.isV2()) {
            SignalDatabase.groups().removeUnmigratedV1Members(groupId.requireV2(), list);
            MutableLiveData<Recipient> mutableLiveData = this.liveRecipient;
            mutableLiveData.postValue(mutableLiveData.getValue());
        }
    }

    public LiveData<Integer> getActionableRequestingMembers() {
        return this.actionableRequestingMembers;
    }

    public LiveData<GroupActiveState> getGroupActiveState() {
        return this.groupActiveState;
    }

    public LiveData<ConversationMemberLevel> getSelfMemberLevel() {
        return this.selfMembershipLevel;
    }

    public LiveData<ReviewState> getReviewState() {
        return this.reviewState;
    }

    public LiveData<List<RecipientId>> getGroupV1MigrationSuggestions() {
        return this.gv1MigrationSuggestions;
    }

    public boolean isNonAdminInAnnouncementGroup() {
        ConversationMemberLevel value = this.selfMembershipLevel.getValue();
        return (value == null || value.getMemberLevel() == GroupDatabase.MemberLevel.ADMINISTRATOR || !value.isAnnouncementGroup()) ? false : true;
    }

    public static GroupDatabase.GroupRecord getGroupRecordForRecipient(Recipient recipient) {
        if (recipient == null || !recipient.isGroup()) {
            return null;
        }
        ApplicationDependencies.getApplication();
        return SignalDatabase.groups().getGroup(recipient.getId()).orElse(null);
    }

    public static int mapToActionableRequestingMemberCount(GroupDatabase.GroupRecord groupRecord) {
        if (groupRecord == null || !groupRecord.isV2Group() || groupRecord.memberLevel(Recipient.self()) != GroupDatabase.MemberLevel.ADMINISTRATOR) {
            return 0;
        }
        return groupRecord.requireV2GroupProperties().getDecryptedGroup().getRequestingMembersCount();
    }

    public static GroupActiveState mapToGroupActiveState(GroupDatabase.GroupRecord groupRecord) {
        if (groupRecord == null) {
            return null;
        }
        return new GroupActiveState(groupRecord.isActive(), groupRecord.isV2Group());
    }

    public static ConversationMemberLevel mapToSelfMembershipLevel(GroupDatabase.GroupRecord groupRecord) {
        if (groupRecord == null) {
            return null;
        }
        return new ConversationMemberLevel(groupRecord.memberLevel(Recipient.self()), groupRecord.isAnnouncementGroup());
    }

    public static List<RecipientId> mapToGroupV1MigrationSuggestions(GroupDatabase.GroupRecord groupRecord) {
        if (groupRecord == null) {
            return Collections.emptyList();
        }
        if (!groupRecord.isV2Group()) {
            return Collections.emptyList();
        }
        if (!groupRecord.isActive() || groupRecord.isPendingMember(Recipient.self())) {
            return Collections.emptyList();
        }
        return Stream.of(groupRecord.getUnmigratedV1Members()).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationGroupViewModel.lambda$mapToGroupV1MigrationSuggestions$3(GroupDatabase.GroupRecord.this, (RecipientId) obj);
            }
        }).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupsV1MigrationUtil.isAutoMigratable((Recipient) obj);
            }
        }).map(new ContactUtil$$ExternalSyntheticLambda3()).toList();
    }

    public static /* synthetic */ boolean lambda$mapToGroupV1MigrationSuggestions$3(GroupDatabase.GroupRecord groupRecord, RecipientId recipientId) {
        return groupRecord.getMembers().contains(recipientId);
    }

    public static void onCancelJoinRequest(Recipient recipient, AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(workerThread) { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda13
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationGroupViewModel.lambda$onCancelJoinRequest$4(Recipient.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$onCancelJoinRequest$4(Recipient recipient, AsynchronousCallback.WorkerThread workerThread) {
        if (recipient.isPushV2Group()) {
            try {
                GroupManager.cancelJoinRequest(ApplicationDependencies.getApplication(), recipient.getGroupId().get().requireV2());
                workerThread.onComplete(null);
            } catch (IOException | GroupChangeBusyException | GroupChangeFailedException e) {
                workerThread.onError(GroupChangeFailureReason.fromException(e));
            }
        } else {
            throw new AssertionError();
        }
    }

    public void inviteFriendsOneTimeIfJustSelfInGroup(FragmentManager fragmentManager, GroupId.V2 v2) {
        if (!this.firstTimeInviteFriendsTriggered) {
            this.firstTimeInviteFriendsTriggered = true;
            SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda3
                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return ConversationGroupViewModel.lambda$inviteFriendsOneTimeIfJustSelfInGroup$5(GroupId.V2.this);
                }
            }, new SimpleTask.ForegroundTask(fragmentManager, v2) { // from class: org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$$ExternalSyntheticLambda4
                public final /* synthetic */ FragmentManager f$1;
                public final /* synthetic */ GroupId.V2 f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    ConversationGroupViewModel.this.lambda$inviteFriendsOneTimeIfJustSelfInGroup$6(this.f$1, this.f$2, (Boolean) obj);
                }
            });
        }
    }

    public static /* synthetic */ Boolean lambda$inviteFriendsOneTimeIfJustSelfInGroup$5(GroupId.V2 v2) {
        return Boolean.valueOf(SignalDatabase.groups().requireGroup(v2).getMembers().equals(Collections.singletonList(Recipient.self().getId())));
    }

    public /* synthetic */ void lambda$inviteFriendsOneTimeIfJustSelfInGroup$6(FragmentManager fragmentManager, GroupId.V2 v2, Boolean bool) {
        if (bool.booleanValue()) {
            inviteFriends(fragmentManager, v2);
        }
    }

    void inviteFriends(FragmentManager fragmentManager, GroupId.V2 v2) {
        GroupLinkInviteFriendsBottomSheetDialogFragment.show(fragmentManager, v2);
    }

    public Single<GroupBlockJoinRequestResult> blockJoinRequests(Recipient recipient, Recipient recipient2) {
        return this.groupManagementRepository.blockJoinRequests(recipient.requireGroupId().requireV2(), recipient2).observeOn(AndroidSchedulers.mainThread());
    }

    /* loaded from: classes4.dex */
    public static final class ReviewState {
        private static final ReviewState EMPTY = new ReviewState(null, Recipient.UNKNOWN, 0);
        private final int count;
        private final GroupId.V2 groupId;
        private final Recipient recipient;

        ReviewState(GroupId.V2 v2, Recipient recipient, int i) {
            this.groupId = v2;
            this.recipient = recipient;
            this.count = i;
        }

        public GroupId.V2 getGroupId() {
            return this.groupId;
        }

        public Recipient getRecipient() {
            return this.recipient;
        }

        public int getCount() {
            return this.count;
        }
    }

    /* loaded from: classes4.dex */
    public static final class GroupActiveState {
        private final boolean isActive;
        private final boolean isActiveV2;

        public GroupActiveState(boolean z, boolean z2) {
            this.isActive = z;
            this.isActiveV2 = z && z2;
        }

        public boolean isActiveGroup() {
            return this.isActive;
        }

        public boolean isActiveV2Group() {
            return this.isActiveV2;
        }
    }

    /* loaded from: classes4.dex */
    public static final class ConversationMemberLevel {
        private final boolean isAnnouncementGroup;
        private final GroupDatabase.MemberLevel memberLevel;

        private ConversationMemberLevel(GroupDatabase.MemberLevel memberLevel, boolean z) {
            this.memberLevel = memberLevel;
            this.isAnnouncementGroup = z;
        }

        public GroupDatabase.MemberLevel getMemberLevel() {
            return this.memberLevel;
        }

        public boolean isAnnouncementGroup() {
            return this.isAnnouncementGroup;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ConversationGroupViewModel());
        }
    }
}
