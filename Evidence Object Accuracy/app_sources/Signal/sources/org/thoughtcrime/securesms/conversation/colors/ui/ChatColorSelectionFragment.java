package org.thoughtcrime.securesms.conversation.colors.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragmentDirections;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChatColorSelectionFragment.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0016J\u001a\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000eH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionFragment;", "Landroidx/fragment/app/Fragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel;", "onResume", "", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "showWarningDialogForMultipleUses", "confirmDeletion", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Event$ConfirmDeletion;", "showWarningDialogForNoUses", "Callbacks", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorSelectionFragment extends Fragment {
    private ChatColorSelectionViewModel viewModel;

    public ChatColorSelectionFragment() {
        super(R.layout.chat_color_selection_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        ChatColorSelectionFragmentArgs fromBundle = ChatColorSelectionFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        ChatColorSelectionViewModel.Companion companion = ChatColorSelectionViewModel.Companion;
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        this.viewModel = companion.getOrCreate(requireActivity, fromBundle.getRecipientId());
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        View findViewById2 = view.findViewById(R.id.preview);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.preview)");
        ChatColorPreviewView chatColorPreviewView = (ChatColorPreviewView) findViewById2;
        View findViewById3 = view.findViewById(R.id.recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.recycler)");
        RecyclerView recyclerView = (RecyclerView) findViewById3;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ChatColorSelectionAdapter chatColorSelectionAdapter = new ChatColorSelectionAdapter(requireContext, new Callbacks(this, fromBundle, view));
        ChatColorSelectionViewModel chatColorSelectionViewModel = null;
        recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(chatColorSelectionAdapter);
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChatColorSelectionFragment.m1498$r8$lambda$29WcbZS6TWFDTaa_1S6lKU5_Rk(view2);
            }
        });
        ChatColorSelectionViewModel chatColorSelectionViewModel2 = this.viewModel;
        if (chatColorSelectionViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            chatColorSelectionViewModel2 = null;
        }
        chatColorSelectionViewModel2.getState().observe(getViewLifecycleOwner(), new Observer(chatColorSelectionAdapter) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ ChatColorSelectionAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatColorSelectionFragment.m1500$r8$lambda$TGCrl5ywnaInQfAsPUZxnclS5E(ChatColorPreviewView.this, this.f$1, (ChatColorSelectionState) obj);
            }
        });
        ChatColorSelectionViewModel chatColorSelectionViewModel3 = this.viewModel;
        if (chatColorSelectionViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            chatColorSelectionViewModel = chatColorSelectionViewModel3;
        }
        chatColorSelectionViewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatColorSelectionFragment.$r8$lambda$o6yg7rrkB_bqboI3LWxeQtI5Cck(ChatColorSelectionFragment.this, (ChatColorSelectionViewModel.Event) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1501onViewCreated$lambda0(View view) {
        Navigation.findNavController(view).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1502onViewCreated$lambda1(ChatColorPreviewView chatColorPreviewView, ChatColorSelectionAdapter chatColorSelectionAdapter, ChatColorSelectionState chatColorSelectionState) {
        Intrinsics.checkNotNullParameter(chatColorPreviewView, "$preview");
        Intrinsics.checkNotNullParameter(chatColorSelectionAdapter, "$adapter");
        chatColorPreviewView.setWallpaper(chatColorSelectionState.getWallpaper());
        if (chatColorSelectionState.getChatColors() != null) {
            chatColorPreviewView.setChatColors(chatColorSelectionState.getChatColors());
        }
        chatColorSelectionAdapter.submitList(chatColorSelectionState.getChatColorModels());
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m1503onViewCreated$lambda2(ChatColorSelectionFragment chatColorSelectionFragment, ChatColorSelectionViewModel.Event event) {
        Intrinsics.checkNotNullParameter(chatColorSelectionFragment, "this$0");
        if (event instanceof ChatColorSelectionViewModel.Event.ConfirmDeletion) {
            ChatColorSelectionViewModel.Event.ConfirmDeletion confirmDeletion = (ChatColorSelectionViewModel.Event.ConfirmDeletion) event;
            if (confirmDeletion.getUsageCount() > 0) {
                chatColorSelectionFragment.showWarningDialogForMultipleUses(confirmDeletion);
            } else {
                chatColorSelectionFragment.showWarningDialogForNoUses(confirmDeletion);
            }
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ChatColorSelectionViewModel chatColorSelectionViewModel = this.viewModel;
        if (chatColorSelectionViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            chatColorSelectionViewModel = null;
        }
        chatColorSelectionViewModel.refresh();
    }

    private final void showWarningDialogForNoUses(ChatColorSelectionViewModel.Event.ConfirmDeletion confirmDeletion) {
        new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ChatColorSelectionFragment__delete_chat_color).setPositiveButton(R.string.ChatColorSelectionFragment__delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(confirmDeletion) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ ChatColorSelectionViewModel.Event.ConfirmDeletion f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatColorSelectionFragment.$r8$lambda$QQot3Z4wkcXE2Bj1Mi7wbghSWAI(ChatColorSelectionFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatColorSelectionFragment.m1499$r8$lambda$AQeYwU3gjjrymomc7joTYS2pg(dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: showWarningDialogForNoUses$lambda-3 */
    public static final void m1506showWarningDialogForNoUses$lambda3(ChatColorSelectionFragment chatColorSelectionFragment, ChatColorSelectionViewModel.Event.ConfirmDeletion confirmDeletion, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(chatColorSelectionFragment, "this$0");
        Intrinsics.checkNotNullParameter(confirmDeletion, "$confirmDeletion");
        ChatColorSelectionViewModel chatColorSelectionViewModel = chatColorSelectionFragment.viewModel;
        if (chatColorSelectionViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            chatColorSelectionViewModel = null;
        }
        chatColorSelectionViewModel.deleteNow(confirmDeletion.getChatColors());
        dialogInterface.dismiss();
    }

    /* renamed from: showWarningDialogForNoUses$lambda-4 */
    public static final void m1507showWarningDialogForNoUses$lambda4(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    private final void showWarningDialogForMultipleUses(ChatColorSelectionViewModel.Event.ConfirmDeletion confirmDeletion) {
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.ChatColorSelectionFragment__delete_color).setMessage((CharSequence) getResources().getQuantityString(R.plurals.ChatColorSelectionFragment__this_custom_color_is_used, confirmDeletion.getUsageCount(), Integer.valueOf(confirmDeletion.getUsageCount()))).setPositiveButton(R.string.delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(confirmDeletion) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ ChatColorSelectionViewModel.Event.ConfirmDeletion f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatColorSelectionFragment.m1497$r8$lambda$DRQkNaCieVRukdgxo22JYWA7V4(ChatColorSelectionFragment.this, this.f$1, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionFragment$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChatColorSelectionFragment.$r8$lambda$1JNvx4xohX5uQASR_bgjz9QVajE(dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: showWarningDialogForMultipleUses$lambda-5 */
    public static final void m1504showWarningDialogForMultipleUses$lambda5(ChatColorSelectionFragment chatColorSelectionFragment, ChatColorSelectionViewModel.Event.ConfirmDeletion confirmDeletion, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(chatColorSelectionFragment, "this$0");
        Intrinsics.checkNotNullParameter(confirmDeletion, "$confirmDeletion");
        ChatColorSelectionViewModel chatColorSelectionViewModel = chatColorSelectionFragment.viewModel;
        if (chatColorSelectionViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            chatColorSelectionViewModel = null;
        }
        chatColorSelectionViewModel.deleteNow(confirmDeletion.getChatColors());
        dialogInterface.dismiss();
    }

    /* renamed from: showWarningDialogForMultipleUses$lambda-6 */
    public static final void m1505showWarningDialogForMultipleUses$lambda6(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    /* compiled from: ChatColorSelectionFragment.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0004\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\r\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\u000e\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionFragment$Callbacks;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;", MultiselectForwardFragment.ARGS, "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionFragmentArgs;", "view", "Landroid/view/View;", "(Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionFragment;Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionFragmentArgs;Landroid/view/View;)V", "onAdd", "", "onDelete", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "onDuplicate", "onEdit", "onSelect", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class Callbacks implements ChatColorSelectionAdapter.Callbacks {
        private final ChatColorSelectionFragmentArgs args;
        final /* synthetic */ ChatColorSelectionFragment this$0;
        private final View view;

        public Callbacks(ChatColorSelectionFragment chatColorSelectionFragment, ChatColorSelectionFragmentArgs chatColorSelectionFragmentArgs, View view) {
            Intrinsics.checkNotNullParameter(chatColorSelectionFragmentArgs, MultiselectForwardFragment.ARGS);
            Intrinsics.checkNotNullParameter(view, "view");
            this.this$0 = chatColorSelectionFragment;
            this.args = chatColorSelectionFragmentArgs;
            this.view = view;
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter.Callbacks
        public void onSelect(ChatColors chatColors) {
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            ChatColorSelectionViewModel chatColorSelectionViewModel = this.this$0.viewModel;
            if (chatColorSelectionViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                chatColorSelectionViewModel = null;
            }
            chatColorSelectionViewModel.save(chatColors);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter.Callbacks
        public void onEdit(ChatColors chatColors) {
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            int i = 1;
            if (chatColors.getColors().length == 1) {
                i = 0;
            }
            ChatColorSelectionFragmentDirections.ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment chatColorId = ChatColorSelectionFragmentDirections.actionChatColorSelectionFragmentToCustomChatColorCreatorFragment(this.args.getRecipientId(), i).setChatColorId(chatColors.getId().getLongValue());
            Intrinsics.checkNotNullExpressionValue(chatColorId, "actionChatColorSelection…(chatColors.id.longValue)");
            NavController findNavController = Navigation.findNavController(this.view);
            Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(view)");
            SafeNavigation.safeNavigate(findNavController, chatColorId);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter.Callbacks
        public void onDuplicate(ChatColors chatColors) {
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            ChatColorSelectionViewModel chatColorSelectionViewModel = this.this$0.viewModel;
            if (chatColorSelectionViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                chatColorSelectionViewModel = null;
            }
            chatColorSelectionViewModel.duplicate(chatColors);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter.Callbacks
        public void onDelete(ChatColors chatColors) {
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            ChatColorSelectionViewModel chatColorSelectionViewModel = this.this$0.viewModel;
            if (chatColorSelectionViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                chatColorSelectionViewModel = null;
            }
            chatColorSelectionViewModel.startDeletion(chatColors);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter.Callbacks
        public void onAdd() {
            ChatColorSelectionFragmentDirections.ActionChatColorSelectionFragmentToCustomChatColorCreatorFragment actionChatColorSelectionFragmentToCustomChatColorCreatorFragment = ChatColorSelectionFragmentDirections.actionChatColorSelectionFragmentToCustomChatColorCreatorFragment(this.args.getRecipientId(), 0);
            Intrinsics.checkNotNullExpressionValue(actionChatColorSelectionFragmentToCustomChatColorCreatorFragment, "actionChatColorSelection…ment(args.recipientId, 0)");
            NavController findNavController = Navigation.findNavController(this.view);
            Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(view)");
            SafeNavigation.safeNavigate(findNavController, actionChatColorSelectionFragmentToCustomChatColorCreatorFragment);
        }
    }
}
