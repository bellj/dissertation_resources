package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardState;

/* compiled from: MultiselectForwardViewModel.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardViewModel$performSend$4 extends Lambda implements Function0<Unit> {
    final /* synthetic */ MultiselectForwardViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MultiselectForwardViewModel$performSend$4(MultiselectForwardViewModel multiselectForwardViewModel) {
        super(0);
        this.this$0 = multiselectForwardViewModel;
    }

    /* renamed from: invoke$lambda-0 */
    public static final MultiselectForwardState m1600invoke$lambda0(MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullExpressionValue(multiselectForwardState, "it");
        return MultiselectForwardState.copy$default(multiselectForwardState, MultiselectForwardState.Stage.SomeFailed.INSTANCE, null, 2, null);
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel$performSend$4$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MultiselectForwardViewModel$performSend$4.m1600invoke$lambda0((MultiselectForwardState) obj);
            }
        });
    }
}
