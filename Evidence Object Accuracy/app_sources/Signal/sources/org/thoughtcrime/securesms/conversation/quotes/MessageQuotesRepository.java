package org.thoughtcrime.securesms.conversation.quotes;

import android.app.Application;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.functions.Cancellable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.ConversationDataSource;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.Quote;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* compiled from: MessageQuotesRepository.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \f2\u00020\u0001:\u0001\fB\u0005¢\u0006\u0002\u0010\u0002J\u001e\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH\u0003J\"\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u000b2\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesRepository;", "", "()V", "getMessageInQuoteChainSync", "", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "application", "Landroid/app/Application;", "messageId", "Lorg/thoughtcrime/securesms/database/model/MessageId;", "getMessagesInQuoteChain", "Lio/reactivex/rxjava3/core/Observable;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageQuotesRepository {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(MessageQuotesRepository.class);

    /* compiled from: MessageQuotesRepository.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuotesRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final Observable<List<ConversationMessage>> getMessagesInQuoteChain(Application application, MessageId messageId) {
        Intrinsics.checkNotNullParameter(application, "application");
        Intrinsics.checkNotNullParameter(messageId, "messageId");
        Observable<List<ConversationMessage>> create = Observable.create(new ObservableOnSubscribe(this, application) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ MessageQuotesRepository f$1;
            public final /* synthetic */ Application f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                MessageQuotesRepository.$r8$lambda$sN72m_soRAFronNEEjSX8naleS0(MessageId.this, this.f$1, this.f$2, observableEmitter);
            }
        });
        Intrinsics.checkNotNullExpressionValue(create, "create { emitter ->\n    …cation, messageId))\n    }");
        return create;
    }

    /* renamed from: getMessagesInQuoteChain$lambda-2 */
    public static final void m1607getMessagesInQuoteChain$lambda2(MessageId messageId, MessageQuotesRepository messageQuotesRepository, Application application, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(messageId, "$messageId");
        Intrinsics.checkNotNullParameter(messageQuotesRepository, "this$0");
        Intrinsics.checkNotNullParameter(application, "$application");
        long threadId = SignalDatabase.Companion.mmsSms().getThreadId(messageId);
        if (threadId < 0) {
            String str = TAG;
            Log.w(str, "Could not find a threadId for " + messageId + '!');
            observableEmitter.onNext(CollectionsKt__CollectionsKt.emptyList());
            return;
        }
        DatabaseObserver databaseObserver = ApplicationDependencies.getDatabaseObserver();
        Intrinsics.checkNotNullExpressionValue(databaseObserver, "getDatabaseObserver()");
        MessageQuotesRepository$$ExternalSyntheticLambda0 messageQuotesRepository$$ExternalSyntheticLambda0 = new DatabaseObserver.Observer(messageQuotesRepository, application, messageId) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ MessageQuotesRepository f$1;
            public final /* synthetic */ Application f$2;
            public final /* synthetic */ MessageId f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                MessageQuotesRepository.m1606$r8$lambda$TpCtqexDEcdLDi0ateTzPHaMg(ObservableEmitter.this, this.f$1, this.f$2, this.f$3);
            }
        };
        databaseObserver.registerConversationObserver(threadId, messageQuotesRepository$$ExternalSyntheticLambda0);
        observableEmitter.setCancellable(new Cancellable(messageQuotesRepository$$ExternalSyntheticLambda0) { // from class: org.thoughtcrime.securesms.conversation.quotes.MessageQuotesRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                MessageQuotesRepository.$r8$lambda$Hhi6G8nTK9AK9o4tTap3CeBwLvM(DatabaseObserver.this, this.f$1);
            }
        });
        observableEmitter.onNext(messageQuotesRepository.getMessageInQuoteChainSync(application, messageId));
    }

    /* renamed from: getMessagesInQuoteChain$lambda-2$lambda-0 */
    public static final void m1608getMessagesInQuoteChain$lambda2$lambda0(ObservableEmitter observableEmitter, MessageQuotesRepository messageQuotesRepository, Application application, MessageId messageId) {
        Intrinsics.checkNotNullParameter(messageQuotesRepository, "this$0");
        Intrinsics.checkNotNullParameter(application, "$application");
        Intrinsics.checkNotNullParameter(messageId, "$messageId");
        observableEmitter.onNext(messageQuotesRepository.getMessageInQuoteChainSync(application, messageId));
    }

    /* renamed from: getMessagesInQuoteChain$lambda-2$lambda-1 */
    public static final void m1609getMessagesInQuoteChain$lambda2$lambda1(DatabaseObserver databaseObserver, DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(databaseObserver, "$databaseObserver");
        Intrinsics.checkNotNullParameter(observer, "$observer");
        databaseObserver.unregisterObserver(observer);
    }

    private final List<ConversationMessage> getMessageInQuoteChainSync(Application application, MessageId messageId) {
        MessageRecord messageRecord;
        if (messageId.isMms()) {
            messageRecord = SignalDatabase.Companion.mms().getMessageRecordOrNull(messageId.getId());
        } else {
            messageRecord = SignalDatabase.Companion.sms().getMessageRecordOrNull(messageId.getId());
        }
        if (messageRecord == null) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        List<MessageRecord> allMessagesThatQuote = SignalDatabase.Companion.mmsSms().getAllMessagesThatQuote(messageId);
        Intrinsics.checkNotNullExpressionValue(allMessagesThatQuote, "SignalDatabase.mmsSms.ge…sagesThatQuote(messageId)");
        ConversationDataSource.ReactionHelper reactionHelper = new ConversationDataSource.ReactionHelper();
        reactionHelper.addAll(allMessagesThatQuote);
        reactionHelper.fetchReactions();
        List<MessageRecord> buildUpdatedModels = reactionHelper.buildUpdatedModels(allMessagesThatQuote);
        Intrinsics.checkNotNullExpressionValue(buildUpdatedModels, "ReactionHelper()\n      .…datedModels(replyRecords)");
        ArrayList<MessageRecord> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(buildUpdatedModels, 10));
        for (MessageRecord messageRecord2 : buildUpdatedModels) {
            Intrinsics.checkNotNullExpressionValue(messageRecord2, "replyRecord");
            Quote quote = MessageRecordUtil.getQuote(messageRecord2);
            if (quote != null && quote.getId() == messageRecord.getDateSent()) {
                messageRecord2 = ((MediaMmsMessageRecord) messageRecord2).withoutQuote();
            }
            arrayList.add(messageRecord2);
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (MessageRecord messageRecord3 : arrayList) {
            arrayList2.add(ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(application, messageRecord3));
        }
        ConversationDataSource.ReactionHelper reactionHelper2 = new ConversationDataSource.ReactionHelper();
        reactionHelper2.add(messageRecord);
        reactionHelper2.fetchReactions();
        List<MessageRecord> buildUpdatedModels2 = reactionHelper2.buildUpdatedModels(CollectionsKt__CollectionsJVMKt.listOf(messageRecord));
        Intrinsics.checkNotNullExpressionValue(buildUpdatedModels2, "ReactionHelper()\n      .…s(listOf(originalRecord))");
        ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(buildUpdatedModels2, 10));
        for (MessageRecord messageRecord4 : buildUpdatedModels2) {
            arrayList3.add(ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(application, messageRecord4, messageRecord4.getDisplayBody(application), false));
        }
        return CollectionsKt___CollectionsKt.plus((Collection) arrayList2, (Iterable) arrayList3);
    }
}
