package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MultiselectForwardState.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001\u0015B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState;", "", "stage", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "storySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "(Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;)V", "getStage", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "getStorySendRequirements", "()Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Stage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardState {
    private final Stage stage;
    private final Stories.MediaTransform.SendRequirements storySendRequirements;

    public MultiselectForwardState() {
        this(null, null, 3, null);
    }

    public static /* synthetic */ MultiselectForwardState copy$default(MultiselectForwardState multiselectForwardState, Stage stage, Stories.MediaTransform.SendRequirements sendRequirements, int i, Object obj) {
        if ((i & 1) != 0) {
            stage = multiselectForwardState.stage;
        }
        if ((i & 2) != 0) {
            sendRequirements = multiselectForwardState.storySendRequirements;
        }
        return multiselectForwardState.copy(stage, sendRequirements);
    }

    public final Stage component1() {
        return this.stage;
    }

    public final Stories.MediaTransform.SendRequirements component2() {
        return this.storySendRequirements;
    }

    public final MultiselectForwardState copy(Stage stage, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        return new MultiselectForwardState(stage, sendRequirements);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MultiselectForwardState)) {
            return false;
        }
        MultiselectForwardState multiselectForwardState = (MultiselectForwardState) obj;
        return Intrinsics.areEqual(this.stage, multiselectForwardState.stage) && this.storySendRequirements == multiselectForwardState.storySendRequirements;
    }

    public int hashCode() {
        return (this.stage.hashCode() * 31) + this.storySendRequirements.hashCode();
    }

    public String toString() {
        return "MultiselectForwardState(stage=" + this.stage + ", storySendRequirements=" + this.storySendRequirements + ')';
    }

    public MultiselectForwardState(Stage stage, Stories.MediaTransform.SendRequirements sendRequirements) {
        Intrinsics.checkNotNullParameter(stage, "stage");
        Intrinsics.checkNotNullParameter(sendRequirements, "storySendRequirements");
        this.stage = stage;
        this.storySendRequirements = sendRequirements;
    }

    public /* synthetic */ MultiselectForwardState(Stage stage, Stories.MediaTransform.SendRequirements sendRequirements, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? Stage.Selection.INSTANCE : stage, (i & 2) != 0 ? Stories.MediaTransform.SendRequirements.CAN_NOT_SEND : sendRequirements);
    }

    public final Stage getStage() {
        return this.stage;
    }

    public final Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return this.storySendRequirements;
    }

    /* compiled from: MultiselectForwardState.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\t\u0003\u0004\u0005\u0006\u0007\b\t\n\u000bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\t\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "", "()V", "AllFailed", "FirstConfirmation", "LoadingIdentities", "SafetyConfirmation", "Selection", "SelectionConfirmed", "SendPending", "SomeFailed", "Success", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$Selection;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$FirstConfirmation;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$LoadingIdentities;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SafetyConfirmation;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SendPending;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SomeFailed;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$AllFailed;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$Success;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SelectionConfirmed;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Stage {
        public /* synthetic */ Stage(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$Selection;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Selection extends Stage {
            public static final Selection INSTANCE = new Selection();

            private Selection() {
                super(null);
            }
        }

        private Stage() {
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$FirstConfirmation;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class FirstConfirmation extends Stage {
            public static final FirstConfirmation INSTANCE = new FirstConfirmation();

            private FirstConfirmation() {
                super(null);
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$LoadingIdentities;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class LoadingIdentities extends Stage {
            public static final LoadingIdentities INSTANCE = new LoadingIdentities();

            private LoadingIdentities() {
                super(null);
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B!\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0003J)\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\t¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SafetyConfirmation;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "identities", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "selectedContacts", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "(Ljava/util/List;Ljava/util/List;)V", "getIdentities", "()Ljava/util/List;", "getSelectedContacts", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SafetyConfirmation extends Stage {
            private final List<IdentityRecord> identities;
            private final List<ContactSearchKey> selectedContacts;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardState$Stage$SafetyConfirmation */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ SafetyConfirmation copy$default(SafetyConfirmation safetyConfirmation, List list, List list2, int i, Object obj) {
                if ((i & 1) != 0) {
                    list = safetyConfirmation.identities;
                }
                if ((i & 2) != 0) {
                    list2 = safetyConfirmation.selectedContacts;
                }
                return safetyConfirmation.copy(list, list2);
            }

            public final List<IdentityRecord> component1() {
                return this.identities;
            }

            public final List<ContactSearchKey> component2() {
                return this.selectedContacts;
            }

            public final SafetyConfirmation copy(List<IdentityRecord> list, List<? extends ContactSearchKey> list2) {
                Intrinsics.checkNotNullParameter(list, "identities");
                Intrinsics.checkNotNullParameter(list2, "selectedContacts");
                return new SafetyConfirmation(list, list2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof SafetyConfirmation)) {
                    return false;
                }
                SafetyConfirmation safetyConfirmation = (SafetyConfirmation) obj;
                return Intrinsics.areEqual(this.identities, safetyConfirmation.identities) && Intrinsics.areEqual(this.selectedContacts, safetyConfirmation.selectedContacts);
            }

            public int hashCode() {
                return (this.identities.hashCode() * 31) + this.selectedContacts.hashCode();
            }

            public String toString() {
                return "SafetyConfirmation(identities=" + this.identities + ", selectedContacts=" + this.selectedContacts + ')';
            }

            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public SafetyConfirmation(List<IdentityRecord> list, List<? extends ContactSearchKey> list2) {
                super(null);
                Intrinsics.checkNotNullParameter(list, "identities");
                Intrinsics.checkNotNullParameter(list2, "selectedContacts");
                this.identities = list;
                this.selectedContacts = list2;
            }

            public final List<IdentityRecord> getIdentities() {
                return this.identities;
            }

            public final List<ContactSearchKey> getSelectedContacts() {
                return this.selectedContacts;
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SendPending;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SendPending extends Stage {
            public static final SendPending INSTANCE = new SendPending();

            private SendPending() {
                super(null);
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SomeFailed;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SomeFailed extends Stage {
            public static final SomeFailed INSTANCE = new SomeFailed();

            private SomeFailed() {
                super(null);
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$AllFailed;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class AllFailed extends Stage {
            public static final AllFailed INSTANCE = new AllFailed();

            private AllFailed() {
                super(null);
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$Success;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Success extends Stage {
            public static final Success INSTANCE = new Success();

            private Success() {
                super(null);
            }
        }

        /* compiled from: MultiselectForwardState.kt */
        @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u000f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u0019\u0010\t\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0001J\u0013\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rHÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage$SelectionConfirmed;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardState$Stage;", "selectedContacts", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "(Ljava/util/Set;)V", "getSelectedContacts", "()Ljava/util/Set;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SelectionConfirmed extends Stage {
            private final Set<ContactSearchKey> selectedContacts;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardState$Stage$SelectionConfirmed */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ SelectionConfirmed copy$default(SelectionConfirmed selectionConfirmed, Set set, int i, Object obj) {
                if ((i & 1) != 0) {
                    set = selectionConfirmed.selectedContacts;
                }
                return selectionConfirmed.copy(set);
            }

            public final Set<ContactSearchKey> component1() {
                return this.selectedContacts;
            }

            public final SelectionConfirmed copy(Set<? extends ContactSearchKey> set) {
                Intrinsics.checkNotNullParameter(set, "selectedContacts");
                return new SelectionConfirmed(set);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof SelectionConfirmed) && Intrinsics.areEqual(this.selectedContacts, ((SelectionConfirmed) obj).selectedContacts);
            }

            public int hashCode() {
                return this.selectedContacts.hashCode();
            }

            public String toString() {
                return "SelectionConfirmed(selectedContacts=" + this.selectedContacts + ')';
            }

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.contacts.paged.ContactSearchKey> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public SelectionConfirmed(Set<? extends ContactSearchKey> set) {
                super(null);
                Intrinsics.checkNotNullParameter(set, "selectedContacts");
                this.selectedContacts = set;
            }

            public final Set<ContactSearchKey> getSelectedContacts() {
                return this.selectedContacts;
            }
        }
    }
}
