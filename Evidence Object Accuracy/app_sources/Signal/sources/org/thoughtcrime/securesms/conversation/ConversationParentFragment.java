package org.thoughtcrime.securesms.conversation;

import android.app.Application;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Annotation;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.pm.ShortcutInfoCompat;
import androidx.core.content.pm.ShortcutManagerCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.core.graphics.drawable.IconCompat;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.SimpleColorFilter;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Optional;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.InvalidMessageException;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.BlockUnblockDialog;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.GroupMembersDialog;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.MuteDialog;
import org.thoughtcrime.securesms.PromptMmsActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.ShortcutLauncherActivity;
import org.thoughtcrime.securesms.attachments.TombstoneAttachment;
import org.thoughtcrime.securesms.audio.AudioRecorder;
import org.thoughtcrime.securesms.badges.gifts.thanks.GiftThanksSheet;
import org.thoughtcrime.securesms.components.AnimatingToggle;
import org.thoughtcrime.securesms.components.ComposeText;
import org.thoughtcrime.securesms.components.ConversationSearchBottomBar;
import org.thoughtcrime.securesms.components.HidingLinearLayout;
import org.thoughtcrime.securesms.components.InputAwareLayout;
import org.thoughtcrime.securesms.components.InputPanel;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.SendButton;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.components.TypingStatusSender;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.components.emoji.EmojiStrings;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel;
import org.thoughtcrime.securesms.components.identity.UnverifiedBannerView;
import org.thoughtcrime.securesms.components.location.SignalPlace;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher;
import org.thoughtcrime.securesms.components.reminder.BubbleOptOutReminder;
import org.thoughtcrime.securesms.components.reminder.ExpiredBuildReminder;
import org.thoughtcrime.securesms.components.reminder.GroupsV1MigrationSuggestionsReminder;
import org.thoughtcrime.securesms.components.reminder.PendingGroupJoinRequestsReminder;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.components.reminder.ReminderView;
import org.thoughtcrime.securesms.components.reminder.ServiceOutageReminder;
import org.thoughtcrime.securesms.components.reminder.UnauthorizedReminder;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsActivity;
import org.thoughtcrime.securesms.components.voice.VoiceNoteDraft;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView;
import org.thoughtcrime.securesms.contacts.ContactAccessor;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactShareEditActivity;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.contactshare.SimpleTextWatcher;
import org.thoughtcrime.securesms.conversation.AttachmentKeyboard;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.conversation.ConversationGroupViewModel;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;
import org.thoughtcrime.securesms.conversation.ConversationReactionOverlay;
import org.thoughtcrime.securesms.conversation.ConversationSearchViewModel;
import org.thoughtcrime.securesms.conversation.ConversationStickerViewModel;
import org.thoughtcrime.securesms.conversation.ConversationViewModel;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.conversation.drafts.DraftRepository;
import org.thoughtcrime.securesms.conversation.drafts.DraftState;
import org.thoughtcrime.securesms.conversation.drafts.DraftViewModel;
import org.thoughtcrime.securesms.conversation.ui.groupcall.GroupCallViewModel;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.crypto.SecurityEvent;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.identity.IdentityRecordList;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.database.model.StoryType;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.GroupCallPeekEvent;
import org.thoughtcrime.securesms.events.ReminderUpdateEvent;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.ManagePendingAndRequestingMembersActivity;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog;
import org.thoughtcrime.securesms.insights.InsightsLauncher;
import org.thoughtcrime.securesms.invites.InviteReminderModel;
import org.thoughtcrime.securesms.invites.InviteReminderRepository;
import org.thoughtcrime.securesms.jobs.GroupV1MigrationJob;
import org.thoughtcrime.securesms.jobs.GroupV2UpdateSelfProfileKeyJob;
import org.thoughtcrime.securesms.jobs.RequestGroupV2InfoJob;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.jobs.ServiceOutageDetectionJob;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerViewModel;
import org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment;
import org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment;
import org.thoughtcrime.securesms.keyboard.sticker.StickerSearchDialogFragment;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewViewModel;
import org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder;
import org.thoughtcrime.securesms.maps.PlacePickerActivity;
import org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.mediasend.v2.MediaSelectionActivity;
import org.thoughtcrime.securesms.messagedetails.MessageDetailsFragment;
import org.thoughtcrime.securesms.messagerequests.MessageRequestState;
import org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel;
import org.thoughtcrime.securesms.messagerequests.MessageRequestsBottomView;
import org.thoughtcrime.securesms.mms.AttachmentManager;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GifSlide;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.LocationSlide;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.OutgoingSecureMediaMessage;
import org.thoughtcrime.securesms.mms.QuoteId;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.mms.SlideFactory;
import org.thoughtcrime.securesms.mms.StickerSlide;
import org.thoughtcrime.securesms.mms.VideoSlide;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.payments.CanNotSendPaymentDialog;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewBannerView;
import org.thoughtcrime.securesms.profiles.spoofing.ReviewCardDialogFragment;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.ratelimit.RecaptchaProofBottomSheetFragment;
import org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientExporter;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.recipients.ui.disappearingmessages.RecipientDisappearingMessagesActivity;
import org.thoughtcrime.securesms.registration.RegistrationNavigationActivity;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.search.MessageResult;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingEncryptedMessage;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.stickers.StickerEventListener;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.stickers.StickerManagementActivity;
import org.thoughtcrime.securesms.stickers.StickerPackInstallEvent;
import org.thoughtcrime.securesms.stickers.StickerSearchRepository;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.BubbleUtil;
import org.thoughtcrime.securesms.util.CharacterCalculator;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.FullscreenHelper;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.MessageUtil;
import org.thoughtcrime.securesms.util.PlayStoreUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.SmsUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.WindowUtil;
import org.thoughtcrime.securesms.util.concurrent.AssertedSuccessListener;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;
import org.thoughtcrime.securesms.util.views.Stub;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperDimLevelUtil;
import org.whispersystems.signalservice.api.SignalSessionLock;
import org.whispersystems.signalservice.api.util.ExpiringProfileCredentialUtil;

/* loaded from: classes.dex */
public class ConversationParentFragment extends Fragment implements ConversationFragment.ConversationFragmentListener, AttachmentManager.AttachmentListener, KeyboardAwareLinearLayout.OnKeyboardShownListener, InputPanel.Listener, InputPanel.MediaListener, ComposeText.CursorPositionChangedListener, ConversationSearchBottomBar.EventListener, StickerEventListener, AttachmentKeyboard.Callback, ConversationReactionOverlay.OnReactionSelectedListener, ReactWithAnyEmojiBottomSheetDialogFragment.Callback, SafetyNumberBottomSheet.Callbacks, ReactionsBottomSheetDialogFragment.Callback, MediaKeyboard.MediaKeyboardListener, EmojiEventListener, GifKeyboardPageFragment.Host, EmojiKeyboardPageFragment.Callback, EmojiSearchFragment.Callback, StickerKeyboardPageFragment.Callback, Material3OnScrollHelperBinder, MessageDetailsFragment.Callback {
    private static final String ACTION_PINNED_SHORTCUT;
    private static final int ADD_CONTACT;
    private static final String ARG_INTENT_DATA;
    private static final int GET_CONTACT_DETAILS;
    private static final int GROUP_EDIT;
    private static final int MEDIA_SENDER;
    private static final int PICK_AUDIO;
    private static final int PICK_CONTACT;
    private static final int PICK_DOCUMENT;
    private static final int PICK_GALLERY;
    public static final int PICK_GIF;
    private static final int PICK_LOCATION;
    private static final int REQUEST_CODE_PIN_SHORTCUT;
    private static final int REQUEST_CODE_SETTINGS;
    private static final int SHORTCUT_ICON_SIZE = ViewUtil.dpToPx(Build.VERSION.SDK_INT >= 26 ? 72 : 80);
    private static final int SMS_DEFAULT;
    private static final String STATE_IS_SEARCH_REQUESTED;
    private static final String STATE_REACT_WITH_ANY_PAGE;
    private static final String TAG = Log.tag(ConversationParentFragment.class);
    private static final int TAKE_PHOTO;
    private ImageButton attachButton;
    private Stub<AttachmentKeyboard> attachmentKeyboardStub;
    private AttachmentManager attachmentManager;
    private AudioRecorder audioRecorder;
    private AnimatingToggle buttonToggle;
    private Callback callback;
    private boolean callingTooltipShown;
    private View cancelJoinRequest;
    private Stub<TextView> cannotSendInAnnouncementGroupBanner;
    private TextView charactersLeft;
    protected ComposeText composeText;
    private InputAwareLayout container;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private int distributionType;
    private DraftViewModel draftViewModel;
    private Stub<MediaKeyboard> emojiDrawerStub;
    private ConversationFragment fragment;
    private GlideRequests glideRequests;
    private GroupCallViewModel groupCallViewModel;
    private ConversationGroupViewModel groupViewModel;
    private boolean hasProcessedShareData = false;
    private IdentityRecordList identityRecords = new IdentityRecordList(Collections.emptyList());
    protected HidingLinearLayout inlineAttachmentToggle;
    private InputPanel inputPanel;
    private InviteReminderModel inviteReminderModel;
    private boolean isSearchRequested = false;
    private MaterialButton joinGroupCallButton;
    private LinkPreviewViewModel linkPreviewViewModel;
    private Button makeDefaultSmsButton;
    private Material3OnScrollHelper material3OnScrollHelper;
    private Stub<View> mentionsSuggestions;
    private MentionsPickerViewModel mentionsViewModel;
    private MessageRequestsBottomView messageRequestBottomView;
    private View navigationBarBackground;
    private View noLongerMemberBanner;
    private final Debouncer optionsMenuDebouncer = new Debouncer(50);
    private BroadcastReceiver pinnedShortcutReceiver;
    protected HidingLinearLayout quickAttachmentToggle;
    private int reactWithAnyEmojiStartPage = -1;
    private ConversationReactionDelegate reactionDelegate;
    private RecentEmojiPageModel recentEmojis;
    private LiveRecipient recipient;
    private Button registerButton;
    private Stub<View> releaseChannelUnmute;
    protected Stub<ReminderView> reminderView;
    private View requestingMemberBanner;
    private Stub<ReviewBannerView> reviewBanner;
    private ConversationSearchBottomBar searchNav;
    private MenuItem searchViewItem;
    private ConversationSearchViewModel searchViewModel;
    private BroadcastReceiver securityUpdateReceiver;
    private SendButton sendButton;
    private ConversationStickerViewModel stickerViewModel;
    private long threadId;
    protected ConversationTitleView titleView;
    private Toolbar toolbar;
    private View toolbarBackground;
    private TypingStatusTextWatcher typingTextWatcher;
    private Button unblockButton;
    private Stub<UnverifiedBannerView> unverifiedBannerView;
    private ConversationViewModel viewModel;
    private VoiceNoteMediaController voiceNoteMediaController;
    private VoiceNotePlayerView voiceNotePlayerView;
    private Stub<FrameLayout> voiceNotePlayerViewStub;
    private VoiceRecorderWakeLock voiceRecorderWakeLock;
    private ImageView wallpaper;
    private View wallpaperDim;

    /* loaded from: classes4.dex */
    public interface Callback {

        /* renamed from: org.thoughtcrime.securesms.conversation.ConversationParentFragment$Callback$-CC */
        /* loaded from: classes4.dex */
        public final /* synthetic */ class CC {
            public static boolean $default$isInBubble(Callback callback) {
                return false;
            }

            public static void $default$onInitializeToolbar(Callback callback, Toolbar toolbar) {
            }

            public static void $default$onSendComplete(Callback callback, long j) {
            }

            public static boolean $default$onUpdateReminders(Callback callback) {
                return false;
            }
        }

        boolean isInBubble();

        void onInitializeToolbar(Toolbar toolbar);

        void onSendComplete(long j);

        boolean onUpdateReminders();
    }

    public static int getActiveToolbarColor(boolean z) {
        return z ? R.color.conversation_toolbar_color_wallpaper_scrolled : R.color.signal_colorSurface2;
    }

    public static int getInactiveToolbarColor(boolean z) {
        return z ? R.color.conversation_toolbar_color_wallpaper : R.color.signal_colorBackground;
    }

    public static /* synthetic */ void lambda$updateReminders$24() {
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onCanceled() {
    }

    public static ConversationParentFragment create(Intent intent) {
        ConversationParentFragment conversationParentFragment = new ConversationParentFragment();
        Bundle bundle = new Bundle();
        bundle.putAll(ConversationIntents.createParentFragmentArguments(intent));
        conversationParentFragment.setArguments(bundle);
        return conversationParentFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.conversation_activity, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.disposables.bindTo(getViewLifecycleOwner());
        if (requireActivity() instanceof Callback) {
            this.callback = (Callback) requireActivity();
        } else if (getParentFragment() instanceof Callback) {
            this.callback = (Callback) getParentFragment();
        } else {
            throw new ClassCastException("Cannot cast activity or parent fragment into a Callback object");
        }
        if (ConversationIntents.isInvalid(requireArguments())) {
            Log.w(TAG, "[onCreate] Missing recipientId!");
            startActivity(MainActivity.clearTop(requireContext()));
            requireActivity().finish();
            return;
        }
        this.voiceNoteMediaController = new VoiceNoteMediaController(requireActivity());
        this.voiceRecorderWakeLock = new VoiceRecorderWakeLock(requireActivity());
        new FullscreenHelper(requireActivity()).showSystemUI();
        ConversationIntents.Args from = ConversationIntents.Args.from(requireArguments());
        if (bundle == null && from.getGiftBadge() != null) {
            GiftThanksSheet.show(getChildFragmentManager(), from.getRecipientId(), from.getGiftBadge());
        }
        this.isSearchRequested = from.isWithSearchOpen();
        reportShortcutLaunch(from.getRecipientId());
        requireActivity().getWindow().getDecorView().setBackgroundResource(R.color.signal_background_primary);
        ConversationFragment conversationFragment = (ConversationFragment) getChildFragmentManager().findFragmentById(R.id.fragment_content);
        this.fragment = conversationFragment;
        if (conversationFragment == null) {
            this.fragment = new ConversationFragment();
            getChildFragmentManager().beginTransaction().replace(R.id.fragment_content, this.fragment).commitNow();
        }
        if (bundle != null) {
            this.hasProcessedShareData = bundle.getBoolean("SHARED", false);
        }
        initializeReceivers();
        initializeViews(view);
        updateWallpaper(from.getWallpaper());
        initializeResources(from);
        initializeLinkPreviewObserver();
        initializeSearchObserver();
        initializeStickerObserver();
        initializeViewModel(from);
        initializeGroupViewModel();
        initializeMentionsViewModel();
        initializeGroupCallViewModel();
        initializeDraftViewModel();
        initializeEnabledCheck();
        initializePendingRequestsBanner();
        initializeGroupV1MigrationsBanners();
        Flowable<ConversationSecurityInfo> conversationSecurityInfo = this.viewModel.getConversationSecurityInfo(from.getRecipientId());
        this.disposables.add(conversationSecurityInfo.subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda17
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationParentFragment.this.handleSecurityChange((ConversationSecurityInfo) obj);
            }
        }));
        this.disposables.add(conversationSecurityInfo.firstOrError().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda18
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationParentFragment.this.lambda$onViewCreated$0((ConversationSecurityInfo) obj);
            }
        }));
        initializeInsightObserver();
        initializeActionBar();
        LifecycleDisposable lifecycleDisposable = this.disposables;
        Observable<StoryViewState> storyViewState = this.viewModel.getStoryViewState();
        ConversationTitleView conversationTitleView = this.titleView;
        Objects.requireNonNull(conversationTitleView);
        lifecycleDisposable.add(storyViewState.subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda19
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationTitleView.this.setStoryRingFromState((StoryViewState) obj);
            }
        }));
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.1
            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                ConversationParentFragment.this.onBackPressed();
            }
        });
        if (this.isSearchRequested && bundle == null) {
            onCreateOptionsMenu(this.toolbar.getMenu(), requireActivity().getMenuInflater());
        }
        this.sendButton.post(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda20
            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.this.lambda$onViewCreated$1();
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(ConversationSecurityInfo conversationSecurityInfo) throws Throwable {
        onInitialSecurityConfigurationLoaded();
    }

    public /* synthetic */ void lambda$onViewCreated$1() {
        this.sendButton.triggerSelectedChangedEvent();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        WindowUtil.setLightNavigationBarFromTheme(requireActivity());
        WindowUtil.setLightStatusBarFromTheme(requireActivity());
        EventBus.getDefault().register(this);
        this.viewModel.checkIfMmsIsEnabled();
        initializeIdentityRecords();
        this.composeText.setMessageSendType(this.sendButton.getSelectedSendType());
        Recipient recipient = this.recipient.get();
        this.titleView.setTitle(this.glideRequests, recipient);
        setBlockedUserState(recipient, this.viewModel.getConversationStateSnapshot().getSecurityInfo());
        calculateCharactersRemaining();
        if (recipient.getGroupId().isPresent() && recipient.getGroupId().get().isV2() && !recipient.isBlocked()) {
            GroupId.V2 requireV2 = recipient.getGroupId().get().requireV2();
            ApplicationDependencies.getJobManager().startChain(new RequestGroupV2InfoJob(requireV2)).then(GroupV2UpdateSelfProfileKeyJob.withoutLimits(requireV2)).enqueue();
            if (this.viewModel.getArgs().isFirstTimeInSelfCreatedGroup()) {
                this.groupViewModel.inviteFriendsOneTimeIfJustSelfInGroup(getChildFragmentManager(), requireV2);
            }
        }
        GroupCallViewModel groupCallViewModel = this.groupCallViewModel;
        if (groupCallViewModel != null) {
            groupCallViewModel.peekGroupCall();
        }
        setVisibleThread(this.threadId);
        ConversationUtil.refreshRecipientShortcuts();
        if (SignalStore.rateLimit().needsRecaptcha()) {
            RecaptchaProofBottomSheetFragment.show(getChildFragmentManager());
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        if (!isInBubble()) {
            ApplicationDependencies.getMessageNotifier().clearVisibleThread();
        }
        if (requireActivity().isFinishing()) {
            requireActivity().overridePendingTransition(R.anim.fade_scale_in, R.anim.slide_to_end);
        }
        this.inputPanel.onPause();
        this.fragment.setLastSeen(System.currentTimeMillis());
        markLastSeen();
        EventBus.getDefault().unregister(this);
        this.material3OnScrollHelper.setColorImmediate();
    }

    @Override // androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        saveDraft();
        EventBus.getDefault().unregister(this);
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        String str = TAG;
        Log.i(str, "onConfigurationChanged(" + configuration.orientation + ")");
        super.onConfigurationChanged(configuration);
        this.composeText.setMessageSendType(this.sendButton.getSelectedSendType());
        if (this.emojiDrawerStub.resolved() && this.container.getCurrentInput() == this.emojiDrawerStub.get()) {
            this.container.hideAttachedInput(true);
        }
        if (this.reactionDelegate.isShowing()) {
            this.reactionDelegate.hide();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroy() {
        if (this.securityUpdateReceiver != null) {
            requireActivity().unregisterReceiver(this.securityUpdateReceiver);
        }
        if (this.pinnedShortcutReceiver != null) {
            requireActivity().unregisterReceiver(this.pinnedShortcutReceiver);
        }
        super.onDestroy();
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        return this.reactionDelegate.applyTouchEvent(motionEvent);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        String str = TAG;
        Log.i(str, "onActivityResult called: " + i + ", " + i2 + " , " + intent);
        super.onActivityResult(i, i2, intent);
        if ((intent != null || i == 7 || i == 11) && (i2 == -1 || i == 11)) {
            switch (i) {
                case 2:
                    setMedia(intent.getData(), SlideFactory.MediaType.DOCUMENT);
                    return;
                case 3:
                    setMedia(intent.getData(), SlideFactory.MediaType.AUDIO);
                    return;
                case 4:
                    if (!this.viewModel.isPushAvailable() || isSmsForced()) {
                        addAttachmentContactInfo(intent.getData());
                        return;
                    } else {
                        openContactShareEditor(intent.getData());
                        return;
                    }
                case 5:
                    sendSharedContact(intent.getParcelableArrayListExtra(ContactShareEditActivity.KEY_CONTACTS));
                    return;
                case 6:
                    Recipient recipient = this.recipient.get();
                    onRecipientChanged(recipient);
                    this.titleView.setTitle(this.glideRequests, recipient);
                    NotificationChannels.updateContactChannelName(requireContext(), recipient);
                    setBlockedUserState(recipient, this.viewModel.getConversationStateSnapshot().getSecurityInfo());
                    invalidateOptionsMenu();
                    return;
                case 7:
                    handleImageFromDeviceCameraApp();
                    return;
                case 8:
                    SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda105
                        @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                        public final Object run() {
                            return ConversationParentFragment.this.lambda$onActivityResult$2();
                        }
                    }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda106
                        @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                        public final void run(Object obj) {
                            ConversationParentFragment.this.lambda$onActivityResult$3(obj);
                        }
                    });
                    return;
                case 9:
                    this.attachmentManager.setLocation(new SignalPlace(PlacePickerActivity.addressFromData(intent)), getCurrentMediaConstraints());
                    return;
                case 10:
                case 12:
                    MediaSendActivityResult fromData = MediaSendActivityResult.fromData(intent);
                    if (!Objects.equals(fromData.getRecipientId(), this.recipient.getId())) {
                        Log.w(str, "Result's recipientId did not match ours! Result: " + fromData.getRecipientId() + ", Activity: " + this.recipient.getId());
                        Toast.makeText(requireContext(), (int) R.string.ConversationActivity_error_sending_media, 0).show();
                        return;
                    }
                    this.sendButton.setSendType(fromData.getMessageSendType());
                    if (fromData.isPushPreUpload()) {
                        sendMediaMessage(fromData);
                        return;
                    }
                    long millis = TimeUnit.SECONDS.toMillis((long) this.recipient.get().getExpiresInSeconds());
                    boolean z = this.threadId == -1;
                    QuoteModel orElse = fromData.isViewOnce() ? null : this.inputPanel.getQuote().orElse(null);
                    final SlideDeck slideDeck = new SlideDeck();
                    ArrayList arrayList = new ArrayList(fromData.getMentions());
                    for (Media media : fromData.getNonUploadedMedia()) {
                        if (MediaUtil.isVideoType(media.getMimeType())) {
                            slideDeck.addSlide(new VideoSlide(requireContext(), media.getUri(), media.getSize(), media.isVideoGif(), media.getWidth(), media.getHeight(), media.getCaption().orElse(null), media.getTransformProperties().orElse(null)));
                        } else if (MediaUtil.isGif(media.getMimeType())) {
                            slideDeck.addSlide(new GifSlide(requireContext(), media.getUri(), media.getSize(), media.getWidth(), media.getHeight(), media.isBorderless(), media.getCaption().orElse(null)));
                        } else if (MediaUtil.isImageType(media.getMimeType())) {
                            slideDeck.addSlide(new ImageSlide(requireContext(), media.getUri(), media.getMimeType(), media.getSize(), media.getWidth(), media.getHeight(), media.isBorderless(), media.getCaption().orElse(null), null, media.getTransformProperties().orElse(null)));
                        } else {
                            String str2 = TAG;
                            Log.w(str2, "Asked to send an unexpected mimeType: '" + media.getMimeType() + "'. Skipping.");
                        }
                    }
                    final Context applicationContext = requireContext().getApplicationContext();
                    sendMediaMessage(fromData.getRecipientId(), fromData.getMessageSendType(), fromData.getBody(), slideDeck, orElse, Collections.emptyList(), Collections.emptyList(), arrayList, millis, fromData.isViewOnce(), z, true, null).addListener(new AssertedSuccessListener<Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.2
                        public void onSuccess(Void r4) {
                            AsyncTask.THREAD_POOL_EXECUTOR.execute(new ConversationParentFragment$2$$ExternalSyntheticLambda0(slideDeck, applicationContext));
                        }

                        public static /* synthetic */ void lambda$onSuccess$1(SlideDeck slideDeck2, Context context) {
                            Stream.of(slideDeck2.getSlides()).map(new ConversationParentFragment$2$$ExternalSyntheticLambda1()).withoutNulls().filter(new ConversationParentFragment$2$$ExternalSyntheticLambda2()).forEach(new ConversationParentFragment$2$$ExternalSyntheticLambda3(context));
                        }

                        public static /* synthetic */ void lambda$onSuccess$0(Context context, Uri uri) {
                            BlobProvider.getInstance().delete(context, uri);
                        }
                    });
                    return;
                case 11:
                    this.viewModel.updateSecurityInfo();
                    return;
                default:
                    return;
            }
        } else {
            updateLinkPreviewState();
            SignalStore.settings().setDefaultSms(Util.isDefaultSmsProvider(requireContext()));
        }
    }

    public /* synthetic */ Object lambda$onActivityResult$2() {
        try {
            ContactDiscovery.refresh(requireContext(), this.recipient.get(), false);
            return null;
        } catch (IOException unused) {
            Log.w(TAG, "Failed to refresh user after adding to contacts.");
            return null;
        }
    }

    public /* synthetic */ void lambda$onActivityResult$3(Object obj) {
        onRecipientChanged(this.recipient.get());
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt(STATE_REACT_WITH_ANY_PAGE, this.reactWithAnyEmojiStartPage);
        bundle.putBoolean(STATE_IS_SEARCH_REQUESTED, this.isSearchRequested);
        bundle.putBoolean("SHARED", this.hasProcessedShareData);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewStateRestored(Bundle bundle) {
        super.onViewStateRestored(bundle);
        if (bundle != null) {
            this.reactWithAnyEmojiStartPage = bundle.getInt(STATE_REACT_WITH_ANY_PAGE, -1);
            this.isSearchRequested = bundle.getBoolean(STATE_IS_SEARCH_REQUESTED, false);
        }
    }

    private void onInitialSecurityConfigurationLoaded() {
        String str = TAG;
        Log.d(str, "Initial security configuration loaded.");
        if (getContext() == null) {
            Log.w(str, "Fragment has become detached from context. Ignoring configuration call.");
            return;
        }
        initializeProfiles();
        initializeGv1Migration();
        Log.d(str, "Initializing draft from initial security configuration load...");
        initializeDraft(this.viewModel.getArgs()).addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.3
            public void onSuccess(Boolean bool) {
                Log.d(ConversationParentFragment.TAG, "Initial security configuration loaded.");
                if (ConversationParentFragment.this.getContext() == null) {
                    Log.w(ConversationParentFragment.TAG, "Fragment has become detached from context. Ignoring draft load.");
                    return;
                }
                if (bool != null && bool.booleanValue()) {
                    Log.i(ConversationParentFragment.TAG, "Finished loading draft");
                    ThreadUtil.runOnMain(new ConversationParentFragment$3$$ExternalSyntheticLambda0(this));
                }
                if (TextSecurePreferences.isTypingIndicatorsEnabled(ApplicationDependencies.getApplication())) {
                    ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
                    conversationParentFragment.composeText.addTextChangedListener(conversationParentFragment.typingTextWatcher);
                }
                ComposeText composeText = ConversationParentFragment.this.composeText;
                composeText.setSelection(composeText.length(), ConversationParentFragment.this.composeText.length());
            }

            public /* synthetic */ void lambda$onSuccess$0() {
                if (ConversationParentFragment.this.fragment == null || !ConversationParentFragment.this.fragment.isResumed()) {
                    Log.w(ConversationParentFragment.TAG, "Wanted to move to the last seen position, but the fragment was in an invalid state");
                } else {
                    ConversationParentFragment.this.fragment.moveToLastSeen();
                }
            }
        });
    }

    private void setVisibleThread(long j) {
        if (!isInBubble()) {
            ApplicationDependencies.getMessageNotifier().setVisibleThread(ConversationId.forConversation(j));
        }
    }

    private void reportShortcutLaunch(RecipientId recipientId) {
        ShortcutManagerCompat.reportShortcutUsed(requireContext(), ConversationUtil.getShortcutId(recipientId));
    }

    private void handleImageFromDeviceCameraApp() {
        if (this.attachmentManager.getCaptureUri() == null) {
            Log.w(TAG, "No image available.");
            return;
        }
        try {
            Uri createForSingleSessionOnDisk = BlobProvider.getInstance().forData(requireContext().getContentResolver().openInputStream(this.attachmentManager.getCaptureUri()), 0).withMimeType(MediaUtil.IMAGE_JPEG).createForSingleSessionOnDisk(requireContext());
            requireContext().getContentResolver().delete(this.attachmentManager.getCaptureUri(), null, null);
            setMedia(createForSingleSessionOnDisk, SlideFactory.MediaType.IMAGE);
        } catch (IOException e) {
            Log.w(TAG, "Could not handle public image", e);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void startActivity(Intent intent) {
        if (intent.getStringExtra("com.android.browser.application_id") != null) {
            intent.removeExtra("com.android.browser.application_id");
        }
        try {
            super.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, e);
            Toast.makeText(requireContext(), (int) R.string.ConversationActivity_there_is_no_app_available_to_handle_this_link_on_your_device, 1).show();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onCreateOptionsMenu(final Menu menu, MenuInflater menuInflater) {
        LiveRecipient liveRecipient;
        menu.clear();
        ConversationGroupViewModel.GroupActiveState value = this.groupViewModel.getGroupActiveState().getValue();
        boolean z = false;
        boolean z2 = value != null && value.isActiveGroup();
        boolean z3 = value != null && value.isActiveV2Group();
        if (value != null && !value.isActiveGroup()) {
            z = true;
        }
        if (isInMessageRequest() && (liveRecipient = this.recipient) != null && !liveRecipient.get().isBlocked()) {
            if (z2) {
                menuInflater.inflate(R.menu.conversation_message_requests_group, menu);
            }
            super.onCreateOptionsMenu(menu, menuInflater);
        }
        if (this.viewModel.isPushAvailable()) {
            if (this.recipient.get().getExpiresInSeconds() > 0) {
                if (!z) {
                    menuInflater.inflate(R.menu.conversation_expiring_on, menu);
                }
                this.titleView.showExpiring(this.recipient);
            } else {
                if (!z) {
                    menuInflater.inflate(R.menu.conversation_expiring_off, menu);
                }
                this.titleView.clearExpiring();
            }
        }
        if (isSingleConversation()) {
            if (this.viewModel.isPushAvailable()) {
                menuInflater.inflate(R.menu.conversation_callable_secure, menu);
            } else if (!this.recipient.get().isReleaseNotes()) {
                menuInflater.inflate(R.menu.conversation_callable_insecure, menu);
            }
        } else if (isGroupConversation()) {
            if (z3 && Build.VERSION.SDK_INT > 19) {
                menuInflater.inflate(R.menu.conversation_callable_groupv2, menu);
                GroupCallViewModel groupCallViewModel = this.groupCallViewModel;
                if (groupCallViewModel != null && Boolean.TRUE.equals(groupCallViewModel.hasActiveGroupCall().getValue())) {
                    hideMenuItem(menu, R.id.menu_video_secure);
                }
                showGroupCallingTooltip();
            }
            menuInflater.inflate(R.menu.conversation_group_options, menu);
            if (!isPushGroupConversation()) {
                menuInflater.inflate(R.menu.conversation_mms_group_options, menu);
                if (this.distributionType == 1) {
                    menu.findItem(R.id.menu_distribution_broadcast).setChecked(true);
                } else {
                    menu.findItem(R.id.menu_distribution_conversation).setChecked(true);
                }
            }
            menuInflater.inflate(R.menu.conversation_active_group_options, menu);
        }
        menuInflater.inflate(R.menu.conversation, menu);
        if (isInMessageRequest() && !this.recipient.get().isBlocked()) {
            hideMenuItem(menu, R.id.menu_conversation_settings);
        }
        if (isSingleConversation() && !this.viewModel.isPushAvailable() && !this.recipient.get().isReleaseNotes()) {
            menuInflater.inflate(R.menu.conversation_insecure, menu);
        }
        LiveRecipient liveRecipient2 = this.recipient;
        if (liveRecipient2 == null || !liveRecipient2.get().isMuted()) {
            menuInflater.inflate(R.menu.conversation_unmuted, menu);
        } else {
            menuInflater.inflate(R.menu.conversation_muted, menu);
        }
        if (isSingleConversation() && getRecipient().getContactUri() == null && !this.recipient.get().isReleaseNotes() && !this.recipient.get().isSelf()) {
            menuInflater.inflate(R.menu.conversation_add_to_contacts, menu);
        }
        LiveRecipient liveRecipient3 = this.recipient;
        if (liveRecipient3 != null && liveRecipient3.get().isSelf()) {
            if (this.viewModel.isPushAvailable()) {
                hideMenuItem(menu, R.id.menu_call_secure);
                hideMenuItem(menu, R.id.menu_video_secure);
            } else {
                hideMenuItem(menu, R.id.menu_call_insecure);
            }
            hideMenuItem(menu, R.id.menu_mute_notifications);
        }
        LiveRecipient liveRecipient4 = this.recipient;
        if (liveRecipient4 != null && liveRecipient4.get().isBlocked()) {
            if (this.viewModel.isPushAvailable()) {
                hideMenuItem(menu, R.id.menu_call_secure);
                hideMenuItem(menu, R.id.menu_video_secure);
                hideMenuItem(menu, R.id.menu_expiring_messages);
                hideMenuItem(menu, R.id.menu_expiring_messages_off);
            } else {
                hideMenuItem(menu, R.id.menu_call_insecure);
            }
            hideMenuItem(menu, R.id.menu_mute_notifications);
        }
        LiveRecipient liveRecipient5 = this.recipient;
        if (liveRecipient5 != null && liveRecipient5.get().isReleaseNotes()) {
            hideMenuItem(menu, R.id.menu_add_shortcut);
        }
        hideMenuItem(menu, R.id.menu_group_recipients);
        if (z3) {
            hideMenuItem(menu, R.id.menu_mute_notifications);
            hideMenuItem(menu, R.id.menu_conversation_settings);
        } else if (isGroupConversation()) {
            hideMenuItem(menu, R.id.menu_conversation_settings);
        }
        hideMenuItem(menu, R.id.menu_create_bubble);
        this.disposables.add(this.viewModel.canShowAsBubble().subscribe(new Consumer(menu) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda100
            public final /* synthetic */ Menu f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationParentFragment.this.lambda$onCreateOptionsMenu$4(this.f$1, (Boolean) obj);
            }
        }));
        if (this.threadId == -1) {
            hideMenuItem(menu, R.id.menu_view_media);
        }
        MenuItem findItem = menu.findItem(R.id.menu_search);
        this.searchViewItem = findItem;
        final SearchView searchView = (SearchView) findItem.getActionView();
        final AnonymousClass4 r1 = new SearchView.OnQueryTextListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.4
            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextSubmit(String str) {
                ConversationParentFragment.this.searchViewModel.onQueryUpdated(str, ConversationParentFragment.this.threadId, true);
                ConversationParentFragment.this.searchNav.showLoading();
                ConversationParentFragment.this.viewModel.setSearchQuery(str);
                return true;
            }

            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextChange(String str) {
                ConversationParentFragment.this.searchViewModel.onQueryUpdated(str, ConversationParentFragment.this.threadId, false);
                ConversationParentFragment.this.searchNav.showLoading();
                ConversationParentFragment.this.viewModel.setSearchQuery(str);
                return true;
            }
        };
        this.searchViewItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.5
            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                searchView.setOnQueryTextListener(r1);
                ConversationParentFragment.this.isSearchRequested = true;
                ConversationParentFragment.this.searchViewModel.onSearchOpened();
                ConversationParentFragment.this.searchNav.setVisibility(0);
                ConversationParentFragment.this.searchNav.setData(0, 0);
                ConversationParentFragment.this.inputPanel.setHideForSearch(true);
                for (int i = 0; i < menu.size(); i++) {
                    if (!menu.getItem(i).equals(ConversationParentFragment.this.searchViewItem)) {
                        menu.getItem(i).setVisible(false);
                    }
                }
                return true;
            }

            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                searchView.setOnQueryTextListener(null);
                ConversationParentFragment.this.isSearchRequested = false;
                ConversationParentFragment.this.searchViewModel.onSearchClosed();
                ConversationParentFragment.this.searchNav.setVisibility(8);
                ConversationParentFragment.this.inputPanel.setHideForSearch(false);
                ConversationParentFragment.this.viewModel.setSearchQuery(null);
                ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
                conversationParentFragment.setBlockedUserState(conversationParentFragment.recipient.get(), ConversationParentFragment.this.viewModel.getConversationStateSnapshot().getSecurityInfo());
                ConversationParentFragment.this.invalidateOptionsMenu();
                return true;
            }
        });
        searchView.setMaxWidth(Integer.MAX_VALUE);
        if (this.isSearchRequested && this.searchViewItem.expandActionView()) {
            this.searchViewModel.onSearchOpened();
        }
        super.onCreateOptionsMenu(menu, menuInflater);
        setToolbarActionItemTint(this.toolbar, getResources().getColor(this.wallpaper.getDrawable() != null ? R.color.signal_colorNeutralInverse : R.color.signal_colorOnSurface));
    }

    public /* synthetic */ void lambda$onCreateOptionsMenu$4(Menu menu, Boolean bool) throws Throwable {
        MenuItem findItem = menu.findItem(R.id.menu_create_bubble);
        if (findItem != null) {
            findItem.setVisible(bool.booleanValue() && !isInBubble());
        }
    }

    public void invalidateOptionsMenu() {
        if (!this.isSearchRequested && getActivity() != null) {
            this.optionsMenuDebouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda96
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationParentFragment.this.lambda$invalidateOptionsMenu$5();
                }
            });
        }
    }

    public /* synthetic */ void lambda$invalidateOptionsMenu$5() {
        if (getActivity() != null) {
            onCreateOptionsMenu(this.toolbar.getMenu(), requireActivity().getMenuInflater());
        }
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        int itemId = menuItem.getItemId();
        if (itemId == R.id.menu_call_secure) {
            handleDial(getRecipient(), true);
        } else if (itemId == R.id.menu_video_secure) {
            handleVideo(getRecipient());
        } else if (itemId == R.id.menu_call_insecure) {
            handleDial(getRecipient(), false);
        } else if (itemId == R.id.menu_view_media) {
            handleViewMedia();
        } else if (itemId == R.id.menu_add_shortcut) {
            handleAddShortcut();
        } else if (itemId == R.id.menu_search) {
            handleSearch();
        } else if (itemId == R.id.menu_add_to_contacts) {
            handleAddToContacts();
        } else if (itemId == R.id.menu_group_recipients) {
            handleDisplayGroupRecipients();
        } else if (itemId == R.id.menu_distribution_broadcast) {
            handleDistributionBroadcastEnabled(menuItem);
        } else if (itemId == R.id.menu_distribution_conversation) {
            handleDistributionConversationEnabled(menuItem);
        } else if (itemId == R.id.menu_group_settings) {
            handleManageGroup();
        } else if (itemId == R.id.menu_leave) {
            handleLeavePushGroup();
        } else if (itemId == R.id.menu_invite) {
            handleInviteLink();
        } else if (itemId == R.id.menu_mute_notifications) {
            handleMuteNotifications();
        } else if (itemId == R.id.menu_unmute_notifications) {
            handleUnmuteNotifications();
        } else if (itemId == R.id.menu_conversation_settings) {
            handleConversationSettings();
        } else if (itemId == R.id.menu_expiring_messages_off || itemId == R.id.menu_expiring_messages) {
            handleSelectMessageExpiration();
        } else if (itemId == R.id.menu_create_bubble) {
            handleCreateBubble();
        } else if (itemId != 16908332) {
            return false;
        } else {
            requireActivity().finish();
        }
        return true;
    }

    public void onBackPressed() {
        Log.d(TAG, "onBackPressed()");
        if (this.reactionDelegate.isShowing()) {
            this.reactionDelegate.hide();
        } else if (this.container.isInputOpen()) {
            this.container.hideCurrentInput(this.composeText);
            this.navigationBarBackground.setVisibility(8);
        } else if (this.isSearchRequested) {
            MenuItem menuItem = this.searchViewItem;
            if (menuItem != null) {
                menuItem.collapseActionView();
            }
        } else {
            requireActivity().finish();
        }
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
    public void onKeyboardShown() {
        this.inputPanel.onKeyboardShown();
        if (this.emojiDrawerStub.resolved() && this.emojiDrawerStub.get().isShowing() && !this.emojiDrawerStub.get().isEmojiSearchMode()) {
            this.emojiDrawerStub.get().hide(true);
        }
        if (this.attachmentKeyboardStub.resolved() && this.attachmentKeyboardStub.get().isShowing()) {
            this.navigationBarBackground.setVisibility(8);
            this.attachmentKeyboardStub.get().hide(true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ReminderUpdateEvent reminderUpdateEvent) {
        updateReminders();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // org.thoughtcrime.securesms.conversation.AttachmentKeyboard.Callback
    public void onAttachmentMediaClicked(Media media) {
        this.linkPreviewViewModel.onUserCancel();
        startActivityForResult(MediaSelectionActivity.editor(requireActivity(), this.sendButton.getSelectedSendType(), Collections.singletonList(media), this.recipient.getId(), this.composeText.getTextTrimmed()), 12);
        this.container.hideCurrentInput(this.composeText);
    }

    @Override // org.thoughtcrime.securesms.conversation.AttachmentKeyboard.Callback
    public void onAttachmentSelectorClicked(AttachmentKeyboardButton attachmentKeyboardButton) {
        int i = AnonymousClass24.$SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton[attachmentKeyboardButton.ordinal()];
        if (i == 1) {
            AttachmentManager.selectGallery(this, 12, this.recipient.get(), this.composeText.getTextTrimmed(), this.sendButton.getSelectedSendType(), this.inputPanel.getQuote().isPresent());
        } else if (i == 2) {
            AttachmentManager.selectDocument(this, 2);
        } else if (i == 3) {
            AttachmentManager.selectContactInfo(this, 4);
        } else if (i == 4) {
            AttachmentManager.selectLocation(this, 9, getSendButtonColor(this.sendButton.getSelectedSendType()));
        } else if (i == 5) {
            if (ExpiringProfileCredentialUtil.isValid(this.recipient.get().getExpiringProfileKeyCredential())) {
                AttachmentManager.selectPayment(this, this.recipient.getId());
            } else {
                CanNotSendPaymentDialog.show(requireActivity());
            }
        }
        this.container.hideCurrentInput(this.composeText);
    }

    @Override // org.thoughtcrime.securesms.conversation.AttachmentKeyboard.Callback
    public void onAttachmentPermissionsRequested() {
        Permissions.with(this).request("android.permission.READ_EXTERNAL_STORAGE").onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda86
            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.this.lambda$onAttachmentPermissionsRequested$6();
            }
        }).withPermanentDenialDialog(getString(R.string.AttachmentManager_signal_requires_the_external_storage_permission_in_order_to_attach_photos_videos_or_audio)).execute();
    }

    public /* synthetic */ void lambda$onAttachmentPermissionsRequested$6() {
        this.viewModel.onAttachmentKeyboardOpen();
    }

    private void handleSelectMessageExpiration() {
        if (!isPushGroupConversation() || isActiveGroup()) {
            startActivity(RecipientDisappearingMessagesActivity.forRecipient(requireContext(), this.recipient.getId()));
        }
    }

    private void handleMuteNotifications() {
        MuteDialog.show(requireActivity(), new MuteDialog.MuteSelectionListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda83
            @Override // org.thoughtcrime.securesms.MuteDialog.MuteSelectionListener
            public final void onMuted(long j) {
                ConversationParentFragment.this.lambda$handleMuteNotifications$7(j);
            }
        });
    }

    public /* synthetic */ void lambda$handleMuteNotifications$7(final long j) {
        new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.6
            public Void doInBackground(Void... voidArr) {
                SignalDatabase.recipients().setMuted(ConversationParentFragment.this.recipient.getId(), j);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    private void handleStoryRingClick() {
        startActivity(StoryViewerActivity.createIntent(requireContext(), new StoryViewerArgs.Builder(this.recipient.getId(), this.recipient.get().shouldHideStory()).build()));
    }

    private void handleConversationSettings() {
        if (isGroupConversation()) {
            handleManageGroup();
        } else if (!isInMessageRequest() || this.recipient.get().isBlocked()) {
            ContextCompat.startActivity(requireActivity(), ConversationSettingsActivity.forRecipient(requireContext(), this.recipient.getId()), ConversationSettingsActivity.createTransitionBundle(requireActivity(), this.titleView.findViewById(R.id.contact_photo_image), this.toolbar));
        }
    }

    private void handleUnmuteNotifications() {
        new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.7
            public Void doInBackground(Void... voidArr) {
                SignalDatabase.recipients().setMuted(ConversationParentFragment.this.recipient.getId(), 0);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    private void handleUnblock() {
        BlockUnblockDialog.showUnblockFor(requireContext(), getLifecycle(), this.recipient.get(), new Runnable(requireContext().getApplicationContext()) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.this.lambda$handleUnblock$9(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$handleUnblock$9(Context context) {
        SignalExecutors.BOUNDED.execute(new Runnable(context) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda15
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.this.lambda$handleUnblock$8(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$handleUnblock$8(Context context) {
        RecipientUtil.unblock(context, this.recipient.get());
    }

    private void handleMakeDefaultSms() {
        startActivityForResult(SmsUtil.getSmsRoleIntent(requireContext()), 11);
    }

    private void handleRegisterForSignal() {
        startActivity(RegistrationNavigationActivity.newIntentForReRegistration(requireContext()));
    }

    private void handleInviteLink() {
        String string = getString(R.string.ConversationActivity_lets_switch_to_signal, getString(R.string.install_url));
        if (this.viewModel.isDefaultSmsApplication()) {
            this.composeText.appendInvite(string);
            return;
        }
        Intent intent = new Intent("android.intent.action.SENDTO");
        intent.setData(Uri.parse("smsto:" + this.recipient.get().requireSmsAddress()));
        intent.putExtra("sms_body", string);
        intent.putExtra("android.intent.extra.TEXT", string);
        startActivity(intent);
    }

    private void handleViewMedia() {
        startActivity(MediaOverviewActivity.forThread(requireContext(), this.threadId));
    }

    private void handleAddShortcut() {
        String str = TAG;
        Log.i(str, "Creating home screen shortcut for recipient " + this.recipient.get().getId());
        final Context applicationContext = requireContext().getApplicationContext();
        final Recipient recipient = this.recipient.get();
        if (this.pinnedShortcutReceiver == null) {
            this.pinnedShortcutReceiver = new BroadcastReceiver() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.8
                @Override // android.content.BroadcastReceiver
                public void onReceive(Context context, Intent intent) {
                    Toast.makeText(context, context.getString(R.string.ConversationActivity_added_to_home_screen), 1).show();
                }
            };
            requireActivity().registerReceiver(this.pinnedShortcutReceiver, new IntentFilter(ACTION_PINNED_SHORTCUT));
        }
        GlideApp.with(this).asBitmap().load((Object) recipient.getContactPhoto()).error(recipient.getFallbackContactPhoto().asDrawable(applicationContext, recipient.getAvatarColor(), false)).into((GlideRequest<Bitmap>) new CustomTarget<Bitmap>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.9
            @Override // com.bumptech.glide.request.target.Target
            public void onLoadCleared(Drawable drawable) {
            }

            @Override // com.bumptech.glide.request.target.Target
            public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                onResourceReady((Bitmap) obj, (Transition<? super Bitmap>) transition);
            }

            @Override // com.bumptech.glide.request.target.CustomTarget, com.bumptech.glide.request.target.Target
            public void onLoadFailed(Drawable drawable) {
                if (drawable != null) {
                    String str2 = ConversationParentFragment.TAG;
                    Log.w(str2, "Utilizing fallback photo for shortcut for recipient " + recipient.getId());
                    SimpleTask.run(new ConversationParentFragment$9$$ExternalSyntheticLambda2(drawable), new ConversationParentFragment$9$$ExternalSyntheticLambda3(applicationContext, recipient));
                    return;
                }
                throw new AssertionError();
            }

            public static /* synthetic */ Bitmap lambda$onLoadFailed$0(Drawable drawable) {
                return DrawableUtil.toBitmap(drawable, ConversationParentFragment.SHORTCUT_ICON_SIZE, ConversationParentFragment.SHORTCUT_ICON_SIZE);
            }

            public static /* synthetic */ Bitmap lambda$onResourceReady$2(Bitmap bitmap) {
                return BitmapUtil.createScaledBitmap(bitmap, ConversationParentFragment.SHORTCUT_ICON_SIZE, ConversationParentFragment.SHORTCUT_ICON_SIZE);
            }

            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                SimpleTask.run(new ConversationParentFragment$9$$ExternalSyntheticLambda0(bitmap), new ConversationParentFragment$9$$ExternalSyntheticLambda1(applicationContext, recipient));
            }
        });
    }

    private void handleCreateBubble() {
        ConversationIntents.Args args = this.viewModel.getArgs();
        BubbleUtil.displayAsBubble(requireContext(), args.getRecipientId(), args.getThreadId());
        requireActivity().finish();
    }

    public static void addIconToHomeScreen(Context context, Bitmap bitmap, Recipient recipient) {
        String str;
        IconCompat createWithAdaptiveBitmap = IconCompat.createWithAdaptiveBitmap(bitmap);
        if (recipient.isSelf()) {
            str = context.getString(R.string.note_to_self);
        } else {
            str = recipient.getDisplayName(context);
        }
        ShortcutManagerCompat.requestPinShortcut(context, new ShortcutInfoCompat.Builder(context, recipient.getId().serialize() + '-' + System.currentTimeMillis()).setShortLabel(str).setIcon(createWithAdaptiveBitmap).setIntent(ShortcutLauncherActivity.createIntent(context, recipient.getId())).build(), PendingIntent.getBroadcast(context, 902, new Intent(ACTION_PINNED_SHORTCUT), 0).getIntentSender());
        bitmap.recycle();
    }

    private void handleSearch() {
        this.searchViewModel.onSearchOpened();
    }

    private void handleLeavePushGroup() {
        if (getRecipient() == null) {
            Toast.makeText(requireContext(), getString(R.string.ConversationActivity_invalid_recipient), 1).show();
        } else {
            LeaveGroupDialog.handleLeavePushGroup(requireActivity(), getRecipient().requireGroupId().requirePush(), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda91
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationParentFragment.this.lambda$handleLeavePushGroup$10();
                }
            });
        }
    }

    public /* synthetic */ void lambda$handleLeavePushGroup$10() {
        requireActivity().finish();
    }

    private void handleManageGroup() {
        ContextCompat.startActivity(requireContext(), ConversationSettingsActivity.forGroup(requireContext(), this.recipient.get().requireGroupId()), ConversationSettingsActivity.createTransitionBundle(requireContext(), this.titleView.findViewById(R.id.contact_photo_image), this.toolbar));
    }

    private void handleDistributionBroadcastEnabled(MenuItem menuItem) {
        this.distributionType = 1;
        menuItem.setChecked(true);
        if (this.threadId != -1) {
            new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.10
                public Void doInBackground(Void... voidArr) {
                    SignalDatabase.threads().setDistributionType(ConversationParentFragment.this.threadId, 1);
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    private void handleDistributionConversationEnabled(MenuItem menuItem) {
        this.distributionType = 2;
        menuItem.setChecked(true);
        if (this.threadId != -1) {
            new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.11
                public Void doInBackground(Void... voidArr) {
                    SignalDatabase.threads().setDistributionType(ConversationParentFragment.this.threadId, 2);
                    return null;
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        }
    }

    private void handleDial(Recipient recipient, boolean z) {
        if (recipient != null) {
            if (z) {
                CommunicationActions.startVoiceCall(requireActivity(), recipient);
            } else {
                CommunicationActions.startInsecureCall(requireActivity(), recipient);
            }
        }
    }

    private void handleVideo(Recipient recipient) {
        if (recipient != null) {
            if (!recipient.isPushV2Group() || this.groupCallViewModel.hasActiveGroupCall().getValue() != Boolean.FALSE || !this.groupViewModel.isNonAdminInAnnouncementGroup()) {
                CommunicationActions.startVideoCall(requireActivity(), recipient);
            } else {
                new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.ConversationActivity_cant_start_group_call).setMessage(R.string.ConversationActivity_only_admins_of_this_group_can_start_a_call).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda81
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                }).show();
            }
        }
    }

    private void handleDisplayGroupRecipients() {
        new GroupMembersDialog(requireActivity(), getRecipient()).display();
    }

    private void handleAddToContacts() {
        if (!this.recipient.get().isGroup()) {
            try {
                startActivityForResult(RecipientExporter.export(this.recipient.get()).asAddContactIntent(), 8);
            } catch (ActivityNotFoundException e) {
                Log.w(TAG, e);
            }
        }
    }

    private boolean handleDisplayQuickContact() {
        if (isInMessageRequest() || this.recipient.get().isGroup()) {
            return false;
        }
        if (this.recipient.get().getContactUri() != null) {
            ContactsContract.QuickContact.showQuickContact(requireContext(), this.titleView, this.recipient.get().getContactUri(), 3, (String[]) null);
            return true;
        }
        handleAddToContacts();
        return true;
    }

    public void handleAddAttachment() {
        if (this.viewModel.getConversationStateSnapshot().isMmsEnabled() || this.viewModel.isPushAvailable()) {
            this.viewModel.getRecentMedia().removeObservers(this);
            if (!this.attachmentKeyboardStub.resolved() || !this.container.isInputOpen() || this.container.getCurrentInput() != this.attachmentKeyboardStub.get()) {
                this.viewModel.getRecentMedia().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda75
                    @Override // androidx.lifecycle.Observer
                    public final void onChanged(Object obj) {
                        ConversationParentFragment.this.lambda$handleAddAttachment$12((List) obj);
                    }
                });
                this.attachmentKeyboardStub.get().setCallback(this);
                this.attachmentKeyboardStub.get().setWallpaperEnabled(this.recipient.get().hasWallpaper());
                updatePaymentsAvailable();
                this.container.show(this.composeText, this.attachmentKeyboardStub.get());
                this.navigationBarBackground.setVisibility(0);
                this.viewModel.onAttachmentKeyboardOpen();
                return;
            }
            this.container.showSoftkey(this.composeText);
            return;
        }
        handleManualMmsRequired();
    }

    public /* synthetic */ void lambda$handleAddAttachment$12(List list) {
        this.attachmentKeyboardStub.get().onMediaChanged(list);
    }

    private void updatePaymentsAvailable() {
        if (this.attachmentKeyboardStub.resolved()) {
            if (!SignalStore.paymentsValues().getPaymentsAvailability().isSendAllowed() || this.recipient.get().isSelf() || this.recipient.get().isGroup() || !this.recipient.get().isRegistered() || this.recipient.get().isForceSmsSelection()) {
                this.attachmentKeyboardStub.get().filterAttachmentKeyboardButtons(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda73
                    @Override // com.annimon.stream.function.Predicate
                    public final boolean test(Object obj) {
                        return ConversationParentFragment.lambda$updatePaymentsAvailable$13((AttachmentKeyboardButton) obj);
                    }
                });
            } else {
                this.attachmentKeyboardStub.get().filterAttachmentKeyboardButtons(null);
            }
        }
    }

    public static /* synthetic */ boolean lambda$updatePaymentsAvailable$13(AttachmentKeyboardButton attachmentKeyboardButton) {
        return attachmentKeyboardButton != AttachmentKeyboardButton.PAYMENT;
    }

    private void handleManualMmsRequired() {
        Toast.makeText(requireContext(), (int) R.string.MmsDownloader_error_reading_mms_settings, 1).show();
        Bundle requireArguments = requireArguments();
        Intent intent = new Intent(requireContext(), PromptMmsActivity.class);
        intent.putExtras(requireArguments);
        startActivity(intent);
    }

    private void handleRecentSafetyNumberChange() {
        List<IdentityRecord> unverifiedRecords = this.identityRecords.getUnverifiedRecords();
        unverifiedRecords.addAll(this.identityRecords.getUntrustedRecords());
        SafetyNumberBottomSheet.forIdentityRecordsAndDestination(unverifiedRecords, new ContactSearchKey.RecipientSearchKey.KnownRecipient(this.recipient.getId())).show(getChildFragmentManager());
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onMessageResentAfterSafetyNumberChangeInBottomSheet() {
        Log.d(TAG, "onMessageResentAfterSafetyNumberChange");
        initializeIdentityRecords().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.12
            public void onSuccess(Boolean bool) {
            }
        });
    }

    public void handleSecurityChange(ConversationSecurityInfo conversationSecurityInfo) {
        String str = TAG;
        Log.i(str, "handleSecurityChange(" + conversationSecurityInfo + ")");
        boolean isPushAvailable = conversationSecurityInfo.isPushAvailable();
        boolean z = false;
        this.sendButton.resetAvailableTransports(this.recipient.get().isMmsGroup() || this.attachmentManager.isAttachmentPresent());
        if (this.recipient.get().isPushGroup() || (!this.recipient.get().isMmsGroup() && !this.recipient.get().hasSmsAddress())) {
            this.sendButton.disableTransportType(MessageSendType.TransportType.SMS);
        } else {
            z = true;
        }
        if (!isPushAvailable && !isPushGroupConversation() && !this.recipient.get().isServiceIdOnly() && !this.recipient.get().isReleaseNotes() && z) {
            this.sendButton.disableTransportType(MessageSendType.TransportType.SIGNAL);
        }
        if (!this.recipient.get().isPushGroup() && this.recipient.get().isForceSmsSelection() && z) {
            this.sendButton.setDefaultTransport(MessageSendType.TransportType.SMS);
        } else if (isPushAvailable || isPushGroupConversation() || this.recipient.get().isServiceIdOnly() || this.recipient.get().isReleaseNotes() || !z) {
            this.sendButton.setDefaultTransport(MessageSendType.TransportType.SIGNAL);
        } else {
            this.sendButton.setDefaultTransport(MessageSendType.TransportType.SMS);
        }
        calculateCharactersRemaining();
        invalidateOptionsMenu();
        setBlockedUserState(this.recipient.get(), conversationSecurityInfo);
        onSecurityUpdated();
    }

    private ListenableFuture<Boolean> initializeDraft(ConversationIntents.Args args) {
        SettableFuture settableFuture = new SettableFuture();
        if (this.hasProcessedShareData) {
            Log.d(TAG, "Already processed this share data. Skipping.");
            settableFuture.set(Boolean.FALSE);
            return settableFuture;
        }
        String draftText = args.getDraftText();
        Uri intentData = ConversationIntents.getIntentData(requireArguments());
        String intentType = ConversationIntents.getIntentType(requireArguments());
        SlideFactory.MediaType from = SlideFactory.MediaType.from(intentType);
        ArrayList<Media> media = args.getMedia();
        StickerLocator stickerLocator = args.getStickerLocator();
        boolean isBorderless = args.isBorderless();
        this.hasProcessedShareData = true;
        if (stickerLocator != null && intentData != null) {
            Log.d(TAG, "Handling shared sticker.");
            Objects.requireNonNull(intentType);
            sendSticker(stickerLocator, intentType, intentData, 0, true);
            return new SettableFuture(Boolean.FALSE);
        } else if (intentData != null && intentType != null && isBorderless) {
            String str = TAG;
            Log.d(str, "Handling borderless draft media with content type " + intentType);
            SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(intentData) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda23
                public final /* synthetic */ Uri f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return ConversationParentFragment.this.lambda$initializeDraft$14(this.f$1);
                }
            }, new SimpleTask.ForegroundTask(intentData, intentType) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda24
                public final /* synthetic */ Uri f$1;
                public final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    ConversationParentFragment.this.lambda$initializeDraft$15(this.f$1, this.f$2, (ConversationParentFragment.KeyboardImageDetails) obj);
                }
            });
            return new SettableFuture(Boolean.FALSE);
        } else if (!Util.isEmpty(media)) {
            Log.d(TAG, "Handling shared Media.");
            startActivityForResult(MediaSelectionActivity.editor(requireContext(), this.sendButton.getSelectedSendType(), media, this.recipient.getId(), draftText), 12);
            return new SettableFuture(Boolean.FALSE);
        } else {
            if (draftText != null) {
                Log.d(TAG, "Handling shared text");
                this.composeText.setText("");
                this.composeText.append(draftText);
                settableFuture.set(Boolean.TRUE);
            }
            if (intentData != null && from != null) {
                Log.d(TAG, "Handling shared Data.");
                return setMedia(intentData, from);
            } else if (draftText == null && intentData == null && from == null) {
                Log.d(TAG, "Initializing draft from database");
                return initializeDraftFromDatabase();
            } else {
                updateToggleButtonState();
                settableFuture.set(Boolean.FALSE);
                return settableFuture;
            }
        }
    }

    private void initializeEnabledCheck() {
        this.groupViewModel.getSelfMemberLevel().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda71
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeEnabledCheck$18((ConversationGroupViewModel.ConversationMemberLevel) obj);
            }
        });
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x004b A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x009d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void lambda$initializeEnabledCheck$18(org.thoughtcrime.securesms.conversation.ConversationGroupViewModel.ConversationMemberLevel r9) {
        /*
        // Method dump skipped, instructions count: 230
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationParentFragment.lambda$initializeEnabledCheck$18(org.thoughtcrime.securesms.conversation.ConversationGroupViewModel$ConversationMemberLevel):void");
    }

    /* renamed from: org.thoughtcrime.securesms.conversation.ConversationParentFragment$24 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass24 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$RequestReviewDisplayState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$util$TextSecurePreferences$MediaKeyboardMode;

        static {
            int[] iArr = new int[GroupDatabase.MemberLevel.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel = iArr;
            try {
                iArr[GroupDatabase.MemberLevel.NOT_A_MEMBER.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel[GroupDatabase.MemberLevel.PENDING_MEMBER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel[GroupDatabase.MemberLevel.REQUESTING_MEMBER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel[GroupDatabase.MemberLevel.FULL_MEMBER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$GroupDatabase$MemberLevel[GroupDatabase.MemberLevel.ADMINISTRATOR.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            int[] iArr2 = new int[MessageRequestViewModel.Status.values().length];
            $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status = iArr2;
            try {
                iArr2[MessageRequestViewModel.Status.IDLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.ACCEPTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.BLOCKING.ordinal()] = 3;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.DELETING.ordinal()] = 4;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.ACCEPTED.ordinal()] = 5;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.BLOCKED_AND_REPORTED.ordinal()] = 6;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.DELETED.ordinal()] = 7;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[MessageRequestViewModel.Status.BLOCKED.ordinal()] = 8;
            } catch (NoSuchFieldError unused13) {
            }
            int[] iArr3 = new int[MessageRequestViewModel.RequestReviewDisplayState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$RequestReviewDisplayState = iArr3;
            try {
                iArr3[MessageRequestViewModel.RequestReviewDisplayState.SHOWN.ordinal()] = 1;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$RequestReviewDisplayState[MessageRequestViewModel.RequestReviewDisplayState.HIDDEN.ordinal()] = 2;
            } catch (NoSuchFieldError unused15) {
            }
            int[] iArr4 = new int[TextSecurePreferences.MediaKeyboardMode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$util$TextSecurePreferences$MediaKeyboardMode = iArr4;
            try {
                iArr4[TextSecurePreferences.MediaKeyboardMode.EMOJI.ordinal()] = 1;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$TextSecurePreferences$MediaKeyboardMode[TextSecurePreferences.MediaKeyboardMode.STICKER.ordinal()] = 2;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$util$TextSecurePreferences$MediaKeyboardMode[TextSecurePreferences.MediaKeyboardMode.GIF.ordinal()] = 3;
            } catch (NoSuchFieldError unused18) {
            }
            int[] iArr5 = new int[AttachmentKeyboardButton.values().length];
            $SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton = iArr5;
            try {
                iArr5[AttachmentKeyboardButton.GALLERY.ordinal()] = 1;
            } catch (NoSuchFieldError unused19) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton[AttachmentKeyboardButton.FILE.ordinal()] = 2;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton[AttachmentKeyboardButton.CONTACT.ordinal()] = 3;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton[AttachmentKeyboardButton.LOCATION.ordinal()] = 4;
            } catch (NoSuchFieldError unused22) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$AttachmentKeyboardButton[AttachmentKeyboardButton.PAYMENT.ordinal()] = 5;
            } catch (NoSuchFieldError unused23) {
            }
        }
    }

    public /* synthetic */ void lambda$initializeEnabledCheck$16(View view) {
        ShowAdminsBottomSheetDialog.show(getChildFragmentManager(), getRecipient().requireGroupId().requireV2());
    }

    public /* synthetic */ void lambda$initializeEnabledCheck$17(View view) {
        ConversationGroupViewModel.onCancelJoinRequest(getRecipient(), new AsynchronousCallback.MainThread<Void, GroupChangeFailureReason>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.13
            @Override // org.thoughtcrime.securesms.util.AsynchronousCallback.MainThread
            public /* synthetic */ AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> toWorkerCallback() {
                return AsynchronousCallback.MainThread.CC.$default$toWorkerCallback(this);
            }

            public void onComplete(Void r2) {
                Log.d(ConversationParentFragment.TAG, "Cancel request complete");
            }

            public void onError(GroupChangeFailureReason groupChangeFailureReason) {
                String str = ConversationParentFragment.TAG;
                Log.d(str, "Cancel join request failed " + groupChangeFailureReason);
                Toast.makeText(ConversationParentFragment.this.requireContext(), GroupErrors.getUserDisplayMessage(groupChangeFailureReason), 0).show();
            }
        }.toWorkerCallback());
    }

    private void initializePendingRequestsBanner() {
        this.groupViewModel.getActionableRequestingMembers().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda107
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializePendingRequestsBanner$19((Integer) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializePendingRequestsBanner$19(Integer num) {
        updateReminders();
    }

    private void initializeGroupV1MigrationsBanners() {
        this.groupViewModel.getGroupV1MigrationSuggestions().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda98
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeGroupV1MigrationsBanners$20((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeGroupV1MigrationsBanners$20(List list) {
        updateReminders();
    }

    private ListenableFuture<Boolean> initializeDraftFromDatabase() {
        final SettableFuture settableFuture = new SettableFuture();
        final Context applicationContext = requireContext().getApplicationContext();
        new AsyncTask<Void, Void, Pair<DraftDatabase.Drafts, CharSequence>>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.14
            public Pair<DraftDatabase.Drafts, CharSequence> doInBackground(Void... voidArr) {
                SpannableString spannableString;
                DraftDatabase drafts = SignalDatabase.drafts();
                DraftDatabase.Drafts drafts2 = drafts.getDrafts(ConversationParentFragment.this.threadId);
                DraftDatabase.Draft draftOfType = drafts2.getDraftOfType("mention");
                if (draftOfType != null) {
                    MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(applicationContext, drafts2.getDraftOfType(DraftDatabase.Draft.TEXT).getValue(), MentionUtil.bodyRangeListToMentions(applicationContext, Base64.decodeOrThrow(draftOfType.getValue())));
                    spannableString = new SpannableString(updateBodyAndMentionsWithDisplayNames.getBody());
                    MentionAnnotation.setMentionAnnotations(spannableString, updateBodyAndMentionsWithDisplayNames.getMentions());
                } else {
                    spannableString = null;
                }
                drafts.clearDrafts(ConversationParentFragment.this.threadId);
                return new Pair<>(drafts2, spannableString);
            }

            /* JADX WARNING: Removed duplicated region for block: B:54:0x009f A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:55:0x00b3 A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:56:0x00cd A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:57:0x00e2 A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:58:0x00f7 A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:59:0x010c A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:60:0x0129 A[SYNTHETIC] */
            /* JADX WARNING: Removed duplicated region for block: B:61:0x009e A[SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onPostExecute(org.signal.libsignal.protocol.util.Pair<org.thoughtcrime.securesms.database.DraftDatabase.Drafts, java.lang.CharSequence> r8) {
                /*
                // Method dump skipped, instructions count: 384
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationParentFragment.AnonymousClass14.onPostExecute(org.signal.libsignal.protocol.util.Pair):void");
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        return settableFuture;
    }

    private void onSecurityUpdated() {
        Log.i(TAG, "onSecurityUpdated()");
        updateReminders();
        updateDefaultSubscriptionId(this.recipient.get().getDefaultSubscriptionId());
    }

    private void initializeInsightObserver() {
        InviteReminderModel inviteReminderModel = new InviteReminderModel(requireContext(), new InviteReminderRepository(requireContext()));
        this.inviteReminderModel = inviteReminderModel;
        inviteReminderModel.loadReminder(this.recipient, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda108
            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.this.updateReminders();
            }
        });
    }

    public void updateReminders() {
        Context context = getContext();
        if (!this.callback.onUpdateReminders() && context != null) {
            Optional<Reminder> reminder = this.inviteReminderModel.getReminder();
            Integer value = this.groupViewModel.getActionableRequestingMembers().getValue();
            List<RecipientId> value2 = this.groupViewModel.getGroupV1MigrationSuggestions().getValue();
            if (UnauthorizedReminder.isEligible(context)) {
                this.reminderView.get().showReminder(new UnauthorizedReminder(context));
            } else if (ExpiredBuildReminder.isEligible()) {
                this.reminderView.get().showReminder(new ExpiredBuildReminder(context));
                this.reminderView.get().setOnActionClickListener(new ReminderView.OnActionClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda7
                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
                    public final void onActionClick(int i) {
                        ConversationParentFragment.this.handleReminderAction(i);
                    }
                });
            } else if (ServiceOutageReminder.isEligible(context)) {
                ApplicationDependencies.getJobManager().add(new ServiceOutageDetectionJob());
                this.reminderView.get().showReminder(new ServiceOutageReminder(context));
            } else if (SignalStore.account().isRegistered() && TextSecurePreferences.isShowInviteReminders(context) && !this.viewModel.isPushAvailable() && reminder.isPresent() && !this.recipient.get().isGroup()) {
                this.reminderView.get().setOnActionClickListener(new ReminderView.OnActionClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda7
                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
                    public final void onActionClick(int i) {
                        ConversationParentFragment.this.handleReminderAction(i);
                    }
                });
                this.reminderView.get().setOnDismissListener(new ReminderView.OnDismissListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda8
                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnDismissListener
                    public final void onDismiss() {
                        ConversationParentFragment.this.lambda$updateReminders$21();
                    }
                });
                this.reminderView.get().showReminder(reminder.get());
            } else if (value != null && value.intValue() > 0) {
                this.reminderView.get().showReminder(PendingGroupJoinRequestsReminder.create(context, value.intValue()));
                this.reminderView.get().setOnActionClickListener(new ReminderView.OnActionClickListener(context) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda9
                    public final /* synthetic */ Context f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
                    public final void onActionClick(int i) {
                        ConversationParentFragment.this.lambda$updateReminders$22(this.f$1, i);
                    }
                });
            } else if (value2 != null && value2.size() > 0 && this.recipient.get().isPushV2Group()) {
                this.reminderView.get().showReminder(new GroupsV1MigrationSuggestionsReminder(context, value2));
                this.reminderView.get().setOnActionClickListener(new ReminderView.OnActionClickListener(value2) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda10
                    public final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
                    public final void onActionClick(int i) {
                        ConversationParentFragment.this.lambda$updateReminders$23(this.f$1, i);
                    }
                });
                this.reminderView.get().setOnDismissListener(new ReminderView.OnDismissListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda11
                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnDismissListener
                    public final void onDismiss() {
                        ConversationParentFragment.lambda$updateReminders$24();
                    }
                });
            } else if (isInBubble() && !SignalStore.tooltips().hasSeenBubbleOptOutTooltip() && Build.VERSION.SDK_INT > 29) {
                this.reminderView.get().showReminder(new BubbleOptOutReminder(context));
                this.reminderView.get().setOnActionClickListener(new ReminderView.OnActionClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda12
                    @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
                    public final void onActionClick(int i) {
                        ConversationParentFragment.this.lambda$updateReminders$25(i);
                    }
                });
            } else if (this.reminderView.resolved()) {
                this.reminderView.get().hide();
            }
        }
    }

    public /* synthetic */ void lambda$updateReminders$21() {
        this.inviteReminderModel.dismissReminder();
    }

    public /* synthetic */ void lambda$updateReminders$22(Context context, int i) {
        if (i == R.id.reminder_action_review_join_requests) {
            startActivity(ManagePendingAndRequestingMembersActivity.newIntent(context, getRecipient().getGroupId().get().requireV2()));
        }
    }

    public /* synthetic */ void lambda$updateReminders$23(List list, int i) {
        if (i == R.id.reminder_action_gv1_suggestion_add_members) {
            GroupsV1MigrationSuggestionsDialog.show(requireActivity(), this.recipient.get().requireGroupId().requireV2(), list);
        } else if (i == R.id.reminder_action_gv1_suggestion_no_thanks) {
            this.groupViewModel.onSuggestedMembersBannerDismissed(this.recipient.get().requireGroupId(), list);
        }
    }

    public /* synthetic */ void lambda$updateReminders$25(int i) {
        SignalStore.tooltips().markBubbleOptOutTooltipSeen();
        this.reminderView.get().hide();
        if (i == R.id.reminder_action_turn_off) {
            startActivity(new Intent("android.settings.APP_NOTIFICATION_BUBBLE_SETTINGS").putExtra("android.provider.extra.APP_PACKAGE", requireContext().getPackageName()).addFlags(SQLiteDatabase.CREATE_IF_NECESSARY));
        }
    }

    public void handleReminderAction(int i) {
        if (i == R.id.reminder_action_invite) {
            handleInviteLink();
            this.reminderView.get().requestDismiss();
        } else if (i == R.id.reminder_action_view_insights) {
            InsightsLauncher.showInsightsDashboard(getChildFragmentManager());
        } else if (i == R.id.reminder_action_update_now) {
            PlayStoreUtil.openPlayStoreOrOurApkDownloadPage(requireContext());
        } else {
            throw new IllegalArgumentException("Unknown ID: " + i);
        }
    }

    private void updateDefaultSubscriptionId(Optional<Integer> optional) {
        String str = TAG;
        Log.i(str, "updateDefaultSubscriptionId(" + optional.orElse(null) + ")");
        this.sendButton.setDefaultSubscriptionId(optional.orElse(null));
    }

    public ListenableFuture<Boolean> initializeIdentityRecords() {
        final SettableFuture settableFuture = new SettableFuture();
        final Context applicationContext = requireContext().getApplicationContext();
        if (SignalStore.account().getAci() == null || SignalStore.account().getPni() == null) {
            Log.w(TAG, "Not registered! Skipping initializeIdentityRecords()");
            settableFuture.set(Boolean.FALSE);
            return settableFuture;
        }
        new AsyncTask<Recipient, Void, Pair<IdentityRecordList, String>>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.15
            public Pair<IdentityRecordList, String> doInBackground(Recipient... recipientArr) {
                List<Recipient> list;
                if (recipientArr[0].isGroup()) {
                    list = SignalDatabase.groups().getGroupMembers(recipientArr[0].requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF);
                } else {
                    list = Collections.singletonList(recipientArr[0]);
                }
                long currentTimeMillis = System.currentTimeMillis();
                IdentityRecordList identityRecords = ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecords(list);
                Log.i(ConversationParentFragment.TAG, String.format(Locale.US, "Loaded %d identities in %d ms", Integer.valueOf(list.size()), Long.valueOf(System.currentTimeMillis() - currentTimeMillis)));
                String str = null;
                if (identityRecords.isUnverified()) {
                    str = IdentityUtil.getUnverifiedBannerDescription(applicationContext, identityRecords.getUnverifiedRecipients());
                }
                return new Pair<>(identityRecords, str);
            }

            public void onPostExecute(Pair<IdentityRecordList, String> pair) {
                String str = ConversationParentFragment.TAG;
                Log.i(str, "Got identity records: " + pair.first().isUnverified());
                ConversationParentFragment.this.identityRecords = pair.first();
                if (pair.second() != null) {
                    Log.d(ConversationParentFragment.TAG, "Replacing banner...");
                    ((UnverifiedBannerView) ConversationParentFragment.this.unverifiedBannerView.get()).display(pair.second(), pair.first().getUnverifiedRecords(), new UnverifiedClickedListener(), new UnverifiedDismissedListener());
                } else if (ConversationParentFragment.this.unverifiedBannerView.resolved()) {
                    Log.d(ConversationParentFragment.TAG, "Clearing banner...");
                    ((UnverifiedBannerView) ConversationParentFragment.this.unverifiedBannerView.get()).hide();
                }
                ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
                conversationParentFragment.titleView.setVerified(conversationParentFragment.viewModel.isPushAvailable() && ConversationParentFragment.this.identityRecords.isVerified() && !ConversationParentFragment.this.recipient.get().isSelf());
                settableFuture.set(Boolean.TRUE);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, this.recipient.get());
        return settableFuture;
    }

    private void initializeViews(View view) {
        this.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.toolbarBackground = view.findViewById(R.id.toolbar_background);
        this.titleView = (ConversationTitleView) view.findViewById(R.id.conversation_title_view);
        this.buttonToggle = (AnimatingToggle) view.findViewById(R.id.button_toggle);
        this.sendButton = (SendButton) view.findViewById(R.id.send_button);
        this.attachButton = (ImageButton) view.findViewById(R.id.attach_button);
        this.composeText = (ComposeText) view.findViewById(R.id.embedded_text_editor);
        this.charactersLeft = (TextView) view.findViewById(R.id.space_left);
        this.emojiDrawerStub = ViewUtil.findStubById(view, (int) R.id.emoji_drawer_stub);
        this.attachmentKeyboardStub = ViewUtil.findStubById(view, (int) R.id.attachment_keyboard_stub);
        this.unblockButton = (Button) view.findViewById(R.id.unblock_button);
        this.makeDefaultSmsButton = (Button) view.findViewById(R.id.make_default_sms_button);
        this.registerButton = (Button) view.findViewById(R.id.register_button);
        this.container = (InputAwareLayout) view.findViewById(R.id.layout_container);
        this.reminderView = ViewUtil.findStubById(view, (int) R.id.reminder_stub);
        this.unverifiedBannerView = ViewUtil.findStubById(view, (int) R.id.unverified_banner_stub);
        this.reviewBanner = ViewUtil.findStubById(view, (int) R.id.review_banner_stub);
        this.quickAttachmentToggle = (HidingLinearLayout) view.findViewById(R.id.quick_attachment_toggle);
        this.inlineAttachmentToggle = (HidingLinearLayout) view.findViewById(R.id.inline_attachment_container);
        this.inputPanel = (InputPanel) view.findViewById(R.id.bottom_panel);
        this.searchNav = (ConversationSearchBottomBar) view.findViewById(R.id.conversation_search_nav);
        this.messageRequestBottomView = (MessageRequestsBottomView) view.findViewById(R.id.conversation_activity_message_request_bottom_bar);
        this.mentionsSuggestions = ViewUtil.findStubById(view, (int) R.id.conversation_mention_suggestions_stub);
        this.wallpaper = (ImageView) view.findViewById(R.id.conversation_wallpaper);
        this.wallpaperDim = view.findViewById(R.id.conversation_wallpaper_dim);
        this.voiceNotePlayerViewStub = ViewUtil.findStubById(view, (int) R.id.voice_note_player_stub);
        this.navigationBarBackground = view.findViewById(R.id.navbar_background);
        ImageButton imageButton = (ImageButton) view.findViewById(R.id.quick_camera_toggle);
        ImageButton imageButton2 = (ImageButton) view.findViewById(R.id.inline_attachment_button);
        this.reactionDelegate = new ConversationReactionDelegate(ViewUtil.findStubById(view, (int) R.id.conversation_reaction_scrubber_stub));
        this.noLongerMemberBanner = view.findViewById(R.id.conversation_no_longer_member_banner);
        this.cannotSendInAnnouncementGroupBanner = ViewUtil.findStubById(view, (int) R.id.conversation_cannot_send_announcement_stub);
        this.requestingMemberBanner = view.findViewById(R.id.conversation_requesting_banner);
        this.cancelJoinRequest = view.findViewById(R.id.conversation_cancel_request);
        this.releaseChannelUnmute = ViewUtil.findStubById(view, (int) R.id.conversation_release_notes_unmute_stub);
        this.joinGroupCallButton = (MaterialButton) view.findViewById(R.id.conversation_group_call_join);
        this.sendButton.setPopupContainer((ViewGroup) view);
        this.container.setIsBubble(isInBubble());
        this.container.addOnKeyboardShownListener(this);
        this.inputPanel.setListener(this);
        this.inputPanel.setMediaListener(this);
        this.attachmentManager = new AttachmentManager(requireActivity(), this);
        this.audioRecorder = new AudioRecorder(requireContext());
        this.typingTextWatcher = new TypingStatusTextWatcher();
        SendButtonListener sendButtonListener = new SendButtonListener();
        ComposeKeyPressedListener composeKeyPressedListener = new ComposeKeyPressedListener();
        this.composeText.setOnEditorActionListener(sendButtonListener);
        this.composeText.setCursorPositionChangedListener(this);
        this.attachButton.setOnClickListener(new AttachButtonListener());
        this.attachButton.setOnLongClickListener(new AttachButtonLongClickListener());
        this.sendButton.setOnClickListener(sendButtonListener);
        this.sendButton.setEnabled(true);
        this.sendButton.addOnSelectionChangedListener(new SendButton.SendTypeChangedListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda25
            @Override // org.thoughtcrime.securesms.components.SendButton.SendTypeChangedListener
            public final void onSendTypeChanged(MessageSendType messageSendType, boolean z) {
                ConversationParentFragment.this.lambda$initializeViews$26(messageSendType, z);
            }
        });
        this.titleView.setOnStoryRingClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda26
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$27(view2);
            }
        });
        this.titleView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda27
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$28(view2);
            }
        });
        this.titleView.setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda28
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view2) {
                return ConversationParentFragment.this.lambda$initializeViews$29(view2);
            }
        });
        this.unblockButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda29
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$30(view2);
            }
        });
        this.makeDefaultSmsButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda30
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$31(view2);
            }
        });
        this.registerButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda31
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$32(view2);
            }
        });
        this.composeText.setOnKeyListener(composeKeyPressedListener);
        this.composeText.addTextChangedListener(composeKeyPressedListener);
        this.composeText.setOnEditorActionListener(sendButtonListener);
        this.composeText.setOnClickListener(composeKeyPressedListener);
        this.composeText.setOnFocusChangeListener(composeKeyPressedListener);
        if (Camera.getNumberOfCameras() > 0) {
            imageButton.setVisibility(0);
            imageButton.setOnClickListener(new QuickCameraToggleListener());
        } else {
            imageButton.setVisibility(8);
        }
        this.searchNav.setEventListener(this);
        imageButton2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda32
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$33(view2);
            }
        });
        this.reactionDelegate.setOnReactionSelectedListener(this);
        this.joinGroupCallButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda33
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ConversationParentFragment.this.lambda$initializeViews$34(view2);
            }
        });
        this.voiceNoteMediaController.getVoiceNotePlayerViewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda34
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeViews$35((Optional) obj);
            }
        });
        this.voiceNoteMediaController.getVoiceNotePlaybackState().observe(getViewLifecycleOwner(), this.inputPanel.getPlaybackStateObserver());
        this.material3OnScrollHelper = new Material3OnScrollHelper(requireActivity(), Collections.singletonList(this.toolbarBackground), Collections.emptyList()) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.16
            @Override // org.thoughtcrime.securesms.util.Material3OnScrollHelper
            public Material3OnScrollHelper.ColorSet getActiveColorSet() {
                return new Material3OnScrollHelper.ColorSet(ConversationParentFragment.getActiveToolbarColor(ConversationParentFragment.this.wallpaper.getDrawable() != null));
            }

            @Override // org.thoughtcrime.securesms.util.Material3OnScrollHelper
            public Material3OnScrollHelper.ColorSet getInactiveColorSet() {
                return new Material3OnScrollHelper.ColorSet(ConversationParentFragment.getInactiveToolbarColor(ConversationParentFragment.this.wallpaper.getDrawable() != null));
            }
        };
    }

    public /* synthetic */ void lambda$initializeViews$26(MessageSendType messageSendType, boolean z) {
        if (getContext() == null) {
            Log.w(TAG, "onSelectionChanged called in detached state. Ignoring.");
            return;
        }
        calculateCharactersRemaining();
        updateLinkPreviewState();
        this.linkPreviewViewModel.onTransportChanged(messageSendType.usesSmsTransport());
        this.composeText.setMessageSendType(messageSendType);
        updateSendButtonColor(messageSendType);
        if (z) {
            recordTransportPreference(messageSendType);
        }
    }

    public /* synthetic */ void lambda$initializeViews$27(View view) {
        handleStoryRingClick();
    }

    public /* synthetic */ void lambda$initializeViews$28(View view) {
        handleConversationSettings();
    }

    public /* synthetic */ boolean lambda$initializeViews$29(View view) {
        return handleDisplayQuickContact();
    }

    public /* synthetic */ void lambda$initializeViews$30(View view) {
        handleUnblock();
    }

    public /* synthetic */ void lambda$initializeViews$31(View view) {
        handleMakeDefaultSms();
    }

    public /* synthetic */ void lambda$initializeViews$32(View view) {
        handleRegisterForSignal();
    }

    public /* synthetic */ void lambda$initializeViews$33(View view) {
        handleAddAttachment();
    }

    public /* synthetic */ void lambda$initializeViews$34(View view) {
        handleVideo(getRecipient());
    }

    public /* synthetic */ void lambda$initializeViews$35(Optional optional) {
        if (optional.isPresent()) {
            requireVoiceNotePlayerView().show();
            requireVoiceNotePlayerView().setState((VoiceNotePlayerView.State) optional.get());
        } else if (this.voiceNotePlayerViewStub.resolved()) {
            requireVoiceNotePlayerView().hide();
        }
    }

    private void updateSendButtonColor(MessageSendType messageSendType) {
        this.buttonToggle.getBackground().setColorFilter(getSendButtonColor(messageSendType), PorterDuff.Mode.MULTIPLY);
        this.buttonToggle.getBackground().invalidateSelf();
    }

    private int getSendButtonColor(MessageSendType messageSendType) {
        if (messageSendType.usesSmsTransport()) {
            return getResources().getColor(messageSendType.getBackgroundColorRes());
        }
        if (this.recipient != null) {
            return getRecipient().getChatColors().asSingleColor();
        }
        return getResources().getColor(messageSendType.getBackgroundColorRes());
    }

    private VoiceNotePlayerView requireVoiceNotePlayerView() {
        if (this.voiceNotePlayerView == null) {
            VoiceNotePlayerView voiceNotePlayerView = (VoiceNotePlayerView) this.voiceNotePlayerViewStub.get().findViewById(R.id.voice_note_player_view);
            this.voiceNotePlayerView = voiceNotePlayerView;
            voiceNotePlayerView.setListener(new VoiceNotePlayerViewListener());
        }
        return this.voiceNotePlayerView;
    }

    private void updateWallpaper(ChatWallpaper chatWallpaper) {
        Log.d(TAG, "Setting wallpaper.");
        boolean z = true;
        if (chatWallpaper != null) {
            chatWallpaper.loadInto(this.wallpaper);
            ChatWallpaperDimLevelUtil.applyDimLevelForNightMode(this.wallpaperDim, chatWallpaper);
            this.inputPanel.setWallpaperEnabled(true);
            if (this.attachmentKeyboardStub.resolved()) {
                this.attachmentKeyboardStub.get().setWallpaperEnabled(true);
            }
            this.material3OnScrollHelper.setColorImmediate();
            int color = getResources().getColor(R.color.signal_colorNeutralInverse);
            this.toolbar.setTitleTextColor(color);
            setToolbarActionItemTint(this.toolbar, color);
            WindowUtil.setNavigationBarColor(requireActivity(), getResources().getColor(R.color.conversation_navigation_wallpaper));
        } else {
            this.wallpaper.setImageDrawable(null);
            this.wallpaperDim.setVisibility(8);
            this.inputPanel.setWallpaperEnabled(false);
            if (this.attachmentKeyboardStub.resolved()) {
                this.attachmentKeyboardStub.get().setWallpaperEnabled(false);
            }
            this.material3OnScrollHelper.setColorImmediate();
            int color2 = getResources().getColor(R.color.signal_colorOnSurface);
            this.toolbar.setTitleTextColor(color2);
            setToolbarActionItemTint(this.toolbar, color2);
            WindowUtil.setNavigationBarColor(requireActivity(), getResources().getColor(R.color.signal_colorBackground));
        }
        this.fragment.onWallpaperChanged(chatWallpaper);
        MessageRequestsBottomView messageRequestsBottomView = this.messageRequestBottomView;
        if (chatWallpaper == null) {
            z = false;
        }
        messageRequestsBottomView.setWallpaperEnabled(z);
    }

    private void setToolbarActionItemTint(Toolbar toolbar, int i) {
        for (int i2 = 0; i2 < toolbar.getMenu().size(); i2++) {
            MenuItemCompat.setIconTintList(toolbar.getMenu().getItem(i2), ColorStateList.valueOf(i));
        }
        if (toolbar.getNavigationIcon() != null) {
            toolbar.getNavigationIcon().setColorFilter(new SimpleColorFilter(i));
        }
        if (toolbar.getOverflowIcon() != null) {
            toolbar.getOverflowIcon().setColorFilter(new SimpleColorFilter(i));
        }
    }

    protected void initializeActionBar() {
        invalidateOptionsMenu();
        this.toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda21
            @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
            public final boolean onMenuItemClick(MenuItem menuItem) {
                return ConversationParentFragment.this.onOptionsItemSelected(menuItem);
            }
        });
        if (isInBubble()) {
            this.toolbar.setNavigationIcon(DrawableUtil.tint(ContextUtil.requireDrawable(requireContext(), R.drawable.ic_notification), ContextCompat.getColor(requireContext(), R.color.signal_accent_primary)));
            this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda22
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationParentFragment.this.lambda$initializeActionBar$36(view);
                }
            });
        }
        this.callback.onInitializeToolbar(this.toolbar);
    }

    public /* synthetic */ void lambda$initializeActionBar$36(View view) {
        startActivity(MainActivity.clearTop(requireContext()));
    }

    protected boolean isInBubble() {
        return this.callback.isInBubble();
    }

    private void initializeResources(ConversationIntents.Args args) {
        LiveRecipient liveRecipient = this.recipient;
        if (liveRecipient != null) {
            liveRecipient.removeObservers(this);
        }
        this.recipient = Recipient.live(args.getRecipientId());
        this.threadId = args.getThreadId();
        this.distributionType = args.getDistributionType();
        this.glideRequests = GlideApp.with(this);
        String str = TAG;
        Log.i(str, "[initializeResources] Recipient: " + this.recipient.getId() + ", Thread: " + this.threadId);
        this.recipient.observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda37
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.onRecipientChanged((Recipient) obj);
            }
        });
    }

    private void initializeLinkPreviewObserver() {
        LinkPreviewViewModel linkPreviewViewModel = (LinkPreviewViewModel) new ViewModelProvider(this, new LinkPreviewViewModel.Factory(new LinkPreviewRepository())).get(LinkPreviewViewModel.class);
        this.linkPreviewViewModel = linkPreviewViewModel;
        linkPreviewViewModel.getLinkPreviewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda53
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeLinkPreviewObserver$37((LinkPreviewViewModel.LinkPreviewState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeLinkPreviewObserver$37(LinkPreviewViewModel.LinkPreviewState linkPreviewState) {
        if (linkPreviewState != null) {
            if (linkPreviewState.isLoading()) {
                this.inputPanel.setLinkPreviewLoading();
            } else if (!linkPreviewState.hasLinks() || linkPreviewState.getLinkPreview().isPresent()) {
                this.inputPanel.setLinkPreview(this.glideRequests, linkPreviewState.getLinkPreview());
            } else {
                this.inputPanel.setLinkPreviewNoPreview(linkPreviewState.getError());
            }
            updateToggleButtonState();
        }
    }

    private void initializeSearchObserver() {
        ConversationSearchViewModel conversationSearchViewModel = (ConversationSearchViewModel) new ViewModelProvider(this, new ConversationSearchViewModel.Factory(getString(R.string.note_to_self))).get(ConversationSearchViewModel.class);
        this.searchViewModel = conversationSearchViewModel;
        conversationSearchViewModel.getSearchResults().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda87
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeSearchObserver$38((ConversationSearchViewModel.SearchResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeSearchObserver$38(ConversationSearchViewModel.SearchResult searchResult) {
        if (searchResult != null) {
            if (!searchResult.getResults().isEmpty()) {
                MessageResult messageResult = searchResult.getResults().get(searchResult.getPosition());
                ConversationFragment conversationFragment = this.fragment;
                RecipientId id = messageResult.getMessageRecipient().getId();
                long receivedTimestampMs = messageResult.getReceivedTimestampMs();
                ConversationSearchViewModel conversationSearchViewModel = this.searchViewModel;
                Objects.requireNonNull(conversationSearchViewModel);
                conversationFragment.jumpToMessage(id, receivedTimestampMs, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda44
                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationSearchViewModel.this.onMissingResult();
                    }
                });
            }
            this.searchNav.setData(searchResult.getPosition(), searchResult.getResults().size());
        }
    }

    private void initializeStickerObserver() {
        ConversationStickerViewModel conversationStickerViewModel = (ConversationStickerViewModel) new ViewModelProvider(this, new ConversationStickerViewModel.Factory(requireActivity().getApplication(), new StickerSearchRepository(requireContext()))).get(ConversationStickerViewModel.class);
        this.stickerViewModel = conversationStickerViewModel;
        conversationStickerViewModel.getStickerResults().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda103
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeStickerObserver$39((List) obj);
            }
        });
        this.stickerViewModel.getStickersAvailability().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda104
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeStickerObserver$40((Boolean) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeStickerObserver$39(List list) {
        if (list != null) {
            this.inputPanel.setStickerSuggestions(list);
        }
    }

    public /* synthetic */ void lambda$initializeStickerObserver$40(Boolean bool) {
        if (bool != null) {
            boolean isPreferSystemEmoji = SignalStore.settings().isPreferSystemEmoji();
            TextSecurePreferences.MediaKeyboardMode mediaKeyboardMode = TextSecurePreferences.getMediaKeyboardMode(requireContext());
            boolean z = !TextSecurePreferences.hasSeenStickerIntroTooltip(requireContext());
            if (bool.booleanValue()) {
                this.inputPanel.showMediaKeyboardToggle(true);
                int i = AnonymousClass24.$SwitchMap$org$thoughtcrime$securesms$util$TextSecurePreferences$MediaKeyboardMode[mediaKeyboardMode.ordinal()];
                if (i == 1) {
                    this.inputPanel.setMediaKeyboardToggleMode(isPreferSystemEmoji ? KeyboardPage.STICKER : KeyboardPage.EMOJI);
                } else if (i == 2) {
                    this.inputPanel.setMediaKeyboardToggleMode(KeyboardPage.STICKER);
                } else if (i == 3) {
                    this.inputPanel.setMediaKeyboardToggleMode(KeyboardPage.GIF);
                }
                if (z) {
                    showStickerIntroductionTooltip();
                }
            }
            if (this.emojiDrawerStub.resolved()) {
                initializeMediaKeyboardProviders();
            }
        }
    }

    private void initializeViewModel(ConversationIntents.Args args) {
        ConversationViewModel conversationViewModel = (ConversationViewModel) new ViewModelProvider(this, new ConversationViewModel.Factory()).get(ConversationViewModel.class);
        this.viewModel = conversationViewModel;
        conversationViewModel.setArgs(args);
        this.viewModel.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda45
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.onViewModelEvent((ConversationViewModel.Event) obj);
            }
        });
        this.disposables.add(this.viewModel.getWallpaper().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda46
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationParentFragment.this.lambda$initializeViewModel$41((Optional) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$initializeViewModel$41(Optional optional) throws Throwable {
        updateWallpaper((ChatWallpaper) optional.orElse(null));
    }

    private void initializeGroupViewModel() {
        ConversationGroupViewModel conversationGroupViewModel = (ConversationGroupViewModel) new ViewModelProvider(this, new ConversationGroupViewModel.Factory()).get(ConversationGroupViewModel.class);
        this.groupViewModel = conversationGroupViewModel;
        LiveRecipient liveRecipient = this.recipient;
        Objects.requireNonNull(conversationGroupViewModel);
        liveRecipient.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda88
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationGroupViewModel.this.onRecipientChange((Recipient) obj);
            }
        });
        this.groupViewModel.getGroupActiveState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda89
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeGroupViewModel$42((ConversationGroupViewModel.GroupActiveState) obj);
            }
        });
        this.groupViewModel.getReviewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda90
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.presentGroupReviewBanner((ConversationGroupViewModel.ReviewState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeGroupViewModel$42(ConversationGroupViewModel.GroupActiveState groupActiveState) {
        invalidateOptionsMenu();
    }

    private void initializeMentionsViewModel() {
        this.mentionsViewModel = (MentionsPickerViewModel) new ViewModelProvider(requireActivity(), new MentionsPickerViewModel.Factory()).get(MentionsPickerViewModel.class);
        this.recipient.observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda40
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeMentionsViewModel$43((Recipient) obj);
            }
        });
        this.composeText.setMentionQueryChangedListener(new ComposeText.MentionQueryChangedListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda41
            @Override // org.thoughtcrime.securesms.components.ComposeText.MentionQueryChangedListener
            public final void onQueryChanged(String str) {
                ConversationParentFragment.this.lambda$initializeMentionsViewModel$44(str);
            }
        });
        this.composeText.setMentionValidator(new MentionValidatorWatcher.MentionValidator() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda42
            @Override // org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher.MentionValidator
            public final List getInvalidMentionAnnotations(List list) {
                return ConversationParentFragment.this.lambda$initializeMentionsViewModel$47(list);
            }
        });
        this.mentionsViewModel.getSelectedRecipient().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda43
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeMentionsViewModel$48((Recipient) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeMentionsViewModel$43(Recipient recipient) {
        if (recipient.isPushV2Group() && !this.mentionsSuggestions.resolved()) {
            this.mentionsSuggestions.get();
        }
        this.mentionsViewModel.onRecipientChange(recipient);
    }

    public /* synthetic */ void lambda$initializeMentionsViewModel$44(String str) {
        if (getRecipient().isPushV2Group() && getRecipient().isActiveGroup()) {
            if (!this.mentionsSuggestions.resolved()) {
                this.mentionsSuggestions.get();
            }
            this.mentionsViewModel.onQueryChange(str);
        }
    }

    public /* synthetic */ List lambda$initializeMentionsViewModel$47(List list) {
        return (!getRecipient().isPushV2Group() || !getRecipient().isActiveGroup()) ? list : Stream.of(list).filterNot(new Predicate((Set) Stream.of(getRecipient().getParticipantIds()).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda55
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MentionAnnotation.idToMentionAnnotationValue((RecipientId) obj);
            }
        }).collect(Collectors.toSet())) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda56
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationParentFragment.lambda$initializeMentionsViewModel$46(this.f$0, (Annotation) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$initializeMentionsViewModel$46(Set set, Annotation annotation) {
        return set.contains(annotation.getValue());
    }

    public /* synthetic */ void lambda$initializeMentionsViewModel$48(Recipient recipient) {
        this.composeText.replaceTextWithMention(recipient.getDisplayName(requireContext()), recipient.getId());
    }

    public void initializeGroupCallViewModel() {
        this.groupCallViewModel = (GroupCallViewModel) new ViewModelProvider(this, new GroupCallViewModel.Factory()).get(GroupCallViewModel.class);
        this.recipient.observe(this, new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeGroupCallViewModel$49((Recipient) obj);
            }
        });
        this.groupCallViewModel.hasActiveGroupCall().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeGroupCallViewModel$50((Boolean) obj);
            }
        });
        this.groupCallViewModel.groupCallHasCapacity().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeGroupCallViewModel$51((Boolean) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeGroupCallViewModel$49(Recipient recipient) {
        this.groupCallViewModel.onRecipientChange(recipient);
    }

    public /* synthetic */ void lambda$initializeGroupCallViewModel$50(Boolean bool) {
        invalidateOptionsMenu();
        this.joinGroupCallButton.setVisibility(bool.booleanValue() ? 0 : 8);
    }

    public /* synthetic */ void lambda$initializeGroupCallViewModel$51(Boolean bool) {
        this.joinGroupCallButton.setText(bool.booleanValue() ? R.string.ConversationActivity_join : R.string.ConversationActivity_full);
    }

    public void initializeDraftViewModel() {
        this.draftViewModel = (DraftViewModel) new ViewModelProvider(this, new DraftViewModel.Factory(new DraftRepository(requireContext().getApplicationContext()))).get(DraftViewModel.class);
        this.recipient.observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda84
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeDraftViewModel$52((Recipient) obj);
            }
        });
        this.draftViewModel.getState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda85
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$initializeDraftViewModel$53((DraftState) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeDraftViewModel$52(Recipient recipient) {
        this.draftViewModel.onRecipientChanged(recipient);
    }

    public /* synthetic */ void lambda$initializeDraftViewModel$53(DraftState draftState) {
        this.inputPanel.setVoiceNoteDraft(draftState.getVoiceNoteDraft());
        updateToggleButtonState();
    }

    private void showGroupCallingTooltip() {
        if (Build.VERSION.SDK_INT != 19 && SignalStore.tooltips().shouldShowGroupCallingTooltip() && !this.callingTooltipShown) {
            View findViewById = requireView().findViewById(R.id.menu_video_secure);
            if (findViewById == null) {
                Log.w(TAG, "Video Call tooltip anchor is null. Skipping tooltip...");
                return;
            }
            this.callingTooltipShown = true;
            SignalStore.tooltips().markGroupCallSpeakerViewSeen();
            TooltipPopup.forTarget(findViewById).setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.signal_accent_green)).setTextColor(getResources().getColor(R.color.core_white)).setText(R.string.ConversationActivity__tap_here_to_start_a_group_call).setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda0
                @Override // android.widget.PopupWindow.OnDismissListener
                public final void onDismiss() {
                    ConversationParentFragment.lambda$showGroupCallingTooltip$54();
                }
            }).show(1);
        }
    }

    public static /* synthetic */ void lambda$showGroupCallingTooltip$54() {
        SignalStore.tooltips().markGroupCallingTooltipSeen();
    }

    private void showStickerIntroductionTooltip() {
        TextSecurePreferences.setMediaKeyboardMode(requireContext(), TextSecurePreferences.MediaKeyboardMode.STICKER);
        this.inputPanel.setMediaKeyboardToggleMode(KeyboardPage.STICKER);
        TooltipPopup.forTarget(this.inputPanel.getMediaKeyboardToggleAnchorView()).setBackgroundTint(getResources().getColor(R.color.core_ultramarine)).setTextColor(getResources().getColor(R.color.core_white)).setText(R.string.ConversationActivity_new_say_it_with_stickers).setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda57
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                ConversationParentFragment.this.lambda$showStickerIntroductionTooltip$55();
            }
        }).show(0);
    }

    public /* synthetic */ void lambda$showStickerIntroductionTooltip$55() {
        TextSecurePreferences.setHasSeenStickerIntroTooltip(requireContext(), true);
        EventBus.getDefault().removeStickyEvent(StickerPackInstallEvent.class);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnReactionSelectedListener
    public void onReactionSelected(MessageRecord messageRecord, String str) {
        Context applicationContext = requireContext().getApplicationContext();
        this.reactionDelegate.hide();
        SignalExecutors.BOUNDED.execute(new Runnable(str, applicationContext) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda99
            public final /* synthetic */ String f$1;
            public final /* synthetic */ Context f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.lambda$onReactionSelected$57(MessageRecord.this, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ void lambda$onReactionSelected$57(MessageRecord messageRecord, String str, Context context) {
        ReactionRecord reactionRecord = (ReactionRecord) Stream.of(messageRecord.getReactions()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda97
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationParentFragment.lambda$onReactionSelected$56((ReactionRecord) obj);
            }
        }).findFirst().orElse(null);
        if (reactionRecord == null || !reactionRecord.getEmoji().equals(str)) {
            MessageSender.sendNewReaction(context, new MessageId(messageRecord.getId(), messageRecord.isMms()), str);
        } else {
            MessageSender.sendReactionRemoval(context, new MessageId(messageRecord.getId(), messageRecord.isMms()), reactionRecord);
        }
    }

    public static /* synthetic */ boolean lambda$onReactionSelected$56(ReactionRecord reactionRecord) {
        return reactionRecord.getAuthor().equals(Recipient.self().getId());
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnReactionSelectedListener
    public void onCustomReactionSelected(MessageRecord messageRecord, boolean z) {
        ReactionRecord reactionRecord = (ReactionRecord) Stream.of(messageRecord.getReactions()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda60
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationParentFragment.lambda$onCustomReactionSelected$58((ReactionRecord) obj);
            }
        }).findFirst().orElse(null);
        if (reactionRecord == null || !z) {
            this.reactionDelegate.hideForReactWithAny();
            ReactWithAnyEmojiBottomSheetDialogFragment.createForMessageRecord(messageRecord, this.reactWithAnyEmojiStartPage).show(getChildFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
            return;
        }
        Context applicationContext = requireContext().getApplicationContext();
        this.reactionDelegate.hide();
        SignalExecutors.BOUNDED.execute(new Runnable(applicationContext, messageRecord, reactionRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda61
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ MessageRecord f$1;
            public final /* synthetic */ ReactionRecord f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.lambda$onCustomReactionSelected$59(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    public static /* synthetic */ boolean lambda$onCustomReactionSelected$58(ReactionRecord reactionRecord) {
        return reactionRecord.getAuthor().equals(Recipient.self().getId());
    }

    public static /* synthetic */ void lambda$onCustomReactionSelected$59(Context context, MessageRecord messageRecord, ReactionRecord reactionRecord) {
        MessageSender.sendReactionRemoval(context, new MessageId(messageRecord.getId(), messageRecord.isMms()), reactionRecord);
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiDialogDismissed() {
        this.reactionDelegate.hide();
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiSelected(String str) {
        this.reactionDelegate.hide();
    }

    @Override // org.thoughtcrime.securesms.components.ConversationSearchBottomBar.EventListener
    public void onSearchMoveUpPressed() {
        this.searchViewModel.onMoveUp();
    }

    @Override // org.thoughtcrime.securesms.components.ConversationSearchBottomBar.EventListener
    public void onSearchMoveDownPressed() {
        this.searchViewModel.onMoveDown();
    }

    private void initializeProfiles() {
        if (!this.viewModel.isPushAvailable()) {
            Log.i(TAG, "SMS contact, no profile fetch needed.");
        } else {
            RetrieveProfileJob.enqueueAsync(this.recipient.getId());
        }
    }

    private void initializeGv1Migration() {
        GroupV1MigrationJob.enqueuePossibleAutoMigrate(this.recipient.getId());
    }

    public void onRecipientChanged(Recipient recipient) {
        if (getContext() == null) {
            Log.w(TAG, "onRecipientChanged called in detached state. Ignoring.");
            return;
        }
        String str = TAG;
        Log.i(str, "onModified(" + recipient.getId() + ") " + recipient.getRegistered());
        this.titleView.setTitle(this.glideRequests, recipient);
        this.titleView.setVerified(this.identityRecords.isVerified() && !recipient.isSelf());
        setBlockedUserState(recipient, this.viewModel.getConversationStateSnapshot().getSecurityInfo());
        updateReminders();
        updateDefaultSubscriptionId(recipient.getDefaultSubscriptionId());
        updatePaymentsAvailable();
        updateSendButtonColor(this.sendButton.getSelectedSendType());
        MenuItem menuItem = this.searchViewItem;
        if (menuItem == null || !menuItem.isActionViewExpanded()) {
            invalidateOptionsMenu();
        }
        ConversationGroupViewModel conversationGroupViewModel = this.groupViewModel;
        if (conversationGroupViewModel != null) {
            conversationGroupViewModel.onRecipientChange(recipient);
        }
        MentionsPickerViewModel mentionsPickerViewModel = this.mentionsViewModel;
        if (mentionsPickerViewModel != null) {
            mentionsPickerViewModel.onRecipientChange(recipient);
        }
        GroupCallViewModel groupCallViewModel = this.groupCallViewModel;
        if (groupCallViewModel != null) {
            groupCallViewModel.onRecipientChange(recipient);
        }
        DraftViewModel draftViewModel = this.draftViewModel;
        if (draftViewModel != null) {
            draftViewModel.onRecipientChanged(recipient);
        }
        if (this.threadId == -1) {
            SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda35
                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return ConversationParentFragment.lambda$onRecipientChanged$60(Recipient.this);
                }
            }, new SimpleTask.ForegroundTask(recipient) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda36
                public final /* synthetic */ Recipient f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    ConversationParentFragment.this.lambda$onRecipientChanged$61(this.f$1, (Long) obj);
                }
            });
        }
    }

    public static /* synthetic */ Long lambda$onRecipientChanged$60(Recipient recipient) {
        return Long.valueOf(SignalDatabase.threads().getThreadIdIfExistsFor(recipient.getId()));
    }

    public /* synthetic */ void lambda$onRecipientChanged$61(Recipient recipient, Long l) {
        if (this.threadId != l.longValue()) {
            Log.d(TAG, "Thread id changed via recipient change");
            long longValue = l.longValue();
            this.threadId = longValue;
            this.fragment.reload(recipient, longValue);
            setVisibleThread(this.threadId);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onIdentityRecordUpdate(IdentityRecord identityRecord) {
        initializeIdentityRecords();
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onStickerPackInstalled(StickerPackInstallEvent stickerPackInstallEvent) {
        if (TextSecurePreferences.hasSeenStickerIntroTooltip(requireContext())) {
            EventBus.getDefault().removeStickyEvent(stickerPackInstallEvent);
            if (!this.inputPanel.isStickerMode()) {
                TooltipPopup.forTarget(this.inputPanel.getMediaKeyboardToggleAnchorView()).setText(R.string.ConversationActivity_sticker_pack_installed).setIconGlideModel(stickerPackInstallEvent.getIconGlideModel()).show(0);
            }
        }
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onGroupCallPeekEvent(GroupCallPeekEvent groupCallPeekEvent) {
        GroupCallViewModel groupCallViewModel = this.groupCallViewModel;
        if (groupCallViewModel != null) {
            groupCallViewModel.onGroupCallPeekEvent(groupCallPeekEvent);
        }
    }

    private void initializeReceivers() {
        this.securityUpdateReceiver = new BroadcastReceiver() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.17
            @Override // android.content.BroadcastReceiver
            public void onReceive(Context context, Intent intent) {
                ConversationParentFragment.this.viewModel.updateSecurityInfo();
                ConversationParentFragment.this.calculateCharactersRemaining();
            }
        };
        requireActivity().registerReceiver(this.securityUpdateReceiver, new IntentFilter(SecurityEvent.SECURITY_UPDATE_EVENT), KeyCachingService.KEY_PERMISSION, null);
    }

    public ListenableFuture<Boolean> setMedia(Uri uri, SlideFactory.MediaType mediaType) {
        return setMedia(uri, mediaType, 0, 0, false, false);
    }

    private ListenableFuture<Boolean> setMedia(Uri uri, SlideFactory.MediaType mediaType, int i, int i2, boolean z, boolean z2) {
        if (uri == null) {
            return new SettableFuture(Boolean.FALSE);
        }
        if (SlideFactory.MediaType.VCARD.equals(mediaType) && this.viewModel.isPushAvailable()) {
            openContactShareEditor(uri);
            return new SettableFuture(Boolean.FALSE);
        } else if (!SlideFactory.MediaType.IMAGE.equals(mediaType) && !SlideFactory.MediaType.GIF.equals(mediaType) && !SlideFactory.MediaType.VIDEO.equals(mediaType)) {
            return this.attachmentManager.setMedia(this.glideRequests, uri, mediaType, getCurrentMediaConstraints(), i, i2);
        } else {
            String mimeType = MediaUtil.getMimeType(requireContext(), uri);
            if (mimeType == null) {
                mimeType = mediaType.toFallbackMimeType();
            }
            startActivityForResult(MediaSelectionActivity.editor(requireContext(), this.sendButton.getSelectedSendType(), Collections.singletonList(new Media(uri, mimeType, 0, i, i2, 0, 0, z, z2, Optional.empty(), Optional.empty(), Optional.empty())), this.recipient.getId(), this.composeText.getTextTrimmed()), 12);
            return new SettableFuture(Boolean.FALSE);
        }
    }

    private void openContactShareEditor(Uri uri) {
        startActivityForResult(ContactShareEditActivity.getIntent(requireContext(), Collections.singletonList(uri), getSendButtonColor(this.sendButton.getSelectedSendType())), 5);
    }

    private void addAttachmentContactInfo(Uri uri) {
        ContactAccessor.ContactData contactData = ContactAccessor.getInstance().getContactData(requireContext(), uri);
        if (contactData.numbers.size() == 1) {
            this.composeText.append(contactData.numbers.get(0).number);
        } else if (contactData.numbers.size() > 1) {
            selectContactInfo(contactData);
        }
    }

    private void sendSharedContact(List<Contact> list) {
        sendMediaMessage(this.recipient.getId(), this.sendButton.getSelectedSendType(), "", this.attachmentManager.buildSlideDeck(), null, list, Collections.emptyList(), Collections.emptyList(), TimeUnit.SECONDS.toMillis((long) this.recipient.get().getExpiresInSeconds()), false, this.threadId == -1, false, null);
    }

    private void selectContactInfo(ContactAccessor.ContactData contactData) {
        CharSequence[] charSequenceArr = new CharSequence[contactData.numbers.size()];
        CharSequence[] charSequenceArr2 = new CharSequence[contactData.numbers.size()];
        for (int i = 0; i < contactData.numbers.size(); i++) {
            charSequenceArr[i] = contactData.numbers.get(i).number;
            charSequenceArr2[i] = contactData.numbers.get(i).type + ": " + contactData.numbers.get(i).number;
        }
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireContext());
        materialAlertDialogBuilder.setIcon(R.drawable.ic_account_box);
        materialAlertDialogBuilder.setTitle(R.string.ConversationActivity_select_contact_info);
        materialAlertDialogBuilder.setItems(charSequenceArr2, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(charSequenceArr) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda94
            public final /* synthetic */ CharSequence[] f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                ConversationParentFragment.this.lambda$selectContactInfo$62(this.f$1, dialogInterface, i2);
            }
        });
        materialAlertDialogBuilder.show();
    }

    public /* synthetic */ void lambda$selectContactInfo$62(CharSequence[] charSequenceArr, DialogInterface dialogInterface, int i) {
        this.composeText.append(charSequenceArr[i]);
    }

    private DraftDatabase.Drafts getDraftsForCurrentState() {
        DraftDatabase.Drafts drafts = new DraftDatabase.Drafts();
        if (this.recipient.get().isGroup() && !this.recipient.get().isActiveGroup()) {
            return drafts;
        }
        if (!Util.isEmpty(this.composeText)) {
            drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.TEXT, this.composeText.getTextTrimmed().toString()));
            List<Mention> mentions = this.composeText.getMentions();
            if (!mentions.isEmpty()) {
                drafts.add(new DraftDatabase.Draft("mention", Base64.encodeBytes(MentionUtil.mentionsToBodyRangeList(mentions).toByteArray())));
            }
        }
        for (Slide slide : this.attachmentManager.buildSlideDeck().getSlides()) {
            if (slide.hasAudio() && slide.getUri() != null) {
                drafts.add(new DraftDatabase.Draft("audio", slide.getUri().toString()));
            } else if (slide.hasVideo() && slide.getUri() != null) {
                drafts.add(new DraftDatabase.Draft("video", slide.getUri().toString()));
            } else if (slide.hasLocation()) {
                drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.LOCATION, ((LocationSlide) slide).getPlace().serialize()));
            } else if (slide.hasImage() && slide.getUri() != null) {
                drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.IMAGE, slide.getUri().toString()));
            }
        }
        Optional<QuoteModel> quote = this.inputPanel.getQuote();
        if (quote.isPresent()) {
            drafts.add(new DraftDatabase.Draft(DraftDatabase.Draft.QUOTE, new QuoteId(quote.get().getId(), quote.get().getAuthor()).serialize()));
        }
        DraftDatabase.Draft voiceNoteDraft = this.draftViewModel.getVoiceNoteDraft();
        if (voiceNoteDraft != null) {
            drafts.add(voiceNoteDraft);
        }
        return drafts;
    }

    public ListenableFuture<Long> saveDraft() {
        final SettableFuture settableFuture = new SettableFuture();
        if (this.recipient == null) {
            settableFuture.set(Long.valueOf(this.threadId));
            return settableFuture;
        }
        final Context applicationContext = requireContext().getApplicationContext();
        final DraftDatabase.Drafts draftsForCurrentState = getDraftsForCurrentState();
        long j = this.threadId;
        final RecipientId id = this.recipient.getId();
        final int i = this.distributionType;
        final ListenableFuture<VoiceNoteDraft> consumeVoiceNoteDraftFuture = this.draftViewModel.consumeVoiceNoteDraftFuture();
        new AsyncTask<Long, Void, Long>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.18
            public Long doInBackground(Long... lArr) {
                ListenableFuture listenableFuture = consumeVoiceNoteDraftFuture;
                if (listenableFuture != null) {
                    try {
                        DraftDatabase.Draft asDraft = ((VoiceNoteDraft) listenableFuture.get()).asDraft();
                        ConversationParentFragment.this.draftViewModel.setVoiceNoteDraft(id, asDraft);
                        draftsForCurrentState.add(asDraft);
                    } catch (InterruptedException | ExecutionException e) {
                        Log.w(ConversationParentFragment.TAG, "Could not extract voice note draft data.", e);
                    }
                }
                ThreadDatabase threads = SignalDatabase.threads();
                DraftDatabase drafts = SignalDatabase.drafts();
                long longValue = lArr[0].longValue();
                if (draftsForCurrentState.size() > 0) {
                    if (longValue == -1) {
                        longValue = threads.getOrCreateThreadIdFor(ConversationParentFragment.this.getRecipient(), i);
                    }
                    drafts.replaceDrafts(longValue, draftsForCurrentState);
                    longValue = longValue;
                    threads.updateSnippet(longValue, draftsForCurrentState.getSnippet(applicationContext), draftsForCurrentState.getUriSnippet(), System.currentTimeMillis(), 27, true);
                } else if (longValue > 0) {
                    threads.update(longValue, false);
                }
                if (draftsForCurrentState.isEmpty()) {
                    drafts.clearDrafts(longValue);
                }
                return Long.valueOf(longValue);
            }

            public void onPostExecute(Long l) {
                settableFuture.set(l);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Long.valueOf(j));
        return settableFuture;
    }

    public void setBlockedUserState(Recipient recipient, ConversationSecurityInfo conversationSecurityInfo) {
        if (!conversationSecurityInfo.isInitialized()) {
            Log.i(TAG, "Ignoring blocked state update for uninitialized security info.");
            return;
        }
        boolean z = false;
        if (!conversationSecurityInfo.isPushAvailable() && isPushGroupConversation()) {
            this.unblockButton.setVisibility(8);
            this.inputPanel.setHideForBlockedState(true);
            this.makeDefaultSmsButton.setVisibility(8);
            this.registerButton.setVisibility(0);
        } else if (!conversationSecurityInfo.isPushAvailable() && !conversationSecurityInfo.isDefaultSmsApplication() && recipient.hasSmsAddress()) {
            this.unblockButton.setVisibility(8);
            this.inputPanel.setHideForBlockedState(true);
            this.makeDefaultSmsButton.setVisibility(0);
            this.registerButton.setVisibility(8);
        } else if (!recipient.isReleaseNotes() || recipient.isBlocked()) {
            if (isPushGroupConversation() && !recipient.isActiveGroup()) {
                z = true;
            }
            this.inputPanel.setHideForBlockedState(z);
            this.unblockButton.setVisibility(8);
            this.makeDefaultSmsButton.setVisibility(8);
            this.registerButton.setVisibility(8);
        } else {
            this.unblockButton.setVisibility(8);
            this.inputPanel.setHideForBlockedState(true);
            this.makeDefaultSmsButton.setVisibility(8);
            this.registerButton.setVisibility(8);
            if (recipient.isMuted()) {
                View view = this.releaseChannelUnmute.get();
                view.setVisibility(0);
                view.findViewById(R.id.conversation_activity_unmute_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda95
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        ConversationParentFragment.this.lambda$setBlockedUserState$63(view2);
                    }
                });
            } else if (this.releaseChannelUnmute.resolved()) {
                this.releaseChannelUnmute.get().setVisibility(8);
            }
        }
        if (this.releaseChannelUnmute.resolved() && !recipient.isReleaseNotes()) {
            this.releaseChannelUnmute.get().setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$setBlockedUserState$63(View view) {
        handleUnmuteNotifications();
    }

    public void calculateCharactersRemaining() {
        CharacterCalculator.CharacterState calculateCharacters = this.sendButton.getSelectedSendType().calculateCharacters(this.composeText.getTextTrimmed().toString());
        if (calculateCharacters.charactersRemaining <= 15 || calculateCharacters.messagesSpent > 1) {
            this.charactersLeft.setText(String.format(Locale.getDefault(), "%d/%d (%d)", Integer.valueOf(calculateCharacters.charactersRemaining), Integer.valueOf(calculateCharacters.maxTotalMessageSize), Integer.valueOf(calculateCharacters.messagesSpent)));
            this.charactersLeft.setVisibility(0);
            return;
        }
        this.charactersLeft.setVisibility(8);
    }

    private void initializeMediaKeyboardProviders() {
        KeyboardPagerViewModel keyboardPagerViewModel = (KeyboardPagerViewModel) new ViewModelProvider(requireActivity()).get(KeyboardPagerViewModel.class);
        int i = AnonymousClass24.$SwitchMap$org$thoughtcrime$securesms$util$TextSecurePreferences$MediaKeyboardMode[TextSecurePreferences.getMediaKeyboardMode(requireContext()).ordinal()];
        if (i == 1) {
            keyboardPagerViewModel.switchToPage(KeyboardPage.EMOJI);
        } else if (i == 2) {
            keyboardPagerViewModel.switchToPage(KeyboardPage.STICKER);
        } else if (i == 3) {
            keyboardPagerViewModel.switchToPage(KeyboardPage.GIF);
        }
    }

    private boolean isInMessageRequest() {
        return this.messageRequestBottomView.getVisibility() == 0;
    }

    private boolean isSingleConversation() {
        return getRecipient() != null && !getRecipient().isGroup();
    }

    private boolean isActiveGroup() {
        if (!isGroupConversation()) {
            return false;
        }
        Optional<GroupDatabase.GroupRecord> group = SignalDatabase.groups().getGroup(getRecipient().getId());
        if (!group.isPresent() || !group.get().isActive()) {
            return false;
        }
        return true;
    }

    private boolean isGroupConversation() {
        return getRecipient() != null && getRecipient().isGroup();
    }

    private boolean isPushGroupConversation() {
        return getRecipient() != null && getRecipient().isPushGroup();
    }

    private boolean isPushGroupV1Conversation() {
        return getRecipient() != null && getRecipient().isPushV1Group();
    }

    public boolean isSmsForced() {
        return this.sendButton.isManualSelection() && this.sendButton.getSelectedSendType().usesSmsTransport();
    }

    public Recipient getRecipient() {
        return this.recipient.get();
    }

    protected long getThreadId() {
        return this.threadId;
    }

    private String getMessage() throws InvalidMessageException {
        String charSequence = this.composeText.getTextTrimmed().toString();
        if (charSequence.length() >= 1 || this.attachmentManager.isAttachmentPresent()) {
            return charSequence;
        }
        throw new InvalidMessageException(getString(R.string.ConversationActivity_message_is_empty_exclamation));
    }

    public MediaConstraints getCurrentMediaConstraints() {
        if (this.sendButton.getSelectedSendType().usesSignalTransport()) {
            return MediaConstraints.getPushMediaConstraints();
        }
        return MediaConstraints.getMmsMediaConstraints(this.sendButton.getSelectedSendType().getSimSubscriptionIdOr(-1));
    }

    private void markLastSeen() {
        new AsyncTask<Long, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.19
            public Void doInBackground(Long... lArr) {
                SignalDatabase.threads().setLastSeen(lArr[0].longValue());
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, Long.valueOf(this.threadId));
    }

    public void sendComplete(long j) {
        boolean z = j != this.threadId;
        this.threadId = j;
        ConversationFragment conversationFragment = this.fragment;
        if (conversationFragment == null || !conversationFragment.isVisible() || requireActivity().isFinishing()) {
            this.callback.onSendComplete(j);
            return;
        }
        this.fragment.setLastSeen(0);
        if (z) {
            this.fragment.reload(this.recipient.get(), j);
            setVisibleThread(j);
        }
        this.fragment.scrollToBottom();
        this.attachmentManager.cleanup();
        updateLinkPreviewState();
        this.callback.onSendComplete(j);
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void sendMessage(java.lang.String r13) {
        /*
        // Method dump skipped, instructions count: 368
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationParentFragment.sendMessage(java.lang.String):void");
    }

    private void sendMediaMessage(MediaSendActivityResult mediaSendActivityResult) {
        long j = this.threadId;
        OutgoingSecureMediaMessage outgoingSecureMediaMessage = new OutgoingSecureMediaMessage(new OutgoingMediaMessage(this.recipient.get(), new SlideDeck(), mediaSendActivityResult.getBody(), System.currentTimeMillis(), -1, TimeUnit.SECONDS.toMillis((long) this.recipient.get().getExpiresInSeconds()), mediaSendActivityResult.isViewOnce(), this.distributionType, mediaSendActivityResult.getStoryType(), null, false, mediaSendActivityResult.isViewOnce() ? null : this.inputPanel.getQuote().orElse(null), Collections.emptyList(), Collections.emptyList(), new ArrayList(mediaSendActivityResult.getMentions()), null));
        Context applicationContext = requireContext().getApplicationContext();
        ApplicationDependencies.getTypingStatusSender().onTypingStopped(j);
        this.inputPanel.clearQuote();
        this.attachmentManager.clear(this.glideRequests, false);
        silentlySetComposeText("");
        this.fragment.stageOutgoingMessage(outgoingSecureMediaMessage);
        SimpleTask.run(new SimpleTask.BackgroundTask(applicationContext, outgoingSecureMediaMessage, mediaSendActivityResult, j) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda13
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ OutgoingMediaMessage f$1;
            public final /* synthetic */ MediaSendActivityResult f$2;
            public final /* synthetic */ long f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationParentFragment.lambda$sendMediaMessage$64(this.f$0, this.f$1, this.f$2, this.f$3);
            }
        }, new ConversationParentFragment$$ExternalSyntheticLambda14(this));
    }

    public static /* synthetic */ Long lambda$sendMediaMessage$64(Context context, OutgoingMediaMessage outgoingMediaMessage, MediaSendActivityResult mediaSendActivityResult, long j) {
        long sendPushWithPreUploadedMedia = MessageSender.sendPushWithPreUploadedMedia(context, outgoingMediaMessage, mediaSendActivityResult.getPreUploadResults(), j, null);
        int deleteAbandonedPreuploadedAttachments = SignalDatabase.attachments().deleteAbandonedPreuploadedAttachments();
        String str = TAG;
        Log.i(str, "Deleted " + deleteAbandonedPreuploadedAttachments + " abandoned attachments.");
        return Long.valueOf(sendPushWithPreUploadedMedia);
    }

    private void sendMediaMessage(MessageSendType messageSendType, long j, boolean z, boolean z2, String str) throws InvalidMessageException {
        Log.i(TAG, "Sending media message...");
        sendMediaMessage(this.recipient.getId(), messageSendType, getMessage(), this.attachmentManager.buildSlideDeck(), this.inputPanel.getQuote().orElse(null), Collections.emptyList(), this.linkPreviewViewModel.onSend(), this.composeText.getMentions(), j, z, z2, true, str);
    }

    private ListenableFuture<Void> sendMediaMessage(RecipientId recipientId, MessageSendType messageSendType, String str, SlideDeck slideDeck, QuoteModel quoteModel, List<Contact> list, List<LinkPreview> list2, List<Mention> list3, long j, boolean z, boolean z2, boolean z3, String str2) {
        String str3;
        OutgoingMediaMessage outgoingMediaMessage;
        if (this.viewModel.isDefaultSmsApplication() || !messageSendType.usesSmsTransport() || !this.recipient.get().hasSmsAddress()) {
            boolean usesSignalTransport = messageSendType.usesSignalTransport();
            long j2 = this.threadId;
            if (usesSignalTransport) {
                MessageUtil.SplitResult splitMessage = MessageUtil.getSplitMessage(requireContext(), str, this.sendButton.getSelectedSendType().calculateCharacters(str).maxPrimaryMessageSize);
                String body = splitMessage.getBody();
                if (splitMessage.getTextSlide().isPresent()) {
                    slideDeck.addSlide(splitMessage.getTextSlide().get());
                }
                str3 = body;
            } else {
                str3 = str;
            }
            OutgoingMediaMessage outgoingMediaMessage2 = new OutgoingMediaMessage(Recipient.resolved(recipientId), slideDeck, str3, System.currentTimeMillis(), messageSendType.getSimSubscriptionIdOr(-1), j, z, this.distributionType, StoryType.NONE, null, false, quoteModel, list, list2, list3, null);
            SettableFuture settableFuture = new SettableFuture();
            Context applicationContext = requireContext().getApplicationContext();
            if (usesSignalTransport) {
                OutgoingSecureMediaMessage outgoingSecureMediaMessage = new OutgoingSecureMediaMessage(outgoingMediaMessage2);
                ApplicationDependencies.getTypingStatusSender().onTypingStopped(j2);
                outgoingMediaMessage = outgoingSecureMediaMessage;
            } else {
                outgoingMediaMessage = outgoingMediaMessage2.withExpiry(0);
            }
            Permissions.with(this).request("android.permission.SEND_SMS", "android.permission.READ_SMS").ifNecessary(!usesSignalTransport).withPermanentDenialDialog(getString(R.string.ConversationActivity_signal_needs_sms_permission_in_order_to_send_an_sms)).onAllGranted(new Runnable(z3, outgoingMediaMessage, applicationContext, j2, messageSendType, str2, settableFuture) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda101
                public final /* synthetic */ boolean f$1;
                public final /* synthetic */ OutgoingMediaMessage f$2;
                public final /* synthetic */ Context f$3;
                public final /* synthetic */ long f$4;
                public final /* synthetic */ MessageSendType f$5;
                public final /* synthetic */ String f$6;
                public final /* synthetic */ SettableFuture f$7;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                    this.f$5 = r7;
                    this.f$6 = r8;
                    this.f$7 = r9;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationParentFragment.this.lambda$sendMediaMessage$67(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7);
                }
            }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda102
                @Override // java.lang.Runnable
                public final void run() {
                    SettableFuture.this.set(null);
                }
            }).execute();
            return settableFuture;
        }
        showDefaultSmsPrompt();
        return new SettableFuture(null);
    }

    public /* synthetic */ void lambda$sendMediaMessage$67(boolean z, OutgoingMediaMessage outgoingMediaMessage, Context context, long j, MessageSendType messageSendType, String str, SettableFuture settableFuture) {
        if (z) {
            this.inputPanel.clearQuote();
            this.attachmentManager.clear(this.glideRequests, false);
            silentlySetComposeText("");
        }
        this.fragment.stageOutgoingMessage(outgoingMediaMessage);
        SimpleTask.run(new SimpleTask.BackgroundTask(context, outgoingMediaMessage, j, messageSendType, str) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda47
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ OutgoingMediaMessage f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ MessageSendType f$3;
            public final /* synthetic */ String f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r5;
                this.f$4 = r6;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationParentFragment.lambda$sendMediaMessage$65(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }, new SimpleTask.ForegroundTask(settableFuture) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda48
            public final /* synthetic */ SettableFuture f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationParentFragment.this.lambda$sendMediaMessage$66(this.f$1, (Long) obj);
            }
        });
    }

    public static /* synthetic */ Long lambda$sendMediaMessage$65(Context context, OutgoingMediaMessage outgoingMediaMessage, long j, MessageSendType messageSendType, String str) {
        return Long.valueOf(MessageSender.send(context, outgoingMediaMessage, j, messageSendType.usesSmsTransport(), str, (MessageDatabase.InsertListener) null));
    }

    public /* synthetic */ void lambda$sendMediaMessage$66(SettableFuture settableFuture, Long l) {
        sendComplete(l.longValue());
        settableFuture.set(null);
    }

    private void sendTextMessage(MessageSendType messageSendType, long j, boolean z, String str) throws InvalidMessageException {
        OutgoingTextMessage outgoingTextMessage;
        if (this.viewModel.isDefaultSmsApplication() || !messageSendType.usesSmsTransport() || !this.recipient.get().hasSmsAddress()) {
            long j2 = this.threadId;
            Context applicationContext = requireContext().getApplicationContext();
            String message = getMessage();
            boolean usesSignalTransport = messageSendType.usesSignalTransport();
            if (usesSignalTransport) {
                outgoingTextMessage = new OutgoingEncryptedMessage(this.recipient.get(), message, j);
                ApplicationDependencies.getTypingStatusSender().onTypingStopped(j2);
            } else {
                outgoingTextMessage = new OutgoingTextMessage(this.recipient.get(), message, 0, messageSendType.getSimSubscriptionIdOr(-1));
            }
            Permissions.with(this).request("android.permission.SEND_SMS").ifNecessary(!usesSignalTransport).withPermanentDenialDialog(getString(R.string.ConversationActivity_signal_needs_sms_permission_in_order_to_send_an_sms)).onAllGranted(new Runnable(applicationContext, outgoingTextMessage, j2, messageSendType, str) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda52
                public final /* synthetic */ Context f$1;
                public final /* synthetic */ OutgoingTextMessage f$2;
                public final /* synthetic */ long f$3;
                public final /* synthetic */ MessageSendType f$4;
                public final /* synthetic */ String f$5;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r6;
                    this.f$5 = r7;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationParentFragment.this.lambda$sendTextMessage$70(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
                }
            }).execute();
            return;
        }
        showDefaultSmsPrompt();
    }

    public /* synthetic */ void lambda$sendTextMessage$70(Context context, OutgoingTextMessage outgoingTextMessage, long j, MessageSendType messageSendType, String str) {
        long nextLong = new SecureRandom().nextLong();
        SimpleTask.run(new SimpleTask.BackgroundTask(context, outgoingTextMessage, j, messageSendType, str) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda51
            public final /* synthetic */ Context f$0;
            public final /* synthetic */ OutgoingTextMessage f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ MessageSendType f$3;
            public final /* synthetic */ String f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r5;
                this.f$4 = r6;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationParentFragment.lambda$sendTextMessage$69(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }, new ConversationParentFragment$$ExternalSyntheticLambda14(this));
        silentlySetComposeText("");
        this.fragment.stageOutgoingMessage(outgoingTextMessage, nextLong);
    }

    public static /* synthetic */ Long lambda$sendTextMessage$69(Context context, OutgoingTextMessage outgoingTextMessage, long j, MessageSendType messageSendType, String str) {
        return Long.valueOf(MessageSender.send(context, outgoingTextMessage, j, messageSendType.usesSmsTransport(), str, (MessageDatabase.InsertListener) null));
    }

    private void showDefaultSmsPrompt() {
        new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ConversationActivity_signal_cannot_sent_sms_mms_messages_because_it_is_not_your_default_sms_app).setNegativeButton(R.string.ConversationActivity_no, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).setPositiveButton(R.string.ConversationActivity_yes, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda5
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ConversationParentFragment.this.lambda$showDefaultSmsPrompt$72(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$showDefaultSmsPrompt$72(DialogInterface dialogInterface, int i) {
        handleMakeDefaultSms();
    }

    public void updateToggleButtonState() {
        if (this.inputPanel.isRecordingInLockedMode()) {
            this.buttonToggle.display(this.sendButton);
            this.quickAttachmentToggle.show();
            this.inlineAttachmentToggle.hide();
        } else if (this.draftViewModel.hasVoiceNoteDraft()) {
            this.buttonToggle.display(this.sendButton);
            this.quickAttachmentToggle.hide();
            this.inlineAttachmentToggle.hide();
        } else if (this.composeText.getText().length() != 0 || this.attachmentManager.isAttachmentPresent()) {
            this.buttonToggle.display(this.sendButton);
            this.quickAttachmentToggle.hide();
            if (this.attachmentManager.isAttachmentPresent() || this.linkPreviewViewModel.hasLinkPreviewUi()) {
                this.inlineAttachmentToggle.hide();
            } else {
                this.inlineAttachmentToggle.show();
            }
        } else {
            this.buttonToggle.display(this.attachButton);
            this.quickAttachmentToggle.show();
            this.inlineAttachmentToggle.hide();
        }
    }

    public void onViewModelEvent(ConversationViewModel.Event event) {
        if (event == ConversationViewModel.Event.SHOW_RECAPTCHA) {
            RecaptchaProofBottomSheetFragment.show(getChildFragmentManager());
            return;
        }
        throw new AssertionError("Unexpected event!");
    }

    private void updateLinkPreviewState() {
        if (!SignalStore.settings().isLinkPreviewsEnabled() || !this.viewModel.isPushAvailable() || this.sendButton.getSelectedSendType().usesSmsTransport() || this.attachmentManager.isAttachmentPresent() || getContext() == null) {
            this.linkPreviewViewModel.onUserCancel();
            return;
        }
        this.linkPreviewViewModel.onEnabled();
        this.linkPreviewViewModel.onTextChanged(requireContext(), this.composeText.getTextTrimmed().toString(), this.composeText.getSelectionStart(), this.composeText.getSelectionEnd());
    }

    private void recordTransportPreference(final MessageSendType messageSendType) {
        new AsyncTask<Void, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.20
            public Void doInBackground(Void... voidArr) {
                RecipientDatabase recipients = SignalDatabase.recipients();
                recipients.setDefaultSubscriptionId(ConversationParentFragment.this.recipient.getId(), messageSendType.getSimSubscriptionIdOr(-1));
                if (ConversationParentFragment.this.recipient.resolve().isPushGroup()) {
                    return null;
                }
                recipients.setForceSmsSelection(ConversationParentFragment.this.recipient.getId(), ConversationParentFragment.this.recipient.get().getRegistered() == RecipientDatabase.RegisteredState.REGISTERED && messageSendType.usesSmsTransport());
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onRecorderPermissionRequired() {
        Permissions.with(this).request("android.permission.RECORD_AUDIO").ifNecessary().withRationaleDialog(getString(R.string.ConversationActivity_to_send_audio_messages_allow_signal_access_to_your_microphone), R.drawable.ic_mic_solid_24).withPermanentDenialDialog(getString(R.string.ConversationActivity_signal_requires_the_microphone_permission_in_order_to_send_audio_messages)).execute();
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onRecorderStarted() {
        ServiceUtil.getVibrator(requireContext()).vibrate(20);
        requireActivity().getWindow().addFlags(128);
        requireActivity().setRequestedOrientation(14);
        this.voiceNoteMediaController.pausePlayback();
        this.audioRecorder.startRecording();
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onRecorderLocked() {
        this.voiceRecorderWakeLock.acquire();
        updateToggleButtonState();
        requireActivity().setRequestedOrientation(-1);
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onRecorderFinished() {
        this.voiceRecorderWakeLock.release();
        updateToggleButtonState();
        ServiceUtil.getVibrator(requireContext()).vibrate(20);
        requireActivity().getWindow().clearFlags(128);
        requireActivity().setRequestedOrientation(-1);
        this.audioRecorder.stopRecording().addListener(new ListenableFuture.Listener<VoiceNoteDraft>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.21
            public void onSuccess(VoiceNoteDraft voiceNoteDraft) {
                ConversationParentFragment.this.sendVoiceNote(voiceNoteDraft.getUri(), voiceNoteDraft.getSize());
            }

            @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
            public void onFailure(ExecutionException executionException) {
                Toast.makeText(ConversationParentFragment.this.requireContext(), (int) R.string.ConversationActivity_unable_to_record_audio, 1).show();
            }
        });
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onRecorderCanceled() {
        this.voiceRecorderWakeLock.release();
        updateToggleButtonState();
        ServiceUtil.getVibrator(requireContext()).vibrate(50);
        requireActivity().getWindow().clearFlags(128);
        requireActivity().setRequestedOrientation(-1);
        ListenableFuture<VoiceNoteDraft> stopRecording = this.audioRecorder.stopRecording();
        if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED)) {
            stopRecording.addListener(new DeleteCanceledVoiceNoteListener());
        } else {
            this.draftViewModel.setVoiceNoteDraftFuture(stopRecording);
        }
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onEmojiToggle() {
        if (!this.emojiDrawerStub.resolved()) {
            initializeMediaKeyboardProviders();
        }
        this.inputPanel.setMediaKeyboard(this.emojiDrawerStub.get());
        this.emojiDrawerStub.get().setFragmentManager(getChildFragmentManager());
        if (this.container.getCurrentInput() == this.emojiDrawerStub.get()) {
            this.container.showSoftkey(this.composeText);
        } else {
            this.container.show(this.composeText, this.emojiDrawerStub.get());
        }
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onLinkPreviewCanceled() {
        this.linkPreviewViewModel.onUserCancel();
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.Listener
    public void onStickerSuggestionSelected(StickerRecord stickerRecord) {
        sendSticker(stickerRecord, true);
    }

    @Override // org.thoughtcrime.securesms.components.InputPanel.MediaListener
    public void onMediaSelected(Uri uri, String str) {
        if (MediaUtil.isGif(str) || MediaUtil.isImageType(str)) {
            SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(uri) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda49
                public final /* synthetic */ Uri f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return ConversationParentFragment.this.lambda$onMediaSelected$73(this.f$1);
                }
            }, new SimpleTask.ForegroundTask(uri, str) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda50
                public final /* synthetic */ Uri f$1;
                public final /* synthetic */ String f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    ConversationParentFragment.this.lambda$onMediaSelected$74(this.f$1, this.f$2, (ConversationParentFragment.KeyboardImageDetails) obj);
                }
            });
        } else if (MediaUtil.isVideoType(str)) {
            setMedia(uri, SlideFactory.MediaType.VIDEO);
        } else if (MediaUtil.isAudioType(str)) {
            setMedia(uri, SlideFactory.MediaType.AUDIO);
        }
    }

    @Override // org.thoughtcrime.securesms.components.ComposeText.CursorPositionChangedListener
    public void onCursorPositionChanged(int i, int i2) {
        this.linkPreviewViewModel.onTextChanged(requireContext(), this.composeText.getTextTrimmed().toString(), i, i2);
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerEventListener
    public void onStickerSelected(StickerRecord stickerRecord) {
        sendSticker(stickerRecord, false);
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerEventListener
    public void onStickerManagementClicked() {
        startActivity(StickerManagementActivity.getIntent(requireContext()));
        this.container.hideAttachedInput(true);
    }

    public void sendVoiceNote(final Uri uri, long j) {
        boolean z = this.threadId == -1;
        long millis = TimeUnit.SECONDS.toMillis((long) this.recipient.get().getExpiresInSeconds());
        AudioSlide audioSlide = new AudioSlide(requireContext(), uri, j, MediaUtil.AUDIO_AAC, true);
        SlideDeck slideDeck = new SlideDeck();
        slideDeck.addSlide(audioSlide);
        sendMediaMessage(this.recipient.getId(), this.sendButton.getSelectedSendType(), "", slideDeck, this.inputPanel.getQuote().orElse(null), Collections.emptyList(), Collections.emptyList(), this.composeText.getMentions(), millis, false, z, true, null).addListener(new AssertedSuccessListener<Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.22
            public void onSuccess(Void r2) {
                ConversationParentFragment.this.draftViewModel.deleteBlob(uri);
            }
        });
    }

    private void sendSticker(StickerRecord stickerRecord, boolean z) {
        sendSticker(new StickerLocator(stickerRecord.getPackId(), stickerRecord.getPackKey(), stickerRecord.getStickerId(), stickerRecord.getEmoji()), stickerRecord.getContentType(), stickerRecord.getUri(), stickerRecord.getSize(), z);
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda54
            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.lambda$sendSticker$75(StickerRecord.this);
            }
        });
    }

    public static /* synthetic */ void lambda$sendSticker$75(StickerRecord stickerRecord) {
        SignalDatabase.stickers().updateStickerLastUsedTime(stickerRecord.getRowId(), System.currentTimeMillis());
    }

    private void sendSticker(StickerLocator stickerLocator, String str, Uri uri, long j, boolean z) {
        if (this.sendButton.getSelectedSendType().usesSmsTransport()) {
            startActivityForResult(MediaSelectionActivity.editor(requireContext(), this.sendButton.getSelectedSendType(), Collections.singletonList(new Media(uri, str, System.currentTimeMillis(), 512, 512, j, 0, false, false, Optional.empty(), Optional.empty(), Optional.empty())), this.recipient.getId(), this.composeText.getTextTrimmed()), 12);
            return;
        }
        long millis = TimeUnit.SECONDS.toMillis((long) this.recipient.get().getExpiresInSeconds());
        boolean z2 = this.threadId == -1;
        MessageSendType selectedSendType = this.sendButton.getSelectedSendType();
        SlideDeck slideDeck = new SlideDeck();
        slideDeck.addSlide(new StickerSlide(requireContext(), uri, j, stickerLocator, str));
        sendMediaMessage(this.recipient.getId(), selectedSendType, "", slideDeck, null, Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), millis, false, z2, z, null);
    }

    private void silentlySetComposeText(String str) {
        this.typingTextWatcher.setEnabled(false);
        this.composeText.setText(str);
        this.typingTextWatcher.setEnabled(true);
    }

    @Override // org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment.Callback
    public void onReactionsDialogDismissed() {
        this.fragment.clearFocusedItem();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onShown() {
        InputPanel inputPanel = this.inputPanel;
        if (inputPanel != null) {
            inputPanel.getMediaKeyboardListener().onShown();
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onHidden() {
        InputPanel inputPanel = this.inputPanel;
        if (inputPanel != null) {
            inputPanel.getMediaKeyboardListener().onHidden();
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onKeyboardChanged(KeyboardPage keyboardPage) {
        InputPanel inputPanel = this.inputPanel;
        if (inputPanel != null) {
            inputPanel.getMediaKeyboardListener().onKeyboardChanged(keyboardPage);
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        InputPanel inputPanel = this.inputPanel;
        if (inputPanel != null) {
            inputPanel.onEmojiSelected(str);
            if (this.recentEmojis == null) {
                this.recentEmojis = new RecentEmojiPageModel(ApplicationDependencies.getApplication(), TextSecurePreferences.RECENT_STORAGE_KEY);
            }
            this.recentEmojis.onCodePointSelected(str);
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
        if (keyEvent != null) {
            this.inputPanel.onKeyEvent(keyEvent);
        }
    }

    @Override // org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment.Host
    public void openGifSearch() {
        AttachmentManager.selectGif(this, 10, this.recipient.getId(), this.sendButton.getSelectedSendType(), isMms(), this.composeText.getTextTrimmed());
    }

    @Override // org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment.Host
    public void onGifSelectSuccess(Uri uri, int i, int i2) {
        SlideFactory.MediaType from = SlideFactory.MediaType.from(BlobProvider.getMimeType(uri));
        Objects.requireNonNull(from);
        setMedia(uri, from, i, i2, false, true);
    }

    @Override // org.thoughtcrime.securesms.keyboard.gif.GifKeyboardPageFragment.Host
    public boolean isMms() {
        return !this.viewModel.isPushAvailable();
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.EmojiKeyboardPageFragment.Callback
    public void openEmojiSearch() {
        if (this.emojiDrawerStub.resolved()) {
            this.emojiDrawerStub.get().onOpenEmojiSearch();
        }
    }

    @Override // org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment.Callback
    public void closeEmojiSearch() {
        if (this.emojiDrawerStub.resolved()) {
            this.emojiDrawerStub.get().onCloseEmojiSearch();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.VoiceNoteDraftView.Listener
    public void onVoiceNoteDraftPlay(Uri uri, double d) {
        this.voiceNoteMediaController.startSinglePlaybackForDraft(uri, this.threadId, d);
    }

    @Override // org.thoughtcrime.securesms.conversation.VoiceNoteDraftView.Listener
    public void onVoiceNoteDraftPause(Uri uri) {
        this.voiceNoteMediaController.pausePlayback(uri);
    }

    @Override // org.thoughtcrime.securesms.conversation.VoiceNoteDraftView.Listener
    public void onVoiceNoteDraftSeekTo(Uri uri, double d) {
        this.voiceNoteMediaController.seekToPosition(uri, d);
    }

    @Override // org.thoughtcrime.securesms.conversation.VoiceNoteDraftView.Listener
    public void onVoiceNoteDraftDelete(Uri uri) {
        this.voiceNoteMediaController.stopPlaybackAndReset(uri);
        this.draftViewModel.deleteVoiceNoteDraft();
    }

    @Override // org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner
    public VoiceNoteMediaController getVoiceNoteMediaController() {
        return this.voiceNoteMediaController;
    }

    @Override // org.thoughtcrime.securesms.keyboard.sticker.StickerKeyboardPageFragment.Callback
    public void openStickerSearch() {
        StickerSearchDialogFragment.show(getChildFragmentManager());
    }

    @Override // org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder
    public void bindScrollHelper(RecyclerView recyclerView) {
        this.material3OnScrollHelper.attach(recyclerView);
    }

    @Override // org.thoughtcrime.securesms.messagedetails.MessageDetailsFragment.Callback
    public void onMessageDetailsFragmentDismissed() {
        this.material3OnScrollHelper.setColorImmediate();
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void sendAnywayAfterSafetyNumberChangedInBottomSheet(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Log.d(TAG, "onSendAnywayAfterSafetyNumberChange");
        initializeIdentityRecords().addListener(new AssertedSuccessListener<Boolean>() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment.23
            public void onSuccess(Boolean bool) {
                ConversationParentFragment.this.sendMessage(null);
            }
        });
    }

    /* loaded from: classes4.dex */
    private final class DeleteCanceledVoiceNoteListener implements ListenableFuture.Listener<VoiceNoteDraft> {
        @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
        public void onFailure(ExecutionException executionException) {
        }

        private DeleteCanceledVoiceNoteListener() {
            ConversationParentFragment.this = r1;
        }

        public void onSuccess(VoiceNoteDraft voiceNoteDraft) {
            ConversationParentFragment.this.draftViewModel.deleteBlob(voiceNoteDraft.getUri());
        }
    }

    /* loaded from: classes4.dex */
    public class QuickCameraToggleListener implements View.OnClickListener {
        private QuickCameraToggleListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Permissions.with(ConversationParentFragment.this.requireActivity()).request("android.permission.CAMERA").ifNecessary().withRationaleDialog(ConversationParentFragment.this.getString(R.string.ConversationActivity_to_capture_photos_and_video_allow_signal_access_to_the_camera), R.drawable.ic_camera_24).withPermanentDenialDialog(ConversationParentFragment.this.getString(R.string.ConversationActivity_signal_needs_the_camera_permission_to_take_photos_or_video)).onAllGranted(new ConversationParentFragment$QuickCameraToggleListener$$ExternalSyntheticLambda0(this)).onAnyDenied(new ConversationParentFragment$QuickCameraToggleListener$$ExternalSyntheticLambda1(this)).execute();
        }

        public /* synthetic */ void lambda$onClick$0() {
            ConversationParentFragment.this.composeText.clearFocus();
            ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
            conversationParentFragment.startActivityForResult(MediaSelectionActivity.camera(conversationParentFragment.requireActivity(), ConversationParentFragment.this.sendButton.getSelectedSendType(), ConversationParentFragment.this.recipient.getId(), ConversationParentFragment.this.inputPanel.getQuote().isPresent()), 12);
            ConversationParentFragment.this.requireActivity().overridePendingTransition(R.anim.camera_slide_from_bottom, R.anim.stationary);
        }

        public /* synthetic */ void lambda$onClick$1() {
            Toast.makeText(ConversationParentFragment.this.requireContext(), (int) R.string.ConversationActivity_signal_needs_camera_permissions_to_take_photos_or_video, 1).show();
        }
    }

    /* loaded from: classes4.dex */
    public class SendButtonListener implements View.OnClickListener, TextView.OnEditorActionListener {
        private SendButtonListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            String str;
            if (ConversationParentFragment.this.recipient.get().isGroup()) {
                str = SignalLocalMetrics.GroupMessageSend.start();
            } else {
                str = SignalLocalMetrics.IndividualMessageSend.start();
            }
            ConversationParentFragment.this.sendMessage(str);
        }

        @Override // android.widget.TextView.OnEditorActionListener
        public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
            if (i != 4) {
                return false;
            }
            ConversationParentFragment.this.sendButton.performClick();
            return true;
        }
    }

    /* loaded from: classes4.dex */
    public class AttachButtonListener implements View.OnClickListener {
        private AttachButtonListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ConversationParentFragment.this.handleAddAttachment();
        }
    }

    /* loaded from: classes4.dex */
    public class AttachButtonLongClickListener implements View.OnLongClickListener {
        private AttachButtonLongClickListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // android.view.View.OnLongClickListener
        public boolean onLongClick(View view) {
            return ConversationParentFragment.this.sendButton.performLongClick();
        }
    }

    /* loaded from: classes4.dex */
    public class ComposeKeyPressedListener implements View.OnKeyListener, View.OnClickListener, TextWatcher, View.OnFocusChangeListener {
        int beforeLength;

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        private ComposeKeyPressedListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // android.view.View.OnKeyListener
        public boolean onKey(View view, int i, KeyEvent keyEvent) {
            if (keyEvent.getAction() != 0 || i != 66) {
                return false;
            }
            if (!SignalStore.settings().isEnterKeySends() && !keyEvent.isCtrlPressed()) {
                return false;
            }
            ConversationParentFragment.this.sendButton.dispatchKeyEvent(new KeyEvent(0, 66));
            ConversationParentFragment.this.sendButton.dispatchKeyEvent(new KeyEvent(1, 66));
            return true;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ConversationParentFragment.this.container.showSoftkey(ConversationParentFragment.this.composeText);
        }

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            this.beforeLength = ConversationParentFragment.this.composeText.getTextTrimmed().length();
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            ConversationParentFragment.this.calculateCharactersRemaining();
            if (ConversationParentFragment.this.composeText.getTextTrimmed().length() == 0 || this.beforeLength == 0) {
                ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
                conversationParentFragment.composeText.postDelayed(new ConversationParentFragment$ComposeKeyPressedListener$$ExternalSyntheticLambda0(conversationParentFragment), 50);
            }
            ConversationParentFragment.this.stickerViewModel.onInputTextUpdated(editable.toString());
        }

        @Override // android.view.View.OnFocusChangeListener
        public void onFocusChange(View view, boolean z) {
            if (z && ConversationParentFragment.this.container.getCurrentInput() == ConversationParentFragment.this.emojiDrawerStub.get()) {
                ConversationParentFragment.this.container.showSoftkey(ConversationParentFragment.this.composeText);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class TypingStatusTextWatcher extends SimpleTextWatcher {
        private boolean enabled;
        private String previousText;

        private TypingStatusTextWatcher() {
            ConversationParentFragment.this = r1;
            this.enabled = true;
            this.previousText = "";
        }

        @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
        public void onTextChanged(String str) {
            if (this.enabled && ConversationParentFragment.this.threadId > 0 && ConversationParentFragment.this.viewModel.isPushAvailable() && !ConversationParentFragment.this.isSmsForced() && !ConversationParentFragment.this.recipient.get().isBlocked() && !ConversationParentFragment.this.recipient.get().isSelf()) {
                TypingStatusSender typingStatusSender = ApplicationDependencies.getTypingStatusSender();
                if (str.length() == 0) {
                    typingStatusSender.onTypingStoppedWithNotify(ConversationParentFragment.this.threadId);
                } else if (str.length() >= this.previousText.length() || !this.previousText.contains(str)) {
                    typingStatusSender.onTypingStarted(ConversationParentFragment.this.threadId);
                } else {
                    typingStatusSender.onTypingStopped(ConversationParentFragment.this.threadId);
                }
                this.previousText = str;
            }
        }

        public void setEnabled(boolean z) {
            this.enabled = z;
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onMessageRequest(MessageRequestViewModel messageRequestViewModel) {
        this.messageRequestBottomView.setAcceptOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda62
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MessageRequestViewModel.this.onAccept();
            }
        });
        this.messageRequestBottomView.setDeleteOnClickListener(new View.OnClickListener(messageRequestViewModel) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda63
            public final /* synthetic */ MessageRequestViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationParentFragment.this.lambda$onMessageRequest$77(this.f$1, view);
            }
        });
        this.messageRequestBottomView.setBlockOnClickListener(new View.OnClickListener(messageRequestViewModel) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda64
            public final /* synthetic */ MessageRequestViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationParentFragment.this.lambda$onMessageRequest$78(this.f$1, view);
            }
        });
        this.messageRequestBottomView.setUnblockOnClickListener(new View.OnClickListener(messageRequestViewModel) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda65
            public final /* synthetic */ MessageRequestViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationParentFragment.this.lambda$onMessageRequest$79(this.f$1, view);
            }
        });
        this.messageRequestBottomView.setGroupV1MigrationContinueListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda66
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationParentFragment.this.lambda$onMessageRequest$80(view);
            }
        });
        messageRequestViewModel.getRequestReviewDisplayState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda67
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.presentRequestReviewBanner((MessageRequestViewModel.RequestReviewDisplayState) obj);
            }
        });
        messageRequestViewModel.getMessageData().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda68
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.presentMessageRequestState((MessageRequestViewModel.MessageData) obj);
            }
        });
        messageRequestViewModel.getFailures().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda69
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.showGroupChangeErrorToast((GroupChangeFailureReason) obj);
            }
        });
        messageRequestViewModel.getMessageRequestStatus().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda70
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationParentFragment.this.lambda$onMessageRequest$81((MessageRequestViewModel.Status) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onMessageRequest$77(MessageRequestViewModel messageRequestViewModel, View view) {
        onMessageRequestDeleteClicked(messageRequestViewModel);
    }

    public /* synthetic */ void lambda$onMessageRequest$78(MessageRequestViewModel messageRequestViewModel, View view) {
        onMessageRequestBlockClicked(messageRequestViewModel);
    }

    public /* synthetic */ void lambda$onMessageRequest$79(MessageRequestViewModel messageRequestViewModel, View view) {
        onMessageRequestUnblockClicked(messageRequestViewModel);
    }

    public /* synthetic */ void lambda$onMessageRequest$80(View view) {
        GroupsV1MigrationInitiationBottomSheetDialogFragment.showForInitiation(getChildFragmentManager(), this.recipient.getId());
    }

    public /* synthetic */ void lambda$onMessageRequest$81(MessageRequestViewModel.Status status) {
        switch (AnonymousClass24.$SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$Status[status.ordinal()]) {
            case 1:
                hideMessageRequestBusy();
                return;
            case 2:
            case 3:
            case 4:
                showMessageRequestBusy();
                return;
            case 5:
                hideMessageRequestBusy();
                return;
            case 6:
                hideMessageRequestBusy();
                Toast.makeText(requireContext(), (int) R.string.ConversationActivity__reported_as_spam_and_blocked, 0).show();
                return;
            case 7:
            case 8:
                hideMessageRequestBusy();
                requireActivity().finish();
                return;
            default:
                return;
        }
    }

    public void presentRequestReviewBanner(MessageRequestViewModel.RequestReviewDisplayState requestReviewDisplayState) {
        int i = AnonymousClass24.$SwitchMap$org$thoughtcrime$securesms$messagerequests$MessageRequestViewModel$RequestReviewDisplayState[requestReviewDisplayState.ordinal()];
        if (i == 1) {
            this.reviewBanner.get().setVisibility(0);
            this.reviewBanner.get().setBannerMessage(new SpannableStringBuilder().append(SpanUtil.bold(getString(R.string.ConversationFragment__review_requests_carefully))).append((CharSequence) " ").append((CharSequence) getString(R.string.ConversationFragment__signal_found_another_contact_with_the_same_name)));
            Drawable mutate = ContextUtil.requireDrawable(requireContext(), R.drawable.ic_info_white_24).mutate();
            DrawableCompat.setTint(mutate, ContextCompat.getColor(requireContext(), R.color.signal_icon_tint_primary));
            this.reviewBanner.get().setBannerIcon(mutate);
            this.reviewBanner.get().setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda74
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationParentFragment.this.lambda$presentRequestReviewBanner$82(view);
                }
            });
        } else if (i == 2) {
            this.reviewBanner.get().setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$presentRequestReviewBanner$82(View view) {
        handleReviewRequest(this.recipient.getId());
    }

    public void presentGroupReviewBanner(ConversationGroupViewModel.ReviewState reviewState) {
        if (reviewState.getCount() > 0) {
            this.reviewBanner.get().setVisibility(0);
            this.reviewBanner.get().setBannerMessage(getString(R.string.ConversationFragment__d_group_members_have_the_same_name, Integer.valueOf(reviewState.getCount())));
            this.reviewBanner.get().setBannerRecipient(reviewState.getRecipient());
            this.reviewBanner.get().setOnClickListener(new View.OnClickListener(reviewState) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda16
                public final /* synthetic */ ConversationGroupViewModel.ReviewState f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationParentFragment.this.lambda$presentGroupReviewBanner$83(this.f$1, view);
                }
            });
        } else if (this.reviewBanner.resolved()) {
            this.reviewBanner.get().setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$presentGroupReviewBanner$83(ConversationGroupViewModel.ReviewState reviewState, View view) {
        handleReviewGroupMembers(reviewState.getGroupId());
    }

    private void showMessageRequestBusy() {
        this.messageRequestBottomView.showBusy();
    }

    private void hideMessageRequestBusy() {
        this.messageRequestBottomView.hideBusy();
    }

    private void handleReviewGroupMembers(GroupId.V2 v2) {
        if (v2 != null) {
            ReviewCardDialogFragment.createForReviewMembers(v2).show(getChildFragmentManager(), (String) null);
        }
    }

    private void handleReviewRequest(RecipientId recipientId) {
        if (recipientId != Recipient.UNKNOWN.getId()) {
            ReviewCardDialogFragment.createForReviewRequest(recipientId).show(getChildFragmentManager(), (String) null);
        }
    }

    public void showGroupChangeErrorToast(GroupChangeFailureReason groupChangeFailureReason) {
        Toast.makeText(requireContext(), GroupErrors.getUserDisplayMessage(groupChangeFailureReason), 1).show();
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void handleReaction(ConversationMessage conversationMessage, ConversationReactionOverlay.OnActionSelectedListener onActionSelectedListener, SelectedConversationModel selectedConversationModel, ConversationReactionOverlay.OnHideListener onHideListener) {
        this.reactionDelegate.setOnActionSelectedListener(onActionSelectedListener);
        this.reactionDelegate.setOnHideListener(onHideListener);
        this.reactionDelegate.show(requireActivity(), this.recipient.get(), conversationMessage, this.groupViewModel.isNonAdminInAnnouncementGroup(), selectedConversationModel);
        if (this.attachmentKeyboardStub.resolved()) {
            this.attachmentKeyboardStub.get().hide(true);
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onMessageWithErrorClicked(MessageRecord messageRecord) {
        if (messageRecord.isIdentityMismatchFailure()) {
            SafetyNumberBottomSheet.forMessageRecord(requireContext(), messageRecord).show(getChildFragmentManager());
        } else if (messageRecord.hasFailedWithNetworkFailures()) {
            new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.conversation_activity__message_could_not_be_sent).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.conversation_activity__send, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda82
                public final /* synthetic */ MessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ConversationParentFragment.this.lambda$onMessageWithErrorClicked$85(this.f$1, dialogInterface, i);
                }
            }).show();
        } else {
            MessageDetailsFragment.create(messageRecord, this.recipient.getId()).show(getChildFragmentManager(), (String) null);
        }
    }

    public /* synthetic */ void lambda$onMessageWithErrorClicked$85(MessageRecord messageRecord, DialogInterface dialogInterface, int i) {
        SignalExecutors.BOUNDED.execute(new Runnable(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda72
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationParentFragment.this.lambda$onMessageWithErrorClicked$84(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onMessageWithErrorClicked$84(MessageRecord messageRecord) {
        MessageSender.resend(requireContext(), messageRecord);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onVoiceNotePause(Uri uri) {
        this.voiceNoteMediaController.pausePlayback(uri);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onVoiceNotePlay(Uri uri, long j, double d) {
        this.voiceNoteMediaController.startConsecutivePlayback(uri, j, d);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onVoiceNoteResume(Uri uri, long j) {
        this.voiceNoteMediaController.resumePlayback(uri, j);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onVoiceNoteSeekTo(Uri uri, double d) {
        this.voiceNoteMediaController.seekToPosition(uri, d);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onVoiceNotePlaybackSpeedChanged(Uri uri, float f) {
        this.voiceNoteMediaController.setPlaybackSpeed(uri, f);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onRegisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer) {
        this.voiceNoteMediaController.getVoiceNotePlaybackState().observe(getViewLifecycleOwner(), observer);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onUnregisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer) {
        this.voiceNoteMediaController.getVoiceNotePlaybackState().removeObserver(observer);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onCursorChanged() {
        if (this.reactionDelegate.isShowing()) {
            SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda58
                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return ConversationParentFragment.this.lambda$onCursorChanged$86();
                }
            }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda59
                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    ConversationParentFragment.this.lambda$onCursorChanged$87((Boolean) obj);
                }
            });
        }
    }

    public /* synthetic */ Boolean lambda$onCursorChanged$86() {
        return Boolean.valueOf(SignalDatabase.mmsSms().checkMessageExists(this.reactionDelegate.getMessageRecord()));
    }

    public /* synthetic */ void lambda$onCursorChanged$87(Boolean bool) {
        if (!bool.booleanValue()) {
            this.reactionDelegate.hide();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public int getSendButtonTint() {
        return getSendButtonColor(this.sendButton.getSelectedSendType());
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public boolean isKeyboardOpen() {
        return this.container.isKeyboardOpen();
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public boolean isAttachmentKeyboardOpen() {
        return this.attachmentKeyboardStub.resolved() && this.attachmentKeyboardStub.get().isShowing();
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void openAttachmentKeyboard() {
        this.attachmentKeyboardStub.get().show(this.container.getKeyboardHeight(), true);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void setThreadId(long j) {
        this.threadId = j;
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void handleReplyMessage(ConversationMessage conversationMessage) {
        Recipient recipient;
        SlideDeck slideDeck;
        if (this.isSearchRequested) {
            this.searchViewItem.collapseActionView();
        }
        MessageRecord messageRecord = conversationMessage.getMessageRecord();
        if (messageRecord.isOutgoing()) {
            recipient = Recipient.self();
        } else {
            recipient = messageRecord.getIndividualRecipient();
        }
        if (messageRecord.isMms()) {
            MmsMessageRecord mmsMessageRecord = (MmsMessageRecord) messageRecord;
            if (!mmsMessageRecord.getSharedContacts().isEmpty()) {
                Contact contact = mmsMessageRecord.getSharedContacts().get(0);
                String string = getString(R.string.ConversationActivity_quoted_contact_message, EmojiStrings.BUST_IN_SILHOUETTE, ContactUtil.getDisplayName(contact));
                SlideDeck slideDeck2 = new SlideDeck();
                if (contact.getAvatarAttachment() != null) {
                    slideDeck2.addSlide(MediaUtil.getSlideForAttachment(requireContext(), contact.getAvatarAttachment()));
                }
                this.inputPanel.setQuote(GlideApp.with(this), messageRecord.getDateSent(), recipient, string, slideDeck2, MessageRecordUtil.getRecordQuoteType(messageRecord));
                this.inputPanel.clickOnComposeInput();
            }
        }
        if (messageRecord.isMms()) {
            MmsMessageRecord mmsMessageRecord2 = (MmsMessageRecord) messageRecord;
            if (!mmsMessageRecord2.getLinkPreviews().isEmpty()) {
                LinkPreview linkPreview = mmsMessageRecord2.getLinkPreviews().get(0);
                SlideDeck slideDeck3 = new SlideDeck();
                if (linkPreview.getThumbnail().isPresent()) {
                    slideDeck3.addSlide(MediaUtil.getSlideForAttachment(requireContext(), linkPreview.getThumbnail().get()));
                }
                this.inputPanel.setQuote(GlideApp.with(this), messageRecord.getDateSent(), recipient, conversationMessage.getDisplayBody(requireContext()), slideDeck3, MessageRecordUtil.getRecordQuoteType(messageRecord));
                this.inputPanel.clickOnComposeInput();
            }
        }
        SlideDeck slideDeck4 = messageRecord.isMms() ? ((MmsMessageRecord) messageRecord).getSlideDeck() : new SlideDeck();
        if (!messageRecord.isMms() || !((MmsMessageRecord) messageRecord).isViewOnce()) {
            slideDeck = slideDeck4;
        } else {
            TombstoneAttachment tombstoneAttachment = new TombstoneAttachment(MediaUtil.VIEW_ONCE, true);
            SlideDeck slideDeck5 = new SlideDeck();
            slideDeck5.addSlide(MediaUtil.getSlideForAttachment(requireContext(), tombstoneAttachment));
            slideDeck = slideDeck5;
        }
        this.inputPanel.setQuote(GlideApp.with(this), messageRecord.getDateSent(), recipient, conversationMessage.getDisplayBody(requireContext()), slideDeck, MessageRecordUtil.getRecordQuoteType(messageRecord));
        this.inputPanel.clickOnComposeInput();
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onMessageActionToolbarOpened() {
        this.searchViewItem.collapseActionView();
        this.toolbar.setVisibility(8);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onMessageActionToolbarClosed() {
        this.toolbar.setVisibility(0);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onBottomActionBarVisibilityChanged(int i) {
        this.inputPanel.setHideForSelection(i == 0);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentListener
    public void onForwardClicked() {
        this.inputPanel.clearQuote();
    }

    @Override // org.thoughtcrime.securesms.mms.AttachmentManager.AttachmentListener
    public void onAttachmentChanged() {
        handleSecurityChange(this.viewModel.getConversationStateSnapshot().getSecurityInfo());
        updateToggleButtonState();
        updateLinkPreviewState();
    }

    private void onMessageRequestDeleteClicked(MessageRequestViewModel messageRequestViewModel) {
        Recipient value = messageRequestViewModel.getRecipient().getValue();
        if (value == null) {
            Log.w(TAG, "[onMessageRequestDeleteClicked] No recipient!");
            return;
        }
        MaterialAlertDialogBuilder neutralButton = new MaterialAlertDialogBuilder(requireContext()).setNeutralButton(R.string.ConversationActivity_cancel, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda76
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        if (value.isGroup() && value.isBlocked()) {
            neutralButton.setTitle(R.string.ConversationActivity_delete_conversation);
            neutralButton.setMessage(R.string.ConversationActivity_this_conversation_will_be_deleted_from_all_of_your_devices);
            neutralButton.setPositiveButton(R.string.ConversationActivity_delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda77
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    MessageRequestViewModel.this.onDelete();
                }
            });
        } else if (value.isGroup()) {
            neutralButton.setTitle(R.string.ConversationActivity_delete_and_leave_group);
            neutralButton.setMessage(R.string.ConversationActivity_you_will_leave_this_group_and_it_will_be_deleted_from_all_of_your_devices);
            neutralButton.setNegativeButton(R.string.ConversationActivity_delete_and_leave, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda78
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    MessageRequestViewModel.this.onDelete();
                }
            });
        } else {
            neutralButton.setTitle(R.string.ConversationActivity_delete_conversation);
            neutralButton.setMessage(R.string.ConversationActivity_this_conversation_will_be_deleted_from_all_of_your_devices);
            neutralButton.setNegativeButton(R.string.ConversationActivity_delete, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda79
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    MessageRequestViewModel.this.onDelete();
                }
            });
        }
        neutralButton.show();
    }

    private void onMessageRequestBlockClicked(MessageRequestViewModel messageRequestViewModel) {
        Recipient value = messageRequestViewModel.getRecipient().getValue();
        if (value == null) {
            Log.w(TAG, "[onMessageRequestBlockClicked] No recipient!");
        } else {
            BlockUnblockDialog.showBlockAndReportSpamFor(requireContext(), getLifecycle(), value, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda38
                @Override // java.lang.Runnable
                public final void run() {
                    MessageRequestViewModel.this.onBlock();
                }
            }, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda39
                @Override // java.lang.Runnable
                public final void run() {
                    MessageRequestViewModel.this.onBlockAndReportSpam();
                }
            });
        }
    }

    private void onMessageRequestUnblockClicked(MessageRequestViewModel messageRequestViewModel) {
        Recipient value = messageRequestViewModel.getRecipient().getValue();
        if (value == null) {
            Log.w(TAG, "[onMessageRequestUnblockClicked] No recipient!");
        } else {
            BlockUnblockDialog.showUnblockFor(requireContext(), getLifecycle(), value, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationParentFragment$$ExternalSyntheticLambda80
                @Override // java.lang.Runnable
                public final void run() {
                    MessageRequestViewModel.this.onUnblock();
                }
            });
        }
    }

    private static void hideMenuItem(Menu menu, int i) {
        if (menu.findItem(i) != null) {
            menu.findItem(i).setVisible(false);
        }
    }

    /* renamed from: getKeyboardImageDetails */
    public KeyboardImageDetails lambda$onMediaSelected$73(Uri uri) {
        try {
            boolean z = true;
            Bitmap bitmap = this.glideRequests.asBitmap().load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).submit().get(1000, TimeUnit.MILLISECONDS);
            int pixel = bitmap.getPixel(0, 0);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            if (Color.alpha(pixel) >= 255) {
                z = false;
            }
            return new KeyboardImageDetails(width, height, z);
        } catch (InterruptedException | ExecutionException | TimeoutException unused) {
            return null;
        }
    }

    /* renamed from: sendKeyboardImage */
    public void lambda$onMediaSelected$74(Uri uri, String str, KeyboardImageDetails keyboardImageDetails) {
        if (keyboardImageDetails == null || !keyboardImageDetails.hasTransparency) {
            SlideFactory.MediaType from = SlideFactory.MediaType.from(str);
            Objects.requireNonNull(from);
            setMedia(uri, from);
            return;
        }
        long millis = TimeUnit.SECONDS.toMillis((long) this.recipient.get().getExpiresInSeconds());
        boolean z = this.threadId == -1;
        SlideDeck slideDeck = new SlideDeck();
        if (MediaUtil.isGif(str)) {
            slideDeck.addSlide(new GifSlide(requireContext(), uri, 0, keyboardImageDetails.width, keyboardImageDetails.height, keyboardImageDetails.hasTransparency, null));
        } else if (MediaUtil.isImageType(str)) {
            slideDeck.addSlide(new ImageSlide(requireContext(), uri, str, 0, keyboardImageDetails.width, keyboardImageDetails.height, keyboardImageDetails.hasTransparency, null, null));
        } else {
            throw new AssertionError("Only images are supported!");
        }
        sendMediaMessage(this.recipient.getId(), this.sendButton.getSelectedSendType(), "", slideDeck, null, Collections.emptyList(), Collections.emptyList(), this.composeText.getMentions(), millis, false, z, false, null);
    }

    /* loaded from: classes4.dex */
    public class UnverifiedDismissedListener implements UnverifiedBannerView.DismissListener {
        private UnverifiedDismissedListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.identity.UnverifiedBannerView.DismissListener
        public void onDismissed(List<IdentityRecord> list) {
            SimpleTask.run(new ConversationParentFragment$UnverifiedDismissedListener$$ExternalSyntheticLambda0(list), new ConversationParentFragment$UnverifiedDismissedListener$$ExternalSyntheticLambda1(this));
        }

        public static /* synthetic */ Object lambda$onDismissed$0(List list) {
            SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
            try {
                Iterator it = list.iterator();
                while (it.hasNext()) {
                    IdentityRecord identityRecord = (IdentityRecord) it.next();
                    ApplicationDependencies.getProtocolStore().aci().identities().setVerified(identityRecord.getRecipientId(), identityRecord.getIdentityKey(), IdentityDatabase.VerifiedStatus.DEFAULT);
                }
                if (acquire == null) {
                    return null;
                }
                acquire.close();
                return null;
            } catch (Throwable th) {
                if (acquire != null) {
                    try {
                        acquire.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        }

        public /* synthetic */ void lambda$onDismissed$1(Object obj) {
            ConversationParentFragment.this.initializeIdentityRecords();
        }
    }

    /* loaded from: classes4.dex */
    public class UnverifiedClickedListener implements UnverifiedBannerView.ClickListener {
        private UnverifiedClickedListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.identity.UnverifiedBannerView.ClickListener
        public void onClicked(List<IdentityRecord> list) {
            String str = ConversationParentFragment.TAG;
            Log.i(str, "onClicked: " + list.size());
            if (list.size() == 1) {
                ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
                conversationParentFragment.startActivity(VerifyIdentityActivity.newIntent(conversationParentFragment.requireContext(), list.get(0), false));
                return;
            }
            String[] strArr = new String[list.size()];
            for (int i = 0; i < list.size(); i++) {
                strArr[i] = Recipient.resolved(list.get(i).getRecipientId()).getDisplayName(ConversationParentFragment.this.requireContext());
            }
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(ConversationParentFragment.this.requireContext());
            materialAlertDialogBuilder.setIcon(R.drawable.ic_warning);
            materialAlertDialogBuilder.setTitle((CharSequence) "No longer verified");
            materialAlertDialogBuilder.setItems((CharSequence[]) strArr, (DialogInterface.OnClickListener) new ConversationParentFragment$UnverifiedClickedListener$$ExternalSyntheticLambda0(this, list));
            materialAlertDialogBuilder.show();
        }

        public /* synthetic */ void lambda$onClicked$0(List list, DialogInterface dialogInterface, int i) {
            ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
            conversationParentFragment.startActivity(VerifyIdentityActivity.newIntent(conversationParentFragment.requireContext(), (IdentityRecord) list.get(i), false));
        }
    }

    /* loaded from: classes4.dex */
    public class QuoteRestorationTask extends AsyncTask<Void, Void, ConversationMessage> {
        private final SettableFuture<Boolean> future;
        private final String serialized;

        QuoteRestorationTask(String str, SettableFuture<Boolean> settableFuture) {
            ConversationParentFragment.this = r1;
            this.serialized = str;
            this.future = settableFuture;
        }

        public ConversationMessage doInBackground(Void... voidArr) {
            QuoteId deserialize = QuoteId.deserialize(ApplicationDependencies.getApplication(), this.serialized);
            if (deserialize == null) {
                return null;
            }
            Application application = ApplicationDependencies.getApplication();
            MessageRecord messageFor = SignalDatabase.mmsSms().getMessageFor(deserialize.getId(), deserialize.getAuthor());
            if (messageFor == null) {
                return null;
            }
            return ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(application, messageFor);
        }

        public void onPostExecute(ConversationMessage conversationMessage) {
            if (conversationMessage != null) {
                ConversationParentFragment.this.handleReplyMessage(conversationMessage);
                this.future.set(Boolean.TRUE);
                return;
            }
            Log.e(ConversationParentFragment.TAG, "Failed to restore a quote from a draft. No matching message record.");
            this.future.set(Boolean.FALSE);
        }
    }

    /* loaded from: classes4.dex */
    public final class VoiceNotePlayerViewListener implements VoiceNotePlayerView.Listener {
        public static /* synthetic */ void lambda$onNavigateToMessage$0() {
        }

        private VoiceNotePlayerViewListener() {
            ConversationParentFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onCloseRequested(Uri uri) {
            ConversationParentFragment.this.voiceNoteMediaController.stopPlaybackAndReset(uri);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onSpeedChangeRequested(Uri uri, float f) {
            ConversationParentFragment.this.voiceNoteMediaController.setPlaybackSpeed(uri, f);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onPlay(Uri uri, long j, double d) {
            ConversationParentFragment.this.voiceNoteMediaController.startSinglePlayback(uri, j, d);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onPause(Uri uri) {
            ConversationParentFragment.this.voiceNoteMediaController.pausePlayback(uri);
        }

        @Override // org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.Listener
        public void onNavigateToMessage(long j, RecipientId recipientId, RecipientId recipientId2, long j2, long j3) {
            if (j != ConversationParentFragment.this.threadId) {
                ConversationParentFragment conversationParentFragment = ConversationParentFragment.this;
                conversationParentFragment.startActivity(ConversationIntents.createBuilder(conversationParentFragment.requireActivity(), recipientId, j).withStartingPosition((int) j3).build());
                return;
            }
            ConversationParentFragment.this.fragment.jumpToMessage(recipientId2, j2, new ConversationParentFragment$VoiceNotePlayerViewListener$$ExternalSyntheticLambda0());
        }
    }

    public void presentMessageRequestState(MessageRequestViewModel.MessageData messageData) {
        if (!Util.isEmpty(this.viewModel.getArgs().getDraftText()) || this.viewModel.getArgs().getMedia() != null || this.viewModel.getArgs().getStickerLocator() != null) {
            Log.d(TAG, "[presentMessageRequestState] Have extra, so ignoring provided state.");
            this.messageRequestBottomView.setVisibility(8);
            this.inputPanel.setHideForMessageRequestState(false);
        } else if (isPushGroupV1Conversation() && !isActiveGroup()) {
            Log.d(TAG, "[presentMessageRequestState] Inactive push group V1, so ignoring provided state.");
            this.messageRequestBottomView.setVisibility(8);
            this.inputPanel.setHideForMessageRequestState(false);
        } else if (messageData == null) {
            Log.d(TAG, "[presentMessageRequestState] Null messageData. Ignoring.");
        } else if (messageData.getMessageState() == MessageRequestState.NONE) {
            Log.d(TAG, "[presentMessageRequestState] No message request necessary.");
            this.messageRequestBottomView.setVisibility(8);
            this.inputPanel.setHideForMessageRequestState(false);
        } else {
            String str = TAG;
            Log.d(str, "[presentMessageRequestState] " + messageData.getMessageState());
            this.messageRequestBottomView.setMessageData(messageData);
            this.messageRequestBottomView.setVisibility(0);
            this.noLongerMemberBanner.setVisibility(8);
            this.inputPanel.setHideForMessageRequestState(true);
        }
        invalidateOptionsMenu();
    }

    /* loaded from: classes4.dex */
    public static class KeyboardImageDetails {
        private final boolean hasTransparency;
        private final int height;
        private final int width;

        private KeyboardImageDetails(int i, int i2, boolean z) {
            this.width = i;
            this.height = i2;
            this.hasTransparency = z;
        }
    }
}
