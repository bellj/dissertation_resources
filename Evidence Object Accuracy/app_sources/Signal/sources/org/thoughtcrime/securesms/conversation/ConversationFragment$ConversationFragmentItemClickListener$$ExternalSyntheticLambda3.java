package org.thoughtcrime.securesms.conversation;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda3 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;
    public final /* synthetic */ RecipientId f$1;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda3(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener, RecipientId recipientId) {
        this.f$0 = conversationFragmentItemClickListener;
        this.f$1 = recipientId;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return this.f$0.lambda$onBadDecryptLearnMoreClicked$10(this.f$1);
    }
}
