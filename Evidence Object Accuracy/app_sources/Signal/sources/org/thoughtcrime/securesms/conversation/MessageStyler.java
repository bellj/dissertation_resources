package org.thoughtcrime.securesms.conversation;

import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.util.PlaceholderURLSpan;

/* compiled from: MessageStyler.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageStyler;", "", "()V", "style", "Lorg/thoughtcrime/securesms/conversation/MessageStyler$Result;", "messageRanges", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList;", "span", "Landroid/text/SpannableString;", "Result", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageStyler {
    public static final MessageStyler INSTANCE = new MessageStyler();

    /* compiled from: MessageStyler.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[BodyRangeList.BodyRange.Style.values().length];
            iArr[BodyRangeList.BodyRange.Style.BOLD.ordinal()] = 1;
            iArr[BodyRangeList.BodyRange.Style.ITALIC.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    private MessageStyler() {
    }

    @JvmStatic
    public static final Result style(BodyRangeList bodyRangeList, SpannableString spannableString) {
        Object obj;
        Intrinsics.checkNotNullParameter(bodyRangeList, "messageRanges");
        Intrinsics.checkNotNullParameter(spannableString, "span");
        boolean z = false;
        BodyRangeList.BodyRange.Button button = null;
        for (BodyRangeList.BodyRange bodyRange : bodyRangeList.getRangesList()) {
            if (bodyRange.hasStyle()) {
                BodyRangeList.BodyRange.Style style = bodyRange.getStyle();
                int i = style == null ? -1 : WhenMappings.$EnumSwitchMapping$0[style.ordinal()];
                if (i == 1) {
                    obj = new TypefaceSpan("sans-serif-medium");
                } else if (i != 2) {
                    obj = null;
                } else {
                    obj = new StyleSpan(2);
                }
                if (obj != null) {
                    spannableString.setSpan(obj, bodyRange.getStart(), bodyRange.getStart() + bodyRange.getLength(), 33);
                }
            } else if (bodyRange.hasLink() && bodyRange.getLink() != null) {
                String link = bodyRange.getLink();
                Intrinsics.checkNotNullExpressionValue(link, "range.link");
                spannableString.setSpan(new PlaceholderURLSpan(link), bodyRange.getStart(), bodyRange.getStart() + bodyRange.getLength(), 33);
                z = true;
            } else if (bodyRange.hasButton() && bodyRange.getButton() != null) {
                button = bodyRange.getButton();
            }
        }
        return new Result(z, button);
    }

    /* compiled from: MessageStyler.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u001b\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\f\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\u001f\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00032\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageStyler$Result;", "", "hasStyleLinks", "", "bottomButton", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList$BodyRange$Button;", "(ZLorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList$BodyRange$Button;)V", "getBottomButton", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/BodyRangeList$BodyRange$Button;", "getHasStyleLinks", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Result {
        public static final Companion Companion = new Companion(null);
        private static final Result NO_STYLE = new Result(false, null, 3, null);
        private final BodyRangeList.BodyRange.Button bottomButton;
        private final boolean hasStyleLinks;

        public Result() {
            this(false, null, 3, null);
        }

        public static /* synthetic */ Result copy$default(Result result, boolean z, BodyRangeList.BodyRange.Button button, int i, Object obj) {
            if ((i & 1) != 0) {
                z = result.hasStyleLinks;
            }
            if ((i & 2) != 0) {
                button = result.bottomButton;
            }
            return result.copy(z, button);
        }

        public static final Result getNO_STYLE() {
            return Companion.getNO_STYLE();
        }

        @JvmStatic
        public static final Result none() {
            return Companion.none();
        }

        public final boolean component1() {
            return this.hasStyleLinks;
        }

        public final BodyRangeList.BodyRange.Button component2() {
            return this.bottomButton;
        }

        public final Result copy(boolean z, BodyRangeList.BodyRange.Button button) {
            return new Result(z, button);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Result)) {
                return false;
            }
            Result result = (Result) obj;
            return this.hasStyleLinks == result.hasStyleLinks && Intrinsics.areEqual(this.bottomButton, result.bottomButton);
        }

        public int hashCode() {
            boolean z = this.hasStyleLinks;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = i * 31;
            BodyRangeList.BodyRange.Button button = this.bottomButton;
            return i4 + (button == null ? 0 : button.hashCode());
        }

        public String toString() {
            return "Result(hasStyleLinks=" + this.hasStyleLinks + ", bottomButton=" + this.bottomButton + ')';
        }

        /* compiled from: MessageStyler.kt */
        @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u00020\u0004H\u0007R\u001c\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u000e\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/MessageStyler$Result$Companion;", "", "()V", "NO_STYLE", "Lorg/thoughtcrime/securesms/conversation/MessageStyler$Result;", "getNO_STYLE$annotations", "getNO_STYLE", "()Lorg/thoughtcrime/securesms/conversation/MessageStyler$Result;", "none", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            @JvmStatic
            public static /* synthetic */ void getNO_STYLE$annotations() {
            }

            private Companion() {
            }

            public final Result getNO_STYLE() {
                return Result.NO_STYLE;
            }

            @JvmStatic
            public final Result none() {
                return getNO_STYLE();
            }
        }

        public Result(boolean z, BodyRangeList.BodyRange.Button button) {
            this.hasStyleLinks = z;
            this.bottomButton = button;
        }

        public /* synthetic */ Result(boolean z, BodyRangeList.BodyRange.Button button, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? null : button);
        }

        public final BodyRangeList.BodyRange.Button getBottomButton() {
            return this.bottomButton;
        }

        public final boolean getHasStyleLinks() {
            return this.hasStyleLinks;
        }
    }
}
