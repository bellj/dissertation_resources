package org.thoughtcrime.securesms.conversation.mutiselect;

import android.content.Context;
import android.net.Uri;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.mms.MediaConstraints;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.util.Util;

/* compiled from: Multiselect.kt */
@Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0016\u0010\t\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bJ\u001e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000e\u001a\u00020\u000fH\u0007J&\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aJ\u0018\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u001cH\u0002¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/Multiselect;", "", "()V", "canSendAllAttachmentsToNonPush", "", "context", "Landroid/content/Context;", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "canSendToNonPush", "multiselectPart", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "getMmsParts", "", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "mmsMessageRecord", "Lorg/thoughtcrime/securesms/database/model/MmsMessageRecord;", "getParts", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection;", "isMmsSupported", "mediaUri", "Landroid/net/Uri;", "mediaType", "", "mediaSize", "", "attachment", "Lorg/thoughtcrime/securesms/attachments/Attachment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Multiselect {
    public static final Multiselect INSTANCE = new Multiselect();

    private Multiselect() {
    }

    @JvmStatic
    public static final MultiselectCollection getParts(ConversationMessage conversationMessage) {
        Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
        MessageRecord messageRecord = conversationMessage.getMessageRecord();
        Intrinsics.checkNotNullExpressionValue(messageRecord, "conversationMessage.messageRecord");
        if (messageRecord.isUpdate()) {
            return new MultiselectCollection.Single(new MultiselectPart.Update(conversationMessage));
        }
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        if (messageRecord instanceof MmsMessageRecord) {
            linkedHashSet.addAll(INSTANCE.getMmsParts(conversationMessage, (MmsMessageRecord) messageRecord));
        }
        String body = messageRecord.getBody();
        Intrinsics.checkNotNullExpressionValue(body, "messageRecord.body");
        if (body.length() > 0) {
            linkedHashSet.add(new MultiselectPart.Text(conversationMessage));
        }
        if (linkedHashSet.isEmpty()) {
            return new MultiselectCollection.Single(new MultiselectPart.Message(conversationMessage));
        }
        return MultiselectCollection.Companion.fromSet(linkedHashSet);
    }

    private final Set<MultiselectPart> getMmsParts(ConversationMessage conversationMessage, MmsMessageRecord mmsMessageRecord) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        SlideDeck slideDeck = mmsMessageRecord.getSlideDeck();
        Intrinsics.checkNotNullExpressionValue(slideDeck, "mmsMessageRecord.slideDeck");
        List<Slide> slides = slideDeck.getSlides();
        Intrinsics.checkNotNullExpressionValue(slides, "slideDeck.slides");
        ArrayList arrayList = new ArrayList();
        for (Object obj : slides) {
            if (!(((Slide) obj) instanceof TextSlide)) {
                arrayList.add(obj);
            }
        }
        boolean isEmpty = arrayList.isEmpty();
        boolean z = true;
        if (!isEmpty) {
            linkedHashSet.add(new MultiselectPart.Attachments(conversationMessage));
        }
        String body = slideDeck.getBody();
        Intrinsics.checkNotNullExpressionValue(body, "slideDeck.body");
        if (body.length() <= 0) {
            z = false;
        }
        if (z) {
            linkedHashSet.add(new MultiselectPart.Text(conversationMessage));
        }
        return linkedHashSet;
    }

    public final boolean canSendToNonPush(Context context, MultiselectPart multiselectPart) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(multiselectPart, "multiselectPart");
        if (multiselectPart instanceof MultiselectPart.Attachments) {
            MessageRecord messageRecord = multiselectPart.getConversationMessage().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord, "multiselectPart.conversationMessage.messageRecord");
            return canSendAllAttachmentsToNonPush(context, messageRecord);
        } else if (multiselectPart instanceof MultiselectPart.Message) {
            MessageRecord messageRecord2 = multiselectPart.getConversationMessage().getMessageRecord();
            Intrinsics.checkNotNullExpressionValue(messageRecord2, "multiselectPart.conversationMessage.messageRecord");
            return canSendAllAttachmentsToNonPush(context, messageRecord2);
        } else if (multiselectPart instanceof MultiselectPart.Text) {
            return true;
        } else {
            if (multiselectPart instanceof MultiselectPart.Update) {
                throw new AssertionError("Should never get to here.");
            }
            throw new NoWhenBranchMatchedException();
        }
    }

    public final boolean isMmsSupported(Context context, Uri uri, String str, long j) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(uri, "mediaUri");
        Intrinsics.checkNotNullParameter(str, "mediaType");
        boolean z = ContextCompat.checkSelfPermission(context, "android.permission.READ_PHONE_STATE") == 0;
        if (!Util.isDefaultSmsProvider(context) || !z || !Util.isMmsCapable(context)) {
            return false;
        }
        Integer simSubscriptionId = MessageSendType.Companion.getFirstForTransport(context, true, MessageSendType.TransportType.SMS).getSimSubscriptionId();
        MediaConstraints mmsMediaConstraints = MediaConstraints.getMmsMediaConstraints(simSubscriptionId != null ? simSubscriptionId.intValue() : -1);
        if (mmsMediaConstraints.isSatisfied(context, uri, str, j) || mmsMediaConstraints.canResize(str)) {
            return true;
        }
        return false;
    }

    private final boolean canSendAllAttachmentsToNonPush(Context context, MessageRecord messageRecord) {
        if (!(messageRecord instanceof MmsMessageRecord)) {
            return true;
        }
        List<Attachment> asAttachments = ((MmsMessageRecord) messageRecord).getSlideDeck().asAttachments();
        Intrinsics.checkNotNullExpressionValue(asAttachments, "messageRecord.slideDeck.asAttachments()");
        if ((asAttachments instanceof Collection) && asAttachments.isEmpty()) {
            return true;
        }
        for (Attachment attachment : asAttachments) {
            Multiselect multiselect = INSTANCE;
            Intrinsics.checkNotNullExpressionValue(attachment, "it");
            if (!multiselect.isMmsSupported(context, attachment)) {
                return false;
            }
        }
        return true;
    }

    private final boolean isMmsSupported(Context context, Attachment attachment) {
        boolean z = ContextCompat.checkSelfPermission(context, "android.permission.READ_PHONE_STATE") == 0;
        if (!Util.isDefaultSmsProvider(context) || !z || !Util.isMmsCapable(context)) {
            return false;
        }
        Integer simSubscriptionId = MessageSendType.Companion.getFirstForTransport(context, true, MessageSendType.TransportType.SMS).getSimSubscriptionId();
        MediaConstraints mmsMediaConstraints = MediaConstraints.getMmsMediaConstraints(simSubscriptionId != null ? simSubscriptionId.intValue() : -1);
        if (mmsMediaConstraints.isSatisfied(context, attachment) || mmsMediaConstraints.canResize(attachment)) {
            return true;
        }
        return false;
    }
}
