package org.thoughtcrime.securesms.conversation.ui.error;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SafetyNumberChangeDialog$$ExternalSyntheticLambda3 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return RecipientId.from((String) obj);
    }
}
