package org.thoughtcrime.securesms.conversation;

import androidx.arch.core.util.Function;
import java.util.List;
import org.thoughtcrime.securesms.conversation.ConversationUpdateItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda2 implements Function {
    @Override // androidx.arch.core.util.Function
    public final Object apply(Object obj) {
        return ConversationUpdateItem.GroupDataManager.lambda$observe$2((List) obj);
    }
}
