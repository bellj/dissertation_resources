package org.thoughtcrime.securesms.conversation.mutiselect;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.view.ViewParent;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.conversation.ConversationAdapter;
import org.thoughtcrime.securesms.conversation.mutiselect.ConversationItemAnimator;

/* compiled from: ConversationItemAnimator.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u0000 &2\u00020\u0001:\u0003%&'B/\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0007J\"\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0016J(\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\n2\u0006\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0016J\"\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00112\b\u0010\u0012\u001a\u0004\u0018\u00010\u0011H\u0016J \u0010\u0017\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0016J\"\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\n2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\nH\u0016J\b\u0010\u001d\u001a\u00020\u001aH\u0016J\u0010\u0010\u001e\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\nH\u0002J\b\u0010\u001f\u001a\u00020\u001aH\u0002J\u0010\u0010 \u001a\u00020\u001a2\u0006\u0010\u000f\u001a\u00020\nH\u0002J\b\u0010!\u001a\u00020\u0004H\u0016J\u0010\u0010\"\u001a\u00020\u001a2\u0006\u0010\u000f\u001a\u00020\nH\u0016J\b\u0010#\u001a\u00020\u001aH\u0016J\b\u0010$\u001a\u00020\u001aH\u0002R\u0014\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\r0\tX\u0004¢\u0006\u0002\n\u0000¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator;", "Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;", "isInMultiSelectMode", "Lkotlin/Function0;", "", "shouldPlayMessageAnimations", "isParentFilled", "(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "pendingSlideAnimations", "", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$TweeningInfo;", "slideAnimations", "Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$AnimationInfo;", "animateAppearance", "viewHolder", "preLayoutInfo", "Landroidx/recyclerview/widget/RecyclerView$ItemAnimator$ItemHolderInfo;", "postLayoutInfo", "animateChange", "oldHolder", "newHolder", "animateDisappearance", "animatePersistence", "animateSlide", "dispatchFinishedWhenDone", "", "endAnimation", "item", "endAnimations", "endSlideAnimation", "endSlideAnimations", "handleAnimationEnd", "isRunning", "onAnimationFinished", "runPendingAnimations", "runPendingSlideAnimations", "AnimationInfo", "Companion", "TweeningInfo", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationItemAnimator extends RecyclerView.ItemAnimator {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ConversationItemAnimator.class);
    private final Function0<Boolean> isInMultiSelectMode;
    private final Function0<Boolean> isParentFilled;
    private final Map<RecyclerView.ViewHolder, TweeningInfo> pendingSlideAnimations = new LinkedHashMap();
    private final Function0<Boolean> shouldPlayMessageAnimations;
    private final Map<RecyclerView.ViewHolder, AnimationInfo> slideAnimations = new LinkedHashMap();

    public ConversationItemAnimator(Function0<Boolean> function0, Function0<Boolean> function02, Function0<Boolean> function03) {
        Intrinsics.checkNotNullParameter(function0, "isInMultiSelectMode");
        Intrinsics.checkNotNullParameter(function02, "shouldPlayMessageAnimations");
        Intrinsics.checkNotNullParameter(function03, "isParentFilled");
        this.isInMultiSelectMode = function0;
        this.shouldPlayMessageAnimations = function02;
        this.isParentFilled = function03;
    }

    /* compiled from: ConversationItemAnimator.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0007\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\u000e\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$TweeningInfo;", "", "startValue", "", "endValue", "(FF)V", "getEndValue", "()F", "getStartValue", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "lerp", "progress", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class TweeningInfo {
        private final float endValue;
        private final float startValue;

        public static /* synthetic */ TweeningInfo copy$default(TweeningInfo tweeningInfo, float f, float f2, int i, Object obj) {
            if ((i & 1) != 0) {
                f = tweeningInfo.startValue;
            }
            if ((i & 2) != 0) {
                f2 = tweeningInfo.endValue;
            }
            return tweeningInfo.copy(f, f2);
        }

        public final float component1() {
            return this.startValue;
        }

        public final float component2() {
            return this.endValue;
        }

        public final TweeningInfo copy(float f, float f2) {
            return new TweeningInfo(f, f2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof TweeningInfo)) {
                return false;
            }
            TweeningInfo tweeningInfo = (TweeningInfo) obj;
            return Intrinsics.areEqual(Float.valueOf(this.startValue), Float.valueOf(tweeningInfo.startValue)) && Intrinsics.areEqual(Float.valueOf(this.endValue), Float.valueOf(tweeningInfo.endValue));
        }

        public int hashCode() {
            return (Float.floatToIntBits(this.startValue) * 31) + Float.floatToIntBits(this.endValue);
        }

        public String toString() {
            return "TweeningInfo(startValue=" + this.startValue + ", endValue=" + this.endValue + ')';
        }

        public TweeningInfo(float f, float f2) {
            this.startValue = f;
            this.endValue = f2;
        }

        public final float getStartValue() {
            return this.startValue;
        }

        public final float getEndValue() {
            return this.endValue;
        }

        public final float lerp(float f) {
            float f2 = this.startValue;
            return f2 + (f * (this.endValue - f2));
        }
    }

    /* compiled from: ConversationItemAnimator.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$AnimationInfo;", "", "sharedAnimator", "Landroid/animation/ValueAnimator;", "tweeningInfo", "Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$TweeningInfo;", "(Landroid/animation/ValueAnimator;Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$TweeningInfo;)V", "getSharedAnimator", "()Landroid/animation/ValueAnimator;", "getTweeningInfo", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$TweeningInfo;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class AnimationInfo {
        private final ValueAnimator sharedAnimator;
        private final TweeningInfo tweeningInfo;

        public static /* synthetic */ AnimationInfo copy$default(AnimationInfo animationInfo, ValueAnimator valueAnimator, TweeningInfo tweeningInfo, int i, Object obj) {
            if ((i & 1) != 0) {
                valueAnimator = animationInfo.sharedAnimator;
            }
            if ((i & 2) != 0) {
                tweeningInfo = animationInfo.tweeningInfo;
            }
            return animationInfo.copy(valueAnimator, tweeningInfo);
        }

        public final ValueAnimator component1() {
            return this.sharedAnimator;
        }

        public final TweeningInfo component2() {
            return this.tweeningInfo;
        }

        public final AnimationInfo copy(ValueAnimator valueAnimator, TweeningInfo tweeningInfo) {
            Intrinsics.checkNotNullParameter(valueAnimator, "sharedAnimator");
            Intrinsics.checkNotNullParameter(tweeningInfo, "tweeningInfo");
            return new AnimationInfo(valueAnimator, tweeningInfo);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof AnimationInfo)) {
                return false;
            }
            AnimationInfo animationInfo = (AnimationInfo) obj;
            return Intrinsics.areEqual(this.sharedAnimator, animationInfo.sharedAnimator) && Intrinsics.areEqual(this.tweeningInfo, animationInfo.tweeningInfo);
        }

        public int hashCode() {
            return (this.sharedAnimator.hashCode() * 31) + this.tweeningInfo.hashCode();
        }

        public String toString() {
            return "AnimationInfo(sharedAnimator=" + this.sharedAnimator + ", tweeningInfo=" + this.tweeningInfo + ')';
        }

        public AnimationInfo(ValueAnimator valueAnimator, TweeningInfo tweeningInfo) {
            Intrinsics.checkNotNullParameter(valueAnimator, "sharedAnimator");
            Intrinsics.checkNotNullParameter(tweeningInfo, "tweeningInfo");
            this.sharedAnimator = valueAnimator;
            this.tweeningInfo = tweeningInfo;
        }

        public final ValueAnimator getSharedAnimator() {
            return this.sharedAnimator;
        }

        public final TweeningInfo getTweeningInfo() {
            return this.tweeningInfo;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public boolean animateDisappearance(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        Intrinsics.checkNotNullParameter(viewHolder, "viewHolder");
        Intrinsics.checkNotNullParameter(itemHolderInfo, "preLayoutInfo");
        if (!(viewHolder instanceof ConversationAdapter.HeaderViewHolder) || this.pendingSlideAnimations.containsKey(viewHolder) || this.slideAnimations.containsKey(viewHolder) || !this.shouldPlayMessageAnimations.invoke().booleanValue() || !this.isParentFilled.invoke().booleanValue()) {
            dispatchAnimationFinished(viewHolder);
            return false;
        }
        this.pendingSlideAnimations.put(viewHolder, new TweeningInfo(0.0f, (float) viewHolder.itemView.getHeight()));
        dispatchAnimationStarted(viewHolder);
        return true;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public boolean animateAppearance(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        Intrinsics.checkNotNullParameter(viewHolder, "viewHolder");
        Intrinsics.checkNotNullParameter(itemHolderInfo2, "postLayoutInfo");
        if (viewHolder.getAbsoluteAdapterPosition() <= 1) {
            return animateSlide(viewHolder, itemHolderInfo, itemHolderInfo2);
        }
        dispatchAnimationFinished(viewHolder);
        return false;
    }

    private final boolean animateSlide(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        int i;
        int i2;
        if (this.isInMultiSelectMode.invoke().booleanValue() || !this.shouldPlayMessageAnimations.invoke().booleanValue()) {
            String str = TAG;
            Log.d(str, "Dropping slide animation: (" + this.isInMultiSelectMode.invoke().booleanValue() + ", " + this.shouldPlayMessageAnimations.invoke().booleanValue() + ") :: " + viewHolder.getAbsoluteAdapterPosition());
            dispatchAnimationFinished(viewHolder);
            return false;
        } else if (this.slideAnimations.containsKey(viewHolder)) {
            dispatchAnimationFinished(viewHolder);
            return false;
        } else {
            if (itemHolderInfo == null) {
                i2 = itemHolderInfo2.bottom;
                i = itemHolderInfo2.top;
            } else {
                i2 = itemHolderInfo.top;
                i = itemHolderInfo2.top;
            }
            float f = (float) (i2 - i);
            if (f == 0.0f) {
                viewHolder.itemView.setTranslationY(0.0f);
                dispatchAnimationFinished(viewHolder);
                return false;
            }
            viewHolder.itemView.setTranslationY(f);
            this.pendingSlideAnimations.put(viewHolder, new TweeningInfo(f, 0.0f));
            dispatchAnimationStarted(viewHolder);
            return true;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public boolean animatePersistence(RecyclerView.ViewHolder viewHolder, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        Intrinsics.checkNotNullParameter(viewHolder, "viewHolder");
        Intrinsics.checkNotNullParameter(itemHolderInfo, "preLayoutInfo");
        Intrinsics.checkNotNullParameter(itemHolderInfo2, "postLayoutInfo");
        if (this.isInMultiSelectMode.invoke().booleanValue() || !this.shouldPlayMessageAnimations.invoke().booleanValue() || !this.isParentFilled.invoke().booleanValue()) {
            String str = TAG;
            Log.d(str, "Dropping persistence animation: (" + this.isInMultiSelectMode.invoke().booleanValue() + ", " + this.shouldPlayMessageAnimations.invoke().booleanValue() + ", " + this.isParentFilled.invoke().booleanValue() + ") :: " + viewHolder.getAbsoluteAdapterPosition());
            dispatchAnimationFinished(viewHolder);
            return false;
        } else if (!this.pendingSlideAnimations.containsKey(viewHolder) && !this.slideAnimations.containsKey(viewHolder)) {
            return animateSlide(viewHolder, itemHolderInfo, itemHolderInfo2);
        } else {
            dispatchAnimationFinished(viewHolder);
            return false;
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo, RecyclerView.ItemAnimator.ItemHolderInfo itemHolderInfo2) {
        Intrinsics.checkNotNullParameter(viewHolder, "oldHolder");
        Intrinsics.checkNotNullParameter(viewHolder2, "newHolder");
        Intrinsics.checkNotNullParameter(itemHolderInfo, "preLayoutInfo");
        Intrinsics.checkNotNullParameter(itemHolderInfo2, "postLayoutInfo");
        if (!Intrinsics.areEqual(viewHolder, viewHolder2)) {
            dispatchAnimationFinished(viewHolder);
        }
        return animatePersistence(viewHolder2, itemHolderInfo, itemHolderInfo2);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public void runPendingAnimations() {
        String str = TAG;
        Log.d(str, "Starting " + this.pendingSlideAnimations.size() + " animations.");
        runPendingSlideAnimations();
    }

    private final void runPendingSlideAnimations() {
        ArrayList arrayList = new ArrayList();
        for (Map.Entry<RecyclerView.ViewHolder, TweeningInfo> entry : this.pendingSlideAnimations.entrySet()) {
            RecyclerView.ViewHolder key = entry.getKey();
            TweeningInfo value = entry.getValue();
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            Map<RecyclerView.ViewHolder, AnimationInfo> map = this.slideAnimations;
            Intrinsics.checkNotNullExpressionValue(ofFloat, "animator");
            map.put(key, new AnimationInfo(ofFloat, value));
            ofFloat.setDuration(150L);
            ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(key, value) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.ConversationItemAnimator$$ExternalSyntheticLambda0
                public final /* synthetic */ RecyclerView.ViewHolder f$1;
                public final /* synthetic */ ConversationItemAnimator.TweeningInfo f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.animation.ValueAnimator.AnimatorUpdateListener
                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    ConversationItemAnimator.m1561runPendingSlideAnimations$lambda0(ConversationItemAnimator.this, this.f$1, this.f$2, valueAnimator);
                }
            });
            ofFloat.addListener(new Animator.AnimatorListener(this, key) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.ConversationItemAnimator$runPendingSlideAnimations$$inlined$doOnEnd$1
                final /* synthetic */ RecyclerView.ViewHolder $viewHolder$inlined;
                final /* synthetic */ ConversationItemAnimator this$0;

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationCancel(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationRepeat(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationStart(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                }

                {
                    this.this$0 = r1;
                    this.$viewHolder$inlined = r2;
                }

                @Override // android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    Intrinsics.checkNotNullParameter(animator, "animator");
                    if (this.this$0.slideAnimations.containsKey(this.$viewHolder$inlined)) {
                        this.this$0.handleAnimationEnd(this.$viewHolder$inlined);
                    }
                }
            });
            arrayList.add(ofFloat);
        }
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playTogether(arrayList);
        animatorSet.start();
        this.pendingSlideAnimations.clear();
    }

    /* renamed from: runPendingSlideAnimations$lambda-0 */
    public static final void m1561runPendingSlideAnimations$lambda0(ConversationItemAnimator conversationItemAnimator, RecyclerView.ViewHolder viewHolder, TweeningInfo tweeningInfo, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(conversationItemAnimator, "this$0");
        Intrinsics.checkNotNullParameter(viewHolder, "$viewHolder");
        Intrinsics.checkNotNullParameter(tweeningInfo, "$tweeningInfo");
        if (conversationItemAnimator.slideAnimations.containsKey(viewHolder)) {
            viewHolder.itemView.setTranslationY(tweeningInfo.lerp(valueAnimator.getAnimatedFraction()));
            RecyclerView recyclerView = (RecyclerView) viewHolder.itemView.getParent();
            if (recyclerView != null) {
                recyclerView.invalidate();
            }
        }
    }

    public final void handleAnimationEnd(RecyclerView.ViewHolder viewHolder) {
        viewHolder.itemView.setTranslationY(0.0f);
        this.slideAnimations.remove(viewHolder);
        dispatchAnimationFinished(viewHolder);
        dispatchFinishedWhenDone();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public void endAnimation(RecyclerView.ViewHolder viewHolder) {
        Intrinsics.checkNotNullParameter(viewHolder, "item");
        endSlideAnimation(viewHolder);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public void endAnimations() {
        endSlideAnimations();
        dispatchAnimationsFinished();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public boolean isRunning() {
        Collection<AnimationInfo> values = this.slideAnimations.values();
        if ((values instanceof Collection) && values.isEmpty()) {
            return false;
        }
        for (AnimationInfo animationInfo : values) {
            if (animationInfo.getSharedAnimator().isRunning()) {
                return true;
            }
        }
        return false;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemAnimator
    public void onAnimationFinished(RecyclerView.ViewHolder viewHolder) {
        Intrinsics.checkNotNullParameter(viewHolder, "viewHolder");
        ViewParent parent = viewHolder.itemView.getParent();
        RecyclerView recyclerView = parent instanceof RecyclerView ? (RecyclerView) parent : null;
        if (recyclerView != null) {
            recyclerView.post(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.ConversationItemAnimator$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationItemAnimator.m1560onAnimationFinished$lambda4(RecyclerView.this);
                }
            });
        }
    }

    /* renamed from: onAnimationFinished$lambda-4 */
    public static final void m1560onAnimationFinished$lambda4(RecyclerView recyclerView) {
        recyclerView.invalidate();
    }

    private final void endSlideAnimation(RecyclerView.ViewHolder viewHolder) {
        ValueAnimator sharedAnimator;
        AnimationInfo animationInfo = this.slideAnimations.get(viewHolder);
        if (animationInfo != null && (sharedAnimator = animationInfo.getSharedAnimator()) != null) {
            sharedAnimator.cancel();
        }
    }

    private final void endSlideAnimations() {
        Collection<AnimationInfo> values = this.slideAnimations.values();
        ArrayList<ValueAnimator> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(values, 10));
        for (AnimationInfo animationInfo : values) {
            arrayList.add(animationInfo.getSharedAnimator());
        }
        for (ValueAnimator valueAnimator : arrayList) {
            valueAnimator.cancel();
        }
    }

    private final void dispatchFinishedWhenDone() {
        if (!isRunning()) {
            Log.d(TAG, "Finished running animations.");
            dispatchAnimationsFinished();
        }
    }

    /* compiled from: ConversationItemAnimator.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/ConversationItemAnimator$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
