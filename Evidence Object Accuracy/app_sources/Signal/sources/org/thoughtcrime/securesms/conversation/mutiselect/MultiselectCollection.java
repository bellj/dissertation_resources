package org.thoughtcrime.securesms.conversation.mutiselect;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;

/* compiled from: MultiselectCollection.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\"\n\u0002\b\u0007\b6\u0018\u0000 \u00172\u00020\u0001:\u0003\u0017\u0018\u0019B\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0006\u0010\u0010\u001a\u00020\fJ\u0014\u0010\u0011\u001a\u00020\f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0013J\b\u0010\u0014\u001a\u00020\fH\u0016J\u0014\u0010\u0015\u001a\u00020\f2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0013J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0013H&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0001\u0002\n\b¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection;", "", "()V", MediaPreviewActivity.SIZE_EXTRA, "", "getSize", "()I", "asDouble", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection$Double;", "asSingle", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection$Single;", "couldContainMedia", "", "multiselectPart", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "couldContainText", "isExpired", "isMediaSelected", "selectedParts", "", "isSingle", "isTextSelected", "toSet", "Companion", "Double", "Single", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class MultiselectCollection {
    public static final Companion Companion = new Companion(null);

    public /* synthetic */ MultiselectCollection(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public abstract int getSize();

    public boolean isSingle() {
        return false;
    }

    public abstract Set<MultiselectPart> toSet();

    private MultiselectCollection() {
    }

    /* compiled from: MultiselectCollection.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0010\"\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u00020\u0000H\u0016J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011HÖ\u0003J\t\u0010\u0012\u001a\u00020\bHÖ\u0001J\b\u0010\u0013\u001a\u00020\u000fH\u0016J\u000e\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00030\u0015H\u0016J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bXD¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection$Single;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection;", "singlePart", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "(Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;)V", "getSinglePart", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", MediaPreviewActivity.SIZE_EXTRA, "", "getSize", "()I", "asSingle", "component1", "copy", "equals", "", "other", "", "hashCode", "isSingle", "toSet", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Single extends MultiselectCollection {
        private final MultiselectPart singlePart;
        private final int size = 1;

        public static /* synthetic */ Single copy$default(Single single, MultiselectPart multiselectPart, int i, Object obj) {
            if ((i & 1) != 0) {
                multiselectPart = single.singlePart;
            }
            return single.copy(multiselectPart);
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public Single asSingle() {
            return this;
        }

        public final MultiselectPart component1() {
            return this.singlePart;
        }

        public final Single copy(MultiselectPart multiselectPart) {
            Intrinsics.checkNotNullParameter(multiselectPart, "singlePart");
            return new Single(multiselectPart);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Single) && Intrinsics.areEqual(this.singlePart, ((Single) obj).singlePart);
        }

        public int hashCode() {
            return this.singlePart.hashCode();
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public boolean isSingle() {
            return true;
        }

        public String toString() {
            return "Single(singlePart=" + this.singlePart + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Single(MultiselectPart multiselectPart) {
            super(null);
            Intrinsics.checkNotNullParameter(multiselectPart, "singlePart");
            this.singlePart = multiselectPart;
        }

        public final MultiselectPart getSinglePart() {
            return this.singlePart;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public int getSize() {
            return this.size;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public Set<MultiselectPart> toSet() {
            return SetsKt__SetsJVMKt.setOf(this.singlePart);
        }
    }

    /* compiled from: MultiselectCollection.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\b\u0010\r\u001a\u00020\u0000H\u0016J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\tHÖ\u0001J\u000e\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00030\u0017H\u0016J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\u00020\tXD¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0007¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection$Double;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection;", "topPart", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "bottomPart", "(Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;)V", "getBottomPart", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", MediaPreviewActivity.SIZE_EXTRA, "", "getSize", "()I", "getTopPart", "asDouble", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "toSet", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Double extends MultiselectCollection {
        private final MultiselectPart bottomPart;
        private final int size = 2;
        private final MultiselectPart topPart;

        public static /* synthetic */ Double copy$default(Double r0, MultiselectPart multiselectPart, MultiselectPart multiselectPart2, int i, Object obj) {
            if ((i & 1) != 0) {
                multiselectPart = r0.topPart;
            }
            if ((i & 2) != 0) {
                multiselectPart2 = r0.bottomPart;
            }
            return r0.copy(multiselectPart, multiselectPart2);
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public Double asDouble() {
            return this;
        }

        public final MultiselectPart component1() {
            return this.topPart;
        }

        public final MultiselectPart component2() {
            return this.bottomPart;
        }

        public final Double copy(MultiselectPart multiselectPart, MultiselectPart multiselectPart2) {
            Intrinsics.checkNotNullParameter(multiselectPart, "topPart");
            Intrinsics.checkNotNullParameter(multiselectPart2, "bottomPart");
            return new Double(multiselectPart, multiselectPart2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Double)) {
                return false;
            }
            Double r5 = (Double) obj;
            return Intrinsics.areEqual(this.topPart, r5.topPart) && Intrinsics.areEqual(this.bottomPart, r5.bottomPart);
        }

        public int hashCode() {
            return (this.topPart.hashCode() * 31) + this.bottomPart.hashCode();
        }

        public String toString() {
            return "Double(topPart=" + this.topPart + ", bottomPart=" + this.bottomPart + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Double(MultiselectPart multiselectPart, MultiselectPart multiselectPart2) {
            super(null);
            Intrinsics.checkNotNullParameter(multiselectPart, "topPart");
            Intrinsics.checkNotNullParameter(multiselectPart2, "bottomPart");
            this.topPart = multiselectPart;
            this.bottomPart = multiselectPart2;
        }

        public final MultiselectPart getBottomPart() {
            return this.bottomPart;
        }

        public final MultiselectPart getTopPart() {
            return this.topPart;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public int getSize() {
            return this.size;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection
        public Set<MultiselectPart> toSet() {
            return SetsKt__SetsKt.linkedSetOf(this.topPart, this.bottomPart);
        }
    }

    /* compiled from: MultiselectCollection.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection$Companion;", "", "()V", "fromSet", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectCollection;", "partsSet", "", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final MultiselectCollection fromSet(Set<? extends MultiselectPart> set) {
            Intrinsics.checkNotNullParameter(set, "partsSet");
            int size = set.size();
            if (size == 1) {
                return new Single((MultiselectPart) CollectionsKt___CollectionsKt.first(set));
            }
            if (size == 2) {
                Iterator<? extends MultiselectPart> it = set.iterator();
                return new Double((MultiselectPart) it.next(), (MultiselectPart) it.next());
            }
            throw new IllegalArgumentException("Unsupported set size: " + set.size());
        }
    }

    public final boolean isExpired() {
        Set<MultiselectPart> set = toSet();
        if ((set instanceof Collection) && set.isEmpty()) {
            return false;
        }
        for (MultiselectPart multiselectPart : set) {
            if (multiselectPart.isExpired()) {
                return true;
            }
        }
        return false;
    }

    public final boolean isTextSelected(Set<? extends MultiselectPart> set) {
        Intrinsics.checkNotNullParameter(set, "selectedParts");
        Set<MultiselectPart> set2 = toSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : set2) {
            if (couldContainText((MultiselectPart) obj)) {
                arrayList.add(obj);
            }
        }
        Set<MultiselectPart> set3 = CollectionsKt___CollectionsKt.toSet(arrayList);
        if ((set3 instanceof Collection) && set3.isEmpty()) {
            return false;
        }
        for (MultiselectPart multiselectPart : set3) {
            if (set.contains(multiselectPart)) {
                return true;
            }
        }
        return false;
    }

    public final boolean isMediaSelected(Set<? extends MultiselectPart> set) {
        Intrinsics.checkNotNullParameter(set, "selectedParts");
        Set<MultiselectPart> set2 = toSet();
        ArrayList arrayList = new ArrayList();
        for (Object obj : set2) {
            if (couldContainMedia((MultiselectPart) obj)) {
                arrayList.add(obj);
            }
        }
        Set<MultiselectPart> set3 = CollectionsKt___CollectionsKt.toSet(arrayList);
        if ((set3 instanceof Collection) && set3.isEmpty()) {
            return false;
        }
        for (MultiselectPart multiselectPart : set3) {
            if (set.contains(multiselectPart)) {
                return true;
            }
        }
        return false;
    }

    private final boolean couldContainText(MultiselectPart multiselectPart) {
        return (multiselectPart instanceof MultiselectPart.Text) || (multiselectPart instanceof MultiselectPart.Message);
    }

    private final boolean couldContainMedia(MultiselectPart multiselectPart) {
        return (multiselectPart instanceof MultiselectPart.Attachments) || (multiselectPart instanceof MultiselectPart.Message);
    }

    public Single asSingle() {
        throw new UnsupportedOperationException();
    }

    public Double asDouble() {
        throw new UnsupportedOperationException();
    }
}
