package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.fragment.app.FragmentKt;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MultiselectForwardBottomSheet.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001\"B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\n\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016J&\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\r2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0010\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u000bH\u0016J\b\u0010\u001d\u001a\u00020\u000bH\u0016J\u001a\u0010\u001e\u001a\u00020\u000b2\u0006\u0010\u001f\u001a\u00020\u00132\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0010\u0010 \u001a\u00020\u000b2\u0006\u0010!\u001a\u00020\u0018H\u0016R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\u00020\u0007XD¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardBottomSheet;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "()V", "callback", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardBottomSheet$Callback;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "exitFlow", "", "getContainer", "Landroid/view/ViewGroup;", "getDialogBackgroundColor", "", "getStorySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onFinishForwardAction", "onSearchInputFocused", "onViewCreated", "view", "setResult", "bundle", "Callback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardBottomSheet extends FixedRoundedCornerBottomSheetDialogFragment implements MultiselectForwardFragment.Callback {
    private Callback callback;
    private final float peekHeightPercentage = 0.67f;

    /* compiled from: MultiselectForwardBottomSheet.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\n\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u0016J\b\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0005H&¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardBottomSheet$Callback;", "", "getStorySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "onDismissForwardSheet", "", "onFinishForwardAction", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {

        /* compiled from: MultiselectForwardBottomSheet.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DefaultImpls {
            public static Stories.MediaTransform.SendRequirements getStorySendRequirements(Callback callback) {
                return null;
            }
        }

        Stories.MediaTransform.SendRequirements getStorySendRequirements();

        void onDismissForwardSheet();

        void onFinishForwardAction();
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001c */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet$Callback] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v9 */
    /* JADX WARN: Type inference failed for: r0v10 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public org.thoughtcrime.securesms.stories.Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        /*
            r3 = this;
            androidx.fragment.app.Fragment r0 = r3.getParentFragment()
        L_0x0004:
            r1 = 0
            if (r0 == 0) goto L_0x0011
            boolean r2 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
            if (r2 == 0) goto L_0x000c
            goto L_0x001c
        L_0x000c:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0011:
            androidx.fragment.app.FragmentActivity r0 = r3.requireActivity()
            boolean r2 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
            if (r2 != 0) goto L_0x001a
            r0 = r1
        L_0x001a:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet$Callback r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback) r0
        L_0x001c:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet$Callback r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback) r0
            if (r0 == 0) goto L_0x0024
            org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements r1 = r0.getStorySendRequirements()
        L_0x0024:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.getStorySendRequirements():org.thoughtcrime.securesms.stories.Stories$MediaTransform$SendRequirements");
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0020 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v2, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r2v5, types: [org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet$Callback] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v12 */
    /* JADX WARN: Type inference failed for: r2v13 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.Fragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewCreated(android.view.View r2, android.os.Bundle r3) {
        /*
            r1 = this;
            java.lang.String r0 = "view"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            androidx.fragment.app.Fragment r2 = r1.getParentFragment()
        L_0x0009:
            if (r2 == 0) goto L_0x0015
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
            if (r0 == 0) goto L_0x0010
            goto L_0x0020
        L_0x0010:
            androidx.fragment.app.Fragment r2 = r2.getParentFragment()
            goto L_0x0009
        L_0x0015:
            androidx.fragment.app.FragmentActivity r2 = r1.requireActivity()
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
            if (r0 != 0) goto L_0x001e
            r2 = 0
        L_0x001e:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet$Callback r2 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback) r2
        L_0x0020:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet$Callback r2 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback) r2
            r1.callback = r2
            if (r3 != 0) goto L_0x0044
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment r2 = new org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment
            r2.<init>()
            android.os.Bundle r3 = r1.requireArguments()
            r2.setArguments(r3)
            androidx.fragment.app.FragmentManager r3 = r1.getChildFragmentManager()
            androidx.fragment.app.FragmentTransaction r3 = r3.beginTransaction()
            r0 = 2131363785(0x7f0a07c9, float:1.8347389E38)
            androidx.fragment.app.FragmentTransaction r2 = r3.replace(r0, r2)
            r2.commitAllowingStateLoss()
        L_0x0044:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.onViewCreated(android.view.View, android.os.Bundle):void");
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.multiselect_bottom_sheet, viewGroup, false);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public ViewGroup getContainer() {
        ViewParent parent = requireView().getParent().getParent().getParent();
        if (parent != null) {
            return (ViewGroup) parent;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public int getDialogBackgroundColor() {
        return getBackgroundColor();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void setResult(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        FragmentKt.setFragmentResult(this, MultiselectForwardFragment.RESULT_KEY, bundle);
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        super.onDismiss(dialogInterface);
        Callback callback = this.callback;
        if (callback != null) {
            callback.onDismissForwardSheet();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onFinishForwardAction() {
        Callback callback = this.callback;
        if (callback != null) {
            callback.onFinishForwardAction();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void exitFlow() {
        dismissAllowingStateLoss();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onSearchInputFocused() {
        ((BottomSheetDialog) requireDialog()).getBehavior().setState(3);
    }
}
