package org.thoughtcrime.securesms.conversation.colors.ui;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: ChatColorMappingModel.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\u00052\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\n¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorMappingModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "isSelected", "", "isAuto", "(Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;ZZ)V", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "()Z", "isCustom", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorMappingModel implements MappingModel<ChatColorMappingModel> {
    private final ChatColors chatColors;
    private final boolean isAuto;
    private final boolean isCustom;
    private final boolean isSelected;

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(ChatColorMappingModel chatColorMappingModel) {
        return MappingModel.CC.$default$getChangePayload(this, chatColorMappingModel);
    }

    public ChatColorMappingModel(ChatColors chatColors, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.chatColors = chatColors;
        this.isSelected = z;
        this.isAuto = z2;
        this.isCustom = chatColors.getId() instanceof ChatColors.Id.Custom;
    }

    public final ChatColors getChatColors() {
        return this.chatColors;
    }

    public final boolean isSelected() {
        return this.isSelected;
    }

    public final boolean isAuto() {
        return this.isAuto;
    }

    public final boolean isCustom() {
        return this.isCustom;
    }

    public boolean areItemsTheSame(ChatColorMappingModel chatColorMappingModel) {
        Intrinsics.checkNotNullParameter(chatColorMappingModel, "newItem");
        return Intrinsics.areEqual(this.chatColors, chatColorMappingModel.chatColors) && this.isAuto == chatColorMappingModel.isAuto;
    }

    public boolean areContentsTheSame(ChatColorMappingModel chatColorMappingModel) {
        Intrinsics.checkNotNullParameter(chatColorMappingModel, "newItem");
        return areItemsTheSame(chatColorMappingModel) && this.isSelected == chatColorMappingModel.isSelected;
    }
}
