package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import android.content.Context;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: CustomChatColorCreatorRepository.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\"\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00060\nJ&\u0010\f\u001a\u00020\u00062\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0014\u0010\t\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u000f\u0012\u0004\u0012\u00020\u00060\nJ\"\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00060\nJ\"\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0013\u001a\u00020\u00112\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\u00060\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getUsageCount", "", "chatColorsId", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "consumer", "Lkotlin/Function1;", "", "getWallpaper", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "loadColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "setChatColors", "chatColors", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorCreatorRepository {
    private final Context context;

    public CustomChatColorCreatorRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final void loadColors(ChatColors.Id id, Function1<? super ChatColors, Unit> function1) {
        Intrinsics.checkNotNullParameter(id, "chatColorsId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomChatColorCreatorRepository.m1543loadColors$lambda0(ChatColors.Id.this, this.f$1);
            }
        });
    }

    /* renamed from: loadColors$lambda-0 */
    public static final void m1543loadColors$lambda0(ChatColors.Id id, Function1 function1) {
        Intrinsics.checkNotNullParameter(id, "$chatColorsId");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        function1.invoke(SignalDatabase.Companion.chatColors().getById(id));
    }

    public final void getWallpaper(RecipientId recipientId, Function1<? super ChatWallpaper, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomChatColorCreatorRepository.m1542getWallpaper$lambda1(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: getWallpaper$lambda-1 */
    public static final void m1542getWallpaper$lambda1(RecipientId recipientId, Function1 function1) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        if (recipientId != null) {
            Recipient resolved = Recipient.resolved(recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
            function1.invoke(resolved.getWallpaper());
            return;
        }
        function1.invoke(SignalStore.wallpaper().getWallpaper());
    }

    public final void setChatColors(ChatColors chatColors, Function1<? super ChatColors, Unit> function1) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomChatColorCreatorRepository.m1544setChatColors$lambda2(ChatColors.this, this.f$1);
            }
        });
    }

    /* renamed from: setChatColors$lambda-2 */
    public static final void m1544setChatColors$lambda2(ChatColors chatColors, Function1 function1) {
        Intrinsics.checkNotNullParameter(chatColors, "$chatColors");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        function1.invoke(SignalDatabase.Companion.chatColors().saveChatColors(chatColors));
    }

    public final void getUsageCount(ChatColors.Id id, Function1<? super Integer, Unit> function1) {
        Intrinsics.checkNotNullParameter(id, "chatColorsId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(id) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ ChatColors.Id f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomChatColorCreatorRepository.m1541getUsageCount$lambda3(Function1.this, this.f$1);
            }
        });
    }

    /* renamed from: getUsageCount$lambda-3 */
    public static final void m1541getUsageCount$lambda3(Function1 function1, ChatColors.Id id) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(id, "$chatColorsId");
        function1.invoke(Integer.valueOf(SignalDatabase.Companion.recipients().getColorUsageCount(id)));
    }
}
