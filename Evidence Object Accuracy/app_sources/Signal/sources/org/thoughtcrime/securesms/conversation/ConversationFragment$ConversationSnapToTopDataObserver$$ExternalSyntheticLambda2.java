package org.thoughtcrime.securesms.conversation;

import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.util.SnapToTopDataObserver;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationSnapToTopDataObserver$$ExternalSyntheticLambda2 implements SnapToTopDataObserver.ScrollToTop {
    public final /* synthetic */ ConversationFragment f$0;

    public /* synthetic */ ConversationFragment$ConversationSnapToTopDataObserver$$ExternalSyntheticLambda2(ConversationFragment conversationFragment) {
        this.f$0 = conversationFragment;
    }

    @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.ScrollToTop
    public final void scrollToTop() {
        ConversationFragment.ConversationSnapToTopDataObserver.lambda$new$1(this.f$0);
    }
}
