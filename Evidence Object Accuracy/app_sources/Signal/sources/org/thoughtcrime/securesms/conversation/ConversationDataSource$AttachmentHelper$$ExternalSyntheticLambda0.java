package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import j$.util.function.Function;
import org.thoughtcrime.securesms.conversation.ConversationDataSource;
import org.thoughtcrime.securesms.database.model.MessageRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationDataSource$AttachmentHelper$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ ConversationDataSource.AttachmentHelper f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ ConversationDataSource$AttachmentHelper$$ExternalSyntheticLambda0(ConversationDataSource.AttachmentHelper attachmentHelper, Context context) {
        this.f$0 = attachmentHelper;
        this.f$1 = context;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return this.f$0.lambda$buildUpdatedModels$0(this.f$1, (MessageRecord) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
