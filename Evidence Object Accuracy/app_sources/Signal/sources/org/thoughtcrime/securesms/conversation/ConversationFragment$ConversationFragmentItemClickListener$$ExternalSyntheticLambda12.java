package org.thoughtcrime.securesms.conversation;

import org.thoughtcrime.securesms.conversation.ConversationFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda12 implements Runnable {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda12(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener) {
        this.f$0 = conversationFragmentItemClickListener;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onQuoteClicked$1();
    }
}
