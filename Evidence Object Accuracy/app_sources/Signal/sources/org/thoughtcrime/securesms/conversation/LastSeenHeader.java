package org.thoughtcrime.securesms.conversation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationAdapter;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;

/* loaded from: classes4.dex */
public class LastSeenHeader extends StickyHeaderDecoration {
    private final ConversationAdapter adapter;
    private final long lastSeenTimestamp;
    private long unreadCount;

    public LastSeenHeader(ConversationAdapter conversationAdapter, long j) {
        super(conversationAdapter, false, false, 3);
        this.adapter = conversationAdapter;
        this.lastSeenTimestamp = j;
    }

    public void setUnreadCount(long j) {
        this.unreadCount = j;
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration
    protected boolean hasHeader(RecyclerView recyclerView, StickyHeaderDecoration.StickyHeaderAdapter stickyHeaderAdapter, int i) {
        if (this.lastSeenTimestamp <= 0) {
            return false;
        }
        long receivedTimestamp = this.adapter.getReceivedTimestamp(i);
        long receivedTimestamp2 = this.adapter.getReceivedTimestamp(i + 1);
        long j = this.lastSeenTimestamp;
        if (receivedTimestamp <= j || receivedTimestamp2 >= j) {
            return false;
        }
        return true;
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration
    protected int getHeaderTop(RecyclerView recyclerView, View view, View view2, int i, int i2) {
        return recyclerView.getLayoutManager().getDecoratedTop(view);
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration
    protected RecyclerView.ViewHolder getHeader(RecyclerView recyclerView, StickyHeaderDecoration.StickyHeaderAdapter stickyHeaderAdapter, int i) {
        ConversationAdapter.StickyHeaderViewHolder stickyHeaderViewHolder = new ConversationAdapter.StickyHeaderViewHolder(LayoutInflater.from(recyclerView.getContext()).inflate(R.layout.conversation_item_last_seen, (ViewGroup) recyclerView, false));
        this.adapter.onBindLastSeenViewHolder(stickyHeaderViewHolder, this.unreadCount);
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 0);
        stickyHeaderViewHolder.itemView.measure(ViewGroup.getChildMeasureSpec(makeMeasureSpec, recyclerView.getPaddingLeft() + recyclerView.getPaddingRight(), stickyHeaderViewHolder.itemView.getLayoutParams().width), ViewGroup.getChildMeasureSpec(makeMeasureSpec2, recyclerView.getPaddingTop() + recyclerView.getPaddingBottom(), stickyHeaderViewHolder.itemView.getLayoutParams().height));
        View view = stickyHeaderViewHolder.itemView;
        view.layout(0, 0, view.getMeasuredWidth(), stickyHeaderViewHolder.itemView.getMeasuredHeight());
        return stickyHeaderViewHolder;
    }
}
