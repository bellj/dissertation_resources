package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.io.CloseableKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.reactions.ReactionsConversationView;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;

/* compiled from: ConversationItemSelection.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J2\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\u00042\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u000bH\u0002J*\u0010\r\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0007¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationItemSelection;", "", "()V", "snapshotMessage", "Landroid/graphics/Bitmap;", "conversationItem", "Lorg/thoughtcrime/securesms/conversation/ConversationItem;", "list", "Landroidx/recyclerview/widget/RecyclerView;", "videoBitmap", "drawConversationItem", "", "hasReaction", "snapshotView", "messageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationItemSelection {
    public static final ConversationItemSelection INSTANCE = new ConversationItemSelection();

    private ConversationItemSelection() {
    }

    @JvmStatic
    public static final Bitmap snapshotView(ConversationItem conversationItem, RecyclerView recyclerView, MessageRecord messageRecord, Bitmap bitmap) {
        Intrinsics.checkNotNullParameter(conversationItem, "conversationItem");
        Intrinsics.checkNotNullParameter(recyclerView, "list");
        Intrinsics.checkNotNullParameter(messageRecord, "messageRecord");
        boolean isOutgoing = messageRecord.isOutgoing();
        Context context = conversationItem.getContext();
        Intrinsics.checkNotNullExpressionValue(context, "conversationItem.context");
        boolean hasNoBubble = MessageRecordUtil.hasNoBubble(messageRecord, context);
        ConversationItemSelection conversationItemSelection = INSTANCE;
        boolean z = !isOutgoing || hasNoBubble;
        List<ReactionRecord> reactions = messageRecord.getReactions();
        Intrinsics.checkNotNullExpressionValue(reactions, "messageRecord.reactions");
        return conversationItemSelection.snapshotMessage(conversationItem, recyclerView, bitmap, z, !reactions.isEmpty());
    }

    /* JADX INFO: finally extract failed */
    private final Bitmap snapshotMessage(ConversationItem conversationItem, RecyclerView recyclerView, Bitmap bitmap, boolean z, boolean z2) {
        Bitmap bitmap2 = bitmap;
        ConversationItemBodyBubble conversationItemBodyBubble = conversationItem.bodyBubble;
        ReactionsConversationView reactionsConversationView = conversationItem.reactionsView;
        float scaleX = conversationItemBodyBubble.getScaleX();
        conversationItemBodyBubble.setScaleX(1.0f);
        conversationItemBodyBubble.setScaleY(1.0f);
        ProjectionList snapshotProjections = conversationItem.getSnapshotProjections(recyclerView, false);
        Intrinsics.checkNotNullExpressionValue(snapshotProjections, "conversationItem.getSnap…tProjections(list, false)");
        Path path = new Path();
        float x = (-conversationItem.getX()) - conversationItemBodyBubble.getX();
        float y = (-conversationItem.getY()) - conversationItemBodyBubble.getY();
        Projection giphyMp4PlayableProjection = conversationItem.getGiphyMp4PlayableProjection(recyclerView);
        Intrinsics.checkNotNullExpressionValue(giphyMp4PlayableProjection, "conversationItem.getGiph…4PlayableProjection(list)");
        if (bitmap2 != null) {
            bitmap2 = Bitmap.createScaledBitmap(bitmap2, (int) (((float) bitmap.getWidth()) / scaleX), (int) (((float) bitmap.getHeight()) / scaleX), true);
            giphyMp4PlayableProjection.translateX(x);
            giphyMp4PlayableProjection.translateY(y);
            giphyMp4PlayableProjection.applyToPath(path);
        }
        try {
            for (Projection projection : snapshotProjections) {
                projection.translateX(x);
                projection.translateY(y);
                projection.applyToPath(path);
            }
            Unit unit = Unit.INSTANCE;
            CloseableKt.closeFinally(snapshotProjections, null);
            ConversationItemSelectionKt.destroyAllDrawingCaches(conversationItem);
            int height = conversationItemBodyBubble.getHeight();
            if (z2) {
                height += (int) (((float) reactionsConversationView.getHeight()) - DimensionUnit.DP.toPixels(4.0f));
            }
            Bitmap createBitmap = Bitmap.createBitmap(conversationItemBodyBubble.getWidth(), height, Bitmap.Config.ARGB_8888);
            Intrinsics.checkNotNullExpressionValue(createBitmap, "Bitmap.createBitmap(width, height, config)");
            Canvas canvas = new Canvas(createBitmap);
            if (z) {
                conversationItemBodyBubble.draw(canvas);
            }
            int save = canvas.save();
            canvas.clipPath(path);
            try {
                int save2 = canvas.save();
                canvas.translate(x, y);
                recyclerView.draw(canvas);
                if (bitmap2 != null) {
                    canvas.drawBitmap(bitmap2, giphyMp4PlayableProjection.getX() - x, giphyMp4PlayableProjection.getY() - y, (Paint) null);
                }
                canvas.restoreToCount(save2);
                canvas.restoreToCount(save);
                float x2 = reactionsConversationView.getX() - conversationItemBodyBubble.getX();
                float y2 = reactionsConversationView.getY() - conversationItemBodyBubble.getY();
                int save3 = canvas.save();
                canvas.translate(x2, y2);
                try {
                    reactionsConversationView.draw(canvas);
                    canvas.restoreToCount(save3);
                    giphyMp4PlayableProjection.release();
                    conversationItemBodyBubble.setScaleX(scaleX);
                    conversationItemBodyBubble.setScaleY(scaleX);
                    return createBitmap;
                } catch (Throwable th) {
                    canvas.restoreToCount(save3);
                    throw th;
                }
            } catch (Throwable th2) {
                canvas.restoreToCount(save);
                throw th2;
            }
        } finally {
            try {
                throw th;
            } catch (Throwable th3) {
            }
        }
    }
}
