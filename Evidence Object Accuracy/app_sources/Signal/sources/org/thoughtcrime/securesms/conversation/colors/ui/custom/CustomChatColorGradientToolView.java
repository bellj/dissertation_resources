package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.core.view.GestureDetectorCompat;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: CustomChatColorGradientToolView.kt */
@Metadata(d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0013\u0018\u0000 B2\u00020\u0001:\u0003BCDB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0014J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u000202H\u0016J\u0010\u00103\u001a\u00020,2\b\b\u0001\u00104\u001a\u00020\u0007J\u000e\u00105\u001a\u00020,2\u0006\u0010\u0011\u001a\u00020\u0012J\u0010\u00106\u001a\u00020,2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u000e\u00107\u001a\u00020,2\u0006\u0010\u0017\u001a\u00020\u0018J\u000e\u00108\u001a\u00020,2\u0006\u0010\"\u001a\u00020#J\u0010\u00109\u001a\u00020,2\b\b\u0001\u00104\u001a\u00020\u0007J\u0014\u0010:\u001a\u00020\u0012*\u00020\f2\u0006\u0010;\u001a\u00020\fH\u0002J\u0014\u0010<\u001a\u00020\u0012*\u00020\f2\u0006\u0010;\u001a\u00020\fH\u0002J\u0014\u0010=\u001a\u00020\u0012*\u00020\f2\u0006\u0010;\u001a\u00020\fH\u0002J\f\u0010>\u001a\u00020\u0012*\u00020\u0012H\u0002J\u0014\u0010?\u001a\u00020\u0012*\u00020\u00122\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\f\u0010@\u001a\u00020\u0012*\u00020\u0012H\u0002J\f\u0010A\u001a\u00020\u0012*\u00020\u0012H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0015\u001a\u00060\u0016R\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020!X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020#X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010'\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006E"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView;", "Landroid/view/View;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "backgroundPaint", "Landroid/graphics/Paint;", "bottom", "Landroid/graphics/PointF;", "bottomColorPaint", "center", "clipRect", "Landroid/graphics/Rect;", "degrees", "", "gestureDetectorCompat", "Landroidx/core/view/GestureDetectorCompat;", "gestureListener", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView$GestureListener;", "listener", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView$Listener;", "opaqueThumbPadding", "opaqueThumbPaddingSelected", "opaqueThumbRadius", "pipeBorder", "pipeBorderPaint", "pipePaint", "pipeWidth", "rect", "Landroid/graphics/RectF;", "selectedEdge", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;", "thumbBorder", "thumbBorderPaint", "thumbBorderPaintSelected", "thumbBorderSelected", "thumbRadius", "top", "topColorPaint", "onDraw", "", "canvas", "Landroid/graphics/Canvas;", "onTouchEvent", "", "event", "Landroid/view/MotionEvent;", "setBottomColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setDegrees", "setDegreesInternal", "setListener", "setSelectedEdge", "setTopColor", "determinate", "other", "distance", "dotProduct", "invertDegrees", "rotate", "toDegrees", "toRadians", "Companion", "GestureListener", "Listener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorGradientToolView extends View {
    public static final Companion Companion = new Companion(null);
    private static final int OPAGUE_THUMB_PADDING;
    private static final int OPAGUE_THUMB_PADDING_SELECTED;
    private static final int OPAQUE_THUMB_RADIUS;
    private static final int PIPE_BORDER;
    private static final int PIPE_WIDTH;
    private static final int THUMB_BORDER;
    private static final int THUMB_BORDER_SELECTED;
    private static final int THUMB_RADIUS;
    private final Paint backgroundPaint;
    private final PointF bottom;
    private final Paint bottomColorPaint;
    private final PointF center;
    private final Rect clipRect;
    private float degrees;
    private final GestureDetectorCompat gestureDetectorCompat;
    private final GestureListener gestureListener;
    private Listener listener;
    private final float opaqueThumbPadding;
    private final float opaqueThumbPaddingSelected;
    private final float opaqueThumbRadius;
    private final float pipeBorder;
    private final Paint pipeBorderPaint;
    private final Paint pipePaint;
    private final float pipeWidth;
    private final RectF rect;
    private CustomChatColorEdge selectedEdge;
    private final float thumbBorder;
    private final Paint thumbBorderPaint;
    private final Paint thumbBorderPaintSelected;
    private final float thumbBorderSelected;
    private final float thumbRadius;
    private final PointF top;
    private final Paint topColorPaint;

    /* compiled from: CustomChatColorGradientToolView.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0003H&J\b\u0010\u0007\u001a\u00020\u0003H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\t\u001a\u00020\nH&¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView$Listener;", "", "onDegreesChanged", "", "degrees", "", "onGestureFinished", "onGestureStarted", "onSelectedEdgeChanged", "edge", "Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorEdge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Listener {
        void onDegreesChanged(float f);

        void onGestureFinished();

        void onGestureStarted();

        void onSelectedEdgeChanged(CustomChatColorEdge customChatColorEdge);
    }

    /* compiled from: CustomChatColorGradientToolView.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CustomChatColorEdge.values().length];
            iArr[CustomChatColorEdge.TOP.ordinal()] = 1;
            iArr[CustomChatColorEdge.BOTTOM.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public CustomChatColorGradientToolView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public CustomChatColorGradientToolView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    private final float rotate(float f, float f2) {
        return ((f + f2) + 360.0f) % 360.0f;
    }

    public final float toDegrees(float f) {
        return f * 57.295776f;
    }

    private final float toRadians(float f) {
        return f * 0.017453292f;
    }

    public /* synthetic */ CustomChatColorGradientToolView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CustomChatColorGradientToolView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        this.clipRect = new Rect();
        this.rect = new RectF();
        this.center = new PointF();
        this.top = new PointF();
        this.bottom = new PointF();
        this.selectedEdge = CustomChatColorEdge.TOP;
        this.degrees = 18.0f;
        this.thumbRadius = (float) ViewUtil.dpToPx(24);
        this.thumbBorder = (float) ViewUtil.dpToPx(1);
        this.thumbBorderSelected = (float) ViewUtil.dpToPx(4);
        this.opaqueThumbRadius = (float) ViewUtil.dpToPx(20);
        this.opaqueThumbPadding = (float) ViewUtil.dpToPx(2);
        this.opaqueThumbPaddingSelected = (float) ViewUtil.dpToPx(1);
        float dpToPx = (float) ViewUtil.dpToPx(6);
        this.pipeWidth = dpToPx;
        float dpToPx2 = (float) ViewUtil.dpToPx(1);
        this.pipeBorder = dpToPx2;
        Paint paint = new Paint(1);
        paint.setColor(-65536);
        this.topColorPaint = paint;
        Paint paint2 = new Paint(1);
        paint2.setColor(-16776961);
        this.bottomColorPaint = paint2;
        Paint paint3 = new Paint(1);
        paint3.setColor(ContextCompat.getColor(context, R.color.signal_background_primary));
        this.backgroundPaint = paint3;
        Paint paint4 = new Paint(1);
        paint4.setColor(ContextCompat.getColor(context, R.color.signal_inverse_transparent_10));
        this.thumbBorderPaint = paint4;
        Paint paint5 = new Paint(1);
        paint5.setColor(ContextCompat.getColor(context, R.color.signal_inverse_transparent_60));
        this.thumbBorderPaintSelected = paint5;
        Paint paint6 = new Paint(1);
        paint6.setStrokeWidth(dpToPx - (dpToPx2 * ((float) 2)));
        paint6.setStyle(Paint.Style.STROKE);
        paint6.setColor(ContextCompat.getColor(context, R.color.signal_background_primary));
        this.pipePaint = paint6;
        Paint paint7 = new Paint(1);
        paint7.setStrokeWidth(dpToPx);
        paint7.setStyle(Paint.Style.STROKE);
        paint7.setColor(ContextCompat.getColor(context, R.color.signal_inverse_transparent_10));
        this.pipeBorderPaint = paint7;
        GestureListener gestureListener = new GestureListener();
        this.gestureListener = gestureListener;
        this.gestureDetectorCompat = new GestureDetectorCompat(context, gestureListener);
    }

    public final void setTopColor(int i) {
        this.topColorPaint.setColor(i);
        invalidate();
    }

    public final void setBottomColor(int i) {
        this.bottomColorPaint.setColor(i);
        invalidate();
    }

    public final void setSelectedEdge(CustomChatColorEdge customChatColorEdge) {
        Intrinsics.checkNotNullParameter(customChatColorEdge, "selectedEdge");
        if (this.selectedEdge != customChatColorEdge) {
            this.selectedEdge = customChatColorEdge;
            invalidate();
            Listener listener = this.listener;
            if (listener != null) {
                listener.onSelectedEdgeChanged(customChatColorEdge);
            }
        }
    }

    public final void setDegrees(float f) {
        setDegreesInternal(invertDegrees(f));
    }

    public final void setDegreesInternal(float f) {
        if (!(this.degrees == f)) {
            this.degrees = f;
            invalidate();
            Listener listener = this.listener;
            if (listener != null) {
                listener.onDegreesChanged(invertDegrees(f));
            }
        }
    }

    private final float invertDegrees(float f) {
        return 360.0f - rotate(f, 90.0f);
    }

    public final void setListener(Listener listener) {
        Intrinsics.checkNotNullParameter(listener, "listener");
        this.listener = listener;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        Listener listener;
        Intrinsics.checkNotNullParameter(motionEvent, "event");
        if (motionEvent.getAction() == 3 || motionEvent.getAction() == 1) {
            Listener listener2 = this.listener;
            if (listener2 != null) {
                listener2.onGestureFinished();
            }
        } else if (motionEvent.getAction() == 0 && (listener = this.listener) != null) {
            listener.onGestureStarted();
        }
        return this.gestureDetectorCompat.onTouchEvent(motionEvent);
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        Pair pair;
        Pair pair2;
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        canvas.getClipBounds(this.clipRect);
        this.rect.set(this.clipRect);
        RectF rectF = this.rect;
        float f = this.thumbRadius;
        rectF.inset(f, f);
        this.center.set(this.rect.width() / 2.0f, this.rect.height() / 2.0f);
        float degrees = toDegrees((float) Math.atan((double) (this.rect.height() / this.rect.width())));
        double d = (double) (((float) 4) * degrees);
        Double.isNaN(d);
        double d2 = 360.0d - d;
        double d3 = (double) 4.0f;
        Double.isNaN(d3);
        double d4 = d2 / d3;
        float f2 = this.degrees;
        if (f2 < degrees) {
            float tan = this.center.x * ((float) Math.tan((double) toRadians(f2)));
            this.top.set(this.rect.width(), this.center.y - tan);
            this.bottom.set(0.0f, this.center.y + tan);
        } else if (f2 < 90.0f) {
            float tan2 = this.center.y * ((float) Math.tan((double) toRadians(90.0f - f2)));
            this.top.set(this.center.x + tan2, 0.0f);
            this.bottom.set(this.center.x - tan2, this.rect.height());
        } else {
            double d5 = (double) 90.0f;
            Double.isNaN(d5);
            if (((double) f2) < d5 + d4) {
                float tan3 = this.center.y * ((float) Math.tan((double) toRadians(f2 - 90.0f)));
                this.top.set(this.center.x - tan3, 0.0f);
                this.bottom.set(this.center.x + tan3, this.rect.height());
            } else if (f2 < 180.0f) {
                float tan4 = this.center.x * ((float) Math.tan((double) toRadians(180.0f - f2)));
                this.top.set(0.0f, this.center.y - tan4);
                this.bottom.set(this.rect.width(), this.center.y + tan4);
            } else if (f2 < degrees + 180.0f) {
                float tan5 = this.center.x * ((float) Math.tan((double) toRadians(f2 - 180.0f)));
                this.top.set(0.0f, this.center.y + tan5);
                this.bottom.set(this.rect.width(), this.center.y - tan5);
            } else if (f2 < 270.0f) {
                float tan6 = this.center.y * ((float) Math.tan((double) toRadians(270.0f - f2)));
                this.top.set(this.center.x - tan6, this.rect.height());
                this.bottom.set(this.center.x + tan6, 0.0f);
            } else {
                double d6 = (double) 270.0f;
                Double.isNaN(d6);
                if (((double) f2) < d6 + d4) {
                    float tan7 = this.center.y * ((float) Math.tan((double) toRadians(f2 - 270.0f)));
                    this.top.set(this.center.x + tan7, this.rect.height());
                    this.bottom.set(this.center.x - tan7, 0.0f);
                } else {
                    float tan8 = this.center.x * ((float) Math.tan((double) toRadians(360.0f - f2)));
                    this.top.set(this.rect.width(), this.center.y + tan8);
                    this.bottom.set(0.0f, this.center.y - tan8);
                }
            }
        }
        CustomChatColorEdge customChatColorEdge = this.selectedEdge;
        int[] iArr = WhenMappings.$EnumSwitchMapping$0;
        int i = iArr[customChatColorEdge.ordinal()];
        if (i == 1) {
            pair = TuplesKt.to(this.top, this.bottom);
        } else if (i == 2) {
            pair = TuplesKt.to(this.bottom, this.top);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        PointF pointF = (PointF) pair.component1();
        PointF pointF2 = (PointF) pair.component2();
        int i2 = iArr[this.selectedEdge.ordinal()];
        if (i2 == 1) {
            pair2 = TuplesKt.to(this.topColorPaint, this.bottomColorPaint);
        } else if (i2 == 2) {
            pair2 = TuplesKt.to(this.bottomColorPaint, this.topColorPaint);
        } else {
            throw new NoWhenBranchMatchedException();
        }
        Paint paint = (Paint) pair2.component1();
        Paint paint2 = (Paint) pair2.component2();
        canvas.save();
        RectF rectF2 = this.rect;
        canvas.translate(rectF2.top, rectF2.left);
        canvas.drawLine(pointF.x, pointF.y, pointF2.x, pointF2.y, this.pipeBorderPaint);
        canvas.drawLine(pointF.x, pointF.y, pointF2.x, pointF2.y, this.pipePaint);
        canvas.drawCircle(pointF2.x, pointF2.y, this.opaqueThumbRadius + this.thumbBorder, this.thumbBorderPaint);
        canvas.drawCircle(pointF2.x, pointF2.y, this.opaqueThumbRadius, this.backgroundPaint);
        canvas.drawCircle(pointF2.x, pointF2.y, this.opaqueThumbRadius - this.opaqueThumbPadding, paint2);
        canvas.drawCircle(pointF.x, pointF.y, this.opaqueThumbRadius + this.thumbBorderSelected, this.thumbBorderPaintSelected);
        canvas.drawCircle(pointF.x, pointF.y, this.opaqueThumbRadius, this.backgroundPaint);
        canvas.drawCircle(pointF.x, pointF.y, this.opaqueThumbRadius - this.opaqueThumbPaddingSelected, paint);
        canvas.restore();
        PointF pointF3 = this.top;
        RectF rectF3 = this.rect;
        pointF3.offset(rectF3.top, rectF3.left);
        PointF pointF4 = this.bottom;
        RectF rectF4 = this.rect;
        pointF4.offset(rectF4.top, rectF4.left);
    }

    public final float distance(PointF pointF, PointF pointF2) {
        double d = (double) 2;
        return Math.abs((float) Math.sqrt((double) (((float) Math.pow((double) (pointF.x - pointF2.x), d)) + ((float) Math.pow((double) (pointF.y - pointF2.y), d)))));
    }

    public final float dotProduct(PointF pointF, PointF pointF2) {
        return (pointF.x * pointF2.x) + (pointF.y * pointF2.y);
    }

    public final float determinate(PointF pointF, PointF pointF2) {
        return (pointF.x * pointF2.y) - (pointF.y * pointF2.x);
    }

    /* compiled from: CustomChatColorGradientToolView.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J(\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0011H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView$GestureListener;", "Landroid/view/GestureDetector$SimpleOnGestureListener;", "(Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView;)V", "activePointerId", "", "getActivePointerId", "()I", "setActivePointerId", "(I)V", "onDown", "", "e", "Landroid/view/MotionEvent;", "onScroll", "e1", "e2", "distanceX", "", "distanceY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        private int activePointerId = -1;

        public GestureListener() {
            CustomChatColorGradientToolView.this = r1;
        }

        public final int getActivePointerId() {
            return this.activePointerId;
        }

        public final void setActivePointerId(int i) {
            this.activePointerId = i;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onDown(MotionEvent motionEvent) {
            Intrinsics.checkNotNullParameter(motionEvent, "e");
            this.activePointerId = motionEvent.getPointerId(0);
            PointF pointF = new PointF(motionEvent.getX(this.activePointerId), motionEvent.getY(this.activePointerId));
            CustomChatColorGradientToolView customChatColorGradientToolView = CustomChatColorGradientToolView.this;
            if (customChatColorGradientToolView.distance(pointF, customChatColorGradientToolView.top) <= CustomChatColorGradientToolView.this.thumbRadius) {
                CustomChatColorGradientToolView.this.setSelectedEdge(CustomChatColorEdge.TOP);
                return true;
            }
            CustomChatColorGradientToolView customChatColorGradientToolView2 = CustomChatColorGradientToolView.this;
            if (customChatColorGradientToolView2.distance(pointF, customChatColorGradientToolView2.bottom) > CustomChatColorGradientToolView.this.thumbRadius) {
                return false;
            }
            CustomChatColorGradientToolView.this.setSelectedEdge(CustomChatColorEdge.BOTTOM);
            return true;
        }

        @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
        public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
            Intrinsics.checkNotNullParameter(motionEvent, "e1");
            Intrinsics.checkNotNullParameter(motionEvent2, "e2");
            PointF pointF = new PointF(motionEvent2.getX(this.activePointerId) - CustomChatColorGradientToolView.this.center.x, motionEvent2.getY(this.activePointerId) - CustomChatColorGradientToolView.this.center.y);
            float f3 = 0.0f;
            PointF pointF2 = new PointF(CustomChatColorGradientToolView.this.center.x, 0.0f);
            float dotProduct = CustomChatColorGradientToolView.this.dotProduct(pointF, pointF2);
            float determinate = CustomChatColorGradientToolView.this.determinate(pointF, pointF2);
            if (CustomChatColorGradientToolView.this.selectedEdge == CustomChatColorEdge.BOTTOM) {
                f3 = 180.0f;
            }
            CustomChatColorGradientToolView.this.setDegreesInternal(((CustomChatColorGradientToolView.this.toDegrees((float) Math.atan2((double) determinate, (double) dotProduct)) + 360.0f) + f3) % 360.0f);
            return true;
        }
    }

    /* compiled from: CustomChatColorGradientToolView.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u00020\u00048\u0002XT¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorGradientToolView$Companion;", "", "()V", "OPAGUE_THUMB_PADDING", "", "OPAGUE_THUMB_PADDING_SELECTED", "OPAQUE_THUMB_RADIUS", "PIPE_BORDER", "PIPE_WIDTH", "THUMB_BORDER", "THUMB_BORDER_SELECTED", "THUMB_RADIUS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
