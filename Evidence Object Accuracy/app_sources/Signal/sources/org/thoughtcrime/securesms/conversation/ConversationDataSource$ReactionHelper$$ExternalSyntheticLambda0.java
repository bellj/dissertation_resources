package org.thoughtcrime.securesms.conversation;

import j$.util.function.Function;
import org.thoughtcrime.securesms.conversation.ConversationDataSource;
import org.thoughtcrime.securesms.database.model.MessageRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationDataSource$ReactionHelper$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ ConversationDataSource.ReactionHelper f$0;

    public /* synthetic */ ConversationDataSource$ReactionHelper$$ExternalSyntheticLambda0(ConversationDataSource.ReactionHelper reactionHelper) {
        this.f$0 = reactionHelper;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return this.f$0.lambda$buildUpdatedModels$0((MessageRecord) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
