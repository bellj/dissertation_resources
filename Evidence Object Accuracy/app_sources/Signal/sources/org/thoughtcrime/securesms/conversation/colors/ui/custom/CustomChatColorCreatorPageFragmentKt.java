package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import kotlin.Metadata;

/* compiled from: CustomChatColorCreatorPageFragment.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0005XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0006\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"GRADIENT_PAGE", "", "MAX_HUE", "MAX_SEEK_DIVISIONS", "PAGE_ARG", "", "SINGLE_PAGE", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorCreatorPageFragmentKt {
    private static final int GRADIENT_PAGE;
    private static final int MAX_HUE;
    private static final int MAX_SEEK_DIVISIONS;
    private static final String PAGE_ARG;
    private static final int SINGLE_PAGE;
}
