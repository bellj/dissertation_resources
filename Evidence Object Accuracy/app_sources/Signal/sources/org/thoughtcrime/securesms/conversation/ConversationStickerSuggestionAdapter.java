package org.thoughtcrime.securesms.conversation;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;

/* loaded from: classes4.dex */
public class ConversationStickerSuggestionAdapter extends RecyclerView.Adapter<StickerSuggestionViewHolder> {
    private final EventListener eventListener;
    private final GlideRequests glideRequests;
    private final List<StickerRecord> stickers = new ArrayList();

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onStickerSuggestionClicked(StickerRecord stickerRecord);
    }

    public ConversationStickerSuggestionAdapter(GlideRequests glideRequests, EventListener eventListener) {
        this.glideRequests = glideRequests;
        this.eventListener = eventListener;
    }

    public StickerSuggestionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new StickerSuggestionViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.sticker_suggestion_list_item, viewGroup, false));
    }

    public void onBindViewHolder(StickerSuggestionViewHolder stickerSuggestionViewHolder, int i) {
        stickerSuggestionViewHolder.bind(this.glideRequests, this.eventListener, this.stickers.get(i));
    }

    public void onViewRecycled(StickerSuggestionViewHolder stickerSuggestionViewHolder) {
        stickerSuggestionViewHolder.recycle();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.stickers.size();
    }

    public void setStickers(List<StickerRecord> list) {
        this.stickers.clear();
        this.stickers.addAll(list);
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class StickerSuggestionViewHolder extends RecyclerView.ViewHolder {
        private final ImageView image;

        StickerSuggestionViewHolder(View view) {
            super(view);
            this.image = (ImageView) view.findViewById(R.id.sticker_suggestion_item_image);
        }

        void bind(GlideRequests glideRequests, EventListener eventListener, StickerRecord stickerRecord) {
            glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(stickerRecord.getUri())).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).into(this.image);
            this.itemView.setOnClickListener(new ConversationStickerSuggestionAdapter$StickerSuggestionViewHolder$$ExternalSyntheticLambda0(eventListener, stickerRecord));
        }

        void recycle() {
            this.itemView.setOnClickListener(null);
        }
    }
}
