package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: CustomChatColorPagerAdapter.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorPagerAdapter;", "Landroidx/viewpager2/adapter/FragmentStateAdapter;", "parentFragment", "Landroidx/fragment/app/Fragment;", "arguments", "Landroid/os/Bundle;", "(Landroidx/fragment/app/Fragment;Landroid/os/Bundle;)V", "createFragment", "position", "", "getItemCount", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorPagerAdapter extends FragmentStateAdapter {
    private final Bundle arguments;

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return 2;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CustomChatColorPagerAdapter(Fragment fragment, Bundle bundle) {
        super(fragment);
        Intrinsics.checkNotNullParameter(fragment, "parentFragment");
        Intrinsics.checkNotNullParameter(bundle, "arguments");
        this.arguments = bundle;
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment createFragment(int i) {
        if (i == 0) {
            return CustomChatColorCreatorPageFragment.Companion.forSingle(this.arguments);
        }
        if (i == 1) {
            return CustomChatColorCreatorPageFragment.Companion.forGradient(this.arguments);
        }
        throw new AssertionError();
    }
}
