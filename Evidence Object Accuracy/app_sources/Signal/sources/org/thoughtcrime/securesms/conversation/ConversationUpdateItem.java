package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.content.res.ColorStateList;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.material.button.MaterialButton;
import com.google.common.collect.Sets;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BindableConversationItem;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog;
import org.thoughtcrime.securesms.database.model.GroupCallUpdateDetailsUtil;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.InMemoryMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicyEnforcer;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.IdentityUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class ConversationUpdateItem extends FrameLayout implements BindableConversationItem {
    private static final ProjectionList EMPTY_PROJECTION_LIST = new ProjectionList();
    private static final String TAG = Log.tag(ConversationUpdateItem.class);
    private MaterialButton actionButton;
    private View background;
    private Set<MultiselectPart> batchSelected;
    private TextView body;
    private ConversationMessage conversationMessage;
    private Recipient conversationRecipient;
    private LiveData<SpannableString> displayBody;
    private BindableConversationItem.EventListener eventListener;
    private final GroupDataManager groupData;
    private final RecipientObserverManager groupObserver;
    private boolean isMessageRequestAccepted;
    private MessageRecord messageRecord;
    private Optional<MessageRecord> nextMessageRecord;
    private final PresentOnChange presentOnChange;
    private Optional<MessageRecord> previousMessageRecord;
    private final RecipientObserverManager senderObserver;
    private final UpdateObserver updateObserver = new UpdateObserver();

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean canPlayContent() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public /* synthetic */ MediaItem getMediaItem() {
        return GiphyMp4Playable.CC.$default$getMediaItem(this);
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public /* synthetic */ GiphyMp4PlaybackPolicyEnforcer getPlaybackPolicyEnforcer() {
        return GiphyMp4Playable.CC.$default$getPlaybackPolicyEnforcer(this);
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public boolean hasNonSelectableMedia() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public boolean shouldProjectContent() {
        return false;
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void showProjectionArea() {
    }

    @Override // org.thoughtcrime.securesms.Unbindable
    public void unbind() {
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public /* synthetic */ void updateContactNameColor() {
        BindableConversationItem.CC.$default$updateContactNameColor(this);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public /* synthetic */ void updateTimestamps() {
        BindableConversationItem.CC.$default$updateTimestamps(this);
    }

    public ConversationUpdateItem(Context context) {
        super(context);
        PresentOnChange presentOnChange = new PresentOnChange();
        this.presentOnChange = presentOnChange;
        this.senderObserver = new RecipientObserverManager(presentOnChange);
        this.groupObserver = new RecipientObserverManager(presentOnChange);
        this.groupData = new GroupDataManager();
    }

    public ConversationUpdateItem(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        PresentOnChange presentOnChange = new PresentOnChange();
        this.presentOnChange = presentOnChange;
        this.senderObserver = new RecipientObserverManager(presentOnChange);
        this.groupObserver = new RecipientObserverManager(presentOnChange);
        this.groupData = new GroupDataManager();
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.body = (TextView) findViewById(R.id.conversation_update_body);
        this.actionButton = (MaterialButton) findViewById(R.id.conversation_update_action);
        this.background = findViewById(R.id.conversation_update_background);
        this.body.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationUpdateItem.$r8$lambda$duX7Q92BBdVDKemIKTVRD4Z_DaM(ConversationUpdateItem.this, view);
            }
        });
        this.body.setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda1
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return ConversationUpdateItem.$r8$lambda$rkThpkrBJdHt8gKOg6Fa_Pr7FMQ(ConversationUpdateItem.this, view);
            }
        });
        setOnClickListener(new InternalClickListener(null));
    }

    public /* synthetic */ void lambda$onFinishInflate$0(View view) {
        performClick();
    }

    public /* synthetic */ boolean lambda$onFinishInflate$1(View view) {
        return performLongClick();
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void bind(LifecycleOwner lifecycleOwner, ConversationMessage conversationMessage, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, GlideRequests glideRequests, Locale locale, Set<MultiselectPart> set, Recipient recipient, String str, boolean z, boolean z2, boolean z3, boolean z4, Colorizer colorizer, boolean z5) {
        this.batchSelected = set;
        bind(lifecycleOwner, conversationMessage, optional, optional2, recipient, z2, z3);
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void setEventListener(BindableConversationItem.EventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem, org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public ConversationMessage getConversationMessage() {
        return this.conversationMessage;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0044, code lost:
        if (r0.isGroupV2JoinRequest(r0.getIndividualRecipient().getServiceId().orElse(null)) != false) goto L_0x0046;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void bind(androidx.lifecycle.LifecycleOwner r6, org.thoughtcrime.securesms.conversation.ConversationMessage r7, j$.util.Optional<org.thoughtcrime.securesms.database.model.MessageRecord> r8, j$.util.Optional<org.thoughtcrime.securesms.database.model.MessageRecord> r9, org.thoughtcrime.securesms.recipients.Recipient r10, boolean r11, boolean r12) {
        /*
            r5 = this;
            r5.conversationMessage = r7
            org.thoughtcrime.securesms.database.model.MessageRecord r0 = r7.getMessageRecord()
            r5.messageRecord = r0
            r5.previousMessageRecord = r8
            r5.nextMessageRecord = r9
            r5.conversationRecipient = r10
            r5.isMessageRequestAccepted = r12
            org.thoughtcrime.securesms.conversation.ConversationUpdateItem$RecipientObserverManager r1 = r5.senderObserver
            org.thoughtcrime.securesms.recipients.Recipient r0 = r0.getIndividualRecipient()
            r1.observe(r6, r0)
            boolean r0 = r10.isActiveGroup()
            r1 = 0
            if (r0 == 0) goto L_0x0051
            org.thoughtcrime.securesms.database.model.MessageRecord r0 = r5.messageRecord
            boolean r0 = r0.isGroupCall()
            if (r0 != 0) goto L_0x0046
            org.thoughtcrime.securesms.database.model.MessageRecord r0 = r5.messageRecord
            boolean r0 = r0.isCollapsedGroupV2JoinUpdate()
            if (r0 != 0) goto L_0x0046
            org.thoughtcrime.securesms.database.model.MessageRecord r0 = r5.messageRecord
            org.thoughtcrime.securesms.recipients.Recipient r2 = r0.getIndividualRecipient()
            j$.util.Optional r2 = r2.getServiceId()
            java.lang.Object r2 = r2.orElse(r1)
            org.whispersystems.signalservice.api.push.ServiceId r2 = (org.whispersystems.signalservice.api.push.ServiceId) r2
            boolean r0 = r0.isGroupV2JoinRequest(r2)
            if (r0 == 0) goto L_0x0051
        L_0x0046:
            org.thoughtcrime.securesms.conversation.ConversationUpdateItem$RecipientObserverManager r0 = r5.groupObserver
            r0.observe(r6, r10)
            org.thoughtcrime.securesms.conversation.ConversationUpdateItem$GroupDataManager r0 = r5.groupData
            r0.observe(r6, r10)
            goto L_0x0056
        L_0x0051:
            org.thoughtcrime.securesms.conversation.ConversationUpdateItem$RecipientObserverManager r0 = r5.groupObserver
            r0.observe(r6, r1)
        L_0x0056:
            android.content.Context r0 = r5.getContext()
            r1 = 2131099813(0x7f0600a5, float:1.781199E38)
            int r0 = androidx.core.content.ContextCompat.getColor(r0, r1)
            android.content.Context r1 = r5.getContext()
            boolean r1 = org.thoughtcrime.securesms.util.ThemeUtil.isDarkTheme(r1)
            if (r1 == 0) goto L_0x0078
            if (r11 == 0) goto L_0x0078
            android.content.Context r0 = r5.getContext()
            r1 = 2131099862(0x7f0600d6, float:1.781209E38)
            int r0 = androidx.core.content.ContextCompat.getColor(r0, r1)
        L_0x0078:
            org.thoughtcrime.securesms.database.model.MessageRecord r1 = r5.messageRecord
            android.content.Context r2 = r5.getContext()
            org.thoughtcrime.securesms.BindableConversationItem$EventListener r3 = r5.eventListener
            java.util.Objects.requireNonNull(r3)
            org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda2 r4 = new org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda2
            r4.<init>()
            org.thoughtcrime.securesms.database.model.UpdateDescription r1 = r1.getUpdateDisplayBody(r2, r4)
            java.util.Objects.requireNonNull(r1)
            android.content.Context r2 = r5.getContext()
            r3 = 1
            androidx.lifecycle.LiveData r0 = org.thoughtcrime.securesms.database.model.LiveUpdateMessage.fromMessageDescription(r2, r1, r0, r3)
            androidx.lifecycle.LiveData r0 = r5.loading(r0)
            r5.observeDisplayBody(r6, r0)
            r5.present(r7, r9, r10, r12)
            org.thoughtcrime.securesms.database.model.MessageRecord r6 = r5.messageRecord
            boolean r6 = shouldCollapse(r6, r8)
            org.thoughtcrime.securesms.database.model.MessageRecord r8 = r5.messageRecord
            boolean r8 = shouldCollapse(r8, r9)
            r5.presentBackground(r6, r8, r11)
            org.thoughtcrime.securesms.database.model.MessageRecord r6 = r7.getMessageRecord()
            boolean r6 = r6.isBoostRequest()
            r5.presentActionButton(r11, r6)
            r5.updateSelectedState()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationUpdateItem.bind(androidx.lifecycle.LifecycleOwner, org.thoughtcrime.securesms.conversation.ConversationMessage, j$.util.Optional, j$.util.Optional, org.thoughtcrime.securesms.recipients.Recipient, boolean, boolean):void");
    }

    @Override // org.thoughtcrime.securesms.BindableConversationItem
    public void updateSelectedState() {
        if (this.batchSelected.size() > 0) {
            this.body.setMovementMethod(null);
        } else {
            this.body.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    private static boolean shouldCollapse(MessageRecord messageRecord, Optional<MessageRecord> optional) {
        return optional.isPresent() && optional.get().isUpdate() && DateUtils.isSameDay(messageRecord.getTimestamp(), optional.get().getTimestamp()) && isSameType(messageRecord, optional.get());
    }

    private LiveData<SpannableString> loading(LiveData<SpannableString> liveData) {
        return LiveDataUtil.until(liveData, LiveDataUtil.delay(250, new SpannableString(getContext().getString(R.string.ConversationUpdateItem_loading))));
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public void hideProjectionArea() {
        throw new UnsupportedOperationException("Call makes no sense for a conversation update item");
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public int getAdapterPosition() {
        throw new UnsupportedOperationException("Don't delegate to this method.");
    }

    @Override // org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable
    public Projection getGiphyMp4PlayableProjection(ViewGroup viewGroup) {
        throw new UnsupportedOperationException("ConversationUpdateItems cannot be projected into.");
    }

    @Override // org.thoughtcrime.securesms.conversation.colors.Colorizable
    public ProjectionList getColorizerProjections(ViewGroup viewGroup) {
        return EMPTY_PROJECTION_LIST;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public View getHorizontalTranslationTarget() {
        return this.background;
    }

    /* loaded from: classes4.dex */
    public static final class RecipientObserverManager {
        private LiveRecipient recipient;
        private final Observer<Recipient> recipientObserver;

        RecipientObserverManager(Observer<Recipient> observer) {
            this.recipientObserver = observer;
        }

        public void observe(LifecycleOwner lifecycleOwner, Recipient recipient) {
            LiveRecipient liveRecipient = this.recipient;
            if (liveRecipient != null) {
                liveRecipient.getLiveData().removeObserver(this.recipientObserver);
            }
            if (recipient != null) {
                LiveRecipient live = recipient.live();
                this.recipient = live;
                live.getLiveData().observe(lifecycleOwner, this.recipientObserver);
                return;
            }
            this.recipient = null;
        }

        Recipient getObservedRecipient() {
            return this.recipient.get();
        }
    }

    /* loaded from: classes4.dex */
    public final class GroupDataManager {
        private Recipient conversationRecipient;
        private LiveData<Set<UUID>> liveBannedMembers;
        private LiveData<Set<UUID>> liveFullMembers;
        private LiveGroup liveGroup;
        private LiveData<Boolean> liveIsSelfAdmin;
        private final Observer<Object> updater = new ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda1(this);

        GroupDataManager() {
            ConversationUpdateItem.this = r1;
        }

        public /* synthetic */ void lambda$new$0(Object obj) {
            update();
        }

        public void observe(LifecycleOwner lifecycleOwner, Recipient recipient) {
            if (this.liveGroup != null) {
                this.liveIsSelfAdmin.removeObserver(this.updater);
                this.liveBannedMembers.removeObserver(this.updater);
                this.liveFullMembers.removeObserver(this.updater);
            }
            if (recipient != null) {
                this.conversationRecipient = recipient;
                LiveGroup liveGroup = new LiveGroup(recipient.requireGroupId());
                this.liveGroup = liveGroup;
                this.liveIsSelfAdmin = liveGroup.isSelfAdmin();
                this.liveBannedMembers = this.liveGroup.getBannedMembers();
                this.liveFullMembers = Transformations.map(this.liveGroup.getFullMembers(), new ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda2());
                this.liveIsSelfAdmin.observe(lifecycleOwner, this.updater);
                this.liveBannedMembers.observe(lifecycleOwner, this.updater);
                this.liveFullMembers.observe(lifecycleOwner, this.updater);
                return;
            }
            this.conversationRecipient = null;
            this.liveGroup = null;
            this.liveBannedMembers = null;
            this.liveIsSelfAdmin = null;
        }

        public static /* synthetic */ Set lambda$observe$2(List list) {
            return (Set) Collection$EL.stream(list).map(new ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda0()).collect(Collectors.toSet());
        }

        public static /* synthetic */ UUID lambda$observe$1(GroupMemberEntry.FullMember fullMember) {
            return fullMember.getMember().requireServiceId().uuid();
        }

        public boolean isSelfAdmin() {
            LiveData<Boolean> liveData = this.liveIsSelfAdmin;
            if (liveData == null || liveData.getValue() == null) {
                return false;
            }
            return this.liveIsSelfAdmin.getValue().booleanValue();
        }

        public boolean isBanned(Recipient recipient) {
            Set<UUID> value;
            LiveData<Set<UUID>> liveData = this.liveBannedMembers;
            if (liveData != null && (value = liveData.getValue()) != null && recipient.getServiceId().isPresent() && value.contains(recipient.requireServiceId().uuid())) {
                return true;
            }
            return false;
        }

        public boolean isFullMember(Recipient recipient) {
            Set<UUID> value;
            LiveData<Set<UUID>> liveData = this.liveFullMembers;
            if (liveData != null && (value = liveData.getValue()) != null && recipient.getServiceId().isPresent() && value.contains(recipient.requireServiceId().uuid())) {
                return true;
            }
            return false;
        }

        private void update() {
            ConversationUpdateItem conversationUpdateItem = ConversationUpdateItem.this;
            conversationUpdateItem.present(conversationUpdateItem.conversationMessage, ConversationUpdateItem.this.nextMessageRecord, this.conversationRecipient, ConversationUpdateItem.this.isMessageRequestAccepted);
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public MultiselectPart getMultiselectPartForLatestTouch() {
        return this.conversationMessage.getMultiselectCollection().asSingle().getSinglePart();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public int getTopBoundaryOfMultiselectPart(MultiselectPart multiselectPart) {
        return getTop();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    public int getBottomBoundaryOfMultiselectPart(MultiselectPart multiselectPart) {
        return getBottom();
    }

    private void observeDisplayBody(LifecycleOwner lifecycleOwner, LiveData<SpannableString> liveData) {
        LiveData<SpannableString> liveData2 = this.displayBody;
        if (liveData2 != liveData) {
            if (liveData2 != null) {
                liveData2.removeObserver(this.updateObserver);
            }
            this.displayBody = liveData;
            if (liveData != null) {
                liveData.observe(lifecycleOwner, this.updateObserver);
            }
        }
    }

    public void setBodyText(CharSequence charSequence) {
        if (charSequence == null) {
            this.body.setVisibility(4);
            return;
        }
        this.body.setText(charSequence);
        this.body.setVisibility(0);
    }

    public void present(ConversationMessage conversationMessage, Optional<MessageRecord> optional, Recipient recipient, boolean z) {
        int i;
        setSelected(!Sets.intersection(conversationMessage.getMultiselectCollection().toSet(), this.batchSelected).isEmpty());
        if (conversationMessage.getMessageRecord().isGroupV1MigrationEvent() && (!optional.isPresent() || !optional.get().isGroupV1MigrationEvent())) {
            this.actionButton.setText(R.string.ConversationUpdateItem_learn_more);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener(conversationMessage) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda5
                public final /* synthetic */ ConversationMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.m1465$r8$lambda$ZPgXlD7loYXK5s9xnujnN65D9Y(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isChatSessionRefresh() && (!optional.isPresent() || !optional.get().isChatSessionRefresh())) {
            this.actionButton.setText(R.string.ConversationUpdateItem_learn_more);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda8
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$TeADvbbdLQamPYxoWcYuBsQv0zQ(ConversationUpdateItem.this, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isIdentityUpdate()) {
            this.actionButton.setText(R.string.ConversationUpdateItem_learn_more);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener(conversationMessage) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda9
                public final /* synthetic */ ConversationMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$MAyMBn5_gkj8iNH4Tbos3z7tPt0(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isGroupCall()) {
            Collection<ServiceId> mentioned = MessageRecord.getGroupCallUpdateDescription(getContext(), conversationMessage.getMessageRecord().getBody(), true).getMentioned();
            if (Util.hasItems(mentioned)) {
                i = mentioned.contains(SignalStore.account().requireAci()) ? R.string.ConversationUpdateItem_return_to_call : GroupCallUpdateDetailsUtil.parse(conversationMessage.getMessageRecord().getBody()).getIsCallFull() ? R.string.ConversationUpdateItem_call_is_full : R.string.ConversationUpdateItem_join_call;
            } else {
                i = 0;
            }
            if (i == 0 || !recipient.isGroup() || !recipient.isActiveGroup()) {
                this.actionButton.setVisibility(8);
                this.actionButton.setOnClickListener(null);
                return;
            }
            this.actionButton.setText(i);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda10
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$wUGFtikrOcU0Vnt2Kl_PiIqGkHg(ConversationUpdateItem.this, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isSelfCreatedGroup()) {
            this.actionButton.setText(R.string.ConversationUpdateItem_invite_friends);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda11
                public final /* synthetic */ Recipient f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$c3b_6ibgbFsResgAyJakmW3F81o(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if ((conversationMessage.getMessageRecord().isMissedAudioCall() || conversationMessage.getMessageRecord().isMissedVideoCall()) && EnableCallNotificationSettingsDialog.shouldShow(getContext())) {
            this.actionButton.setVisibility(0);
            this.actionButton.setText(R.string.ConversationUpdateItem_enable_call_notifications);
            this.actionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda12
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$lh4J1ibx5gxspfsVKPxEPNvLjdw(ConversationUpdateItem.this, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isInMemoryMessageRecord() && ((InMemoryMessageRecord) conversationMessage.getMessageRecord()).showActionButton()) {
            InMemoryMessageRecord inMemoryMessageRecord = (InMemoryMessageRecord) conversationMessage.getMessageRecord();
            this.actionButton.setVisibility(0);
            this.actionButton.setText(inMemoryMessageRecord.getActionButtonText());
            this.actionButton.setOnClickListener(new View.OnClickListener(inMemoryMessageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda13
                public final /* synthetic */ InMemoryMessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.m1463$r8$lambda$XjwJEvdLghxyODskctpvH1Dfyw(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isGroupV2DescriptionUpdate()) {
            this.actionButton.setVisibility(0);
            this.actionButton.setText(R.string.ConversationUpdateItem_view);
            this.actionButton.setOnClickListener(new View.OnClickListener(recipient, conversationMessage, z) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda14
                public final /* synthetic */ Recipient f$1;
                public final /* synthetic */ ConversationMessage f$2;
                public final /* synthetic */ boolean f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$XxEUkgBm7mNEEUFooJZ3Qimq0gM(ConversationUpdateItem.this, this.f$1, this.f$2, this.f$3, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isBadDecryptType() && (!optional.isPresent() || !optional.get().isBadDecryptType())) {
            this.actionButton.setText(R.string.ConversationUpdateItem_learn_more);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener(conversationMessage) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda15
                public final /* synthetic */ ConversationMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$h7QyhHS9eCgXlJ1WCC1R9i0bFTo(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isChangeNumber() && conversationMessage.getMessageRecord().getIndividualRecipient().isSystemContact()) {
            this.actionButton.setText(R.string.ConversationUpdateItem_update_contact);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener(conversationMessage) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda16
                public final /* synthetic */ ConversationMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$oUbI_KaeusNGoy18j0JBHq8he9c(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if (shouldShowBlockRequestAction(conversationMessage.getMessageRecord())) {
            this.actionButton.setText(R.string.ConversationUpdateItem_block_request);
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener(conversationMessage) { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda6
                public final /* synthetic */ ConversationMessage f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.m1464$r8$lambda$YslIzJ4Mlk7_d73QJG3Cc8c14(ConversationUpdateItem.this, this.f$1, view);
                }
            });
        } else if (conversationMessage.getMessageRecord().isBoostRequest()) {
            this.actionButton.setVisibility(0);
            this.actionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda7
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ConversationUpdateItem.$r8$lambda$aGyY6I5Fhh9aqyNGXFPA_pgD1mk(ConversationUpdateItem.this, view);
                }
            });
            this.actionButton.setText(R.string.ConversationUpdateItem_donate);
        } else {
            this.actionButton.setVisibility(8);
            this.actionButton.setOnClickListener(null);
        }
    }

    public /* synthetic */ void lambda$present$2(ConversationMessage conversationMessage, View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onGroupMigrationLearnMoreClicked(conversationMessage.getMessageRecord().getGroupV1MigrationMembershipChanges());
        }
    }

    public /* synthetic */ void lambda$present$3(View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onChatSessionRefreshLearnMoreClicked();
        }
    }

    public /* synthetic */ void lambda$present$4(ConversationMessage conversationMessage, View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onSafetyNumberLearnMoreClicked(conversationMessage.getMessageRecord().getIndividualRecipient());
        }
    }

    public /* synthetic */ void lambda$present$5(View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onJoinGroupCallClicked();
        }
    }

    public /* synthetic */ void lambda$present$6(Recipient recipient, View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onInviteFriendsToGroupClicked(recipient.requireGroupId().requireV2());
        }
    }

    public /* synthetic */ void lambda$present$7(View view) {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onEnableCallNotificationsClicked();
        }
    }

    public /* synthetic */ void lambda$present$8(InMemoryMessageRecord inMemoryMessageRecord, View view) {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onInMemoryMessageClicked(inMemoryMessageRecord);
        }
    }

    public /* synthetic */ void lambda$present$9(Recipient recipient, ConversationMessage conversationMessage, boolean z, View view) {
        BindableConversationItem.EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onViewGroupDescriptionChange(recipient.getGroupId().orElse(null), conversationMessage.getMessageRecord().getGroupV2DescriptionUpdate(), z);
        }
    }

    public /* synthetic */ void lambda$present$10(ConversationMessage conversationMessage, View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onBadDecryptLearnMoreClicked(conversationMessage.getMessageRecord().getRecipient().getId());
        }
    }

    public /* synthetic */ void lambda$present$11(ConversationMessage conversationMessage, View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onChangeNumberUpdateContact(conversationMessage.getMessageRecord().getIndividualRecipient());
        }
    }

    public /* synthetic */ void lambda$present$12(ConversationMessage conversationMessage, View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onBlockJoinRequest(conversationMessage.getMessageRecord().getIndividualRecipient());
        }
    }

    public /* synthetic */ void lambda$present$13(View view) {
        BindableConversationItem.EventListener eventListener;
        if (this.batchSelected.isEmpty() && (eventListener = this.eventListener) != null) {
            eventListener.onDonateClicked();
        }
    }

    private boolean shouldShowBlockRequestAction(MessageRecord messageRecord) {
        Recipient individualRecipient = messageRecord.getIndividualRecipient();
        if (!individualRecipient.hasServiceId() || !this.groupData.isSelfAdmin() || this.groupData.isBanned(individualRecipient) || this.groupData.isFullMember(individualRecipient)) {
            return false;
        }
        if ((!messageRecord.isCollapsedGroupV2JoinUpdate() || ((Boolean) this.nextMessageRecord.map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationUpdateItem.m1466$r8$lambda$m2fYojFYcYy7hkuX5KmCDwIVu8(Recipient.this, (MessageRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Boolean.FALSE)).booleanValue()) && (!messageRecord.isGroupV2JoinRequest(individualRecipient.requireServiceId()) || !((Boolean) this.previousMessageRecord.map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationUpdateItem.$r8$lambda$x7U4BngdZ6orSxVXnOUHKM93IKs(Recipient.this, (MessageRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Boolean.FALSE)).booleanValue())) {
            return false;
        }
        return true;
    }

    public static /* synthetic */ Boolean lambda$shouldShowBlockRequestAction$14(Recipient recipient, MessageRecord messageRecord) {
        return Boolean.valueOf(messageRecord.isGroupV2JoinRequest(recipient.requireServiceId()));
    }

    public static /* synthetic */ Boolean lambda$shouldShowBlockRequestAction$15(Recipient recipient, MessageRecord messageRecord) {
        return Boolean.valueOf(messageRecord.isCollapsedGroupV2JoinUpdate(recipient.requireServiceId()));
    }

    private void presentBackground(boolean z, boolean z2, boolean z3) {
        int dimensionPixelOffset = getContext().getResources().getDimensionPixelOffset(R.dimen.conversation_update_vertical_margin);
        int dimensionPixelOffset2 = getContext().getResources().getDimensionPixelOffset(R.dimen.conversation_update_vertical_padding);
        int dimensionPixelOffset3 = getContext().getResources().getDimensionPixelOffset(R.dimen.conversation_update_vertical_padding_collapsed);
        if (z && z2) {
            ViewUtil.setTopMargin(this.background, 0);
            ViewUtil.setBottomMargin(this.background, 0);
            ViewUtil.setPaddingTop(this.background, dimensionPixelOffset3);
            ViewUtil.setPaddingBottom(this.background, dimensionPixelOffset3);
            ViewUtil.updateLayoutParams(this.background, -1, -2);
            if (z3) {
                this.background.setBackgroundResource(R.drawable.conversation_update_wallpaper_background_middle);
            } else {
                this.background.setBackground(null);
            }
        } else if (z) {
            ViewUtil.setTopMargin(this.background, 0);
            ViewUtil.setBottomMargin(this.background, dimensionPixelOffset);
            ViewUtil.setPaddingTop(this.background, dimensionPixelOffset2);
            ViewUtil.setPaddingBottom(this.background, dimensionPixelOffset2);
            ViewUtil.updateLayoutParams(this.background, -1, -2);
            if (z3) {
                this.background.setBackgroundResource(R.drawable.conversation_update_wallpaper_background_bottom);
            } else {
                this.background.setBackground(null);
            }
        } else if (z2) {
            ViewUtil.setTopMargin(this.background, dimensionPixelOffset);
            ViewUtil.setBottomMargin(this.background, 0);
            ViewUtil.setPaddingTop(this.background, dimensionPixelOffset2);
            ViewUtil.setPaddingBottom(this.background, dimensionPixelOffset3);
            ViewUtil.updateLayoutParams(this.background, -1, -2);
            if (z3) {
                this.background.setBackgroundResource(R.drawable.conversation_update_wallpaper_background_top);
            } else {
                this.background.setBackground(null);
            }
        } else {
            ViewUtil.setTopMargin(this.background, dimensionPixelOffset);
            ViewUtil.setBottomMargin(this.background, dimensionPixelOffset);
            ViewUtil.setPaddingTop(this.background, dimensionPixelOffset2);
            ViewUtil.setPaddingBottom(this.background, dimensionPixelOffset2);
            ViewUtil.updateLayoutParams(this.background, -2, -2);
            if (z3) {
                this.background.setBackgroundResource(R.drawable.conversation_update_wallpaper_background_singular);
            } else {
                this.background.setBackground(null);
            }
        }
    }

    private void presentActionButton(boolean z, boolean z2) {
        if (z2) {
            this.actionButton.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.signal_colorSecondaryContainer)));
            this.actionButton.setTextColor(ColorStateList.valueOf(ContextCompat.getColor(getContext(), R.color.signal_colorOnSecondaryContainer)));
        } else if (z) {
            this.actionButton.setBackgroundTintList(AppCompatResources.getColorStateList(getContext(), R.color.conversation_update_item_button_background_wallpaper));
            this.actionButton.setTextColor(AppCompatResources.getColorStateList(getContext(), R.color.conversation_update_item_button_text_color_wallpaper));
        } else {
            this.actionButton.setBackgroundTintList(AppCompatResources.getColorStateList(getContext(), R.color.conversation_update_item_button_background_normal));
            this.actionButton.setTextColor(AppCompatResources.getColorStateList(getContext(), R.color.conversation_update_item_button_text_color_normal));
        }
    }

    private static boolean isSameType(MessageRecord messageRecord, MessageRecord messageRecord2) {
        return (messageRecord.isGroupUpdate() && messageRecord2.isGroupUpdate()) || (messageRecord.isProfileChange() && messageRecord2.isProfileChange()) || ((messageRecord.isGroupCall() && messageRecord2.isGroupCall()) || ((messageRecord.isExpirationTimerUpdate() && messageRecord2.isExpirationTimerUpdate()) || (messageRecord.isChangeNumber() && messageRecord2.isChangeNumber())));
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(new InternalClickListener(onClickListener));
    }

    /* loaded from: classes4.dex */
    private final class PresentOnChange implements Observer<Recipient> {
        private PresentOnChange() {
            ConversationUpdateItem.this = r1;
        }

        public void onChanged(Recipient recipient) {
            if (recipient.getId() != ConversationUpdateItem.this.conversationRecipient.getId()) {
                return;
            }
            if (ConversationUpdateItem.this.conversationRecipient == null || !ConversationUpdateItem.this.conversationRecipient.hasSameContent(recipient)) {
                ConversationUpdateItem.this.conversationRecipient = recipient;
                ConversationUpdateItem conversationUpdateItem = ConversationUpdateItem.this;
                conversationUpdateItem.present(conversationUpdateItem.conversationMessage, ConversationUpdateItem.this.nextMessageRecord, ConversationUpdateItem.this.conversationRecipient, ConversationUpdateItem.this.isMessageRequestAccepted);
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class UpdateObserver implements Observer<Spannable> {
        private UpdateObserver() {
            ConversationUpdateItem.this = r1;
        }

        public void onChanged(Spannable spannable) {
            ConversationUpdateItem.this.setBodyText(spannable);
        }
    }

    /* loaded from: classes4.dex */
    public class InternalClickListener implements View.OnClickListener {
        private final View.OnClickListener parent;

        InternalClickListener(View.OnClickListener onClickListener) {
            ConversationUpdateItem.this = r1;
            this.parent = onClickListener;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if ((ConversationUpdateItem.this.messageRecord.isIdentityUpdate() || ConversationUpdateItem.this.messageRecord.isIdentityDefault() || ConversationUpdateItem.this.messageRecord.isIdentityVerified()) && ConversationUpdateItem.this.batchSelected.isEmpty()) {
                IdentityUtil.getRemoteIdentityKey(ConversationUpdateItem.this.getContext(), ConversationUpdateItem.this.senderObserver.getObservedRecipient()).addListener(new ListenableFuture.Listener<Optional<IdentityRecord>>() { // from class: org.thoughtcrime.securesms.conversation.ConversationUpdateItem.InternalClickListener.1
                    public void onSuccess(Optional<IdentityRecord> optional) {
                        if (optional.isPresent()) {
                            ConversationUpdateItem.this.getContext().startActivity(VerifyIdentityActivity.newIntent(ConversationUpdateItem.this.getContext(), optional.get()));
                        }
                    }

                    @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
                    public void onFailure(ExecutionException executionException) {
                        Log.w(ConversationUpdateItem.TAG, executionException);
                    }
                });
                return;
            }
            View.OnClickListener onClickListener = this.parent;
            if (onClickListener != null) {
                onClickListener.onClick(view);
            }
        }
    }
}
