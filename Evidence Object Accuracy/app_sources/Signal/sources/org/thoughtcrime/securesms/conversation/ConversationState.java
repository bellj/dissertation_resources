package org.thoughtcrime.securesms.conversation;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ConversationState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\b\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0019\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000b\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u00032\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\u000e\u0010\u0013\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003J\u000e\u0010\u0014\u001a\u00020\u00002\u0006\u0010\u0004\u001a\u00020\u0005R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationState;", "", "isMmsEnabled", "", "securityInfo", "Lorg/thoughtcrime/securesms/conversation/ConversationSecurityInfo;", "(ZLorg/thoughtcrime/securesms/conversation/ConversationSecurityInfo;)V", "()Z", "getSecurityInfo", "()Lorg/thoughtcrime/securesms/conversation/ConversationSecurityInfo;", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "withMmsEnabled", "withSecurityInfo", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationState {
    public static final Companion Companion = new Companion(null);
    private final boolean isMmsEnabled;
    private final ConversationSecurityInfo securityInfo;

    public ConversationState() {
        this(false, null, 3, null);
    }

    public static /* synthetic */ ConversationState copy$default(ConversationState conversationState, boolean z, ConversationSecurityInfo conversationSecurityInfo, int i, Object obj) {
        if ((i & 1) != 0) {
            z = conversationState.isMmsEnabled;
        }
        if ((i & 2) != 0) {
            conversationSecurityInfo = conversationState.securityInfo;
        }
        return conversationState.copy(z, conversationSecurityInfo);
    }

    @JvmStatic
    public static final ConversationState create() {
        return Companion.create();
    }

    public final boolean component1() {
        return this.isMmsEnabled;
    }

    public final ConversationSecurityInfo component2() {
        return this.securityInfo;
    }

    public final ConversationState copy(boolean z, ConversationSecurityInfo conversationSecurityInfo) {
        Intrinsics.checkNotNullParameter(conversationSecurityInfo, "securityInfo");
        return new ConversationState(z, conversationSecurityInfo);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConversationState)) {
            return false;
        }
        ConversationState conversationState = (ConversationState) obj;
        return this.isMmsEnabled == conversationState.isMmsEnabled && Intrinsics.areEqual(this.securityInfo, conversationState.securityInfo);
    }

    public int hashCode() {
        boolean z = this.isMmsEnabled;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return (i * 31) + this.securityInfo.hashCode();
    }

    public String toString() {
        return "ConversationState(isMmsEnabled=" + this.isMmsEnabled + ", securityInfo=" + this.securityInfo + ')';
    }

    public ConversationState(boolean z, ConversationSecurityInfo conversationSecurityInfo) {
        Intrinsics.checkNotNullParameter(conversationSecurityInfo, "securityInfo");
        this.isMmsEnabled = z;
        this.securityInfo = conversationSecurityInfo;
    }

    public final boolean isMmsEnabled() {
        return this.isMmsEnabled;
    }

    public /* synthetic */ ConversationState(boolean z, ConversationSecurityInfo conversationSecurityInfo, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? true : z, (i & 2) != 0 ? new ConversationSecurityInfo(null, false, false, false, 15, null) : conversationSecurityInfo);
    }

    public final ConversationSecurityInfo getSecurityInfo() {
        return this.securityInfo;
    }

    /* compiled from: ConversationState.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationState$Companion;", "", "()V", "create", "Lorg/thoughtcrime/securesms/conversation/ConversationState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final ConversationState create() {
            return new ConversationState(false, null, 3, null);
        }
    }

    public final ConversationState withMmsEnabled(boolean z) {
        return copy$default(this, z, null, 2, null);
    }

    public final ConversationState withSecurityInfo(ConversationSecurityInfo conversationSecurityInfo) {
        Intrinsics.checkNotNullParameter(conversationSecurityInfo, "securityInfo");
        return copy$default(this, false, conversationSecurityInfo, 1, null);
    }
}
