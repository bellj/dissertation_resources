package org.thoughtcrime.securesms.conversation.colors;

import android.content.Context;
import androidx.core.content.ContextCompat;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: Colorizer.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010$\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0007H\u0003J\u0018\u0010\u000e\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0004H\u0007J\u0018\u0010\u0010\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0004H\u0007J\u0018\u0010\u0011\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u0004H\u0007J\u0018\u0010\u0012\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\u0016\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u001a\u0010\u0018\u001a\u00020\u00192\u0012\u0010\u001a\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u001bR\u000e\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/Colorizer;", "", "()V", "colorsHaveBeenSet", "", "groupSenderColors", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "getDefaultColor", "", "context", "Landroid/content/Context;", "recipientId", "getIncomingBodyTextColor", "hasWallpaper", "getIncomingFooterIconColor", "getIncomingFooterTextColor", "getIncomingGroupSenderColor", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getOutgoingBodyTextColor", "getOutgoingFooterIconColor", "getOutgoingFooterTextColor", "onNameColorsChanged", "", "nameColorMap", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Colorizer {
    private boolean colorsHaveBeenSet;
    private final Map<RecipientId, NameColor> groupSenderColors = new LinkedHashMap();

    public final int getOutgoingBodyTextColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, R.color.conversation_outgoing_body_color);
    }

    public final int getOutgoingFooterTextColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, R.color.conversation_outgoing_footer_color);
    }

    public final int getOutgoingFooterIconColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, R.color.conversation_outgoing_footer_color);
    }

    public final int getIncomingBodyTextColor(Context context, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (z) {
            return ContextCompat.getColor(context, R.color.signal_colorNeutralInverse);
        }
        return ContextCompat.getColor(context, R.color.signal_colorOnSurface);
    }

    public final int getIncomingFooterTextColor(Context context, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (z) {
            return ContextCompat.getColor(context, R.color.signal_colorNeutralVariantInverse);
        }
        return ContextCompat.getColor(context, R.color.signal_colorOnSurfaceVariant);
    }

    public final int getIncomingFooterIconColor(Context context, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (z) {
            return ContextCompat.getColor(context, R.color.signal_colorNeutralVariantInverse);
        }
        return ContextCompat.getColor(context, R.color.signal_colorOnSurfaceVariant);
    }

    public final int getIncomingGroupSenderColor(Context context, Recipient recipient) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        NameColor nameColor = this.groupSenderColors.get(recipient.getId());
        if (nameColor != null) {
            return nameColor.getColor(context);
        }
        RecipientId id = recipient.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        return getDefaultColor(context, id);
    }

    public final void onNameColorsChanged(Map<RecipientId, NameColor> map) {
        Intrinsics.checkNotNullParameter(map, "nameColorMap");
        this.groupSenderColors.clear();
        this.groupSenderColors.putAll(map);
        this.colorsHaveBeenSet = true;
    }

    private final int getDefaultColor(Context context, RecipientId recipientId) {
        if (!this.colorsHaveBeenSet) {
            return 0;
        }
        NameColor nameColor = ChatColorsPalette.Names.getAll().get(this.groupSenderColors.size() % ChatColorsPalette.Names.getAll().size());
        this.groupSenderColors.put(recipientId, nameColor);
        return nameColor.getColor(context);
    }
}
