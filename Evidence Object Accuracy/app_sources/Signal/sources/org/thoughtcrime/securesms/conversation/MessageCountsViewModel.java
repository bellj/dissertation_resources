package org.thoughtcrime.securesms.conversation;

import android.app.Application;
import android.content.Context;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;

/* loaded from: classes4.dex */
public class MessageCountsViewModel extends ViewModel {
    private static final Executor EXECUTOR = new SerialMonoLifoExecutor(SignalExecutors.BOUNDED);
    private final Application context = ApplicationDependencies.getApplication();
    private DatabaseObserver.Observer observer;
    private final MutableLiveData<Long> threadId;
    private final LiveData<Pair<Integer, Integer>> unreadCounts;

    public MessageCountsViewModel() {
        MutableLiveData<Long> mutableLiveData = new MutableLiveData<>(-1L);
        this.threadId = mutableLiveData;
        this.unreadCounts = Transformations.switchMap(Transformations.distinctUntilChanged(mutableLiveData), new Function() { // from class: org.thoughtcrime.securesms.conversation.MessageCountsViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return MessageCountsViewModel.this.lambda$new$0((Long) obj);
            }
        });
    }

    public /* synthetic */ LiveData lambda$new$0(final Long l) {
        final MutableLiveData mutableLiveData = new MutableLiveData(new Pair(0, 0));
        if (l.longValue() == -1) {
            return mutableLiveData;
        }
        if (this.observer != null) {
            ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
        }
        AnonymousClass1 r1 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.conversation.MessageCountsViewModel.1
            private int previousUnreadCount = -1;

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public void onChanged() {
                MessageCountsViewModel.EXECUTOR.execute(new MessageCountsViewModel$1$$ExternalSyntheticLambda0(this, l, mutableLiveData));
            }

            public /* synthetic */ void lambda$onChanged$0(Long l2, MutableLiveData mutableLiveData2) {
                MessageCountsViewModel messageCountsViewModel = MessageCountsViewModel.this;
                int unreadCount = messageCountsViewModel.getUnreadCount(messageCountsViewModel.context, l2.longValue());
                if (unreadCount != this.previousUnreadCount) {
                    this.previousUnreadCount = unreadCount;
                    Integer valueOf = Integer.valueOf(unreadCount);
                    MessageCountsViewModel messageCountsViewModel2 = MessageCountsViewModel.this;
                    mutableLiveData2.postValue(new Pair(valueOf, Integer.valueOf(messageCountsViewModel2.getUnreadMentionsCount(messageCountsViewModel2.context, l2.longValue()))));
                }
            }
        };
        this.observer = r1;
        r1.onChanged();
        ApplicationDependencies.getDatabaseObserver().registerConversationListObserver(this.observer);
        return mutableLiveData;
    }

    public void setThreadId(long j) {
        this.threadId.setValue(Long.valueOf(j));
    }

    public void clearThreadId() {
        this.threadId.postValue(-1L);
    }

    public LiveData<Integer> getUnreadMessagesCount() {
        return Transformations.map(this.unreadCounts, new Function() { // from class: org.thoughtcrime.securesms.conversation.MessageCountsViewModel$$ExternalSyntheticLambda1
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return (Integer) ((Pair) obj).first();
            }
        });
    }

    public LiveData<Integer> getUnreadMentionsCount() {
        return Transformations.map(this.unreadCounts, new Function() { // from class: org.thoughtcrime.securesms.conversation.MessageCountsViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return (Integer) ((Pair) obj).second();
            }
        });
    }

    public int getUnreadCount(Context context, long j) {
        ThreadRecord threadRecord = SignalDatabase.threads().getThreadRecord(Long.valueOf(j));
        if (threadRecord != null) {
            return threadRecord.getUnreadCount();
        }
        return 0;
    }

    public int getUnreadMentionsCount(Context context, long j) {
        return SignalDatabase.mms().getUnreadMentionCount(j);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        if (this.observer != null) {
            ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
        }
    }
}
