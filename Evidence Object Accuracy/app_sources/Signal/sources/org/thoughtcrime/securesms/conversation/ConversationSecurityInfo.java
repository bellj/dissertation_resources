package org.thoughtcrime.securesms.conversation;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ConversationSecurityInfo.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0005¢\u0006\u0002\u0010\bJ\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J1\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00052\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\tR\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationSecurityInfo;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "isPushAvailable", "", "isDefaultSmsApplication", "isInitialized", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;ZZZ)V", "()Z", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationSecurityInfo {
    private final boolean isDefaultSmsApplication;
    private final boolean isInitialized;
    private final boolean isPushAvailable;
    private final RecipientId recipientId;

    public ConversationSecurityInfo() {
        this(null, false, false, false, 15, null);
    }

    public static /* synthetic */ ConversationSecurityInfo copy$default(ConversationSecurityInfo conversationSecurityInfo, RecipientId recipientId, boolean z, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = conversationSecurityInfo.recipientId;
        }
        if ((i & 2) != 0) {
            z = conversationSecurityInfo.isPushAvailable;
        }
        if ((i & 4) != 0) {
            z2 = conversationSecurityInfo.isDefaultSmsApplication;
        }
        if ((i & 8) != 0) {
            z3 = conversationSecurityInfo.isInitialized;
        }
        return conversationSecurityInfo.copy(recipientId, z, z2, z3);
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final boolean component2() {
        return this.isPushAvailable;
    }

    public final boolean component3() {
        return this.isDefaultSmsApplication;
    }

    public final boolean component4() {
        return this.isInitialized;
    }

    public final ConversationSecurityInfo copy(RecipientId recipientId, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        return new ConversationSecurityInfo(recipientId, z, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConversationSecurityInfo)) {
            return false;
        }
        ConversationSecurityInfo conversationSecurityInfo = (ConversationSecurityInfo) obj;
        return Intrinsics.areEqual(this.recipientId, conversationSecurityInfo.recipientId) && this.isPushAvailable == conversationSecurityInfo.isPushAvailable && this.isDefaultSmsApplication == conversationSecurityInfo.isDefaultSmsApplication && this.isInitialized == conversationSecurityInfo.isInitialized;
    }

    public int hashCode() {
        int hashCode = this.recipientId.hashCode() * 31;
        boolean z = this.isPushAvailable;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.isDefaultSmsApplication;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isInitialized;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i9 + i;
    }

    public String toString() {
        return "ConversationSecurityInfo(recipientId=" + this.recipientId + ", isPushAvailable=" + this.isPushAvailable + ", isDefaultSmsApplication=" + this.isDefaultSmsApplication + ", isInitialized=" + this.isInitialized + ')';
    }

    public ConversationSecurityInfo(RecipientId recipientId, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.recipientId = recipientId;
        this.isPushAvailable = z;
        this.isDefaultSmsApplication = z2;
        this.isInitialized = z3;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ConversationSecurityInfo(org.thoughtcrime.securesms.recipients.RecipientId r2, boolean r3, boolean r4, boolean r5, int r6, kotlin.jvm.internal.DefaultConstructorMarker r7) {
        /*
            r1 = this;
            r7 = r6 & 1
            if (r7 == 0) goto L_0x000b
            org.thoughtcrime.securesms.recipients.RecipientId r2 = org.thoughtcrime.securesms.recipients.RecipientId.UNKNOWN
            java.lang.String r7 = "UNKNOWN"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r7)
        L_0x000b:
            r7 = r6 & 2
            r0 = 0
            if (r7 == 0) goto L_0x0011
            r3 = 0
        L_0x0011:
            r7 = r6 & 4
            if (r7 == 0) goto L_0x0016
            r4 = 0
        L_0x0016:
            r6 = r6 & 8
            if (r6 == 0) goto L_0x001b
            r5 = 0
        L_0x001b:
            r1.<init>(r2, r3, r4, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationSecurityInfo.<init>(org.thoughtcrime.securesms.recipients.RecipientId, boolean, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final boolean isPushAvailable() {
        return this.isPushAvailable;
    }

    public final boolean isDefaultSmsApplication() {
        return this.isDefaultSmsApplication;
    }

    public final boolean isInitialized() {
        return this.isInitialized;
    }
}
