package org.thoughtcrime.securesms.conversation;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;

/* compiled from: AttachmentButtonCenterHelper.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\b\u001a\u00020\tH\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/AttachmentButtonCenterHelper;", "Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "(Landroidx/recyclerview/widget/RecyclerView;)V", "defaultPadding", "", "itemWidth", "onChanged", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AttachmentButtonCenterHelper extends RecyclerView.AdapterDataObserver {
    private final float defaultPadding;
    private final float itemWidth;
    private final RecyclerView recyclerView;

    public AttachmentButtonCenterHelper(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        this.recyclerView = recyclerView;
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        this.itemWidth = dimensionUnit.toPixels(88.0f);
        this.defaultPadding = dimensionUnit.toPixels(16.0f);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
    public void onChanged() {
        RecyclerView.Adapter adapter = this.recyclerView.getAdapter();
        if (adapter != null) {
            this.recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener(this.itemWidth * ((float) adapter.getItemCount()), this) { // from class: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1
                final /* synthetic */ float $requiredSpace$inlined;
                final /* synthetic */ AttachmentButtonCenterHelper this$0;

                {
                    this.$requiredSpace$inlined = r1;
                    this.this$0 = r2;
                }

                /*  JADX ERROR: Method code generation error
                    jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0023: INVOKE  
                      (r1v0 'view' android.view.View)
                      (wrap: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1 : 0x0020: CONSTRUCTOR  (r3v5 org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1 A[REMOVE]) = 
                      (r1v0 'view' android.view.View)
                      (wrap: float : 0x001d: ARITH  (r2v11 float A[REMOVE]) = (wrap: float : 0x001a: ARITH  (r2v10 float A[REMOVE]) = (wrap: float : 0x0017: CAST (r2v9 float A[REMOVE]) = (float) (wrap: int : 0x0013: INVOKE  (r2v8 int A[REMOVE]) = (r1v0 'view' android.view.View) type: VIRTUAL call: android.view.View.getMeasuredWidth():int)) - (wrap: float : 0x0018: IGET  (r3v3 float A[REMOVE]) = 
                      (r0v0 'this' org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                     org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1.$requiredSpace$inlined float)) / (2.0f float))
                     call: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1.<init>(android.view.View, float):void type: CONSTRUCTOR)
                     type: VIRTUAL call: android.view.View.post(java.lang.Runnable):boolean in method: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                    	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                    	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                    	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                    	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.dex.regions.Region.generate(Region.java:35)
                    	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                    	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                    	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                    	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                    	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                    Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0020: CONSTRUCTOR  (r3v5 org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1 A[REMOVE]) = 
                      (r1v0 'view' android.view.View)
                      (wrap: float : 0x001d: ARITH  (r2v11 float A[REMOVE]) = (wrap: float : 0x001a: ARITH  (r2v10 float A[REMOVE]) = (wrap: float : 0x0017: CAST (r2v9 float A[REMOVE]) = (float) (wrap: int : 0x0013: INVOKE  (r2v8 int A[REMOVE]) = (r1v0 'view' android.view.View) type: VIRTUAL call: android.view.View.getMeasuredWidth():int)) - (wrap: float : 0x0018: IGET  (r3v3 float A[REMOVE]) = 
                      (r0v0 'this' org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1 A[IMMUTABLE_TYPE, THIS])
                     org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1.$requiredSpace$inlined float)) / (2.0f float))
                     call: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1.<init>(android.view.View, float):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void, file: classes4.dex
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                    	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                    	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                    	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                    	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                    	... 16 more
                    Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1, state: NOT_LOADED
                    	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                    	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                    	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                    	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                    	... 22 more
                    */
                @Override // android.view.View.OnLayoutChangeListener
                public void onLayoutChange(android.view.View r1, int r2, int r3, int r4, int r5, int r6, int r7, int r8, int r9) {
                    /*
                        r0 = this;
                        java.lang.String r2 = "view"
                        kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r1, r2)
                        r1.removeOnLayoutChangeListener(r0)
                        int r2 = r1.getMeasuredWidth()
                        float r2 = (float) r2
                        float r3 = r0.$requiredSpace$inlined
                        int r2 = (r2 > r3 ? 1 : (r2 == r3 ? 0 : -1))
                        if (r2 < 0) goto L_0x0027
                        int r2 = r1.getMeasuredWidth()
                        float r2 = (float) r2
                        float r3 = r0.$requiredSpace$inlined
                        float r2 = r2 - r3
                        r3 = 1073741824(0x40000000, float:2.0)
                        float r2 = r2 / r3
                        org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1 r3 = new org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$1$1
                        r3.<init>(r1, r2)
                        r1.post(r3)
                        goto L_0x0040
                    L_0x0027:
                        org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper r2 = r0.this$0
                        float r2 = org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper.access$getDefaultPadding$p(r2)
                        int r2 = (int) r2
                        int r3 = r1.getPaddingTop()
                        org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper r4 = r0.this$0
                        float r4 = org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper.access$getDefaultPadding$p(r4)
                        int r4 = (int) r4
                        int r5 = r1.getPaddingBottom()
                        r1.setPadding(r2, r3, r4, r5)
                    L_0x0040:
                        return
                    */
                    throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.AttachmentButtonCenterHelper$onChanged$$inlined$doOnNextLayout$1.onLayoutChange(android.view.View, int, int, int, int, int, int, int, int):void");
                }
            });
        }
    }
}
