package org.thoughtcrime.securesms.conversation;

import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$QuickCameraToggleListener$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ ConversationParentFragment.QuickCameraToggleListener f$0;

    public /* synthetic */ ConversationParentFragment$QuickCameraToggleListener$$ExternalSyntheticLambda1(ConversationParentFragment.QuickCameraToggleListener quickCameraToggleListener) {
        this.f$0 = quickCameraToggleListener;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onClick$1();
    }
}
