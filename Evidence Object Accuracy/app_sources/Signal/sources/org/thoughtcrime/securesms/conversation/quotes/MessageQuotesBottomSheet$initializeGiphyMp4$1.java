package org.thoughtcrime.securesms.conversation.quotes;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Lambda;

/* compiled from: MessageQuotesBottomSheet.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageQuotesBottomSheet$initializeGiphyMp4$1 extends Lambda implements Function1<Float, Unit> {
    public static final MessageQuotesBottomSheet$initializeGiphyMp4$1 INSTANCE = new MessageQuotesBottomSheet$initializeGiphyMp4$1();

    MessageQuotesBottomSheet$initializeGiphyMp4$1() {
        super(1);
    }

    public final void invoke(float f) {
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Float f) {
        invoke(f.floatValue());
        return Unit.INSTANCE;
    }
}
