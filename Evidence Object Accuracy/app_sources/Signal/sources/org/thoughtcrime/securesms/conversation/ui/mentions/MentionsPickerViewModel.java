package org.thoughtcrime.securesms.conversation.ui.mentions;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import j$.util.function.Function;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerRepository;
import org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class MentionsPickerViewModel extends ViewModel {
    private final MutableLiveData<Boolean> isShowing = new MutableLiveData<>(Boolean.FALSE);
    private final MutableLiveData<Query> liveQuery;
    private final MutableLiveData<LiveRecipient> liveRecipient;
    private final LiveData<List<MappingModel<?>>> mentionList;
    private final SingleLiveEvent<Recipient> selectedRecipient = new SingleLiveEvent<>();

    MentionsPickerViewModel(MentionsPickerRepository mentionsPickerRepository) {
        MutableLiveData<LiveRecipient> mutableLiveData = new MutableLiveData<>();
        this.liveRecipient = mutableLiveData;
        MutableLiveData<Query> mutableLiveData2 = new MutableLiveData<>();
        this.liveQuery = mutableLiveData2;
        LiveData switchMap = Transformations.switchMap(mutableLiveData, new WebRtcCallViewModel$$ExternalSyntheticLambda5());
        Objects.requireNonNull(mentionsPickerRepository);
        this.mentionList = LiveDataUtil.mapAsync(LiveDataUtil.combineLatest(mutableLiveData2, Transformations.distinctUntilChanged(LiveDataUtil.mapAsync(switchMap, new Function() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MentionsPickerRepository.this.getMembers((Recipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        })), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return MentionsPickerViewModel.lambda$new$0((MentionsPickerViewModel.Query) obj, (List) obj2);
            }
        }), new Function() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return MentionsPickerViewModel.lambda$new$1(MentionsPickerRepository.this, (MentionsPickerRepository.MentionQuery) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public static /* synthetic */ MentionsPickerRepository.MentionQuery lambda$new$0(Query query, List list) {
        return new MentionsPickerRepository.MentionQuery(query.query, list);
    }

    public static /* synthetic */ List lambda$new$1(MentionsPickerRepository mentionsPickerRepository, MentionsPickerRepository.MentionQuery mentionQuery) {
        return Stream.of(mentionsPickerRepository.search(mentionQuery)).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return new MentionViewState((Recipient) obj);
            }
        }).toList();
    }

    public LiveData<List<MappingModel<?>>> getMentionList() {
        return this.mentionList;
    }

    public void onSelectionChange(Recipient recipient) {
        this.selectedRecipient.setValue(recipient);
    }

    public void setIsShowing(boolean z) {
        if (!Objects.equals(this.isShowing.getValue(), Boolean.valueOf(z))) {
            this.isShowing.setValue(Boolean.valueOf(z));
        }
    }

    public LiveData<Recipient> getSelectedRecipient() {
        return this.selectedRecipient;
    }

    public LiveData<Boolean> isShowing() {
        return this.isShowing;
    }

    public void onQueryChange(String str) {
        this.liveQuery.setValue(str == null ? Query.NONE : new Query(str));
    }

    public void onRecipientChange(Recipient recipient) {
        this.liveRecipient.setValue(recipient.live());
    }

    /* loaded from: classes4.dex */
    public static class Query {
        static final Query NONE = new Query(null);
        private final String query;

        Query(String str) {
            this.query = str;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            return Objects.equals(this.query, ((Query) obj).query);
        }

        public int hashCode() {
            return Objects.hash(this.query);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new MentionsPickerViewModel(new MentionsPickerRepository()));
        }
    }
}
