package org.thoughtcrime.securesms.conversation.ui.error;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.util.DeviceProperties;

/* loaded from: classes4.dex */
public final class EnableCallNotificationSettingsDialog extends DialogFragment {
    private static final int BACKGROUND_RESTRICTED;
    private static final int CALL_CHANNEL_INVALID;
    private static final int CALL_NOTIFICATIONS_DISABLED;
    private static final String FRAGMENT_TAG;
    private static final int NOTIFICATIONS_DISABLED;
    private static final String TAG = Log.tag(EnableCallNotificationSettingsDialog.class);
    private View view;

    public static boolean shouldShow(Context context) {
        return getCallNotificationSettingsBitmask(context) != 0;
    }

    public static void fixAutomatically(Context context) {
        if (areCallNotificationsDisabled(context)) {
            SignalStore.settings().setCallNotificationsEnabled(true);
            Toast.makeText(context, (int) R.string.EnableCallNotificationSettingsDialog__call_notifications_enabled, 0).show();
        }
    }

    public static void show(FragmentManager fragmentManager) {
        if (fragmentManager.findFragmentByTag(FRAGMENT_TAG) != null) {
            Log.i(TAG, "Dialog already being shown");
        } else {
            new EnableCallNotificationSettingsDialog().show(fragmentManager, FRAGMENT_TAG);
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        Runnable runnable;
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Signal_MaterialAlertDialog);
        int callNotificationSettingsBitmask = getCallNotificationSettingsBitmask(requireContext());
        if (callNotificationSettingsBitmask == 2) {
            materialAlertDialogBuilder.setTitle(R.string.EnableCallNotificationSettingsDialog__enable_call_notifications).setMessage(R.string.EnableCallNotificationSettingsDialog__to_receive_call_notifications_tap_settings_and_turn_on_show_notifications).setPositiveButton(R.string.EnableCallNotificationSettingsDialog__settings, (DialogInterface.OnClickListener) null);
            runnable = new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    EnableCallNotificationSettingsDialog.this.showNotificationSettings();
                }
            };
        } else if (callNotificationSettingsBitmask == 16) {
            materialAlertDialogBuilder.setTitle(R.string.EnableCallNotificationSettingsDialog__enable_call_notifications).setMessage(R.string.EnableCallNotificationSettingsDialog__to_receive_call_notifications_tap_settings_and_turn_on_notifications).setPositiveButton(R.string.EnableCallNotificationSettingsDialog__settings, (DialogInterface.OnClickListener) null);
            runnable = new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    EnableCallNotificationSettingsDialog.this.showNotificationChannelSettings();
                }
            };
        } else if (callNotificationSettingsBitmask != 256) {
            materialAlertDialogBuilder.setTitle(R.string.EnableCallNotificationSettingsDialog__enable_call_notifications).setView(createView()).setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
            runnable = null;
        } else {
            materialAlertDialogBuilder.setTitle(R.string.EnableCallNotificationSettingsDialog__enable_background_activity).setMessage(R.string.EnableCallNotificationSettingsDialog__to_receive_call_notifications_tap_settings_and_enable_background_activity_in_battery_settings).setPositiveButton(R.string.EnableCallNotificationSettingsDialog__settings, (DialogInterface.OnClickListener) null);
            runnable = new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda5
                @Override // java.lang.Runnable
                public final void run() {
                    EnableCallNotificationSettingsDialog.this.showAppSettings();
                }
            };
        }
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        AlertDialog create = materialAlertDialogBuilder.create();
        if (runnable != null) {
            create.setOnShowListener(new DialogInterface.OnShowListener(runnable) { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda6
                public final /* synthetic */ Runnable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnShowListener
                public final void onShow(DialogInterface dialogInterface) {
                    EnableCallNotificationSettingsDialog.lambda$onCreateDialog$1(AlertDialog.this, this.f$1, dialogInterface);
                }
            });
        }
        return create;
    }

    public static /* synthetic */ void lambda$onCreateDialog$1(AlertDialog alertDialog, Runnable runnable, DialogInterface dialogInterface) {
        alertDialog.getButton(-1).setOnClickListener(new View.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda7
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                this.f$0.run();
            }
        });
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (getCallNotificationSettingsBitmask(requireContext()) == 0) {
            dismissAllowingStateLoss();
            return;
        }
        View view = this.view;
        if (view != null) {
            bind(view);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        super.onDismiss(dialogInterface);
        if (getParentFragment() instanceof ConversationFragment) {
            ((ConversationFragment) getParentFragment()).refreshList();
        }
    }

    private View createView() {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.enable_call_notification_settings_dialog_fragment, (ViewGroup) null, false);
        this.view = inflate;
        bind(inflate);
        return this.view;
    }

    private void bind(View view) {
        TextView textView = (TextView) view.findViewById(R.id.enable_call_notification_settings_dialog_system_all_configured);
        AppCompatImageView appCompatImageView = (AppCompatImageView) view.findViewById(R.id.enable_call_notification_settings_dialog_system_setting_indicator);
        TextView textView2 = (TextView) view.findViewById(R.id.enable_call_notification_settings_dialog_system_setting_text);
        AppCompatImageView appCompatImageView2 = (AppCompatImageView) view.findViewById(R.id.enable_call_notification_settings_dialog_channel_setting_indicator);
        TextView textView3 = (TextView) view.findViewById(R.id.enable_call_notification_settings_dialog_channel_setting_text);
        AppCompatImageView appCompatImageView3 = (AppCompatImageView) view.findViewById(R.id.enable_call_notification_settings_dialog_background_restricted_indicator);
        TextView textView4 = (TextView) view.findViewById(R.id.enable_call_notification_settings_dialog_background_restricted_text);
        int i = 0;
        if (areNotificationsDisabled(requireContext())) {
            appCompatImageView.setVisibility(0);
            textView2.setVisibility(0);
            textView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EnableCallNotificationSettingsDialog.this.lambda$bind$2(view2);
                }
            });
        } else {
            appCompatImageView.setVisibility(8);
            textView2.setVisibility(8);
            textView2.setOnClickListener(null);
        }
        if (isCallChannelInvalid(requireContext())) {
            appCompatImageView2.setVisibility(0);
            textView3.setVisibility(0);
            textView3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EnableCallNotificationSettingsDialog.this.lambda$bind$3(view2);
                }
            });
        } else {
            appCompatImageView2.setVisibility(8);
            textView3.setVisibility(8);
            textView3.setOnClickListener(null);
        }
        if (isBackgroundRestricted(requireContext())) {
            appCompatImageView3.setVisibility(0);
            textView4.setVisibility(0);
            textView4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog$$ExternalSyntheticLambda2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EnableCallNotificationSettingsDialog.this.lambda$bind$4(view2);
                }
            });
        } else {
            appCompatImageView3.setVisibility(8);
            textView4.setVisibility(8);
            textView4.setOnClickListener(null);
        }
        if (shouldShow(requireContext())) {
            i = 8;
        }
        textView.setVisibility(i);
    }

    public /* synthetic */ void lambda$bind$2(View view) {
        showNotificationSettings();
    }

    public /* synthetic */ void lambda$bind$3(View view) {
        showNotificationChannelSettings();
    }

    public /* synthetic */ void lambda$bind$4(View view) {
        showAppSettings();
    }

    public void showNotificationSettings() {
        Intent intent = new Intent("android.settings.APP_NOTIFICATION_SETTINGS");
        intent.putExtra("android.provider.extra.APP_PACKAGE", requireContext().getPackageName());
        startActivity(intent);
    }

    public void showNotificationChannelSettings() {
        NotificationChannels.openChannelSettings(requireContext(), NotificationChannels.CALLS, null);
    }

    public void showAppSettings() {
        startActivity(new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.fromParts("package", requireContext().getPackageName(), null)));
    }

    private static boolean areNotificationsDisabled(Context context) {
        return !NotificationChannels.areNotificationsEnabled(context);
    }

    private static boolean areCallNotificationsDisabled(Context context) {
        return !SignalStore.settings().isCallNotificationsEnabled();
    }

    private static boolean isCallChannelInvalid(Context context) {
        return !NotificationChannels.isCallsChannelValid(context);
    }

    private static boolean isBackgroundRestricted(Context context) {
        return Build.VERSION.SDK_INT >= 28 && DeviceProperties.isBackgroundRestricted(context);
    }

    private static int getCallNotificationSettingsBitmask(Context context) {
        int i = areNotificationsDisabled(context) ? 2 : 0;
        if (areCallNotificationsDisabled(context)) {
            i |= 4;
        }
        if (isCallChannelInvalid(context)) {
            i |= 16;
        }
        return isBackgroundRestricted(context) ? i | 256 : i;
    }
}
