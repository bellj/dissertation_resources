package org.thoughtcrime.securesms.conversation;

import io.reactivex.rxjava3.functions.Function;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationViewModel$$ExternalSyntheticLambda6 implements Function {
    @Override // io.reactivex.rxjava3.functions.Function
    public final Object apply(Object obj) {
        return Recipient.resolved((RecipientId) obj);
    }
}
