package org.thoughtcrime.securesms.conversation;

import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityOptionsCompat;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;

/* loaded from: classes4.dex */
public class ConversationPopupActivity extends ConversationActivity {
    private static final String TAG = Log.tag(ConversationPopupActivity.class);

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public void onInitializeToolbar(Toolbar toolbar) {
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        super.onPreCreate();
        overridePendingTransition(R.anim.slide_from_top, R.anim.slide_to_top);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        getWindow().setFlags(2, 2);
        WindowManager.LayoutParams attributes = getWindow().getAttributes();
        attributes.alpha = 1.0f;
        attributes.dimAmount = 0.1f;
        attributes.gravity = 48;
        getWindow().setAttributes(attributes);
        Display defaultDisplay = getWindowManager().getDefaultDisplay();
        int width = defaultDisplay.getWidth();
        int height = defaultDisplay.getHeight();
        if (height > width) {
            Window window = getWindow();
            double d = (double) width;
            Double.isNaN(d);
            int i = (int) (d * 0.85d);
            double d2 = (double) height;
            Double.isNaN(d2);
            window.setLayout(i, (int) (d2 * 0.5d));
        } else {
            Window window2 = getWindow();
            double d3 = (double) width;
            Double.isNaN(d3);
            int i2 = (int) (d3 * 0.7d);
            double d4 = (double) height;
            Double.isNaN(d4);
            window2.setLayout(i2, (int) (d4 * 0.75d));
        }
        super.onCreate(bundle, z);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        getTitleView().setOnClickListener(null);
        getComposeText().requestFocus();
        getQuickAttachmentToggle().disable();
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onPause() {
        super.onPause();
        if (isFinishing()) {
            overridePendingTransition(R.anim.slide_from_top, R.anim.slide_to_top);
        }
    }

    @Override // android.app.Activity
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menu.clear();
        menuInflater.inflate(R.menu.conversation_popup, menu);
        return true;
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.menu_expand) {
            return false;
        }
        saveDraft().addListener(new ListenableFuture.Listener<Long>() { // from class: org.thoughtcrime.securesms.conversation.ConversationPopupActivity.1
            public void onSuccess(Long l) {
                ActivityOptionsCompat makeScaleUpAnimation = ActivityOptionsCompat.makeScaleUpAnimation(ConversationPopupActivity.this.getWindow().getDecorView(), 0, 0, ConversationPopupActivity.this.getWindow().getAttributes().width, ConversationPopupActivity.this.getWindow().getAttributes().height);
                ConversationPopupActivity conversationPopupActivity = ConversationPopupActivity.this;
                ConversationPopupActivity.this.startActivity(ConversationIntents.createBuilder(conversationPopupActivity, conversationPopupActivity.getRecipient().getId(), l.longValue()).build(), makeScaleUpAnimation.toBundle());
                ConversationPopupActivity.this.finish();
            }

            @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
            public void onFailure(ExecutionException executionException) {
                Log.w(ConversationPopupActivity.TAG, executionException);
            }
        });
        return true;
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public void onSendComplete(long j) {
        finish();
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationActivity, org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public boolean onUpdateReminders() {
        if (!getReminderView().resolved()) {
            return false;
        }
        getReminderView().get().setVisibility(8);
        return false;
    }
}
