package org.thoughtcrime.securesms.conversation;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.thoughtcrime.securesms.database.model.ThreadRecord;

/* compiled from: ThreadCountAggregator.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\n\u000b\fB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00002\u0006\u0010\b\u001a\u00020\tH&R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u0001\u0003\r\u000e\u000f¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator;", "", "()V", NewHtcHomeBadger.COUNT, "", "getCount", "()I", "updateWith", "record", "Lorg/thoughtcrime/securesms/database/model/ThreadRecord;", "Count", "Init", "Outgoing", "Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator$Init;", "Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator$Outgoing;", "Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator$Count;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ThreadCountAggregator {
    public /* synthetic */ ThreadCountAggregator(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public abstract int getCount();

    public abstract ThreadCountAggregator updateWith(ThreadRecord threadRecord);

    private ThreadCountAggregator() {
    }

    /* compiled from: ThreadCountAggregator.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\tH\u0016R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator$Init;", "Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator;", "()V", NewHtcHomeBadger.COUNT, "", "getCount", "()I", "updateWith", "record", "Lorg/thoughtcrime/securesms/database/model/ThreadRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Init extends ThreadCountAggregator {
        public static final Init INSTANCE = new Init();
        private static final int count;

        private Init() {
            super(null);
        }

        @Override // org.thoughtcrime.securesms.conversation.ThreadCountAggregator
        public int getCount() {
            return count;
        }

        @Override // org.thoughtcrime.securesms.conversation.ThreadCountAggregator
        public ThreadCountAggregator updateWith(ThreadRecord threadRecord) {
            Intrinsics.checkNotNullParameter(threadRecord, "record");
            if (threadRecord.isOutgoing()) {
                return Outgoing.INSTANCE;
            }
            return new Count(threadRecord.getThreadId(), threadRecord.getUnreadCount(), threadRecord.getDate());
        }
    }

    /* compiled from: ThreadCountAggregator.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00012\u0006\u0010\b\u001a\u00020\tH\u0016R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator$Outgoing;", "Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator;", "()V", NewHtcHomeBadger.COUNT, "", "getCount", "()I", "updateWith", "record", "Lorg/thoughtcrime/securesms/database/model/ThreadRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Outgoing extends ThreadCountAggregator {
        public static final Outgoing INSTANCE = new Outgoing();
        private static final int count;

        private Outgoing() {
            super(null);
        }

        @Override // org.thoughtcrime.securesms.conversation.ThreadCountAggregator
        public int getCount() {
            return count;
        }

        @Override // org.thoughtcrime.securesms.conversation.ThreadCountAggregator
        public ThreadCountAggregator updateWith(ThreadRecord threadRecord) {
            Intrinsics.checkNotNullParameter(threadRecord, "record");
            if (threadRecord.isOutgoing()) {
                return INSTANCE;
            }
            return new Count(threadRecord.getThreadId(), threadRecord.getUnreadCount(), threadRecord.getDate());
        }
    }

    /* compiled from: ThreadCountAggregator.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\u0010\u0010\u000f\u001a\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u0014\u0010\b\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator$Count;", "Lorg/thoughtcrime/securesms/conversation/ThreadCountAggregator;", "threadId", "", "unreadCount", "", "threadDate", "(JIJ)V", NewHtcHomeBadger.COUNT, "getCount", "()I", "getThreadDate", "()J", "getThreadId", "getUnreadCount", "updateWith", "record", "Lorg/thoughtcrime/securesms/database/model/ThreadRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Count extends ThreadCountAggregator {
        private final int count;
        private final long threadDate;
        private final long threadId;
        private final int unreadCount;

        public Count(long j, int i, long j2) {
            super(null);
            this.threadId = j;
            this.unreadCount = i;
            this.threadDate = j2;
            this.count = i;
        }

        public final long getThreadDate() {
            return this.threadDate;
        }

        public final long getThreadId() {
            return this.threadId;
        }

        public final int getUnreadCount() {
            return this.unreadCount;
        }

        @Override // org.thoughtcrime.securesms.conversation.ThreadCountAggregator
        public int getCount() {
            return this.count;
        }

        @Override // org.thoughtcrime.securesms.conversation.ThreadCountAggregator
        public ThreadCountAggregator updateWith(ThreadRecord threadRecord) {
            Intrinsics.checkNotNullParameter(threadRecord, "record");
            if (threadRecord.isOutgoing()) {
                return Outgoing.INSTANCE;
            }
            if (this.threadId != threadRecord.getThreadId()) {
                return Init.INSTANCE.updateWith(threadRecord);
            }
            if (this.threadDate >= threadRecord.getDate()) {
                return this;
            }
            if (threadRecord.getUnreadCount() > 1) {
                return Init.INSTANCE.updateWith(threadRecord);
            }
            return new Count(this.threadId, this.unreadCount + 1, threadRecord.getDate());
        }
    }
}
