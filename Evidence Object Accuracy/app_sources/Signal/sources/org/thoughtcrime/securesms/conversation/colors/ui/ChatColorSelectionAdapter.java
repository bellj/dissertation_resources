package org.thoughtcrime.securesms.conversation.colors.ui;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.FixedSizeDrawableKt;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: ChatColorSelectionAdapter.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0004\u0007\b\t\nB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "context", "Landroid/content/Context;", "callbacks", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;)V", "CallbackBinder", "Callbacks", "ChatColorMappingViewHolder", "CustomColorMappingViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorSelectionAdapter extends MappingAdapter {
    private final Callbacks callbacks;

    /* compiled from: ChatColorSelectionAdapter.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;", "", "onAdd", "", "onDelete", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "onDuplicate", "onEdit", "onSelect", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onAdd();

        void onDelete(ChatColors chatColors);

        void onDuplicate(ChatColors chatColors);

        void onEdit(ChatColors chatColors);

        void onSelect(ChatColors chatColors);
    }

    public ChatColorSelectionAdapter(Context context, Callbacks callbacks) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(callbacks, "callbacks");
        this.callbacks = callbacks;
        registerFactory(ChatColorMappingModel.class, new LayoutFactory(new Function(this) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter$$ExternalSyntheticLambda0
            public final /* synthetic */ ChatColorSelectionAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ChatColorSelectionAdapter.m1490$r8$lambda$VJG3YJDsIdBFPY_dp_6p8ij630(ChatSelectionContextMenu.this, this.f$1, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.chat_color_selection_adapter_item));
        registerFactory(CustomColorMappingModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ChatColorSelectionAdapter.$r8$lambda$DAjvz7PfCK6s76R9jnfuaHtp0FE(ChatColorSelectionAdapter.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.chat_color_custom_adapter_item));
    }

    /* renamed from: _init_$lambda-0 */
    public static final MappingViewHolder m1491_init_$lambda0(ChatSelectionContextMenu chatSelectionContextMenu, ChatColorSelectionAdapter chatColorSelectionAdapter, View view) {
        Intrinsics.checkNotNullParameter(chatSelectionContextMenu, "$popupWindow");
        Intrinsics.checkNotNullParameter(chatColorSelectionAdapter, "this$0");
        Intrinsics.checkNotNullExpressionValue(view, "v");
        return new ChatColorMappingViewHolder(view, chatSelectionContextMenu, chatColorSelectionAdapter.callbacks);
    }

    /* renamed from: _init_$lambda-1 */
    public static final MappingViewHolder m1492_init_$lambda1(ChatColorSelectionAdapter chatColorSelectionAdapter, View view) {
        Intrinsics.checkNotNullParameter(chatColorSelectionAdapter, "this$0");
        Intrinsics.checkNotNullExpressionValue(view, "v");
        return new CustomColorMappingViewHolder(view, new Function0<Unit>(chatColorSelectionAdapter.callbacks) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter$2$1
            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                ((ChatColorSelectionAdapter.Callbacks) this.receiver).onAdd();
            }
        });
    }

    /* compiled from: ChatColorSelectionAdapter.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\t\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u0002H\u0016¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$CustomColorMappingViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/CustomColorMappingModel;", "itemView", "Landroid/view/View;", "onClicked", "Lkotlin/Function0;", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CustomColorMappingViewHolder extends MappingViewHolder<CustomColorMappingModel> {
        public void bind(CustomColorMappingModel customColorMappingModel) {
            Intrinsics.checkNotNullParameter(customColorMappingModel, "model");
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public CustomColorMappingViewHolder(View view, Function0<Unit> function0) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function0, "onClicked");
            view.setOnClickListener(new ChatColorSelectionAdapter$CustomColorMappingViewHolder$$ExternalSyntheticLambda0(function0));
        }

        /* renamed from: _init_$lambda-0 */
        public static final void m1496_init_$lambda0(Function0 function0, View view) {
            Intrinsics.checkNotNullParameter(function0, "$onClicked");
            function0.invoke();
        }
    }

    /* compiled from: ChatColorSelectionAdapter.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0002H\u0016R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$ChatColorMappingViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorMappingModel;", "itemView", "Landroid/view/View;", "popupWindow", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu;", "callbacks", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;", "(Landroid/view/View;Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu;Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;)V", "auto", "Landroid/widget/TextView;", "edit", "preview", "Landroid/widget/ImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ChatColorMappingViewHolder extends MappingViewHolder<ChatColorMappingModel> {
        private final TextView auto;
        private final Callbacks callbacks;
        private final View edit;
        private final ChatSelectionContextMenu popupWindow;
        private final ImageView preview;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ChatColorMappingViewHolder(View view, ChatSelectionContextMenu chatSelectionContextMenu, Callbacks callbacks) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(chatSelectionContextMenu, "popupWindow");
            Intrinsics.checkNotNullParameter(callbacks, "callbacks");
            this.popupWindow = chatSelectionContextMenu;
            this.callbacks = callbacks;
            View findViewById = view.findViewById(R.id.chat_color);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.chat_color)");
            this.preview = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.auto);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.auto)");
            this.auto = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.edit);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.edit)");
            this.edit = findViewById3;
        }

        public void bind(ChatColorMappingModel chatColorMappingModel) {
            Intrinsics.checkNotNullParameter(chatColorMappingModel, "model");
            this.itemView.setSelected(chatColorMappingModel.isSelected());
            int i = 8;
            this.auto.setVisibility(chatColorMappingModel.isAuto() ? 0 : 8);
            View view = this.edit;
            if (chatColorMappingModel.isCustom() && chatColorMappingModel.isSelected()) {
                i = 0;
            }
            view.setVisibility(i);
            this.preview.setOnClickListener(new ChatColorSelectionAdapter$ChatColorMappingViewHolder$$ExternalSyntheticLambda0(chatColorMappingModel, this));
            if (chatColorMappingModel.isCustom()) {
                this.preview.setOnLongClickListener(new ChatColorSelectionAdapter$ChatColorMappingViewHolder$$ExternalSyntheticLambda1(this, chatColorMappingModel));
            } else {
                this.preview.setOnLongClickListener(null);
                this.preview.setLongClickable(false);
            }
            this.preview.setImageDrawable(FixedSizeDrawableKt.withFixedSize(chatColorMappingModel.getChatColors().asCircle(), ViewUtil.dpToPx(56)));
            if (chatColorMappingModel.isAuto() && SignalStore.chatColorsValues().getShouldShowAutoTooltip()) {
                SignalStore.chatColorsValues().setShouldShowAutoTooltip(false);
                TooltipPopup.forTarget(this.itemView).setText(R.string.ChatColorSelectionFragment__auto_matches_the_color_to_the_wallpaper).setBackgroundTint(ContextCompat.getColor(this.context, R.color.signal_accent_primary)).setTextColor(-1).show(1);
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1493bind$lambda0(ChatColorMappingModel chatColorMappingModel, ChatColorMappingViewHolder chatColorMappingViewHolder, View view) {
            Intrinsics.checkNotNullParameter(chatColorMappingModel, "$model");
            Intrinsics.checkNotNullParameter(chatColorMappingViewHolder, "this$0");
            if (!chatColorMappingModel.isCustom() || !chatColorMappingModel.isSelected()) {
                chatColorMappingViewHolder.callbacks.onSelect(chatColorMappingModel.getChatColors());
            } else {
                chatColorMappingViewHolder.callbacks.onEdit(chatColorMappingModel.getChatColors());
            }
        }

        /* renamed from: bind$lambda-1 */
        public static final boolean m1494bind$lambda1(ChatColorMappingViewHolder chatColorMappingViewHolder, ChatColorMappingModel chatColorMappingModel, View view) {
            Intrinsics.checkNotNullParameter(chatColorMappingViewHolder, "this$0");
            Intrinsics.checkNotNullParameter(chatColorMappingModel, "$model");
            chatColorMappingViewHolder.popupWindow.setCallback(new CallbackBinder(chatColorMappingViewHolder.callbacks, chatColorMappingModel.getChatColors()));
            ChatSelectionContextMenu chatSelectionContextMenu = chatColorMappingViewHolder.popupWindow;
            View view2 = chatColorMappingViewHolder.itemView;
            Intrinsics.checkNotNullExpressionValue(view2, "itemView");
            chatSelectionContextMenu.show(view2);
            return true;
        }
    }

    /* compiled from: ChatColorSelectionAdapter.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\bH\u0016J\b\u0010\n\u001a\u00020\bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$CallbackBinder;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatSelectionContextMenu$Callback;", "callbacks", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "(Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionAdapter$Callbacks;Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;)V", "onDeletePressed", "", "onDuplicatePressed", "onEditPressed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CallbackBinder implements ChatSelectionContextMenu.Callback {
        private final Callbacks callbacks;
        private final ChatColors chatColors;

        public CallbackBinder(Callbacks callbacks, ChatColors chatColors) {
            Intrinsics.checkNotNullParameter(callbacks, "callbacks");
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            this.callbacks = callbacks;
            this.chatColors = chatColors;
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu.Callback
        public void onEditPressed() {
            this.callbacks.onEdit(this.chatColors);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu.Callback
        public void onDuplicatePressed() {
            this.callbacks.onDuplicate(this.chatColors);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatSelectionContextMenu.Callback
        public void onDeletePressed() {
            this.callbacks.onDelete(this.chatColors);
        }
    }
}
