package org.thoughtcrime.securesms.conversation;

import android.view.View;
import org.thoughtcrime.securesms.conversation.ConversationStickerSuggestionAdapter;
import org.thoughtcrime.securesms.database.model.StickerRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationStickerSuggestionAdapter$StickerSuggestionViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ConversationStickerSuggestionAdapter.EventListener f$0;
    public final /* synthetic */ StickerRecord f$1;

    public /* synthetic */ ConversationStickerSuggestionAdapter$StickerSuggestionViewHolder$$ExternalSyntheticLambda0(ConversationStickerSuggestionAdapter.EventListener eventListener, StickerRecord stickerRecord) {
        this.f$0 = eventListener;
        this.f$1 = stickerRecord;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onStickerSuggestionClicked(this.f$1);
    }
}
