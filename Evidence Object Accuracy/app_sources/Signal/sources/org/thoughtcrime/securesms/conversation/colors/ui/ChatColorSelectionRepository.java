package org.thoughtcrime.securesms.conversation.colors.ui;

import android.content.Context;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: ChatColorSelectionRepository.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00192\u00020\u0001:\u0003\u0019\u001a\u001bB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001c\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\fJ\u000e\u0010\r\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u001c\u0010\u000e\u001a\u00020\b2\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\b0\u0010H&J\"\u0010\u0011\u001a\u00020\b2\u0006\u0010\u0012\u001a\u00020\u00132\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0014\u0012\u0004\u0012\u00020\b0\u0010J\u001e\u0010\u0015\u001a\u00020\b2\u0014\u0010\u000f\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0016\u0012\u0004\u0012\u00020\b0\u0010H&J\u001e\u0010\u0017\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\b0\fH&R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0002\u001c\u001d¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getContext", "()Landroid/content/Context;", "delete", "", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "onDeleted", "Lkotlin/Function0;", "duplicate", "getChatColors", "consumer", "Lkotlin/Function1;", "getUsageCount", "chatColorsId", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "", "getWallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "save", "onSaved", "Companion", "Global", "Single", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository$Global;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository$Single;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ChatColorSelectionRepository {
    public static final Companion Companion = new Companion(null);
    private final Context context;

    public /* synthetic */ ChatColorSelectionRepository(Context context, DefaultConstructorMarker defaultConstructorMarker) {
        this(context);
    }

    public abstract void getChatColors(Function1<? super ChatColors, Unit> function1);

    public abstract void getWallpaper(Function1<? super ChatWallpaper, Unit> function1);

    public abstract void save(ChatColors chatColors, Function0<Unit> function0);

    private ChatColorSelectionRepository(Context context) {
        Context applicationContext = context.getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
        this.context = applicationContext;
    }

    protected final Context getContext() {
        return this.context;
    }

    public final void duplicate(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                ChatColorSelectionRepository.$r8$lambda$jG8TRmm_0o6e3vEQIvEFviLUNDQ(ChatColors.this);
            }
        });
    }

    /* renamed from: duplicate$lambda-0 */
    public static final void m1509duplicate$lambda0(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "$chatColors");
        SignalDatabase.Companion.chatColors().saveChatColors(chatColors.withId(ChatColors.Id.NotSet.INSTANCE));
    }

    public final void getUsageCount(ChatColors.Id id, Function1<? super Integer, Unit> function1) {
        Intrinsics.checkNotNullParameter(id, "chatColorsId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(id) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ ChatColors.Id f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatColorSelectionRepository.$r8$lambda$WIJO8Wm1XUl926E2krlRxGBct_U(Function1.this, this.f$1);
            }
        });
    }

    /* renamed from: getUsageCount$lambda-1 */
    public static final void m1510getUsageCount$lambda1(Function1 function1, ChatColors.Id id) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(id, "$chatColorsId");
        function1.invoke(Integer.valueOf(SignalDatabase.Companion.recipients().getColorUsageCount(id)));
    }

    public final void delete(ChatColors chatColors, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        Intrinsics.checkNotNullParameter(function0, "onDeleted");
        SignalExecutors.BOUNDED.execute(new Runnable(function0) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Function0 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ChatColorSelectionRepository.$r8$lambda$eiOiSBouAlMAQYqItXLIj9HChfY(ChatColors.this, this.f$1);
            }
        });
    }

    /* renamed from: delete$lambda-2 */
    public static final void m1508delete$lambda2(ChatColors chatColors, Function0 function0) {
        Intrinsics.checkNotNullParameter(chatColors, "$chatColors");
        Intrinsics.checkNotNullParameter(function0, "$onDeleted");
        SignalDatabase.Companion.chatColors().deleteChatColors(chatColors);
        function0.invoke();
    }

    /* compiled from: ChatColorSelectionRepository.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001c\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\bH\u0016J\u001e\u0010\n\u001a\u00020\u00062\u0014\u0010\u0007\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\u0004\u0012\u00020\u00060\bH\u0016J\u001e\u0010\f\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\t2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u000fH\u0016¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository$Global;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getChatColors", "", "consumer", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getWallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "save", "chatColors", "onSaved", "Lkotlin/Function0;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Global extends ChatColorSelectionRepository {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Global(Context context) {
            super(context, null);
            Intrinsics.checkNotNullParameter(context, "context");
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository
        public void getWallpaper(Function1<? super ChatWallpaper, Unit> function1) {
            Intrinsics.checkNotNullParameter(function1, "consumer");
            function1.invoke(SignalStore.wallpaper().getWallpaper());
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository
        public void getChatColors(Function1<? super ChatColors, Unit> function1) {
            Intrinsics.checkNotNullParameter(function1, "consumer");
            if (SignalStore.chatColorsValues().hasChatColors()) {
                ChatColors chatColors = SignalStore.chatColorsValues().getChatColors();
                if (chatColors != null) {
                    function1.invoke(chatColors);
                    return;
                }
                throw new IllegalArgumentException("Required value was null.".toString());
            }
            getWallpaper(new ChatColorSelectionRepository$Global$getChatColors$1(function1));
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository
        public void save(ChatColors chatColors, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            Intrinsics.checkNotNullParameter(function0, "onSaved");
            if (Intrinsics.areEqual(chatColors.getId(), ChatColors.Id.Auto.INSTANCE)) {
                SignalStore.chatColorsValues().setChatColors(null);
            } else {
                SignalStore.chatColorsValues().setChatColors(chatColors);
            }
            function0.invoke();
        }
    }

    /* compiled from: ChatColorSelectionRepository.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\b0\nH\u0016J\u001e\u0010\f\u001a\u00020\b2\u0014\u0010\t\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\r\u0012\u0004\u0012\u00020\b0\nH\u0016J\u001e\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u000b2\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\b0\u0011H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository$Single;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;", "context", "Landroid/content/Context;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getChatColors", "", "consumer", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getWallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "save", "chatColors", "onSaved", "Lkotlin/Function0;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Single extends ChatColorSelectionRepository {
        private final RecipientId recipientId;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Single(Context context, RecipientId recipientId) {
            super(context, null);
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository
        public void getWallpaper(Function1<? super ChatWallpaper, Unit> function1) {
            Intrinsics.checkNotNullParameter(function1, "consumer");
            SignalExecutors.BOUNDED.execute(new ChatColorSelectionRepository$Single$$ExternalSyntheticLambda1(this, function1));
        }

        /* renamed from: getWallpaper$lambda-0 */
        public static final void m1513getWallpaper$lambda0(Single single, Function1 function1) {
            Intrinsics.checkNotNullParameter(single, "this$0");
            Intrinsics.checkNotNullParameter(function1, "$consumer");
            Recipient resolved = Recipient.resolved(single.recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
            function1.invoke(resolved.getWallpaper());
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository
        public void getChatColors(Function1<? super ChatColors, Unit> function1) {
            Intrinsics.checkNotNullParameter(function1, "consumer");
            SignalExecutors.BOUNDED.execute(new ChatColorSelectionRepository$Single$$ExternalSyntheticLambda2(this, function1));
        }

        /* renamed from: getChatColors$lambda-1 */
        public static final void m1512getChatColors$lambda1(Single single, Function1 function1) {
            Intrinsics.checkNotNullParameter(single, "this$0");
            Intrinsics.checkNotNullParameter(function1, "$consumer");
            Recipient resolved = Recipient.resolved(single.recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
            ChatColors chatColors = resolved.getChatColors();
            Intrinsics.checkNotNullExpressionValue(chatColors, "recipient.chatColors");
            function1.invoke(chatColors);
        }

        @Override // org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionRepository
        public void save(ChatColors chatColors, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(chatColors, "chatColors");
            Intrinsics.checkNotNullParameter(function0, "onSaved");
            SignalExecutors.BOUNDED.execute(new ChatColorSelectionRepository$Single$$ExternalSyntheticLambda0(this, chatColors, function0));
        }

        /* renamed from: save$lambda-2 */
        public static final void m1514save$lambda2(Single single, ChatColors chatColors, Function0 function0) {
            Intrinsics.checkNotNullParameter(single, "this$0");
            Intrinsics.checkNotNullParameter(chatColors, "$chatColors");
            Intrinsics.checkNotNullParameter(function0, "$onSaved");
            SignalDatabase.Companion.recipients().setColor(single.recipientId, chatColors);
            function0.invoke();
        }
    }

    /* compiled from: ChatColorSelectionRepository.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository$Companion;", "", "()V", "create", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;", "context", "Landroid/content/Context;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final ChatColorSelectionRepository create(Context context, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(context, "context");
            if (recipientId != null) {
                return new Single(context, recipientId);
            }
            return new Global(context);
        }
    }
}
