package org.thoughtcrime.securesms.conversation.ui.mentions;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.VibrateUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* loaded from: classes4.dex */
public class MentionsPickerFragment extends LoggingFragment {
    private MentionsPickerAdapter adapter;
    private BottomSheetBehavior<View> behavior;
    private View bottomDivider;
    private final Handler handler = new Handler(Looper.getMainLooper());
    private RecyclerView list;
    private final Runnable lockSheetAfterListUpdate = new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment$$ExternalSyntheticLambda0
        @Override // java.lang.Runnable
        public final void run() {
            MentionsPickerFragment.$r8$lambda$adl66dEh_XD6v5qymhZcGvVfjQ4(MentionsPickerFragment.this);
        }
    };
    private View topDivider;
    private MentionsPickerViewModel viewModel;

    public /* synthetic */ void lambda$new$0() {
        this.behavior.setHideable(false);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.mentions_picker_fragment, viewGroup, false);
        this.list = (RecyclerView) inflate.findViewById(R.id.mentions_picker_list);
        this.topDivider = inflate.findViewById(R.id.mentions_picker_top_divider);
        this.bottomDivider = inflate.findViewById(R.id.mentions_picker_bottom_divider);
        this.behavior = BottomSheetBehavior.from(inflate.findViewById(R.id.mentions_picker_bottom_sheet));
        initializeBehavior();
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.viewModel = (MentionsPickerViewModel) ViewModelProviders.of(requireActivity()).get(MentionsPickerViewModel.class);
        initializeList();
        this.viewModel.getMentionList().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MentionsPickerFragment.m1620$r8$lambda$PG2cH7aUwIXLMSwVc7nKeyBdLE(MentionsPickerFragment.this, (List) obj);
            }
        });
        this.viewModel.isShowing().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                MentionsPickerFragment.m1619$r8$lambda$E0uJPeuTWnA7vm0h7kwdBqVGk(MentionsPickerFragment.this, (Boolean) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onActivityCreated$1(Boolean bool) {
        if (bool.booleanValue()) {
            VibrateUtil.vibrateTick(requireContext());
        }
    }

    private void initializeBehavior() {
        this.behavior.setHideable(true);
        this.behavior.setState(5);
        this.behavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment.1
            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onStateChanged(View view, int i) {
                if (i == 5) {
                    MentionsPickerFragment.this.adapter.submitList(Collections.emptyList());
                    MentionsPickerFragment.this.showDividers(false);
                    return;
                }
                MentionsPickerFragment.this.showDividers(true);
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onSlide(View view, float f) {
                MentionsPickerFragment.this.showDividers(Float.isNaN(f) || f > -0.8f);
            }
        });
    }

    private void initializeList() {
        this.adapter = new MentionsPickerAdapter(new RecipientViewHolder.EventListener() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder.EventListener
            public final void onClick(Recipient recipient) {
                MentionsPickerFragment.m1621$r8$lambda$zqZ8Fyg_bvm95NnH38KqkwA(MentionsPickerFragment.this, recipient);
            }

            @Override // org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder.EventListener
            public /* synthetic */ void onModelClick(RecipientMappingModel recipientMappingModel) {
                RecipientViewHolder.EventListener.CC.$default$onModelClick(this, recipientMappingModel);
            }
        }, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ui.mentions.MentionsPickerFragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                MentionsPickerFragment.$r8$lambda$YBAwNWwR9BiSNRd4Am66g3zPrVg(MentionsPickerFragment.this);
            }
        });
        this.list.setLayoutManager(new LinearLayoutManager(requireContext()));
        this.list.setAdapter(this.adapter);
        this.list.setItemAnimator(null);
    }

    public /* synthetic */ void lambda$initializeList$2() {
        updateBottomSheetBehavior(this.adapter.getItemCount());
    }

    public void handleMentionClicked(Recipient recipient) {
        this.viewModel.onSelectionChange(recipient);
    }

    public void updateList(List<MappingModel<?>> list) {
        if (this.adapter.getItemCount() <= 0 || !list.isEmpty()) {
            this.adapter.submitList(list);
        } else {
            updateBottomSheetBehavior(0);
        }
    }

    private void updateBottomSheetBehavior(int i) {
        boolean z = i > 0;
        this.viewModel.setIsShowing(z);
        if (z) {
            this.list.scrollToPosition(0);
            this.behavior.setState(4);
            this.handler.post(this.lockSheetAfterListUpdate);
            showDividers(true);
            return;
        }
        this.handler.removeCallbacks(this.lockSheetAfterListUpdate);
        this.behavior.setHideable(true);
        this.behavior.setState(5);
    }

    public void showDividers(boolean z) {
        int i = 0;
        this.topDivider.setVisibility(z ? 0 : 8);
        View view = this.bottomDivider;
        if (!z) {
            i = 8;
        }
        view.setVisibility(i);
    }
}
