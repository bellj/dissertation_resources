package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.content.Context;
import j$.util.function.Consumer;
import java.util.Set;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda3 implements Runnable {
    public final /* synthetic */ Set f$0;
    public final /* synthetic */ Context f$1;
    public final /* synthetic */ Consumer f$2;

    public /* synthetic */ MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda3(Set set, Context context, Consumer consumer) {
        this.f$0 = set;
        this.f$1 = context;
        this.f$2 = consumer;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MultiselectForwardFragmentArgs.Companion.m1580create$lambda9(this.f$0, this.f$1, this.f$2);
    }
}
