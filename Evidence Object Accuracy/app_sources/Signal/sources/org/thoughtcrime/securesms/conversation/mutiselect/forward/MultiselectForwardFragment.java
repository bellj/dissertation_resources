package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import androidx.core.os.BundleKt;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt__SetsJVMKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ContactFilterView;
import org.thoughtcrime.securesms.components.TooltipPopup;
import org.thoughtcrime.securesms.components.WrapperDialogFragment;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchMediator;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardState;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardViewModel;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseGroupStoryBottomSheet;
import org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.sharing.MultiShareArgs;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryFlowDialogFragment;
import org.thoughtcrime.securesms.stories.settings.create.CreateStoryWithViewersFragment;
import org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* compiled from: MultiselectForwardFragment.kt */
@Metadata(d1 = {"\u0000Ì\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 [2\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u00042\u00020\u0005:\u0002Z[B\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\"\u001a\u00020#H\u0002J\u0012\u0010$\u001a\u00020%2\b\b\u0001\u0010&\u001a\u00020'H\u0002J\u0016\u0010(\u001a\u00020%2\f\u0010)\u001a\b\u0012\u0004\u0012\u00020+0*H\u0002J\u0012\u0010,\u001a\u00020%2\b\b\u0001\u0010&\u001a\u00020'H\u0002J\b\u0010-\u001a\u00020%H\u0002J$\u0010.\u001a\u00020%2\f\u0010/\u001a\b\u0012\u0004\u0012\u000201002\f\u0010)\u001a\b\u0012\u0004\u0012\u00020+00H\u0002J\u001a\u00102\u001a\u00020%2\u0006\u00103\u001a\u0002042\b\b\u0001\u00105\u001a\u00020'H\u0002J&\u00106\u001a\b\u0012\u0004\u0012\u00020+0*2\b\u00107\u001a\u0004\u0018\u0001042\f\u00108\u001a\b\u0012\u0004\u0012\u00020+0*H\u0002J\u0010\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020<H\u0002J\b\u0010=\u001a\u00020'H\u0002J\b\u0010>\u001a\u00020?H\u0002J\b\u0010@\u001a\u00020%H\u0002J\b\u0010A\u001a\u00020BH\u0002J\b\u0010C\u001a\u00020BH\u0002J\b\u0010D\u001a\u00020BH\u0002J\b\u0010E\u001a\u00020%H\u0016J\b\u0010F\u001a\u00020%H\u0016J\u0012\u0010G\u001a\u00020H2\b\u0010I\u001a\u0004\u0018\u00010JH\u0016J\b\u0010K\u001a\u00020%H\u0016J\b\u0010L\u001a\u00020%H\u0016J\u0010\u0010M\u001a\u00020%2\u0006\u0010N\u001a\u00020OH\u0016J\b\u0010P\u001a\u00020%H\u0016J\b\u0010Q\u001a\u00020%H\u0016J\b\u0010R\u001a\u00020%H\u0016J\u0010\u0010S\u001a\u00020%2\u0006\u0010T\u001a\u000204H\u0002J\u001a\u0010U\u001a\u00020%2\u0006\u00107\u001a\u0002042\b\u0010I\u001a\u0004\u0018\u00010JH\u0016J\b\u0010V\u001a\u00020%H\u0016J\u0016\u0010W\u001a\u00020%2\f\u0010X\u001a\b\u0012\u0004\u0012\u00020Y00H\u0016R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u000f\u001a\u00020\u0010X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X.¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u001cX\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\u001d\u001a\u00020\u001e8BX\u0002¢\u0006\f\n\u0004\b!\u0010\u000e\u001a\u0004\b\u001f\u0010 ¨\u0006\\"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment;", "Landroidx/fragment/app/Fragment;", "Lorg/thoughtcrime/securesms/safety/SafetyNumberBottomSheet$Callbacks;", "Lorg/thoughtcrime/securesms/mediasend/v2/stories/ChooseStoryTypeBottomSheet$Callback;", "Lorg/thoughtcrime/securesms/components/WrapperDialogFragment$WrapperDialogFragmentCallback;", "Lorg/thoughtcrime/securesms/stories/settings/privacy/ChooseInitialMyStoryMembershipBottomSheetDialogFragment$Callback;", "()V", "addMessage", "Landroid/widget/EditText;", MultiselectForwardFragment.ARGS, "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "getArgs", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "args$delegate", "Lkotlin/Lazy;", "callback", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "contactFilterView", "Lorg/thoughtcrime/securesms/components/ContactFilterView;", "contactSearchMediator", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchMediator;", "contactSearchRecycler", "Landroidx/recyclerview/widget/RecyclerView;", "dismissibleDialog", "Lorg/thoughtcrime/securesms/util/views/SimpleProgressDialog$DismissibleDialog;", "disposables", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "handler", "Landroid/os/Handler;", "viewModel", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardViewModel;", "viewModel$delegate", "createViewModelFactory", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardViewModel$Factory;", "dismissAndShowToast", "", "toastTextResId", "", "dismissWithSelection", "selectedContacts", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey;", "dismissWithSuccess", "displayFirstSendConfirmation", "displaySafetyNumberConfirmation", "identityRecords", "", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "displayTooltip", "anchor", "Landroid/view/View;", DraftDatabase.Draft.TEXT, "filterContacts", "view", "contactSet", "getConfiguration", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchConfiguration;", "contactSearchState", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchState;", "getMessageCount", "getStorySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "handleMessageExpired", "includeSms", "", "isSelectedMediaValidForNonStories", "isSelectedMediaValidForStories", "onCanceled", "onDestroyView", "onGetLayoutInflater", "Landroid/view/LayoutInflater;", "savedInstanceState", "Landroid/os/Bundle;", "onGroupStoryClicked", "onMessageResentAfterSafetyNumberChangeInBottomSheet", "onMyStoryConfigured", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onNewStoryClicked", "onPause", "onResume", "onSend", "sendButton", "onViewCreated", "onWrapperDialogFragmentDismissed", "sendAnywayAfterSafetyNumberChangedInBottomSheet", "destinations", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "Callback", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardFragment extends Fragment implements SafetyNumberBottomSheet.Callbacks, ChooseStoryTypeBottomSheet.Callback, WrapperDialogFragment.WrapperDialogFragmentCallback, ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback {
    public static final String ARGS;
    public static final Companion Companion = new Companion(null);
    public static final String DIALOG_TITLE;
    public static final String RESULT_KEY;
    public static final String RESULT_SELECTION;
    public static final String RESULT_SENT;
    private EditText addMessage;
    private final Lazy args$delegate = LazyKt__LazyJVMKt.lazy(new Function0<MultiselectForwardFragmentArgs>(this) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$args$2
        final /* synthetic */ MultiselectForwardFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final MultiselectForwardFragmentArgs invoke() {
            Parcelable parcelable = this.this$0.requireArguments().getParcelable(MultiselectForwardFragment.ARGS);
            Intrinsics.checkNotNull(parcelable);
            return (MultiselectForwardFragmentArgs) parcelable;
        }
    });
    private Callback callback;
    private ContactFilterView contactFilterView;
    private ContactSearchMediator contactSearchMediator;
    private RecyclerView contactSearchRecycler;
    private SimpleProgressDialog.DismissibleDialog dismissibleDialog;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private Handler handler;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(MultiselectForwardViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<MultiselectForwardViewModel.Factory>(this) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final MultiselectForwardViewModel.Factory invoke() {
            return ((MultiselectForwardFragment) this.receiver).createViewModelFactory();
        }
    });

    /* compiled from: MultiselectForwardFragment.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\b\u0010\u0004\u001a\u00020\u0005H&J\b\u0010\u0006\u001a\u00020\u0007H&J\n\u0010\b\u001a\u0004\u0018\u00010\tH\u0016J\b\u0010\n\u001a\u00020\u0003H&J\b\u0010\u000b\u001a\u00020\u0003H&J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH&¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "", "exitFlow", "", "getContainer", "Landroid/view/ViewGroup;", "getDialogBackgroundColor", "", "getStorySendRequirements", "Lorg/thoughtcrime/securesms/stories/Stories$MediaTransform$SendRequirements;", "onFinishForwardAction", "onSearchInputFocused", "setResult", "bundle", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {

        /* compiled from: MultiselectForwardFragment.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DefaultImpls {
            public static Stories.MediaTransform.SendRequirements getStorySendRequirements(Callback callback) {
                return null;
            }
        }

        void exitFlow();

        ViewGroup getContainer();

        int getDialogBackgroundColor();

        Stories.MediaTransform.SendRequirements getStorySendRequirements();

        void onFinishForwardAction();

        void onSearchInputFocused();

        void setResult(Bundle bundle);
    }

    /* compiled from: MultiselectForwardFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[Stories.MediaTransform.SendRequirements.values().length];
            iArr[Stories.MediaTransform.SendRequirements.REQUIRES_CLIP.ordinal()] = 1;
            iArr[Stories.MediaTransform.SendRequirements.CAN_NOT_SEND.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @JvmStatic
    public static final Fragment create(MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        return Companion.create(multiselectForwardFragmentArgs);
    }

    @JvmStatic
    public static final void showBottomSheet(FragmentManager fragmentManager, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        Companion.showBottomSheet(fragmentManager, multiselectForwardFragmentArgs);
    }

    @JvmStatic
    public static final void showFullScreen(FragmentManager fragmentManager, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        Companion.showFullScreen(fragmentManager, multiselectForwardFragmentArgs);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:18:0x001b */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r0v1, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r0v4, types: [org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider] */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARN: Type inference failed for: r0v12 */
    /* JADX WARN: Type inference failed for: r0v13 */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration getConfiguration(org.thoughtcrime.securesms.contacts.paged.ContactSearchState r4) {
        /*
            r3 = this;
            androidx.fragment.app.Fragment r0 = r3.getParentFragment()
        L_0x0004:
            if (r0 == 0) goto L_0x0010
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider
            if (r1 == 0) goto L_0x000b
            goto L_0x001b
        L_0x000b:
            androidx.fragment.app.Fragment r0 = r0.getParentFragment()
            goto L_0x0004
        L_0x0010:
            androidx.fragment.app.FragmentActivity r0 = r3.requireActivity()
            boolean r1 = r0 instanceof org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider
            if (r1 != 0) goto L_0x0019
            r0 = 0
        L_0x0019:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider) r0
        L_0x001b:
            org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider r0 = (org.thoughtcrime.securesms.conversation.mutiselect.forward.SearchConfigurationProvider) r0
            if (r0 == 0) goto L_0x002e
            androidx.fragment.app.FragmentManager r1 = r3.getChildFragmentManager()
            java.lang.String r2 = "childFragmentManager"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
            org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration r0 = r0.getSearchConfiguration(r1, r4)
            if (r0 != 0) goto L_0x0039
        L_0x002e:
            org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration$Companion r0 = org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration.Companion
            org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$getConfiguration$1 r1 = new org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$getConfiguration$1
            r1.<init>(r4, r3)
            org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration r0 = r0.build(r1)
        L_0x0039:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.getConfiguration(org.thoughtcrime.securesms.contacts.paged.ContactSearchState):org.thoughtcrime.securesms.contacts.paged.ContactSearchConfiguration");
    }

    private final void dismissWithSuccess(int i) {
        Callback callback;
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            Bundle bundle = new Bundle();
            bundle.putBoolean(RESULT_SENT, true);
            callback.setResult(bundle);
            dismissAndShowToast(i);
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    private final Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        Callback callback;
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        callback = (Callback) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback");
                    }
                } else if (fragment instanceof Callback) {
                    callback = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            Stories.MediaTransform.SendRequirements storySendRequirements = callback.getStorySendRequirements();
            return storySendRequirements == null ? getViewModel().getSnapshot().getStorySendRequirements() : storySendRequirements;
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    public MultiselectForwardFragment() {
        super(R.layout.multiselect_forward_fragment);
    }

    private final MultiselectForwardViewModel getViewModel() {
        return (MultiselectForwardViewModel) this.viewModel$delegate.getValue();
    }

    public final MultiselectForwardViewModel.Factory createViewModelFactory() {
        return new MultiselectForwardViewModel.Factory(getArgs().getStorySendRequirements(), getArgs().getMultiShareArgs(), getArgs().getForceSelectionOnly(), new MultiselectForwardRepository());
    }

    private final MultiselectForwardFragmentArgs getArgs() {
        return (MultiselectForwardFragmentArgs) this.args$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public LayoutInflater onGetLayoutInflater(Bundle bundle) {
        if (getParentFragment() != null) {
            LayoutInflater onGetLayoutInflater = requireParentFragment().onGetLayoutInflater(bundle);
            Intrinsics.checkNotNullExpressionValue(onGetLayoutInflater, "{\n      requireParentFra…savedInstanceState)\n    }");
            return onGetLayoutInflater;
        }
        LayoutInflater onGetLayoutInflater2 = super.onGetLayoutInflater(bundle);
        Intrinsics.checkNotNullExpressionValue(onGetLayoutInflater2, "{\n      super.onGetLayou…savedInstanceState)\n    }");
        return onGetLayoutInflater2;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:43:0x006e */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r15v12, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r15v15, types: [org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$Callback] */
    /* JADX WARN: Type inference failed for: r15v17, types: [java.lang.Object] */
    /* JADX WARN: Type inference failed for: r15v41 */
    /* JADX WARN: Type inference failed for: r15v42 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.Fragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onViewCreated(android.view.View r14, android.os.Bundle r15) {
        /*
        // Method dump skipped, instructions count: 517
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.onViewCreated(android.view.View, android.os.Bundle):void");
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1571onViewCreated$lambda0(MultiselectForwardFragment multiselectForwardFragment, View view, boolean z) {
        Intrinsics.checkNotNullParameter(multiselectForwardFragment, "this$0");
        if (z) {
            Callback callback = multiselectForwardFragment.callback;
            if (callback == null) {
                Intrinsics.throwUninitializedPropertyAccessException("callback");
                callback = null;
            }
            callback.onSearchInputFocused();
        }
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1572onViewCreated$lambda1(MultiselectForwardFragment multiselectForwardFragment, String str) {
        Intrinsics.checkNotNullParameter(multiselectForwardFragment, "this$0");
        ContactSearchMediator contactSearchMediator = multiselectForwardFragment.contactSearchMediator;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
            contactSearchMediator = null;
        }
        contactSearchMediator.onFilterChanged(str);
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m1573onViewCreated$lambda3(MultiselectForwardFragment multiselectForwardFragment, View view) {
        Intrinsics.checkNotNullParameter(multiselectForwardFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        multiselectForwardFragment.onSend(view);
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:62:? A[RETURN, SYNTHETIC] */
    /* renamed from: onViewCreated$lambda-6 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void m1574onViewCreated$lambda6(org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment r7, androidx.appcompat.widget.AppCompatImageView r8, org.thoughtcrime.securesms.sharing.ShareSelectionAdapter r9, android.view.ViewGroup r10, java.util.Set r11) {
        /*
        // Method dump skipped, instructions count: 252
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.m1574onViewCreated$lambda6(org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment, androidx.appcompat.widget.AppCompatImageView, org.thoughtcrime.securesms.sharing.ShareSelectionAdapter, android.view.ViewGroup, java.util.Set):void");
    }

    /* renamed from: onViewCreated$lambda-7 */
    public static final void m1575onViewCreated$lambda7(MultiselectForwardFragment multiselectForwardFragment, AppCompatImageView appCompatImageView, MultiselectForwardState multiselectForwardState) {
        Intrinsics.checkNotNullParameter(multiselectForwardFragment, "this$0");
        Intrinsics.checkNotNullParameter(appCompatImageView, "$sendButton");
        MultiselectForwardState.Stage stage = multiselectForwardState.getStage();
        MultiselectForwardState.Stage.Selection selection = MultiselectForwardState.Stage.Selection.INSTANCE;
        if (!Intrinsics.areEqual(stage, selection)) {
            if (Intrinsics.areEqual(stage, MultiselectForwardState.Stage.FirstConfirmation.INSTANCE)) {
                multiselectForwardFragment.displayFirstSendConfirmation();
            } else if (stage instanceof MultiselectForwardState.Stage.SafetyConfirmation) {
                multiselectForwardFragment.displaySafetyNumberConfirmation(((MultiselectForwardState.Stage.SafetyConfirmation) multiselectForwardState.getStage()).getIdentities(), ((MultiselectForwardState.Stage.SafetyConfirmation) multiselectForwardState.getStage()).getSelectedContacts());
            } else if (!Intrinsics.areEqual(stage, MultiselectForwardState.Stage.LoadingIdentities.INSTANCE)) {
                if (Intrinsics.areEqual(stage, MultiselectForwardState.Stage.SendPending.INSTANCE)) {
                    Handler handler = multiselectForwardFragment.handler;
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    }
                    SimpleProgressDialog.DismissibleDialog dismissibleDialog = multiselectForwardFragment.dismissibleDialog;
                    if (dismissibleDialog != null) {
                        dismissibleDialog.dismiss();
                    }
                    multiselectForwardFragment.dismissibleDialog = SimpleProgressDialog.showDelayed(multiselectForwardFragment.requireContext());
                } else if (Intrinsics.areEqual(stage, MultiselectForwardState.Stage.SomeFailed.INSTANCE)) {
                    multiselectForwardFragment.dismissWithSuccess(R.plurals.MultiselectForwardFragment_messages_sent);
                } else if (Intrinsics.areEqual(stage, MultiselectForwardState.Stage.AllFailed.INSTANCE)) {
                    multiselectForwardFragment.dismissAndShowToast(R.plurals.MultiselectForwardFragment_messages_failed_to_send);
                } else if (Intrinsics.areEqual(stage, MultiselectForwardState.Stage.Success.INSTANCE)) {
                    multiselectForwardFragment.dismissWithSuccess(R.plurals.MultiselectForwardFragment_messages_sent);
                } else if (stage instanceof MultiselectForwardState.Stage.SelectionConfirmed) {
                    multiselectForwardFragment.dismissWithSelection(((MultiselectForwardState.Stage.SelectionConfirmed) multiselectForwardState.getStage()).getSelectedContacts());
                }
            }
        }
        appCompatImageView.setEnabled(Intrinsics.areEqual(multiselectForwardState.getStage(), selection));
    }

    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r2v2. Raw type applied. Possible types: java.util.Iterator<T>, java.util.Iterator */
    /* JADX WARN: Type inference failed for: r3v6 */
    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        MultiShareArgs multiShareArgs;
        super.onResume();
        long currentTimeMillis = System.currentTimeMillis();
        List<MultiShareArgs> multiShareArgs2 = getArgs().getMultiShareArgs();
        ArrayList arrayList = new ArrayList();
        for (Object obj : multiShareArgs2) {
            if (((MultiShareArgs) obj).getExpiresAt() > 0) {
                arrayList.add(obj);
            }
        }
        Iterator it = arrayList.iterator();
        if (!it.hasNext()) {
            multiShareArgs = null;
        } else {
            Object next = it.next();
            if (!it.hasNext()) {
                multiShareArgs = next;
            } else {
                long expiresAt = ((MultiShareArgs) next).getExpiresAt();
                do {
                    Object next2 = it.next();
                    long expiresAt2 = ((MultiShareArgs) next2).getExpiresAt();
                    if (expiresAt > expiresAt2) {
                        next = next2;
                        expiresAt = expiresAt2;
                    }
                } while (it.hasNext());
                multiShareArgs = next;
            }
        }
        MultiShareArgs multiShareArgs3 = multiShareArgs;
        long expiresAt3 = multiShareArgs3 != null ? multiShareArgs3.getExpiresAt() : -1;
        if (expiresAt3 <= 0) {
            return;
        }
        if (expiresAt3 <= currentTimeMillis) {
            handleMessageExpired();
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        this.handler = handler;
        handler.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                MultiselectForwardFragment.this.handleMessageExpired();
            }
        }, expiresAt3 - currentTimeMillis);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        Handler handler = this.handler;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        SimpleProgressDialog.DismissibleDialog dismissibleDialog = this.dismissibleDialog;
        if (dismissibleDialog != null) {
            dismissibleDialog.dismissNow();
        }
        super.onDestroyView();
    }

    private final void displayFirstSendConfirmation() {
        SignalStore.tooltips().markMultiForwardDialogSeen();
        int messageCount = getMessageCount();
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.MultiselectForwardFragment__faster_forwards).setMessage(R.string.MultiselectForwardFragment__forwarded_messages_are_now).setPositiveButton((CharSequence) getResources().getQuantityString(R.plurals.MultiselectForwardFragment_send_d_messages, messageCount, Integer.valueOf(messageCount)), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MultiselectForwardFragment.m1569displayFirstSendConfirmation$lambda10(MultiselectForwardFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MultiselectForwardFragment.m1570displayFirstSendConfirmation$lambda11(MultiselectForwardFragment.this, dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: displayFirstSendConfirmation$lambda-10 */
    public static final void m1569displayFirstSendConfirmation$lambda10(MultiselectForwardFragment multiselectForwardFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(multiselectForwardFragment, "this$0");
        dialogInterface.dismiss();
        MultiselectForwardViewModel viewModel = multiselectForwardFragment.getViewModel();
        EditText editText = multiselectForwardFragment.addMessage;
        ContactSearchMediator contactSearchMediator = null;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("addMessage");
            editText = null;
        }
        String obj = editText.getText().toString();
        ContactSearchMediator contactSearchMediator2 = multiselectForwardFragment.contactSearchMediator;
        if (contactSearchMediator2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
        } else {
            contactSearchMediator = contactSearchMediator2;
        }
        viewModel.confirmFirstSend(obj, contactSearchMediator.getSelectedContacts());
    }

    /* renamed from: displayFirstSendConfirmation$lambda-11 */
    public static final void m1570displayFirstSendConfirmation$lambda11(MultiselectForwardFragment multiselectForwardFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(multiselectForwardFragment, "this$0");
        dialogInterface.dismiss();
        multiselectForwardFragment.getViewModel().cancelSend();
    }

    private final void onSend(View view) {
        view.setEnabled(false);
        MultiselectForwardViewModel viewModel = getViewModel();
        EditText editText = this.addMessage;
        ContactSearchMediator contactSearchMediator = null;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("addMessage");
            editText = null;
        }
        String obj = editText.getText().toString();
        ContactSearchMediator contactSearchMediator2 = this.contactSearchMediator;
        if (contactSearchMediator2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
        } else {
            contactSearchMediator = contactSearchMediator2;
        }
        viewModel.send(obj, contactSearchMediator.getSelectedContacts());
    }

    private final void displaySafetyNumberConfirmation(List<IdentityRecord> list, List<? extends ContactSearchKey> list2) {
        SafetyNumberBottomSheet.Factory forIdentityRecordsAndDestinations = SafetyNumberBottomSheet.forIdentityRecordsAndDestinations(list, list2);
        FragmentManager childFragmentManager = getChildFragmentManager();
        Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
        forIdentityRecordsAndDestinations.show(childFragmentManager);
    }

    private final void dismissAndShowToast(int i) {
        int messageCount = getMessageCount();
        Callback callback = this.callback;
        Callback callback2 = null;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.onFinishForwardAction();
        SimpleProgressDialog.DismissibleDialog dismissibleDialog = this.dismissibleDialog;
        if (dismissibleDialog != null) {
            dismissibleDialog.dismiss();
        }
        Toast.makeText(requireContext(), requireContext().getResources().getQuantityString(i, messageCount), 0).show();
        Callback callback3 = this.callback;
        if (callback3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
        } else {
            callback2 = callback3;
        }
        callback2.exitFlow();
    }

    private final int getMessageCount() {
        int size = getArgs().getMultiShareArgs().size();
        EditText editText = this.addMessage;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("addMessage");
            editText = null;
        }
        Editable text = editText.getText();
        Intrinsics.checkNotNullExpressionValue(text, "addMessage.text");
        return size + (text.length() > 0 ? 1 : 0);
    }

    public final void handleMessageExpired() {
        Callback callback = this.callback;
        Callback callback2 = null;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.onFinishForwardAction();
        SimpleProgressDialog.DismissibleDialog dismissibleDialog = this.dismissibleDialog;
        if (dismissibleDialog != null) {
            dismissibleDialog.dismiss();
        }
        Toast.makeText(requireContext(), getResources().getQuantityString(R.plurals.MultiselectForwardFragment__couldnt_forward_messages, getArgs().getMultiShareArgs().size()), 1).show();
        Callback callback3 = this.callback;
        if (callback3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
        } else {
            callback2 = callback3;
        }
        callback2.exitFlow();
    }

    private final void dismissWithSelection(Set<? extends ContactSearchKey> set) {
        Callback callback = this.callback;
        Callback callback2 = null;
        if (callback == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback = null;
        }
        callback.onFinishForwardAction();
        SimpleProgressDialog.DismissibleDialog dismissibleDialog = this.dismissibleDialog;
        if (dismissibleDialog != null) {
            dismissibleDialog.dismiss();
        }
        Bundle bundle = new Bundle();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(set, 10));
        for (ContactSearchKey contactSearchKey : set) {
            arrayList.add(contactSearchKey.requireParcelable());
        }
        bundle.putParcelableArrayList(RESULT_SELECTION, new ArrayList<>(arrayList));
        Callback callback3 = this.callback;
        if (callback3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
            callback3 = null;
        }
        callback3.setResult(bundle);
        Callback callback4 = this.callback;
        if (callback4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("callback");
        } else {
            callback2 = callback4;
        }
        callback2.exitFlow();
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void sendAnywayAfterSafetyNumberChangedInBottomSheet(List<? extends ContactSearchKey.RecipientSearchKey> list) {
        Intrinsics.checkNotNullParameter(list, "destinations");
        MultiselectForwardViewModel viewModel = getViewModel();
        EditText editText = this.addMessage;
        if (editText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("addMessage");
            editText = null;
        }
        viewModel.confirmSafetySend(editText.getText().toString(), CollectionsKt___CollectionsKt.toSet(list));
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onMessageResentAfterSafetyNumberChangeInBottomSheet() {
        throw new UnsupportedOperationException();
    }

    @Override // org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet.Callbacks
    public void onCanceled() {
        getViewModel().cancelSend();
    }

    public final Set<ContactSearchKey> filterContacts(View view, Set<? extends ContactSearchKey> set) {
        boolean z;
        boolean z2;
        Stories.MediaTransform.SendRequirements storySendRequirements = getStorySendRequirements();
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = set.iterator();
        while (true) {
            boolean z3 = true;
            z = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            ContactSearchKey contactSearchKey = (ContactSearchKey) next;
            if (!(contactSearchKey instanceof ContactSearchKey.RecipientSearchKey) || !((ContactSearchKey.RecipientSearchKey) contactSearchKey).isStory() || storySendRequirements != Stories.MediaTransform.SendRequirements.CAN_NOT_SEND) {
                z3 = false;
            }
            if (!z3) {
                arrayList.add(next);
            }
        }
        if (view != null) {
            if (!(set instanceof Collection) || !set.isEmpty()) {
                Iterator<T> it2 = set.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    ContactSearchKey contactSearchKey2 = (ContactSearchKey) it2.next();
                    if (!(contactSearchKey2 instanceof ContactSearchKey.RecipientSearchKey) || !((ContactSearchKey.RecipientSearchKey) contactSearchKey2).isStory()) {
                        z2 = false;
                        continue;
                    } else {
                        z2 = true;
                        continue;
                    }
                    if (z2) {
                        z = true;
                        break;
                    }
                }
            }
            if (z) {
                int i = WhenMappings.$EnumSwitchMapping$0[storySendRequirements.ordinal()];
                if (i == 1) {
                    displayTooltip(view, R.string.MultiselectForwardFragment__videos_will_be_trimmed);
                } else if (i == 2) {
                    displayTooltip(view, R.string.MultiselectForwardFragment__videos_sent_to_stories_cant);
                }
            }
        }
        return CollectionsKt___CollectionsKt.toSet(arrayList);
    }

    private final void displayTooltip(View view, int i) {
        TooltipPopup.forTarget(view).setStartMargin((int) DimensionUnit.DP.toPixels(16.0f)).setText(i).setTextColor(ContextCompat.getColor(requireContext(), R.color.signal_colorOnPrimaryContainer)).setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.signal_colorPrimaryContainer)).show(1);
    }

    public final boolean includeSms() {
        return Util.isDefaultSmsProvider(requireContext()) && getArgs().getCanSendToNonPush();
    }

    public final boolean isSelectedMediaValidForStories() {
        List<MultiShareArgs> multiShareArgs = getArgs().getMultiShareArgs();
        if ((multiShareArgs instanceof Collection) && multiShareArgs.isEmpty()) {
            return true;
        }
        for (MultiShareArgs multiShareArgs2 : multiShareArgs) {
            if (!multiShareArgs2.isValidForStories()) {
                return false;
            }
        }
        return true;
    }

    public final boolean isSelectedMediaValidForNonStories() {
        List<MultiShareArgs> multiShareArgs = getArgs().getMultiShareArgs();
        if ((multiShareArgs instanceof Collection) && multiShareArgs.isEmpty()) {
            return true;
        }
        for (MultiShareArgs multiShareArgs2 : multiShareArgs) {
            if (!multiShareArgs2.isValidForNonStories()) {
                return false;
            }
        }
        return true;
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet.Callback
    public void onGroupStoryClicked() {
        new ChooseGroupStoryBottomSheet().show(getParentFragmentManager(), ChooseGroupStoryBottomSheet.GROUP_STORY);
    }

    @Override // org.thoughtcrime.securesms.mediasend.v2.stories.ChooseStoryTypeBottomSheet.Callback
    public void onNewStoryClicked() {
        new CreateStoryFlowDialogFragment().show(getParentFragmentManager(), CreateStoryWithViewersFragment.REQUEST_KEY);
    }

    @Override // org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback
    public void onWrapperDialogFragmentDismissed() {
        ContactSearchMediator contactSearchMediator = this.contactSearchMediator;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
            contactSearchMediator = null;
        }
        contactSearchMediator.refresh();
    }

    @Override // org.thoughtcrime.securesms.stories.settings.privacy.ChooseInitialMyStoryMembershipBottomSheetDialogFragment.Callback
    public void onMyStoryConfigured(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        ContactSearchMediator contactSearchMediator = this.contactSearchMediator;
        ContactSearchMediator contactSearchMediator2 = null;
        if (contactSearchMediator == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
            contactSearchMediator = null;
        }
        contactSearchMediator.setKeysSelected(SetsKt__SetsJVMKt.setOf(new ContactSearchKey.RecipientSearchKey.Story(recipientId)));
        ContactSearchMediator contactSearchMediator3 = this.contactSearchMediator;
        if (contactSearchMediator3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("contactSearchMediator");
        } else {
            contactSearchMediator2 = contactSearchMediator3;
        }
        contactSearchMediator2.refresh();
    }

    /* compiled from: MultiselectForwardFragment.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007J\u0018\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\fH\u0007J \u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u0018\u0010\u0014\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u000b\u001a\u00020\fH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Companion;", "", "()V", "ARGS", "", "DIALOG_TITLE", "RESULT_KEY", "RESULT_SELECTION", "RESULT_SENT", "create", "Landroidx/fragment/app/Fragment;", "multiselectForwardFragmentArgs", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "showBottomSheet", "", "supportFragmentManager", "Landroidx/fragment/app/FragmentManager;", "showDialogFragment", "fragment", "Landroidx/fragment/app/DialogFragment;", "showFullScreen", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void showBottomSheet(FragmentManager fragmentManager, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
            Intrinsics.checkNotNullParameter(fragmentManager, "supportFragmentManager");
            Intrinsics.checkNotNullParameter(multiselectForwardFragmentArgs, "multiselectForwardFragmentArgs");
            showDialogFragment(fragmentManager, new MultiselectForwardBottomSheet(), multiselectForwardFragmentArgs);
        }

        @JvmStatic
        public final void showFullScreen(FragmentManager fragmentManager, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
            Intrinsics.checkNotNullParameter(fragmentManager, "supportFragmentManager");
            Intrinsics.checkNotNullParameter(multiselectForwardFragmentArgs, "multiselectForwardFragmentArgs");
            showDialogFragment(fragmentManager, new MultiselectForwardFullScreenDialogFragment(), multiselectForwardFragmentArgs);
        }

        @JvmStatic
        public final Fragment create(MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
            Intrinsics.checkNotNullParameter(multiselectForwardFragmentArgs, "multiselectForwardFragmentArgs");
            MultiselectForwardFragment multiselectForwardFragment = new MultiselectForwardFragment();
            multiselectForwardFragment.setArguments(BundleKt.bundleOf(TuplesKt.to(MultiselectForwardFragment.ARGS, multiselectForwardFragmentArgs)));
            return multiselectForwardFragment;
        }

        private final void showDialogFragment(FragmentManager fragmentManager, DialogFragment dialogFragment, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
            dialogFragment.setArguments(BundleKt.bundleOf(TuplesKt.to(MultiselectForwardFragment.ARGS, multiselectForwardFragmentArgs), TuplesKt.to(MultiselectForwardFragment.DIALOG_TITLE, Integer.valueOf(multiselectForwardFragmentArgs.getTitle()))));
            dialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }
}
