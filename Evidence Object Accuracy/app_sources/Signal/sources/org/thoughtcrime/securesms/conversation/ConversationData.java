package org.thoughtcrime.securesms.conversation;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: ConversationData.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u001b\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001*BE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\u0006\u0012\u0006\u0010\t\u001a\u00020\u0006\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001d\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0006HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0006HÆ\u0003J\t\u0010 \u001a\u00020\u000bHÆ\u0003J\t\u0010!\u001a\u00020\rHÆ\u0003JY\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00062\b\b\u0002\u0010\b\u001a\u00020\u00062\b\b\u0002\u0010\t\u001a\u00020\u00062\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\rHÆ\u0001J\u0013\u0010#\u001a\u00020\r2\b\u0010$\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010%\u001a\u00020\u0006HÖ\u0001J\u0006\u0010&\u001a\u00020\rJ\u0006\u0010'\u001a\u00020\rJ\t\u0010(\u001a\u00020)HÖ\u0001R\u0011\u0010\b\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0013\u0010\f\u001a\u00020\r8\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0013R\u0011\u0010\t\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0010¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationData;", "", "threadId", "", "lastSeen", "lastSeenPosition", "", "lastScrolledPosition", "jumpToPosition", "threadSize", "messageRequestData", "Lorg/thoughtcrime/securesms/conversation/ConversationData$MessageRequestData;", "showUniversalExpireTimerMessage", "", "(JJIIIILorg/thoughtcrime/securesms/conversation/ConversationData$MessageRequestData;Z)V", "getJumpToPosition", "()I", "getLastScrolledPosition", "getLastSeen", "()J", "getLastSeenPosition", "getMessageRequestData", "()Lorg/thoughtcrime/securesms/conversation/ConversationData$MessageRequestData;", "()Z", "getThreadId", "getThreadSize", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "equals", "other", "hashCode", "shouldJumpToMessage", "shouldScrollToLastSeen", "toString", "", "MessageRequestData", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationData {
    private final int jumpToPosition;
    private final int lastScrolledPosition;
    private final long lastSeen;
    private final int lastSeenPosition;
    private final MessageRequestData messageRequestData;
    private final boolean showUniversalExpireTimerMessage;
    private final long threadId;
    private final int threadSize;

    public final long component1() {
        return this.threadId;
    }

    public final long component2() {
        return this.lastSeen;
    }

    public final int component3() {
        return this.lastSeenPosition;
    }

    public final int component4() {
        return this.lastScrolledPosition;
    }

    public final int component5() {
        return this.jumpToPosition;
    }

    public final int component6() {
        return this.threadSize;
    }

    public final MessageRequestData component7() {
        return this.messageRequestData;
    }

    public final boolean component8() {
        return this.showUniversalExpireTimerMessage;
    }

    public final ConversationData copy(long j, long j2, int i, int i2, int i3, int i4, MessageRequestData messageRequestData, boolean z) {
        Intrinsics.checkNotNullParameter(messageRequestData, "messageRequestData");
        return new ConversationData(j, j2, i, i2, i3, i4, messageRequestData, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConversationData)) {
            return false;
        }
        ConversationData conversationData = (ConversationData) obj;
        return this.threadId == conversationData.threadId && this.lastSeen == conversationData.lastSeen && this.lastSeenPosition == conversationData.lastSeenPosition && this.lastScrolledPosition == conversationData.lastScrolledPosition && this.jumpToPosition == conversationData.jumpToPosition && this.threadSize == conversationData.threadSize && Intrinsics.areEqual(this.messageRequestData, conversationData.messageRequestData) && this.showUniversalExpireTimerMessage == conversationData.showUniversalExpireTimerMessage;
    }

    public int hashCode() {
        int m = ((((((((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.lastSeen)) * 31) + this.lastSeenPosition) * 31) + this.lastScrolledPosition) * 31) + this.jumpToPosition) * 31) + this.threadSize) * 31) + this.messageRequestData.hashCode()) * 31;
        boolean z = this.showUniversalExpireTimerMessage;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return m + i;
    }

    public String toString() {
        return "ConversationData(threadId=" + this.threadId + ", lastSeen=" + this.lastSeen + ", lastSeenPosition=" + this.lastSeenPosition + ", lastScrolledPosition=" + this.lastScrolledPosition + ", jumpToPosition=" + this.jumpToPosition + ", threadSize=" + this.threadSize + ", messageRequestData=" + this.messageRequestData + ", showUniversalExpireTimerMessage=" + this.showUniversalExpireTimerMessage + ')';
    }

    public ConversationData(long j, long j2, int i, int i2, int i3, int i4, MessageRequestData messageRequestData, boolean z) {
        Intrinsics.checkNotNullParameter(messageRequestData, "messageRequestData");
        this.threadId = j;
        this.lastSeen = j2;
        this.lastSeenPosition = i;
        this.lastScrolledPosition = i2;
        this.jumpToPosition = i3;
        this.threadSize = i4;
        this.messageRequestData = messageRequestData;
        this.showUniversalExpireTimerMessage = z;
    }

    public final long getThreadId() {
        return this.threadId;
    }

    public final long getLastSeen() {
        return this.lastSeen;
    }

    public final int getLastSeenPosition() {
        return this.lastSeenPosition;
    }

    public final int getLastScrolledPosition() {
        return this.lastScrolledPosition;
    }

    public final int getJumpToPosition() {
        return this.jumpToPosition;
    }

    public final int getThreadSize() {
        return this.threadSize;
    }

    public final MessageRequestData getMessageRequestData() {
        return this.messageRequestData;
    }

    public final boolean showUniversalExpireTimerMessage() {
        return this.showUniversalExpireTimerMessage;
    }

    public final boolean shouldJumpToMessage() {
        return this.jumpToPosition >= 0;
    }

    public final boolean shouldScrollToLastSeen() {
        return this.lastSeenPosition > 0;
    }

    /* compiled from: ConversationData.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B#\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\b\u001a\u00020\u0003HÆ\u0003J\t\u0010\t\u001a\u00020\u0003HÂ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J'\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\u00032\b\u0010\r\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001J\u0006\u0010\u0010\u001a\u00020\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0007¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationData$MessageRequestData;", "", "isMessageRequestAccepted", "", "groupsInCommon", "isGroup", "(ZZZ)V", "()Z", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "includeWarningUpdateMessage", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MessageRequestData {
        private final boolean groupsInCommon;
        private final boolean isGroup;
        private final boolean isMessageRequestAccepted;

        public MessageRequestData(boolean z) {
            this(z, false, false, 6, null);
        }

        public MessageRequestData(boolean z, boolean z2) {
            this(z, z2, false, 4, null);
        }

        private final boolean component2() {
            return this.groupsInCommon;
        }

        public static /* synthetic */ MessageRequestData copy$default(MessageRequestData messageRequestData, boolean z, boolean z2, boolean z3, int i, Object obj) {
            if ((i & 1) != 0) {
                z = messageRequestData.isMessageRequestAccepted;
            }
            if ((i & 2) != 0) {
                z2 = messageRequestData.groupsInCommon;
            }
            if ((i & 4) != 0) {
                z3 = messageRequestData.isGroup;
            }
            return messageRequestData.copy(z, z2, z3);
        }

        public final boolean component1() {
            return this.isMessageRequestAccepted;
        }

        public final boolean component3() {
            return this.isGroup;
        }

        public final MessageRequestData copy(boolean z, boolean z2, boolean z3) {
            return new MessageRequestData(z, z2, z3);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof MessageRequestData)) {
                return false;
            }
            MessageRequestData messageRequestData = (MessageRequestData) obj;
            return this.isMessageRequestAccepted == messageRequestData.isMessageRequestAccepted && this.groupsInCommon == messageRequestData.groupsInCommon && this.isGroup == messageRequestData.isGroup;
        }

        public int hashCode() {
            boolean z = this.isMessageRequestAccepted;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = i2 * 31;
            boolean z2 = this.groupsInCommon;
            if (z2) {
                z2 = true;
            }
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i5 + i6) * 31;
            boolean z3 = this.isGroup;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return i9 + i;
        }

        public String toString() {
            return "MessageRequestData(isMessageRequestAccepted=" + this.isMessageRequestAccepted + ", groupsInCommon=" + this.groupsInCommon + ", isGroup=" + this.isGroup + ')';
        }

        public MessageRequestData(boolean z, boolean z2, boolean z3) {
            this.isMessageRequestAccepted = z;
            this.groupsInCommon = z2;
            this.isGroup = z3;
        }

        public /* synthetic */ MessageRequestData(boolean z, boolean z2, boolean z3, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3);
        }

        public final boolean isMessageRequestAccepted() {
            return this.isMessageRequestAccepted;
        }

        public final boolean isGroup() {
            return this.isGroup;
        }

        public final boolean includeWarningUpdateMessage() {
            return !this.isMessageRequestAccepted && !this.groupsInCommon;
        }
    }
}
