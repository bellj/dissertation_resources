package org.thoughtcrime.securesms.conversation;

import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda10 implements DialogInterface.OnShowListener {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;
    public final /* synthetic */ AlertDialog f$1;
    public final /* synthetic */ Recipient f$2;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda10(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener, AlertDialog alertDialog, Recipient recipient) {
        this.f$0 = conversationFragmentItemClickListener;
        this.f$1 = alertDialog;
        this.f$2 = recipient;
    }

    @Override // android.content.DialogInterface.OnShowListener
    public final void onShow(DialogInterface dialogInterface) {
        this.f$0.lambda$onSafetyNumberLearnMoreClicked$16(this.f$1, this.f$2, dialogInterface);
    }
}
