package org.thoughtcrime.securesms.conversation.ui.error;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class TrustAndVerifyResult {
    private final List<RecipientId> changedRecipients;
    private final MessageRecord messageRecord;
    private final Result result;

    /* loaded from: classes4.dex */
    public enum Result {
        TRUST_AND_VERIFY,
        TRUST_VERIFY_AND_RESEND,
        UNKNOWN
    }

    public static TrustAndVerifyResult trustAndVerify(List<ChangedRecipient> list) {
        return new TrustAndVerifyResult(list, null, Result.TRUST_AND_VERIFY);
    }

    public static TrustAndVerifyResult trustVerifyAndResend(List<ChangedRecipient> list, MessageRecord messageRecord) {
        return new TrustAndVerifyResult(list, messageRecord, Result.TRUST_VERIFY_AND_RESEND);
    }

    TrustAndVerifyResult(List<ChangedRecipient> list, MessageRecord messageRecord, Result result) {
        this.changedRecipients = Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return TrustAndVerifyResult.lambda$new$0((ChangedRecipient) obj);
            }
        }).toList();
        this.messageRecord = messageRecord;
        this.result = result;
    }

    public static /* synthetic */ RecipientId lambda$new$0(ChangedRecipient changedRecipient) {
        return changedRecipient.getRecipient().getId();
    }

    public List<RecipientId> getChangedRecipients() {
        return this.changedRecipients;
    }

    public MessageRecord getMessageRecord() {
        return this.messageRecord;
    }

    public Result getResult() {
        return this.result;
    }
}
