package org.thoughtcrime.securesms.conversation;

import android.net.Uri;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda18 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;
    public final /* synthetic */ MmsMessageRecord f$1;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda18(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener, MmsMessageRecord mmsMessageRecord) {
        this.f$0 = conversationFragmentItemClickListener;
        this.f$1 = mmsMessageRecord;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.lambda$onViewOnceMessageClicked$5(this.f$1, (Uri) obj);
    }
}
