package org.thoughtcrime.securesms.conversation;

import android.view.MotionEvent;
import androidx.recyclerview.widget.RecyclerView;

/* loaded from: classes4.dex */
public final class ConversationItemTouchListener extends RecyclerView.SimpleOnItemTouchListener {
    private final Callback callback;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onDownEvent(float f, float f2);
    }

    public ConversationItemTouchListener(Callback callback) {
        this.callback = callback;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.SimpleOnItemTouchListener, androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
    public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        if (motionEvent.getAction() != 0) {
            return false;
        }
        this.callback.onDownEvent(motionEvent.getRawX(), motionEvent.getRawY());
        return false;
    }
}
