package org.thoughtcrime.securesms.conversation.colors;

import j$.util.Map;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/* loaded from: classes4.dex */
public enum AvatarColor {
    A100("A100", -1842178),
    A110("A110", -2234372),
    A120("A120", -2561808),
    A130("A130", -3283763),
    A140("A140", -1384200),
    A150("A150", -662530),
    A160("A160", -599828),
    A170("A170", -665641),
    A180("A180", -68144),
    A190("A190", -1382699),
    A200("A200", -2960676),
    A210("A210", -2631719);
    
    private static final Map<String, AvatarColor> NAME_MAP = new HashMap();
    private static final AvatarColor[] RANDOM_OPTIONS;
    public static final AvatarColor UNKNOWN = A210;
    private final int color;
    private final String name;

    static {
        UNKNOWN = A210;
        NAME_MAP = new HashMap();
        AvatarColor[] values = values();
        for (AvatarColor avatarColor : values) {
            NAME_MAP.put(avatarColor.serialize(), avatarColor);
        }
        Map<String, AvatarColor> map = NAME_MAP;
        AvatarColor avatarColor2 = A170;
        map.put("C020", avatarColor2);
        map.put("C030", avatarColor2);
        AvatarColor avatarColor3 = A180;
        map.put("C040", avatarColor3);
        map.put("C050", avatarColor3);
        AvatarColor avatarColor4 = A190;
        map.put("C000", avatarColor4);
        map.put("C060", avatarColor4);
        map.put("C070", avatarColor4);
        AvatarColor avatarColor5 = A130;
        map.put("C080", avatarColor5);
        map.put("C090", avatarColor5);
        map.put("C100", avatarColor5);
        map.put("C110", avatarColor5);
        map.put("C120", avatarColor5);
        map.put("C130", avatarColor5);
        map.put("C140", avatarColor5);
        map.put("C150", avatarColor5);
        map.put("C160", avatarColor5);
        AvatarColor avatarColor6 = A120;
        map.put("C170", avatarColor6);
        map.put("C180", avatarColor6);
        map.put("C190", avatarColor6);
        AvatarColor avatarColor7 = A110;
        map.put("C200", avatarColor7);
        map.put("C210", avatarColor7);
        map.put("C220", avatarColor7);
        AvatarColor avatarColor8 = A100;
        map.put("C230", avatarColor8);
        map.put("C240", avatarColor8);
        map.put("C250", avatarColor8);
        map.put("C260", avatarColor8);
        AvatarColor avatarColor9 = A140;
        map.put("C270", avatarColor9);
        map.put("C280", avatarColor9);
        map.put("C290", avatarColor9);
        AvatarColor avatarColor10 = A150;
        map.put("C300", avatarColor10);
        map.put("C010", avatarColor2);
        map.put("C310", avatarColor10);
        map.put("C320", avatarColor10);
        AvatarColor avatarColor11 = A160;
        map.put("C330", avatarColor11);
        map.put("C340", avatarColor11);
        map.put("C350", avatarColor11);
        map.put("crimson", avatarColor2);
        map.put("vermillion", avatarColor2);
        map.put("burlap", avatarColor4);
        map.put("forest", avatarColor5);
        map.put("wintergreen", avatarColor5);
        map.put("teal", avatarColor6);
        map.put("blue", avatarColor7);
        map.put("indigo", avatarColor8);
        map.put("violet", avatarColor9);
        map.put("plum", avatarColor10);
        map.put("taupe", avatarColor4);
        AvatarColor avatarColor12 = A210;
        map.put("steel", avatarColor12);
        map.put("ultramarine", avatarColor8);
        map.put("unknown", avatarColor12);
        map.put("red", avatarColor2);
        map.put("orange", avatarColor2);
        map.put("deep_orange", avatarColor2);
        map.put("brown", avatarColor4);
        map.put("green", avatarColor5);
        map.put("light_green", avatarColor5);
        map.put("purple", avatarColor9);
        map.put("deep_purple", avatarColor9);
        map.put("pink", avatarColor10);
        map.put("blue_grey", avatarColor4);
        map.put("grey", avatarColor12);
        RANDOM_OPTIONS = new AvatarColor[]{avatarColor8, avatarColor7, avatarColor6, avatarColor5, avatarColor9, avatarColor10, avatarColor11, avatarColor2, avatarColor3, avatarColor4, A200, avatarColor12};
    }

    AvatarColor(String str, int i) {
        this.name = str;
        this.color = i;
    }

    public int colorInt() {
        return this.color;
    }

    public static AvatarColor random() {
        double random = Math.random();
        AvatarColor[] avatarColorArr = RANDOM_OPTIONS;
        double length = (double) avatarColorArr.length;
        Double.isNaN(length);
        return avatarColorArr[(int) Math.floor(random * length)];
    }

    public String serialize() {
        return this.name;
    }

    public static AvatarColor deserialize(String str) {
        AvatarColor avatarColor = (AvatarColor) Map.EL.getOrDefault(NAME_MAP, str, A210);
        Objects.requireNonNull(avatarColor);
        return avatarColor;
    }
}
