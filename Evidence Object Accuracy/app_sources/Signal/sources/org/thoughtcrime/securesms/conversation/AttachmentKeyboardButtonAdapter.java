package org.thoughtcrime.securesms.conversation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class AttachmentKeyboardButtonAdapter extends RecyclerView.Adapter<ButtonViewHolder> {
    private final List<AttachmentKeyboardButton> buttons = new ArrayList();
    private final Listener listener;
    private boolean wallpaperEnabled;

    /* loaded from: classes4.dex */
    public interface Listener {
        void onClick(AttachmentKeyboardButton attachmentKeyboardButton);
    }

    public AttachmentKeyboardButtonAdapter(Listener listener) {
        this.listener = listener;
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return (long) this.buttons.get(i).getTitleRes();
    }

    public ButtonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ButtonViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.attachment_keyboard_button_item, viewGroup, false));
    }

    public void onBindViewHolder(ButtonViewHolder buttonViewHolder, int i) {
        buttonViewHolder.bind(this.buttons.get(i), this.wallpaperEnabled, this.listener);
    }

    public void onViewRecycled(ButtonViewHolder buttonViewHolder) {
        buttonViewHolder.recycle();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.buttons.size();
    }

    public void setButtons(List<AttachmentKeyboardButton> list) {
        this.buttons.clear();
        this.buttons.addAll(list);
        notifyDataSetChanged();
    }

    public void setWallpaperEnabled(boolean z) {
        if (this.wallpaperEnabled != z) {
            this.wallpaperEnabled = z;
            notifyDataSetChanged();
        }
    }

    /* loaded from: classes4.dex */
    public static class ButtonViewHolder extends RecyclerView.ViewHolder {
        private final ImageView image;
        private final TextView title;

        public ButtonViewHolder(View view) {
            super(view);
            this.image = (ImageView) view.findViewById(R.id.icon);
            this.title = (TextView) view.findViewById(R.id.label);
        }

        void bind(AttachmentKeyboardButton attachmentKeyboardButton, boolean z, Listener listener) {
            this.image.setImageResource(attachmentKeyboardButton.getIconRes());
            this.title.setText(attachmentKeyboardButton.getTitleRes());
            this.itemView.setOnClickListener(new AttachmentKeyboardButtonAdapter$ButtonViewHolder$$ExternalSyntheticLambda0(listener, attachmentKeyboardButton));
        }

        void recycle() {
            this.itemView.setOnClickListener(null);
        }
    }
}
