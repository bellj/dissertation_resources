package org.thoughtcrime.securesms.conversation;

import java.util.List;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$UnverifiedDismissedListener$$ExternalSyntheticLambda0 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ List f$0;

    public /* synthetic */ ConversationParentFragment$UnverifiedDismissedListener$$ExternalSyntheticLambda0(List list) {
        this.f$0 = list;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return ConversationParentFragment.UnverifiedDismissedListener.lambda$onDismissed$0(this.f$0);
    }
}
