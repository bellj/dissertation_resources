package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.View;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.conversation.ConversationItemTouchListener;
import org.thoughtcrime.securesms.util.AccessibilityUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public class ConversationItemSwipeCallback extends ItemTouchHelper.SimpleCallback {
    private static float SWIPE_SUCCESS_DX = ConversationSwipeAnimationHelper.TRIGGER_DX;
    private static long SWIPE_SUCCESS_VIBE_TIME_MS = 10;
    private boolean canTriggerSwipe;
    private final ConversationItemTouchListener itemTouchListener = new ConversationItemTouchListener(new ConversationItemTouchListener.Callback() { // from class: org.thoughtcrime.securesms.conversation.ConversationItemSwipeCallback$$ExternalSyntheticLambda0
        @Override // org.thoughtcrime.securesms.conversation.ConversationItemTouchListener.Callback
        public final void onDownEvent(float f, float f2) {
            ConversationItemSwipeCallback.this.updateLatestDownCoordinate(f, f2);
        }
    });
    private float latestDownX;
    private float latestDownY;
    private final OnSwipeListener onSwipeListener;
    private boolean shouldTriggerSwipeFeedback;
    private final SwipeAvailabilityProvider swipeAvailabilityProvider;
    private boolean swipeBack;

    /* loaded from: classes4.dex */
    public interface OnSwipeListener {
        void onSwipe(ConversationMessage conversationMessage);
    }

    /* loaded from: classes4.dex */
    public interface SwipeAvailabilityProvider {
        boolean isSwipeAvailable(ConversationMessage conversationMessage);
    }

    private static boolean sameSign(float f, float f2) {
        return f * f2 > 0.0f;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
        return false;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int i) {
    }

    public ConversationItemSwipeCallback(SwipeAvailabilityProvider swipeAvailabilityProvider, OnSwipeListener onSwipeListener) {
        super(0, 32);
        this.swipeAvailabilityProvider = swipeAvailabilityProvider;
        this.onSwipeListener = onSwipeListener;
        this.shouldTriggerSwipeFeedback = true;
        this.canTriggerSwipe = true;
    }

    public void attachToRecyclerView(RecyclerView recyclerView) {
        recyclerView.addOnItemTouchListener(this.itemTouchListener);
        new ItemTouchHelper(this).attachToRecyclerView(recyclerView);
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.SimpleCallback
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (cannotSwipeViewHolder(viewHolder)) {
            return 0;
        }
        return super.getSwipeDirs(recyclerView, viewHolder);
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public int convertToAbsoluteDirection(int i, int i2) {
        if (!this.swipeBack) {
            return super.convertToAbsoluteDirection(i, i2);
        }
        this.swipeBack = false;
        return 0;
    }

    @Override // androidx.recyclerview.widget.ItemTouchHelper.Callback
    public void onChildDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f, float f2, int i, boolean z) {
        if (!cannotSwipeViewHolder(viewHolder)) {
            float signFromDirection = getSignFromDirection(viewHolder.itemView);
            boolean sameSign = sameSign(f, signFromDirection);
            if (i == 1 && sameSign) {
                ConversationSwipeAnimationHelper.update((ConversationItem) viewHolder.itemView, Math.abs(f), signFromDirection);
                recyclerView.invalidate();
                handleSwipeFeedback((ConversationItem) viewHolder.itemView, Math.abs(f));
                if (this.canTriggerSwipe) {
                    setTouchListener(recyclerView, viewHolder, Math.abs(f));
                }
            } else if (i == 0 || f == 0.0f) {
                ConversationSwipeAnimationHelper.update((ConversationItem) viewHolder.itemView, 0.0f, 1.0f);
                recyclerView.invalidate();
            }
            if (f == 0.0f) {
                this.shouldTriggerSwipeFeedback = true;
                this.canTriggerSwipe = true;
            }
        }
    }

    private void handleSwipeFeedback(ConversationItem conversationItem, float f) {
        if (f > SWIPE_SUCCESS_DX && this.shouldTriggerSwipeFeedback) {
            vibrate(conversationItem.getContext());
            ConversationSwipeAnimationHelper.trigger(conversationItem);
            this.shouldTriggerSwipeFeedback = false;
        }
    }

    private void onSwiped(RecyclerView.ViewHolder viewHolder) {
        if (!cannotSwipeViewHolder(viewHolder)) {
            this.onSwipeListener.onSwipe(((ConversationItem) viewHolder.itemView).getConversationMessage());
        }
    }

    private void setTouchListener(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f) {
        recyclerView.setOnTouchListener(new View.OnTouchListener(recyclerView, viewHolder, f) { // from class: org.thoughtcrime.securesms.conversation.ConversationItemSwipeCallback$$ExternalSyntheticLambda1
            public final /* synthetic */ RecyclerView f$1;
            public final /* synthetic */ RecyclerView.ViewHolder f$2;
            public final /* synthetic */ float f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                return ConversationItemSwipeCallback.this.lambda$setTouchListener$0(this.f$1, this.f$2, this.f$3, view, motionEvent);
            }
        });
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000b, code lost:
        if (r5 != 3) goto L_0x001b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ boolean lambda$setTouchListener$0(androidx.recyclerview.widget.RecyclerView r2, androidx.recyclerview.widget.RecyclerView.ViewHolder r3, float r4, android.view.View r5, android.view.MotionEvent r6) {
        /*
            r1 = this;
            int r5 = r6.getAction()
            r6 = 0
            r0 = 1
            if (r5 == 0) goto L_0x0019
            if (r5 == r0) goto L_0x000e
            r4 = 3
            if (r5 == r4) goto L_0x0011
            goto L_0x001b
        L_0x000e:
            r1.handleTouchActionUp(r2, r3, r4)
        L_0x0011:
            r1.swipeBack = r0
            r1.shouldTriggerSwipeFeedback = r6
            r1.resetProgressIfAnimationsDisabled(r2, r3)
            goto L_0x001b
        L_0x0019:
            r1.shouldTriggerSwipeFeedback = r0
        L_0x001b:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationItemSwipeCallback.lambda$setTouchListener$0(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$ViewHolder, float, android.view.View, android.view.MotionEvent):boolean");
    }

    private void handleTouchActionUp(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float f) {
        if (f > SWIPE_SUCCESS_DX) {
            this.canTriggerSwipe = false;
            onSwiped(viewHolder);
            if (this.shouldTriggerSwipeFeedback) {
                vibrate(viewHolder.itemView.getContext());
            }
            recyclerView.setOnTouchListener(null);
        }
        recyclerView.cancelPendingInputEvents();
    }

    private void resetProgressIfAnimationsDisabled(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (AccessibilityUtil.areAnimationsDisabled(viewHolder.itemView.getContext())) {
            View view = viewHolder.itemView;
            ConversationSwipeAnimationHelper.update((ConversationItem) view, 0.0f, getSignFromDirection(view));
            recyclerView.invalidate();
        }
    }

    private boolean cannotSwipeViewHolder(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        if (!(view instanceof ConversationItem)) {
            return true;
        }
        ConversationItem conversationItem = (ConversationItem) view;
        if (!this.swipeAvailabilityProvider.isSwipeAvailable(conversationItem.getConversationMessage()) || conversationItem.disallowSwipe(this.latestDownX, this.latestDownY)) {
            return true;
        }
        return false;
    }

    public void updateLatestDownCoordinate(float f, float f2) {
        this.latestDownX = f;
        this.latestDownY = f2;
    }

    private static float getSignFromDirection(View view) {
        return view.getLayoutDirection() == 1 ? -1.0f : 1.0f;
    }

    private static void vibrate(Context context) {
        Vibrator vibrator = ServiceUtil.getVibrator(context);
        if (vibrator != null) {
            vibrator.vibrate(SWIPE_SUCCESS_VIBE_TIME_MS);
        }
    }
}
