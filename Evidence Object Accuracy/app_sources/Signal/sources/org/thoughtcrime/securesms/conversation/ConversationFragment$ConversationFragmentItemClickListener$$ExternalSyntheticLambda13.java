package org.thoughtcrime.securesms.conversation;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda13 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ Recipient f$0;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda13(Recipient recipient) {
        this.f$0 = recipient;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return ConversationFragment.ConversationFragmentItemClickListener.lambda$onSafetyNumberLearnMoreClicked$12(this.f$0);
    }
}
