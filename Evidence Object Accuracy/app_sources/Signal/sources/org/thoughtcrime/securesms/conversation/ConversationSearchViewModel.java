package org.thoughtcrime.securesms.conversation;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.search.MessageResult;
import org.thoughtcrime.securesms.search.SearchRepository;
import org.thoughtcrime.securesms.util.Debouncer;

/* loaded from: classes4.dex */
public class ConversationSearchViewModel extends ViewModel {
    private String activeQuery;
    private long activeThreadId;
    private final Debouncer debouncer = new Debouncer(500);
    private boolean firstSearch;
    private final MutableLiveData<SearchResult> result = new MutableLiveData<>();
    private boolean searchOpen;
    private final SearchRepository searchRepository;

    public ConversationSearchViewModel(String str) {
        this.searchRepository = new SearchRepository(str);
    }

    public LiveData<SearchResult> getSearchResults() {
        return this.result;
    }

    public void onQueryUpdated(String str, long j, boolean z) {
        if (this.firstSearch && str.length() < 2) {
            this.result.postValue(new SearchResult(Collections.emptyList(), 0));
        } else if (!str.equals(this.activeQuery) || z) {
            updateQuery(str, j);
        }
    }

    public void onMissingResult() {
        String str = this.activeQuery;
        if (str != null) {
            updateQuery(str, this.activeThreadId);
        }
    }

    public void onMoveUp() {
        if (this.result.getValue() != null) {
            this.debouncer.clear();
            List<MessageResult> results = this.result.getValue().getResults();
            this.result.setValue(new SearchResult(results, Math.min(this.result.getValue().getPosition() + 1, results.size() - 1)));
        }
    }

    public void onMoveDown() {
        if (this.result.getValue() != null) {
            this.debouncer.clear();
            this.result.setValue(new SearchResult(this.result.getValue().getResults(), Math.max(this.result.getValue().getPosition() - 1, 0)));
        }
    }

    public void onSearchOpened() {
        this.searchOpen = true;
        this.firstSearch = true;
    }

    public void onSearchClosed() {
        this.searchOpen = false;
        this.debouncer.clear();
    }

    private void updateQuery(String str, long j) {
        this.activeQuery = str;
        this.activeThreadId = j;
        this.debouncer.publish(new Runnable(str, j) { // from class: org.thoughtcrime.securesms.conversation.ConversationSearchViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSearchViewModel.this.lambda$updateQuery$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$updateQuery$2(String str, long j) {
        this.firstSearch = false;
        this.searchRepository.query(str, j, new SearchRepository.Callback(str) { // from class: org.thoughtcrime.securesms.conversation.ConversationSearchViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.search.SearchRepository.Callback
            public final void onResult(Object obj) {
                ConversationSearchViewModel.this.lambda$updateQuery$1(this.f$1, (List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$updateQuery$1(String str, List list) {
        ThreadUtil.runOnMain(new Runnable(str, list) { // from class: org.thoughtcrime.securesms.conversation.ConversationSearchViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSearchViewModel.this.lambda$updateQuery$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$updateQuery$0(String str, List list) {
        if (this.searchOpen && str.equals(this.activeQuery)) {
            this.result.setValue(new SearchResult(list, 0));
        }
    }

    /* loaded from: classes4.dex */
    public static class SearchResult {
        private final int position;
        private final List<MessageResult> results;

        SearchResult(List<MessageResult> list, int i) {
            this.results = list;
            this.position = i;
        }

        public List<MessageResult> getResults() {
            return this.results;
        }

        public int getPosition() {
            return this.position;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final String noteToSelfTitle;

        public Factory(String str) {
            this.noteToSelfTitle = str;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ConversationSearchViewModel(this.noteToSelfTitle));
        }
    }
}
