package org.thoughtcrime.securesms.conversation;

import androidx.lifecycle.MutableLiveData;
import org.thoughtcrime.securesms.stickers.StickerSearchRepository;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationStickerViewModel$$ExternalSyntheticLambda0 implements StickerSearchRepository.Callback {
    public final /* synthetic */ MutableLiveData f$0;

    public /* synthetic */ ConversationStickerViewModel$$ExternalSyntheticLambda0(MutableLiveData mutableLiveData) {
        this.f$0 = mutableLiveData;
    }

    @Override // org.thoughtcrime.securesms.stickers.StickerSearchRepository.Callback
    public final void onResult(Object obj) {
        this.f$0.postValue((Boolean) obj);
    }
}
