package org.thoughtcrime.securesms.conversation;

import android.view.View;
import kotlin.jvm.functions.Function1;
import org.thoughtcrime.securesms.conversation.ConversationFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$3$$ExternalSyntheticLambda0 implements Function1 {
    public final /* synthetic */ ConversationFragment.AnonymousClass3 f$0;
    public final /* synthetic */ int f$1;

    public /* synthetic */ ConversationFragment$3$$ExternalSyntheticLambda0(ConversationFragment.AnonymousClass3 r1, int i) {
        this.f$0 = r1;
        this.f$1 = i;
    }

    @Override // kotlin.jvm.functions.Function1
    public final Object invoke(Object obj) {
        return this.f$0.lambda$onSuccess$0(this.f$1, (View) obj);
    }
}
