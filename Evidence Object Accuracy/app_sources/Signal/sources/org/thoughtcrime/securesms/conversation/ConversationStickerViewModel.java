package org.thoughtcrime.securesms.conversation;

import android.app.Application;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.stickers.StickerSearchRepository;
import org.thoughtcrime.securesms.util.Throttler;

/* loaded from: classes4.dex */
public class ConversationStickerViewModel extends ViewModel {
    private final Throttler availabilityThrottler;
    private final DatabaseObserver.Observer packObserver;
    private final StickerSearchRepository repository;
    private final MutableLiveData<List<StickerRecord>> stickers;
    private final MutableLiveData<Boolean> stickersAvailable;

    private ConversationStickerViewModel(Application application, StickerSearchRepository stickerSearchRepository) {
        this.repository = stickerSearchRepository;
        this.stickers = new MutableLiveData<>();
        this.stickersAvailable = new MutableLiveData<>();
        this.availabilityThrottler = new Throttler(500);
        ConversationStickerViewModel$$ExternalSyntheticLambda3 conversationStickerViewModel$$ExternalSyntheticLambda3 = new DatabaseObserver.Observer(stickerSearchRepository) { // from class: org.thoughtcrime.securesms.conversation.ConversationStickerViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ StickerSearchRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ConversationStickerViewModel.this.lambda$new$1(this.f$1);
            }
        };
        this.packObserver = conversationStickerViewModel$$ExternalSyntheticLambda3;
        ApplicationDependencies.getDatabaseObserver().registerStickerPackObserver(conversationStickerViewModel$$ExternalSyntheticLambda3);
    }

    public /* synthetic */ void lambda$new$0(StickerSearchRepository stickerSearchRepository) {
        MutableLiveData<Boolean> mutableLiveData = this.stickersAvailable;
        Objects.requireNonNull(mutableLiveData);
        stickerSearchRepository.getStickerFeatureAvailability(new ConversationStickerViewModel$$ExternalSyntheticLambda0(mutableLiveData));
    }

    public /* synthetic */ void lambda$new$1(StickerSearchRepository stickerSearchRepository) {
        this.availabilityThrottler.publish(new Runnable(stickerSearchRepository) { // from class: org.thoughtcrime.securesms.conversation.ConversationStickerViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ StickerSearchRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationStickerViewModel.this.lambda$new$0(this.f$1);
            }
        });
    }

    public LiveData<List<StickerRecord>> getStickerResults() {
        return this.stickers;
    }

    public LiveData<Boolean> getStickersAvailability() {
        StickerSearchRepository stickerSearchRepository = this.repository;
        MutableLiveData<Boolean> mutableLiveData = this.stickersAvailable;
        Objects.requireNonNull(mutableLiveData);
        stickerSearchRepository.getStickerFeatureAvailability(new ConversationStickerViewModel$$ExternalSyntheticLambda0(mutableLiveData));
        return this.stickersAvailable;
    }

    public void onInputTextUpdated(String str) {
        if (TextUtils.isEmpty(str) || str.length() > EmojiSource.getLatest().getMaxEmojiLength()) {
            this.stickers.setValue(Collections.emptyList());
            return;
        }
        StickerSearchRepository stickerSearchRepository = this.repository;
        MutableLiveData<List<StickerRecord>> mutableLiveData = this.stickers;
        Objects.requireNonNull(mutableLiveData);
        stickerSearchRepository.searchByEmoji(str, new StickerSearchRepository.Callback() { // from class: org.thoughtcrime.securesms.conversation.ConversationStickerViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.stickers.StickerSearchRepository.Callback
            public final void onResult(Object obj) {
                MutableLiveData.this.postValue((List) obj);
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.packObserver);
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final Application application;
        private final StickerSearchRepository repository;

        public Factory(Application application, StickerSearchRepository stickerSearchRepository) {
            this.application = application;
            this.repository = stickerSearchRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ConversationStickerViewModel(this.application, this.repository));
        }
    }
}
