package org.thoughtcrime.securesms.conversation;

import android.animation.ValueAnimator;
import org.thoughtcrime.securesms.conversation.ConversationItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationItem$1$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ ConversationItem.AnonymousClass1 f$0;

    public /* synthetic */ ConversationItem$1$$ExternalSyntheticLambda0(ConversationItem.AnonymousClass1 r1) {
        this.f$0 = r1;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.lambda$run$0(valueAnimator);
    }
}
