package org.thoughtcrime.securesms.conversation.colors.ui;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: ChatColorSelectionViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "wallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorSelectionViewModel$refresh$1 extends Lambda implements Function1<ChatWallpaper, Unit> {
    final /* synthetic */ ChatColorSelectionViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatColorSelectionViewModel$refresh$1(ChatColorSelectionViewModel chatColorSelectionViewModel) {
        super(1);
        this.this$0 = chatColorSelectionViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ChatWallpaper chatWallpaper) {
        invoke(chatWallpaper);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final ChatColorSelectionState m1518invoke$lambda0(ChatWallpaper chatWallpaper, ChatColorSelectionState chatColorSelectionState) {
        Intrinsics.checkNotNullExpressionValue(chatColorSelectionState, "it");
        return ChatColorSelectionState.copy$default(chatColorSelectionState, chatWallpaper, null, null, 6, null);
    }

    public final void invoke(ChatWallpaper chatWallpaper) {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel$refresh$1$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatColorSelectionViewModel$refresh$1.m1518invoke$lambda0(ChatWallpaper.this, (ChatColorSelectionState) obj);
            }
        });
    }
}
