package org.thoughtcrime.securesms.conversation;

import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SelectedConversationModel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\u0006\u0010\r\u001a\u00020\u000e\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\u0002\u0010\u0011J\t\u0010 \u001a\u00020\u0003HÆ\u0003J\t\u0010!\u001a\u00020\u0005HÆ\u0003J\t\u0010\"\u001a\u00020\u0005HÆ\u0003J\t\u0010#\u001a\u00020\u0005HÆ\u0003J\t\u0010$\u001a\u00020\u0005HÆ\u0003J\t\u0010%\u001a\u00020\nHÆ\u0003J\u000b\u0010&\u001a\u0004\u0018\u00010\fHÆ\u0003J\t\u0010'\u001a\u00020\u000eHÆ\u0003J\u000b\u0010(\u001a\u0004\u0018\u00010\u0010HÆ\u0003Jg\u0010)\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00052\b\b\u0002\u0010\b\u001a\u00020\u00052\b\b\u0002\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\r\u001a\u00020\u000e2\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÆ\u0001J\u0013\u0010*\u001a\u00020\u000e2\b\u0010+\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010,\u001a\u00020\nHÖ\u0001J\t\u0010-\u001a\u00020.HÖ\u0001R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0007\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\b\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0019R\u0013\u0010\u000f\u001a\u0004\u0018\u00010\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u001dR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0019R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0019¨\u0006/"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/SelectedConversationModel;", "", "bitmap", "Landroid/graphics/Bitmap;", "itemX", "", "itemY", "bubbleX", "bubbleY", "bubbleWidth", "", "audioUri", "Landroid/net/Uri;", "isOutgoing", "", "focusedView", "Landroid/view/View;", "(Landroid/graphics/Bitmap;FFFFILandroid/net/Uri;ZLandroid/view/View;)V", "getAudioUri", "()Landroid/net/Uri;", "getBitmap", "()Landroid/graphics/Bitmap;", "getBubbleWidth", "()I", "getBubbleX", "()F", "getBubbleY", "getFocusedView", "()Landroid/view/View;", "()Z", "getItemX", "getItemY", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SelectedConversationModel {
    private final Uri audioUri;
    private final Bitmap bitmap;
    private final int bubbleWidth;
    private final float bubbleX;
    private final float bubbleY;
    private final View focusedView;
    private final boolean isOutgoing;
    private final float itemX;
    private final float itemY;

    public final Bitmap component1() {
        return this.bitmap;
    }

    public final float component2() {
        return this.itemX;
    }

    public final float component3() {
        return this.itemY;
    }

    public final float component4() {
        return this.bubbleX;
    }

    public final float component5() {
        return this.bubbleY;
    }

    public final int component6() {
        return this.bubbleWidth;
    }

    public final Uri component7() {
        return this.audioUri;
    }

    public final boolean component8() {
        return this.isOutgoing;
    }

    public final View component9() {
        return this.focusedView;
    }

    public final SelectedConversationModel copy(Bitmap bitmap, float f, float f2, float f3, float f4, int i, Uri uri, boolean z, View view) {
        Intrinsics.checkNotNullParameter(bitmap, "bitmap");
        return new SelectedConversationModel(bitmap, f, f2, f3, f4, i, uri, z, view);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SelectedConversationModel)) {
            return false;
        }
        SelectedConversationModel selectedConversationModel = (SelectedConversationModel) obj;
        return Intrinsics.areEqual(this.bitmap, selectedConversationModel.bitmap) && Intrinsics.areEqual(Float.valueOf(this.itemX), Float.valueOf(selectedConversationModel.itemX)) && Intrinsics.areEqual(Float.valueOf(this.itemY), Float.valueOf(selectedConversationModel.itemY)) && Intrinsics.areEqual(Float.valueOf(this.bubbleX), Float.valueOf(selectedConversationModel.bubbleX)) && Intrinsics.areEqual(Float.valueOf(this.bubbleY), Float.valueOf(selectedConversationModel.bubbleY)) && this.bubbleWidth == selectedConversationModel.bubbleWidth && Intrinsics.areEqual(this.audioUri, selectedConversationModel.audioUri) && this.isOutgoing == selectedConversationModel.isOutgoing && Intrinsics.areEqual(this.focusedView, selectedConversationModel.focusedView);
    }

    public int hashCode() {
        int hashCode = ((((((((((this.bitmap.hashCode() * 31) + Float.floatToIntBits(this.itemX)) * 31) + Float.floatToIntBits(this.itemY)) * 31) + Float.floatToIntBits(this.bubbleX)) * 31) + Float.floatToIntBits(this.bubbleY)) * 31) + this.bubbleWidth) * 31;
        Uri uri = this.audioUri;
        int i = 0;
        int hashCode2 = (hashCode + (uri == null ? 0 : uri.hashCode())) * 31;
        boolean z = this.isOutgoing;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode2 + i2) * 31;
        View view = this.focusedView;
        if (view != null) {
            i = view.hashCode();
        }
        return i5 + i;
    }

    public String toString() {
        return "SelectedConversationModel(bitmap=" + this.bitmap + ", itemX=" + this.itemX + ", itemY=" + this.itemY + ", bubbleX=" + this.bubbleX + ", bubbleY=" + this.bubbleY + ", bubbleWidth=" + this.bubbleWidth + ", audioUri=" + this.audioUri + ", isOutgoing=" + this.isOutgoing + ", focusedView=" + this.focusedView + ')';
    }

    public SelectedConversationModel(Bitmap bitmap, float f, float f2, float f3, float f4, int i, Uri uri, boolean z, View view) {
        Intrinsics.checkNotNullParameter(bitmap, "bitmap");
        this.bitmap = bitmap;
        this.itemX = f;
        this.itemY = f2;
        this.bubbleX = f3;
        this.bubbleY = f4;
        this.bubbleWidth = i;
        this.audioUri = uri;
        this.isOutgoing = z;
        this.focusedView = view;
    }

    public /* synthetic */ SelectedConversationModel(Bitmap bitmap, float f, float f2, float f3, float f4, int i, Uri uri, boolean z, View view, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(bitmap, f, f2, f3, f4, i, (i2 & 64) != 0 ? null : uri, z, view);
    }

    public final Bitmap getBitmap() {
        return this.bitmap;
    }

    public final float getItemX() {
        return this.itemX;
    }

    public final float getItemY() {
        return this.itemY;
    }

    public final float getBubbleX() {
        return this.bubbleX;
    }

    public final float getBubbleY() {
        return this.bubbleY;
    }

    public final int getBubbleWidth() {
        return this.bubbleWidth;
    }

    public final Uri getAudioUri() {
        return this.audioUri;
    }

    public final boolean isOutgoing() {
        return this.isOutgoing;
    }

    public final View getFocusedView() {
        return this.focusedView;
    }
}
