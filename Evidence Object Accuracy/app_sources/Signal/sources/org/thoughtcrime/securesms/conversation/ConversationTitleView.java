package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.Comparator;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.view.AvatarView;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ConversationTitleView extends RelativeLayout {
    private static final String STATE_IS_SELF;
    private static final String STATE_ROOT;
    private AvatarView avatar;
    private BadgeImageView badge;
    private View expirationBadgeContainer;
    private TextView expirationBadgeTime;
    private boolean isSelf;
    private TextView subtitle;
    private View subtitleContainer;
    private TextView title;
    private ImageView verified;
    private View verifiedSubtitle;

    public ConversationTitleView(Context context) {
        this(context, null);
    }

    public ConversationTitleView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.title = (TextView) findViewById(R.id.title);
        this.badge = (BadgeImageView) findViewById(R.id.badge);
        this.subtitle = (TextView) findViewById(R.id.subtitle);
        this.verified = (ImageView) findViewById(R.id.verified_indicator);
        this.subtitleContainer = findViewById(R.id.subtitle_container);
        this.verifiedSubtitle = findViewById(R.id.verified_subtitle);
        this.avatar = (AvatarView) findViewById(R.id.contact_photo_image);
        this.expirationBadgeContainer = findViewById(R.id.expiration_badge_container);
        this.expirationBadgeTime = (TextView) findViewById(R.id.expiration_badge);
        ViewUtil.setTextViewGravityStart(this.title, getContext());
        ViewUtil.setTextViewGravityStart(this.subtitle, getContext());
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(STATE_ROOT, super.onSaveInstanceState());
        bundle.putBoolean(STATE_IS_SELF, this.isSelf);
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            super.onRestoreInstanceState(bundle.getParcelable(STATE_ROOT));
            this.isSelf = bundle.getBoolean(STATE_IS_SELF, false);
            return;
        }
        super.onRestoreInstanceState(parcelable);
    }

    public void showExpiring(LiveRecipient liveRecipient) {
        this.isSelf = liveRecipient.get().isSelf();
        this.expirationBadgeTime.setText(ExpirationUtil.getExpirationAbbreviatedDisplayValue(getContext(), liveRecipient.get().getExpiresInSeconds()));
        this.expirationBadgeContainer.setVisibility(0);
        updateSubtitleVisibility();
    }

    public void clearExpiring() {
        this.expirationBadgeContainer.setVisibility(8);
        updateSubtitleVisibility();
    }

    public void setTitle(GlideRequests glideRequests, Recipient recipient) {
        Drawable drawable;
        this.isSelf = recipient != null && recipient.isSelf();
        this.subtitleContainer.setVisibility(0);
        if (recipient == null) {
            setComposeTitle();
        } else {
            setRecipientTitle(recipient);
        }
        if (recipient != null && recipient.isBlocked()) {
            drawable = ContextUtil.requireDrawable(getContext(), R.drawable.ic_block_white_18dp);
        } else if (recipient == null || !recipient.isMuted()) {
            drawable = null;
        } else {
            drawable = ContextUtil.requireDrawable(getContext(), R.drawable.ic_bell_disabled_16);
            drawable.setBounds(0, 0, ViewUtil.dpToPx(18), ViewUtil.dpToPx(18));
        }
        Drawable drawable2 = (recipient == null || !recipient.isSystemContact() || recipient.isSelf()) ? null : ContextCompat.getDrawable(getContext(), R.drawable.ic_profile_circle_outline_16);
        if (drawable != null) {
            drawable = DrawableUtil.tint(drawable, ContextCompat.getColor(getContext(), R.color.signal_inverse_transparent_80));
        }
        if (drawable2 != null) {
            drawable2 = DrawableUtil.tint(drawable2, ContextCompat.getColor(getContext(), R.color.signal_inverse_transparent_80));
        }
        if (recipient != null && recipient.showVerified()) {
            drawable2 = ContextUtil.requireDrawable(getContext(), R.drawable.ic_official_24);
        }
        this.title.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, (Drawable) null, drawable2, (Drawable) null);
        if (recipient != null) {
            this.avatar.displayChatAvatar(glideRequests, recipient, false);
        }
        if (recipient == null || recipient.isSelf()) {
            this.badge.setBadgeFromRecipient(null);
        } else {
            this.badge.setBadgeFromRecipient(recipient);
        }
        updateVerifiedSubtitleVisibility();
    }

    public void setStoryRingFromState(StoryViewState storyViewState) {
        this.avatar.setStoryRingFromState(storyViewState);
    }

    public void setOnStoryRingClickListener(View.OnClickListener onClickListener) {
        this.avatar.setOnClickListener(new View.OnClickListener(onClickListener) { // from class: org.thoughtcrime.securesms.conversation.ConversationTitleView$$ExternalSyntheticLambda2
            public final /* synthetic */ View.OnClickListener f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationTitleView.this.lambda$setOnStoryRingClickListener$0(this.f$1, view);
            }
        });
    }

    public /* synthetic */ void lambda$setOnStoryRingClickListener$0(View.OnClickListener onClickListener, View view) {
        if (this.avatar.hasStory()) {
            onClickListener.onClick(view);
        } else {
            performClick();
        }
    }

    public void setVerified(boolean z) {
        this.verified.setVisibility(z ? 0 : 8);
        updateVerifiedSubtitleVisibility();
    }

    private void setComposeTitle() {
        this.title.setText(R.string.ConversationActivity_compose_message);
        this.subtitle.setText((CharSequence) null);
        updateSubtitleVisibility();
    }

    private void setRecipientTitle(Recipient recipient) {
        if (recipient.isGroup()) {
            setGroupRecipientTitle(recipient);
        } else if (recipient.isSelf()) {
            setSelfTitle();
        } else {
            setIndividualRecipientTitle(recipient);
        }
    }

    private void setGroupRecipientTitle(Recipient recipient) {
        this.title.setText(recipient.getDisplayName(getContext()));
        this.subtitle.setText((CharSequence) Stream.of(recipient.getParticipantIds()).limit(10).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).sorted(new Comparator() { // from class: org.thoughtcrime.securesms.conversation.ConversationTitleView$$ExternalSyntheticLambda0
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return ConversationTitleView.lambda$setGroupRecipientTitle$1((Recipient) obj, (Recipient) obj2);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationTitleView$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationTitleView.this.lambda$setGroupRecipientTitle$2((Recipient) obj);
            }
        }).collect(Collectors.joining(", ")));
        updateSubtitleVisibility();
    }

    public static /* synthetic */ int lambda$setGroupRecipientTitle$1(Recipient recipient, Recipient recipient2) {
        return Boolean.compare(recipient.isSelf(), recipient2.isSelf());
    }

    public /* synthetic */ String lambda$setGroupRecipientTitle$2(Recipient recipient) {
        if (recipient.isSelf()) {
            return getResources().getString(R.string.ConversationTitleView_you);
        }
        return recipient.getDisplayName(getContext());
    }

    private void setSelfTitle() {
        this.title.setText(R.string.note_to_self);
        updateSubtitleVisibility();
    }

    private void setIndividualRecipientTitle(Recipient recipient) {
        this.title.setText(recipient.getDisplayNameOrUsername(getContext()));
        this.subtitle.setText((CharSequence) null);
        updateSubtitleVisibility();
    }

    private void updateVerifiedSubtitleVisibility() {
        this.verifiedSubtitle.setVisibility((this.isSelf || this.subtitle.getVisibility() == 0 || this.verified.getVisibility() != 0) ? 8 : 0);
    }

    private void updateSubtitleVisibility() {
        this.subtitle.setVisibility((this.isSelf || this.expirationBadgeContainer.getVisibility() == 0 || TextUtils.isEmpty(this.subtitle.getText())) ? 8 : 0);
        updateVerifiedSubtitleVisibility();
    }
}
