package org.thoughtcrime.securesms.conversation.ui.error;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeAdapter;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeViewModel;
import org.thoughtcrime.securesms.conversation.ui.error.TrustAndVerifyResult;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;

/* loaded from: classes4.dex */
public final class SafetyNumberChangeDialog extends DialogFragment implements SafetyNumberChangeAdapter.Callbacks {
    private static final String CANCEL_TEXT_RESOURCE_EXTRA;
    private static final String CONTINUE_TEXT_RESOURCE_EXTRA;
    private static final String MESSAGE_ID_EXTRA;
    private static final String MESSAGE_TYPE_EXTRA;
    private static final String RECIPIENT_IDS_EXTRA;
    public static final String SAFETY_NUMBER_DIALOG;
    private static final String SKIP_CALLBACKS_EXTRA;
    private static final String TAG = Log.tag(SafetyNumberChangeDialog.class);
    private SafetyNumberChangeAdapter adapter;
    private View dialogView;
    private SafetyNumberChangeViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onCanceled();

        void onMessageResentAfterSafetyNumberChange();

        void onSendAnywayAfterSafetyNumberChange(List<RecipientId> list);
    }

    public static void show(FragmentManager fragmentManager, RecipientId recipientId) {
        Bundle bundle = new Bundle();
        bundle.putStringArray(RECIPIENT_IDS_EXTRA, new String[]{recipientId.serialize()});
        bundle.putInt(CONTINUE_TEXT_RESOURCE_EXTRA, R.string.safety_number_change_dialog__accept);
        bundle.putBoolean(SKIP_CALLBACKS_EXTRA, true);
        SafetyNumberChangeDialog safetyNumberChangeDialog = new SafetyNumberChangeDialog();
        safetyNumberChangeDialog.setArguments(bundle);
        safetyNumberChangeDialog.show(fragmentManager, SAFETY_NUMBER_DIALOG);
    }

    public static void showForCall(FragmentManager fragmentManager, RecipientId recipientId) {
        Bundle bundle = new Bundle();
        bundle.putStringArray(RECIPIENT_IDS_EXTRA, new String[]{recipientId.serialize()});
        bundle.putInt(CONTINUE_TEXT_RESOURCE_EXTRA, R.string.safety_number_change_dialog__call_anyway);
        SafetyNumberChangeDialog safetyNumberChangeDialog = new SafetyNumberChangeDialog();
        safetyNumberChangeDialog.setArguments(bundle);
        safetyNumberChangeDialog.show(fragmentManager, SAFETY_NUMBER_DIALOG);
    }

    public static void showForGroupCall(FragmentManager fragmentManager, List<IdentityRecord> list) {
        List list2 = Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((IdentityRecord) obj).isFirstUse();
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SafetyNumberChangeDialog.lambda$showForGroupCall$0((IdentityRecord) obj);
            }
        }).distinct().toList();
        Bundle bundle = new Bundle();
        bundle.putStringArray(RECIPIENT_IDS_EXTRA, (String[]) list2.toArray(new String[0]));
        bundle.putInt(CONTINUE_TEXT_RESOURCE_EXTRA, R.string.safety_number_change_dialog__join_call);
        SafetyNumberChangeDialog safetyNumberChangeDialog = new SafetyNumberChangeDialog();
        safetyNumberChangeDialog.setArguments(bundle);
        safetyNumberChangeDialog.show(fragmentManager, SAFETY_NUMBER_DIALOG);
    }

    public static /* synthetic */ String lambda$showForGroupCall$0(IdentityRecord identityRecord) {
        return identityRecord.getRecipientId().serialize();
    }

    public static void showForDuringGroupCall(FragmentManager fragmentManager, Collection<RecipientId> collection) {
        Fragment findFragmentByTag = fragmentManager.findFragmentByTag(SAFETY_NUMBER_DIALOG);
        if (findFragmentByTag != null) {
            ((SafetyNumberChangeDialog) findFragmentByTag).updateRecipients(collection);
            return;
        }
        List list = Stream.of(collection).map(new SafetyNumberChangeDialog$$ExternalSyntheticLambda5()).distinct().toList();
        Bundle bundle = new Bundle();
        bundle.putStringArray(RECIPIENT_IDS_EXTRA, (String[]) list.toArray(new String[0]));
        bundle.putInt(CONTINUE_TEXT_RESOURCE_EXTRA, R.string.safety_number_change_dialog__continue_call);
        bundle.putInt(CANCEL_TEXT_RESOURCE_EXTRA, R.string.safety_number_change_dialog__leave_call);
        SafetyNumberChangeDialog safetyNumberChangeDialog = new SafetyNumberChangeDialog();
        safetyNumberChangeDialog.setArguments(bundle);
        safetyNumberChangeDialog.show(fragmentManager, SAFETY_NUMBER_DIALOG);
    }

    private SafetyNumberChangeDialog() {
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.dialogView;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        List list = Stream.of(getArguments().getStringArray(RECIPIENT_IDS_EXTRA)).map(new SafetyNumberChangeDialog$$ExternalSyntheticLambda3()).toList();
        long j = getArguments().getLong("message_id", -1);
        Long l = null;
        String string = getArguments().getString(MESSAGE_TYPE_EXTRA, null);
        if (j != -1) {
            l = Long.valueOf(j);
        }
        SafetyNumberChangeViewModel safetyNumberChangeViewModel = (SafetyNumberChangeViewModel) new ViewModelProvider(this, new SafetyNumberChangeViewModel.Factory(list, l, string)).get(SafetyNumberChangeViewModel.class);
        this.viewModel = safetyNumberChangeViewModel;
        LiveData<List<ChangedRecipient>> changedRecipients = safetyNumberChangeViewModel.getChangedRecipients();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        SafetyNumberChangeAdapter safetyNumberChangeAdapter = this.adapter;
        Objects.requireNonNull(safetyNumberChangeAdapter);
        changedRecipients.observe(viewLifecycleOwner, new Observer() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SafetyNumberChangeAdapter.this.submitList((List) obj);
            }
        });
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        int i = requireArguments().getInt(CONTINUE_TEXT_RESOURCE_EXTRA, 17039370);
        int i2 = requireArguments().getInt(CANCEL_TEXT_RESOURCE_EXTRA, 17039360);
        this.dialogView = LayoutInflater.from(requireActivity()).inflate(R.layout.safety_number_change_dialog, (ViewGroup) null);
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
        configureView(this.dialogView);
        materialAlertDialogBuilder.setTitle(R.string.safety_number_change_dialog__safety_number_changes).setView(this.dialogView).setPositiveButton(i, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                SafetyNumberChangeDialog.this.handleSendAnyway(dialogInterface, i3);
            }
        }).setNegativeButton(i2, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda7
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i3) {
                SafetyNumberChangeDialog.this.handleCancel(dialogInterface, i3);
            }
        });
        setCancelable(false);
        AlertDialog create = materialAlertDialogBuilder.create();
        create.setOnShowListener(new DialogInterface.OnShowListener(create) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda8
            public final /* synthetic */ AlertDialog f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnShowListener
            public final void onShow(DialogInterface dialogInterface) {
                SafetyNumberChangeDialog.this.lambda$onCreateDialog$1(this.f$1, dialogInterface);
            }
        });
        return create;
    }

    public /* synthetic */ void lambda$onCreateDialog$1(AlertDialog alertDialog, DialogInterface dialogInterface) {
        Button button = alertDialog.getButton(-1);
        button.setEnabled(false);
        this.viewModel.getTrustOrVerifyReady().observe(getViewLifecycleOwner(), new Observer(button) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ Button f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                this.f$0.setEnabled(((Boolean) obj).booleanValue());
            }
        });
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        this.dialogView = null;
        super.onDestroyView();
    }

    private void configureView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.safety_number_change_dialog_list);
        SafetyNumberChangeAdapter safetyNumberChangeAdapter = new SafetyNumberChangeAdapter(this);
        this.adapter = safetyNumberChangeAdapter;
        recyclerView.setAdapter(safetyNumberChangeAdapter);
        recyclerView.setItemAnimator(null);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
    }

    private void updateRecipients(Collection<RecipientId> collection) {
        this.viewModel.updateRecipients(collection);
    }

    public void handleSendAnyway(DialogInterface dialogInterface, int i) {
        final Callback callback;
        Log.d(TAG, "handleSendAnyway");
        boolean z = requireArguments().getBoolean(SKIP_CALLBACKS_EXTRA, false);
        FragmentActivity activity = getActivity();
        if (!(activity instanceof Callback) || z) {
            Fragment parentFragment = getParentFragment();
            callback = (!(parentFragment instanceof Callback) || z) ? null : (Callback) parentFragment;
        } else {
            callback = (Callback) activity;
        }
        final LiveData<TrustAndVerifyResult> trustOrVerifyChangedRecipients = this.viewModel.trustOrVerifyChangedRecipients();
        trustOrVerifyChangedRecipients.observeForever(new Observer<TrustAndVerifyResult>() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog.1
            public void onChanged(TrustAndVerifyResult trustAndVerifyResult) {
                if (callback != null) {
                    int i2 = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$conversation$ui$error$TrustAndVerifyResult$Result[trustAndVerifyResult.getResult().ordinal()];
                    if (i2 == 1) {
                        Log.d(SafetyNumberChangeDialog.TAG, "Trust and verify");
                        callback.onSendAnywayAfterSafetyNumberChange(trustAndVerifyResult.getChangedRecipients());
                    } else if (i2 == 2) {
                        Log.d(SafetyNumberChangeDialog.TAG, "Trust, verify, and resent");
                        callback.onMessageResentAfterSafetyNumberChange();
                    }
                }
                trustOrVerifyChangedRecipients.removeObserver(this);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeDialog$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$conversation$ui$error$TrustAndVerifyResult$Result;

        static {
            int[] iArr = new int[TrustAndVerifyResult.Result.values().length];
            $SwitchMap$org$thoughtcrime$securesms$conversation$ui$error$TrustAndVerifyResult$Result = iArr;
            try {
                iArr[TrustAndVerifyResult.Result.TRUST_AND_VERIFY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ui$error$TrustAndVerifyResult$Result[TrustAndVerifyResult.Result.TRUST_VERIFY_AND_RESEND.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public void handleCancel(DialogInterface dialogInterface, int i) {
        if (getActivity() instanceof Callback) {
            ((Callback) getActivity()).onCanceled();
        } else if (getParentFragment() instanceof Callback) {
            ((Callback) getParentFragment()).onCanceled();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeAdapter.Callbacks
    public void onViewIdentityRecord(IdentityRecord identityRecord) {
        startActivity(VerifyIdentityActivity.newIntent(requireContext(), identityRecord));
    }
}
