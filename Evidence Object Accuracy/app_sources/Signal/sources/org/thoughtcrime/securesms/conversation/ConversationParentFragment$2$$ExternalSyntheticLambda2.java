package org.thoughtcrime.securesms.conversation;

import android.net.Uri;
import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.providers.BlobProvider;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$2$$ExternalSyntheticLambda2 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return BlobProvider.isAuthority((Uri) obj);
    }
}
