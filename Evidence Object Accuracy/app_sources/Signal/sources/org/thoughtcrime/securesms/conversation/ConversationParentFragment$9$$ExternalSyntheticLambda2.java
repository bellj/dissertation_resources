package org.thoughtcrime.securesms.conversation;

import android.graphics.drawable.Drawable;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$9$$ExternalSyntheticLambda2 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ Drawable f$0;

    public /* synthetic */ ConversationParentFragment$9$$ExternalSyntheticLambda2(Drawable drawable) {
        this.f$0 = drawable;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return ConversationParentFragment.AnonymousClass9.lambda$onLoadFailed$0(this.f$0);
    }
}
