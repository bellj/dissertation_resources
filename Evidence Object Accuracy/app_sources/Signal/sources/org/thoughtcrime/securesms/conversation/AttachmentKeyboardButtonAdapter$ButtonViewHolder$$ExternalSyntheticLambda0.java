package org.thoughtcrime.securesms.conversation;

import android.view.View;
import org.thoughtcrime.securesms.conversation.AttachmentKeyboardButtonAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AttachmentKeyboardButtonAdapter$ButtonViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AttachmentKeyboardButtonAdapter.Listener f$0;
    public final /* synthetic */ AttachmentKeyboardButton f$1;

    public /* synthetic */ AttachmentKeyboardButtonAdapter$ButtonViewHolder$$ExternalSyntheticLambda0(AttachmentKeyboardButtonAdapter.Listener listener, AttachmentKeyboardButton attachmentKeyboardButton) {
        this.f$0 = listener;
        this.f$1 = attachmentKeyboardButton;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.onClick(this.f$1);
    }
}
