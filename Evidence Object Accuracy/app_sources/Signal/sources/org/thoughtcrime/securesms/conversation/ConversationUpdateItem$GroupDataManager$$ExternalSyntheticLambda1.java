package org.thoughtcrime.securesms.conversation;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.conversation.ConversationUpdateItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda1 implements Observer {
    public final /* synthetic */ ConversationUpdateItem.GroupDataManager f$0;

    public /* synthetic */ ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda1(ConversationUpdateItem.GroupDataManager groupDataManager) {
        this.f$0 = groupDataManager;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.lambda$new$0(obj);
    }
}
