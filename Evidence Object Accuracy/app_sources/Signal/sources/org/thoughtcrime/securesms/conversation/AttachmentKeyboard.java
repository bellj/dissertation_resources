package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Arrays;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.InputAwareLayout;
import org.thoughtcrime.securesms.conversation.AttachmentKeyboardButtonAdapter;
import org.thoughtcrime.securesms.conversation.AttachmentKeyboardMediaAdapter;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.StorageUtil;

/* loaded from: classes4.dex */
public class AttachmentKeyboard extends FrameLayout implements InputAwareLayout.InputView {
    private static final List<AttachmentKeyboardButton> DEFAULT_BUTTONS = Arrays.asList(AttachmentKeyboardButton.GALLERY, AttachmentKeyboardButton.FILE, AttachmentKeyboardButton.PAYMENT, AttachmentKeyboardButton.CONTACT, AttachmentKeyboardButton.LOCATION);
    private AttachmentKeyboardButtonAdapter buttonAdapter;
    private Callback callback;
    private View container;
    private AttachmentKeyboardMediaAdapter mediaAdapter;
    private RecyclerView mediaList;
    private View permissionButton;
    private View permissionText;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onAttachmentMediaClicked(Media media);

        void onAttachmentPermissionsRequested();

        void onAttachmentSelectorClicked(AttachmentKeyboardButton attachmentKeyboardButton);
    }

    public AttachmentKeyboard(Context context) {
        super(context);
        init(context);
    }

    public AttachmentKeyboard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        FrameLayout.inflate(context, R.layout.attachment_keyboard, this);
        this.container = findViewById(R.id.attachment_keyboard_container);
        this.mediaList = (RecyclerView) findViewById(R.id.attachment_keyboard_media_list);
        this.permissionText = findViewById(R.id.attachment_keyboard_permission_text);
        this.permissionButton = findViewById(R.id.attachment_keyboard_permission_button);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.attachment_keyboard_button_list);
        this.mediaAdapter = new AttachmentKeyboardMediaAdapter(GlideApp.with(this), new AttachmentKeyboardMediaAdapter.Listener() { // from class: org.thoughtcrime.securesms.conversation.AttachmentKeyboard$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.conversation.AttachmentKeyboardMediaAdapter.Listener
            public final void onMediaClicked(Media media) {
                AttachmentKeyboard.$r8$lambda$aoqc2mOSJXhDeb9hDl34ELjRKzM(AttachmentKeyboard.this, media);
            }
        });
        this.buttonAdapter = new AttachmentKeyboardButtonAdapter(new AttachmentKeyboardButtonAdapter.Listener() { // from class: org.thoughtcrime.securesms.conversation.AttachmentKeyboard$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.conversation.AttachmentKeyboardButtonAdapter.Listener
            public final void onClick(AttachmentKeyboardButton attachmentKeyboardButton) {
                AttachmentKeyboard.$r8$lambda$Gf1uUlDq_dkMMmC5v1r9tHEzFvE(AttachmentKeyboard.this, attachmentKeyboardButton);
            }
        });
        this.mediaList.setAdapter(this.mediaAdapter);
        recyclerView.setAdapter(this.buttonAdapter);
        this.buttonAdapter.registerAdapterDataObserver(new AttachmentButtonCenterHelper(recyclerView));
        this.mediaList.setLayoutManager(new GridLayoutManager(context, 1, 0, false));
        recyclerView.setLayoutManager(new LinearLayoutManager(context, 0, false));
        this.buttonAdapter.setButtons(DEFAULT_BUTTONS);
    }

    public /* synthetic */ void lambda$init$0(Media media) {
        Callback callback = this.callback;
        if (callback != null) {
            callback.onAttachmentMediaClicked(media);
        }
    }

    public /* synthetic */ void lambda$init$1(AttachmentKeyboardButton attachmentKeyboardButton) {
        Callback callback = this.callback;
        if (callback != null) {
            callback.onAttachmentSelectorClicked(attachmentKeyboardButton);
        }
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void filterAttachmentKeyboardButtons(Predicate<AttachmentKeyboardButton> predicate) {
        if (predicate == null) {
            this.buttonAdapter.setButtons(DEFAULT_BUTTONS);
        } else {
            this.buttonAdapter.setButtons(Stream.of(DEFAULT_BUTTONS).filter(predicate).toList());
        }
    }

    public void onMediaChanged(List<Media> list) {
        if (StorageUtil.canReadFromMediaStore()) {
            this.mediaAdapter.setMedia(list);
            this.permissionButton.setVisibility(8);
            this.permissionText.setVisibility(8);
            return;
        }
        this.permissionButton.setVisibility(0);
        this.permissionText.setVisibility(0);
        this.permissionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.AttachmentKeyboard$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AttachmentKeyboard.$r8$lambda$wy9iSL6F_9NsaNQT5v2GuUaqgsw(AttachmentKeyboard.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$onMediaChanged$2(View view) {
        Callback callback = this.callback;
        if (callback != null) {
            callback.onAttachmentPermissionsRequested();
        }
    }

    public void setWallpaperEnabled(boolean z) {
        if (z) {
            this.container.setBackgroundColor(getContext().getResources().getColor(R.color.wallpaper_compose_background));
        } else {
            this.container.setBackgroundColor(getContext().getResources().getColor(R.color.signal_background_primary));
        }
        this.buttonAdapter.setWallpaperEnabled(z);
    }

    @Override // org.thoughtcrime.securesms.components.InputAwareLayout.InputView
    public void show(int i, boolean z) {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = i;
        setLayoutParams(layoutParams);
        setVisibility(0);
    }

    @Override // org.thoughtcrime.securesms.components.InputAwareLayout.InputView
    public void hide(boolean z) {
        setVisibility(8);
    }

    @Override // org.thoughtcrime.securesms.components.InputAwareLayout.InputView
    public boolean isShowing() {
        return getVisibility() == 0;
    }
}
