package org.thoughtcrime.securesms.conversation.colors;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import com.google.common.collect.ImmutableMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.color.MaterialColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.GradientChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.SingleColorChatWallpaper;

/* compiled from: ChatColorsMapper.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010'\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\b\u0006\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u0006H\u0007J\u0010\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u000fH\u0007J\u0010\u0010\u0013\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0007H\u0007R,\u0010\u0003\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00050\u00048FX\u0004¢\u0006\f\u0012\u0004\b\b\u0010\u0002\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\fX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u00070\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ChatColorsMapper;", "", "()V", "entrySet", "", "", "Lorg/thoughtcrime/securesms/color/MaterialColor;", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getEntrySet$annotations", "getEntrySet", "()Ljava/util/Set;", "materialColorToChatColorsBiMap", "Lcom/google/common/collect/BiMap;", "wallpaperToChatColorsMap", "", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "getChatColors", "materialColor", "wallpaper", "getMaterialColor", "chatColors", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorsMapper {
    public static final ChatColorsMapper INSTANCE = new ChatColorsMapper();
    private static final BiMap<MaterialColor, ChatColors> materialColorToChatColorsBiMap;
    private static final Map<ChatWallpaper, ChatColors> wallpaperToChatColorsMap;

    @JvmStatic
    public static /* synthetic */ void getEntrySet$annotations() {
    }

    private ChatColorsMapper() {
    }

    static {
        INSTANCE = new ChatColorsMapper();
        ImmutableBiMap.Builder builder = new ImmutableBiMap.Builder();
        MaterialColor materialColor = MaterialColor.CRIMSON;
        ChatColors chatColors = ChatColorsPalette.Bubbles.CRIMSON;
        builder.put((ImmutableBiMap.Builder) materialColor, (MaterialColor) chatColors);
        MaterialColor materialColor2 = MaterialColor.VERMILLION;
        ChatColors chatColors2 = ChatColorsPalette.Bubbles.VERMILION;
        builder.put((ImmutableBiMap.Builder) materialColor2, (MaterialColor) chatColors2);
        MaterialColor materialColor3 = MaterialColor.BURLAP;
        ChatColors chatColors3 = ChatColorsPalette.Bubbles.BURLAP;
        builder.put((ImmutableBiMap.Builder) materialColor3, (MaterialColor) chatColors3);
        MaterialColor materialColor4 = MaterialColor.FOREST;
        ChatColors chatColors4 = ChatColorsPalette.Bubbles.FOREST;
        builder.put((ImmutableBiMap.Builder) materialColor4, (MaterialColor) chatColors4);
        MaterialColor materialColor5 = MaterialColor.WINTERGREEN;
        ChatColors chatColors5 = ChatColorsPalette.Bubbles.WINTERGREEN;
        builder.put((ImmutableBiMap.Builder) materialColor5, (MaterialColor) chatColors5);
        MaterialColor materialColor6 = MaterialColor.TEAL;
        ChatColors chatColors6 = ChatColorsPalette.Bubbles.TEAL;
        builder.put((ImmutableBiMap.Builder) materialColor6, (MaterialColor) chatColors6);
        MaterialColor materialColor7 = MaterialColor.BLUE;
        ChatColors chatColors7 = ChatColorsPalette.Bubbles.BLUE;
        builder.put((ImmutableBiMap.Builder) materialColor7, (MaterialColor) chatColors7);
        MaterialColor materialColor8 = MaterialColor.INDIGO;
        ChatColors chatColors8 = ChatColorsPalette.Bubbles.INDIGO;
        builder.put((ImmutableBiMap.Builder) materialColor8, (MaterialColor) chatColors8);
        MaterialColor materialColor9 = MaterialColor.VIOLET;
        ChatColors chatColors9 = ChatColorsPalette.Bubbles.VIOLET;
        builder.put((ImmutableBiMap.Builder) materialColor9, (MaterialColor) chatColors9);
        MaterialColor materialColor10 = MaterialColor.PLUM;
        ChatColors chatColors10 = ChatColorsPalette.Bubbles.PLUM;
        builder.put((ImmutableBiMap.Builder) materialColor10, (MaterialColor) chatColors10);
        MaterialColor materialColor11 = MaterialColor.TAUPE;
        ChatColors chatColors11 = ChatColorsPalette.Bubbles.TAUPE;
        builder.put((ImmutableBiMap.Builder) materialColor11, (MaterialColor) chatColors11);
        MaterialColor materialColor12 = MaterialColor.STEEL;
        ChatColors chatColors12 = ChatColorsPalette.Bubbles.STEEL;
        builder.put((ImmutableBiMap.Builder) materialColor12, (MaterialColor) chatColors12);
        builder.put((ImmutableBiMap.Builder) MaterialColor.ULTRAMARINE, (MaterialColor) ChatColorsPalette.Bubbles.ULTRAMARINE);
        ImmutableBiMap build = builder.build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder<MaterialColor, C….ULTRAMARINE)\n  }.build()");
        materialColorToChatColorsBiMap = build;
        ImmutableMap.Builder builder2 = new ImmutableMap.Builder();
        builder2.put(SingleColorChatWallpaper.BLUSH, chatColors);
        builder2.put(SingleColorChatWallpaper.COPPER, chatColors2);
        builder2.put(SingleColorChatWallpaper.DUST, chatColors3);
        builder2.put(SingleColorChatWallpaper.CELADON, chatColors4);
        builder2.put(SingleColorChatWallpaper.RAINFOREST, chatColors5);
        builder2.put(SingleColorChatWallpaper.PACIFIC, chatColors6);
        builder2.put(SingleColorChatWallpaper.FROST, chatColors7);
        builder2.put(SingleColorChatWallpaper.NAVY, chatColors8);
        builder2.put(SingleColorChatWallpaper.LILAC, chatColors9);
        builder2.put(SingleColorChatWallpaper.PINK, chatColors10);
        builder2.put(SingleColorChatWallpaper.EGGPLANT, chatColors11);
        builder2.put(SingleColorChatWallpaper.SILVER, chatColors12);
        builder2.put(GradientChatWallpaper.SUNSET, ChatColorsPalette.Bubbles.EMBER);
        builder2.put(GradientChatWallpaper.NOIR, ChatColorsPalette.Bubbles.MIDNIGHT);
        builder2.put(GradientChatWallpaper.HEATMAP, ChatColorsPalette.Bubbles.INFRARED);
        builder2.put(GradientChatWallpaper.AQUA, ChatColorsPalette.Bubbles.LAGOON);
        builder2.put(GradientChatWallpaper.IRIDESCENT, ChatColorsPalette.Bubbles.FLUORESCENT);
        builder2.put(GradientChatWallpaper.MONSTERA, ChatColorsPalette.Bubbles.BASIL);
        builder2.put(GradientChatWallpaper.BLISS, ChatColorsPalette.Bubbles.SUBLIME);
        builder2.put(GradientChatWallpaper.SKY, ChatColorsPalette.Bubbles.SEA);
        builder2.put(GradientChatWallpaper.PEACH, ChatColorsPalette.Bubbles.TANGERINE);
        ImmutableMap build2 = builder2.build();
        Intrinsics.checkNotNullExpressionValue(build2, "Builder<ChatWallpaper, C…es.TANGERINE)\n  }.build()");
        wallpaperToChatColorsMap = build2;
    }

    public static final Set<Map.Entry<MaterialColor, ChatColors>> getEntrySet() {
        return materialColorToChatColorsBiMap.entrySet();
    }

    @JvmStatic
    public static final ChatColors getChatColors(MaterialColor materialColor) {
        Intrinsics.checkNotNullParameter(materialColor, "materialColor");
        ChatColors chatColors = materialColorToChatColorsBiMap.get(materialColor);
        return chatColors == null ? ChatColorsPalette.Bubbles.getDefault() : chatColors;
    }

    @JvmStatic
    public static final ChatColors getChatColors(ChatWallpaper chatWallpaper) {
        Object obj;
        ChatColors chatColors;
        Intrinsics.checkNotNullParameter(chatWallpaper, "wallpaper");
        Iterator<T> it = wallpaperToChatColorsMap.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((ChatWallpaper) ((Map.Entry) obj).getKey()).isSameSource(chatWallpaper)) {
                break;
            }
        }
        Map.Entry entry = (Map.Entry) obj;
        return (entry == null || (chatColors = (ChatColors) entry.getValue()) == null) ? ChatColorsPalette.Bubbles.getDefault() : chatColors;
    }

    @JvmStatic
    public static final MaterialColor getMaterialColor(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        MaterialColor materialColor = materialColorToChatColorsBiMap.inverse().get(chatColors);
        return materialColor == null ? MaterialColor.ULTRAMARINE : materialColor;
    }
}
