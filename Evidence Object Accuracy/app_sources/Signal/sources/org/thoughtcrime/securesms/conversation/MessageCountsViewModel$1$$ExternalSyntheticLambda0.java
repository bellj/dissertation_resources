package org.thoughtcrime.securesms.conversation;

import androidx.lifecycle.MutableLiveData;
import org.thoughtcrime.securesms.conversation.MessageCountsViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MessageCountsViewModel$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ MessageCountsViewModel.AnonymousClass1 f$0;
    public final /* synthetic */ Long f$1;
    public final /* synthetic */ MutableLiveData f$2;

    public /* synthetic */ MessageCountsViewModel$1$$ExternalSyntheticLambda0(MessageCountsViewModel.AnonymousClass1 r1, Long l, MutableLiveData mutableLiveData) {
        this.f$0 = r1;
        this.f$1 = l;
        this.f$2 = mutableLiveData;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onChanged$0(this.f$1, this.f$2);
    }
}
