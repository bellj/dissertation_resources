package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorViewModel;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CustomChatColorCreatorViewModel$1$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ ChatWallpaper f$0;

    public /* synthetic */ CustomChatColorCreatorViewModel$1$$ExternalSyntheticLambda0(ChatWallpaper chatWallpaper) {
        this.f$0 = chatWallpaper;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return CustomChatColorCreatorViewModel.AnonymousClass1.m1551invoke$lambda0(this.f$0, (CustomChatColorCreatorState) obj);
    }
}
