package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import java.util.List;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.notifications.MarkReadReceiver;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.util.Debouncer;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;

/* loaded from: classes4.dex */
public class MarkReadHelper {
    private static final long DEBOUNCE_TIMEOUT;
    private static final Executor EXECUTOR = new SerialMonoLifoExecutor(SignalExecutors.BOUNDED);
    private static final String TAG = Log.tag(MarkReadHelper.class);
    private final Context context;
    private final ConversationId conversationId;
    private final Debouncer debouncer = new Debouncer(DEBOUNCE_TIMEOUT);
    private long latestTimestamp;
    private final LifecycleOwner lifecycleOwner;

    public MarkReadHelper(ConversationId conversationId, Context context, LifecycleOwner lifecycleOwner) {
        this.conversationId = conversationId;
        this.context = context.getApplicationContext();
        this.lifecycleOwner = lifecycleOwner;
    }

    public void onViewsRevealed(long j) {
        if (j > this.latestTimestamp && this.lifecycleOwner.getLifecycle().getCurrentState() == Lifecycle.State.RESUMED) {
            this.latestTimestamp = j;
            this.debouncer.publish(new Runnable(j) { // from class: org.thoughtcrime.securesms.conversation.MarkReadHelper$$ExternalSyntheticLambda1
                public final /* synthetic */ long f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MarkReadHelper.this.lambda$onViewsRevealed$1(this.f$1);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onViewsRevealed$1(long j) {
        EXECUTOR.execute(new Runnable(j) { // from class: org.thoughtcrime.securesms.conversation.MarkReadHelper$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MarkReadHelper.this.lambda$onViewsRevealed$0(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onViewsRevealed$0(long j) {
        List<MessageDatabase.MarkedMessageInfo> readSince = SignalDatabase.threads().setReadSince(this.conversationId, false, j);
        String str = TAG;
        Log.d(str, "Marking " + readSince.size() + " messages as read.");
        ApplicationDependencies.getMessageNotifier().updateNotification(this.context);
        MarkReadReceiver.process(this.context, readSince);
    }
}
