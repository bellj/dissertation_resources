package org.thoughtcrime.securesms.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$$ExternalSyntheticLambda8 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((MultiselectPart) obj).getConversationMessage();
    }
}
