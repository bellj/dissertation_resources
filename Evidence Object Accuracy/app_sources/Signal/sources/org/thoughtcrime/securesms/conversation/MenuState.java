package org.thoughtcrime.securesms.conversation;

import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.Set;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.MessageRecordUtil;

/* loaded from: classes4.dex */
public final class MenuState {
    private static final int MAX_FORWARDABLE_COUNT;
    private final boolean copy;
    private final boolean delete;
    private final boolean details;
    private final boolean forward;
    private final boolean reactions;
    private final boolean reply;
    private final boolean resend;
    private final boolean saveAttachment;

    private MenuState(Builder builder) {
        this.forward = builder.forward;
        this.reply = builder.reply;
        this.details = builder.details;
        this.saveAttachment = builder.saveAttachment;
        this.resend = builder.resend;
        this.copy = builder.copy;
        this.delete = builder.delete;
        this.reactions = builder.reactions;
    }

    public boolean shouldShowForwardAction() {
        return this.forward;
    }

    public boolean shouldShowReplyAction() {
        return this.reply;
    }

    public boolean shouldShowDetailsAction() {
        return this.details;
    }

    public boolean shouldShowSaveAttachmentAction() {
        return this.saveAttachment;
    }

    public boolean shouldShowResendAction() {
        return this.resend;
    }

    public boolean shouldShowCopyAction() {
        return this.copy;
    }

    public boolean shouldShowDeleteAction() {
        return this.delete;
    }

    public boolean shouldShowReactions() {
        return this.reactions;
    }

    public static MenuState getMenuState(Recipient recipient, Set<MultiselectPart> set, boolean z, boolean z2) {
        boolean z3;
        Builder builder = new Builder();
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = false;
        boolean z10 = false;
        boolean z11 = false;
        boolean z12 = false;
        boolean z13 = false;
        for (MultiselectPart multiselectPart : set) {
            MessageRecord messageRecord = multiselectPart.getMessageRecord();
            if (isActionMessage(messageRecord)) {
                if (messageRecord.isInMemoryMessageRecord()) {
                    z5 = true;
                    z13 = true;
                } else {
                    z5 = true;
                }
            }
            if (multiselectPart instanceof MultiselectPart.Attachments) {
                if (messageRecord.isMediaPending()) {
                    z9 = true;
                }
                z12 = true;
            } else if (messageRecord.getBody().length() > 0) {
                z11 = true;
            }
            if (messageRecord.isMms() && !((MmsMessageRecord) messageRecord).getSharedContacts().isEmpty()) {
                z6 = true;
            }
            if (messageRecord.isViewOnce()) {
                z7 = true;
            }
            if (messageRecord.isRemoteDelete()) {
                z8 = true;
            }
            if (MessageRecordUtil.hasGiftBadge(messageRecord)) {
                z10 = true;
            }
        }
        boolean z14 = !z5 && !z6 && !z7 && !z8 && !z9 && !z10 && set.size() <= 32;
        if (((Set) Collection$EL.stream(set).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.MenuState$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((MultiselectPart) obj).getMessageRecord();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toSet())).size() > 1) {
            builder.shouldShowForwardAction(z14).shouldShowReplyAction(false).shouldShowDetailsAction(false).shouldShowSaveAttachmentAction(false).shouldShowResendAction(false);
        } else {
            MessageRecord messageRecord2 = set.iterator().next().getMessageRecord();
            Builder shouldShowResendAction = builder.shouldShowResendAction(messageRecord2.isFailed());
            if (z12 && !z5 && !z7 && messageRecord2.isMms() && !z9 && !z10 && !messageRecord2.isMmsNotification()) {
                MediaMmsMessageRecord mediaMmsMessageRecord = (MediaMmsMessageRecord) messageRecord2;
                if (mediaMmsMessageRecord.containsMediaSlide() && mediaMmsMessageRecord.getSlideDeck().getStickerSlide() == null) {
                    z3 = true;
                    shouldShowResendAction.shouldShowSaveAttachmentAction(z3).shouldShowForwardAction(z14).shouldShowDetailsAction(z5 && !recipient.isReleaseNotes()).shouldShowReplyAction(canReplyToMessage(recipient, z5, messageRecord2, z, z2));
                }
            }
            z3 = false;
            shouldShowResendAction.shouldShowSaveAttachmentAction(z3).shouldShowForwardAction(z14).shouldShowDetailsAction(z5 && !recipient.isReleaseNotes()).shouldShowReplyAction(canReplyToMessage(recipient, z5, messageRecord2, z, z2));
        }
        Builder shouldShowCopyAction = builder.shouldShowCopyAction(!z5 && !z8 && z11 && !z10);
        if (!z13 && onlyContainsCompleteMessages(set)) {
            z4 = true;
        }
        return shouldShowCopyAction.shouldShowDeleteAction(z4).shouldShowReactions(!recipient.isReleaseNotes()).build();
    }

    private static boolean onlyContainsCompleteMessages(Set<MultiselectPart> set) {
        return Collection$EL.stream(set).map(new ConversationFragment$$ExternalSyntheticLambda14()).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.MenuState$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((ConversationMessage) obj).getMultiselectCollection();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).allMatch(new Predicate(set) { // from class: org.thoughtcrime.securesms.conversation.MenuState$$ExternalSyntheticLambda1
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return MenuState.lambda$onlyContainsCompleteMessages$0(this.f$0, (MultiselectCollection) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$onlyContainsCompleteMessages$0(Set set, MultiselectCollection multiselectCollection) {
        return set.containsAll(multiselectCollection.toSet());
    }

    public static boolean canReplyToMessage(Recipient recipient, boolean z, MessageRecord messageRecord, boolean z2, boolean z3) {
        return !z && !z3 && !messageRecord.isRemoteDelete() && !messageRecord.isPending() && !messageRecord.isFailed() && !z2 && messageRecord.isSecure() && (!recipient.isGroup() || recipient.isActiveGroup()) && !messageRecord.getRecipient().isBlocked() && !recipient.isReleaseNotes();
    }

    public static boolean isActionMessage(MessageRecord messageRecord) {
        return messageRecord.isGroupAction() || messageRecord.isCallLog() || messageRecord.isJoined() || messageRecord.isExpirationTimerUpdate() || messageRecord.isEndSession() || messageRecord.isIdentityUpdate() || messageRecord.isIdentityVerified() || messageRecord.isIdentityDefault() || messageRecord.isProfileChange() || messageRecord.isGroupV1MigrationEvent() || messageRecord.isChatSessionRefresh() || messageRecord.isInMemoryMessageRecord() || messageRecord.isChangeNumber() || messageRecord.isBoostRequest();
    }

    /* loaded from: classes4.dex */
    public static final class Builder {
        private boolean copy;
        private boolean delete;
        private boolean details;
        private boolean forward;
        private boolean reactions;
        private boolean reply;
        private boolean resend;
        private boolean saveAttachment;

        private Builder() {
        }

        Builder shouldShowForwardAction(boolean z) {
            this.forward = z;
            return this;
        }

        Builder shouldShowReplyAction(boolean z) {
            this.reply = z;
            return this;
        }

        Builder shouldShowDetailsAction(boolean z) {
            this.details = z;
            return this;
        }

        Builder shouldShowSaveAttachmentAction(boolean z) {
            this.saveAttachment = z;
            return this;
        }

        Builder shouldShowResendAction(boolean z) {
            this.resend = z;
            return this;
        }

        Builder shouldShowCopyAction(boolean z) {
            this.copy = z;
            return this;
        }

        Builder shouldShowDeleteAction(boolean z) {
            this.delete = z;
            return this;
        }

        Builder shouldShowReactions(boolean z) {
            this.reactions = z;
            return this;
        }

        MenuState build() {
            return new MenuState(this);
        }
    }
}
