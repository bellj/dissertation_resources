package org.thoughtcrime.securesms.conversation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ThumbnailView;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.adapter.StableIdGenerator;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class AttachmentKeyboardMediaAdapter extends RecyclerView.Adapter<MediaViewHolder> {
    private final GlideRequests glideRequests;
    private final StableIdGenerator<Media> idGenerator = new StableIdGenerator<>();
    private final Listener listener;
    private final List<Media> media = new ArrayList();

    /* loaded from: classes4.dex */
    public interface Listener {
        void onMediaClicked(Media media);
    }

    public AttachmentKeyboardMediaAdapter(GlideRequests glideRequests, Listener listener) {
        this.glideRequests = glideRequests;
        this.listener = listener;
        setHasStableIds(true);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public long getItemId(int i) {
        return this.idGenerator.getId(this.media.get(i));
    }

    public MediaViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MediaViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.attachment_keyboad_media_item, viewGroup, false));
    }

    public void onBindViewHolder(MediaViewHolder mediaViewHolder, int i) {
        mediaViewHolder.bind(this.media.get(i), this.glideRequests, this.listener);
    }

    public void onViewRecycled(MediaViewHolder mediaViewHolder) {
        mediaViewHolder.recycle();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.media.size();
    }

    public void setMedia(List<Media> list) {
        this.media.clear();
        this.media.addAll(list);
        notifyDataSetChanged();
    }

    /* loaded from: classes4.dex */
    public static class MediaViewHolder extends RecyclerView.ViewHolder {
        private final TextView duration;
        private final ThumbnailView image;
        private final View videoIcon;

        public MediaViewHolder(View view) {
            super(view);
            this.image = (ThumbnailView) view.findViewById(R.id.attachment_keyboard_item_image);
            this.duration = (TextView) view.findViewById(R.id.attachment_keyboard_item_video_time);
            this.videoIcon = view.findViewById(R.id.attachment_keyboard_item_video_icon);
        }

        void bind(Media media, GlideRequests glideRequests, Listener listener) {
            this.image.setImageResource(glideRequests, media.getUri(), 400, 400);
            this.image.setOnClickListener(new AttachmentKeyboardMediaAdapter$MediaViewHolder$$ExternalSyntheticLambda0(listener, media));
            this.duration.setVisibility(8);
            this.videoIcon.setVisibility(8);
            if (media.getDuration() > 0) {
                this.duration.setVisibility(0);
                this.duration.setText(formatTime(media.getDuration()));
            } else if (MediaUtil.isVideoType(media.getMimeType())) {
                this.videoIcon.setVisibility(0);
            }
        }

        void recycle() {
            this.image.setOnClickListener(null);
        }

        static String formatTime(long j) {
            TimeUnit timeUnit = TimeUnit.MILLISECONDS;
            long hours = timeUnit.toHours(j);
            long millis = j - TimeUnit.HOURS.toMillis(hours);
            long minutes = timeUnit.toMinutes(millis);
            long seconds = timeUnit.toSeconds(millis - TimeUnit.MINUTES.toMillis(minutes));
            if (hours > 0) {
                return zeroPad(hours) + ":" + zeroPad(minutes) + ":" + zeroPad(seconds);
            }
            return zeroPad(minutes) + ":" + zeroPad(seconds);
        }

        static String zeroPad(long j) {
            if (j >= 10) {
                return String.valueOf(j);
            }
            return "0" + j;
        }
    }
}
