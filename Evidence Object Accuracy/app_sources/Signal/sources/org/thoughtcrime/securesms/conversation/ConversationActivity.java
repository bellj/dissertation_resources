package org.thoughtcrime.securesms.conversation;

import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ComposeText;
import org.thoughtcrime.securesms.components.HidingLinearLayout;
import org.thoughtcrime.securesms.components.reminder.ReminderView;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.views.Stub;

/* compiled from: ConversationActivity.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\b\u0016\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005¢\u0006\u0002\u0010\u0004J\u0012\u0010\u0014\u001a\u00020\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0016J\u0006\u0010\u0018\u001a\u00020\u0019J\u0006\u0010\u001a\u001a\u00020\u001bJ\u0006\u0010\u001c\u001a\u00020\u001dJ\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020 0\u001fJ\u0006\u0010!\u001a\u00020\u0019J\u001a\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%2\u0006\u0010&\u001a\u00020\u0015H\u0014J\u0010\u0010'\u001a\u00020#2\u0006\u0010(\u001a\u00020)H\u0016J\u0012\u0010*\u001a\u00020#2\b\u0010+\u001a\u0004\u0018\u00010,H\u0014J\b\u0010-\u001a\u00020#H\u0014J\b\u0010.\u001a\u00020#H\u0014J\u0010\u0010/\u001a\u00020#2\u0006\u0010+\u001a\u00020,H\u0002J\f\u00100\u001a\b\u0012\u0004\u0012\u00020201R\u001b\u0010\u0005\u001a\u00020\u00068VX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ConversationActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "Lorg/thoughtcrime/securesms/conversation/ConversationParentFragment$Callback;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "()V", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "getDonationPaymentRepository", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "donationPaymentRepository$delegate", "Lkotlin/Lazy;", "dynamicTheme", "Lorg/thoughtcrime/securesms/util/DynamicTheme;", "fragment", "Lorg/thoughtcrime/securesms/conversation/ConversationParentFragment;", "googlePayResultPublisher", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent$GooglePayResult;", "getGooglePayResultPublisher", "()Lio/reactivex/rxjava3/subjects/Subject;", "dispatchTouchEvent", "", "ev", "Landroid/view/MotionEvent;", "getComposeText", "Landroid/view/View;", "getQuickAttachmentToggle", "Lorg/thoughtcrime/securesms/components/HidingLinearLayout;", "getRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getReminderView", "Lorg/thoughtcrime/securesms/util/views/Stub;", "Lorg/thoughtcrime/securesms/components/reminder/ReminderView;", "getTitleView", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "ready", "onInitializeToolbar", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "onNewIntent", "intent", "Landroid/content/Intent;", "onPreCreate", "onResume", "replaceFragment", "saveDraft", "Lorg/thoughtcrime/securesms/util/concurrent/ListenableFuture;", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class ConversationActivity extends PassphraseRequiredActivity implements ConversationParentFragment.Callback, DonationPaymentComponent {
    private final Lazy donationPaymentRepository$delegate = LazyKt__LazyJVMKt.lazy(new Function0<DonationPaymentRepository>(this) { // from class: org.thoughtcrime.securesms.conversation.ConversationActivity$donationPaymentRepository$2
        final /* synthetic */ ConversationActivity this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final DonationPaymentRepository invoke() {
            return new DonationPaymentRepository(this.this$0);
        }
    });
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private ConversationParentFragment fragment;
    private final Subject<DonationPaymentComponent.GooglePayResult> googlePayResultPublisher;

    @Override // org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public /* synthetic */ boolean isInBubble() {
        return ConversationParentFragment.Callback.CC.$default$isInBubble(this);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public /* synthetic */ void onSendComplete(long j) {
        ConversationParentFragment.Callback.CC.$default$onSendComplete(this, j);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public /* synthetic */ boolean onUpdateReminders() {
        return ConversationParentFragment.Callback.CC.$default$onUpdateReminders(this);
    }

    public ConversationActivity() {
        PublishSubject create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.googlePayResultPublisher = create;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.conversation_parent_fragment_container);
        if (bundle == null) {
            Intent intent = getIntent();
            Intrinsics.checkNotNull(intent);
            replaceFragment(intent);
            return;
        }
        Fragment findFragmentById = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (findFragmentById != null) {
            this.fragment = (ConversationParentFragment) findFragmentById;
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.ConversationParentFragment");
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        Intrinsics.checkNotNull(intent);
        replaceFragment(intent);
    }

    private final void replaceFragment(Intent intent) {
        ConversationParentFragment create = ConversationParentFragment.create(intent);
        Intrinsics.checkNotNullExpressionValue(create, "create(intent)");
        this.fragment = create;
        FragmentTransaction beginTransaction = getSupportFragmentManager().beginTransaction();
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        beginTransaction.replace(R.id.fragment_container, conversationParentFragment).disallowAddToBackStack().commitNowAllowingStateLoss();
    }

    @Override // android.app.Activity, android.view.Window.Callback
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        return conversationParentFragment.dispatchTouchEvent(motionEvent) || super.dispatchTouchEvent(motionEvent);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationParentFragment.Callback
    public void onInitializeToolbar(Toolbar toolbar) {
        Intrinsics.checkNotNullParameter(toolbar, "toolbar");
        toolbar.setNavigationIcon(AppCompatResources.getDrawable(this, R.drawable.ic_arrow_left_24));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationActivity.m1367onInitializeToolbar$lambda0(ConversationActivity.this, view);
            }
        });
    }

    /* renamed from: onInitializeToolbar$lambda-0 */
    public static final void m1367onInitializeToolbar$lambda0(ConversationActivity conversationActivity, View view) {
        Intrinsics.checkNotNullParameter(conversationActivity, "this$0");
        conversationActivity.finish();
    }

    public final ListenableFuture<Long> saveDraft() {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        ListenableFuture<Long> saveDraft = conversationParentFragment.saveDraft();
        Intrinsics.checkNotNullExpressionValue(saveDraft, "fragment.saveDraft()");
        return saveDraft;
    }

    public final Recipient getRecipient() {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        Recipient recipient = conversationParentFragment.getRecipient();
        Intrinsics.checkNotNullExpressionValue(recipient, "fragment.recipient");
        return recipient;
    }

    public final View getTitleView() {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        ConversationTitleView conversationTitleView = conversationParentFragment.titleView;
        Intrinsics.checkNotNullExpressionValue(conversationTitleView, "fragment.titleView");
        return conversationTitleView;
    }

    public final View getComposeText() {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        ComposeText composeText = conversationParentFragment.composeText;
        Intrinsics.checkNotNullExpressionValue(composeText, "fragment.composeText");
        return composeText;
    }

    public final HidingLinearLayout getQuickAttachmentToggle() {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        HidingLinearLayout hidingLinearLayout = conversationParentFragment.quickAttachmentToggle;
        Intrinsics.checkNotNullExpressionValue(hidingLinearLayout, "fragment.quickAttachmentToggle");
        return hidingLinearLayout;
    }

    public final Stub<ReminderView> getReminderView() {
        ConversationParentFragment conversationParentFragment = this.fragment;
        if (conversationParentFragment == null) {
            Intrinsics.throwUninitializedPropertyAccessException("fragment");
            conversationParentFragment = null;
        }
        Stub<ReminderView> stub = conversationParentFragment.reminderView;
        Intrinsics.checkNotNullExpressionValue(stub, "fragment.reminderView");
        return stub;
    }

    @Override // org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent
    public DonationPaymentRepository getDonationPaymentRepository() {
        return (DonationPaymentRepository) this.donationPaymentRepository$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent
    public Subject<DonationPaymentComponent.GooglePayResult> getGooglePayResultPublisher() {
        return this.googlePayResultPublisher;
    }
}
