package org.thoughtcrime.securesms.conversation.ui.error;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import j$.util.function.Function;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public final class SafetyNumberChangeViewModel extends ViewModel {
    private final LiveData<List<ChangedRecipient>> changedRecipients;
    private final MutableLiveData<Collection<RecipientId>> recipientIds;
    private final SafetyNumberChangeRepository safetyNumberChangeRepository;
    private final LiveData<SafetyNumberChangeRepository.SafetyNumberChangeState> safetyNumberChangeState;
    private final LiveData<Boolean> trustOrVerifyReady;

    private SafetyNumberChangeViewModel(List<RecipientId> list, Long l, String str, SafetyNumberChangeRepository safetyNumberChangeRepository) {
        this.safetyNumberChangeRepository = safetyNumberChangeRepository;
        MutableLiveData<Collection<RecipientId>> mutableLiveData = new MutableLiveData<>(list);
        this.recipientIds = mutableLiveData;
        LiveData<SafetyNumberChangeRepository.SafetyNumberChangeState> mapAsync = LiveDataUtil.mapAsync(mutableLiveData, new Function(l, str) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ Long f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SafetyNumberChangeViewModel.this.lambda$new$0(this.f$1, this.f$2, (Collection) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        this.safetyNumberChangeState = mapAsync;
        this.changedRecipients = Transformations.map(mapAsync, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeViewModel$$ExternalSyntheticLambda1
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((SafetyNumberChangeRepository.SafetyNumberChangeState) obj).getChangedRecipients();
            }
        });
        this.trustOrVerifyReady = Transformations.map(mapAsync, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeViewModel$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((SafetyNumberChangeRepository.SafetyNumberChangeState) obj));
            }
        });
    }

    public /* synthetic */ SafetyNumberChangeRepository.SafetyNumberChangeState lambda$new$0(Long l, String str, Collection collection) {
        return this.safetyNumberChangeRepository.getSafetyNumberChangeState(collection, l, str);
    }

    public LiveData<List<ChangedRecipient>> getChangedRecipients() {
        return this.changedRecipients;
    }

    public LiveData<Boolean> getTrustOrVerifyReady() {
        return this.trustOrVerifyReady;
    }

    public LiveData<TrustAndVerifyResult> trustOrVerifyChangedRecipients() {
        SafetyNumberChangeRepository.SafetyNumberChangeState value = this.safetyNumberChangeState.getValue();
        Objects.requireNonNull(value);
        if (value.getMessageRecord() != null) {
            return this.safetyNumberChangeRepository.trustOrVerifyChangedRecipientsAndResend(value.getChangedRecipients(), value.getMessageRecord());
        }
        return this.safetyNumberChangeRepository.trustOrVerifyChangedRecipients(value.getChangedRecipients());
    }

    public void updateRecipients(Collection<RecipientId> collection) {
        this.recipientIds.setValue(collection);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Long messageId;
        private final String messageType;
        private final List<RecipientId> recipientIds;

        public Factory(List<RecipientId> list, Long l, String str) {
            this.recipientIds = list;
            this.messageId = l;
            this.messageType = str;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new SafetyNumberChangeViewModel(this.recipientIds, this.messageId, this.messageType, new SafetyNumberChangeRepository(ApplicationDependencies.getApplication())));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
