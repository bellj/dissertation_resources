package org.thoughtcrime.securesms.conversation;

import android.animation.LayoutTransition;
import kotlin.Metadata;

/* compiled from: BodyBubbleLayoutTransition.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/BodyBubbleLayoutTransition;", "Landroid/animation/LayoutTransition;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BodyBubbleLayoutTransition extends LayoutTransition {
    public BodyBubbleLayoutTransition() {
        disableTransitionType(2);
        disableTransitionType(3);
        disableTransitionType(0);
        disableTransitionType(4);
        setDuration(100);
    }
}
