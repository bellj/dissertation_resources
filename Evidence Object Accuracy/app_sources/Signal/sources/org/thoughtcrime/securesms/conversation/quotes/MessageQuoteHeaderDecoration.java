package org.thoughtcrime.securesms.conversation.quotes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.ViewGroupKt;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Iterator;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: MessageQuoteHeaderDecoration.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J(\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J \u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0016\u001a\u00020\u0017H\u0016R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/quotes/MessageQuoteHeaderDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "cachedHeader", "Landroid/view/View;", "dividerHeight", "", "dividerMargin", "dividerPaint", "Landroid/graphics/Paint;", "dividerRect", "Landroid/graphics/Rect;", "headerMargin", "getHeader", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "getItemOffsets", "", "outRect", "view", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "onDrawOver", "canvas", "Landroid/graphics/Canvas;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageQuoteHeaderDecoration extends RecyclerView.ItemDecoration {
    private View cachedHeader;
    private final int dividerHeight;
    private final int dividerMargin;
    private final Paint dividerPaint;
    private final Rect dividerRect = new Rect();
    private final int headerMargin;

    public MessageQuoteHeaderDecoration(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.dividerMargin = ViewUtil.dpToPx(context, 32);
        this.dividerHeight = ViewUtil.dpToPx(context, 2);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(context.getResources().getColor(R.color.signal_colorSurfaceVariant));
        this.dividerPaint = paint;
        this.headerMargin = ViewUtil.dpToPx(24);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        int i;
        ViewGroup.MarginLayoutParams marginLayoutParams;
        View view;
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        Iterator<View> it = ViewGroupKt.getChildren(recyclerView).iterator();
        while (true) {
            i = 0;
            marginLayoutParams = null;
            if (!it.hasNext()) {
                view = null;
                break;
            }
            view = it.next();
            boolean z = true;
            if (recyclerView.getChildAdapterPosition(view) != state.getItemCount() - 1) {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        View view2 = view;
        if (view2 != null) {
            Rect rect = this.dividerRect;
            rect.left = recyclerView.getLeft();
            rect.top = view2.getBottom() + this.dividerMargin;
            rect.right = recyclerView.getRight();
            rect.bottom = view2.getBottom() + this.dividerMargin + this.dividerHeight;
            canvas.drawRect(this.dividerRect, this.dividerPaint);
            View header = getHeader(recyclerView);
            canvas.save();
            int left = recyclerView.getLeft();
            ViewGroup.LayoutParams layoutParams = header.getLayoutParams();
            if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
                marginLayoutParams = layoutParams;
            }
            ViewGroup.MarginLayoutParams marginLayoutParams2 = marginLayoutParams;
            if (marginLayoutParams2 != null) {
                i = marginLayoutParams2.leftMargin;
            }
            canvas.translate((float) (left + i), (float) (this.dividerRect.bottom + this.dividerMargin));
            header.draw(canvas);
            canvas.restore();
        }
    }

    private final View getHeader(RecyclerView recyclerView) {
        View view = this.cachedHeader;
        if (view != null) {
            return view;
        }
        View inflate = LayoutInflater.from(recyclerView.getContext()).inflate(R.layout.message_quote_header_decoration, (ViewGroup) recyclerView, false);
        Intrinsics.checkNotNullExpressionValue(inflate, "from(parent.context).inf…ecoration, parent, false)");
        inflate.measure(ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(recyclerView.getWidth(), 1073741824), recyclerView.getPaddingLeft() + recyclerView.getPaddingRight(), inflate.getLayoutParams().width), ViewGroup.getChildMeasureSpec(View.MeasureSpec.makeMeasureSpec(recyclerView.getHeight(), 0), recyclerView.getPaddingTop() + recyclerView.getPaddingBottom(), inflate.getLayoutParams().height));
        ViewGroup.LayoutParams layoutParams = inflate.getLayoutParams();
        if (!(layoutParams instanceof ViewGroup.MarginLayoutParams)) {
            layoutParams = null;
        }
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) layoutParams;
        inflate.layout(marginLayoutParams != null ? marginLayoutParams.leftMargin : 0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
        this.cachedHeader = inflate;
        return inflate;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        if (recyclerView.getChildAdapterPosition(view) == state.getItemCount() - 1) {
            rect.bottom = ViewUtil.dpToPx(view.getContext(), 110);
        }
    }
}
