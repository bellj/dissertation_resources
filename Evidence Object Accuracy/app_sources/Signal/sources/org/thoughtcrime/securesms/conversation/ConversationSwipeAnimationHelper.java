package org.thoughtcrime.securesms.conversation;

import android.animation.ValueAnimator;
import android.content.res.Resources;
import android.view.View;
import android.view.animation.Interpolator;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class ConversationSwipeAnimationHelper {
    private static final Interpolator AVATAR_INTERPOLATOR = new ClampingLinearInterpolator(MAX_DX, (float) dpToPx(8));
    private static final Interpolator BUBBLE_INTERPOLATOR;
    static final float MAX_DX;
    private static final Interpolator QUOTED_ALPHA_INTERPOLATOR = new ClampingLinearInterpolator(REPLY_SCALE_MIN, MAX_DX, 3.0f);
    private static final Interpolator REPLY_ALPHA_INTERPOLATOR = new ClampingLinearInterpolator(MAX_DX, REPLY_SCALE_MIN, REPLY_SCALE_MIN);
    private static final Interpolator REPLY_SCALE_INTERPOLATOR = new ClampingLinearInterpolator(REPLY_SCALE_MIN, REPLY_SCALE_MAX);
    private static final float REPLY_SCALE_MAX;
    private static final float REPLY_SCALE_MIN;
    private static final float REPLY_SCALE_OVERSHOOT;
    private static final long REPLY_SCALE_OVERSHOOT_DURATION;
    private static final Interpolator REPLY_TRANSITION_INTERPOLATOR = new ClampingLinearInterpolator(MAX_DX, (float) dpToPx(10));
    static final float TRIGGER_DX;

    static {
        float dpToPx = (float) dpToPx(64);
        TRIGGER_DX = dpToPx;
        float dpToPx2 = (float) dpToPx(96);
        MAX_DX = dpToPx2;
        BUBBLE_INTERPOLATOR = new BubblePositionInterpolator(MAX_DX, dpToPx, dpToPx2);
        REPLY_ALPHA_INTERPOLATOR = new ClampingLinearInterpolator(MAX_DX, REPLY_SCALE_MIN, REPLY_SCALE_MIN);
        REPLY_TRANSITION_INTERPOLATOR = new ClampingLinearInterpolator(MAX_DX, (float) dpToPx(10));
        AVATAR_INTERPOLATOR = new ClampingLinearInterpolator(MAX_DX, (float) dpToPx(8));
        REPLY_SCALE_INTERPOLATOR = new ClampingLinearInterpolator(REPLY_SCALE_MIN, REPLY_SCALE_MAX);
        QUOTED_ALPHA_INTERPOLATOR = new ClampingLinearInterpolator(REPLY_SCALE_MIN, MAX_DX, 3.0f);
    }

    private ConversationSwipeAnimationHelper() {
    }

    public static void update(ConversationItem conversationItem, float f, float f2) {
        float f3 = f / TRIGGER_DX;
        updateBodyBubbleTransition(conversationItem.bodyBubble, f, f2);
        updateReactionsTransition(conversationItem.reactionsView, f, f2);
        updateQuotedIndicatorTransition(conversationItem.quotedIndicator, f, f3, f2);
        updateReplyIconTransition(conversationItem.reply, f, f3, f2);
        updateContactPhotoHolderTransition(conversationItem.contactPhotoHolder, f3, f2);
        updateContactPhotoHolderTransition(conversationItem.badgeImageView, f3, f2);
    }

    public static void trigger(ConversationItem conversationItem) {
        triggerReplyIcon(conversationItem.reply);
    }

    private static void updateBodyBubbleTransition(View view, float f, float f2) {
        view.setTranslationX(BUBBLE_INTERPOLATOR.getInterpolation(f) * f2);
    }

    private static void updateReactionsTransition(View view, float f, float f2) {
        view.setTranslationX(BUBBLE_INTERPOLATOR.getInterpolation(f) * f2);
    }

    private static void updateQuotedIndicatorTransition(View view, float f, float f2, float f3) {
        if (view != null) {
            view.setTranslationX(BUBBLE_INTERPOLATOR.getInterpolation(f) * f3);
            view.setAlpha(QUOTED_ALPHA_INTERPOLATOR.getInterpolation(f2));
        }
    }

    private static void updateReplyIconTransition(View view, float f, float f2, float f3) {
        if (f2 > 0.05f) {
            view.setAlpha(REPLY_ALPHA_INTERPOLATOR.getInterpolation(f2));
        } else {
            view.setAlpha(MAX_DX);
        }
        view.setTranslationX(REPLY_TRANSITION_INTERPOLATOR.getInterpolation(f2) * f3);
        if (f < TRIGGER_DX) {
            float interpolation = REPLY_SCALE_INTERPOLATOR.getInterpolation(f2);
            view.setScaleX(interpolation);
            view.setScaleY(interpolation);
        }
    }

    private static void updateContactPhotoHolderTransition(View view, float f, float f2) {
        if (view != null) {
            view.setTranslationX(AVATAR_INTERPOLATOR.getInterpolation(f) * f2);
        }
    }

    private static void triggerReplyIcon(View view) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(REPLY_SCALE_MAX, REPLY_SCALE_OVERSHOOT, REPLY_SCALE_MAX);
        ofFloat.setDuration(REPLY_SCALE_OVERSHOOT_DURATION);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(view) { // from class: org.thoughtcrime.securesms.conversation.ConversationSwipeAnimationHelper$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                ConversationSwipeAnimationHelper.lambda$triggerReplyIcon$0(this.f$0, valueAnimator);
            }
        });
        ofFloat.start();
    }

    public static /* synthetic */ void lambda$triggerReplyIcon$0(View view, ValueAnimator valueAnimator) {
        view.setScaleX(((Float) valueAnimator.getAnimatedValue()).floatValue());
        view.setScaleY(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }

    private static int dpToPx(int i) {
        return (int) (((float) i) * Resources.getSystem().getDisplayMetrics().density);
    }

    /* loaded from: classes4.dex */
    private static final class BubblePositionInterpolator implements Interpolator {
        private final float end;
        private final float middle;
        private final float start;

        private BubblePositionInterpolator(float f, float f2, float f3) {
            this.start = f;
            this.middle = f2;
            this.end = f3;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            float f2 = this.start;
            if (f < f2) {
                return f2;
            }
            float f3 = this.middle;
            if (f < f3) {
                return f;
            }
            float f4 = this.end;
            float f5 = f4 - f3;
            return Math.min(f3 + (f5 * ((f - f3) / f5) * (f3 / (f * 2.0f))), f4);
        }
    }

    /* loaded from: classes4.dex */
    private static final class ClampingLinearInterpolator implements Interpolator {
        private final float max;
        private final float min;
        private final float slope;
        private final float yIntercept;

        ClampingLinearInterpolator(float f, float f2) {
            this(f, f2, ConversationSwipeAnimationHelper.REPLY_SCALE_MIN);
        }

        ClampingLinearInterpolator(float f, float f2, float f3) {
            this.slope = (f2 - f) * f3;
            this.yIntercept = f;
            this.max = Math.max(f, f2);
            this.min = Math.min(f, f2);
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            return Util.clamp((this.slope * f) + this.yIntercept, this.min, this.max);
        }
    }
}
