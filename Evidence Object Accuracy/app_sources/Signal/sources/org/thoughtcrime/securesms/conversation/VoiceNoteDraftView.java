package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.lifecycle.Observer;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AudioView;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.mms.AudioSlide;

/* compiled from: VoiceNoteDraftView.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\u001e\u001fB%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\r\u001a\u00020\fR\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\"\u0010\r\u001a\u0004\u0018\u00010\f2\b\u0010\u000b\u001a\u0004\u0018\u00010\f@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u00178F¢\u0006\u0006\u001a\u0004\b\u0019\u0010\u001a¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView;", "Landroidx/appcompat/widget/LinearLayoutCompat;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "audioView", "Lorg/thoughtcrime/securesms/components/AudioView;", "<set-?>", "Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "draft", "getDraft", "()Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "listener", "Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView$Listener;", "getListener", "()Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView$Listener;", "setListener", "(Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView$Listener;)V", "playbackStateObserver", "Landroidx/lifecycle/Observer;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState;", "getPlaybackStateObserver", "()Landroidx/lifecycle/Observer;", "clearDraft", "", "setDraft", "AudioViewCallbacksAdapter", "Listener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNoteDraftView extends LinearLayoutCompat {
    private AudioView audioView;
    private DraftDatabase.Draft draft;
    private Listener listener;

    /* compiled from: VoiceNoteDraftView.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J\u0018\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\tH&J\u0018\u0010\n\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\b\u001a\u00020\tH&¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView$Listener;", "", "onVoiceNoteDraftDelete", "", "audioUri", "Landroid/net/Uri;", "onVoiceNoteDraftPause", "onVoiceNoteDraftPlay", "progress", "", "onVoiceNoteDraftSeekTo", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Listener {
        void onVoiceNoteDraftDelete(Uri uri);

        void onVoiceNoteDraftPause(Uri uri);

        void onVoiceNoteDraftPlay(Uri uri, double d);

        void onVoiceNoteDraftSeekTo(Uri uri, double d);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public VoiceNoteDraftView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public VoiceNoteDraftView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ VoiceNoteDraftView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceNoteDraftView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.voice_note_draft_view, this);
        View findViewById = findViewById(R.id.voice_note_draft_delete);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.voice_note_draft_delete)");
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.VoiceNoteDraftView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                VoiceNoteDraftView.m1475_init_$lambda0(VoiceNoteDraftView.this, view);
            }
        });
        View findViewById2 = findViewById(R.id.voice_note_audio_view);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.voice_note_audio_view)");
        this.audioView = (AudioView) findViewById2;
    }

    public final Listener getListener() {
        return this.listener;
    }

    public final void setListener(Listener listener) {
        this.listener = listener;
    }

    public final DraftDatabase.Draft getDraft() {
        return this.draft;
    }

    public final Observer<VoiceNotePlaybackState> getPlaybackStateObserver() {
        AudioView audioView = this.audioView;
        if (audioView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("audioView");
            audioView = null;
        }
        Observer<VoiceNotePlaybackState> playbackStateObserver = audioView.getPlaybackStateObserver();
        Intrinsics.checkNotNullExpressionValue(playbackStateObserver, "audioView.playbackStateObserver");
        return playbackStateObserver;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1475_init_$lambda0(VoiceNoteDraftView voiceNoteDraftView, View view) {
        Listener listener;
        Intrinsics.checkNotNullParameter(voiceNoteDraftView, "this$0");
        if (voiceNoteDraftView.draft != null) {
            AudioView audioView = voiceNoteDraftView.audioView;
            if (audioView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("audioView");
                audioView = null;
            }
            Uri audioSlideUri = audioView.getAudioSlideUri();
            if (audioSlideUri != null && (listener = voiceNoteDraftView.listener) != null) {
                listener.onVoiceNoteDraftDelete(audioSlideUri);
            }
        }
    }

    public final void clearDraft() {
        this.draft = null;
    }

    public final void setDraft(DraftDatabase.Draft draft) {
        Intrinsics.checkNotNullParameter(draft, "draft");
        AudioView audioView = this.audioView;
        if (audioView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("audioView");
            audioView = null;
        }
        audioView.setAudio(AudioSlide.createFromVoiceNoteDraft(getContext(), draft), new AudioViewCallbacksAdapter(), true, false);
        this.draft = draft;
    }

    /* compiled from: VoiceNoteDraftView.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0018\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0016J\u0018\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\fH\u0016J\u0018\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0016J\u0018\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u0010\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView$AudioViewCallbacksAdapter;", "Lorg/thoughtcrime/securesms/components/AudioView$Callbacks;", "(Lorg/thoughtcrime/securesms/conversation/VoiceNoteDraftView;)V", "onPause", "", "audioUri", "Landroid/net/Uri;", "onPlay", "progress", "", "onProgressUpdated", "durationMillis", "", "playheadMillis", "onSeekTo", "onSpeedChanged", "speed", "", "isPlaying", "", "onStopAndReset", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class AudioViewCallbacksAdapter implements AudioView.Callbacks {
        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onProgressUpdated(long j, long j2) {
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onSpeedChanged(float f, boolean z) {
        }

        public AudioViewCallbacksAdapter() {
            VoiceNoteDraftView.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onPlay(Uri uri, double d) {
            Intrinsics.checkNotNullParameter(uri, "audioUri");
            Listener listener = VoiceNoteDraftView.this.getListener();
            if (listener != null) {
                listener.onVoiceNoteDraftPlay(uri, d);
            }
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onPause(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "audioUri");
            Listener listener = VoiceNoteDraftView.this.getListener();
            if (listener != null) {
                listener.onVoiceNoteDraftPause(uri);
            }
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onSeekTo(Uri uri, double d) {
            Intrinsics.checkNotNullParameter(uri, "audioUri");
            Listener listener = VoiceNoteDraftView.this.getListener();
            if (listener != null) {
                listener.onVoiceNoteDraftSeekTo(uri, d);
            }
        }

        @Override // org.thoughtcrime.securesms.components.AudioView.Callbacks
        public void onStopAndReset(Uri uri) {
            Intrinsics.checkNotNullParameter(uri, "audioUri");
            throw new UnsupportedOperationException();
        }
    }
}
