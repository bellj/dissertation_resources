package org.thoughtcrime.securesms.conversation.colors;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.view.View;
import android.widget.EdgeEffect;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.RotatableGradientDrawable;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;

/* compiled from: RecyclerViewColorizer.kt */
@Metadata(d1 = {"\u0000K\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000*\u0003\n\r\u0013\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u0017H\u0002J\u000e\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u0007\u001a\u00020\bR\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0004\n\u0002\u0010\u000bR\u0010\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0004\n\u0002\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u00020\u0013X\u0004¢\u0006\u0004\n\u0002\u0010\u0014R\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u000e¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer;", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "(Landroidx/recyclerview/widget/RecyclerView;)V", "bottomEdgeEffect", "Landroid/widget/EdgeEffect;", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "edgeEffectFactory", "org/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer$edgeEffectFactory$1", "Lorg/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer$edgeEffectFactory$1;", "itemDecoration", "org/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer$itemDecoration$1", "Lorg/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer$itemDecoration$1;", "layerXfermode", "Landroid/graphics/PorterDuffXfermode;", "noLayerXfermode", "scrollListener", "org/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer$scrollListener$1", "Lorg/thoughtcrime/securesms/conversation/colors/RecyclerViewColorizer$scrollListener$1;", "topEdgeEffect", "useLayer", "", "getLayoutManager", "Landroidx/recyclerview/widget/LinearLayoutManager;", "isOverscrolled", "setChatColors", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RecyclerViewColorizer {
    private EdgeEffect bottomEdgeEffect;
    private ChatColors chatColors;
    private final RecyclerViewColorizer$edgeEffectFactory$1 edgeEffectFactory;
    private final RecyclerViewColorizer$itemDecoration$1 itemDecoration;
    private final PorterDuffXfermode layerXfermode = new PorterDuffXfermode(PorterDuff.Mode.XOR);
    private final PorterDuffXfermode noLayerXfermode = new PorterDuffXfermode(PorterDuff.Mode.DST_OVER);
    private final RecyclerView recyclerView;
    private final RecyclerViewColorizer$scrollListener$1 scrollListener;
    private EdgeEffect topEdgeEffect;
    private boolean useLayer;

    public RecyclerViewColorizer(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        this.recyclerView = recyclerView;
        RecyclerViewColorizer$edgeEffectFactory$1 recyclerViewColorizer$edgeEffectFactory$1 = new RecyclerView.EdgeEffectFactory(this) { // from class: org.thoughtcrime.securesms.conversation.colors.RecyclerViewColorizer$edgeEffectFactory$1
            final /* synthetic */ RecyclerViewColorizer this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* access modifiers changed from: protected */
            @Override // androidx.recyclerview.widget.RecyclerView.EdgeEffectFactory
            public EdgeEffect createEdgeEffect(RecyclerView recyclerView2, int i) {
                Intrinsics.checkNotNullParameter(recyclerView2, "view");
                EdgeEffect createEdgeEffect = super.createEdgeEffect(recyclerView2, i);
                Intrinsics.checkNotNullExpressionValue(createEdgeEffect, "super.createEdgeEffect(view, direction)");
                if (i == 1) {
                    this.this$0.topEdgeEffect = createEdgeEffect;
                } else if (i == 3) {
                    this.this$0.bottomEdgeEffect = createEdgeEffect;
                }
                return createEdgeEffect;
            }
        };
        this.edgeEffectFactory = recyclerViewColorizer$edgeEffectFactory$1;
        RecyclerViewColorizer$scrollListener$1 recyclerViewColorizer$scrollListener$1 = new RecyclerView.OnScrollListener(this) { // from class: org.thoughtcrime.securesms.conversation.colors.RecyclerViewColorizer$scrollListener$1
            final /* synthetic */ RecyclerViewColorizer this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView2, int i, int i2) {
                Intrinsics.checkNotNullParameter(recyclerView2, "recyclerView");
                super.onScrolled(recyclerView2, i, i2);
                int findFirstVisibleItemPosition = this.this$0.getLayoutManager().findFirstVisibleItemPosition();
                int findLastVisibleItemPosition = this.this$0.getLayoutManager().findLastVisibleItemPosition();
                int itemCount = this.this$0.getLayoutManager().getItemCount();
                boolean z = findFirstVisibleItemPosition == 0 && itemCount >= 1;
                boolean z2 = findLastVisibleItemPosition == itemCount + -1 && itemCount >= 1;
                if (z || z2 || (this.this$0.isOverscrolled())) {
                    this.this$0.useLayer = true;
                    recyclerView2.setLayerType(2, null);
                    return;
                }
                this.this$0.useLayer = false;
                recyclerView2.setLayerType(0, null);
            }
        };
        this.scrollListener = recyclerViewColorizer$scrollListener$1;
        RecyclerViewColorizer$itemDecoration$1 recyclerViewColorizer$itemDecoration$1 = new RecyclerView.ItemDecoration(this) { // from class: org.thoughtcrime.securesms.conversation.colors.RecyclerViewColorizer$itemDecoration$1
            private final Paint colorPaint = new Paint();
            private final Paint holePunchPaint;
            private final Paint outOfBoundsPaint = new Paint();
            final /* synthetic */ RecyclerViewColorizer this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r3;
                Paint paint = new Paint();
                paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
                paint.setColor(-16777216);
                this.holePunchPaint = paint;
            }

            @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
            public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView2, RecyclerView.State state) {
                Intrinsics.checkNotNullParameter(rect, "outRect");
                Intrinsics.checkNotNullParameter(view, "view");
                Intrinsics.checkNotNullParameter(recyclerView2, "parent");
                Intrinsics.checkNotNullParameter(state, "state");
                rect.setEmpty();
            }

            @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
            public void onDraw(Canvas canvas, RecyclerView recyclerView2, RecyclerView.State state) {
                Intrinsics.checkNotNullParameter(canvas, "c");
                Intrinsics.checkNotNullParameter(recyclerView2, "parent");
                Intrinsics.checkNotNullParameter(state, "state");
                super.onDraw(canvas, recyclerView2, state);
                ChatColors chatColors = this.this$0.chatColors;
                if (chatColors != null) {
                    if (this.this$0.useLayer) {
                        canvas.drawColor(-1);
                    }
                    int childCount = recyclerView2.getChildCount();
                    for (int i = 0; i < childCount; i++) {
                        View childAt = recyclerView2.getChildAt(i);
                        if (childAt != null) {
                            RecyclerView.ViewHolder childViewHolder = recyclerView2.getChildViewHolder(childAt);
                            if (childViewHolder instanceof Colorizable) {
                                ProjectionList<Projection> colorizerProjections = ((Colorizable) childViewHolder).getColorizerProjections(recyclerView2);
                                th = null;
                                try {
                                    for (Projection projection : colorizerProjections) {
                                        canvas.drawPath(projection.getPath(), this.holePunchPaint);
                                    }
                                    Unit unit = Unit.INSTANCE;
                                } finally {
                                    try {
                                        throw th;
                                    } finally {
                                    }
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                    drawShaderMask(canvas, recyclerView2, chatColors);
                }
            }

            private final void drawShaderMask(Canvas canvas, RecyclerView recyclerView2, ChatColors chatColors) {
                if (this.this$0.useLayer) {
                    this.colorPaint.setXfermode(this.this$0.layerXfermode);
                } else {
                    this.colorPaint.setXfermode(this.this$0.noLayerXfermode);
                }
                if (chatColors.isGradient()) {
                    RotatableGradientDrawable rotatableGradientDrawable = (RotatableGradientDrawable) chatColors.getChatBubbleMask();
                    rotatableGradientDrawable.setXfermode(this.colorPaint.getXfermode());
                    rotatableGradientDrawable.setBounds(0, 0, recyclerView2.getWidth(), recyclerView2.getHeight());
                    rotatableGradientDrawable.draw(canvas);
                    return;
                }
                this.colorPaint.setColor(chatColors.asSingleColor());
                canvas.drawRect(0.0f, 0.0f, (float) recyclerView2.getWidth(), (float) recyclerView2.getHeight(), this.colorPaint);
                this.outOfBoundsPaint.setColor(chatColors.asSingleColor());
                canvas.drawRect(0.0f, -((float) recyclerView2.getHeight()), (float) recyclerView2.getWidth(), 0.0f, this.outOfBoundsPaint);
                canvas.drawRect(0.0f, (float) recyclerView2.getHeight(), (float) recyclerView2.getWidth(), ((float) recyclerView2.getHeight()) * 2.0f, this.outOfBoundsPaint);
            }
        };
        this.itemDecoration = recyclerViewColorizer$itemDecoration$1;
        recyclerView.setEdgeEffectFactory(recyclerViewColorizer$edgeEffectFactory$1);
        recyclerView.addOnScrollListener(recyclerViewColorizer$scrollListener$1);
        recyclerView.addItemDecoration(recyclerViewColorizer$itemDecoration$1);
    }

    public final LinearLayoutManager getLayoutManager() {
        RecyclerView.LayoutManager layoutManager = this.recyclerView.getLayoutManager();
        if (layoutManager != null) {
            return (LinearLayoutManager) layoutManager;
        }
        throw new NullPointerException("null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager");
    }

    public final void setChatColors(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.chatColors = chatColors;
        this.recyclerView.invalidateItemDecorations();
    }

    public final boolean isOverscrolled() {
        EdgeEffect edgeEffect = this.topEdgeEffect;
        boolean isFinished = edgeEffect != null ? edgeEffect.isFinished() : true;
        EdgeEffect edgeEffect2 = this.bottomEdgeEffect;
        boolean isFinished2 = edgeEffect2 != null ? edgeEffect2.isFinished() : true;
        if (!isFinished || !isFinished2) {
            return true;
        }
        return false;
    }
}
