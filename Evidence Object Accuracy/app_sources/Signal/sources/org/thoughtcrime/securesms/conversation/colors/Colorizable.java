package org.thoughtcrime.securesms.conversation.colors;

import android.view.ViewGroup;
import kotlin.Metadata;
import org.thoughtcrime.securesms.util.ProjectionList;

/* compiled from: Colorizable.kt */
@Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/Colorizable;", "", "getColorizerProjections", "Lorg/thoughtcrime/securesms/util/ProjectionList;", "coordinateRoot", "Landroid/view/ViewGroup;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface Colorizable {
    ProjectionList getColorizerProjections(ViewGroup viewGroup);
}
