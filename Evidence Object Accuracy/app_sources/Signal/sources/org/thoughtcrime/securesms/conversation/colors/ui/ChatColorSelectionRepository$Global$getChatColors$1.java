package org.thoughtcrime.securesms.conversation.colors.ui;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: ChatColorSelectionRepository.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "wallpaper", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ChatColorSelectionRepository$Global$getChatColors$1 extends Lambda implements Function1<ChatWallpaper, Unit> {
    final /* synthetic */ Function1<ChatColors, Unit> $consumer;

    /* JADX DEBUG: Multi-variable search result rejected for r1v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.conversation.colors.ChatColors, kotlin.Unit> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatColorSelectionRepository$Global$getChatColors$1(Function1<? super ChatColors, Unit> function1) {
        super(1);
        this.$consumer = function1;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ChatWallpaper chatWallpaper) {
        invoke(chatWallpaper);
        return Unit.INSTANCE;
    }

    public final void invoke(ChatWallpaper chatWallpaper) {
        if (chatWallpaper != null) {
            Function1<ChatColors, Unit> function1 = this.$consumer;
            ChatColors autoChatColors = chatWallpaper.getAutoChatColors();
            Intrinsics.checkNotNullExpressionValue(autoChatColors, "wallpaper.autoChatColors");
            function1.invoke(autoChatColors);
            return;
        }
        this.$consumer.invoke(ChatColorsPalette.Bubbles.getDefault().withId(ChatColors.Id.Auto.INSTANCE));
    }
}
