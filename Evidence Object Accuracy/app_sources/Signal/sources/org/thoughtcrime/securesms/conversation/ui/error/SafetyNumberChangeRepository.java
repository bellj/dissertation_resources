package org.thoughtcrime.securesms.conversation.ui.error;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.SignalProtocolAddress;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.safety.SafetyNumberRecipient;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes4.dex */
public final class SafetyNumberChangeRepository {
    private static final String TAG = Log.tag(SafetyNumberChangeRepository.class);
    private final Context context;

    public SafetyNumberChangeRepository(Context context) {
        this.context = context.getApplicationContext();
    }

    public Single<TrustAndVerifyResult> trustOrVerifyChangedRecipientsRx(List<SafetyNumberRecipient> list) {
        String str = TAG;
        Log.d(str, "Trust or verify changed recipients for: " + Util.join(list, ","));
        return Single.fromCallable(new Callable(list) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return SafetyNumberChangeRepository.$r8$lambda$0JdXa6FCYUnLjNB7zsq1HAE8AD0(SafetyNumberChangeRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
    }

    public /* synthetic */ TrustAndVerifyResult lambda$trustOrVerifyChangedRecipientsRx$0(List list) throws Exception {
        return trustOrVerifyChangedRecipientsInternal(fromSafetyNumberRecipients(list));
    }

    public Single<TrustAndVerifyResult> trustOrVerifyChangedRecipientsAndResendRx(List<SafetyNumberRecipient> list, MessageId messageId) {
        String str = TAG;
        Log.d(str, "Trust or verify changed recipients and resend message: " + messageId + " for: " + Util.join(list, ","));
        return Single.fromCallable(new Callable(messageId, list) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ MessageId f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return SafetyNumberChangeRepository.m1614$r8$lambda$7NpKPVdM3352pol2AyCJxx4FKA(SafetyNumberChangeRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
    }

    public /* synthetic */ TrustAndVerifyResult lambda$trustOrVerifyChangedRecipientsAndResendRx$1(MessageId messageId, List list) throws Exception {
        MessageRecord messageRecord;
        if (messageId.isMms()) {
            messageRecord = SignalDatabase.mms().getMessageRecord(messageId.getId());
        } else {
            messageRecord = SignalDatabase.sms().getMessageRecord(messageId.getId());
        }
        return trustOrVerifyChangedRecipientsAndResendInternal(fromSafetyNumberRecipients(list), messageRecord);
    }

    public LiveData<TrustAndVerifyResult> trustOrVerifyChangedRecipients(List<ChangedRecipient> list) {
        String str = TAG;
        Log.d(str, "Trust or verify changed recipients for: " + Util.join(list, ","));
        MutableLiveData mutableLiveData = new MutableLiveData();
        SignalExecutors.BOUNDED.execute(new Runnable(mutableLiveData, list) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ MutableLiveData f$1;
            public final /* synthetic */ List f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SafetyNumberChangeRepository.$r8$lambda$vs9_CcE5ZwrMDtX0lRaceD7TLLM(SafetyNumberChangeRepository.this, this.f$1, this.f$2);
            }
        });
        return mutableLiveData;
    }

    public /* synthetic */ void lambda$trustOrVerifyChangedRecipients$2(MutableLiveData mutableLiveData, List list) {
        mutableLiveData.postValue(trustOrVerifyChangedRecipientsInternal(list));
    }

    public LiveData<TrustAndVerifyResult> trustOrVerifyChangedRecipientsAndResend(List<ChangedRecipient> list, MessageRecord messageRecord) {
        String str = TAG;
        Log.d(str, "Trust or verify changed recipients and resend message: " + messageRecord.getId() + " for: " + Util.join(list, ","));
        MutableLiveData mutableLiveData = new MutableLiveData();
        SignalExecutors.BOUNDED.execute(new Runnable(mutableLiveData, list, messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ MutableLiveData f$1;
            public final /* synthetic */ List f$2;
            public final /* synthetic */ MessageRecord f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SafetyNumberChangeRepository.m1616$r8$lambda$TTh3OXuJ1HHPuQrvpXQUKPNZLs(SafetyNumberChangeRepository.this, this.f$1, this.f$2, this.f$3);
            }
        });
        return mutableLiveData;
    }

    public /* synthetic */ void lambda$trustOrVerifyChangedRecipientsAndResend$3(MutableLiveData mutableLiveData, List list, MessageRecord messageRecord) {
        mutableLiveData.postValue(trustOrVerifyChangedRecipientsAndResendInternal(list, messageRecord));
    }

    public SafetyNumberChangeState getSafetyNumberChangeState(Collection<RecipientId> collection, Long l, String str) {
        MessageRecord messageRecord = (l == null || str == null) ? null : getMessageRecord(l, str);
        List list = Stream.of(ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecords(Stream.of(collection).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList()).getIdentityRecords()).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SafetyNumberChangeRepository.m1615$r8$lambda$9KFA81XkoUOsQZfzWgVOmLZzOI((IdentityRecord) obj);
            }
        }).toList();
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Safety number change state, message: ");
        sb.append(messageRecord != null ? Long.valueOf(messageRecord.getId()) : "null");
        sb.append(" records: ");
        sb.append(Util.join((Collection) list, ","));
        Log.d(str2, sb.toString());
        return new SafetyNumberChangeState(list, messageRecord);
    }

    public static /* synthetic */ ChangedRecipient lambda$getSafetyNumberChangeState$4(IdentityRecord identityRecord) {
        return new ChangedRecipient(Recipient.resolved(identityRecord.getRecipientId()), identityRecord);
    }

    private List<ChangedRecipient> fromSafetyNumberRecipients(List<SafetyNumberRecipient> list) {
        return (List) Collection$EL.stream(list).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return SafetyNumberChangeRepository.$r8$lambda$o3jsbYhu_VqOCXd4Lhkezq_0mwo(SafetyNumberChangeRepository.this, (SafetyNumberRecipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
    }

    public ChangedRecipient fromSafetyNumberRecipient(SafetyNumberRecipient safetyNumberRecipient) {
        return new ChangedRecipient(safetyNumberRecipient.getRecipient(), safetyNumberRecipient.getIdentityRecord());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0026, code lost:
        if (r0 != 1) goto L_0x0035;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0034, code lost:
        return org.thoughtcrime.securesms.database.SignalDatabase.mms().getMessageRecord(r5.longValue());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x003c, code lost:
        throw new java.lang.AssertionError("no valid message type specified");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.thoughtcrime.securesms.database.model.MessageRecord getMessageRecord(java.lang.Long r5, java.lang.String r6) {
        /*
            r4 = this;
            r0 = -1
            int r1 = r6.hashCode()     // Catch: NoSuchMessageException -> 0x004a
            r2 = 108243(0x1a6d3, float:1.51681E-40)
            r3 = 1
            if (r1 == r2) goto L_0x001b
            r2 = 114009(0x1bd59, float:1.5976E-40)
            if (r1 == r2) goto L_0x0011
            goto L_0x0024
        L_0x0011:
            java.lang.String r1 = "sms"
            boolean r6 = r6.equals(r1)     // Catch: NoSuchMessageException -> 0x004a
            if (r6 == 0) goto L_0x0024
            r0 = 0
            goto L_0x0024
        L_0x001b:
            java.lang.String r1 = "mms"
            boolean r6 = r6.equals(r1)     // Catch: NoSuchMessageException -> 0x004a
            if (r6 == 0) goto L_0x0024
            r0 = 1
        L_0x0024:
            if (r0 == 0) goto L_0x003d
            if (r0 != r3) goto L_0x0035
            org.thoughtcrime.securesms.database.MmsDatabase r6 = org.thoughtcrime.securesms.database.SignalDatabase.mms()     // Catch: NoSuchMessageException -> 0x004a
            long r0 = r5.longValue()     // Catch: NoSuchMessageException -> 0x004a
            org.thoughtcrime.securesms.database.model.MessageRecord r5 = r6.getMessageRecord(r0)     // Catch: NoSuchMessageException -> 0x004a
            return r5
        L_0x0035:
            java.lang.AssertionError r5 = new java.lang.AssertionError     // Catch: NoSuchMessageException -> 0x004a
            java.lang.String r6 = "no valid message type specified"
            r5.<init>(r6)     // Catch: NoSuchMessageException -> 0x004a
            throw r5     // Catch: NoSuchMessageException -> 0x004a
        L_0x003d:
            org.thoughtcrime.securesms.database.SmsDatabase r6 = org.thoughtcrime.securesms.database.SignalDatabase.sms()     // Catch: NoSuchMessageException -> 0x004a
            long r0 = r5.longValue()     // Catch: NoSuchMessageException -> 0x004a
            org.thoughtcrime.securesms.database.model.MessageRecord r5 = r6.getMessageRecord(r0)     // Catch: NoSuchMessageException -> 0x004a
            return r5
        L_0x004a:
            r5 = move-exception
            java.lang.String r6 = org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository.TAG
            org.signal.core.util.logging.Log.i(r6, r5)
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ui.error.SafetyNumberChangeRepository.getMessageRecord(java.lang.Long, java.lang.String):org.thoughtcrime.securesms.database.model.MessageRecord");
    }

    private TrustAndVerifyResult trustOrVerifyChangedRecipientsInternal(List<ChangedRecipient> list) {
        SignalIdentityKeyStore identities = ApplicationDependencies.getProtocolStore().aci().identities();
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            for (ChangedRecipient changedRecipient : list) {
                IdentityRecord identityRecord = changedRecipient.getIdentityRecord();
                if (changedRecipient.isUnverified()) {
                    String str = TAG;
                    Log.d(str, "Setting " + identityRecord.getRecipientId() + " as verified");
                    ApplicationDependencies.getProtocolStore().aci().identities().setVerified(identityRecord.getRecipientId(), identityRecord.getIdentityKey(), IdentityDatabase.VerifiedStatus.DEFAULT);
                } else {
                    String str2 = TAG;
                    Log.d(str2, "Setting " + identityRecord.getRecipientId() + " as approved");
                    identities.setApproval(identityRecord.getRecipientId(), true);
                }
            }
            if (acquire != null) {
                acquire.close();
            }
            return TrustAndVerifyResult.trustAndVerify(list);
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private TrustAndVerifyResult trustOrVerifyChangedRecipientsAndResendInternal(List<ChangedRecipient> list, MessageRecord messageRecord) {
        if (list.isEmpty()) {
            Log.d(TAG, "No changed recipients to process, will still process message record");
        }
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            for (ChangedRecipient changedRecipient : list) {
                SignalProtocolAddress protocolAddress = changedRecipient.getRecipient().requireServiceId().toProtocolAddress(1);
                String str = TAG;
                Log.d(str, "Saving identity for: " + changedRecipient.getRecipient().getId() + " " + changedRecipient.getIdentityRecord().getIdentityKey().hashCode());
                SignalIdentityKeyStore.SaveResult saveIdentity = ApplicationDependencies.getProtocolStore().aci().identities().saveIdentity(protocolAddress, changedRecipient.getIdentityRecord().getIdentityKey(), true);
                StringBuilder sb = new StringBuilder();
                sb.append("Saving identity result: ");
                sb.append(saveIdentity);
                Log.d(str, sb.toString());
                if (saveIdentity == SignalIdentityKeyStore.SaveResult.NO_CHANGE) {
                    Log.i(str, "Archiving sessions explicitly as they appear to be out of sync.");
                    ApplicationDependencies.getProtocolStore().aci().sessions().archiveSession(changedRecipient.getRecipient().getId(), 1);
                    ApplicationDependencies.getProtocolStore().aci().sessions().archiveSiblingSessions(protocolAddress);
                    SignalDatabase.senderKeyShared().deleteAllFor(changedRecipient.getRecipient().getId());
                }
            }
            if (acquire != null) {
                acquire.close();
            }
            if (messageRecord.isOutgoing()) {
                processOutgoingMessageRecord(list, messageRecord);
            }
            return TrustAndVerifyResult.trustVerifyAndResend(list, messageRecord);
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private void processOutgoingMessageRecord(List<ChangedRecipient> list, MessageRecord messageRecord) {
        Log.d(TAG, "processOutgoingMessageRecord");
        SmsDatabase sms = SignalDatabase.sms();
        MmsDatabase mms = SignalDatabase.mms();
        HashSet hashSet = new HashSet();
        for (ChangedRecipient changedRecipient : list) {
            RecipientId id = changedRecipient.getRecipient().getId();
            IdentityKey identityKey = changedRecipient.getIdentityRecord().getIdentityKey();
            if (messageRecord.isMms()) {
                mms.removeMismatchedIdentity(messageRecord.getId(), id, identityKey);
                if (messageRecord.getRecipient().isDistributionList() || messageRecord.getRecipient().isPushGroup()) {
                    hashSet.add(id);
                } else {
                    MessageSender.resend(this.context, messageRecord);
                }
            } else {
                sms.removeMismatchedIdentity(messageRecord.getId(), id, identityKey);
                MessageSender.resend(this.context, messageRecord);
            }
        }
        if (!Util.hasItems(hashSet)) {
            return;
        }
        if (messageRecord.getRecipient().isPushGroup()) {
            MessageSender.resendGroupMessage(this.context, messageRecord, hashSet);
        } else {
            MessageSender.resendDistributionList(this.context, messageRecord, hashSet);
        }
    }

    /* loaded from: classes4.dex */
    public static final class SafetyNumberChangeState {
        private final List<ChangedRecipient> changedRecipients;
        private final MessageRecord messageRecord;

        SafetyNumberChangeState(List<ChangedRecipient> list, MessageRecord messageRecord) {
            this.changedRecipients = list;
            this.messageRecord = messageRecord;
        }

        public List<ChangedRecipient> getChangedRecipients() {
            return this.changedRecipients;
        }

        public MessageRecord getMessageRecord() {
            return this.messageRecord;
        }
    }
}
