package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FragmentWrapperActivity;
import org.thoughtcrime.securesms.contacts.paged.ContactSearchKey;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.stories.Stories;

/* compiled from: MultiselectForwardActivity.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00182\u00020\u00012\u00020\u0002:\u0002\u0018\u0019B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\f\u001a\u00020\rH\u0016J\b\u0010\u000e\u001a\u00020\u000fH\u0016J\b\u0010\u0010\u001a\u00020\tH\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0016J\b\u0010\u0013\u001a\u00020\rH\u0016J\b\u0010\u0014\u001a\u00020\rH\u0016J\u0010\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016R\u0014\u0010\u0004\u001a\u00020\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\u00020\tXD¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardActivity;", "Lorg/thoughtcrime/securesms/components/FragmentWrapperActivity;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragment$Callback;", "()V", "args", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "getArgs", "()Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "contentViewId", "", "getContentViewId", "()I", "exitFlow", "", "getContainer", "Landroid/view/ViewGroup;", "getDialogBackgroundColor", "getFragment", "Landroidx/fragment/app/Fragment;", "onFinishForwardAction", "onSearchInputFocused", "setResult", "bundle", "Landroid/os/Bundle;", "Companion", "SelectionContract", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectForwardActivity extends FragmentWrapperActivity implements MultiselectForwardFragment.Callback {
    private static final String ARGS;
    public static final Companion Companion = new Companion(null);
    private final int contentViewId = R.layout.multiselect_forward_activity;

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onFinishForwardAction() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void onSearchInputFocused() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return MultiselectForwardFragment.Callback.DefaultImpls.getStorySendRequirements(this);
    }

    /* compiled from: MultiselectForwardActivity.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardActivity$Companion;", "", "()V", "ARGS", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    private final MultiselectForwardFragmentArgs getArgs() {
        Parcelable parcelableExtra = getIntent().getParcelableExtra("args");
        Intrinsics.checkNotNull(parcelableExtra);
        return (MultiselectForwardFragmentArgs) parcelableExtra;
    }

    @Override // org.thoughtcrime.securesms.components.FragmentWrapperActivity
    protected int getContentViewId() {
        return this.contentViewId;
    }

    @Override // org.thoughtcrime.securesms.components.FragmentWrapperActivity
    public Fragment getFragment() {
        return MultiselectForwardFragment.Companion.create(getArgs());
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void exitFlow() {
        getOnBackPressedDispatcher().onBackPressed();
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public void setResult(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        setResult(-1, new Intent().putExtras(bundle));
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public ViewGroup getContainer() {
        View findViewById = findViewById(R.id.fragment_container_wrapper);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.fragment_container_wrapper)");
        return (ViewGroup) findViewById;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment.Callback
    public int getDialogBackgroundColor() {
        return ContextCompat.getColor(this, R.color.signal_colorBackground);
    }

    /* compiled from: MultiselectForwardActivity.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u0014\u0012\u0004\u0012\u00020\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0001B\u0005¢\u0006\u0002\u0010\u0005J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J \u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0007H\u0016¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardActivity$SelectionContract;", "Landroidx/activity/result/contract/ActivityResultContract;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/forward/MultiselectForwardFragmentArgs;", "", "Lorg/thoughtcrime/securesms/contacts/paged/ContactSearchKey$RecipientSearchKey;", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "input", "parseResult", "resultCode", "", "intent", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SelectionContract extends ActivityResultContract<MultiselectForwardFragmentArgs, List<? extends ContactSearchKey.RecipientSearchKey>> {
        public Intent createIntent(Context context, MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(multiselectForwardFragmentArgs, "input");
            Intent putExtra = new Intent(context, MultiselectForwardActivity.class).putExtra("args", multiselectForwardFragmentArgs);
            Intrinsics.checkNotNullExpressionValue(putExtra, "Intent(context, Multisel…va).putExtra(ARGS, input)");
            return putExtra;
        }

        public List<ContactSearchKey.RecipientSearchKey> parseResult(int i, Intent intent) {
            if (i != -1) {
                return CollectionsKt__CollectionsKt.emptyList();
            }
            if (intent == null || !intent.hasExtra(MultiselectForwardFragment.RESULT_SELECTION)) {
                throw new IllegalStateException("Selection contract requires a selection.");
            }
            ArrayList<ContactSearchKey.ParcelableRecipientSearchKey> parcelableArrayListExtra = intent.getParcelableArrayListExtra(MultiselectForwardFragment.RESULT_SELECTION);
            Intrinsics.checkNotNull(parcelableArrayListExtra);
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(parcelableArrayListExtra, 10));
            for (ContactSearchKey.ParcelableRecipientSearchKey parcelableRecipientSearchKey : parcelableArrayListExtra) {
                arrayList.add(parcelableRecipientSearchKey.asRecipientSearchKey());
            }
            return arrayList;
        }
    }
}
