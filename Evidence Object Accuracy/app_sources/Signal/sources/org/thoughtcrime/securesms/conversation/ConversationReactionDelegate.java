package org.thoughtcrime.securesms.conversation;

import android.app.Activity;
import android.graphics.PointF;
import android.view.MotionEvent;
import org.thoughtcrime.securesms.conversation.ConversationReactionOverlay;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public final class ConversationReactionDelegate {
    private final PointF lastSeenDownPoint = new PointF();
    private ConversationReactionOverlay.OnActionSelectedListener onActionSelectedListener;
    private ConversationReactionOverlay.OnHideListener onHideListener;
    private ConversationReactionOverlay.OnReactionSelectedListener onReactionSelectedListener;
    private final Stub<ConversationReactionOverlay> overlayStub;

    public ConversationReactionDelegate(Stub<ConversationReactionOverlay> stub) {
        this.overlayStub = stub;
    }

    public boolean isShowing() {
        return this.overlayStub.resolved() && this.overlayStub.get().isShowing();
    }

    public void show(Activity activity, Recipient recipient, ConversationMessage conversationMessage, boolean z, SelectedConversationModel selectedConversationModel) {
        resolveOverlay().show(activity, recipient, conversationMessage, this.lastSeenDownPoint, z, selectedConversationModel);
    }

    public void hide() {
        this.overlayStub.get().hide();
    }

    public void hideForReactWithAny() {
        this.overlayStub.get().hideForReactWithAny();
    }

    public void setOnReactionSelectedListener(ConversationReactionOverlay.OnReactionSelectedListener onReactionSelectedListener) {
        this.onReactionSelectedListener = onReactionSelectedListener;
        if (this.overlayStub.resolved()) {
            this.overlayStub.get().setOnReactionSelectedListener(onReactionSelectedListener);
        }
    }

    public void setOnActionSelectedListener(ConversationReactionOverlay.OnActionSelectedListener onActionSelectedListener) {
        this.onActionSelectedListener = onActionSelectedListener;
        if (this.overlayStub.resolved()) {
            this.overlayStub.get().setOnActionSelectedListener(onActionSelectedListener);
        }
    }

    public void setOnHideListener(ConversationReactionOverlay.OnHideListener onHideListener) {
        this.onHideListener = onHideListener;
        if (this.overlayStub.resolved()) {
            this.overlayStub.get().setOnHideListener(onHideListener);
        }
    }

    public MessageRecord getMessageRecord() {
        if (this.overlayStub.resolved()) {
            return this.overlayStub.get().getMessageRecord();
        }
        throw new IllegalStateException("Cannot call getMessageRecord right now.");
    }

    public boolean applyTouchEvent(MotionEvent motionEvent) {
        if (this.overlayStub.resolved() && this.overlayStub.get().isShowing()) {
            return this.overlayStub.get().applyTouchEvent(motionEvent);
        }
        if (motionEvent.getAction() != 0) {
            return false;
        }
        this.lastSeenDownPoint.set(motionEvent.getX(), motionEvent.getY());
        return false;
    }

    private ConversationReactionOverlay resolveOverlay() {
        ConversationReactionOverlay conversationReactionOverlay = this.overlayStub.get();
        conversationReactionOverlay.requestFitSystemWindows();
        conversationReactionOverlay.setOnHideListener(this.onHideListener);
        conversationReactionOverlay.setOnActionSelectedListener(this.onActionSelectedListener);
        conversationReactionOverlay.setOnReactionSelectedListener(this.onReactionSelectedListener);
        return conversationReactionOverlay;
    }
}
