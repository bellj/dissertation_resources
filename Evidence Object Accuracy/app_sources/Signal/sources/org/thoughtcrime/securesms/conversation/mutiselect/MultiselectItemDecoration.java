package org.thoughtcrime.securesms.conversation.mutiselect;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewGroupKt;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import com.airbnb.lottie.SimpleColorFilter;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.io.CloseableKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.Sequence;
import kotlin.sequences.SequencesKt___SequencesJvmKt;
import org.signal.core.util.SetUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationAdapter;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* compiled from: MultiselectItemDecoration.kt */
@Metadata(d1 = {"\u0000º\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010%\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u00012\u00020\u0002:\u0001]B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u000e\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006¢\u0006\u0002\u0010\bJ \u0010.\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u000205H\u0002J\u0018\u00106\u001a\u00020/2\u0006\u00102\u001a\u0002032\u0006\u00100\u001a\u000201H\u0002J\u0018\u00107\u001a\u00020/2\u0006\u00102\u001a\u0002032\u0006\u00100\u001a\u000201H\u0002J(\u00108\u001a\u00020/2\u0006\u00102\u001a\u0002032\u0006\u00100\u001a\u0002012\u0006\u00109\u001a\u00020\u00122\u0006\u0010:\u001a\u00020\u0012H\u0002J0\u0010;\u001a\u00020/2\u0006\u00102\u001a\u0002032\u0006\u00100\u001a\u0002012\u0006\u00109\u001a\u00020\u00122\u0006\u0010:\u001a\u00020\u00122\u0006\u0010<\u001a\u00020=H\u0002J0\u0010>\u001a\u00020/2\u0006\u0010?\u001a\u0002032\u0006\u00100\u001a\u0002012\u0006\u00109\u001a\u00020\u00122\u0006\u0010:\u001a\u00020\u00122\u0006\u0010<\u001a\u00020=H\u0002J\u0016\u0010@\u001a\b\u0012\u0004\u0012\u00020\u00170A2\u0006\u00100\u001a\u000201H\u0002J\u001e\u0010B\u001a\u00020C2\f\u0010D\u001a\b\u0012\u0004\u0012\u00020\u00170A2\u0006\u0010E\u001a\u00020\u0017H\u0002J(\u0010F\u001a\u00020/2\u0006\u0010G\u001a\u00020$2\u0006\u0010H\u001a\u00020I2\u0006\u00100\u001a\u0002012\u0006\u0010J\u001a\u00020KH\u0016J\u000e\u0010L\u001a\u00020/2\u0006\u0010M\u001a\u000201J\u0010\u0010N\u001a\u00020/2\u0006\u00100\u001a\u000201H\u0002J\b\u0010O\u001a\u00020PH\u0002J\u0010\u0010Q\u001a\u00020/2\u0006\u0010R\u001a\u00020SH\u0016J\u0010\u0010T\u001a\u00020/2\u0006\u0010R\u001a\u00020SH\u0016J \u0010U\u001a\u00020/2\u0006\u00102\u001a\u0002032\u0006\u00100\u001a\u0002012\u0006\u0010J\u001a\u00020KH\u0016J \u0010V\u001a\u00020/2\u0006\u00102\u001a\u0002032\u0006\u00100\u001a\u0002012\u0006\u0010J\u001a\u00020KH\u0016J\u0010\u0010W\u001a\u00020=2\u0006\u0010E\u001a\u00020\u0017H\u0002J\u0010\u0010X\u001a\u00020/2\b\u0010E\u001a\u0004\u0018\u00010\u0017J\u0018\u0010Y\u001a\u00020/2\u0006\u00100\u001a\u0002012\u0006\u0010Z\u001a\u00020IH\u0002J\u001e\u0010[\u001a\u00020/2\f\u0010D\u001a\b\u0012\u0004\u0012\u00020\u00170A2\u0006\u0010E\u001a\u00020\u0017H\u0002J\f\u0010\\\u001a\u00020/*\u000203H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\u0015X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u001b\u001a\u000e\u0012\u0004\u0012\u00020\u0017\u0012\u0004\u0012\u00020\u00150\u001cX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00170&X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010'\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006^"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "context", "Landroid/content/Context;", "chatWallpaperProvider", "Lkotlin/Function0;", "Lorg/thoughtcrime/securesms/wallpaper/ChatWallpaper;", "(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V", "argbEvaluator", "Landroid/animation/ArgbEvaluator;", "checkDrawable", "Landroid/graphics/drawable/Drawable;", "checkPaint", "Landroid/graphics/Paint;", "checkedBitmap", "Landroid/graphics/Bitmap;", "circleRadius", "", "darkShadeColor", "enterExitAnimation", "Landroid/animation/ValueAnimator;", "focusedItem", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "gutter", "hideShadeAnimation", "lightShadeColor", "multiselectPartAnimatorMap", "", "paddingStart", "path", "Landroid/graphics/Path;", "photoCirclePaddingStart", "photoCirclePaint", "photoCircleRadius", "rect", "Landroid/graphics/Rect;", "selectedParts", "", "shadePaint", "transparentBlack20", "transparentWhite20", "transparentWhite60", "ultramarine", "ultramarine30", "unselectedPaint", "drawChecks", "", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "canvas", "Landroid/graphics/Canvas;", "adapter", "Lorg/thoughtcrime/securesms/conversation/ConversationAdapter;", "drawFocusShadeOverIfNecessary", "drawFocusShadeUnderIfNecessary", "drawPhotoCircle", "topBoundary", "bottomBoundary", "drawSelectedCircle", "alphaProgress", "", "drawUnselectedCircle", "c", "getCurrentSelection", "", "getDifferenceForPart", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectItemDecoration$Difference;", "currentSelection", "multiselectPart", "getItemOffsets", "outRect", "view", "Landroid/view/View;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "hideShade", "list", "invalidateIfAnimatorsAreRunning", "isInitialAnimation", "", "onCreate", "owner", "Landroidx/lifecycle/LifecycleOwner;", "onDestroy", "onDraw", "onDrawOver", "selectedAnimationProgress", "setFocusedItem", "updateChildOffsets", "child", "updateMultiselectPartAnimator", "drawShade", "Difference", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiselectItemDecoration extends RecyclerView.ItemDecoration implements DefaultLifecycleObserver {
    private final ArgbEvaluator argbEvaluator;
    private final Function0<ChatWallpaper> chatWallpaperProvider;
    private final Drawable checkDrawable;
    private final Paint checkPaint;
    private Bitmap checkedBitmap;
    private final int circleRadius;
    private final int darkShadeColor;
    private ValueAnimator enterExitAnimation;
    private MultiselectPart focusedItem;
    private final int gutter = ViewUtil.dpToPx(48);
    private ValueAnimator hideShadeAnimation;
    private final int lightShadeColor;
    private final Map<MultiselectPart, ValueAnimator> multiselectPartAnimatorMap;
    private final int paddingStart = ViewUtil.dpToPx(17);
    private final Path path = new Path();
    private final int photoCirclePaddingStart;
    private final Paint photoCirclePaint;
    private final int photoCircleRadius;
    private final Rect rect = new Rect();
    private final Set<MultiselectPart> selectedParts;
    private final Paint shadePaint;
    private final int transparentBlack20;
    private final int transparentWhite20;
    private final int transparentWhite60;
    private final int ultramarine;
    private final int ultramarine30;
    private final Paint unselectedPaint;

    /* compiled from: MultiselectItemDecoration.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectItemDecoration$Difference;", "", "(Ljava/lang/String;I)V", "REMOVED", "ADDED", "SAME", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Difference {
        REMOVED,
        ADDED,
        SAME
    }

    /* compiled from: MultiselectItemDecoration.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[Difference.values().length];
            iArr[Difference.SAME.ordinal()] = 1;
            iArr[Difference.ADDED.ordinal()] = 2;
            iArr[Difference.REMOVED.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function0<? extends org.thoughtcrime.securesms.wallpaper.ChatWallpaper> */
    /* JADX WARN: Multi-variable type inference failed */
    public MultiselectItemDecoration(Context context, Function0<? extends ChatWallpaper> function0) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(function0, "chatWallpaperProvider");
        this.chatWallpaperProvider = function0;
        int dpToPx = ViewUtil.dpToPx(11);
        this.circleRadius = dpToPx;
        Drawable drawable = AppCompatResources.getDrawable(context, R.drawable.ic_check_circle_solid_24);
        if (drawable != null) {
            drawable.setBounds(0, 0, dpToPx * 2, dpToPx * 2);
            Intrinsics.checkNotNullExpressionValue(drawable, "requireNotNull(AppCompat… 2, circleRadius * 2)\n  }");
            this.checkDrawable = drawable;
            this.photoCircleRadius = ViewUtil.dpToPx(12);
            this.photoCirclePaddingStart = ViewUtil.dpToPx(16);
            int color = ContextCompat.getColor(context, R.color.transparent_black_20);
            this.transparentBlack20 = color;
            this.transparentWhite20 = ContextCompat.getColor(context, R.color.transparent_white_20);
            this.transparentWhite60 = ContextCompat.getColor(context, R.color.transparent_white_60);
            this.ultramarine30 = ContextCompat.getColor(context, R.color.core_ultramarine_33);
            this.ultramarine = ContextCompat.getColor(context, R.color.signal_accent_primary);
            this.selectedParts = new LinkedHashSet();
            this.multiselectPartAnimatorMap = new LinkedHashMap();
            this.darkShadeColor = ContextCompat.getColor(context, R.color.reactions_screen_dark_shade_color);
            this.lightShadeColor = ContextCompat.getColor(context, R.color.reactions_screen_light_shade_color);
            this.argbEvaluator = new ArgbEvaluator();
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setStrokeWidth(1.5f);
            paint.setStyle(Paint.Style.STROKE);
            this.unselectedPaint = paint;
            Paint paint2 = new Paint();
            paint2.setAntiAlias(true);
            paint2.setStyle(Paint.Style.FILL);
            this.shadePaint = paint2;
            Paint paint3 = new Paint();
            paint3.setAntiAlias(true);
            paint3.setStyle(Paint.Style.FILL);
            paint3.setColor(color);
            this.photoCirclePaint = paint3;
            Paint paint4 = new Paint();
            paint4.setAntiAlias(true);
            paint4.setStyle(Paint.Style.FILL);
            this.checkPaint = paint4;
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    public final void setFocusedItem(MultiselectPart multiselectPart) {
        this.focusedItem = multiselectPart;
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onCreate(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        int i = this.circleRadius;
        Bitmap createBitmap = Bitmap.createBitmap(i * 2, i * 2, Bitmap.Config.ARGB_8888);
        this.checkDrawable.draw(new Canvas(createBitmap));
        this.checkedBitmap = createBitmap;
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onDestroy(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        Bitmap bitmap = this.checkedBitmap;
        if (bitmap != null) {
            bitmap.recycle();
        }
        this.checkedBitmap = null;
    }

    private final Set<MultiselectPart> getCurrentSelection(RecyclerView recyclerView) {
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            Set<MultiselectPart> selectedItems = ((ConversationAdapter) adapter).getSelectedItems();
            Intrinsics.checkNotNullExpressionValue(selectedItems, "parent.adapter as Conver…ionAdapter).selectedItems");
            return selectedItems;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.ConversationAdapter");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Float f;
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        Set<MultiselectPart> currentSelection = getCurrentSelection(recyclerView);
        float f2 = 1.0f;
        float f3 = 0.0f;
        if (this.selectedParts.isEmpty() && (!currentSelection.isEmpty())) {
            ValueAnimator valueAnimator = this.enterExitAnimation;
            boolean isRunning = valueAnimator != null ? valueAnimator.isRunning() : false;
            ValueAnimator valueAnimator2 = this.enterExitAnimation;
            if (valueAnimator2 != null) {
                valueAnimator2.end();
            }
            if (isRunning) {
                ValueAnimator valueAnimator3 = this.enterExitAnimation;
                f = valueAnimator3 != null ? Float.valueOf(valueAnimator3.getAnimatedFraction()) : null;
            } else {
                f = Float.valueOf(0.0f);
            }
            float[] fArr = new float[2];
            if (f != null) {
                f3 = f.floatValue();
            }
            fArr[0] = f3;
            fArr[1] = 1.0f;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
            ofFloat.setDuration(150L);
            ofFloat.start();
            this.enterExitAnimation = ofFloat;
        } else if ((!this.selectedParts.isEmpty()) && currentSelection.isEmpty()) {
            ValueAnimator valueAnimator4 = this.enterExitAnimation;
            if (valueAnimator4 != null) {
                valueAnimator4.end();
            }
            float[] fArr2 = new float[2];
            ValueAnimator valueAnimator5 = this.enterExitAnimation;
            if (valueAnimator5 != null) {
                f2 = valueAnimator5.getAnimatedFraction();
            }
            fArr2[0] = f2;
            fArr2[1] = 0.0f;
            ValueAnimator ofFloat2 = ValueAnimator.ofFloat(fArr2);
            ofFloat2.setDuration(150L);
            ofFloat2.start();
            this.enterExitAnimation = ofFloat2;
        }
        if (view instanceof Multiselectable) {
            for (MultiselectPart multiselectPart : ((Multiselectable) view).getConversationMessage().getMultiselectCollection().toSet()) {
                updateMultiselectPartAnimator(currentSelection, multiselectPart);
            }
        }
        this.selectedParts.clear();
        this.selectedParts.addAll(currentSelection);
        rect.setEmpty();
        updateChildOffsets(recyclerView, view);
    }

    /* JADX INFO: finally extract failed */
    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDraw(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        int i;
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            ConversationAdapter conversationAdapter = (ConversationAdapter) adapter;
            if (conversationAdapter.getSelectedItems().isEmpty()) {
                drawFocusShadeUnderIfNecessary(canvas, recyclerView);
                if (this.enterExitAnimation == null || !isInitialAnimation()) {
                    return;
                }
            }
            Paint paint = this.shadePaint;
            if (this.chatWallpaperProvider.invoke() != null) {
                i = this.transparentBlack20;
            } else if (ThemeUtil.isDarkTheme(recyclerView.getContext())) {
                i = this.transparentWhite20;
            } else {
                i = this.ultramarine30;
            }
            paint.setColor(i);
            Iterator it = SequencesKt___SequencesJvmKt.filterIsInstance(ViewGroupKt.getChildren(recyclerView), Multiselectable.class).iterator();
            while (true) {
                boolean z = true;
                if (!it.hasNext()) {
                    break;
                }
                Multiselectable multiselectable = (Multiselectable) it.next();
                updateChildOffsets(recyclerView, (View) multiselectable);
                MultiselectCollection multiselectCollection = multiselectable.getConversationMessage().getMultiselectCollection();
                Intrinsics.checkNotNullExpressionValue(multiselectCollection, "child.conversationMessage.multiselectCollection");
                ProjectionList<Projection> colorizerProjections = multiselectable.getColorizerProjections(recyclerView);
                if (multiselectable.canPlayContent()) {
                    colorizerProjections.add(multiselectable.getGiphyMp4PlayableProjection(recyclerView));
                }
                this.path.reset();
                try {
                    for (Projection projection : colorizerProjections) {
                        projection.applyToPath(this.path);
                    }
                    Unit unit = Unit.INSTANCE;
                    CloseableKt.closeFinally(colorizerProjections, null);
                    canvas.save();
                    canvas.clipPath(this.path, Region.Op.DIFFERENCE);
                    Set intersection = SetUtil.intersection(multiselectCollection.toSet(), conversationAdapter.getSelectedItems());
                    Intrinsics.checkNotNullExpressionValue(intersection, "intersection(parts.toSet(), adapter.selectedItems)");
                    if (!intersection.isEmpty()) {
                        MultiselectPart multiselectPart = (MultiselectPart) CollectionsKt___CollectionsKt.first(intersection);
                        if (intersection.size() != multiselectCollection.getSize() && (!(multiselectPart instanceof MultiselectPart.Text) || !multiselectable.hasNonSelectableMedia())) {
                            z = false;
                        }
                        if (z) {
                            View view = (View) multiselectable;
                            this.rect.set(0, view.getTop(), view.getRight(), view.getBottom());
                        } else {
                            this.rect.set(0, multiselectable.getTopBoundaryOfMultiselectPart(multiselectPart), recyclerView.getRight(), multiselectable.getBottomBoundaryOfMultiselectPart(multiselectPart));
                        }
                        canvas.drawRect(this.rect, this.shadePaint);
                    }
                    canvas.restore();
                } catch (Throwable th) {
                    try {
                        throw th;
                    } catch (Throwable th2) {
                        CloseableKt.closeFinally(colorizerProjections, th);
                        throw th2;
                    }
                }
            }
            Set<MultiselectPart> selectedItems = conversationAdapter.getSelectedItems();
            Intrinsics.checkNotNullExpressionValue(selectedItems, "adapter.selectedItems");
            if (!selectedItems.isEmpty()) {
                drawChecks(recyclerView, canvas, conversationAdapter);
                return;
            }
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.ConversationAdapter");
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter != null) {
            if (((ConversationAdapter) adapter).getSelectedItems().isEmpty()) {
                drawFocusShadeOverIfNecessary(canvas, recyclerView);
            }
            invalidateIfAnimatorsAreRunning(recyclerView);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.ConversationAdapter");
    }

    private final void drawChecks(RecyclerView recyclerView, Canvas canvas, ConversationAdapter conversationAdapter) {
        int i;
        ChatWallpaper invoke = this.chatWallpaperProvider.invoke();
        boolean z = true;
        boolean z2 = invoke != null && invoke.isPhoto();
        Sequence<Multiselectable> sequence = SequencesKt___SequencesJvmKt.filterIsInstance(ViewGroupKt.getChildren(recyclerView), Multiselectable.class);
        boolean isDarkTheme = ThemeUtil.isDarkTheme(recyclerView.getContext());
        Paint paint = this.unselectedPaint;
        ChatWallpaper invoke2 = this.chatWallpaperProvider.invoke();
        if (invoke2 == null || !invoke2.isPhoto()) {
            z = false;
        }
        if (z) {
            i = -1;
        } else if (this.chatWallpaperProvider.invoke() != null || isDarkTheme) {
            i = this.transparentWhite60;
        } else {
            i = this.transparentBlack20;
        }
        paint.setColor(i);
        if (this.chatWallpaperProvider.invoke() != null || isDarkTheme) {
            this.checkPaint.setColorFilter(null);
        } else {
            this.checkPaint.setColorFilter(new SimpleColorFilter(this.ultramarine));
        }
        for (Multiselectable multiselectable : sequence) {
            MultiselectCollection multiselectCollection = multiselectable.getConversationMessage().getMultiselectCollection();
            Intrinsics.checkNotNullExpressionValue(multiselectCollection, "child.conversationMessage.multiselectCollection");
            for (MultiselectPart multiselectPart : multiselectCollection.toSet()) {
                int topBoundaryOfMultiselectPart = multiselectable.getTopBoundaryOfMultiselectPart(multiselectPart);
                int bottomBoundaryOfMultiselectPart = multiselectable.getBottomBoundaryOfMultiselectPart(multiselectPart);
                if (z2) {
                    drawPhotoCircle(canvas, recyclerView, topBoundaryOfMultiselectPart, bottomBoundaryOfMultiselectPart);
                }
                float selectedAnimationProgress = selectedAnimationProgress(multiselectPart);
                if (conversationAdapter.getSelectedItems().contains(multiselectPart)) {
                    drawUnselectedCircle(canvas, recyclerView, topBoundaryOfMultiselectPart, bottomBoundaryOfMultiselectPart, 1.0f - selectedAnimationProgress);
                    drawSelectedCircle(canvas, recyclerView, topBoundaryOfMultiselectPart, bottomBoundaryOfMultiselectPart, selectedAnimationProgress);
                } else {
                    drawUnselectedCircle(canvas, recyclerView, topBoundaryOfMultiselectPart, bottomBoundaryOfMultiselectPart, selectedAnimationProgress);
                    if (!isInitialAnimation()) {
                        drawSelectedCircle(canvas, recyclerView, topBoundaryOfMultiselectPart, bottomBoundaryOfMultiselectPart, 1.0f - selectedAnimationProgress);
                    }
                }
            }
        }
    }

    private final void drawPhotoCircle(Canvas canvas, RecyclerView recyclerView, int i, int i2) {
        int i3;
        if (ViewUtil.isLtr(recyclerView)) {
            i3 = this.photoCirclePaddingStart + this.photoCircleRadius;
        } else {
            i3 = (recyclerView.getRight() - this.photoCircleRadius) - this.photoCirclePaddingStart;
        }
        canvas.drawCircle((float) i3, ((float) i) + (((float) (i2 - i)) / ((float) 2)), (float) this.photoCircleRadius, this.photoCirclePaint);
    }

    private final void drawSelectedCircle(Canvas canvas, RecyclerView recyclerView, int i, int i2, float f) {
        int i3;
        if (ViewUtil.isLtr(recyclerView)) {
            i3 = this.paddingStart;
        } else {
            i3 = (recyclerView.getRight() - this.paddingStart) - (this.circleRadius * 2);
        }
        float f2 = (float) i3;
        float f3 = (((float) i) + (((float) (i2 - i)) / ((float) 2))) - ((float) this.circleRadius);
        Bitmap bitmap = this.checkedBitmap;
        int alpha = this.checkPaint.getAlpha();
        this.checkPaint.setAlpha((int) (((float) alpha) * f));
        if (bitmap != null) {
            canvas.drawBitmap(bitmap, f2, f3, this.checkPaint);
        }
        this.checkPaint.setAlpha(alpha);
    }

    private final void drawUnselectedCircle(Canvas canvas, RecyclerView recyclerView, int i, int i2, float f) {
        int i3;
        if (ViewUtil.isLtr(recyclerView)) {
            i3 = this.paddingStart + this.circleRadius;
        } else {
            i3 = (recyclerView.getRight() - this.circleRadius) - this.paddingStart;
        }
        int alpha = this.unselectedPaint.getAlpha();
        this.unselectedPaint.setAlpha((int) (((float) alpha) * f));
        canvas.drawCircle((float) i3, ((float) i) + (((float) (i2 - i)) / ((float) 2)), (float) this.circleRadius, this.unselectedPaint);
        this.unselectedPaint.setAlpha(alpha);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        if ((!r0.isEmpty()) != false) goto L_0x002d;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void updateChildOffsets(androidx.recyclerview.widget.RecyclerView r6, android.view.View r7) {
        /*
            r5 = this;
            androidx.recyclerview.widget.RecyclerView$Adapter r0 = r6.getAdapter()
            if (r0 == 0) goto L_0x0089
            org.thoughtcrime.securesms.conversation.ConversationAdapter r0 = (org.thoughtcrime.securesms.conversation.ConversationAdapter) r0
            boolean r1 = org.thoughtcrime.securesms.util.ViewUtil.isLtr(r7)
            android.animation.ValueAnimator r2 = r5.enterExitAnimation
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x001a
            boolean r2 = r5.isInitialAnimation()
            if (r2 == 0) goto L_0x001a
            r2 = 1
            goto L_0x001b
        L_0x001a:
            r2 = 0
        L_0x001b:
            if (r2 != 0) goto L_0x002d
            java.util.Set r0 = r0.getSelectedItems()
            java.lang.String r2 = "adapter.selectedItems"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r2)
            boolean r0 = r0.isEmpty()
            r0 = r0 ^ r3
            if (r0 == 0) goto L_0x0080
        L_0x002d:
            boolean r0 = r7 instanceof org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
            if (r0 == 0) goto L_0x0080
            r0 = r7
            org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable r0 = (org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable) r0
            android.view.View r0 = r0.getHorizontalTranslationTarget()
            if (r0 == 0) goto L_0x0088
            if (r1 == 0) goto L_0x0041
            int r6 = r0.getLeft()
            goto L_0x004a
        L_0x0041:
            int r6 = r6.getRight()
            int r0 = r0.getRight()
            int r6 = r6 - r0
        L_0x004a:
            boolean r0 = r5.isInitialAnimation()
            if (r0 == 0) goto L_0x0070
            int r0 = r5.gutter
            int r0 = r0 - r6
            int r6 = java.lang.Math.max(r4, r0)
            float r6 = (float) r6
            android.animation.ValueAnimator r0 = r5.enterExitAnimation
            if (r0 == 0) goto L_0x0061
            java.lang.Object r0 = r0.getAnimatedValue()
            goto L_0x0062
        L_0x0061:
            r0 = 0
        L_0x0062:
            java.lang.Float r0 = (java.lang.Float) r0
            if (r0 == 0) goto L_0x006b
            float r0 = r0.floatValue()
            goto L_0x006d
        L_0x006b:
            r0 = 1065353216(0x3f800000, float:1.0)
        L_0x006d:
            float r6 = r6 * r0
            goto L_0x0078
        L_0x0070:
            int r0 = r5.gutter
            int r0 = r0 - r6
            int r6 = java.lang.Math.max(r4, r0)
            float r6 = (float) r6
        L_0x0078:
            if (r1 == 0) goto L_0x007b
            goto L_0x007c
        L_0x007b:
            float r6 = -r6
        L_0x007c:
            r7.setTranslationX(r6)
            goto L_0x0088
        L_0x0080:
            boolean r6 = r7 instanceof org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
            if (r6 == 0) goto L_0x0088
            r6 = 0
            r7.setTranslationX(r6)
        L_0x0088:
            return
        L_0x0089:
            java.lang.NullPointerException r6 = new java.lang.NullPointerException
            java.lang.String r7 = "null cannot be cast to non-null type org.thoughtcrime.securesms.conversation.ConversationAdapter"
            r6.<init>(r7)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.mutiselect.MultiselectItemDecoration.updateChildOffsets(androidx.recyclerview.widget.RecyclerView, android.view.View):void");
    }

    /* JADX INFO: finally extract failed */
    private final void drawFocusShadeUnderIfNecessary(Canvas canvas, RecyclerView recyclerView) {
        MultiselectPart multiselectPart = this.focusedItem;
        if (multiselectPart != null) {
            this.path.reset();
            canvas.save();
            int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                Intrinsics.checkNotNullExpressionValue(childAt, "getChildAt(index)");
                if ((childAt instanceof Multiselectable) && Intrinsics.areEqual(((Multiselectable) childAt).getConversationMessage(), multiselectPart.getConversationMessage())) {
                    this.path.addRect((float) childAt.getLeft(), (float) childAt.getTop(), (float) childAt.getRight(), (float) childAt.getBottom(), Path.Direction.CW);
                    Multiselectable multiselectable = (Multiselectable) childAt;
                    ProjectionList<Projection> colorizerProjections = multiselectable.getColorizerProjections(recyclerView);
                    try {
                        for (Projection projection : colorizerProjections) {
                            this.path.op(projection.getPath(), Path.Op.DIFFERENCE);
                        }
                        Unit unit = Unit.INSTANCE;
                        CloseableKt.closeFinally(colorizerProjections, null);
                        if (multiselectable.canPlayContent() && multiselectable.shouldProjectContent()) {
                            View rootView = childAt.getRootView();
                            if (rootView != null) {
                                Projection giphyMp4PlayableProjection = multiselectable.getGiphyMp4PlayableProjection((ViewGroup) rootView);
                                Intrinsics.checkNotNullExpressionValue(giphyMp4PlayableProjection, "child.getGiphyMp4Playabl…ld.rootView as ViewGroup)");
                                this.path.op(giphyMp4PlayableProjection.getPath(), Path.Op.DIFFERENCE);
                                giphyMp4PlayableProjection.release();
                            } else {
                                throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
                            }
                        }
                    } catch (Throwable th) {
                        try {
                            throw th;
                        } catch (Throwable th2) {
                            CloseableKt.closeFinally(colorizerProjections, th);
                            throw th2;
                        }
                    }
                }
            }
            canvas.clipPath(this.path);
            drawShade(canvas);
            canvas.restore();
        }
    }

    private final void drawFocusShadeOverIfNecessary(Canvas canvas, RecyclerView recyclerView) {
        MultiselectPart multiselectPart = this.focusedItem;
        if (multiselectPart != null) {
            this.path.reset();
            canvas.save();
            int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                Intrinsics.checkNotNullExpressionValue(childAt, "getChildAt(index)");
                if ((childAt instanceof Multiselectable) && Intrinsics.areEqual(((Multiselectable) childAt).getConversationMessage(), multiselectPart.getConversationMessage())) {
                    this.path.addRect((float) childAt.getLeft(), (float) childAt.getTop(), (float) childAt.getRight(), (float) childAt.getBottom(), Path.Direction.CW);
                }
            }
            canvas.clipPath(this.path, Region.Op.DIFFERENCE);
            drawShade(canvas);
            canvas.restore();
        }
    }

    private final void drawShade(Canvas canvas) {
        ValueAnimator valueAnimator = this.hideShadeAnimation;
        Float f = null;
        Object animatedValue = valueAnimator != null ? valueAnimator.getAnimatedValue() : null;
        if (animatedValue instanceof Float) {
            f = (Float) animatedValue;
        }
        if (f == null) {
            canvas.drawColor(this.lightShadeColor);
            canvas.drawColor(this.darkShadeColor);
            return;
        }
        Object evaluate = this.argbEvaluator.evaluate(f.floatValue(), Integer.valueOf(this.lightShadeColor), 0);
        if (evaluate != null) {
            canvas.drawColor(((Integer) evaluate).intValue());
            Object evaluate2 = this.argbEvaluator.evaluate(f.floatValue(), Integer.valueOf(this.darkShadeColor), 0);
            if (evaluate2 != null) {
                canvas.drawColor(((Integer) evaluate2).intValue());
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type kotlin.Int");
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Int");
    }

    public final void hideShade(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "list");
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        ofFloat.setDuration(150L);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(recyclerView) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.MultiselectItemDecoration$$ExternalSyntheticLambda0
            public final /* synthetic */ RecyclerView f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                MultiselectItemDecoration.m1563hideShade$lambda19$lambda17(MultiselectItemDecoration.this, this.f$1, valueAnimator);
            }
        });
        Intrinsics.checkNotNullExpressionValue(ofFloat, "");
        ofFloat.addListener(new Animator.AnimatorListener(this) { // from class: org.thoughtcrime.securesms.conversation.mutiselect.MultiselectItemDecoration$hideShade$lambda-19$$inlined$doOnEnd$1
            final /* synthetic */ MultiselectItemDecoration this$0;

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationRepeat(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationStart(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
            }

            {
                this.this$0 = r1;
            }

            @Override // android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                Intrinsics.checkNotNullParameter(animator, "animator");
                this.this$0.hideShadeAnimation = null;
            }
        });
        ofFloat.start();
        this.hideShadeAnimation = ofFloat;
    }

    /* renamed from: hideShade$lambda-19$lambda-17 */
    public static final void m1563hideShade$lambda19$lambda17(MultiselectItemDecoration multiselectItemDecoration, RecyclerView recyclerView, ValueAnimator valueAnimator) {
        Intrinsics.checkNotNullParameter(multiselectItemDecoration, "this$0");
        Intrinsics.checkNotNullParameter(recyclerView, "$list");
        multiselectItemDecoration.invalidateIfAnimatorsAreRunning(recyclerView);
    }

    private final boolean isInitialAnimation() {
        ValueAnimator valueAnimator = this.enterExitAnimation;
        return (valueAnimator != null ? valueAnimator.getAnimatedFraction() : 0.0f) < 1.0f;
    }

    private final void updateMultiselectPartAnimator(Set<? extends MultiselectPart> set, MultiselectPart multiselectPart) {
        Difference differenceForPart = getDifferenceForPart(set, multiselectPart);
        ValueAnimator valueAnimator = this.multiselectPartAnimatorMap.get(multiselectPart);
        int i = WhenMappings.$EnumSwitchMapping$0[differenceForPart.ordinal()];
        float f = 1.0f;
        float f2 = 0.0f;
        if (i == 2) {
            float[] fArr = new float[2];
            if (valueAnimator != null) {
                f2 = valueAnimator.getAnimatedFraction();
            }
            fArr[0] = f2;
            fArr[1] = 1.0f;
            ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
            ofFloat.setDuration(150L);
            ofFloat.start();
            if (valueAnimator != null) {
                valueAnimator.end();
            }
            Map<MultiselectPart, ValueAnimator> map = this.multiselectPartAnimatorMap;
            Intrinsics.checkNotNullExpressionValue(ofFloat, "newAnimator");
            map.put(multiselectPart, ofFloat);
        } else if (i == 3) {
            float[] fArr2 = new float[2];
            if (valueAnimator != null) {
                f = valueAnimator.getAnimatedFraction();
            }
            fArr2[0] = f;
            fArr2[1] = 0.0f;
            ValueAnimator ofFloat2 = ValueAnimator.ofFloat(fArr2);
            ofFloat2.setDuration(150L);
            ofFloat2.start();
            if (valueAnimator != null) {
                valueAnimator.end();
            }
            Map<MultiselectPart, ValueAnimator> map2 = this.multiselectPartAnimatorMap;
            Intrinsics.checkNotNullExpressionValue(ofFloat2, "newAnimator");
            map2.put(multiselectPart, ofFloat2);
        }
    }

    private final float selectedAnimationProgress(MultiselectPart multiselectPart) {
        ValueAnimator valueAnimator = this.multiselectPartAnimatorMap.get(multiselectPart);
        if (valueAnimator != null) {
            return valueAnimator.getAnimatedFraction();
        }
        return 1.0f;
    }

    private final Difference getDifferenceForPart(Set<? extends MultiselectPart> set, MultiselectPart multiselectPart) {
        boolean contains = set.contains(multiselectPart);
        boolean contains2 = this.selectedParts.contains(multiselectPart);
        if (contains && !contains2) {
            return Difference.ADDED;
        }
        if (contains || !contains2) {
            return Difference.SAME;
        }
        return Difference.REMOVED;
    }

    private final void invalidateIfAnimatorsAreRunning(RecyclerView recyclerView) {
        boolean z;
        ValueAnimator valueAnimator = this.enterExitAnimation;
        boolean z2 = true;
        if (!(valueAnimator != null && valueAnimator.isRunning())) {
            Collection<ValueAnimator> values = this.multiselectPartAnimatorMap.values();
            if (!(values instanceof Collection) || !values.isEmpty()) {
                for (ValueAnimator valueAnimator2 : values) {
                    if (valueAnimator2.isRunning()) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (!z) {
                ValueAnimator valueAnimator3 = this.hideShadeAnimation;
                if (valueAnimator3 == null || !valueAnimator3.isRunning()) {
                    z2 = false;
                }
                if (!z2) {
                    return;
                }
            }
        }
        recyclerView.invalidate();
    }
}
