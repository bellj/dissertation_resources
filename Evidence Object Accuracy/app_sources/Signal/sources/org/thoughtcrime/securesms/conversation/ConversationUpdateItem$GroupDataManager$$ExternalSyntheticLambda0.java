package org.thoughtcrime.securesms.conversation;

import j$.util.function.Function;
import org.thoughtcrime.securesms.conversation.ConversationUpdateItem;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationUpdateItem$GroupDataManager$$ExternalSyntheticLambda0 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ConversationUpdateItem.GroupDataManager.lambda$observe$1((GroupMemberEntry.FullMember) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
