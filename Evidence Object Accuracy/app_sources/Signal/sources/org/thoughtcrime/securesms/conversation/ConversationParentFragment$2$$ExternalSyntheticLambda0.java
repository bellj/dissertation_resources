package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import org.thoughtcrime.securesms.conversation.ConversationParentFragment;
import org.thoughtcrime.securesms.mms.SlideDeck;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$2$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SlideDeck f$0;
    public final /* synthetic */ Context f$1;

    public /* synthetic */ ConversationParentFragment$2$$ExternalSyntheticLambda0(SlideDeck slideDeck, Context context) {
        this.f$0 = slideDeck;
        this.f$1 = context;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ConversationParentFragment.AnonymousClass2.lambda$onSuccess$1(this.f$0, this.f$1);
    }
}
