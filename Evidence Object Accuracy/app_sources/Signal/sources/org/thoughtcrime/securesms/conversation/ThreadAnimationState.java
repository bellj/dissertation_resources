package org.thoughtcrime.securesms.conversation;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: ThreadAnimationState.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J)\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\u0006\u0010\u0017\u001a\u00020\u0007J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ThreadAnimationState;", "", "threadId", "", "threadMetadata", "Lorg/thoughtcrime/securesms/conversation/ConversationData;", "hasCommittedNonEmptyMessageList", "", "(JLorg/thoughtcrime/securesms/conversation/ConversationData;Z)V", "getHasCommittedNonEmptyMessageList", "()Z", "getThreadId", "()J", "getThreadMetadata", "()Lorg/thoughtcrime/securesms/conversation/ConversationData;", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "shouldPlayMessageAnimations", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ThreadAnimationState {
    private final boolean hasCommittedNonEmptyMessageList;
    private final long threadId;
    private final ConversationData threadMetadata;

    public static /* synthetic */ ThreadAnimationState copy$default(ThreadAnimationState threadAnimationState, long j, ConversationData conversationData, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            j = threadAnimationState.threadId;
        }
        if ((i & 2) != 0) {
            conversationData = threadAnimationState.threadMetadata;
        }
        if ((i & 4) != 0) {
            z = threadAnimationState.hasCommittedNonEmptyMessageList;
        }
        return threadAnimationState.copy(j, conversationData, z);
    }

    public final long component1() {
        return this.threadId;
    }

    public final ConversationData component2() {
        return this.threadMetadata;
    }

    public final boolean component3() {
        return this.hasCommittedNonEmptyMessageList;
    }

    public final ThreadAnimationState copy(long j, ConversationData conversationData, boolean z) {
        return new ThreadAnimationState(j, conversationData, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ThreadAnimationState)) {
            return false;
        }
        ThreadAnimationState threadAnimationState = (ThreadAnimationState) obj;
        return this.threadId == threadAnimationState.threadId && Intrinsics.areEqual(this.threadMetadata, threadAnimationState.threadMetadata) && this.hasCommittedNonEmptyMessageList == threadAnimationState.hasCommittedNonEmptyMessageList;
    }

    public int hashCode() {
        int m = SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId) * 31;
        ConversationData conversationData = this.threadMetadata;
        int hashCode = (m + (conversationData == null ? 0 : conversationData.hashCode())) * 31;
        boolean z = this.hasCommittedNonEmptyMessageList;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "ThreadAnimationState(threadId=" + this.threadId + ", threadMetadata=" + this.threadMetadata + ", hasCommittedNonEmptyMessageList=" + this.hasCommittedNonEmptyMessageList + ')';
    }

    public ThreadAnimationState(long j, ConversationData conversationData, boolean z) {
        this.threadId = j;
        this.threadMetadata = conversationData;
        this.hasCommittedNonEmptyMessageList = z;
    }

    public final long getThreadId() {
        return this.threadId;
    }

    public final ConversationData getThreadMetadata() {
        return this.threadMetadata;
    }

    public final boolean getHasCommittedNonEmptyMessageList() {
        return this.hasCommittedNonEmptyMessageList;
    }

    public final boolean shouldPlayMessageAnimations() {
        ConversationData conversationData;
        if (!(this.threadId == -1 || (conversationData = this.threadMetadata) == null)) {
            if (conversationData.getThreadSize() == 0) {
                return true;
            }
            if (this.threadMetadata.getThreadSize() > 0 && this.hasCommittedNonEmptyMessageList) {
                return true;
            }
        }
        return false;
    }
}
