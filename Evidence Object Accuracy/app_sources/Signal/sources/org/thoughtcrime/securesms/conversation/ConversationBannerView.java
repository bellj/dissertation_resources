package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.LongClickMovementMethod;
import org.thoughtcrime.securesms.util.SpanUtil;

/* loaded from: classes4.dex */
public class ConversationBannerView extends ConstraintLayout {
    private TextView contactAbout;
    private AvatarImageView contactAvatar;
    private BadgeImageView contactBadge;
    private EmojiTextView contactDescription;
    private TextView contactSubtitle;
    private TextView contactTitle;
    private View tapToView;

    public ConversationBannerView(Context context) {
        this(context, null);
    }

    public ConversationBannerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ConversationBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ViewGroup.inflate(getContext(), R.layout.conversation_banner_view, this);
        this.contactAvatar = (AvatarImageView) findViewById(R.id.message_request_avatar);
        this.contactBadge = (BadgeImageView) findViewById(R.id.message_request_badge);
        this.contactTitle = (TextView) findViewById(R.id.message_request_title);
        this.contactAbout = (TextView) findViewById(R.id.message_request_about);
        this.contactSubtitle = (TextView) findViewById(R.id.message_request_subtitle);
        this.contactDescription = (EmojiTextView) findViewById(R.id.message_request_description);
        this.tapToView = findViewById(R.id.message_request_avatar_tap_to_view);
        this.contactAvatar.setFallbackPhotoProvider(new FallbackPhotoProvider());
    }

    public void setBadge(Recipient recipient) {
        if (recipient == null || recipient.isSelf()) {
            this.contactBadge.setBadge(null);
        } else {
            this.contactBadge.setBadgeFromRecipient(recipient);
        }
    }

    public void setAvatar(GlideRequests glideRequests, Recipient recipient) {
        this.contactAvatar.setAvatar(glideRequests, recipient, false);
        if (recipient == null || !recipient.shouldBlurAvatar() || recipient.getContactPhoto() == null) {
            this.tapToView.setVisibility(8);
            this.tapToView.setOnClickListener(null);
            return;
        }
        this.tapToView.setVisibility(0);
        this.tapToView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationBannerView$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationBannerView.m1368$r8$lambda$5WZbk2j49LimZwhpD22IeMP1XQ(Recipient.this, view);
            }
        });
    }

    public static /* synthetic */ void lambda$setAvatar$0(Recipient recipient) {
        SignalDatabase.recipients().manuallyShowAvatar(recipient.getId());
    }

    public static /* synthetic */ void lambda$setAvatar$1(Recipient recipient, View view) {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationBannerView$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ConversationBannerView.$r8$lambda$qqrNTLsQwz1tHuSRnYy6dqv7hfY(Recipient.this);
            }
        });
    }

    public String setTitle(Recipient recipient) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(recipient.isSelf() ? getContext().getString(R.string.note_to_self) : recipient.getDisplayNameOrUsername(getContext()));
        if (recipient.showVerified()) {
            SpanUtil.appendCenteredImageSpan(spannableStringBuilder, ContextUtil.requireDrawable(getContext(), R.drawable.ic_official_28), 28, 28);
        }
        this.contactTitle.setText(spannableStringBuilder);
        return spannableStringBuilder.toString();
    }

    public void setAbout(Recipient recipient) {
        String str;
        if (recipient.isReleaseNotes()) {
            str = getContext().getString(R.string.ReleaseNotes__signal_release_notes_and_news);
        } else {
            str = recipient.getCombinedAboutAndEmoji();
        }
        this.contactAbout.setText(str);
        this.contactAbout.setVisibility(TextUtils.isEmpty(str) ? 8 : 0);
    }

    public void setSubtitle(CharSequence charSequence) {
        this.contactSubtitle.setText(charSequence);
        this.contactSubtitle.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
    }

    public void setDescription(CharSequence charSequence) {
        this.contactDescription.setText(charSequence);
        this.contactDescription.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
    }

    public EmojiTextView getDescription() {
        return this.contactDescription;
    }

    public void showBackgroundBubble(boolean z) {
        if (z) {
            setBackgroundResource(R.drawable.wallpaper_bubble_background_12);
        } else {
            setBackground(null);
        }
    }

    public void hideSubtitle() {
        this.contactSubtitle.setVisibility(8);
    }

    public void showDescription() {
        this.contactDescription.setVisibility(0);
    }

    public void hideDescription() {
        this.contactDescription.setVisibility(8);
    }

    public void setLinkifyDescription(boolean z) {
        this.contactDescription.setMovementMethod(z ? LongClickMovementMethod.getInstance(getContext()) : null);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class FallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        private FallbackPhotoProvider() {
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithoutName() {
            return new ResourceContactPhoto(R.drawable.ic_profile_64);
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForGroup() {
            return new ResourceContactPhoto(R.drawable.ic_group_64);
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForLocalNumber() {
            return new ResourceContactPhoto(R.drawable.ic_note_64);
        }
    }
}
