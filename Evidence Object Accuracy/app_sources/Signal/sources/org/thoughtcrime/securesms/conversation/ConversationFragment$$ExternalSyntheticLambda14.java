package org.thoughtcrime.securesms.conversation;

import j$.util.function.Function;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$$ExternalSyntheticLambda14 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((MultiselectPart) obj).getConversationMessage();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
