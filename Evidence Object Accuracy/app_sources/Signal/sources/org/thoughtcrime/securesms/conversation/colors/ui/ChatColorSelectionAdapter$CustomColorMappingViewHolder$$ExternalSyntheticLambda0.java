package org.thoughtcrime.securesms.conversation.colors.ui;

import android.view.View;
import kotlin.jvm.functions.Function0;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ChatColorSelectionAdapter$CustomColorMappingViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Function0 f$0;

    public /* synthetic */ ChatColorSelectionAdapter$CustomColorMappingViewHolder$$ExternalSyntheticLambda0(Function0 function0) {
        this.f$0 = function0;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ChatColorSelectionAdapter.CustomColorMappingViewHolder.m1496_init_$lambda0(this.f$0, view);
    }
}
