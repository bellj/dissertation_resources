package org.thoughtcrime.securesms.conversation;

import android.os.Build;
import android.os.PowerManager;
import androidx.activity.ComponentActivity;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.WakeLockUtil;

/* compiled from: VoiceRecorderWakeLock.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\b\u001a\u00020\tJ\u0010\u0010\n\u001a\u00020\t2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0006\u0010\r\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0018\u00010\u0006R\u00020\u0007X\u000e¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/VoiceRecorderWakeLock;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "activity", "Landroidx/activity/ComponentActivity;", "(Landroidx/activity/ComponentActivity;)V", "wakeLock", "Landroid/os/PowerManager$WakeLock;", "Landroid/os/PowerManager;", "acquire", "", "onPause", "owner", "Landroidx/lifecycle/LifecycleOwner;", "release", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceRecorderWakeLock implements DefaultLifecycleObserver {
    private final ComponentActivity activity;
    private PowerManager.WakeLock wakeLock;

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public VoiceRecorderWakeLock(ComponentActivity componentActivity) {
        Intrinsics.checkNotNullParameter(componentActivity, "activity");
        this.activity = componentActivity;
        componentActivity.getLifecycle().addObserver(this);
    }

    public final void acquire() {
        synchronized (this) {
            PowerManager.WakeLock wakeLock = this.wakeLock;
            boolean z = true;
            if (wakeLock == null || !wakeLock.isHeld()) {
                z = false;
            }
            if (!z) {
                if (Build.VERSION.SDK_INT >= 21) {
                    this.wakeLock = WakeLockUtil.acquire(this.activity, 32, TimeUnit.HOURS.toMillis(1), "voiceRecorder");
                }
                Unit unit = Unit.INSTANCE;
            }
        }
    }

    public final void release() {
        PowerManager.WakeLock wakeLock;
        synchronized (this) {
            PowerManager.WakeLock wakeLock2 = this.wakeLock;
            boolean z = true;
            if (wakeLock2 == null || !wakeLock2.isHeld()) {
                z = false;
            }
            if (z && (wakeLock = this.wakeLock) != null) {
                wakeLock.release();
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        release();
    }
}
