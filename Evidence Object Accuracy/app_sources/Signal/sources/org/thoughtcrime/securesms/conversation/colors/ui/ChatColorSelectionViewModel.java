package org.thoughtcrime.securesms.conversation.colors.ui;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: ChatColorSelectionViewModel.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001a2\u00020\u0001:\u0003\u001a\u001b\u001cB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0015J\u000e\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0015J\u0006\u0010\u0017\u001a\u00020\u0014J\u000e\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0015J\u000e\u0010\u0019\u001a\u00020\u00142\u0006\u0010\u0005\u001a\u00020\u0015R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000bR\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0012X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;", "(Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;)V", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorsOptionsLiveData;", "events", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Event;", "getEvents", "()Landroidx/lifecycle/LiveData;", "internalEvents", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "state", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionState;", "getState", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "deleteNow", "", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "duplicate", "refresh", "save", "startDeletion", "Companion", "Event", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorSelectionViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private final ChatColorsOptionsLiveData chatColors;
    private final LiveData<Event> events;
    private final SingleLiveEvent<Event> internalEvents;
    private final ChatColorSelectionRepository repository;
    private final LiveData<ChatColorSelectionState> state;
    private final Store<ChatColorSelectionState> store;

    public ChatColorSelectionViewModel(ChatColorSelectionRepository chatColorSelectionRepository) {
        Intrinsics.checkNotNullParameter(chatColorSelectionRepository, "repository");
        this.repository = chatColorSelectionRepository;
        Store<ChatColorSelectionState> store = new Store<>(new ChatColorSelectionState(null, null, null, 7, null));
        this.store = store;
        ChatColorsOptionsLiveData chatColorsOptionsLiveData = new ChatColorsOptionsLiveData();
        this.chatColors = chatColorsOptionsLiveData;
        SingleLiveEvent<Event> singleLiveEvent = new SingleLiveEvent<>();
        this.internalEvents = singleLiveEvent;
        LiveData<ChatColorSelectionState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        this.events = singleLiveEvent;
        store.update(chatColorsOptionsLiveData, new Store.Action() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ChatColorSelectionViewModel.m1516_init_$lambda0((List) obj, (ChatColorSelectionState) obj2);
            }
        });
    }

    public final LiveData<ChatColorSelectionState> getState() {
        return this.state;
    }

    public final LiveData<Event> getEvents() {
        return this.events;
    }

    /* renamed from: _init_$lambda-0 */
    public static final ChatColorSelectionState m1516_init_$lambda0(List list, ChatColorSelectionState chatColorSelectionState) {
        Intrinsics.checkNotNullExpressionValue(chatColorSelectionState, "state");
        Intrinsics.checkNotNullExpressionValue(list, "colors");
        return ChatColorSelectionState.copy$default(chatColorSelectionState, null, null, list, 3, null);
    }

    public final void refresh() {
        this.repository.getWallpaper(new ChatColorSelectionViewModel$refresh$1(this));
        this.repository.getChatColors(new ChatColorSelectionViewModel$refresh$2(this));
    }

    public final void save(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.repository.save(chatColors, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel$save$1
            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                ((ChatColorSelectionViewModel) this.receiver).refresh();
            }
        });
    }

    public final void duplicate(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.repository.duplicate(chatColors);
    }

    public final void startDeletion(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.repository.getUsageCount(chatColors.getId(), new Function1<Integer, Unit>(this, chatColors) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel$startDeletion$1
            final /* synthetic */ ChatColors $chatColors;
            final /* synthetic */ ChatColorSelectionViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$chatColors = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                invoke(num.intValue());
                return Unit.INSTANCE;
            }

            public final void invoke(int i) {
                this.this$0.internalEvents.postValue(new ChatColorSelectionViewModel.Event.ConfirmDeletion(i, this.$chatColors));
            }
        });
    }

    public final void deleteNow(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.repository.delete(chatColors, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel$deleteNow$1
            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                ((ChatColorSelectionViewModel) this.receiver).refresh();
            }
        });
    }

    /* compiled from: ChatColorSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;", "(Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final ChatColorSelectionRepository repository;

        public Factory(ChatColorSelectionRepository chatColorSelectionRepository) {
            Intrinsics.checkNotNullParameter(chatColorSelectionRepository, "repository");
            this.repository = chatColorSelectionRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ChatColorSelectionViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* compiled from: ChatColorSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Companion;", "", "()V", "getOrCreate", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel;", "activity", "Landroidx/fragment/app/FragmentActivity;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final ChatColorSelectionViewModel getOrCreate(FragmentActivity fragmentActivity, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(fragmentActivity, "activity");
            ViewModel viewModel = new ViewModelProvider(fragmentActivity, new Factory(ChatColorSelectionRepository.Companion.create(fragmentActivity, recipientId))).get(ChatColorSelectionViewModel.class);
            Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(activi…ionViewModel::class.java)");
            return (ChatColorSelectionViewModel) viewModel;
        }
    }

    /* compiled from: ChatColorSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0003B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0001\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Event;", "", "()V", "ConfirmDeletion", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Event$ConfirmDeletion;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Event {
        public /* synthetic */ Event(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: ChatColorSelectionViewModel.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Event$ConfirmDeletion;", "Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorSelectionViewModel$Event;", "usageCount", "", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "(ILorg/thoughtcrime/securesms/conversation/colors/ChatColors;)V", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "getUsageCount", "()I", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class ConfirmDeletion extends Event {
            private final ChatColors chatColors;
            private final int usageCount;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public ConfirmDeletion(int i, ChatColors chatColors) {
                super(null);
                Intrinsics.checkNotNullParameter(chatColors, "chatColors");
                this.usageCount = i;
                this.chatColors = chatColors;
            }

            public final ChatColors getChatColors() {
                return this.chatColors;
            }

            public final int getUsageCount() {
                return this.usageCount;
            }
        }

        private Event() {
        }
    }
}
