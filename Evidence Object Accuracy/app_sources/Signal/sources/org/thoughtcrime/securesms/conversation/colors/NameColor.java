package org.thoughtcrime.securesms.conversation.colors;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* compiled from: NameColor.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/NameColor;", "", "lightColor", "", "darkColor", "(II)V", "getColor", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NameColor {
    private final int darkColor;
    private final int lightColor;

    public NameColor(int i, int i2) {
        this.lightColor = i;
        this.darkColor = i2;
    }

    public final int getColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (ThemeUtil.isDarkTheme(context)) {
            return this.darkColor;
        }
        return this.lightColor;
    }
}
