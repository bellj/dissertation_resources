package org.thoughtcrime.securesms.conversation.colors.ui;

import android.view.View;
import org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ChatColorSelectionAdapter$ChatColorMappingViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ ChatColorSelectionAdapter.ChatColorMappingViewHolder f$0;
    public final /* synthetic */ ChatColorMappingModel f$1;

    public /* synthetic */ ChatColorSelectionAdapter$ChatColorMappingViewHolder$$ExternalSyntheticLambda1(ChatColorSelectionAdapter.ChatColorMappingViewHolder chatColorMappingViewHolder, ChatColorMappingModel chatColorMappingModel) {
        this.f$0 = chatColorMappingViewHolder;
        this.f$1 = chatColorMappingModel;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return ChatColorSelectionAdapter.ChatColorMappingViewHolder.m1494bind$lambda1(this.f$0, this.f$1, view);
    }
}
