package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import kotlin.Metadata;

/* compiled from: CustomChatColorCreatorState.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/ColorSlidersState;", "", "huePosition", "", "saturationPosition", "(II)V", "getHuePosition", "()I", "getSaturationPosition", "component1", "component2", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ColorSlidersState {
    private final int huePosition;
    private final int saturationPosition;

    public static /* synthetic */ ColorSlidersState copy$default(ColorSlidersState colorSlidersState, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = colorSlidersState.huePosition;
        }
        if ((i3 & 2) != 0) {
            i2 = colorSlidersState.saturationPosition;
        }
        return colorSlidersState.copy(i, i2);
    }

    public final int component1() {
        return this.huePosition;
    }

    public final int component2() {
        return this.saturationPosition;
    }

    public final ColorSlidersState copy(int i, int i2) {
        return new ColorSlidersState(i, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ColorSlidersState)) {
            return false;
        }
        ColorSlidersState colorSlidersState = (ColorSlidersState) obj;
        return this.huePosition == colorSlidersState.huePosition && this.saturationPosition == colorSlidersState.saturationPosition;
    }

    public int hashCode() {
        return (this.huePosition * 31) + this.saturationPosition;
    }

    public String toString() {
        return "ColorSlidersState(huePosition=" + this.huePosition + ", saturationPosition=" + this.saturationPosition + ')';
    }

    public ColorSlidersState(int i, int i2) {
        this.huePosition = i;
        this.saturationPosition = i2;
    }

    public final int getHuePosition() {
        return this.huePosition;
    }

    public final int getSaturationPosition() {
        return this.saturationPosition;
    }
}
