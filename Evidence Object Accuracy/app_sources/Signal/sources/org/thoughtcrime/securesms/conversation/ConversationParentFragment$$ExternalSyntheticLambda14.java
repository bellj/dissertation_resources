package org.thoughtcrime.securesms.conversation;

import org.signal.core.util.concurrent.SimpleTask;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationParentFragment$$ExternalSyntheticLambda14 implements SimpleTask.ForegroundTask {
    public final /* synthetic */ ConversationParentFragment f$0;

    public /* synthetic */ ConversationParentFragment$$ExternalSyntheticLambda14(ConversationParentFragment conversationParentFragment) {
        this.f$0 = conversationParentFragment;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
    public final void run(Object obj) {
        this.f$0.sendComplete(((Long) obj).longValue());
    }
}
