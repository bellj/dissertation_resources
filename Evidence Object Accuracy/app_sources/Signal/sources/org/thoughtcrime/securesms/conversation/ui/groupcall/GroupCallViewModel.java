package org.thoughtcrime.securesms.conversation.ui.groupcall;

import android.os.Build;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.GroupCallPeekEvent;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class GroupCallViewModel extends ViewModel {
    private static final String TAG = Log.tag(GroupCallViewModel.class);
    private final MutableLiveData<Boolean> activeGroup;
    private final LiveData<Boolean> activeGroupCall;
    private Recipient currentRecipient;
    private final MutableLiveData<Boolean> groupCallHasCapacity;
    private final MutableLiveData<Boolean> ongoingGroupCall;

    GroupCallViewModel() {
        Boolean bool = Boolean.FALSE;
        MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>(bool);
        this.activeGroup = mutableLiveData;
        MutableLiveData<Boolean> mutableLiveData2 = new MutableLiveData<>(bool);
        this.ongoingGroupCall = mutableLiveData2;
        this.groupCallHasCapacity = new MutableLiveData<>(bool);
        this.activeGroupCall = LiveDataUtil.combineLatest(mutableLiveData, mutableLiveData2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.conversation.ui.groupcall.GroupCallViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return GroupCallViewModel.lambda$new$0((Boolean) obj, (Boolean) obj2);
            }
        });
    }

    public static /* synthetic */ Boolean lambda$new$0(Boolean bool, Boolean bool2) {
        return Boolean.valueOf(bool.booleanValue() && bool2.booleanValue());
    }

    public LiveData<Boolean> hasActiveGroupCall() {
        return this.activeGroupCall;
    }

    public LiveData<Boolean> groupCallHasCapacity() {
        return this.groupCallHasCapacity;
    }

    public void onRecipientChange(Recipient recipient) {
        this.activeGroup.postValue(Boolean.valueOf(recipient != null && recipient.isActiveGroup()));
        if (!Objects.equals(this.currentRecipient, recipient)) {
            MutableLiveData<Boolean> mutableLiveData = this.ongoingGroupCall;
            Boolean bool = Boolean.FALSE;
            mutableLiveData.postValue(bool);
            this.groupCallHasCapacity.postValue(bool);
            this.currentRecipient = recipient;
            peekGroupCall();
        }
    }

    public void peekGroupCall() {
        if (isGroupCallCapable(this.currentRecipient)) {
            String str = TAG;
            Log.i(str, "peek call for " + this.currentRecipient.getId());
            ApplicationDependencies.getSignalCallManager().peekGroupCall(this.currentRecipient.getId());
        }
    }

    public void onGroupCallPeekEvent(GroupCallPeekEvent groupCallPeekEvent) {
        if (!isGroupCallCapable(this.currentRecipient) || !groupCallPeekEvent.getGroupRecipientId().equals(this.currentRecipient.getId())) {
            Log.i(TAG, "Ignore call event for different recipient.");
            return;
        }
        String str = TAG;
        Log.i(str, "update UI with call event: ongoing call: " + groupCallPeekEvent.isOngoing() + " hasCapacity: " + groupCallPeekEvent.callHasCapacity());
        this.ongoingGroupCall.postValue(Boolean.valueOf(groupCallPeekEvent.isOngoing()));
        this.groupCallHasCapacity.postValue(Boolean.valueOf(groupCallPeekEvent.callHasCapacity()));
    }

    private static boolean isGroupCallCapable(Recipient recipient) {
        return recipient != null && recipient.isActiveGroup() && recipient.isPushV2Group() && Build.VERSION.SDK_INT > 19;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new GroupCallViewModel());
        }
    }
}
