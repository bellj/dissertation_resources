package org.thoughtcrime.securesms.conversation;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.LiveDataReactiveStreams;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.BackpressureStrategy;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.processors.PublishProcessor;
import io.reactivex.rxjava3.schedulers.Schedulers;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Function;
import j$.util.function.Predicate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.signal.paging.ObservablePagedData;
import org.signal.paging.PagedData;
import org.signal.paging.PagingConfig;
import org.signal.paging.PagingController;
import org.signal.paging.ProxyPagingController;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.conversation.ConversationData;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.ConversationViewModel;
import org.thoughtcrime.securesms.conversation.ThreadCountAggregator;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.GroupAuthorNameColorHelper;
import org.thoughtcrime.securesms.conversation.colors.NameColor;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.database.model.ThreadRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mediasend.MediaRepository;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.ratelimit.RecaptchaRequiredEvent;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.thoughtcrime.securesms.util.rx.RxStore;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes.dex */
public class ConversationViewModel extends ViewModel {
    private static final String TAG = Log.tag(ConversationViewModel.class);
    private ConversationIntents.Args args;
    private final Observable<Boolean> canShowAsBubble;
    private final Observable<ChatColors> chatColors;
    private final Application context;
    private final DatabaseObserver.Observer conversationObserver;
    private final ConversationRepository conversationRepository;
    private final RxStore<ConversationState> conversationStateStore;
    private final BehaviorSubject<Unit> conversationStateTick;
    private final LiveData<Integer> conversationTopMargin;
    private final CompositeDisposable disposables;
    private final SingleLiveEvent<Event> events;
    private final GroupAuthorNameColorHelper groupAuthorNameColorHelper;
    private final MutableLiveData<Boolean> hasUnreadMentions;
    private final MutableLiveData<Integer> inlinePlayerHeight;
    private int jumpToPosition;
    private final PublishProcessor<Long> markReadRequestPublisher;
    private final MediaRepository mediaRepository;
    private final Observable<MessageData> messageData;
    private final DatabaseObserver.MessageObserver messageInsertObserver;
    private final DatabaseObserver.MessageObserver messageUpdateObserver;
    private final NotificationProfilesRepository notificationProfilesRepository;
    private final ProxyPagingController<MessageId> pagingController;
    private final MutableLiveData<List<Media>> recentMedia;
    private final BehaviorSubject<RecipientId> recipientId;
    private final MutableLiveData<String> searchQuery;
    private final MutableLiveData<Boolean> showScrollButtons;
    private final Store<ThreadAnimationState> threadAnimationStateStore;
    private final Observer<ThreadAnimationState> threadAnimationStateStoreDriver;
    private final RxStore<ThreadCountAggregator> threadCountStore;
    private final BehaviorSubject<Long> threadId;
    private final MutableLiveData<Integer> toolbarBottom;
    private final Observable<Optional<ChatWallpaper>> wallpaper;

    /* loaded from: classes4.dex */
    public enum Event {
        SHOW_RECAPTCHA
    }

    public static /* synthetic */ List lambda$getActiveNotificationProfile$22(Long l, List list) throws Throwable {
        return list;
    }

    public static /* synthetic */ Long lambda$getMarkReadRequests$18(Long l, ThreadCountAggregator threadCountAggregator) throws Throwable {
        return l;
    }

    public static /* synthetic */ void lambda$new$12(ThreadAnimationState threadAnimationState) {
    }

    public static /* synthetic */ RecipientId lambda$new$2(RecipientId recipientId, Unit unit) throws Throwable {
        return recipientId;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r10v7, resolved type: io.reactivex.rxjava3.core.Flowable<U> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX DEBUG: Type inference failed for r0v3. Raw type applied. Possible types: io.reactivex.rxjava3.core.Observable<R>, io.reactivex.rxjava3.core.Observable<java.lang.Boolean> */
    private ConversationViewModel() {
        this.context = ApplicationDependencies.getApplication();
        this.mediaRepository = new MediaRepository();
        ConversationRepository conversationRepository = new ConversationRepository();
        this.conversationRepository = conversationRepository;
        this.recentMedia = new MutableLiveData<>();
        Boolean bool = Boolean.FALSE;
        this.showScrollButtons = new MutableLiveData<>(bool);
        this.hasUnreadMentions = new MutableLiveData<>(bool);
        this.events = new SingleLiveEvent<>();
        ProxyPagingController<MessageId> proxyPagingController = new ProxyPagingController<>();
        this.pagingController = proxyPagingController;
        Objects.requireNonNull(proxyPagingController);
        this.conversationObserver = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda14
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                ProxyPagingController.this.onDataInvalidated();
            }
        };
        Objects.requireNonNull(proxyPagingController);
        ConversationViewModel$$ExternalSyntheticLambda25 conversationViewModel$$ExternalSyntheticLambda25 = new DatabaseObserver.MessageObserver() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda25
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                ProxyPagingController.this.onDataItemChanged(messageId);
            }
        };
        this.messageUpdateObserver = conversationViewModel$$ExternalSyntheticLambda25;
        this.messageInsertObserver = new DatabaseObserver.MessageObserver() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda28
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.MessageObserver
            public final void onMessageChanged(MessageId messageId) {
                ConversationViewModel.this.lambda$new$0(messageId);
            }
        };
        MutableLiveData<Integer> mutableLiveData = new MutableLiveData<>();
        this.toolbarBottom = mutableLiveData;
        MutableLiveData<Integer> mutableLiveData2 = new MutableLiveData<>();
        this.inlinePlayerHeight = mutableLiveData2;
        this.conversationTopMargin = Transformations.distinctUntilChanged(LiveDataUtil.combineLatest(mutableLiveData, mutableLiveData2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda29
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return Integer.valueOf(((Integer) obj).intValue() + ((Integer) obj2).intValue());
            }
        }));
        Store<ThreadAnimationState> store = new Store<>(new ThreadAnimationState(-1, null, false));
        this.threadAnimationStateStore = store;
        this.notificationProfilesRepository = new NotificationProfilesRepository();
        this.searchQuery = new MutableLiveData<>();
        BehaviorSubject<RecipientId> create = BehaviorSubject.create();
        this.recipientId = create;
        BehaviorSubject<Long> create2 = BehaviorSubject.create();
        this.threadId = create2;
        this.groupAuthorNameColorHelper = new GroupAuthorNameColorHelper();
        RxStore<ConversationState> rxStore = new RxStore<>(ConversationState.create(), Schedulers.io());
        this.conversationStateStore = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        BehaviorSubject<Unit> createDefault = BehaviorSubject.createDefault(Unit.INSTANCE);
        this.conversationStateTick = createDefault;
        RxStore<ThreadCountAggregator> rxStore2 = new RxStore<>(ThreadCountAggregator.Init.INSTANCE, Schedulers.computation());
        this.threadCountStore = rxStore2;
        this.markReadRequestPublisher = PublishProcessor.create();
        BehaviorSubject create3 = BehaviorSubject.create();
        create.observeOn(Schedulers.io()).distinctUntilChanged().map(new ConversationViewModel$$ExternalSyntheticLambda6()).subscribe(create3);
        Objects.requireNonNull(conversationRepository);
        compositeDisposable.add(rxStore2.update(create2.switchMap(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda30
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationRepository.this.getThreadRecord(((Long) obj).longValue());
            }
        }).toFlowable(BackpressureStrategy.BUFFER), new Function2() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda31
            @Override // kotlin.jvm.functions.Function2
            public final Object invoke(Object obj, Object obj2) {
                return ConversationViewModel.lambda$new$1((Optional) obj, (ThreadCountAggregator) obj2);
            }
        }));
        Observable distinctUntilChanged = Observable.combineLatest(create, createDefault, new BiFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda32
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$new$2((RecipientId) obj, (Unit) obj2);
            }
        }).distinctUntilChanged();
        Objects.requireNonNull(conversationRepository);
        rxStore.update(distinctUntilChanged.switchMap(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda33
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationRepository.this.getSecurityInfo((RecipientId) obj);
            }
        }).toFlowable(BackpressureStrategy.LATEST), new Function2() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda34
            @Override // kotlin.jvm.functions.Function2
            public final Object invoke(Object obj, Object obj2) {
                return ((ConversationState) obj2).withSecurityInfo((ConversationSecurityInfo) obj);
            }
        });
        BehaviorSubject create4 = BehaviorSubject.create();
        Observable.combineLatest(create2, create3, new BiFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda15
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return new Pair((Long) obj, (Recipient) obj2);
            }
        }).observeOn(Schedulers.io()).distinctUntilChanged().map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda16
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.this.lambda$new$4((Pair) obj);
            }
        }).subscribe(create4);
        ApplicationDependencies.getDatabaseObserver().registerMessageUpdateObserver(conversationViewModel$$ExternalSyntheticLambda25);
        this.messageData = create4.observeOn(Schedulers.io()).switchMap(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda17
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.this.lambda$new$5((ConversationData) obj);
            }
        }).observeOn(Schedulers.io()).withLatestFrom(create4, new BiFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda18
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$new$6((List) obj, (ConversationData) obj2);
            }
        }).doOnNext(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda19
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationViewModel.MessageData messageData = (ConversationViewModel.MessageData) obj;
                SignalLocalMetrics.ConversationOpen.onDataLoaded();
            }
        });
        Observable<R> switchMap = create.distinctUntilChanged().switchMap(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda20
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.lambda$new$8((RecipientId) obj);
            }
        });
        Observable<Long> observeOn = create2.observeOn(Schedulers.io());
        Objects.requireNonNull(conversationRepository);
        this.canShowAsBubble = observeOn.map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda21
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(ConversationRepository.this.canShowAsBubble(((Long) obj).longValue()));
            }
        });
        this.wallpaper = switchMap.map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda22
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.lambda$new$9((Recipient) obj);
            }
        }).distinctUntilChanged();
        this.chatColors = switchMap.map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda23
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ((Recipient) obj).getChatColors();
            }
        }).distinctUntilChanged();
        store.update(create2, new Store.Action() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda24
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$new$10((Long) obj, (ThreadAnimationState) obj2);
            }
        });
        store.update(create4, new Store.Action() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda26
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$new$11((ConversationData) obj, (ThreadAnimationState) obj2);
            }
        });
        ConversationViewModel$$ExternalSyntheticLambda27 conversationViewModel$$ExternalSyntheticLambda27 = new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda27
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationViewModel.lambda$new$12((ThreadAnimationState) obj);
            }
        };
        this.threadAnimationStateStoreDriver = conversationViewModel$$ExternalSyntheticLambda27;
        store.getStateLiveData().observeForever(conversationViewModel$$ExternalSyntheticLambda27);
        EventBus.getDefault().register(this);
    }

    public /* synthetic */ void lambda$new$0(MessageId messageId) {
        this.pagingController.onDataItemInserted(messageId, 0);
    }

    public static /* synthetic */ ThreadCountAggregator lambda$new$1(Optional optional, ThreadCountAggregator threadCountAggregator) {
        Objects.requireNonNull(threadCountAggregator);
        return (ThreadCountAggregator) optional.map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda11
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ThreadCountAggregator.this.updateWith((ThreadRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(threadCountAggregator);
    }

    public /* synthetic */ ConversationData lambda$new$4(Pair pair) throws Throwable {
        SignalLocalMetrics.ConversationOpen.onMetadataLoadStarted();
        ConversationData conversationData = this.conversationRepository.getConversationData(((Long) pair.first()).longValue(), (Recipient) pair.second(), this.jumpToPosition);
        SignalLocalMetrics.ConversationOpen.onMetadataLoaded();
        this.jumpToPosition = -1;
        return conversationData;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v18, resolved type: org.signal.paging.ProxyPagingController<org.thoughtcrime.securesms.database.model.MessageId> */
    /* JADX WARN: Multi-variable type inference failed */
    public /* synthetic */ ObservableSource lambda$new$5(ConversationData conversationData) throws Throwable {
        int threadSize;
        ConversationData.MessageRequestData messageRequestData = conversationData.getMessageRequestData();
        if (conversationData.shouldJumpToMessage()) {
            threadSize = conversationData.getJumpToPosition();
        } else if (messageRequestData.isMessageRequestAccepted() && conversationData.shouldScrollToLastSeen()) {
            threadSize = conversationData.getLastSeenPosition();
        } else if (messageRequestData.isMessageRequestAccepted()) {
            threadSize = conversationData.getLastScrolledPosition();
        } else {
            threadSize = conversationData.getThreadSize();
        }
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.conversationObserver);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.messageInsertObserver);
        ApplicationDependencies.getDatabaseObserver().registerConversationObserver(conversationData.getThreadId(), this.conversationObserver);
        ApplicationDependencies.getDatabaseObserver().registerMessageInsertObserver(conversationData.getThreadId(), this.messageInsertObserver);
        ConversationDataSource conversationDataSource = new ConversationDataSource(this.context, conversationData.getThreadId(), messageRequestData, conversationData.showUniversalExpireTimerMessage(), conversationData.getThreadSize());
        PagingConfig build = new PagingConfig.Builder().setPageSize(25).setBufferPages(2).setStartIndex(Math.max(threadSize, 0)).build();
        String str = TAG;
        Log.d(str, "Starting at position: " + threadSize + " || jumpToPosition: " + conversationData.getJumpToPosition() + ", lastSeenPosition: " + conversationData.getLastSeenPosition() + ", lastScrolledPosition: " + conversationData.getLastScrolledPosition());
        ObservablePagedData createForObservable = PagedData.createForObservable(conversationDataSource, build);
        this.pagingController.set(createForObservable.getController());
        return createForObservable.getData();
    }

    public static /* synthetic */ MessageData lambda$new$6(List list, ConversationData conversationData) throws Throwable {
        return new MessageData(conversationData, list);
    }

    public static /* synthetic */ ObservableSource lambda$new$8(RecipientId recipientId) throws Throwable {
        return Recipient.live(recipientId).asObservable();
    }

    public static /* synthetic */ Optional lambda$new$9(Recipient recipient) throws Throwable {
        return Optional.ofNullable(recipient.getWallpaper());
    }

    public static /* synthetic */ ThreadAnimationState lambda$new$10(Long l, ThreadAnimationState threadAnimationState) {
        if (threadAnimationState.getThreadId() == l.longValue()) {
            return threadAnimationState;
        }
        return new ThreadAnimationState(l.longValue(), null, false);
    }

    public static /* synthetic */ ThreadAnimationState lambda$new$11(ConversationData conversationData, ThreadAnimationState threadAnimationState) {
        if (threadAnimationState.getThreadId() == conversationData.getThreadId()) {
            return threadAnimationState.copy(threadAnimationState.getThreadId(), conversationData, threadAnimationState.getHasCommittedNonEmptyMessageList());
        }
        return threadAnimationState.copy(conversationData.getThreadId(), conversationData, false);
    }

    public Observable<StoryViewState> getStoryViewState() {
        return this.recipientId.subscribeOn(Schedulers.io()).switchMap(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda8
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return StoryViewState.getForRecipientId((RecipientId) obj);
            }
        }).distinctUntilChanged().observeOn(AndroidSchedulers.mainThread());
    }

    public void onMessagesCommitted(List<ConversationMessage> list) {
        if (Util.hasItems(list)) {
            this.threadAnimationStateStore.update(new com.annimon.stream.function.Function(list) { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda2
                public final /* synthetic */ List f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ConversationViewModel.lambda$onMessagesCommitted$14(this.f$0, (ThreadAnimationState) obj);
                }
            });
        }
    }

    public static /* synthetic */ ThreadAnimationState lambda$onMessagesCommitted$14(List list, ThreadAnimationState threadAnimationState) {
        return threadAnimationState.getThreadId() == ((Long) Collection$EL.stream(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda9
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((ConversationMessage) obj);
            }
        }).findFirst().map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda10
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.lambda$onMessagesCommitted$13((ConversationMessage) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(-2L)).longValue() ? threadAnimationState.copy(threadAnimationState.getThreadId(), threadAnimationState.getThreadMetadata(), true) : threadAnimationState;
    }

    public static /* synthetic */ Long lambda$onMessagesCommitted$13(ConversationMessage conversationMessage) {
        return Long.valueOf(conversationMessage.getMessageRecord().getThreadId());
    }

    public void submitMarkReadRequest(long j) {
        this.markReadRequestPublisher.onNext(Long.valueOf(j));
    }

    public boolean shouldPlayMessageAnimations() {
        return this.threadAnimationStateStore.getState().shouldPlayMessageAnimations();
    }

    public void setToolbarBottom(int i) {
        this.toolbarBottom.setValue(Integer.valueOf(i));
    }

    public void setInlinePlayerVisible(boolean z) {
        this.inlinePlayerHeight.setValue(Integer.valueOf(z ? ViewUtil.dpToPx(36) : 0));
    }

    public void onAttachmentKeyboardOpen() {
        MediaRepository mediaRepository = this.mediaRepository;
        Application application = this.context;
        MutableLiveData<List<Media>> mutableLiveData = this.recentMedia;
        Objects.requireNonNull(mutableLiveData);
        mediaRepository.getMediaInBucket(application, Media.ALL_MEDIA_BUCKET_ID, new MediaRepository.Callback() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda12
            @Override // org.thoughtcrime.securesms.mediasend.MediaRepository.Callback
            public final void onComplete(Object obj) {
                MutableLiveData.this.postValue((List) obj);
            }
        });
    }

    public void onConversationDataAvailable(RecipientId recipientId, long j, int i) {
        String str = TAG;
        Log.d(str, "[onConversationDataAvailable] recipientId: " + recipientId + ", threadId: " + j + ", startingPosition: " + i);
        this.jumpToPosition = i;
        this.threadId.onNext(Long.valueOf(j));
        this.recipientId.onNext(recipientId);
    }

    public void clearThreadId() {
        this.jumpToPosition = -1;
        this.threadId.onNext(-1L);
    }

    public void setSearchQuery(String str) {
        this.searchQuery.setValue(str);
    }

    public void markGiftBadgeRevealed(long j) {
        this.conversationRepository.markGiftBadgeRevealed(j);
    }

    public void checkIfMmsIsEnabled() {
        this.disposables.add(this.conversationRepository.checkIfMmsIsEnabled().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda36
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationViewModel.this.lambda$checkIfMmsIsEnabled$16((Boolean) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$checkIfMmsIsEnabled$16(Boolean bool) throws Throwable {
        this.conversationStateStore.update(new Function1() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda3
            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return ((ConversationState) obj).withMmsEnabled(true);
            }
        });
    }

    public static /* synthetic */ boolean lambda$getMarkReadRequests$17(ThreadCountAggregator threadCountAggregator) throws Throwable {
        return !(threadCountAggregator instanceof ThreadCountAggregator.Init);
    }

    public Flowable<Long> getMarkReadRequests() {
        return Flowable.combineLatest(this.markReadRequestPublisher.onBackpressureBuffer(), this.threadCountStore.getStateFlowable().filter(new io.reactivex.rxjava3.functions.Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Predicate
            public final boolean test(Object obj) {
                return ConversationViewModel.lambda$getMarkReadRequests$17((ThreadCountAggregator) obj);
            }
        }).take(1), new BiFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$getMarkReadRequests$18((Long) obj, (ThreadCountAggregator) obj2);
            }
        });
    }

    /* JADX DEBUG: Type inference failed for r0v2. Raw type applied. Possible types: io.reactivex.rxjava3.core.Flowable<R>, io.reactivex.rxjava3.core.Flowable<java.lang.Integer> */
    public Flowable<Integer> getThreadUnreadCount() {
        return this.threadCountStore.getStateFlowable().map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda35
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return Integer.valueOf(((ThreadCountAggregator) obj).getCount());
            }
        });
    }

    Flowable<ConversationState> getConversationState() {
        return this.conversationStateStore.getStateFlowable().observeOn(AndroidSchedulers.mainThread());
    }

    public Flowable<ConversationSecurityInfo> getConversationSecurityInfo(RecipientId recipientId) {
        return getConversationState().map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda37
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ((ConversationState) obj).getSecurityInfo();
            }
        }).filter(new io.reactivex.rxjava3.functions.Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda38
            @Override // io.reactivex.rxjava3.functions.Predicate
            public final boolean test(Object obj) {
                return ConversationViewModel.lambda$getConversationSecurityInfo$19(RecipientId.this, (ConversationSecurityInfo) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$getConversationSecurityInfo$19(RecipientId recipientId, ConversationSecurityInfo conversationSecurityInfo) throws Throwable {
        return conversationSecurityInfo.isInitialized() && Objects.equals(conversationSecurityInfo.getRecipientId(), recipientId);
    }

    public void updateSecurityInfo() {
        this.conversationStateTick.onNext(Unit.INSTANCE);
    }

    public boolean isDefaultSmsApplication() {
        return this.conversationStateStore.getState().getSecurityInfo().isDefaultSmsApplication();
    }

    public boolean isPushAvailable() {
        return this.conversationStateStore.getState().getSecurityInfo().isPushAvailable();
    }

    public ConversationState getConversationStateSnapshot() {
        return this.conversationStateStore.getState();
    }

    public LiveData<String> getSearchQuery() {
        return this.searchQuery;
    }

    public LiveData<Integer> getConversationTopMargin() {
        return this.conversationTopMargin;
    }

    public Observable<Boolean> canShowAsBubble() {
        return this.canShowAsBubble.observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<Boolean> getShowScrollToBottom() {
        return Transformations.distinctUntilChanged(this.showScrollButtons);
    }

    public static /* synthetic */ Boolean lambda$getShowMentionsButton$20(Boolean bool, Boolean bool2) {
        return Boolean.valueOf(bool.booleanValue() && bool2.booleanValue());
    }

    public LiveData<Boolean> getShowMentionsButton() {
        return Transformations.distinctUntilChanged(LiveDataUtil.combineLatest(this.showScrollButtons, this.hasUnreadMentions, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda13
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$getShowMentionsButton$20((Boolean) obj, (Boolean) obj2);
            }
        }));
    }

    public Observable<Optional<ChatWallpaper>> getWallpaper() {
        return this.wallpaper.observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public Observable<ChatColors> getChatColors() {
        return this.chatColors.observeOn(AndroidSchedulers.mainThread());
    }

    public void setHasUnreadMentions(boolean z) {
        this.hasUnreadMentions.setValue(Boolean.valueOf(z));
    }

    public boolean getShowScrollButtons() {
        return this.showScrollButtons.getValue().booleanValue();
    }

    public void setShowScrollButtons(boolean z) {
        this.showScrollButtons.setValue(Boolean.valueOf(z));
    }

    public LiveData<List<Media>> getRecentMedia() {
        return this.recentMedia;
    }

    public Observable<MessageData> getMessageData() {
        return this.messageData.observeOn(AndroidSchedulers.mainThread());
    }

    public PagingController<MessageId> getPagingController() {
        return this.pagingController;
    }

    public Observable<Map<RecipientId, NameColor>> getNameColorsMap() {
        return this.recipientId.observeOn(Schedulers.io()).distinctUntilChanged().map(new ConversationViewModel$$ExternalSyntheticLambda6()).map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.this.lambda$getNameColorsMap$21((Recipient) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
    }

    public /* synthetic */ Map lambda$getNameColorsMap$21(Recipient recipient) throws Throwable {
        if (recipient.getGroupId().isPresent()) {
            return this.groupAuthorNameColorHelper.getColorMap(recipient.getGroupId().get());
        }
        return Collections.emptyMap();
    }

    public LiveData<Optional<NotificationProfile>> getActiveNotificationProfile() {
        return LiveDataReactiveStreams.fromPublisher(Observable.combineLatest(Observable.interval(0, 30, TimeUnit.SECONDS), this.notificationProfilesRepository.getProfiles(), new BiFunction() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return ConversationViewModel.lambda$getActiveNotificationProfile$22((Long) obj, (List) obj2);
            }
        }).map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationViewModel.lambda$getActiveNotificationProfile$23((List) obj);
            }
        }).toFlowable(BackpressureStrategy.LATEST));
    }

    public static /* synthetic */ Optional lambda$getActiveNotificationProfile$23(List list) throws Throwable {
        return Optional.ofNullable(NotificationProfiles.getActiveProfile(list));
    }

    public void setArgs(ConversationIntents.Args args) {
        this.args = args;
    }

    public ConversationIntents.Args getArgs() {
        ConversationIntents.Args args = this.args;
        Objects.requireNonNull(args);
        return args;
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onRecaptchaRequiredEvent(RecaptchaRequiredEvent recaptchaRequiredEvent) {
        this.events.postValue(Event.SHOW_RECAPTCHA);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        this.threadAnimationStateStore.getStateLiveData().removeObserver(this.threadAnimationStateStoreDriver);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.conversationObserver);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.messageUpdateObserver);
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.messageInsertObserver);
        this.disposables.clear();
        EventBus.getDefault().unregister(this);
    }

    /* loaded from: classes4.dex */
    public static class MessageData {
        private final List<ConversationMessage> messages;
        private final ConversationData metadata;

        MessageData(ConversationData conversationData, List<ConversationMessage> list) {
            this.metadata = conversationData;
            this.messages = list;
        }

        public List<ConversationMessage> getMessages() {
            return this.messages;
        }

        public ConversationData getMetadata() {
            return this.metadata;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory extends ViewModelProvider.NewInstanceFactory {
        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ConversationViewModel());
        }
    }
}
