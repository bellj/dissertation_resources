package org.thoughtcrime.securesms.conversation;

import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Region;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.Projection;

/* compiled from: ClipProjectionDrawable.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0016¢\u0006\u0002\u0010\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0006\u0010\u000b\u001a\u00020\fJ\u0010\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0014\u0010\u0010\u001a\u00020\f2\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\u0011J\u000e\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0013\u001a\u00020\u0004R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/ClipProjectionDrawable;", "Landroid/graphics/drawable/LayerDrawable;", "()V", "wrapped", "Landroid/graphics/drawable/Drawable;", "(Landroid/graphics/drawable/Drawable;)V", "clipPath", "Landroid/graphics/Path;", "projections", "", "Lorg/thoughtcrime/securesms/util/Projection;", "clearProjections", "", "draw", "canvas", "Landroid/graphics/Canvas;", "setProjections", "", "setWrappedDrawable", "drawable", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ClipProjectionDrawable extends LayerDrawable {
    private final Path clipPath;
    private List<Projection> projections;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ClipProjectionDrawable(Drawable drawable) {
        super(new Drawable[]{drawable});
        Intrinsics.checkNotNullParameter(drawable, "wrapped");
        setId(0, 0);
        this.clipPath = new Path();
        this.projections = CollectionsKt__CollectionsKt.emptyList();
    }

    public ClipProjectionDrawable() {
        this(new ColorDrawable(0));
    }

    public final void setWrappedDrawable(Drawable drawable) {
        Intrinsics.checkNotNullParameter(drawable, "drawable");
        setDrawableByLayerId(0, drawable);
    }

    public final void setProjections(Set<Projection> set) {
        Intrinsics.checkNotNullParameter(set, "projections");
        this.projections = CollectionsKt___CollectionsKt.toList(set);
        invalidateSelf();
    }

    public final void clearProjections() {
        this.projections = CollectionsKt__CollectionsKt.emptyList();
        invalidateSelf();
    }

    @Override // android.graphics.drawable.LayerDrawable, android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        if (!this.projections.isEmpty()) {
            canvas.save();
            this.clipPath.rewind();
            for (Projection projection : this.projections) {
                projection.applyToPath(this.clipPath);
            }
            canvas.clipPath(this.clipPath, Region.Op.DIFFERENCE);
            super.draw(canvas);
            canvas.restore();
            return;
        }
        super.draw(canvas);
    }
}
