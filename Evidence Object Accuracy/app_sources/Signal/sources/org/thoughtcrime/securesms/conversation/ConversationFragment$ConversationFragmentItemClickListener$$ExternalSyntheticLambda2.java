package org.thoughtcrime.securesms.conversation;

import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda2 implements Runnable {
    public final /* synthetic */ MmsMessageRecord f$0;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda2(MmsMessageRecord mmsMessageRecord) {
        this.f$0 = mmsMessageRecord;
    }

    @Override // java.lang.Runnable
    public final void run() {
        ConversationFragment.ConversationFragmentItemClickListener.lambda$onViewOnceMessageClicked$4(this.f$0);
    }
}
