package org.thoughtcrime.securesms.conversation;

import android.animation.ValueAnimator;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$TransitionListener$$ExternalSyntheticLambda0 implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ RecyclerView f$0;

    public /* synthetic */ ConversationFragment$TransitionListener$$ExternalSyntheticLambda0(RecyclerView recyclerView) {
        this.f$0 = recyclerView;
    }

    @Override // android.animation.ValueAnimator.AnimatorUpdateListener
    public final void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.f$0.invalidate();
    }
}
