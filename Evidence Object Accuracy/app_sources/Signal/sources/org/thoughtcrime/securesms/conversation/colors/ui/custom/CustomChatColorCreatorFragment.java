package org.thoughtcrime.securesms.conversation.colors.ui.custom;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: CustomChatColorCreatorFragment.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/custom/CustomChatColorCreatorFragment;", "Landroidx/fragment/app/Fragment;", "()V", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomChatColorCreatorFragment extends Fragment {
    public CustomChatColorCreatorFragment() {
        super(R.layout.custom_chat_color_creator_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        View findViewById2 = view.findViewById(R.id.tab_layout);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.tab_layout)");
        TabLayout tabLayout = (TabLayout) findViewById2;
        View findViewById3 = view.findViewById(R.id.pager);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.pager)");
        ViewPager2 viewPager2 = (ViewPager2) findViewById3;
        Bundle requireArguments = requireArguments();
        Intrinsics.checkNotNullExpressionValue(requireArguments, "requireArguments()");
        CustomChatColorPagerAdapter customChatColorPagerAdapter = new CustomChatColorPagerAdapter(this, requireArguments);
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorFragment$$ExternalSyntheticLambda0
            @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
            public final void onConfigureTab(TabLayout.Tab tab, int i) {
                CustomChatColorCreatorFragment.m1528$r8$lambda$_iEnqmY2OduQJcisgoLqtbCCCc(tab, i);
            }
        });
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.custom.CustomChatColorCreatorFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                CustomChatColorCreatorFragment.$r8$lambda$fKvTzeY4iKfW1ifLMsU1_eucpjo(view2);
            }
        });
        viewPager2.setUserInputEnabled(false);
        viewPager2.setAdapter(customChatColorPagerAdapter);
        if (Build.VERSION.SDK_INT < 21) {
            tabLayout.setVisibility(8);
        } else {
            tabLayoutMediator.attach();
        }
        viewPager2.setCurrentItem(CustomChatColorCreatorFragmentArgs.fromBundle(requireArguments()).getStartPage(), false);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1529onViewCreated$lambda0(TabLayout.Tab tab, int i) {
        Intrinsics.checkNotNullParameter(tab, "tab");
        tab.setText(i == 0 ? R.string.CustomChatColorCreatorFragment__solid : R.string.CustomChatColorCreatorFragment__gradient);
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1530onViewCreated$lambda1(View view) {
        Navigation.findNavController(view).popBackStack();
    }
}
