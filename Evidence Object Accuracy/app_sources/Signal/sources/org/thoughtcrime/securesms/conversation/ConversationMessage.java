package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.text.SpannableString;
import java.security.MessageDigest;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.Conversions;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.conversation.MessageStyler;
import org.thoughtcrime.securesms.conversation.mutiselect.Multiselect;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectCollection;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;

/* loaded from: classes4.dex */
public class ConversationMessage {
    private final SpannableString body;
    private final boolean hasBeenQuoted;
    private final List<Mention> mentions;
    private final MessageRecord messageRecord;
    private final MultiselectCollection multiselectCollection;
    private final MessageStyler.Result styleResult;

    private ConversationMessage(MessageRecord messageRecord) {
        this(messageRecord, null, null, false);
    }

    private ConversationMessage(MessageRecord messageRecord, boolean z) {
        this(messageRecord, null, null, z);
    }

    private ConversationMessage(MessageRecord messageRecord, CharSequence charSequence, List<Mention> list, boolean z) {
        SpannableString spannableString;
        this.messageRecord = messageRecord;
        this.hasBeenQuoted = z;
        list = list == null ? Collections.emptyList() : list;
        this.mentions = list;
        if (charSequence != null) {
            this.body = SpannableString.valueOf(charSequence);
        } else if (messageRecord.hasMessageRanges()) {
            this.body = SpannableString.valueOf(messageRecord.getBody());
        } else {
            this.body = null;
        }
        if (!list.isEmpty() && (spannableString = this.body) != null) {
            MentionAnnotation.setMentionAnnotations(spannableString, list);
        }
        if (this.body == null || !messageRecord.hasMessageRanges()) {
            this.styleResult = MessageStyler.Result.none();
        } else {
            this.styleResult = MessageStyler.style(messageRecord.requireMessageRanges(), this.body);
        }
        this.multiselectCollection = Multiselect.getParts(this);
    }

    public MessageRecord getMessageRecord() {
        return this.messageRecord;
    }

    public List<Mention> getMentions() {
        return this.mentions;
    }

    public MultiselectCollection getMultiselectCollection() {
        return this.multiselectCollection;
    }

    public boolean hasBeenQuoted() {
        return this.hasBeenQuoted;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        return this.messageRecord.equals(((ConversationMessage) obj).messageRecord);
    }

    public int hashCode() {
        return this.messageRecord.hashCode();
    }

    public long getUniqueId(MessageDigest messageDigest) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.messageRecord.isMms() ? "MMS::" : "SMS::");
        sb.append(this.messageRecord.getId());
        return Conversions.byteArrayToLong(messageDigest.digest(sb.toString().getBytes()));
    }

    public SpannableString getDisplayBody(Context context) {
        SpannableString spannableString = this.body;
        return spannableString != null ? spannableString : this.messageRecord.getDisplayBody(context);
    }

    public boolean hasStyleLinks() {
        return this.styleResult.getHasStyleLinks();
    }

    public BodyRangeList.BodyRange.Button getBottomButton() {
        return this.styleResult.getBottomButton();
    }

    /* loaded from: classes4.dex */
    public static class ConversationMessageFactory {
        public static ConversationMessage createWithResolvedData(MessageRecord messageRecord, boolean z) {
            return new ConversationMessage(messageRecord, z);
        }

        public static ConversationMessage createWithResolvedData(MessageRecord messageRecord, CharSequence charSequence, List<Mention> list, boolean z) {
            if (!messageRecord.isMms() || list == null || list.isEmpty()) {
                return new ConversationMessage(messageRecord, charSequence, null, z);
            }
            return new ConversationMessage(messageRecord, charSequence, list, z);
        }

        public static ConversationMessage createWithUnresolvedData(Context context, MessageRecord messageRecord, List<Mention> list) {
            boolean isQuoted = SignalDatabase.mmsSms().isQuoted(messageRecord);
            if (!messageRecord.isMms() || list == null || list.isEmpty()) {
                return createWithResolvedData(messageRecord, isQuoted);
            }
            MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(context, messageRecord, list);
            return new ConversationMessage(messageRecord, updateBodyAndMentionsWithDisplayNames.getBody(), updateBodyAndMentionsWithDisplayNames.getMentions(), isQuoted);
        }

        public static ConversationMessage createWithUnresolvedData(Context context, MessageRecord messageRecord) {
            return createWithUnresolvedData(context, messageRecord, messageRecord.getDisplayBody(context));
        }

        public static ConversationMessage createWithUnresolvedData(Context context, MessageRecord messageRecord, CharSequence charSequence) {
            boolean isQuoted = SignalDatabase.mmsSms().isQuoted(messageRecord);
            if (messageRecord.isMms()) {
                List<Mention> mentionsForMessage = SignalDatabase.mentions().getMentionsForMessage(messageRecord.getId());
                if (!mentionsForMessage.isEmpty()) {
                    MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(context, charSequence, mentionsForMessage);
                    return new ConversationMessage(messageRecord, updateBodyAndMentionsWithDisplayNames.getBody(), updateBodyAndMentionsWithDisplayNames.getMentions(), isQuoted);
                }
            }
            return createWithResolvedData(messageRecord, charSequence, null, isQuoted);
        }

        public static ConversationMessage createWithUnresolvedData(Context context, MessageRecord messageRecord, CharSequence charSequence, boolean z) {
            if (messageRecord.isMms()) {
                List<Mention> mentionsForMessage = SignalDatabase.mentions().getMentionsForMessage(messageRecord.getId());
                if (!mentionsForMessage.isEmpty()) {
                    MentionUtil.UpdatedBodyAndMentions updateBodyAndMentionsWithDisplayNames = MentionUtil.updateBodyAndMentionsWithDisplayNames(context, charSequence, mentionsForMessage);
                    return new ConversationMessage(messageRecord, updateBodyAndMentionsWithDisplayNames.getBody(), updateBodyAndMentionsWithDisplayNames.getMentions(), z);
                }
            }
            return createWithResolvedData(messageRecord, charSequence, null, z);
        }
    }
}
