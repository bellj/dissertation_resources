package org.thoughtcrime.securesms.conversation;

import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda17 implements SimpleTask.BackgroundTask {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;
    public final /* synthetic */ MmsMessageRecord f$1;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda17(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener, MmsMessageRecord mmsMessageRecord) {
        this.f$0 = conversationFragmentItemClickListener;
        this.f$1 = mmsMessageRecord;
    }

    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
    public final Object run() {
        return this.f$0.lambda$onViewOnceMessageClicked$3(this.f$1);
    }
}
