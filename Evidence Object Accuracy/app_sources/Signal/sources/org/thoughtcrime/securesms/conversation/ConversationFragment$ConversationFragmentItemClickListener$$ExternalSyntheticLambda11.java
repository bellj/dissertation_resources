package org.thoughtcrime.securesms.conversation;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.conversation.ConversationFragment;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda11 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ConversationFragment.ConversationFragmentItemClickListener f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda11(ConversationFragment.ConversationFragmentItemClickListener conversationFragmentItemClickListener, Recipient recipient) {
        this.f$0 = conversationFragmentItemClickListener;
        this.f$1 = recipient;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f$0.lambda$onBlockJoinRequest$18(this.f$1, dialogInterface, i);
    }
}
