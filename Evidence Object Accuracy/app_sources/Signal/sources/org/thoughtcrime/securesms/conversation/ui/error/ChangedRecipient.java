package org.thoughtcrime.securesms.conversation.ui.error;

import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class ChangedRecipient {
    private final Recipient recipient;
    private final IdentityRecord record;

    public ChangedRecipient(Recipient recipient, IdentityRecord identityRecord) {
        this.recipient = recipient;
        this.record = identityRecord;
    }

    public Recipient getRecipient() {
        return this.recipient;
    }

    public IdentityRecord getIdentityRecord() {
        return this.record;
    }

    public boolean isUnverified() {
        return this.record.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.UNVERIFIED;
    }

    public boolean isVerified() {
        return this.record.getVerifiedStatus() == IdentityDatabase.VerifiedStatus.VERIFIED;
    }

    public String toString() {
        return "ChangedRecipient{recipient=" + this.recipient.getId() + ", record=" + this.record.getIdentityKey().hashCode() + '}';
    }
}
