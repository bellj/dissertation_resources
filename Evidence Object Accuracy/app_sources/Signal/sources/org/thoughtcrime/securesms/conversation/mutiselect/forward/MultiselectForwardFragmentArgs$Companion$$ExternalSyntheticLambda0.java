package org.thoughtcrime.securesms.conversation.mutiselect.forward;

import android.content.Context;
import android.net.Uri;
import j$.util.function.Consumer;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ Context f$0;
    public final /* synthetic */ Uri f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ long f$3;
    public final /* synthetic */ Consumer f$4;

    public /* synthetic */ MultiselectForwardFragmentArgs$Companion$$ExternalSyntheticLambda0(Context context, Uri uri, String str, long j, Consumer consumer) {
        this.f$0 = context;
        this.f$1 = uri;
        this.f$2 = str;
        this.f$3 = j;
        this.f$4 = consumer;
    }

    @Override // java.lang.Runnable
    public final void run() {
        MultiselectForwardFragmentArgs.Companion.m1578create$lambda3(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
    }
}
