package org.thoughtcrime.securesms.conversation.ui.error;

import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.FromTextView;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.AlwaysChangedDiffUtil;

/* loaded from: classes4.dex */
public final class SafetyNumberChangeAdapter extends ListAdapter<ChangedRecipient, ViewHolder> {
    private final Callbacks callbacks;

    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onViewIdentityRecord(IdentityRecord identityRecord);
    }

    public SafetyNumberChangeAdapter(Callbacks callbacks) {
        super(new AlwaysChangedDiffUtil());
        this.callbacks = callbacks;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.safety_number_change_recipient, viewGroup, false));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(getItem(i));
    }

    /* loaded from: classes4.dex */
    public class ViewHolder extends RecyclerView.ViewHolder {
        final AvatarImageView avatar;
        final FromTextView name;
        final TextView subtitle;
        final View viewButton;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            SafetyNumberChangeAdapter.this = r1;
            this.avatar = (AvatarImageView) view.findViewById(R.id.safety_number_change_recipient_avatar);
            this.name = (FromTextView) view.findViewById(R.id.safety_number_change_recipient_name);
            this.subtitle = (TextView) view.findViewById(R.id.safety_number_change_recipient_subtitle);
            this.viewButton = view.findViewById(R.id.safety_number_change_recipient_view);
        }

        void bind(ChangedRecipient changedRecipient) {
            this.avatar.setRecipient(changedRecipient.getRecipient());
            this.name.setText(changedRecipient.getRecipient());
            int i = 0;
            if (changedRecipient.isUnverified() || changedRecipient.isVerified()) {
                this.subtitle.setText(R.string.safety_number_change_dialog__previous_verified);
                Drawable tint = DrawableUtil.tint(ContextUtil.requireDrawable(this.itemView.getContext(), R.drawable.check), ContextCompat.getColor(this.itemView.getContext(), R.color.signal_text_secondary));
                tint.setBounds(0, 0, ViewUtil.dpToPx(12), ViewUtil.dpToPx(12));
                this.subtitle.setCompoundDrawables(tint, null, null, null);
            } else if (changedRecipient.getRecipient().hasAUserSetDisplayName(this.itemView.getContext())) {
                this.subtitle.setText(changedRecipient.getRecipient().getE164().orElse(""));
                this.subtitle.setCompoundDrawables(null, null, null, null);
            } else {
                this.subtitle.setText("");
            }
            TextView textView = this.subtitle;
            if (TextUtils.isEmpty(textView.getText())) {
                i = 8;
            }
            textView.setVisibility(i);
            this.viewButton.setOnClickListener(new SafetyNumberChangeAdapter$ViewHolder$$ExternalSyntheticLambda0(this, changedRecipient));
        }

        public /* synthetic */ void lambda$bind$0(ChangedRecipient changedRecipient, View view) {
            SafetyNumberChangeAdapter.this.callbacks.onViewIdentityRecord(changedRecipient.getIdentityRecord());
        }
    }
}
