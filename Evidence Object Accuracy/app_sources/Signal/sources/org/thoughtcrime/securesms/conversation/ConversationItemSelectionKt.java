package org.thoughtcrime.securesms.conversation;

import android.view.View;
import android.view.ViewGroup;
import androidx.core.view.ViewGroupKt;
import kotlin.Metadata;

/* compiled from: ConversationItemSelection.kt */
@Metadata(d1 = {"\u0000\f\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0002¨\u0006\u0003"}, d2 = {"destroyAllDrawingCaches", "", "Landroid/view/ViewGroup;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationItemSelectionKt {
    public static final void destroyAllDrawingCaches(ViewGroup viewGroup) {
        for (View view : ViewGroupKt.getChildren(viewGroup)) {
            view.destroyDrawingCache();
            if (view instanceof ViewGroup) {
                destroyAllDrawingCaches((ViewGroup) view);
            }
        }
    }
}
