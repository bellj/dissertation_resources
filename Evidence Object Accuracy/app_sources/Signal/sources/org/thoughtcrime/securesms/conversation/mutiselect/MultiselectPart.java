package org.thoughtcrime.securesms.conversation.mutiselect;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.database.model.MessageRecord;

/* compiled from: MultiselectPart.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u000b\f\r\u000eB\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\nR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0001\u0004\u000f\u0010\u0011\u0012¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;)V", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "getMessageRecord", "Lorg/thoughtcrime/securesms/database/model/MessageRecord;", "isExpired", "", "Attachments", "Message", "Text", "Update", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Text;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Attachments;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Update;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Message;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class MultiselectPart {
    private final ConversationMessage conversationMessage;

    public /* synthetic */ MultiselectPart(ConversationMessage conversationMessage, DefaultConstructorMarker defaultConstructorMarker) {
        this(conversationMessage);
    }

    private MultiselectPart(ConversationMessage conversationMessage) {
        this.conversationMessage = conversationMessage;
    }

    public ConversationMessage getConversationMessage() {
        return this.conversationMessage;
    }

    public final MessageRecord getMessageRecord() {
        MessageRecord messageRecord = getConversationMessage().getMessageRecord();
        Intrinsics.checkNotNullExpressionValue(messageRecord, "conversationMessage.messageRecord");
        return messageRecord;
    }

    public final boolean isExpired() {
        long expireStarted = getConversationMessage().getMessageRecord().getExpireStarted() + getConversationMessage().getMessageRecord().getExpiresIn();
        return expireStarted > 0 && expireStarted < System.currentTimeMillis();
    }

    /* compiled from: MultiselectPart.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Text;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;)V", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Text extends MultiselectPart {
        private final ConversationMessage conversationMessage;

        public static /* synthetic */ Text copy$default(Text text, ConversationMessage conversationMessage, int i, Object obj) {
            if ((i & 1) != 0) {
                conversationMessage = text.getConversationMessage();
            }
            return text.copy(conversationMessage);
        }

        public final ConversationMessage component1() {
            return getConversationMessage();
        }

        public final Text copy(ConversationMessage conversationMessage) {
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            return new Text(conversationMessage);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Text) && Intrinsics.areEqual(getConversationMessage(), ((Text) obj).getConversationMessage());
        }

        public int hashCode() {
            return getConversationMessage().hashCode();
        }

        public String toString() {
            return "Text(conversationMessage=" + getConversationMessage() + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Text(ConversationMessage conversationMessage) {
            super(conversationMessage, null);
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            this.conversationMessage = conversationMessage;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart
        public ConversationMessage getConversationMessage() {
            return this.conversationMessage;
        }
    }

    /* compiled from: MultiselectPart.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Attachments;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;)V", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Attachments extends MultiselectPart {
        private final ConversationMessage conversationMessage;

        public static /* synthetic */ Attachments copy$default(Attachments attachments, ConversationMessage conversationMessage, int i, Object obj) {
            if ((i & 1) != 0) {
                conversationMessage = attachments.getConversationMessage();
            }
            return attachments.copy(conversationMessage);
        }

        public final ConversationMessage component1() {
            return getConversationMessage();
        }

        public final Attachments copy(ConversationMessage conversationMessage) {
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            return new Attachments(conversationMessage);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Attachments) && Intrinsics.areEqual(getConversationMessage(), ((Attachments) obj).getConversationMessage());
        }

        public int hashCode() {
            return getConversationMessage().hashCode();
        }

        public String toString() {
            return "Attachments(conversationMessage=" + getConversationMessage() + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Attachments(ConversationMessage conversationMessage) {
            super(conversationMessage, null);
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            this.conversationMessage = conversationMessage;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart
        public ConversationMessage getConversationMessage() {
            return this.conversationMessage;
        }
    }

    /* compiled from: MultiselectPart.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Update;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;)V", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Update extends MultiselectPart {
        private final ConversationMessage conversationMessage;

        public static /* synthetic */ Update copy$default(Update update, ConversationMessage conversationMessage, int i, Object obj) {
            if ((i & 1) != 0) {
                conversationMessage = update.getConversationMessage();
            }
            return update.copy(conversationMessage);
        }

        public final ConversationMessage component1() {
            return getConversationMessage();
        }

        public final Update copy(ConversationMessage conversationMessage) {
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            return new Update(conversationMessage);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Update) && Intrinsics.areEqual(getConversationMessage(), ((Update) obj).getConversationMessage());
        }

        public int hashCode() {
            return getConversationMessage().hashCode();
        }

        public String toString() {
            return "Update(conversationMessage=" + getConversationMessage() + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Update(ConversationMessage conversationMessage) {
            super(conversationMessage, null);
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            this.conversationMessage = conversationMessage;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart
        public ConversationMessage getConversationMessage() {
            return this.conversationMessage;
        }
    }

    /* compiled from: MultiselectPart.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart$Message;", "Lorg/thoughtcrime/securesms/conversation/mutiselect/MultiselectPart;", "conversationMessage", "Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "(Lorg/thoughtcrime/securesms/conversation/ConversationMessage;)V", "getConversationMessage", "()Lorg/thoughtcrime/securesms/conversation/ConversationMessage;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Message extends MultiselectPart {
        private final ConversationMessage conversationMessage;

        public static /* synthetic */ Message copy$default(Message message, ConversationMessage conversationMessage, int i, Object obj) {
            if ((i & 1) != 0) {
                conversationMessage = message.getConversationMessage();
            }
            return message.copy(conversationMessage);
        }

        public final ConversationMessage component1() {
            return getConversationMessage();
        }

        public final Message copy(ConversationMessage conversationMessage) {
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            return new Message(conversationMessage);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof Message) && Intrinsics.areEqual(getConversationMessage(), ((Message) obj).getConversationMessage());
        }

        public int hashCode() {
            return getConversationMessage().hashCode();
        }

        public String toString() {
            return "Message(conversationMessage=" + getConversationMessage() + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Message(ConversationMessage conversationMessage) {
            super(conversationMessage, null);
            Intrinsics.checkNotNullParameter(conversationMessage, "conversationMessage");
            this.conversationMessage = conversationMessage;
        }

        @Override // org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart
        public ConversationMessage getConversationMessage() {
            return this.conversationMessage;
        }
    }
}
