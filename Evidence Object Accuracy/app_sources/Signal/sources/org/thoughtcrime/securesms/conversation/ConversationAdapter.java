package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.exoplayer2.MediaItem;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.signal.paging.PagingController;
import org.thoughtcrime.securesms.BindableConversationItem;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.colors.Colorizable;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicyEnforcer;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.CachedInflater;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ProjectionList;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ConversationAdapter extends ListAdapter<ConversationMessage, RecyclerView.ViewHolder> implements StickyHeaderDecoration.StickyHeaderAdapter<StickyHeaderViewHolder> {
    public static final int HEADER_TYPE_INLINE_DATE;
    public static final int HEADER_TYPE_LAST_SEEN;
    public static final int HEADER_TYPE_POPOVER_DATE;
    public static final int MESSAGE_TYPE_FOOTER;
    private static final int MESSAGE_TYPE_HEADER;
    private static final int MESSAGE_TYPE_INCOMING_MULTIMEDIA;
    private static final int MESSAGE_TYPE_INCOMING_TEXT;
    private static final int MESSAGE_TYPE_OUTGOING_MULTIMEDIA;
    private static final int MESSAGE_TYPE_OUTGOING_TEXT;
    private static final int MESSAGE_TYPE_PLACEHOLDER;
    private static final int MESSAGE_TYPE_UPDATE;
    public static final int PAYLOAD_NAME_COLORS;
    public static final int PAYLOAD_SELECTED;
    private static final int PAYLOAD_TIMESTAMP;
    private static final String TAG = Log.tag(ConversationAdapter.class);
    private final Calendar calendar = Calendar.getInstance();
    private final ItemClickListener clickListener;
    private Colorizer colorizer;
    private boolean condensedMode;
    private final Context context;
    private final MessageDigest digest = getMessageDigestOrThrow();
    private View footerView;
    private final GlideRequests glideRequests;
    private boolean hasWallpaper;
    private ConversationMessage inlineContent;
    private boolean isMessageRequestAccepted;
    private boolean isTypingViewEnabled;
    private final LifecycleOwner lifecycleOwner;
    private final Locale locale;
    private PagingController pagingController;
    private final Recipient recipient;
    private ConversationMessage recordToPulse;
    private String searchQuery;
    private final Set<MultiselectPart> selected = new HashSet();
    private View typingView;

    /* loaded from: classes4.dex */
    public interface ItemClickListener extends BindableConversationItem.EventListener {
        void onItemClick(MultiselectPart multiselectPart);

        void onItemLongClick(View view, MultiselectPart multiselectPart);
    }

    public ConversationAdapter(Context context, LifecycleOwner lifecycleOwner, GlideRequests glideRequests, Locale locale, ItemClickListener itemClickListener, Recipient recipient, Colorizer colorizer) {
        super(new DiffUtil.ItemCallback<ConversationMessage>() { // from class: org.thoughtcrime.securesms.conversation.ConversationAdapter.1
            public boolean areContentsTheSame(ConversationMessage conversationMessage, ConversationMessage conversationMessage2) {
                return false;
            }

            public boolean areItemsTheSame(ConversationMessage conversationMessage, ConversationMessage conversationMessage2) {
                return conversationMessage.getMessageRecord().getId() == conversationMessage2.getMessageRecord().getId();
            }
        });
        this.lifecycleOwner = lifecycleOwner;
        this.context = context;
        this.glideRequests = glideRequests;
        this.locale = locale;
        this.clickListener = itemClickListener;
        this.recipient = recipient;
        this.hasWallpaper = recipient.hasWallpaper();
        this.isMessageRequestAccepted = true;
        this.colorizer = colorizer;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        if (isTypingViewEnabled() && i == 0) {
            return 5;
        }
        if (hasFooter() && i == getItemCount() - 1) {
            return 6;
        }
        ConversationMessage item = getItem(i);
        MessageRecord messageRecord = item != null ? item.getMessageRecord() : null;
        if (messageRecord == null) {
            return 7;
        }
        if (messageRecord.isUpdate()) {
            return 4;
        }
        if (!messageRecord.isOutgoing()) {
            return (!MessageRecordUtil.isTextOnly(messageRecord, this.context) || item.hasBeenQuoted()) ? 2 : 3;
        }
        if (!MessageRecordUtil.isTextOnly(messageRecord, this.context) || item.hasBeenQuoted()) {
            return 0;
        }
        return 1;
    }

    public /* synthetic */ void lambda$onCreateViewHolder$0(BindableConversationItem bindableConversationItem, View view) {
        ItemClickListener itemClickListener = this.clickListener;
        if (itemClickListener != null) {
            itemClickListener.onItemClick(bindableConversationItem.getMultiselectPartForLatestTouch());
        }
    }

    public /* synthetic */ boolean lambda$onCreateViewHolder$1(View view, BindableConversationItem bindableConversationItem, View view2) {
        ItemClickListener itemClickListener = this.clickListener;
        if (itemClickListener == null) {
            return true;
        }
        itemClickListener.onItemLongClick(view, bindableConversationItem.getMultiselectPartForLatestTouch());
        return true;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
                View inflate = CachedInflater.from(viewGroup.getContext()).inflate(getLayoutForViewType(i), viewGroup, false);
                BindableConversationItem bindableConversationItem = (BindableConversationItem) inflate;
                inflate.setOnClickListener(new View.OnClickListener(bindableConversationItem) { // from class: org.thoughtcrime.securesms.conversation.ConversationAdapter$$ExternalSyntheticLambda0
                    public final /* synthetic */ BindableConversationItem f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        ConversationAdapter.$r8$lambda$q34l8SqOcgcVZywQXV36PHUH4HY(ConversationAdapter.this, this.f$1, view);
                    }
                });
                inflate.setOnLongClickListener(new View.OnLongClickListener(inflate, bindableConversationItem) { // from class: org.thoughtcrime.securesms.conversation.ConversationAdapter$$ExternalSyntheticLambda1
                    public final /* synthetic */ View f$1;
                    public final /* synthetic */ BindableConversationItem f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // android.view.View.OnLongClickListener
                    public final boolean onLongClick(View view) {
                        return ConversationAdapter.$r8$lambda$mFWSV_zvHuXeuqUPDWQmcioXv3U(ConversationAdapter.this, this.f$1, this.f$2, view);
                    }
                });
                bindableConversationItem.setEventListener(this.clickListener);
                return new ConversationViewHolder(inflate);
            case 5:
                return new HeaderViewHolder(CachedInflater.from(viewGroup.getContext()).inflate(R.layout.cursor_adapter_header_footer_view, viewGroup, false));
            case 6:
                return new FooterViewHolder(CachedInflater.from(viewGroup.getContext()).inflate(R.layout.cursor_adapter_header_footer_view, viewGroup, false));
            case 7:
                FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(1, ViewUtil.dpToPx(100)));
                return new PlaceholderViewHolder(frameLayout);
            default:
                throw new IllegalStateException("Cannot create viewholder for type: " + i);
        }
    }

    private boolean containsValidPayload(List<Object> list) {
        return list.contains(0) || list.contains(1) || list.contains(2);
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i, List<Object> list) {
        if (containsValidPayload(list)) {
            int itemViewType = getItemViewType(i);
            if (itemViewType == 0 || itemViewType == 1 || itemViewType == 2 || itemViewType == 3 || itemViewType == 4) {
                ConversationViewHolder conversationViewHolder = (ConversationViewHolder) viewHolder;
                if (list.contains(0)) {
                    conversationViewHolder.getBindable().updateTimestamps();
                }
                if (list.contains(1)) {
                    conversationViewHolder.getBindable().updateContactNameColor();
                }
                if (list.contains(2)) {
                    conversationViewHolder.getBindable().updateSelectedState();
                    return;
                }
                return;
            }
            return;
        }
        super.onBindViewHolder(viewHolder, i, list);
    }

    public void setCondensedMode(boolean z) {
        this.condensedMode = z;
        notifyDataSetChanged();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x004b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0084  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0086  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onBindViewHolder(androidx.recyclerview.widget.RecyclerView.ViewHolder r21, int r22) {
        /*
            r20 = this;
            r0 = r20
            r1 = r22
            int r2 = r0.getItemViewType(r1)
            switch(r2) {
                case 0: goto L_0x0023;
                case 1: goto L_0x0023;
                case 2: goto L_0x0023;
                case 3: goto L_0x0023;
                case 4: goto L_0x0023;
                case 5: goto L_0x0018;
                case 6: goto L_0x000d;
                default: goto L_0x000b;
            }
        L_0x000b:
            goto L_0x00c7
        L_0x000d:
            r1 = r21
            org.thoughtcrime.securesms.conversation.ConversationAdapter$HeaderFooterViewHolder r1 = (org.thoughtcrime.securesms.conversation.ConversationAdapter.HeaderFooterViewHolder) r1
            android.view.View r2 = r0.footerView
            r1.bind(r2)
            goto L_0x00c7
        L_0x0018:
            r1 = r21
            org.thoughtcrime.securesms.conversation.ConversationAdapter$HeaderViewHolder r1 = (org.thoughtcrime.securesms.conversation.ConversationAdapter.HeaderViewHolder) r1
            android.view.View r2 = r0.typingView
            r1.bind(r2)
            goto L_0x00c7
        L_0x0023:
            r2 = r21
            org.thoughtcrime.securesms.conversation.ConversationAdapter$ConversationViewHolder r2 = (org.thoughtcrime.securesms.conversation.ConversationAdapter.ConversationViewHolder) r2
            org.thoughtcrime.securesms.conversation.ConversationMessage r1 = r0.getItem(r1)
            java.util.Objects.requireNonNull(r1)
            int r3 = r21.getAdapterPosition()
            int r4 = r20.getItemCount()
            r5 = 1
            int r4 = r4 - r5
            r15 = 0
            if (r3 >= r4) goto L_0x0048
            int r4 = r3 + 1
            boolean r6 = r0.isFooterPosition(r4)
            if (r6 != 0) goto L_0x0048
            org.thoughtcrime.securesms.conversation.ConversationMessage r4 = r0.getItem(r4)
            goto L_0x0049
        L_0x0048:
            r4 = r15
        L_0x0049:
            if (r3 <= 0) goto L_0x0057
            int r3 = r3 - r5
            boolean r6 = r0.isHeaderPosition(r3)
            if (r6 != 0) goto L_0x0057
            org.thoughtcrime.securesms.conversation.ConversationMessage r3 = r0.getItem(r3)
            goto L_0x0058
        L_0x0057:
            r3 = r15
        L_0x0058:
            org.thoughtcrime.securesms.BindableConversationItem r2 = r2.getBindable()
            androidx.lifecycle.LifecycleOwner r6 = r0.lifecycleOwner
            if (r4 == 0) goto L_0x0065
            org.thoughtcrime.securesms.database.model.MessageRecord r4 = r4.getMessageRecord()
            goto L_0x0066
        L_0x0065:
            r4 = r15
        L_0x0066:
            j$.util.Optional r7 = j$.util.Optional.ofNullable(r4)
            if (r3 == 0) goto L_0x0071
            org.thoughtcrime.securesms.database.model.MessageRecord r3 = r3.getMessageRecord()
            goto L_0x0072
        L_0x0071:
            r3 = r15
        L_0x0072:
            j$.util.Optional r8 = j$.util.Optional.ofNullable(r3)
            org.thoughtcrime.securesms.mms.GlideRequests r9 = r0.glideRequests
            java.util.Locale r10 = r0.locale
            java.util.Set<org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart> r11 = r0.selected
            org.thoughtcrime.securesms.recipients.Recipient r12 = r0.recipient
            java.lang.String r13 = r0.searchQuery
            org.thoughtcrime.securesms.conversation.ConversationMessage r3 = r0.recordToPulse
            if (r1 != r3) goto L_0x0086
            r14 = 1
            goto L_0x0087
        L_0x0086:
            r14 = 0
        L_0x0087:
            boolean r3 = r0.hasWallpaper
            if (r3 == 0) goto L_0x0092
            boolean r3 = r0.condensedMode
            if (r3 != 0) goto L_0x0092
            r16 = 1
            goto L_0x0094
        L_0x0092:
            r16 = 0
        L_0x0094:
            boolean r3 = r0.isMessageRequestAccepted
            org.thoughtcrime.securesms.conversation.ConversationMessage r4 = r0.inlineContent
            if (r1 != r4) goto L_0x009d
            r17 = 1
            goto L_0x009f
        L_0x009d:
            r17 = 0
        L_0x009f:
            org.thoughtcrime.securesms.conversation.colors.Colorizer r5 = r0.colorizer
            boolean r4 = r0.condensedMode
            r18 = r3
            r3 = r2
            r2 = r4
            r4 = r6
            r19 = r5
            r5 = r1
            r6 = r7
            r7 = r8
            r8 = r9
            r9 = r10
            r10 = r11
            r11 = r12
            r12 = r13
            r13 = r14
            r14 = r16
            r15 = r18
            r16 = r17
            r17 = r19
            r18 = r2
            r3.bind(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
            org.thoughtcrime.securesms.conversation.ConversationMessage r2 = r0.recordToPulse
            if (r1 != r2) goto L_0x00c7
            r1 = 0
            r0.recordToPulse = r1
        L_0x00c7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.conversation.ConversationAdapter.onBindViewHolder(androidx.recyclerview.widget.RecyclerView$ViewHolder, int):void");
    }

    @Override // androidx.recyclerview.widget.ListAdapter, androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return super.getItemCount() + (this.isTypingViewEnabled ? 1 : 0) + (this.footerView != null ? 1 : 0);
    }

    public void onViewRecycled(RecyclerView.ViewHolder viewHolder) {
        if (viewHolder instanceof ConversationViewHolder) {
            ((ConversationViewHolder) viewHolder).getBindable().unbind();
        }
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration.StickyHeaderAdapter
    public long getHeaderId(int i) {
        ConversationMessage item;
        if (isHeaderPosition(i) || isFooterPosition(i) || i >= getItemCount() || i < 0 || (item = getItem(i)) == null) {
            return -1;
        }
        this.calendar.setTimeInMillis(item.getMessageRecord().getDateSent());
        return (((long) this.calendar.get(1)) * 1000) + ((long) this.calendar.get(6));
    }

    public StickyHeaderViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i, int i2) {
        return new StickyHeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.conversation_item_header, viewGroup, false));
    }

    public void onBindHeaderViewHolder(StickyHeaderViewHolder stickyHeaderViewHolder, int i, int i2) {
        Context context = stickyHeaderViewHolder.itemView.getContext();
        ConversationMessage item = getItem(i);
        Objects.requireNonNull(item);
        stickyHeaderViewHolder.setText(DateUtils.getConversationDateHeaderString(stickyHeaderViewHolder.itemView.getContext(), this.locale, item.getMessageRecord().getDateSent()));
        if (i2 == 1) {
            if (this.hasWallpaper) {
                stickyHeaderViewHolder.setBackgroundRes(R.drawable.wallpaper_bubble_background_18);
            } else {
                stickyHeaderViewHolder.setBackgroundRes(R.drawable.sticky_date_header_background);
            }
        } else if (i2 == 2) {
            if (this.hasWallpaper) {
                stickyHeaderViewHolder.setBackgroundRes(R.drawable.wallpaper_bubble_background_18);
            } else {
                stickyHeaderViewHolder.clearBackground();
            }
        }
        if (!this.hasWallpaper || !ThemeUtil.isDarkTheme(context)) {
            stickyHeaderViewHolder.setTextColor(ContextCompat.getColor(context, R.color.signal_colorOnSurfaceVariant));
        } else {
            stickyHeaderViewHolder.setTextColor(ContextCompat.getColor(context, R.color.signal_colorNeutralInverse));
        }
    }

    @Override // androidx.recyclerview.widget.ListAdapter
    public ConversationMessage getItem(int i) {
        if (isTypingViewEnabled()) {
            i--;
        }
        if (i < 0) {
            return null;
        }
        PagingController pagingController = this.pagingController;
        if (pagingController != null) {
            pagingController.onDataNeededAroundIndex(i);
        }
        if (i < super.getItemCount()) {
            return (ConversationMessage) super.getItem(i);
        }
        Log.d(TAG, "Could not access corrected position " + i + " as it is out of bounds.");
        return null;
    }

    public void setPagingController(PagingController pagingController) {
        this.pagingController = pagingController;
    }

    public boolean isForRecipientId(RecipientId recipientId) {
        return this.recipient.getId().equals(recipientId);
    }

    public void onBindLastSeenViewHolder(StickyHeaderViewHolder stickyHeaderViewHolder, long j) {
        int i = (int) j;
        stickyHeaderViewHolder.setText(stickyHeaderViewHolder.itemView.getContext().getResources().getQuantityString(R.plurals.ConversationAdapter_n_unread_messages, i, Integer.valueOf(i)));
        if (this.hasWallpaper) {
            stickyHeaderViewHolder.setBackgroundRes(R.drawable.wallpaper_bubble_background_18);
            stickyHeaderViewHolder.setDividerColor(stickyHeaderViewHolder.itemView.getResources().getColor(R.color.transparent_black_80));
            return;
        }
        stickyHeaderViewHolder.clearBackground();
        stickyHeaderViewHolder.setDividerColor(stickyHeaderViewHolder.itemView.getResources().getColor(R.color.core_grey_45));
    }

    public boolean hasNoConversationMessages() {
        return super.getItemCount() == 0;
    }

    public int getAdapterPositionForMessagePosition(int i) {
        return isTypingViewEnabled() ? i + 1 : i;
    }

    public long getReceivedTimestamp(int i) {
        ConversationMessage item;
        if (!isHeaderPosition(i) && !isFooterPosition(i) && i < getItemCount() && i >= 0 && (item = getItem(i)) != null && !item.getMessageRecord().isOutgoing()) {
            return item.getMessageRecord().getDateReceived();
        }
        return 0;
    }

    public void setFooterView(View view) {
        boolean hasFooter = hasFooter();
        this.footerView = view;
        if (view == null && hasFooter) {
            notifyItemRemoved(getItemCount());
        } else if (view != null && hasFooter) {
            notifyItemChanged(getItemCount() - 1);
        } else if (view != null) {
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public void setTypingView(View view) {
        this.typingView = view;
    }

    public void setTypingViewEnabled(boolean z) {
        if (this.typingView != null || !z) {
            boolean z2 = this.isTypingViewEnabled;
            if (z2 && !z) {
                this.isTypingViewEnabled = false;
                notifyItemRemoved(0);
            } else if (z2) {
                notifyItemChanged(0);
            } else if (z) {
                this.isTypingViewEnabled = true;
                notifyItemInserted(0);
            }
        } else {
            throw new IllegalStateException("Must set header before enabling.");
        }
    }

    public void pulseAtPosition(int i) {
        if (i >= 0 && i < getItemCount()) {
            if (isHeaderPosition(i)) {
                i++;
            }
            this.recordToPulse = getItem(i);
            notifyItemChanged(i);
        }
    }

    public void onSearchQueryUpdated(String str) {
        if (!Objects.equals(str, this.searchQuery)) {
            this.searchQuery = str;
            notifyDataSetChanged();
        }
    }

    public boolean onHasWallpaperChanged(boolean z) {
        if (this.hasWallpaper == z) {
            return false;
        }
        Log.d(TAG, "Resetting adapter due to wallpaper change.");
        this.hasWallpaper = z;
        notifyDataSetChanged();
        return true;
    }

    public Set<MultiselectPart> getSelectedItems() {
        return new HashSet(this.selected);
    }

    public void removeFromSelection(Set<MultiselectPart> set) {
        this.selected.removeAll(set);
        updateSelected();
    }

    public void clearSelection() {
        this.selected.clear();
        updateSelected();
    }

    public void toggleSelection(MultiselectPart multiselectPart) {
        if (this.selected.contains(multiselectPart)) {
            this.selected.remove(multiselectPart);
        } else {
            this.selected.add(multiselectPart);
        }
        updateSelected();
    }

    private void updateSelected() {
        notifyItemRangeChanged(0, getItemCount(), 2);
    }

    public static void initializePool(RecyclerView.RecycledViewPool recycledViewPool) {
        recycledViewPool.setMaxRecycledViews(3, 25);
        recycledViewPool.setMaxRecycledViews(2, 15);
        recycledViewPool.setMaxRecycledViews(1, 25);
        recycledViewPool.setMaxRecycledViews(0, 15);
        recycledViewPool.setMaxRecycledViews(7, 15);
        recycledViewPool.setMaxRecycledViews(5, 1);
        recycledViewPool.setMaxRecycledViews(6, 1);
        recycledViewPool.setMaxRecycledViews(4, 5);
    }

    public boolean isTypingViewEnabled() {
        return this.isTypingViewEnabled;
    }

    public boolean hasFooter() {
        return this.footerView != null;
    }

    private boolean isHeaderPosition(int i) {
        return isTypingViewEnabled() && i == 0;
    }

    private boolean isFooterPosition(int i) {
        return hasFooter() && i == getItemCount() - 1;
    }

    private static int getLayoutForViewType(int i) {
        if (i == 0) {
            return R.layout.conversation_item_sent_multimedia;
        }
        if (i == 1) {
            return R.layout.conversation_item_sent_text_only;
        }
        if (i == 2) {
            return R.layout.conversation_item_received_multimedia;
        }
        if (i == 3) {
            return R.layout.conversation_item_received_text_only;
        }
        if (i == 4) {
            return R.layout.conversation_item_update;
        }
        throw new IllegalArgumentException("Unknown type!");
    }

    private static MessageDigest getMessageDigestOrThrow() {
        try {
            return MessageDigest.getInstance("SHA1");
        } catch (NoSuchAlgorithmException e) {
            throw new AssertionError(e);
        }
    }

    public ConversationMessage getLastVisibleConversationMessage(int i) {
        try {
            int i2 = 1;
            if (!hasFooter() || i != getItemCount() - 1) {
                i2 = 0;
            }
            return getItem(i - i2);
        } catch (IndexOutOfBoundsException e) {
            Log.w(TAG, "Race condition changed size of conversation", e);
            return null;
        }
    }

    public void setMessageRequestAccepted(boolean z) {
        if (this.isMessageRequestAccepted != z) {
            this.isMessageRequestAccepted = z;
            notifyDataSetChanged();
        }
    }

    public void playInlineContent(ConversationMessage conversationMessage) {
        if (this.inlineContent != conversationMessage) {
            this.inlineContent = conversationMessage;
            notifyDataSetChanged();
        }
    }

    public void updateTimestamps() {
        notifyItemRangeChanged(0, getItemCount(), 0);
    }

    /* loaded from: classes4.dex */
    static final class ConversationViewHolder extends RecyclerView.ViewHolder implements GiphyMp4Playable, Colorizable {
        public ConversationViewHolder(View view) {
            super(view);
        }

        public BindableConversationItem getBindable() {
            return (BindableConversationItem) this.itemView;
        }

        public void showProjectionArea() {
            getBindable().showProjectionArea();
        }

        public void hideProjectionArea() {
            getBindable().hideProjectionArea();
        }

        public MediaItem getMediaItem() {
            return getBindable().getMediaItem();
        }

        public GiphyMp4PlaybackPolicyEnforcer getPlaybackPolicyEnforcer() {
            return getBindable().getPlaybackPolicyEnforcer();
        }

        public Projection getGiphyMp4PlayableProjection(ViewGroup viewGroup) {
            return getBindable().getGiphyMp4PlayableProjection(viewGroup);
        }

        public boolean canPlayContent() {
            return getBindable().canPlayContent();
        }

        public boolean shouldProjectContent() {
            return getBindable().shouldProjectContent();
        }

        public ProjectionList getColorizerProjections(ViewGroup viewGroup) {
            return getBindable().getColorizerProjections(viewGroup);
        }
    }

    /* loaded from: classes4.dex */
    public static class StickyHeaderViewHolder extends RecyclerView.ViewHolder {
        View divider;
        TextView textView;

        public StickyHeaderViewHolder(View view) {
            super(view);
            this.textView = (TextView) view.findViewById(R.id.text);
            this.divider = view.findViewById(R.id.last_seen_divider);
        }

        public StickyHeaderViewHolder(TextView textView) {
            super(textView);
            this.textView = textView;
        }

        public void setText(CharSequence charSequence) {
            this.textView.setText(charSequence);
        }

        public void setTextColor(int i) {
            this.textView.setTextColor(i);
        }

        public void setBackgroundRes(int i) {
            this.textView.setBackgroundResource(i);
        }

        public void setDividerColor(int i) {
            View view = this.divider;
            if (view != null) {
                view.setBackgroundColor(i);
            }
        }

        public void clearBackground() {
            this.textView.setBackground(null);
        }
    }

    /* loaded from: classes4.dex */
    public static abstract class HeaderFooterViewHolder extends RecyclerView.ViewHolder {
        private ViewGroup container;

        HeaderFooterViewHolder(View view) {
            super(view);
            this.container = (ViewGroup) view;
        }

        void bind(View view) {
            unbind();
            if (view != null) {
                removeViewFromParent(view);
                this.container.addView(view);
            }
        }

        void unbind() {
            this.container.removeAllViews();
        }

        private void removeViewFromParent(View view) {
            if (view.getParent() != null) {
                ((ViewGroup) view.getParent()).removeView(view);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class FooterViewHolder extends HeaderFooterViewHolder {
        FooterViewHolder(View view) {
            super(view);
            setPaddingTop();
        }

        void bind(View view) {
            super.bind(view);
            setPaddingTop();
        }

        private void setPaddingTop() {
            if (Build.VERSION.SDK_INT <= 23) {
                int statusBarHeight = ViewUtil.getStatusBarHeight(this.itemView) + ((int) ThemeUtil.getThemedDimen(this.itemView.getContext(), 16843499));
                View view = this.itemView;
                ViewUtil.setPaddingTop(view, view.getPaddingTop() + statusBarHeight);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class HeaderViewHolder extends HeaderFooterViewHolder {
        HeaderViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    private static class PlaceholderViewHolder extends RecyclerView.ViewHolder {
        PlaceholderViewHolder(View view) {
            super(view);
        }
    }
}
