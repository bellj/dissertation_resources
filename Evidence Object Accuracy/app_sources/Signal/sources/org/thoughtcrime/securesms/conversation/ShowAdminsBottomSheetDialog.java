package org.thoughtcrime.securesms.conversation;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.function.Function;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.RecipientClickListener;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* loaded from: classes4.dex */
public final class ShowAdminsBottomSheetDialog extends BottomSheetDialogFragment {
    private static final String KEY_GROUP_ID;
    private final LifecycleDisposable disposables = new LifecycleDisposable();

    public static void show(FragmentManager fragmentManager, GroupId.V2 v2) {
        ShowAdminsBottomSheetDialog showAdminsBottomSheetDialog = new ShowAdminsBottomSheetDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("group_id", ParcelableGroupId.from(v2));
        showAdminsBottomSheetDialog.setArguments(bundle);
        showAdminsBottomSheetDialog.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, R.style.Signal_DayNight_BottomSheet_Rounded);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.show_admin_bottom_sheet, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.disposables.bindTo(getViewLifecycleOwner().getLifecycle());
        GroupMemberListView groupMemberListView = (GroupMemberListView) view.findViewById(R.id.show_admin_list);
        groupMemberListView.initializeAdapter(getViewLifecycleOwner());
        groupMemberListView.setDisplayOnlyMembers(Collections.emptyList());
        groupMemberListView.setRecipientClickListener(new RecipientClickListener() { // from class: org.thoughtcrime.securesms.conversation.ShowAdminsBottomSheetDialog$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.groups.ui.RecipientClickListener
            public final void onClick(Recipient recipient) {
                ShowAdminsBottomSheetDialog.this.lambda$onViewCreated$0(recipient);
            }
        });
        this.disposables.add(Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.conversation.ShowAdminsBottomSheetDialog$$ExternalSyntheticLambda2
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ShowAdminsBottomSheetDialog.this.lambda$onViewCreated$1();
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ShowAdminsBottomSheetDialog$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                GroupMemberListView.this.setDisplayOnlyMembers((List) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$onViewCreated$0(Recipient recipient) {
        CommunicationActions.startConversation(requireContext(), recipient, null);
        dismissAllowingStateLoss();
    }

    public /* synthetic */ List lambda$onViewCreated$1() throws Exception {
        return getAdmins(requireContext().getApplicationContext(), getGroupId());
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }

    private GroupId getGroupId() {
        return ParcelableGroupId.get((ParcelableGroupId) requireArguments().getParcelable("group_id"));
    }

    private static List<Recipient> getAdmins(Context context, GroupId groupId) {
        return (List) SignalDatabase.groups().getGroup(groupId).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ShowAdminsBottomSheetDialog$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).getAdmins();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(Collections.emptyList());
    }
}
