package org.thoughtcrime.securesms.conversation;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ValueAnimator;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ActionMode;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;
import androidx.core.text.HtmlCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewKt;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiConsumer;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.annimon.stream.function.Supplier;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.functions.Consumer;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.util.Pair;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.gifts.OpenableGift;
import org.thoughtcrime.securesms.badges.gifts.OpenableGiftItemDecoration;
import org.thoughtcrime.securesms.badges.gifts.viewgift.received.ViewReceivedGiftBottomSheet;
import org.thoughtcrime.securesms.badges.gifts.viewgift.sent.ViewSentGiftBottomSheet;
import org.thoughtcrime.securesms.blocked.BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.components.ConversationScrollToView;
import org.thoughtcrime.securesms.components.ConversationTypingView;
import org.thoughtcrime.securesms.components.TypingStatusRepository;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalBottomActionBar;
import org.thoughtcrime.securesms.components.recyclerview.SmoothScrollingLinearLayoutManager;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.voice.VoiceNoteMediaControllerOwner;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.contactshare.SharedContactDetailsActivity;
import org.thoughtcrime.securesms.conversation.ConversationAdapter;
import org.thoughtcrime.securesms.conversation.ConversationGroupViewModel;
import org.thoughtcrime.securesms.conversation.ConversationItemSwipeCallback;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.ConversationReactionOverlay;
import org.thoughtcrime.securesms.conversation.ConversationUpdateTick;
import org.thoughtcrime.securesms.conversation.ConversationViewModel;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.colors.RecyclerViewColorizer;
import org.thoughtcrime.securesms.conversation.mutiselect.ConversationItemAnimator;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectItemDecoration;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragmentArgs;
import org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet;
import org.thoughtcrime.securesms.conversation.ui.error.EnableCallNotificationSettingsDialog;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.InMemoryMessageRecord;
import org.thoughtcrime.securesms.database.model.MediaMmsMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.database.model.ReactionRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ItemDecoration;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackController;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4PlaybackPolicy;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionPlayerHolder;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4ProjectionRecycler;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupDescriptionDialog;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.v2.GroupBlockJoinRequestResult;
import org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil;
import org.thoughtcrime.securesms.jobs.DirectoryRefreshJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceViewOnceOpenJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.longmessage.LongMessageFragment;
import org.thoughtcrime.securesms.main.Material3OnScrollHelperBinder;
import org.thoughtcrime.securesms.messagedetails.MessageDetailsFragment;
import org.thoughtcrime.securesms.messagerequests.MessageRequestState;
import org.thoughtcrime.securesms.messagerequests.MessageRequestViewModel;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.TextSlide;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.ratelimit.RecaptchaProofBottomSheetFragment;
import org.thoughtcrime.securesms.reactions.ReactionsBottomSheetDialogFragment;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientExporter;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;
import org.thoughtcrime.securesms.revealable.ViewOnceMessageActivity;
import org.thoughtcrime.securesms.revealable.ViewOnceUtil;
import org.thoughtcrime.securesms.safety.SafetyNumberBottomSheet;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.stickers.StickerLocator;
import org.thoughtcrime.securesms.stickers.StickerPackPreviewActivity;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.CachedInflater;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.HtmlUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.RemoteDeleteUtil;
import org.thoughtcrime.securesms.util.SaveAttachmentTask;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.SignalProxyUtil;
import org.thoughtcrime.securesms.util.SnapToTopDataObserver;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.StorageUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.TopToastPopup;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.WindowUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;

/* loaded from: classes4.dex */
public class ConversationFragment extends LoggingFragment implements MultiselectForwardBottomSheet.Callback, MessageQuotesBottomSheet.Callback {
    private static final int CODE_ADD_EDIT_CONTACT;
    private static final int MAX_SCROLL_DELAY_COUNT;
    private static final int SCROLL_ANIMATION_THRESHOLD;
    private static final String TAG = Log.tag(ConversationFragment.class);
    private ActionMode actionMode;
    private final ActionModeCallback actionModeCallback = new ActionModeCallback();
    private SignalBottomActionBar bottomActionBar;
    private ViewSwitcher bottomLoadMoreView;
    private ChatWallpaper chatWallpaper;
    private Colorizer colorizer;
    private View composeDivider;
    private ConversationBannerView conversationBanner;
    private ConversationData conversationData;
    private RecyclerView.OnScrollListener conversationScrollListener;
    private ConversationUpdateTick conversationUpdateTick;
    private ConversationViewModel conversationViewModel;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private GiphyMp4ProjectionRecycler giphyMp4ProjectionRecycler;
    private ConversationGroupViewModel groupViewModel;
    private RecyclerView.ItemDecoration inlineDateDecoration;
    private LastSeenHeader lastSeenDecoration;
    private final LifecycleDisposable lastSeenDisposable = new LifecycleDisposable();
    private int lastSeenScrollOffset;
    private LayoutTransition layoutTransition;
    private RecyclerView list;
    private ConversationFragmentListener listener;
    private Locale locale;
    private MarkReadHelper markReadHelper;
    private Animation mentionButtonInAnimation;
    private Animation mentionButtonOutAnimation;
    private MessageCountsViewModel messageCountsViewModel;
    private MessageRequestViewModel messageRequestViewModel;
    private MultiselectItemDecoration multiselectItemDecoration;
    private OpenableGiftItemDecoration openableGiftItemDecoration;
    private View reactionsShade;
    private LiveRecipient recipient;
    private Animation scrollButtonInAnimation;
    private Animation scrollButtonOutAnimation;
    private TextView scrollDateHeader;
    private ConversationScrollToView scrollToBottomButton;
    private ConversationScrollToView scrollToMentionButton;
    private final ConversationAdapter.ItemClickListener selectionClickListener = new ConversationFragmentItemClickListener();
    private SnapToTopDataObserver snapToTopDataObserver;
    private Stopwatch startupStopwatch;
    private long threadId;
    private ViewSwitcher topLoadMoreView;
    private TransitionListener transitionListener;
    private ConversationTypingView typingView;
    private FrameLayout videoContainer;

    /* loaded from: classes4.dex */
    public interface ConversationFragmentListener extends VoiceNoteMediaControllerOwner {
        int getSendButtonTint();

        void handleReaction(ConversationMessage conversationMessage, ConversationReactionOverlay.OnActionSelectedListener onActionSelectedListener, SelectedConversationModel selectedConversationModel, ConversationReactionOverlay.OnHideListener onHideListener);

        void handleReplyMessage(ConversationMessage conversationMessage);

        boolean isAttachmentKeyboardOpen();

        boolean isKeyboardOpen();

        void onBottomActionBarVisibilityChanged(int i);

        void onCursorChanged();

        void onForwardClicked();

        void onMessageActionToolbarClosed();

        void onMessageActionToolbarOpened();

        void onMessageRequest(MessageRequestViewModel messageRequestViewModel);

        void onMessageWithErrorClicked(MessageRecord messageRecord);

        void onRegisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer);

        void onUnregisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer);

        void onVoiceNotePause(Uri uri);

        void onVoiceNotePlay(Uri uri, long j, double d);

        void onVoiceNotePlaybackSpeedChanged(Uri uri, float f);

        void onVoiceNoteResume(Uri uri, long j);

        void onVoiceNoteSeekTo(Uri uri, double d);

        void openAttachmentKeyboard();

        void setThreadId(long j);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$scrollToNextMention$65() {
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
    public Stories.MediaTransform.SendRequirements getStorySendRequirements() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
    public void onDismissForwardSheet() {
    }

    public static void prepare(Context context) {
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -2));
        CachedInflater.from(context).cacheUntilLimit(R.layout.conversation_item_received_text_only, frameLayout, 25);
        CachedInflater.from(context).cacheUntilLimit(R.layout.conversation_item_sent_text_only, frameLayout, 25);
        CachedInflater.from(context).cacheUntilLimit(R.layout.conversation_item_received_multimedia, frameLayout, 10);
        CachedInflater.from(context).cacheUntilLimit(R.layout.conversation_item_sent_multimedia, frameLayout, 10);
        CachedInflater.from(context).cacheUntilLimit(R.layout.conversation_item_update, frameLayout, 5);
        CachedInflater.from(context).cacheUntilLimit(R.layout.cursor_adapter_header_footer_view, frameLayout, 2);
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.locale = Locale.getDefault();
        this.startupStopwatch = new Stopwatch("conversation-open");
        SignalLocalMetrics.ConversationOpen.start();
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.disposables.bindTo(getViewLifecycleOwner());
        this.lastSeenDisposable.bindTo(getViewLifecycleOwner());
        View inflate = layoutInflater.inflate(R.layout.conversation_fragment, viewGroup, false);
        this.videoContainer = (FrameLayout) inflate.findViewById(R.id.video_container);
        this.list = (RecyclerView) inflate.findViewById(16908298);
        this.composeDivider = inflate.findViewById(R.id.compose_divider);
        this.layoutTransition = new LayoutTransition();
        this.transitionListener = new TransitionListener(this.list);
        this.scrollToBottomButton = (ConversationScrollToView) inflate.findViewById(R.id.scroll_to_bottom);
        this.scrollToMentionButton = (ConversationScrollToView) inflate.findViewById(R.id.scroll_to_mention);
        this.scrollDateHeader = (TextView) inflate.findViewById(R.id.scroll_date_header);
        this.reactionsShade = inflate.findViewById(R.id.reactions_shade);
        this.bottomActionBar = (SignalBottomActionBar) inflate.findViewById(R.id.conversation_bottom_action_bar);
        SmoothScrollingLinearLayoutManager smoothScrollingLinearLayoutManager = new SmoothScrollingLinearLayoutManager(getActivity(), true);
        ConversationItemAnimator conversationItemAnimator = new ConversationItemAnimator(new Function0() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda39
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return ConversationFragment.$r8$lambda$dIyPoDh0jNWvF9kGyCvKJo73OS8(ConversationFragment.this);
            }
        }, new Function0() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda50
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return ConversationFragment.m1378$r8$lambda$D28SoAgjjxjKu6fFnKWdwVpJ2I(ConversationFragment.this);
            }
        }, new Function0() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda51
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return ConversationFragment.$r8$lambda$smPwPwAL7xEyBocihvOPViSXGDc(ConversationFragment.this);
            }
        });
        this.multiselectItemDecoration = new MultiselectItemDecoration(requireContext(), new Function0() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda52
            @Override // kotlin.jvm.functions.Function0
            public final Object invoke() {
                return ConversationFragment.$r8$lambda$caZZc3em009jfAue9VPcB3JSZGI(ConversationFragment.this);
            }
        });
        this.list.setHasFixedSize(false);
        this.list.setLayoutManager(smoothScrollingLinearLayoutManager);
        RecyclerViewColorizer recyclerViewColorizer = new RecyclerViewColorizer(this.list);
        this.openableGiftItemDecoration = new OpenableGiftItemDecoration(requireContext());
        getViewLifecycleOwner().getLifecycle().addObserver(this.openableGiftItemDecoration);
        this.list.addItemDecoration(this.openableGiftItemDecoration);
        this.list.addItemDecoration(this.multiselectItemDecoration);
        this.list.setItemAnimator(conversationItemAnimator);
        ((Material3OnScrollHelperBinder) requireParentFragment()).bindScrollHelper(this.list);
        getViewLifecycleOwner().getLifecycle().addObserver(this.multiselectItemDecoration);
        this.snapToTopDataObserver = new ConversationSnapToTopDataObserver(this.list, new ConversationScrollRequestValidator());
        this.conversationBanner = (ConversationBannerView) layoutInflater.inflate(R.layout.conversation_item_banner, viewGroup, false);
        this.topLoadMoreView = (ViewSwitcher) layoutInflater.inflate(R.layout.load_more_header, viewGroup, false);
        this.bottomLoadMoreView = (ViewSwitcher) layoutInflater.inflate(R.layout.load_more_header, viewGroup, false);
        initializeLoadMoreView(this.topLoadMoreView);
        initializeLoadMoreView(this.bottomLoadMoreView);
        this.typingView = (ConversationTypingView) layoutInflater.inflate(R.layout.conversation_typing_view, viewGroup, false);
        new ConversationItemSwipeCallback(new ConversationItemSwipeCallback.SwipeAvailabilityProvider() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda53
            @Override // org.thoughtcrime.securesms.conversation.ConversationItemSwipeCallback.SwipeAvailabilityProvider
            public final boolean isSwipeAvailable(ConversationMessage conversationMessage) {
                return ConversationFragment.$r8$lambda$YDrZiLIzx2GNRaMo_jZwxXiS308(ConversationFragment.this, conversationMessage);
            }
        }, new ConversationItemSwipeCallback.OnSwipeListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda54
            @Override // org.thoughtcrime.securesms.conversation.ConversationItemSwipeCallback.OnSwipeListener
            public final void onSwipe(ConversationMessage conversationMessage) {
                ConversationFragment.$r8$lambda$DamX1KMuFUKDsBitb98rdkRwZY8(ConversationFragment.this, conversationMessage);
            }
        }).attachToRecyclerView(this.list);
        this.giphyMp4ProjectionRecycler = initializeGiphyMp4();
        this.groupViewModel = (ConversationGroupViewModel) new ViewModelProvider(getParentFragment(), new ConversationGroupViewModel.Factory()).get(ConversationGroupViewModel.class);
        this.messageCountsViewModel = (MessageCountsViewModel) new ViewModelProvider(getParentFragment()).get(MessageCountsViewModel.class);
        ConversationViewModel conversationViewModel = (ConversationViewModel) new ViewModelProvider(getParentFragment(), new ConversationViewModel.Factory()).get(ConversationViewModel.class);
        this.conversationViewModel = conversationViewModel;
        this.disposables.add(conversationViewModel.getChatColors().subscribe(new Consumer(recyclerViewColorizer) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda55
            public final /* synthetic */ RecyclerViewColorizer f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationFragment.m1379$r8$lambda$E_wO4oZPF_ayl3gY03YHGznXWs(ConversationFragment.this, this.f$1, (ChatColors) obj);
            }
        }));
        this.disposables.add(this.conversationViewModel.getMessageData().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda56
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationFragment.m1390$r8$lambda$lafupKjlZMpJqf2MEFP32TgRvQ(ConversationFragment.this, (ConversationViewModel.MessageData) obj);
            }
        }));
        this.disposables.add(this.conversationViewModel.getWallpaper().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda57
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationFragment.$r8$lambda$F4uQ8BUYv1DKJsYgvbwvKx8Ztfg(ConversationFragment.this, (Optional) obj);
            }
        }));
        this.conversationViewModel.getShowMentionsButton().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda58
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$jW775kACnvHBUUsenKibjKWPTLo(ConversationFragment.this, (Boolean) obj);
            }
        });
        this.conversationViewModel.getShowScrollToBottom().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda40
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$Yhhca6U2GCOvCqFa17coSDZ9GUw(ConversationFragment.this, (Boolean) obj);
            }
        });
        this.scrollToBottomButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda41
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationFragment.$r8$lambda$myEFSSTVpjsNFjdwa6F4iMYHW14(ConversationFragment.this, view);
            }
        });
        this.scrollToMentionButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda42
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationFragment.$r8$lambda$Ow1aCuirpItDGL8uGaGj0x0R25o(ConversationFragment.this, view);
            }
        });
        updateToolbarDependentMargins();
        this.colorizer = new Colorizer();
        this.disposables.add(this.conversationViewModel.getNameColorsMap().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda43
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationFragment.$r8$lambda$wVl6FJZNNYyQiW_qOjcxsrjiO1g(ConversationFragment.this, (Map) obj);
            }
        }));
        this.conversationUpdateTick = new ConversationUpdateTick(new ConversationUpdateTick.OnTickListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda44
            @Override // org.thoughtcrime.securesms.conversation.ConversationUpdateTick.OnTickListener
            public final void onTick() {
                ConversationFragment.$r8$lambda$zIKmwOx5DHarKDlflduEQNMmmS0(ConversationFragment.this);
            }
        });
        getViewLifecycleOwner().getLifecycle().addObserver(this.conversationUpdateTick);
        this.listener.getVoiceNoteMediaController().getVoiceNotePlayerViewState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda45
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$RfQdMYRHIYbYyOnQIX92s_c7mSs(ConversationFragment.this, (Optional) obj);
            }
        });
        this.conversationViewModel.getConversationTopMargin().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda46
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$7qdMsCAS4Z_xuCmzhkYm43MpuAI(ConversationFragment.this, (Integer) obj);
            }
        });
        this.conversationViewModel.getActiveNotificationProfile().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda47
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.m1373$r8$lambda$4obcuvzL2Os9lPl00gBCnPvfwY(ConversationFragment.this, (Optional) obj);
            }
        });
        initializeScrollButtonAnimations();
        initializeResources();
        initializeMessageRequestViewModel();
        initializeListAdapter();
        this.conversationViewModel.getSearchQuery().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda48
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$r__Ai5KnpZe3iI6DtnvM5q_thIM(ConversationFragment.this, (String) obj);
            }
        });
        this.disposables.add(this.conversationViewModel.getMarkReadRequests().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda49
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationFragment.m1375$r8$lambda$AMx_n3S3ieYyc4GOXS0b5poKI8(ConversationFragment.this, (Long) obj);
            }
        }));
        return inflate;
    }

    public /* synthetic */ Boolean lambda$onCreateView$0() {
        ConversationAdapter listAdapter = getListAdapter();
        if (listAdapter == null) {
            return Boolean.FALSE;
        }
        return Boolean.valueOf(Util.hasItems(listAdapter.getSelectedItems()));
    }

    public /* synthetic */ Boolean lambda$onCreateView$1() {
        return Boolean.valueOf(this.conversationViewModel.shouldPlayMessageAnimations() && this.list.getScrollState() == 0);
    }

    public /* synthetic */ Boolean lambda$onCreateView$2() {
        boolean z = true;
        if (!this.list.canScrollVertically(1) && !this.list.canScrollVertically(-1)) {
            z = false;
        }
        return Boolean.valueOf(z);
    }

    public /* synthetic */ ChatWallpaper lambda$onCreateView$3() {
        return this.chatWallpaper;
    }

    public /* synthetic */ boolean lambda$onCreateView$4(ConversationMessage conversationMessage) {
        return this.actionMode == null && MenuState.canReplyToMessage(this.recipient.get(), MenuState.isActionMessage(conversationMessage.getMessageRecord()), conversationMessage.getMessageRecord(), this.messageRequestViewModel.shouldShowMessageRequest(), this.groupViewModel.isNonAdminInAnnouncementGroup());
    }

    public /* synthetic */ void lambda$onCreateView$5(RecyclerViewColorizer recyclerViewColorizer, ChatColors chatColors) throws Throwable {
        recyclerViewColorizer.setChatColors(chatColors);
        this.scrollToMentionButton.setUnreadCountBackgroundTint(chatColors.asSingleColor());
        this.scrollToBottomButton.setUnreadCountBackgroundTint(chatColors.asSingleColor());
    }

    public /* synthetic */ void lambda$onCreateView$8(ConversationViewModel.MessageData messageData) throws Throwable {
        SignalLocalMetrics.ConversationOpen.onDataPostedToMain();
        if (getListAdapter() != null) {
            List<ConversationMessage> messages = messageData.getMessages();
            getListAdapter().submitList(messages, new Runnable(messages) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda17
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$RfhJnUg4jNcUXWdmtW3EmbgjTMY(ConversationFragment.this, this.f$1);
                }
            });
        }
        presentConversationMetadata(messageData.getMetadata());
    }

    public /* synthetic */ void lambda$onCreateView$7(List list) {
        this.list.post(new Runnable(list) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda63
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.$r8$lambda$GKIjlafTX_B8UFmBWISDvdnbhGE(ConversationFragment.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onCreateView$6(List list) {
        this.conversationViewModel.onMessagesCommitted(list);
    }

    public /* synthetic */ void lambda$onCreateView$9(Optional optional) throws Throwable {
        this.chatWallpaper = (ChatWallpaper) optional.orElse(null);
        this.scrollToBottomButton.setWallpaperEnabled(optional.isPresent());
        this.scrollToMentionButton.setWallpaperEnabled(optional.isPresent());
    }

    public /* synthetic */ void lambda$onCreateView$10(Boolean bool) {
        if (bool.booleanValue()) {
            ViewUtil.animateIn(this.scrollToMentionButton, this.mentionButtonInAnimation);
        } else {
            ViewUtil.animateOut(this.scrollToMentionButton, this.mentionButtonOutAnimation, 4);
        }
    }

    public /* synthetic */ void lambda$onCreateView$11(Boolean bool) {
        if (bool.booleanValue()) {
            ViewUtil.animateIn(this.scrollToBottomButton, this.scrollButtonInAnimation);
        } else {
            ViewUtil.animateOut(this.scrollToBottomButton, this.scrollButtonOutAnimation, 4);
        }
    }

    public /* synthetic */ void lambda$onCreateView$12(View view) {
        scrollToBottom();
    }

    public /* synthetic */ void lambda$onCreateView$13(View view) {
        scrollToNextMention();
    }

    public /* synthetic */ void lambda$onCreateView$14(Map map) throws Throwable {
        this.colorizer.onNameColorsChanged(map);
        ConversationAdapter listAdapter = getListAdapter();
        if (listAdapter != null) {
            listAdapter.notifyItemRangeChanged(0, listAdapter.getItemCount(), 1);
        }
    }

    public /* synthetic */ void lambda$onCreateView$15(Optional optional) {
        this.conversationViewModel.setInlinePlayerVisible(optional.isPresent());
    }

    public /* synthetic */ void lambda$onCreateView$16(Integer num) {
        this.lastSeenScrollOffset = num.intValue();
        ViewUtil.setTopMargin(this.scrollDateHeader, num.intValue() + ViewUtil.dpToPx(8));
    }

    public /* synthetic */ void lambda$onCreateView$17(Long l) throws Throwable {
        this.markReadHelper.onViewsRevealed(l.longValue());
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        getChildFragmentManager().setFragmentResultListener(ViewReceivedGiftBottomSheet.REQUEST_KEY, getViewLifecycleOwner(), new FragmentResultListener(view) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda19
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                ConversationFragment.$r8$lambda$tMdTyKAHMRyHaZqEQc2eg3l6EMM(this.f$0, str, bundle2);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$18(View view, String str, Bundle bundle) {
        if (bundle.getBoolean(ViewReceivedGiftBottomSheet.RESULT_NOT_NOW, false)) {
            Snackbar.make(view.getRootView(), (int) R.string.ConversationFragment__you_can_redeem_your_badge_later, -1).show();
        }
    }

    private GiphyMp4ProjectionRecycler initializeGiphyMp4() {
        int maxSimultaneousPlaybackInConversation = GiphyMp4PlaybackPolicy.maxSimultaneousPlaybackInConversation();
        GiphyMp4ProjectionRecycler giphyMp4ProjectionRecycler = new GiphyMp4ProjectionRecycler(GiphyMp4ProjectionPlayerHolder.injectVideoViews(requireContext(), getViewLifecycleOwner().getLifecycle(), this.videoContainer, maxSimultaneousPlaybackInConversation));
        GiphyMp4PlaybackController.attach(this.list, giphyMp4ProjectionRecycler, maxSimultaneousPlaybackInConversation);
        this.list.addItemDecoration(new GiphyMp4ItemDecoration(giphyMp4ProjectionRecycler, new Function1() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda21
            @Override // kotlin.jvm.functions.Function1
            public final Object invoke(Object obj) {
                return ConversationFragment.$r8$lambda$JclFLPkfrr7nmaUAh03rS_rxEQE(ConversationFragment.this, (Float) obj);
            }
        }), 0);
        return giphyMp4ProjectionRecycler;
    }

    public /* synthetic */ Unit lambda$initializeGiphyMp4$19(Float f) {
        this.reactionsShade.setTranslationY(f.floatValue() + ((float) this.list.getHeight()));
        return Unit.INSTANCE;
    }

    public void clearFocusedItem() {
        this.multiselectItemDecoration.setFocusedItem(null);
        this.list.invalidateItemDecorations();
    }

    public void updateConversationItemTimestamps() {
        if (getListAdapter() != null) {
            getListAdapter().updateTimestamps();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (ConversationFragmentListener) getParentFragment();
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        initializeTypingObserver();
        SignalProxyUtil.startListeningToWebsocket();
        this.layoutTransition.getAnimator(1).addListener(this.transitionListener);
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        ConversationMessage lastVisibleConversationMessage;
        super.onPause();
        int findLastVisibleItemPosition = getListLayoutManager().findLastVisibleItemPosition();
        long j = 0;
        if (!(getListLayoutManager().findFirstCompletelyVisibleItemPosition() <= 0 || findLastVisibleItemPosition == -1 || (lastVisibleConversationMessage = getListAdapter().getLastVisibleConversationMessage(findLastVisibleItemPosition)) == null)) {
            j = lastVisibleConversationMessage.getMessageRecord().getDateReceived();
        }
        SignalExecutors.BOUNDED.submit(new Runnable(j) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda16
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.m1372$r8$lambda$3Gc0MABGr9mKe1dNUlFnRRMS6E(ConversationFragment.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onPause$20(long j) {
        SignalDatabase.threads().setLastScrolled(this.threadId, j);
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStop() {
        super.onStop();
        ApplicationDependencies.getTypingStatusRepository().getTypists(this.threadId).removeObservers(getViewLifecycleOwner());
        this.layoutTransition.getAnimator(1).removeListener(this.transitionListener);
    }

    @Override // androidx.fragment.app.Fragment, android.content.ComponentCallbacks
    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        updateToolbarDependentMargins();
    }

    public void onNewIntent() {
        Log.d(TAG, "[onNewIntent]");
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
        long j = this.threadId;
        initializeResources();
        this.messageRequestViewModel.setConversationInfo(this.recipient.getId(), this.threadId);
        int startPosition = getStartPosition();
        if (startPosition == -1 || j != this.threadId) {
            initializeListAdapter();
        } else {
            this.list.post(new Runnable(startPosition) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda6
                public final /* synthetic */ int f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$hViIA4jp0VNKQ_4bvWSAFMbboNg(ConversationFragment.this, this.f$1);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$onNewIntent$21() {
        Log.w(TAG, "Could not scroll to requested message.");
    }

    public /* synthetic */ void lambda$onNewIntent$22(int i) {
        moveToPosition(i, new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda26
            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.$r8$lambda$NTyMwNehylqEPMEIXdGoX_C83lQ();
            }
        });
    }

    public void moveToLastSeen() {
        ConversationData conversationData = this.conversationData;
        int lastSeenPosition = conversationData != null ? conversationData.getLastSeenPosition() : 0;
        if (lastSeenPosition <= 0) {
            Log.i(TAG, "No need to move to last seen.");
        } else if (this.list == null || getListAdapter() == null) {
            Log.w(TAG, "Tried to move to last seen position, but we hadn't initialized the view yet.");
        } else {
            this.snapToTopDataObserver.requestScrollPosition(getListAdapter().getAdapterPositionForMessagePosition(lastSeenPosition));
        }
    }

    public void onWallpaperChanged(ChatWallpaper chatWallpaper) {
        ConversationAdapter listAdapter;
        TextView textView = this.scrollDateHeader;
        if (textView != null) {
            textView.setBackgroundResource(chatWallpaper != null ? R.drawable.sticky_date_header_background_wallpaper : R.drawable.sticky_date_header_background);
            this.scrollDateHeader.setTextColor(ContextCompat.getColor(requireContext(), chatWallpaper != null ? R.color.sticky_header_foreground_wallpaper : R.color.signal_colorOnSurfaceVariant));
        }
        if (this.list != null && (listAdapter = getListAdapter()) != null) {
            Log.d(TAG, "Notifying adapter that wallpaper state has changed.");
            if (listAdapter.onHasWallpaperChanged(chatWallpaper != null)) {
                setInlineDateDecoration(listAdapter);
            }
        }
    }

    private int getStartPosition() {
        return this.conversationViewModel.getArgs().getStartingPosition();
    }

    private void initializeMessageRequestViewModel() {
        MessageRequestViewModel messageRequestViewModel = (MessageRequestViewModel) new ViewModelProvider(requireParentFragment(), new MessageRequestViewModel.Factory(requireContext())).get(MessageRequestViewModel.class);
        this.messageRequestViewModel = messageRequestViewModel;
        messageRequestViewModel.setConversationInfo(this.recipient.getId(), this.threadId);
        this.listener.onMessageRequest(this.messageRequestViewModel);
        this.messageRequestViewModel.getRecipientInfo().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda33
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$MWk6cIfnjfyaWtgmBnDs7uwwNDM(ConversationFragment.this, (MessageRequestViewModel.RecipientInfo) obj);
            }
        });
        this.messageRequestViewModel.getMessageData().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda34
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.$r8$lambda$39qc9Rzmb9Gf794NKXdoDsCXlxw(ConversationFragment.this, (MessageRequestViewModel.MessageData) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeMessageRequestViewModel$23(MessageRequestViewModel.RecipientInfo recipientInfo) {
        presentMessageRequestProfileView(requireContext(), recipientInfo, this.conversationBanner);
    }

    public /* synthetic */ void lambda$initializeMessageRequestViewModel$24(MessageRequestViewModel.MessageData messageData) {
        ConversationAdapter listAdapter = getListAdapter();
        if (listAdapter != null) {
            listAdapter.setMessageRequestAccepted(messageData.getMessageState() == MessageRequestState.NONE);
        }
    }

    private void presentMessageRequestProfileView(Context context, MessageRequestViewModel.RecipientInfo recipientInfo, ConversationBannerView conversationBannerView) {
        String str;
        if (conversationBannerView != null) {
            Recipient recipient = recipientInfo.getRecipient();
            boolean equals = Recipient.self().equals(recipient);
            int groupMemberCount = recipientInfo.getGroupMemberCount();
            int groupPendingMemberCount = recipientInfo.getGroupPendingMemberCount();
            List<String> sharedGroups = recipientInfo.getSharedGroups();
            conversationBannerView.setBadge(recipient);
            boolean z = true;
            if (recipient != null) {
                conversationBannerView.setAvatar(GlideApp.with(context), recipient);
                conversationBannerView.showBackgroundBubble(recipient.hasWallpaper());
                String title = conversationBannerView.setTitle(recipient);
                conversationBannerView.setAbout(recipient);
                if (recipient.isGroup()) {
                    if (groupPendingMemberCount > 0) {
                        conversationBannerView.setSubtitle(context.getResources().getQuantityString(R.plurals.MessageRequestProfileView_members_and_invited, groupMemberCount, Integer.valueOf(groupMemberCount), context.getResources().getQuantityString(R.plurals.MessageRequestProfileView_invited, groupPendingMemberCount, Integer.valueOf(groupPendingMemberCount))));
                    } else if (groupMemberCount > 0) {
                        conversationBannerView.setSubtitle(context.getResources().getQuantityString(R.plurals.MessageRequestProfileView_members, groupMemberCount, Integer.valueOf(groupMemberCount)));
                    } else {
                        conversationBannerView.setSubtitle(null);
                    }
                } else if (equals) {
                    conversationBannerView.setSubtitle(context.getString(R.string.ConversationFragment__you_can_add_notes_for_yourself_in_this_conversation));
                } else {
                    String str2 = (String) recipient.getE164().map(new BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1()).orElse(null);
                    if (str2 == null || str2.equals(title)) {
                        conversationBannerView.hideSubtitle();
                    } else {
                        conversationBannerView.setSubtitle(str2);
                    }
                }
            }
            if (!sharedGroups.isEmpty() && !equals) {
                int size = sharedGroups.size();
                if (size == 1) {
                    str = context.getString(R.string.MessageRequestProfileView_member_of_one_group, HtmlUtil.bold(sharedGroups.get(0)));
                } else if (size == 2) {
                    str = context.getString(R.string.MessageRequestProfileView_member_of_two_groups, HtmlUtil.bold(sharedGroups.get(0)), HtmlUtil.bold(sharedGroups.get(1)));
                } else if (size != 3) {
                    int size2 = sharedGroups.size() - 2;
                    str = context.getString(R.string.MessageRequestProfileView_member_of_many_groups, HtmlUtil.bold(sharedGroups.get(0)), HtmlUtil.bold(sharedGroups.get(1)), context.getResources().getQuantityString(R.plurals.MessageRequestProfileView_member_of_d_additional_groups, size2, Integer.valueOf(size2)));
                } else {
                    str = context.getString(R.string.MessageRequestProfileView_member_of_many_groups, HtmlUtil.bold(sharedGroups.get(0)), HtmlUtil.bold(sharedGroups.get(1)), HtmlUtil.bold(sharedGroups.get(2)));
                }
                conversationBannerView.setDescription(HtmlCompat.fromHtml(str, 0));
                conversationBannerView.showDescription();
            } else if (TextUtils.isEmpty(recipientInfo.getGroupDescription())) {
                conversationBannerView.setLinkifyDescription(false);
                conversationBannerView.hideDescription();
            } else {
                conversationBannerView.setLinkifyDescription(true);
                if (recipientInfo.getMessageRequestState() != MessageRequestState.NONE) {
                    z = false;
                }
                conversationBannerView.showDescription();
                GroupDescriptionUtil.setText(context, conversationBannerView.getDescription(), recipientInfo.getGroupDescription(), z, new Runnable(recipient, context, recipientInfo, z) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda75
                    public final /* synthetic */ Recipient f$1;
                    public final /* synthetic */ Context f$2;
                    public final /* synthetic */ MessageRequestViewModel.RecipientInfo f$3;
                    public final /* synthetic */ boolean f$4;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                        this.f$3 = r4;
                        this.f$4 = r5;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$Q8loN8ne9fq7WH_sUb4UFUbMly4(ConversationFragment.this, this.f$1, this.f$2, this.f$3, this.f$4);
                    }
                });
            }
        }
    }

    public /* synthetic */ void lambda$presentMessageRequestProfileView$25(Recipient recipient, Context context, MessageRequestViewModel.RecipientInfo recipientInfo, boolean z) {
        GroupDescriptionDialog.show(getChildFragmentManager(), recipient.getDisplayName(context), recipientInfo.getGroupDescription(), z);
    }

    private void initializeResources() {
        long j = this.threadId;
        int startPosition = getStartPosition();
        this.recipient = Recipient.live(this.conversationViewModel.getArgs().getRecipientId());
        this.threadId = this.conversationViewModel.getArgs().getThreadId();
        this.markReadHelper = new MarkReadHelper(ConversationId.forConversation(this.threadId), requireContext(), getViewLifecycleOwner());
        this.conversationViewModel.onConversationDataAvailable(this.recipient.getId(), this.threadId, startPosition);
        this.messageCountsViewModel.setThreadId(this.threadId);
        LiveData<Integer> unreadMessagesCount = this.messageCountsViewModel.getUnreadMessagesCount();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        ConversationScrollToView conversationScrollToView = this.scrollToBottomButton;
        Objects.requireNonNull(conversationScrollToView);
        unreadMessagesCount.observe(viewLifecycleOwner, new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationScrollToView.this.setUnreadCount(((Integer) obj).intValue());
            }
        });
        this.messageCountsViewModel.getUnreadMentionsCount().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationFragment.m1389$r8$lambda$ks_o7GOLih1Tq6U0l7TugQNFk(ConversationFragment.this, (Integer) obj);
            }
        });
        ConversationScrollListener conversationScrollListener = new ConversationScrollListener(requireContext());
        this.conversationScrollListener = conversationScrollListener;
        this.list.addOnScrollListener(conversationScrollListener);
        if (j != this.threadId) {
            ApplicationDependencies.getTypingStatusRepository().getTypists(j).removeObservers(getViewLifecycleOwner());
        }
    }

    public /* synthetic */ void lambda$initializeResources$26(Integer num) {
        this.scrollToMentionButton.setUnreadCount(num.intValue());
        this.conversationViewModel.setHasUnreadMentions(num.intValue() > 0);
    }

    private void initializeListAdapter() {
        if (this.recipient == null) {
            return;
        }
        if (getListAdapter() == null || !getListAdapter().isForRecipientId(this.recipient.getId())) {
            String str = TAG;
            Log.d(str, "Initializing adapter for " + this.recipient.getId());
            final ConversationAdapter conversationAdapter = new ConversationAdapter(requireContext(), this, GlideApp.with(this), this.locale, this.selectionClickListener, this.recipient.get(), this.colorizer);
            conversationAdapter.setPagingController(this.conversationViewModel.getPagingController());
            this.list.setAdapter(conversationAdapter);
            setInlineDateDecoration(conversationAdapter);
            ConversationAdapter.initializePool(this.list.getRecycledViewPool());
            conversationAdapter.registerAdapterDataObserver(this.snapToTopDataObserver);
            conversationAdapter.registerAdapterDataObserver(new CheckExpirationDataObserver());
            ConversationData conversationData = this.conversationData;
            setLastSeen(conversationData != null ? conversationData.getLastSeen() : 0);
            conversationAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.1
                @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
                public void onItemRangeInserted(int i, int i2) {
                    conversationAdapter.unregisterAdapterDataObserver(this);
                    ConversationFragment.this.startupStopwatch.split("data-set");
                    ConversationFragment.this.list.post(new ConversationFragment$1$$ExternalSyntheticLambda0(this));
                }

                public /* synthetic */ void lambda$onItemRangeInserted$0() {
                    ConversationFragment.this.startupStopwatch.split("first-render");
                    ConversationFragment.this.startupStopwatch.stop(ConversationFragment.TAG);
                    SignalLocalMetrics.ConversationOpen.onRenderFinished();
                }
            });
            return;
        }
        String str2 = TAG;
        Log.d(str2, "List adapter already initialized for " + this.recipient.getId());
    }

    private void initializeLoadMoreView(ViewSwitcher viewSwitcher) {
        viewSwitcher.setOnClickListener(new View.OnClickListener(viewSwitcher) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda80
            public final /* synthetic */ ViewSwitcher f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationFragment.m1391$r8$lambda$skwXkSPTolYECi0rXzq9SiGcug(this.f$0, view);
            }
        });
    }

    public static /* synthetic */ void lambda$initializeLoadMoreView$27(ViewSwitcher viewSwitcher, View view) {
        viewSwitcher.showNext();
        viewSwitcher.setOnClickListener(null);
    }

    private void initializeTypingObserver() {
        if (TextSecurePreferences.isTypingIndicatorsEnabled(requireContext())) {
            LiveData<TypingStatusRepository.TypingState> typists = ApplicationDependencies.getTypingStatusRepository().getTypists(this.threadId);
            typists.removeObservers(getViewLifecycleOwner());
            typists.observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda65
                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    ConversationFragment.$r8$lambda$HtI4qfzzyTnAq9x3X4SRwa42L7s(ConversationFragment.this, (TypingStatusRepository.TypingState) obj);
                }
            });
        }
    }

    public /* synthetic */ void lambda$initializeTypingObserver$28(TypingStatusRepository.TypingState typingState) {
        boolean z;
        List<Recipient> list;
        if (typingState != null) {
            list = typingState.getTypists();
            z = typingState.isReplacedByIncomingMessage();
        } else {
            list = Collections.emptyList();
            z = false;
        }
        Recipient recipient = this.recipient.get();
        this.typingView.setTypists(GlideApp.with(this), list, recipient.isGroup(), recipient.hasWallpaper());
        ConversationAdapter listAdapter = getListAdapter();
        listAdapter.setTypingView(this.typingView);
        if (list.size() > 0) {
            if (isTypingIndicatorShowing() || !isAtBottom()) {
                listAdapter.setTypingViewEnabled(true);
                return;
            }
            listAdapter.setTypingViewEnabled(true);
            this.list.scrollToPosition(0);
        } else if (isTypingIndicatorShowing() && getListLayoutManager().findFirstCompletelyVisibleItemPosition() == 0 && getListLayoutManager().getItemCount() > 1 && !z) {
            listAdapter.setTypingViewEnabled(false);
        } else if (!z) {
            listAdapter.setTypingViewEnabled(false);
        } else {
            listAdapter.setTypingViewEnabled(false);
        }
    }

    public void setCorrectActionModeMenuVisibility() {
        Set<MultiselectPart> selectedItems = getListAdapter().getSelectedItems();
        if (this.actionMode == null || selectedItems.size() != 0) {
            setBottomActionBarVisibility(true);
            MenuState menuState = MenuState.getMenuState(this.recipient.get(), selectedItems, this.messageRequestViewModel.shouldShowMessageRequest(), this.groupViewModel.isNonAdminInAnnouncementGroup());
            ArrayList arrayList = new ArrayList();
            if (menuState.shouldShowReplyAction()) {
                arrayList.add(new ActionItem(R.drawable.ic_reply_24_tinted, getResources().getString(R.string.conversation_selection__menu_reply), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda66
                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$Rywx3WAKMfkCAKivMgBJrxF2yio(ConversationFragment.this);
                    }
                }));
            }
            if (menuState.shouldShowForwardAction()) {
                arrayList.add(new ActionItem(R.drawable.ic_forward_24_tinted, getResources().getString(R.string.conversation_selection__menu_forward), new Runnable(selectedItems) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda67
                    public final /* synthetic */ Set f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$hGsHQQ89MJwxOqpJ7N7I8KXGeZY(ConversationFragment.this, this.f$1);
                    }
                }));
            }
            if (menuState.shouldShowSaveAttachmentAction()) {
                arrayList.add(new ActionItem(R.drawable.ic_save_24_tinted, getResources().getString(R.string.conversation_selection__menu_save), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda68
                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$4T2gVPCbKHuk0yVDxPrOyUsekCY(ConversationFragment.this);
                    }
                }));
            }
            if (menuState.shouldShowCopyAction()) {
                arrayList.add(new ActionItem(R.drawable.ic_copy_24_tinted, getResources().getString(R.string.conversation_selection__menu_copy), new Runnable(selectedItems) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda69
                    public final /* synthetic */ Set f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$luLap7_cWmmJXDsuZE56DmDAMi0(ConversationFragment.this, this.f$1);
                    }
                }));
            }
            if (menuState.shouldShowDetailsAction()) {
                arrayList.add(new ActionItem(R.drawable.ic_info_tinted_24, getResources().getString(R.string.conversation_selection__menu_message_details), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda70
                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.m1381$r8$lambda$Nhbq19zj5KkLhJxcz7mx76bu_o(ConversationFragment.this);
                    }
                }));
            }
            if (menuState.shouldShowDeleteAction()) {
                arrayList.add(new ActionItem(R.drawable.ic_delete_tinted_24, getResources().getString(R.string.conversation_selection__menu_delete), new Runnable(selectedItems) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda71
                    public final /* synthetic */ Set f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$Pj3OfOem70Wc07fQ3fGpUjjvMZ0(ConversationFragment.this, this.f$1);
                    }
                }));
            }
            this.bottomActionBar.setItems(arrayList);
            return;
        }
        this.actionMode.finish();
    }

    public /* synthetic */ void lambda$setCorrectActionModeMenuVisibility$29() {
        maybeShowSwipeToReplyTooltip();
        handleReplyMessage(getSelectedConversationMessage());
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    public /* synthetic */ void lambda$setCorrectActionModeMenuVisibility$31() {
        handleSaveAttachment((MediaMmsMessageRecord) getSelectedConversationMessage().getMessageRecord());
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    public /* synthetic */ void lambda$setCorrectActionModeMenuVisibility$32(Set set) {
        handleCopyMessage(set);
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    public /* synthetic */ void lambda$setCorrectActionModeMenuVisibility$33() {
        handleDisplayDetails(getSelectedConversationMessage());
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    public /* synthetic */ void lambda$setCorrectActionModeMenuVisibility$34(Set set) {
        handleDeleteMessages(set);
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    public void setBottomActionBarVisibility(boolean z) {
        if (z != (this.bottomActionBar.getVisibility() == 0)) {
            final int pixels = (int) DimensionUnit.DP.toPixels(54.0f);
            if (z) {
                SignalBottomActionBar signalBottomActionBar = this.bottomActionBar;
                ViewUtil.animateIn(signalBottomActionBar, signalBottomActionBar.getEnterAnimation());
                this.listener.onBottomActionBarVisibilityChanged(0);
                this.bottomActionBar.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.2
                    @Override // android.view.ViewTreeObserver.OnPreDrawListener
                    public boolean onPreDraw() {
                        if (ConversationFragment.this.bottomActionBar.getHeight() == 0 && ConversationFragment.this.bottomActionBar.getVisibility() == 0) {
                            return false;
                        }
                        ConversationFragment.this.bottomActionBar.getViewTreeObserver().removeOnPreDrawListener(this);
                        int height = ConversationFragment.this.bottomActionBar.getHeight() + ((int) DimensionUnit.DP.toPixels(18.0f));
                        ConversationFragment.this.list.setPadding(ConversationFragment.this.list.getPaddingLeft(), ConversationFragment.this.list.getPaddingTop(), ConversationFragment.this.list.getPaddingRight(), height);
                        ConversationFragment.this.list.scrollBy(0, -(height - pixels));
                        return false;
                    }
                });
                return;
            }
            SignalBottomActionBar signalBottomActionBar2 = this.bottomActionBar;
            ViewUtil.animateOut(signalBottomActionBar2, signalBottomActionBar2.getExitAnimation()).addListener(new ListenableFuture.Listener<Boolean>() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.3
                @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
                public void onFailure(ExecutionException executionException) {
                }

                public void onSuccess(Boolean bool) {
                    int paddingBottom = ConversationFragment.this.list.getPaddingBottom() - pixels;
                    ConversationFragment.this.listener.onBottomActionBarVisibilityChanged(8);
                    ConversationFragment.this.list.setPadding(ConversationFragment.this.list.getPaddingLeft(), ConversationFragment.this.list.getPaddingTop(), ConversationFragment.this.list.getPaddingRight(), ConversationFragment.this.getResources().getDimensionPixelSize(R.dimen.conversation_bottom_padding));
                    ViewKt.doOnPreDraw(ConversationFragment.this.list, new ConversationFragment$3$$ExternalSyntheticLambda0(this, paddingBottom));
                }

                public /* synthetic */ Unit lambda$onSuccess$0(int i, View view) {
                    ConversationFragment.this.list.scrollBy(0, i);
                    return Unit.INSTANCE;
                }
            });
        }
    }

    public ConversationAdapter getListAdapter() {
        return (ConversationAdapter) this.list.getAdapter();
    }

    public SmoothScrollingLinearLayoutManager getListLayoutManager() {
        return (SmoothScrollingLinearLayoutManager) this.list.getLayoutManager();
    }

    private ConversationMessage getSelectedConversationMessage() {
        Set set = (Set) Stream.of(getListAdapter().getSelectedItems()).map(new ConversationFragment$$ExternalSyntheticLambda8()).distinct().collect(Collectors.toSet());
        if (set.size() == 1) {
            return (ConversationMessage) Collection$EL.stream(set).findFirst().get();
        }
        throw new AssertionError();
    }

    public void reload(Recipient recipient, long j) {
        String str = TAG;
        Log.d(str, "[reload] Recipient: " + recipient.getId() + ", ThreadId: " + j);
        this.recipient = recipient.live();
        if (this.threadId != j) {
            Log.i(str, "ThreadId changed from " + this.threadId + " to " + j + ". Recipient was " + this.recipient.getId() + " and is now " + recipient.getId());
            this.threadId = j;
            this.messageRequestViewModel.setConversationInfo(recipient.getId(), j);
            this.snapToTopDataObserver.requestScrollPosition(0);
            this.conversationViewModel.onConversationDataAvailable(recipient.getId(), j, -1);
            this.messageCountsViewModel.setThreadId(j);
            this.markReadHelper = new MarkReadHelper(ConversationId.forConversation(j), requireContext(), getViewLifecycleOwner());
            initializeListAdapter();
            initializeTypingObserver();
        }
    }

    public void scrollToBottom() {
        if (getListLayoutManager().findFirstVisibleItemPosition() < 50) {
            Log.d(TAG, "scrollToBottom: Smooth scrolling to bottom of screen.");
            this.list.smoothScrollToPosition(0);
            return;
        }
        Log.d(TAG, "scrollToBottom: Scrolling to bottom of screen.");
        this.list.scrollToPosition(0);
    }

    public void setInlineDateDecoration(ConversationAdapter conversationAdapter) {
        RecyclerView.ItemDecoration itemDecoration = this.inlineDateDecoration;
        if (itemDecoration != null) {
            this.list.removeItemDecoration(itemDecoration);
        }
        StickyHeaderDecoration stickyHeaderDecoration = new StickyHeaderDecoration(conversationAdapter, false, false, 2);
        this.inlineDateDecoration = stickyHeaderDecoration;
        this.list.addItemDecoration(stickyHeaderDecoration, 0);
    }

    public void setLastSeen(long j) {
        this.lastSeenDisposable.clear();
        LastSeenHeader lastSeenHeader = this.lastSeenDecoration;
        if (lastSeenHeader != null) {
            this.list.removeItemDecoration(lastSeenHeader);
        }
        LastSeenHeader lastSeenHeader2 = new LastSeenHeader(getListAdapter(), j);
        this.lastSeenDecoration = lastSeenHeader2;
        this.list.addItemDecoration(lastSeenHeader2, 0);
        if (j > 0) {
            this.lastSeenDisposable.add(this.conversationViewModel.getThreadUnreadCount().distinctUntilChanged().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda83
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    ConversationFragment.$r8$lambda$VBdGI_9kGILeydf3lep4sj8NgF0(ConversationFragment.this, (Integer) obj);
                }
            }));
        }
    }

    public /* synthetic */ void lambda$setLastSeen$35(Integer num) throws Throwable {
        this.lastSeenDecoration.setUnreadCount((long) num.intValue());
        this.list.invalidateItemDecorations();
    }

    public void handleCopyMessage(Set<MultiselectPart> set) {
        SimpleTask.run(new SimpleTask.BackgroundTask(set) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda81
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationFragment.$r8$lambda$BxTxsWQbHUkwmiO1XblhdNr3CzY(ConversationFragment.this, this.f$1);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda82
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationFragment.m1385$r8$lambda$enm_yDVPIndumkkNm4N7m8zn0(ConversationFragment.this, (CharSequence) obj);
            }
        });
    }

    public /* synthetic */ void lambda$handleCopyMessage$37(CharSequence charSequence) {
        if (!Util.isEmpty(charSequence)) {
            Util.copyToClipboard(requireContext(), charSequence);
        }
    }

    /* renamed from: extractBodies */
    public CharSequence lambda$handleCopyMessage$36(Set<MultiselectPart> set) {
        return (CharSequence) Stream.of(set).sortBy(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationFragment.$r8$lambda$vlrNTQPjYhdlOlQ1MG6yAG66xp8((MultiselectPart) obj);
            }
        }).map(new ConversationFragment$$ExternalSyntheticLambda8()).distinct().map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationFragment.$r8$lambda$Cbei5YbNBwyOjPyo7cdvVAvsPLM(ConversationFragment.this, (ConversationMessage) obj);
            }
        }).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda10
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return Util.isEmpty((SpannableString) obj);
            }
        }).collect(new Supplier() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Supplier
            public final Object get() {
                return new SpannableStringBuilder();
            }
        }, new BiConsumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.BiConsumer
            public final void accept(Object obj, Object obj2) {
                ConversationFragment.$r8$lambda$uf5dVaFR5vPAcrRZYvQns69Bs2Y((SpannableStringBuilder) obj, (SpannableString) obj2);
            }
        });
    }

    public static /* synthetic */ Long lambda$extractBodies$38(MultiselectPart multiselectPart) {
        return Long.valueOf(multiselectPart.getMessageRecord().getDateReceived());
    }

    public /* synthetic */ SpannableString lambda$extractBodies$39(ConversationMessage conversationMessage) {
        if (MessageRecordUtil.hasTextSlide(conversationMessage.getMessageRecord())) {
            TextSlide requireTextSlide = MessageRecordUtil.requireTextSlide(conversationMessage.getMessageRecord());
            if (requireTextSlide.getUri() == null) {
                return conversationMessage.getDisplayBody(requireContext());
            }
            try {
                InputStream attachmentStream = PartAuthority.getAttachmentStream(requireContext(), requireTextSlide.getUri());
                SpannableString displayBody = ConversationMessage.ConversationMessageFactory.createWithUnresolvedData(requireContext(), conversationMessage.getMessageRecord(), StreamUtil.readFullyAsString(attachmentStream)).getDisplayBody(requireContext());
                if (attachmentStream != null) {
                    attachmentStream.close();
                }
                return displayBody;
            } catch (IOException unused) {
                Log.w(TAG, "Failed to read text slide data.");
            }
        }
        return conversationMessage.getDisplayBody(requireContext());
    }

    public static /* synthetic */ void lambda$extractBodies$40(SpannableStringBuilder spannableStringBuilder, SpannableString spannableString) {
        if (spannableStringBuilder.length() > 0) {
            spannableStringBuilder.append('\n');
        }
        spannableStringBuilder.append((CharSequence) spannableString);
    }

    public void handleDeleteMessages(Set<MultiselectPart> set) {
        buildRemoteDeleteConfirmationDialog((Set) Stream.of(set).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda18
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((MultiselectPart) obj).getMessageRecord();
            }
        }).collect(Collectors.toSet())).show();
    }

    private AlertDialog.Builder buildRemoteDeleteConfirmationDialog(Set<MessageRecord> set) {
        requireActivity();
        int size = set.size();
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(getActivity());
        materialAlertDialogBuilder.setTitle((CharSequence) getActivity().getResources().getQuantityString(R.plurals.ConversationFragment_delete_selected_messages, size, Integer.valueOf(size)));
        materialAlertDialogBuilder.setCancelable(true);
        materialAlertDialogBuilder.setPositiveButton(R.string.ConversationFragment_delete_for_me, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(set) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda37
            public final /* synthetic */ Set f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ConversationFragment.$r8$lambda$20OEDFJTQS9nd_k1jXtfB9kWqns(ConversationFragment.this, this.f$1, dialogInterface, i);
            }
        });
        if (RemoteDeleteUtil.isValidSend(set, System.currentTimeMillis())) {
            materialAlertDialogBuilder.setNeutralButton(R.string.ConversationFragment_delete_for_everyone, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(set) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda38
                public final /* synthetic */ Set f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ConversationFragment.m1382$r8$lambda$St8AA0K8HCmGLQxTcUP2Ib_Xeg(ConversationFragment.this, this.f$1, dialogInterface, i);
                }
            });
        }
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        return materialAlertDialogBuilder;
    }

    public /* synthetic */ void lambda$buildRemoteDeleteConfirmationDialog$41(final Set set, DialogInterface dialogInterface, int i) {
        new ProgressDialogAsyncTask<Void, Void, Void>(getActivity(), R.string.ConversationFragment_deleting, R.string.ConversationFragment_deleting_messages) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.4
            public Void doInBackground(Void... voidArr) {
                boolean z;
                for (MessageRecord messageRecord : set) {
                    if (messageRecord.isMms()) {
                        z = SignalDatabase.mms().deleteMessage(messageRecord.getId());
                    } else {
                        z = SignalDatabase.sms().deleteMessage(messageRecord.getId());
                    }
                    if (z) {
                        ConversationFragment.this.threadId = -1;
                        ConversationFragment.this.conversationViewModel.clearThreadId();
                        ConversationFragment.this.messageCountsViewModel.clearThreadId();
                        ConversationFragment.this.listener.setThreadId(ConversationFragment.this.threadId);
                    }
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
    }

    public /* synthetic */ void lambda$buildRemoteDeleteConfirmationDialog$42(Set set, DialogInterface dialogInterface, int i) {
        handleDeleteForEveryone(set);
    }

    private void handleDeleteForEveryone(Set<MessageRecord> set) {
        ConversationFragment$$ExternalSyntheticLambda27 conversationFragment$$ExternalSyntheticLambda27 = new Runnable(set) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda27
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.$r8$lambda$JkULyZfSn1HWnqqZrvwXdl4ieIw(this.f$0);
            }
        };
        if (SignalStore.uiHints().hasConfirmedDeleteForEveryoneOnce()) {
            conversationFragment$$ExternalSyntheticLambda27.run();
        } else {
            new MaterialAlertDialogBuilder(requireActivity()).setMessage(R.string.ConversationFragment_this_message_will_be_deleted_for_everyone_in_the_conversation).setPositiveButton(R.string.ConversationFragment_delete_for_everyone, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(conversationFragment$$ExternalSyntheticLambda27) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda28
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ConversationFragment.m1386$r8$lambda$gixqD36p4v3IEswwsa91GinpNk(this.f$0, dialogInterface, i);
                }
            }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
        }
    }

    public static /* synthetic */ void lambda$handleDeleteForEveryone$44(Set set) {
        SignalExecutors.BOUNDED.execute(new Runnable(set) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda74
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.$r8$lambda$7HmraHY5DXnkXfNlYZiI1TGDEJc(this.f$0);
            }
        });
    }

    public static /* synthetic */ void lambda$handleDeleteForEveryone$43(Set set) {
        Iterator it = set.iterator();
        while (it.hasNext()) {
            MessageRecord messageRecord = (MessageRecord) it.next();
            MessageSender.sendRemoteDelete(messageRecord.getId(), messageRecord.isMms());
        }
    }

    public static /* synthetic */ void lambda$handleDeleteForEveryone$45(Runnable runnable, DialogInterface dialogInterface, int i) {
        SignalStore.uiHints().markHasConfirmedDeleteForEveryoneOnce();
        runnable.run();
    }

    public void handleDisplayDetails(ConversationMessage conversationMessage) {
        MessageDetailsFragment.create(conversationMessage.getMessageRecord(), this.recipient.getId()).show(getParentFragment().getChildFragmentManager(), (String) null);
    }

    /* renamed from: handleForwardMessageParts */
    public void lambda$setCorrectActionModeMenuVisibility$30(Set<MultiselectPart> set) {
        this.listener.onForwardClicked();
        MultiselectForwardFragmentArgs.create(requireContext(), set, new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda79
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ConversationFragment.m1374$r8$lambda$9cV_vxrmdlCdm8ZxDsLB8E_MgY(ConversationFragment.this, (MultiselectForwardFragmentArgs) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
    }

    public /* synthetic */ void lambda$handleForwardMessageParts$46(MultiselectForwardFragmentArgs multiselectForwardFragmentArgs) {
        MultiselectForwardFragment.showBottomSheet(getChildFragmentManager(), multiselectForwardFragmentArgs.withSendButtonTint(this.listener.getSendButtonTint()));
    }

    public void handleResendMessage(MessageRecord messageRecord) {
        final Context applicationContext = getActivity().getApplicationContext();
        new AsyncTask<MessageRecord, Void, Void>() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.5
            public Void doInBackground(MessageRecord... messageRecordArr) {
                MessageSender.resend(applicationContext, messageRecordArr[0]);
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, messageRecord);
    }

    public void handleReplyMessage(ConversationMessage conversationMessage) {
        this.listener.handleReplyMessage(conversationMessage);
    }

    public void handleSaveAttachment(MediaMmsMessageRecord mediaMmsMessageRecord) {
        if (!mediaMmsMessageRecord.isViewOnce()) {
            SaveAttachmentTask.showWarningDialog(getActivity(), new DialogInterface.OnClickListener(mediaMmsMessageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda62
                public final /* synthetic */ MediaMmsMessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    ConversationFragment.$r8$lambda$DEPG6zFQhjap24Y0vRqW_NZqqdA(ConversationFragment.this, this.f$1, dialogInterface, i);
                }
            });
            return;
        }
        throw new AssertionError("Cannot save a view-once message.");
    }

    public /* synthetic */ void lambda$handleSaveAttachment$49(MediaMmsMessageRecord mediaMmsMessageRecord, DialogInterface dialogInterface, int i) {
        if (StorageUtil.canWriteToMediaStore()) {
            lambda$handleSaveAttachment$48(mediaMmsMessageRecord);
        } else {
            Permissions.with(this).request("android.permission.WRITE_EXTERNAL_STORAGE").ifNecessary().withPermanentDenialDialog(getString(R.string.MediaPreviewActivity_signal_needs_the_storage_permission_in_order_to_write_to_external_storage_but_it_has_been_permanently_denied)).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda77
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.m1387$r8$lambda$hiVqMwSD84Xsf9xj8ot_BzUEc(ConversationFragment.this);
                }
            }).onAllGranted(new Runnable(mediaMmsMessageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda78
                public final /* synthetic */ MediaMmsMessageRecord f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$2NbbBJPwF0GtCTO16Ncp6bFKB6w(ConversationFragment.this, this.f$1);
                }
            }).execute();
        }
    }

    public /* synthetic */ void lambda$handleSaveAttachment$47() {
        Toast.makeText(requireContext(), (int) R.string.MediaPreviewActivity_unable_to_write_to_external_storage_without_permission, 1).show();
    }

    /* renamed from: performSave */
    public void lambda$handleSaveAttachment$48(MediaMmsMessageRecord mediaMmsMessageRecord) {
        List list = Stream.of(mediaMmsMessageRecord.getSlideDeck().getSlides()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda35
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ConversationFragment.m1376$r8$lambda$AUKJ8ILvf_dCWUpQAXYhWAhBvA((Slide) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda36
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ConversationFragment.m1392$r8$lambda$xsVM6VW0dBW0lEQ_YHiMjRLc9w(MediaMmsMessageRecord.this, (Slide) obj);
            }
        }).toList();
        if (!Util.isEmpty(list)) {
            new SaveAttachmentTask(getActivity()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (SaveAttachmentTask.Attachment[]) list.toArray(new SaveAttachmentTask.Attachment[0]));
            return;
        }
        Log.w(TAG, "No slide with attachable media found, failing nicely.");
        Toast.makeText(getActivity(), getResources().getQuantityString(R.plurals.ConversationFragment_error_while_saving_attachments_to_sd_card, 1), 1).show();
    }

    public static /* synthetic */ boolean lambda$performSave$50(Slide slide) {
        return slide.getUri() != null && (slide.hasImage() || slide.hasVideo() || slide.hasAudio() || slide.hasDocument());
    }

    public static /* synthetic */ SaveAttachmentTask.Attachment lambda$performSave$51(MediaMmsMessageRecord mediaMmsMessageRecord, Slide slide) {
        return new SaveAttachmentTask.Attachment(slide.getUri(), slide.getContentType(), mediaMmsMessageRecord.getDateSent(), slide.getFileName().orElse(null));
    }

    public long stageOutgoingMessage(OutgoingMediaMessage outgoingMediaMessage) {
        MessageRecord current = MmsDatabase.readerFor(outgoingMediaMessage, this.threadId).getCurrent();
        if (getListAdapter() != null) {
            setLastSeen(0);
            this.list.post(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda61
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$VswW40QRyaLf8qBfnLtD54RSde4(ConversationFragment.this);
                }
            });
        }
        return current.getId();
    }

    public /* synthetic */ void lambda$stageOutgoingMessage$52() {
        this.list.scrollToPosition(0);
    }

    public long stageOutgoingMessage(OutgoingTextMessage outgoingTextMessage, long j) {
        MessageRecord current = SmsDatabase.readerFor(outgoingTextMessage, this.threadId, j).getCurrent();
        if (getListAdapter() != null) {
            setLastSeen(0);
            this.list.post(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$l7AZ6iSHtK2CX230644BpxZ8X04(ConversationFragment.this);
                }
            });
        }
        return current.getId();
    }

    public /* synthetic */ void lambda$stageOutgoingMessage$53() {
        this.list.scrollToPosition(0);
    }

    private void presentConversationMetadata(ConversationData conversationData) {
        ConversationData conversationData2 = this.conversationData;
        if (conversationData2 == null || conversationData2.getThreadId() != conversationData.getThreadId()) {
            this.conversationData = conversationData;
            ConversationAdapter listAdapter = getListAdapter();
            if (listAdapter != null) {
                listAdapter.setFooterView(this.conversationBanner);
                ConversationFragment$$ExternalSyntheticLambda22 conversationFragment$$ExternalSyntheticLambda22 = new Runnable(conversationData, listAdapter) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda22
                    public final /* synthetic */ ConversationData f$1;
                    public final /* synthetic */ ConversationAdapter f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationFragment.$r8$lambda$GNgnBveD87DeSpa4B9VueFX8tvQ(ConversationFragment.this, this.f$1, this.f$2);
                    }
                };
                int adapterPositionForMessagePosition = listAdapter.getAdapterPositionForMessagePosition(conversationData.getLastSeenPosition());
                int adapterPositionForMessagePosition2 = listAdapter.getAdapterPositionForMessagePosition(conversationData.getLastScrolledPosition());
                if (conversationData.getThreadSize() == 0) {
                    conversationFragment$$ExternalSyntheticLambda22.run();
                } else if (conversationData.shouldJumpToMessage()) {
                    this.snapToTopDataObserver.buildScrollPosition(conversationData.getJumpToPosition()).withOnScrollRequestComplete(new Runnable(conversationFragment$$ExternalSyntheticLambda22, conversationData) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda23
                        public final /* synthetic */ Runnable f$1;
                        public final /* synthetic */ ConversationData f$2;

                        {
                            this.f$1 = r2;
                            this.f$2 = r3;
                        }

                        @Override // java.lang.Runnable
                        public final void run() {
                            ConversationFragment.$r8$lambda$jD2qGseN0iM2ybh0a1fccTQI8Ps(ConversationFragment.this, this.f$1, this.f$2);
                        }
                    }).submit();
                } else if (conversationData.getMessageRequestData().isMessageRequestAccepted()) {
                    SnapToTopDataObserver snapToTopDataObserver = this.snapToTopDataObserver;
                    if (!conversationData.shouldScrollToLastSeen()) {
                        adapterPositionForMessagePosition = adapterPositionForMessagePosition2;
                    }
                    snapToTopDataObserver.buildScrollPosition(adapterPositionForMessagePosition).withOnPerformScroll(new SnapToTopDataObserver.OnPerformScroll(conversationData) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda24
                        public final /* synthetic */ ConversationData f$1;

                        {
                            this.f$1 = r2;
                        }

                        @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.OnPerformScroll
                        public final void onPerformScroll(LinearLayoutManager linearLayoutManager, int i) {
                            ConversationFragment.$r8$lambda$xYhGA8rdC3CohbSzjtDA6ZtDa4o(ConversationFragment.this, this.f$1, linearLayoutManager, i);
                        }
                    }).withOnScrollRequestComplete(conversationFragment$$ExternalSyntheticLambda22).submit();
                } else {
                    this.snapToTopDataObserver.buildScrollPosition(listAdapter.getItemCount() - 1).withOnScrollRequestComplete(conversationFragment$$ExternalSyntheticLambda22).submit();
                }
            }
        } else {
            String str = TAG;
            Log.d(str, "Already presented conversation data for thread " + this.threadId);
        }
    }

    public /* synthetic */ void lambda$presentConversationMetadata$54(ConversationData conversationData, ConversationAdapter conversationAdapter) {
        if (!conversationData.getMessageRequestData().isMessageRequestAccepted()) {
            this.snapToTopDataObserver.requestScrollPosition(conversationAdapter.getItemCount() - 1);
        }
        setLastSeen(conversationData.getLastSeen());
        this.listener.onCursorChanged();
        this.conversationScrollListener.onScrolled(this.list, 0, 0);
    }

    public /* synthetic */ void lambda$presentConversationMetadata$55(Runnable runnable, ConversationData conversationData) {
        runnable.run();
        getListAdapter().pulseAtPosition(conversationData.getJumpToPosition());
    }

    public /* synthetic */ void lambda$presentConversationMetadata$56(ConversationData conversationData, LinearLayoutManager linearLayoutManager, int i) {
        scrollToLastSeenIfNecessary(conversationData, linearLayoutManager, i, 0);
    }

    private void scrollToLastSeenIfNecessary(ConversationData conversationData, LinearLayoutManager linearLayoutManager, int i, int i2) {
        if (getView() == null) {
            Log.w(TAG, "[scrollToLastSeenIfNecessary] No view! Skipping.");
        } else if (i2 >= 5 || !(this.list.getHeight() == 0 || this.lastSeenScrollOffset == 0)) {
            if (i2 >= 5) {
                Log.w(TAG, "[scrollToLastSeeenIfNecessary] Hit maximum call count! Doing default behavior.");
            }
            linearLayoutManager.scrollToPositionWithOffset(i, this.list.getHeight() - (conversationData.shouldScrollToLastSeen() ? this.lastSeenScrollOffset : 0));
        } else {
            Log.w(TAG, "[scrollToLastSeenIfNecessary] List height or scroll offsets not available yet. Delaying jumping to last seen.");
            requireView().post(new Runnable(conversationData, linearLayoutManager, i, i2) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda13
                public final /* synthetic */ ConversationData f$1;
                public final /* synthetic */ LinearLayoutManager f$2;
                public final /* synthetic */ int f$3;
                public final /* synthetic */ int f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                    this.f$4 = r5;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$X1EXBvj9hPrVuGpoMthZnqJSAJY(ConversationFragment.this, this.f$1, this.f$2, this.f$3, this.f$4);
                }
            });
        }
    }

    public /* synthetic */ void lambda$scrollToLastSeenIfNecessary$57(ConversationData conversationData, LinearLayoutManager linearLayoutManager, int i, int i2) {
        scrollToLastSeenIfNecessary(conversationData, linearLayoutManager, i, i2 + 1);
    }

    public void updateNotificationProfileStatus(Optional<NotificationProfile> optional) {
        if (optional.isPresent() && optional.get().getId() != SignalStore.notificationProfileValues().getLastProfilePopup()) {
            requireView().postDelayed(new Runnable(optional) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda76
                public final /* synthetic */ Optional f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.m1384$r8$lambda$d3HHbeKfeHLU48qUNllchq0sqw(ConversationFragment.this, this.f$1);
                }
            }, 500);
        }
    }

    public /* synthetic */ void lambda$updateNotificationProfileStatus$58(Optional optional) {
        SignalStore.notificationProfileValues().setLastProfilePopup(((NotificationProfile) optional.get()).getId());
        SignalStore.notificationProfileValues().setLastProfilePopupTime(System.currentTimeMillis());
        TopToastPopup.show((ViewGroup) requireView(), R.drawable.ic_moon_16, getString(R.string.ConversationFragment__s_on, ((NotificationProfile) optional.get()).getName()));
    }

    private boolean isAtBottom() {
        if (this.list.getChildCount() == 0) {
            return true;
        }
        int findFirstVisibleItemPosition = getListLayoutManager().findFirstVisibleItemPosition();
        if (isTypingIndicatorShowing()) {
            RecyclerView.ViewHolder findViewHolderForAdapterPosition = this.list.findViewHolderForAdapterPosition(1);
            if (findFirstVisibleItemPosition > 1 || findViewHolderForAdapterPosition == null || findViewHolderForAdapterPosition.itemView.getBottom() > this.list.getHeight()) {
                return false;
            }
            return true;
        } else if (findFirstVisibleItemPosition != 0 || this.list.getChildAt(0).getBottom() > this.list.getHeight()) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isTypingIndicatorShowing() {
        return getListAdapter().isTypingViewEnabled();
    }

    public void onSearchQueryUpdated(String str) {
        if (getListAdapter() != null) {
            getListAdapter().onSearchQueryUpdated(str);
        }
    }

    public Colorizer getColorizer() {
        Colorizer colorizer = this.colorizer;
        Objects.requireNonNull(colorizer);
        return colorizer;
    }

    public void jumpToMessage(RecipientId recipientId, long j, Runnable runnable) {
        SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(j, recipientId) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda59
            public final /* synthetic */ long f$1;
            public final /* synthetic */ RecipientId f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationFragment.m1377$r8$lambda$BxGd5YNJGTM2OC_kMltAcf8z3M(ConversationFragment.this, this.f$1, this.f$2);
            }
        }, new SimpleTask.ForegroundTask(runnable) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda60
            public final /* synthetic */ Runnable f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationFragment.m1388$r8$lambda$i2OiIleQyv6Gc09B0YfqLC20k4(ConversationFragment.this, this.f$1, (Integer) obj);
            }
        });
    }

    public /* synthetic */ Integer lambda$jumpToMessage$59(long j, RecipientId recipientId) {
        return Integer.valueOf(SignalDatabase.mmsSms().getMessagePositionInConversation(this.threadId, j, recipientId));
    }

    public /* synthetic */ void lambda$jumpToMessage$60(Runnable runnable, Integer num) {
        moveToPosition(num.intValue() + (isTypingIndicatorShowing() ? 1 : 0), runnable);
    }

    public void moveToPosition(int i, Runnable runnable) {
        String str = TAG;
        Log.d(str, "moveToPosition(" + i + ")");
        this.conversationViewModel.getPagingController().onDataNeededAroundIndex(i);
        this.snapToTopDataObserver.buildScrollPosition(i).withOnPerformScroll(new SnapToTopDataObserver.OnPerformScroll(i) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda72
            public final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.OnPerformScroll
            public final void onPerformScroll(LinearLayoutManager linearLayoutManager, int i2) {
                ConversationFragment.$r8$lambda$IkrmBMe1lZ3fHfB2Tb_U9gSJUw8(ConversationFragment.this, this.f$1, linearLayoutManager, i2);
            }
        }).withOnInvalidPosition(new Runnable(runnable) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda73
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.m1383$r8$lambda$Wf_v5uvkV9nzDwzIexWxmYNFYA(this.f$0);
            }
        }).submit();
    }

    public /* synthetic */ void lambda$moveToPosition$62(int i, LinearLayoutManager linearLayoutManager, int i2) {
        this.list.post(new Runnable(linearLayoutManager, i2, i) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda20
            public final /* synthetic */ LinearLayoutManager f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ int f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.$r8$lambda$PnpkzdoEk1HyqaIcjVqVRH_vKt4(ConversationFragment.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public /* synthetic */ void lambda$moveToPosition$61(LinearLayoutManager linearLayoutManager, int i, int i2) {
        if (Math.abs(linearLayoutManager.findFirstVisibleItemPosition() - i) < 50) {
            View findViewByPosition = linearLayoutManager.findViewByPosition(i2);
            if (findViewByPosition == null || !linearLayoutManager.isViewPartiallyVisible(findViewByPosition, true, false)) {
                linearLayoutManager.scrollToPositionWithOffset(i, this.list.getHeight() / 4);
            }
        } else {
            linearLayoutManager.scrollToPositionWithOffset(i, this.list.getHeight() / 4);
        }
        getListAdapter().pulseAtPosition(i2);
    }

    public static /* synthetic */ void lambda$moveToPosition$63(Runnable runnable) {
        if (runnable != null) {
            runnable.run();
        }
        Log.w(TAG, "[moveToMentionPosition] Tried to navigate to mention, but it wasn't found.");
    }

    private void maybeShowSwipeToReplyTooltip() {
        if (!TextSecurePreferences.hasSeenSwipeToReplyTooltip(requireContext())) {
            Snackbar.make(this.list, ViewUtil.isLtr(requireContext()) ? R.string.ConversationFragment_you_can_swipe_to_the_right_reply : R.string.ConversationFragment_you_can_swipe_to_the_left_reply, 0).show();
            TextSecurePreferences.setHasSeenSwipeToReplyTooltip(requireContext(), true);
        }
    }

    private void initializeScrollButtonAnimations() {
        this.scrollButtonInAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_scale_in);
        this.scrollButtonOutAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_scale_out);
        this.mentionButtonInAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_scale_in);
        this.mentionButtonOutAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_scale_out);
        this.scrollButtonInAnimation.setDuration(100);
        this.scrollButtonOutAnimation.setDuration(50);
        this.mentionButtonInAnimation.setDuration(100);
        this.mentionButtonOutAnimation.setDuration(50);
    }

    private void scrollToNextMention() {
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda29
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationFragment.m1371$r8$lambda$1H2YIYiFGotp5QQq_QYXPpYDrk(ConversationFragment.this);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda30
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationFragment.$r8$lambda$rQbSQaXY_Gg9L7QImZPCLCtbEUI(ConversationFragment.this, (Pair) obj);
            }
        });
    }

    public /* synthetic */ Pair lambda$scrollToNextMention$64() {
        return SignalDatabase.mms().getOldestUnreadMentionDetails(this.threadId);
    }

    public /* synthetic */ void lambda$scrollToNextMention$66(Pair pair) {
        if (pair != null) {
            jumpToMessage((RecipientId) pair.first(), ((Long) pair.second()).longValue(), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda15
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationFragment.$r8$lambda$QQ4J0xXMqIlF2l7TaE7hzSaTNJo();
                }
            });
        }
    }

    public void postMarkAsReadRequest() {
        int findFirstVisibleItemPosition;
        if (!getListAdapter().hasNoConversationMessages() && (findFirstVisibleItemPosition = getListLayoutManager().findFirstVisibleItemPosition()) != getListAdapter().getItemCount() - 1) {
            ConversationMessage item = getListAdapter().getItem(findFirstVisibleItemPosition);
            if (item == null) {
                item = getListAdapter().getItem(findFirstVisibleItemPosition + 1);
            }
            if (item != null) {
                MessageRecord messageRecord = item.getMessageRecord();
                this.conversationViewModel.submitMarkReadRequest(Math.max(messageRecord.getDateReceived(), ((Long) Stream.of(messageRecord.getReactions()).map(new Function() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda31
                    @Override // com.annimon.stream.function.Function
                    public final Object apply(Object obj) {
                        return Long.valueOf(((ReactionRecord) obj).getDateReceived());
                    }
                }).max(new Comparator() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda32
                    @Override // java.util.Comparator
                    public final int compare(Object obj, Object obj2) {
                        return ((Long) obj).compareTo((Long) obj2);
                    }
                }).orElse(0L)).longValue()));
            }
        }
    }

    private void updateToolbarDependentMargins() {
        final Toolbar toolbar = (Toolbar) requireActivity().findViewById(R.id.toolbar);
        toolbar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.6
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public void onGlobalLayout() {
                Rect rect = new Rect();
                toolbar.getGlobalVisibleRect(rect);
                ConversationFragment.this.conversationViewModel.setToolbarBottom(rect.bottom);
                ViewUtil.setTopMargin(ConversationFragment.this.conversationBanner, rect.bottom + ViewUtil.dpToPx(16));
                toolbar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
    }

    public String calculateSelectedItemCount() {
        ConversationAdapter listAdapter = getListAdapter();
        int count = (listAdapter == null || listAdapter.getSelectedItems().isEmpty()) ? 0 : (int) Collection$EL.stream(listAdapter.getSelectedItems()).map(new ConversationFragment$$ExternalSyntheticLambda14()).distinct().count();
        return requireContext().getResources().getQuantityString(R.plurals.conversation_context__s_selected, count, Integer.valueOf(count));
    }

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardBottomSheet.Callback
    public void onFinishForwardAction() {
        ActionMode actionMode = this.actionMode;
        if (actionMode != null) {
            actionMode.finish();
        }
    }

    @Override // org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback
    public ConversationAdapter.ItemClickListener getConversationAdapterListener() {
        return this.selectionClickListener;
    }

    @Override // org.thoughtcrime.securesms.conversation.quotes.MessageQuotesBottomSheet.Callback
    public void jumpToMessage(MessageRecord messageRecord) {
        SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(messageRecord) { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ MessageRecord f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ConversationFragment.$r8$lambda$4_RtOOxKySctqX6w13RHqpdlxmA(ConversationFragment.this, this.f$1);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ConversationFragment.$r8$lambda$h4Jx4pGDGPXxryubKsIz39JPd94(ConversationFragment.this, (Integer) obj);
            }
        });
    }

    public /* synthetic */ Integer lambda$jumpToMessage$67(MessageRecord messageRecord) {
        return Integer.valueOf(SignalDatabase.mmsSms().getMessagePositionInConversation(this.threadId, messageRecord.getDateReceived(), (messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getRecipient()).getId()));
    }

    public /* synthetic */ void lambda$jumpToMessage$69(Integer num) {
        moveToPosition(num.intValue() + (isTypingIndicatorShowing() ? 1 : 0), new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda25
            @Override // java.lang.Runnable
            public final void run() {
                ConversationFragment.$r8$lambda$_mTkEHcNgmE7wsC6_7Ngjvtv_Yc(ConversationFragment.this);
            }
        });
    }

    public /* synthetic */ void lambda$jumpToMessage$68() {
        Toast.makeText(getContext(), (int) R.string.ConversationFragment_failed_to_open_message, 0).show();
    }

    /* loaded from: classes4.dex */
    public class ConversationScrollListener extends RecyclerView.OnScrollListener {
        private final ConversationDateHeader conversationDateHeader;
        private long lastPositionId = -1;
        private boolean wasAtBottom = true;

        ConversationScrollListener(Context context) {
            ConversationFragment.this = r3;
            this.conversationDateHeader = new ConversationDateHeader(context, r3.scrollDateHeader);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrolled(RecyclerView recyclerView, int i, int i2) {
            boolean z = !recyclerView.canScrollVertically(1);
            boolean isAtZoomScrollHeight = isAtZoomScrollHeight();
            int headerPositionId = getHeaderPositionId();
            if (z && !this.wasAtBottom) {
                ViewUtil.fadeOut(ConversationFragment.this.composeDivider, 50, 4);
            } else if (!z && this.wasAtBottom) {
                ViewUtil.fadeIn(ConversationFragment.this.composeDivider, 500);
            }
            if (z) {
                ConversationFragment.this.conversationViewModel.setShowScrollButtons(false);
            } else if (isAtZoomScrollHeight) {
                ConversationFragment.this.conversationViewModel.setShowScrollButtons(true);
            }
            long j = (long) headerPositionId;
            if (j != this.lastPositionId) {
                bindScrollHeader(this.conversationDateHeader, headerPositionId);
            }
            this.wasAtBottom = z;
            this.lastPositionId = j;
            ConversationFragment.this.postMarkAsReadRequest();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
        public void onScrollStateChanged(RecyclerView recyclerView, int i) {
            if (i == 1) {
                this.conversationDateHeader.show();
            } else if (i == 0) {
                this.conversationDateHeader.hide();
            }
        }

        private boolean isAtZoomScrollHeight() {
            return ConversationFragment.this.getListLayoutManager().findFirstCompletelyVisibleItemPosition() > 4;
        }

        private int getHeaderPositionId() {
            return ConversationFragment.this.getListLayoutManager().findLastVisibleItemPosition();
        }

        private void bindScrollHeader(ConversationAdapter.StickyHeaderViewHolder stickyHeaderViewHolder, int i) {
            if (((ConversationAdapter) ConversationFragment.this.list.getAdapter()).getHeaderId(i) != -1) {
                ((ConversationAdapter) ConversationFragment.this.list.getAdapter()).onBindHeaderViewHolder(stickyHeaderViewHolder, i, 1);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class ConversationFragmentItemClickListener implements ConversationAdapter.ItemClickListener {
        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onCallToAction(String str) {
        }

        private ConversationFragmentItemClickListener() {
            ConversationFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.conversation.ConversationAdapter.ItemClickListener
        public void onItemClick(MultiselectPart multiselectPart) {
            if (ConversationFragment.this.actionMode != null) {
                ((ConversationAdapter) ConversationFragment.this.list.getAdapter()).toggleSelection(multiselectPart);
                ConversationFragment.this.list.invalidateItemDecorations();
                if (ConversationFragment.this.getListAdapter().getSelectedItems().size() == 0) {
                    ConversationFragment.this.actionMode.finish();
                    return;
                }
                ConversationFragment.this.setCorrectActionModeMenuVisibility();
                ConversationFragment.this.actionMode.setTitle(ConversationFragment.this.calculateSelectedItemCount());
            }
        }

        @Override // org.thoughtcrime.securesms.conversation.ConversationAdapter.ItemClickListener
        public void onItemLongClick(View view, MultiselectPart multiselectPart) {
            ConversationFragmentItemClickListener conversationFragmentItemClickListener;
            Bitmap bitmap;
            final GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder;
            View view2;
            if (ConversationFragment.this.actionMode == null) {
                final MessageRecord messageRecord = multiselectPart.getConversationMessage().getMessageRecord();
                if (!ConversationFragment.this.isUnopenedGift(view, messageRecord)) {
                    if (messageRecord.isSecure() && !messageRecord.isRemoteDelete() && !messageRecord.isUpdate() && !ConversationFragment.this.recipient.get().isBlocked() && !ConversationFragment.this.messageRequestViewModel.shouldShowMessageRequest()) {
                        if (ConversationFragment.this.recipient.get().isGroup() && !ConversationFragment.this.recipient.get().isActiveGroup()) {
                            conversationFragmentItemClickListener = this;
                            ConversationFragment.this.clearFocusedItem();
                            ((ConversationAdapter) ConversationFragment.this.list.getAdapter()).toggleSelection(multiselectPart);
                            ConversationFragment.this.list.invalidateItemDecorations();
                            ConversationFragment conversationFragment = ConversationFragment.this;
                            conversationFragment.actionMode = ((AppCompatActivity) conversationFragment.getActivity()).startSupportActionMode(ConversationFragment.this.actionModeCallback);
                        } else if (((ConversationAdapter) ConversationFragment.this.list.getAdapter()).getSelectedItems().isEmpty()) {
                            ConversationFragment.this.multiselectItemDecoration.setFocusedItem(new MultiselectPart.Message(multiselectPart.getConversationMessage()));
                            ConversationFragment.this.list.invalidateItemDecorations();
                            ConversationFragment.this.reactionsShade.setVisibility(0);
                            ConversationFragment.this.list.setLayoutFrozen(true);
                            if (view instanceof ConversationItem) {
                                Uri audioUriForLongClick = getAudioUriForLongClick(messageRecord);
                                if (audioUriForLongClick != null) {
                                    ConversationFragment.this.listener.onVoiceNotePause(audioUriForLongClick);
                                }
                                int childAdapterPosition = ConversationFragment.this.list.getChildAdapterPosition(view);
                                if (childAdapterPosition == -1 || (giphyMp4ProjectionPlayerHolder = ConversationFragment.this.giphyMp4ProjectionRecycler.getCurrentHolder(childAdapterPosition)) == null || !giphyMp4ProjectionPlayerHolder.isVisible()) {
                                    giphyMp4ProjectionPlayerHolder = null;
                                    bitmap = null;
                                } else {
                                    giphyMp4ProjectionPlayerHolder.pause();
                                    bitmap = giphyMp4ProjectionPlayerHolder.getBitmap();
                                    giphyMp4ProjectionPlayerHolder.hide();
                                }
                                final ConversationItem conversationItem = (ConversationItem) view;
                                Bitmap snapshotView = ConversationItemSelection.snapshotView(conversationItem, ConversationFragment.this.list, messageRecord, bitmap);
                                View findFocus = ConversationFragment.this.listener.isKeyboardOpen() ? conversationItem.getRootView().findFocus() : null;
                                final ConversationItemBodyBubble conversationItemBodyBubble = conversationItem.bodyBubble;
                                final SelectedConversationModel selectedConversationModel = new SelectedConversationModel(snapshotView, view.getX(), view.getY() + ConversationFragment.this.list.getTranslationY(), conversationItemBodyBubble.getX(), conversationItemBodyBubble.getY(), conversationItemBodyBubble.getWidth(), audioUriForLongClick, messageRecord.isOutgoing(), findFocus);
                                conversationItemBodyBubble.setVisibility(4);
                                conversationItem.reactionsView.setVisibility(4);
                                View view3 = conversationItem.quotedIndicator;
                                final boolean z = view3 != null && view3.getVisibility() == 0;
                                if (z && (view2 = conversationItem.quotedIndicator) != null) {
                                    ViewUtil.fadeOut(view2, 150, 4);
                                }
                                ViewUtil.hideKeyboard(ConversationFragment.this.requireContext(), conversationItem);
                                final boolean showScrollButtons = ConversationFragment.this.conversationViewModel.getShowScrollButtons();
                                if (showScrollButtons) {
                                    ConversationFragment.this.conversationViewModel.setShowScrollButtons(false);
                                }
                                final boolean isAttachmentKeyboardOpen = ConversationFragment.this.listener.isAttachmentKeyboardOpen();
                                ConversationFragment.this.listener.handleReaction(multiselectPart.getConversationMessage(), new ReactionsToolbarListener(multiselectPart.getConversationMessage()), selectedConversationModel, new ConversationReactionOverlay.OnHideListener() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentItemClickListener.1
                                    @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnHideListener
                                    public void startHide() {
                                        ConversationFragment.this.multiselectItemDecoration.hideShade(ConversationFragment.this.list);
                                        ViewUtil.fadeOut(ConversationFragment.this.reactionsShade, ConversationFragment.this.getResources().getInteger(R.integer.reaction_scrubber_hide_duration), 8);
                                    }

                                    @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnHideListener
                                    public void onHide() {
                                        View view4;
                                        ConversationFragment.this.list.setLayoutFrozen(false);
                                        if (selectedConversationModel.getAudioUri() != null) {
                                            ConversationFragment.this.listener.onVoiceNoteResume(selectedConversationModel.getAudioUri(), messageRecord.getId());
                                        }
                                        WindowUtil.setLightStatusBarFromTheme(ConversationFragment.this.requireActivity());
                                        WindowUtil.setLightNavigationBarFromTheme(ConversationFragment.this.requireActivity());
                                        ConversationFragment.this.clearFocusedItem();
                                        GiphyMp4ProjectionPlayerHolder giphyMp4ProjectionPlayerHolder2 = giphyMp4ProjectionPlayerHolder;
                                        if (giphyMp4ProjectionPlayerHolder2 != null) {
                                            giphyMp4ProjectionPlayerHolder2.show();
                                            giphyMp4ProjectionPlayerHolder.resume();
                                        }
                                        conversationItemBodyBubble.setVisibility(0);
                                        conversationItem.reactionsView.setVisibility(0);
                                        if (z && (view4 = conversationItem.quotedIndicator) != null) {
                                            ViewUtil.fadeIn(view4, 150);
                                        }
                                        if (showScrollButtons) {
                                            ConversationFragment.this.conversationViewModel.setShowScrollButtons(true);
                                        }
                                        if (isAttachmentKeyboardOpen) {
                                            ConversationFragment.this.listener.openAttachmentKeyboard();
                                        }
                                    }
                                });
                            }
                            return;
                        }
                    }
                    conversationFragmentItemClickListener = this;
                    ConversationFragment.this.clearFocusedItem();
                    ((ConversationAdapter) ConversationFragment.this.list.getAdapter()).toggleSelection(multiselectPart);
                    ConversationFragment.this.list.invalidateItemDecorations();
                    ConversationFragment conversationFragment = ConversationFragment.this;
                    conversationFragment.actionMode = ((AppCompatActivity) conversationFragment.getActivity()).startSupportActionMode(ConversationFragment.this.actionModeCallback);
                }
            }
        }

        private Uri getAudioUriForLongClick(MessageRecord messageRecord) {
            VoiceNotePlaybackState value = ConversationFragment.this.listener.getVoiceNoteMediaController().getVoiceNotePlaybackState().getValue();
            if (value == null || !value.isPlaying() || !MessageRecordUtil.hasAudio(messageRecord) || !messageRecord.isMms()) {
                return null;
            }
            Uri uri = ((MmsMessageRecord) messageRecord).getSlideDeck().getAudioSlide().getUri();
            if (value.getUri().equals(uri)) {
                return uri;
            }
            return null;
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onQuoteClicked(MmsMessageRecord mmsMessageRecord) {
            if (mmsMessageRecord.getQuote() == null) {
                Log.w(ConversationFragment.TAG, "Received a 'quote clicked' event, but there's no quote...");
            } else if (mmsMessageRecord.getQuote().isOriginalMissing()) {
                Log.i(ConversationFragment.TAG, "Clicked on a quote whose original message we never had.");
                Toast.makeText(ConversationFragment.this.getContext(), (int) R.string.ConversationFragment_quoted_message_not_found, 0).show();
            } else if (mmsMessageRecord.getParentStoryId() != null) {
                ConversationFragment conversationFragment = ConversationFragment.this;
                conversationFragment.startActivity(StoryViewerActivity.createIntent(conversationFragment.requireContext(), new StoryViewerArgs.Builder(mmsMessageRecord.getQuote().getAuthor(), Recipient.resolved(mmsMessageRecord.getQuote().getAuthor()).shouldHideStory()).withStoryId(mmsMessageRecord.getParentStoryId().asMessageId().getId()).isFromQuote(true).build()));
            } else {
                SimpleTask.run(ConversationFragment.this.getLifecycle(), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda5(this, mmsMessageRecord), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda6(this));
            }
        }

        public /* synthetic */ Integer lambda$onQuoteClicked$0(MmsMessageRecord mmsMessageRecord) {
            return Integer.valueOf(SignalDatabase.mmsSms().getQuotedMessagePosition(ConversationFragment.this.threadId, mmsMessageRecord.getQuote().getId(), mmsMessageRecord.getQuote().getAuthor()));
        }

        public /* synthetic */ void lambda$onQuoteClicked$2(Integer num) {
            ConversationFragment.this.moveToPosition(num.intValue() + (ConversationFragment.this.isTypingIndicatorShowing() ? 1 : 0), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda12(this));
        }

        public /* synthetic */ void lambda$onQuoteClicked$1() {
            Toast.makeText(ConversationFragment.this.getContext(), (int) R.string.ConversationFragment_quoted_message_no_longer_available, 0).show();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onLinkPreviewClicked(LinkPreview linkPreview) {
            if (ConversationFragment.this.getContext() != null && ConversationFragment.this.getActivity() != null) {
                CommunicationActions.openBrowserLink(ConversationFragment.this.getActivity(), linkPreview.getUrl());
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onQuotedIndicatorClicked(MessageRecord messageRecord) {
            if (ConversationFragment.this.getContext() != null && ConversationFragment.this.getActivity() != null) {
                MessageQuotesBottomSheet.show(ConversationFragment.this.getChildFragmentManager(), new MessageId(messageRecord.getId(), messageRecord.isMms()), ConversationFragment.this.recipient.getId());
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMoreTextClicked(RecipientId recipientId, long j, boolean z) {
            if (ConversationFragment.this.getContext() != null && ConversationFragment.this.getActivity() != null) {
                LongMessageFragment.create(j, z).show(ConversationFragment.this.getChildFragmentManager(), (String) null);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onStickerClicked(StickerLocator stickerLocator) {
            if (ConversationFragment.this.getContext() != null && ConversationFragment.this.getActivity() != null) {
                ConversationFragment.this.startActivity(StickerPackPreviewActivity.getIntent(stickerLocator.getPackId(), stickerLocator.getPackKey()));
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onViewOnceMessageClicked(MmsMessageRecord mmsMessageRecord) {
            if (!mmsMessageRecord.isViewOnce()) {
                throw new AssertionError("Non-revealable message clicked.");
            } else if (!ViewOnceUtil.isViewable(mmsMessageRecord)) {
                Toast.makeText(ConversationFragment.this.requireContext(), mmsMessageRecord.isOutgoing() ? R.string.ConversationFragment_outgoing_view_once_media_files_are_automatically_removed : R.string.ConversationFragment_you_already_viewed_this_message, 0).show();
            } else {
                SimpleTask.run(ConversationFragment.this.getLifecycle(), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda17(this, mmsMessageRecord), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda18(this, mmsMessageRecord));
            }
        }

        public /* synthetic */ Uri lambda$onViewOnceMessageClicked$3(MmsMessageRecord mmsMessageRecord) {
            Log.i(ConversationFragment.TAG, "Copying the view-once photo to temp storage and deleting underlying media.");
            try {
                Slide thumbnailSlide = mmsMessageRecord.getSlideDeck().getThumbnailSlide();
                Uri createForSingleSessionOnDisk = BlobProvider.getInstance().forData(PartAuthority.getAttachmentStream(ConversationFragment.this.requireContext(), thumbnailSlide.getUri()), thumbnailSlide.getFileSize()).withMimeType(thumbnailSlide.getContentType()).createForSingleSessionOnDisk(ConversationFragment.this.requireContext());
                SignalDatabase.attachments().deleteAttachmentFilesForViewOnceMessage(mmsMessageRecord.getId());
                ApplicationDependencies.getViewOnceMessageManager().scheduleIfNecessary();
                ApplicationDependencies.getJobManager().add(new MultiDeviceViewOnceOpenJob(new MessageDatabase.SyncMessageId(mmsMessageRecord.getIndividualRecipient().getId(), mmsMessageRecord.getDateSent())));
                return createForSingleSessionOnDisk;
            } catch (IOException unused) {
                return null;
            }
        }

        public /* synthetic */ void lambda$onViewOnceMessageClicked$5(MmsMessageRecord mmsMessageRecord, Uri uri) {
            if (uri != null) {
                ConversationFragment conversationFragment = ConversationFragment.this;
                conversationFragment.startActivity(ViewOnceMessageActivity.getIntent(conversationFragment.requireContext(), mmsMessageRecord.getId(), uri));
                return;
            }
            Log.w(ConversationFragment.TAG, "Failed to open view-once photo. Showing a toast and deleting the attachments for the message just in case.");
            Toast.makeText(ConversationFragment.this.requireContext(), (int) R.string.ConversationFragment_failed_to_open_message, 0).show();
            SignalExecutors.BOUNDED.execute(new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda2(mmsMessageRecord));
        }

        public static /* synthetic */ void lambda$onViewOnceMessageClicked$4(MmsMessageRecord mmsMessageRecord) {
            SignalDatabase.attachments().deleteAttachmentFilesForViewOnceMessage(mmsMessageRecord.getId());
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onSharedContactDetailsClicked(Contact contact, View view) {
            if (ConversationFragment.this.getContext() != null && ConversationFragment.this.getActivity() != null) {
                ViewCompat.setTransitionName(view, "avatar");
                ContextCompat.startActivity(ConversationFragment.this.getActivity(), SharedContactDetailsActivity.getIntent(ConversationFragment.this.getContext(), contact), ActivityOptionsCompat.makeSceneTransitionAnimation(ConversationFragment.this.getActivity(), view, "avatar").toBundle());
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onAddToContactsClicked(final Contact contact) {
            if (ConversationFragment.this.getContext() != null) {
                new AsyncTask<Void, Void, Intent>() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationFragmentItemClickListener.2
                    public Intent doInBackground(Void... voidArr) {
                        return ContactUtil.buildAddToContactsIntent(ConversationFragment.this.getContext(), contact);
                    }

                    public void onPostExecute(Intent intent) {
                        try {
                            ConversationFragment.this.startActivityForResult(intent, 77);
                        } catch (ActivityNotFoundException e) {
                            Log.w(ConversationFragment.TAG, "Could not locate contacts activity", e);
                            Toast.makeText(ConversationFragment.this.requireContext(), (int) R.string.ConversationFragment__contacts_app_not_found, 0).show();
                        }
                    }
                }.execute(new Void[0]);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMessageSharedContactClicked(List<Recipient> list) {
            if (ConversationFragment.this.getContext() != null) {
                ContactUtil.selectRecipientThroughDialog(ConversationFragment.this.getContext(), list, ConversationFragment.this.locale, new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda1(this));
            }
        }

        public /* synthetic */ void lambda$onMessageSharedContactClicked$6(Recipient recipient) {
            CommunicationActions.startConversation(ConversationFragment.this.getContext(), recipient, null);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onInviteSharedContactClicked(List<Recipient> list) {
            if (ConversationFragment.this.getContext() != null) {
                ContactUtil.selectRecipientThroughDialog(ConversationFragment.this.getContext(), list, ConversationFragment.this.locale, new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda7(this));
            }
        }

        public /* synthetic */ void lambda$onInviteSharedContactClicked$7(Recipient recipient) {
            Context context = ConversationFragment.this.getContext();
            ConversationFragment conversationFragment = ConversationFragment.this;
            CommunicationActions.composeSmsThroughDefaultApp(context, recipient, conversationFragment.getString(R.string.InviteActivity_lets_switch_to_signal, conversationFragment.getString(R.string.install_url)));
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onReactionClicked(MultiselectPart multiselectPart, long j, boolean z) {
            if (ConversationFragment.this.getParentFragment() != null) {
                ReactionsBottomSheetDialogFragment.create(j, z).show(ConversationFragment.this.getParentFragmentManager(), (String) null);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onGroupMemberClicked(RecipientId recipientId, GroupId groupId) {
            if (ConversationFragment.this.getParentFragment() != null) {
                RecipientBottomSheetDialogFragment.create(recipientId, groupId).show(ConversationFragment.this.getParentFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMessageWithErrorClicked(MessageRecord messageRecord) {
            ConversationFragment.this.listener.onMessageWithErrorClicked(messageRecord);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onMessageWithRecaptchaNeededClicked(MessageRecord messageRecord) {
            RecaptchaProofBottomSheetFragment.show(ConversationFragment.this.getChildFragmentManager());
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onIncomingIdentityMismatchClicked(RecipientId recipientId) {
            SafetyNumberBottomSheet.forRecipientId(recipientId).show(ConversationFragment.this.getParentFragmentManager());
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNotePause(Uri uri) {
            ConversationFragment.this.listener.onVoiceNotePause(uri);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNotePlay(Uri uri, long j, double d) {
            ConversationFragment.this.listener.onVoiceNotePlay(uri, j, d);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNoteSeekTo(Uri uri, double d) {
            ConversationFragment.this.listener.onVoiceNoteSeekTo(uri, d);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onVoiceNotePlaybackSpeedChanged(Uri uri, float f) {
            ConversationFragment.this.listener.onVoiceNotePlaybackSpeedChanged(uri, f);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onRegisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer) {
            ConversationFragment.this.listener.onRegisterVoiceNoteCallbacks(observer);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onUnregisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer) {
            ConversationFragment.this.listener.onUnregisterVoiceNoteCallbacks(observer);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public boolean onUrlClicked(String str) {
            return CommunicationActions.handlePotentialGroupLinkUrl(ConversationFragment.this.requireActivity(), str) || CommunicationActions.handlePotentialProxyLinkUrl(ConversationFragment.this.requireActivity(), str);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onGroupMigrationLearnMoreClicked(GroupMigrationMembershipChange groupMigrationMembershipChange) {
            if (ConversationFragment.this.getParentFragment() != null) {
                GroupsV1MigrationInfoBottomSheetDialogFragment.show(ConversationFragment.this.getParentFragmentManager(), groupMigrationMembershipChange);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onChatSessionRefreshLearnMoreClicked() {
            new AlertDialog.Builder(ConversationFragment.this.requireContext()).setView(R.layout.decryption_failed_dialog).setPositiveButton(17039370, new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda15()).setNeutralButton(R.string.ConversationFragment_contact_us, new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda16(this)).show();
        }

        public /* synthetic */ void lambda$onChatSessionRefreshLearnMoreClicked$9(DialogInterface dialogInterface, int i) {
            ConversationFragment conversationFragment = ConversationFragment.this;
            conversationFragment.startActivity(AppSettingsActivity.help(conversationFragment.requireContext(), 0));
            dialogInterface.dismiss();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onBadDecryptLearnMoreClicked(RecipientId recipientId) {
            SimpleTask.run(ConversationFragment.this.getLifecycle(), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda3(this, recipientId), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda4(this));
        }

        public /* synthetic */ String lambda$onBadDecryptLearnMoreClicked$10(RecipientId recipientId) {
            return Recipient.resolved(recipientId).getDisplayName(ConversationFragment.this.requireContext());
        }

        public /* synthetic */ void lambda$onBadDecryptLearnMoreClicked$11(String str) {
            BadDecryptLearnMoreDialog.show(ConversationFragment.this.getParentFragmentManager(), str, ConversationFragment.this.recipient.get().isGroup());
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onSafetyNumberLearnMoreClicked(Recipient recipient) {
            if (!recipient.isGroup()) {
                AlertDialog create = new AlertDialog.Builder(ConversationFragment.this.requireContext()).setView(R.layout.safety_number_changed_learn_more_dialog).setPositiveButton(R.string.ConversationFragment_verify, new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda8(this, recipient)).setNegativeButton(R.string.ConversationFragment_not_now, new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda9()).create();
                create.setOnShowListener(new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda10(this, create, recipient));
                create.show();
                return;
            }
            throw new AssertionError("Must be individual");
        }

        public /* synthetic */ void lambda$onSafetyNumberLearnMoreClicked$14(Recipient recipient, DialogInterface dialogInterface, int i) {
            SimpleTask.run(ConversationFragment.this.getLifecycle(), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda13(recipient), new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda14(this));
            dialogInterface.dismiss();
        }

        public static /* synthetic */ Optional lambda$onSafetyNumberLearnMoreClicked$12(Recipient recipient) {
            return ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipient.getId());
        }

        public /* synthetic */ void lambda$onSafetyNumberLearnMoreClicked$13(Optional optional) {
            if (optional.isPresent()) {
                ConversationFragment conversationFragment = ConversationFragment.this;
                conversationFragment.startActivity(VerifyIdentityActivity.newIntent(conversationFragment.requireContext(), (IdentityRecord) optional.get()));
            }
        }

        public /* synthetic */ void lambda$onSafetyNumberLearnMoreClicked$16(AlertDialog alertDialog, Recipient recipient, DialogInterface dialogInterface) {
            TextView textView = (TextView) alertDialog.findViewById(R.id.safety_number_learn_more_title);
            Objects.requireNonNull(textView);
            TextView textView2 = (TextView) alertDialog.findViewById(R.id.safety_number_learn_more_body);
            Objects.requireNonNull(textView2);
            ConversationFragment conversationFragment = ConversationFragment.this;
            textView.setText(conversationFragment.getString(R.string.ConversationFragment_your_safety_number_with_s_changed, recipient.getDisplayName(conversationFragment.requireContext())));
            ConversationFragment conversationFragment2 = ConversationFragment.this;
            textView2.setText(conversationFragment2.getString(R.string.ConversationFragment_your_safety_number_with_s_changed_likey_because_they_reinstalled_signal, recipient.getDisplayName(conversationFragment2.requireContext())));
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onJoinGroupCallClicked() {
            CommunicationActions.startVideoCall(ConversationFragment.this.requireActivity(), ConversationFragment.this.recipient.get());
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onInviteFriendsToGroupClicked(GroupId.V2 v2) {
            GroupLinkInviteFriendsBottomSheetDialogFragment.show(ConversationFragment.this.requireActivity().getSupportFragmentManager(), v2);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onEnableCallNotificationsClicked() {
            EnableCallNotificationSettingsDialog.fixAutomatically(ConversationFragment.this.requireContext());
            if (EnableCallNotificationSettingsDialog.shouldShow(ConversationFragment.this.requireContext())) {
                EnableCallNotificationSettingsDialog.show(ConversationFragment.this.getChildFragmentManager());
            } else {
                ConversationFragment.this.refreshList();
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onPlayInlineContent(ConversationMessage conversationMessage) {
            ConversationFragment.this.getListAdapter().playInlineContent(conversationMessage);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onInMemoryMessageClicked(InMemoryMessageRecord inMemoryMessageRecord) {
            if (inMemoryMessageRecord instanceof InMemoryMessageRecord.NoGroupsInCommon) {
                new MaterialAlertDialogBuilder(ConversationFragment.this.requireContext(), R.style.ThemeOverlay_Signal_MaterialAlertDialog).setMessage(((InMemoryMessageRecord.NoGroupsInCommon) inMemoryMessageRecord).isGroup() ? R.string.GroupsInCommonMessageRequest__none_of_your_contacts_or_people_you_chat_with_are_in_this_group : R.string.GroupsInCommonMessageRequest__you_have_no_groups_in_common_with_this_person).setNeutralButton(R.string.GroupsInCommonMessageRequest__about_message_requests, (DialogInterface.OnClickListener) new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda0(this)).setPositiveButton(R.string.GroupsInCommonMessageRequest__okay, (DialogInterface.OnClickListener) null).show();
            }
        }

        public /* synthetic */ void lambda$onInMemoryMessageClicked$17(DialogInterface dialogInterface, int i) {
            CommunicationActions.openBrowserLink(ConversationFragment.this.requireContext(), ConversationFragment.this.getString(R.string.GroupsInCommonMessageRequest__support_article));
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onViewGroupDescriptionChange(GroupId groupId, String str, boolean z) {
            if (groupId != null) {
                GroupDescriptionDialog.show(ConversationFragment.this.getChildFragmentManager(), groupId, str, z);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onChangeNumberUpdateContact(Recipient recipient) {
            ConversationFragment.this.startActivity(RecipientExporter.export(recipient).asAddContactIntent());
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onDonateClicked() {
            ConversationFragment.this.requireActivity().getSupportFragmentManager().beginTransaction().add(NavHostFragment.create(R.navigation.boosts), "boost_nav").commitNow();
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onBlockJoinRequest(Recipient recipient) {
            MaterialAlertDialogBuilder title = new MaterialAlertDialogBuilder(ConversationFragment.this.requireContext()).setTitle(R.string.ConversationFragment__block_request);
            ConversationFragment conversationFragment = ConversationFragment.this;
            title.setMessage((CharSequence) conversationFragment.getString(R.string.ConversationFragment__s_will_not_be_able_to_join_or_request_to_join_this_group_via_the_group_link, recipient.getDisplayName(conversationFragment.requireContext()))).setNegativeButton(R.string.ConversationFragment__cancel, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.ConversationFragment__block_request_button, (DialogInterface.OnClickListener) new ConversationFragment$ConversationFragmentItemClickListener$$ExternalSyntheticLambda11(this, recipient)).show();
        }

        public /* synthetic */ void lambda$onBlockJoinRequest$18(Recipient recipient, DialogInterface dialogInterface, int i) {
            ConversationFragment.this.handleBlockJoinRequest(recipient);
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onRecipientNameClicked(RecipientId recipientId) {
            if (ConversationFragment.this.getParentFragment() != null) {
                RecipientBottomSheetDialogFragment.create(recipientId, ConversationFragment.this.recipient.get().getGroupId().orElse(null)).show(ConversationFragment.this.getParentFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onViewGiftBadgeClicked(MessageRecord messageRecord) {
            if (MessageRecordUtil.hasGiftBadge(messageRecord)) {
                if (messageRecord.isOutgoing()) {
                    ViewSentGiftBottomSheet.show(ConversationFragment.this.getChildFragmentManager(), (MmsMessageRecord) messageRecord);
                } else {
                    ViewReceivedGiftBottomSheet.show(ConversationFragment.this.getChildFragmentManager(), (MmsMessageRecord) messageRecord);
                }
            }
        }

        @Override // org.thoughtcrime.securesms.BindableConversationItem.EventListener
        public void onGiftBadgeRevealed(MessageRecord messageRecord) {
            if (messageRecord.isOutgoing() && MessageRecordUtil.hasGiftBadge(messageRecord)) {
                ConversationFragment.this.conversationViewModel.markGiftBadgeRevealed(messageRecord.getId());
            }
        }
    }

    public boolean isUnopenedGift(View view, MessageRecord messageRecord) {
        Projection openableGiftProjection;
        if (!(view instanceof OpenableGift) || (openableGiftProjection = ((OpenableGift) view).getOpenableGiftProjection(false)) == null) {
            return false;
        }
        openableGiftProjection.release();
        return !this.openableGiftItemDecoration.hasOpenedGiftThisSession(messageRecord.getId());
    }

    public void refreshList() {
        ConversationAdapter listAdapter = getListAdapter();
        if (listAdapter != null) {
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == 77 && getContext() != null) {
            ApplicationDependencies.getJobManager().add(new DirectoryRefreshJob(false));
        }
    }

    public void handleEnterMultiSelect(ConversationMessage conversationMessage) {
        Collection$EL.stream(conversationMessage.getMultiselectCollection().toSet()).forEach(new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda1
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                ConversationFragment.$r8$lambda$g1gmEhAcWDeV1gDP1VFq4uC1tUk(ConversationFragment.this, (MultiselectPart) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        this.list.invalidateItemDecorations();
        this.actionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(this.actionModeCallback);
    }

    public /* synthetic */ void lambda$handleEnterMultiSelect$70(MultiselectPart multiselectPart) {
        ((ConversationAdapter) this.list.getAdapter()).toggleSelection(multiselectPart);
    }

    public void handleBlockJoinRequest(Recipient recipient) {
        this.disposables.add(this.groupViewModel.blockJoinRequests(this.recipient.get(), recipient).subscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment$$ExternalSyntheticLambda64
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ConversationFragment.m1380$r8$lambda$EdsoBJb9HvCHB4F651Xf2Oaqrg(ConversationFragment.this, (GroupBlockJoinRequestResult) obj);
            }
        }));
    }

    public /* synthetic */ void lambda$handleBlockJoinRequest$71(GroupBlockJoinRequestResult groupBlockJoinRequestResult) throws Throwable {
        if (groupBlockJoinRequestResult.isFailure()) {
            Toast.makeText(requireContext(), GroupErrors.getUserDisplayMessage(((GroupBlockJoinRequestResult.Failure) groupBlockJoinRequestResult).getReason()), 0).show();
            return;
        }
        Toast.makeText(requireContext(), (int) R.string.ConversationFragment__blocked, 0).show();
    }

    /* loaded from: classes4.dex */
    public final class CheckExpirationDataObserver extends RecyclerView.AdapterDataObserver {
        private CheckExpirationDataObserver() {
            ConversationFragment.this = r1;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onItemRangeRemoved(int i, int i2) {
            ConversationAdapter listAdapter = ConversationFragment.this.getListAdapter();
            if (listAdapter != null && ConversationFragment.this.actionMode != null) {
                Set<MultiselectPart> selectedItems = listAdapter.getSelectedItems();
                HashSet hashSet = new HashSet();
                for (MultiselectPart multiselectPart : selectedItems) {
                    if (multiselectPart.isExpired()) {
                        hashSet.add(multiselectPart);
                    }
                }
                listAdapter.removeFromSelection(hashSet);
                if (listAdapter.getSelectedItems().isEmpty()) {
                    ConversationFragment.this.actionMode.finish();
                } else {
                    ConversationFragment.this.actionMode.setTitle(ConversationFragment.this.calculateSelectedItemCount());
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class ConversationSnapToTopDataObserver extends SnapToTopDataObserver {
        @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver, androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onItemRangeMoved(int i, int i2, int i3) {
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ConversationSnapToTopDataObserver(RecyclerView recyclerView, SnapToTopDataObserver.ScrollRequestValidator scrollRequestValidator) {
            super(recyclerView, scrollRequestValidator, new ConversationFragment$ConversationSnapToTopDataObserver$$ExternalSyntheticLambda2(r2));
            ConversationFragment.this = r2;
        }

        public static /* synthetic */ void lambda$new$1(ConversationFragment conversationFragment) {
            conversationFragment.list.scrollToPosition(0);
            conversationFragment.list.post(new ConversationFragment$ConversationSnapToTopDataObserver$$ExternalSyntheticLambda0(conversationFragment));
        }

        @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver, androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onItemRangeInserted(int i, int i2) {
            if (i != 0 || i2 != 1 || !ConversationFragment.this.isTypingIndicatorShowing()) {
                super.onItemRangeInserted(i, i2);
            }
        }

        @Override // androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
        public void onItemRangeChanged(int i, int i2) {
            super.onItemRangeChanged(i, i2);
            ConversationFragment.this.list.post(new ConversationFragment$ConversationSnapToTopDataObserver$$ExternalSyntheticLambda1(ConversationFragment.this));
        }
    }

    /* loaded from: classes4.dex */
    private final class ConversationScrollRequestValidator implements SnapToTopDataObserver.ScrollRequestValidator {
        private ConversationScrollRequestValidator() {
            ConversationFragment.this = r1;
        }

        @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.ScrollRequestValidator
        public boolean isPositionStillValid(int i) {
            if (ConversationFragment.this.getListAdapter() == null) {
                return i >= 0;
            }
            if (i < 0 || i >= ConversationFragment.this.getListAdapter().getItemCount()) {
                return false;
            }
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.SnapToTopDataObserver.ScrollRequestValidator
        public boolean isItemAtPositionLoaded(int i) {
            if (ConversationFragment.this.getListAdapter() == null) {
                return false;
            }
            if ((!ConversationFragment.this.getListAdapter().hasFooter() || i != ConversationFragment.this.getListAdapter().getItemCount() - 1) && ConversationFragment.this.getListAdapter().getItem(i) == null) {
                return false;
            }
            return true;
        }
    }

    /* loaded from: classes4.dex */
    private class ReactionsToolbarListener implements ConversationReactionOverlay.OnActionSelectedListener {
        private final ConversationMessage conversationMessage;

        private ReactionsToolbarListener(ConversationMessage conversationMessage) {
            ConversationFragment.this = r1;
            this.conversationMessage = conversationMessage;
        }

        @Override // org.thoughtcrime.securesms.conversation.ConversationReactionOverlay.OnActionSelectedListener
        public void onActionSelected(ConversationReactionOverlay.Action action) {
            switch (AnonymousClass7.$SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[action.ordinal()]) {
                case 1:
                    ConversationFragment.this.handleReplyMessage(this.conversationMessage);
                    return;
                case 2:
                    ConversationFragment.this.lambda$setCorrectActionModeMenuVisibility$30(this.conversationMessage.getMultiselectCollection().toSet());
                    return;
                case 3:
                    ConversationFragment.this.handleResendMessage(this.conversationMessage.getMessageRecord());
                    return;
                case 4:
                    ConversationFragment.this.handleSaveAttachment((MediaMmsMessageRecord) this.conversationMessage.getMessageRecord());
                    return;
                case 5:
                    ConversationFragment.this.handleCopyMessage(this.conversationMessage.getMultiselectCollection().toSet());
                    return;
                case 6:
                    ConversationFragment.this.handleEnterMultiSelect(this.conversationMessage);
                    return;
                case 7:
                    ConversationFragment.this.handleDisplayDetails(this.conversationMessage);
                    return;
                case 8:
                    ConversationFragment.this.handleDeleteMessages(this.conversationMessage.getMultiselectCollection().toSet());
                    return;
                default:
                    return;
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.conversation.ConversationFragment$7 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass7 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action;

        static {
            int[] iArr = new int[ConversationReactionOverlay.Action.values().length];
            $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action = iArr;
            try {
                iArr[ConversationReactionOverlay.Action.REPLY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.FORWARD.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.RESEND.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.DOWNLOAD.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.COPY.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.MULTISELECT.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.VIEW_INFO.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$conversation$ConversationReactionOverlay$Action[ConversationReactionOverlay.Action.DELETE.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public class ActionModeCallback implements ActionMode.Callback {
        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            return false;
        }

        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        private ActionModeCallback() {
            ConversationFragment.this = r1;
        }

        @Override // androidx.appcompat.view.ActionMode.Callback
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.setTitle(ConversationFragment.this.calculateSelectedItemCount());
            ConversationFragment.this.setCorrectActionModeMenuVisibility();
            ConversationFragment.this.listener.onMessageActionToolbarOpened();
            return true;
        }

        @Override // androidx.appcompat.view.ActionMode.Callback
        public void onDestroyActionMode(ActionMode actionMode) {
            ((ConversationAdapter) ConversationFragment.this.list.getAdapter()).clearSelection();
            ConversationFragment.this.list.invalidateItemDecorations();
            ConversationFragment.this.setBottomActionBarVisibility(false);
            ConversationFragment.this.actionMode = null;
            ConversationFragment.this.listener.onMessageActionToolbarClosed();
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class ConversationDateHeader extends ConversationAdapter.StickyHeaderViewHolder {
        private final Animation animateIn;
        private final Animation animateOut;
        private boolean pendingHide;

        private ConversationDateHeader(Context context, TextView textView) {
            super(textView);
            this.pendingHide = false;
            Animation loadAnimation = AnimationUtils.loadAnimation(context, R.anim.slide_from_top);
            this.animateIn = loadAnimation;
            Animation loadAnimation2 = AnimationUtils.loadAnimation(context, R.anim.slide_to_top);
            this.animateOut = loadAnimation2;
            loadAnimation.setDuration(100);
            loadAnimation2.setDuration(100);
        }

        public void show() {
            if (this.textView.getText() != null && this.textView.getText().length() != 0) {
                if (this.pendingHide) {
                    this.pendingHide = false;
                } else {
                    ViewUtil.animateIn(this.textView, this.animateIn);
                }
            }
        }

        public void hide() {
            this.pendingHide = true;
            this.textView.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.ConversationFragment.ConversationDateHeader.1
                @Override // java.lang.Runnable
                public void run() {
                    if (ConversationDateHeader.this.pendingHide) {
                        ConversationDateHeader.this.pendingHide = false;
                        ConversationDateHeader conversationDateHeader = ConversationDateHeader.this;
                        ViewUtil.animateOut(conversationDateHeader.textView, conversationDateHeader.animateOut, 8);
                    }
                }
            }, 400);
        }
    }

    /* loaded from: classes4.dex */
    public static final class TransitionListener implements Animator.AnimatorListener {
        private final ValueAnimator animator;

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationCancel(Animator animator) {
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationRepeat(Animator animator) {
        }

        TransitionListener(RecyclerView recyclerView) {
            ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
            this.animator = ofFloat;
            ofFloat.addUpdateListener(new ConversationFragment$TransitionListener$$ExternalSyntheticLambda0(recyclerView));
            ofFloat.setDuration(100L);
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationStart(Animator animator) {
            this.animator.start();
        }

        @Override // android.animation.Animator.AnimatorListener
        public void onAnimationEnd(Animator animator) {
            this.animator.end();
        }
    }
}
