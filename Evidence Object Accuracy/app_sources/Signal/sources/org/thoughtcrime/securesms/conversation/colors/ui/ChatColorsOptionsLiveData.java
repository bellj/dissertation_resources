package org.thoughtcrime.securesms.conversation.colors.ui;

import androidx.lifecycle.LiveData;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.database.ChatColorsDatabase;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;

/* compiled from: ChatColorsOptionsLiveData.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0004J\b\u0010\u000b\u001a\u00020\fH\u0014J\b\u0010\r\u001a\u00020\fH\u0014J\b\u0010\u000e\u001a\u00020\fH\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/conversation/colors/ui/ChatColorsOptionsLiveData;", "Landroidx/lifecycle/LiveData;", "", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "()V", "chatColorsDatabase", "Lorg/thoughtcrime/securesms/database/ChatColorsDatabase;", "executor", "Ljava/util/concurrent/Executor;", "observer", "Lorg/thoughtcrime/securesms/database/DatabaseObserver$Observer;", "onActive", "", "onInactive", "refreshChatColors", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorsOptionsLiveData extends LiveData<List<? extends ChatColors>> {
    private final ChatColorsDatabase chatColorsDatabase = SignalDatabase.Companion.chatColors();
    private final Executor executor = new SerialMonoLifoExecutor(SignalExecutors.BOUNDED);
    private final DatabaseObserver.Observer observer = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorsOptionsLiveData$$ExternalSyntheticLambda1
        @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
        public final void onChanged() {
            ChatColorsOptionsLiveData.m1522observer$lambda0(ChatColorsOptionsLiveData.this);
        }
    };

    /* renamed from: observer$lambda-0 */
    public static final void m1522observer$lambda0(ChatColorsOptionsLiveData chatColorsOptionsLiveData) {
        Intrinsics.checkNotNullParameter(chatColorsOptionsLiveData, "this$0");
        chatColorsOptionsLiveData.refreshChatColors();
    }

    @Override // androidx.lifecycle.LiveData
    public void onActive() {
        refreshChatColors();
        ApplicationDependencies.getDatabaseObserver().registerChatColorsObserver(this.observer);
    }

    @Override // androidx.lifecycle.LiveData
    public void onInactive() {
        ApplicationDependencies.getDatabaseObserver().unregisterObserver(this.observer);
    }

    private final void refreshChatColors() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorsOptionsLiveData$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ChatColorsOptionsLiveData.m1523refreshChatColors$lambda2(ChatColorsOptionsLiveData.this);
            }
        });
    }

    /* renamed from: refreshChatColors$lambda-2 */
    public static final void m1523refreshChatColors$lambda2(ChatColorsOptionsLiveData chatColorsOptionsLiveData) {
        Intrinsics.checkNotNullParameter(chatColorsOptionsLiveData, "this$0");
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(ChatColorsPalette.Bubbles.INSTANCE.getAll());
        arrayList.addAll(chatColorsOptionsLiveData.chatColorsDatabase.getSavedChatColors());
        chatColorsOptionsLiveData.postValue(arrayList);
    }
}
