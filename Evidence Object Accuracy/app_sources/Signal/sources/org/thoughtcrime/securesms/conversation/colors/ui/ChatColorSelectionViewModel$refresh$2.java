package org.thoughtcrime.securesms.conversation.colors.ui;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;

/* compiled from: ChatColorSelectionViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "chatColors", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorSelectionViewModel$refresh$2 extends Lambda implements Function1<ChatColors, Unit> {
    final /* synthetic */ ChatColorSelectionViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatColorSelectionViewModel$refresh$2(ChatColorSelectionViewModel chatColorSelectionViewModel) {
        super(1);
        this.this$0 = chatColorSelectionViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ChatColors chatColors) {
        invoke(chatColors);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final ChatColorSelectionState m1520invoke$lambda0(ChatColors chatColors, ChatColorSelectionState chatColorSelectionState) {
        Intrinsics.checkNotNullParameter(chatColors, "$chatColors");
        Intrinsics.checkNotNullExpressionValue(chatColorSelectionState, "it");
        return ChatColorSelectionState.copy$default(chatColorSelectionState, null, chatColors, null, 5, null);
    }

    public final void invoke(ChatColors chatColors) {
        Intrinsics.checkNotNullParameter(chatColors, "chatColors");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.conversation.colors.ui.ChatColorSelectionViewModel$refresh$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatColorSelectionViewModel$refresh$2.m1520invoke$lambda0(ChatColors.this, (ChatColorSelectionState) obj);
            }
        });
    }
}
