package org.thoughtcrime.securesms;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import org.thoughtcrime.securesms.preferences.MmsPreferencesActivity;

/* loaded from: classes.dex */
public class PromptMmsActivity extends PassphraseRequiredActivity {
    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.prompt_apn_activity);
        initializeResources();
    }

    private void initializeResources() {
        ((Button) findViewById(R.id.ok_button)).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.PromptMmsActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PromptMmsActivity.$r8$lambda$r9jDWRhERMRK4IMEYhumLeVeaSs(PromptMmsActivity.this, view);
            }
        });
        ((Button) findViewById(R.id.cancel_button)).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.PromptMmsActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PromptMmsActivity.m259$r8$lambda$umPiDuYjeVaaJ3V15byd2myJjs(PromptMmsActivity.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeResources$0(View view) {
        Intent intent = new Intent(this, MmsPreferencesActivity.class);
        intent.putExtras(getIntent().getExtras());
        startActivity(intent);
        finish();
    }

    public /* synthetic */ void lambda$initializeResources$1(View view) {
        finish();
    }
}
