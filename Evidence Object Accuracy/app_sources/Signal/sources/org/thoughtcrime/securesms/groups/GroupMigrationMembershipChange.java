package org.thoughtcrime.securesms.groups;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class GroupMigrationMembershipChange {
    private final List<RecipientId> dropped;
    private final List<RecipientId> pending;

    public GroupMigrationMembershipChange(List<RecipientId> list, List<RecipientId> list2) {
        this.pending = list;
        this.dropped = list2;
    }

    public static GroupMigrationMembershipChange empty() {
        return new GroupMigrationMembershipChange(Collections.emptyList(), Collections.emptyList());
    }

    public static GroupMigrationMembershipChange deserialize(String str) {
        if (Util.isEmpty(str)) {
            return empty();
        }
        String[] split = str.split("\\|");
        if (split.length == 1) {
            return new GroupMigrationMembershipChange(RecipientId.fromSerializedList(split[0]), Collections.emptyList());
        }
        if (split.length == 2) {
            return new GroupMigrationMembershipChange(RecipientId.fromSerializedList(split[0]), RecipientId.fromSerializedList(split[1]));
        }
        return empty();
    }

    public List<RecipientId> getPending() {
        return this.pending;
    }

    public List<RecipientId> getDropped() {
        return this.dropped;
    }

    public String serialize() {
        return RecipientId.toSerializedList(this.pending) + "|" + RecipientId.toSerializedList(this.dropped);
    }

    public boolean isEmpty() {
        return this.pending.isEmpty() && this.dropped.isEmpty();
    }
}
