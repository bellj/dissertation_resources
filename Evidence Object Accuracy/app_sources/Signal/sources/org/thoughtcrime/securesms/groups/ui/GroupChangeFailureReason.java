package org.thoughtcrime.securesms.groups.ui;

import java.io.IOException;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupInsufficientRightsException;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.groups.MembershipNotSuitableForV2Exception;

/* loaded from: classes4.dex */
public enum GroupChangeFailureReason {
    NO_RIGHTS,
    NOT_GV2_CAPABLE,
    NOT_ANNOUNCEMENT_CAPABLE,
    NOT_A_MEMBER,
    BUSY,
    NETWORK,
    OTHER;

    public static GroupChangeFailureReason fromException(Throwable th) {
        if (th instanceof MembershipNotSuitableForV2Exception) {
            return NOT_GV2_CAPABLE;
        }
        if (th instanceof IOException) {
            return NETWORK;
        }
        if (th instanceof GroupNotAMemberException) {
            return NOT_A_MEMBER;
        }
        if (th instanceof GroupChangeBusyException) {
            return BUSY;
        }
        if (th instanceof GroupInsufficientRightsException) {
            return NO_RIGHTS;
        }
        return OTHER;
    }
}
