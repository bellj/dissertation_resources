package org.thoughtcrime.securesms.groups.ui.migration;

import android.content.DialogInterface;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import java.io.IOException;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupInsufficientRightsException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.groups.MembershipNotSuitableForV2Exception;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public final class GroupsV1MigrationSuggestionsDialog {
    private static final String TAG = Log.tag(GroupsV1MigrationSuggestionsDialog.class);
    private final FragmentActivity fragmentActivity;
    private final GroupId.V2 groupId;
    private final List<RecipientId> suggestions;

    /* loaded from: classes4.dex */
    public enum Result {
        SUCCESS,
        NETWORK_ERROR,
        IMPOSSIBLE
    }

    public static void show(FragmentActivity fragmentActivity, GroupId.V2 v2, List<RecipientId> list) {
        new GroupsV1MigrationSuggestionsDialog(fragmentActivity, v2, list).display();
    }

    private GroupsV1MigrationSuggestionsDialog(FragmentActivity fragmentActivity, GroupId.V2 v2, List<RecipientId> list) {
        this.fragmentActivity = fragmentActivity;
        this.groupId = v2;
        this.suggestions = list;
    }

    private void display() {
        GroupMemberListView groupMemberListView = (GroupMemberListView) new AlertDialog.Builder(this.fragmentActivity).setTitle(this.fragmentActivity.getResources().getQuantityString(R.plurals.GroupsV1MigrationSuggestionsDialog_add_members_question, this.suggestions.size())).setMessage(this.fragmentActivity.getResources().getQuantityString(R.plurals.GroupsV1MigrationSuggestionsDialog_these_members_couldnt_be_automatically_added, this.suggestions.size())).setView(R.layout.dialog_group_members).setPositiveButton(this.fragmentActivity.getResources().getQuantityString(R.plurals.GroupsV1MigrationSuggestionsDialog_add_members, this.suggestions.size()), new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                GroupsV1MigrationSuggestionsDialog.this.lambda$display$0(dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).show().findViewById(R.id.list_members);
        groupMemberListView.initializeAdapter(this.fragmentActivity);
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$$ExternalSyntheticLambda2
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return GroupsV1MigrationSuggestionsDialog.this.lambda$display$2();
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                GroupMemberListView.this.setDisplayOnlyMembers((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$display$0(DialogInterface dialogInterface, int i) {
        onAddClicked(dialogInterface);
    }

    public /* synthetic */ List lambda$display$2() {
        return Recipient.resolvedList(this.suggestions);
    }

    private void onAddClicked(DialogInterface dialogInterface) {
        SimpleTask.run(SignalExecutors.UNBOUNDED, new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$$ExternalSyntheticLambda4
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return GroupsV1MigrationSuggestionsDialog.this.lambda$onAddClicked$3();
            }
        }, new SimpleTask.ForegroundTask(SimpleProgressDialog.showDelayed(this.fragmentActivity, WebRtcCallView.PIP_RESIZE_DURATION, 0), dialogInterface) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$$ExternalSyntheticLambda5
            public final /* synthetic */ SimpleProgressDialog.DismissibleDialog f$1;
            public final /* synthetic */ DialogInterface f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                GroupsV1MigrationSuggestionsDialog.this.lambda$onAddClicked$4(this.f$1, this.f$2, (GroupsV1MigrationSuggestionsDialog.Result) obj);
            }
        });
    }

    public /* synthetic */ Result lambda$onAddClicked$3() {
        Throwable e;
        Throwable e2;
        try {
            GroupManager.addMembers(this.fragmentActivity, this.groupId.requirePush(), this.suggestions);
            Log.i(TAG, "Successfully added members! Removing these dropped members from the list.");
            SignalDatabase.groups().removeUnmigratedV1Members(this.groupId, this.suggestions);
            return Result.SUCCESS;
        } catch (IOException e3) {
            e = e3;
            Log.w(TAG, "Temporary failure.", e);
            return Result.NETWORK_ERROR;
        } catch (GroupChangeBusyException e4) {
            e = e4;
            Log.w(TAG, "Temporary failure.", e);
            return Result.NETWORK_ERROR;
        } catch (GroupChangeFailedException e5) {
            e2 = e5;
            Log.w(TAG, "Permanent failure! Removing these dropped members from the list.", e2);
            SignalDatabase.groups().removeUnmigratedV1Members(this.groupId, this.suggestions);
            return Result.IMPOSSIBLE;
        } catch (GroupInsufficientRightsException e6) {
            e2 = e6;
            Log.w(TAG, "Permanent failure! Removing these dropped members from the list.", e2);
            SignalDatabase.groups().removeUnmigratedV1Members(this.groupId, this.suggestions);
            return Result.IMPOSSIBLE;
        } catch (GroupNotAMemberException e7) {
            e2 = e7;
            Log.w(TAG, "Permanent failure! Removing these dropped members from the list.", e2);
            SignalDatabase.groups().removeUnmigratedV1Members(this.groupId, this.suggestions);
            return Result.IMPOSSIBLE;
        } catch (MembershipNotSuitableForV2Exception e8) {
            e2 = e8;
            Log.w(TAG, "Permanent failure! Removing these dropped members from the list.", e2);
            SignalDatabase.groups().removeUnmigratedV1Members(this.groupId, this.suggestions);
            return Result.IMPOSSIBLE;
        }
    }

    public /* synthetic */ void lambda$onAddClicked$4(SimpleProgressDialog.DismissibleDialog dismissibleDialog, DialogInterface dialogInterface, Result result) {
        dismissibleDialog.dismiss();
        dialogInterface.dismiss();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$GroupsV1MigrationSuggestionsDialog$Result[result.ordinal()];
        if (i == 1) {
            FragmentActivity fragmentActivity = this.fragmentActivity;
            Toast.makeText(fragmentActivity, fragmentActivity.getResources().getQuantityText(R.plurals.GroupsV1MigrationSuggestionsDialog_failed_to_add_members_try_again_later, this.suggestions.size()), 0).show();
        } else if (i == 2) {
            FragmentActivity fragmentActivity2 = this.fragmentActivity;
            Toast.makeText(fragmentActivity2, fragmentActivity2.getResources().getQuantityText(R.plurals.GroupsV1MigrationSuggestionsDialog_cannot_add_members, this.suggestions.size()), 0).show();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationSuggestionsDialog$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$GroupsV1MigrationSuggestionsDialog$Result;

        static {
            int[] iArr = new int[Result.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$GroupsV1MigrationSuggestionsDialog$Result = iArr;
            try {
                iArr[Result.NETWORK_ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$GroupsV1MigrationSuggestionsDialog$Result[Result.IMPOSSIBLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }
}
