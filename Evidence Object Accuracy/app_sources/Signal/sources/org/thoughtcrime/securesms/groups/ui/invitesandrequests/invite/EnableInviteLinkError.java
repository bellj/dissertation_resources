package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite;

/* loaded from: classes4.dex */
public enum EnableInviteLinkError {
    BUSY,
    FAILED,
    NETWORK_ERROR,
    INSUFFICIENT_RIGHTS,
    NOT_IN_GROUP
}
