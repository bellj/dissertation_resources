package org.thoughtcrime.securesms.groups.ui;

import android.view.View;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$RequestingMemberViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ GroupMemberListAdapter.RequestingMemberViewHolder f$0;
    public final /* synthetic */ GroupMemberEntry.RequestingMember f$1;

    public /* synthetic */ GroupMemberListAdapter$RequestingMemberViewHolder$$ExternalSyntheticLambda1(GroupMemberListAdapter.RequestingMemberViewHolder requestingMemberViewHolder, GroupMemberEntry.RequestingMember requestingMember) {
        this.f$0 = requestingMemberViewHolder;
        this.f$1 = requestingMember;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$1(this.f$1, view);
    }
}
