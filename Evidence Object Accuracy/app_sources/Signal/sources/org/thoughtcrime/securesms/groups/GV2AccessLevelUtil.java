package org.thoughtcrime.securesms.groups;

import android.content.Context;
import org.signal.storageservice.protos.groups.AccessControl;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class GV2AccessLevelUtil {
    private GV2AccessLevelUtil() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.groups.GV2AccessLevelUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired;

        static {
            int[] iArr = new int[AccessControl.AccessRequired.values().length];
            $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired = iArr;
            try {
                iArr[AccessControl.AccessRequired.ANY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.MEMBER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[AccessControl.AccessRequired.ADMINISTRATOR.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public static String toString(Context context, AccessControl.AccessRequired accessRequired) {
        int i = AnonymousClass1.$SwitchMap$org$signal$storageservice$protos$groups$AccessControl$AccessRequired[accessRequired.ordinal()];
        if (i == 1) {
            return context.getString(R.string.GroupManagement_access_level_anyone);
        }
        if (i == 2) {
            return context.getString(R.string.GroupManagement_access_level_all_members);
        }
        if (i != 3) {
            return context.getString(R.string.GroupManagement_access_level_unknown);
        }
        return context.getString(R.string.GroupManagement_access_level_only_admins);
    }
}
