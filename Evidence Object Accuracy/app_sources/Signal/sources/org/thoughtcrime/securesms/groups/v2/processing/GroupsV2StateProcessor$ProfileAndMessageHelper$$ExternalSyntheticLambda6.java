package org.thoughtcrime.securesms.groups.v2.processing;

import j$.util.function.Function;
import java.util.UUID;
import org.whispersystems.signalservice.api.push.ServiceId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda6 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ServiceId.from((UUID) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
