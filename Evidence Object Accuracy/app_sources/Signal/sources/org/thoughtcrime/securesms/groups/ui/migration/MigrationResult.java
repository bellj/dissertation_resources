package org.thoughtcrime.securesms.groups.ui.migration;

/* loaded from: classes4.dex */
public enum MigrationResult {
    SUCCESS,
    FAILURE_GENERAL,
    FAILURE_NETWORK
}
