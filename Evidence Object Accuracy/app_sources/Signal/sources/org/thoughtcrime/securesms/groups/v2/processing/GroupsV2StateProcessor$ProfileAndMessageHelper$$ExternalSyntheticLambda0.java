package org.thoughtcrime.securesms.groups.v2.processing;

import com.annimon.stream.function.Function;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((ServerGroupLogEntry) obj).getChange();
    }
}
