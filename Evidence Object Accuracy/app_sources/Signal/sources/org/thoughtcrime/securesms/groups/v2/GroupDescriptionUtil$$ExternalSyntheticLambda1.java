package org.thoughtcrime.securesms.groups.v2;

import android.text.SpannableString;
import android.text.style.URLSpan;
import com.annimon.stream.function.Consumer;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupDescriptionUtil$$ExternalSyntheticLambda1 implements Consumer {
    public final /* synthetic */ SpannableString f$0;

    public /* synthetic */ GroupDescriptionUtil$$ExternalSyntheticLambda1(SpannableString spannableString) {
        this.f$0 = spannableString;
    }

    @Override // com.annimon.stream.function.Consumer
    public final void accept(Object obj) {
        this.f$0.removeSpan((URLSpan) obj);
    }
}
