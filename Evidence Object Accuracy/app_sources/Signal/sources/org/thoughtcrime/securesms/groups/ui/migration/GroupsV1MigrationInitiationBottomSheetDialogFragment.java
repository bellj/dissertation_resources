package org.thoughtcrime.securesms.groups.ui.migration;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public final class GroupsV1MigrationInitiationBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private static final String KEY_GROUP_RECIPIENT_ID;
    private View ineligibleContainer;
    private GroupMemberListView ineligibleList;
    private TextView ineligibleTitle;
    private View inviteContainer;
    private GroupMemberListView inviteList;
    private TextView inviteTitle;
    private View spinner;
    private View upgradeButton;
    private GroupsV1MigrationInitiationViewModel viewModel;

    public static void showForInitiation(FragmentManager fragmentManager, RecipientId recipientId) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_GROUP_RECIPIENT_ID, recipientId);
        GroupsV1MigrationInitiationBottomSheetDialogFragment groupsV1MigrationInitiationBottomSheetDialogFragment = new GroupsV1MigrationInitiationBottomSheetDialogFragment();
        groupsV1MigrationInitiationBottomSheetDialogFragment.setArguments(bundle);
        groupsV1MigrationInitiationBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.groupsv1_migration_bottom_sheet, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.inviteContainer = view.findViewById(R.id.gv1_migrate_invite_container);
        this.inviteTitle = (TextView) view.findViewById(R.id.gv1_migrate_invite_title);
        this.inviteList = (GroupMemberListView) view.findViewById(R.id.gv1_migrate_invite_list);
        this.ineligibleContainer = view.findViewById(R.id.gv1_migrate_ineligible_container);
        this.ineligibleTitle = (TextView) view.findViewById(R.id.gv1_migrate_ineligible_title);
        this.ineligibleList = (GroupMemberListView) view.findViewById(R.id.gv1_migrate_ineligible_list);
        this.upgradeButton = view.findViewById(R.id.gv1_migrate_upgrade_button);
        this.spinner = view.findViewById(R.id.gv1_migrate_spinner);
        this.inviteList.initializeAdapter(getViewLifecycleOwner());
        this.ineligibleList.initializeAdapter(getViewLifecycleOwner());
        this.inviteList.setNestedScrollingEnabled(false);
        this.ineligibleList.setNestedScrollingEnabled(false);
        GroupsV1MigrationInitiationViewModel groupsV1MigrationInitiationViewModel = (GroupsV1MigrationInitiationViewModel) ViewModelProviders.of(this, new GroupsV1MigrationInitiationViewModel.Factory((RecipientId) getArguments().getParcelable(KEY_GROUP_RECIPIENT_ID))).get(GroupsV1MigrationInitiationViewModel.class);
        this.viewModel = groupsV1MigrationInitiationViewModel;
        groupsV1MigrationInitiationViewModel.getMigrationState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupsV1MigrationInitiationBottomSheetDialogFragment.this.onMigrationStateChanged((MigrationState) obj);
            }
        });
        this.upgradeButton.setEnabled(false);
        this.upgradeButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GroupsV1MigrationInitiationBottomSheetDialogFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        view.findViewById(R.id.gv1_migrate_cancel_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GroupsV1MigrationInitiationBottomSheetDialogFragment.this.lambda$onViewCreated$1(view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        onUpgradeClicked();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        dismiss();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }

    public void onMigrationStateChanged(MigrationState migrationState) {
        if (migrationState.getNeedsInvite().size() > 0) {
            this.inviteContainer.setVisibility(0);
            this.inviteTitle.setText(getResources().getQuantityText(R.plurals.GroupsV1MigrationInitiation_these_members_will_need_to_accept_an_invite, migrationState.getNeedsInvite().size()));
            this.inviteList.setDisplayOnlyMembers(migrationState.getNeedsInvite());
        } else {
            this.inviteContainer.setVisibility(8);
        }
        if (migrationState.getIneligible().size() > 0) {
            this.ineligibleContainer.setVisibility(0);
            this.ineligibleTitle.setText(getResources().getQuantityText(R.plurals.GroupsV1MigrationInitiation_these_members_are_not_capable_of_joining_new_groups, migrationState.getIneligible().size()));
            this.ineligibleList.setDisplayOnlyMembers(migrationState.getIneligible());
        } else {
            this.ineligibleContainer.setVisibility(8);
        }
        this.upgradeButton.setEnabled(true);
        this.spinner.setVisibility(8);
    }

    private void onUpgradeClicked() {
        this.viewModel.onUpgradeClicked().observe(getViewLifecycleOwner(), new Observer(SimpleProgressDialog.show(requireContext())) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ AlertDialog f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupsV1MigrationInitiationBottomSheetDialogFragment.this.lambda$onUpgradeClicked$2(this.f$1, (MigrationResult) obj);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$MigrationResult;

        static {
            int[] iArr = new int[MigrationResult.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$MigrationResult = iArr;
            try {
                iArr[MigrationResult.SUCCESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$MigrationResult[MigrationResult.FAILURE_GENERAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$MigrationResult[MigrationResult.FAILURE_NETWORK.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public /* synthetic */ void lambda$onUpgradeClicked$2(AlertDialog alertDialog, MigrationResult migrationResult) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$ui$migration$MigrationResult[migrationResult.ordinal()];
        if (i == 1) {
            dismiss();
        } else if (i == 2) {
            Toast.makeText(requireContext(), (int) R.string.GroupsV1MigrationInitiation_failed_to_upgrade, 0).show();
            dismiss();
        } else if (i == 3) {
            Toast.makeText(requireContext(), (int) R.string.GroupsV1MigrationInitiation_encountered_a_network_error, 0).show();
            dismiss();
        } else {
            throw new IllegalStateException();
        }
        alertDialog.dismiss();
    }
}
