package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

import android.content.Context;
import java.io.IOException;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.MembershipNotSuitableForV2Exception;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.jobs.AvatarGroupsV2DownloadJob;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.whispersystems.signalservice.api.groupsv2.GroupLinkNotActiveException;

/* loaded from: classes4.dex */
public final class GroupJoinRepository {
    private static final String TAG = Log.tag(GroupJoinRepository.class);
    private final Context context;
    private final GroupInviteLinkUrl groupInviteLinkUrl;

    public GroupJoinRepository(Context context, GroupInviteLinkUrl groupInviteLinkUrl) {
        this.context = context;
        this.groupInviteLinkUrl = groupInviteLinkUrl;
    }

    public void getGroupDetails(AsynchronousCallback.WorkerThread<GroupDetails, FetchGroupDetailsError> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(workerThread) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupJoinRepository.$r8$lambda$ZonT0Zzq8JQMtpKWqPn25Jf273Q(GroupJoinRepository.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$getGroupDetails$0(AsynchronousCallback.WorkerThread workerThread) {
        try {
            workerThread.onComplete(getGroupDetails());
        } catch (IOException unused) {
            workerThread.onError(FetchGroupDetailsError.NetworkError);
        } catch (VerificationFailedException unused2) {
            workerThread.onError(FetchGroupDetailsError.GroupLinkNotActive);
        } catch (GroupLinkNotActiveException e) {
            workerThread.onError(e.getReason() == GroupLinkNotActiveException.Reason.BANNED ? FetchGroupDetailsError.BannedFromGroup : FetchGroupDetailsError.GroupLinkNotActive);
        }
    }

    public void joinGroup(GroupDetails groupDetails, AsynchronousCallback.WorkerThread<JoinGroupSuccess, JoinGroupError> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(groupDetails, workerThread) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ GroupDetails f$1;
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupJoinRepository.$r8$lambda$LGWOPOPFIykOx0sSSkynFj9yjk4(GroupJoinRepository.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$joinGroup$1(GroupDetails groupDetails, AsynchronousCallback.WorkerThread workerThread) {
        try {
            GroupManager.GroupActionResult joinGroup = GroupManager.joinGroup(this.context, this.groupInviteLinkUrl.getGroupMasterKey(), this.groupInviteLinkUrl.getPassword(), groupDetails.getJoinInfo(), groupDetails.getAvatarBytes());
            workerThread.onComplete(new JoinGroupSuccess(joinGroup.getGroupRecipient(), joinGroup.getThreadId()));
        } catch (IOException unused) {
            workerThread.onError(JoinGroupError.NETWORK_ERROR);
        } catch (GroupChangeBusyException unused2) {
            workerThread.onError(JoinGroupError.BUSY);
        } catch (GroupChangeFailedException | MembershipNotSuitableForV2Exception unused3) {
            workerThread.onError(JoinGroupError.FAILED);
        } catch (GroupLinkNotActiveException e) {
            workerThread.onError(e.getReason() == GroupLinkNotActiveException.Reason.BANNED ? JoinGroupError.BANNED : JoinGroupError.GROUP_LINK_NOT_ACTIVE);
        }
    }

    private GroupDetails getGroupDetails() throws VerificationFailedException, IOException, GroupLinkNotActiveException {
        DecryptedGroupJoinInfo groupJoinInfoFromServer = GroupManager.getGroupJoinInfoFromServer(this.context, this.groupInviteLinkUrl.getGroupMasterKey(), this.groupInviteLinkUrl.getPassword());
        return new GroupDetails(groupJoinInfoFromServer, tryGetAvatarBytes(groupJoinInfoFromServer));
    }

    private byte[] tryGetAvatarBytes(DecryptedGroupJoinInfo decryptedGroupJoinInfo) {
        try {
            return AvatarGroupsV2DownloadJob.downloadGroupAvatarBytes(this.context, this.groupInviteLinkUrl.getGroupMasterKey(), decryptedGroupJoinInfo.getAvatar());
        } catch (IOException e) {
            Log.w(TAG, "Failed to get group avatar", e);
            return null;
        }
    }
}
