package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.navigation.fragment.NavHostFragment;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragmentArgs;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupInviteSentDialog;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class AddGroupDetailsActivity extends PassphraseRequiredActivity implements AddGroupDetailsFragment.Callback {
    private static final String EXTRA_RECIPIENTS;
    private final DynamicTheme theme = new DynamicNoActionBarTheme();

    public static Intent newIntent(Context context, Collection<RecipientId> collection) {
        Intent intent = new Intent(context, AddGroupDetailsActivity.class);
        intent.putParcelableArrayListExtra(EXTRA_RECIPIENTS, new ArrayList<>(collection));
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        this.theme.onCreate(this);
        setContentView(R.layout.add_group_details_activity);
        if (bundle == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, NavHostFragment.create(R.navigation.create_group, new AddGroupDetailsFragmentArgs.Builder((RecipientId[]) getIntent().getParcelableArrayListExtra(EXTRA_RECIPIENTS).toArray(new RecipientId[0])).build().toBundle())).commit();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.theme.onResume(this);
    }

    @Override // org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment.Callback
    public void onGroupCreated(RecipientId recipientId, long j, List<Recipient> list) {
        Dialog showInvitesSent = GroupInviteSentDialog.showInvitesSent(this, this, list);
        if (showInvitesSent != null) {
            showInvitesSent.setOnDismissListener(new DialogInterface.OnDismissListener(recipientId, j) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsActivity$$ExternalSyntheticLambda0
                public final /* synthetic */ RecipientId f$1;
                public final /* synthetic */ long f$2;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                }

                @Override // android.content.DialogInterface.OnDismissListener
                public final void onDismiss(DialogInterface dialogInterface) {
                    AddGroupDetailsActivity.this.lambda$onGroupCreated$0(this.f$1, this.f$2, dialogInterface);
                }
            });
        } else {
            goToConversation(recipientId, j);
        }
    }

    public /* synthetic */ void lambda$onGroupCreated$0(RecipientId recipientId, long j, DialogInterface dialogInterface) {
        goToConversation(recipientId, j);
    }

    void goToConversation(RecipientId recipientId, long j) {
        startActivity(ConversationIntents.createBuilder(this, recipientId, j).firstTimeInSelfCreatedGroup().build());
        setResult(-1);
        finish();
    }

    @Override // org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment.Callback
    public void onNavigationButtonPressed() {
        setResult(0);
        finish();
    }
}
