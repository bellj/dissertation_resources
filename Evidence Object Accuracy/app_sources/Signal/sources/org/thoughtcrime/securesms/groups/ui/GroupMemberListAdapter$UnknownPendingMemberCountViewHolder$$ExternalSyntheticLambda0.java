package org.thoughtcrime.securesms.groups.ui;

import android.view.MenuItem;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;
import org.thoughtcrime.securesms.groups.ui.PopupMenuView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$UnknownPendingMemberCountViewHolder$$ExternalSyntheticLambda0 implements PopupMenuView.PrepareOptionsMenuItem {
    public final /* synthetic */ GroupMemberListAdapter.UnknownPendingMemberCountViewHolder f$0;
    public final /* synthetic */ GroupMemberEntry.UnknownPendingMemberCount f$1;

    public /* synthetic */ GroupMemberListAdapter$UnknownPendingMemberCountViewHolder$$ExternalSyntheticLambda0(GroupMemberListAdapter.UnknownPendingMemberCountViewHolder unknownPendingMemberCountViewHolder, GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
        this.f$0 = unknownPendingMemberCountViewHolder;
        this.f$1 = unknownPendingMemberCount;
    }

    @Override // org.thoughtcrime.securesms.groups.ui.PopupMenuView.PrepareOptionsMenuItem
    public final boolean onPrepareOptionsMenuItem(MenuItem menuItem) {
        return this.f$0.lambda$bind$0(this.f$1, menuItem);
    }
}
