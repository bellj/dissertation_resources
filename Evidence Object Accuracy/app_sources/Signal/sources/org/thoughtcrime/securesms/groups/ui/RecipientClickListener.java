package org.thoughtcrime.securesms.groups.ui;

import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public interface RecipientClickListener {
    void onClick(Recipient recipient);
}
