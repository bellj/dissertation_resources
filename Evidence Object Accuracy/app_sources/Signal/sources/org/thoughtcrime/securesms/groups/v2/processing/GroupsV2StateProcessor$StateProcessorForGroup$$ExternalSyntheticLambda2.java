package org.thoughtcrime.securesms.groups.v2.processing;

import com.google.protobuf.ByteString;
import j$.util.function.Function;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda2 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return UuidUtil.fromByteStringOrNull((ByteString) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
