package org.thoughtcrime.securesms.groups.v2;

import com.google.protobuf.ByteString;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.util.UuidUtil;

/* loaded from: classes4.dex */
public final class ProfileKeySet {
    private static final String TAG = Log.tag(ProfileKeySet.class);
    private final Map<ServiceId, ProfileKey> authoritativeProfileKeys = new LinkedHashMap();
    private final Map<ServiceId, ProfileKey> profileKeys = new LinkedHashMap();

    public void addKeysFromGroupChange(DecryptedGroupChange decryptedGroupChange) {
        UUID fromByteStringOrNull = UuidUtil.fromByteStringOrNull(decryptedGroupChange.getEditor());
        for (DecryptedMember decryptedMember : decryptedGroupChange.getNewMembersList()) {
            addMemberKey(decryptedMember, fromByteStringOrNull);
        }
        for (DecryptedMember decryptedMember2 : decryptedGroupChange.getPromotePendingMembersList()) {
            addMemberKey(decryptedMember2, fromByteStringOrNull);
        }
        for (DecryptedMember decryptedMember3 : decryptedGroupChange.getModifiedProfileKeysList()) {
            addMemberKey(decryptedMember3, fromByteStringOrNull);
        }
        for (DecryptedRequestingMember decryptedRequestingMember : decryptedGroupChange.getNewRequestingMembersList()) {
            addMemberKey(fromByteStringOrNull, decryptedRequestingMember.getUuid(), decryptedRequestingMember.getProfileKey());
        }
    }

    public void addKeysFromGroupState(DecryptedGroup decryptedGroup) {
        for (DecryptedMember decryptedMember : decryptedGroup.getMembersList()) {
            addMemberKey(decryptedMember, null);
        }
    }

    private void addMemberKey(DecryptedMember decryptedMember, UUID uuid) {
        addMemberKey(uuid, decryptedMember.getUuid(), decryptedMember.getProfileKey());
    }

    private void addMemberKey(UUID uuid, ByteString byteString, ByteString byteString2) {
        UUID fromByteString = UuidUtil.fromByteString(byteString);
        if (UuidUtil.UNKNOWN_UUID.equals(fromByteString)) {
            Log.w(TAG, "Seen unknown member UUID");
            return;
        }
        try {
            ProfileKey profileKey = new ProfileKey(byteString2.toByteArray());
            if (fromByteString.equals(uuid)) {
                this.authoritativeProfileKeys.put(ServiceId.from(fromByteString), profileKey);
                this.profileKeys.remove(ServiceId.from(fromByteString));
            } else if (!this.authoritativeProfileKeys.containsKey(ServiceId.from(fromByteString))) {
                this.profileKeys.put(ServiceId.from(fromByteString), profileKey);
            }
        } catch (InvalidInputException unused) {
            Log.w(TAG, "Bad profile key in group");
        }
    }

    public Map<ServiceId, ProfileKey> getProfileKeys() {
        return this.profileKeys;
    }

    public Map<ServiceId, ProfileKey> getAuthoritativeProfileKeys() {
        return this.authoritativeProfileKeys;
    }
}
