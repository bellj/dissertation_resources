package org.thoughtcrime.securesms.groups.ui.addmembers;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.core.util.Consumer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.ContactSelectionActivity;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class AddMembersActivity extends PushContactSelectionActivity {
    public static final String ANNOUNCEMENT_GROUP;
    public static final String GROUP_ID;
    private View done;
    private AddMembersViewModel viewModel;

    public static Intent createIntent(Context context, GroupId groupId, int i, int i2, int i3, boolean z, List<RecipientId> list) {
        Intent intent = new Intent(context, AddMembersActivity.class);
        intent.putExtra("group_id", groupId.toString());
        intent.putExtra(ANNOUNCEMENT_GROUP, z);
        intent.putExtra("display_mode", i);
        intent.putExtra("selection_limits", new SelectionLimits(i2, i3));
        intent.putParcelableArrayListExtra("current_selection", new ArrayList<>(list));
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PushContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        getIntent().putExtra(ContactSelectionActivity.EXTRA_LAYOUT_RES_ID, R.layout.add_members_activity);
        super.onCreate(bundle, z);
        AddMembersViewModel.Factory factory = new AddMembersViewModel.Factory(getGroupId());
        this.done = findViewById(R.id.done);
        this.viewModel = (AddMembersViewModel) ViewModelProviders.of(this, factory).get(AddMembersViewModel.class);
        this.done.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersActivity$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AddMembersActivity.this.lambda$onCreate$0(view);
            }
        });
        disableDone();
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        this.viewModel.getDialogStateForSelectedContacts(this.contactsFragment.getSelectedContacts(), new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersActivity$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                AddMembersActivity.this.displayAlertMessage((AddMembersViewModel.AddMemberDialogMessageState) obj);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.PushContactSelectionActivity
    protected void initializeToolbar() {
        getToolbar().setNavigationIcon(R.drawable.ic_arrow_left_24);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersActivity$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AddMembersActivity.this.lambda$initializeToolbar$1(view);
            }
        });
    }

    public /* synthetic */ void lambda$initializeToolbar$1(View view) {
        setResult(0);
        finish();
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, j$.util.function.Consumer<Boolean> consumer) {
        if (!getGroupId().isV1() || !optional.isPresent() || Recipient.resolved(optional.get()).hasE164()) {
            if (this.contactsFragment.hasQueryFilter()) {
                getContactFilterView().clear();
            }
            enableDone();
            consumer.accept(Boolean.TRUE);
            return;
        }
        Toast.makeText(this, (int) R.string.AddMembersActivity__this_person_cant_be_added_to_legacy_groups, 0).show();
        consumer.accept(Boolean.FALSE);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
        if (this.contactsFragment.hasQueryFilter()) {
            getContactFilterView().clear();
        }
        if (this.contactsFragment.getSelectedContactsCount() < 1) {
            disableDone();
        }
    }

    @Override // org.thoughtcrime.securesms.PushContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
        int totalMemberCount = this.contactsFragment.getTotalMemberCount() + 1;
        if (totalMemberCount == 0) {
            getToolbar().setTitle(getString(R.string.AddMembersActivity__add_members));
        } else {
            getToolbar().setTitle(getResources().getQuantityString(R.plurals.CreateGroupActivity__d_members, totalMemberCount, Integer.valueOf(totalMemberCount)));
        }
    }

    private void enableDone() {
        this.done.setEnabled(true);
        this.done.animate().alpha(1.0f);
    }

    private void disableDone() {
        this.done.setEnabled(false);
        this.done.animate().alpha(0.5f);
    }

    private GroupId getGroupId() {
        return GroupId.parseOrThrow(getIntent().getStringExtra("group_id"));
    }

    private boolean isAnnouncementGroup() {
        return getIntent().getBooleanExtra(ANNOUNCEMENT_GROUP, false);
    }

    public void displayAlertMessage(AddMembersViewModel.AddMemberDialogMessageState addMemberDialogMessageState) {
        new MaterialAlertDialogBuilder(this).setMessage((CharSequence) getResources().getQuantityString(R.plurals.AddMembersActivity__add_d_members_to_s, addMemberDialogMessageState.getSelectionCount(), ((Recipient) Util.firstNonNull(addMemberDialogMessageState.getRecipient(), Recipient.UNKNOWN)).getDisplayName(this), addMemberDialogMessageState.getGroupTitle(), Integer.valueOf(addMemberDialogMessageState.getSelectionCount()))).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersActivity$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        }).setPositiveButton(R.string.AddMembersActivity__add, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersActivity$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                AddMembersActivity.this.lambda$displayAlertMessage$3(dialogInterface, i);
            }
        }).setCancelable(true).show();
    }

    public /* synthetic */ void lambda$displayAlertMessage$3(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
        onFinishedSelection();
    }
}
