package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupInsufficientRightsException extends GroupChangeException {
    public GroupInsufficientRightsException(Throwable th) {
        super(th);
    }
}
