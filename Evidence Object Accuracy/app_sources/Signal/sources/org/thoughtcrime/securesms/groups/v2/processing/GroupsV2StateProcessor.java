package org.thoughtcrime.securesms.groups.v2.processing;

import android.content.Context;
import android.text.TextUtils;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.stream.Stream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupDoesNotExistException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupMutation;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.groups.GroupProtoUtil;
import org.thoughtcrime.securesms.groups.GroupsV2Authorization;
import org.thoughtcrime.securesms.groups.v2.ProfileKeySet;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobs.AvatarGroupsV2DownloadJob;
import org.thoughtcrime.securesms.jobs.LeaveGroupV2Job;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.IncomingGroupUpdateMessage;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupHistoryEntry;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.groupsv2.GroupHistoryPage;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Api;
import org.whispersystems.signalservice.api.groupsv2.InvalidGroupStateException;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.ServiceIds;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.push.exceptions.GroupNotFoundException;
import org.whispersystems.signalservice.internal.push.exceptions.NotInGroupException;

/* loaded from: classes4.dex */
public class GroupsV2StateProcessor {
    public static final int LATEST;
    public static final int PLACEHOLDER_REVISION;
    public static final int RESTORE_PLACEHOLDER_REVISION;
    private static final String TAG = Log.tag(GroupsV2StateProcessor.class);
    private final Context context;
    private final GroupDatabase groupDatabase = SignalDatabase.groups();
    private final GroupsV2Api groupsV2Api = ApplicationDependencies.getSignalServiceAccountManager().getGroupsV2Api();
    private final GroupsV2Authorization groupsV2Authorization = ApplicationDependencies.getGroupsV2Authorization();
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();

    /* loaded from: classes4.dex */
    public enum GroupState {
        GROUP_UPDATED,
        GROUP_CONSISTENT_OR_AHEAD
    }

    public GroupsV2StateProcessor(Context context) {
        this.context = context.getApplicationContext();
    }

    public StateProcessorForGroup forGroup(ServiceIds serviceIds, GroupMasterKey groupMasterKey) {
        return new StateProcessorForGroup(serviceIds, this.context, this.groupDatabase, this.groupsV2Api, this.groupsV2Authorization, groupMasterKey, new ProfileAndMessageHelper(this.context, serviceIds.getAci(), groupMasterKey, GroupId.v2(groupMasterKey), this.recipientDatabase));
    }

    /* loaded from: classes4.dex */
    public static class GroupUpdateResult {
        private final GroupState groupState;
        private final DecryptedGroup latestServer;

        GroupUpdateResult(GroupState groupState, DecryptedGroup decryptedGroup) {
            this.groupState = groupState;
            this.latestServer = decryptedGroup;
        }

        public GroupState getGroupState() {
            return this.groupState;
        }

        public DecryptedGroup getLatestServer() {
            return this.latestServer;
        }
    }

    /* loaded from: classes4.dex */
    public static final class StateProcessorForGroup {
        private final Context context;
        private final GroupDatabase groupDatabase;
        private final GroupId.V2 groupId;
        private final GroupSecretParams groupSecretParams;
        private final GroupsV2Api groupsV2Api;
        private final GroupsV2Authorization groupsV2Authorization;
        private final GroupMasterKey masterKey;
        private final ProfileAndMessageHelper profileAndMessageHelper;
        private final ServiceIds serviceIds;

        StateProcessorForGroup(ServiceIds serviceIds, Context context, GroupDatabase groupDatabase, GroupsV2Api groupsV2Api, GroupsV2Authorization groupsV2Authorization, GroupMasterKey groupMasterKey, ProfileAndMessageHelper profileAndMessageHelper) {
            this.serviceIds = serviceIds;
            this.context = context;
            this.groupDatabase = groupDatabase;
            this.groupsV2Api = groupsV2Api;
            this.groupsV2Authorization = groupsV2Authorization;
            this.masterKey = groupMasterKey;
            this.groupId = GroupId.v2(groupMasterKey);
            this.groupSecretParams = GroupSecretParams.deriveFromMasterKey(groupMasterKey);
            this.profileAndMessageHelper = profileAndMessageHelper;
        }

        /* JADX WARNING: Removed duplicated region for block: B:27:0x0080  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor.GroupUpdateResult updateLocalGroupToRevision(int r12, long r13, org.signal.storageservice.protos.groups.local.DecryptedGroupChange r15) throws java.io.IOException, org.thoughtcrime.securesms.groups.GroupNotAMemberException {
            /*
            // Method dump skipped, instructions count: 412
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor.StateProcessorForGroup.updateLocalGroupToRevision(int, long, org.signal.storageservice.protos.groups.local.DecryptedGroupChange):org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor$GroupUpdateResult");
        }

        public static /* synthetic */ DecryptedGroup lambda$updateLocalGroupToRevision$0(GroupDatabase.GroupRecord groupRecord) {
            return groupRecord.requireV2GroupProperties().getDecryptedGroup();
        }

        private boolean notInGroupAndNotBeingAdded(Optional<GroupDatabase.GroupRecord> optional, DecryptedGroupChange decryptedGroupChange) {
            boolean z = optional.isPresent() && optional.get().isActive();
            Stream filter = Collection$EL.stream(decryptedGroupChange.getNewMembersList()).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda5()).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda2()).filter(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda6());
            ServiceIds serviceIds = this.serviceIds;
            Objects.requireNonNull(serviceIds);
            boolean anyMatch = filter.anyMatch(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda4(serviceIds));
            Stream filter2 = Collection$EL.stream(decryptedGroupChange.getNewPendingMembersList()).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda7()).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda2()).filter(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda8());
            ServiceIds serviceIds2 = this.serviceIds;
            Objects.requireNonNull(serviceIds2);
            boolean anyMatch2 = filter2.anyMatch(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda4(serviceIds2));
            if (z || anyMatch || anyMatch2) {
                return false;
            }
            return true;
        }

        private boolean notHavingInviteRevoked(DecryptedGroupChange decryptedGroupChange) {
            Stream filter = Collection$EL.stream(decryptedGroupChange.getDeletePendingMembersList()).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda1()).map(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda2()).filter(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda3());
            ServiceIds serviceIds = this.serviceIds;
            Objects.requireNonNull(serviceIds);
            return !filter.anyMatch(new GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda4(serviceIds));
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v16, resolved type: org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor$ProfileAndMessageHelper */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX WARN: Type inference failed for: r10v4 */
        /* JADX WARN: Type inference failed for: r10v5, types: [org.signal.storageservice.protos.groups.local.DecryptedGroup, org.signal.storageservice.protos.groups.local.DecryptedGroupChange] */
        /* JADX WARN: Type inference failed for: r10v8 */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor.GroupUpdateResult updateLocalGroupFromServerPaged(int r19, org.signal.storageservice.protos.groups.local.DecryptedGroup r20, long r21, boolean r23) throws java.io.IOException, org.thoughtcrime.securesms.groups.GroupNotAMemberException {
            /*
            // Method dump skipped, instructions count: 723
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor.StateProcessorForGroup.updateLocalGroupFromServerPaged(int, org.signal.storageservice.protos.groups.local.DecryptedGroup, long, boolean):org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor$GroupUpdateResult");
        }

        public DecryptedGroup getCurrentGroupStateFromServer() throws IOException, GroupNotAMemberException, GroupDoesNotExistException {
            Throwable e;
            try {
                GroupsV2Api groupsV2Api = this.groupsV2Api;
                GroupSecretParams groupSecretParams = this.groupSecretParams;
                return groupsV2Api.getGroup(groupSecretParams, this.groupsV2Authorization.getAuthorizationForToday(this.serviceIds, groupSecretParams));
            } catch (VerificationFailedException e2) {
                e = e2;
                throw new IOException(e);
            } catch (InvalidGroupStateException e3) {
                e = e3;
                throw new IOException(e);
            } catch (GroupNotFoundException e4) {
                throw new GroupDoesNotExistException(e4);
            } catch (NotInGroupException e5) {
                throw new GroupNotAMemberException(e5);
            }
        }

        public DecryptedGroup getSpecificVersionFromServer(int i) throws IOException, GroupNotAMemberException, GroupDoesNotExistException {
            Throwable e;
            try {
                GroupsV2Api groupsV2Api = this.groupsV2Api;
                GroupSecretParams groupSecretParams = this.groupSecretParams;
                return groupsV2Api.getGroupHistoryPage(groupSecretParams, i, this.groupsV2Authorization.getAuthorizationForToday(this.serviceIds, groupSecretParams), true).getResults().get(0).getGroup().orElse(null);
            } catch (VerificationFailedException e2) {
                e = e2;
                throw new IOException(e);
            } catch (InvalidGroupStateException e3) {
                e = e3;
                throw new IOException(e);
            } catch (GroupNotFoundException e4) {
                throw new GroupDoesNotExistException(e4);
            } catch (NotInGroupException e5) {
                throw new GroupNotAMemberException(e5);
            }
        }

        private void insertGroupLeave() {
            if (!this.groupDatabase.isActive(this.groupId)) {
                warn("Group has already been left.");
                return;
            }
            Recipient externalGroupExact = Recipient.externalGroupExact(this.groupId);
            UUID uuid = this.serviceIds.getAci().uuid();
            DecryptedGroup decryptedGroup = this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getDecryptedGroup();
            DecryptedGroup removeMember = DecryptedGroupUtil.removeMember(decryptedGroup, uuid, decryptedGroup.getRevision() + 1);
            OutgoingGroupUpdateMessage outgoingGroupUpdateMessage = new OutgoingGroupUpdateMessage(externalGroupExact, GroupProtoUtil.createDecryptedGroupV2Context(this.masterKey, new GroupMutation(decryptedGroup, DecryptedGroupChange.newBuilder().setEditor(UuidUtil.toByteString(UuidUtil.UNKNOWN_UUID)).setRevision(removeMember.getRevision()).addDeleteMembers(UuidUtil.toByteString(uuid)).build(), removeMember), null), (Attachment) null, System.currentTimeMillis(), 0L, false, (QuoteModel) null, (List<Contact>) Collections.emptyList(), (List<LinkPreview>) Collections.emptyList(), (List<Mention>) Collections.emptyList());
            try {
                MmsDatabase mms = SignalDatabase.mms();
                ThreadDatabase threads = SignalDatabase.threads();
                long orCreateThreadIdFor = threads.getOrCreateThreadIdFor(externalGroupExact);
                mms.markAsSent(mms.insertMessageOutbox(outgoingGroupUpdateMessage, orCreateThreadIdFor, false, null), true);
                threads.update(orCreateThreadIdFor, false, false);
            } catch (MmsException e) {
                warn("Failed to insert leave message.", e);
            }
            this.groupDatabase.setActive(this.groupId, false);
            this.groupDatabase.remove(this.groupId, Recipient.self().getId());
        }

        private boolean localIsAtLeast(int i) {
            if (this.groupDatabase.isUnknownGroup(this.groupId) || i == Integer.MAX_VALUE || i > this.groupDatabase.getGroup(this.groupId).get().requireV2GroupProperties().getGroupRevision()) {
                return false;
            }
            return true;
        }

        private void updateLocalDatabaseGroupState(GlobalGroupState globalGroupState, DecryptedGroup decryptedGroup) {
            boolean z;
            if (globalGroupState.getLocalState() == null) {
                this.groupDatabase.create(this.masterKey, decryptedGroup);
                z = TextUtils.isEmpty(decryptedGroup.getAvatar());
            } else {
                this.groupDatabase.update(this.masterKey, decryptedGroup);
                z = decryptedGroup.getAvatar().equals(globalGroupState.getLocalState().getAvatar());
            }
            if (!z) {
                ApplicationDependencies.getJobManager().add(new AvatarGroupsV2DownloadJob(this.groupId, decryptedGroup.getAvatar()));
            }
            this.profileAndMessageHelper.determineProfileSharing(globalGroupState, decryptedGroup);
        }

        private GlobalGroupState getFullMemberHistoryPage(DecryptedGroup decryptedGroup, int i, boolean z) throws IOException {
            try {
                GroupsV2Api groupsV2Api = this.groupsV2Api;
                GroupSecretParams groupSecretParams = this.groupSecretParams;
                GroupHistoryPage groupHistoryPage = groupsV2Api.getGroupHistoryPage(groupSecretParams, i, this.groupsV2Authorization.getAuthorizationForToday(this.serviceIds, groupSecretParams), z);
                ArrayList arrayList = new ArrayList(groupHistoryPage.getResults().size());
                boolean gv2IgnoreServerChanges = SignalStore.internalValues().gv2IgnoreServerChanges();
                if (gv2IgnoreServerChanges) {
                    warn("Server change logs are ignored by setting");
                }
                for (DecryptedGroupHistoryEntry decryptedGroupHistoryEntry : groupHistoryPage.getResults()) {
                    DecryptedGroupChange decryptedGroupChange = null;
                    DecryptedGroup orElse = decryptedGroupHistoryEntry.getGroup().orElse(null);
                    if (!gv2IgnoreServerChanges) {
                        decryptedGroupChange = decryptedGroupHistoryEntry.getChange().orElse(null);
                    }
                    if (orElse != null || decryptedGroupChange != null) {
                        arrayList.add(new ServerGroupLogEntry(orElse, decryptedGroupChange));
                    }
                }
                return new GlobalGroupState(decryptedGroup, arrayList, groupHistoryPage.getPagingData());
            } catch (VerificationFailedException | InvalidGroupStateException e) {
                throw new IOException(e);
            }
        }

        private void info(String str) {
            info(str, null);
        }

        private void info(String str, Throwable th) {
            String str2 = GroupsV2StateProcessor.TAG;
            Log.i(str2, "[" + this.groupId.toString() + "] " + str, th);
        }

        private void warn(String str) {
            warn(str, null);
        }

        private void warn(String str, Throwable th) {
            String str2 = GroupsV2StateProcessor.TAG;
            Log.w(str2, "[" + this.groupId.toString() + "] " + str, th);
        }
    }

    /* loaded from: classes4.dex */
    public static class ProfileAndMessageHelper {
        private final Context context;
        private final GroupId.V2 groupId;
        private final GroupMasterKey masterKey;
        private final RecipientDatabase recipientDatabase;
        private final ServiceId serviceId;

        ProfileAndMessageHelper(Context context, ServiceId serviceId, GroupMasterKey groupMasterKey, GroupId.V2 v2, RecipientDatabase recipientDatabase) {
            this.context = context;
            this.serviceId = serviceId;
            this.masterKey = groupMasterKey;
            this.groupId = v2;
            this.recipientDatabase = recipientDatabase;
        }

        void determineProfileSharing(GlobalGroupState globalGroupState, DecryptedGroup decryptedGroup) {
            if (globalGroupState.getLocalState() == null || !DecryptedGroupUtil.findMemberByUuid(globalGroupState.getLocalState().getMembersList(), this.serviceId.uuid()).isPresent()) {
                Optional<DecryptedMember> findMemberByUuid = DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), this.serviceId.uuid());
                Optional<DecryptedPendingMember> findPendingByUuid = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), this.serviceId.uuid());
                if (findMemberByUuid.isPresent()) {
                    Optional optional = (Optional) com.annimon.stream.Stream.of(globalGroupState.getServerHistory()).map(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda0()).filter(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda1(findMemberByUuid.get().getJoinedAtRevision())).findFirst().map(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda2()).orElse(Optional.empty());
                    if (optional.isPresent()) {
                        Recipient recipient = (Recipient) optional.get();
                        Log.i(GroupsV2StateProcessor.TAG, String.format("Added as a full member of %s by %s", this.groupId, recipient.getId()));
                        if (recipient.isBlocked() && (globalGroupState.getLocalState() == null || !DecryptedGroupUtil.isRequesting(globalGroupState.getLocalState(), this.serviceId.uuid()))) {
                            Log.i(GroupsV2StateProcessor.TAG, "Added by a blocked user. Leaving group.");
                            ApplicationDependencies.getJobManager().add(new LeaveGroupV2Job(this.groupId));
                        } else if (recipient.isSystemContact() || recipient.isProfileSharing()) {
                            String str = GroupsV2StateProcessor.TAG;
                            Log.i(str, "Group 'adder' is trusted. contact: " + recipient.isSystemContact() + ", profileSharing: " + recipient.isProfileSharing());
                            Log.i(GroupsV2StateProcessor.TAG, "Added to a group and auto-enabling profile sharing");
                            this.recipientDatabase.setProfileSharing(Recipient.externalGroupExact(this.groupId).getId(), true);
                        } else {
                            Log.i(GroupsV2StateProcessor.TAG, "Added to a group, but not enabling profile sharing, as 'adder' is not trusted");
                        }
                    } else {
                        Log.w(GroupsV2StateProcessor.TAG, "Could not find founding member during gv2 create. Not enabling profile sharing.");
                    }
                } else if (findPendingByUuid.isPresent()) {
                    Optional<U> flatMap = findPendingByUuid.flatMap(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda3());
                    if (!flatMap.isPresent() || !((Recipient) flatMap.get()).isBlocked()) {
                        Log.i(GroupsV2StateProcessor.TAG, String.format("Added to %s, but not enabling profile sharing as we are a pending member.", this.groupId));
                        return;
                    }
                    Log.i(GroupsV2StateProcessor.TAG, String.format("Added to group %s by a blocked user %s. Leaving group.", this.groupId, ((Recipient) flatMap.get()).getId()));
                    ApplicationDependencies.getJobManager().add(new LeaveGroupV2Job(this.groupId));
                } else {
                    Log.i(GroupsV2StateProcessor.TAG, String.format("Added to %s, but not enabling profile sharing as not a fullMember.", this.groupId));
                }
            }
        }

        public static /* synthetic */ boolean lambda$determineProfileSharing$0(int i, DecryptedGroupChange decryptedGroupChange) {
            return decryptedGroupChange != null && decryptedGroupChange.getRevision() == i;
        }

        public static /* synthetic */ Optional lambda$determineProfileSharing$2(DecryptedGroupChange decryptedGroupChange) {
            return Optional.ofNullable(UuidUtil.fromByteStringOrNull(decryptedGroupChange.getEditor())).map(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda4());
        }

        public static /* synthetic */ Recipient lambda$determineProfileSharing$1(UUID uuid) {
            return Recipient.externalPush(ServiceId.from(uuid));
        }

        public static /* synthetic */ Optional lambda$determineProfileSharing$4(DecryptedPendingMember decryptedPendingMember) {
            return Optional.ofNullable(UuidUtil.fromByteStringOrNull(decryptedPendingMember.getAddedByUuid())).map(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda5());
        }

        public static /* synthetic */ Recipient lambda$determineProfileSharing$3(UUID uuid) {
            return Recipient.externalPush(ServiceId.from(uuid));
        }

        long insertUpdateMessages(long j, DecryptedGroup decryptedGroup, Collection<LocalGroupLogEntry> collection) {
            for (LocalGroupLogEntry localGroupLogEntry : collection) {
                if (localGroupLogEntry.getChange() != null && DecryptedGroupUtil.changeIsEmptyExceptForProfileKeyChanges(localGroupLogEntry.getChange()) && !DecryptedGroupUtil.changeIsEmpty(localGroupLogEntry.getChange())) {
                    Log.d(GroupsV2StateProcessor.TAG, "Skipping profile key changes only update message");
                } else if (localGroupLogEntry.getChange() != null && DecryptedGroupUtil.changeIsEmptyExceptForBanChangesAndOptionalProfileKeyChanges(localGroupLogEntry.getChange())) {
                    Log.d(GroupsV2StateProcessor.TAG, "Skipping ban changes only update message");
                } else if (localGroupLogEntry.getChange() == null || !DecryptedGroupUtil.changeIsEmpty(localGroupLogEntry.getChange()) || decryptedGroup == null) {
                    storeMessage(GroupProtoUtil.createDecryptedGroupV2Context(this.masterKey, new GroupMutation(decryptedGroup, localGroupLogEntry.getChange(), localGroupLogEntry.getGroup()), null), j);
                    j++;
                } else {
                    Log.w(GroupsV2StateProcessor.TAG, "Empty group update message seen. Not inserting.");
                }
                decryptedGroup = localGroupLogEntry.getGroup();
            }
            return j;
        }

        void persistLearnedProfileKeys(GlobalGroupState globalGroupState) {
            ProfileKeySet profileKeySet = new ProfileKeySet();
            for (ServerGroupLogEntry serverGroupLogEntry : globalGroupState.getServerHistory()) {
                if (serverGroupLogEntry.getGroup() != null) {
                    profileKeySet.addKeysFromGroupState(serverGroupLogEntry.getGroup());
                }
                if (serverGroupLogEntry.getChange() != null) {
                    profileKeySet.addKeysFromGroupChange(serverGroupLogEntry.getChange());
                }
            }
            persistLearnedProfileKeys(profileKeySet);
        }

        void persistLearnedProfileKeys(ProfileKeySet profileKeySet) {
            Set<RecipientId> persistProfileKeySet = this.recipientDatabase.persistProfileKeySet(profileKeySet);
            if (!persistProfileKeySet.isEmpty()) {
                Log.i(GroupsV2StateProcessor.TAG, String.format(Locale.US, "Learned %d new profile keys, fetching profiles", Integer.valueOf(persistProfileKeySet.size())));
                for (Job job : RetrieveProfileJob.forRecipients(persistProfileKeySet)) {
                    ApplicationDependencies.getJobManager().runSynchronously(job, 5000);
                }
            }
        }

        void storeMessage(DecryptedGroupV2Context decryptedGroupV2Context, long j) {
            Optional<U> map = getEditor(decryptedGroupV2Context).map(new GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda6());
            if (!map.isPresent() || this.serviceId.equals(map.get())) {
                try {
                    MmsDatabase mms = SignalDatabase.mms();
                    ThreadDatabase threads = SignalDatabase.threads();
                    Recipient resolved = Recipient.resolved(this.recipientDatabase.getOrInsertFromGroupId(this.groupId));
                    OutgoingGroupUpdateMessage outgoingGroupUpdateMessage = new OutgoingGroupUpdateMessage(resolved, decryptedGroupV2Context, (Attachment) null, j, 0L, false, (QuoteModel) null, (List<Contact>) Collections.emptyList(), (List<LinkPreview>) Collections.emptyList(), (List<Mention>) Collections.emptyList());
                    long orCreateThreadIdFor = threads.getOrCreateThreadIdFor(resolved);
                    mms.markAsSent(mms.insertMessageOutbox(outgoingGroupUpdateMessage, orCreateThreadIdFor, false, null), true);
                    threads.update(orCreateThreadIdFor, false, false);
                } catch (MmsException e) {
                    Log.w(GroupsV2StateProcessor.TAG, e);
                }
            } else {
                Optional<MessageDatabase.InsertResult> insertMessageInbox = SignalDatabase.sms().insertMessageInbox(new IncomingGroupUpdateMessage(new IncomingTextMessage(RecipientId.from((ServiceId) map.get()), -1, j, j, j, "", Optional.of(this.groupId), 0, false, null), decryptedGroupV2Context));
                if (insertMessageInbox.isPresent()) {
                    SignalDatabase.threads().update(insertMessageInbox.get().getThreadId(), false, false);
                } else {
                    Log.w(GroupsV2StateProcessor.TAG, "Could not insert update message");
                }
            }
        }

        private Optional<UUID> getEditor(DecryptedGroupV2Context decryptedGroupV2Context) {
            Optional<UUID> editorUuid = DecryptedGroupUtil.editorUuid(decryptedGroupV2Context.getChange());
            if (editorUuid.isPresent()) {
                return editorUuid;
            }
            Optional<DecryptedPendingMember> findPendingByUuid = DecryptedGroupUtil.findPendingByUuid(decryptedGroupV2Context.getGroupState().getPendingMembersList(), this.serviceId.uuid());
            if (findPendingByUuid.isPresent()) {
                return Optional.ofNullable(UuidUtil.fromByteStringOrNull(findPendingByUuid.get().getAddedByUuid()));
            }
            return Optional.empty();
        }
    }
}
