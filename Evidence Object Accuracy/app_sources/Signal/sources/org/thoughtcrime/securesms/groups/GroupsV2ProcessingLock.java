package org.thoughtcrime.securesms.groups;

import java.io.Closeable;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class GroupsV2ProcessingLock {
    private static final String TAG = Log.tag(GroupsV2ProcessingLock.class);
    private static final Lock lock = new ReentrantLock();

    private GroupsV2ProcessingLock() {
    }

    public static Closeable acquireGroupProcessingLock() throws GroupChangeBusyException {
        return acquireGroupProcessingLock(5000);
    }

    static Closeable acquireGroupProcessingLock(long j) throws GroupChangeBusyException {
        ThreadUtil.assertNotMainThread();
        try {
            Lock lock2 = lock;
            if (lock2.tryLock(j, TimeUnit.MILLISECONDS)) {
                Objects.requireNonNull(lock2);
                return new Closeable(lock2) { // from class: org.thoughtcrime.securesms.groups.GroupsV2ProcessingLock$$ExternalSyntheticLambda0
                    public final /* synthetic */ Lock f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // java.io.Closeable, java.lang.AutoCloseable
                    public final void close() {
                        this.f$0.unlock();
                    }
                };
            }
            throw new GroupChangeBusyException("Failed to get a lock on the group processing in the timeout period");
        } catch (InterruptedException e) {
            Log.w(TAG, e);
            throw new GroupChangeBusyException(e);
        }
    }
}
