package org.thoughtcrime.securesms.groups.ui.chooseadmin;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupChangeResult;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class ChooseNewAdminViewModel extends ViewModel {
    private final GroupId.V2 groupId;
    private final LiveGroup liveGroup;
    private final ChooseNewAdminRepository repository;
    private final MutableLiveData<Set<GroupMemberEntry.FullMember>> selection = new MutableLiveData<>(Collections.emptySet());

    public ChooseNewAdminViewModel(GroupId.V2 v2, ChooseNewAdminRepository chooseNewAdminRepository) {
        this.groupId = v2;
        this.repository = chooseNewAdminRepository;
        this.liveGroup = new LiveGroup(v2);
    }

    public LiveData<List<GroupMemberEntry.FullMember>> getNonAdminFullMembers() {
        return this.liveGroup.getNonAdminFullMembers();
    }

    public LiveData<Set<GroupMemberEntry.FullMember>> getSelection() {
        return this.selection;
    }

    public void setSelection(Set<GroupMemberEntry.FullMember> set) {
        this.selection.setValue(set);
    }

    public static /* synthetic */ RecipientId lambda$updateAdminsAndLeave$0(GroupMemberEntry.FullMember fullMember) {
        return fullMember.getMember().getId();
    }

    public void updateAdminsAndLeave(Consumer<GroupChangeResult> consumer) {
        ChooseNewAdminViewModel$$ExternalSyntheticLambda1 chooseNewAdminViewModel$$ExternalSyntheticLambda1 = new SimpleTask.BackgroundTask(Stream.of(this.selection.getValue()).map(new Function() { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChooseNewAdminViewModel.lambda$updateAdminsAndLeave$0((GroupMemberEntry.FullMember) obj);
            }
        }).toList()) { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ChooseNewAdminViewModel.this.lambda$updateAdminsAndLeave$1(this.f$1);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(chooseNewAdminViewModel$$ExternalSyntheticLambda1, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminViewModel$$ExternalSyntheticLambda2
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((GroupChangeResult) obj);
            }
        });
    }

    public /* synthetic */ GroupChangeResult lambda$updateAdminsAndLeave$1(List list) {
        return this.repository.updateAdminsAndLeave(this.groupId, list);
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final GroupId.V2 groupId;

        public Factory(GroupId.V2 v2) {
            this.groupId = v2;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new ChooseNewAdminViewModel(this.groupId, new ChooseNewAdminRepository(ApplicationDependencies.getApplication())));
        }
    }
}
