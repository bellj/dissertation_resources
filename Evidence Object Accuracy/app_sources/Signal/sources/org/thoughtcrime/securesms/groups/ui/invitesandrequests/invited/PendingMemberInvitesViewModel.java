package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited;

import android.content.Context;
import android.widget.Toast;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesRepository;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;

/* loaded from: classes4.dex */
public class PendingMemberInvitesViewModel extends ViewModel {
    private final Context context;
    private final PendingMemberInvitesRepository pendingMemberRepository;
    private final DefaultValueLiveData<List<GroupMemberEntry.UnknownPendingMemberCount>> whoOthersInvited;
    private final DefaultValueLiveData<List<GroupMemberEntry.PendingMember>> whoYouInvited;

    private PendingMemberInvitesViewModel(Context context, PendingMemberInvitesRepository pendingMemberInvitesRepository) {
        this.whoYouInvited = new DefaultValueLiveData<>(Collections.emptyList());
        this.whoOthersInvited = new DefaultValueLiveData<>(Collections.emptyList());
        this.context = context;
        this.pendingMemberRepository = pendingMemberInvitesRepository;
        pendingMemberInvitesRepository.getInvitees(new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                PendingMemberInvitesViewModel.this.setMembers((PendingMemberInvitesRepository.InviteeResult) obj);
            }
        });
    }

    public LiveData<List<GroupMemberEntry.PendingMember>> getWhoYouInvited() {
        return this.whoYouInvited;
    }

    public LiveData<List<GroupMemberEntry.UnknownPendingMemberCount>> getWhoOthersInvited() {
        return this.whoOthersInvited;
    }

    private void setInvitees(List<GroupMemberEntry.PendingMember> list, List<GroupMemberEntry.UnknownPendingMemberCount> list2) {
        this.whoYouInvited.postValue(list);
        this.whoOthersInvited.postValue(list2);
    }

    public void setMembers(PendingMemberInvitesRepository.InviteeResult inviteeResult) {
        ArrayList arrayList = new ArrayList(inviteeResult.getByMe().size());
        ArrayList arrayList2 = new ArrayList(inviteeResult.getByOthers().size());
        for (PendingMemberInvitesRepository.SinglePendingMemberInvitedByYou singlePendingMemberInvitedByYou : inviteeResult.getByMe()) {
            arrayList.add(new GroupMemberEntry.PendingMember(singlePendingMemberInvitedByYou.getInvitee(), singlePendingMemberInvitedByYou.getInviteeCipherText(), inviteeResult.isCanRevokeInvites()));
        }
        for (PendingMemberInvitesRepository.MultiplePendingMembersInvitedByAnother multiplePendingMembersInvitedByAnother : inviteeResult.getByOthers()) {
            arrayList2.add(new GroupMemberEntry.UnknownPendingMemberCount(multiplePendingMembersInvitedByAnother.getInviter(), multiplePendingMembersInvitedByAnother.getUuidCipherTexts(), inviteeResult.isCanRevokeInvites()));
        }
        setInvitees(arrayList, arrayList2);
    }

    public void revokeInviteFor(GroupMemberEntry.PendingMember pendingMember) {
        InviteRevokeConfirmationDialog.showOwnInviteRevokeConfirmationDialog(this.context, pendingMember.getInvitee(), new Runnable(pendingMember, pendingMember.getInviteeCipherText()) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ GroupMemberEntry.PendingMember f$1;
            public final /* synthetic */ UuidCiphertext f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PendingMemberInvitesViewModel.this.lambda$revokeInviteFor$2(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$revokeInviteFor$2(GroupMemberEntry.PendingMember pendingMember, UuidCiphertext uuidCiphertext) {
        SimpleTask.run(new SimpleTask.BackgroundTask(pendingMember, uuidCiphertext) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda4
            public final /* synthetic */ GroupMemberEntry.PendingMember f$1;
            public final /* synthetic */ UuidCiphertext f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return PendingMemberInvitesViewModel.this.lambda$revokeInviteFor$0(this.f$1, this.f$2);
            }
        }, new SimpleTask.ForegroundTask(uuidCiphertext) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ UuidCiphertext f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                PendingMemberInvitesViewModel.this.lambda$revokeInviteFor$1(this.f$1, (Boolean) obj);
            }
        });
    }

    public /* synthetic */ Boolean lambda$revokeInviteFor$0(GroupMemberEntry.PendingMember pendingMember, UuidCiphertext uuidCiphertext) {
        pendingMember.setBusy(true);
        try {
            return Boolean.valueOf(this.pendingMemberRepository.revokeInvites(Collections.singleton(uuidCiphertext)));
        } finally {
            pendingMember.setBusy(false);
        }
    }

    public /* synthetic */ void lambda$revokeInviteFor$1(UuidCiphertext uuidCiphertext, Boolean bool) {
        if (bool.booleanValue()) {
            ArrayList arrayList = new ArrayList(this.whoYouInvited.getValue());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (((GroupMemberEntry.PendingMember) it.next()).getInviteeCipherText().equals(uuidCiphertext)) {
                    it.remove();
                }
            }
            this.whoYouInvited.setValue(arrayList);
            return;
        }
        toastErrorCanceling(1);
    }

    public void revokeInvitesFor(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
        InviteRevokeConfirmationDialog.showOthersInviteRevokeConfirmationDialog(this.context, unknownPendingMemberCount.getInviter(), unknownPendingMemberCount.getInviteCount(), new Runnable(unknownPendingMemberCount) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda6
            public final /* synthetic */ GroupMemberEntry.UnknownPendingMemberCount f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PendingMemberInvitesViewModel.this.lambda$revokeInvitesFor$5(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$revokeInvitesFor$5(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
        SimpleTask.run(new SimpleTask.BackgroundTask(unknownPendingMemberCount) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ GroupMemberEntry.UnknownPendingMemberCount f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return PendingMemberInvitesViewModel.this.lambda$revokeInvitesFor$3(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(unknownPendingMemberCount) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ GroupMemberEntry.UnknownPendingMemberCount f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                PendingMemberInvitesViewModel.this.lambda$revokeInvitesFor$4(this.f$1, (Boolean) obj);
            }
        });
    }

    public /* synthetic */ Boolean lambda$revokeInvitesFor$3(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
        unknownPendingMemberCount.setBusy(true);
        try {
            return Boolean.valueOf(this.pendingMemberRepository.revokeInvites(unknownPendingMemberCount.getCiphertexts()));
        } finally {
            unknownPendingMemberCount.setBusy(false);
        }
    }

    public /* synthetic */ void lambda$revokeInvitesFor$4(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount, Boolean bool) {
        if (bool.booleanValue()) {
            ArrayList arrayList = new ArrayList(this.whoOthersInvited.getValue());
            Iterator it = arrayList.iterator();
            while (it.hasNext()) {
                if (((GroupMemberEntry.UnknownPendingMemberCount) it.next()).getInviter().equals(unknownPendingMemberCount.getInviter())) {
                    it.remove();
                }
            }
            this.whoOthersInvited.setValue(arrayList);
            return;
        }
        toastErrorCanceling(unknownPendingMemberCount.getInviteCount());
    }

    private void toastErrorCanceling(int i) {
        Context context = this.context;
        Toast.makeText(context, context.getResources().getQuantityText(R.plurals.PendingMembersActivity_error_revoking_invite, i), 0).show();
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final Context context;
        private final GroupId.V2 groupId;

        public Factory(Context context, GroupId.V2 v2) {
            this.context = context;
            this.groupId = v2;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new PendingMemberInvitesViewModel(this.context, new PendingMemberInvitesRepository(this.context.getApplicationContext(), this.groupId));
        }
    }
}
