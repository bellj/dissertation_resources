package org.thoughtcrime.securesms.groups.ui;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda1 implements Observer {
    public final /* synthetic */ GroupMemberListAdapter.ViewHolder f$0;

    public /* synthetic */ GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda1(GroupMemberListAdapter.ViewHolder viewHolder) {
        this.f$0 = viewHolder;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.onBusyChanged(((Boolean) obj).booleanValue());
    }
}
