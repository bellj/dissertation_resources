package org.thoughtcrime.securesms.groups.v2.processing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/* loaded from: classes4.dex */
public final class StateChain<State, Delta> {
    private final AddDelta<State, Delta> add;
    private final List<Pair<State, Delta>> pairs = new LinkedList();
    private final StateEquality<State> stateEquality;
    private final SubtractStates<State, Delta> subtract;

    /* loaded from: classes4.dex */
    public interface AddDelta<State, Delta> {
        State add(State state, Delta delta);
    }

    /* loaded from: classes4.dex */
    public interface StateEquality<State> {
        boolean equals(State state, State state2);
    }

    /* loaded from: classes4.dex */
    public interface SubtractStates<State, Delta> {
        Delta subtract(State state, State state2);
    }

    public StateChain(AddDelta<State, Delta> addDelta, SubtractStates<State, Delta> subtractStates, StateEquality<State> stateEquality) {
        this.add = addDelta;
        this.subtract = subtractStates;
        this.stateEquality = stateEquality;
    }

    public void push(State state, Delta delta) {
        if (delta != null || state != null) {
            boolean z = (state == null || delta == null) ? false : true;
            State latestState = getLatestState();
            if (latestState != null || state != null) {
                if (latestState != null) {
                    if (delta == null) {
                        delta = this.subtract.subtract(state, latestState);
                    }
                    if (state != null || (state = this.add.add(latestState, delta)) != null) {
                        if (z) {
                            State add = this.add.add(latestState, delta);
                            if (add == null) {
                                push(state, null);
                                return;
                            } else if (!this.stateEquality.equals(state, add)) {
                                push(null, delta);
                                push(state, null);
                                return;
                            }
                        }
                    } else {
                        return;
                    }
                }
                if (latestState == null || !this.stateEquality.equals(latestState, state)) {
                    this.pairs.add(new Pair<>(state, delta));
                }
            }
        }
    }

    public State getLatestState() {
        int size = this.pairs.size();
        if (size == 0) {
            return null;
        }
        return this.pairs.get(size - 1).getState();
    }

    public List<Pair<State, Delta>> getList() {
        return new ArrayList(this.pairs);
    }

    /* loaded from: classes4.dex */
    public static final class Pair<State, Delta> {
        private final Delta delta;
        private final State state;

        Pair(State state, Delta delta) {
            this.state = state;
            this.delta = delta;
        }

        public State getState() {
            return this.state;
        }

        public Delta getDelta() {
            return this.delta;
        }

        public String toString() {
            return String.format("(%s, %s)", this.state, this.delta);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Pair.class != obj.getClass()) {
                return false;
            }
            Pair pair = (Pair) obj;
            if (!this.state.equals(pair.state) || !Objects.equals(this.delta, pair.delta)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int hashCode = this.state.hashCode() * 31;
            Delta delta = this.delta;
            return hashCode + (delta != null ? delta.hashCode() : 0);
        }
    }
}
