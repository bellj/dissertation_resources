package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinViewModel;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupDescriptionDialog;
import org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LongClickMovementMethod;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes4.dex */
public final class GroupJoinBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private static final String ARG_GROUP_INVITE_LINK_URL;
    private static final String TAG = Log.tag(GroupJoinBottomSheetDialogFragment.class);
    private AvatarImageView avatar;
    private ProgressBar busy;
    private Button groupCancelButton;
    private EmojiTextView groupDescription;
    private TextView groupDetails;
    private Button groupJoinButton;
    private TextView groupJoinExplain;
    private TextView groupName;

    public static void show(FragmentManager fragmentManager, GroupInviteLinkUrl groupInviteLinkUrl) {
        GroupJoinBottomSheetDialogFragment groupJoinBottomSheetDialogFragment = new GroupJoinBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(ARG_GROUP_INVITE_LINK_URL, groupInviteLinkUrl.getUrl());
        groupJoinBottomSheetDialogFragment.setArguments(bundle);
        groupJoinBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.group_join_bottom_sheet, viewGroup, false);
        this.groupCancelButton = (Button) inflate.findViewById(R.id.group_join_cancel_button);
        this.groupJoinButton = (Button) inflate.findViewById(R.id.group_join_button);
        this.busy = (ProgressBar) inflate.findViewById(R.id.group_join_busy);
        this.avatar = (AvatarImageView) inflate.findViewById(R.id.group_join_recipient_avatar);
        this.groupName = (TextView) inflate.findViewById(R.id.group_join_group_name);
        this.groupDescription = (EmojiTextView) inflate.findViewById(R.id.group_join_group_description);
        this.groupDetails = (TextView) inflate.findViewById(R.id.group_join_group_details);
        this.groupJoinExplain = (TextView) inflate.findViewById(R.id.group_join_explain);
        this.groupCancelButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GroupJoinBottomSheetDialogFragment.$r8$lambda$TaOqJJJrKHpCYKvnwFHhkHqp6GU(GroupJoinBottomSheetDialogFragment.this, view);
            }
        });
        this.avatar.setImageBytesForGroup(null, new FallbackPhotoProvider(null), AvatarColor.UNKNOWN);
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(View view) {
        dismiss();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        GroupJoinViewModel groupJoinViewModel = (GroupJoinViewModel) ViewModelProviders.of(this, new GroupJoinViewModel.Factory(requireContext().getApplicationContext(), getGroupInviteLinkUrl())).get(GroupJoinViewModel.class);
        groupJoinViewModel.getGroupDetails().observe(getViewLifecycleOwner(), new Observer(groupJoinViewModel) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ GroupJoinViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupJoinBottomSheetDialogFragment.m1844$r8$lambda$ucIxn6oUepivZBzAhaMWCI1kCs(GroupJoinBottomSheetDialogFragment.this, this.f$1, (GroupDetails) obj);
            }
        });
        groupJoinViewModel.isBusy().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupJoinBottomSheetDialogFragment.m1843$r8$lambda$pgR_xsYOxeVnYEnnm5w5aa5HOQ(GroupJoinBottomSheetDialogFragment.this, (Boolean) obj);
            }
        });
        groupJoinViewModel.getErrors().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupJoinBottomSheetDialogFragment.$r8$lambda$1lU3UjFS4_6Z2s6lDQZemSjzbAM(GroupJoinBottomSheetDialogFragment.this, (FetchGroupDetailsError) obj);
            }
        });
        groupJoinViewModel.getJoinErrors().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupJoinBottomSheetDialogFragment.$r8$lambda$DF9xSzzJbavL2Tab7E6U6CeWq4I(GroupJoinBottomSheetDialogFragment.this, (JoinGroupError) obj);
            }
        });
        groupJoinViewModel.getJoinSuccess().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda6
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupJoinBottomSheetDialogFragment.$r8$lambda$_9PlkSjgIhy1QANDbGjM1m5LzKs(GroupJoinBottomSheetDialogFragment.this, (JoinGroupSuccess) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$2(GroupJoinViewModel groupJoinViewModel, GroupDetails groupDetails) {
        this.groupName.setText(groupDetails.getGroupName());
        this.groupDetails.setText(requireContext().getResources().getQuantityString(R.plurals.GroupJoinBottomSheetDialogFragment_group_dot_d_members, groupDetails.getGroupMembershipCount(), Integer.valueOf(groupDetails.getGroupMembershipCount())));
        if (!TextUtils.isEmpty(groupDetails.getGroupDescription())) {
            updateGroupDescription(groupDetails.getGroupName(), groupDetails.getGroupDescription());
        }
        this.groupJoinExplain.setText(groupDetails.joinRequiresAdminApproval() ? R.string.GroupJoinBottomSheetDialogFragment_admin_approval_needed : R.string.GroupJoinBottomSheetDialogFragment_direct_join);
        this.groupJoinButton.setText(groupDetails.joinRequiresAdminApproval() ? R.string.GroupJoinBottomSheetDialogFragment_request_to_join : R.string.GroupJoinBottomSheetDialogFragment_join);
        this.groupJoinButton.setOnClickListener(new View.OnClickListener(groupJoinViewModel) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ GroupJoinViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GroupJoinBottomSheetDialogFragment.$r8$lambda$3AiDVTlYOxTKesqdYoiCN5XG2us(GroupDetails.this, this.f$1, view);
            }
        });
        this.groupJoinButton.setVisibility(0);
        this.avatar.setImageBytesForGroup(groupDetails.getAvatarBytes(), new FallbackPhotoProvider(null), AvatarColor.UNKNOWN);
        this.groupCancelButton.setVisibility(0);
    }

    public static /* synthetic */ void lambda$onViewCreated$1(GroupDetails groupDetails, GroupJoinViewModel groupJoinViewModel, View view) {
        Log.i(TAG, groupDetails.joinRequiresAdminApproval() ? "Attempting to direct join group" : "Attempting to request to join group");
        groupJoinViewModel.join(groupDetails);
    }

    public /* synthetic */ void lambda$onViewCreated$3(Boolean bool) {
        this.busy.setVisibility(bool.booleanValue() ? 0 : 8);
    }

    public /* synthetic */ void lambda$onViewCreated$4(JoinGroupError joinGroupError) {
        Toast.makeText(requireContext(), errorToMessage(joinGroupError), 0).show();
    }

    public /* synthetic */ void lambda$onViewCreated$5(JoinGroupSuccess joinGroupSuccess) {
        Log.i(TAG, "Group joined, navigating to group");
        requireActivity().startActivity(ConversationIntents.createBuilder(requireContext(), joinGroupSuccess.getGroupRecipient().getId(), joinGroupSuccess.getGroupThreadId()).build());
        dismiss();
    }

    private void updateGroupDescription(String str, String str2) {
        this.groupDescription.setVisibility(0);
        this.groupDescription.setMovementMethod(LongClickMovementMethod.getInstance(requireContext()));
        GroupDescriptionUtil.setText(requireContext(), this.groupDescription, str2, true, new Runnable(str, str2) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupJoinBottomSheetDialogFragment.m1842$r8$lambda$PWZeL2jbzUH9BvlbXzXjjPnMO4(GroupJoinBottomSheetDialogFragment.this, this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$updateGroupDescription$6(String str, String str2) {
        GroupDescriptionDialog.show(getChildFragmentManager(), str, str2, true);
    }

    public void showError(FetchGroupDetailsError fetchGroupDetailsError) {
        this.avatar.setVisibility(4);
        this.groupCancelButton.setVisibility(8);
        this.groupDetails.setVisibility(0);
        this.groupJoinButton.setVisibility(0);
        this.groupJoinButton.setText(getString(17039370));
        this.groupJoinButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GroupJoinBottomSheetDialogFragment.$r8$lambda$NblKjcnUgcauqHSbcUqvx7vDD08(GroupJoinBottomSheetDialogFragment.this, view);
            }
        });
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$FetchGroupDetailsError[fetchGroupDetailsError.ordinal()];
        if (i == 1) {
            this.groupName.setText(R.string.GroupJoinBottomSheetDialogFragment_cant_join_group);
            this.groupDetails.setText(R.string.GroupJoinBottomSheetDialogFragment_this_group_link_is_no_longer_valid);
        } else if (i == 2) {
            this.groupName.setText(R.string.GroupJoinBottomSheetDialogFragment_cant_join_group);
            this.groupDetails.setText(R.string.GroupJoinBottomSheetDialogFragment_you_cant_join_this_group_via_the_group_link_because_an_admin_removed_you);
        } else if (i == 3) {
            this.groupName.setText(R.string.GroupJoinBottomSheetDialogFragment_link_error);
            this.groupDetails.setText(R.string.GroupJoinBottomSheetDialogFragment_joining_via_this_link_failed_try_joining_again_later);
        }
    }

    public /* synthetic */ void lambda$showError$7(View view) {
        dismissAllowingStateLoss();
    }

    /* renamed from: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinBottomSheetDialogFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$FetchGroupDetailsError;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$JoinGroupError;

        static {
            int[] iArr = new int[JoinGroupError.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$JoinGroupError = iArr;
            try {
                iArr[JoinGroupError.GROUP_LINK_NOT_ACTIVE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$JoinGroupError[JoinGroupError.BANNED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$JoinGroupError[JoinGroupError.NETWORK_ERROR.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[FetchGroupDetailsError.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$FetchGroupDetailsError = iArr2;
            try {
                iArr2[FetchGroupDetailsError.GroupLinkNotActive.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$FetchGroupDetailsError[FetchGroupDetailsError.BannedFromGroup.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$FetchGroupDetailsError[FetchGroupDetailsError.NetworkError.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    private String errorToMessage(JoinGroupError joinGroupError) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$joining$JoinGroupError[joinGroupError.ordinal()];
        if (i == 1) {
            return getString(R.string.GroupJoinBottomSheetDialogFragment_this_group_link_is_not_active);
        }
        if (i == 2) {
            return getString(R.string.GroupJoinBottomSheetDialogFragment_you_cant_join_this_group_via_the_group_link_because_an_admin_removed_you);
        }
        if (i != 3) {
            return getString(R.string.GroupJoinBottomSheetDialogFragment_unable_to_join_group_please_try_again_later);
        }
        return getString(R.string.GroupJoinBottomSheetDialogFragment_encountered_a_network_error);
    }

    private GroupInviteLinkUrl getGroupInviteLinkUrl() {
        try {
            return GroupInviteLinkUrl.fromUri(requireArguments().getString(ARG_GROUP_INVITE_LINK_URL));
        } catch (GroupInviteLinkUrl.InvalidGroupLinkException | GroupInviteLinkUrl.UnknownGroupLinkVersionException unused) {
            throw new AssertionError();
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }

    /* loaded from: classes4.dex */
    public static final class FallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        private FallbackPhotoProvider() {
        }

        /* synthetic */ FallbackPhotoProvider(AnonymousClass1 r1) {
            this();
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForGroup() {
            return new ResourceContactPhoto(R.drawable.ic_group_outline_48);
        }
    }
}
