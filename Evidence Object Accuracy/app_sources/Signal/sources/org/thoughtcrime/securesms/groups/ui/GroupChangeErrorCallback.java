package org.thoughtcrime.securesms.groups.ui;

/* loaded from: classes4.dex */
public interface GroupChangeErrorCallback {
    void onError(GroupChangeFailureReason groupChangeFailureReason);
}
