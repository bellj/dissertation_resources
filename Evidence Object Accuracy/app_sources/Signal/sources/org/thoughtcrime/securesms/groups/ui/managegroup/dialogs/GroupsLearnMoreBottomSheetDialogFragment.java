package org.thoughtcrime.securesms.groups.ui.managegroup.dialogs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes4.dex */
public final class GroupsLearnMoreBottomSheetDialogFragment extends BottomSheetDialogFragment {
    public static void show(FragmentManager fragmentManager) {
        new GroupsLearnMoreBottomSheetDialogFragment().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.groups_learn_more_bottom_sheet, viewGroup, false);
        inflate.findViewById(R.id.lbs_ok_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupsLearnMoreBottomSheetDialogFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GroupsLearnMoreBottomSheetDialogFragment.m1851$r8$lambda$Iw40N5K8unL_kjCJMV3zFI3RWo(GroupsLearnMoreBottomSheetDialogFragment.this, view);
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(View view) {
        dismiss();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }
}
