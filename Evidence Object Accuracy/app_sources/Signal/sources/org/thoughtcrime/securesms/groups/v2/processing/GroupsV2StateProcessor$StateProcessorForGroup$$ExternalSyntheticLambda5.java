package org.thoughtcrime.securesms.groups.v2.processing;

import j$.util.function.Function;
import org.signal.storageservice.protos.groups.local.DecryptedMember;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda5 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((DecryptedMember) obj).getUuid();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
