package org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public class RequestingMemberInvitesViewModel extends ViewModel {
    private final Context context;
    private final LiveData<GroupLinkUrlAndStatus> inviteLink;
    private final LiveData<List<GroupMemberEntry.RequestingMember>> requesting;
    private final RequestingMemberRepository requestingMemberRepository;
    private final MutableLiveData<String> toasts;

    private RequestingMemberInvitesViewModel(Context context, GroupId.V2 v2, RequestingMemberRepository requestingMemberRepository) {
        this.context = context;
        this.requestingMemberRepository = requestingMemberRepository;
        this.toasts = new SingleLiveEvent();
        LiveGroup liveGroup = new LiveGroup(v2);
        this.requesting = liveGroup.getRequestingMembers();
        this.inviteLink = liveGroup.getGroupLink();
    }

    public LiveData<List<GroupMemberEntry.RequestingMember>> getRequesting() {
        return this.requesting;
    }

    public LiveData<GroupLinkUrlAndStatus> getInviteLink() {
        return this.inviteLink;
    }

    public LiveData<String> getToasts() {
        return this.toasts;
    }

    public void approveRequestFor(final GroupMemberEntry.RequestingMember requestingMember) {
        requestingMember.setBusy(true);
        this.requestingMemberRepository.approveRequest(requestingMember.getRequester(), new AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason>() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMemberInvitesViewModel.1
            public void onComplete(Void r6) {
                requestingMember.setBusy(false);
                RequestingMemberInvitesViewModel.this.toasts.postValue(RequestingMemberInvitesViewModel.this.context.getString(R.string.RequestingMembersFragment_added_s, requestingMember.getRequester().getDisplayName(RequestingMemberInvitesViewModel.this.context)));
            }

            public void onError(GroupChangeFailureReason groupChangeFailureReason) {
                requestingMember.setBusy(false);
                RequestingMemberInvitesViewModel.this.toasts.postValue(RequestingMemberInvitesViewModel.this.context.getString(GroupErrors.getUserDisplayMessage(groupChangeFailureReason)));
            }
        });
    }

    public void denyRequestFor(final GroupMemberEntry.RequestingMember requestingMember) {
        requestingMember.setBusy(true);
        this.requestingMemberRepository.denyRequest(requestingMember.getRequester(), new AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason>() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMemberInvitesViewModel.2
            public void onComplete(Void r6) {
                requestingMember.setBusy(false);
                RequestingMemberInvitesViewModel.this.toasts.postValue(RequestingMemberInvitesViewModel.this.context.getString(R.string.RequestingMembersFragment_denied_s, requestingMember.getRequester().getDisplayName(RequestingMemberInvitesViewModel.this.context)));
            }

            public void onError(GroupChangeFailureReason groupChangeFailureReason) {
                requestingMember.setBusy(false);
                RequestingMemberInvitesViewModel.this.toasts.postValue(RequestingMemberInvitesViewModel.this.context.getString(GroupErrors.getUserDisplayMessage(groupChangeFailureReason)));
            }
        });
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final Context context;
        private final GroupId.V2 groupId;

        public Factory(Context context, GroupId.V2 v2) {
            this.context = context;
            this.groupId = v2;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new RequestingMemberInvitesViewModel(this.context, this.groupId, new RequestingMemberRepository(this.context.getApplicationContext(), this.groupId));
        }
    }
}
