package org.thoughtcrime.securesms.groups;

import com.google.protobuf.ByteString;
import j$.util.function.Predicate;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.thoughtcrime.securesms.groups.GroupManagerV2;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupManagerV2$GroupEditor$$ExternalSyntheticLambda3 implements Predicate {
    public final /* synthetic */ ByteString f$0;

    public /* synthetic */ GroupManagerV2$GroupEditor$$ExternalSyntheticLambda3(ByteString byteString) {
        this.f$0 = byteString;
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate and(Predicate predicate) {
        return Predicate.CC.$default$and(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate negate() {
        return Predicate.CC.$default$negate(this);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate or(Predicate predicate) {
        return Predicate.CC.$default$or(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public final boolean test(Object obj) {
        return GroupManagerV2.GroupEditor.lambda$ban$3(this.f$0, (DecryptedRequestingMember) obj);
    }
}
