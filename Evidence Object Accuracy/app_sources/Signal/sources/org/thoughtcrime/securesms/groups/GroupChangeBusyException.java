package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupChangeBusyException extends GroupChangeException {
    public GroupChangeBusyException(Throwable th) {
        super(th);
    }

    public GroupChangeBusyException(String str) {
        super(str);
    }
}
