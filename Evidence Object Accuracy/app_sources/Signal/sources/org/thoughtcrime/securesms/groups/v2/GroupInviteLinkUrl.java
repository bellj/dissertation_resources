package org.thoughtcrime.securesms.groups.v2;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.storageservice.protos.groups.GroupInviteLink;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.whispersystems.util.Base64UrlSafe;

/* loaded from: classes4.dex */
public final class GroupInviteLinkUrl {
    private static final String GROUP_URL_HOST;
    private static final String GROUP_URL_PREFIX;
    private final GroupMasterKey groupMasterKey;
    private final GroupLinkPassword password;
    private final String url;

    public static GroupInviteLinkUrl forGroup(GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup) {
        return new GroupInviteLinkUrl(groupMasterKey, GroupLinkPassword.fromBytes(decryptedGroup.getInviteLinkPassword().toByteArray()));
    }

    public static boolean isGroupLink(String str) {
        return getGroupUrl(str) != null;
    }

    public static GroupInviteLinkUrl fromUri(String str) throws InvalidGroupLinkException, UnknownGroupLinkVersionException {
        URI groupUrl = getGroupUrl(str);
        if (groupUrl == null) {
            return null;
        }
        try {
            if (!"/".equals(groupUrl.getPath()) && groupUrl.getPath().length() > 0) {
                throw new InvalidGroupLinkException("No path was expected in uri");
            }
            String fragment = groupUrl.getFragment();
            if (fragment == null || fragment.length() == 0) {
                throw new InvalidGroupLinkException("No reference was in the uri");
            }
            GroupInviteLink parseFrom = GroupInviteLink.parseFrom(Base64UrlSafe.decodePaddingAgnostic(fragment));
            if (AnonymousClass1.$SwitchMap$org$signal$storageservice$protos$groups$GroupInviteLink$ContentsCase[parseFrom.getContentsCase().ordinal()] == 1) {
                GroupInviteLink.GroupInviteLinkContentsV1 v1Contents = parseFrom.getV1Contents();
                return new GroupInviteLinkUrl(new GroupMasterKey(v1Contents.getGroupMasterKey().toByteArray()), GroupLinkPassword.fromBytes(v1Contents.getInviteLinkPassword().toByteArray()));
            }
            throw new UnknownGroupLinkVersionException("Url contains no known group link content");
        } catch (IOException | InvalidInputException e) {
            throw new InvalidGroupLinkException(e);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$storageservice$protos$groups$GroupInviteLink$ContentsCase;

        static {
            int[] iArr = new int[GroupInviteLink.ContentsCase.values().length];
            $SwitchMap$org$signal$storageservice$protos$groups$GroupInviteLink$ContentsCase = iArr;
            try {
                iArr[GroupInviteLink.ContentsCase.V1CONTENTS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    private static URI getGroupUrl(String str) {
        try {
            URI uri = new URI(str);
            if (!"https".equalsIgnoreCase(uri.getScheme()) && !"sgnl".equalsIgnoreCase(uri.getScheme())) {
                return null;
            }
            if (GROUP_URL_HOST.equalsIgnoreCase(uri.getHost())) {
                return uri;
            }
            return null;
        } catch (URISyntaxException unused) {
            return null;
        }
    }

    private GroupInviteLinkUrl(GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword) {
        this.groupMasterKey = groupMasterKey;
        this.password = groupLinkPassword;
        this.url = createUrl(groupMasterKey, groupLinkPassword);
    }

    protected static String createUrl(GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword) {
        String encodeBytesWithoutPadding = Base64UrlSafe.encodeBytesWithoutPadding(GroupInviteLink.newBuilder().setV1Contents(GroupInviteLink.GroupInviteLinkContentsV1.newBuilder().setGroupMasterKey(ByteString.copyFrom(groupMasterKey.serialize())).setInviteLinkPassword(ByteString.copyFrom(groupLinkPassword.serialize()))).build().toByteArray());
        return GROUP_URL_PREFIX + encodeBytesWithoutPadding;
    }

    public String getUrl() {
        return this.url;
    }

    public GroupMasterKey getGroupMasterKey() {
        return this.groupMasterKey;
    }

    public GroupLinkPassword getPassword() {
        return this.password;
    }

    /* loaded from: classes4.dex */
    public static final class InvalidGroupLinkException extends Exception {
        public InvalidGroupLinkException(String str) {
            super(str);
        }

        public InvalidGroupLinkException(Throwable th) {
            super(th);
        }
    }

    /* loaded from: classes4.dex */
    public static final class UnknownGroupLinkVersionException extends Exception {
        public UnknownGroupLinkVersionException(String str) {
            super(str);
        }
    }
}
