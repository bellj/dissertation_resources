package org.thoughtcrime.securesms.groups;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.auth.AuthCredentialWithPniResponse;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Api;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2AuthorizationString;
import org.whispersystems.signalservice.api.groupsv2.NoCredentialForRedemptionTimeException;
import org.whispersystems.signalservice.api.push.ServiceIds;

/* loaded from: classes.dex */
public class GroupsV2Authorization {
    private static final String TAG = Log.tag(GroupsV2Authorization.class);
    private final ValueCache authCache;
    private final GroupsV2Api groupsV2Api;

    /* loaded from: classes4.dex */
    public interface ValueCache {
        void clear();

        Map<Long, AuthCredentialWithPniResponse> read();

        void write(Map<Long, AuthCredentialWithPniResponse> map);
    }

    public GroupsV2Authorization(GroupsV2Api groupsV2Api, ValueCache valueCache) {
        this.groupsV2Api = groupsV2Api;
        this.authCache = valueCache;
    }

    public GroupsV2AuthorizationString getAuthorizationForToday(ServiceIds serviceIds, GroupSecretParams groupSecretParams) throws IOException, VerificationFailedException {
        long currentDaySeconds = currentDaySeconds();
        try {
            return getAuthorization(serviceIds, groupSecretParams, this.authCache.read(), currentDaySeconds);
        } catch (VerificationFailedException e) {
            Log.w(TAG, "Verification failed, will update auth and try again", e);
            this.authCache.clear();
            Log.i(TAG, "Getting new auth credential responses");
            HashMap<Long, AuthCredentialWithPniResponse> credentials = this.groupsV2Api.getCredentials(currentDaySeconds);
            this.authCache.write(credentials);
            try {
                return getAuthorization(serviceIds, groupSecretParams, credentials, currentDaySeconds);
            } catch (NoCredentialForRedemptionTimeException unused) {
                Log.w(TAG, "The credentials returned did not include the day requested");
                throw new IOException("Failed to get credentials");
            }
        } catch (NoCredentialForRedemptionTimeException unused2) {
            Log.i(TAG, "Auth out of date, will update auth and try again");
            this.authCache.clear();
            Log.i(TAG, "Getting new auth credential responses");
            HashMap<Long, AuthCredentialWithPniResponse> credentials = this.groupsV2Api.getCredentials(currentDaySeconds);
            this.authCache.write(credentials);
            return getAuthorization(serviceIds, groupSecretParams, credentials, currentDaySeconds);
        }
    }

    public void clear() {
        this.authCache.clear();
    }

    private static long currentDaySeconds() {
        return TimeUnit.DAYS.toSeconds(TimeUnit.MILLISECONDS.toDays(System.currentTimeMillis()));
    }

    private GroupsV2AuthorizationString getAuthorization(ServiceIds serviceIds, GroupSecretParams groupSecretParams, Map<Long, AuthCredentialWithPniResponse> map, long j) throws NoCredentialForRedemptionTimeException, VerificationFailedException {
        AuthCredentialWithPniResponse authCredentialWithPniResponse = map.get(Long.valueOf(j));
        if (authCredentialWithPniResponse != null) {
            return this.groupsV2Api.getGroupsV2AuthorizationString(serviceIds.getAci(), serviceIds.requirePni(), j, groupSecretParams, authCredentialWithPniResponse);
        }
        throw new NoCredentialForRedemptionTimeException();
    }
}
