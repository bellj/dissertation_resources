package org.thoughtcrime.securesms.groups;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.groups.GroupManagerV2;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupManagerV2$GroupEditor$$ExternalSyntheticLambda2 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return GroupManagerV2.GroupEditor.lambda$denyRequests$1((RecipientId) obj);
    }
}
