package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupNotAMemberException extends GroupChangeException {
    private final boolean likelyPendingMember;

    public GroupNotAMemberException(Throwable th) {
        super(th);
        this.likelyPendingMember = false;
    }

    public GroupNotAMemberException(GroupNotAMemberException groupNotAMemberException, boolean z) {
        super(groupNotAMemberException.getCause() != null ? groupNotAMemberException.getCause() : groupNotAMemberException);
        this.likelyPendingMember = z;
    }

    public GroupNotAMemberException() {
        this.likelyPendingMember = false;
    }

    public boolean isLikelyPendingMember() {
        return this.likelyPendingMember;
    }
}
