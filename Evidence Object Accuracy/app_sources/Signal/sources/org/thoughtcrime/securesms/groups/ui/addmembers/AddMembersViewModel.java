package org.thoughtcrime.securesms.groups.ui.addmembers;

import android.text.TextUtils;
import androidx.core.util.Consumer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public final class AddMembersViewModel extends ViewModel {
    private final AddMembersRepository repository;

    private AddMembersViewModel(GroupId groupId) {
        this.repository = new AddMembersRepository(groupId);
    }

    public void getDialogStateForSelectedContacts(List<SelectedContact> list, Consumer<AddMemberDialogMessageState> consumer) {
        AddMembersViewModel$$ExternalSyntheticLambda0 addMembersViewModel$$ExternalSyntheticLambda0 = new SimpleTask.BackgroundTask(list) { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return AddMembersViewModel.this.lambda$getDialogStateForSelectedContacts$0(this.f$1);
            }
        };
        Objects.requireNonNull(consumer);
        SimpleTask.run(addMembersViewModel$$ExternalSyntheticLambda0, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersViewModel$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                Consumer.this.accept((AddMembersViewModel.AddMemberDialogMessageState) obj);
            }
        });
    }

    public /* synthetic */ AddMemberDialogMessageState lambda$getDialogStateForSelectedContacts$0(List list) {
        AddMemberDialogMessageStatePartial addMemberDialogMessageStatePartial;
        if (list.size() == 1) {
            addMemberDialogMessageStatePartial = getDialogStateForSingleRecipient((SelectedContact) list.get(0));
        } else {
            addMemberDialogMessageStatePartial = getDialogStateForMultipleRecipients(list.size());
        }
        return new AddMemberDialogMessageState(addMemberDialogMessageStatePartial.recipientId == null ? Recipient.UNKNOWN : Recipient.resolved(addMemberDialogMessageStatePartial.recipientId), addMemberDialogMessageStatePartial.memberCount, titleOrDefault(this.repository.getGroupTitle()));
    }

    private AddMemberDialogMessageStatePartial getDialogStateForSingleRecipient(SelectedContact selectedContact) {
        return new AddMemberDialogMessageStatePartial(this.repository.getOrCreateRecipientId(selectedContact));
    }

    private AddMemberDialogMessageStatePartial getDialogStateForMultipleRecipients(int i) {
        return new AddMemberDialogMessageStatePartial(i);
    }

    private static String titleOrDefault(String str) {
        if (TextUtils.isEmpty(str)) {
            return ApplicationDependencies.getApplication().getString(R.string.Recipient_unknown);
        }
        Objects.requireNonNull(str);
        return str;
    }

    /* loaded from: classes4.dex */
    public static final class AddMemberDialogMessageStatePartial {
        private final int memberCount;
        private final RecipientId recipientId;

        private AddMemberDialogMessageStatePartial(RecipientId recipientId) {
            this.recipientId = recipientId;
            this.memberCount = 1;
        }

        private AddMemberDialogMessageStatePartial(int i) {
            Preconditions.checkArgument(i <= 1 ? false : true);
            this.memberCount = i;
            this.recipientId = null;
        }
    }

    /* loaded from: classes4.dex */
    public static final class AddMemberDialogMessageState {
        private final String groupTitle;
        private final Recipient recipient;
        private final int selectionCount;

        private AddMemberDialogMessageState(Recipient recipient, int i, String str) {
            this.recipient = recipient;
            this.groupTitle = str;
            this.selectionCount = i;
        }

        public Recipient getRecipient() {
            return this.recipient;
        }

        public int getSelectionCount() {
            return this.selectionCount;
        }

        public String getGroupTitle() {
            return this.groupTitle;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final GroupId groupId;

        public Factory(GroupId groupId) {
            this.groupId = groupId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new AddMembersViewModel(this.groupId));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
