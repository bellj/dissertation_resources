package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;

/* loaded from: classes4.dex */
public final class GroupDetails {
    private final byte[] avatarBytes;
    private final DecryptedGroupJoinInfo joinInfo;

    public GroupDetails(DecryptedGroupJoinInfo decryptedGroupJoinInfo, byte[] bArr) {
        this.joinInfo = decryptedGroupJoinInfo;
        this.avatarBytes = bArr;
    }

    public String getGroupName() {
        return this.joinInfo.getTitle();
    }

    public String getGroupDescription() {
        return this.joinInfo.getDescription();
    }

    public byte[] getAvatarBytes() {
        return this.avatarBytes;
    }

    public DecryptedGroupJoinInfo getJoinInfo() {
        return this.joinInfo;
    }

    public int getGroupMembershipCount() {
        return this.joinInfo.getMemberCount();
    }

    public boolean joinRequiresAdminApproval() {
        return this.joinInfo.getAddFromInviteLink() == AccessControl.AccessRequired.ADMINISTRATOR;
    }
}
