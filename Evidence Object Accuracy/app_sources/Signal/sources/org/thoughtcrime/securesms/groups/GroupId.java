package org.thoughtcrime.securesms.groups;

import java.io.IOException;
import java.security.SecureRandom;
import org.signal.core.util.Hex;
import org.signal.libsignal.protocol.kdf.HKDF;
import org.signal.libsignal.protocol.kdf.HKDFv3;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.GroupIdentifier;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public abstract class GroupId {
    private static final String ENCODED_MMS_GROUP_PREFIX;
    private static final String ENCODED_SIGNAL_GROUP_V1_PREFIX;
    private static final String ENCODED_SIGNAL_GROUP_V2_PREFIX;
    private static final int MMS_BYTE_LENGTH;
    private static final int V1_BYTE_LENGTH;
    private static final int V1_MMS_BYTE_LENGTH;
    private static final int V2_BYTE_LENGTH;
    private final String encodedId;

    public abstract boolean isMms();

    public abstract boolean isPush();

    public abstract boolean isV1();

    public abstract boolean isV2();

    private GroupId(String str, byte[] bArr) {
        this.encodedId = str + Hex.toStringCondensed(bArr);
    }

    public static Mms mms(byte[] bArr) {
        return new Mms(bArr);
    }

    public static V1 v1orThrow(byte[] bArr) {
        try {
            return v1(bArr);
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public static V1 v1(byte[] bArr) throws BadGroupIdException {
        if (bArr.length == 16) {
            return new V1(bArr);
        }
        throw new BadGroupIdException();
    }

    public static V1 createV1(SecureRandom secureRandom) {
        return v1orThrow(Util.getSecretBytes(secureRandom, 16));
    }

    public static Mms createMms(SecureRandom secureRandom) {
        return mms(Util.getSecretBytes(secureRandom, 16));
    }

    private static V2 v2(byte[] bArr) throws BadGroupIdException {
        if (bArr.length == 32) {
            return new V2(bArr);
        }
        throw new BadGroupIdException();
    }

    public static V2 v2(GroupIdentifier groupIdentifier) {
        try {
            return v2(groupIdentifier.serialize());
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public static V2 v2(GroupMasterKey groupMasterKey) {
        return v2(GroupSecretParams.deriveFromMasterKey(groupMasterKey).getPublicParams().getGroupIdentifier());
    }

    public static Push push(byte[] bArr) throws BadGroupIdException {
        return bArr.length == 32 ? v2(bArr) : v1(bArr);
    }

    public static Push pushOrThrow(byte[] bArr) {
        try {
            return push(bArr);
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public static GroupId parseOrThrow(String str) {
        try {
            return parse(str);
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public static GroupId parse(String str) throws BadGroupIdException {
        try {
            if (isEncodedGroup(str)) {
                byte[] extractDecodedId = extractDecodedId(str);
                if (str.startsWith(ENCODED_SIGNAL_GROUP_V2_PREFIX)) {
                    return v2(extractDecodedId);
                }
                if (str.startsWith(ENCODED_SIGNAL_GROUP_V1_PREFIX)) {
                    return v1(extractDecodedId);
                }
                if (str.startsWith(ENCODED_MMS_GROUP_PREFIX)) {
                    return mms(extractDecodedId);
                }
                throw new BadGroupIdException();
            }
            throw new BadGroupIdException("Invalid encoding");
        } catch (IOException e) {
            throw new BadGroupIdException(e);
        }
    }

    public static GroupId parseNullable(String str) throws BadGroupIdException {
        if (str == null) {
            return null;
        }
        return parse(str);
    }

    public static GroupId parseNullableOrThrow(String str) {
        if (str == null) {
            return null;
        }
        return parseOrThrow(str);
    }

    public static boolean isEncodedGroup(String str) {
        return str.startsWith(ENCODED_SIGNAL_GROUP_V2_PREFIX) || str.startsWith(ENCODED_SIGNAL_GROUP_V1_PREFIX) || str.startsWith(ENCODED_MMS_GROUP_PREFIX);
    }

    private static byte[] extractDecodedId(String str) throws IOException {
        return Hex.fromStringCondensed(str.split("!", 2)[1]);
    }

    public byte[] getDecodedId() {
        try {
            return extractDecodedId(this.encodedId);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof GroupId) {
            return ((GroupId) obj).encodedId.equals(this.encodedId);
        }
        return false;
    }

    public int hashCode() {
        return this.encodedId.hashCode();
    }

    public String toString() {
        return this.encodedId;
    }

    public Mms requireMms() {
        if (this instanceof Mms) {
            return (Mms) this;
        }
        throw new AssertionError();
    }

    public V1 requireV1() {
        if (this instanceof V1) {
            return (V1) this;
        }
        throw new AssertionError();
    }

    public V2 requireV2() {
        if (this instanceof V2) {
            return (V2) this;
        }
        throw new AssertionError();
    }

    public Push requirePush() {
        if (this instanceof Push) {
            return (Push) this;
        }
        throw new AssertionError();
    }

    /* loaded from: classes4.dex */
    public static final class Mms extends GroupId {
        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isMms() {
            return true;
        }

        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isPush() {
            return false;
        }

        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isV1() {
            return false;
        }

        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isV2() {
            return false;
        }

        private Mms(byte[] bArr) {
            super(GroupId.ENCODED_MMS_GROUP_PREFIX, bArr);
        }
    }

    /* loaded from: classes4.dex */
    public static abstract class Push extends GroupId {
        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isMms() {
            return false;
        }

        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isPush() {
            return true;
        }

        private Push(String str, byte[] bArr) {
            super(str, bArr);
        }
    }

    /* loaded from: classes4.dex */
    public static final class V1 extends Push {
        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isV1() {
            return true;
        }

        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isV2() {
            return false;
        }

        private V1(byte[] bArr) {
            super(GroupId.ENCODED_SIGNAL_GROUP_V1_PREFIX, bArr);
        }

        public GroupMasterKey deriveV2MigrationMasterKey() {
            try {
                new HKDFv3();
                return new GroupMasterKey(HKDF.deriveSecrets(getDecodedId(), "GV2 Migration".getBytes(), 32));
            } catch (InvalidInputException e) {
                throw new AssertionError(e);
            }
        }

        public V2 deriveV2MigrationGroupId() {
            return GroupId.v2(deriveV2MigrationMasterKey());
        }
    }

    /* loaded from: classes4.dex */
    public static final class V2 extends Push {
        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isV1() {
            return false;
        }

        @Override // org.thoughtcrime.securesms.groups.GroupId
        public boolean isV2() {
            return true;
        }

        private V2(byte[] bArr) {
            super(GroupId.ENCODED_SIGNAL_GROUP_V2_PREFIX, bArr);
        }
    }
}
