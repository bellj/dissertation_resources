package org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.AdminActionsListener;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.RecipientClickListener;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMemberInvitesViewModel;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* loaded from: classes4.dex */
public class RequestingMembersFragment extends Fragment {
    private static final String GROUP_ID;
    private View noRequestingMessage;
    private View requestingExplanation;
    private GroupMemberListView requestingMembers;
    private RequestingMemberInvitesViewModel viewModel;

    public static RequestingMembersFragment newInstance(GroupId.V2 v2) {
        RequestingMembersFragment requestingMembersFragment = new RequestingMembersFragment();
        Bundle bundle = new Bundle();
        bundle.putString(GROUP_ID, v2.toString());
        requestingMembersFragment.setArguments(bundle);
        return requestingMembersFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.group_requesting_member_fragment, viewGroup, false);
        this.requestingMembers = (GroupMemberListView) inflate.findViewById(R.id.requesting_members);
        this.noRequestingMessage = inflate.findViewById(R.id.no_requesting);
        this.requestingExplanation = inflate.findViewById(R.id.requesting_members_explain);
        this.requestingMembers.initializeAdapter(getViewLifecycleOwner());
        this.requestingMembers.setRecipientClickListener(new RecipientClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMembersFragment$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.groups.ui.RecipientClickListener
            public final void onClick(Recipient recipient) {
                RequestingMembersFragment.this.lambda$onCreateView$0(recipient);
            }
        });
        this.requestingMembers.setAdminActionsListener(new AdminActionsListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMembersFragment.1
            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onRevokeInvite(GroupMemberEntry.PendingMember pendingMember) {
                throw new UnsupportedOperationException();
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onRevokeAllInvites(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
                throw new UnsupportedOperationException();
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onApproveRequest(GroupMemberEntry.RequestingMember requestingMember) {
                RequestConfirmationDialog.showApprove(RequestingMembersFragment.this.requireContext(), requestingMember.getRequester(), new RequestingMembersFragment$1$$ExternalSyntheticLambda1(this, requestingMember));
            }

            public /* synthetic */ void lambda$onApproveRequest$0(GroupMemberEntry.RequestingMember requestingMember) {
                RequestingMembersFragment.this.viewModel.approveRequestFor(requestingMember);
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onDenyRequest(GroupMemberEntry.RequestingMember requestingMember) {
                GroupLinkUrlAndStatus value = RequestingMembersFragment.this.viewModel.getInviteLink().getValue();
                RequestConfirmationDialog.showDeny(RequestingMembersFragment.this.requireContext(), requestingMember.getRequester(), value == null || value.isEnabled(), new RequestingMembersFragment$1$$ExternalSyntheticLambda0(this, requestingMember));
            }

            public /* synthetic */ void lambda$onDenyRequest$1(GroupMemberEntry.RequestingMember requestingMember) {
                RequestingMembersFragment.this.viewModel.denyRequestFor(requestingMember);
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(Recipient recipient) {
        RecipientBottomSheetDialogFragment.create(recipient.getId(), null).show(requireActivity().getSupportFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        String string = requireArguments().getString(GROUP_ID);
        Objects.requireNonNull(string);
        RequestingMemberInvitesViewModel requestingMemberInvitesViewModel = (RequestingMemberInvitesViewModel) new ViewModelProvider(this, new RequestingMemberInvitesViewModel.Factory(requireContext(), GroupId.parseOrThrow(string).requireV2())).get(RequestingMemberInvitesViewModel.class);
        this.viewModel = requestingMemberInvitesViewModel;
        requestingMemberInvitesViewModel.getRequesting().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMembersFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RequestingMembersFragment.this.lambda$onViewCreated$1((List) obj);
            }
        });
        this.viewModel.getToasts().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMembersFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                RequestingMembersFragment.this.lambda$onViewCreated$2((String) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$1(List list) {
        this.requestingMembers.setMembers(list);
        int i = 0;
        this.noRequestingMessage.setVisibility(list.isEmpty() ? 0 : 8);
        View view = this.requestingExplanation;
        if (list.isEmpty()) {
            i = 8;
        }
        view.setVisibility(i);
    }

    public /* synthetic */ void lambda$onViewCreated$2(String str) {
        Toast.makeText(requireContext(), str, 0).show();
    }
}
