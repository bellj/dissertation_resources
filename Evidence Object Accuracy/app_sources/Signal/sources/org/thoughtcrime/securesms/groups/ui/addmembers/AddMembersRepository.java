package org.thoughtcrime.securesms.groups.ui.addmembers;

import android.content.Context;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class AddMembersRepository {
    private final Context context = ApplicationDependencies.getApplication();
    private final GroupId groupId;

    public AddMembersRepository(GroupId groupId) {
        this.groupId = groupId;
    }

    public RecipientId getOrCreateRecipientId(SelectedContact selectedContact) {
        return selectedContact.getOrCreateRecipientId(this.context);
    }

    public String getGroupTitle() {
        return SignalDatabase.groups().requireGroup(this.groupId).getTitle();
    }
}
