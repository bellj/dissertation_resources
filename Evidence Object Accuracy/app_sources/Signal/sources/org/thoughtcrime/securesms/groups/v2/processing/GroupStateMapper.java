package org.thoughtcrime.securesms.groups.v2.processing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.thoughtcrime.securesms.groups.v2.processing.StateChain;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.groupsv2.GroupChangeReconstruct;
import org.whispersystems.signalservice.api.groupsv2.GroupChangeUtil;
import org.whispersystems.signalservice.api.groupsv2.NotAbleToApplyGroupV2ChangeException;

/* loaded from: classes4.dex */
public final class GroupStateMapper {
    private static final Comparator<ServerGroupLogEntry> BY_REVISION = new Comparator() { // from class: org.thoughtcrime.securesms.groups.v2.processing.GroupStateMapper$$ExternalSyntheticLambda0
        @Override // java.util.Comparator
        public final int compare(Object obj, Object obj2) {
            return GroupStateMapper.$r8$lambda$GenR6YCmvDXAdpniWDAAQ0tbIYM((ServerGroupLogEntry) obj, (ServerGroupLogEntry) obj2);
        }
    };
    static final int LATEST;
    static final int PLACEHOLDER_REVISION;
    static final int RESTORE_PLACEHOLDER_REVISION;
    private static final String TAG = Log.tag(GroupStateMapper.class);

    public static /* synthetic */ int lambda$static$0(ServerGroupLogEntry serverGroupLogEntry, ServerGroupLogEntry serverGroupLogEntry2) {
        return Integer.compare(serverGroupLogEntry.getRevision(), serverGroupLogEntry2.getRevision());
    }

    private GroupStateMapper() {
    }

    public static AdvanceGroupStateResult partiallyAdvanceGroupState(GlobalGroupState globalGroupState, int i) {
        return cleanDuplicatedChanges(processChanges(globalGroupState, i), globalGroupState.getLocalState());
    }

    private static AdvanceGroupStateResult processChanges(GlobalGroupState globalGroupState, int i) {
        HashMap hashMap = new HashMap(globalGroupState.getServerHistory().size());
        ArrayList arrayList = new ArrayList(globalGroupState.getServerHistory().size());
        DecryptedGroup localState = globalGroupState.getLocalState();
        StateChain<DecryptedGroup, DecryptedGroupChange> createNewMapper = createNewMapper();
        if (globalGroupState.getServerHistory().isEmpty()) {
            return new AdvanceGroupStateResult(Collections.emptyList(), new GlobalGroupState(localState, Collections.emptyList()));
        }
        for (ServerGroupLogEntry serverGroupLogEntry : globalGroupState.getServerHistory()) {
            if (serverGroupLogEntry.getRevision() > i) {
                arrayList.add(serverGroupLogEntry);
            } else {
                hashMap.put(Integer.valueOf(serverGroupLogEntry.getRevision()), serverGroupLogEntry);
            }
        }
        Collections.sort(arrayList, BY_REVISION);
        int max = Math.max(0, globalGroupState.getEarliestRevisionNumber());
        int min = Math.min(globalGroupState.getLatestRevisionNumber(), i);
        if (localState == null || localState.getRevision() != -1) {
            createNewMapper.push(localState, null);
        } else {
            Log.i(TAG, "Ignoring place holder group state");
        }
        while (max >= 0 && max <= min) {
            ServerGroupLogEntry serverGroupLogEntry2 = (ServerGroupLogEntry) hashMap.get(Integer.valueOf(max));
            if (serverGroupLogEntry2 == null) {
                String str = TAG;
                Log.w(str, "Could not find group log on server V" + max);
            } else {
                if (createNewMapper.getLatestState() == null && serverGroupLogEntry2.getGroup() != null && localState != null && localState.getRevision() == -1) {
                    createNewMapper.push(DecryptedGroup.newBuilder(serverGroupLogEntry2.getGroup()).setTitle(localState.getTitle()).setAvatar(localState.getAvatar()).build(), null);
                }
                createNewMapper.push(serverGroupLogEntry2.getGroup(), serverGroupLogEntry2.getChange());
            }
            max++;
        }
        List<StateChain.Pair<DecryptedGroup, DecryptedGroupChange>> list = createNewMapper.getList();
        ArrayList arrayList2 = new ArrayList(list.size());
        for (StateChain.Pair<DecryptedGroup, DecryptedGroupChange> pair : list) {
            if (localState == null || pair.getDelta() != null) {
                arrayList2.add(new LocalGroupLogEntry(pair.getState(), pair.getDelta()));
            }
        }
        return new AdvanceGroupStateResult(arrayList2, new GlobalGroupState(createNewMapper.getLatestState(), arrayList));
    }

    private static AdvanceGroupStateResult cleanDuplicatedChanges(AdvanceGroupStateResult advanceGroupStateResult, DecryptedGroup decryptedGroup) {
        if (decryptedGroup == null) {
            return advanceGroupStateResult;
        }
        ArrayList arrayList = new ArrayList(advanceGroupStateResult.getProcessedLogEntries().size());
        for (LocalGroupLogEntry localGroupLogEntry : advanceGroupStateResult.getProcessedLogEntries()) {
            DecryptedGroupChange change = localGroupLogEntry.getChange();
            if (change != null) {
                change = GroupChangeUtil.resolveConflict(decryptedGroup, change).build();
            }
            arrayList.add(new LocalGroupLogEntry(localGroupLogEntry.getGroup(), change));
            decryptedGroup = localGroupLogEntry.getGroup();
        }
        return new AdvanceGroupStateResult(arrayList, advanceGroupStateResult.getNewGlobalGroupState());
    }

    private static StateChain<DecryptedGroup, DecryptedGroupChange> createNewMapper() {
        return new StateChain<>(new StateChain.AddDelta() { // from class: org.thoughtcrime.securesms.groups.v2.processing.GroupStateMapper$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.groups.v2.processing.StateChain.AddDelta
            public final Object add(Object obj, Object obj2) {
                return GroupStateMapper.m1865$r8$lambda$qDcsem3ElsIvBIV96yGCbNiTs((DecryptedGroup) obj, (DecryptedGroupChange) obj2);
            }
        }, new StateChain.SubtractStates() { // from class: org.thoughtcrime.securesms.groups.v2.processing.GroupStateMapper$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.groups.v2.processing.StateChain.SubtractStates
            public final Object subtract(Object obj, Object obj2) {
                return GroupStateMapper.$r8$lambda$9qH4E66gY51lOFCaL0MWmqJJKXY((DecryptedGroup) obj, (DecryptedGroup) obj2);
            }
        }, new StateChain.StateEquality() { // from class: org.thoughtcrime.securesms.groups.v2.processing.GroupStateMapper$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.groups.v2.processing.StateChain.StateEquality
            public final boolean equals(Object obj, Object obj2) {
                return GroupStateMapper.$r8$lambda$MCvCmR3cdqDpnmKj5m3ray9lzRs((DecryptedGroup) obj, (DecryptedGroup) obj2);
            }
        });
    }

    public static /* synthetic */ DecryptedGroup lambda$createNewMapper$1(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) {
        try {
            return DecryptedGroupUtil.applyWithoutRevisionCheck(decryptedGroup, decryptedGroupChange);
        } catch (NotAbleToApplyGroupV2ChangeException e) {
            String str = TAG;
            Log.w(str, "Unable to apply V" + decryptedGroupChange.getRevision(), e);
            return null;
        }
    }

    public static /* synthetic */ boolean lambda$createNewMapper$3(DecryptedGroup decryptedGroup, DecryptedGroup decryptedGroup2) {
        return decryptedGroup.getRevision() == decryptedGroup2.getRevision() && DecryptedGroupUtil.changeIsEmpty(GroupChangeReconstruct.reconstructGroupChange(decryptedGroup, decryptedGroup2));
    }
}
