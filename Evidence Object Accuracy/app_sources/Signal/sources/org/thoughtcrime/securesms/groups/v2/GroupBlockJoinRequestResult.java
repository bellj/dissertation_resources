package org.thoughtcrime.securesms.groups.v2;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;

/* compiled from: GroupManagementResults.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult;", "", "()V", "isFailure", "", "Failure", "Success", "Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult$Success;", "Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult$Failure;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class GroupBlockJoinRequestResult {
    public /* synthetic */ GroupBlockJoinRequestResult(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: GroupManagementResults.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult$Success;", "Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Success extends GroupBlockJoinRequestResult {
        public static final Success INSTANCE = new Success();

        private Success() {
            super(null);
        }
    }

    private GroupBlockJoinRequestResult() {
    }

    /* compiled from: GroupManagementResults.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult$Failure;", "Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult;", "reason", "Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "(Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;)V", "getReason", "()Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Failure extends GroupBlockJoinRequestResult {
        private final GroupChangeFailureReason reason;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Failure(GroupChangeFailureReason groupChangeFailureReason) {
            super(null);
            Intrinsics.checkNotNullParameter(groupChangeFailureReason, "reason");
            this.reason = groupChangeFailureReason;
        }

        public final GroupChangeFailureReason getReason() {
            return this.reason;
        }
    }

    public final boolean isFailure() {
        return this instanceof Failure;
    }
}
