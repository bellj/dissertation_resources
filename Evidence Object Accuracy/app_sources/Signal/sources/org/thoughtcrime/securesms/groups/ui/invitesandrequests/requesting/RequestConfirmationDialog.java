package org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
final class RequestConfirmationDialog {
    private RequestConfirmationDialog() {
    }

    public static AlertDialog showApprove(Context context, Recipient recipient, Runnable runnable) {
        return new MaterialAlertDialogBuilder(context).setMessage((CharSequence) context.getString(R.string.RequestConfirmationDialog_add_s_to_the_group, recipient.getDisplayName(context))).setPositiveButton(R.string.RequestConfirmationDialog_add, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestConfirmationDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RequestConfirmationDialog.$r8$lambda$2R7tiN639ZLj3bePws1aKEWz8p8(this.f$0, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    public static AlertDialog showDeny(Context context, Recipient recipient, boolean z, Runnable runnable) {
        String str;
        if (z) {
            str = context.getString(R.string.RequestConfirmationDialog_deny_request_from_s_they_will_not_be_able_to_request, recipient.getDisplayName(context));
        } else {
            str = context.getString(R.string.RequestConfirmationDialog_deny_request_from_s, recipient.getDisplayName(context));
        }
        return new MaterialAlertDialogBuilder(context).setMessage((CharSequence) str).setPositiveButton(R.string.RequestConfirmationDialog_deny, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestConfirmationDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RequestConfirmationDialog.$r8$lambda$n2PlanyifN4kLf3rNSDn3lbVzTg(this.f$0, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }
}
