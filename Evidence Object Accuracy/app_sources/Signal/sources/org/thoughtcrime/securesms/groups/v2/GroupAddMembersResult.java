package org.thoughtcrime.securesms.groups.v2;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: GroupManagementResults.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004\u0001\u0002\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult;", "", "()V", "isFailure", "", "Failure", "Success", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult$Success;", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult$Failure;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class GroupAddMembersResult {
    public /* synthetic */ GroupAddMembersResult(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: GroupManagementResults.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult$Success;", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult;", "numberOfMembersAdded", "", "newMembersInvited", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(ILjava/util/List;)V", "getNewMembersInvited", "()Ljava/util/List;", "getNumberOfMembersAdded", "()I", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Success extends GroupAddMembersResult {
        private final List<Recipient> newMembersInvited;
        private final int numberOfMembersAdded;

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Success(int i, List<? extends Recipient> list) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "newMembersInvited");
            this.numberOfMembersAdded = i;
            this.newMembersInvited = list;
        }

        public final List<Recipient> getNewMembersInvited() {
            return this.newMembersInvited;
        }

        public final int getNumberOfMembersAdded() {
            return this.numberOfMembersAdded;
        }
    }

    private GroupAddMembersResult() {
    }

    /* compiled from: GroupManagementResults.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult$Failure;", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult;", "reason", "Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "(Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;)V", "getReason", "()Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Failure extends GroupAddMembersResult {
        private final GroupChangeFailureReason reason;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Failure(GroupChangeFailureReason groupChangeFailureReason) {
            super(null);
            Intrinsics.checkNotNullParameter(groupChangeFailureReason, "reason");
            this.reason = groupChangeFailureReason;
        }

        public final GroupChangeFailureReason getReason() {
            return this.reason;
        }
    }

    public final boolean isFailure() {
        return this instanceof Failure;
    }
}
