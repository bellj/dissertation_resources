package org.thoughtcrime.securesms.groups.ui.creategroup;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import j$.util.Collection$EL;
import j$.util.Optional;
import j$.util.function.Consumer;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.ContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Stopwatch;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public class CreateGroupActivity extends ContactSelectionActivity {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final short REQUEST_CODE_ADD_DETAILS;
    private static final String TAG = Log.tag(CreateGroupActivity.class);
    private FloatingActionButton next;
    private MaterialButton skip;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, CreateGroupActivity.class);
        intent.putExtra("refreshable", false);
        intent.putExtra(ContactSelectionActivity.EXTRA_LAYOUT_RES_ID, R.layout.create_group_activity);
        intent.putExtra("display_mode", Util.isDefaultSmsProvider(context) ? 3 : 1);
        intent.putExtra("selection_limits", FeatureFlags.groupLimits().excludingSelf());
        return intent;
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.skip = (MaterialButton) findViewById(R.id.skip);
        this.next = (FloatingActionButton) findViewById(R.id.next);
        extendSkip();
        this.skip.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CreateGroupActivity.this.lambda$onCreate$0(view);
            }
        });
        this.next.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                CreateGroupActivity.this.lambda$onCreate$1(view);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        handleNextPressed();
    }

    public /* synthetic */ void lambda$onCreate$1(View view) {
        handleNextPressed();
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 17275 && i2 == -1) {
            finish();
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer) {
        if (this.contactsFragment.hasQueryFilter()) {
            getContactFilterView().clear();
        }
        shrinkSkip();
        consumer.accept(Boolean.TRUE);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
        if (this.contactsFragment.hasQueryFilter()) {
            getContactFilterView().clear();
        }
        if (this.contactsFragment.getSelectedContactsCount() == 0) {
            extendSkip();
        }
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
        int totalMemberCount = this.contactsFragment.getTotalMemberCount();
        if (totalMemberCount == 0) {
            getToolbar().setTitle(getString(R.string.CreateGroupActivity__select_members));
        } else {
            getToolbar().setTitle(getResources().getQuantityString(R.plurals.CreateGroupActivity__d_members, totalMemberCount, Integer.valueOf(totalMemberCount)));
        }
    }

    private void extendSkip() {
        this.skip.setVisibility(0);
        this.next.setVisibility(8);
    }

    private void shrinkSkip() {
        this.skip.setVisibility(8);
        this.next.setVisibility(0);
    }

    private void handleNextPressed() {
        Stopwatch stopwatch = new Stopwatch("Recipient Refresh");
        SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(stopwatch) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity$$ExternalSyntheticLambda2
            public final /* synthetic */ Stopwatch f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return CreateGroupActivity.this.lambda$handleNextPressed$4(this.f$1);
            }
        }, new SimpleTask.ForegroundTask(SimpleProgressDialog.showDelayed(this), stopwatch) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity$$ExternalSyntheticLambda3
            public final /* synthetic */ SimpleProgressDialog.DismissibleDialog f$1;
            public final /* synthetic */ Stopwatch f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                CreateGroupActivity.this.lambda$handleNextPressed$5(this.f$1, this.f$2, (List) obj);
            }
        });
    }

    public /* synthetic */ List lambda$handleNextPressed$4(Stopwatch stopwatch) {
        List list = (List) Collection$EL.stream(this.contactsFragment.getSelectedContacts()).map(new Function() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return CreateGroupActivity.this.lambda$handleNextPressed$2((SelectedContact) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList());
        List<Recipient> resolvedList = Recipient.resolvedList(list);
        stopwatch.split("resolve");
        Set<Recipient> set = (Set) Collection$EL.stream(resolvedList).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity$$ExternalSyntheticLambda5
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return CreateGroupActivity.lambda$handleNextPressed$3((Recipient) obj);
            }
        }).collect(Collectors.toSet());
        String str = TAG;
        Log.i(str, "Need to do " + set.size() + " registration checks.");
        for (Recipient recipient : set) {
            try {
                ContactDiscovery.refresh((Context) this, recipient, false);
            } catch (IOException e) {
                String str2 = TAG;
                Log.w(str2, "Failed to refresh registered status for " + recipient.getId(), e);
            }
        }
        stopwatch.split(RecipientDatabase.REGISTERED);
        return list;
    }

    public /* synthetic */ RecipientId lambda$handleNextPressed$2(SelectedContact selectedContact) {
        return selectedContact.getOrCreateRecipientId(this);
    }

    public static /* synthetic */ boolean lambda$handleNextPressed$3(Recipient recipient) {
        return recipient.getRegistered() == RecipientDatabase.RegisteredState.UNKNOWN;
    }

    public /* synthetic */ void lambda$handleNextPressed$5(SimpleProgressDialog.DismissibleDialog dismissibleDialog, Stopwatch stopwatch, List list) {
        dismissibleDialog.dismiss();
        stopwatch.stop(TAG);
        startActivityForResult(AddGroupDetailsActivity.newIntent(this, list), 17275);
    }
}
