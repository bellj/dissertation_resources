package org.thoughtcrime.securesms.groups.v2.processing;

import java.util.Collection;

/* loaded from: classes4.dex */
public final class AdvanceGroupStateResult {
    private final GlobalGroupState newGlobalGroupState;
    private final Collection<LocalGroupLogEntry> processedLogEntries;

    public AdvanceGroupStateResult(Collection<LocalGroupLogEntry> collection, GlobalGroupState globalGroupState) {
        this.processedLogEntries = collection;
        this.newGlobalGroupState = globalGroupState;
    }

    public Collection<LocalGroupLogEntry> getProcessedLogEntries() {
        return this.processedLogEntries;
    }

    public GlobalGroupState getNewGlobalGroupState() {
        return this.newGlobalGroupState;
    }
}
