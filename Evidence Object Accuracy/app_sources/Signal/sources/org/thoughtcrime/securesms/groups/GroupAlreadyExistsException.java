package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupAlreadyExistsException extends GroupChangeException {
    public GroupAlreadyExistsException(Throwable th) {
        super(th);
    }
}
