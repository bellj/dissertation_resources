package org.thoughtcrime.securesms.groups.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class GroupMemberListAdapter extends RecyclerView.Adapter<ViewHolder> {
    private static final int FULL_MEMBER;
    private static final int NEW_GROUP_CANDIDATE;
    private static final int OTHER_INVITE_PENDING_COUNT;
    private static final int OWN_INVITE_PENDING;
    private static final int REQUESTING_MEMBER;
    private AdminActionsListener adminActionsListener;
    private final List<GroupMemberEntry> data = new ArrayList();
    private final LifecycleOwner lifecycleOwner;
    private RecipientClickListener recipientClickListener;
    private RecipientLongClickListener recipientLongClickListener;
    private RecipientSelectionChangeListener recipientSelectionChangeListener;
    private final boolean selectable;
    private final Set<GroupMemberEntry> selection = new HashSet();
    private final SelectionChangeListener selectionChangeListener = new SelectionChangeListener();

    public GroupMemberListAdapter(boolean z, LifecycleOwner lifecycleOwner) {
        this.selectable = z;
        this.lifecycleOwner = lifecycleOwner;
    }

    public void updateData(List<? extends GroupMemberEntry> list) {
        if (this.data.isEmpty()) {
            this.data.addAll(list);
            notifyDataSetChanged();
            return;
        }
        DiffUtil.DiffResult calculateDiff = DiffUtil.calculateDiff(new DiffCallback(this.data, list));
        this.data.clear();
        this.data.addAll(list);
        if (!this.selection.isEmpty()) {
            HashSet hashSet = new HashSet();
            for (GroupMemberEntry groupMemberEntry : list) {
                if (this.selection.contains(groupMemberEntry)) {
                    hashSet.add(groupMemberEntry);
                }
            }
            this.selection.clear();
            this.selection.addAll(hashSet);
            RecipientSelectionChangeListener recipientSelectionChangeListener = this.recipientSelectionChangeListener;
            if (recipientSelectionChangeListener != null) {
                recipientSelectionChangeListener.onSelectionChanged(this.selection);
            }
        }
        calculateDiff.dispatchUpdatesTo(this);
    }

    public void onViewDetachedFromWindow(ViewHolder viewHolder) {
        super.onViewDetachedFromWindow((GroupMemberListAdapter) viewHolder);
        viewHolder.unbind();
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new FullMemberViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_recipient_list_item, viewGroup, false), this.recipientClickListener, this.recipientLongClickListener, this.adminActionsListener, this.selectionChangeListener, this.selectable, this.lifecycleOwner);
        }
        if (i == 1) {
            return new OwnInvitePendingMemberViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_recipient_list_item, viewGroup, false), this.recipientClickListener, this.recipientLongClickListener, this.adminActionsListener, this.selectionChangeListener, this.selectable, this.lifecycleOwner);
        }
        if (i == 2) {
            return new UnknownPendingMemberCountViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_recipient_list_item, viewGroup, false), this.adminActionsListener, this.selectionChangeListener, this.selectable, this.lifecycleOwner);
        }
        if (i == 3) {
            return new NewGroupInviteeViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_new_candidate_recipient_list_item, viewGroup, false), this.recipientClickListener, this.recipientLongClickListener, this.selectionChangeListener, this.selectable, this.lifecycleOwner);
        }
        if (i == 4) {
            return new RequestingMemberViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.group_recipient_requesting_list_item, viewGroup, false), this.recipientClickListener, this.recipientLongClickListener, this.adminActionsListener, this.selectionChangeListener, this.selectable, this.lifecycleOwner);
        }
        throw new AssertionError();
    }

    public void setAdminActionsListener(AdminActionsListener adminActionsListener) {
        this.adminActionsListener = adminActionsListener;
    }

    public void setRecipientClickListener(RecipientClickListener recipientClickListener) {
        this.recipientClickListener = recipientClickListener;
    }

    public void setRecipientLongClickListener(RecipientLongClickListener recipientLongClickListener) {
        this.recipientLongClickListener = recipientLongClickListener;
    }

    public void setRecipientSelectionChangeListener(RecipientSelectionChangeListener recipientSelectionChangeListener) {
        this.recipientSelectionChangeListener = recipientSelectionChangeListener;
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        GroupMemberEntry groupMemberEntry = this.data.get(i);
        viewHolder.bind(groupMemberEntry, this.selection.contains(groupMemberEntry));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        GroupMemberEntry groupMemberEntry = this.data.get(i);
        if (groupMemberEntry instanceof GroupMemberEntry.FullMember) {
            return 0;
        }
        if (groupMemberEntry instanceof GroupMemberEntry.PendingMember) {
            return 1;
        }
        if (groupMemberEntry instanceof GroupMemberEntry.UnknownPendingMemberCount) {
            return 2;
        }
        if (groupMemberEntry instanceof GroupMemberEntry.NewGroupCandidate) {
            return 3;
        }
        if (groupMemberEntry instanceof GroupMemberEntry.RequestingMember) {
            return 4;
        }
        throw new AssertionError();
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.data.size();
    }

    /* loaded from: classes4.dex */
    public static abstract class ViewHolder extends RecyclerView.ViewHolder {
        final EmojiTextView about;
        final View admin;
        final AdminActionsListener adminActionsListener;
        final AvatarImageView avatar;
        final BadgeImageView badge;
        final Observer<Boolean> busyObserver = new GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda1(this);
        final ProgressBar busyProgress;
        final Context context;
        final LifecycleOwner lifecycleOwner;
        final PopupMenuView popupMenu;
        final View popupMenuContainer;
        final TextView recipient;
        final RecipientClickListener recipientClickListener;
        final RecipientLongClickListener recipientLongClickListener;
        final boolean selectable;
        final CheckBox selected;
        final SelectionChangeListener selectionChangeListener;
        Runnable stopListeningToBusyChanges;

        ViewHolder(View view, RecipientClickListener recipientClickListener, RecipientLongClickListener recipientLongClickListener, AdminActionsListener adminActionsListener, SelectionChangeListener selectionChangeListener, boolean z, LifecycleOwner lifecycleOwner) {
            super(view);
            this.context = view.getContext();
            this.avatar = (AvatarImageView) view.findViewById(R.id.recipient_avatar);
            this.badge = (BadgeImageView) view.findViewById(R.id.recipient_badge);
            this.recipient = (TextView) view.findViewById(R.id.recipient_name);
            this.about = (EmojiTextView) view.findViewById(R.id.recipient_about);
            this.selected = (CheckBox) view.findViewById(R.id.recipient_selected);
            this.popupMenu = (PopupMenuView) view.findViewById(R.id.popupMenu);
            this.popupMenuContainer = view.findViewById(R.id.popupMenuProgressContainer);
            this.busyProgress = (ProgressBar) view.findViewById(R.id.menuBusyProgress);
            this.admin = view.findViewById(R.id.admin);
            this.recipientClickListener = recipientClickListener;
            this.recipientLongClickListener = recipientLongClickListener;
            this.adminActionsListener = adminActionsListener;
            this.selectionChangeListener = selectionChangeListener;
            this.selectable = z;
            this.lifecycleOwner = lifecycleOwner;
        }

        public void onBusyChanged(boolean z) {
            int i = 0;
            this.busyProgress.setVisibility(z ? 0 : 8);
            PopupMenuView popupMenuView = this.popupMenu;
            if (z) {
                i = 8;
            }
            popupMenuView.setVisibility(i);
        }

        void unbind() {
            Runnable runnable = this.stopListeningToBusyChanges;
            if (runnable != null) {
                runnable.run();
            }
        }

        void bindRecipient(Recipient recipient) {
            String str;
            if (recipient.isSelf()) {
                str = this.context.getString(R.string.GroupMembersDialog_you);
            } else {
                str = recipient.getDisplayName(this.itemView.getContext());
            }
            bindImageAndText(recipient, str, recipient.getCombinedAboutAndEmoji());
        }

        void bindImageAndText(Recipient recipient, String str, String str2) {
            this.recipient.setText(str);
            this.avatar.setRecipient(recipient);
            BadgeImageView badgeImageView = this.badge;
            int i = 0;
            if (badgeImageView != null) {
                badgeImageView.setBadgeFromRecipient(recipient);
                this.badge.setClickable(false);
            }
            EmojiTextView emojiTextView = this.about;
            if (emojiTextView != null) {
                emojiTextView.setText(str2);
                EmojiTextView emojiTextView2 = this.about;
                if (Util.isEmpty(str2)) {
                    i = 8;
                }
                emojiTextView2.setVisibility(i);
            }
        }

        void bindRecipientClick(Recipient recipient) {
            if (recipient.equals(Recipient.self())) {
                this.itemView.setEnabled(false);
                return;
            }
            this.itemView.setEnabled(true);
            this.itemView.setOnClickListener(new GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda2(this, recipient));
            this.itemView.setOnLongClickListener(new GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda3(this, recipient));
        }

        public /* synthetic */ void lambda$bindRecipientClick$0(Recipient recipient, View view) {
            if (getAdapterPosition() != -1) {
                RecipientClickListener recipientClickListener = this.recipientClickListener;
                if (recipientClickListener != null) {
                    recipientClickListener.onClick(recipient);
                }
                if (this.selected != null) {
                    this.selectionChangeListener.onSelectionChange(getAdapterPosition(), !this.selected.isChecked());
                }
            }
        }

        public /* synthetic */ boolean lambda$bindRecipientClick$1(Recipient recipient, View view) {
            if (this.recipientLongClickListener == null || getAdapterPosition() == -1) {
                return false;
            }
            return this.recipientLongClickListener.onLongClick(recipient);
        }

        void bind(GroupMemberEntry groupMemberEntry, boolean z) {
            this.busyProgress.setVisibility(8);
            View view = this.admin;
            if (view != null) {
                view.setVisibility(8);
            }
            hideMenu();
            this.itemView.setOnClickListener(null);
            groupMemberEntry.getBusy().observe(this.lifecycleOwner, this.busyObserver);
            this.stopListeningToBusyChanges = new GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda0(this, groupMemberEntry);
            this.selected.setChecked(z);
            if (this.selectable || z) {
                this.selected.setVisibility(0);
            } else {
                this.selected.setVisibility(8);
            }
        }

        public /* synthetic */ void lambda$bind$2(GroupMemberEntry groupMemberEntry) {
            groupMemberEntry.getBusy().removeObserver(this.busyObserver);
        }

        void hideMenu() {
            this.popupMenuContainer.setVisibility(8);
            this.popupMenu.setVisibility(8);
        }

        void showMenu() {
            this.popupMenuContainer.setVisibility(0);
            this.popupMenu.setVisibility(0);
        }
    }

    /* loaded from: classes4.dex */
    public static final class FullMemberViewHolder extends ViewHolder {
        FullMemberViewHolder(View view, RecipientClickListener recipientClickListener, RecipientLongClickListener recipientLongClickListener, AdminActionsListener adminActionsListener, SelectionChangeListener selectionChangeListener, boolean z, LifecycleOwner lifecycleOwner) {
            super(view, recipientClickListener, recipientLongClickListener, adminActionsListener, selectionChangeListener, z, lifecycleOwner);
        }

        void bind(GroupMemberEntry groupMemberEntry, boolean z) {
            super.bind(groupMemberEntry, z);
            GroupMemberEntry.FullMember fullMember = (GroupMemberEntry.FullMember) groupMemberEntry;
            bindRecipient(fullMember.getMember());
            bindRecipientClick(fullMember.getMember());
            View view = this.admin;
            if (view != null) {
                view.setVisibility(fullMember.isAdmin() ? 0 : 4);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class NewGroupInviteeViewHolder extends ViewHolder {
        private final View smsContact;
        private final View smsWarning;

        NewGroupInviteeViewHolder(View view, RecipientClickListener recipientClickListener, RecipientLongClickListener recipientLongClickListener, SelectionChangeListener selectionChangeListener, boolean z, LifecycleOwner lifecycleOwner) {
            super(view, recipientClickListener, recipientLongClickListener, null, selectionChangeListener, z, lifecycleOwner);
            this.smsContact = view.findViewById(R.id.sms_contact);
            this.smsWarning = view.findViewById(R.id.sms_warning);
        }

        void bind(GroupMemberEntry groupMemberEntry, boolean z) {
            GroupMemberEntry.NewGroupCandidate newGroupCandidate = (GroupMemberEntry.NewGroupCandidate) groupMemberEntry;
            bindRecipient(newGroupCandidate.getMember());
            bindRecipientClick(newGroupCandidate.getMember());
            int i = newGroupCandidate.getMember().isRegistered() ? 8 : 0;
            this.smsContact.setVisibility(i);
            this.smsWarning.setVisibility(i);
        }
    }

    /* loaded from: classes4.dex */
    public static final class OwnInvitePendingMemberViewHolder extends ViewHolder {
        OwnInvitePendingMemberViewHolder(View view, RecipientClickListener recipientClickListener, RecipientLongClickListener recipientLongClickListener, AdminActionsListener adminActionsListener, SelectionChangeListener selectionChangeListener, boolean z, LifecycleOwner lifecycleOwner) {
            super(view, recipientClickListener, recipientLongClickListener, adminActionsListener, selectionChangeListener, z, lifecycleOwner);
        }

        void bind(GroupMemberEntry groupMemberEntry, boolean z) {
            super.bind(groupMemberEntry, z);
            GroupMemberEntry.PendingMember pendingMember = (GroupMemberEntry.PendingMember) groupMemberEntry;
            bindImageAndText(pendingMember.getInvitee(), pendingMember.getInvitee().getDisplayNameOrUsername(this.context), pendingMember.getInvitee().getCombinedAboutAndEmoji());
            bindRecipientClick(pendingMember.getInvitee());
            if (pendingMember.isCancellable() && this.adminActionsListener != null) {
                this.popupMenu.setMenu(R.menu.own_invite_pending_menu, new GroupMemberListAdapter$OwnInvitePendingMemberViewHolder$$ExternalSyntheticLambda0(this, pendingMember));
                showMenu();
            }
        }

        public /* synthetic */ boolean lambda$bind$0(GroupMemberEntry.PendingMember pendingMember, int i) {
            if (i != R.id.revoke_invite) {
                return false;
            }
            this.adminActionsListener.onRevokeInvite(pendingMember);
            return true;
        }
    }

    /* loaded from: classes4.dex */
    public static final class UnknownPendingMemberCountViewHolder extends ViewHolder {
        UnknownPendingMemberCountViewHolder(View view, AdminActionsListener adminActionsListener, SelectionChangeListener selectionChangeListener, boolean z, LifecycleOwner lifecycleOwner) {
            super(view, null, null, adminActionsListener, selectionChangeListener, z, lifecycleOwner);
        }

        void bind(GroupMemberEntry groupMemberEntry, boolean z) {
            super.bind(groupMemberEntry, z);
            GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount = (GroupMemberEntry.UnknownPendingMemberCount) groupMemberEntry;
            Recipient inviter = unknownPendingMemberCount.getInviter();
            bindImageAndText(inviter, this.context.getResources().getQuantityString(R.plurals.GroupMemberList_invited, unknownPendingMemberCount.getInviteCount(), inviter.getDisplayName(this.itemView.getContext()), Integer.valueOf(unknownPendingMemberCount.getInviteCount())), inviter.getAbout());
            if (unknownPendingMemberCount.isCancellable() && this.adminActionsListener != null) {
                this.popupMenu.setMenu(R.menu.others_invite_pending_menu, new GroupMemberListAdapter$UnknownPendingMemberCountViewHolder$$ExternalSyntheticLambda0(this, unknownPendingMemberCount), new GroupMemberListAdapter$UnknownPendingMemberCountViewHolder$$ExternalSyntheticLambda1(this, unknownPendingMemberCount));
                showMenu();
            }
        }

        public /* synthetic */ boolean lambda$bind$0(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount, MenuItem menuItem) {
            if (menuItem.getItemId() == R.id.revoke_invites) {
                menuItem.setTitle(this.context.getResources().getQuantityString(R.plurals.PendingMembersActivity_revoke_d_invites, unknownPendingMemberCount.getInviteCount(), Integer.valueOf(unknownPendingMemberCount.getInviteCount())));
            }
            return true;
        }

        public /* synthetic */ boolean lambda$bind$1(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount, int i) {
            if (i != R.id.revoke_invites) {
                return false;
            }
            this.adminActionsListener.onRevokeAllInvites(unknownPendingMemberCount);
            return true;
        }
    }

    /* loaded from: classes4.dex */
    public static final class RequestingMemberViewHolder extends ViewHolder {
        private final View approveRequest;
        private final View denyRequest;

        RequestingMemberViewHolder(View view, RecipientClickListener recipientClickListener, RecipientLongClickListener recipientLongClickListener, AdminActionsListener adminActionsListener, SelectionChangeListener selectionChangeListener, boolean z, LifecycleOwner lifecycleOwner) {
            super(view, recipientClickListener, recipientLongClickListener, adminActionsListener, selectionChangeListener, z, lifecycleOwner);
            this.approveRequest = view.findViewById(R.id.request_approve);
            this.denyRequest = view.findViewById(R.id.request_deny);
        }

        void bind(GroupMemberEntry groupMemberEntry, boolean z) {
            super.bind(groupMemberEntry, z);
            GroupMemberEntry.RequestingMember requestingMember = (GroupMemberEntry.RequestingMember) groupMemberEntry;
            if (this.adminActionsListener == null || !requestingMember.isApprovableDeniable()) {
                this.approveRequest.setVisibility(8);
                this.denyRequest.setVisibility(8);
                this.approveRequest.setOnClickListener(null);
                this.denyRequest.setOnClickListener(null);
            } else {
                this.approveRequest.setVisibility(0);
                this.denyRequest.setVisibility(0);
                this.approveRequest.setOnClickListener(new GroupMemberListAdapter$RequestingMemberViewHolder$$ExternalSyntheticLambda0(this, requestingMember));
                this.denyRequest.setOnClickListener(new GroupMemberListAdapter$RequestingMemberViewHolder$$ExternalSyntheticLambda1(this, requestingMember));
            }
            bindRecipient(requestingMember.getRequester());
            bindRecipientClick(requestingMember.getRequester());
        }

        public /* synthetic */ void lambda$bind$0(GroupMemberEntry.RequestingMember requestingMember, View view) {
            this.adminActionsListener.onApproveRequest(requestingMember);
        }

        public /* synthetic */ void lambda$bind$1(GroupMemberEntry.RequestingMember requestingMember, View view) {
            this.adminActionsListener.onDenyRequest(requestingMember);
        }
    }

    /* loaded from: classes4.dex */
    public final class SelectionChangeListener {
        private SelectionChangeListener() {
            GroupMemberListAdapter.this = r1;
        }

        void onSelectionChange(int i, boolean z) {
            if (GroupMemberListAdapter.this.selectable) {
                if (z) {
                    GroupMemberListAdapter.this.selection.add((GroupMemberEntry) GroupMemberListAdapter.this.data.get(i));
                } else {
                    GroupMemberListAdapter.this.selection.remove(GroupMemberListAdapter.this.data.get(i));
                }
                GroupMemberListAdapter.this.notifyItemChanged(i);
                if (GroupMemberListAdapter.this.recipientSelectionChangeListener != null) {
                    GroupMemberListAdapter.this.recipientSelectionChangeListener.onSelectionChanged(GroupMemberListAdapter.this.selection);
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class DiffCallback extends DiffUtil.Callback {
        private final List<? extends GroupMemberEntry> newData;
        private final List<? extends GroupMemberEntry> oldData;

        DiffCallback(List<? extends GroupMemberEntry> list, List<? extends GroupMemberEntry> list2) {
            this.oldData = list;
            this.newData = list2;
        }

        public int getOldListSize() {
            return this.oldData.size();
        }

        public int getNewListSize() {
            return this.newData.size();
        }

        public boolean areItemsTheSame(int i, int i2) {
            return ((GroupMemberEntry) this.oldData.get(i)).sameId((GroupMemberEntry) this.newData.get(i2));
        }

        public boolean areContentsTheSame(int i, int i2) {
            return ((GroupMemberEntry) this.oldData.get(i)).equals((GroupMemberEntry) this.newData.get(i2));
        }
    }
}
