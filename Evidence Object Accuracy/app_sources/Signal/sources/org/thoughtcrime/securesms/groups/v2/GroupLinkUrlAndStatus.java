package org.thoughtcrime.securesms.groups.v2;

/* loaded from: classes4.dex */
public final class GroupLinkUrlAndStatus {
    public static final GroupLinkUrlAndStatus NONE = new GroupLinkUrlAndStatus(false, false, "");
    private final boolean enabled;
    private final boolean requiresApproval;
    private final String url;

    public GroupLinkUrlAndStatus(boolean z, boolean z2, String str) {
        this.enabled = z;
        this.requiresApproval = z2;
        this.url = str;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public boolean isRequiresApproval() {
        return this.requiresApproval;
    }

    public String getUrl() {
        return this.url;
    }
}
