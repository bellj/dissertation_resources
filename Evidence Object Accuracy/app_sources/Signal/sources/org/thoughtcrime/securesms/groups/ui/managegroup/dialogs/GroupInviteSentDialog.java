package org.thoughtcrime.securesms.groups.ui.managegroup.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LifecycleOwner;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class GroupInviteSentDialog {
    private GroupInviteSentDialog() {
    }

    public static Dialog showInvitesSent(Context context, LifecycleOwner lifecycleOwner, List<Recipient> list) {
        int size = list.size();
        if (size == 0) {
            return null;
        }
        AlertDialog.Builder positiveButton = new AlertDialog.Builder(context).setTitle(context.getResources().getQuantityString(R.plurals.GroupManagement_invitation_sent, size, Integer.valueOf(size))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null);
        if (size == 1) {
            positiveButton.setMessage(context.getString(R.string.GroupManagement_invite_single_user, list.get(0).getDisplayName(context)));
        } else {
            positiveButton.setMessage(R.string.GroupManagement_invite_multiple_users).setView(R.layout.dialog_multiple_group_invites_sent);
        }
        AlertDialog show = positiveButton.show();
        if (size > 1) {
            GroupMemberListView groupMemberListView = (GroupMemberListView) show.findViewById(R.id.list_invitees);
            groupMemberListView.initializeAdapter(lifecycleOwner);
            ArrayList arrayList = new ArrayList(list.size());
            for (Recipient recipient : list) {
                arrayList.add(new GroupMemberEntry.PendingMember(recipient));
            }
            groupMemberListView.setMembers(arrayList);
        }
        return show;
    }
}
