package org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting;

import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMembersFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class RequestingMembersFragment$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ RequestingMembersFragment.AnonymousClass1 f$0;
    public final /* synthetic */ GroupMemberEntry.RequestingMember f$1;

    public /* synthetic */ RequestingMembersFragment$1$$ExternalSyntheticLambda0(RequestingMembersFragment.AnonymousClass1 r1, GroupMemberEntry.RequestingMember requestingMember) {
        this.f$0 = r1;
        this.f$1 = requestingMember;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$onDenyRequest$1(this.f$1);
    }
}
