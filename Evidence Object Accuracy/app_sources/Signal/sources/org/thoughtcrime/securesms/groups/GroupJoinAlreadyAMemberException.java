package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupJoinAlreadyAMemberException extends GroupChangeException {
    public GroupJoinAlreadyAMemberException(Throwable th) {
        super(th);
    }
}
