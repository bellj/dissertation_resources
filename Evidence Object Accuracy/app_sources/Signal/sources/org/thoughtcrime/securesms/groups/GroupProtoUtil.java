package org.thoughtcrime.securesms.groups;

import android.content.Context;
import com.google.protobuf.ByteString;
import java.util.List;
import java.util.UUID;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.storageservice.protos.groups.GroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.thoughtcrime.securesms.database.model.databaseprotos.DecryptedGroupV2Context;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.groupsv2.PartialDecryptedGroup;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class GroupProtoUtil {
    private GroupProtoUtil() {
    }

    public static int findRevisionWeWereAdded(PartialDecryptedGroup partialDecryptedGroup, UUID uuid) throws GroupNotAMemberException {
        ByteString byteString = UuidUtil.toByteString(uuid);
        for (DecryptedMember decryptedMember : partialDecryptedGroup.getMembersList()) {
            if (decryptedMember.getUuid().equals(byteString)) {
                return decryptedMember.getJoinedAtRevision();
            }
        }
        for (DecryptedPendingMember decryptedPendingMember : partialDecryptedGroup.getPendingMembersList()) {
            if (decryptedPendingMember.getUuid().equals(byteString)) {
                return partialDecryptedGroup.getRevision();
            }
        }
        throw new GroupNotAMemberException();
    }

    public static DecryptedGroupV2Context createDecryptedGroupV2Context(GroupMasterKey groupMasterKey, GroupMutation groupMutation, GroupChange groupChange) {
        DecryptedGroupChange groupChange2 = groupMutation.getGroupChange();
        DecryptedGroup newGroupState = groupMutation.getNewGroupState();
        SignalServiceProtos.GroupContextV2.Builder revision = SignalServiceProtos.GroupContextV2.newBuilder().setMasterKey(ByteString.copyFrom(groupMasterKey.serialize())).setRevision(groupChange2 != null ? groupChange2.getRevision() : newGroupState.getRevision());
        if (groupChange != null) {
            revision.setGroupChange(groupChange.toByteString());
        }
        DecryptedGroupV2Context.Builder groupState = DecryptedGroupV2Context.newBuilder().setContext(revision.build()).setGroupState(newGroupState);
        if (groupMutation.getPreviousGroupState() != null) {
            groupState.setPreviousGroupState(groupMutation.getPreviousGroupState());
        }
        if (groupChange2 != null) {
            groupState.setChange(groupChange2);
        }
        return groupState.build();
    }

    public static Recipient pendingMemberToRecipient(Context context, DecryptedPendingMember decryptedPendingMember) {
        return uuidByteStringToRecipient(context, decryptedPendingMember.getUuid());
    }

    public static Recipient uuidByteStringToRecipient(Context context, ByteString byteString) {
        ServiceId fromByteString = ServiceId.fromByteString(byteString);
        if (fromByteString.isUnknown()) {
            return Recipient.UNKNOWN;
        }
        return Recipient.externalPush(fromByteString);
    }

    public static RecipientId uuidByteStringToRecipientId(ByteString byteString) {
        ServiceId fromByteString = ServiceId.fromByteString(byteString);
        if (fromByteString.isUnknown()) {
            return RecipientId.UNKNOWN;
        }
        return RecipientId.from(fromByteString);
    }

    public static boolean isMember(UUID uuid, List<DecryptedMember> list) {
        ByteString byteString = UuidUtil.toByteString(uuid);
        for (DecryptedMember decryptedMember : list) {
            if (byteString.equals(decryptedMember.getUuid())) {
                return true;
            }
        }
        return false;
    }
}
