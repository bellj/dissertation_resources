package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class InviteRevokeConfirmationDialog {
    private InviteRevokeConfirmationDialog() {
    }

    public static AlertDialog showOwnInviteRevokeConfirmationDialog(Context context, Recipient recipient, Runnable runnable) {
        return new AlertDialog.Builder(context).setMessage(context.getString(R.string.InviteRevokeConfirmationDialog_revoke_own_single_invite, recipient.getDisplayName(context))).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.InviteRevokeConfirmationDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InviteRevokeConfirmationDialog.m1838$r8$lambda$1gSu4q3EJoNs3FhEqQocqGY8E(this.f$0, dialogInterface, i);
            }
        }).setNegativeButton(R.string.no, (DialogInterface.OnClickListener) null).show();
    }

    public static AlertDialog showOthersInviteRevokeConfirmationDialog(Context context, Recipient recipient, int i, Runnable runnable) {
        return new AlertDialog.Builder(context).setMessage(context.getResources().getQuantityString(R.plurals.InviteRevokeConfirmationDialog_revoke_others_invites, i, recipient.getDisplayName(context), Integer.valueOf(i))).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener(runnable) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.InviteRevokeConfirmationDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ Runnable f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i2) {
                InviteRevokeConfirmationDialog.$r8$lambda$ygOKRQskwl2wmcUzYFd64_7R33U(this.f$0, dialogInterface, i2);
            }
        }).setNegativeButton(R.string.no, (DialogInterface.OnClickListener) null).show();
    }
}
