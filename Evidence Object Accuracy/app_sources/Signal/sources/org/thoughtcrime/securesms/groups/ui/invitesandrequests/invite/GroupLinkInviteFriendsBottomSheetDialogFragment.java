package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import androidx.appcompat.widget.SwitchCompat;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsViewModel;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.ui.sharablegrouplink.GroupLinkBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public final class GroupLinkInviteFriendsBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private static final String ARG_GROUP_ID;
    private static final String TAG = Log.tag(GroupLinkInviteFriendsBottomSheetDialogFragment.class);
    private SimpleProgressDialog.DismissibleDialog busyDialog;
    private Group controlGroup;
    private View controlOutline;
    private Button groupLinkEnableAndShareButton;
    private Button groupLinkShareButton;
    private SwitchCompat memberApprovalSwitch;

    public static void show(FragmentManager fragmentManager, GroupId.V2 v2) {
        GroupLinkInviteFriendsBottomSheetDialogFragment groupLinkInviteFriendsBottomSheetDialogFragment = new GroupLinkInviteFriendsBottomSheetDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("group_id", v2.toString());
        groupLinkInviteFriendsBottomSheetDialogFragment.setArguments(bundle);
        groupLinkInviteFriendsBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.group_invite_link_enable_and_share_bottom_sheet, viewGroup, false);
        this.groupLinkEnableAndShareButton = (Button) inflate.findViewById(R.id.group_link_enable_and_share_button);
        this.groupLinkShareButton = (Button) inflate.findViewById(R.id.group_link_share_button);
        this.controlGroup = (Group) inflate.findViewById(R.id.control_group);
        this.controlOutline = inflate.findViewById(R.id.group_link_enable_and_share_approve_outline);
        this.memberApprovalSwitch = (SwitchCompat) inflate.findViewById(R.id.group_link_enable_and_share_approve_new_members_switch);
        inflate.findViewById(R.id.group_link_enable_and_share_cancel_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GroupLinkInviteFriendsBottomSheetDialogFragment.this.lambda$onCreateView$0(view);
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(View view) {
        dismiss();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        GroupId.V2 groupId = getGroupId();
        GroupLinkInviteFriendsViewModel groupLinkInviteFriendsViewModel = (GroupLinkInviteFriendsViewModel) ViewModelProviders.of(this, new GroupLinkInviteFriendsViewModel.Factory(requireContext().getApplicationContext(), groupId)).get(GroupLinkInviteFriendsViewModel.class);
        groupLinkInviteFriendsViewModel.getGroupInviteLinkAndStatus().observe(getViewLifecycleOwner(), new Observer(groupId) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ GroupId.V2 f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkInviteFriendsBottomSheetDialogFragment.this.lambda$onViewCreated$2(this.f$1, (GroupLinkUrlAndStatus) obj);
            }
        });
        this.controlOutline.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GroupLinkInviteFriendsViewModel.this.toggleMemberApproval();
            }
        });
        groupLinkInviteFriendsViewModel.getMemberApproval().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda3
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkInviteFriendsBottomSheetDialogFragment.this.lambda$onViewCreated$4((Boolean) obj);
            }
        });
        groupLinkInviteFriendsViewModel.isBusy().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda4
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkInviteFriendsBottomSheetDialogFragment.this.setBusy(((Boolean) obj).booleanValue());
            }
        });
        groupLinkInviteFriendsViewModel.getEnableErrors().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda5
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkInviteFriendsBottomSheetDialogFragment.this.lambda$onViewCreated$5((EnableInviteLinkError) obj);
            }
        });
        this.groupLinkEnableAndShareButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GroupLinkInviteFriendsViewModel.this.enable();
            }
        });
        groupLinkInviteFriendsViewModel.getEnableSuccess().observe(getViewLifecycleOwner(), new Observer(groupId) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda7
            public final /* synthetic */ GroupId.V2 f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupLinkInviteFriendsBottomSheetDialogFragment.this.lambda$onViewCreated$7(this.f$1, (GroupInviteLinkUrl) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$2(GroupId.V2 v2, GroupLinkUrlAndStatus groupLinkUrlAndStatus) {
        if (groupLinkUrlAndStatus.isEnabled()) {
            this.groupLinkShareButton.setVisibility(0);
            this.groupLinkEnableAndShareButton.setVisibility(4);
            this.controlGroup.setVisibility(8);
            this.groupLinkShareButton.setOnClickListener(new View.OnClickListener(v2) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$$ExternalSyntheticLambda8
                public final /* synthetic */ GroupId.V2 f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    GroupLinkInviteFriendsBottomSheetDialogFragment.this.lambda$onViewCreated$1(this.f$1, view);
                }
            });
            return;
        }
        this.controlGroup.setVisibility(0);
        this.groupLinkEnableAndShareButton.setVisibility(0);
        this.groupLinkShareButton.setVisibility(4);
    }

    public /* synthetic */ void lambda$onViewCreated$1(GroupId.V2 v2, View view) {
        shareGroupLinkAndDismiss(v2);
    }

    public /* synthetic */ void lambda$onViewCreated$4(Boolean bool) {
        this.memberApprovalSwitch.setChecked(bool.booleanValue());
    }

    public /* synthetic */ void lambda$onViewCreated$5(EnableInviteLinkError enableInviteLinkError) {
        Toast.makeText(requireContext(), errorToMessage(enableInviteLinkError), 0).show();
        if (enableInviteLinkError == EnableInviteLinkError.NOT_IN_GROUP || enableInviteLinkError == EnableInviteLinkError.INSUFFICIENT_RIGHTS) {
            dismiss();
        }
    }

    public /* synthetic */ void lambda$onViewCreated$7(GroupId.V2 v2, GroupInviteLinkUrl groupInviteLinkUrl) {
        Log.i(TAG, "Group link enabled, sharing");
        shareGroupLinkAndDismiss(v2);
    }

    protected void shareGroupLinkAndDismiss(GroupId.V2 v2) {
        dismiss();
        GroupLinkBottomSheetDialogFragment.show(requireFragmentManager(), v2);
    }

    protected GroupId.V2 getGroupId() {
        try {
            String string = requireArguments().getString("group_id");
            Objects.requireNonNull(string);
            return GroupId.parse(string).requireV2();
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public void setBusy(boolean z) {
        if (!z) {
            SimpleProgressDialog.DismissibleDialog dismissibleDialog = this.busyDialog;
            if (dismissibleDialog != null) {
                dismissibleDialog.dismiss();
                this.busyDialog = null;
            }
        } else if (this.busyDialog == null) {
            this.busyDialog = SimpleProgressDialog.showDelayed(requireContext());
        }
    }

    /* renamed from: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsBottomSheetDialogFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$invite$EnableInviteLinkError;

        static {
            int[] iArr = new int[EnableInviteLinkError.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$invite$EnableInviteLinkError = iArr;
            try {
                iArr[EnableInviteLinkError.NETWORK_ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$invite$EnableInviteLinkError[EnableInviteLinkError.INSUFFICIENT_RIGHTS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$invite$EnableInviteLinkError[EnableInviteLinkError.NOT_IN_GROUP.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    private String errorToMessage(EnableInviteLinkError enableInviteLinkError) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$ui$invitesandrequests$invite$EnableInviteLinkError[enableInviteLinkError.ordinal()];
        if (i == 1) {
            return getString(R.string.GroupInviteLinkEnableAndShareBottomSheetDialogFragment_encountered_a_network_error);
        }
        if (i == 2) {
            return getString(R.string.GroupInviteLinkEnableAndShareBottomSheetDialogFragment_you_dont_have_the_right_to_enable_group_link);
        }
        if (i != 3) {
            return getString(R.string.GroupInviteLinkEnableAndShareBottomSheetDialogFragment_unable_to_enable_group_link_please_try_again_later);
        }
        return getString(R.string.GroupInviteLinkEnableAndShareBottomSheetDialogFragment_you_are_not_currently_a_member_of_the_group);
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }
}
