package org.thoughtcrime.securesms.groups.v2.processing;

import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;

/* loaded from: classes4.dex */
public final class ServerGroupLogEntry {
    private static final String TAG = Log.tag(ServerGroupLogEntry.class);
    private final DecryptedGroupChange change;
    private final DecryptedGroup group;

    public ServerGroupLogEntry(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) {
        if (!(decryptedGroupChange == null || decryptedGroup == null || decryptedGroup.getRevision() == decryptedGroupChange.getRevision())) {
            Log.w(TAG, "Ignoring change with revision number not matching group");
            decryptedGroupChange = null;
        }
        if (decryptedGroupChange == null && decryptedGroup == null) {
            throw new AssertionError();
        }
        this.group = decryptedGroup;
        this.change = decryptedGroupChange;
    }

    public DecryptedGroup getGroup() {
        return this.group;
    }

    public DecryptedGroupChange getChange() {
        return this.change;
    }

    public int getRevision() {
        DecryptedGroup decryptedGroup = this.group;
        if (decryptedGroup != null) {
            return decryptedGroup.getRevision();
        }
        DecryptedGroupChange decryptedGroupChange = this.change;
        if (decryptedGroupChange != null) {
            return decryptedGroupChange.getRevision();
        }
        throw new AssertionError();
    }
}
