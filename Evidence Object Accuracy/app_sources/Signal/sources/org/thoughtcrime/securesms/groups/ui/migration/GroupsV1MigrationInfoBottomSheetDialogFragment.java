package org.thoughtcrime.securesms.groups.ui.migration;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.ThemeUtil;

/* loaded from: classes4.dex */
public final class GroupsV1MigrationInfoBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private static final String KEY_MEMBERSHIP_CHANGE;
    private View droppedContainer;
    private GroupMemberListView droppedList;
    private TextView droppedTitle;
    private View pendingContainer;
    private GroupMemberListView pendingList;
    private TextView pendingTitle;
    private GroupsV1MigrationInfoViewModel viewModel;

    public static void show(FragmentManager fragmentManager, GroupMigrationMembershipChange groupMigrationMembershipChange) {
        Bundle bundle = new Bundle();
        bundle.putString(KEY_MEMBERSHIP_CHANGE, groupMigrationMembershipChange.serialize());
        GroupsV1MigrationInfoBottomSheetDialogFragment groupsV1MigrationInfoBottomSheetDialogFragment = new GroupsV1MigrationInfoBottomSheetDialogFragment();
        groupsV1MigrationInfoBottomSheetDialogFragment.setArguments(bundle);
        groupsV1MigrationInfoBottomSheetDialogFragment.show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, ThemeUtil.isDarkTheme(requireContext()) ? R.style.Theme_Signal_RoundedBottomSheet : R.style.Theme_Signal_RoundedBottomSheet_Light);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.groupsv1_migration_learn_more_bottom_sheet, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.pendingContainer = view.findViewById(R.id.gv1_learn_more_pending_container);
        this.pendingTitle = (TextView) view.findViewById(R.id.gv1_learn_more_pending_title);
        this.pendingList = (GroupMemberListView) view.findViewById(R.id.gv1_learn_more_pending_list);
        this.droppedContainer = view.findViewById(R.id.gv1_learn_more_dropped_container);
        this.droppedTitle = (TextView) view.findViewById(R.id.gv1_learn_more_dropped_title);
        this.droppedList = (GroupMemberListView) view.findViewById(R.id.gv1_learn_more_dropped_list);
        this.pendingList.initializeAdapter(getViewLifecycleOwner());
        this.droppedList.initializeAdapter(getViewLifecycleOwner());
        GroupsV1MigrationInfoViewModel groupsV1MigrationInfoViewModel = (GroupsV1MigrationInfoViewModel) ViewModelProviders.of(this, new GroupsV1MigrationInfoViewModel.Factory(GroupMigrationMembershipChange.deserialize(getArguments().getString(KEY_MEMBERSHIP_CHANGE)))).get(GroupsV1MigrationInfoViewModel.class);
        this.viewModel = groupsV1MigrationInfoViewModel;
        groupsV1MigrationInfoViewModel.getPendingMembers().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoBottomSheetDialogFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupsV1MigrationInfoBottomSheetDialogFragment.this.onPendingMembersChanged((List) obj);
            }
        });
        this.viewModel.getDroppedMembers().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoBottomSheetDialogFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                GroupsV1MigrationInfoBottomSheetDialogFragment.this.onDroppedMembersChanged((List) obj);
            }
        });
        view.findViewById(R.id.gv1_learn_more_ok_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoBottomSheetDialogFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                GroupsV1MigrationInfoBottomSheetDialogFragment.this.lambda$onViewCreated$0(view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        dismiss();
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }

    public void onPendingMembersChanged(List<Recipient> list) {
        if (list.size() == 1 && list.get(0).isSelf()) {
            this.pendingContainer.setVisibility(0);
            this.pendingTitle.setText(R.string.GroupsV1MigrationLearnMore_you_will_need_to_accept_an_invite_to_join_this_group_again);
        } else if (list.size() > 0) {
            this.pendingContainer.setVisibility(0);
            this.pendingTitle.setText(getResources().getQuantityText(R.plurals.GroupsV1MigrationLearnMore_these_members_will_need_to_accept_an_invite, list.size()));
            this.pendingList.setDisplayOnlyMembers(list);
        } else {
            this.pendingContainer.setVisibility(8);
        }
    }

    public void onDroppedMembersChanged(List<Recipient> list) {
        if (list.size() > 0) {
            this.droppedContainer.setVisibility(0);
            this.droppedTitle.setText(getResources().getQuantityText(R.plurals.GroupsV1MigrationLearnMore_these_members_were_removed_from_the_group, list.size()));
            this.droppedList.setDisplayOnlyMembers(list);
            return;
        }
        this.droppedContainer.setVisibility(8);
    }
}
