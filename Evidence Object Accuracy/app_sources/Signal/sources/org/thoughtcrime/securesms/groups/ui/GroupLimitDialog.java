package org.thoughtcrime.securesms.groups.ui;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes4.dex */
public final class GroupLimitDialog {
    public static void showHardLimitMessage(Context context) {
        new AlertDialog.Builder(context).setTitle(R.string.ContactSelectionListFragment_maximum_group_size_reached).setMessage(context.getString(R.string.ContactSelectionListFragment_signal_groups_can_have_a_maximum_of_d_members, Integer.valueOf(FeatureFlags.groupLimits().getHardLimit()))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    public static void showRecommendedLimitMessage(Context context) {
        new AlertDialog.Builder(context).setTitle(R.string.ContactSelectionListFragment_recommended_member_limit_reached).setMessage(context.getString(R.string.ContactSelectionListFragment_signal_groups_perform_best_with_d_members_or_fewer, Integer.valueOf(FeatureFlags.groupLimits().getRecommendedLimit()))).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }
}
