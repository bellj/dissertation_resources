package org.thoughtcrime.securesms.groups.ui;

import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;
import org.thoughtcrime.securesms.groups.ui.PopupMenuView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$OwnInvitePendingMemberViewHolder$$ExternalSyntheticLambda0 implements PopupMenuView.ItemClick {
    public final /* synthetic */ GroupMemberListAdapter.OwnInvitePendingMemberViewHolder f$0;
    public final /* synthetic */ GroupMemberEntry.PendingMember f$1;

    public /* synthetic */ GroupMemberListAdapter$OwnInvitePendingMemberViewHolder$$ExternalSyntheticLambda0(GroupMemberListAdapter.OwnInvitePendingMemberViewHolder ownInvitePendingMemberViewHolder, GroupMemberEntry.PendingMember pendingMember) {
        this.f$0 = ownInvitePendingMemberViewHolder;
        this.f$1 = pendingMember;
    }

    @Override // org.thoughtcrime.securesms.groups.ui.PopupMenuView.ItemClick
    public final boolean onItemClick(int i) {
        return this.f$0.lambda$bind$0(this.f$1, i);
    }
}
