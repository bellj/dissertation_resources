package org.thoughtcrime.securesms.groups;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.GroupUtil;

/* loaded from: classes4.dex */
public final class GroupsV1MigrationUtil {
    private static final String TAG = Log.tag(GroupsV1MigrationUtil.class);

    /* loaded from: classes4.dex */
    public static final class InvalidMigrationStateException extends Exception {
    }

    private GroupsV1MigrationUtil() {
    }

    public static void migrate(Context context, RecipientId recipientId, boolean z) throws IOException, RetryLaterException, GroupChangeBusyException, InvalidMigrationStateException {
        List<Recipient> list;
        GroupAlreadyExistsException e;
        Recipient resolved = Recipient.resolved(recipientId);
        Long threadIdFor = SignalDatabase.threads().getThreadIdFor(recipientId);
        GroupDatabase groups = SignalDatabase.groups();
        if (threadIdFor == null) {
            Log.w(TAG, "No thread found!");
            throw new InvalidMigrationStateException();
        } else if (!resolved.isPushV1Group()) {
            Log.w(TAG, "Not a V1 group!");
            throw new InvalidMigrationStateException();
        } else if (resolved.getParticipantIds().size() <= FeatureFlags.groupLimits().getHardLimit()) {
            GroupId.V1 requireV1 = resolved.requireGroupId().requireV1();
            GroupId.V2 deriveV2MigrationGroupId = requireV1.deriveV2MigrationGroupId();
            GroupMasterKey deriveV2MigrationMasterKey = requireV1.deriveV2MigrationMasterKey();
            boolean z2 = false;
            if (groups.groupExists(deriveV2MigrationGroupId)) {
                Log.w(TAG, "We already have a V2 group for this V1 group! Must have been added before we were migration-capable.");
                throw new InvalidMigrationStateException();
            } else if (resolved.isActiveGroup()) {
                int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$V2GroupServerStatus[GroupManager.v2GroupStatus(context, SignalStore.account().requireAci(), deriveV2MigrationMasterKey).ordinal()];
                if (i == 1) {
                    String str = TAG;
                    Log.i(str, "Group does not exist on the service.");
                    if (!resolved.isProfileSharing()) {
                        Log.w(str, "Profile sharing is disabled! Can't migrate.");
                        throw new InvalidMigrationStateException();
                    } else if (z || !SignalStore.internalValues().disableGv1AutoMigrateInitiation()) {
                        List<Recipient> eligibleForSending = RecipientUtil.getEligibleForSending(Recipient.resolvedList(resolved.getParticipantIds()));
                        if (RecipientUtil.ensureUuidsAreAvailable(context, eligibleForSending)) {
                            Log.i(str, "Newly-discovered UUIDs. Getting fresh recipients.");
                            eligibleForSending = Stream.of(eligibleForSending).map(new Function() { // from class: org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil$$ExternalSyntheticLambda2
                                @Override // com.annimon.stream.function.Function
                                public final Object apply(Object obj) {
                                    return ((Recipient) obj).fresh();
                                }
                            }).toList();
                        }
                        if (z) {
                            list = getMigratableManualMigrationMembers(eligibleForSending);
                        } else {
                            list = getMigratableAutoMigrationMembers(eligibleForSending);
                        }
                        if (!z && !resolved.hasName()) {
                            Log.w(str, "Group has no name. Skipping auto-migration.");
                            throw new InvalidMigrationStateException();
                        } else if (z || list.size() == eligibleForSending.size()) {
                            Log.i(str, "Attempting to create group.");
                            try {
                                try {
                                    GroupManager.migrateGroupToServer(context, requireV1, list);
                                } catch (GroupAlreadyExistsException e2) {
                                    e = e2;
                                }
                                try {
                                    Log.i(str, "Successfully created!");
                                    z2 = true;
                                } catch (GroupAlreadyExistsException e3) {
                                    e = e3;
                                    z2 = true;
                                    Log.w(TAG, "Someone else created the group while we were trying to do the same! It exists now. Continuing on.", e);
                                    String str2 = TAG;
                                    Log.i(str2, "Migrating local group " + requireV1 + " to " + deriveV2MigrationGroupId);
                                    DecryptedGroup performLocalMigration = performLocalMigration(context, requireV1, threadIdFor.longValue(), resolved);
                                    if (z2) {
                                        return;
                                    }
                                    return;
                                }
                            } catch (GroupChangeFailedException e4) {
                                Log.w(TAG, "Failed to migrate group. Retrying.", e4);
                                throw new RetryLaterException();
                            } catch (MembershipNotSuitableForV2Exception e5) {
                                Log.w(TAG, "Failed to migrate job due to the membership not yet being suitable for GV2. Aborting.", e5);
                                return;
                            }
                        } else {
                            Log.w(str, "Not allowed to invite or leave registered users behind in an auto-migration! Skipping.");
                            throw new InvalidMigrationStateException();
                        }
                    } else {
                        Log.w(str, "Auto migration initiation has been disabled! Skipping.");
                        throw new InvalidMigrationStateException();
                    }
                } else if (i == 2) {
                    Log.w(TAG, "The migrated group already exists, but we are not a member. Doing a local leave.");
                    handleLeftBehind(context, requireV1, resolved, threadIdFor.longValue());
                    return;
                } else if (i == 3) {
                    Log.w(TAG, "The migrated group already exists, and we're in it. Continuing on.");
                } else {
                    throw new AssertionError();
                }
                String str2 = TAG;
                Log.i(str2, "Migrating local group " + requireV1 + " to " + deriveV2MigrationGroupId);
                DecryptedGroup performLocalMigration = performLocalMigration(context, requireV1, threadIdFor.longValue(), resolved);
                if (z2 && performLocalMigration != null && !SignalStore.internalValues().disableGv1AutoMigrateNotification()) {
                    Log.i(str2, "Sending no-op update to notify others.");
                    GroupManager.sendNoopUpdate(context, deriveV2MigrationMasterKey, performLocalMigration);
                }
            } else {
                Log.w(TAG, "Group is inactive! Can't migrate.");
                throw new InvalidMigrationStateException();
            }
        } else {
            String str3 = TAG;
            Log.w(str3, "Too many members! Size: " + resolved.getParticipantIds().size());
            throw new InvalidMigrationStateException();
        }
    }

    /* renamed from: org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$V2GroupServerStatus;

        static {
            int[] iArr = new int[GroupManager.V2GroupServerStatus.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$V2GroupServerStatus = iArr;
            try {
                iArr[GroupManager.V2GroupServerStatus.DOES_NOT_EXIST.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$V2GroupServerStatus[GroupManager.V2GroupServerStatus.NOT_A_MEMBER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$V2GroupServerStatus[GroupManager.V2GroupServerStatus.FULL_OR_PENDING_MEMBER.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public static void performLocalMigration(Context context, GroupId.V1 v1) throws IOException {
        String str = TAG;
        Log.i(str, "Beginning local migration! V1 ID: " + v1, new Throwable());
        try {
            Closeable acquireGroupProcessingLock = GroupsV2ProcessingLock.acquireGroupProcessingLock();
            if (SignalDatabase.groups().groupExists(v1.deriveV2MigrationGroupId())) {
                Log.w(str, "Group was already migrated! Could have been waiting for the lock.", new Throwable());
                if (acquireGroupProcessingLock != null) {
                    acquireGroupProcessingLock.close();
                    return;
                }
                return;
            }
            Recipient externalGroupExact = Recipient.externalGroupExact(v1);
            long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(externalGroupExact);
            performLocalMigration(context, v1, orCreateThreadIdFor, externalGroupExact);
            Log.i(str, "Migration complete! (" + v1 + ", " + orCreateThreadIdFor + ", " + externalGroupExact.getId() + ")", new Throwable());
            if (acquireGroupProcessingLock != null) {
                acquireGroupProcessingLock.close();
            }
        } catch (GroupChangeBusyException e) {
            throw new IOException(e);
        }
    }

    private static DecryptedGroup performLocalMigration(Context context, GroupId.V1 v1, long j, Recipient recipient) throws IOException, GroupChangeBusyException {
        Closeable acquireGroupProcessingLock;
        try {
            String str = TAG;
            Log.i(str, "performLocalMigration(" + v1 + ", " + j + ", " + recipient.getId());
            acquireGroupProcessingLock = GroupsV2ProcessingLock.acquireGroupProcessingLock();
            try {
                try {
                    DecryptedGroup addedGroupVersion = GroupManager.addedGroupVersion(SignalStore.account().requireAci(), context, v1.deriveV2MigrationMasterKey());
                    Log.i(str, "[Local] Migrating group over to the version we were added to: V" + addedGroupVersion.getRevision());
                    SignalDatabase.groups().migrateToV2(j, v1, addedGroupVersion);
                    Log.i(str, "[Local] Applying all changes since V" + addedGroupVersion.getRevision());
                    try {
                        GroupManager.updateGroupFromServer(context, v1.deriveV2MigrationMasterKey(), Integer.MAX_VALUE, System.currentTimeMillis(), null);
                    } catch (GroupChangeBusyException | GroupNotAMemberException e) {
                        Log.w(TAG, e);
                    }
                    if (acquireGroupProcessingLock != null) {
                        acquireGroupProcessingLock.close();
                    }
                    return addedGroupVersion;
                } catch (GroupDoesNotExistException unused) {
                    throw new IOException("[Local] The group should exist already!");
                }
            } catch (GroupNotAMemberException unused2) {
                Log.w(TAG, "[Local] We are not in the group. Doing a local leave.");
                handleLeftBehind(context, v1, recipient, j);
                if (acquireGroupProcessingLock != null) {
                    acquireGroupProcessingLock.close();
                }
                return null;
            }
        } catch (Throwable th) {
            if (acquireGroupProcessingLock != null) {
                try {
                    acquireGroupProcessingLock.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    private static void handleLeftBehind(Context context, GroupId.V1 v1, Recipient recipient, long j) {
        try {
            SignalDatabase.mms().markAsSent(SignalDatabase.mms().insertMessageOutbox(GroupUtil.createGroupV1LeaveMessage(v1, recipient), j, false, null), true);
        } catch (MmsException e) {
            Log.w(TAG, "Failed to insert group leave message!", e);
        }
        SignalDatabase.groups().setActive(v1, false);
        SignalDatabase.groups().remove(v1, Recipient.self().getId());
    }

    private static List<Recipient> getMigratableAutoMigrationMembers(List<Recipient> list) {
        return Stream.of(getMigratableManualMigrationMembers(list)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupsV1MigrationUtil.lambda$getMigratableAutoMigrationMembers$0((Recipient) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$getMigratableAutoMigrationMembers$0(Recipient recipient) {
        return recipient.getProfileKey() != null;
    }

    private static List<Recipient> getMigratableManualMigrationMembers(List<Recipient> list) {
        return Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupsV1MigrationUtil.lambda$getMigratableManualMigrationMembers$1((Recipient) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$getMigratableManualMigrationMembers$1(Recipient recipient) {
        return recipient.getGroupsV1MigrationCapability() == Recipient.Capability.SUPPORTED;
    }

    public static boolean isAutoMigratable(Recipient recipient) {
        return recipient.hasServiceId() && recipient.getGroupsV1MigrationCapability() == Recipient.Capability.SUPPORTED && recipient.getRegistered() == RecipientDatabase.RegisteredState.REGISTERED && recipient.getProfileKey() != null;
    }
}
