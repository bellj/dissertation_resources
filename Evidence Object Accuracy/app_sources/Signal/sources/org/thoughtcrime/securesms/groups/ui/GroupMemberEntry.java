package org.thoughtcrime.securesms.groups.ui;

import androidx.lifecycle.LiveData;
import java.util.Collection;
import java.util.Objects;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;

/* loaded from: classes4.dex */
public abstract class GroupMemberEntry {
    private final DefaultValueLiveData<Boolean> busy;

    public abstract boolean equals(Object obj);

    public abstract int hashCode();

    /* access modifiers changed from: package-private */
    public abstract boolean sameId(GroupMemberEntry groupMemberEntry);

    private GroupMemberEntry() {
        this.busy = new DefaultValueLiveData<>(Boolean.FALSE);
    }

    public LiveData<Boolean> getBusy() {
        return this.busy;
    }

    public void setBusy(boolean z) {
        this.busy.postValue(Boolean.valueOf(z));
    }

    /* loaded from: classes4.dex */
    public static final class NewGroupCandidate extends GroupMemberEntry {
        private final Recipient member;

        public NewGroupCandidate(Recipient recipient) {
            super();
            this.member = recipient;
        }

        public Recipient getMember() {
            return this.member;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean sameId(GroupMemberEntry groupMemberEntry) {
            if (NewGroupCandidate.class != groupMemberEntry.getClass()) {
                return false;
            }
            return this.member.getId().equals(((NewGroupCandidate) groupMemberEntry).member.getId());
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean equals(Object obj) {
            if (!(obj instanceof NewGroupCandidate)) {
                return false;
            }
            return ((NewGroupCandidate) obj).member.equals(this.member);
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public int hashCode() {
            return this.member.hashCode();
        }
    }

    /* loaded from: classes4.dex */
    public static final class FullMember extends GroupMemberEntry {
        private final boolean isAdmin;
        private final Recipient member;

        public FullMember(Recipient recipient, boolean z) {
            super();
            this.member = recipient;
            this.isAdmin = z;
        }

        public Recipient getMember() {
            return this.member;
        }

        public boolean isAdmin() {
            return this.isAdmin;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean sameId(GroupMemberEntry groupMemberEntry) {
            if (FullMember.class != groupMemberEntry.getClass()) {
                return false;
            }
            return this.member.getId().equals(((FullMember) groupMemberEntry).member.getId());
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean equals(Object obj) {
            if (!(obj instanceof FullMember)) {
                return false;
            }
            FullMember fullMember = (FullMember) obj;
            if (!fullMember.member.equals(this.member) || fullMember.isAdmin != this.isAdmin) {
                return false;
            }
            return true;
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public int hashCode() {
            return (this.member.hashCode() * 31) + (this.isAdmin ? 1 : 0);
        }
    }

    /* loaded from: classes4.dex */
    public static final class PendingMember extends GroupMemberEntry {
        private final boolean cancellable;
        private final Recipient invitee;
        private final UuidCiphertext inviteeCipherText;

        public PendingMember(Recipient recipient, UuidCiphertext uuidCiphertext, boolean z) {
            super();
            this.invitee = recipient;
            this.inviteeCipherText = uuidCiphertext;
            this.cancellable = z;
            if (z && uuidCiphertext == null) {
                throw new IllegalArgumentException("inviteeCipherText must be supplied to enable cancellation");
            }
        }

        public PendingMember(Recipient recipient) {
            this(recipient, null, false);
        }

        public Recipient getInvitee() {
            return this.invitee;
        }

        public UuidCiphertext getInviteeCipherText() {
            if (this.cancellable) {
                return this.inviteeCipherText;
            }
            throw new UnsupportedOperationException();
        }

        public boolean isCancellable() {
            return this.cancellable;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean sameId(GroupMemberEntry groupMemberEntry) {
            if (PendingMember.class != groupMemberEntry.getClass()) {
                return false;
            }
            return this.invitee.getId().equals(((PendingMember) groupMemberEntry).invitee.getId());
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean equals(Object obj) {
            if (!(obj instanceof PendingMember)) {
                return false;
            }
            PendingMember pendingMember = (PendingMember) obj;
            if (!pendingMember.invitee.equals(this.invitee) || !Objects.equals(pendingMember.inviteeCipherText, this.inviteeCipherText) || pendingMember.cancellable != this.cancellable) {
                return false;
            }
            return true;
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public int hashCode() {
            return (((this.invitee.hashCode() * 31) + Objects.hashCode(this.inviteeCipherText)) * 31) + (this.cancellable ? 1 : 0);
        }
    }

    /* loaded from: classes4.dex */
    public static final class UnknownPendingMemberCount extends GroupMemberEntry {
        private final boolean cancellable;
        private final Collection<UuidCiphertext> ciphertexts;
        private final Recipient inviter;

        public UnknownPendingMemberCount(Recipient recipient, Collection<UuidCiphertext> collection, boolean z) {
            super();
            this.inviter = recipient;
            this.ciphertexts = collection;
            this.cancellable = z;
        }

        public Recipient getInviter() {
            return this.inviter;
        }

        public int getInviteCount() {
            return this.ciphertexts.size();
        }

        public Collection<UuidCiphertext> getCiphertexts() {
            return this.ciphertexts;
        }

        public boolean isCancellable() {
            return this.cancellable;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean sameId(GroupMemberEntry groupMemberEntry) {
            if (UnknownPendingMemberCount.class != groupMemberEntry.getClass()) {
                return false;
            }
            return this.inviter.getId().equals(((UnknownPendingMemberCount) groupMemberEntry).inviter.getId());
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean equals(Object obj) {
            if (!(obj instanceof UnknownPendingMemberCount)) {
                return false;
            }
            UnknownPendingMemberCount unknownPendingMemberCount = (UnknownPendingMemberCount) obj;
            if (!unknownPendingMemberCount.inviter.equals(this.inviter) || !unknownPendingMemberCount.ciphertexts.equals(this.ciphertexts) || unknownPendingMemberCount.cancellable != this.cancellable) {
                return false;
            }
            return true;
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public int hashCode() {
            return (((this.inviter.hashCode() * 31) + this.ciphertexts.hashCode()) * 31) + (this.cancellable ? 1 : 0);
        }
    }

    /* loaded from: classes4.dex */
    public static final class RequestingMember extends GroupMemberEntry {
        private final boolean approvableDeniable;
        private final Recipient requester;

        public RequestingMember(Recipient recipient, boolean z) {
            super();
            this.requester = recipient;
            this.approvableDeniable = z;
        }

        public Recipient getRequester() {
            return this.requester;
        }

        public boolean isApprovableDeniable() {
            return this.approvableDeniable;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean sameId(GroupMemberEntry groupMemberEntry) {
            if (RequestingMember.class != groupMemberEntry.getClass()) {
                return false;
            }
            return this.requester.getId().equals(((RequestingMember) groupMemberEntry).requester.getId());
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public boolean equals(Object obj) {
            if (!(obj instanceof RequestingMember)) {
                return false;
            }
            RequestingMember requestingMember = (RequestingMember) obj;
            if (!requestingMember.requester.equals(this.requester) || requestingMember.approvableDeniable != this.approvableDeniable) {
                return false;
            }
            return true;
        }

        @Override // org.thoughtcrime.securesms.groups.ui.GroupMemberEntry
        public int hashCode() {
            return (this.requester.hashCode() * 31) + (this.approvableDeniable ? 1 : 0);
        }
    }
}
