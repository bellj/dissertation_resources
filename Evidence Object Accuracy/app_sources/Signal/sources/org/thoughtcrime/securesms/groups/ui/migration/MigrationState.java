package org.thoughtcrime.securesms.groups.ui.migration;

import java.util.List;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class MigrationState {
    private final List<Recipient> ineligible;
    private final List<Recipient> needsInvite;

    public MigrationState(List<Recipient> list, List<Recipient> list2) {
        this.needsInvite = list;
        this.ineligible = list2;
    }

    public List<Recipient> getNeedsInvite() {
        return this.needsInvite;
    }

    public List<Recipient> getIneligible() {
        return this.ineligible;
    }
}
