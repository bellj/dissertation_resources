package org.thoughtcrime.securesms.groups;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.signal.libsignal.zkgroup.auth.AuthCredentialWithPniResponse;
import org.thoughtcrime.securesms.groups.GroupsV2Authorization;

/* loaded from: classes4.dex */
public final class GroupsV2AuthorizationMemoryValueCache implements GroupsV2Authorization.ValueCache {
    private final GroupsV2Authorization.ValueCache inner;
    private Map<Long, AuthCredentialWithPniResponse> values;

    public GroupsV2AuthorizationMemoryValueCache(GroupsV2Authorization.ValueCache valueCache) {
        this.inner = valueCache;
    }

    @Override // org.thoughtcrime.securesms.groups.GroupsV2Authorization.ValueCache
    public synchronized void clear() {
        this.inner.clear();
        this.values = null;
    }

    @Override // org.thoughtcrime.securesms.groups.GroupsV2Authorization.ValueCache
    public synchronized Map<Long, AuthCredentialWithPniResponse> read() {
        Map<Long, AuthCredentialWithPniResponse> map;
        map = this.values;
        if (map == null) {
            map = this.inner.read();
            this.values = map;
        }
        return map;
    }

    @Override // org.thoughtcrime.securesms.groups.GroupsV2Authorization.ValueCache
    public synchronized void write(Map<Long, AuthCredentialWithPniResponse> map) {
        this.inner.write(map);
        this.values = Collections.unmodifiableMap(new HashMap(map));
    }
}
