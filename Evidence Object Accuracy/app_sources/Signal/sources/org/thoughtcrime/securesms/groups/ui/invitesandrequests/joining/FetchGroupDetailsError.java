package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

/* loaded from: classes4.dex */
public enum FetchGroupDetailsError {
    GroupLinkNotActive,
    BannedFromGroup,
    NetworkError
}
