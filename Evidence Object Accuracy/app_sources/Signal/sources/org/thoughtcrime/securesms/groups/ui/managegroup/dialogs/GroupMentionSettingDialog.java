package org.thoughtcrime.securesms.groups.ui.managegroup.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckedTextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.util.Consumer;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupMentionSettingDialog;

/* loaded from: classes4.dex */
public final class GroupMentionSettingDialog {
    public static void show(Context context, RecipientDatabase.MentionSetting mentionSetting, Consumer<RecipientDatabase.MentionSetting> consumer) {
        SelectionCallback selectionCallback = new SelectionCallback(mentionSetting, consumer);
        new AlertDialog.Builder(context).setTitle(R.string.GroupMentionSettingDialog_notify_me_for_mentions).setView(getView(context, mentionSetting, selectionCallback)).setPositiveButton(17039370, selectionCallback).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    private static View getView(Context context, RecipientDatabase.MentionSetting mentionSetting, SelectionCallback selectionCallback) {
        View inflate = LayoutInflater.from(context).inflate(R.layout.group_mention_setting_dialog, (ViewGroup) null, false);
        CheckedTextView checkedTextView = (CheckedTextView) inflate.findViewById(R.id.group_mention_setting_always_notify);
        CheckedTextView checkedTextView2 = (CheckedTextView) inflate.findViewById(R.id.group_mention_setting_dont_notify);
        GroupMentionSettingDialog$$ExternalSyntheticLambda0 groupMentionSettingDialog$$ExternalSyntheticLambda0 = new View.OnClickListener(checkedTextView, checkedTextView2, selectionCallback) { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupMentionSettingDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ CheckedTextView f$0;
            public final /* synthetic */ CheckedTextView f$1;
            public final /* synthetic */ GroupMentionSettingDialog.SelectionCallback f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                GroupMentionSettingDialog.m1847$r8$lambda$c6zXALTvuJcbhJctSHwOnp3l6c(this.f$0, this.f$1, this.f$2, view);
            }
        };
        checkedTextView.setOnClickListener(groupMentionSettingDialog$$ExternalSyntheticLambda0);
        checkedTextView2.setOnClickListener(groupMentionSettingDialog$$ExternalSyntheticLambda0);
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting[mentionSetting.ordinal()];
        if (i == 1) {
            groupMentionSettingDialog$$ExternalSyntheticLambda0.onClick(checkedTextView);
        } else if (i == 2) {
            groupMentionSettingDialog$$ExternalSyntheticLambda0.onClick(checkedTextView2);
        }
        return inflate;
    }

    public static /* synthetic */ void lambda$getView$0(CheckedTextView checkedTextView, CheckedTextView checkedTextView2, SelectionCallback selectionCallback, View view) {
        boolean z = true;
        checkedTextView.setChecked(checkedTextView == view);
        if (checkedTextView2 != view) {
            z = false;
        }
        checkedTextView2.setChecked(z);
        if (checkedTextView.isChecked()) {
            selectionCallback.selection = RecipientDatabase.MentionSetting.ALWAYS_NOTIFY;
        } else if (checkedTextView2.isChecked()) {
            selectionCallback.selection = RecipientDatabase.MentionSetting.DO_NOT_NOTIFY;
        }
    }

    /* renamed from: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupMentionSettingDialog$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting;

        static {
            int[] iArr = new int[RecipientDatabase.MentionSetting.values().length];
            $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting = iArr;
            try {
                iArr[RecipientDatabase.MentionSetting.ALWAYS_NOTIFY.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$database$RecipientDatabase$MentionSetting[RecipientDatabase.MentionSetting.DO_NOT_NOTIFY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class SelectionCallback implements DialogInterface.OnClickListener {
        private final Consumer<RecipientDatabase.MentionSetting> callback;
        private final RecipientDatabase.MentionSetting previousMentionSetting;
        private RecipientDatabase.MentionSetting selection;

        public SelectionCallback(RecipientDatabase.MentionSetting mentionSetting, Consumer<RecipientDatabase.MentionSetting> consumer) {
            this.previousMentionSetting = mentionSetting;
            this.selection = mentionSetting;
            this.callback = consumer;
        }

        @Override // android.content.DialogInterface.OnClickListener
        public void onClick(DialogInterface dialogInterface, int i) {
            RecipientDatabase.MentionSetting mentionSetting;
            Consumer<RecipientDatabase.MentionSetting> consumer = this.callback;
            if (consumer != null && (mentionSetting = this.selection) != this.previousMentionSetting) {
                consumer.accept(mentionSetting);
            }
        }
    }
}
