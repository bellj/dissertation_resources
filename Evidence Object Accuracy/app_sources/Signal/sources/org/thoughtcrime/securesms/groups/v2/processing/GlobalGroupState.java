package org.thoughtcrime.securesms.groups.v2.processing;

import java.util.Collection;
import java.util.List;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.whispersystems.signalservice.api.groupsv2.GroupHistoryPage;

/* loaded from: classes4.dex */
public final class GlobalGroupState {
    private final DecryptedGroup localState;
    private final GroupHistoryPage.PagingData pagingData;
    private final List<ServerGroupLogEntry> serverHistory;

    public GlobalGroupState(DecryptedGroup decryptedGroup, List<ServerGroupLogEntry> list, GroupHistoryPage.PagingData pagingData) {
        this.localState = decryptedGroup;
        this.serverHistory = list;
        this.pagingData = pagingData;
    }

    public GlobalGroupState(DecryptedGroup decryptedGroup, List<ServerGroupLogEntry> list) {
        this(decryptedGroup, list, GroupHistoryPage.PagingData.NONE);
    }

    public DecryptedGroup getLocalState() {
        return this.localState;
    }

    public Collection<ServerGroupLogEntry> getServerHistory() {
        return this.serverHistory;
    }

    public int getEarliestRevisionNumber() {
        DecryptedGroup decryptedGroup = this.localState;
        if (decryptedGroup != null) {
            return decryptedGroup.getRevision();
        }
        if (!this.serverHistory.isEmpty()) {
            return this.serverHistory.get(0).getRevision();
        }
        throw new AssertionError();
    }

    public int getLatestRevisionNumber() {
        if (this.serverHistory.isEmpty()) {
            DecryptedGroup decryptedGroup = this.localState;
            if (decryptedGroup != null) {
                return decryptedGroup.getRevision();
            }
            throw new AssertionError();
        }
        List<ServerGroupLogEntry> list = this.serverHistory;
        return list.get(list.size() - 1).getRevision();
    }

    public boolean hasMore() {
        return this.pagingData.hasMorePages();
    }

    public int getNextPageRevision() {
        if (this.pagingData.hasMorePages()) {
            return this.pagingData.getNextPageRevision();
        }
        throw new AssertionError("No paging data available");
    }
}
