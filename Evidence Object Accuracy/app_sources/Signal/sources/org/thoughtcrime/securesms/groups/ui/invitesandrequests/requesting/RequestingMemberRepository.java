package org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting;

import android.content.Context;
import java.io.IOException;
import java.util.Collections;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* loaded from: classes4.dex */
public final class RequestingMemberRepository {
    private static final String TAG = Log.tag(RequestingMemberRepository.class);
    private final Context context;
    private final GroupId.V2 groupId;

    public RequestingMemberRepository(Context context, GroupId.V2 v2) {
        this.context = context.getApplicationContext();
        this.groupId = v2;
    }

    public void approveRequest(Recipient recipient, AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(recipient, workerThread) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMemberRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RequestingMemberRepository.this.lambda$approveRequest$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$approveRequest$0(Recipient recipient, AsynchronousCallback.WorkerThread workerThread) {
        try {
            GroupManager.approveRequests(this.context, this.groupId, Collections.singleton(recipient.getId()));
            workerThread.onComplete(null);
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            workerThread.onError(GroupChangeFailureReason.fromException(e));
        }
    }

    public void denyRequest(Recipient recipient, AsynchronousCallback.WorkerThread<Void, GroupChangeFailureReason> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(recipient, workerThread) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMemberRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RequestingMemberRepository.this.lambda$denyRequest$1(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$denyRequest$1(Recipient recipient, AsynchronousCallback.WorkerThread workerThread) {
        try {
            GroupManager.denyRequests(this.context, this.groupId, Collections.singleton(recipient.getId()));
            workerThread.onComplete(null);
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            workerThread.onError(GroupChangeFailureReason.fromException(e));
        }
    }
}
