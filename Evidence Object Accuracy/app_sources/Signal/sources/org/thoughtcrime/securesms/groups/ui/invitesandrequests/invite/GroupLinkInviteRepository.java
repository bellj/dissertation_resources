package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite;

import android.content.Context;
import java.io.IOException;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeFailedException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupInsufficientRightsException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupNotAMemberException;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.util.AsynchronousCallback;

/* loaded from: classes4.dex */
public final class GroupLinkInviteRepository {
    private final Context context;
    private final GroupId.V2 groupId;

    public GroupLinkInviteRepository(Context context, GroupId.V2 v2) {
        this.context = context;
        this.groupId = v2;
    }

    public void enableGroupInviteLink(boolean z, AsynchronousCallback.WorkerThread<GroupInviteLinkUrl, EnableInviteLinkError> workerThread) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(z, workerThread) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ AsynchronousCallback.WorkerThread f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupLinkInviteRepository.this.lambda$enableGroupInviteLink$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$enableGroupInviteLink$0(boolean z, AsynchronousCallback.WorkerThread workerThread) {
        GroupManager.GroupLinkState groupLinkState;
        try {
            Context context = this.context;
            GroupId.V2 v2 = this.groupId;
            if (z) {
                groupLinkState = GroupManager.GroupLinkState.ENABLED_WITH_APPROVAL;
            } else {
                groupLinkState = GroupManager.GroupLinkState.ENABLED;
            }
            GroupInviteLinkUrl groupLinkEnabledState = GroupManager.setGroupLinkEnabledState(context, v2, groupLinkState);
            if (groupLinkEnabledState != null) {
                workerThread.onComplete(groupLinkEnabledState);
                return;
            }
            throw new AssertionError();
        } catch (IOException unused) {
            workerThread.onError(EnableInviteLinkError.NETWORK_ERROR);
        } catch (GroupChangeBusyException unused2) {
            workerThread.onError(EnableInviteLinkError.BUSY);
        } catch (GroupChangeFailedException unused3) {
            workerThread.onError(EnableInviteLinkError.FAILED);
        } catch (GroupInsufficientRightsException unused4) {
            workerThread.onError(EnableInviteLinkError.INSUFFICIENT_RIGHTS);
        } catch (GroupNotAMemberException unused5) {
            workerThread.onError(EnableInviteLinkError.NOT_IN_GROUP);
        }
    }
}
