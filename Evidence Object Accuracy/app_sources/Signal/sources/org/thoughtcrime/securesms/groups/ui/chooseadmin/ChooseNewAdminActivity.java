package org.thoughtcrime.securesms.groups.ui.chooseadmin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.thoughtcrime.securesms.GroupMembersDialog$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.BadGroupIdException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.GroupChangeResult;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.RecipientSelectionChangeListener;
import org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public final class ChooseNewAdminActivity extends PassphraseRequiredActivity {
    private static final String EXTRA_GROUP_ID;
    private CircularProgressMaterialButton done;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private GroupId.V2 groupId;
    private GroupMemberListView groupList;
    private ChooseNewAdminViewModel viewModel;

    public static Intent createIntent(Context context, GroupId.V2 v2) {
        Intent intent = new Intent(context, ChooseNewAdminActivity.class);
        intent.putExtra("group_id", v2.toString());
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        setContentView(R.layout.choose_new_admin_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        try {
            String stringExtra = getIntent().getStringExtra("group_id");
            Objects.requireNonNull(stringExtra);
            this.groupId = GroupId.parse(stringExtra).requireV2();
            this.groupList = (GroupMemberListView) findViewById(R.id.choose_new_admin_group_list);
            this.done = (CircularProgressMaterialButton) findViewById(R.id.choose_new_admin_done);
            initializeViewModel();
            this.groupList.initializeAdapter(this);
            this.groupList.setRecipientSelectionChangeListener(new RecipientSelectionChangeListener() { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminActivity$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.groups.ui.RecipientSelectionChangeListener
                public final void onSelectionChanged(Set set) {
                    ChooseNewAdminActivity.this.lambda$onCreate$0(set);
                }
            });
            this.done.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminActivity$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ChooseNewAdminActivity.this.lambda$onCreate$1(view);
                }
            });
        } catch (BadGroupIdException e) {
            throw new AssertionError(e);
        }
    }

    public /* synthetic */ void lambda$onCreate$0(Set set) {
        this.viewModel.setSelection((Set) Stream.of(set).select(GroupMemberEntry.FullMember.class).collect(Collectors.toSet()));
    }

    public /* synthetic */ void lambda$onCreate$1(View view) {
        this.done.setSpinning();
        this.viewModel.updateAdminsAndLeave(new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminActivity$$ExternalSyntheticLambda3
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ChooseNewAdminActivity.this.handleUpdateAndLeaveResult((GroupChangeResult) obj);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    private void initializeViewModel() {
        ChooseNewAdminViewModel chooseNewAdminViewModel = (ChooseNewAdminViewModel) ViewModelProviders.of(this, new ChooseNewAdminViewModel.Factory(this.groupId)).get(ChooseNewAdminViewModel.class);
        this.viewModel = chooseNewAdminViewModel;
        LiveData<List<GroupMemberEntry.FullMember>> nonAdminFullMembers = chooseNewAdminViewModel.getNonAdminFullMembers();
        GroupMemberListView groupMemberListView = this.groupList;
        Objects.requireNonNull(groupMemberListView);
        nonAdminFullMembers.observe(this, new GroupMembersDialog$$ExternalSyntheticLambda0(groupMemberListView));
        this.viewModel.getSelection().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminActivity$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChooseNewAdminActivity.this.lambda$initializeViewModel$2((Set) obj);
            }
        });
    }

    public /* synthetic */ void lambda$initializeViewModel$2(Set set) {
        this.done.setVisibility(set.isEmpty() ? 8 : 0);
    }

    public void handleUpdateAndLeaveResult(GroupChangeResult groupChangeResult) {
        if (groupChangeResult.isSuccess()) {
            Toast.makeText(this, getString(R.string.ChooseNewAdminActivity_you_left, new Object[]{Recipient.externalGroupExact(this.groupId).getDisplayName(this)}), 1).show();
            startActivity(MainActivity.clearTop(this));
            finish();
            return;
        }
        this.done.cancelSpinning();
        Toast.makeText(this, GroupErrors.getUserDisplayMessage(groupChangeResult.getFailureReason()), 1).show();
    }
}
