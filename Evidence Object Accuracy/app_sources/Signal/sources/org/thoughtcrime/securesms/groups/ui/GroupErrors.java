package org.thoughtcrime.securesms.groups.ui;

import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class GroupErrors {
    private GroupErrors() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.groups.ui.GroupErrors$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason;

        static {
            int[] iArr = new int[GroupChangeFailureReason.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason = iArr;
            try {
                iArr[GroupChangeFailureReason.NO_RIGHTS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason[GroupChangeFailureReason.NOT_GV2_CAPABLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason[GroupChangeFailureReason.NOT_ANNOUNCEMENT_CAPABLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason[GroupChangeFailureReason.NOT_A_MEMBER.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason[GroupChangeFailureReason.BUSY.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason[GroupChangeFailureReason.NETWORK.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    public static int getUserDisplayMessage(GroupChangeFailureReason groupChangeFailureReason) {
        if (groupChangeFailureReason == null) {
            return R.string.ManageGroupActivity_failed_to_update_the_group;
        }
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$ui$GroupChangeFailureReason[groupChangeFailureReason.ordinal()]) {
            case 1:
                return R.string.ManageGroupActivity_you_dont_have_the_rights_to_do_this;
            case 2:
                return R.string.ManageGroupActivity_not_capable;
            case 3:
                return R.string.ManageGroupActivity_not_announcement_capable;
            case 4:
                return R.string.ManageGroupActivity_youre_not_a_member_of_the_group;
            case 5:
                return R.string.ManageGroupActivity_failed_to_update_the_group_please_retry_later;
            case 6:
                return R.string.ManageGroupActivity_failed_to_update_the_group_due_to_a_network_error_please_retry_later;
            default:
                return R.string.ManageGroupActivity_failed_to_update_the_group;
        }
    }
}
