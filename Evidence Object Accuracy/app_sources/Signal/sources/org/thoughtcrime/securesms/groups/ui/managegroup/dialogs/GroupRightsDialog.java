package org.thoughtcrime.securesms.groups.ui.managegroup.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupAccessControl;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupRightsDialog;

/* loaded from: classes4.dex */
public final class GroupRightsDialog {
    private final AlertDialog.Builder builder;
    private GroupAccessControl rights;

    /* loaded from: classes4.dex */
    public interface OnChange {
        void changed(GroupAccessControl groupAccessControl, GroupAccessControl groupAccessControl2);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void lambda$new$1(DialogInterface dialogInterface, int i) {
    }

    public GroupRightsDialog(Context context, Type type, GroupAccessControl groupAccessControl, OnChange onChange) {
        this.rights = groupAccessControl;
        this.builder = new AlertDialog.Builder(context).setTitle(type.message).setSingleChoiceItems(type.choices, groupAccessControl.ordinal(), new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupRightsDialog$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                GroupRightsDialog.m1850$r8$lambda$rEgTKFc5W_YsIdx3e503x6YZCA(GroupRightsDialog.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupRightsDialog$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                GroupRightsDialog.m1848$r8$lambda$CbkJSZA4vREFoZB6CeNEb6CXQE(dialogInterface, i);
            }
        }).setPositiveButton(17039370, new DialogInterface.OnClickListener(groupAccessControl, onChange) { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupRightsDialog$$ExternalSyntheticLambda2
            public final /* synthetic */ GroupAccessControl f$1;
            public final /* synthetic */ GroupRightsDialog.OnChange f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                GroupRightsDialog.m1849$r8$lambda$pqmZFG5seIqTyCzyBNv9j61XFk(GroupRightsDialog.this, this.f$1, this.f$2, dialogInterface, i);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(DialogInterface dialogInterface, int i) {
        this.rights = GroupAccessControl.values()[i];
    }

    public /* synthetic */ void lambda$new$2(GroupAccessControl groupAccessControl, OnChange onChange, DialogInterface dialogInterface, int i) {
        GroupAccessControl groupAccessControl2 = this.rights;
        if (groupAccessControl2 != groupAccessControl) {
            onChange.changed(groupAccessControl, groupAccessControl2);
        }
    }

    public void show() {
        this.builder.show();
    }

    /* loaded from: classes4.dex */
    public enum Type {
        MEMBERSHIP(R.string.ManageGroupActivity_who_can_add_new_members, R.array.GroupManagement_edit_group_membership_choices),
        ATTRIBUTES(R.string.ManageGroupActivity_who_can_edit_this_groups_info, R.array.GroupManagement_edit_group_info_choices);
        
        private final int choices;
        private final int message;

        Type(int i, int i2) {
            this.message = i;
            this.choices = i2;
        }
    }
}
