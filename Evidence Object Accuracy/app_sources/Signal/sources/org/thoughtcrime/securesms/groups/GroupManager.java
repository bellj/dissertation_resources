package org.thoughtcrime.securesms.groups;

import android.content.Context;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.storageservice.protos.groups.GroupExternalCredential;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManagerV2;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.groups.v2.GroupLinkPassword;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.groupsv2.GroupLinkNotActiveException;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class GroupManager {
    private static final String TAG = Log.tag(GroupManager.class);

    /* loaded from: classes4.dex */
    public enum GroupLinkState {
        DISABLED,
        ENABLED,
        ENABLED_WITH_APPROVAL
    }

    /* loaded from: classes4.dex */
    public enum V2GroupServerStatus {
        DOES_NOT_EXIST,
        NOT_A_MEMBER,
        FULL_OR_PENDING_MEMBER
    }

    public static GroupActionResult createGroup(ServiceId serviceId, Context context, Set<Recipient> set, byte[] bArr, String str, boolean z, int i) throws GroupChangeBusyException, GroupChangeFailedException, IOException {
        boolean z2 = !z && !SignalStore.internalValues().gv2DoNotCreateGv2Groups();
        Set<RecipientId> memberIds = getMemberIds(set);
        if (!z2) {
            return GroupManagerV1.createGroup(context, memberIds, bArr, str, z);
        }
        try {
            GroupManagerV2.GroupCreator create = new GroupManagerV2(context).create();
            GroupActionResult createGroup = create.createGroup(serviceId, memberIds, str, bArr, i);
            create.close();
            return createGroup;
        } catch (MembershipNotSuitableForV2Exception e) {
            Log.w(TAG, "Attempted to make a GV2, but membership was not suitable, falling back to GV1", e);
            return GroupManagerV1.createGroup(context, memberIds, bArr, str, false);
        }
    }

    public static GroupActionResult updateGroupDetails(Context context, GroupId groupId, byte[] bArr, boolean z, String str, boolean z2, String str2, boolean z3) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        if (groupId.isV2()) {
            GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(groupId.requireV2());
            if (!z2) {
                str = null;
            }
            if (!z3) {
                str2 = null;
            }
            try {
                GroupActionResult updateGroupTitleDescriptionAndAvatar = edit.updateGroupTitleDescriptionAndAvatar(str, str2, bArr, z);
                edit.close();
                return updateGroupTitleDescriptionAndAvatar;
            } catch (Throwable th) {
                if (edit != null) {
                    try {
                        edit.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } else if (!groupId.isV1()) {
            return GroupManagerV1.updateGroup(context, groupId.requireMms(), bArr, str);
        } else {
            return GroupManagerV1.updateGroup(context, groupId.requireV1(), getMemberIds(new HashSet(SignalDatabase.groups().getGroupMembers(groupId, GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF))), bArr, str, 0);
        }
    }

    public static void migrateGroupToServer(Context context, GroupId.V1 v1, Collection<Recipient> collection) throws IOException, GroupChangeFailedException, MembershipNotSuitableForV2Exception, GroupAlreadyExistsException {
        new GroupManagerV2(context).migrateGroupOnToServer(v1, collection);
    }

    private static Set<RecipientId> getMemberIds(Collection<Recipient> collection) {
        HashSet hashSet = new HashSet(collection.size());
        for (Recipient recipient : collection) {
            hashSet.add(recipient.getId());
        }
        return hashSet;
    }

    public static void leaveGroup(Context context, GroupId.Push push) throws GroupChangeBusyException, GroupChangeFailedException, IOException {
        if (push.isV2()) {
            try {
                GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(push.requireV2());
                try {
                    edit.leaveGroup();
                    String str = TAG;
                    Log.i(str, "Left group " + push);
                    edit.close();
                } catch (Throwable th) {
                    if (edit != null) {
                        try {
                            edit.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                    }
                    throw th;
                }
            } catch (GroupInsufficientRightsException e) {
                String str2 = TAG;
                Log.w(str2, "Unexpected prevention from leaving " + push + " due to rights", e);
                throw new GroupChangeFailedException(e);
            } catch (GroupNotAMemberException e2) {
                String str3 = TAG;
                Log.w(str3, "Already left group " + push, e2);
            }
        } else if (!GroupManagerV1.leaveGroup(context, push.requireV1())) {
            String str4 = TAG;
            Log.w(str4, "GV1 group leave failed" + push);
            throw new GroupChangeFailedException();
        }
    }

    public static void leaveGroupFromBlockOrMessageRequest(Context context, GroupId.Push push) throws IOException, GroupChangeBusyException, GroupChangeFailedException {
        if (push.isV2()) {
            leaveGroup(context, push.requireV2());
        } else if (!GroupManagerV1.silentLeaveGroup(context, push.requireV1())) {
            throw new GroupChangeFailedException();
        }
    }

    public static void addMemberAdminsAndLeaveGroup(Context context, GroupId.V2 v2, Collection<RecipientId> collection) throws GroupChangeBusyException, GroupChangeFailedException, IOException, GroupInsufficientRightsException, GroupNotAMemberException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.addMemberAdminsAndLeaveGroup(collection);
            String str = TAG;
            Log.i(str, "Left group " + v2);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void ejectAndBanFromGroup(Context context, GroupId.V2 v2, Recipient recipient) throws GroupChangeBusyException, GroupChangeFailedException, GroupInsufficientRightsException, GroupNotAMemberException, IOException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.ejectMember(recipient.requireServiceId(), false, true);
            String str = TAG;
            Log.i(str, "Member removed from group " + v2);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void updateGroupFromServer(Context context, GroupMasterKey groupMasterKey, int i, long j, byte[] bArr) throws GroupChangeBusyException, IOException, GroupNotAMemberException {
        GroupManagerV2.GroupUpdater updater = new GroupManagerV2(context).updater(groupMasterKey);
        try {
            updater.updateLocalToServerRevision(i, j, bArr);
            updater.close();
        } catch (Throwable th) {
            if (updater != null) {
                try {
                    updater.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static V2GroupServerStatus v2GroupStatus(Context context, ServiceId serviceId, GroupMasterKey groupMasterKey) throws IOException {
        try {
            new GroupManagerV2(context).groupServerQuery(serviceId, groupMasterKey);
            return V2GroupServerStatus.FULL_OR_PENDING_MEMBER;
        } catch (GroupDoesNotExistException unused) {
            return V2GroupServerStatus.DOES_NOT_EXIST;
        } catch (GroupNotAMemberException unused2) {
            return V2GroupServerStatus.NOT_A_MEMBER;
        }
    }

    public static DecryptedGroup addedGroupVersion(ServiceId serviceId, Context context, GroupMasterKey groupMasterKey) throws IOException, GroupDoesNotExistException, GroupNotAMemberException {
        return new GroupManagerV2(context).addedGroupVersion(serviceId, groupMasterKey);
    }

    public static void setMemberAdmin(Context context, GroupId.V2 v2, RecipientId recipientId, boolean z) throws GroupChangeBusyException, GroupChangeFailedException, GroupInsufficientRightsException, GroupNotAMemberException, IOException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.setMemberAdmin(recipientId, z);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void updateSelfProfileKeyInGroup(Context context, GroupId.V2 v2) throws IOException, GroupChangeBusyException, GroupInsufficientRightsException, GroupNotAMemberException, GroupChangeFailedException {
        if (!SignalDatabase.groups().groupExists(v2)) {
            String str = TAG;
            Log.i(str, "Group is not available locally " + v2);
            return;
        }
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.updateSelfProfileKeyInGroup();
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void acceptInvite(Context context, GroupId.V2 v2) throws GroupChangeBusyException, GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.acceptInvite();
            SignalDatabase.groups().setActive(v2, true);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void updateGroupTimer(Context context, GroupId.Push push, int i) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        if (push.isV2()) {
            GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(push.requireV2());
            try {
                edit.updateGroupTimer(i);
                edit.close();
            } catch (Throwable th) {
                if (edit != null) {
                    try {
                        edit.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } else {
            GroupManagerV1.updateGroupTimer(context, push.requireV1(), i);
        }
    }

    public static void revokeInvites(Context context, ServiceId serviceId, GroupId.V2 v2, Collection<UuidCiphertext> collection) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.revokeInvites(serviceId, collection, true);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void ban(Context context, GroupId.V2 v2, RecipientId recipientId) throws GroupChangeBusyException, IOException, GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException {
        GroupDatabase.V2GroupProperties requireV2GroupProperties = SignalDatabase.groups().requireGroup(v2).requireV2GroupProperties();
        Recipient resolved = Recipient.resolved(recipientId);
        if (requireV2GroupProperties.getBannedMembers().contains(resolved.requireServiceId().uuid())) {
            String str = TAG;
            Log.i(str, "Attempt to ban already banned recipient: " + recipientId);
            return;
        }
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.ban(resolved.requireServiceId().uuid());
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void unban(Context context, GroupId.V2 v2, RecipientId recipientId) throws GroupChangeBusyException, IOException, GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.unban(Collections.singleton(Recipient.resolved(recipientId).requireServiceId().uuid()));
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void applyMembershipAdditionRightsChange(Context context, GroupId.V2 v2, GroupAccessControl groupAccessControl) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.updateMembershipRights(groupAccessControl);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void applyAttributesRightsChange(Context context, GroupId.V2 v2, GroupAccessControl groupAccessControl) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.updateAttributesRights(groupAccessControl);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void applyAnnouncementGroupChange(Context context, GroupId.V2 v2, boolean z) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.updateAnnouncementGroup(z);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void cycleGroupLinkPassword(Context context, GroupId.V2 v2) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.cycleGroupLinkPassword();
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static GroupInviteLinkUrl setGroupLinkEnabledState(Context context, GroupId.V2 v2, GroupLinkState groupLinkState) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            GroupInviteLinkUrl joinByGroupLinkState = edit.setJoinByGroupLinkState(groupLinkState);
            edit.close();
            return joinByGroupLinkState;
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void approveRequests(Context context, GroupId.V2 v2, Collection<RecipientId> collection) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.approveRequests(collection);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void denyRequests(Context context, GroupId.V2 v2, Collection<RecipientId> collection) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException {
        GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(v2.requireV2());
        try {
            edit.denyRequests(collection);
            edit.close();
        } catch (Throwable th) {
            if (edit != null) {
                try {
                    edit.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static GroupActionResult addMembers(Context context, GroupId.Push push, Collection<RecipientId> collection) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, GroupChangeBusyException, MembershipNotSuitableForV2Exception {
        if (push.isV2()) {
            GroupDatabase.GroupRecord requireGroup = SignalDatabase.groups().requireGroup(push);
            GroupManagerV2.GroupEditor edit = new GroupManagerV2(context).edit(push.requireV2());
            try {
                GroupActionResult addMembers = edit.addMembers(collection, requireGroup.requireV2GroupProperties().getBannedMembers());
                edit.close();
                return addMembers;
            } catch (Throwable th) {
                if (edit != null) {
                    try {
                        edit.close();
                    } catch (Throwable th2) {
                        th.addSuppressed(th2);
                    }
                }
                throw th;
            }
        } else {
            GroupDatabase.GroupRecord requireGroup2 = SignalDatabase.groups().requireGroup(push);
            List<RecipientId> members = requireGroup2.getMembers();
            byte[] avatarBytes = requireGroup2.hasAvatar() ? AvatarHelper.getAvatarBytes(context, requireGroup2.getRecipientId()) : null;
            HashSet hashSet = new HashSet(members);
            int size = hashSet.size();
            hashSet.addAll(collection);
            return GroupManagerV1.updateGroup(context, push, hashSet, avatarBytes, requireGroup2.getTitle(), hashSet.size() - size);
        }
    }

    public static DecryptedGroupJoinInfo getGroupJoinInfoFromServer(Context context, GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword) throws IOException, VerificationFailedException, GroupLinkNotActiveException {
        return new GroupManagerV2(context).getGroupJoinInfoFromServer(groupMasterKey, groupLinkPassword);
    }

    public static GroupActionResult joinGroup(Context context, GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword, DecryptedGroupJoinInfo decryptedGroupJoinInfo, byte[] bArr) throws IOException, GroupChangeBusyException, GroupChangeFailedException, MembershipNotSuitableForV2Exception, GroupLinkNotActiveException {
        GroupManagerV2.GroupJoiner join = new GroupManagerV2(context).join(groupMasterKey, groupLinkPassword);
        try {
            GroupActionResult joinGroup = join.joinGroup(decryptedGroupJoinInfo, bArr);
            join.close();
            return joinGroup;
        } catch (Throwable th) {
            if (join != null) {
                try {
                    join.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void cancelJoinRequest(Context context, GroupId.V2 v2) throws GroupChangeFailedException, IOException, GroupChangeBusyException {
        GroupManagerV2.GroupJoiner cancelRequest = new GroupManagerV2(context).cancelRequest(v2.requireV2());
        try {
            cancelRequest.cancelJoinRequest();
            cancelRequest.close();
        } catch (Throwable th) {
            if (cancelRequest != null) {
                try {
                    cancelRequest.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public static void sendNoopUpdate(Context context, GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup) {
        new GroupManagerV2(context).sendNoopGroupUpdate(groupMasterKey, decryptedGroup);
    }

    public static GroupExternalCredential getGroupExternalCredential(Context context, GroupId.V2 v2) throws IOException, VerificationFailedException {
        return new GroupManagerV2(context).getGroupExternalCredential(v2);
    }

    public static Map<UUID, UuidCiphertext> getUuidCipherTexts(Context context, GroupId.V2 v2) {
        return new GroupManagerV2(context).getUuidCipherTexts(v2);
    }

    /* loaded from: classes4.dex */
    public static class GroupActionResult {
        private final int addedMemberCount;
        private final Recipient groupRecipient;
        private final List<RecipientId> invitedMembers;
        private final long threadId;

        public GroupActionResult(Recipient recipient, long j, int i, List<RecipientId> list) {
            this.groupRecipient = recipient;
            this.threadId = j;
            this.addedMemberCount = i;
            this.invitedMembers = list;
        }

        public Recipient getGroupRecipient() {
            return this.groupRecipient;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public int getAddedMemberCount() {
            return this.addedMemberCount;
        }

        public List<RecipientId> getInvitedMembers() {
            return this.invitedMembers;
        }
    }
}
