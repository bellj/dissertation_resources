package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public class GroupLinkInviteFriendsViewModel extends ViewModel {
    private static final boolean INITIAL_MEMBER_APPROVAL_STATE;
    private final MutableLiveData<Boolean> busy;
    private final MutableLiveData<EnableInviteLinkError> enableErrors;
    private final MutableLiveData<GroupInviteLinkUrl> enableSuccess;
    private final LiveData<GroupLinkUrlAndStatus> groupLink;
    private final MutableLiveData<Boolean> memberApproval;
    private final GroupLinkInviteRepository repository;

    private GroupLinkInviteFriendsViewModel(GroupId.V2 v2, GroupLinkInviteRepository groupLinkInviteRepository) {
        this.enableErrors = new SingleLiveEvent();
        this.busy = new MediatorLiveData();
        this.enableSuccess = new SingleLiveEvent();
        this.memberApproval = new MutableLiveData<>(Boolean.FALSE);
        this.repository = groupLinkInviteRepository;
        this.groupLink = new LiveGroup(v2).getGroupLink();
    }

    public LiveData<GroupLinkUrlAndStatus> getGroupInviteLinkAndStatus() {
        return this.groupLink;
    }

    public void enable() {
        this.busy.setValue(Boolean.TRUE);
        this.repository.enableGroupInviteLink(getCurrentMemberApproval(), new AsynchronousCallback.WorkerThread<GroupInviteLinkUrl, EnableInviteLinkError>() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invite.GroupLinkInviteFriendsViewModel.1
            public void onComplete(GroupInviteLinkUrl groupInviteLinkUrl) {
                GroupLinkInviteFriendsViewModel.this.busy.postValue(Boolean.FALSE);
                GroupLinkInviteFriendsViewModel.this.enableSuccess.postValue(groupInviteLinkUrl);
            }

            public void onError(EnableInviteLinkError enableInviteLinkError) {
                GroupLinkInviteFriendsViewModel.this.busy.postValue(Boolean.FALSE);
                GroupLinkInviteFriendsViewModel.this.enableErrors.postValue(enableInviteLinkError);
            }
        });
    }

    public LiveData<Boolean> isBusy() {
        return this.busy;
    }

    public LiveData<GroupInviteLinkUrl> getEnableSuccess() {
        return this.enableSuccess;
    }

    public LiveData<EnableInviteLinkError> getEnableErrors() {
        return this.enableErrors;
    }

    public LiveData<Boolean> getMemberApproval() {
        return this.memberApproval;
    }

    private boolean getCurrentMemberApproval() {
        Boolean value = this.memberApproval.getValue();
        if (value == null) {
            return false;
        }
        return value.booleanValue();
    }

    public void toggleMemberApproval() {
        this.memberApproval.postValue(Boolean.valueOf(!getCurrentMemberApproval()));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final Context context;
        private final GroupId.V2 groupId;

        public Factory(Context context, GroupId.V2 v2) {
            this.context = context;
            this.groupId = v2;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new GroupLinkInviteFriendsViewModel(this.groupId, new GroupLinkInviteRepository(this.context.getApplicationContext(), this.groupId));
        }
    }
}
