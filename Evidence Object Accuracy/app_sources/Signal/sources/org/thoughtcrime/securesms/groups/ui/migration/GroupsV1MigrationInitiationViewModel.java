package org.thoughtcrime.securesms.groups.ui.migration;

import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Objects;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class GroupsV1MigrationInitiationViewModel extends ViewModel {
    private final RecipientId groupRecipientId;
    private final MutableLiveData<MigrationState> migrationState;
    private final GroupsV1MigrationRepository repository;

    private GroupsV1MigrationInitiationViewModel(RecipientId recipientId) {
        this.groupRecipientId = recipientId;
        MutableLiveData<MigrationState> mutableLiveData = new MutableLiveData<>();
        this.migrationState = mutableLiveData;
        GroupsV1MigrationRepository groupsV1MigrationRepository = new GroupsV1MigrationRepository();
        this.repository = groupsV1MigrationRepository;
        Objects.requireNonNull(mutableLiveData);
        groupsV1MigrationRepository.getMigrationState(recipientId, new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationViewModel$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((MigrationState) obj);
            }
        });
    }

    public LiveData<MigrationState> getMigrationState() {
        return this.migrationState;
    }

    public LiveData<MigrationResult> onUpgradeClicked() {
        MutableLiveData mutableLiveData = new MutableLiveData();
        this.repository.upgradeGroup(this.groupRecipientId, new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationViewModel$$ExternalSyntheticLambda1
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                MutableLiveData.this.postValue((MigrationResult) obj);
            }
        });
        return mutableLiveData;
    }

    /* loaded from: classes4.dex */
    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final RecipientId groupRecipientId;

        public Factory(RecipientId recipientId) {
            this.groupRecipientId = recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new GroupsV1MigrationInitiationViewModel(this.groupRecipientId));
        }
    }
}
