package org.thoughtcrime.securesms.groups.ui;

import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ GroupMemberListAdapter.ViewHolder f$0;
    public final /* synthetic */ GroupMemberEntry f$1;

    public /* synthetic */ GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda0(GroupMemberListAdapter.ViewHolder viewHolder, GroupMemberEntry groupMemberEntry) {
        this.f$0 = viewHolder;
        this.f$1 = groupMemberEntry;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$bind$2(this.f$1);
    }
}
