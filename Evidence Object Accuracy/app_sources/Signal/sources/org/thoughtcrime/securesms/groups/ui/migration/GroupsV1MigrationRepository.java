package org.thoughtcrime.securesms.groups.ui.migration;

import androidx.core.util.Consumer;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda9;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupsV1MigrationUtil;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.RetrieveProfileJob;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.transport.RetryLaterException;

/* loaded from: classes4.dex */
public final class GroupsV1MigrationRepository {
    private static final String TAG = Log.tag(GroupsV1MigrationRepository.class);

    public /* synthetic */ void lambda$getMigrationState$0(Consumer consumer, RecipientId recipientId) {
        consumer.accept(getMigrationState(recipientId));
    }

    public void getMigrationState(RecipientId recipientId, Consumer<MigrationState> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(consumer, recipientId) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Consumer f$1;
            public final /* synthetic */ RecipientId f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupsV1MigrationRepository.this.lambda$getMigrationState$0(this.f$1, this.f$2);
            }
        });
    }

    public void upgradeGroup(RecipientId recipientId, Consumer<MigrationResult> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupsV1MigrationRepository.lambda$upgradeGroup$1(Consumer.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$upgradeGroup$1(Consumer consumer, RecipientId recipientId) {
        if (!NetworkConstraint.isMet(ApplicationDependencies.getApplication())) {
            Log.w(TAG, "No network!");
            consumer.accept(MigrationResult.FAILURE_NETWORK);
        } else if (!Recipient.resolved(recipientId).isPushV1Group()) {
            Log.w(TAG, "Not a V1 group!");
            consumer.accept(MigrationResult.FAILURE_GENERAL);
        } else {
            try {
                GroupsV1MigrationUtil.migrate(ApplicationDependencies.getApplication(), recipientId, true);
                consumer.accept(MigrationResult.SUCCESS);
            } catch (IOException | GroupChangeBusyException | RetryLaterException unused) {
                consumer.accept(MigrationResult.FAILURE_NETWORK);
            } catch (GroupsV1MigrationUtil.InvalidMigrationStateException unused2) {
                consumer.accept(MigrationResult.FAILURE_GENERAL);
            }
        }
    }

    private MigrationState getMigrationState(RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        if (!resolved.isPushV1Group()) {
            return new MigrationState(Collections.emptyList(), Collections.emptyList());
        }
        List<Recipient> resolvedList = Recipient.resolvedList(resolved.getParticipantIds());
        for (Job job : RetrieveProfileJob.forRecipients((Set) Stream.of(resolvedList).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupsV1MigrationRepository.lambda$getMigrationState$2((Recipient) obj);
            }
        }).map(new ContactUtil$$ExternalSyntheticLambda3()).collect(Collectors.toSet()))) {
            if (!ApplicationDependencies.getJobManager().runSynchronously(job, TimeUnit.SECONDS.toMillis(3)).isPresent()) {
                Log.w(TAG, "Failed to refresh capabilities in time!");
            }
        }
        try {
            RecipientUtil.ensureUuidsAreAvailable(ApplicationDependencies.getApplication(), Stream.of(resolvedList).filter(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda9()).toList());
        } catch (IOException e) {
            Log.w(TAG, "Failed to refresh UUIDs!", e);
        }
        resolved.fresh();
        List list = Stream.of(resolvedList).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupsV1MigrationRepository.lambda$getMigrationState$3((Recipient) obj);
            }
        }).toList();
        Stream of = Stream.of(resolvedList);
        Objects.requireNonNull(list);
        return new MigrationState(of.filterNot(new Predicate(list) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return this.f$0.contains((Recipient) obj);
            }
        }).filterNot(new GroupsV1MigrationRepository$$ExternalSyntheticLambda5()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationRepository$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupsV1MigrationRepository.lambda$getMigrationState$4((Recipient) obj);
            }
        }).toList(), list);
    }

    public static /* synthetic */ boolean lambda$getMigrationState$2(Recipient recipient) {
        return recipient.getGroupsV1MigrationCapability() != Recipient.Capability.SUPPORTED;
    }

    public static /* synthetic */ boolean lambda$getMigrationState$3(Recipient recipient) {
        return (recipient.hasServiceId() && recipient.getGroupsV1MigrationCapability() == Recipient.Capability.SUPPORTED && recipient.getRegistered() == RecipientDatabase.RegisteredState.REGISTERED) ? false : true;
    }

    public static /* synthetic */ boolean lambda$getMigrationState$4(Recipient recipient) {
        return recipient.getProfileKey() == null;
    }
}
