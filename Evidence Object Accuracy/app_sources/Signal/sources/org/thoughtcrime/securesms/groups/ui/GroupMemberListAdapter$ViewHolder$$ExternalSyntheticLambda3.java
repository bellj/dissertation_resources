package org.thoughtcrime.securesms.groups.ui;

import android.view.View;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda3 implements View.OnLongClickListener {
    public final /* synthetic */ GroupMemberListAdapter.ViewHolder f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda3(GroupMemberListAdapter.ViewHolder viewHolder, Recipient recipient) {
        this.f$0 = viewHolder;
        this.f$1 = recipient;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return this.f$0.lambda$bindRecipientClick$1(this.f$1, view);
    }
}
