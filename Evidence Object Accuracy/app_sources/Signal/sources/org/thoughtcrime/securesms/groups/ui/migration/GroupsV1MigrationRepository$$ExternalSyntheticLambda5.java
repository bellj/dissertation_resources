package org.thoughtcrime.securesms.groups.ui.migration;

import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV1MigrationRepository$$ExternalSyntheticLambda5 implements Predicate {
    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return ((Recipient) obj).isSelf();
    }
}
