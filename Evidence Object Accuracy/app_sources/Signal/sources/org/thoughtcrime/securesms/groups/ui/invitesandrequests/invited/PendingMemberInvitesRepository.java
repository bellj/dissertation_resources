package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited;

import android.content.Context;
import androidx.core.util.Consumer;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.google.protobuf.ByteString;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.GroupProtoUtil;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class PendingMemberInvitesRepository {
    private static final String TAG = Log.tag(PendingMemberInvitesRepository.class);
    private final Context context;
    private final Executor executor = SignalExecutors.BOUNDED;
    private final GroupId.V2 groupId;

    public PendingMemberInvitesRepository(Context context, GroupId.V2 v2) {
        this.context = context.getApplicationContext();
        this.groupId = v2;
    }

    public void getInvitees(Consumer<InviteeResult> consumer) {
        this.executor.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PendingMemberInvitesRepository.$r8$lambda$ZPPLXoHVsSJ1F1OVxf_3Y_zCzDE(PendingMemberInvitesRepository.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$getInvitees$1(Consumer consumer) {
        GroupDatabase.V2GroupProperties requireV2GroupProperties = SignalDatabase.groups().getGroup(this.groupId).get().requireV2GroupProperties();
        List<DecryptedPendingMember> pendingMembersList = requireV2GroupProperties.getDecryptedGroup().getPendingMembersList();
        ArrayList arrayList = new ArrayList(pendingMembersList.size());
        ArrayList arrayList2 = new ArrayList(pendingMembersList.size());
        ByteString byteString = SignalStore.account().requireAci().toByteString();
        boolean isAdmin = requireV2GroupProperties.isAdmin(Recipient.self());
        Stream.of(pendingMembersList).groupBy(new Function() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesRepository$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ((DecryptedPendingMember) obj).getAddedByUuid();
            }
        }).forEach(new com.annimon.stream.function.Consumer(byteString, arrayList, arrayList2) { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ ByteString f$1;
            public final /* synthetic */ List f$2;
            public final /* synthetic */ List f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                PendingMemberInvitesRepository.$r8$lambda$hlaO_o2mtlNZQELGBQ0m0uLfK4c(PendingMemberInvitesRepository.this, this.f$1, this.f$2, this.f$3, (Map.Entry) obj);
            }
        });
        consumer.accept(new InviteeResult(arrayList, arrayList2, isAdmin));
    }

    public /* synthetic */ void lambda$getInvitees$0(ByteString byteString, List list, List list2, Map.Entry entry) {
        ByteString byteString2 = (ByteString) entry.getKey();
        List<DecryptedPendingMember> list3 = (List) entry.getValue();
        if (byteString.equals(byteString2)) {
            for (DecryptedPendingMember decryptedPendingMember : list3) {
                try {
                    list.add(new SinglePendingMemberInvitedByYou(GroupProtoUtil.pendingMemberToRecipient(this.context, decryptedPendingMember), new UuidCiphertext(decryptedPendingMember.getUuidCipherText().toByteArray())));
                } catch (InvalidInputException e) {
                    Log.w(TAG, e);
                }
            }
            return;
        }
        Recipient uuidByteStringToRecipient = GroupProtoUtil.uuidByteStringToRecipient(this.context, byteString2);
        ArrayList arrayList = new ArrayList(list3.size());
        for (DecryptedPendingMember decryptedPendingMember2 : list3) {
            try {
                arrayList.add(new UuidCiphertext(decryptedPendingMember2.getUuidCipherText().toByteArray()));
            } catch (InvalidInputException e2) {
                Log.w(TAG, e2);
            }
        }
        list2.add(new MultiplePendingMembersInvitedByAnother(uuidByteStringToRecipient, arrayList));
    }

    public boolean revokeInvites(Collection<UuidCiphertext> collection) {
        try {
            GroupManager.revokeInvites(this.context, SignalStore.account().requireAci(), this.groupId, collection);
            return true;
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            return false;
        }
    }

    /* loaded from: classes4.dex */
    public static final class InviteeResult {
        private final List<SinglePendingMemberInvitedByYou> byMe;
        private final List<MultiplePendingMembersInvitedByAnother> byOthers;
        private final boolean canRevokeInvites;

        private InviteeResult(List<SinglePendingMemberInvitedByYou> list, List<MultiplePendingMembersInvitedByAnother> list2, boolean z) {
            this.byMe = list;
            this.byOthers = list2;
            this.canRevokeInvites = z;
        }

        public List<SinglePendingMemberInvitedByYou> getByMe() {
            return this.byMe;
        }

        public List<MultiplePendingMembersInvitedByAnother> getByOthers() {
            return this.byOthers;
        }

        public boolean isCanRevokeInvites() {
            return this.canRevokeInvites;
        }
    }

    /* loaded from: classes4.dex */
    public static final class SinglePendingMemberInvitedByYou {
        private final Recipient invitee;
        private final UuidCiphertext inviteeCipherText;

        private SinglePendingMemberInvitedByYou(Recipient recipient, UuidCiphertext uuidCiphertext) {
            this.invitee = recipient;
            this.inviteeCipherText = uuidCiphertext;
        }

        public Recipient getInvitee() {
            return this.invitee;
        }

        public UuidCiphertext getInviteeCipherText() {
            return this.inviteeCipherText;
        }
    }

    /* loaded from: classes4.dex */
    public static final class MultiplePendingMembersInvitedByAnother {
        private final Recipient inviter;
        private final Collection<UuidCiphertext> uuidCipherTexts;

        private MultiplePendingMembersInvitedByAnother(Recipient recipient, Collection<UuidCiphertext> collection) {
            this.inviter = recipient;
            this.uuidCipherTexts = collection;
        }

        public Recipient getInviter() {
            return this.inviter;
        }

        public Collection<UuidCiphertext> getUuidCipherTexts() {
            return this.uuidCipherTexts;
        }
    }
}
