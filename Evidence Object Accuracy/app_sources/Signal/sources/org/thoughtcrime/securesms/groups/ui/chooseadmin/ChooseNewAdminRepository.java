package org.thoughtcrime.securesms.groups.ui.chooseadmin;

import android.app.Application;
import java.io.IOException;
import java.util.List;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.GroupChangeResult;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class ChooseNewAdminRepository {
    private final Application context;

    public ChooseNewAdminRepository(Application application) {
        this.context = application;
    }

    public GroupChangeResult updateAdminsAndLeave(GroupId.V2 v2, List<RecipientId> list) {
        try {
            GroupManager.addMemberAdminsAndLeaveGroup(this.context, v2, list);
            return GroupChangeResult.SUCCESS;
        } catch (IOException | GroupChangeException e) {
            return GroupChangeResult.failure(GroupChangeFailureReason.fromException(e));
        }
    }
}
