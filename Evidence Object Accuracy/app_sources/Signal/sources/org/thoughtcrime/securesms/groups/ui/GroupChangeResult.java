package org.thoughtcrime.securesms.groups.ui;

/* loaded from: classes4.dex */
public final class GroupChangeResult {
    public static final GroupChangeResult SUCCESS = new GroupChangeResult(null);
    private final GroupChangeFailureReason failureReason;

    GroupChangeResult(GroupChangeFailureReason groupChangeFailureReason) {
        this.failureReason = groupChangeFailureReason;
    }

    public static GroupChangeResult failure(GroupChangeFailureReason groupChangeFailureReason) {
        return new GroupChangeResult(groupChangeFailureReason);
    }

    public boolean isSuccess() {
        return this.failureReason == null;
    }

    public GroupChangeFailureReason getFailureReason() {
        if (!isSuccess()) {
            return this.failureReason;
        }
        throw new UnsupportedOperationException();
    }
}
