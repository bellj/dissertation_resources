package org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.AdminActionsListener;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.RecipientClickListener;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* loaded from: classes4.dex */
public class PendingMemberInvitesFragment extends Fragment {
    private static final String GROUP_ID;
    private GroupMemberListView othersInvited;
    private View othersInvitedEmptyState;
    private PendingMemberInvitesViewModel viewModel;
    private GroupMemberListView youInvited;
    private View youInvitedEmptyState;

    public static PendingMemberInvitesFragment newInstance(GroupId.V2 v2) {
        PendingMemberInvitesFragment pendingMemberInvitesFragment = new PendingMemberInvitesFragment();
        Bundle bundle = new Bundle();
        bundle.putString(GROUP_ID, v2.toString());
        pendingMemberInvitesFragment.setArguments(bundle);
        return pendingMemberInvitesFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.group_pending_member_invites_fragment, viewGroup, false);
        this.youInvited = (GroupMemberListView) inflate.findViewById(R.id.members_you_invited);
        this.othersInvited = (GroupMemberListView) inflate.findViewById(R.id.members_others_invited);
        this.youInvitedEmptyState = inflate.findViewById(R.id.no_pending_from_you);
        this.othersInvitedEmptyState = inflate.findViewById(R.id.no_pending_from_others);
        this.youInvited.initializeAdapter(getViewLifecycleOwner());
        this.othersInvited.initializeAdapter(getViewLifecycleOwner());
        this.youInvited.setRecipientClickListener(new RecipientClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.groups.ui.RecipientClickListener
            public final void onClick(Recipient recipient) {
                PendingMemberInvitesFragment.this.lambda$onCreateView$0(recipient);
            }
        });
        this.youInvited.setAdminActionsListener(new AdminActionsListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesFragment.1
            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onRevokeInvite(GroupMemberEntry.PendingMember pendingMember) {
                PendingMemberInvitesFragment.this.viewModel.revokeInviteFor(pendingMember);
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onRevokeAllInvites(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
                throw new UnsupportedOperationException();
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onApproveRequest(GroupMemberEntry.RequestingMember requestingMember) {
                throw new UnsupportedOperationException();
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onDenyRequest(GroupMemberEntry.RequestingMember requestingMember) {
                throw new UnsupportedOperationException();
            }
        });
        this.othersInvited.setAdminActionsListener(new AdminActionsListener() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesFragment.2
            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onRevokeInvite(GroupMemberEntry.PendingMember pendingMember) {
                throw new UnsupportedOperationException();
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onRevokeAllInvites(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount) {
                PendingMemberInvitesFragment.this.viewModel.revokeInvitesFor(unknownPendingMemberCount);
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onApproveRequest(GroupMemberEntry.RequestingMember requestingMember) {
                throw new UnsupportedOperationException();
            }

            @Override // org.thoughtcrime.securesms.groups.ui.AdminActionsListener
            public void onDenyRequest(GroupMemberEntry.RequestingMember requestingMember) {
                throw new UnsupportedOperationException();
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(Recipient recipient) {
        RecipientBottomSheetDialogFragment.create(recipient.getId(), null).show(requireActivity().getSupportFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        String string = requireArguments().getString(GROUP_ID);
        Objects.requireNonNull(string);
        PendingMemberInvitesViewModel pendingMemberInvitesViewModel = (PendingMemberInvitesViewModel) ViewModelProviders.of(requireActivity(), new PendingMemberInvitesViewModel.Factory(requireContext(), GroupId.parseOrThrow(string).requireV2())).get(PendingMemberInvitesViewModel.class);
        this.viewModel = pendingMemberInvitesViewModel;
        pendingMemberInvitesViewModel.getWhoYouInvited().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PendingMemberInvitesFragment.this.lambda$onActivityCreated$1((List) obj);
            }
        });
        this.viewModel.getWhoOthersInvited().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PendingMemberInvitesFragment.this.lambda$onActivityCreated$2((List) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onActivityCreated$1(List list) {
        this.youInvited.setMembers(list);
        this.youInvitedEmptyState.setVisibility(list.isEmpty() ? 0 : 8);
    }

    public /* synthetic */ void lambda$onActivityCreated$2(List list) {
        this.othersInvited.setMembers(list);
        this.othersInvitedEmptyState.setVisibility(list.isEmpty() ? 0 : 8);
    }
}
