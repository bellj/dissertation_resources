package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import androidx.core.util.Consumer;
import java.util.List;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public abstract class GroupCreateResult {
    /* access modifiers changed from: package-private */
    public abstract void consume(Consumer<Success> consumer, Consumer<Error> consumer2);

    public static GroupCreateResult success(GroupManager.GroupActionResult groupActionResult) {
        return new Success(groupActionResult.getThreadId(), groupActionResult.getGroupRecipient(), groupActionResult.getAddedMemberCount(), Recipient.resolvedList(groupActionResult.getInvitedMembers()));
    }

    public static GroupCreateResult error(Error.Type type) {
        return new Error(type);
    }

    private GroupCreateResult() {
    }

    /* loaded from: classes4.dex */
    public static final class Success extends GroupCreateResult {
        private final int addedMemberCount;
        private final Recipient groupRecipient;
        private final List<Recipient> invitedMembers;
        private final long threadId;

        private Success(long j, Recipient recipient, int i, List<Recipient> list) {
            super();
            this.threadId = j;
            this.groupRecipient = recipient;
            this.addedMemberCount = i;
            this.invitedMembers = list;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public Recipient getGroupRecipient() {
            return this.groupRecipient;
        }

        int getAddedMemberCount() {
            return this.addedMemberCount;
        }

        public List<Recipient> getInvitedMembers() {
            return this.invitedMembers;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.creategroup.details.GroupCreateResult
        public void consume(Consumer<Success> consumer, Consumer<Error> consumer2) {
            consumer.accept(this);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Error extends GroupCreateResult {
        private final Type errorType;

        /* loaded from: classes4.dex */
        public enum Type {
            ERROR_IO,
            ERROR_BUSY,
            ERROR_FAILED,
            ERROR_INVALID_NAME
        }

        private Error(Type type) {
            super();
            this.errorType = type;
        }

        /* access modifiers changed from: package-private */
        @Override // org.thoughtcrime.securesms.groups.ui.creategroup.details.GroupCreateResult
        public void consume(Consumer<Success> consumer, Consumer<Error> consumer2) {
            consumer2.accept(this);
        }

        public Type getErrorType() {
            return this.errorType;
        }
    }
}
