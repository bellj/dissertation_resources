package org.thoughtcrime.securesms.groups.ui.invitesandrequests;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.invited.PendingMemberInvitesFragment;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.requesting.RequestingMembersFragment;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public class ManagePendingAndRequestingMembersActivity extends PassphraseRequiredActivity {
    private static final String GROUP_ID;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    public static Intent newIntent(Context context, GroupId.V2 v2) {
        Intent intent = new Intent(context, ManagePendingAndRequestingMembersActivity.class);
        intent.putExtra(GROUP_ID, v2.toString());
        return intent;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        setContentView(R.layout.group_pending_and_requesting_member_activity);
        if (bundle == null) {
            GroupId.V2 requireV2 = GroupId.parseOrThrow(getIntent().getStringExtra(GROUP_ID)).requireV2();
            ViewPager2 viewPager2 = (ViewPager2) findViewById(R.id.pending_and_requesting_pager);
            viewPager2.setAdapter(new ViewPagerAdapter(this, requireV2));
            new TabLayoutMediator((TabLayout) findViewById(R.id.pending_and_requesting_tabs), viewPager2, new TabLayoutMediator.TabConfigurationStrategy() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.ManagePendingAndRequestingMembersActivity$$ExternalSyntheticLambda0
                @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
                public final void onConfigureTab(TabLayout.Tab tab, int i) {
                    ManagePendingAndRequestingMembersActivity.lambda$onCreate$0(tab, i);
                }
            }).attach();
        }
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        requireSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public static /* synthetic */ void lambda$onCreate$0(TabLayout.Tab tab, int i) {
        if (i == 0) {
            tab.setText(R.string.PendingMembersActivity_requests);
        } else if (i == 1) {
            tab.setText(R.string.PendingMembersActivity_invites);
        } else {
            throw new AssertionError();
        }
    }

    /* loaded from: classes4.dex */
    private static class ViewPagerAdapter extends FragmentStateAdapter {
        private final GroupId.V2 groupId;

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return 2;
        }

        public ViewPagerAdapter(FragmentActivity fragmentActivity, GroupId.V2 v2) {
            super(fragmentActivity);
            this.groupId = v2;
        }

        @Override // androidx.viewpager2.adapter.FragmentStateAdapter
        public Fragment createFragment(int i) {
            if (i == 0) {
                return RequestingMembersFragment.newInstance(this.groupId);
            }
            if (i == 1) {
                return PendingMemberInvitesFragment.newInstance(this.groupId);
            }
            throw new AssertionError();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    @Override // androidx.appcompat.app.AppCompatActivity
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
