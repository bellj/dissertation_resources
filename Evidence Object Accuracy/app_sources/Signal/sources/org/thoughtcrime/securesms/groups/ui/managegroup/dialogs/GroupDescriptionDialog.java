package org.thoughtcrime.securesms.groups.ui.managegroup.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil;
import org.thoughtcrime.securesms.util.LongClickMovementMethod;

/* loaded from: classes4.dex */
public final class GroupDescriptionDialog extends DialogFragment {
    private static final String ARGUMENT_DESCRIPTION;
    private static final String ARGUMENT_GROUP_ID;
    private static final String ARGUMENT_LINKIFY;
    private static final String ARGUMENT_TITLE;
    private static final String DIALOG_TAG;
    private EmojiTextView descriptionText;

    public static void show(FragmentManager fragmentManager, String str, String str2, boolean z) {
        show(fragmentManager, null, str, str2, z);
    }

    public static void show(FragmentManager fragmentManager, GroupId groupId, String str, boolean z) {
        show(fragmentManager, groupId, null, str, z);
    }

    private static void show(FragmentManager fragmentManager, GroupId groupId, String str, String str2, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("group_id", ParcelableGroupId.from(groupId));
        bundle.putString("title", str);
        bundle.putString(ARGUMENT_DESCRIPTION, str2);
        bundle.putBoolean(ARGUMENT_LINKIFY, z);
        GroupDescriptionDialog groupDescriptionDialog = new GroupDescriptionDialog();
        groupDescriptionDialog.setArguments(bundle);
        groupDescriptionDialog.show(fragmentManager, DIALOG_TAG);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.group_description_dialog, (ViewGroup) null, false);
        String string = requireArguments().getString("title", null);
        String string2 = requireArguments().getString(ARGUMENT_DESCRIPTION, null);
        GroupId groupId = ParcelableGroupId.get((ParcelableGroupId) requireArguments().getParcelable("group_id"));
        boolean z = requireArguments().getBoolean(ARGUMENT_LINKIFY, false);
        LiveGroup liveGroup = groupId != null ? new LiveGroup(groupId) : null;
        EmojiTextView emojiTextView = (EmojiTextView) inflate.findViewById(R.id.group_description_dialog_text);
        this.descriptionText = emojiTextView;
        emojiTextView.setMovementMethod(LongClickMovementMethod.getInstance(requireContext()));
        AlertDialog create = new MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Signal_MaterialAlertDialog).setTitle((CharSequence) (TextUtils.isEmpty(string) ? getString(R.string.GroupDescriptionDialog__group_description) : string)).setView(inflate).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).create();
        if (string2 != null) {
            GroupDescriptionUtil.setText(requireContext(), this.descriptionText, string2, z, null);
        } else if (liveGroup != null) {
            liveGroup.getDescription().observe(this, new Observer(z) { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupDescriptionDialog$$ExternalSyntheticLambda0
                public final /* synthetic */ boolean f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    GroupDescriptionDialog.this.lambda$onCreateDialog$0(this.f$1, (String) obj);
                }
            });
        }
        if (TextUtils.isEmpty(string) && liveGroup != null) {
            LiveData<String> title = liveGroup.getTitle();
            Objects.requireNonNull(create);
            title.observe(this, new Observer(create) { // from class: org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupDescriptionDialog$$ExternalSyntheticLambda1
                public final /* synthetic */ Dialog f$0;

                {
                    this.f$0 = r1;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    this.f$0.setTitle((String) obj);
                }
            });
        }
        return create;
    }

    public /* synthetic */ void lambda$onCreateDialog$0(boolean z, String str) {
        GroupDescriptionUtil.setText(requireContext(), this.descriptionText, str, z, null);
    }
}
