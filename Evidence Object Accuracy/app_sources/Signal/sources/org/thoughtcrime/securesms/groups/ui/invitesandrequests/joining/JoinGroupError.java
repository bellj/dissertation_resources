package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

/* loaded from: classes4.dex */
public enum JoinGroupError {
    BUSY,
    GROUP_LINK_NOT_ACTIVE,
    BANNED,
    FAILED,
    NETWORK_ERROR
}
