package org.thoughtcrime.securesms.groups.v2;

import j$.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.groupsv2.GroupCandidate;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public class GroupCandidateHelper {
    private static final String TAG = Log.tag(GroupCandidateHelper.class);
    private final RecipientDatabase recipientDatabase = SignalDatabase.recipients();
    private final SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();

    public GroupCandidate recipientIdToCandidate(RecipientId recipientId) throws IOException {
        Recipient resolved = Recipient.resolved(recipientId);
        ServiceId orElse = resolved.getServiceId().orElse(null);
        if (orElse != null) {
            GroupCandidate groupCandidate = new GroupCandidate(orElse.uuid(), Optional.ofNullable(resolved.getExpiringProfileKeyCredential()));
            if (groupCandidate.hasValidProfileKeyCredential()) {
                return groupCandidate;
            }
            this.recipientDatabase.clearProfileKeyCredential(resolved.getId());
            Optional<ExpiringProfileKeyCredential> updateExpiringProfileKeyCredential = ProfileUtil.updateExpiringProfileKeyCredential(resolved);
            if (updateExpiringProfileKeyCredential.isPresent()) {
                return groupCandidate.withExpiringProfileKeyCredential(updateExpiringProfileKeyCredential.get());
            }
            return groupCandidate.withoutExpiringProfileKeyCredential();
        }
        throw new AssertionError("Non UUID members should have need detected by now");
    }

    public Set<GroupCandidate> recipientIdsToCandidates(Collection<RecipientId> collection) throws IOException {
        HashSet hashSet = new HashSet(collection.size());
        for (RecipientId recipientId : collection) {
            hashSet.add(recipientIdToCandidate(recipientId));
        }
        return hashSet;
    }

    public List<GroupCandidate> recipientIdsToCandidatesList(Collection<RecipientId> collection) throws IOException {
        ArrayList arrayList = new ArrayList(collection.size());
        for (RecipientId recipientId : collection) {
            arrayList.add(recipientIdToCandidate(recipientId));
        }
        return arrayList;
    }
}
