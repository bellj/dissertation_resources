package org.thoughtcrime.securesms.groups.v2;

import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.View;
import androidx.core.text.util.LinkifyCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.util.LinkUtil;
import org.thoughtcrime.securesms.util.LongClickCopySpan;

/* loaded from: classes4.dex */
public final class GroupDescriptionUtil {
    public static void setText(Context context, EmojiTextView emojiTextView, String str, boolean z, final Runnable runnable) {
        boolean z2 = runnable != null;
        if (z2) {
            str = str.replaceAll("\\n", " ");
        }
        SpannableString spannableString = new SpannableString(str);
        if (z && LinkifyCompat.addLinks(spannableString, 7)) {
            Stream.of((URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class)).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return GroupDescriptionUtil.lambda$setText$0((URLSpan) obj);
                }
            }).forEach(new GroupDescriptionUtil$$ExternalSyntheticLambda1(spannableString));
            URLSpan[] uRLSpanArr = (URLSpan[]) spannableString.getSpans(0, spannableString.length(), URLSpan.class);
            for (URLSpan uRLSpan : uRLSpanArr) {
                spannableString.setSpan(new LongClickCopySpan(uRLSpan.getURL()), spannableString.getSpanStart(uRLSpan), spannableString.getSpanEnd(uRLSpan), 33);
            }
        }
        if (z2) {
            AnonymousClass1 r12 = new ClickableSpan() { // from class: org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil.1
                @Override // android.text.style.ClickableSpan
                public void onClick(View view) {
                    runnable.run();
                }

                @Override // android.text.style.ClickableSpan, android.text.style.CharacterStyle
                public void updateDrawState(TextPaint textPaint) {
                    textPaint.setTypeface(Typeface.DEFAULT_BOLD);
                }
            };
            emojiTextView.setEllipsize(TextUtils.TruncateAt.END);
            emojiTextView.setMaxLines(2);
            SpannableString spannableString2 = new SpannableString(context.getString(R.string.ManageGroupActivity_more));
            spannableString2.setSpan(r12, 0, spannableString2.length(), 33);
            emojiTextView.setOverflowText(spannableString2);
        }
        emojiTextView.setText(spannableString);
    }

    public static /* synthetic */ boolean lambda$setText$0(URLSpan uRLSpan) {
        return LinkUtil.isLegalUrl(uRLSpan.getURL());
    }
}
