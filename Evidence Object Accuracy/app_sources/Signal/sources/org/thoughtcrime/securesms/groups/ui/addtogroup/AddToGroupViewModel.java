package org.thoughtcrime.securesms.groups.ui.addtogroup;

import android.app.Application;
import androidx.core.util.Consumer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupViewModel;
import org.thoughtcrime.securesms.groups.v2.GroupAddMembersResult;
import org.thoughtcrime.securesms.groups.v2.GroupManagementRepository;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public final class AddToGroupViewModel extends ViewModel {
    private final Application context;
    private final SingleLiveEvent<Event> events;
    private final RecipientId recipientId;
    private final GroupManagementRepository repository;

    private AddToGroupViewModel(RecipientId recipientId) {
        this.events = new SingleLiveEvent<>();
        this.context = ApplicationDependencies.getApplication();
        this.recipientId = recipientId;
        this.repository = new GroupManagementRepository();
    }

    public SingleLiveEvent<Event> getEvents() {
        return this.events;
    }

    public void onContinueWithSelection(List<RecipientId> list) {
        if (list.isEmpty()) {
            this.events.postValue(new Event.CloseEvent());
        } else if (list.size() == 1) {
            SignalExecutors.BOUNDED.execute(new Runnable(list) { // from class: org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupViewModel$$ExternalSyntheticLambda1
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AddToGroupViewModel.$r8$lambda$_5eVQ95oZZDlg8DB_8Cq1lYEfn4(AddToGroupViewModel.this, this.f$1);
                }
            });
        } else {
            throw new AssertionError("Does not support multi-select");
        }
    }

    public /* synthetic */ void lambda$onContinueWithSelection$0(List list) {
        Recipient resolved = Recipient.resolved(this.recipientId);
        Recipient resolved2 = Recipient.resolved((RecipientId) list.get(0));
        String displayName = resolved.getDisplayName(this.context);
        String displayName2 = resolved2.getDisplayName(this.context);
        if (!resolved2.getGroupId().get().isV1() || resolved.hasE164()) {
            this.events.postValue(new Event.AddToSingleGroupConfirmationEvent(this.context.getResources().getString(R.string.AddToGroupActivity_add_member), this.context.getResources().getString(R.string.AddToGroupActivity_add_s_to_s, displayName, displayName2), resolved2, displayName, displayName2));
        } else {
            this.events.postValue(new Event.LegacyGroupDenialEvent());
        }
    }

    public void onAddToGroupsConfirmed(Event.AddToSingleGroupConfirmationEvent addToSingleGroupConfirmationEvent) {
        this.repository.addMembers(addToSingleGroupConfirmationEvent.groupRecipient, Collections.singletonList(this.recipientId), new Consumer(addToSingleGroupConfirmationEvent) { // from class: org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ AddToGroupViewModel.Event.AddToSingleGroupConfirmationEvent f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                AddToGroupViewModel.$r8$lambda$PoloI9mPISSV6YIaHBQtIaCijus(AddToGroupViewModel.this, this.f$1, (GroupAddMembersResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onAddToGroupsConfirmed$1(Event.AddToSingleGroupConfirmationEvent addToSingleGroupConfirmationEvent, GroupAddMembersResult groupAddMembersResult) {
        if (groupAddMembersResult.isFailure()) {
            this.events.postValue(new Event.ToastEvent(this.context.getResources().getString(GroupErrors.getUserDisplayMessage(((GroupAddMembersResult.Failure) groupAddMembersResult).getReason()))));
            return;
        }
        this.events.postValue(new Event.ToastEvent(this.context.getResources().getString(R.string.AddToGroupActivity_s_added_to_s, addToSingleGroupConfirmationEvent.recipientName, addToSingleGroupConfirmationEvent.groupName)));
        this.events.postValue(new Event.CloseEvent());
    }

    /* loaded from: classes4.dex */
    public static abstract class Event {
        Event() {
        }

        /* loaded from: classes4.dex */
        public static class CloseEvent extends Event {
            CloseEvent() {
            }
        }

        /* loaded from: classes4.dex */
        public static class ToastEvent extends Event {
            private final String message;

            ToastEvent(String str) {
                this.message = str;
            }

            public String getMessage() {
                return this.message;
            }
        }

        /* loaded from: classes4.dex */
        public static class AddToSingleGroupConfirmationEvent extends Event {
            private final String groupName;
            private final Recipient groupRecipient;
            private final String message;
            private final String recipientName;
            private final String title;

            AddToSingleGroupConfirmationEvent(String str, String str2, Recipient recipient, String str3, String str4) {
                this.title = str;
                this.message = str2;
                this.groupRecipient = recipient;
                this.recipientName = str3;
                this.groupName = str4;
            }

            public String getTitle() {
                return this.title;
            }

            public String getMessage() {
                return this.message;
            }
        }

        /* loaded from: classes4.dex */
        public static class LegacyGroupDenialEvent extends Event {
            LegacyGroupDenialEvent() {
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;

        public Factory(RecipientId recipientId) {
            this.recipientId = recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new AddToGroupViewModel(this.recipientId));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
