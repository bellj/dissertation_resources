package org.thoughtcrime.securesms.groups.v2;

import android.content.Context;
import androidx.core.util.Consumer;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.v2.GroupAddMembersResult;
import org.thoughtcrime.securesms.groups.v2.GroupBlockJoinRequestResult;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: GroupManagementRepository.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0011\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J*\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ8\u0010\u0005\u001a\u00020\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\b2\b\u0010\u0010\u001a\u0004\u0018\u00010\u00112\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u0002J*\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u00112\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n2\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u001c\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\u0006\u0010\u0007\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0011R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/groups/v2/GroupManagementRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "addMembers", "", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "selected", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "consumer", "Landroidx/core/util/Consumer;", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult;", "potentialGroupId", "potentialGroupRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "groupRecipient", "blockJoinRequests", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/groups/v2/GroupBlockJoinRequestResult;", "Lorg/thoughtcrime/securesms/groups/GroupId$V2;", RecipientDatabase.TABLE_NAME, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupManagementRepository {
    private final Context context;

    public GroupManagementRepository() {
        this(null, 1, null);
    }

    public GroupManagementRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ GroupManagementRepository(android.content.Context r1, int r2, kotlin.jvm.internal.DefaultConstructorMarker r3) {
        /*
            r0 = this;
            r2 = r2 & 1
            if (r2 == 0) goto L_0x000d
            android.app.Application r1 = org.thoughtcrime.securesms.dependencies.ApplicationDependencies.getApplication()
            java.lang.String r2 = "getApplication()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
        L_0x000d:
            r0.<init>(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.groups.v2.GroupManagementRepository.<init>(android.content.Context, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final void addMembers(Recipient recipient, List<? extends RecipientId> list, Consumer<GroupAddMembersResult> consumer) {
        Intrinsics.checkNotNullParameter(recipient, "groupRecipient");
        Intrinsics.checkNotNullParameter(list, "selected");
        Intrinsics.checkNotNullParameter(consumer, "consumer");
        addMembers(null, recipient, list, consumer);
    }

    public final void addMembers(GroupId groupId, List<? extends RecipientId> list, Consumer<GroupAddMembersResult> consumer) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(list, "selected");
        Intrinsics.checkNotNullParameter(consumer, "consumer");
        addMembers(groupId, null, list, consumer);
    }

    private final void addMembers(GroupId groupId, Recipient recipient, List<? extends RecipientId> list, Consumer<GroupAddMembersResult> consumer) {
        SignalExecutors.UNBOUNDED.execute(new Runnable(recipient, list, this, consumer) { // from class: org.thoughtcrime.securesms.groups.v2.GroupManagementRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Recipient f$1;
            public final /* synthetic */ List f$2;
            public final /* synthetic */ GroupManagementRepository f$3;
            public final /* synthetic */ Consumer f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupManagementRepository.m1863addMembers$lambda3(GroupId.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        });
    }

    /* renamed from: addMembers$lambda-3 */
    public static final void m1863addMembers$lambda3(GroupId groupId, Recipient recipient, List list, GroupManagementRepository groupManagementRepository, Consumer consumer) {
        GroupId.Push push;
        Object obj;
        Intrinsics.checkNotNullParameter(list, "$selected");
        Intrinsics.checkNotNullParameter(groupManagementRepository, "this$0");
        Intrinsics.checkNotNullParameter(consumer, "$consumer");
        if (groupId != null) {
            push = groupId.requirePush();
        } else {
            push = null;
        }
        if (push == null) {
            Intrinsics.checkNotNull(recipient);
            push = recipient.requireGroupId().requirePush();
            Intrinsics.checkNotNullExpressionValue(push, "potentialGroupRecipient!…reGroupId().requirePush()");
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(Recipient.resolved((RecipientId) it.next()));
        }
        ArrayList arrayList2 = new ArrayList();
        Iterator it2 = arrayList.iterator();
        while (true) {
            boolean z = true;
            if (!it2.hasNext()) {
                break;
            }
            Object next = it2.next();
            Recipient recipient2 = (Recipient) next;
            if (!recipient2.hasServiceId() || !recipient2.isRegistered()) {
                z = false;
            }
            if (!z) {
                arrayList2.add(next);
            }
        }
        List<Recipient> list2 = CollectionsKt___CollectionsKt.toList(arrayList2);
        try {
            ContactDiscovery.refresh(groupManagementRepository.context, (List<? extends Recipient>) list2, false);
            for (Recipient recipient3 : list2) {
                Recipient.live(recipient3.getId()).refresh();
            }
        } catch (IOException unused) {
            consumer.accept(new GroupAddMembersResult.Failure(GroupChangeFailureReason.NETWORK));
        }
        try {
            ArrayList arrayList3 = new ArrayList();
            for (Object obj2 : list) {
                if (Recipient.resolved((RecipientId) obj2).isRegistered()) {
                    arrayList3.add(obj2);
                }
            }
            if (!arrayList3.isEmpty()) {
                GroupManager.GroupActionResult addMembers = GroupManager.addMembers(groupManagementRepository.context, push, arrayList3);
                Intrinsics.checkNotNullExpressionValue(addMembers, "addMembers(context, groupId, toAdd)");
                int addedMemberCount = addMembers.getAddedMemberCount();
                List<Recipient> resolvedList = Recipient.resolvedList(addMembers.getInvitedMembers());
                Intrinsics.checkNotNullExpressionValue(resolvedList, "resolvedList(groupActionResult.invitedMembers)");
                obj = new GroupAddMembersResult.Success(addedMemberCount, resolvedList);
            } else {
                obj = new GroupAddMembersResult.Failure(GroupChangeFailureReason.NOT_GV2_CAPABLE);
            }
        } catch (Exception e) {
            Log.d(GroupManagementRepositoryKt.TAG, "Failure to add member", e);
            GroupChangeFailureReason fromException = GroupChangeFailureReason.fromException(e);
            Intrinsics.checkNotNullExpressionValue(fromException, "fromException(e)");
            obj = new GroupAddMembersResult.Failure(fromException);
        }
        consumer.accept(obj);
    }

    public final Single<GroupBlockJoinRequestResult> blockJoinRequests(GroupId.V2 v2, Recipient recipient) {
        Intrinsics.checkNotNullParameter(v2, "groupId");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Single<GroupBlockJoinRequestResult> subscribeOn = Single.fromCallable(new Callable(v2, recipient) { // from class: org.thoughtcrime.securesms.groups.v2.GroupManagementRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ GroupId.V2 f$1;
            public final /* synthetic */ Recipient f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return GroupManagementRepository.m1864blockJoinRequests$lambda4(GroupManagementRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      try…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: blockJoinRequests$lambda-4 */
    public static final GroupBlockJoinRequestResult m1864blockJoinRequests$lambda4(GroupManagementRepository groupManagementRepository, GroupId.V2 v2, Recipient recipient) {
        GroupBlockJoinRequestResult.Failure failure;
        Intrinsics.checkNotNullParameter(groupManagementRepository, "this$0");
        Intrinsics.checkNotNullParameter(v2, "$groupId");
        Intrinsics.checkNotNullParameter(recipient, "$recipient");
        try {
            GroupManager.ban(groupManagementRepository.context, v2, recipient.getId());
            return GroupBlockJoinRequestResult.Success.INSTANCE;
        } catch (IOException e) {
            Log.w(GroupManagementRepositoryKt.TAG, e);
            GroupChangeFailureReason fromException = GroupChangeFailureReason.fromException(e);
            Intrinsics.checkNotNullExpressionValue(fromException, "fromException(e)");
            failure = new GroupBlockJoinRequestResult.Failure(fromException);
            return failure;
        } catch (GroupChangeException e2) {
            Log.w(GroupManagementRepositoryKt.TAG, e2);
            GroupChangeFailureReason fromException2 = GroupChangeFailureReason.fromException(e2);
            Intrinsics.checkNotNullExpressionValue(fromException2, "fromException(e)");
            failure = new GroupBlockJoinRequestResult.Failure(fromException2);
            return failure;
        }
    }
}
