package org.thoughtcrime.securesms.groups;

import j$.util.function.Function;
import org.thoughtcrime.securesms.groups.v2.GroupLinkPassword;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupManagerV2$$ExternalSyntheticLambda1 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ((GroupLinkPassword) obj).serialize();
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
