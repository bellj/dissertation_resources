package org.thoughtcrime.securesms.groups;

import android.content.Context;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.VerificationFailedException;
import org.signal.libsignal.zkgroup.groups.ClientZkGroupCipher;
import org.signal.libsignal.zkgroup.groups.GroupMasterKey;
import org.signal.libsignal.zkgroup.groups.GroupSecretParams;
import org.signal.libsignal.zkgroup.groups.UuidCiphertext;
import org.signal.libsignal.zkgroup.profiles.ExpiringProfileKeyCredential;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.GroupChange;
import org.signal.storageservice.protos.groups.GroupExternalCredential;
import org.signal.storageservice.protos.groups.Member;
import org.signal.storageservice.protos.groups.local.DecryptedBannedMember;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo;
import org.signal.storageservice.protos.groups.local.DecryptedMember;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactUtil$$ExternalSyntheticLambda3;
import org.thoughtcrime.securesms.conversationlist.ConversationListDataSource$$ExternalSyntheticLambda0;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.v2.GroupCandidateHelper;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.groups.v2.GroupLinkPassword;
import org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor;
import org.thoughtcrime.securesms.jobs.ProfileUploadJob;
import org.thoughtcrime.securesms.jobs.PushGroupSilentUpdateSendJob;
import org.thoughtcrime.securesms.jobs.RequestGroupV2InfoJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.groupsv2.DecryptedGroupUtil;
import org.whispersystems.signalservice.api.groupsv2.GroupCandidate;
import org.whispersystems.signalservice.api.groupsv2.GroupChangeReconstruct;
import org.whispersystems.signalservice.api.groupsv2.GroupChangeUtil;
import org.whispersystems.signalservice.api.groupsv2.GroupLinkNotActiveException;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Api;
import org.whispersystems.signalservice.api.groupsv2.GroupsV2Operations;
import org.whispersystems.signalservice.api.groupsv2.InvalidGroupStateException;
import org.whispersystems.signalservice.api.groupsv2.NotAbleToApplyGroupV2ChangeException;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceId;
import org.whispersystems.signalservice.api.push.ServiceIds;
import org.whispersystems.signalservice.api.push.exceptions.AuthorizationFailedException;
import org.whispersystems.signalservice.api.push.exceptions.ConflictException;
import org.whispersystems.signalservice.api.util.UuidUtil;
import org.whispersystems.signalservice.internal.push.exceptions.GroupExistsException;
import org.whispersystems.signalservice.internal.push.exceptions.GroupPatchNotAcceptedException;
import org.whispersystems.signalservice.internal.push.exceptions.NotInGroupException;

/* loaded from: classes4.dex */
public final class GroupManagerV2 {
    private static final String TAG = Log.tag(GroupManagerV2.class);
    private final GroupsV2Authorization authorization;
    private final Context context;
    private final GroupCandidateHelper groupCandidateHelper;
    private final GroupDatabase groupDatabase;
    private final GroupsV2Api groupsV2Api;
    private final GroupsV2Operations groupsV2Operations;
    private final GroupsV2StateProcessor groupsV2StateProcessor;
    private final ACI selfAci;
    private final PNI selfPni;
    private final SendGroupUpdateHelper sendGroupUpdateHelper;
    private final ServiceIds serviceIds;

    public GroupManagerV2(Context context) {
        this(context, SignalDatabase.groups(), ApplicationDependencies.getSignalServiceAccountManager().getGroupsV2Api(), ApplicationDependencies.getGroupsV2Operations(), ApplicationDependencies.getGroupsV2Authorization(), ApplicationDependencies.getGroupsV2StateProcessor(), SignalStore.account().getServiceIds(), new GroupCandidateHelper(), new SendGroupUpdateHelper(context));
    }

    GroupManagerV2(Context context, GroupDatabase groupDatabase, GroupsV2Api groupsV2Api, GroupsV2Operations groupsV2Operations, GroupsV2Authorization groupsV2Authorization, GroupsV2StateProcessor groupsV2StateProcessor, ServiceIds serviceIds, GroupCandidateHelper groupCandidateHelper, SendGroupUpdateHelper sendGroupUpdateHelper) {
        this.context = context;
        this.groupDatabase = groupDatabase;
        this.groupsV2Api = groupsV2Api;
        this.groupsV2Operations = groupsV2Operations;
        this.authorization = groupsV2Authorization;
        this.groupsV2StateProcessor = groupsV2StateProcessor;
        this.serviceIds = serviceIds;
        this.selfAci = serviceIds.getAci();
        this.selfPni = serviceIds.requirePni();
        this.groupCandidateHelper = groupCandidateHelper;
        this.sendGroupUpdateHelper = sendGroupUpdateHelper;
    }

    public DecryptedGroupJoinInfo getGroupJoinInfoFromServer(GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword) throws IOException, VerificationFailedException, GroupLinkNotActiveException {
        GroupSecretParams deriveFromMasterKey = GroupSecretParams.deriveFromMasterKey(groupMasterKey);
        return this.groupsV2Api.getGroupJoinInfo(deriveFromMasterKey, Optional.ofNullable(groupLinkPassword).map(new GroupManagerV2$$ExternalSyntheticLambda1()), this.authorization.getAuthorizationForToday(this.serviceIds, deriveFromMasterKey));
    }

    public GroupExternalCredential getGroupExternalCredential(GroupId.V2 v2) throws IOException, VerificationFailedException {
        return this.groupsV2Api.getGroupExternalCredential(this.authorization.getAuthorizationForToday(this.serviceIds, GroupSecretParams.deriveFromMasterKey(SignalDatabase.groups().requireGroup(v2).requireV2GroupProperties().getGroupMasterKey())));
    }

    public Map<UUID, UuidCiphertext> getUuidCipherTexts(GroupId.V2 v2) {
        GroupDatabase.V2GroupProperties requireV2GroupProperties = SignalDatabase.groups().requireGroup(v2).requireV2GroupProperties();
        ClientZkGroupCipher clientZkGroupCipher = new ClientZkGroupCipher(GroupSecretParams.deriveFromMasterKey(requireV2GroupProperties.getGroupMasterKey()));
        List<Recipient> memberRecipients = requireV2GroupProperties.getMemberRecipients(GroupDatabase.MemberSet.FULL_MEMBERS_INCLUDING_SELF);
        HashMap hashMap = new HashMap();
        for (Recipient recipient : memberRecipients) {
            hashMap.put(recipient.requireServiceId().uuid(), clientZkGroupCipher.encryptUuid(recipient.requireServiceId().uuid()));
        }
        return hashMap;
    }

    public GroupCreator create() throws GroupChangeBusyException {
        return new GroupCreator(GroupsV2ProcessingLock.acquireGroupProcessingLock());
    }

    public GroupEditor edit(GroupId.V2 v2) throws GroupChangeBusyException {
        return new GroupEditor(v2, GroupsV2ProcessingLock.acquireGroupProcessingLock());
    }

    public GroupJoiner join(GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword) throws GroupChangeBusyException {
        return new GroupJoiner(groupMasterKey, groupLinkPassword, GroupsV2ProcessingLock.acquireGroupProcessingLock());
    }

    public GroupJoiner cancelRequest(GroupId.V2 v2) throws GroupChangeBusyException {
        return new GroupJoiner(SignalDatabase.groups().requireGroup(v2).requireV2GroupProperties().getGroupMasterKey(), null, GroupsV2ProcessingLock.acquireGroupProcessingLock());
    }

    public GroupUpdater updater(GroupMasterKey groupMasterKey) throws GroupChangeBusyException {
        return new GroupUpdater(groupMasterKey, GroupsV2ProcessingLock.acquireGroupProcessingLock());
    }

    public void groupServerQuery(ServiceId serviceId, GroupMasterKey groupMasterKey) throws GroupNotAMemberException, IOException, GroupDoesNotExistException {
        new GroupsV2StateProcessor(this.context).forGroup(this.serviceIds, groupMasterKey).getCurrentGroupStateFromServer();
    }

    public DecryptedGroup addedGroupVersion(ServiceId serviceId, GroupMasterKey groupMasterKey) throws GroupNotAMemberException, IOException, GroupDoesNotExistException {
        GroupsV2StateProcessor.StateProcessorForGroup forGroup = new GroupsV2StateProcessor(this.context).forGroup(this.serviceIds, groupMasterKey);
        DecryptedGroup currentGroupStateFromServer = forGroup.getCurrentGroupStateFromServer();
        if (currentGroupStateFromServer.getRevision() == 0) {
            return currentGroupStateFromServer;
        }
        Optional<DecryptedMember> findMemberByUuid = DecryptedGroupUtil.findMemberByUuid(currentGroupStateFromServer.getMembersList(), this.selfAci.uuid());
        if (!findMemberByUuid.isPresent()) {
            return currentGroupStateFromServer;
        }
        DecryptedGroup specificVersionFromServer = forGroup.getSpecificVersionFromServer(findMemberByUuid.get().getJoinedAtRevision());
        if (specificVersionFromServer != null) {
            return specificVersionFromServer;
        }
        Log.w(TAG, "Unable to retrieve exact version joined at, using latest");
        return currentGroupStateFromServer;
    }

    public void migrateGroupOnToServer(GroupId.V1 v1, Collection<Recipient> collection) throws IOException, MembershipNotSuitableForV2Exception, GroupAlreadyExistsException, GroupChangeFailedException {
        GroupSecretParams deriveFromMasterKey = GroupSecretParams.deriveFromMasterKey(v1.deriveV2MigrationMasterKey());
        GroupDatabase.GroupRecord requireGroup = this.groupDatabase.requireGroup(v1);
        createGroupOnServer(deriveFromMasterKey, Util.emptyIfNull(requireGroup.getTitle()), requireGroup.hasAvatar() ? AvatarHelper.getAvatarBytes(this.context, requireGroup.getRecipientId()) : null, (Set) Stream.of(collection).map(new ContactUtil$$ExternalSyntheticLambda3()).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.groups.GroupManagerV2$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupManagerV2.lambda$migrateGroupOnToServer$0((RecipientId) obj);
            }
        }).collect(Collectors.toSet()), Member.Role.ADMINISTRATOR, Recipient.resolved(requireGroup.getRecipientId()).getExpiresInSeconds());
    }

    public static /* synthetic */ boolean lambda$migrateGroupOnToServer$0(RecipientId recipientId) {
        return recipientId.equals(Recipient.self().getId());
    }

    public void sendNoopGroupUpdate(GroupMasterKey groupMasterKey, DecryptedGroup decryptedGroup) {
        this.sendGroupUpdateHelper.sendGroupUpdate(groupMasterKey, new GroupMutation(decryptedGroup, DecryptedGroupChange.newBuilder().build(), decryptedGroup), null);
    }

    /* loaded from: classes4.dex */
    public final class GroupCreator extends LockOwner {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        GroupCreator(Closeable closeable) {
            super(closeable);
            GroupManagerV2.this = r1;
        }

        public GroupManager.GroupActionResult createGroup(ServiceId serviceId, Collection<RecipientId> collection, String str, byte[] bArr, int i) throws GroupChangeFailedException, IOException, MembershipNotSuitableForV2Exception {
            GroupSecretParams generate = GroupSecretParams.generate();
            try {
                DecryptedGroup createGroupOnServer = GroupManagerV2.this.createGroupOnServer(generate, str, bArr, collection, Member.Role.DEFAULT, i);
                GroupMasterKey masterKey = generate.getMasterKey();
                GroupId.V2 create = GroupManagerV2.this.groupDatabase.create(masterKey, createGroupOnServer);
                RecipientId orInsertFromGroupId = SignalDatabase.recipients().getOrInsertFromGroupId(create);
                Recipient resolved = Recipient.resolved(orInsertFromGroupId);
                AvatarHelper.setAvatar(GroupManagerV2.this.context, orInsertFromGroupId, bArr != null ? new ByteArrayInputStream(bArr) : null);
                GroupManagerV2.this.groupDatabase.onAvatarUpdated(create, bArr != null);
                SignalDatabase.recipients().setProfileSharing(resolved.getId(), true);
                RecipientAndThread sendGroupUpdate = GroupManagerV2.this.sendGroupUpdateHelper.sendGroupUpdate(masterKey, new GroupMutation(null, DecryptedGroupChange.newBuilder(GroupChangeReconstruct.reconstructGroupChange(DecryptedGroup.newBuilder().build(), createGroupOnServer)).setEditor(GroupManagerV2.this.selfAci.toByteString()).build(), createGroupOnServer), null);
                return new GroupManager.GroupActionResult(sendGroupUpdate.groupRecipient, sendGroupUpdate.threadId, createGroupOnServer.getMembersCount() - 1, GroupManagerV2.getPendingMemberRecipientIds(createGroupOnServer.getPendingMembersList()));
            } catch (GroupAlreadyExistsException e) {
                throw new GroupChangeFailedException(e);
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class GroupEditor extends LockOwner {
        private final GroupId.V2 groupId;
        private final GroupMasterKey groupMasterKey;
        private final GroupsV2Operations.GroupOperations groupOperations;
        private final GroupSecretParams groupSecretParams;
        private final GroupDatabase.V2GroupProperties v2GroupProperties;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        GroupEditor(GroupId.V2 v2, Closeable closeable) {
            super(closeable);
            GroupManagerV2.this = r1;
            GroupDatabase.GroupRecord requireGroup = r1.groupDatabase.requireGroup(v2);
            this.groupId = v2;
            GroupDatabase.V2GroupProperties requireV2GroupProperties = requireGroup.requireV2GroupProperties();
            this.v2GroupProperties = requireV2GroupProperties;
            GroupMasterKey groupMasterKey = requireV2GroupProperties.getGroupMasterKey();
            this.groupMasterKey = groupMasterKey;
            GroupSecretParams deriveFromMasterKey = GroupSecretParams.deriveFromMasterKey(groupMasterKey);
            this.groupSecretParams = deriveFromMasterKey;
            this.groupOperations = r1.groupsV2Operations.forGroup(deriveFromMasterKey);
        }

        public GroupManager.GroupActionResult addMembers(Collection<RecipientId> collection, Set<UUID> set) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException, MembershipNotSuitableForV2Exception {
            if (GroupsV2CapabilityChecker.allHaveServiceId(collection)) {
                Set<GroupCandidate> recipientIdsToCandidates = GroupManagerV2.this.groupCandidateHelper.recipientIdsToCandidates(new HashSet(collection));
                if (SignalStore.internalValues().gv2ForceInvites()) {
                    recipientIdsToCandidates = GroupCandidate.withoutExpiringProfileKeyCredentials(recipientIdsToCandidates);
                }
                return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createModifyGroupMembershipChange(recipientIdsToCandidates, set, GroupManagerV2.this.selfAci.uuid()));
            }
            throw new MembershipNotSuitableForV2Exception("At least one potential new member does not support GV2 or UUID capabilities");
        }

        public GroupManager.GroupActionResult updateGroupTimer(int i) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createModifyGroupTimerChange(i));
        }

        public GroupManager.GroupActionResult updateAttributesRights(GroupAccessControl groupAccessControl) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createChangeAttributesRights(GroupManagerV2.rightsToAccessControl(groupAccessControl)));
        }

        public GroupManager.GroupActionResult updateMembershipRights(GroupAccessControl groupAccessControl) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createChangeMembershipRights(GroupManagerV2.rightsToAccessControl(groupAccessControl)));
        }

        public GroupManager.GroupActionResult updateAnnouncementGroup(boolean z) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createAnnouncementGroupChange(z));
        }

        public GroupManager.GroupActionResult updateGroupTitleDescriptionAndAvatar(String str, String str2, byte[] bArr, boolean z) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            GroupChange.Actions.Builder builder;
            try {
                if (str != null) {
                    builder = this.groupOperations.createModifyGroupTitle(str);
                } else {
                    builder = GroupChange.Actions.newBuilder();
                }
                if (str2 != null) {
                    builder.setModifyDescription(this.groupOperations.createModifyGroupDescriptionAction(str2));
                }
                if (z) {
                    builder.setModifyAvatar(GroupChange.Actions.ModifyAvatarAction.newBuilder().setAvatar(bArr != null ? GroupManagerV2.this.groupsV2Api.uploadAvatar(bArr, this.groupSecretParams, GroupManagerV2.this.authorization.getAuthorizationForToday(GroupManagerV2.this.serviceIds, this.groupSecretParams)) : ""));
                }
                GroupManager.GroupActionResult commitChangeWithConflictResolution = commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, builder);
                if (z) {
                    AvatarHelper.setAvatar(GroupManagerV2.this.context, Recipient.externalGroupExact(this.groupId).getId(), bArr != null ? new ByteArrayInputStream(bArr) : null);
                    GroupManagerV2.this.groupDatabase.onAvatarUpdated(this.groupId, bArr != null);
                }
                return commitChangeWithConflictResolution;
            } catch (VerificationFailedException e) {
                throw new GroupChangeFailedException(e);
            }
        }

        public GroupManager.GroupActionResult revokeInvites(ServiceId serviceId, Collection<UuidCiphertext> collection, boolean z) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(serviceId, this.groupOperations.createRemoveInvitationChange(new HashSet(collection)), false, z);
        }

        public GroupManager.GroupActionResult approveRequests(Collection<RecipientId> collection) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createApproveGroupJoinRequest((Set) Stream.of(collection).map(new GroupManagerV2$GroupEditor$$ExternalSyntheticLambda1()).collect(Collectors.toSet())));
        }

        public static /* synthetic */ UUID lambda$approveRequests$0(RecipientId recipientId) {
            return Recipient.resolved(recipientId).requireServiceId().uuid();
        }

        public GroupManager.GroupActionResult denyRequests(Collection<RecipientId> collection) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createRefuseGroupJoinRequest((Set) Stream.of(collection).map(new GroupManagerV2$GroupEditor$$ExternalSyntheticLambda2()).collect(Collectors.toSet()), true, this.v2GroupProperties.getDecryptedGroup().getBannedMembersList()));
        }

        public static /* synthetic */ UUID lambda$denyRequests$1(RecipientId recipientId) {
            return Recipient.resolved(recipientId).requireServiceId().uuid();
        }

        public GroupManager.GroupActionResult setMemberAdmin(RecipientId recipientId, boolean z) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createChangeMemberRole(Recipient.resolved(recipientId).requireServiceId().uuid(), z ? Member.Role.ADMINISTRATOR : Member.Role.DEFAULT));
        }

        public void leaveGroup() throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            DecryptedGroup decryptedGroup = GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getDecryptedGroup();
            Optional<DecryptedMember> findMemberByUuid = DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), GroupManagerV2.this.selfAci.uuid());
            Optional<DecryptedPendingMember> findPendingByUuid = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), GroupManagerV2.this.selfAci.uuid());
            Optional<DecryptedPendingMember> findPendingByUuid2 = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), GroupManagerV2.this.selfPni.uuid());
            Optional<DecryptedPendingMember> empty = Optional.empty();
            ServiceId serviceId = GroupManagerV2.this.selfAci;
            if (!findPendingByUuid.isPresent()) {
                if (!findPendingByUuid2.isPresent() || findMemberByUuid.isPresent()) {
                    findPendingByUuid = empty;
                } else {
                    serviceId = GroupManagerV2.this.selfPni;
                    findPendingByUuid = findPendingByUuid2;
                }
            }
            if (findPendingByUuid.isPresent()) {
                try {
                    revokeInvites(serviceId, Collections.singleton(new UuidCiphertext(findPendingByUuid.get().getUuidCipherText().toByteArray())), false);
                } catch (InvalidInputException e) {
                    throw new AssertionError(e);
                }
            } else if (findMemberByUuid.isPresent()) {
                ejectMember(serviceId, true, false);
            } else {
                Log.i(GroupManagerV2.TAG, "Unable to leave group we are not pending or in");
            }
        }

        public GroupManager.GroupActionResult ejectMember(ServiceId serviceId, boolean z, boolean z2) throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            List<DecryptedBannedMember> list;
            ACI aci = GroupManagerV2.this.selfAci;
            GroupsV2Operations.GroupOperations groupOperations = this.groupOperations;
            Set<UUID> singleton = Collections.singleton(serviceId.uuid());
            if (z2) {
                list = this.v2GroupProperties.getDecryptedGroup().getBannedMembersList();
            } else {
                list = Collections.emptyList();
            }
            return commitChangeWithConflictResolution(aci, groupOperations.createRemoveMembersChange(singleton, z2, list), z);
        }

        public static /* synthetic */ UUID lambda$addMemberAdminsAndLeaveGroup$2(RecipientId recipientId) {
            return Recipient.resolved(recipientId).requireServiceId().uuid();
        }

        public GroupManager.GroupActionResult addMemberAdminsAndLeaveGroup(Collection<RecipientId> collection) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createLeaveAndPromoteMembersToAdmin(GroupManagerV2.this.selfAci.uuid(), Stream.of(collection).map(new GroupManagerV2$GroupEditor$$ExternalSyntheticLambda0()).toList()));
        }

        public GroupManager.GroupActionResult updateSelfProfileKeyInGroup() throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            ProfileKey selfProfileKey = ProfileKeyUtil.getSelfProfileKey();
            Optional<DecryptedMember> findMemberByUuid = DecryptedGroupUtil.findMemberByUuid(GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getDecryptedGroup().getMembersList(), GroupManagerV2.this.selfAci.uuid());
            if (!findMemberByUuid.isPresent()) {
                String str = GroupManagerV2.TAG;
                Log.w(str, "Self not in group " + this.groupId);
                return null;
            } else if (Arrays.equals(selfProfileKey.serialize(), findMemberByUuid.get().getProfileKey().toByteArray())) {
                String str2 = GroupManagerV2.TAG;
                Log.i(str2, "Own Profile Key is already up to date in group " + this.groupId);
                return null;
            } else {
                String str3 = GroupManagerV2.TAG;
                Log.i(str3, "Profile Key does not match that in group " + this.groupId);
                GroupCandidate recipientIdToCandidate = GroupManagerV2.this.groupCandidateHelper.recipientIdToCandidate(Recipient.self().getId());
                if (recipientIdToCandidate.hasValidProfileKeyCredential()) {
                    return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createUpdateProfileKeyCredentialChange(recipientIdToCandidate.requireExpiringProfileKeyCredential()));
                }
                Log.w(GroupManagerV2.TAG, "No credential available, repairing");
                ApplicationDependencies.getJobManager().add(new ProfileUploadJob());
                return null;
            }
        }

        public GroupManager.GroupActionResult acceptInvite() throws GroupChangeFailedException, GroupInsufficientRightsException, IOException, GroupNotAMemberException {
            DecryptedGroup decryptedGroup = GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getDecryptedGroup();
            if (DecryptedGroupUtil.findMemberByUuid(decryptedGroup.getMembersList(), GroupManagerV2.this.selfAci.uuid()).isPresent()) {
                Log.w(GroupManagerV2.TAG, "Self already in group");
                return null;
            }
            Optional<DecryptedPendingMember> findPendingByUuid = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), GroupManagerV2.this.selfAci.uuid());
            Optional<DecryptedPendingMember> findPendingByUuid2 = DecryptedGroupUtil.findPendingByUuid(decryptedGroup.getPendingMembersList(), GroupManagerV2.this.selfPni.uuid());
            GroupCandidate recipientIdToCandidate = GroupManagerV2.this.groupCandidateHelper.recipientIdToCandidate(Recipient.self().getId());
            if (!recipientIdToCandidate.hasValidProfileKeyCredential()) {
                Log.w(GroupManagerV2.TAG, "No credential available");
                return null;
            } else if (findPendingByUuid.isPresent()) {
                return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createAcceptInviteChange(recipientIdToCandidate.requireExpiringProfileKeyCredential()));
            } else {
                if (findPendingByUuid2.isPresent() && FeatureFlags.phoneNumberPrivacy()) {
                    return commitChangeWithConflictResolution(GroupManagerV2.this.selfPni, this.groupOperations.createAcceptPniInviteChange(recipientIdToCandidate.requireExpiringProfileKeyCredential()));
                }
                throw new GroupChangeFailedException("Unable to accept invite when not in pending list");
            }
        }

        public GroupManager.GroupActionResult ban(UUID uuid) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createBanUuidsChange(Collections.singleton(uuid), Collection$EL.stream(this.v2GroupProperties.getDecryptedGroup().getRequestingMembersList()).anyMatch(new GroupManagerV2$GroupEditor$$ExternalSyntheticLambda3(UuidUtil.toByteString(uuid))), this.v2GroupProperties.getDecryptedGroup().getBannedMembersList()));
        }

        public static /* synthetic */ boolean lambda$ban$3(ByteString byteString, DecryptedRequestingMember decryptedRequestingMember) {
            return decryptedRequestingMember.getUuid().equals(byteString);
        }

        public GroupManager.GroupActionResult unban(Set<UUID> set) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createUnbanUuidsChange(set));
        }

        public GroupManager.GroupActionResult cycleGroupLinkPassword() throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            return commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, this.groupOperations.createModifyGroupLinkPasswordChange(GroupLinkPassword.createNew().serialize()));
        }

        public GroupInviteLinkUrl setJoinByGroupLinkState(GroupManager.GroupLinkState groupLinkState) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            AccessControl.AccessRequired accessRequired;
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$GroupLinkState[groupLinkState.ordinal()];
            if (i == 1) {
                accessRequired = AccessControl.AccessRequired.UNSATISFIABLE;
            } else if (i == 2) {
                accessRequired = AccessControl.AccessRequired.ANY;
            } else if (i == 3) {
                accessRequired = AccessControl.AccessRequired.ADMINISTRATOR;
            } else {
                throw new AssertionError();
            }
            GroupChange.Actions.Builder createChangeJoinByLinkRights = this.groupOperations.createChangeJoinByLinkRights(accessRequired);
            GroupManager.GroupLinkState groupLinkState2 = GroupManager.GroupLinkState.DISABLED;
            if (groupLinkState != groupLinkState2 && GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getDecryptedGroup().getInviteLinkPassword().isEmpty()) {
                Log.d(GroupManagerV2.TAG, "First time enabling group links for group and password empty, generating");
                createChangeJoinByLinkRights = this.groupOperations.createModifyGroupLinkPasswordAndRightsChange(GroupLinkPassword.createNew().serialize(), accessRequired);
            }
            commitChangeWithConflictResolution(GroupManagerV2.this.selfAci, createChangeJoinByLinkRights);
            if (groupLinkState == groupLinkState2) {
                return null;
            }
            GroupDatabase.V2GroupProperties requireV2GroupProperties = GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties();
            return GroupInviteLinkUrl.forGroup(requireV2GroupProperties.getGroupMasterKey(), requireV2GroupProperties.getDecryptedGroup());
        }

        private GroupManager.GroupActionResult commitChangeWithConflictResolution(ServiceId serviceId, GroupChange.Actions.Builder builder) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            return commitChangeWithConflictResolution(serviceId, builder, false);
        }

        private GroupManager.GroupActionResult commitChangeWithConflictResolution(ServiceId serviceId, GroupChange.Actions.Builder builder, boolean z) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            return commitChangeWithConflictResolution(serviceId, builder, z, true);
        }

        private GroupManager.GroupActionResult commitChangeWithConflictResolution(ServiceId serviceId, GroupChange.Actions.Builder builder, boolean z, boolean z2) throws GroupChangeFailedException, GroupNotAMemberException, GroupInsufficientRightsException, IOException {
            builder.setSourceUuid(UuidUtil.toByteString(serviceId.uuid()));
            boolean z3 = false;
            for (int i = 0; i < 5; i++) {
                try {
                    return commitChange(serviceId, builder, z, z2);
                } catch (ConflictException e) {
                    Log.w(GroupManagerV2.TAG, "Invalid group patch or conflict", e);
                    builder = resolveConflict(serviceId, builder);
                    if (GroupChangeUtil.changeIsEmpty(builder.build())) {
                        Log.i(GroupManagerV2.TAG, "Change is empty after conflict resolution");
                        Recipient externalGroupExact = Recipient.externalGroupExact(this.groupId);
                        return new GroupManager.GroupActionResult(externalGroupExact, SignalDatabase.threads().getOrCreateThreadIdFor(externalGroupExact), 0, Collections.emptyList());
                    }
                } catch (GroupPatchNotAcceptedException e2) {
                    if (builder.getAddMembersCount() <= 0 || z3) {
                        throw new GroupChangeFailedException(e2);
                    }
                    builder = refetchAddMemberCredentials(builder);
                    z3 = true;
                }
            }
            throw new GroupChangeFailedException("Unable to apply change to group after conflicts");
        }

        private GroupChange.Actions.Builder resolveConflict(ServiceId serviceId, GroupChange.Actions.Builder builder) throws IOException, GroupNotAMemberException, GroupChangeFailedException {
            GroupsV2StateProcessor.GroupUpdateResult updateLocalGroupToRevision = GroupManagerV2.this.groupsV2StateProcessor.forGroup(GroupManagerV2.this.serviceIds, this.groupMasterKey).updateLocalGroupToRevision(Integer.MAX_VALUE, System.currentTimeMillis(), null);
            if (updateLocalGroupToRevision.getLatestServer() == null) {
                Log.w(GroupManagerV2.TAG, "Latest server state null.");
                throw new GroupChangeFailedException();
            } else if (updateLocalGroupToRevision.getGroupState() == GroupsV2StateProcessor.GroupState.GROUP_UPDATED) {
                Log.w(GroupManagerV2.TAG, "Group has been updated");
                try {
                    GroupChange.Actions build = builder.build();
                    return GroupChangeUtil.resolveConflict(updateLocalGroupToRevision.getLatestServer(), this.groupOperations.decryptChange(build, serviceId.uuid()), build);
                } catch (VerificationFailedException | InvalidGroupStateException e) {
                    throw new GroupChangeFailedException(e);
                }
            } else {
                Log.w(GroupManagerV2.TAG, String.format(Locale.US, "Server is ahead by %d revisions", Integer.valueOf(updateLocalGroupToRevision.getLatestServer().getRevision() - GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getGroupRevision())));
                throw new GroupChangeFailedException();
            }
        }

        private GroupChange.Actions.Builder refetchAddMemberCredentials(GroupChange.Actions.Builder builder) {
            try {
                List<RecipientId> list = (List) Collection$EL.stream(this.groupOperations.decryptAddMembers(builder.getAddMembersList())).map(new ConversationListDataSource$$ExternalSyntheticLambda0()).collect(j$.util.stream.Collectors.toList());
                for (RecipientId recipientId : list) {
                    ProfileUtil.updateExpiringProfileKeyCredential(Recipient.resolved(recipientId));
                }
                return this.groupOperations.replaceAddMembers(builder, GroupManagerV2.this.groupCandidateHelper.recipientIdsToCandidatesList(list));
            } catch (IOException | InvalidInputException | VerificationFailedException e) {
                Log.w(GroupManagerV2.TAG, "Unable to refetch credentials for added members, failing change", e);
                return builder;
            }
        }

        private GroupManager.GroupActionResult commitChange(ServiceId serviceId, GroupChange.Actions.Builder builder, boolean z, boolean z2) throws GroupNotAMemberException, GroupChangeFailedException, IOException, GroupInsufficientRightsException {
            GroupDatabase.V2GroupProperties requireV2GroupProperties = GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties();
            GroupChange.Actions build = builder.setRevision(requireV2GroupProperties.getGroupRevision() + 1).build();
            if (z || !Recipient.externalGroupExact(this.groupId).isBlocked()) {
                DecryptedGroup decryptedGroup = requireV2GroupProperties.getDecryptedGroup();
                GroupChange commitToServer = commitToServer(serviceId, build);
                try {
                    DecryptedGroupChange decryptedGroupChange = this.groupOperations.decryptChange(commitToServer, false).get();
                    DecryptedGroup apply = DecryptedGroupUtil.apply(decryptedGroup, decryptedGroupChange);
                    GroupManagerV2.this.groupDatabase.update(this.groupId, apply);
                    RecipientAndThread sendGroupUpdate = GroupManagerV2.this.sendGroupUpdateHelper.sendGroupUpdate(this.groupMasterKey, new GroupMutation(decryptedGroup, decryptedGroupChange, apply), commitToServer, z2);
                    return new GroupManager.GroupActionResult(sendGroupUpdate.groupRecipient, sendGroupUpdate.threadId, decryptedGroupChange.getNewMembersCount(), GroupManagerV2.getPendingMemberRecipientIds(decryptedGroupChange.getNewPendingMembersList()));
                } catch (VerificationFailedException | InvalidGroupStateException | NotAbleToApplyGroupV2ChangeException e) {
                    Log.w(GroupManagerV2.TAG, e);
                    throw new IOException(e);
                }
            } else {
                throw new GroupChangeFailedException("Group is blocked.");
            }
        }

        private GroupChange commitToServer(ServiceId serviceId, GroupChange.Actions actions) throws GroupNotAMemberException, GroupChangeFailedException, IOException, GroupInsufficientRightsException {
            try {
                return GroupManagerV2.this.groupsV2Api.patchGroup(actions, GroupManagerV2.this.authorization.getAuthorizationForToday(GroupManagerV2.this.serviceIds, this.groupSecretParams), Optional.empty());
            } catch (VerificationFailedException e) {
                Log.w(GroupManagerV2.TAG, e);
                throw new GroupChangeFailedException(e);
            } catch (AuthorizationFailedException e2) {
                Log.w(GroupManagerV2.TAG, e2);
                throw new GroupInsufficientRightsException(e2);
            } catch (NotInGroupException e3) {
                Log.w(GroupManagerV2.TAG, e3);
                throw new GroupNotAMemberException(e3);
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class GroupUpdater extends LockOwner {
        private final GroupMasterKey groupMasterKey;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        GroupUpdater(GroupMasterKey groupMasterKey, Closeable closeable) {
            super(closeable);
            GroupManagerV2.this = r1;
            this.groupMasterKey = groupMasterKey;
        }

        public void updateLocalToServerRevision(int i, long j, byte[] bArr) throws IOException, GroupNotAMemberException {
            new GroupsV2StateProcessor(GroupManagerV2.this.context).forGroup(GroupManagerV2.this.serviceIds, this.groupMasterKey).updateLocalGroupToRevision(i, j, getDecryptedGroupChange(bArr));
        }

        private DecryptedGroupChange getDecryptedGroupChange(byte[] bArr) {
            if (bArr != null) {
                try {
                    return GroupManagerV2.this.groupsV2Operations.forGroup(GroupSecretParams.deriveFromMasterKey(this.groupMasterKey)).decryptChange(GroupChange.parseFrom(bArr), true).orElse(null);
                } catch (InvalidProtocolBufferException | VerificationFailedException | InvalidGroupStateException e) {
                    Log.w(GroupManagerV2.TAG, "Unable to verify supplied group change", e);
                }
            }
            return null;
        }
    }

    public DecryptedGroup createGroupOnServer(GroupSecretParams groupSecretParams, String str, byte[] bArr, Collection<RecipientId> collection, Member.Role role, int i) throws GroupChangeFailedException, IOException, MembershipNotSuitableForV2Exception, GroupAlreadyExistsException {
        Throwable e;
        if (GroupsV2CapabilityChecker.allAndSelfHaveServiceId(collection)) {
            GroupCandidate recipientIdToCandidate = this.groupCandidateHelper.recipientIdToCandidate(Recipient.self().getId());
            Set<GroupCandidate> hashSet = new HashSet<>(this.groupCandidateHelper.recipientIdsToCandidates(collection));
            if (SignalStore.internalValues().gv2ForceInvites()) {
                Log.w(TAG, "Forcing GV2 invites due to internal setting");
                hashSet = GroupCandidate.withoutExpiringProfileKeyCredentials(hashSet);
            }
            if (recipientIdToCandidate.hasValidProfileKeyCredential()) {
                try {
                    this.groupsV2Api.putNewGroup(this.groupsV2Operations.createNewGroup(groupSecretParams, str, Optional.ofNullable(bArr), recipientIdToCandidate, hashSet, role, i), this.authorization.getAuthorizationForToday(this.serviceIds, groupSecretParams));
                    DecryptedGroup group = this.groupsV2Api.getGroup(groupSecretParams, ApplicationDependencies.getGroupsV2Authorization().getAuthorizationForToday(this.serviceIds, groupSecretParams));
                    if (group != null) {
                        return group;
                    }
                    throw new GroupChangeFailedException();
                } catch (VerificationFailedException e2) {
                    e = e2;
                    throw new GroupChangeFailedException(e);
                } catch (InvalidGroupStateException e3) {
                    e = e3;
                    throw new GroupChangeFailedException(e);
                } catch (GroupExistsException e4) {
                    throw new GroupAlreadyExistsException(e4);
                }
            } else {
                Log.w(TAG, "Cannot create a V2 group as self does not have a versioned profile");
                throw new MembershipNotSuitableForV2Exception("Cannot create a V2 group as self does not have a versioned profile");
            }
        } else {
            throw new MembershipNotSuitableForV2Exception("At least one potential new member does not support GV2 capability or we don't have their UUID");
        }
    }

    /* loaded from: classes4.dex */
    public final class GroupJoiner extends LockOwner {
        private final GroupId.V2 groupId;
        private final GroupMasterKey groupMasterKey;
        private final GroupsV2Operations.GroupOperations groupOperations;
        private final GroupSecretParams groupSecretParams;
        private final GroupLinkPassword password;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupJoiner(GroupMasterKey groupMasterKey, GroupLinkPassword groupLinkPassword, Closeable closeable) {
            super(closeable);
            GroupManagerV2.this = r1;
            this.groupId = GroupId.v2(groupMasterKey);
            this.password = groupLinkPassword;
            this.groupMasterKey = groupMasterKey;
            GroupSecretParams deriveFromMasterKey = GroupSecretParams.deriveFromMasterKey(groupMasterKey);
            this.groupSecretParams = deriveFromMasterKey;
            this.groupOperations = r1.groupsV2Operations.forGroup(deriveFromMasterKey);
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x00b2  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00ea  */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x0127  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0151  */
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0157  */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0165  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x0167  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x0174  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0193  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public org.thoughtcrime.securesms.groups.GroupManager.GroupActionResult joinGroup(org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo r18, byte[] r19) throws org.thoughtcrime.securesms.groups.GroupChangeFailedException, java.io.IOException, org.thoughtcrime.securesms.groups.MembershipNotSuitableForV2Exception, org.whispersystems.signalservice.api.groupsv2.GroupLinkNotActiveException {
            /*
            // Method dump skipped, instructions count: 461
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.groups.GroupManagerV2.GroupJoiner.joinGroup(org.signal.storageservice.protos.groups.local.DecryptedGroupJoinInfo, byte[]):org.thoughtcrime.securesms.groups.GroupManager$GroupActionResult");
        }

        private GroupManager.GroupActionResult fetchGroupStateAndSendUpdate(Recipient recipient, DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, GroupChange groupChange) throws GroupChangeFailedException, IOException {
            try {
                new GroupsV2StateProcessor(GroupManagerV2.this.context).forGroup(GroupManagerV2.this.serviceIds, this.groupMasterKey).updateLocalGroupToRevision(decryptedGroupChange.getRevision(), System.currentTimeMillis(), decryptedGroupChange);
                return new GroupManager.GroupActionResult(recipient, GroupManagerV2.this.sendGroupUpdateHelper.sendGroupUpdate(this.groupMasterKey, new GroupMutation(null, decryptedGroupChange, decryptedGroup), groupChange).threadId, 1, Collections.emptyList());
            } catch (IOException e) {
                String str = GroupManagerV2.TAG;
                Log.w(str, "Group data fetch failed, scheduling refresh of group info " + this.groupId, e);
                ApplicationDependencies.getJobManager().add(new RequestGroupV2InfoJob(this.groupId));
                throw e;
            } catch (GroupNotAMemberException e2) {
                String str2 = GroupManagerV2.TAG;
                Log.w(str2, "Despite adding self to group, server says we are not a member, scheduling refresh of group info " + this.groupId, e2);
                ApplicationDependencies.getJobManager().add(new RequestGroupV2InfoJob(this.groupId));
                throw new GroupChangeFailedException(e2);
            }
        }

        private DecryptedGroupChange decryptChange(GroupChange groupChange) throws GroupChangeFailedException {
            try {
                return this.groupOperations.decryptChange(groupChange, false).get();
            } catch (InvalidProtocolBufferException | VerificationFailedException | InvalidGroupStateException e) {
                Log.w(GroupManagerV2.TAG, e);
                throw new GroupChangeFailedException(e);
            }
        }

        private DecryptedGroup createPlaceholderGroup(DecryptedGroupJoinInfo decryptedGroupJoinInfo, boolean z) {
            DecryptedGroup.Builder revision = DecryptedGroup.newBuilder().setTitle(decryptedGroupJoinInfo.getTitle()).setAvatar(decryptedGroupJoinInfo.getAvatar()).setRevision(-1);
            Recipient self = Recipient.self();
            ByteString byteString = GroupManagerV2.this.selfAci.toByteString();
            byte[] profileKey = self.getProfileKey();
            Objects.requireNonNull(profileKey);
            ByteString copyFrom = ByteString.copyFrom(profileKey);
            if (z) {
                revision.addRequestingMembers(DecryptedRequestingMember.newBuilder().setUuid(byteString).setProfileKey(copyFrom));
            } else {
                revision.addMembers(DecryptedMember.newBuilder().setUuid(byteString).setProfileKey(copyFrom));
            }
            return revision.build();
        }

        private GroupChange joinGroupOnServer(boolean z, int i) throws GroupChangeFailedException, IOException, MembershipNotSuitableForV2Exception, GroupLinkNotActiveException, GroupJoinAlreadyAMemberException {
            GroupChange.Actions.Builder builder;
            if (GroupsV2CapabilityChecker.allAndSelfHaveServiceId(Collections.singleton(Recipient.self().getId()))) {
                GroupCandidate recipientIdToCandidate = GroupManagerV2.this.groupCandidateHelper.recipientIdToCandidate(Recipient.self().getId());
                if (recipientIdToCandidate.hasValidProfileKeyCredential()) {
                    ExpiringProfileKeyCredential requireExpiringProfileKeyCredential = recipientIdToCandidate.requireExpiringProfileKeyCredential();
                    if (z) {
                        builder = this.groupOperations.createGroupJoinRequest(requireExpiringProfileKeyCredential);
                    } else {
                        builder = this.groupOperations.createGroupJoinDirect(requireExpiringProfileKeyCredential);
                    }
                    builder.setSourceUuid(GroupManagerV2.this.selfAci.toByteString());
                    return commitJoinChangeWithConflictResolution(i, builder);
                }
                throw new MembershipNotSuitableForV2Exception("No profile key credential for self");
            }
            throw new MembershipNotSuitableForV2Exception("Self does not support GV2 or UUID capabilities");
        }

        private GroupChange commitJoinChangeWithConflictResolution(int i, GroupChange.Actions.Builder builder) throws GroupChangeFailedException, IOException, GroupLinkNotActiveException, GroupJoinAlreadyAMemberException {
            for (int i2 = 0; i2 < 5; i2++) {
                try {
                    GroupChange.Actions build = builder.setRevision(i + 1).build();
                    String str = GroupManagerV2.TAG;
                    Log.i(str, "Trying to join group at V" + build.getRevision());
                    GroupChange commitJoinToServer = commitJoinToServer(build);
                    String str2 = GroupManagerV2.TAG;
                    Log.i(str2, "Successfully joined group at V" + build.getRevision());
                    return commitJoinToServer;
                } catch (ConflictException e) {
                    Log.w(GroupManagerV2.TAG, "Revision conflict", e);
                    i = getCurrentGroupRevisionFromServer();
                } catch (GroupPatchNotAcceptedException e2) {
                    Log.w(GroupManagerV2.TAG, "Patch not accepted", e2);
                    try {
                        if (!alreadyPendingAdminApproval() && !testGroupMembership()) {
                            throw new GroupChangeFailedException(e2);
                        }
                        throw new GroupJoinAlreadyAMemberException(e2);
                    } catch (VerificationFailedException | InvalidGroupStateException e3) {
                        throw new GroupChangeFailedException(e3);
                    }
                }
            }
            throw new GroupChangeFailedException("Unable to join group after conflicts");
        }

        private GroupChange commitJoinToServer(GroupChange.Actions actions) throws GroupChangeFailedException, IOException, GroupLinkNotActiveException {
            Throwable e;
            try {
                return GroupManagerV2.this.groupsV2Api.patchGroup(actions, GroupManagerV2.this.authorization.getAuthorizationForToday(GroupManagerV2.this.serviceIds, this.groupSecretParams), Optional.ofNullable(this.password).map(new GroupManagerV2$$ExternalSyntheticLambda1()));
            } catch (VerificationFailedException e2) {
                e = e2;
                Log.w(GroupManagerV2.TAG, e);
                throw new GroupChangeFailedException(e);
            } catch (AuthorizationFailedException e3) {
                Log.w(GroupManagerV2.TAG, e3);
                throw new GroupLinkNotActiveException(e3, Optional.empty());
            } catch (NotInGroupException e4) {
                e = e4;
                Log.w(GroupManagerV2.TAG, e);
                throw new GroupChangeFailedException(e);
            }
        }

        private int getCurrentGroupRevisionFromServer() throws IOException, GroupLinkNotActiveException, GroupChangeFailedException {
            try {
                int revision = GroupManagerV2.this.getGroupJoinInfoFromServer(this.groupMasterKey, this.password).getRevision();
                String str = GroupManagerV2.TAG;
                Log.i(str, "Server now on V" + revision);
                return revision;
            } catch (VerificationFailedException e) {
                throw new GroupChangeFailedException(e);
            }
        }

        private boolean alreadyPendingAdminApproval() throws IOException, GroupLinkNotActiveException, GroupChangeFailedException {
            try {
                boolean pendingAdminApproval = GroupManagerV2.this.getGroupJoinInfoFromServer(this.groupMasterKey, this.password).getPendingAdminApproval();
                if (pendingAdminApproval) {
                    Log.i(GroupManagerV2.TAG, "User is already pending admin approval");
                }
                return pendingAdminApproval;
            } catch (VerificationFailedException e) {
                throw new GroupChangeFailedException(e);
            }
        }

        private boolean testGroupMembership() throws IOException, VerificationFailedException, InvalidGroupStateException {
            try {
                GroupManagerV2.this.groupsV2Api.getGroup(this.groupSecretParams, GroupManagerV2.this.authorization.getAuthorizationForToday(GroupManagerV2.this.serviceIds, this.groupSecretParams));
                return true;
            } catch (NotInGroupException unused) {
                return false;
            }
        }

        public void cancelJoinRequest() throws GroupChangeFailedException, IOException {
            try {
                GroupChange commitCancelChangeWithConflictResolution = commitCancelChangeWithConflictResolution(this.groupOperations.createRefuseGroupJoinRequest(Collections.singleton(GroupManagerV2.this.selfAci.uuid()), false, Collections.emptyList()));
                DecryptedGroup decryptedGroup = GroupManagerV2.this.groupDatabase.requireGroup(this.groupId).requireV2GroupProperties().getDecryptedGroup();
                try {
                    DecryptedGroupChange decryptedGroupChange = this.groupOperations.decryptChange(commitCancelChangeWithConflictResolution, false).get();
                    DecryptedGroup applyWithoutRevisionCheck = DecryptedGroupUtil.applyWithoutRevisionCheck(decryptedGroup, decryptedGroupChange);
                    GroupManagerV2.this.groupDatabase.update(this.groupId, resetRevision(applyWithoutRevisionCheck, decryptedGroup.getRevision()));
                    GroupManagerV2.this.sendGroupUpdateHelper.sendGroupUpdate(this.groupMasterKey, new GroupMutation(decryptedGroup, decryptedGroupChange, applyWithoutRevisionCheck), commitCancelChangeWithConflictResolution, false);
                } catch (VerificationFailedException | InvalidGroupStateException | NotAbleToApplyGroupV2ChangeException e) {
                    throw new GroupChangeFailedException(e);
                }
            } catch (GroupLinkNotActiveException e2) {
                Log.d(GroupManagerV2.TAG, "Unexpected unable to leave group due to group link off");
                throw new GroupChangeFailedException(e2);
            }
        }

        private DecryptedGroup resetRevision(DecryptedGroup decryptedGroup, int i) {
            return DecryptedGroup.newBuilder(decryptedGroup).setRevision(i).build();
        }

        private GroupChange commitCancelChangeWithConflictResolution(GroupChange.Actions.Builder builder) throws GroupChangeFailedException, IOException, GroupLinkNotActiveException {
            int currentGroupRevisionFromServer = getCurrentGroupRevisionFromServer();
            for (int i = 0; i < 5; i++) {
                try {
                    GroupChange.Actions build = builder.setRevision(currentGroupRevisionFromServer + 1).build();
                    String str = GroupManagerV2.TAG;
                    Log.i(str, "Trying to cancel request group at V" + build.getRevision());
                    GroupChange commitJoinToServer = commitJoinToServer(build);
                    String str2 = GroupManagerV2.TAG;
                    Log.i(str2, "Successfully cancelled group join at V" + build.getRevision());
                    return commitJoinToServer;
                } catch (ConflictException e) {
                    Log.w(GroupManagerV2.TAG, "Revision conflict", e);
                    currentGroupRevisionFromServer = getCurrentGroupRevisionFromServer();
                } catch (GroupPatchNotAcceptedException e2) {
                    throw new GroupChangeFailedException(e2);
                }
            }
            throw new GroupChangeFailedException("Unable to cancel group join request after conflicts");
        }
    }

    /* loaded from: classes4.dex */
    public static abstract class LockOwner implements Closeable {
        final Closeable lock;

        LockOwner(Closeable closeable) {
            this.lock = closeable;
        }

        @Override // java.io.Closeable, java.lang.AutoCloseable
        public void close() throws IOException {
            this.lock.close();
        }
    }

    /* loaded from: classes4.dex */
    public static class SendGroupUpdateHelper {
        private final Context context;

        SendGroupUpdateHelper(Context context) {
            this.context = context;
        }

        RecipientAndThread sendGroupUpdate(GroupMasterKey groupMasterKey, GroupMutation groupMutation, GroupChange groupChange) {
            return sendGroupUpdate(groupMasterKey, groupMutation, groupChange, true);
        }

        RecipientAndThread sendGroupUpdate(GroupMasterKey groupMasterKey, GroupMutation groupMutation, GroupChange groupChange, boolean z) {
            GroupId.V2 v2 = GroupId.v2(groupMasterKey);
            Recipient externalGroupExact = Recipient.externalGroupExact(v2);
            OutgoingGroupUpdateMessage outgoingGroupUpdateMessage = new OutgoingGroupUpdateMessage(externalGroupExact, GroupProtoUtil.createDecryptedGroupV2Context(groupMasterKey, groupMutation, groupChange), (Attachment) null, System.currentTimeMillis(), 0L, false, (QuoteModel) null, (List<Contact>) Collections.emptyList(), (List<LinkPreview>) Collections.emptyList(), (List<Mention>) Collections.emptyList());
            DecryptedGroupChange groupChange2 = groupMutation.getGroupChange();
            if (groupChange2 != null && DecryptedGroupUtil.changeIsSilent(groupChange2)) {
                if (z) {
                    ApplicationDependencies.getJobManager().add(PushGroupSilentUpdateSendJob.create(this.context, v2, groupMutation.getNewGroupState(), outgoingGroupUpdateMessage));
                }
                return new RecipientAndThread(externalGroupExact, -1);
            } else if (z) {
                return new RecipientAndThread(externalGroupExact, MessageSender.send(this.context, (OutgoingMediaMessage) outgoingGroupUpdateMessage, -1L, false, (String) null, (MessageDatabase.InsertListener) null));
            } else {
                long orCreateValidThreadId = SignalDatabase.threads().getOrCreateValidThreadId(outgoingGroupUpdateMessage.getRecipient(), -1, outgoingGroupUpdateMessage.getDistributionType());
                try {
                    SignalDatabase.mms().markAsSent(SignalDatabase.mms().insertMessageOutbox(outgoingGroupUpdateMessage, orCreateValidThreadId, false, null), true);
                    SignalDatabase.threads().update(orCreateValidThreadId, true);
                    return new RecipientAndThread(externalGroupExact, orCreateValidThreadId);
                } catch (MmsException e) {
                    throw new AssertionError(e);
                }
            }
        }
    }

    public static List<RecipientId> getPendingMemberRecipientIds(List<DecryptedPendingMember> list) {
        return Stream.of(DecryptedGroupUtil.pendingToUuidList(list)).map(new Function() { // from class: org.thoughtcrime.securesms.groups.GroupManagerV2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return GroupManagerV2.lambda$getPendingMemberRecipientIds$1((UUID) obj);
            }
        }).toList();
    }

    public static /* synthetic */ RecipientId lambda$getPendingMemberRecipientIds$1(UUID uuid) {
        return RecipientId.from(ServiceId.from(uuid));
    }

    /* renamed from: org.thoughtcrime.securesms.groups.GroupManagerV2$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$GroupLinkState;

        static {
            int[] iArr = new int[GroupAccessControl.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl = iArr;
            try {
                iArr[GroupAccessControl.ALL_MEMBERS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl[GroupAccessControl.ONLY_ADMINS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl[GroupAccessControl.NO_ONE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            int[] iArr2 = new int[GroupManager.GroupLinkState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$GroupLinkState = iArr2;
            try {
                iArr2[GroupManager.GroupLinkState.DISABLED.ordinal()] = 1;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$GroupLinkState[GroupManager.GroupLinkState.ENABLED.ordinal()] = 2;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupManager$GroupLinkState[GroupManager.GroupLinkState.ENABLED_WITH_APPROVAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused6) {
            }
        }
    }

    public static AccessControl.AccessRequired rightsToAccessControl(GroupAccessControl groupAccessControl) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl[groupAccessControl.ordinal()];
        if (i == 1) {
            return AccessControl.AccessRequired.MEMBER;
        }
        if (i == 2) {
            return AccessControl.AccessRequired.ADMINISTRATOR;
        }
        if (i == 3) {
            return AccessControl.AccessRequired.UNSATISFIABLE;
        }
        throw new AssertionError();
    }

    /* loaded from: classes4.dex */
    public static class RecipientAndThread {
        private final Recipient groupRecipient;
        private final long threadId;

        RecipientAndThread(Recipient recipient, long j) {
            this.groupRecipient = recipient;
            this.threadId = j;
        }
    }
}
