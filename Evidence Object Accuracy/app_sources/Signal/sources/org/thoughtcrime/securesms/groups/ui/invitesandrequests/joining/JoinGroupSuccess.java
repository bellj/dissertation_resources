package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class JoinGroupSuccess {
    private final Recipient groupRecipient;
    private final long groupThreadId;

    public JoinGroupSuccess(Recipient recipient, long j) {
        this.groupRecipient = recipient;
        this.groupThreadId = j;
    }

    public Recipient getGroupRecipient() {
        return this.groupRecipient;
    }

    public long getGroupThreadId() {
        return this.groupThreadId;
    }
}
