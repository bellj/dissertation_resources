package org.thoughtcrime.securesms.groups.ui.addtogroup;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.ActionBar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.ContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class AddToGroupsActivity extends ContactSelectionActivity {
    private static final String EXTRA_RECIPIENT_ID;
    private static final int MINIMUM_GROUP_SELECT_SIZE;
    private View next;
    private AddToGroupViewModel viewModel;

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    public static Intent newIntent(Context context, RecipientId recipientId, List<RecipientId> list) {
        Intent intent = new Intent(context, AddToGroupsActivity.class);
        intent.putExtra("refreshable", false);
        intent.putExtra("recents", true);
        intent.putExtra(ContactSelectionActivity.EXTRA_LAYOUT_RES_ID, R.layout.add_to_group_activity);
        intent.putExtra(EXTRA_RECIPIENT_ID, recipientId);
        intent.putExtra("display_mode", 4);
        intent.putParcelableArrayListExtra("current_selection", new ArrayList<>(list));
        return intent;
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        ActionBar supportActionBar = getSupportActionBar();
        Objects.requireNonNull(supportActionBar);
        supportActionBar.setDisplayHomeAsUpEnabled(true);
        this.next = findViewById(R.id.next);
        getContactFilterView().setHint(this.contactsFragment.isMulti() ? R.string.AddToGroupActivity_add_to_groups : R.string.AddToGroupActivity_add_to_group);
        this.next.setVisibility(this.contactsFragment.isMulti() ? 0 : 8);
        disableNext();
        this.next.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupsActivity$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AddToGroupsActivity.this.lambda$onCreate$0(view);
            }
        });
        AddToGroupViewModel addToGroupViewModel = (AddToGroupViewModel) ViewModelProviders.of(this, new AddToGroupViewModel.Factory(getRecipientId())).get(AddToGroupViewModel.class);
        this.viewModel = addToGroupViewModel;
        addToGroupViewModel.getEvents().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupsActivity$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddToGroupsActivity.this.lambda$onCreate$2((AddToGroupViewModel.Event) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        handleNextPressed();
    }

    public /* synthetic */ void lambda$onCreate$2(AddToGroupViewModel.Event event) {
        if (event instanceof AddToGroupViewModel.Event.CloseEvent) {
            finish();
        } else if (event instanceof AddToGroupViewModel.Event.ToastEvent) {
            Toast.makeText(this, ((AddToGroupViewModel.Event.ToastEvent) event).getMessage(), 0).show();
        } else if (event instanceof AddToGroupViewModel.Event.AddToSingleGroupConfirmationEvent) {
            AddToGroupViewModel.Event.AddToSingleGroupConfirmationEvent addToSingleGroupConfirmationEvent = (AddToGroupViewModel.Event.AddToSingleGroupConfirmationEvent) event;
            new MaterialAlertDialogBuilder(this).setTitle((CharSequence) addToSingleGroupConfirmationEvent.getTitle()).setMessage((CharSequence) addToSingleGroupConfirmationEvent.getMessage()).setPositiveButton(R.string.AddToGroupActivity_add, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(addToSingleGroupConfirmationEvent) { // from class: org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupsActivity$$ExternalSyntheticLambda3
                public final /* synthetic */ AddToGroupViewModel.Event.AddToSingleGroupConfirmationEvent f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    AddToGroupsActivity.this.lambda$onCreate$1(this.f$1, dialogInterface, i);
                }
            }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
        } else if (event instanceof AddToGroupViewModel.Event.LegacyGroupDenialEvent) {
            Toast.makeText(this, (int) R.string.AddToGroupActivity_this_person_cant_be_added_to_legacy_groups, 0).show();
        } else {
            throw new AssertionError();
        }
    }

    public /* synthetic */ void lambda$onCreate$1(AddToGroupViewModel.Event.AddToSingleGroupConfirmationEvent addToSingleGroupConfirmationEvent, DialogInterface dialogInterface, int i) {
        this.viewModel.onAddToGroupsConfirmed(addToSingleGroupConfirmationEvent);
    }

    private RecipientId getRecipientId() {
        return (RecipientId) getIntent().getParcelableExtra(EXTRA_RECIPIENT_ID);
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        finish();
        return true;
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer) {
        if (!this.contactsFragment.isMulti()) {
            if (optional.isPresent()) {
                this.viewModel.onContinueWithSelection(Collections.singletonList(optional.get()));
            }
            consumer.accept(Boolean.TRUE);
            return;
        }
        throw new UnsupportedOperationException("Not yet built to handle multi-select.");
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
        if (this.contactsFragment.hasQueryFilter()) {
            getContactFilterView().clear();
        }
        if (this.contactsFragment.getSelectedContactsCount() < 1) {
            disableNext();
        }
    }

    private void enableNext() {
        this.next.setEnabled(true);
        this.next.animate().alpha(1.0f);
    }

    private void disableNext() {
        this.next.setEnabled(false);
        this.next.animate().alpha(0.5f);
    }

    private void handleNextPressed() {
        this.viewModel.onContinueWithSelection(Stream.of(this.contactsFragment.getSelectedContacts()).map(new Function() { // from class: org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupsActivity$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AddToGroupsActivity.this.lambda$handleNextPressed$3((SelectedContact) obj);
            }
        }).toList());
    }

    public /* synthetic */ RecipientId lambda$handleNextPressed$3(SelectedContact selectedContact) {
        return selectedContact.getOrCreateRecipientId(this);
    }
}
