package org.thoughtcrime.securesms.groups.v2.processing;

import j$.util.function.Function;
import java.util.UUID;
import org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda5 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return GroupsV2StateProcessor.ProfileAndMessageHelper.lambda$determineProfileSharing$3((UUID) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
