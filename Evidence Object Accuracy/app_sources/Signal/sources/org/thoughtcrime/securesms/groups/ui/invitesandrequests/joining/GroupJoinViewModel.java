package org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.util.AsynchronousCallback;
import org.thoughtcrime.securesms.util.SingleLiveEvent;

/* loaded from: classes4.dex */
public class GroupJoinViewModel extends ViewModel {
    private final MutableLiveData<Boolean> busy;
    private final MutableLiveData<FetchGroupDetailsError> errors;
    private final MutableLiveData<GroupDetails> groupDetails;
    private final MutableLiveData<JoinGroupError> joinErrors;
    private final MutableLiveData<JoinGroupSuccess> joinSuccess;
    private final GroupJoinRepository repository;

    private GroupJoinViewModel(GroupJoinRepository groupJoinRepository) {
        this.groupDetails = new MutableLiveData<>();
        this.errors = new SingleLiveEvent();
        this.joinErrors = new SingleLiveEvent();
        MediatorLiveData mediatorLiveData = new MediatorLiveData();
        this.busy = mediatorLiveData;
        this.joinSuccess = new SingleLiveEvent();
        this.repository = groupJoinRepository;
        mediatorLiveData.setValue(Boolean.TRUE);
        groupJoinRepository.getGroupDetails(new AsynchronousCallback.WorkerThread<GroupDetails, FetchGroupDetailsError>() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinViewModel.1
            public void onComplete(GroupDetails groupDetails) {
                GroupJoinViewModel.this.busy.postValue(Boolean.FALSE);
                GroupJoinViewModel.this.groupDetails.postValue(groupDetails);
            }

            public void onError(FetchGroupDetailsError fetchGroupDetailsError) {
                GroupJoinViewModel.this.busy.postValue(Boolean.FALSE);
                GroupJoinViewModel.this.errors.postValue(fetchGroupDetailsError);
            }
        });
    }

    public void join(GroupDetails groupDetails) {
        this.busy.setValue(Boolean.TRUE);
        this.repository.joinGroup(groupDetails, new AsynchronousCallback.WorkerThread<JoinGroupSuccess, JoinGroupError>() { // from class: org.thoughtcrime.securesms.groups.ui.invitesandrequests.joining.GroupJoinViewModel.2
            public void onComplete(JoinGroupSuccess joinGroupSuccess) {
                GroupJoinViewModel.this.busy.postValue(Boolean.FALSE);
                GroupJoinViewModel.this.joinSuccess.postValue(joinGroupSuccess);
            }

            public void onError(JoinGroupError joinGroupError) {
                GroupJoinViewModel.this.busy.postValue(Boolean.FALSE);
                GroupJoinViewModel.this.joinErrors.postValue(joinGroupError);
            }
        });
    }

    public LiveData<GroupDetails> getGroupDetails() {
        return this.groupDetails;
    }

    public LiveData<JoinGroupSuccess> getJoinSuccess() {
        return this.joinSuccess;
    }

    public LiveData<Boolean> isBusy() {
        return this.busy;
    }

    public LiveData<FetchGroupDetailsError> getErrors() {
        return this.errors;
    }

    public LiveData<JoinGroupError> getJoinErrors() {
        return this.joinErrors;
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final Context context;
        private final GroupInviteLinkUrl groupInviteLinkUrl;

        public Factory(Context context, GroupInviteLinkUrl groupInviteLinkUrl) {
            this.context = context;
            this.groupInviteLinkUrl = groupInviteLinkUrl;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return new GroupJoinViewModel(new GroupJoinRepository(this.context.getApplicationContext(), this.groupInviteLinkUrl));
        }
    }
}
