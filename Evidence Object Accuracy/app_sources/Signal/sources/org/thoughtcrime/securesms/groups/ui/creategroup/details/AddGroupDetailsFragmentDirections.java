package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.mediasend.Media;

/* loaded from: classes4.dex */
public class AddGroupDetailsFragmentDirections {
    private AddGroupDetailsFragmentDirections() {
    }

    public static ActionAddGroupDetailsFragmentToAvatarPicker actionAddGroupDetailsFragmentToAvatarPicker(ParcelableGroupId parcelableGroupId, Media media) {
        return new ActionAddGroupDetailsFragmentToAvatarPicker(parcelableGroupId, media);
    }

    /* loaded from: classes4.dex */
    public static class ActionAddGroupDetailsFragmentToAvatarPicker implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_addGroupDetailsFragment_to_avatar_picker;
        }

        private ActionAddGroupDetailsFragmentToAvatarPicker(ParcelableGroupId parcelableGroupId, Media media) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("group_id", parcelableGroupId);
            hashMap.put("group_avatar_media", media);
        }

        public ActionAddGroupDetailsFragmentToAvatarPicker setGroupId(ParcelableGroupId parcelableGroupId) {
            this.arguments.put("group_id", parcelableGroupId);
            return this;
        }

        public ActionAddGroupDetailsFragmentToAvatarPicker setIsNewGroup(boolean z) {
            this.arguments.put("is_new_group", Boolean.valueOf(z));
            return this;
        }

        public ActionAddGroupDetailsFragmentToAvatarPicker setGroupAvatarMedia(Media media) {
            this.arguments.put("group_avatar_media", media);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("group_id")) {
                ParcelableGroupId parcelableGroupId = (ParcelableGroupId) this.arguments.get("group_id");
                if (Parcelable.class.isAssignableFrom(ParcelableGroupId.class) || parcelableGroupId == null) {
                    bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelableGroupId));
                } else if (Serializable.class.isAssignableFrom(ParcelableGroupId.class)) {
                    bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelableGroupId));
                } else {
                    throw new UnsupportedOperationException(ParcelableGroupId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("is_new_group")) {
                bundle.putBoolean("is_new_group", ((Boolean) this.arguments.get("is_new_group")).booleanValue());
            } else {
                bundle.putBoolean("is_new_group", false);
            }
            if (this.arguments.containsKey("group_avatar_media")) {
                Media media = (Media) this.arguments.get("group_avatar_media");
                if (Parcelable.class.isAssignableFrom(Media.class) || media == null) {
                    bundle.putParcelable("group_avatar_media", (Parcelable) Parcelable.class.cast(media));
                } else if (Serializable.class.isAssignableFrom(Media.class)) {
                    bundle.putSerializable("group_avatar_media", (Serializable) Serializable.class.cast(media));
                } else {
                    throw new UnsupportedOperationException(Media.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public ParcelableGroupId getGroupId() {
            return (ParcelableGroupId) this.arguments.get("group_id");
        }

        public boolean getIsNewGroup() {
            return ((Boolean) this.arguments.get("is_new_group")).booleanValue();
        }

        public Media getGroupAvatarMedia() {
            return (Media) this.arguments.get("group_avatar_media");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionAddGroupDetailsFragmentToAvatarPicker actionAddGroupDetailsFragmentToAvatarPicker = (ActionAddGroupDetailsFragmentToAvatarPicker) obj;
            if (this.arguments.containsKey("group_id") != actionAddGroupDetailsFragmentToAvatarPicker.arguments.containsKey("group_id")) {
                return false;
            }
            if (getGroupId() == null ? actionAddGroupDetailsFragmentToAvatarPicker.getGroupId() != null : !getGroupId().equals(actionAddGroupDetailsFragmentToAvatarPicker.getGroupId())) {
                return false;
            }
            if (this.arguments.containsKey("is_new_group") != actionAddGroupDetailsFragmentToAvatarPicker.arguments.containsKey("is_new_group") || getIsNewGroup() != actionAddGroupDetailsFragmentToAvatarPicker.getIsNewGroup() || this.arguments.containsKey("group_avatar_media") != actionAddGroupDetailsFragmentToAvatarPicker.arguments.containsKey("group_avatar_media")) {
                return false;
            }
            if (getGroupAvatarMedia() == null ? actionAddGroupDetailsFragmentToAvatarPicker.getGroupAvatarMedia() == null : getGroupAvatarMedia().equals(actionAddGroupDetailsFragmentToAvatarPicker.getGroupAvatarMedia())) {
                return getActionId() == actionAddGroupDetailsFragmentToAvatarPicker.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((getGroupId() != null ? getGroupId().hashCode() : 0) + 31) * 31) + (getIsNewGroup() ? 1 : 0)) * 31;
            if (getGroupAvatarMedia() != null) {
                i = getGroupAvatarMedia().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionAddGroupDetailsFragmentToAvatarPicker(actionId=" + getActionId() + "){groupId=" + getGroupId() + ", isNewGroup=" + getIsNewGroup() + ", groupAvatarMedia=" + getGroupAvatarMedia() + "}";
        }
    }
}
