package org.thoughtcrime.securesms.groups;

import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;

/* loaded from: classes4.dex */
public final class GroupMutation {
    private final DecryptedGroupChange groupChange;
    private final DecryptedGroup newGroupState;
    private final DecryptedGroup previousGroupState;

    public GroupMutation(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange, DecryptedGroup decryptedGroup2) {
        this.previousGroupState = decryptedGroup;
        this.groupChange = decryptedGroupChange;
        this.newGroupState = decryptedGroup2;
    }

    public DecryptedGroup getPreviousGroupState() {
        return this.previousGroupState;
    }

    public DecryptedGroupChange getGroupChange() {
        return this.groupChange;
    }

    public DecryptedGroup getNewGroupState() {
        return this.newGroupState;
    }
}
