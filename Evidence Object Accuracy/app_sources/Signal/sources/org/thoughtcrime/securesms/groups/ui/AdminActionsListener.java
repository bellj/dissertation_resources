package org.thoughtcrime.securesms.groups.ui;

import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;

/* loaded from: classes4.dex */
public interface AdminActionsListener {
    void onApproveRequest(GroupMemberEntry.RequestingMember requestingMember);

    void onDenyRequest(GroupMemberEntry.RequestingMember requestingMember);

    void onRevokeAllInvites(GroupMemberEntry.UnknownPendingMemberCount unknownPendingMemberCount);

    void onRevokeInvite(GroupMemberEntry.PendingMember pendingMember);
}
