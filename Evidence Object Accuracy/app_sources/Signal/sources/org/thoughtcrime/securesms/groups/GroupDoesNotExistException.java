package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupDoesNotExistException extends GroupChangeException {
    public GroupDoesNotExistException(Throwable th) {
        super(th);
    }
}
