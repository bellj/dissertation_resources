package org.thoughtcrime.securesms.groups.v2;

import java.util.Arrays;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class GroupLinkPassword {
    private static final int SIZE;
    private final byte[] bytes;

    public static GroupLinkPassword createNew() {
        return new GroupLinkPassword(Util.getSecretBytes(16));
    }

    public static GroupLinkPassword fromBytes(byte[] bArr) {
        return new GroupLinkPassword(bArr);
    }

    private GroupLinkPassword(byte[] bArr) {
        this.bytes = bArr;
    }

    public byte[] serialize() {
        return (byte[]) this.bytes.clone();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GroupLinkPassword)) {
            return false;
        }
        return Arrays.equals(this.bytes, ((GroupLinkPassword) obj).bytes);
    }

    public int hashCode() {
        return Arrays.hashCode(this.bytes);
    }
}
