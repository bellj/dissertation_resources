package org.thoughtcrime.securesms.groups;

import android.content.res.Resources;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import com.annimon.stream.ComparatorCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import j$.util.function.Function;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.storageservice.protos.groups.AccessControl;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedRequestingMember;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda5;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.v2.GroupInviteLinkUrl;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.whispersystems.signalservice.api.push.ServiceId;

/* loaded from: classes4.dex */
public final class LiveGroup {
    private static final Comparator<GroupMemberEntry.FullMember> ADMIN_FIRST;
    private static final Comparator<GroupMemberEntry.FullMember> ALPHABETICAL;
    private static final Comparator<GroupMemberEntry.FullMember> HAS_DISPLAY_NAME;
    private static final Comparator<GroupMemberEntry.FullMember> LOCAL_FIRST;
    private static final Comparator<? super GroupMemberEntry.FullMember> MEMBER_ORDER;
    private final LiveData<List<GroupMemberEntry.FullMember>> fullMembers;
    private final GroupDatabase groupDatabase = SignalDatabase.groups();
    private final LiveData<GroupLinkUrlAndStatus> groupLink;
    private final LiveData<GroupDatabase.GroupRecord> groupRecord;
    private final LiveData<Recipient> recipient;
    private final LiveData<List<GroupMemberEntry.RequestingMember>> requestingMembers;

    static {
        LiveGroup$$ExternalSyntheticLambda10 liveGroup$$ExternalSyntheticLambda10 = new Comparator() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda10
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return LiveGroup.lambda$static$0((GroupMemberEntry.FullMember) obj, (GroupMemberEntry.FullMember) obj2);
            }
        };
        LOCAL_FIRST = liveGroup$$ExternalSyntheticLambda10;
        LiveGroup$$ExternalSyntheticLambda11 liveGroup$$ExternalSyntheticLambda11 = new Comparator() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda11
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return LiveGroup.lambda$static$1((GroupMemberEntry.FullMember) obj, (GroupMemberEntry.FullMember) obj2);
            }
        };
        ADMIN_FIRST = liveGroup$$ExternalSyntheticLambda11;
        LiveGroup$$ExternalSyntheticLambda12 liveGroup$$ExternalSyntheticLambda12 = new Comparator() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda12
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return LiveGroup.lambda$static$2((GroupMemberEntry.FullMember) obj, (GroupMemberEntry.FullMember) obj2);
            }
        };
        HAS_DISPLAY_NAME = liveGroup$$ExternalSyntheticLambda12;
        LiveGroup$$ExternalSyntheticLambda13 liveGroup$$ExternalSyntheticLambda13 = new Comparator() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda13
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return LiveGroup.lambda$static$3((GroupMemberEntry.FullMember) obj, (GroupMemberEntry.FullMember) obj2);
            }
        };
        ALPHABETICAL = liveGroup$$ExternalSyntheticLambda13;
        MEMBER_ORDER = ComparatorCompat.chain(liveGroup$$ExternalSyntheticLambda10).thenComparing((Comparator) liveGroup$$ExternalSyntheticLambda11).thenComparing((Comparator) liveGroup$$ExternalSyntheticLambda12).thenComparing((Comparator) liveGroup$$ExternalSyntheticLambda13);
    }

    public static /* synthetic */ int lambda$static$0(GroupMemberEntry.FullMember fullMember, GroupMemberEntry.FullMember fullMember2) {
        return Boolean.compare(fullMember2.getMember().isSelf(), fullMember.getMember().isSelf());
    }

    public static /* synthetic */ int lambda$static$1(GroupMemberEntry.FullMember fullMember, GroupMemberEntry.FullMember fullMember2) {
        return Boolean.compare(fullMember2.isAdmin(), fullMember.isAdmin());
    }

    public static /* synthetic */ int lambda$static$2(GroupMemberEntry.FullMember fullMember, GroupMemberEntry.FullMember fullMember2) {
        return Boolean.compare(fullMember2.getMember().hasAUserSetDisplayName(ApplicationDependencies.getApplication()), fullMember.getMember().hasAUserSetDisplayName(ApplicationDependencies.getApplication()));
    }

    public static /* synthetic */ int lambda$static$3(GroupMemberEntry.FullMember fullMember, GroupMemberEntry.FullMember fullMember2) {
        return fullMember.getMember().getDisplayName(ApplicationDependencies.getApplication()).compareToIgnoreCase(fullMember2.getMember().getDisplayName(ApplicationDependencies.getApplication()));
    }

    public LiveGroup(GroupId groupId) {
        ApplicationDependencies.getApplication();
        MutableLiveData mutableLiveData = new MutableLiveData();
        LiveData<Recipient> switchMap = Transformations.switchMap(mutableLiveData, new WebRtcCallViewModel$$ExternalSyntheticLambda5());
        this.recipient = switchMap;
        LiveData<GroupDatabase.GroupRecord> filterNotNull = LiveDataUtil.filterNotNull(LiveDataUtil.mapAsync(switchMap, new Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda22
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return LiveGroup.this.lambda$new$4((Recipient) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }));
        this.groupRecord = filterNotNull;
        this.fullMembers = mapToFullMembers(filterNotNull);
        this.requestingMembers = mapToRequestingMembers(filterNotNull);
        if (groupId.isV2()) {
            this.groupLink = Transformations.map(Transformations.map(filterNotNull, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda23
                @Override // androidx.arch.core.util.Function
                public final Object apply(Object obj) {
                    return ((GroupDatabase.GroupRecord) obj).requireV2GroupProperties();
                }
            }), new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda24
                @Override // androidx.arch.core.util.Function
                public final Object apply(Object obj) {
                    return LiveGroup.lambda$new$5((GroupDatabase.V2GroupProperties) obj);
                }
            });
        } else {
            this.groupLink = new MutableLiveData(GroupLinkUrlAndStatus.NONE);
        }
        SignalExecutors.BOUNDED.execute(new Runnable(groupId) { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda25
            public final /* synthetic */ GroupId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                LiveGroup.lambda$new$6(MutableLiveData.this, this.f$1);
            }
        });
    }

    public /* synthetic */ GroupDatabase.GroupRecord lambda$new$4(Recipient recipient) {
        return this.groupDatabase.getGroup(recipient.getId()).orElse(null);
    }

    public static /* synthetic */ GroupLinkUrlAndStatus lambda$new$5(GroupDatabase.V2GroupProperties v2GroupProperties) {
        DecryptedGroup decryptedGroup = v2GroupProperties.getDecryptedGroup();
        AccessControl.AccessRequired addFromInviteLink = decryptedGroup.getAccessControl().getAddFromInviteLink();
        if (decryptedGroup.getInviteLinkPassword().isEmpty()) {
            return GroupLinkUrlAndStatus.NONE;
        }
        boolean z = false;
        boolean z2 = addFromInviteLink == AccessControl.AccessRequired.ANY || addFromInviteLink == AccessControl.AccessRequired.ADMINISTRATOR;
        if (addFromInviteLink == AccessControl.AccessRequired.ADMINISTRATOR) {
            z = true;
        }
        return new GroupLinkUrlAndStatus(z2, z, GroupInviteLinkUrl.forGroup(v2GroupProperties.getGroupMasterKey(), decryptedGroup).getUrl());
    }

    public static /* synthetic */ void lambda$new$6(MutableLiveData mutableLiveData, GroupId groupId) {
        mutableLiveData.postValue(Recipient.externalGroupExact(groupId).live());
    }

    protected static LiveData<List<GroupMemberEntry.FullMember>> mapToFullMembers(LiveData<GroupDatabase.GroupRecord> liveData) {
        return LiveDataUtil.mapAsync(liveData, new Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$mapToFullMembers$8((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public static /* synthetic */ List lambda$mapToFullMembers$8(GroupDatabase.GroupRecord groupRecord) {
        return Stream.of(groupRecord.getMembers()).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$mapToFullMembers$7(GroupDatabase.GroupRecord.this, (RecipientId) obj);
            }
        }).sorted(MEMBER_ORDER).toList();
    }

    public static /* synthetic */ GroupMemberEntry.FullMember lambda$mapToFullMembers$7(GroupDatabase.GroupRecord groupRecord, RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        return new GroupMemberEntry.FullMember(resolved, groupRecord.isAdmin(resolved));
    }

    protected static LiveData<List<GroupMemberEntry.RequestingMember>> mapToRequestingMembers(LiveData<GroupDatabase.GroupRecord> liveData) {
        return LiveDataUtil.mapAsync(liveData, new Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$mapToRequestingMembers$10((GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public static /* synthetic */ List lambda$mapToRequestingMembers$10(GroupDatabase.GroupRecord groupRecord) {
        if (!groupRecord.isV2Group()) {
            return Collections.emptyList();
        }
        return Stream.of(groupRecord.requireV2GroupProperties().getDecryptedGroup().getRequestingMembersList()).map(new com.annimon.stream.function.Function(groupRecord.isAdmin(Recipient.self())) { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda20
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$mapToRequestingMembers$9(this.f$0, (DecryptedRequestingMember) obj);
            }
        }).toList();
    }

    public static /* synthetic */ GroupMemberEntry.RequestingMember lambda$mapToRequestingMembers$9(boolean z, DecryptedRequestingMember decryptedRequestingMember) {
        return new GroupMemberEntry.RequestingMember(Recipient.externalPush(ServiceId.fromByteString(decryptedRequestingMember.getUuid())), z);
    }

    public LiveData<String> getTitle() {
        return LiveDataUtil.combineLatest(this.groupRecord, this.recipient, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda18
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return LiveGroup.lambda$getTitle$11((GroupDatabase.GroupRecord) obj, (Recipient) obj2);
            }
        });
    }

    public static /* synthetic */ String lambda$getTitle$11(GroupDatabase.GroupRecord groupRecord, Recipient recipient) {
        String title = groupRecord.getTitle();
        if (!TextUtils.isEmpty(title)) {
            return title;
        }
        return recipient.getDisplayName(ApplicationDependencies.getApplication());
    }

    public LiveData<String> getDescription() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda9
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).getDescription();
            }
        });
    }

    public LiveData<Boolean> isAnnouncementGroup() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda3
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(((GroupDatabase.GroupRecord) obj).isAnnouncementGroup());
            }
        });
    }

    public LiveData<Recipient> getGroupRecipient() {
        return this.recipient;
    }

    public static /* synthetic */ Boolean lambda$isSelfAdmin$12(GroupDatabase.GroupRecord groupRecord) {
        return Boolean.valueOf(groupRecord.isAdmin(Recipient.self()));
    }

    public LiveData<Boolean> isSelfAdmin() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda27
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$isSelfAdmin$12((GroupDatabase.GroupRecord) obj);
            }
        });
    }

    public static /* synthetic */ Set lambda$getBannedMembers$13(GroupDatabase.GroupRecord groupRecord) {
        return groupRecord.isV2Group() ? groupRecord.requireV2GroupProperties().getBannedMembers() : Collections.emptySet();
    }

    public LiveData<Set<UUID>> getBannedMembers() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda5
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$getBannedMembers$13((GroupDatabase.GroupRecord) obj);
            }
        });
    }

    public LiveData<Boolean> isActive() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda29
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(((GroupDatabase.GroupRecord) obj).isActive());
            }
        });
    }

    public static /* synthetic */ Boolean lambda$getRecipientIsAdmin$14(RecipientId recipientId, GroupDatabase.GroupRecord groupRecord) {
        return Boolean.valueOf(groupRecord.isAdmin(Recipient.resolved(recipientId)));
    }

    public LiveData<Boolean> getRecipientIsAdmin(RecipientId recipientId) {
        return LiveDataUtil.mapAsync(this.groupRecord, new Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda26
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$getRecipientIsAdmin$14(RecipientId.this, (GroupDatabase.GroupRecord) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
    }

    public static /* synthetic */ Integer lambda$getPendingMemberCount$15(GroupDatabase.GroupRecord groupRecord) {
        return Integer.valueOf(groupRecord.isV2Group() ? groupRecord.requireV2GroupProperties().getDecryptedGroup().getPendingMembersCount() : 0);
    }

    public LiveData<Integer> getPendingMemberCount() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$getPendingMemberCount$15((GroupDatabase.GroupRecord) obj);
            }
        });
    }

    public LiveData<Integer> getPendingAndRequestingMemberCount() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda19
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$getPendingAndRequestingMemberCount$16((GroupDatabase.GroupRecord) obj);
            }
        });
    }

    public static /* synthetic */ Integer lambda$getPendingAndRequestingMemberCount$16(GroupDatabase.GroupRecord groupRecord) {
        if (!groupRecord.isV2Group()) {
            return 0;
        }
        DecryptedGroup decryptedGroup = groupRecord.requireV2GroupProperties().getDecryptedGroup();
        return Integer.valueOf(decryptedGroup.getPendingMembersCount() + decryptedGroup.getRequestingMembersCount());
    }

    public LiveData<GroupAccessControl> getMembershipAdditionAccessControl() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda28
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).getMembershipAdditionAccessControl();
            }
        });
    }

    public LiveData<GroupAccessControl> getAttributesAccessControl() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda6
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).getAttributesAccessControl();
            }
        });
    }

    public LiveData<List<GroupMemberEntry.FullMember>> getNonAdminFullMembers() {
        return Transformations.map(this.fullMembers, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$getNonAdminFullMembers$17((List) obj);
            }
        });
    }

    public static /* synthetic */ List lambda$getNonAdminFullMembers$17(List list) {
        return Stream.of(list).filterNot(new Predicate() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((GroupMemberEntry.FullMember) obj).isAdmin();
            }
        }).toList();
    }

    public LiveData<List<GroupMemberEntry.FullMember>> getFullMembers() {
        return this.fullMembers;
    }

    public LiveData<List<GroupMemberEntry.RequestingMember>> getRequestingMembers() {
        return this.requestingMembers;
    }

    public LiveData<Integer> getExpireMessages() {
        return Transformations.map(this.recipient, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda30
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Integer.valueOf(((Recipient) obj).getExpiresInSeconds());
            }
        });
    }

    public LiveData<Boolean> selfCanEditGroupAttributes() {
        return LiveDataUtil.combineLatest(selfMemberLevel(), getAttributesAccessControl(), new LiveGroup$$ExternalSyntheticLambda15());
    }

    public LiveData<Boolean> selfCanAddMembers() {
        return LiveDataUtil.combineLatest(selfMemberLevel(), getMembershipAdditionAccessControl(), new LiveGroup$$ExternalSyntheticLambda15());
    }

    public LiveData<String> getMembershipCountDescription(Resources resources) {
        return LiveDataUtil.combineLatest(getFullMembers(), getPendingMemberCount(), new LiveDataUtil.Combine(resources) { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda4
            public final /* synthetic */ Resources f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return LiveGroup.lambda$getMembershipCountDescription$18(this.f$0, (List) obj, (Integer) obj2);
            }
        });
    }

    public static /* synthetic */ String lambda$getMembershipCountDescription$18(Resources resources, List list, Integer num) {
        return getMembershipDescription(resources, num.intValue(), list.size());
    }

    public static /* synthetic */ String lambda$getFullMembershipCountDescription$19(Resources resources, List list) {
        return getMembershipDescription(resources, 0, list.size());
    }

    public LiveData<String> getFullMembershipCountDescription(Resources resources) {
        return Transformations.map(getFullMembers(), new androidx.arch.core.util.Function(resources) { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda21
            public final /* synthetic */ Resources f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$getFullMembershipCountDescription$19(this.f$0, (List) obj);
            }
        });
    }

    public LiveData<GroupDatabase.MemberLevel> getMemberLevel(Recipient recipient) {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda14
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).memberLevel(Recipient.this);
            }
        });
    }

    private static String getMembershipDescription(Resources resources, int i, int i2) {
        if (i > 0) {
            return resources.getQuantityString(R.plurals.MessageRequestProfileView_members_and_invited, i2, Integer.valueOf(i2), resources.getQuantityString(R.plurals.MessageRequestProfileView_invited, i, Integer.valueOf(i)));
        }
        return resources.getQuantityString(R.plurals.MessageRequestProfileView_members, i2, Integer.valueOf(i2));
    }

    public static /* synthetic */ GroupDatabase.MemberLevel lambda$selfMemberLevel$21(GroupDatabase.GroupRecord groupRecord) {
        return groupRecord.memberLevel(Recipient.self());
    }

    private LiveData<GroupDatabase.MemberLevel> selfMemberLevel() {
        return Transformations.map(this.groupRecord, new androidx.arch.core.util.Function() { // from class: org.thoughtcrime.securesms.groups.LiveGroup$$ExternalSyntheticLambda17
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return LiveGroup.lambda$selfMemberLevel$21((GroupDatabase.GroupRecord) obj);
            }
        });
    }

    /* renamed from: org.thoughtcrime.securesms.groups.LiveGroup$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl;

        static {
            int[] iArr = new int[GroupAccessControl.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl = iArr;
            try {
                iArr[GroupAccessControl.ALL_MEMBERS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl[GroupAccessControl.ONLY_ADMINS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl[GroupAccessControl.NO_ONE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public static boolean applyAccessControl(GroupDatabase.MemberLevel memberLevel, GroupAccessControl groupAccessControl) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$groups$GroupAccessControl[groupAccessControl.ordinal()];
        if (i == 1) {
            return memberLevel.isInGroup();
        }
        if (i != 2) {
            if (i == 3) {
                return false;
            }
            throw new AssertionError();
        } else if (memberLevel == GroupDatabase.MemberLevel.ADMINISTRATOR) {
            return true;
        } else {
            return false;
        }
    }

    public LiveData<GroupLinkUrlAndStatus> getGroupLink() {
        return this.groupLink;
    }
}
