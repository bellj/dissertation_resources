package org.thoughtcrime.securesms.groups;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes4.dex */
public final class ParcelableGroupId implements Parcelable {
    public static final Parcelable.Creator<ParcelableGroupId> CREATOR = new Parcelable.Creator<ParcelableGroupId>() { // from class: org.thoughtcrime.securesms.groups.ParcelableGroupId.1
        @Override // android.os.Parcelable.Creator
        public ParcelableGroupId createFromParcel(Parcel parcel) {
            return new ParcelableGroupId(GroupId.parseNullableOrThrow(parcel.readString()));
        }

        @Override // android.os.Parcelable.Creator
        public ParcelableGroupId[] newArray(int i) {
            return new ParcelableGroupId[i];
        }
    };
    private final GroupId groupId;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public static Parcelable from(GroupId groupId) {
        return new ParcelableGroupId(groupId);
    }

    public static GroupId get(ParcelableGroupId parcelableGroupId) {
        if (parcelableGroupId == null) {
            return null;
        }
        return parcelableGroupId.groupId;
    }

    ParcelableGroupId(GroupId groupId) {
        this.groupId = groupId;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        GroupId groupId = this.groupId;
        if (groupId != null) {
            parcel.writeString(groupId.toString());
        } else {
            parcel.writeString(null);
        }
    }
}
