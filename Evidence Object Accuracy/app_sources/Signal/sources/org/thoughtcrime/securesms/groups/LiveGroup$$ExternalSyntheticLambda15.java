package org.thoughtcrime.securesms.groups;

import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LiveGroup$$ExternalSyntheticLambda15 implements LiveDataUtil.Combine {
    @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
    public final Object apply(Object obj, Object obj2) {
        return Boolean.valueOf(LiveGroup.applyAccessControl((GroupDatabase.MemberLevel) obj, (GroupAccessControl) obj2));
    }
}
