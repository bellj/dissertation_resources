package org.thoughtcrime.securesms.groups.v2.processing;

import com.annimon.stream.function.Function;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda2 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return GroupsV2StateProcessor.ProfileAndMessageHelper.lambda$determineProfileSharing$2((DecryptedGroupChange) obj);
    }
}
