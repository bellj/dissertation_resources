package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public abstract class GroupChangeException extends Exception {
    public GroupChangeException() {
    }

    public GroupChangeException(Throwable th) {
        super(th);
    }

    public GroupChangeException(String str) {
        super(str);
    }
}
