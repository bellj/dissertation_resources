package org.thoughtcrime.securesms.groups.v2.processing;

import j$.util.function.Predicate;
import java.util.UUID;
import org.whispersystems.signalservice.api.push.ServiceIds;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda4 implements Predicate {
    public final /* synthetic */ ServiceIds f$0;

    public /* synthetic */ GroupsV2StateProcessor$StateProcessorForGroup$$ExternalSyntheticLambda4(ServiceIds serviceIds) {
        this.f$0 = serviceIds;
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate and(Predicate predicate) {
        return Predicate.CC.$default$and(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate negate() {
        return Predicate.CC.$default$negate(this);
    }

    @Override // j$.util.function.Predicate
    public /* synthetic */ Predicate or(Predicate predicate) {
        return Predicate.CC.$default$or(this, predicate);
    }

    @Override // j$.util.function.Predicate
    public final boolean test(Object obj) {
        return this.f$0.matches((UUID) obj);
    }
}
