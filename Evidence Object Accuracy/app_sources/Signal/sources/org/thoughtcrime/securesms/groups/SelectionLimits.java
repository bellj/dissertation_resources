package org.thoughtcrime.securesms.groups;

import android.os.Parcel;
import android.os.Parcelable;

/* loaded from: classes4.dex */
public final class SelectionLimits implements Parcelable {
    public static final Parcelable.Creator<SelectionLimits> CREATOR = new Parcelable.Creator<SelectionLimits>() { // from class: org.thoughtcrime.securesms.groups.SelectionLimits.1
        @Override // android.os.Parcelable.Creator
        public SelectionLimits createFromParcel(Parcel parcel) {
            return new SelectionLimits(parcel.readInt(), parcel.readInt());
        }

        @Override // android.os.Parcelable.Creator
        public SelectionLimits[] newArray(int i) {
            return new SelectionLimits[i];
        }
    };
    private static final int NO_LIMIT;
    public static final SelectionLimits NO_LIMITS = new SelectionLimits(Integer.MAX_VALUE, Integer.MAX_VALUE);
    private final int hardLimit;
    private final int recommendedLimit;

    @Override // android.os.Parcelable
    public int describeContents() {
        return 0;
    }

    public SelectionLimits(int i, int i2) {
        this.recommendedLimit = i;
        this.hardLimit = i2;
    }

    public int getRecommendedLimit() {
        return this.recommendedLimit;
    }

    public int getHardLimit() {
        return this.hardLimit;
    }

    public boolean hasRecommendedLimit() {
        return this.recommendedLimit != Integer.MAX_VALUE;
    }

    public boolean hasHardLimit() {
        return this.hardLimit != Integer.MAX_VALUE;
    }

    @Override // android.os.Parcelable
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.recommendedLimit);
        parcel.writeInt(this.hardLimit);
    }

    public SelectionLimits excludingSelf() {
        return excluding(1);
    }

    public SelectionLimits excluding(int i) {
        return new SelectionLimits(this.recommendedLimit - i, this.hardLimit - i);
    }
}
