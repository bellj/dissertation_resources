package org.thoughtcrime.securesms.groups.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import java.util.Objects;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class PopupMenuView extends View {
    private ItemClick callback;
    private int menu;
    private PrepareOptionsMenuItem prepareOptionsMenuItemCallback;

    /* loaded from: classes4.dex */
    public interface ItemClick {
        boolean onItemClick(int i);
    }

    /* loaded from: classes4.dex */
    public interface PrepareOptionsMenuItem {
        boolean onPrepareOptionsMenuItem(MenuItem menuItem);
    }

    public PopupMenuView(Context context) {
        super(context);
        init(null);
    }

    public PopupMenuView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public PopupMenuView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        setBackgroundResource(R.drawable.ic_more_vert_24);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.PopupMenuView, 0, 0);
            int color = obtainStyledAttributes.getColor(0, -16777216);
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_more_vert_24);
            Objects.requireNonNull(drawable);
            DrawableCompat.setTint(drawable, color);
            setBackground(drawable);
            obtainStyledAttributes.recycle();
        }
        setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.PopupMenuView$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PopupMenuView.this.lambda$init$1(view);
            }
        });
    }

    public /* synthetic */ void lambda$init$1(View view) {
        if (this.callback != null) {
            PopupMenu popupMenu = new PopupMenu(getContext(), view);
            popupMenu.getMenuInflater().inflate(this.menu, popupMenu.getMenu());
            if (this.prepareOptionsMenuItemCallback != null) {
                Menu menu = popupMenu.getMenu();
                for (int size = menu.size() - 1; size >= 0; size--) {
                    MenuItem item = menu.getItem(size);
                    if (!this.prepareOptionsMenuItemCallback.onPrepareOptionsMenuItem(item)) {
                        menu.removeItem(item.getItemId());
                    }
                }
            }
            popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.PopupMenuView$$ExternalSyntheticLambda0
                @Override // androidx.appcompat.widget.PopupMenu.OnMenuItemClickListener
                public final boolean onMenuItemClick(MenuItem menuItem) {
                    return PopupMenuView.this.lambda$init$0(menuItem);
                }
            });
            popupMenu.show();
        }
    }

    public /* synthetic */ boolean lambda$init$0(MenuItem menuItem) {
        return this.callback.onItemClick(menuItem.getItemId());
    }

    public void setMenu(int i, ItemClick itemClick) {
        this.menu = i;
        this.prepareOptionsMenuItemCallback = null;
        this.callback = itemClick;
    }

    public void setMenu(int i, PrepareOptionsMenuItem prepareOptionsMenuItem, ItemClick itemClick) {
        this.menu = i;
        this.prepareOptionsMenuItemCallback = prepareOptionsMenuItem;
        this.callback = itemClick;
    }
}
