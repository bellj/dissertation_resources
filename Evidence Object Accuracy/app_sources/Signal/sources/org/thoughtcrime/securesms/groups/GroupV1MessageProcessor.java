package org.thoughtcrime.securesms.groups;

import android.content.Context;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.MmsDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.SmsDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.jobs.AvatarGroupsV1DownloadJob;
import org.thoughtcrime.securesms.jobs.PushGroupUpdateJob;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.MmsException;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.IncomingGroupUpdateMessage;
import org.thoughtcrime.securesms.sms.IncomingTextMessage;
import org.thoughtcrime.securesms.util.Base64;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachment;
import org.whispersystems.signalservice.api.messages.SignalServiceAttachmentPointer;
import org.whispersystems.signalservice.api.messages.SignalServiceContent;
import org.whispersystems.signalservice.api.messages.SignalServiceDataMessage;
import org.whispersystems.signalservice.api.messages.SignalServiceGroup;
import org.whispersystems.signalservice.api.messages.SignalServiceGroupContext;
import org.whispersystems.signalservice.api.push.SignalServiceAddress;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class GroupV1MessageProcessor {
    private static final String TAG = Log.tag(GroupV1MessageProcessor.class);

    public static Long process(Context context, SignalServiceContent signalServiceContent, SignalServiceDataMessage signalServiceDataMessage, boolean z) throws BadGroupIdException {
        SignalServiceGroupContext signalServiceGroupContext = signalServiceDataMessage.getGroupContext().get();
        Optional<SignalServiceGroup> groupV1 = signalServiceGroupContext.getGroupV1();
        if (signalServiceGroupContext.getGroupV2().isPresent()) {
            throw new AssertionError("Cannot process GV2");
        } else if (!groupV1.isPresent() || groupV1.get().getGroupId() == null) {
            Log.w(TAG, "Received group message with no id! Ignoring...");
            return null;
        } else {
            GroupDatabase groups = SignalDatabase.groups();
            SignalServiceGroup signalServiceGroup = groupV1.get();
            Optional<GroupDatabase.GroupRecord> group = groups.getGroup(GroupId.v1(signalServiceGroup.getGroupId()));
            if (group.isPresent() && signalServiceGroup.getType() == SignalServiceGroup.Type.UPDATE) {
                return handleGroupUpdate(context, signalServiceContent, signalServiceGroup, group.get(), z);
            }
            if (!group.isPresent() && signalServiceGroup.getType() == SignalServiceGroup.Type.UPDATE) {
                return handleGroupCreate(context, signalServiceContent, signalServiceGroup, z);
            }
            if (group.isPresent() && signalServiceGroup.getType() == SignalServiceGroup.Type.QUIT) {
                return handleGroupLeave(context, signalServiceContent, signalServiceGroup, group.get(), z);
            }
            if (group.isPresent() && signalServiceGroup.getType() == SignalServiceGroup.Type.REQUEST_INFO) {
                return handleGroupInfoRequest(context, signalServiceContent, group.get());
            }
            Log.w(TAG, "Received unknown type, ignoring...");
            return null;
        }
    }

    private static Long handleGroupCreate(Context context, SignalServiceContent signalServiceContent, SignalServiceGroup signalServiceGroup, boolean z) {
        GroupDatabase groups = SignalDatabase.groups();
        GroupId.V1 v1orThrow = GroupId.v1orThrow(signalServiceGroup.getGroupId());
        SignalServiceProtos.GroupContext.Builder createGroupContext = createGroupContext(signalServiceGroup);
        createGroupContext.setType(SignalServiceProtos.GroupContext.Type.UPDATE);
        SignalServiceAttachment orElse = signalServiceGroup.getAvatar().orElse(null);
        LinkedList linkedList = new LinkedList();
        if (signalServiceGroup.getMembers().isPresent()) {
            for (SignalServiceAddress signalServiceAddress : signalServiceGroup.getMembers().get()) {
                linkedList.add(Recipient.externalGV1Member(signalServiceAddress).getId());
            }
        }
        groups.create(v1orThrow, signalServiceGroup.getName().orElse(null), linkedList, (orElse == null || !orElse.isPointer()) ? null : orElse.asPointer(), null);
        Recipient externalPush = Recipient.externalPush(signalServiceContent.getSender());
        if (externalPush.isSystemContact() || externalPush.isProfileSharing()) {
            String str = TAG;
            Log.i(str, "Auto-enabling profile sharing because 'adder' is trusted. contact: " + externalPush.isSystemContact() + ", profileSharing: " + externalPush.isProfileSharing());
            SignalDatabase.recipients().setProfileSharing(Recipient.externalGroupExact(v1orThrow).getId(), true);
        }
        return storeMessage(context, signalServiceContent, signalServiceGroup, createGroupContext.build(), z);
    }

    private static Long handleGroupUpdate(Context context, SignalServiceContent signalServiceContent, SignalServiceGroup signalServiceGroup, GroupDatabase.GroupRecord groupRecord, boolean z) {
        GroupDatabase groups = SignalDatabase.groups();
        GroupId.V1 v1orThrow = GroupId.v1orThrow(signalServiceGroup.getGroupId());
        HashSet hashSet = new HashSet(groupRecord.getMembers());
        HashSet hashSet2 = new HashSet();
        if (signalServiceGroup.getMembers().isPresent()) {
            for (SignalServiceAddress signalServiceAddress : signalServiceGroup.getMembers().get()) {
                hashSet2.add(Recipient.externalGV1Member(signalServiceAddress).getId());
            }
        }
        HashSet<RecipientId> hashSet3 = new HashSet(hashSet2);
        hashSet3.removeAll(hashSet);
        HashSet hashSet4 = new HashSet(hashSet);
        hashSet4.removeAll(hashSet2);
        SignalServiceProtos.GroupContext.Builder createGroupContext = createGroupContext(signalServiceGroup);
        createGroupContext.setType(SignalServiceProtos.GroupContext.Type.UPDATE);
        if (hashSet3.size() > 0) {
            HashSet hashSet5 = new HashSet(hashSet);
            hashSet5.addAll(hashSet2);
            groups.updateMembers(v1orThrow, new LinkedList(hashSet5));
            createGroupContext.clearMembers();
            createGroupContext.clearMembersE164();
            for (RecipientId recipientId : hashSet3) {
                Recipient resolved = Recipient.resolved(recipientId);
                if (resolved.getE164().isPresent()) {
                    createGroupContext.addMembersE164(resolved.requireE164());
                    createGroupContext.addMembers(createMember(resolved.requireE164()));
                }
            }
        } else {
            createGroupContext.clearMembers();
            createGroupContext.clearMembersE164();
        }
        hashSet4.size();
        if (signalServiceGroup.getName().isPresent() || signalServiceGroup.getAvatar().isPresent()) {
            SignalServiceAttachmentPointer signalServiceAttachmentPointer = null;
            SignalServiceAttachment orElse = signalServiceGroup.getAvatar().orElse(null);
            String orElse2 = signalServiceGroup.getName().orElse(null);
            if (orElse != null) {
                signalServiceAttachmentPointer = orElse.asPointer();
            }
            groups.update(v1orThrow, orElse2, signalServiceAttachmentPointer);
        }
        if (signalServiceGroup.getName().isPresent() && signalServiceGroup.getName().get().equals(groupRecord.getTitle())) {
            createGroupContext.clearName();
        }
        if (!groupRecord.isActive()) {
            groups.setActive(v1orThrow, true);
        }
        return storeMessage(context, signalServiceContent, signalServiceGroup, createGroupContext.build(), z);
    }

    private static Long handleGroupInfoRequest(Context context, SignalServiceContent signalServiceContent, GroupDatabase.GroupRecord groupRecord) {
        Recipient externalPush = Recipient.externalPush(signalServiceContent.getSender());
        if (!groupRecord.getMembers().contains(externalPush.getId())) {
            return null;
        }
        ApplicationDependencies.getJobManager().add(new PushGroupUpdateJob(externalPush.getId(), groupRecord.getId()));
        return null;
    }

    private static Long handleGroupLeave(Context context, SignalServiceContent signalServiceContent, SignalServiceGroup signalServiceGroup, GroupDatabase.GroupRecord groupRecord, boolean z) {
        GroupDatabase groups = SignalDatabase.groups();
        GroupId.V1 v1orThrow = GroupId.v1orThrow(signalServiceGroup.getGroupId());
        List<RecipientId> members = groupRecord.getMembers();
        SignalServiceProtos.GroupContext.Builder createGroupContext = createGroupContext(signalServiceGroup);
        createGroupContext.setType(SignalServiceProtos.GroupContext.Type.QUIT);
        RecipientId from = RecipientId.from(signalServiceContent.getSender());
        if (!members.contains(from)) {
            return null;
        }
        groups.remove(v1orThrow, from);
        if (z) {
            groups.setActive(v1orThrow, false);
        }
        return storeMessage(context, signalServiceContent, signalServiceGroup, createGroupContext.build(), z);
    }

    private static Long storeMessage(Context context, SignalServiceContent signalServiceContent, SignalServiceGroup signalServiceGroup, SignalServiceProtos.GroupContext groupContext, boolean z) {
        if (signalServiceGroup.getAvatar().isPresent()) {
            ApplicationDependencies.getJobManager().add(new AvatarGroupsV1DownloadJob(GroupId.v1orThrow(signalServiceGroup.getGroupId())));
        }
        try {
            if (z) {
                MmsDatabase mms = SignalDatabase.mms();
                Recipient resolved = Recipient.resolved(SignalDatabase.recipients().getOrInsertFromGroupId(GroupId.v1orThrow(signalServiceGroup.getGroupId())));
                OutgoingGroupUpdateMessage outgoingGroupUpdateMessage = new OutgoingGroupUpdateMessage(resolved, groupContext, (Attachment) null, signalServiceContent.getTimestamp(), 0L, false, (QuoteModel) null, (List<Contact>) Collections.emptyList(), (List<LinkPreview>) Collections.emptyList(), (List<Mention>) Collections.emptyList());
                long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(resolved);
                mms.markAsSent(mms.insertMessageOutbox(outgoingGroupUpdateMessage, orCreateThreadIdFor, false, null), true);
                return Long.valueOf(orCreateThreadIdFor);
            }
            SmsDatabase sms = SignalDatabase.sms();
            String encodeBytes = Base64.encodeBytes(groupContext.toByteArray());
            Optional<MessageDatabase.InsertResult> insertMessageInbox = sms.insertMessageInbox(new IncomingGroupUpdateMessage(new IncomingTextMessage(Recipient.externalPush(signalServiceContent.getSender()).getId(), signalServiceContent.getSenderDevice(), signalServiceContent.getTimestamp(), signalServiceContent.getServerReceivedTimestamp(), System.currentTimeMillis(), encodeBytes, Optional.of(GroupId.v1orThrow(signalServiceGroup.getGroupId())), 0, signalServiceContent.isNeedsReceipt(), signalServiceContent.getServerUuid()), groupContext, encodeBytes));
            if (!insertMessageInbox.isPresent()) {
                return null;
            }
            ApplicationDependencies.getMessageNotifier().updateNotification(context, ConversationId.forConversation(insertMessageInbox.get().getThreadId()));
            return Long.valueOf(insertMessageInbox.get().getThreadId());
        } catch (MmsException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    private static SignalServiceProtos.GroupContext.Builder createGroupContext(SignalServiceGroup signalServiceGroup) {
        SignalServiceProtos.GroupContext.Builder newBuilder = SignalServiceProtos.GroupContext.newBuilder();
        newBuilder.setId(ByteString.copyFrom(signalServiceGroup.getGroupId()));
        if (signalServiceGroup.getAvatar().isPresent() && signalServiceGroup.getAvatar().get().isPointer() && signalServiceGroup.getAvatar().get().asPointer().getRemoteId().getV2().isPresent()) {
            newBuilder.setAvatar(SignalServiceProtos.AttachmentPointer.newBuilder().setCdnId(signalServiceGroup.getAvatar().get().asPointer().getRemoteId().getV2().get().longValue()).setKey(ByteString.copyFrom(signalServiceGroup.getAvatar().get().asPointer().getKey())).setContentType(signalServiceGroup.getAvatar().get().getContentType()));
        }
        if (signalServiceGroup.getName().isPresent()) {
            newBuilder.setName(signalServiceGroup.getName().get());
        }
        if (signalServiceGroup.getMembers().isPresent()) {
            newBuilder.addAllMembersE164(Stream.of(signalServiceGroup.getMembers().get()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.GroupV1MessageProcessor$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return GroupV1MessageProcessor.lambda$createGroupContext$0((SignalServiceAddress) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.groups.GroupV1MessageProcessor$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return GroupV1MessageProcessor.lambda$createGroupContext$1((SignalServiceAddress) obj);
                }
            }).toList());
            newBuilder.addAllMembers(Stream.of(signalServiceGroup.getMembers().get()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.GroupV1MessageProcessor$$ExternalSyntheticLambda2
                @Override // com.annimon.stream.function.Predicate
                public final boolean test(Object obj) {
                    return GroupV1MessageProcessor.lambda$createGroupContext$2((SignalServiceAddress) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.groups.GroupV1MessageProcessor$$ExternalSyntheticLambda3
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return GroupV1MessageProcessor.lambda$createGroupContext$3((SignalServiceAddress) obj);
                }
            }).map(new Function() { // from class: org.thoughtcrime.securesms.groups.GroupV1MessageProcessor$$ExternalSyntheticLambda4
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return GroupV1MessageProcessor.createMember((String) obj);
                }
            }).toList());
        }
        return newBuilder;
    }

    public static /* synthetic */ boolean lambda$createGroupContext$0(SignalServiceAddress signalServiceAddress) {
        return signalServiceAddress.getNumber().isPresent();
    }

    public static /* synthetic */ String lambda$createGroupContext$1(SignalServiceAddress signalServiceAddress) {
        return signalServiceAddress.getNumber().get();
    }

    public static /* synthetic */ boolean lambda$createGroupContext$2(SignalServiceAddress signalServiceAddress) {
        return signalServiceAddress.getNumber().isPresent();
    }

    public static /* synthetic */ String lambda$createGroupContext$3(SignalServiceAddress signalServiceAddress) {
        return signalServiceAddress.getNumber().get();
    }

    public static SignalServiceProtos.GroupContext.Member createMember(String str) {
        SignalServiceProtos.GroupContext.Member.Builder newBuilder = SignalServiceProtos.GroupContext.Member.newBuilder();
        newBuilder.setE164(str);
        return newBuilder.build();
    }
}
