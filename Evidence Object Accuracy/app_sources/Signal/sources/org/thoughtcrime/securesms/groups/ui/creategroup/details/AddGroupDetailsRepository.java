package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import android.content.Context;
import androidx.core.util.Consumer;
import com.annimon.stream.Stream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8;
import org.thoughtcrime.securesms.groups.GroupChangeBusyException;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.GroupCreateResult;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.push.ACI;

/* loaded from: classes4.dex */
public final class AddGroupDetailsRepository {
    private static final String TAG = Log.tag(AddGroupDetailsRepository.class);
    private final Context context;

    public AddGroupDetailsRepository(Context context) {
        this.context = context;
    }

    public void resolveMembers(Collection<RecipientId> collection, Consumer<List<GroupMemberEntry.NewGroupCandidate>> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(collection, consumer) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Collection f$0;
            public final /* synthetic */ Consumer f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AddGroupDetailsRepository.lambda$resolveMembers$0(this.f$0, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$resolveMembers$0(Collection collection, Consumer consumer) {
        ArrayList arrayList = new ArrayList(collection.size());
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            arrayList.add(new GroupMemberEntry.NewGroupCandidate(Recipient.resolved((RecipientId) it.next())));
        }
        consumer.accept(arrayList);
    }

    public void createGroup(Set<RecipientId> set, byte[] bArr, String str, boolean z, Integer num, Consumer<GroupCreateResult> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(set, bArr, str, z, num, consumer) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ Set f$1;
            public final /* synthetic */ byte[] f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ boolean f$4;
            public final /* synthetic */ Integer f$5;
            public final /* synthetic */ Consumer f$6;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AddGroupDetailsRepository.this.lambda$createGroup$1(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6);
            }
        });
    }

    public /* synthetic */ void lambda$createGroup$1(Set set, byte[] bArr, String str, boolean z, Integer num, Consumer consumer) {
        int i;
        HashSet hashSet = new HashSet(Stream.of(set).map(new ContactDiscoveryRefreshV1$$ExternalSyntheticLambda8()).toList());
        try {
            ACI requireAci = SignalStore.account().requireAci();
            Context context = this.context;
            if (num != null) {
                i = num.intValue();
            } else {
                i = SignalStore.settings().getUniversalExpireTimer();
            }
            consumer.accept(GroupCreateResult.success(GroupManager.createGroup(requireAci, context, hashSet, bArr, str, z, i)));
        } catch (IOException unused) {
            consumer.accept(GroupCreateResult.error(GroupCreateResult.Error.Type.ERROR_IO));
        } catch (GroupChangeBusyException unused2) {
            consumer.accept(GroupCreateResult.error(GroupCreateResult.Error.Type.ERROR_BUSY));
        } catch (GroupChangeException unused3) {
            consumer.accept(GroupCreateResult.error(GroupCreateResult.Error.Type.ERROR_FAILED));
        }
    }
}
