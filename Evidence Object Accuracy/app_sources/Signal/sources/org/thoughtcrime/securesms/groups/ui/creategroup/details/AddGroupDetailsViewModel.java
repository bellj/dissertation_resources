package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import android.text.TextUtils;
import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.thoughtcrime.securesms.blocked.BlockedUsersViewModel$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.GroupCreateResult;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public final class AddGroupDetailsViewModel extends ViewModel {
    private final MutableLiveData<byte[]> avatar;
    private Media avatarMedia;
    private final LiveData<Boolean> canSubmitForm;
    private final DefaultValueLiveData<Set<RecipientId>> deleted;
    private final MutableLiveData<Integer> disappearingMessagesTimer;
    private final SingleLiveEvent<GroupCreateResult> groupCreateResult;
    private final LiveData<Boolean> isMms;
    private final LiveData<List<GroupMemberEntry.NewGroupCandidate>> members;
    private final MutableLiveData<String> name;
    private final AddGroupDetailsRepository repository;

    private AddGroupDetailsViewModel(Collection<RecipientId> collection, AddGroupDetailsRepository addGroupDetailsRepository) {
        DefaultValueLiveData<Set<RecipientId>> defaultValueLiveData = new DefaultValueLiveData<>(new HashSet());
        this.deleted = defaultValueLiveData;
        MutableLiveData<String> mutableLiveData = new MutableLiveData<>("");
        this.name = mutableLiveData;
        this.avatar = new MutableLiveData<>();
        this.groupCreateResult = new SingleLiveEvent<>();
        this.disappearingMessagesTimer = new MutableLiveData<>(Integer.valueOf(SignalStore.settings().getUniversalExpireTimer()));
        this.repository = addGroupDetailsRepository;
        MutableLiveData mutableLiveData2 = new MutableLiveData();
        LiveData map = Transformations.map(mutableLiveData, new Function() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda4
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return AddGroupDetailsViewModel.lambda$new$0((String) obj);
            }
        });
        LiveData<List<GroupMemberEntry.NewGroupCandidate>> combineLatest = LiveDataUtil.combineLatest(mutableLiveData2, defaultValueLiveData, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return AddGroupDetailsViewModel.filterDeletedMembers((List) obj, (Set) obj2);
            }
        });
        this.members = combineLatest;
        LiveData<Boolean> map2 = Transformations.map(combineLatest, new Function() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda6
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(AddGroupDetailsViewModel.isAnyForcedSms((List) obj));
            }
        });
        this.isMms = map2;
        LiveDataUtil.combineLatest(map2, combineLatest, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return AddGroupDetailsViewModel.lambda$new$1((Boolean) obj, (List) obj2);
            }
        });
        this.canSubmitForm = LiveDataUtil.combineLatest(map2, map, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda8
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return AddGroupDetailsViewModel.lambda$new$2((Boolean) obj, (Boolean) obj2);
            }
        });
        addGroupDetailsRepository.resolveMembers(collection, new BlockedUsersViewModel$$ExternalSyntheticLambda2(mutableLiveData2));
    }

    public static /* synthetic */ Boolean lambda$new$0(String str) {
        return Boolean.valueOf(!TextUtils.isEmpty(str));
    }

    public static /* synthetic */ List lambda$new$1(Boolean bool, List list) {
        if (SignalStore.internalValues().gv2DoNotCreateGv2Groups() || bool.booleanValue()) {
            return Collections.emptyList();
        }
        return list;
    }

    public static /* synthetic */ Boolean lambda$new$2(Boolean bool, Boolean bool2) {
        return Boolean.valueOf(bool.booleanValue() || bool2.booleanValue());
    }

    public LiveData<List<GroupMemberEntry.NewGroupCandidate>> getMembers() {
        return this.members;
    }

    public LiveData<Boolean> getCanSubmitForm() {
        return this.canSubmitForm;
    }

    public LiveData<GroupCreateResult> getGroupCreateResult() {
        return this.groupCreateResult;
    }

    public LiveData<byte[]> getAvatar() {
        return this.avatar;
    }

    public LiveData<Boolean> getIsMms() {
        return this.isMms;
    }

    public LiveData<Integer> getDisappearingMessagesTimer() {
        return this.disappearingMessagesTimer;
    }

    public void setAvatar(byte[] bArr) {
        this.avatar.setValue(bArr);
    }

    boolean hasAvatar() {
        return this.avatar.getValue() != null;
    }

    public void setName(String str) {
        this.name.setValue(str);
    }

    public void delete(RecipientId recipientId) {
        Set<RecipientId> value = this.deleted.getValue();
        value.add(recipientId);
        this.deleted.setValue(value);
    }

    public void create() {
        List<GroupMemberEntry.NewGroupCandidate> value = this.members.getValue();
        Objects.requireNonNull(value);
        Set<RecipientId> set = (Set) Stream.of(value).map(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AddGroupDetailsViewModel.lambda$create$3((GroupMemberEntry.NewGroupCandidate) obj);
            }
        }).collect(Collectors.toSet());
        byte[] value2 = this.avatar.getValue();
        boolean z = this.isMms.getValue() == Boolean.TRUE;
        String value3 = this.name.getValue();
        Integer value4 = this.disappearingMessagesTimer.getValue();
        if (z || !TextUtils.isEmpty(value3)) {
            AddGroupDetailsRepository addGroupDetailsRepository = this.repository;
            SingleLiveEvent<GroupCreateResult> singleLiveEvent = this.groupCreateResult;
            Objects.requireNonNull(singleLiveEvent);
            addGroupDetailsRepository.createGroup(set, value2, value3, z, value4, new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda2
                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    SingleLiveEvent.this.postValue((GroupCreateResult) obj);
                }
            });
            return;
        }
        this.groupCreateResult.postValue(GroupCreateResult.error(GroupCreateResult.Error.Type.ERROR_INVALID_NAME));
    }

    public static /* synthetic */ RecipientId lambda$create$3(GroupMemberEntry.NewGroupCandidate newGroupCandidate) {
        return newGroupCandidate.getMember().getId();
    }

    public static List<GroupMemberEntry.NewGroupCandidate> filterDeletedMembers(List<GroupMemberEntry.NewGroupCandidate> list, Set<RecipientId> set) {
        return Stream.of(list).filterNot(new Predicate(set) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return AddGroupDetailsViewModel.lambda$filterDeletedMembers$4(this.f$0, (GroupMemberEntry.NewGroupCandidate) obj);
            }
        }).toList();
    }

    public static /* synthetic */ boolean lambda$filterDeletedMembers$4(Set set, GroupMemberEntry.NewGroupCandidate newGroupCandidate) {
        return set.contains(newGroupCandidate.getMember().getId());
    }

    public static boolean isAnyForcedSms(List<GroupMemberEntry.NewGroupCandidate> list) {
        return Stream.of(list).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return AddGroupDetailsViewModel.lambda$isAnyForcedSms$5((GroupMemberEntry.NewGroupCandidate) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$isAnyForcedSms$5(GroupMemberEntry.NewGroupCandidate newGroupCandidate) {
        return !newGroupCandidate.getMember().isRegistered();
    }

    public void setDisappearingMessageTimer(int i) {
        this.disappearingMessagesTimer.setValue(Integer.valueOf(i));
    }

    public void setAvatarMedia(Media media) {
        this.avatarMedia = media;
    }

    public Media getAvatarMedia() {
        return this.avatarMedia;
    }

    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Collection<RecipientId> recipientIds;
        private final AddGroupDetailsRepository repository;

        public Factory(Collection<RecipientId> collection, AddGroupDetailsRepository addGroupDetailsRepository) {
            this.recipientIds = collection;
            this.repository = addGroupDetailsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new AddGroupDetailsViewModel(this.recipientIds, this.repository));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
