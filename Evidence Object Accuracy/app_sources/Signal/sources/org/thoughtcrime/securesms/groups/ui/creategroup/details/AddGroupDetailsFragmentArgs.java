package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import android.os.Bundle;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class AddGroupDetailsFragmentArgs {
    private final HashMap arguments;

    private AddGroupDetailsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private AddGroupDetailsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static AddGroupDetailsFragmentArgs fromBundle(Bundle bundle) {
        RecipientId[] recipientIdArr;
        AddGroupDetailsFragmentArgs addGroupDetailsFragmentArgs = new AddGroupDetailsFragmentArgs();
        bundle.setClassLoader(AddGroupDetailsFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("recipient_ids")) {
            Parcelable[] parcelableArray = bundle.getParcelableArray("recipient_ids");
            if (parcelableArray != null) {
                recipientIdArr = new RecipientId[parcelableArray.length];
                System.arraycopy(parcelableArray, 0, recipientIdArr, 0, parcelableArray.length);
            } else {
                recipientIdArr = null;
            }
            if (recipientIdArr != null) {
                addGroupDetailsFragmentArgs.arguments.put("recipient_ids", recipientIdArr);
                return addGroupDetailsFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"recipient_ids\" is marked as non-null but was passed a null value.");
        }
        throw new IllegalArgumentException("Required argument \"recipient_ids\" is missing and does not have an android:defaultValue");
    }

    public RecipientId[] getRecipientIds() {
        return (RecipientId[]) this.arguments.get("recipient_ids");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("recipient_ids")) {
            bundle.putParcelableArray("recipient_ids", (RecipientId[]) this.arguments.get("recipient_ids"));
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AddGroupDetailsFragmentArgs addGroupDetailsFragmentArgs = (AddGroupDetailsFragmentArgs) obj;
        if (this.arguments.containsKey("recipient_ids") != addGroupDetailsFragmentArgs.arguments.containsKey("recipient_ids")) {
            return false;
        }
        return getRecipientIds() == null ? addGroupDetailsFragmentArgs.getRecipientIds() == null : getRecipientIds().equals(addGroupDetailsFragmentArgs.getRecipientIds());
    }

    public int hashCode() {
        return 31 + Arrays.hashCode(getRecipientIds());
    }

    public String toString() {
        return "AddGroupDetailsFragmentArgs{recipientIds=" + getRecipientIds() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(AddGroupDetailsFragmentArgs addGroupDetailsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(addGroupDetailsFragmentArgs.arguments);
        }

        public Builder(RecipientId[] recipientIdArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientIdArr != null) {
                hashMap.put("recipient_ids", recipientIdArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipient_ids\" is marked as non-null but was passed a null value.");
        }

        public AddGroupDetailsFragmentArgs build() {
            return new AddGroupDetailsFragmentArgs(this.arguments);
        }

        public Builder setRecipientIds(RecipientId[] recipientIdArr) {
            if (recipientIdArr != null) {
                this.arguments.put("recipient_ids", recipientIdArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipient_ids\" is marked as non-null but was passed a null value.");
        }

        public RecipientId[] getRecipientIds() {
            return (RecipientId[]) this.arguments.get("recipient_ids");
        }
    }
}
