package org.thoughtcrime.securesms.groups.ui.creategroup.details;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.util.Consumer;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.vectordrawable.graphics.drawable.VectorDrawableCompat;
import com.airbnb.lottie.SimpleColorFilter;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.picker.AvatarPickerFragment;
import org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListView;
import org.thoughtcrime.securesms.groups.ui.RecipientClickListener;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsViewModel;
import org.thoughtcrime.securesms.groups.ui.creategroup.details.GroupCreateResult;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mediasend.Media;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.ui.disappearingmessages.RecipientDisappearingMessagesActivity;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* loaded from: classes4.dex */
public class AddGroupDetailsFragment extends LoggingFragment {
    private static final int AVATAR_PLACEHOLDER_INSET_DP;
    private static final short REQUEST_DISAPPEARING_TIMER;
    private Drawable avatarPlaceholder;
    private Callback callback;
    private CircularProgressMaterialButton create;
    private View disappearingMessagesRow;
    private EditText name;
    private Toolbar toolbar;
    private AddGroupDetailsViewModel viewModel;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onGroupCreated(RecipientId recipientId, long j, List<Recipient> list);

        void onNavigationButtonPressed();
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Callback) {
            this.callback = (Callback) context;
            return;
        }
        throw new ClassCastException("Parent context should implement AddGroupDetailsFragment.Callback");
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.add_group_details_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.create = (CircularProgressMaterialButton) view.findViewById(R.id.create);
        this.name = (EditText) view.findViewById(R.id.name);
        this.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.disappearingMessagesRow = view.findViewById(R.id.group_disappearing_messages_row);
        setCreateEnabled(false);
        GroupMemberListView groupMemberListView = (GroupMemberListView) view.findViewById(R.id.member_list);
        ImageView imageView = (ImageView) view.findViewById(R.id.group_avatar);
        View findViewById = view.findViewById(R.id.mms_warning);
        View findViewById2 = view.findViewById(R.id.add_later);
        TextView textView = (TextView) view.findViewById(R.id.group_disappearing_messages_value);
        groupMemberListView.initializeAdapter(getViewLifecycleOwner());
        VectorDrawableCompat create = VectorDrawableCompat.create(getResources(), R.drawable.ic_camera_outline_24, requireActivity().getTheme());
        Objects.requireNonNull(create);
        VectorDrawableCompat vectorDrawableCompat = create;
        this.avatarPlaceholder = vectorDrawableCompat;
        vectorDrawableCompat.setColorFilter(new SimpleColorFilter(ContextCompat.getColor(requireContext(), R.color.signal_icon_tint_primary)));
        if (bundle == null) {
            imageView.setImageDrawable(new InsetDrawable(this.avatarPlaceholder, ViewUtil.dpToPx(18)));
        }
        initializeViewModel();
        imageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$0(view2);
            }
        });
        groupMemberListView.setRecipientClickListener(new RecipientClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.groups.ui.RecipientClickListener
            public final void onClick(Recipient recipient) {
                AddGroupDetailsFragment.this.handleRecipientClick(recipient);
            }
        });
        EditTextUtil.addGraphemeClusterLimitFilter(this.name, FeatureFlags.getMaxGroupNameGraphemeLength());
        this.name.addTextChangedListener(new AfterTextChanged(new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda5
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$1((Editable) obj);
            }
        }));
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda6
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$2(view2);
            }
        });
        this.create.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$3(view2);
            }
        });
        this.viewModel.getMembers().observe(getViewLifecycleOwner(), new Observer(findViewById2, groupMemberListView) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ View f$0;
            public final /* synthetic */ GroupMemberListView f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddGroupDetailsFragment.lambda$onViewCreated$4(this.f$0, this.f$1, (List) obj);
            }
        });
        this.viewModel.getCanSubmitForm().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda9
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddGroupDetailsFragment.this.setCreateEnabled(((Boolean) obj).booleanValue());
            }
        });
        this.viewModel.getIsMms().observe(getViewLifecycleOwner(), new Observer(findViewById) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda10
            public final /* synthetic */ View f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$5(this.f$1, (Boolean) obj);
            }
        });
        this.viewModel.getAvatar().observe(getViewLifecycleOwner(), new Observer(imageView) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda11
            public final /* synthetic */ ImageView f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$6(this.f$1, (byte[]) obj);
            }
        });
        this.viewModel.getDisappearingMessagesTimer().observe(getViewLifecycleOwner(), new Observer(textView) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda12
            public final /* synthetic */ TextView f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$7(this.f$1, (Integer) obj);
            }
        });
        this.disappearingMessagesRow.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$8(view2);
            }
        });
        this.name.requestFocus();
        getParentFragmentManager().setFragmentResultListener(AvatarPickerFragment.REQUEST_KEY_SELECT_AVATAR, getViewLifecycleOwner(), new FragmentResultListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda3
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                AddGroupDetailsFragment.this.lambda$onViewCreated$9(str, bundle2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        showAvatarPicker();
    }

    public /* synthetic */ void lambda$onViewCreated$1(Editable editable) {
        this.viewModel.setName(editable.toString());
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        this.callback.onNavigationButtonPressed();
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        handleCreateClicked();
    }

    public static /* synthetic */ void lambda$onViewCreated$4(View view, GroupMemberListView groupMemberListView, List list) {
        view.setVisibility(list.isEmpty() ? 0 : 8);
        groupMemberListView.setMembers(list);
    }

    public /* synthetic */ void lambda$onViewCreated$5(View view, Boolean bool) {
        int i = 8;
        this.disappearingMessagesRow.setVisibility(bool.booleanValue() ? 8 : 0);
        if (bool.booleanValue()) {
            i = 0;
        }
        view.setVisibility(i);
        this.name.setHint(bool.booleanValue() ? R.string.AddGroupDetailsFragment__group_name_optional : R.string.AddGroupDetailsFragment__group_name_required);
        this.toolbar.setTitle(bool.booleanValue() ? R.string.AddGroupDetailsFragment__create_group : R.string.AddGroupDetailsFragment__name_this_group);
    }

    public /* synthetic */ void lambda$onViewCreated$6(ImageView imageView, byte[] bArr) {
        if (bArr == null) {
            imageView.setImageDrawable(new InsetDrawable(this.avatarPlaceholder, ViewUtil.dpToPx(18)));
        } else {
            GlideApp.with(this).load(bArr).circleCrop().skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).into(imageView);
        }
    }

    public /* synthetic */ void lambda$onViewCreated$7(TextView textView, Integer num) {
        textView.setText(ExpirationUtil.getExpirationDisplayValue(requireContext(), num.intValue()));
    }

    public /* synthetic */ void lambda$onViewCreated$8(View view) {
        startActivityForResult(RecipientDisappearingMessagesActivity.forCreateGroup(requireContext(), this.viewModel.getDisappearingMessagesTimer().getValue()), 28621);
    }

    public /* synthetic */ void lambda$onViewCreated$9(String str, Bundle bundle) {
        handleMediaResult(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 28621 && i2 == -1 && intent != null) {
            this.viewModel.setDisappearingMessageTimer(intent.getIntExtra(ExpireTimerSettingsFragment.FOR_RESULT_VALUE, SignalStore.settings().getUniversalExpireTimer()));
        } else {
            super.onActivityResult(i, i2, intent);
        }
    }

    private void handleMediaResult(Bundle bundle) {
        if (bundle.getBoolean(AvatarPickerFragment.SELECT_AVATAR_CLEAR)) {
            this.viewModel.setAvatarMedia(null);
            this.viewModel.setAvatar(null);
            return;
        }
        Media media = (Media) bundle.getParcelable(AvatarPickerFragment.SELECT_AVATAR_MEDIA);
        DecryptableStreamUriLoader.DecryptableUri decryptableUri = new DecryptableStreamUriLoader.DecryptableUri(media.getUri());
        this.viewModel.setAvatarMedia(media);
        GlideRequest<Bitmap> centerCrop = GlideApp.with(this).asBitmap().load((Object) decryptableUri).skipMemoryCache(true).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop();
        int i = AvatarHelper.AVATAR_DIMENSIONS;
        centerCrop.override(i, i).into((GlideRequest<Bitmap>) new CustomTarget<Bitmap>() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment.1
            @Override // com.bumptech.glide.request.target.Target
            public void onLoadCleared(Drawable drawable) {
            }

            @Override // com.bumptech.glide.request.target.Target
            public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
                onResourceReady((Bitmap) obj, (Transition<? super Bitmap>) transition);
            }

            public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
                AddGroupDetailsViewModel addGroupDetailsViewModel = AddGroupDetailsFragment.this.viewModel;
                byte[] byteArray = BitmapUtil.toByteArray(bitmap);
                Objects.requireNonNull(byteArray);
                addGroupDetailsViewModel.setAvatar(byteArray);
            }
        });
    }

    private void initializeViewModel() {
        AddGroupDetailsFragmentArgs fromBundle = AddGroupDetailsFragmentArgs.fromBundle(requireArguments());
        AddGroupDetailsViewModel addGroupDetailsViewModel = (AddGroupDetailsViewModel) ViewModelProviders.of(this, new AddGroupDetailsViewModel.Factory(Arrays.asList(fromBundle.getRecipientIds()), new AddGroupDetailsRepository(requireContext()))).get(AddGroupDetailsViewModel.class);
        this.viewModel = addGroupDetailsViewModel;
        addGroupDetailsViewModel.getGroupCreateResult().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AddGroupDetailsFragment.this.handleGroupCreateResult((GroupCreateResult) obj);
            }
        });
    }

    private void handleCreateClicked() {
        if (this.create.isClickable()) {
            this.create.setSpinning();
            this.viewModel.create();
        }
    }

    public void handleRecipientClick(Recipient recipient) {
        new MaterialAlertDialogBuilder(requireContext()).setMessage((CharSequence) getString(R.string.AddGroupDetailsFragment__remove_s_from_this_group, recipient.getDisplayName(requireContext()))).setCancelable(true).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda15
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        }).setPositiveButton(R.string.AddGroupDetailsFragment__remove, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda16
            public final /* synthetic */ Recipient f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                AddGroupDetailsFragment.this.lambda$handleRecipientClick$11(this.f$1, dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$handleRecipientClick$11(Recipient recipient, DialogInterface dialogInterface, int i) {
        this.viewModel.delete(recipient.getId());
        dialogInterface.dismiss();
    }

    public void handleGroupCreateResult(GroupCreateResult groupCreateResult) {
        groupCreateResult.consume(new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda13
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                AddGroupDetailsFragment.this.handleGroupCreateResultSuccess((GroupCreateResult.Success) obj);
            }
        }, new Consumer() { // from class: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$$ExternalSyntheticLambda14
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                AddGroupDetailsFragment.this.handleGroupCreateResultError((GroupCreateResult.Error) obj);
            }
        });
    }

    public void handleGroupCreateResultSuccess(GroupCreateResult.Success success) {
        this.callback.onGroupCreated(success.getGroupRecipient().getId(), success.getThreadId(), success.getInvitedMembers());
    }

    /* renamed from: org.thoughtcrime.securesms.groups.ui.creategroup.details.AddGroupDetailsFragment$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$groups$ui$creategroup$details$GroupCreateResult$Error$Type;

        static {
            int[] iArr = new int[GroupCreateResult.Error.Type.values().length];
            $SwitchMap$org$thoughtcrime$securesms$groups$ui$creategroup$details$GroupCreateResult$Error$Type = iArr;
            try {
                iArr[GroupCreateResult.Error.Type.ERROR_IO.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$creategroup$details$GroupCreateResult$Error$Type[GroupCreateResult.Error.Type.ERROR_BUSY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$creategroup$details$GroupCreateResult$Error$Type[GroupCreateResult.Error.Type.ERROR_FAILED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$groups$ui$creategroup$details$GroupCreateResult$Error$Type[GroupCreateResult.Error.Type.ERROR_INVALID_NAME.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public void handleGroupCreateResultError(GroupCreateResult.Error error) {
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$groups$ui$creategroup$details$GroupCreateResult$Error$Type[error.getErrorType().ordinal()];
        if (i == 1 || i == 2) {
            toast(R.string.AddGroupDetailsFragment__try_again_later);
        } else if (i == 3) {
            toast(R.string.AddGroupDetailsFragment__group_creation_failed);
        } else if (i == 4) {
            this.name.setError(getString(R.string.AddGroupDetailsFragment__this_field_is_required));
        } else {
            throw new IllegalStateException("Unexpected error: " + error.getErrorType().name());
        }
    }

    private void toast(int i) {
        Toast.makeText(requireContext(), i, 0).show();
    }

    public void setCreateEnabled(boolean z) {
        if (this.create.isClickable() != z) {
            this.create.setClickable(z);
        }
    }

    private void showAvatarPicker() {
        SafeNavigation.safeNavigate(Navigation.findNavController(requireView()), AddGroupDetailsFragmentDirections.actionAddGroupDetailsFragmentToAvatarPicker(null, this.viewModel.getAvatarMedia()).setIsNewGroup(true));
    }
}
