package org.thoughtcrime.securesms.groups.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class GroupMemberListView extends RecyclerView {
    private int maxHeight;
    private GroupMemberListAdapter membersAdapter;
    private boolean selectable;

    public GroupMemberListView(Context context) {
        super(context);
        initialize(context, null);
    }

    public GroupMemberListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(context, attributeSet);
    }

    public GroupMemberListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(context, attributeSet);
    }

    private void initialize(Context context, AttributeSet attributeSet) {
        if (this.maxHeight > 0) {
            setHasFixedSize(true);
        }
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.GroupMemberListView, 0, 0);
            try {
                this.maxHeight = obtainStyledAttributes.getDimensionPixelSize(0, 0);
                this.selectable = obtainStyledAttributes.getBoolean(1, false);
            } finally {
                obtainStyledAttributes.recycle();
            }
        }
    }

    public void initializeAdapter(LifecycleOwner lifecycleOwner) {
        this.membersAdapter = new GroupMemberListAdapter(this.selectable, lifecycleOwner);
        setLayoutManager(new LinearLayoutManager(getContext()));
        setAdapter(this.membersAdapter);
    }

    public void setAdminActionsListener(AdminActionsListener adminActionsListener) {
        this.membersAdapter.setAdminActionsListener(adminActionsListener);
    }

    public void setRecipientClickListener(RecipientClickListener recipientClickListener) {
        this.membersAdapter.setRecipientClickListener(recipientClickListener);
    }

    public void setRecipientLongClickListener(RecipientLongClickListener recipientLongClickListener) {
        this.membersAdapter.setRecipientLongClickListener(recipientLongClickListener);
    }

    public void setRecipientSelectionChangeListener(RecipientSelectionChangeListener recipientSelectionChangeListener) {
        this.membersAdapter.setRecipientSelectionChangeListener(recipientSelectionChangeListener);
    }

    public void setMembers(List<? extends GroupMemberEntry> list) {
        this.membersAdapter.updateData(list);
    }

    public static /* synthetic */ GroupMemberEntry.FullMember lambda$setDisplayOnlyMembers$0(Recipient recipient) {
        return new GroupMemberEntry.FullMember(recipient, false);
    }

    public void setDisplayOnlyMembers(List<Recipient> list) {
        this.membersAdapter.updateData(Stream.of(list).map(new Function() { // from class: org.thoughtcrime.securesms.groups.ui.GroupMemberListView$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return GroupMemberListView.lambda$setDisplayOnlyMembers$0((Recipient) obj);
            }
        }).toList());
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    public void onMeasure(int i, int i2) {
        int i3 = this.maxHeight;
        if (i3 > 0) {
            i2 = View.MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE);
        }
        super.onMeasure(i, i2);
    }
}
