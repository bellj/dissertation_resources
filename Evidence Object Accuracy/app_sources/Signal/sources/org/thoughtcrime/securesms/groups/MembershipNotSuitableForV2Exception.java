package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class MembershipNotSuitableForV2Exception extends Exception {
    public MembershipNotSuitableForV2Exception(String str) {
        super(str);
    }
}
