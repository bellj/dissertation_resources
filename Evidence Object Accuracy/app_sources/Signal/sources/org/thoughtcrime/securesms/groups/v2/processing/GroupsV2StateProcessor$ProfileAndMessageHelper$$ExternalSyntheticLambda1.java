package org.thoughtcrime.securesms.groups.v2.processing;

import com.annimon.stream.function.Predicate;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;
import org.thoughtcrime.securesms.groups.v2.processing.GroupsV2StateProcessor;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda1 implements Predicate {
    public final /* synthetic */ int f$0;

    public /* synthetic */ GroupsV2StateProcessor$ProfileAndMessageHelper$$ExternalSyntheticLambda1(int i) {
        this.f$0 = i;
    }

    @Override // com.annimon.stream.function.Predicate
    public final boolean test(Object obj) {
        return GroupsV2StateProcessor.ProfileAndMessageHelper.lambda$determineProfileSharing$0(this.f$0, (DecryptedGroupChange) obj);
    }
}
