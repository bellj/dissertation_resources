package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class BadGroupIdException extends Exception {
    public BadGroupIdException() {
    }

    public BadGroupIdException(String str) {
        super(str);
    }

    public BadGroupIdException(Exception exc) {
        super(exc);
    }
}
