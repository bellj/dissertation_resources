package org.thoughtcrime.securesms.groups.ui.migration;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.recipients.Recipient;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class GroupsV1MigrationInfoViewModel extends ViewModel {
    private final MutableLiveData<List<Recipient>> droppedMembers;
    private final MutableLiveData<List<Recipient>> pendingMembers;

    private GroupsV1MigrationInfoViewModel(GroupMigrationMembershipChange groupMigrationMembershipChange) {
        this.pendingMembers = new MutableLiveData<>();
        this.droppedMembers = new MutableLiveData<>();
        ExecutorService executorService = SignalExecutors.BOUNDED;
        executorService.execute(new Runnable(groupMigrationMembershipChange) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ GroupMigrationMembershipChange f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupsV1MigrationInfoViewModel.this.lambda$new$0(this.f$1);
            }
        });
        executorService.execute(new Runnable(groupMigrationMembershipChange) { // from class: org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInfoViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ GroupMigrationMembershipChange f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                GroupsV1MigrationInfoViewModel.this.lambda$new$1(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(GroupMigrationMembershipChange groupMigrationMembershipChange) {
        this.pendingMembers.postValue(Recipient.resolvedList(groupMigrationMembershipChange.getPending()));
    }

    public /* synthetic */ void lambda$new$1(GroupMigrationMembershipChange groupMigrationMembershipChange) {
        this.droppedMembers.postValue(Recipient.resolvedList(groupMigrationMembershipChange.getDropped()));
    }

    public LiveData<List<Recipient>> getPendingMembers() {
        return this.pendingMembers;
    }

    public LiveData<List<Recipient>> getDroppedMembers() {
        return this.droppedMembers;
    }

    /* loaded from: classes4.dex */
    static class Factory extends ViewModelProvider.NewInstanceFactory {
        private final GroupMigrationMembershipChange membershipChange;

        public Factory(GroupMigrationMembershipChange groupMigrationMembershipChange) {
            this.membershipChange = groupMigrationMembershipChange;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            return cls.cast(new GroupsV1MigrationInfoViewModel(this.membershipChange));
        }
    }
}
