package org.thoughtcrime.securesms.groups;

/* loaded from: classes4.dex */
public final class GroupChangeFailedException extends GroupChangeException {
    public GroupChangeFailedException() {
    }

    public GroupChangeFailedException(Throwable th) {
        super(th);
    }

    public GroupChangeFailedException(String str) {
        super(str);
    }
}
