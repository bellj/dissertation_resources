package org.thoughtcrime.securesms.groups;

import j$.util.Collection$EL;
import java.util.Collection;
import java.util.HashSet;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.crypto.storage.SignalBaseIdentityKeyStore$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class GroupsV2CapabilityChecker {
    private static final String TAG = Log.tag(GroupsV2CapabilityChecker.class);

    private GroupsV2CapabilityChecker() {
    }

    public static boolean allAndSelfHaveServiceId(Collection<RecipientId> collection) {
        HashSet hashSet = new HashSet(collection);
        hashSet.add(Recipient.self().getId());
        return allHaveServiceId(hashSet);
    }

    public static boolean allHaveServiceId(Collection<RecipientId> collection) {
        return Collection$EL.stream(Recipient.resolvedList(collection)).allMatch(new SignalBaseIdentityKeyStore$$ExternalSyntheticLambda1());
    }
}
