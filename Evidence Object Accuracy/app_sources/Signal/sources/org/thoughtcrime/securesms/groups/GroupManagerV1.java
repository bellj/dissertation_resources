package org.thoughtcrime.securesms.groups;

import android.content.Context;
import com.google.protobuf.ByteString;
import j$.util.Optional;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.attachments.UriAttachment;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.OutgoingExpirationUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingGroupUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.util.GroupUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.whispersystems.signalservice.internal.push.SignalServiceProtos;

/* loaded from: classes4.dex */
public final class GroupManagerV1 {
    private static final String TAG = Log.tag(GroupManagerV1.class);

    public static boolean leaveGroup(Context context, GroupId.V1 v1) {
        return false;
    }

    public static boolean silentLeaveGroup(Context context, GroupId.V1 v1) {
        return false;
    }

    GroupManagerV1() {
    }

    public static GroupManager.GroupActionResult createGroup(Context context, Set<RecipientId> set, byte[] bArr, String str, boolean z) {
        GroupDatabase groups = SignalDatabase.groups();
        SecureRandom secureRandom = new SecureRandom();
        GroupId createMms = z ? GroupId.createMms(secureRandom) : GroupId.createV1(secureRandom);
        RecipientId orInsertFromGroupId = SignalDatabase.recipients().getOrInsertFromGroupId(createMms);
        Recipient resolved = Recipient.resolved(orInsertFromGroupId);
        set.add(Recipient.self().getId());
        ByteArrayInputStream byteArrayInputStream = null;
        if (createMms.isV1()) {
            GroupId.V1 requireV1 = createMms.requireV1();
            groups.create(requireV1, str, set, null, null);
            if (bArr != null) {
                try {
                    byteArrayInputStream = new ByteArrayInputStream(bArr);
                } catch (IOException e) {
                    Log.w(TAG, "Failed to save avatar!", e);
                }
            }
            AvatarHelper.setAvatar(context, orInsertFromGroupId, byteArrayInputStream);
            groups.onAvatarUpdated(requireV1, bArr != null);
            SignalDatabase.recipients().setProfileSharing(resolved.getId(), true);
            return sendGroupUpdate(context, requireV1, set, str, bArr, set.size() - 1);
        }
        groups.create(createMms.requireMms(), str, set);
        if (bArr != null) {
            try {
                byteArrayInputStream = new ByteArrayInputStream(bArr);
            } catch (IOException e2) {
                Log.w(TAG, "Failed to save avatar!", e2);
            }
        }
        AvatarHelper.setAvatar(context, orInsertFromGroupId, byteArrayInputStream);
        groups.onAvatarUpdated(createMms, bArr != null);
        return new GroupManager.GroupActionResult(resolved, SignalDatabase.threads().getOrCreateThreadIdFor(resolved, 2), set.size() - 1, Collections.emptyList());
    }

    public static GroupManager.GroupActionResult updateGroup(Context context, GroupId groupId, Set<RecipientId> set, byte[] bArr, String str, int i) {
        ByteArrayInputStream byteArrayInputStream;
        GroupDatabase groups = SignalDatabase.groups();
        RecipientId orInsertFromGroupId = SignalDatabase.recipients().getOrInsertFromGroupId(groupId);
        set.add(Recipient.self().getId());
        groups.updateMembers(groupId, new LinkedList(set));
        if (groupId.isPush()) {
            GroupId.V1 requireV1 = groupId.requireV1();
            groups.updateTitle(requireV1, str);
            groups.onAvatarUpdated(requireV1, bArr != null);
            if (bArr != null) {
                try {
                    byteArrayInputStream = new ByteArrayInputStream(bArr);
                } catch (IOException e) {
                    Log.w(TAG, "Failed to save avatar!", e);
                }
            } else {
                byteArrayInputStream = null;
            }
            AvatarHelper.setAvatar(context, orInsertFromGroupId, byteArrayInputStream);
            return sendGroupUpdate(context, requireV1, set, str, bArr, i);
        }
        Recipient resolved = Recipient.resolved(orInsertFromGroupId);
        return new GroupManager.GroupActionResult(resolved, SignalDatabase.threads().getOrCreateThreadIdFor(resolved), i, Collections.emptyList());
    }

    public static GroupManager.GroupActionResult updateGroup(Context context, GroupId.Mms mms, byte[] bArr, String str) {
        ByteArrayInputStream byteArrayInputStream;
        GroupDatabase groups = SignalDatabase.groups();
        RecipientId orInsertFromGroupId = SignalDatabase.recipients().getOrInsertFromGroupId(mms);
        Recipient resolved = Recipient.resolved(orInsertFromGroupId);
        long orCreateThreadIdFor = SignalDatabase.threads().getOrCreateThreadIdFor(resolved);
        groups.updateTitle(mms, str);
        groups.onAvatarUpdated(mms, bArr != null);
        if (bArr != null) {
            try {
                byteArrayInputStream = new ByteArrayInputStream(bArr);
            } catch (IOException e) {
                Log.w(TAG, "Failed to save avatar!", e);
            }
        } else {
            byteArrayInputStream = null;
        }
        AvatarHelper.setAvatar(context, orInsertFromGroupId, byteArrayInputStream);
        return new GroupManager.GroupActionResult(resolved, orCreateThreadIdFor, 0, Collections.emptyList());
    }

    private static GroupManager.GroupActionResult sendGroupUpdate(Context context, GroupId.V1 v1, Set<RecipientId> set, String str, byte[] bArr, int i) {
        Recipient resolved = Recipient.resolved(SignalDatabase.recipients().getOrInsertFromGroupId(v1));
        ArrayList arrayList = new ArrayList(set.size());
        ArrayList arrayList2 = new ArrayList(set.size());
        for (RecipientId recipientId : set) {
            Recipient resolved2 = Recipient.resolved(recipientId);
            if (resolved2.hasE164()) {
                arrayList2.add(resolved2.requireE164());
                arrayList.add(GroupV1MessageProcessor.createMember(resolved2.requireE164()));
            }
        }
        SignalServiceProtos.GroupContext.Builder addAllMembers = SignalServiceProtos.GroupContext.newBuilder().setId(ByteString.copyFrom(v1.getDecodedId())).setType(SignalServiceProtos.GroupContext.Type.UPDATE).addAllMembersE164(arrayList2).addAllMembers(arrayList);
        if (str != null) {
            addAllMembers.setName(str);
        }
        return new GroupManager.GroupActionResult(resolved, MessageSender.send(context, (OutgoingMediaMessage) new OutgoingGroupUpdateMessage(resolved, addAllMembers.build(), (Attachment) (bArr != null ? new UriAttachment(BlobProvider.getInstance().forData(bArr).createForSingleUseInMemory(), MediaUtil.IMAGE_PNG, 0, (long) bArr.length, null, false, false, false, false, null, null, null, null, null) : null), System.currentTimeMillis(), 0L, false, (QuoteModel) null, (List<Contact>) Collections.emptyList(), (List<LinkPreview>) Collections.emptyList(), (List<Mention>) Collections.emptyList()), -1L, false, (String) null, (MessageDatabase.InsertListener) null), i, Collections.emptyList());
    }

    public static void updateGroupTimer(Context context, GroupId.V1 v1, int i) {
        RecipientDatabase recipients = SignalDatabase.recipients();
        ThreadDatabase threads = SignalDatabase.threads();
        Recipient externalGroupExact = Recipient.externalGroupExact(v1);
        long orCreateThreadIdFor = threads.getOrCreateThreadIdFor(externalGroupExact);
        recipients.setExpireMessages(externalGroupExact.getId(), i);
        MessageSender.send(context, (OutgoingMediaMessage) new OutgoingExpirationUpdateMessage(externalGroupExact, System.currentTimeMillis(), 1000 * ((long) i)), orCreateThreadIdFor, false, (String) null, (MessageDatabase.InsertListener) null);
    }

    private static Optional<OutgoingGroupUpdateMessage> createGroupLeaveMessage(Context context, GroupId.V1 v1, Recipient recipient) {
        if (SignalDatabase.groups().isActive(v1)) {
            return Optional.of(GroupUtil.createGroupV1LeaveMessage(v1, recipient));
        }
        Log.w(TAG, "Group has already been left.");
        return Optional.empty();
    }
}
