package org.thoughtcrime.securesms.groups;

import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public enum GroupAccessControl {
    ALL_MEMBERS(R.string.GroupManagement_access_level_all_members),
    ONLY_ADMINS(R.string.GroupManagement_access_level_only_admins),
    NO_ONE(R.string.GroupManagement_access_level_no_one);
    
    private final int string;

    GroupAccessControl(int i) {
        this.string = i;
    }

    public int getString() {
        return this.string;
    }
}
