package org.thoughtcrime.securesms.groups.v2.processing;

import java.util.Objects;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedGroupChange;

/* loaded from: classes4.dex */
public final class LocalGroupLogEntry {
    private final DecryptedGroupChange change;
    private final DecryptedGroup group;

    public LocalGroupLogEntry(DecryptedGroup decryptedGroup, DecryptedGroupChange decryptedGroupChange) {
        if (decryptedGroupChange == null || decryptedGroup.getRevision() == decryptedGroupChange.getRevision()) {
            this.group = decryptedGroup;
            this.change = decryptedGroupChange;
            return;
        }
        throw new AssertionError();
    }

    public DecryptedGroup getGroup() {
        return this.group;
    }

    public DecryptedGroupChange getChange() {
        return this.change;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof LocalGroupLogEntry)) {
            return false;
        }
        LocalGroupLogEntry localGroupLogEntry = (LocalGroupLogEntry) obj;
        if (!this.group.equals(localGroupLogEntry.group) || !Objects.equals(this.change, localGroupLogEntry.change)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.group.hashCode() * 31;
        DecryptedGroupChange decryptedGroupChange = this.change;
        return hashCode + (decryptedGroupChange != null ? decryptedGroupChange.hashCode() : 0);
    }
}
