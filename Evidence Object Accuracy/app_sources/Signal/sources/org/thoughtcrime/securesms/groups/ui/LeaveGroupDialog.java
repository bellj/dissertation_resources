package org.thoughtcrime.securesms.groups.ui;

import android.content.DialogInterface;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.function.Function;
import java.io.IOException;
import java.util.List;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.chooseadmin.ChooseNewAdminActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes4.dex */
public final class LeaveGroupDialog {
    private static final String TAG = Log.tag(LeaveGroupDialog.class);
    private final FragmentActivity activity;
    private final GroupId.Push groupId;
    private final Runnable onSuccess;

    public static void handleLeavePushGroup(FragmentActivity fragmentActivity, GroupId.Push push, Runnable runnable) {
        new LeaveGroupDialog(fragmentActivity, push, runnable).show();
    }

    private LeaveGroupDialog(FragmentActivity fragmentActivity, GroupId.Push push, Runnable runnable) {
        this.activity = fragmentActivity;
        this.groupId = push;
        this.onSuccess = runnable;
    }

    public void show() {
        if (!this.groupId.isV2()) {
            showLeaveDialog();
        } else {
            SimpleTask.run(this.activity.getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda0
                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return LeaveGroupDialog.this.lambda$show$0();
                }
            }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda1
                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    LeaveGroupDialog.this.lambda$show$1((Boolean) obj);
                }
            });
        }
    }

    public /* synthetic */ Boolean lambda$show$0() {
        GroupDatabase.V2GroupProperties v2GroupProperties = (GroupDatabase.V2GroupProperties) SignalDatabase.groups().getGroup(this.groupId).map(new Function() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ((GroupDatabase.GroupRecord) obj).requireV2GroupProperties();
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(null);
        if (v2GroupProperties == null || !v2GroupProperties.isAdmin(Recipient.self())) {
            return Boolean.FALSE;
        }
        List<Recipient> memberRecipients = v2GroupProperties.getMemberRecipients(GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF);
        return Boolean.valueOf(Stream.of(memberRecipients).filter(new Predicate() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return GroupDatabase.V2GroupProperties.this.isAdmin((Recipient) obj);
            }
        }).count() == 0 && !memberRecipients.isEmpty());
    }

    public /* synthetic */ void lambda$show$1(Boolean bool) {
        if (bool.booleanValue()) {
            showSelectNewAdminDialog();
        } else {
            showLeaveDialog();
        }
    }

    private void showSelectNewAdminDialog() {
        new MaterialAlertDialogBuilder(this.activity).setTitle(R.string.LeaveGroupDialog_choose_new_admin).setMessage(R.string.LeaveGroupDialog_before_you_leave_you_must_choose_at_least_one_new_admin_for_this_group).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.LeaveGroupDialog_choose_admin, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                LeaveGroupDialog.this.lambda$showSelectNewAdminDialog$2(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$showSelectNewAdminDialog$2(DialogInterface dialogInterface, int i) {
        FragmentActivity fragmentActivity = this.activity;
        fragmentActivity.startActivity(ChooseNewAdminActivity.createIntent(fragmentActivity, this.groupId.requireV2()));
    }

    private void showLeaveDialog() {
        new MaterialAlertDialogBuilder(this.activity).setTitle(R.string.LeaveGroupDialog_leave_group).setCancelable(true).setMessage(R.string.LeaveGroupDialog_you_will_no_longer_be_able_to_send_or_receive_messages_in_this_group).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).setPositiveButton(R.string.LeaveGroupDialog_leave, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda5
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                LeaveGroupDialog.this.lambda$showLeaveDialog$4(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$showLeaveDialog$4(DialogInterface dialogInterface, int i) {
        SimpleTask.run(this.activity.getLifecycle(), new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda6
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return LeaveGroupDialog.this.leaveGroup();
            }
        }, new SimpleTask.ForegroundTask(SimpleProgressDialog.show(this.activity)) { // from class: org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog$$ExternalSyntheticLambda7
            public final /* synthetic */ AlertDialog f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                LeaveGroupDialog.this.lambda$showLeaveDialog$3(this.f$1, (GroupChangeResult) obj);
            }
        });
    }

    public /* synthetic */ void lambda$showLeaveDialog$3(AlertDialog alertDialog, GroupChangeResult groupChangeResult) {
        alertDialog.dismiss();
        handleLeaveGroupResult(groupChangeResult);
    }

    public GroupChangeResult leaveGroup() {
        try {
            GroupManager.leaveGroup(this.activity, this.groupId);
            return GroupChangeResult.SUCCESS;
        } catch (IOException | GroupChangeException e) {
            Log.w(TAG, e);
            return GroupChangeResult.failure(GroupChangeFailureReason.fromException(e));
        }
    }

    private void handleLeaveGroupResult(GroupChangeResult groupChangeResult) {
        if (groupChangeResult.isSuccess()) {
            Runnable runnable = this.onSuccess;
            if (runnable != null) {
                runnable.run();
                return;
            }
            return;
        }
        Toast.makeText(this.activity, GroupErrors.getUserDisplayMessage(groupChangeResult.getFailureReason()), 1).show();
    }
}
