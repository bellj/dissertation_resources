package org.thoughtcrime.securesms.groups.ui;

import android.view.View;
import org.thoughtcrime.securesms.groups.ui.GroupMemberListAdapter;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ GroupMemberListAdapter.ViewHolder f$0;
    public final /* synthetic */ Recipient f$1;

    public /* synthetic */ GroupMemberListAdapter$ViewHolder$$ExternalSyntheticLambda2(GroupMemberListAdapter.ViewHolder viewHolder, Recipient recipient) {
        this.f$0 = viewHolder;
        this.f$1 = recipient;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bindRecipientClick$0(this.f$1, view);
    }
}
