package org.thoughtcrime.securesms.groups.ui;

import java.util.Set;

/* loaded from: classes4.dex */
public interface RecipientSelectionChangeListener {
    void onSelectionChanged(Set<GroupMemberEntry> set);
}
