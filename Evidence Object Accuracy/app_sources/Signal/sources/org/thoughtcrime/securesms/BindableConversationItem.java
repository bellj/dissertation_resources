package org.thoughtcrime.securesms;

import android.net.Uri;
import android.view.View;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import j$.util.Optional;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.conversation.ConversationMessage;
import org.thoughtcrime.securesms.conversation.colors.Colorizable;
import org.thoughtcrime.securesms.conversation.colors.Colorizer;
import org.thoughtcrime.securesms.conversation.mutiselect.MultiselectPart;
import org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable;
import org.thoughtcrime.securesms.database.model.InMemoryMessageRecord;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.giph.mp4.GiphyMp4Playable;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupMigrationMembershipChange;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.stickers.StickerLocator;

/* loaded from: classes.dex */
public interface BindableConversationItem extends Unbindable, GiphyMp4Playable, Colorizable, Multiselectable {

    /* renamed from: org.thoughtcrime.securesms.BindableConversationItem$-CC */
    /* loaded from: classes.dex */
    public final /* synthetic */ class CC {
        public static void $default$updateContactNameColor(BindableConversationItem bindableConversationItem) {
        }

        public static void $default$updateSelectedState(BindableConversationItem bindableConversationItem) {
        }

        public static void $default$updateTimestamps(BindableConversationItem bindableConversationItem) {
        }
    }

    /* loaded from: classes.dex */
    public interface EventListener {
        void onAddToContactsClicked(Contact contact);

        void onBadDecryptLearnMoreClicked(RecipientId recipientId);

        void onBlockJoinRequest(Recipient recipient);

        void onCallToAction(String str);

        void onChangeNumberUpdateContact(Recipient recipient);

        void onChatSessionRefreshLearnMoreClicked();

        void onDonateClicked();

        void onEnableCallNotificationsClicked();

        void onGiftBadgeRevealed(MessageRecord messageRecord);

        void onGroupMemberClicked(RecipientId recipientId, GroupId groupId);

        void onGroupMigrationLearnMoreClicked(GroupMigrationMembershipChange groupMigrationMembershipChange);

        void onInMemoryMessageClicked(InMemoryMessageRecord inMemoryMessageRecord);

        void onIncomingIdentityMismatchClicked(RecipientId recipientId);

        void onInviteFriendsToGroupClicked(GroupId.V2 v2);

        void onInviteSharedContactClicked(List<Recipient> list);

        void onJoinGroupCallClicked();

        void onLinkPreviewClicked(LinkPreview linkPreview);

        void onMessageSharedContactClicked(List<Recipient> list);

        void onMessageWithErrorClicked(MessageRecord messageRecord);

        void onMessageWithRecaptchaNeededClicked(MessageRecord messageRecord);

        void onMoreTextClicked(RecipientId recipientId, long j, boolean z);

        void onPlayInlineContent(ConversationMessage conversationMessage);

        void onQuoteClicked(MmsMessageRecord mmsMessageRecord);

        void onQuotedIndicatorClicked(MessageRecord messageRecord);

        void onReactionClicked(MultiselectPart multiselectPart, long j, boolean z);

        void onRecipientNameClicked(RecipientId recipientId);

        void onRegisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer);

        void onSafetyNumberLearnMoreClicked(Recipient recipient);

        void onSharedContactDetailsClicked(Contact contact, View view);

        void onStickerClicked(StickerLocator stickerLocator);

        void onUnregisterVoiceNoteCallbacks(Observer<VoiceNotePlaybackState> observer);

        boolean onUrlClicked(String str);

        void onViewGiftBadgeClicked(MessageRecord messageRecord);

        void onViewGroupDescriptionChange(GroupId groupId, String str, boolean z);

        void onViewOnceMessageClicked(MmsMessageRecord mmsMessageRecord);

        void onVoiceNotePause(Uri uri);

        void onVoiceNotePlay(Uri uri, long j, double d);

        void onVoiceNotePlaybackSpeedChanged(Uri uri, float f);

        void onVoiceNoteSeekTo(Uri uri, double d);
    }

    void bind(LifecycleOwner lifecycleOwner, ConversationMessage conversationMessage, Optional<MessageRecord> optional, Optional<MessageRecord> optional2, GlideRequests glideRequests, Locale locale, Set<MultiselectPart> set, Recipient recipient, String str, boolean z, boolean z2, boolean z3, boolean z4, Colorizer colorizer, boolean z5);

    @Override // org.thoughtcrime.securesms.conversation.mutiselect.Multiselectable
    ConversationMessage getConversationMessage();

    void setEventListener(EventListener eventListener);

    void updateContactNameColor();

    void updateSelectedState();

    void updateTimestamps();
}
