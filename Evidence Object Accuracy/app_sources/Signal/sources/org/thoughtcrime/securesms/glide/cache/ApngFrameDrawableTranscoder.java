package org.thoughtcrime.securesms.glide.cache;

import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.drawable.DrawableResource;
import com.bumptech.glide.load.resource.transcode.ResourceTranscoder;
import org.signal.glide.apng.APNGDrawable;
import org.signal.glide.apng.decode.APNGDecoder;

/* loaded from: classes4.dex */
public class ApngFrameDrawableTranscoder implements ResourceTranscoder<APNGDecoder, Drawable> {
    @Override // com.bumptech.glide.load.resource.transcode.ResourceTranscoder
    public Resource<Drawable> transcode(Resource<APNGDecoder> resource, Options options) {
        APNGDrawable aPNGDrawable = new APNGDrawable(resource.get());
        aPNGDrawable.setAutoPlay(false);
        aPNGDrawable.setLoopLimit(0);
        return new DrawableResource<Drawable>(aPNGDrawable) { // from class: org.thoughtcrime.securesms.glide.cache.ApngFrameDrawableTranscoder.1
            @Override // com.bumptech.glide.load.engine.Resource
            public int getSize() {
                return 0;
            }

            @Override // com.bumptech.glide.load.engine.Resource
            public void recycle() {
            }

            @Override // com.bumptech.glide.load.engine.Resource
            public Class<Drawable> getResourceClass() {
                return Drawable.class;
            }

            @Override // com.bumptech.glide.load.resource.drawable.DrawableResource, com.bumptech.glide.load.engine.Initializable
            public void initialize() {
                super.initialize();
            }
        };
    }
}
