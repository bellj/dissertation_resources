package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.crypto.CipherInputStream;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class EncryptedCacheDecoder<DecodeType> extends EncryptedCoder implements ResourceDecoder<File, DecodeType> {
    private static final String TAG = Log.tag(EncryptedCacheDecoder.class);
    private final ResourceDecoder<InputStream, DecodeType> decoder;
    private final byte[] secret;

    public EncryptedCacheDecoder(byte[] bArr, ResourceDecoder<InputStream, DecodeType> resourceDecoder) {
        this.secret = bArr;
        this.decoder = resourceDecoder;
    }

    public boolean handles(File file, Options options) throws IOException {
        try {
            CipherInputStream createEncryptedInputStream = createEncryptedInputStream(this.secret, file);
            boolean handles = this.decoder.handles(createEncryptedInputStream, options);
            if (createEncryptedInputStream != null) {
                createEncryptedInputStream.close();
            }
            return handles;
        } catch (IOException e) {
            Log.w(TAG, e);
            return false;
        }
    }

    public Resource<DecodeType> decode(File file, int i, int i2, Options options) throws IOException {
        CipherInputStream createEncryptedInputStream = createEncryptedInputStream(this.secret, file);
        try {
            Resource<DecodeType> decode = this.decoder.decode(createEncryptedInputStream, i, i2, options);
            if (createEncryptedInputStream != null) {
                createEncryptedInputStream.close();
            }
            return decode;
        } catch (Throwable th) {
            if (createEncryptedInputStream != null) {
                try {
                    createEncryptedInputStream.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }
}
