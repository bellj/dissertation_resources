package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.engine.bitmap_recycle.ArrayPool;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class EncryptedCacheEncoder extends EncryptedCoder implements Encoder<InputStream> {
    private static final String TAG = Log.tag(EncryptedCacheEncoder.class);
    private final ArrayPool byteArrayPool;
    private final byte[] secret;

    public EncryptedCacheEncoder(byte[] bArr, ArrayPool arrayPool) {
        this.secret = bArr;
        this.byteArrayPool = arrayPool;
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:31:0x0000 */
    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.io.InputStream */
    /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: com.bumptech.glide.load.Options */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r6v1 */
    /* JADX WARN: Type inference failed for: r6v4, types: [java.lang.Object, byte[]] */
    public boolean encode(InputStream inputStream, File file, Options options) {
        try {
            options = (byte[]) this.byteArrayPool.get(65536, byte[].class);
            OutputStream createEncryptedOutputStream = createEncryptedOutputStream(this.secret, file);
            while (true) {
                try {
                    int read = inputStream.read(options);
                    if (read == -1) {
                        break;
                    }
                    createEncryptedOutputStream.write(options, 0, read);
                } catch (Throwable th) {
                    if (createEncryptedOutputStream != null) {
                        try {
                            createEncryptedOutputStream.close();
                        } catch (Throwable th2) {
                            th.addSuppressed(th2);
                        }
                    }
                    throw th;
                }
            }
            if (createEncryptedOutputStream != null) {
                createEncryptedOutputStream.close();
            }
            return true;
        } catch (IOException e) {
            if (e instanceof SocketException) {
                Log.d(TAG, "Socket exception. Likely a cancellation.");
            } else {
                Log.w(TAG, e);
            }
            return false;
        } finally {
            this.byteArrayPool.put(options == true ? 1 : 0);
        }
    }
}
