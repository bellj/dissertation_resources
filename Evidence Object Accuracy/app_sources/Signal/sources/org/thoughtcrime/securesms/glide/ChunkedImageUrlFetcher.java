package org.thoughtcrime.securesms.glide;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.InputStream;
import okhttp3.OkHttpClient;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.giph.model.ChunkedImageUrl;
import org.thoughtcrime.securesms.net.ChunkedDataFetcher;
import org.thoughtcrime.securesms.net.RequestController;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ChunkedImageUrlFetcher implements DataFetcher<InputStream> {
    private static final String TAG = Log.tag(ChunkedImageUrlFetcher.class);
    private final OkHttpClient client;
    private RequestController requestController;
    private final ChunkedImageUrl url;

    public ChunkedImageUrlFetcher(OkHttpClient okHttpClient, ChunkedImageUrl chunkedImageUrl) {
        this.client = okHttpClient;
        this.url = chunkedImageUrl;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void loadData(Priority priority, final DataFetcher.DataCallback<? super InputStream> dataCallback) {
        this.requestController = new ChunkedDataFetcher(this.client).fetch(this.url.getUrl(), this.url.getSize(), new ChunkedDataFetcher.Callback() { // from class: org.thoughtcrime.securesms.glide.ChunkedImageUrlFetcher.1
            @Override // org.thoughtcrime.securesms.net.ChunkedDataFetcher.Callback
            public void onSuccess(InputStream inputStream) {
                dataCallback.onDataReady(inputStream);
            }

            @Override // org.thoughtcrime.securesms.net.ChunkedDataFetcher.Callback
            public void onFailure(Exception exc) {
                dataCallback.onLoadFailed(exc);
            }
        });
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cleanup() {
        RequestController requestController = this.requestController;
        if (requestController != null) {
            requestController.cancel();
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cancel() {
        Log.d(TAG, "Canceled.");
        RequestController requestController = this.requestController;
        if (requestController != null) {
            requestController.cancel();
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public DataSource getDataSource() {
        return DataSource.REMOTE;
    }
}
