package org.thoughtcrime.securesms.glide;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import okhttp3.OkHttpClient;
import org.thoughtcrime.securesms.giph.model.ChunkedImageUrl;
import org.thoughtcrime.securesms.net.ContentProxySafetyInterceptor;
import org.thoughtcrime.securesms.net.ContentProxySelector;
import org.thoughtcrime.securesms.net.StandardUserAgentInterceptor;
import org.thoughtcrime.securesms.push.SignalServiceNetworkAccess;

/* loaded from: classes4.dex */
public class ChunkedImageUrlLoader implements ModelLoader<ChunkedImageUrl, InputStream> {
    private final OkHttpClient client;

    public boolean handles(ChunkedImageUrl chunkedImageUrl) {
        return true;
    }

    private ChunkedImageUrlLoader(OkHttpClient okHttpClient) {
        this.client = okHttpClient;
    }

    public ModelLoader.LoadData<InputStream> buildLoadData(ChunkedImageUrl chunkedImageUrl, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(chunkedImageUrl, new ChunkedImageUrlFetcher(this.client, chunkedImageUrl));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ModelLoaderFactory<ChunkedImageUrl, InputStream> {
        private final OkHttpClient client = new OkHttpClient.Builder().proxySelector(new ContentProxySelector()).cache(null).addInterceptor(new StandardUserAgentInterceptor()).addNetworkInterceptor(new ContentProxySafetyInterceptor()).addNetworkInterceptor(new PaddedHeadersInterceptor()).dns(SignalServiceNetworkAccess.DNS).build();

        public void teardown() {
        }

        public ModelLoader<ChunkedImageUrl, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ChunkedImageUrlLoader(this.client);
        }
    }
}
