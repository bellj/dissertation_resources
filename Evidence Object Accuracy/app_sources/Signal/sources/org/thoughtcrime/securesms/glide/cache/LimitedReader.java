package org.thoughtcrime.securesms.glide.cache;

import java.io.IOException;
import java.io.InputStream;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.glide.common.io.Reader;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: LimitedReader.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0005\n\u0002\b\u0003\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\t\u0010\u0006\u001a\u00020\u0004H\u0001J\t\u0010\u0007\u001a\u00020\bH\u0001J\t\u0010\t\u001a\u00020\nH\u0001J\t\u0010\u000b\u001a\u00020\u0004H\u0001J\"\u0010\f\u001a\u00020\u00042\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000f\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u0004H\u0016J\t\u0010\u0011\u001a\u00020\bH\u0001J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0013H\u0016J\u0011\u0010\u0015\u001a\n \u0017*\u0004\u0018\u00010\u00160\u0016H\u0001R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/glide/cache/LimitedReader;", "Lorg/signal/glide/common/io/Reader;", "reader", "readLimit", "", "(Lorg/signal/glide/common/io/Reader;I)V", "available", "close", "", "peek", "", "position", "read", "buffer", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "byteCount", "reset", "skip", "", "total", "toInputStream", "Ljava/io/InputStream;", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LimitedReader implements Reader {
    private final int readLimit;
    private final Reader reader;

    @Override // org.signal.glide.common.io.Reader
    public int available() {
        return this.reader.available();
    }

    @Override // org.signal.glide.common.io.Reader
    public void close() {
        this.reader.close();
    }

    @Override // org.signal.glide.common.io.Reader
    public byte peek() {
        return this.reader.peek();
    }

    @Override // org.signal.glide.common.io.Reader
    public int position() {
        return this.reader.position();
    }

    @Override // org.signal.glide.common.io.Reader
    public void reset() {
        this.reader.reset();
    }

    @Override // org.signal.glide.common.io.Reader
    public InputStream toInputStream() {
        return this.reader.toInputStream();
    }

    public LimitedReader(Reader reader, int i) {
        Intrinsics.checkNotNullParameter(reader, "reader");
        this.reader = reader;
        this.readLimit = i;
    }

    @Override // org.signal.glide.common.io.Reader
    public int read(byte[] bArr, int i, int i2) throws IOException {
        if (position() + i2 < this.readLimit) {
            return this.reader.read(bArr, i, i2);
        }
        throw new IOException("Read limit exceeded");
    }

    @Override // org.signal.glide.common.io.Reader
    public long skip(long j) throws IOException {
        if (((long) position()) + j < ((long) this.readLimit)) {
            return this.reader.skip(j);
        }
        throw new IOException("Read limit exceeded");
    }
}
