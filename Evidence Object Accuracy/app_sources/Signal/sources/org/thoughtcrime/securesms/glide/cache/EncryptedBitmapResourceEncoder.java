package org.thoughtcrime.securesms.glide.cache;

import android.graphics.Bitmap;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.bitmap.BitmapEncoder;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class EncryptedBitmapResourceEncoder extends EncryptedCoder implements ResourceEncoder<Bitmap> {
    private static final String TAG = Log.tag(EncryptedBitmapResourceEncoder.class);
    private final byte[] secret;

    public EncryptedBitmapResourceEncoder(byte[] bArr) {
        this.secret = bArr;
    }

    public EncodeStrategy getEncodeStrategy(Options options) {
        return EncodeStrategy.TRANSFORMED;
    }

    public boolean encode(Resource<Bitmap> resource, File file, Options options) {
        Bitmap bitmap = resource.get();
        Bitmap.CompressFormat format = getFormat(bitmap, options);
        int intValue = ((Integer) options.get(BitmapEncoder.COMPRESSION_QUALITY)).intValue();
        try {
            OutputStream createEncryptedOutputStream = createEncryptedOutputStream(this.secret, file);
            bitmap.compress(format, intValue, createEncryptedOutputStream);
            createEncryptedOutputStream.close();
            createEncryptedOutputStream.close();
            return true;
        } catch (IOException e) {
            Log.w(TAG, e);
            return false;
        }
    }

    private Bitmap.CompressFormat getFormat(Bitmap bitmap, Options options) {
        Bitmap.CompressFormat compressFormat = (Bitmap.CompressFormat) options.get(BitmapEncoder.COMPRESSION_FORMAT);
        if (compressFormat != null) {
            return compressFormat;
        }
        if (bitmap.hasAlpha()) {
            return Bitmap.CompressFormat.PNG;
        }
        return Bitmap.CompressFormat.JPEG;
    }
}
