package org.thoughtcrime.securesms.glide;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.util.ContentLengthInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.signal.core.util.logging.Log;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class OkHttpStreamFetcher implements DataFetcher<InputStream> {
    private static final String TAG = Log.tag(OkHttpStreamFetcher.class);
    private final OkHttpClient client;
    private ResponseBody responseBody;
    private InputStream stream;
    private final GlideUrl url;

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cancel() {
    }

    public OkHttpStreamFetcher(OkHttpClient okHttpClient, GlideUrl glideUrl) {
        this.client = okHttpClient;
        this.url = glideUrl;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void loadData(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        try {
            Request.Builder url = new Request.Builder().url(this.url.toStringUrl());
            for (Map.Entry<String, String> entry : this.url.getHeaders().entrySet()) {
                url.addHeader(entry.getKey(), entry.getValue());
            }
            Response execute = this.client.newCall(url.build()).execute();
            this.responseBody = execute.body();
            if (execute.isSuccessful()) {
                InputStream obtain = ContentLengthInputStream.obtain(this.responseBody.byteStream(), this.responseBody.contentLength());
                this.stream = obtain;
                dataCallback.onDataReady(obtain);
                return;
            }
            throw new IOException("Request failed with code: " + execute.code());
        } catch (IOException e) {
            dataCallback.onLoadFailed(e);
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cleanup() {
        InputStream inputStream = this.stream;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        ResponseBody responseBody = this.responseBody;
        if (responseBody != null) {
            responseBody.close();
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public DataSource getDataSource() {
        return DataSource.REMOTE;
    }
}
