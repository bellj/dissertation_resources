package org.thoughtcrime.securesms.glide;

import android.content.Context;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.data.DataFetcher;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class ContactPhotoFetcher implements DataFetcher<InputStream> {
    private final ContactPhoto contactPhoto;
    private final Context context;
    private InputStream inputStream;

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cancel() {
    }

    public ContactPhotoFetcher(Context context, ContactPhoto contactPhoto) {
        this.context = context.getApplicationContext();
        this.contactPhoto = contactPhoto;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void loadData(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
        try {
            InputStream openInputStream = this.contactPhoto.openInputStream(this.context);
            this.inputStream = openInputStream;
            dataCallback.onDataReady(openInputStream);
        } catch (FileNotFoundException unused) {
            dataCallback.onDataReady(null);
        } catch (IOException e) {
            dataCallback.onLoadFailed(e);
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public void cleanup() {
        try {
            InputStream inputStream = this.inputStream;
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException unused) {
        }
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public Class<InputStream> getDataClass() {
        return InputStream.class;
    }

    @Override // com.bumptech.glide.load.data.DataFetcher
    public DataSource getDataSource() {
        return DataSource.LOCAL;
    }
}
