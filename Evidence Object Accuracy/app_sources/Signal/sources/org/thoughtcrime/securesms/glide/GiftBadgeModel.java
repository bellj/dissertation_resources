package org.thoughtcrime.securesms.glide;

import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.data.DataFetcher;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.OkHttpClient;
import org.signal.libsignal.zkgroup.receipts.ReceiptCredentialPresentation;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.database.model.databaseprotos.GiftBadge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: GiftBadgeModel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\b\u0018\u0000 \u00152\u00020\u0001:\u0004\u0015\u0016\u0017\u0018B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/glide/GiftBadgeModel;", "Lcom/bumptech/glide/load/Key;", "giftBadge", "Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "(Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;)V", "getGiftBadge", "()Lorg/thoughtcrime/securesms/database/model/databaseprotos/GiftBadge;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "updateDiskCacheKey", "", "messageDigest", "Ljava/security/MessageDigest;", "Companion", "Factory", "Fetcher", "Loader", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GiftBadgeModel implements Key {
    public static final Companion Companion = new Companion(null);
    private final GiftBadge giftBadge;

    public static /* synthetic */ GiftBadgeModel copy$default(GiftBadgeModel giftBadgeModel, GiftBadge giftBadge, int i, Object obj) {
        if ((i & 1) != 0) {
            giftBadge = giftBadgeModel.giftBadge;
        }
        return giftBadgeModel.copy(giftBadge);
    }

    @JvmStatic
    public static final Factory createFactory() {
        return Companion.createFactory();
    }

    public final GiftBadge component1() {
        return this.giftBadge;
    }

    public final GiftBadgeModel copy(GiftBadge giftBadge) {
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        return new GiftBadgeModel(giftBadge);
    }

    @Override // com.bumptech.glide.load.Key
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof GiftBadgeModel) && Intrinsics.areEqual(this.giftBadge, ((GiftBadgeModel) obj).giftBadge);
    }

    @Override // com.bumptech.glide.load.Key
    public int hashCode() {
        return this.giftBadge.hashCode();
    }

    public String toString() {
        return "GiftBadgeModel(giftBadge=" + this.giftBadge + ')';
    }

    /* compiled from: GiftBadgeModel.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J0\u0010\t\u001a\n\u0012\u0004\u0012\u00020\u0003\u0018\u00010\n2\u0006\u0010\u000b\u001a\u00020\u00022\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\r2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/glide/GiftBadgeModel$Loader;", "Lcom/bumptech/glide/load/model/ModelLoader;", "Lorg/thoughtcrime/securesms/glide/GiftBadgeModel;", "Ljava/io/InputStream;", "client", "Lokhttp3/OkHttpClient;", "(Lokhttp3/OkHttpClient;)V", "getClient", "()Lokhttp3/OkHttpClient;", "buildLoadData", "Lcom/bumptech/glide/load/model/ModelLoader$LoadData;", "model", "width", "", "height", "options", "Lcom/bumptech/glide/load/Options;", "handles", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Loader implements ModelLoader<GiftBadgeModel, InputStream> {
        private final OkHttpClient client;

        public boolean handles(GiftBadgeModel giftBadgeModel) {
            Intrinsics.checkNotNullParameter(giftBadgeModel, "model");
            return true;
        }

        public Loader(OkHttpClient okHttpClient) {
            Intrinsics.checkNotNullParameter(okHttpClient, "client");
            this.client = okHttpClient;
        }

        public final OkHttpClient getClient() {
            return this.client;
        }

        public ModelLoader.LoadData<InputStream> buildLoadData(GiftBadgeModel giftBadgeModel, int i, int i2, Options options) {
            Intrinsics.checkNotNullParameter(giftBadgeModel, "model");
            Intrinsics.checkNotNullParameter(options, "options");
            return new ModelLoader.LoadData<>(giftBadgeModel, new Fetcher(this.client, giftBadgeModel));
        }
    }

    public GiftBadgeModel(GiftBadge giftBadge) {
        Intrinsics.checkNotNullParameter(giftBadge, "giftBadge");
        this.giftBadge = giftBadge;
    }

    public final GiftBadge getGiftBadge() {
        return this.giftBadge;
    }

    @Override // com.bumptech.glide.load.Key
    public void updateDiskCacheKey(MessageDigest messageDigest) {
        Intrinsics.checkNotNullParameter(messageDigest, "messageDigest");
        messageDigest.update(this.giftBadge.toByteArray());
    }

    /* compiled from: GiftBadgeModel.kt */
    @Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0015\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\b\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u000bH\u0016J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\u000eH\u0016J\b\u0010\u000f\u001a\u00020\u0010H\u0016J \u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u00132\u000e\u0010\u0014\u001a\n\u0012\u0006\b\u0000\u0012\u00020\u00020\u0015H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/glide/GiftBadgeModel$Fetcher;", "Lcom/bumptech/glide/load/data/DataFetcher;", "Ljava/io/InputStream;", "client", "Lokhttp3/OkHttpClient;", "giftBadge", "Lorg/thoughtcrime/securesms/glide/GiftBadgeModel;", "(Lokhttp3/OkHttpClient;Lorg/thoughtcrime/securesms/glide/GiftBadgeModel;)V", "okHttpStreamFetcher", "Lorg/thoughtcrime/securesms/glide/OkHttpStreamFetcher;", "cancel", "", "cleanup", "getDataClass", "Ljava/lang/Class;", "getDataSource", "Lcom/bumptech/glide/load/DataSource;", "loadData", "priority", "Lcom/bumptech/glide/Priority;", "callback", "Lcom/bumptech/glide/load/data/DataFetcher$DataCallback;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Fetcher implements DataFetcher<InputStream> {
        private final OkHttpClient client;
        private final GiftBadgeModel giftBadge;
        private OkHttpStreamFetcher okHttpStreamFetcher;

        @Override // com.bumptech.glide.load.data.DataFetcher
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        public Fetcher(OkHttpClient okHttpClient, GiftBadgeModel giftBadgeModel) {
            Intrinsics.checkNotNullParameter(okHttpClient, "client");
            Intrinsics.checkNotNullParameter(giftBadgeModel, "giftBadge");
            this.client = okHttpClient;
            this.giftBadge = giftBadgeModel;
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public void loadData(Priority priority, DataFetcher.DataCallback<? super InputStream> dataCallback) {
            Intrinsics.checkNotNullParameter(priority, "priority");
            Intrinsics.checkNotNullParameter(dataCallback, "callback");
            try {
                ServiceResponse<SignalServiceProfile.Badge> blockingGet = ApplicationDependencies.getDonationsService().getGiftBadge(Locale.getDefault(), new ReceiptCredentialPresentation(this.giftBadge.getGiftBadge().getRedemptionToken().toByteArray()).getReceiptLevel()).blockingGet();
                if (blockingGet.getResult().isPresent()) {
                    SignalServiceProfile.Badge badge = blockingGet.getResult().get();
                    Intrinsics.checkNotNullExpressionValue(badge, "giftBadgeResponse.result.get()");
                    OkHttpStreamFetcher okHttpStreamFetcher = new OkHttpStreamFetcher(this.client, new GlideUrl(Badges.fromServiceBadge(badge).getImageUrl().toString()));
                    this.okHttpStreamFetcher = okHttpStreamFetcher;
                    okHttpStreamFetcher.loadData(priority, dataCallback);
                } else if (blockingGet.getApplicationError().isPresent()) {
                    dataCallback.onLoadFailed(new Exception(blockingGet.getApplicationError().get()));
                } else if (blockingGet.getExecutionError().isPresent()) {
                    dataCallback.onLoadFailed(new Exception(blockingGet.getExecutionError().get()));
                } else {
                    dataCallback.onLoadFailed(new Exception("No result or error in service response."));
                }
            } catch (Exception e) {
                while (true) {
                    dataCallback.onLoadFailed(e);
                    return;
                }
            }
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public void cleanup() {
            OkHttpStreamFetcher okHttpStreamFetcher = this.okHttpStreamFetcher;
            if (okHttpStreamFetcher != null) {
                okHttpStreamFetcher.cleanup();
            }
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public void cancel() {
            OkHttpStreamFetcher okHttpStreamFetcher = this.okHttpStreamFetcher;
            if (okHttpStreamFetcher != null) {
                okHttpStreamFetcher.cancel();
            }
        }

        @Override // com.bumptech.glide.load.data.DataFetcher
        public DataSource getDataSource() {
            return DataSource.REMOTE;
        }
    }

    /* compiled from: GiftBadgeModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u001c\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\b2\u0006\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\fH\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/glide/GiftBadgeModel$Factory;", "Lcom/bumptech/glide/load/model/ModelLoaderFactory;", "Lorg/thoughtcrime/securesms/glide/GiftBadgeModel;", "Ljava/io/InputStream;", "client", "Lokhttp3/OkHttpClient;", "(Lokhttp3/OkHttpClient;)V", "build", "Lcom/bumptech/glide/load/model/ModelLoader;", "multiFactory", "Lcom/bumptech/glide/load/model/MultiModelLoaderFactory;", "teardown", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ModelLoaderFactory<GiftBadgeModel, InputStream> {
        private final OkHttpClient client;

        @Override // com.bumptech.glide.load.model.ModelLoaderFactory
        public void teardown() {
        }

        public Factory(OkHttpClient okHttpClient) {
            Intrinsics.checkNotNullParameter(okHttpClient, "client");
            this.client = okHttpClient;
        }

        @Override // com.bumptech.glide.load.model.ModelLoaderFactory
        public ModelLoader<GiftBadgeModel, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            Intrinsics.checkNotNullParameter(multiModelLoaderFactory, "multiFactory");
            return new Loader(this.client);
        }
    }

    /* compiled from: GiftBadgeModel.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/glide/GiftBadgeModel$Companion;", "", "()V", "createFactory", "Lorg/thoughtcrime/securesms/glide/GiftBadgeModel$Factory;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Factory createFactory() {
            OkHttpClient signalOkHttpClient = ApplicationDependencies.getSignalOkHttpClient();
            Intrinsics.checkNotNullExpressionValue(signalOkHttpClient, "getSignalOkHttpClient()");
            return new Factory(signalOkHttpClient);
        }
    }
}
