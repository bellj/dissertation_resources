package org.thoughtcrime.securesms.glide;

import java.io.IOException;
import java.security.SecureRandom;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.Response;

/* loaded from: classes4.dex */
public class PaddedHeadersInterceptor implements Interceptor {
    private static final int MAX_RANDOM_BYTES;
    private static final int MIN_RANDOM_BYTES;
    private static final String PADDING_HEADER;

    @Override // okhttp3.Interceptor
    public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.proceed(chain.request().newBuilder().headers(getPaddedHeaders(chain.request().headers())).build());
    }

    private Headers getPaddedHeaders(Headers headers) {
        return headers.newBuilder().add(PADDING_HEADER, getRandomString(new SecureRandom(), 1, 64)).build();
    }

    private static String getRandomString(SecureRandom secureRandom, int i, int i2) {
        int nextInt = secureRandom.nextInt(i2 - i) + i;
        char[] cArr = new char[nextInt];
        for (int i3 = 0; i3 < nextInt; i3++) {
            cArr[i3] = (char) (secureRandom.nextInt(74) + 48);
        }
        return new String(cArr);
    }
}
