package org.thoughtcrime.securesms.glide;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import okhttp3.OkHttpClient;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes4.dex */
public class BadgeLoader implements ModelLoader<Badge, InputStream> {
    private final OkHttpClient client;

    public boolean handles(Badge badge) {
        return true;
    }

    private BadgeLoader(OkHttpClient okHttpClient) {
        this.client = okHttpClient;
    }

    public ModelLoader.LoadData<InputStream> buildLoadData(Badge badge, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(badge, new OkHttpStreamFetcher(this.client, new GlideUrl(badge.getImageUrl().toString())));
    }

    public static Factory createFactory() {
        return new Factory(ApplicationDependencies.getSignalOkHttpClient());
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ModelLoaderFactory<Badge, InputStream> {
        private final OkHttpClient client;

        public void teardown() {
        }

        private Factory(OkHttpClient okHttpClient) {
            this.client = okHttpClient;
        }

        public ModelLoader<Badge, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new BadgeLoader(this.client);
        }
    }
}
