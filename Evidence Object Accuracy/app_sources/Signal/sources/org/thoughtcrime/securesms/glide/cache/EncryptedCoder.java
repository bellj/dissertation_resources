package org.thoughtcrime.securesms.glide.cache;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.signal.core.util.StreamUtil;
import org.thoughtcrime.securesms.util.Util;

/* access modifiers changed from: package-private */
/* loaded from: classes4.dex */
public class EncryptedCoder {
    private static byte[] MAGIC_BYTES = {-111, 94, 109, -76, 9, -90, 104, -66, -27, -79, 27, -41, 41, -27, 4, -52};

    public OutputStream createEncryptedOutputStream(byte[] bArr, File file) throws IOException {
        try {
            byte[] secretBytes = Util.getSecretBytes(32);
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] doFinal = instance.doFinal(secretBytes);
            Cipher instance2 = Cipher.getInstance("AES/CTR/NoPadding");
            instance2.init(1, new SecretKeySpec(doFinal, "AES"), new IvParameterSpec(new byte[16]));
            fileOutputStream.write(MAGIC_BYTES);
            fileOutputStream.write(secretBytes);
            CipherOutputStream cipherOutputStream = new CipherOutputStream(fileOutputStream, instance2);
            cipherOutputStream.write(MAGIC_BYTES);
            return cipherOutputStream;
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }

    public CipherInputStream createEncryptedInputStream(byte[] bArr, File file) throws IOException {
        try {
            Mac instance = Mac.getInstance("HmacSHA256");
            instance.init(new SecretKeySpec(bArr, "HmacSHA256"));
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bArr2 = MAGIC_BYTES;
            byte[] bArr3 = new byte[bArr2.length];
            byte[] bArr4 = new byte[32];
            byte[] bArr5 = new byte[bArr2.length];
            StreamUtil.readFully(fileInputStream, bArr3);
            StreamUtil.readFully(fileInputStream, bArr4);
            if (MessageDigest.isEqual(bArr3, MAGIC_BYTES)) {
                byte[] doFinal = instance.doFinal(bArr4);
                Cipher instance2 = Cipher.getInstance("AES/CTR/NoPadding");
                instance2.init(2, new SecretKeySpec(doFinal, "AES"), new IvParameterSpec(new byte[16]));
                CipherInputStream cipherInputStream = new CipherInputStream(fileInputStream, instance2);
                StreamUtil.readFully(cipherInputStream, bArr5);
                if (MessageDigest.isEqual(bArr5, MAGIC_BYTES)) {
                    return cipherInputStream;
                }
                throw new IOException("Key change on encrypted cache file!");
            }
            throw new IOException("Not an encrypted cache file!");
        } catch (InvalidAlgorithmParameterException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new AssertionError(e);
        }
    }
}
