package org.thoughtcrime.securesms.glide;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;

/* loaded from: classes4.dex */
public class GlideLiveDataTarget extends CustomTarget<Bitmap> {
    private final MutableLiveData<Bitmap> liveData = new MutableLiveData<>();

    @Override // com.bumptech.glide.request.target.Target
    public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
        onResourceReady((Bitmap) obj, (Transition<? super Bitmap>) transition);
    }

    public GlideLiveDataTarget(int i, int i2) {
        super(i, i2);
    }

    public void onResourceReady(Bitmap bitmap, Transition<? super Bitmap> transition) {
        this.liveData.postValue(bitmap);
    }

    @Override // com.bumptech.glide.request.target.Target
    public void onLoadCleared(Drawable drawable) {
        this.liveData.postValue(null);
    }

    @Override // com.bumptech.glide.request.target.CustomTarget, com.bumptech.glide.request.target.Target
    public void onLoadFailed(Drawable drawable) {
        this.liveData.postValue(null);
    }

    public LiveData<Bitmap> getLiveData() {
        return this.liveData;
    }
}
