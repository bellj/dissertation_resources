package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.Option;
import java.security.MessageDigest;
import org.signal.core.util.Conversions;

/* loaded from: classes4.dex */
public final class ApngOptions {
    public static Option<Boolean> ANIMATE = Option.disk(KEY, Boolean.TRUE, new Option.CacheKeyUpdater() { // from class: org.thoughtcrime.securesms.glide.cache.ApngOptions$$ExternalSyntheticLambda0
        @Override // com.bumptech.glide.load.Option.CacheKeyUpdater
        public final void update(byte[] bArr, Object obj, MessageDigest messageDigest) {
            ApngOptions.lambda$static$0(bArr, (Boolean) obj, messageDigest);
        }
    });
    private static final String KEY;

    public static /* synthetic */ void lambda$static$0(byte[] bArr, Boolean bool, MessageDigest messageDigest) {
        messageDigest.update(bArr);
        messageDigest.update(Conversions.intToByteArray(bool.booleanValue() ? 1 : 0));
    }

    private ApngOptions() {
    }
}
