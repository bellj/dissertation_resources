package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class EncryptedGifDrawableResourceEncoder extends EncryptedCoder implements ResourceEncoder<GifDrawable> {
    private static final String TAG = Log.tag(EncryptedGifDrawableResourceEncoder.class);
    private final byte[] secret;

    public EncryptedGifDrawableResourceEncoder(byte[] bArr) {
        this.secret = bArr;
    }

    public EncodeStrategy getEncodeStrategy(Options options) {
        return EncodeStrategy.TRANSFORMED;
    }

    public boolean encode(Resource<GifDrawable> resource, File file, Options options) {
        GifDrawable gifDrawable = resource.get();
        try {
            OutputStream createEncryptedOutputStream = createEncryptedOutputStream(this.secret, file);
            ByteBufferUtil.toStream(gifDrawable.getBuffer(), createEncryptedOutputStream);
            if (createEncryptedOutputStream != null) {
                createEncryptedOutputStream.close();
            }
            return true;
        } catch (IOException e) {
            Log.w(TAG, e);
            return false;
        }
    }
}
