package org.thoughtcrime.securesms.glide;

import android.content.Context;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import java.io.InputStream;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;

/* loaded from: classes4.dex */
public class ContactPhotoLoader implements ModelLoader<ContactPhoto, InputStream> {
    private final Context context;

    public boolean handles(ContactPhoto contactPhoto) {
        return true;
    }

    private ContactPhotoLoader(Context context) {
        this.context = context;
    }

    public ModelLoader.LoadData<InputStream> buildLoadData(ContactPhoto contactPhoto, int i, int i2, Options options) {
        return new ModelLoader.LoadData<>(contactPhoto, new ContactPhotoFetcher(this.context, contactPhoto));
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ModelLoaderFactory<ContactPhoto, InputStream> {
        private final Context context;

        public void teardown() {
        }

        public Factory(Context context) {
            this.context = context.getApplicationContext();
        }

        public ModelLoader<ContactPhoto, InputStream> build(MultiModelLoaderFactory multiModelLoaderFactory) {
            return new ContactPhotoLoader(this.context);
        }
    }
}
