package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.IOException;
import java.nio.ByteBuffer;
import org.signal.glide.apng.decode.APNGDecoder;
import org.signal.glide.apng.decode.APNGParser;
import org.signal.glide.common.io.ByteBufferReader;
import org.signal.glide.common.loader.ByteBufferLoader;

/* loaded from: classes4.dex */
public class ApngBufferCacheDecoder implements ResourceDecoder<ByteBuffer, APNGDecoder> {
    public boolean handles(ByteBuffer byteBuffer, Options options) {
        if (((Boolean) options.get(ApngOptions.ANIMATE)).booleanValue()) {
            return APNGParser.isAPNG(new ByteBufferReader(byteBuffer));
        }
        return false;
    }

    public Resource<APNGDecoder> decode(final ByteBuffer byteBuffer, int i, int i2, Options options) throws IOException {
        if (!APNGParser.isAPNG(new ByteBufferReader(byteBuffer))) {
            return null;
        }
        return new FrameSeqDecoderResource(new APNGDecoder(new ByteBufferLoader() { // from class: org.thoughtcrime.securesms.glide.cache.ApngBufferCacheDecoder.1
            public ByteBuffer getByteBuffer() {
                byteBuffer.position(0);
                return byteBuffer;
            }
        }, null), byteBuffer.limit());
    }

    /* loaded from: classes4.dex */
    public static class FrameSeqDecoderResource implements Resource<APNGDecoder> {
        private final APNGDecoder decoder;
        private final int size;

        FrameSeqDecoderResource(APNGDecoder aPNGDecoder, int i) {
            this.decoder = aPNGDecoder;
            this.size = i;
        }

        public Class<APNGDecoder> getResourceClass() {
            return APNGDecoder.class;
        }

        public APNGDecoder get() {
            return this.decoder;
        }

        public int getSize() {
            return this.size;
        }

        public void recycle() {
            this.decoder.stop();
        }
    }
}
