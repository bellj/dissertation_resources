package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.File;
import java.io.IOException;
import org.signal.core.util.StreamUtil;
import org.signal.core.util.logging.Log;
import org.signal.glide.apng.decode.APNGDecoder;

/* loaded from: classes4.dex */
public class EncryptedApngCacheEncoder extends EncryptedCoder implements ResourceEncoder<APNGDecoder> {
    private static final String TAG = Log.tag(EncryptedApngCacheEncoder.class);
    private final byte[] secret;

    public EncryptedApngCacheEncoder(byte[] bArr) {
        this.secret = bArr;
    }

    public EncodeStrategy getEncodeStrategy(Options options) {
        return EncodeStrategy.SOURCE;
    }

    public boolean encode(Resource<APNGDecoder> resource, File file, Options options) {
        try {
            StreamUtil.copy(resource.get().getLoader().obtain().toInputStream(), createEncryptedOutputStream(this.secret, file));
            return true;
        } catch (IOException e) {
            Log.w(TAG, e);
            return false;
        }
    }
}
