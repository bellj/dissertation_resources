package org.thoughtcrime.securesms.glide.cache;

import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.engine.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.signal.core.util.StreamUtil;
import org.signal.glide.apng.decode.APNGDecoder;
import org.signal.glide.apng.decode.APNGParser;
import org.signal.glide.common.io.StreamReader;

/* loaded from: classes4.dex */
public class ApngStreamCacheDecoder implements ResourceDecoder<InputStream, APNGDecoder> {
    private static final int READ_LIMIT;
    private final ResourceDecoder<ByteBuffer, APNGDecoder> byteBufferDecoder;

    public ApngStreamCacheDecoder(ResourceDecoder<ByteBuffer, APNGDecoder> resourceDecoder) {
        this.byteBufferDecoder = resourceDecoder;
    }

    public boolean handles(InputStream inputStream, Options options) {
        if (((Boolean) options.get(ApngOptions.ANIMATE)).booleanValue()) {
            return APNGParser.isAPNG(new LimitedReader(new StreamReader(inputStream), READ_LIMIT));
        }
        return false;
    }

    public Resource<APNGDecoder> decode(InputStream inputStream, int i, int i2, Options options) throws IOException {
        byte[] readFully = StreamUtil.readFully(inputStream);
        if (readFully == null) {
            return null;
        }
        return this.byteBufferDecoder.decode(ByteBuffer.wrap(readFully), i, i2, options);
    }
}
