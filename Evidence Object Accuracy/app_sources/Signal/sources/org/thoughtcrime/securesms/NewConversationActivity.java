package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.app.AlertDialog;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.io.IOException;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;

/* loaded from: classes.dex */
public class NewConversationActivity extends ContactSelectionActivity implements ContactSelectionListFragment.ListCallback {
    static final /* synthetic */ boolean $assertionsDisabled;
    private static final String TAG = Log.tag(NewConversationActivity.class);

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.NewConversationActivity__new_message);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionActivity, org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer) {
        if (optional.isPresent()) {
            launch(Recipient.resolved(optional.get()));
        } else {
            String str2 = TAG;
            Log.i(str2, "[onContactSelected] Maybe creating a new recipient.");
            if (!SignalStore.account().isRegistered() || !NetworkConstraint.isMet(getApplication())) {
                launch(Recipient.external(this, str));
            } else {
                Log.i(str2, "[onContactSelected] Doing contact refresh.");
                SimpleTask.run(getLifecycle(), new SimpleTask.BackgroundTask(str) { // from class: org.thoughtcrime.securesms.NewConversationActivity$$ExternalSyntheticLambda0
                    public final /* synthetic */ String f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                    public final Object run() {
                        return NewConversationActivity.$r8$lambda$vSrdJlgTfNWH0Uw296t2OBoiyV0(NewConversationActivity.this, this.f$1);
                    }
                }, new SimpleTask.ForegroundTask(SimpleProgressDialog.show(this)) { // from class: org.thoughtcrime.securesms.NewConversationActivity$$ExternalSyntheticLambda1
                    public final /* synthetic */ AlertDialog f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                    public final void run(Object obj) {
                        NewConversationActivity.$r8$lambda$0tEo5YWjLtzkMTtRGl8AM8lLlZc(NewConversationActivity.this, this.f$1, (Recipient) obj);
                    }
                });
            }
        }
        consumer.accept(Boolean.TRUE);
    }

    public /* synthetic */ Recipient lambda$onBeforeContactSelected$0(String str) {
        Recipient external = Recipient.external(this, str);
        if (external.isRegistered() && external.hasServiceId()) {
            return external;
        }
        Log.i(TAG, "[onContactSelected] Not registered or no UUID. Doing a directory refresh.");
        try {
            ContactDiscovery.refresh((Context) this, external, false);
            return Recipient.resolved(external.getId());
        } catch (IOException unused) {
            Log.w(TAG, "[onContactSelected] Failed to refresh directory for new contact.");
            return external;
        }
    }

    public /* synthetic */ void lambda$onBeforeContactSelected$1(AlertDialog alertDialog, Recipient recipient) {
        alertDialog.dismiss();
        launch(recipient);
    }

    private void launch(Recipient recipient) {
        startActivity(ConversationIntents.createBuilder(this, recipient.getId(), SignalDatabase.threads().getThreadIdIfExistsFor(recipient.getId())).withDraftText(getIntent().getStringExtra("android.intent.extra.TEXT")).withDataUri(getIntent().getData()).withDataType(getIntent().getType()).build());
        finish();
    }

    @Override // android.app.Activity
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        super.onOptionsItemSelected(menuItem);
        switch (menuItem.getItemId()) {
            case 16908332:
                super.onBackPressed();
                return true;
            case R.id.menu_invite /* 2131363676 */:
                handleInvite();
                return true;
            case R.id.menu_new_group /* 2131363680 */:
                handleCreateGroup();
                return true;
            case R.id.menu_refresh /* 2131363684 */:
                handleManualRefresh();
                return true;
            default:
                return false;
        }
    }

    private void handleManualRefresh() {
        this.contactsFragment.setRefreshing(true);
        onRefresh();
    }

    private void handleCreateGroup() {
        startActivity(CreateGroupActivity.newIntent(this));
    }

    private void handleInvite() {
        startActivity(new Intent(this, InviteActivity.class));
    }

    @Override // android.app.Activity
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.new_conversation_activity, menu);
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.ListCallback
    public void onInvite() {
        handleInvite();
        finish();
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.ListCallback
    public void onNewGroup(boolean z) {
        handleCreateGroup();
        finish();
    }
}
