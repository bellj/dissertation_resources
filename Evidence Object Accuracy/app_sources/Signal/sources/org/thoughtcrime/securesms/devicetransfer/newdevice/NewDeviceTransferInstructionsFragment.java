package org.thoughtcrime.securesms.devicetransfer.newdevice;

import android.os.Bundle;
import android.view.View;
import androidx.navigation.Navigation;
import org.greenrobot.eventbus.EventBus;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class NewDeviceTransferInstructionsFragment extends LoggingFragment {
    public NewDeviceTransferInstructionsFragment() {
        super(R.layout.new_device_transfer_instructions_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        view.findViewById(R.id.new_device_transfer_instructions_fragment_continue).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.newdevice.NewDeviceTransferInstructionsFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                NewDeviceTransferInstructionsFragment.$r8$lambda$bockGpubHIabnfOEnj3ca56iThc(view2);
            }
        });
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_device_transfer_setup);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
    }
}
