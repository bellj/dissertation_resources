package org.thoughtcrime.securesms.devicetransfer.newdevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class NewDeviceTransferCompleteFragmentDirections {
    private NewDeviceTransferCompleteFragmentDirections() {
    }

    public static NavDirections actionNewDeviceTransferCompleteToEnterPhoneNumberFragment() {
        return new ActionOnlyNavDirections(R.id.action_newDeviceTransferComplete_to_enterPhoneNumberFragment);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
