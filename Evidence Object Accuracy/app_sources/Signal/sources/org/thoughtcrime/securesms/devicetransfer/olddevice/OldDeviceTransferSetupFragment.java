package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.navigation.fragment.NavHostFragment;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment;
import org.thoughtcrime.securesms.devicetransfer.SetupStep;
import org.thoughtcrime.securesms.jobs.LocalBackupJob;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class OldDeviceTransferSetupFragment extends DeviceTransferSetupFragment {
    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        ApplicationDependencies.getJobManager().cancelAllInQueue(LocalBackupJob.QUEUE);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected void navigateAwayFromTransfer() {
        NavHostFragment.findNavController(this).popBackStack();
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected void navigateToTransferConnected() {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_oldDeviceTransferSetup_to_oldDeviceTransfer);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected void navigateWhenWifiDirectUnavailable() {
        startActivity(AppSettingsActivity.backups(requireContext()));
        requireActivity().finish();
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected void startTransfer() {
        Intent intent = new Intent(requireContext(), OldDeviceTransferActivity.class);
        intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
        PendingIntent activity = PendingIntent.getActivity(requireContext(), 0, intent, 0);
        DeviceToDeviceTransferService.startClient(requireContext(), new OldDeviceClientTask(), new DeviceToDeviceTransferService.TransferNotificationData(NotificationIds.DEVICE_TRANSFER, NotificationChannels.BACKUPS, R.drawable.ic_signal_backup), activity);
    }

    /* renamed from: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferSetupFragment$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep;

        static {
            int[] iArr = new int[SetupStep.values().length];
            $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep = iArr;
            try {
                iArr[SetupStep.PERMISSIONS_DENIED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.LOCATION_DISABLED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WIFI_DISABLED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WIFI_DIRECT_UNAVAILABLE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.ERROR.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.SETTING_UP.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WAITING.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.TROUBLESHOOTING.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
        }
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected int getErrorTextForStep(SetupStep setupStep) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[setupStep.ordinal()];
        if (i == 1) {
            return R.string.OldDeviceTransferSetup__signal_needs_the_location_permission_to_discover_and_connect_with_your_new_device;
        }
        if (i == 2) {
            return R.string.OldDeviceTransferSetup__signal_needs_location_services_enabled_to_discover_and_connect_with_your_new_device;
        }
        if (i == 3) {
            return R.string.OldDeviceTransferSetup__signal_needs_wifi_on_to_discover_and_connect_with_your_new_device;
        }
        if (i == 4) {
            return R.string.OldDeviceTransferSetup__sorry_it_appears_your_device_does_not_support_wifi_direct;
        }
        if (i == 5) {
            return R.string.OldDeviceTransferSetup__an_unexpected_error_occurred_while_attempting_to_connect_to_your_old_device;
        }
        throw new AssertionError("No error text for step: " + setupStep);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected int getErrorResolveButtonTextForStep(SetupStep setupStep) {
        if (setupStep == SetupStep.WIFI_DIRECT_UNAVAILABLE) {
            return R.string.OldDeviceTransferSetup__create_a_backup;
        }
        throw new AssertionError("No error resolve button text for step: " + setupStep);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment
    protected int getStatusTextForStep(SetupStep setupStep, boolean z) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[setupStep.ordinal()];
        if (i == 5) {
            return R.string.OldDeviceTransferSetup__an_unexpected_error_occurred_while_attempting_to_connect_to_your_old_device;
        }
        if (i == 6 || i == 7) {
            return R.string.OldDeviceTransferSetup__searching_for_new_android_device;
        }
        if (i == 8) {
            return R.string.DeviceTransferSetup__unable_to_discover_new_device;
        }
        throw new AssertionError("No status text for step: " + setupStep);
    }
}
