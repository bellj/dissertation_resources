package org.thoughtcrime.securesms.devicetransfer.newdevice;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.ServerTask;
import org.thoughtcrime.securesms.AppInitialization;
import org.thoughtcrime.securesms.backup.BackupPassphrase;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.backup.FullBackupImporter;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.notifications.NotificationChannels;

/* loaded from: classes.dex */
final class NewDeviceServerTask implements ServerTask {
    private static final String TAG = Log.tag(NewDeviceServerTask.class);

    @Override // org.signal.devicetransfer.ServerTask
    public void run(Context context, InputStream inputStream) {
        try {
            long currentTimeMillis = System.currentTimeMillis();
            String str = TAG;
            Log.i(str, "Starting backup restore.");
            EventBus.getDefault().register(this);
            try {
                SQLiteDatabase backupDatabase = SignalDatabase.getBackupDatabase();
                BackupPassphrase.set(context, "deadbeef");
                FullBackupImporter.importFile(context, AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), backupDatabase, inputStream, "deadbeef");
                SignalDatabase.upgradeRestored(backupDatabase);
                NotificationChannels.restoreContactNotificationChannels(context);
                AppInitialization.onPostBackupRestore(context);
                Log.i(str, "Backup restore complete.");
            } catch (FullBackupImporter.DatabaseDowngradeException e) {
                Log.w(TAG, "Failed due to the backup being from a newer version of Signal.", e);
                EventBus.getDefault().post(new Status(0, Status.State.FAILURE_VERSION_DOWNGRADE));
            } catch (IOException e2) {
                Log.w(TAG, e2);
                EventBus.getDefault().post(new Status(0, Status.State.FAILURE_UNKNOWN));
            }
            long currentTimeMillis2 = System.currentTimeMillis();
            String str2 = TAG;
            Log.i(str2, "Receive took: " + (currentTimeMillis2 - currentTimeMillis));
        } finally {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(FullBackupBase.BackupEvent backupEvent) {
        if (backupEvent.getType() == FullBackupBase.BackupEvent.Type.PROGRESS) {
            EventBus.getDefault().post(new Status(backupEvent.getCount(), Status.State.IN_PROGRESS));
        } else if (backupEvent.getType() == FullBackupBase.BackupEvent.Type.FINISHED) {
            EventBus.getDefault().post(new Status(backupEvent.getCount(), Status.State.SUCCESS));
        }
    }

    /* loaded from: classes4.dex */
    public static final class Status {
        private final long messageCount;
        private final State state;

        /* loaded from: classes4.dex */
        public enum State {
            IN_PROGRESS,
            SUCCESS,
            FAILURE_VERSION_DOWNGRADE,
            FAILURE_UNKNOWN
        }

        public Status(long j, State state) {
            this.messageCount = j;
            this.state = state;
        }

        public long getMessageCount() {
            return this.messageCount;
        }

        public State getState() {
            return this.state;
        }
    }
}
