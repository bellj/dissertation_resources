package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.os.Bundle;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* loaded from: classes4.dex */
public final class OldDeviceTransferActivity extends PassphraseRequiredActivity {
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        this.dynamicTheme.onCreate(this);
        setContentView(R.layout.old_device_transfer_activity);
        Navigation.findNavController(this, R.id.nav_host_fragment).setGraph(R.navigation.old_device_transfer);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }
}
