package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.DeviceTransferBlockingInterceptor;

/* loaded from: classes4.dex */
public final class OldDeviceTransferLockedDialog extends DialogFragment {
    private static final String FRAGMENT_TAG;
    private static final String TAG = Log.tag(OldDeviceTransferLockedDialog.class);

    public static void show(FragmentManager fragmentManager) {
        if (fragmentManager.findFragmentByTag(FRAGMENT_TAG) != null) {
            Log.i(TAG, "Locked dialog already being shown");
        } else {
            new OldDeviceTransferLockedDialog().show(fragmentManager, FRAGMENT_TAG);
        }
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setCancelable(false);
    }

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_Signal_MaterialAlertDialog);
        materialAlertDialogBuilder.setView(R.layout.old_device_transfer_locked_dialog_fragment).setPositiveButton(R.string.OldDeviceTransferLockedDialog__done, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferLockedDialog$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                OldDeviceTransferLockedDialog.this.lambda$onCreateDialog$0(dialogInterface, i);
            }
        }).setNegativeButton(R.string.OldDeviceTransferLockedDialog__cancel_and_activate_this_device, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferLockedDialog$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                OldDeviceTransferLockedDialog.this.lambda$onCreateDialog$1(dialogInterface, i);
            }
        });
        return materialAlertDialogBuilder.create();
    }

    public /* synthetic */ void lambda$onCreateDialog$0(DialogInterface dialogInterface, int i) {
        OldDeviceExitActivity.exit(requireActivity());
    }

    public /* synthetic */ void lambda$onCreateDialog$1(DialogInterface dialogInterface, int i) {
        onUnlockRequest();
    }

    private void onUnlockRequest() {
        SignalStore.misc().clearOldDeviceTransferLocked();
        DeviceTransferBlockingInterceptor.getInstance().unblockNetwork();
    }
}
