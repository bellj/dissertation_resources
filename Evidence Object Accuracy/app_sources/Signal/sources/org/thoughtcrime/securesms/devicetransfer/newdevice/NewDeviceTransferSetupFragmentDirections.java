package org.thoughtcrime.securesms.devicetransfer.newdevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class NewDeviceTransferSetupFragmentDirections {
    private NewDeviceTransferSetupFragmentDirections() {
    }

    public static NavDirections actionNewDeviceTransfer() {
        return new ActionOnlyNavDirections(R.id.action_new_device_transfer);
    }

    public static NavDirections actionDeviceTransferSetupToTransferOrRestore() {
        return new ActionOnlyNavDirections(R.id.action_deviceTransferSetup_to_transferOrRestore);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
