package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.os.Bundle;
import android.view.View;
import androidx.navigation.fragment.NavHostFragment;
import java.text.NumberFormat;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment;
import org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceClientTask;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class OldDeviceTransferFragment extends DeviceTransferFragment {
    private final ClientTaskListener clientTaskListener = new ClientTaskListener();

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        EventBus.getDefault().register(this.clientTaskListener);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        EventBus.getDefault().unregister(this.clientTaskListener);
        super.onDestroyView();
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment
    protected void navigateToRestartTransfer() {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_directly_to_oldDeviceTransferInstructions);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment
    protected void navigateAwayFromTransfer() {
        EventBus.getDefault().unregister(this.clientTaskListener);
        requireActivity().finish();
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment
    protected void navigateToTransferComplete() {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_oldDeviceTransfer_to_oldDeviceTransferComplete);
    }

    /* loaded from: classes.dex */
    private class ClientTaskListener {
        private final NumberFormat formatter;

        public ClientTaskListener() {
            OldDeviceTransferFragment.this = r2;
            NumberFormat instance = NumberFormat.getInstance();
            this.formatter = instance;
            instance.setMinimumFractionDigits(1);
            instance.setMaximumFractionDigits(1);
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onEventMainThread(OldDeviceClientTask.Status status) {
            if (status.isDone()) {
                ((DeviceTransferFragment) OldDeviceTransferFragment.this).transferFinished = true;
                OldDeviceTransferFragment.this.ignoreTransferStatusEvents();
                EventBus.getDefault().removeStickyEvent(TransferStatus.class);
                DeviceToDeviceTransferService.stop(OldDeviceTransferFragment.this.requireContext());
                SafeNavigation.safeNavigate(NavHostFragment.findNavController(OldDeviceTransferFragment.this), (int) R.id.action_oldDeviceTransfer_to_oldDeviceTransferComplete);
            } else if (status.getEstimatedMessageCount() == 0) {
                ((DeviceTransferFragment) OldDeviceTransferFragment.this).status.setText(OldDeviceTransferFragment.this.getString(R.string.DeviceTransfer__d_messages_so_far, Long.valueOf(status.getMessageCount())));
            } else {
                ((DeviceTransferFragment) OldDeviceTransferFragment.this).status.setText(OldDeviceTransferFragment.this.getString(R.string.DeviceTransfer__s_of_messages_so_far, this.formatter.format(status.getCompletionPercentage())));
            }
        }
    }
}
