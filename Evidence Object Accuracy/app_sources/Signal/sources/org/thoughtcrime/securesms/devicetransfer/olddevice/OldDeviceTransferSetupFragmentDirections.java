package org.thoughtcrime.securesms.devicetransfer.olddevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.OldDeviceTransferDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class OldDeviceTransferSetupFragmentDirections {
    private OldDeviceTransferSetupFragmentDirections() {
    }

    public static NavDirections actionOldDeviceTransferSetupToOldDeviceTransfer() {
        return new ActionOnlyNavDirections(R.id.action_oldDeviceTransferSetup_to_oldDeviceTransfer);
    }

    public static NavDirections actionDirectlyToOldDeviceTransferInstructions() {
        return OldDeviceTransferDirections.actionDirectlyToOldDeviceTransferInstructions();
    }
}
