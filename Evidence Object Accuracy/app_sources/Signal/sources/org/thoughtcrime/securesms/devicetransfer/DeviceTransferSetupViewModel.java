package org.thoughtcrime.securesms.devicetransfer;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.TransferStatus;
import org.signal.devicetransfer.WifiDirect;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;

/* loaded from: classes4.dex */
public final class DeviceTransferSetupViewModel extends ViewModel {
    private static final String TAG = Log.tag(DeviceTransferSetupViewModel.class);
    private final LiveData<DeviceSetupState> distinctStepChanges;
    private boolean shutdown;
    private final Store<DeviceSetupState> store;

    public DeviceTransferSetupViewModel() {
        Store<DeviceSetupState> store = new Store<>(new DeviceSetupState());
        this.store = store;
        this.distinctStepChanges = LiveDataUtil.distinctUntilChanged(store.getStateLiveData(), new LiveDataUtil.EqualityChecker() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda10
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.EqualityChecker
            public final boolean contentsMatch(Object obj, Object obj2) {
                return DeviceTransferSetupViewModel.lambda$new$0((DeviceSetupState) obj, (DeviceSetupState) obj2);
            }
        });
    }

    public static /* synthetic */ boolean lambda$new$0(DeviceSetupState deviceSetupState, DeviceSetupState deviceSetupState2) {
        return deviceSetupState.getCurrentSetupStep() == deviceSetupState2.getCurrentSetupStep();
    }

    public LiveData<DeviceSetupState> getState() {
        return this.distinctStepChanges;
    }

    public boolean isNotShutdown() {
        return !this.shutdown;
    }

    public void onTransferEvent(TransferStatus transferStatus) {
        if (!this.shutdown) {
            String str = TAG;
            Log.i(str, "Handling transferStatus: " + transferStatus.getTransferMode());
            switch (AnonymousClass1.$SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[transferStatus.getTransferMode().ordinal()]) {
                case 1:
                case 2:
                    Log.d(str, "Ignore event: " + transferStatus.getTransferMode());
                    return;
                case 3:
                case 4:
                    this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda2
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return DeviceTransferSetupViewModel.lambda$onTransferEvent$1((DeviceSetupState) obj);
                        }
                    });
                    return;
                case 5:
                    this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda3
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return DeviceTransferSetupViewModel.lambda$onTransferEvent$2((DeviceSetupState) obj);
                        }
                    });
                    return;
                case 6:
                    this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda4
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return DeviceTransferSetupViewModel.lambda$onTransferEvent$3(TransferStatus.this, (DeviceSetupState) obj);
                        }
                    });
                    return;
                case 7:
                    this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda5
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return DeviceTransferSetupViewModel.lambda$onTransferEvent$4((DeviceSetupState) obj);
                        }
                    });
                    return;
                case 8:
                case 9:
                    this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda6
                        @Override // com.annimon.stream.function.Function
                        public final Object apply(Object obj) {
                            return DeviceTransferSetupViewModel.lambda$onTransferEvent$5((DeviceSetupState) obj);
                        }
                    });
                    return;
                default:
                    return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode;

        static {
            int[] iArr = new int[TransferStatus.TransferMode.values().length];
            $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode = iArr;
            try {
                iArr[TransferStatus.TransferMode.UNAVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.NETWORK_CONNECTED.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.READY.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.STARTING_UP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.DISCOVERY.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.VERIFICATION_REQUIRED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.SERVICE_CONNECTED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.SHUTDOWN.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$signal$devicetransfer$TransferStatus$TransferMode[TransferStatus.TransferMode.FAILED.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
        }
    }

    public static /* synthetic */ DeviceSetupState lambda$onTransferEvent$1(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.SETTING_UP);
    }

    public static /* synthetic */ DeviceSetupState lambda$onTransferEvent$2(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.WAITING);
    }

    public static /* synthetic */ DeviceSetupState lambda$onTransferEvent$3(TransferStatus transferStatus, DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateVerificationRequired(transferStatus.getAuthenticationCode());
    }

    public static /* synthetic */ DeviceSetupState lambda$onTransferEvent$4(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.CONNECTED);
    }

    public static /* synthetic */ DeviceSetupState lambda$onTransferEvent$5(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.ERROR);
    }

    public void onLocationPermissionDenied() {
        Log.i(TAG, "Location permissions denied");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onLocationPermissionDenied$6((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onLocationPermissionDenied$6(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.PERMISSIONS_DENIED);
    }

    public void onWifiDisabled(boolean z) {
        String str = TAG;
        Log.i(str, "Wifi disabled manager: " + z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onWifiDisabled$7((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onWifiDisabled$7(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.WIFI_DISABLED);
    }

    public void onWifiDirectUnavailable(WifiDirect.AvailableStatus availableStatus) {
        String str = TAG;
        Log.i(str, "Wifi Direct unavailable: " + availableStatus);
        if (availableStatus == WifiDirect.AvailableStatus.FINE_LOCATION_PERMISSION_NOT_GRANTED) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda17
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return DeviceTransferSetupViewModel.lambda$onWifiDirectUnavailable$8((DeviceSetupState) obj);
                }
            });
        } else {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda18
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return DeviceTransferSetupViewModel.lambda$onWifiDirectUnavailable$9((DeviceSetupState) obj);
                }
            });
        }
    }

    public static /* synthetic */ DeviceSetupState lambda$onWifiDirectUnavailable$8(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.PERMISSIONS_CHECK);
    }

    public static /* synthetic */ DeviceSetupState lambda$onWifiDirectUnavailable$9(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.WIFI_DIRECT_UNAVAILABLE);
    }

    public void checkPermissions() {
        Log.d(TAG, "Check for permissions");
        this.shutdown = false;
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$checkPermissions$10((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$checkPermissions$10(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.PERMISSIONS_CHECK);
    }

    public void onPermissionsGranted() {
        Log.d(TAG, "Permissions granted");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onPermissionsGranted$11((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onPermissionsGranted$11(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.LOCATION_CHECK);
    }

    public void onLocationEnabled() {
        Log.d(TAG, "Location enabled");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda14
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onLocationEnabled$12((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onLocationEnabled$12(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.WIFI_CHECK);
    }

    public void onLocationDisabled() {
        Log.d(TAG, "Location disabled");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onLocationDisabled$13((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onLocationDisabled$13(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.LOCATION_DISABLED);
    }

    public void onWifiEnabled() {
        Log.d(TAG, "Wifi enabled");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda16
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onWifiEnabled$14((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onWifiEnabled$14(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.WIFI_DIRECT_CHECK);
    }

    public void onWifiDirectAvailable() {
        Log.d(TAG, "Wifi direct available");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onWifiDirectAvailable$15((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onWifiDirectAvailable$15(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.START);
    }

    public static /* synthetic */ DeviceSetupState lambda$onVerified$16(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.WAITING_FOR_OTHER_TO_VERIFY);
    }

    public void onVerified() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda15
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onVerified$16((DeviceSetupState) obj);
            }
        });
    }

    public void onResume() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda13
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onResume$17((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onResume$17(DeviceSetupState deviceSetupState) {
        if (deviceSetupState.getCurrentSetupStep() == SetupStep.WIFI_DISABLED) {
            return deviceSetupState.updateStep(SetupStep.WIFI_CHECK);
        }
        return deviceSetupState.getCurrentSetupStep() == SetupStep.LOCATION_DISABLED ? deviceSetupState.updateStep(SetupStep.LOCATION_CHECK) : deviceSetupState;
    }

    public void onWaitingTookTooLong() {
        this.shutdown = true;
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DeviceTransferSetupViewModel.lambda$onWaitingTookTooLong$18((DeviceSetupState) obj);
            }
        });
    }

    public static /* synthetic */ DeviceSetupState lambda$onWaitingTookTooLong$18(DeviceSetupState deviceSetupState) {
        return deviceSetupState.updateStep(SetupStep.TROUBLESHOOTING);
    }
}
