package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import net.zetetic.database.sqlcipher.SQLiteDatabase;

/* loaded from: classes4.dex */
public class OldDeviceExitActivity extends AppCompatActivity {
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        finishAll(this);
    }

    public static void exit(Activity activity) {
        Intent intent = new Intent(activity, OldDeviceExitActivity.class);
        intent.addFlags(67108864);
        intent.addFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
        intent.addFlags(32768);
        activity.startActivity(intent);
        finishAll(activity);
    }

    private static void finishAll(Activity activity) {
        if (Build.VERSION.SDK_INT < 21) {
            activity.finishAffinity();
        } else {
            activity.finishAndRemoveTask();
        }
    }
}
