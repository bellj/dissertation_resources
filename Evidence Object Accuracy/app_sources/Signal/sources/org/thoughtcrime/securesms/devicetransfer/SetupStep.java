package org.thoughtcrime.securesms.devicetransfer;

/* loaded from: classes4.dex */
public enum SetupStep {
    INITIAL(true, false),
    PERMISSIONS_CHECK(true, false),
    PERMISSIONS_DENIED(false, true),
    LOCATION_CHECK(true, false),
    LOCATION_DISABLED(false, true),
    WIFI_CHECK(true, false),
    WIFI_DISABLED(false, true),
    WIFI_DIRECT_CHECK(true, false),
    WIFI_DIRECT_UNAVAILABLE(false, true),
    START(true, false),
    SETTING_UP(true, false),
    WAITING(true, false),
    VERIFY(false, false),
    WAITING_FOR_OTHER_TO_VERIFY(false, false),
    CONNECTED(true, false),
    TROUBLESHOOTING(false, false),
    ERROR(false, true);
    
    private final boolean isError;
    private final boolean isProgress;

    SetupStep(boolean z, boolean z2) {
        this.isProgress = z;
        this.isError = z2;
    }

    public boolean isProgress() {
        return this.isProgress;
    }

    public boolean isError() {
        return this.isError;
    }
}
