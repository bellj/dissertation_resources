package org.thoughtcrime.securesms.devicetransfer.olddevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.OldDeviceTransferDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class OldDeviceTransferInstructionsFragmentDirections {
    private OldDeviceTransferInstructionsFragmentDirections() {
    }

    public static NavDirections actionOldDeviceTransferInstructionsToOldDeviceTransferSetup() {
        return new ActionOnlyNavDirections(R.id.action_oldDeviceTransferInstructions_to_oldDeviceTransferSetup);
    }

    public static NavDirections actionDirectlyToOldDeviceTransferInstructions() {
        return OldDeviceTransferDirections.actionDirectlyToOldDeviceTransferInstructions();
    }
}
