package org.thoughtcrime.securesms.devicetransfer.newdevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class NewDeviceTransferInstructionsFragmentDirections {
    private NewDeviceTransferInstructionsFragmentDirections() {
    }

    public static NavDirections actionDeviceTransferSetup() {
        return new ActionOnlyNavDirections(R.id.action_device_transfer_setup);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
