package org.thoughtcrime.securesms.devicetransfer.newdevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class TransferOrRestoreFragmentDirections {
    private TransferOrRestoreFragmentDirections() {
    }

    public static NavDirections actionChooseBackup() {
        return new ActionOnlyNavDirections(R.id.action_choose_backup);
    }

    public static NavDirections actionNewDeviceTransferInstructions() {
        return new ActionOnlyNavDirections(R.id.action_new_device_transfer_instructions);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
