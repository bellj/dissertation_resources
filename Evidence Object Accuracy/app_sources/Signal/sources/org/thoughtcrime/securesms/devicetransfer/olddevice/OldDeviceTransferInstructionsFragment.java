package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import org.greenrobot.eventbus.EventBus;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class OldDeviceTransferInstructionsFragment extends LoggingFragment {
    public OldDeviceTransferInstructionsFragment() {
        super(R.layout.old_device_transfer_instructions_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        ((Toolbar) view.findViewById(R.id.old_device_transfer_instructions_fragment_toolbar)).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferInstructionsFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                OldDeviceTransferInstructionsFragment.$r8$lambda$6EgOqte8pvVpH29qxgPwuS3wjso(OldDeviceTransferInstructionsFragment.this, view2);
            }
        });
        view.findViewById(R.id.old_device_transfer_instructions_fragment_continue).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferInstructionsFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                OldDeviceTransferInstructionsFragment.$r8$lambda$j9b22vhbYh_eL6suK71kdrD8_Yo(view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        if (!Navigation.findNavController(view).popBackStack()) {
            requireActivity().finish();
        }
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_oldDeviceTransferInstructions_to_oldDeviceTransferSetup);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        if (EventBus.getDefault().getStickyEvent(TransferStatus.class) != null) {
            SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_oldDeviceTransferInstructions_to_oldDeviceTransferSetup);
        } else {
            DeviceToDeviceTransferService.stop(requireContext());
        }
    }
}
