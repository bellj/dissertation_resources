package org.thoughtcrime.securesms.devicetransfer.newdevice;

import android.os.Bundle;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.navigation.fragment.NavHostFragment;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class NewDeviceTransferCompleteFragment extends LoggingFragment {
    public NewDeviceTransferCompleteFragment() {
        super(R.layout.new_device_transfer_complete_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        view.findViewById(R.id.new_device_transfer_complete_fragment_continue_registration).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.newdevice.NewDeviceTransferCompleteFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                NewDeviceTransferCompleteFragment.$r8$lambda$PaDL0OXKUE5KPe9jCzV_8kvy1dw(NewDeviceTransferCompleteFragment.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_newDeviceTransferComplete_to_enterPhoneNumberFragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) { // from class: org.thoughtcrime.securesms.devicetransfer.newdevice.NewDeviceTransferCompleteFragment.1
            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
            }
        });
    }
}
