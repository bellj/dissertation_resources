package org.thoughtcrime.securesms.devicetransfer.olddevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.OldDeviceTransferDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class OldDeviceTransferFragmentDirections {
    private OldDeviceTransferFragmentDirections() {
    }

    public static NavDirections actionOldDeviceTransferToOldDeviceTransferComplete() {
        return new ActionOnlyNavDirections(R.id.action_oldDeviceTransfer_to_oldDeviceTransferComplete);
    }

    public static NavDirections actionDirectlyToOldDeviceTransferInstructions() {
        return OldDeviceTransferDirections.actionDirectlyToOldDeviceTransferInstructions();
    }
}
