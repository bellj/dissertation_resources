package org.thoughtcrime.securesms.devicetransfer.olddevice;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.OldDeviceTransferDirections;

/* loaded from: classes4.dex */
public class OldDeviceTransferCompleteFragmentDirections {
    private OldDeviceTransferCompleteFragmentDirections() {
    }

    public static NavDirections actionDirectlyToOldDeviceTransferInstructions() {
        return OldDeviceTransferDirections.actionDirectlyToOldDeviceTransferInstructions();
    }
}
