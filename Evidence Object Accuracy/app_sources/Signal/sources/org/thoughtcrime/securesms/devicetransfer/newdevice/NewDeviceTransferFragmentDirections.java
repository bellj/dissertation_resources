package org.thoughtcrime.securesms.devicetransfer.newdevice;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.SignupDirections;

/* loaded from: classes4.dex */
public class NewDeviceTransferFragmentDirections {
    private NewDeviceTransferFragmentDirections() {
    }

    public static NavDirections actionNewDeviceTransferToNewDeviceTransferInstructions() {
        return new ActionOnlyNavDirections(R.id.action_newDeviceTransfer_to_newDeviceTransferInstructions);
    }

    public static NavDirections actionNewDeviceTransferToNewDeviceTransferComplete() {
        return new ActionOnlyNavDirections(R.id.action_newDeviceTransfer_to_newDeviceTransferComplete);
    }

    public static NavDirections actionRestartToWelcomeFragment() {
        return SignupDirections.actionRestartToWelcomeFragment();
    }
}
