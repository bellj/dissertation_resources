package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.os.Bundle;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class OldDeviceTransferCompleteFragment extends LoggingFragment {
    public OldDeviceTransferCompleteFragment() {
        super(R.layout.old_device_transfer_complete_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        view.findViewById(R.id.old_device_transfer_complete_fragment_close).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferCompleteFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                OldDeviceTransferCompleteFragment.m1775$r8$lambda$GFdIih3kfWjlEBBvujifFUFGak(OldDeviceTransferCompleteFragment.this, view2);
            }
        });
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        close();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(true) { // from class: org.thoughtcrime.securesms.devicetransfer.olddevice.OldDeviceTransferCompleteFragment.1
            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                OldDeviceTransferCompleteFragment.this.close();
            }
        });
    }

    public void close() {
        OldDeviceExitActivity.exit(requireActivity());
    }
}
