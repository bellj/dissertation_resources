package org.thoughtcrime.securesms.devicetransfer;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.signal.devicetransfer.TransferStatus;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public abstract class DeviceTransferFragment extends LoggingFragment {
    private static final String TRANSFER_FINISHED_KEY;
    protected View alert;
    protected Button cancel;
    private final OnBackPressed onBackPressed = new OnBackPressed();
    protected View progress;
    protected TextView status;
    protected TextView title;
    protected boolean transferFinished;
    private final TransferModeListener transferModeListener = new TransferModeListener();
    protected View tryAgain;

    protected abstract void navigateAwayFromTransfer();

    protected abstract void navigateToRestartTransfer();

    protected abstract void navigateToTransferComplete();

    public DeviceTransferFragment() {
        super(R.layout.device_transfer_fragment);
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (bundle != null) {
            this.transferFinished = bundle.getBoolean(TRANSFER_FINISHED_KEY);
        }
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onStart() {
        super.onStart();
        if (this.transferFinished) {
            navigateToTransferComplete();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean(TRANSFER_FINISHED_KEY, this.transferFinished);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        this.title = (TextView) view.findViewById(R.id.device_transfer_fragment_title);
        this.tryAgain = view.findViewById(R.id.device_transfer_fragment_try_again);
        this.cancel = (Button) view.findViewById(R.id.device_transfer_fragment_cancel);
        this.progress = view.findViewById(R.id.device_transfer_fragment_progress);
        this.alert = view.findViewById(R.id.device_transfer_fragment_alert);
        this.status = (TextView) view.findViewById(R.id.device_transfer_fragment_status);
        this.cancel.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeviceTransferFragment.m1762$r8$lambda$p3uTXJvzTJrcnmunA1VYKZIbCg(DeviceTransferFragment.this, view2);
            }
        });
        this.tryAgain.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DeviceTransferFragment.m1761$r8$lambda$YudO27SUr4uFDjiopvAV8RDaHQ(DeviceTransferFragment.this, view2);
            }
        });
        EventBus.getDefault().register(this.transferModeListener);
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        cancelActiveTransfer();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        EventBus.getDefault().unregister(this.transferModeListener);
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
        navigateToRestartTransfer();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), this.onBackPressed);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        EventBus.getDefault().unregister(this.transferModeListener);
        super.onDestroyView();
    }

    public void cancelActiveTransfer() {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.DeviceTransfer__stop_transfer).setMessage(R.string.DeviceTransfer__all_transfer_progress_will_be_lost).setPositiveButton(R.string.DeviceTransfer__stop_transfer, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeviceTransferFragment.$r8$lambda$V3Ppm6_W8qtMCdeXW7k4gwYyWVI(DeviceTransferFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    public /* synthetic */ void lambda$cancelActiveTransfer$2(DialogInterface dialogInterface, int i) {
        EventBus.getDefault().unregister(this.transferModeListener);
        DeviceToDeviceTransferService.stop(requireContext());
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
        navigateAwayFromTransfer();
    }

    public void ignoreTransferStatusEvents() {
        EventBus.getDefault().unregister(this.transferModeListener);
    }

    /* loaded from: classes.dex */
    public class TransferModeListener {
        private TransferModeListener() {
            DeviceTransferFragment.this = r1;
        }

        @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
        public void onEventMainThread(TransferStatus transferStatus) {
            if (transferStatus.getTransferMode() != TransferStatus.TransferMode.SERVICE_CONNECTED) {
                DeviceTransferFragment.this.abort();
            }
        }
    }

    public void abort() {
        abort(R.string.DeviceTransfer__transfer_failed);
    }

    public void abort(int i) {
        EventBus.getDefault().unregister(this.transferModeListener);
        DeviceToDeviceTransferService.stop(requireContext());
        this.progress.setVisibility(8);
        this.alert.setVisibility(0);
        this.tryAgain.setVisibility(0);
        this.title.setText(R.string.DeviceTransfer__unable_to_transfer);
        this.status.setText(i);
        this.cancel.setText(R.string.DeviceTransfer__cancel);
        this.cancel.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                DeviceTransferFragment.$r8$lambda$w9dA1zJz2YzArJh6MbRyp_EjSjw(DeviceTransferFragment.this, view);
            }
        });
        this.onBackPressed.isActiveTransfer = false;
    }

    public /* synthetic */ void lambda$abort$3(View view) {
        navigateAwayFromTransfer();
    }

    /* loaded from: classes4.dex */
    public class OnBackPressed extends OnBackPressedCallback {
        private boolean isActiveTransfer = true;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            DeviceTransferFragment.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            if (this.isActiveTransfer) {
                DeviceTransferFragment.this.cancelActiveTransfer();
            } else {
                DeviceTransferFragment.this.navigateAwayFromTransfer();
            }
        }
    }
}
