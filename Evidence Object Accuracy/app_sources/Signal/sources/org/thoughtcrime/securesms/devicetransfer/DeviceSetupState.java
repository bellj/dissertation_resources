package org.thoughtcrime.securesms.devicetransfer;

/* loaded from: classes4.dex */
public final class DeviceSetupState {
    private final int authenticationCode;
    private final SetupStep currentSetupStep;

    public DeviceSetupState() {
        this(SetupStep.INITIAL, 0);
    }

    public DeviceSetupState(SetupStep setupStep, int i) {
        this.currentSetupStep = setupStep;
        this.authenticationCode = i;
    }

    public SetupStep getCurrentSetupStep() {
        return this.currentSetupStep;
    }

    public int getAuthenticationCode() {
        return this.authenticationCode;
    }

    public DeviceSetupState updateStep(SetupStep setupStep) {
        return new DeviceSetupState(setupStep, this.authenticationCode);
    }

    public DeviceSetupState updateVerificationRequired(int i) {
        return new DeviceSetupState(SetupStep.VERIFY, i);
    }
}
