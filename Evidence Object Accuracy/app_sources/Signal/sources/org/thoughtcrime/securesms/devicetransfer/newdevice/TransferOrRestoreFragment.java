package org.thoughtcrime.securesms.devicetransfer.newdevice;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.navigation.Navigation;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class TransferOrRestoreFragment extends LoggingFragment {
    public TransferOrRestoreFragment() {
        super(R.layout.fragment_transfer_restore);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        view.findViewById(R.id.transfer_or_restore_fragment_transfer).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.newdevice.TransferOrRestoreFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TransferOrRestoreFragment.$r8$lambda$Bq3UTYiR_92vx96nXUZo1TlrHnQ(view2);
            }
        });
        View findViewById = view.findViewById(R.id.transfer_or_restore_fragment_restore);
        if (Build.VERSION.SDK_INT >= 21) {
            findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.newdevice.TransferOrRestoreFragment$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    TransferOrRestoreFragment.$r8$lambda$UEmSPiLOOiXE_zDVlNA2L9Y9gYM(view2);
                }
            });
        } else {
            findViewById.setVisibility(8);
        }
        ((TextView) view.findViewById(R.id.transfer_or_restore_fragment_transfer_description)).setText(SpanUtil.boldSubstring(getString(R.string.TransferOrRestoreFragment__transfer_your_account_and_messages_from_your_old_android_device), getString(R.string.TransferOrRestoreFragment__you_need_access_to_your_old_device)));
    }

    public static /* synthetic */ void lambda$onViewCreated$0(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_new_device_transfer_instructions);
    }

    public static /* synthetic */ void lambda$onViewCreated$1(View view) {
        SafeNavigation.safeNavigate(Navigation.findNavController(view), (int) R.id.action_choose_backup);
    }
}
