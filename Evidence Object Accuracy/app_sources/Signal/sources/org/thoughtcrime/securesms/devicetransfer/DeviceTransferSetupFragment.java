package org.thoughtcrime.securesms.devicetransfer;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.Group;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.fragment.NavHostFragment;
import com.google.android.material.button.MaterialButton;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.signal.devicetransfer.TransferStatus;
import org.signal.devicetransfer.WifiDirect;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes.dex */
public abstract class DeviceTransferSetupFragment extends LoggingFragment {
    private static final long PREPARE_TAKING_TOO_LONG_TIME;
    private static final String TAG = Log.tag(DeviceTransferSetupFragment.class);
    private static final long WAITING_TAKING_TOO_LONG_TIME;
    private final OnBackPressed onBackPressed = new OnBackPressed();
    private Runnable takingTooLong;
    private DeviceTransferSetupViewModel viewModel;

    protected abstract int getErrorResolveButtonTextForStep(SetupStep setupStep);

    protected abstract int getErrorTextForStep(SetupStep setupStep);

    protected abstract int getStatusTextForStep(SetupStep setupStep, boolean z);

    protected abstract void navigateAwayFromTransfer();

    protected abstract void navigateToTransferConnected();

    protected abstract void navigateWhenWifiDirectUnavailable();

    protected abstract void startTransfer();

    static {
        TAG = Log.tag(DeviceTransferSetupFragment.class);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        PREPARE_TAKING_TOO_LONG_TIME = timeUnit.toMillis(30);
        WAITING_TAKING_TOO_LONG_TIME = timeUnit.toMillis(90);
    }

    public DeviceTransferSetupFragment() {
        super(R.layout.device_transfer_setup_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        MaterialButton materialButton = (MaterialButton) view.findViewById(R.id.device_transfer_setup_fragment_sas_verify_yes);
        DeviceTransferSetupViewModel deviceTransferSetupViewModel = (DeviceTransferSetupViewModel) ViewModelProviders.of(this).get(DeviceTransferSetupViewModel.class);
        this.viewModel = deviceTransferSetupViewModel;
        deviceTransferSetupViewModel.getState().observe(getViewLifecycleOwner(), new Observer((Group) view.findViewById(R.id.device_transfer_setup_fragment_progress_group), (Group) view.findViewById(R.id.device_transfer_setup_fragment_error_group), view.findViewById(R.id.device_transfer_setup_fragment_verify), view.findViewById(R.id.device_transfer_setup_fragment_waiting), view.findViewById(R.id.device_transfer_setup_fragment_troubleshooting), (TextView) view.findViewById(R.id.device_transfer_setup_fragment_status), (TextView) view.findViewById(R.id.device_transfer_setup_fragment_error), (MaterialButton) view.findViewById(R.id.device_transfer_setup_fragment_error_resolve), (TextView) view.findViewById(R.id.device_transfer_setup_fragment_sas_verify_code), (MaterialButton) view.findViewById(R.id.device_transfer_setup_fragment_sas_verify_no), materialButton) { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ Group f$1;
            public final /* synthetic */ MaterialButton f$10;
            public final /* synthetic */ MaterialButton f$11;
            public final /* synthetic */ Group f$2;
            public final /* synthetic */ View f$3;
            public final /* synthetic */ View f$4;
            public final /* synthetic */ View f$5;
            public final /* synthetic */ TextView f$6;
            public final /* synthetic */ TextView f$7;
            public final /* synthetic */ MaterialButton f$8;
            public final /* synthetic */ TextView f$9;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r7;
                this.f$7 = r8;
                this.f$8 = r9;
                this.f$9 = r10;
                this.f$10 = r11;
                this.f$11 = r12;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DeviceTransferSetupFragment.m1765$r8$lambda$llAM6y7P0ITquaaPreDGY5LldI(DeviceTransferSetupFragment.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, this.f$8, this.f$9, this.f$10, this.f$11, (DeviceSetupState) obj);
            }
        });
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public /* synthetic */ void lambda$onViewCreated$19(Group group, Group group2, View view, View view2, View view3, TextView textView, TextView textView2, MaterialButton materialButton, TextView textView3, MaterialButton materialButton2, MaterialButton materialButton3, DeviceSetupState deviceSetupState) {
        SetupStep currentSetupStep = deviceSetupState.getCurrentSetupStep();
        int i = 8;
        group.setVisibility(currentSetupStep.isProgress() ? 0 : 8);
        group2.setVisibility(currentSetupStep.isError() ? 0 : 8);
        view.setVisibility(currentSetupStep == SetupStep.VERIFY ? 0 : 8);
        view2.setVisibility(currentSetupStep == SetupStep.WAITING_FOR_OTHER_TO_VERIFY ? 0 : 8);
        if (currentSetupStep == SetupStep.TROUBLESHOOTING) {
            i = 0;
        }
        view3.setVisibility(i);
        String str = TAG;
        Log.i(str, "Handling step: " + currentSetupStep.name());
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[currentSetupStep.ordinal()]) {
            case 1:
                textView.setText("");
                break;
            case 2:
                break;
            case 3:
                textView2.setText(getErrorTextForStep(currentSetupStep));
                materialButton.setText(R.string.DeviceTransferSetup__grant_location_permission);
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda5
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$M8Qz94Pe6Ruo_yz_xE2a4Pak1PI(DeviceTransferSetupFragment.this, view4);
                    }
                });
                return;
            case 4:
                verifyLocationEnabled();
                return;
            case 5:
                textView2.setText(getErrorTextForStep(currentSetupStep));
                materialButton.setText(R.string.DeviceTransferSetup__turn_on_location_services);
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda14
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$ejvGPX6wQGz0F7aQNJFQX7iDVYw(DeviceTransferSetupFragment.this, view4);
                    }
                });
                return;
            case 6:
                verifyWifiEnabled();
                return;
            case 7:
                textView2.setText(getErrorTextForStep(currentSetupStep));
                materialButton.setText(R.string.DeviceTransferSetup__turn_on_wifi);
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda15
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$_ITQNGG1eVEgj1ex0ohnKRoSuC0(DeviceTransferSetupFragment.this, view4);
                    }
                });
                return;
            case 8:
                verifyWifiDirectAvailable();
                return;
            case 9:
                textView2.setText(getErrorTextForStep(currentSetupStep));
                materialButton.setText(getErrorResolveButtonTextForStep(currentSetupStep));
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda16
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.m1763$r8$lambda$BAR3Nwzv4Sl84vqflm3v2e2BU(DeviceTransferSetupFragment.this, view4);
                    }
                });
                return;
            case 10:
                textView.setText(getStatusTextForStep(SetupStep.SETTING_UP, false));
                startTransfer();
                return;
            case 11:
                textView.setText(getStatusTextForStep(currentSetupStep, false));
                startTakingTooLong(new Runnable(textView, currentSetupStep) { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda17
                    public final /* synthetic */ TextView f$1;
                    public final /* synthetic */ SetupStep f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        DeviceTransferSetupFragment.m1764$r8$lambda$5eI1LQxQh3KPVE99AyOs2EQX4(DeviceTransferSetupFragment.this, this.f$1, this.f$2);
                    }
                }, PREPARE_TAKING_TOO_LONG_TIME);
                return;
            case 12:
                textView.setText(getStatusTextForStep(currentSetupStep, false));
                cancelTakingTooLong();
                startTakingTooLong(new Runnable() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda18
                    @Override // java.lang.Runnable
                    public final void run() {
                        DeviceTransferSetupFragment.$r8$lambda$94XTNRynsPL1Cs1zsFb5VivCoHE(DeviceTransferSetupFragment.this);
                    }
                }, WAITING_TAKING_TOO_LONG_TIME);
                return;
            case 13:
                cancelTakingTooLong();
                textView3.setText(String.format(Locale.US, "%07d", Integer.valueOf(deviceSetupState.getAuthenticationCode())));
                materialButton2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda19
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$inuOOHT1yTIKFHaUWEtZoJ4CiNU(DeviceTransferSetupFragment.this, view4);
                    }
                });
                materialButton3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda20
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$1fWk1YBzv0qaRVclBq3m_qBTvwg(DeviceTransferSetupFragment.this, view4);
                    }
                });
                return;
            case 14:
            default:
                return;
            case 15:
                Log.d(str, "Connected! isNotShutdown: " + this.viewModel.isNotShutdown());
                if (this.viewModel.isNotShutdown()) {
                    navigateToTransferConnected();
                    return;
                }
                return;
            case 16:
                ((TextView) view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_title)).setText(getStatusTextForStep(currentSetupStep, false));
                int dpToPx = ViewUtil.dpToPx(12);
                ((TextView) view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_step1)).setText(SpanUtil.bullet(getString(R.string.DeviceTransferSetup__make_sure_the_following_permissions_are_enabled), dpToPx));
                TextView textView4 = (TextView) view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_step2);
                textView4.setMovementMethod(LinkMovementMethod.getInstance());
                textView4.setText(SpanUtil.clickSubstring(requireContext(), SpanUtil.bullet(getString(R.string.DeviceTransferSetup__on_the_wifi_direct_screen_remove_all_remembered_groups_and_unlink_any_invited_or_connected_devices), dpToPx), getString(R.string.DeviceTransferSetup__wifi_direct_screen), new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda21
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$PJsAVo2tE7Eyner6QQ63Z76mKcg(DeviceTransferSetupFragment.this, view4);
                    }
                }));
                ((TextView) view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_step3)).setText(SpanUtil.bullet(getString(R.string.DeviceTransferSetup__try_turning_wifi_off_and_on_on_both_devices), dpToPx));
                ((TextView) view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_step4)).setText(SpanUtil.bullet(getString(R.string.DeviceTransferSetup__make_sure_both_devices_are_in_transfer_mode), dpToPx));
                view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_location_permission).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda22
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$OnA7mObCcQsBJ5xWBoESTSfAFls(DeviceTransferSetupFragment.this, view4);
                    }
                });
                view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_location_services).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda6
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.m1766$r8$lambda$nd6XojdoRmK2Dsg7BciJDvCG4A(DeviceTransferSetupFragment.this, view4);
                    }
                });
                view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_wifi).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda7
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$6vAuivDT7fteQN3W7KIZ9661qXY(DeviceTransferSetupFragment.this, view4);
                    }
                });
                view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_go_to_support).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda8
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$KbxNlci7PgLh3kLsXzqUl46nFBI(DeviceTransferSetupFragment.this, view4);
                    }
                });
                view3.findViewById(R.id.device_transfer_setup_fragment_troubleshooting_try_again).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda9
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$W8oVq_QpbapRQIdc4IuAtZ9DFis(DeviceTransferSetupFragment.this, view4);
                    }
                });
                return;
            case 17:
                textView2.setText(getErrorTextForStep(currentSetupStep));
                materialButton.setText(R.string.DeviceTransferSetup__retry);
                materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda10
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view4) {
                        DeviceTransferSetupFragment.$r8$lambda$Qq6_ZrKlLfWPxX5T6G_4S5Cjhko(DeviceTransferSetupFragment.this, view4);
                    }
                });
                DeviceToDeviceTransferService.stop(requireContext());
                cancelTakingTooLong();
                new AlertDialog.Builder(requireContext()).setTitle(R.string.DeviceTransferSetup__error_connecting).setMessage(getStatusTextForStep(currentSetupStep, false)).setPositiveButton(R.string.DeviceTransferSetup__retry, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda11
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        DeviceTransferSetupFragment.$r8$lambda$pakgblz5FBxeS_erMlt61IegOvk(DeviceTransferSetupFragment.this, dialogInterface, i2);
                    }
                }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda12
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        DeviceTransferSetupFragment.$r8$lambda$rHMbC3dPqLkgDmkmVpF4ITAc4tc(DeviceTransferSetupFragment.this, dialogInterface, i2);
                    }
                }).setNeutralButton(R.string.DeviceTransferSetup__submit_debug_logs, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda13
                    @Override // android.content.DialogInterface.OnClickListener
                    public final void onClick(DialogInterface dialogInterface, int i2) {
                        DeviceTransferSetupFragment.$r8$lambda$TRuCSDlp6bs_ebnQr5m4OcWP3mA(DeviceTransferSetupFragment.this, dialogInterface, i2);
                    }
                }).setCancelable(false).show();
                return;
        }
        requestLocationPermission();
    }

    /* renamed from: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep;

        static {
            int[] iArr = new int[SetupStep.values().length];
            $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep = iArr;
            try {
                iArr[SetupStep.INITIAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.PERMISSIONS_CHECK.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.PERMISSIONS_DENIED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.LOCATION_CHECK.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.LOCATION_DISABLED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WIFI_CHECK.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WIFI_DISABLED.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WIFI_DIRECT_CHECK.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WIFI_DIRECT_UNAVAILABLE.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.START.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.SETTING_UP.ordinal()] = 11;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WAITING.ordinal()] = 12;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.VERIFY.ordinal()] = 13;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.WAITING_FOR_OTHER_TO_VERIFY.ordinal()] = 14;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.CONNECTED.ordinal()] = 15;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.TROUBLESHOOTING.ordinal()] = 16;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$SetupStep[SetupStep.ERROR.ordinal()] = 17;
            } catch (NoSuchFieldError unused17) {
            }
        }
    }

    public /* synthetic */ void lambda$onViewCreated$0(View view) {
        this.viewModel.checkPermissions();
    }

    public /* synthetic */ void lambda$onViewCreated$1(View view) {
        openLocationServices();
    }

    public /* synthetic */ void lambda$onViewCreated$2(View view) {
        openWifiSettings();
    }

    public /* synthetic */ void lambda$onViewCreated$3(View view) {
        navigateWhenWifiDirectUnavailable();
    }

    public /* synthetic */ void lambda$onViewCreated$4(TextView textView, SetupStep setupStep) {
        textView.setText(getStatusTextForStep(setupStep, true));
    }

    public /* synthetic */ void lambda$onViewCreated$5() {
        DeviceToDeviceTransferService.stop(requireContext());
        this.viewModel.onWaitingTookTooLong();
    }

    public /* synthetic */ void lambda$onViewCreated$7(View view) {
        new AlertDialog.Builder(requireContext()).setTitle(R.string.DeviceTransferSetup__the_numbers_do_not_match).setMessage(R.string.DeviceTransferSetup__if_the_numbers_on_your_devices_do_not_match_its_possible_you_connected_to_the_wrong_device).setPositiveButton(R.string.DeviceTransferSetup__stop_transfer, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                DeviceTransferSetupFragment.$r8$lambda$vtJsZJysp8TujEipBLArsH9Z8lU(DeviceTransferSetupFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    public /* synthetic */ void lambda$onViewCreated$6(DialogInterface dialogInterface, int i) {
        EventBus.getDefault().unregister(this);
        DeviceToDeviceTransferService.setAuthenticationCodeVerified(requireContext(), false);
        DeviceToDeviceTransferService.stop(requireContext());
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
        navigateAwayFromTransfer();
    }

    public /* synthetic */ void lambda$onViewCreated$8(View view) {
        DeviceToDeviceTransferService.setAuthenticationCodeVerified(requireContext(), true);
        this.viewModel.onVerified();
    }

    public /* synthetic */ void lambda$onViewCreated$9(View view) {
        openWifiDirectSettings();
    }

    public /* synthetic */ void lambda$onViewCreated$10(View view) {
        openApplicationSystemSettings();
    }

    public /* synthetic */ void lambda$onViewCreated$11(View view) {
        openLocationServices();
    }

    public /* synthetic */ void lambda$onViewCreated$12(View view) {
        openWifiSettings();
    }

    public /* synthetic */ void lambda$onViewCreated$13(View view) {
        gotoSupport();
    }

    public /* synthetic */ void lambda$onViewCreated$14(View view) {
        this.viewModel.checkPermissions();
    }

    public /* synthetic */ void lambda$onViewCreated$15(View view) {
        this.viewModel.checkPermissions();
    }

    public /* synthetic */ void lambda$onViewCreated$16(DialogInterface dialogInterface, int i) {
        this.viewModel.checkPermissions();
    }

    public /* synthetic */ void lambda$onViewCreated$17(DialogInterface dialogInterface, int i) {
        EventBus.getDefault().unregister(this);
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
        navigateAwayFromTransfer();
    }

    public /* synthetic */ void lambda$onViewCreated$18(DialogInterface dialogInterface, int i) {
        EventBus.getDefault().unregister(this);
        EventBus.getDefault().removeStickyEvent(TransferStatus.class);
        navigateAwayFromTransfer();
        startActivity(new Intent(requireContext(), SubmitDebugLogActivity.class));
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), this.onBackPressed);
        if (((TransferStatus) EventBus.getDefault().getStickyEvent(TransferStatus.class)) == null) {
            this.viewModel.checkPermissions();
        } else {
            Log.i(TAG, "Sticky event already exists for transfer, assuming service is running and we are reattaching");
        }
        EventBus.getDefault().register(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        this.viewModel.onResume();
    }

    @Override // androidx.fragment.app.Fragment
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        Permissions.onRequestPermissionsResult(this, i, strArr, iArr);
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        cancelTakingTooLong();
        EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    private void requestLocationPermission() {
        Permissions.PermissionsBuilder ifNecessary = Permissions.with(this).request("android.permission.ACCESS_FINE_LOCATION").ifNecessary();
        SetupStep setupStep = SetupStep.PERMISSIONS_DENIED;
        ifNecessary.withRationaleDialog(getString(getErrorTextForStep(setupStep)), false, R.drawable.ic_location_on_white_24dp).withPermanentDenialDialog(getString(getErrorTextForStep(setupStep))).onAllGranted(new Runnable() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda3
            @Override // java.lang.Runnable
            public final void run() {
                DeviceTransferSetupFragment.$r8$lambda$v_gMdPXikJQ4SS6j5sIQ3bnQhZ8(DeviceTransferSetupFragment.this);
            }
        }).onAnyDenied(new Runnable() { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda4
            @Override // java.lang.Runnable
            public final void run() {
                DeviceTransferSetupFragment.m1767$r8$lambda$ykqphZ8LPwUWRabXA3kjlJM3fA(DeviceTransferSetupFragment.this);
            }
        }).execute();
    }

    public /* synthetic */ void lambda$requestLocationPermission$20() {
        this.viewModel.onPermissionsGranted();
    }

    public /* synthetic */ void lambda$requestLocationPermission$21() {
        this.viewModel.onLocationPermissionDenied();
    }

    private void openApplicationSystemSettings() {
        startActivity(Permissions.getApplicationSettingsIntent(requireContext()));
    }

    private void verifyLocationEnabled() {
        LocationManager locationManager = (LocationManager) ContextCompat.getSystemService(requireContext(), LocationManager.class);
        if (locationManager == null || !locationManager.isProviderEnabled("gps")) {
            this.viewModel.onLocationDisabled();
        } else {
            this.viewModel.onLocationEnabled();
        }
    }

    private void openLocationServices() {
        try {
            startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "No location settings", e);
            Toast.makeText(requireContext(), (int) R.string.DeviceTransferSetup__unable_to_open_wifi_settings, 1).show();
        }
    }

    private void verifyWifiEnabled() {
        WifiManager wifiManager = (WifiManager) ContextCompat.getSystemService(requireContext(), WifiManager.class);
        if (wifiManager == null || !wifiManager.isWifiEnabled()) {
            this.viewModel.onWifiDisabled(wifiManager == null);
        } else {
            this.viewModel.onWifiEnabled();
        }
    }

    private void openWifiSettings() {
        try {
            startActivity(new Intent("android.settings.WIFI_SETTINGS"));
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "No wifi settings", e);
            Toast.makeText(requireContext(), (int) R.string.DeviceTransferSetup__unable_to_open_wifi_settings, 1).show();
        }
    }

    private void openWifiDirectSettings() {
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY).setClassName("com.android.settings", "com.android.settings.Settings$WifiP2pSettingsActivity");
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "Unable to open wifi direct settings", e);
            openWifiSettings();
        }
    }

    private void verifyWifiDirectAvailable() {
        WifiDirect.AvailableStatus availability = WifiDirect.getAvailability(requireContext());
        if (availability != WifiDirect.AvailableStatus.AVAILABLE) {
            this.viewModel.onWifiDirectUnavailable(availability);
        } else {
            this.viewModel.onWifiDirectAvailable();
        }
    }

    private void gotoSupport() {
        CommunicationActions.openBrowserLink(requireContext(), getString(R.string.transfer_support_url));
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventMainThread(TransferStatus transferStatus) {
        this.viewModel.onTransferEvent(transferStatus);
    }

    private void startTakingTooLong(Runnable runnable, long j) {
        if (this.takingTooLong == null) {
            DeviceTransferSetupFragment$$ExternalSyntheticLambda2 deviceTransferSetupFragment$$ExternalSyntheticLambda2 = new Runnable(runnable) { // from class: org.thoughtcrime.securesms.devicetransfer.DeviceTransferSetupFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ Runnable f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    DeviceTransferSetupFragment.$r8$lambda$i75mdUZqbdfV0h4CK281qSllxpM(DeviceTransferSetupFragment.this, this.f$1);
                }
            };
            this.takingTooLong = deviceTransferSetupFragment$$ExternalSyntheticLambda2;
            ThreadUtil.runOnMainDelayed(deviceTransferSetupFragment$$ExternalSyntheticLambda2, j);
        }
    }

    public /* synthetic */ void lambda$startTakingTooLong$22(Runnable runnable) {
        this.takingTooLong = null;
        runnable.run();
    }

    private void cancelTakingTooLong() {
        Runnable runnable = this.takingTooLong;
        if (runnable != null) {
            ThreadUtil.cancelRunnableOnMain(runnable);
            this.takingTooLong = null;
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            DeviceTransferSetupFragment.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            DeviceToDeviceTransferService.stop(DeviceTransferSetupFragment.this.requireContext());
            EventBus.getDefault().removeStickyEvent(TransferStatus.class);
            NavHostFragment.findNavController(DeviceTransferSetupFragment.this).popBackStack();
        }
    }
}
