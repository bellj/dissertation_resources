package org.thoughtcrime.securesms.devicetransfer.newdevice;

import android.os.Bundle;
import android.view.View;
import androidx.navigation.fragment.NavHostFragment;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.devicetransfer.DeviceToDeviceTransferService;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment;
import org.thoughtcrime.securesms.devicetransfer.newdevice.NewDeviceServerTask;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* loaded from: classes4.dex */
public final class NewDeviceTransferFragment extends DeviceTransferFragment {
    private final ServerTaskListener serverTaskListener = new ServerTaskListener(this, null);

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        EventBus.getDefault().register(this.serverTaskListener);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        EventBus.getDefault().unregister(this.serverTaskListener);
        super.onDestroyView();
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment
    protected void navigateToRestartTransfer() {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_newDeviceTransfer_to_newDeviceTransferInstructions);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment
    protected void navigateAwayFromTransfer() {
        EventBus.getDefault().unregister(this.serverTaskListener);
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_restart_to_welcomeFragment);
    }

    @Override // org.thoughtcrime.securesms.devicetransfer.DeviceTransferFragment
    protected void navigateToTransferComplete() {
        SafeNavigation.safeNavigate(NavHostFragment.findNavController(this), (int) R.id.action_newDeviceTransfer_to_newDeviceTransferComplete);
    }

    /* loaded from: classes.dex */
    private class ServerTaskListener {
        private ServerTaskListener() {
            NewDeviceTransferFragment.this = r1;
        }

        /* synthetic */ ServerTaskListener(NewDeviceTransferFragment newDeviceTransferFragment, AnonymousClass1 r2) {
            this();
        }

        @Subscribe(threadMode = ThreadMode.MAIN)
        public void onEventMainThread(NewDeviceServerTask.Status status) {
            ((DeviceTransferFragment) NewDeviceTransferFragment.this).status.setText(NewDeviceTransferFragment.this.getString(R.string.DeviceTransfer__d_messages_so_far, Long.valueOf(status.getMessageCount())));
            int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$devicetransfer$newdevice$NewDeviceServerTask$Status$State[status.getState().ordinal()];
            if (i == 2) {
                ((DeviceTransferFragment) NewDeviceTransferFragment.this).transferFinished = true;
                DeviceToDeviceTransferService.stop(NewDeviceTransferFragment.this.requireContext());
                NewDeviceTransferFragment.this.navigateToTransferComplete();
            } else if (i == 3) {
                NewDeviceTransferFragment.this.abort(R.string.NewDeviceTransfer__cannot_transfer_from_a_newer_version_of_signal);
            } else if (i == 4) {
                NewDeviceTransferFragment.this.abort();
            }
        }
    }

    /* renamed from: org.thoughtcrime.securesms.devicetransfer.newdevice.NewDeviceTransferFragment$1 */
    /* loaded from: classes4.dex */
    static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$devicetransfer$newdevice$NewDeviceServerTask$Status$State;

        static {
            int[] iArr = new int[NewDeviceServerTask.Status.State.values().length];
            $SwitchMap$org$thoughtcrime$securesms$devicetransfer$newdevice$NewDeviceServerTask$Status$State = iArr;
            try {
                iArr[NewDeviceServerTask.Status.State.IN_PROGRESS.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$newdevice$NewDeviceServerTask$Status$State[NewDeviceServerTask.Status.State.SUCCESS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$newdevice$NewDeviceServerTask$Status$State[NewDeviceServerTask.Status.State.FAILURE_VERSION_DOWNGRADE.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$devicetransfer$newdevice$NewDeviceServerTask$Status$State[NewDeviceServerTask.Status.State.FAILURE_UNKNOWN.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }
}
