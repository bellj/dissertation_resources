package org.thoughtcrime.securesms.devicetransfer.olddevice;

import android.content.Context;
import java.io.IOException;
import java.io.OutputStream;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.signal.devicetransfer.ClientTask;
import org.thoughtcrime.securesms.backup.FullBackupBase;
import org.thoughtcrime.securesms.backup.FullBackupExporter;
import org.thoughtcrime.securesms.crypto.AttachmentSecretProvider;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.net.DeviceTransferBlockingInterceptor;

/* loaded from: classes.dex */
final class OldDeviceClientTask implements ClientTask {
    private static final long PROGRESS_UPDATE_THROTTLE;
    private static final String TAG = Log.tag(OldDeviceClientTask.class);
    private long lastProgressUpdate = 0;

    /* JADX INFO: finally extract failed */
    @Override // org.signal.devicetransfer.ClientTask
    public void run(Context context, OutputStream outputStream) throws IOException {
        try {
            DeviceTransferBlockingInterceptor.getInstance().blockNetwork();
            long currentTimeMillis = System.currentTimeMillis();
            EventBus.getDefault().register(this);
            try {
                FullBackupExporter.transfer(context, AttachmentSecretProvider.getInstance(context).getOrCreateAttachmentSecret(), SignalDatabase.getBackupDatabase(), outputStream, "deadbeef");
                EventBus.getDefault().unregister(this);
                long currentTimeMillis2 = System.currentTimeMillis();
                String str = TAG;
                Log.i(str, "Sending took: " + (currentTimeMillis2 - currentTimeMillis));
            } catch (Exception e) {
                DeviceTransferBlockingInterceptor.getInstance().unblockNetwork();
                throw e;
            }
        } catch (Throwable th) {
            EventBus.getDefault().unregister(this);
            throw th;
        }
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onEvent(FullBackupBase.BackupEvent backupEvent) {
        if (backupEvent.getType() == FullBackupBase.BackupEvent.Type.PROGRESS && System.currentTimeMillis() > this.lastProgressUpdate + PROGRESS_UPDATE_THROTTLE) {
            EventBus.getDefault().post(new Status(backupEvent.getCount(), backupEvent.getEstimatedTotalCount(), backupEvent.getCompletionPercentage(), false));
            this.lastProgressUpdate = System.currentTimeMillis();
        }
    }

    @Override // org.signal.devicetransfer.ClientTask
    public void success() {
        SignalStore.misc().markOldDeviceTransferLocked();
        EventBus.getDefault().post(new Status(0, 0, 0.0d, true));
    }

    /* loaded from: classes4.dex */
    public static final class Status {
        private final double completionPercentage;
        private final boolean done;
        private final long estimatedMessages;
        private final long messages;

        public Status(long j, long j2, double d, boolean z) {
            this.messages = j;
            this.estimatedMessages = j2;
            this.completionPercentage = d;
            this.done = z;
        }

        public long getMessageCount() {
            return this.messages;
        }

        public long getEstimatedMessageCount() {
            return this.estimatedMessages;
        }

        public double getCompletionPercentage() {
            return this.completionPercentage;
        }

        public boolean isDone() {
            return this.done;
        }
    }
}
