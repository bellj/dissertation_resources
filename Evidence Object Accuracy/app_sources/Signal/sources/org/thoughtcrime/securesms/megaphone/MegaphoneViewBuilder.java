package org.thoughtcrime.securesms.megaphone;

import android.content.Context;
import android.view.View;
import org.thoughtcrime.securesms.megaphone.Megaphone;

/* loaded from: classes4.dex */
public class MegaphoneViewBuilder {

    /* renamed from: org.thoughtcrime.securesms.megaphone.MegaphoneViewBuilder$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphone$Style;

        static {
            int[] iArr = new int[Megaphone.Style.values().length];
            $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphone$Style = iArr;
            try {
                iArr[Megaphone.Style.BASIC.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphone$Style[Megaphone.Style.FULLSCREEN.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphone$Style[Megaphone.Style.ONBOARDING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphone$Style[Megaphone.Style.POPUP.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
        }
    }

    public static View build(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphone$Style[megaphone.getStyle().ordinal()];
        if (i == 1) {
            return buildBasicMegaphone(context, megaphone, megaphoneActionController);
        }
        if (i == 2) {
            return null;
        }
        if (i == 3) {
            return buildOnboardingMegaphone(context, megaphone, megaphoneActionController);
        }
        if (i == 4) {
            return buildPopupMegaphone(context, megaphone, megaphoneActionController);
        }
        throw new IllegalArgumentException("No view implemented for style!");
    }

    private static View buildBasicMegaphone(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        BasicMegaphoneView basicMegaphoneView = new BasicMegaphoneView(context);
        basicMegaphoneView.present(megaphone, megaphoneActionController);
        return basicMegaphoneView;
    }

    private static View buildOnboardingMegaphone(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        OnboardingMegaphoneView onboardingMegaphoneView = new OnboardingMegaphoneView(context);
        onboardingMegaphoneView.present(megaphone, megaphoneActionController);
        return onboardingMegaphoneView;
    }

    private static View buildPopupMegaphone(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        PopupMegaphoneView popupMegaphoneView = new PopupMegaphoneView(context);
        popupMegaphoneView.present(megaphone, megaphoneActionController);
        return popupMegaphoneView;
    }
}
