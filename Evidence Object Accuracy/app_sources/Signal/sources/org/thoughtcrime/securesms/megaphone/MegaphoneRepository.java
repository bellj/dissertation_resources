package org.thoughtcrime.securesms.megaphone;

import android.app.Application;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.MegaphoneDatabase;
import org.thoughtcrime.securesms.database.model.MegaphoneRecord;
import org.thoughtcrime.securesms.megaphone.MegaphoneRepository;
import org.thoughtcrime.securesms.megaphone.Megaphones;

/* loaded from: classes.dex */
public class MegaphoneRepository {
    private final Application context;
    private final MegaphoneDatabase database;
    private final Map<Megaphones.Event, MegaphoneRecord> databaseCache = new HashMap();
    private boolean enabled;
    private final Executor executor;

    /* loaded from: classes4.dex */
    public interface Callback<E> {
        void onResult(E e);
    }

    public static /* synthetic */ MegaphoneRecord lambda$resetDatabaseCache$6(MegaphoneRecord megaphoneRecord) {
        return megaphoneRecord;
    }

    public MegaphoneRepository(Application application) {
        this.context = application;
        ExecutorService executorService = SignalExecutors.SERIAL;
        this.executor = executorService;
        this.database = MegaphoneDatabase.getInstance(application);
        executorService.execute(new Runnable() { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.init();
            }
        });
    }

    public void onFirstEverAppLaunch() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.lambda$onFirstEverAppLaunch$0();
            }
        });
    }

    public /* synthetic */ void lambda$onFirstEverAppLaunch$0() {
        this.database.markFinished(Megaphones.Event.ADD_A_PROFILE_PHOTO);
        resetDatabaseCache();
    }

    public /* synthetic */ void lambda$onAppForegrounded$1() {
        this.enabled = true;
    }

    public void onAppForegrounded() {
        this.executor.execute(new Runnable() { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.lambda$onAppForegrounded$1();
            }
        });
    }

    public void getNextMegaphone(Callback<Megaphone> callback) {
        this.executor.execute(new Runnable(callback) { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ MegaphoneRepository.Callback f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.lambda$getNextMegaphone$2(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$getNextMegaphone$2(Callback callback) {
        if (this.enabled) {
            init();
            callback.onResult(Megaphones.getNextMegaphone(this.context, this.databaseCache));
            return;
        }
        callback.onResult(null);
    }

    public void markVisible(Megaphones.Event event) {
        this.executor.execute(new Runnable(event, System.currentTimeMillis()) { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Megaphones.Event f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.lambda$markVisible$3(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$markVisible$3(Megaphones.Event event, long j) {
        if (getRecord(event).getFirstVisible() == 0) {
            this.database.markFirstVisible(event, j);
            resetDatabaseCache();
        }
    }

    public void markSeen(Megaphones.Event event) {
        this.executor.execute(new Runnable(event, System.currentTimeMillis()) { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ Megaphones.Event f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.lambda$markSeen$4(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$markSeen$4(Megaphones.Event event, long j) {
        this.database.markSeen(event, getRecord(event).getSeenCount() + 1, j);
        this.enabled = false;
        resetDatabaseCache();
    }

    public void markFinished(Megaphones.Event event) {
        markFinished(event, null);
    }

    public void markFinished(Megaphones.Event event, Runnable runnable) {
        this.executor.execute(new Runnable(event, runnable) { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ Megaphones.Event f$1;
            public final /* synthetic */ Runnable f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                MegaphoneRepository.this.lambda$markFinished$5(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$markFinished$5(Megaphones.Event event, Runnable runnable) {
        MegaphoneRecord megaphoneRecord = this.databaseCache.get(event);
        if (megaphoneRecord == null || !megaphoneRecord.isFinished()) {
            this.database.markFinished(event);
            resetDatabaseCache();
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public void init() {
        Set set = (Set) Stream.of(this.database.getAllAndDeleteMissing()).map(new MegaphoneRepository$$ExternalSyntheticLambda6()).collect(Collectors.toSet());
        Stream of = Stream.of(Megaphones.Event.values());
        Objects.requireNonNull(set);
        this.database.insert((Set) of.filterNot(new Predicate(set) { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ Set f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return this.f$0.contains((Megaphones.Event) obj);
            }
        }).collect(Collectors.toSet()));
        resetDatabaseCache();
    }

    private MegaphoneRecord getRecord(Megaphones.Event event) {
        return this.databaseCache.get(event);
    }

    private void resetDatabaseCache() {
        this.databaseCache.clear();
        this.databaseCache.putAll((Map) Stream.of(this.database.getAllAndDeleteMissing()).collect(Collectors.toMap(new MegaphoneRepository$$ExternalSyntheticLambda6(), new Function() { // from class: org.thoughtcrime.securesms.megaphone.MegaphoneRepository$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MegaphoneRepository.lambda$resetDatabaseCache$6((MegaphoneRecord) obj);
            }
        })));
    }
}
