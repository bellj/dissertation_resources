package org.thoughtcrime.securesms.megaphone;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: MegaphoneText.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\b\b\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u001b\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\n\u001a\u00020\u0003HÂ\u0003J\u000b\u0010\u000b\u001a\u0004\u0018\u00010\u0005HÂ\u0003J\u001f\u0010\f\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÖ\u0001J\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u00052\u0006\u0010\u0011\u001a\u00020\u0012J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0013\u0010\u0007\u001a\u00020\b8G¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\tR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/megaphone/MegaphoneText;", "", "stringRes", "", "string", "", "(ILjava/lang/String;)V", "hasText", "", "()Z", "component1", "component2", "copy", "equals", "other", "hashCode", "resolve", "context", "Landroid/content/Context;", "toString", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MegaphoneText {
    public static final Companion Companion = new Companion(null);
    private final boolean hasText;
    private final String string;
    private final int stringRes;

    public MegaphoneText() {
        this(0, null, 3, null);
    }

    private final int component1() {
        return this.stringRes;
    }

    private final String component2() {
        return this.string;
    }

    public static /* synthetic */ MegaphoneText copy$default(MegaphoneText megaphoneText, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = megaphoneText.stringRes;
        }
        if ((i2 & 2) != 0) {
            str = megaphoneText.string;
        }
        return megaphoneText.copy(i, str);
    }

    @JvmStatic
    public static final MegaphoneText from(int i) {
        return Companion.from(i);
    }

    @JvmStatic
    public static final MegaphoneText from(String str) {
        return Companion.from(str);
    }

    public final MegaphoneText copy(int i, String str) {
        return new MegaphoneText(i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MegaphoneText)) {
            return false;
        }
        MegaphoneText megaphoneText = (MegaphoneText) obj;
        return this.stringRes == megaphoneText.stringRes && Intrinsics.areEqual(this.string, megaphoneText.string);
    }

    public int hashCode() {
        int i = this.stringRes * 31;
        String str = this.string;
        return i + (str == null ? 0 : str.hashCode());
    }

    public String toString() {
        return "MegaphoneText(stringRes=" + this.stringRes + ", string=" + this.string + ')';
    }

    public MegaphoneText(int i, String str) {
        this.stringRes = i;
        this.string = str;
        this.hasText = (i == 0 && str == null) ? false : true;
    }

    public /* synthetic */ MegaphoneText(int i, String str, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? 0 : i, (i2 & 2) != 0 ? null : str);
    }

    public final boolean hasText() {
        return this.hasText;
    }

    public final String resolve(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        int i = this.stringRes;
        return i != 0 ? context.getString(i) : this.string;
    }

    /* compiled from: MegaphoneText.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0005\u001a\u00020\u0006H\u0007J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0007\u001a\u00020\bH\u0007¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/megaphone/MegaphoneText$Companion;", "", "()V", "from", "Lorg/thoughtcrime/securesms/megaphone/MegaphoneText;", "stringRes", "", "string", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final MegaphoneText from(int i) {
            return new MegaphoneText(i, null, 2, null);
        }

        @JvmStatic
        public final MegaphoneText from(String str) {
            Intrinsics.checkNotNullParameter(str, "string");
            return new MegaphoneText(0, str, 1, null);
        }
    }
}
