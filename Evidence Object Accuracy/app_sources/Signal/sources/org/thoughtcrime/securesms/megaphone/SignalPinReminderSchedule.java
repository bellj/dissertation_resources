package org.thoughtcrime.securesms.megaphone;

import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public final class SignalPinReminderSchedule implements MegaphoneSchedule {
    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneSchedule
    public boolean shouldDisplay(int i, long j, long j2, long j3) {
        if (SignalStore.kbsValues().hasOptedOut() || !SignalStore.kbsValues().hasPin() || !SignalStore.pinValues().arePinRemindersEnabled() || !SignalStore.account().isRegistered()) {
            return false;
        }
        if (j3 - SignalStore.pinValues().getLastSuccessfulEntryTime() >= SignalStore.pinValues().getCurrentInterval()) {
            return true;
        }
        return false;
    }
}
