package org.thoughtcrime.securesms.megaphone;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.AlertDialog;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.PlayStoreUtil;

/* loaded from: classes4.dex */
public class ClientDeprecatedActivity extends PassphraseRequiredActivity {
    private final DynamicTheme theme = new DynamicNoActionBarTheme();

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.client_deprecated_activity);
        findViewById(R.id.client_deprecated_update_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.megaphone.ClientDeprecatedActivity$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ClientDeprecatedActivity.$r8$lambda$5u2BUM3Gcgs7ThqFYXpDq0xvf5Q(ClientDeprecatedActivity.this, view);
            }
        });
        findViewById(R.id.client_deprecated_dont_update_button).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.megaphone.ClientDeprecatedActivity$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ClientDeprecatedActivity.m2295$r8$lambda$H_yHXuOH0VQlnmznf87baMigZs(ClientDeprecatedActivity.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$onCreate$0(View view) {
        onUpdateClicked();
    }

    public /* synthetic */ void lambda$onCreate$1(View view) {
        onDontUpdateClicked();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.theme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.theme.onResume(this);
    }

    private void onUpdateClicked() {
        PlayStoreUtil.openPlayStoreOrOurApkDownloadPage(this);
    }

    private void onDontUpdateClicked() {
        new AlertDialog.Builder(this).setTitle(R.string.ClientDeprecatedActivity_warning).setMessage(R.string.ClientDeprecatedActivity_your_version_of_signal_has_expired_you_can_view_your_message_history).setPositiveButton(R.string.ClientDeprecatedActivity_dont_update, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.megaphone.ClientDeprecatedActivity$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ClientDeprecatedActivity.$r8$lambda$L1CdPeylrOS9O5Qbmk6F5tyAZs8(ClientDeprecatedActivity.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.megaphone.ClientDeprecatedActivity$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ClientDeprecatedActivity.m2296$r8$lambda$uXtxm2phaTvhQFKWa4hQvsgyw(dialogInterface, i);
            }
        }).show();
    }

    public /* synthetic */ void lambda$onDontUpdateClicked$3(DialogInterface dialogInterface, int i) {
        ApplicationDependencies.getMegaphoneRepository().markFinished(Megaphones.Event.CLIENT_DEPRECATED, new Runnable() { // from class: org.thoughtcrime.securesms.megaphone.ClientDeprecatedActivity$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                ClientDeprecatedActivity.$r8$lambda$pfRmrHTyatNbEUtQwIMdwahWd64(ClientDeprecatedActivity.this);
            }
        });
    }

    public /* synthetic */ void lambda$onDontUpdateClicked$2() {
        ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.megaphone.ClientDeprecatedActivity$$ExternalSyntheticLambda5
            @Override // java.lang.Runnable
            public final void run() {
                ClientDeprecatedActivity.this.finish();
            }
        });
    }
}
