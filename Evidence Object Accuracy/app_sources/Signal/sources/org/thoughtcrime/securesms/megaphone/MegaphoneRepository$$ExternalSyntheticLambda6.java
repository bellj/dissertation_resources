package org.thoughtcrime.securesms.megaphone;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.database.model.MegaphoneRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class MegaphoneRepository$$ExternalSyntheticLambda6 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ((MegaphoneRecord) obj).getEvent();
    }
}
