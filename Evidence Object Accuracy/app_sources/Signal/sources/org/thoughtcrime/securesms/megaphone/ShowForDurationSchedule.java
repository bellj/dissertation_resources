package org.thoughtcrime.securesms.megaphone;

import java.util.concurrent.TimeUnit;

/* loaded from: classes4.dex */
public class ShowForDurationSchedule implements MegaphoneSchedule {
    private final long duration;

    public static MegaphoneSchedule showForDays(int i) {
        return new ShowForDurationSchedule(TimeUnit.DAYS.toMillis((long) i));
    }

    public ShowForDurationSchedule(long j) {
        this.duration = j;
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneSchedule
    public boolean shouldDisplay(int i, long j, long j2, long j3) {
        return j2 == 0 || j3 < j2 + this.duration;
    }
}
