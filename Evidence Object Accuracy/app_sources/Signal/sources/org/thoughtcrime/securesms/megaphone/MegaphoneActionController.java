package org.thoughtcrime.securesms.megaphone;

import android.app.Activity;
import android.content.Intent;
import androidx.fragment.app.DialogFragment;
import org.thoughtcrime.securesms.megaphone.Megaphones;

/* loaded from: classes4.dex */
public interface MegaphoneActionController {
    Activity getMegaphoneActivity();

    void onMegaphoneCompleted(Megaphones.Event event);

    void onMegaphoneDialogFragmentRequested(DialogFragment dialogFragment);

    void onMegaphoneNavigationRequested(Intent intent);

    void onMegaphoneNavigationRequested(Intent intent, int i);

    void onMegaphoneSnooze(Megaphones.Event event);

    void onMegaphoneToastRequested(String str);
}
