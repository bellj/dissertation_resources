package org.thoughtcrime.securesms.megaphone;

/* loaded from: classes4.dex */
final class ForeverSchedule implements MegaphoneSchedule {
    private final boolean enabled;

    public ForeverSchedule(boolean z) {
        this.enabled = z;
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneSchedule
    public boolean shouldDisplay(int i, long j, long j2, long j3) {
        return this.enabled;
    }
}
