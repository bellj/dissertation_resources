package org.thoughtcrime.securesms.megaphone;

import android.view.View;
import org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class OnboardingMegaphoneView$CardViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ OnboardingMegaphoneView.CardViewHolder f$0;
    public final /* synthetic */ MegaphoneActionController f$1;
    public final /* synthetic */ OnboardingMegaphoneView.ActionClickListener f$2;

    public /* synthetic */ OnboardingMegaphoneView$CardViewHolder$$ExternalSyntheticLambda0(OnboardingMegaphoneView.CardViewHolder cardViewHolder, MegaphoneActionController megaphoneActionController, OnboardingMegaphoneView.ActionClickListener actionClickListener) {
        this.f$0 = cardViewHolder;
        this.f$1 = megaphoneActionController;
        this.f$2 = actionClickListener;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, this.f$2, view);
    }
}
