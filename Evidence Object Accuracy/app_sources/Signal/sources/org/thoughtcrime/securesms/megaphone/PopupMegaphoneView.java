package org.thoughtcrime.securesms.megaphone;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.airbnb.lottie.LottieAnimationView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PopupMegaphoneView extends FrameLayout {
    private TextView bodyText;
    private LottieAnimationView image;
    private Megaphone megaphone;
    private MegaphoneActionController megaphoneListener;
    private TextView titleText;
    private View xButton;

    public PopupMegaphoneView(Context context) {
        super(context);
        init(context);
    }

    public PopupMegaphoneView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        FrameLayout.inflate(context, R.layout.popup_megaphone_view, this);
        this.image = (LottieAnimationView) findViewById(R.id.popup_megaphone_image);
        this.titleText = (TextView) findViewById(R.id.popup_megaphone_title);
        this.bodyText = (TextView) findViewById(R.id.popup_megaphone_body);
        this.xButton = findViewById(R.id.popup_x);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Megaphone megaphone = this.megaphone;
        if (megaphone != null && this.megaphoneListener != null && megaphone.getOnVisibleListener() != null) {
            this.megaphone.getOnVisibleListener().onEvent(this.megaphone, this.megaphoneListener);
        }
    }

    public void present(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        this.megaphone = megaphone;
        this.megaphoneListener = megaphoneActionController;
        if (megaphone.getImageRequest() != null) {
            this.image.setVisibility(0);
            megaphone.getImageRequest().into(this.image);
        } else if (megaphone.getLottieRes() != 0) {
            this.image.setVisibility(0);
            this.image.setAnimation(megaphone.getLottieRes());
            this.image.playAnimation();
        } else {
            this.image.setVisibility(8);
        }
        if (megaphone.getTitle().hasText()) {
            this.titleText.setVisibility(0);
            this.titleText.setText(megaphone.getTitle().resolve(getContext()));
        } else {
            this.titleText.setVisibility(8);
        }
        if (megaphone.getBody().hasText()) {
            this.bodyText.setVisibility(0);
            this.bodyText.setText(megaphone.getBody().resolve(getContext()));
        } else {
            this.bodyText.setVisibility(8);
        }
        if (megaphone.hasButton()) {
            this.xButton.setOnClickListener(new View.OnClickListener(megaphoneActionController) { // from class: org.thoughtcrime.securesms.megaphone.PopupMegaphoneView$$ExternalSyntheticLambda0
                public final /* synthetic */ MegaphoneActionController f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PopupMegaphoneView.lambda$present$0(Megaphone.this, this.f$1, view);
                }
            });
        } else {
            this.xButton.setOnClickListener(new View.OnClickListener(megaphone) { // from class: org.thoughtcrime.securesms.megaphone.PopupMegaphoneView$$ExternalSyntheticLambda1
                public final /* synthetic */ Megaphone f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    PopupMegaphoneView.lambda$present$1(MegaphoneActionController.this, this.f$1, view);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$present$0(Megaphone megaphone, MegaphoneActionController megaphoneActionController, View view) {
        megaphone.getButtonClickListener().onEvent(megaphone, megaphoneActionController);
    }

    public static /* synthetic */ void lambda$present$1(MegaphoneActionController megaphoneActionController, Megaphone megaphone, View view) {
        megaphoneActionController.onMegaphoneCompleted(megaphone.getEvent());
    }
}
