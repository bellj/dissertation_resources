package org.thoughtcrime.securesms.megaphone;

import android.graphics.drawable.Drawable;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.mms.GlideRequest;

/* loaded from: classes4.dex */
public class Megaphone {
    private final MegaphoneText bodyText;
    private final EventListener buttonListener;
    private final MegaphoneText buttonText;
    private final boolean canSnooze;
    private final Megaphones.Event event;
    private final GlideRequest<Drawable> imageRequest;
    private final int imageRes;
    private final int lottieRes;
    private final EventListener onVisibleListener;
    private final EventListener secondaryButtonListener;
    private final MegaphoneText secondaryButtonText;
    private final EventListener snoozeListener;
    private final Style style;
    private final MegaphoneText titleText;

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController);
    }

    /* loaded from: classes4.dex */
    public enum Style {
        ONBOARDING,
        BASIC,
        FULLSCREEN,
        POPUP
    }

    private Megaphone(Builder builder) {
        this.event = builder.event;
        this.style = builder.style;
        this.canSnooze = builder.canSnooze;
        this.titleText = builder.titleText;
        this.bodyText = builder.bodyText;
        this.imageRes = builder.imageRes;
        this.lottieRes = builder.lottieRes;
        this.imageRequest = builder.imageRequest;
        this.buttonText = builder.buttonText;
        this.buttonListener = builder.buttonListener;
        this.snoozeListener = builder.snoozeListener;
        this.secondaryButtonText = builder.secondaryButtonText;
        this.secondaryButtonListener = builder.secondaryButtonListener;
        this.onVisibleListener = builder.onVisibleListener;
    }

    public Megaphones.Event getEvent() {
        return this.event;
    }

    public boolean canSnooze() {
        return this.canSnooze;
    }

    public Style getStyle() {
        return this.style;
    }

    public MegaphoneText getTitle() {
        return this.titleText;
    }

    public MegaphoneText getBody() {
        return this.bodyText;
    }

    public int getLottieRes() {
        return this.lottieRes;
    }

    public int getImageRes() {
        return this.imageRes;
    }

    public GlideRequest<Drawable> getImageRequest() {
        return this.imageRequest;
    }

    public MegaphoneText getButtonText() {
        return this.buttonText;
    }

    public boolean hasButton() {
        MegaphoneText megaphoneText = this.buttonText;
        return megaphoneText != null && megaphoneText.hasText();
    }

    public EventListener getButtonClickListener() {
        return this.buttonListener;
    }

    public EventListener getSnoozeListener() {
        return this.snoozeListener;
    }

    public MegaphoneText getSecondaryButtonText() {
        return this.secondaryButtonText;
    }

    public boolean hasSecondaryButton() {
        MegaphoneText megaphoneText = this.secondaryButtonText;
        return megaphoneText != null && megaphoneText.hasText();
    }

    public EventListener getSecondaryButtonClickListener() {
        return this.secondaryButtonListener;
    }

    public EventListener getOnVisibleListener() {
        return this.onVisibleListener;
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private MegaphoneText bodyText;
        private EventListener buttonListener;
        private MegaphoneText buttonText;
        private boolean canSnooze;
        private final Megaphones.Event event;
        private GlideRequest<Drawable> imageRequest;
        private int imageRes;
        private int lottieRes;
        private EventListener onVisibleListener;
        private EventListener secondaryButtonListener;
        private MegaphoneText secondaryButtonText;
        private EventListener snoozeListener;
        private final Style style;
        private MegaphoneText titleText;

        public Builder(Megaphones.Event event, Style style) {
            this.event = event;
            this.style = style;
        }

        public Builder enableSnooze(EventListener eventListener) {
            this.canSnooze = true;
            this.snoozeListener = eventListener;
            return this;
        }

        public Builder disableSnooze() {
            this.canSnooze = false;
            this.snoozeListener = null;
            return this;
        }

        public Builder setTitle(int i) {
            this.titleText = MegaphoneText.from(i);
            return this;
        }

        public Builder setTitle(String str) {
            this.titleText = MegaphoneText.from(str);
            return this;
        }

        public Builder setBody(int i) {
            this.bodyText = MegaphoneText.from(i);
            return this;
        }

        public Builder setBody(String str) {
            this.bodyText = MegaphoneText.from(str);
            return this;
        }

        public Builder setImage(int i) {
            this.imageRes = i;
            return this;
        }

        public Builder setLottie(int i) {
            this.lottieRes = i;
            return this;
        }

        public Builder setImageRequest(GlideRequest<Drawable> glideRequest) {
            this.imageRequest = glideRequest;
            return this;
        }

        public Builder setActionButton(int i, EventListener eventListener) {
            this.buttonText = MegaphoneText.from(i);
            this.buttonListener = eventListener;
            return this;
        }

        public Builder setActionButton(String str, EventListener eventListener) {
            this.buttonText = MegaphoneText.from(str);
            this.buttonListener = eventListener;
            return this;
        }

        public Builder setSecondaryButton(int i, EventListener eventListener) {
            this.secondaryButtonText = MegaphoneText.from(i);
            this.secondaryButtonListener = eventListener;
            return this;
        }

        public Builder setSecondaryButton(String str, EventListener eventListener) {
            this.secondaryButtonText = MegaphoneText.from(str);
            this.secondaryButtonListener = eventListener;
            return this;
        }

        public Builder setOnVisibleListener(EventListener eventListener) {
            this.onVisibleListener = eventListener;
            return this;
        }

        public Megaphone build() {
            return new Megaphone(this);
        }
    }
}
