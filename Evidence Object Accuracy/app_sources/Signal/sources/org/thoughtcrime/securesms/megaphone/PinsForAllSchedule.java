package org.thoughtcrime.securesms.megaphone;

import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class PinsForAllSchedule implements MegaphoneSchedule {
    static final long DAYS_UNTIL_FULLSCREEN;
    private static final String TAG = Log.tag(PinsForAllSchedule.class);
    private final MegaphoneSchedule schedule = new RecurringSchedule(TimeUnit.HOURS.toMillis(2));

    public static boolean shouldDisplayFullScreen(long j, long j2) {
        return j != 0 && j2 - j >= TimeUnit.DAYS.toMillis(DAYS_UNTIL_FULLSCREEN);
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneSchedule
    public boolean shouldDisplay(int i, long j, long j2, long j3) {
        if (!isEnabled()) {
            return false;
        }
        if (shouldDisplayFullScreen(j2, j3)) {
            return true;
        }
        boolean shouldDisplay = this.schedule.shouldDisplay(i, j, j2, j3);
        Log.i(TAG, String.format(Locale.ENGLISH, "seenCount: %d,  lastSeen: %d,  firstVisible: %d,  currentTime: %d, result: %b", Integer.valueOf(i), Long.valueOf(j), Long.valueOf(j2), Long.valueOf(j3), Boolean.valueOf(shouldDisplay)));
        return shouldDisplay;
    }

    private static boolean isEnabled() {
        if (SignalStore.kbsValues().hasOptedOut() || SignalStore.kbsValues().hasPin()) {
            return false;
        }
        if (!pinCreationFailedDuringRegistration() && !newlyRegisteredRegistrationLockV1User() && SignalStore.registrationValues().pinWasRequiredAtRegistration()) {
            return false;
        }
        return true;
    }

    private static boolean pinCreationFailedDuringRegistration() {
        return SignalStore.registrationValues().pinWasRequiredAtRegistration() && !SignalStore.kbsValues().hasPin() && !TextSecurePreferences.isV1RegistrationLockEnabled(ApplicationDependencies.getApplication());
    }

    private static boolean newlyRegisteredRegistrationLockV1User() {
        return SignalStore.registrationValues().pinWasRequiredAtRegistration() && TextSecurePreferences.isV1RegistrationLockEnabled(ApplicationDependencies.getApplication());
    }
}
