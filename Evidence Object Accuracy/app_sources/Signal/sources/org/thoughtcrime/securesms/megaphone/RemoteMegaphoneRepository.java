package org.thoughtcrime.securesms.megaphone;

import android.app.Application;
import android.content.Context;
import j$.util.Collection$EL;
import j$.util.function.Predicate;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.RemoteMegaphoneDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.RemoteMegaphoneRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.PlayServicesUtil;
import org.thoughtcrime.securesms.util.VersionTracker;

/* compiled from: RemoteMegaphoneRepository.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u001cB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0005H\u0002J\u0010\u0010\u0011\u001a\u00020\u00062\u0006\u0010\u0012\u001a\u00020\u0013H\u0007J\n\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0007J\u0010\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u000fH\u0007J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0005H\u0007J\b\u0010\u001b\u001a\u00020\u000fH\u0002R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/megaphone/RemoteMegaphoneRepository;", "", "()V", "actions", "", "", "Lorg/thoughtcrime/securesms/megaphone/RemoteMegaphoneRepository$Action;", "context", "Landroid/app/Application;", "db", "Lorg/thoughtcrime/securesms/database/RemoteMegaphoneDatabase;", "donate", "finish", "snooze", "checkCondition", "", "conditionalId", "getAction", "action", "Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord$ActionId;", "getRemoteMegaphoneToShow", "Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord;", "hasRemoteMegaphoneToShow", "canShowLocalDonate", "markShown", "", RecipientDatabase.SERVICE_ID, "shouldShowDonateMegaphone", "Action", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RemoteMegaphoneRepository {
    public static final RemoteMegaphoneRepository INSTANCE = new RemoteMegaphoneRepository();
    private static final Map<String, Action> actions;
    private static final Application context;
    private static final RemoteMegaphoneDatabase db = SignalDatabase.Companion.remoteMegaphones();
    private static final Action donate;
    private static final Action finish;
    private static final Action snooze;

    /* compiled from: RemoteMegaphoneRepository.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bæ\u0001\u0018\u00002\u00020\u0001J \u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tH&¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/megaphone/RemoteMegaphoneRepository$Action;", "", "run", "", "context", "Landroid/content/Context;", "controller", "Lorg/thoughtcrime/securesms/megaphone/MegaphoneActionController;", "remoteMegaphone", "Lorg/thoughtcrime/securesms/database/model/RemoteMegaphoneRecord;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Action {
        void run(Context context, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord);
    }

    private RemoteMegaphoneRepository() {
    }

    static {
        INSTANCE = new RemoteMegaphoneRepository();
        db = SignalDatabase.Companion.remoteMegaphones();
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        context = application;
        RemoteMegaphoneRepository$$ExternalSyntheticLambda0 remoteMegaphoneRepository$$ExternalSyntheticLambda0 = new Action() { // from class: org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository.Action
            public final void run(Context context2, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord) {
                RemoteMegaphoneRepository.m2320snooze$lambda0(context2, megaphoneActionController, remoteMegaphoneRecord);
            }
        };
        snooze = remoteMegaphoneRepository$$ExternalSyntheticLambda0;
        RemoteMegaphoneRepository$$ExternalSyntheticLambda1 remoteMegaphoneRepository$$ExternalSyntheticLambda1 = new Action() { // from class: org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository.Action
            public final void run(Context context2, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord) {
                RemoteMegaphoneRepository.m2316finish$lambda1(context2, megaphoneActionController, remoteMegaphoneRecord);
            }
        };
        finish = remoteMegaphoneRepository$$ExternalSyntheticLambda1;
        RemoteMegaphoneRepository$$ExternalSyntheticLambda2 remoteMegaphoneRepository$$ExternalSyntheticLambda2 = new Action() { // from class: org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository.Action
            public final void run(Context context2, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord) {
                RemoteMegaphoneRepository.m2315donate$lambda2(context2, megaphoneActionController, remoteMegaphoneRecord);
            }
        };
        donate = remoteMegaphoneRepository$$ExternalSyntheticLambda2;
        actions = MapsKt__MapsKt.mapOf(TuplesKt.to(RemoteMegaphoneRecord.ActionId.SNOOZE.getId(), remoteMegaphoneRepository$$ExternalSyntheticLambda0), TuplesKt.to(RemoteMegaphoneRecord.ActionId.FINISH.getId(), remoteMegaphoneRepository$$ExternalSyntheticLambda1), TuplesKt.to(RemoteMegaphoneRecord.ActionId.DONATE.getId(), remoteMegaphoneRepository$$ExternalSyntheticLambda2));
    }

    /* renamed from: snooze$lambda-0 */
    public static final void m2320snooze$lambda0(Context context2, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord) {
        Intrinsics.checkNotNullParameter(context2, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(megaphoneActionController, "controller");
        Intrinsics.checkNotNullParameter(remoteMegaphoneRecord, "<anonymous parameter 2>");
        megaphoneActionController.onMegaphoneSnooze(Megaphones.Event.REMOTE_MEGAPHONE);
    }

    /* renamed from: finish$lambda-1 */
    public static final void m2316finish$lambda1(Context context2, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord) {
        Intrinsics.checkNotNullParameter(context2, "context");
        Intrinsics.checkNotNullParameter(megaphoneActionController, "controller");
        Intrinsics.checkNotNullParameter(remoteMegaphoneRecord, "remote");
        if (remoteMegaphoneRecord.getImageUri() != null) {
            BlobProvider.getInstance().delete(context2, remoteMegaphoneRecord.getImageUri());
        }
        megaphoneActionController.onMegaphoneSnooze(Megaphones.Event.REMOTE_MEGAPHONE);
        db.markFinished(remoteMegaphoneRecord.getUuid());
    }

    /* renamed from: donate$lambda-2 */
    public static final void m2315donate$lambda2(Context context2, MegaphoneActionController megaphoneActionController, RemoteMegaphoneRecord remoteMegaphoneRecord) {
        Intrinsics.checkNotNullParameter(context2, "context");
        Intrinsics.checkNotNullParameter(megaphoneActionController, "controller");
        Intrinsics.checkNotNullParameter(remoteMegaphoneRecord, "remote");
        megaphoneActionController.onMegaphoneNavigationRequested(AppSettingsActivity.Companion.subscriptions(context2));
        finish.run(context2, megaphoneActionController, remoteMegaphoneRecord);
    }

    @JvmStatic
    public static final boolean hasRemoteMegaphoneToShow(boolean z) {
        RemoteMegaphoneRecord remoteMegaphoneToShow = getRemoteMegaphoneToShow();
        boolean z2 = false;
        if (remoteMegaphoneToShow == null) {
            return false;
        }
        RemoteMegaphoneRecord.ActionId primaryActionId = remoteMegaphoneToShow.getPrimaryActionId();
        if (primaryActionId != null && primaryActionId.isDonateAction()) {
            z2 = true;
        }
        if (z2) {
            return z;
        }
        return true;
    }

    @JvmStatic
    public static final RemoteMegaphoneRecord getRemoteMegaphoneToShow() {
        return (RemoteMegaphoneRecord) SequencesKt___SequencesKt.firstOrNull(SequencesKt___SequencesKt.filter(SequencesKt___SequencesKt.filter(SequencesKt___SequencesKt.filter(CollectionsKt___CollectionsKt.asSequence(RemoteMegaphoneDatabase.getPotentialMegaphonesAndClearOld$default(db, 0, 1, null)), RemoteMegaphoneRepository$getRemoteMegaphoneToShow$1.INSTANCE), RemoteMegaphoneRepository$getRemoteMegaphoneToShow$2.INSTANCE), RemoteMegaphoneRepository$getRemoteMegaphoneToShow$3.INSTANCE));
    }

    @JvmStatic
    public static final Action getAction(RemoteMegaphoneRecord.ActionId actionId) {
        Intrinsics.checkNotNullParameter(actionId, "action");
        Action action = actions.get(actionId.getId());
        return action == null ? finish : action;
    }

    @JvmStatic
    public static final void markShown(String str) {
        Intrinsics.checkNotNullParameter(str, RecipientDatabase.SERVICE_ID);
        SignalExecutors.BOUNDED_IO.execute(new Runnable(str) { // from class: org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RemoteMegaphoneRepository.m2317markShown$lambda3(this.f$0);
            }
        });
    }

    /* renamed from: markShown$lambda-3 */
    public static final void m2317markShown$lambda3(String str) {
        Intrinsics.checkNotNullParameter(str, "$uuid");
        db.markShown(str);
    }

    public final boolean checkCondition(String str) {
        if (Intrinsics.areEqual(str, "standard_donate")) {
            return shouldShowDonateMegaphone();
        }
        return false;
    }

    private final boolean shouldShowDonateMegaphone() {
        Application application = context;
        return VersionTracker.getDaysSinceFirstInstalled(application) >= 7 && PlayServicesUtil.getPlayServicesStatus(application) == PlayServicesUtil.PlayServicesStatus.SUCCESS && Collection$EL.stream(Recipient.self().getBadges()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository$$ExternalSyntheticLambda4
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return RemoteMegaphoneRepository.m2318shouldShowDonateMegaphone$lambda4((Badge) obj);
            }
        }).noneMatch(new Predicate() { // from class: org.thoughtcrime.securesms.megaphone.RemoteMegaphoneRepository$$ExternalSyntheticLambda5
            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate and(Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ Predicate or(Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return RemoteMegaphoneRepository.m2319shouldShowDonateMegaphone$lambda5((Badge) obj);
            }
        });
    }

    /* renamed from: shouldShowDonateMegaphone$lambda-4 */
    public static final boolean m2318shouldShowDonateMegaphone$lambda4(Badge badge) {
        return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m(badge);
    }

    /* renamed from: shouldShowDonateMegaphone$lambda-5 */
    public static final boolean m2319shouldShowDonateMegaphone$lambda5(Badge badge) {
        Intrinsics.checkNotNullParameter(badge, "<name for destructuring parameter 0>");
        return badge.component2() == Badge.Category.Donor;
    }
}
