package org.thoughtcrime.securesms.megaphone;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.InviteActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.groups.ui.creategroup.CreateGroupActivity;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileActivity;
import org.thoughtcrime.securesms.util.SmsUtil;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperActivity;

/* loaded from: classes4.dex */
public class OnboardingMegaphoneView extends FrameLayout {
    private static final String TAG = Log.tag(OnboardingMegaphoneView.class);
    private RecyclerView cardList;

    /* loaded from: classes4.dex */
    public interface ActionClickListener {
        void onClick();
    }

    public OnboardingMegaphoneView(Context context) {
        super(context);
        initialize(context);
    }

    public OnboardingMegaphoneView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(context);
    }

    private void initialize(Context context) {
        FrameLayout.inflate(context, R.layout.onboarding_megaphone, this);
        this.cardList = (RecyclerView) findViewById(R.id.onboarding_megaphone_list);
    }

    public void present(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        this.cardList.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        this.cardList.setAdapter(new CardAdapter(getContext(), megaphoneActionController));
    }

    /* loaded from: classes4.dex */
    public static class CardAdapter extends RecyclerView.Adapter<CardViewHolder> implements ActionClickListener {
        private static final int TYPE_ADD_PHOTO;
        private static final int TYPE_APPEARANCE;
        private static final int TYPE_GROUP;
        private static final int TYPE_INVITE;
        private static final int TYPE_SMS;
        private final Context context;
        private final MegaphoneActionController controller;
        private final List<Integer> data;

        CardAdapter(Context context, MegaphoneActionController megaphoneActionController) {
            this.context = context;
            this.controller = megaphoneActionController;
            List<Integer> buildData = buildData(context);
            this.data = buildData;
            if (buildData.isEmpty()) {
                Log.i(OnboardingMegaphoneView.TAG, "Nothing to show (constructor)! Considering megaphone completed.");
                megaphoneActionController.onMegaphoneCompleted(Megaphones.Event.ONBOARDING);
            }
            setHasStableIds(true);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemViewType(int i) {
            return this.data.get(i).intValue();
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public long getItemId(int i) {
            return (long) this.data.get(i).intValue();
        }

        public CardViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.onboarding_megaphone_list_item, viewGroup, false);
            if (i == 0) {
                return new GroupCardViewHolder(inflate);
            }
            if (i == 1) {
                return new InviteCardViewHolder(inflate);
            }
            if (i == 2) {
                return new SmsCardViewHolder(inflate);
            }
            if (i == 3) {
                return new AppearanceCardViewHolder(inflate);
            }
            if (i == 4) {
                return new AddPhotoCardViewHolder(inflate);
            }
            throw new IllegalStateException("Invalid viewType! " + i);
        }

        public void onBindViewHolder(CardViewHolder cardViewHolder, int i) {
            cardViewHolder.bind(this, this.controller);
        }

        @Override // androidx.recyclerview.widget.RecyclerView.Adapter
        public int getItemCount() {
            return this.data.size();
        }

        public void onClick() {
            this.data.clear();
            this.data.addAll(buildData(this.context));
            if (this.data.isEmpty()) {
                Log.i(OnboardingMegaphoneView.TAG, "Nothing to show! Considering megaphone completed.");
                this.controller.onMegaphoneCompleted(Megaphones.Event.ONBOARDING);
            }
            notifyDataSetChanged();
        }

        private static List<Integer> buildData(Context context) {
            ArrayList arrayList = new ArrayList();
            if (SignalStore.onboarding().shouldShowNewGroup()) {
                arrayList.add(0);
            }
            if (SignalStore.onboarding().shouldShowInviteFriends()) {
                arrayList.add(1);
            }
            if (SignalStore.onboarding().shouldShowAddPhoto() && !SignalStore.misc().hasEverHadAnAvatar()) {
                arrayList.add(4);
            }
            if (SignalStore.onboarding().shouldShowAppearance()) {
                arrayList.add(3);
            }
            if (SignalStore.onboarding().shouldShowSms()) {
                arrayList.add(2);
            }
            return arrayList;
        }
    }

    /* loaded from: classes4.dex */
    public static abstract class CardViewHolder extends RecyclerView.ViewHolder {
        private final TextView actionButton;
        private final View closeButton;
        private final ImageView image;

        abstract int getButtonStringRes();

        abstract int getImageRes();

        abstract void onActionClicked(MegaphoneActionController megaphoneActionController);

        abstract void onCloseClicked();

        public CardViewHolder(View view) {
            super(view);
            this.image = (ImageView) view.findViewById(R.id.onboarding_megaphone_item_image);
            this.actionButton = (TextView) view.findViewById(R.id.onboarding_megaphone_item_button);
            this.closeButton = view.findViewById(R.id.onboarding_megaphone_item_close);
        }

        public void bind(ActionClickListener actionClickListener, MegaphoneActionController megaphoneActionController) {
            this.image.setImageResource(getImageRes());
            this.actionButton.setText(getButtonStringRes());
            this.actionButton.setOnClickListener(new OnboardingMegaphoneView$CardViewHolder$$ExternalSyntheticLambda0(this, megaphoneActionController, actionClickListener));
            this.closeButton.setOnClickListener(new OnboardingMegaphoneView$CardViewHolder$$ExternalSyntheticLambda1(this, actionClickListener));
        }

        public /* synthetic */ void lambda$bind$0(MegaphoneActionController megaphoneActionController, ActionClickListener actionClickListener, View view) {
            onActionClicked(megaphoneActionController);
            actionClickListener.onClick();
        }

        public /* synthetic */ void lambda$bind$1(ActionClickListener actionClickListener, View view) {
            onCloseClicked();
            actionClickListener.onClick();
        }
    }

    /* loaded from: classes4.dex */
    public static class GroupCardViewHolder extends CardViewHolder {
        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getButtonStringRes() {
            return R.string.Megaphones_new_group;
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getImageRes() {
            return R.drawable.ic_megaphone_start_group;
        }

        public GroupCardViewHolder(View view) {
            super(view);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onActionClicked(MegaphoneActionController megaphoneActionController) {
            megaphoneActionController.onMegaphoneNavigationRequested(CreateGroupActivity.newIntent(megaphoneActionController.getMegaphoneActivity()));
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onCloseClicked() {
            SignalStore.onboarding().setShowNewGroup(false);
        }
    }

    /* loaded from: classes4.dex */
    public static class InviteCardViewHolder extends CardViewHolder {
        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getButtonStringRes() {
            return R.string.Megaphones_invite_friends;
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getImageRes() {
            return R.drawable.ic_megaphone_invite_friends;
        }

        public InviteCardViewHolder(View view) {
            super(view);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onActionClicked(MegaphoneActionController megaphoneActionController) {
            megaphoneActionController.onMegaphoneNavigationRequested(new Intent(megaphoneActionController.getMegaphoneActivity(), InviteActivity.class));
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onCloseClicked() {
            SignalStore.onboarding().setShowInviteFriends(false);
        }
    }

    /* loaded from: classes4.dex */
    public static class SmsCardViewHolder extends CardViewHolder {
        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getButtonStringRes() {
            return R.string.Megaphones_use_sms;
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getImageRes() {
            return R.drawable.ic_megaphone_use_sms;
        }

        public SmsCardViewHolder(View view) {
            super(view);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onActionClicked(MegaphoneActionController megaphoneActionController) {
            megaphoneActionController.onMegaphoneNavigationRequested(SmsUtil.getSmsRoleIntent(megaphoneActionController.getMegaphoneActivity()), 32563);
            SignalStore.onboarding().setShowSms(false);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onCloseClicked() {
            SignalStore.onboarding().setShowSms(false);
        }
    }

    /* loaded from: classes4.dex */
    public static class AppearanceCardViewHolder extends CardViewHolder {
        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getButtonStringRes() {
            return R.string.Megaphones_appearance;
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getImageRes() {
            return R.drawable.ic_signal_appearance;
        }

        public AppearanceCardViewHolder(View view) {
            super(view);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onActionClicked(MegaphoneActionController megaphoneActionController) {
            megaphoneActionController.onMegaphoneNavigationRequested(ChatWallpaperActivity.createIntent(megaphoneActionController.getMegaphoneActivity()));
            SignalStore.onboarding().setShowAppearance(false);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onCloseClicked() {
            SignalStore.onboarding().setShowAppearance(false);
        }
    }

    /* loaded from: classes4.dex */
    public static class AddPhotoCardViewHolder extends CardViewHolder {
        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getButtonStringRes() {
            return R.string.Megaphones_add_photo;
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        int getImageRes() {
            return R.drawable.ic_signal_add_photo;
        }

        public AddPhotoCardViewHolder(View view) {
            super(view);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onActionClicked(MegaphoneActionController megaphoneActionController) {
            megaphoneActionController.onMegaphoneNavigationRequested(ManageProfileActivity.getIntentForAvatarEdit(megaphoneActionController.getMegaphoneActivity()));
            SignalStore.onboarding().setShowAddPhoto(false);
        }

        @Override // org.thoughtcrime.securesms.megaphone.OnboardingMegaphoneView.CardViewHolder
        void onCloseClicked() {
            SignalStore.onboarding().setShowAddPhoto(false);
        }
    }
}
