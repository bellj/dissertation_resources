package org.thoughtcrime.securesms.megaphone;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.SetUtil;
import org.signal.core.util.TranslationDetection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.model.MegaphoneRecord;
import org.thoughtcrime.securesms.database.model.RemoteMegaphoneRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.SignalPinReminderDialog;
import org.thoughtcrime.securesms.lock.SignalPinReminders;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.lock.v2.KbsMigrationActivity;
import org.thoughtcrime.securesms.megaphone.Megaphone;
import org.thoughtcrime.securesms.megaphone.Megaphones;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.profiles.AvatarHelper;
import org.thoughtcrime.securesms.profiles.manage.ManageProfileActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.LocaleFeatureFlags;
import org.thoughtcrime.securesms.util.PlayServicesUtil;
import org.thoughtcrime.securesms.util.VersionTracker;
import org.thoughtcrime.securesms.util.dynamiclanguage.DynamicLanguageContextWrapper;

/* loaded from: classes4.dex */
public final class Megaphones {
    private static final MegaphoneSchedule ALWAYS = new ForeverSchedule(true);
    private static final Set<Event> DONATE_EVENTS = SetUtil.newHashSet(Event.BECOME_A_SUSTAINER, Event.DONATE_Q2_2022);
    private static final long MIN_TIME_BETWEEN_DONATE_MEGAPHONES = TimeUnit.DAYS.toMillis(30);
    private static final MegaphoneSchedule NEVER = new ForeverSchedule(false);
    private static final String TAG = Log.tag(Megaphones.class);

    private Megaphones() {
    }

    public static Megaphone getNextMegaphone(Context context, Map<Event, MegaphoneRecord> map) {
        Stream map2 = Stream.of(buildDisplayOrder(context, map)).filter(new Predicate(map, System.currentTimeMillis()) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda16
            public final /* synthetic */ Map f$0;
            public final /* synthetic */ long f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return Megaphones.lambda$getNextMegaphone$0(this.f$0, this.f$1, (Map.Entry) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda17
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (Megaphones.Event) ((Map.Entry) obj).getKey();
            }
        });
        Objects.requireNonNull(map);
        List list = map2.map(new Function(map) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda18
            public final /* synthetic */ Map f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return (MegaphoneRecord) this.f$0.get((Megaphones.Event) obj);
            }
        }).map(new Function(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda19
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return Megaphones.forRecord(this.f$0, (MegaphoneRecord) obj);
            }
        }).toList();
        if (list.size() > 0) {
            return (Megaphone) list.get(0);
        }
        return null;
    }

    public static /* synthetic */ boolean lambda$getNextMegaphone$0(Map map, long j, Map.Entry entry) {
        MegaphoneRecord megaphoneRecord = (MegaphoneRecord) map.get(entry.getKey());
        Objects.requireNonNull(megaphoneRecord);
        return !megaphoneRecord.isFinished() && ((MegaphoneSchedule) entry.getValue()).shouldDisplay(megaphoneRecord.getSeenCount(), megaphoneRecord.getLastSeen(), megaphoneRecord.getFirstVisible(), j);
    }

    private static Map<Event, MegaphoneSchedule> buildDisplayOrder(Context context, Map<Event, MegaphoneRecord> map) {
        return new LinkedHashMap<Event, MegaphoneSchedule>(context, map) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones.1
            final /* synthetic */ Context val$context;
            final /* synthetic */ Map val$records;

            {
                MegaphoneSchedule megaphoneSchedule;
                MegaphoneSchedule megaphoneSchedule2;
                MegaphoneSchedule megaphoneSchedule3;
                MegaphoneSchedule megaphoneSchedule4;
                this.val$context = r5;
                this.val$records = r6;
                put(Event.PINS_FOR_ALL, new PinsForAllSchedule());
                put(Event.CLIENT_DEPRECATED, SignalStore.misc().isClientDeprecated() ? Megaphones.ALWAYS : Megaphones.NEVER);
                Event event = Event.NOTIFICATIONS;
                if (Megaphones.shouldShowNotificationsMegaphone(r5)) {
                    megaphoneSchedule = RecurringSchedule.every(TimeUnit.DAYS.toMillis(30));
                } else {
                    megaphoneSchedule = Megaphones.NEVER;
                }
                put(event, megaphoneSchedule);
                put(Event.ONBOARDING, Megaphones.shouldShowOnboardingMegaphone(r5) ? Megaphones.ALWAYS : Megaphones.NEVER);
                Event event2 = Event.TURN_OFF_CENSORSHIP_CIRCUMVENTION;
                if (Megaphones.shouldShowTurnOffCircumventionMegaphone()) {
                    megaphoneSchedule2 = RecurringSchedule.every(TimeUnit.DAYS.toMillis(7));
                } else {
                    megaphoneSchedule2 = Megaphones.NEVER;
                }
                put(event2, megaphoneSchedule2);
                Event event3 = Event.DONATE_Q2_2022;
                if (Megaphones.shouldShowDonateMegaphone(r5, event3, r6)) {
                    megaphoneSchedule3 = ShowForDurationSchedule.showForDays(7);
                } else {
                    megaphoneSchedule3 = Megaphones.NEVER;
                }
                put(event3, megaphoneSchedule3);
                Event event4 = Event.REMOTE_MEGAPHONE;
                if (Megaphones.shouldShowRemoteMegaphone(r6)) {
                    megaphoneSchedule4 = RecurringSchedule.every(TimeUnit.DAYS.toMillis(3));
                } else {
                    megaphoneSchedule4 = Megaphones.NEVER;
                }
                put(event4, megaphoneSchedule4);
                put(Event.PIN_REMINDER, new SignalPinReminderSchedule());
                put(Event.ADD_A_PROFILE_PHOTO, Megaphones.shouldShowAddAProfilePhotoMegaphone(r5) ? Megaphones.ALWAYS : Megaphones.NEVER);
            }
        };
    }

    /* renamed from: org.thoughtcrime.securesms.megaphone.Megaphones$3 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass3 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event;

        static {
            int[] iArr = new int[Event.values().length];
            $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event = iArr;
            try {
                iArr[Event.PINS_FOR_ALL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.PIN_REMINDER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.CLIENT_DEPRECATED.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.ONBOARDING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.NOTIFICATIONS.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.ADD_A_PROFILE_PHOTO.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.BECOME_A_SUSTAINER.ordinal()] = 7;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.DONATE_Q2_2022.ordinal()] = 8;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.TURN_OFF_CENSORSHIP_CIRCUMVENTION.ordinal()] = 9;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[Event.REMOTE_MEGAPHONE.ordinal()] = 10;
            } catch (NoSuchFieldError unused10) {
            }
        }
    }

    public static Megaphone forRecord(Context context, MegaphoneRecord megaphoneRecord) {
        switch (AnonymousClass3.$SwitchMap$org$thoughtcrime$securesms$megaphone$Megaphones$Event[megaphoneRecord.getEvent().ordinal()]) {
            case 1:
                return buildPinsForAllMegaphone(megaphoneRecord);
            case 2:
                return buildPinReminderMegaphone(context);
            case 3:
                return buildClientDeprecatedMegaphone(context);
            case 4:
                return buildOnboardingMegaphone();
            case 5:
                return buildNotificationsMegaphone(context);
            case 6:
                return buildAddAProfilePhotoMegaphone(context);
            case 7:
                return buildBecomeASustainerMegaphone(context);
            case 8:
                return buildDonateQ2Megaphone(context);
            case 9:
                return buildTurnOffCircumventionMegaphone(context);
            case 10:
                return buildRemoteMegaphone(context);
            default:
                throw new IllegalArgumentException("Event not handled!");
        }
    }

    private static Megaphone buildPinsForAllMegaphone(MegaphoneRecord megaphoneRecord) {
        if (PinsForAllSchedule.shouldDisplayFullScreen(megaphoneRecord.getFirstVisible(), System.currentTimeMillis())) {
            return new Megaphone.Builder(Event.PINS_FOR_ALL, Megaphone.Style.FULLSCREEN).enableSnooze(null).setOnVisibleListener(new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda24
                @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
                public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                    Megaphones.lambda$buildPinsForAllMegaphone$2(megaphone, megaphoneActionController);
                }
            }).build();
        }
        return new Megaphone.Builder(Event.PINS_FOR_ALL, Megaphone.Style.BASIC).setImage(R.drawable.kbs_pin_megaphone).setTitle(R.string.KbsMegaphone__create_a_pin).setBody(R.string.KbsMegaphone__pins_keep_information_thats_stored_with_signal_encrytped).setActionButton(R.string.KbsMegaphone__create_pin, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda25
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildPinsForAllMegaphone$3(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildPinsForAllMegaphone$2(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        if (new NetworkConstraint.Factory(ApplicationDependencies.getApplication()).create().isMet()) {
            megaphoneActionController.onMegaphoneNavigationRequested(KbsMigrationActivity.createIntent(), 27698);
        }
    }

    public static /* synthetic */ void lambda$buildPinsForAllMegaphone$3(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneNavigationRequested(CreateKbsPinActivity.getIntentForPinCreate(ApplicationDependencies.getApplication()), 27698);
    }

    private static Megaphone buildPinReminderMegaphone(Context context) {
        return new Megaphone.Builder(Event.PIN_REMINDER, Megaphone.Style.BASIC).setTitle(R.string.Megaphones_verify_your_signal_pin).setBody(R.string.Megaphones_well_occasionally_ask_you_to_verify_your_pin).setImage(R.drawable.kbs_pin_megaphone).setActionButton(R.string.Megaphones_verify_pin, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda9
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildPinReminderMegaphone$4(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildPinReminderMegaphone$4(Megaphone megaphone, final MegaphoneActionController megaphoneActionController) {
        SignalPinReminderDialog.show(megaphoneActionController.getMegaphoneActivity(), new SignalPinReminderDialog.Launcher() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda20
            @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.Launcher
            public final void launch(Intent intent, int i) {
                MegaphoneActionController.this.onMegaphoneNavigationRequested(intent, i);
            }
        }, new SignalPinReminderDialog.Callback() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones.2
            @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.Callback
            public void onReminderDismissed(boolean z) {
                String str = Megaphones.TAG;
                Log.i(str, "[PinReminder] onReminderDismissed(" + z + ")");
                if (z) {
                    SignalStore.pinValues().onEntrySkipWithWrongGuess();
                }
            }

            @Override // org.thoughtcrime.securesms.lock.SignalPinReminderDialog.Callback
            public void onReminderCompleted(String str, boolean z) {
                String str2 = Megaphones.TAG;
                Log.i(str2, "[PinReminder] onReminderCompleted(" + z + ")");
                if (z) {
                    SignalStore.pinValues().onEntrySuccessWithWrongGuess(str);
                } else {
                    SignalStore.pinValues().onEntrySuccess(str);
                }
                megaphoneActionController.onMegaphoneSnooze(Event.PIN_REMINDER);
                MegaphoneActionController megaphoneActionController2 = megaphoneActionController;
                megaphoneActionController2.onMegaphoneToastRequested(megaphoneActionController2.getMegaphoneActivity().getString(SignalPinReminders.getReminderString(SignalStore.pinValues().getCurrentInterval())));
            }
        });
    }

    private static Megaphone buildClientDeprecatedMegaphone(Context context) {
        return new Megaphone.Builder(Event.CLIENT_DEPRECATED, Megaphone.Style.FULLSCREEN).disableSnooze().setOnVisibleListener(new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda8
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildClientDeprecatedMegaphone$5(this.f$0, megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildClientDeprecatedMegaphone$5(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneNavigationRequested(new Intent(context, ClientDeprecatedActivity.class));
    }

    private static Megaphone buildOnboardingMegaphone() {
        return new Megaphone.Builder(Event.ONBOARDING, Megaphone.Style.ONBOARDING).build();
    }

    private static Megaphone buildNotificationsMegaphone(Context context) {
        return new Megaphone.Builder(Event.NOTIFICATIONS, Megaphone.Style.BASIC).setTitle(R.string.NotificationsMegaphone_turn_on_notifications).setBody(R.string.NotificationsMegaphone_never_miss_a_message).setImage(R.drawable.megaphone_notifications_64).setActionButton(R.string.NotificationsMegaphone_turn_on, new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda14
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildNotificationsMegaphone$6(this.f$0, megaphone, megaphoneActionController);
            }
        }).setSecondaryButton(R.string.NotificationsMegaphone_not_now, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda15
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildNotificationsMegaphone$7(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildNotificationsMegaphone$6(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26 && !NotificationChannels.isMessageChannelEnabled(context)) {
            Intent intent = new Intent("android.settings.CHANNEL_NOTIFICATION_SETTINGS");
            intent.putExtra("android.provider.extra.CHANNEL_ID", NotificationChannels.getMessagesChannel(context));
            intent.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
            megaphoneActionController.onMegaphoneNavigationRequested(intent);
        } else if (i < 26 || (NotificationChannels.areNotificationsEnabled(context) && NotificationChannels.isMessagesChannelGroupEnabled(context))) {
            megaphoneActionController.onMegaphoneNavigationRequested(AppSettingsActivity.notifications(context));
        } else {
            Intent intent2 = new Intent("android.settings.APP_NOTIFICATION_SETTINGS");
            intent2.putExtra("android.provider.extra.APP_PACKAGE", context.getPackageName());
            megaphoneActionController.onMegaphoneNavigationRequested(intent2);
        }
    }

    public static /* synthetic */ void lambda$buildNotificationsMegaphone$7(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneSnooze(Event.NOTIFICATIONS);
    }

    private static Megaphone buildAddAProfilePhotoMegaphone(Context context) {
        return new Megaphone.Builder(Event.ADD_A_PROFILE_PHOTO, Megaphone.Style.BASIC).setTitle(R.string.AddAProfilePhotoMegaphone__add_a_profile_photo).setImage(R.drawable.ic_add_a_profile_megaphone_image).setBody(R.string.AddAProfilePhotoMegaphone__choose_a_look_and_color).setActionButton(R.string.AddAProfilePhotoMegaphone__add_photo, new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda12
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildAddAProfilePhotoMegaphone$8(this.f$0, megaphone, megaphoneActionController);
            }
        }).setSecondaryButton(R.string.AddAProfilePhotoMegaphone__not_now, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda13
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildAddAProfilePhotoMegaphone$9(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildAddAProfilePhotoMegaphone$8(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneNavigationRequested(ManageProfileActivity.getIntentForAvatarEdit(context));
        megaphoneActionController.onMegaphoneCompleted(Event.ADD_A_PROFILE_PHOTO);
    }

    public static /* synthetic */ void lambda$buildAddAProfilePhotoMegaphone$9(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneCompleted(Event.ADD_A_PROFILE_PHOTO);
    }

    private static Megaphone buildBecomeASustainerMegaphone(Context context) {
        return new Megaphone.Builder(Event.BECOME_A_SUSTAINER, Megaphone.Style.BASIC).setTitle(R.string.BecomeASustainerMegaphone__become_a_sustainer).setImage(R.drawable.ic_become_a_sustainer_megaphone).setBody(R.string.BecomeASustainerMegaphone__signal_is_powered_by).setActionButton(R.string.BecomeASustainerMegaphone__donate, new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda26
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildBecomeASustainerMegaphone$10(this.f$0, megaphone, megaphoneActionController);
            }
        }).setSecondaryButton(R.string.BecomeASustainerMegaphone__not_now, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda27
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildBecomeASustainerMegaphone$11(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildBecomeASustainerMegaphone$10(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneNavigationRequested(AppSettingsActivity.subscriptions(context));
        megaphoneActionController.onMegaphoneCompleted(Event.BECOME_A_SUSTAINER);
    }

    public static /* synthetic */ void lambda$buildBecomeASustainerMegaphone$11(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneCompleted(Event.BECOME_A_SUSTAINER);
    }

    private static Megaphone buildDonateQ2Megaphone(Context context) {
        return new Megaphone.Builder(Event.DONATE_Q2_2022, Megaphone.Style.BASIC).setTitle(R.string.Donate2022Q2Megaphone_donate_to_signal).setImage(R.drawable.ic_donate_q2_2022).setBody(R.string.Donate2022Q2Megaphone_signal_is_powered_by_people_like_you).setActionButton(R.string.Donate2022Q2Megaphone_donate, new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildDonateQ2Megaphone$12(this.f$0, megaphone, megaphoneActionController);
            }
        }).setSecondaryButton(R.string.Donate2022Q2Megaphone_not_now, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildDonateQ2Megaphone$13(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildDonateQ2Megaphone$12(Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneNavigationRequested(AppSettingsActivity.subscriptions(context));
        megaphoneActionController.onMegaphoneCompleted(Event.DONATE_Q2_2022);
    }

    public static /* synthetic */ void lambda$buildDonateQ2Megaphone$13(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneCompleted(Event.DONATE_Q2_2022);
    }

    private static Megaphone buildTurnOffCircumventionMegaphone(Context context) {
        return new Megaphone.Builder(Event.TURN_OFF_CENSORSHIP_CIRCUMVENTION, Megaphone.Style.BASIC).setTitle(R.string.CensorshipCircumventionMegaphone_turn_off_censorship_circumvention).setImage(R.drawable.ic_censorship_megaphone_64).setBody(R.string.CensorshipCircumventionMegaphone_you_can_now_connect_to_the_signal_service).setActionButton(R.string.CensorshipCircumventionMegaphone_turn_off, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildTurnOffCircumventionMegaphone$14(megaphone, megaphoneActionController);
            }
        }).setSecondaryButton(R.string.CensorshipCircumventionMegaphone_no_thanks, new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda7
            @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
            public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                Megaphones.lambda$buildTurnOffCircumventionMegaphone$15(megaphone, megaphoneActionController);
            }
        }).build();
    }

    public static /* synthetic */ void lambda$buildTurnOffCircumventionMegaphone$14(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        SignalStore.settings().setCensorshipCircumventionEnabled(false);
        megaphoneActionController.onMegaphoneSnooze(Event.TURN_OFF_CENSORSHIP_CIRCUMVENTION);
    }

    public static /* synthetic */ void lambda$buildTurnOffCircumventionMegaphone$15(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        megaphoneActionController.onMegaphoneSnooze(Event.TURN_OFF_CENSORSHIP_CIRCUMVENTION);
    }

    private static Megaphone buildRemoteMegaphone(Context context) {
        RemoteMegaphoneRecord remoteMegaphoneToShow = RemoteMegaphoneRepository.getRemoteMegaphoneToShow();
        if (remoteMegaphoneToShow != null) {
            Megaphone.Builder body = new Megaphone.Builder(Event.REMOTE_MEGAPHONE, Megaphone.Style.BASIC).setTitle(remoteMegaphoneToShow.getTitle()).setBody(remoteMegaphoneToShow.getBody());
            if (remoteMegaphoneToShow.getImageUri() != null) {
                body.setImageRequest(GlideApp.with(context).asDrawable().load(remoteMegaphoneToShow.getImageUri()));
            }
            if (remoteMegaphoneToShow.hasPrimaryAction()) {
                body.setActionButton(remoteMegaphoneToShow.getPrimaryActionText(), new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda21
                    public final /* synthetic */ Context f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
                    public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                        Megaphones.lambda$buildRemoteMegaphone$16(RemoteMegaphoneRecord.this, this.f$1, megaphone, megaphoneActionController);
                    }
                });
            }
            if (remoteMegaphoneToShow.hasSecondaryAction()) {
                body.setSecondaryButton(remoteMegaphoneToShow.getSecondaryActionText(), new Megaphone.EventListener(context) { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda22
                    public final /* synthetic */ Context f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
                    public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                        Megaphones.lambda$buildRemoteMegaphone$17(RemoteMegaphoneRecord.this, this.f$1, megaphone, megaphoneActionController);
                    }
                });
            }
            body.setOnVisibleListener(new Megaphone.EventListener() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda23
                @Override // org.thoughtcrime.securesms.megaphone.Megaphone.EventListener
                public final void onEvent(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
                    Megaphones.lambda$buildRemoteMegaphone$18(RemoteMegaphoneRecord.this, megaphone, megaphoneActionController);
                }
            });
            return body.build();
        }
        throw new IllegalStateException("No record to show");
    }

    public static /* synthetic */ void lambda$buildRemoteMegaphone$16(RemoteMegaphoneRecord remoteMegaphoneRecord, Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        RemoteMegaphoneRecord.ActionId primaryActionId = remoteMegaphoneRecord.getPrimaryActionId();
        Objects.requireNonNull(primaryActionId);
        RemoteMegaphoneRepository.getAction(primaryActionId).run(context, megaphoneActionController, remoteMegaphoneRecord);
    }

    public static /* synthetic */ void lambda$buildRemoteMegaphone$17(RemoteMegaphoneRecord remoteMegaphoneRecord, Context context, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        RemoteMegaphoneRecord.ActionId secondaryActionId = remoteMegaphoneRecord.getSecondaryActionId();
        Objects.requireNonNull(secondaryActionId);
        RemoteMegaphoneRepository.getAction(secondaryActionId).run(context, megaphoneActionController, remoteMegaphoneRecord);
    }

    public static /* synthetic */ void lambda$buildRemoteMegaphone$18(RemoteMegaphoneRecord remoteMegaphoneRecord, Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        RemoteMegaphoneRepository.markShown(remoteMegaphoneRecord.getUuid());
    }

    public static boolean shouldShowDonateMegaphone(Context context, Event event, Map<Event, MegaphoneRecord> map) {
        return timeSinceLastDonatePrompt(event, map) > MIN_TIME_BETWEEN_DONATE_MEGAPHONES && VersionTracker.getDaysSinceFirstInstalled(context) >= 7 && LocaleFeatureFlags.isInDonateMegaphone() && PlayServicesUtil.getPlayServicesStatus(context) == PlayServicesUtil.PlayServicesStatus.SUCCESS && Collection$EL.stream(Recipient.self().getBadges()).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda10
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((Badge) obj);
            }
        }).noneMatch(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda11
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return Megaphones.lambda$shouldShowDonateMegaphone$19((Badge) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$shouldShowDonateMegaphone$19(Badge badge) {
        return badge.getCategory() == Badge.Category.Donor;
    }

    public static boolean shouldShowOnboardingMegaphone(Context context) {
        return SignalStore.onboarding().hasOnboarding(context);
    }

    public static boolean shouldShowTurnOffCircumventionMegaphone() {
        return ApplicationDependencies.getSignalServiceNetworkAccess().isCensored() && SignalStore.misc().isServiceReachableWithoutCircumvention();
    }

    public static boolean shouldShowNotificationsMegaphone(Context context) {
        boolean z = !SignalStore.settings().isMessageNotificationsEnabled() || !NotificationChannels.isMessageChannelEnabled(context) || !NotificationChannels.isMessagesChannelGroupEnabled(context) || !NotificationChannels.areNotificationsEnabled(context);
        if (z) {
            Locale usersSelectedLocale = DynamicLanguageContextWrapper.getUsersSelectedLocale(context);
            if (!new TranslationDetection(context, usersSelectedLocale).textExistsInUsersLanguage(R.string.NotificationsMegaphone_turn_on_notifications, R.string.NotificationsMegaphone_never_miss_a_message, R.string.NotificationsMegaphone_turn_on, R.string.NotificationsMegaphone_not_now)) {
                String str = TAG;
                Log.i(str, "Would show NotificationsMegaphone but is not yet translated in " + usersSelectedLocale);
                return false;
            }
        }
        return z;
    }

    public static boolean shouldShowAddAProfilePhotoMegaphone(Context context) {
        if (SignalStore.misc().hasEverHadAnAvatar()) {
            return false;
        }
        if (!AvatarHelper.hasAvatar(context, Recipient.self().getId())) {
            return true;
        }
        SignalStore.misc().markHasEverHadAnAvatar();
        return false;
    }

    public static boolean shouldShowRemoteMegaphone(Map<Event, MegaphoneRecord> map) {
        return RemoteMegaphoneRepository.hasRemoteMegaphoneToShow(timeSinceLastDonatePrompt(Event.REMOTE_MEGAPHONE, map) > MIN_TIME_BETWEEN_DONATE_MEGAPHONES);
    }

    private static long timeSinceLastDonatePrompt(Event event, Map<Event, MegaphoneRecord> map) {
        return System.currentTimeMillis() - ((Long) Collection$EL.stream(map.entrySet()).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda2
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return Megaphones.lambda$timeSinceLastDonatePrompt$20((Map.Entry) obj);
            }
        }).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda3
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return Megaphones.lambda$timeSinceLastDonatePrompt$21(Megaphones.Event.this, (Map.Entry) obj);
            }
        }).map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Megaphones.lambda$timeSinceLastDonatePrompt$22((Map.Entry) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).filter(new j$.util.function.Predicate() { // from class: org.thoughtcrime.securesms.megaphone.Megaphones$$ExternalSyntheticLambda5
            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate and(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$and(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate negate() {
                return Predicate.CC.$default$negate(this);
            }

            @Override // j$.util.function.Predicate
            public /* synthetic */ j$.util.function.Predicate or(j$.util.function.Predicate predicate) {
                return Predicate.CC.$default$or(this, predicate);
            }

            @Override // j$.util.function.Predicate
            public final boolean test(Object obj) {
                return Megaphones.lambda$timeSinceLastDonatePrompt$23((Long) obj);
            }
        }).sorted().findFirst().orElse(0L)).longValue();
    }

    public static /* synthetic */ boolean lambda$timeSinceLastDonatePrompt$20(Map.Entry entry) {
        return DONATE_EVENTS.contains(entry.getKey());
    }

    public static /* synthetic */ boolean lambda$timeSinceLastDonatePrompt$21(Event event, Map.Entry entry) {
        return !((Event) entry.getKey()).equals(event);
    }

    public static /* synthetic */ Long lambda$timeSinceLastDonatePrompt$22(Map.Entry entry) {
        return Long.valueOf(((MegaphoneRecord) entry.getValue()).getFirstVisible());
    }

    public static /* synthetic */ boolean lambda$timeSinceLastDonatePrompt$23(Long l) {
        return l.longValue() > 0;
    }

    /* loaded from: classes4.dex */
    public enum Event {
        PINS_FOR_ALL("pins_for_all"),
        PIN_REMINDER("pin_reminder"),
        CLIENT_DEPRECATED("client_deprecated"),
        ONBOARDING("onboarding"),
        NOTIFICATIONS("notifications"),
        ADD_A_PROFILE_PHOTO("add_a_profile_photo"),
        BECOME_A_SUSTAINER("become_a_sustainer"),
        DONATE_Q2_2022("donate_q2_2022"),
        TURN_OFF_CENSORSHIP_CIRCUMVENTION("turn_off_censorship_circumvention"),
        REMOTE_MEGAPHONE("remote_megaphone");
        
        private final String key;

        Event(String str) {
            this.key = str;
        }

        public String getKey() {
            return this.key;
        }

        public static Event fromKey(String str) {
            Event[] values = values();
            for (Event event : values) {
                if (event.getKey().equals(str)) {
                    return event;
                }
            }
            throw new IllegalArgumentException("No event for key: " + str);
        }

        public static boolean hasKey(String str) {
            for (Event event : values()) {
                if (event.getKey().equals(str)) {
                    return true;
                }
            }
            return false;
        }
    }
}
