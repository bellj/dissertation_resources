package org.thoughtcrime.securesms.megaphone;

/* loaded from: classes4.dex */
public class RecurringSchedule implements MegaphoneSchedule {
    private final long[] gaps;

    public RecurringSchedule(long... jArr) {
        this.gaps = jArr;
    }

    public static MegaphoneSchedule every(long j) {
        return new RecurringSchedule(j);
    }

    @Override // org.thoughtcrime.securesms.megaphone.MegaphoneSchedule
    public boolean shouldDisplay(int i, long j, long j2, long j3) {
        if (i == 0) {
            return true;
        }
        long[] jArr = this.gaps;
        return j + jArr[Math.min(i - 1, jArr.length - 1)] <= j3;
    }
}
