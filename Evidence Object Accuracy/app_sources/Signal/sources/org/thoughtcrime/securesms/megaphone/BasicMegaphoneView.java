package org.thoughtcrime.securesms.megaphone;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.airbnb.lottie.LottieAnimationView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class BasicMegaphoneView extends FrameLayout {
    private Button actionButton;
    private TextView bodyText;
    private LottieAnimationView image;
    private Megaphone megaphone;
    private MegaphoneActionController megaphoneListener;
    private Button secondaryButton;
    private TextView titleText;

    public BasicMegaphoneView(Context context) {
        super(context);
        init(context);
    }

    public BasicMegaphoneView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context);
    }

    private void init(Context context) {
        FrameLayout.inflate(context, R.layout.basic_megaphone_view, this);
        this.image = (LottieAnimationView) findViewById(R.id.basic_megaphone_image);
        this.titleText = (TextView) findViewById(R.id.basic_megaphone_title);
        this.bodyText = (TextView) findViewById(R.id.basic_megaphone_body);
        this.actionButton = (Button) findViewById(R.id.basic_megaphone_action);
        this.secondaryButton = (Button) findViewById(R.id.basic_megaphone_secondary);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        Megaphone megaphone = this.megaphone;
        if (megaphone != null && this.megaphoneListener != null && megaphone.getOnVisibleListener() != null) {
            this.megaphone.getOnVisibleListener().onEvent(this.megaphone, this.megaphoneListener);
        }
    }

    public void present(Megaphone megaphone, MegaphoneActionController megaphoneActionController) {
        this.megaphone = megaphone;
        this.megaphoneListener = megaphoneActionController;
        if (megaphone.getImageRes() != 0) {
            this.image.setVisibility(0);
            this.image.setImageResource(megaphone.getImageRes());
        } else if (megaphone.getImageRequest() != null) {
            this.image.setVisibility(0);
            megaphone.getImageRequest().into(this.image);
        } else if (megaphone.getLottieRes() != 0) {
            this.image.setVisibility(0);
            this.image.setAnimation(megaphone.getLottieRes());
            this.image.playAnimation();
        } else {
            this.image.setVisibility(8);
        }
        if (megaphone.getTitle().hasText()) {
            this.titleText.setVisibility(0);
            this.titleText.setText(megaphone.getTitle().resolve(getContext()));
        } else {
            this.titleText.setVisibility(8);
        }
        if (megaphone.getBody().hasText()) {
            this.bodyText.setVisibility(0);
            this.bodyText.setText(megaphone.getBody().resolve(getContext()));
        } else {
            this.bodyText.setVisibility(8);
        }
        if (megaphone.hasButton()) {
            this.actionButton.setVisibility(0);
            this.actionButton.setText(megaphone.getButtonText().resolve(getContext()));
            this.actionButton.setOnClickListener(new View.OnClickListener(megaphoneActionController) { // from class: org.thoughtcrime.securesms.megaphone.BasicMegaphoneView$$ExternalSyntheticLambda0
                public final /* synthetic */ MegaphoneActionController f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    BasicMegaphoneView.m2294$r8$lambda$hoOcFUzTmVxp4eEuuoNrwhzlBA(Megaphone.this, this.f$1, view);
                }
            });
        } else {
            this.actionButton.setVisibility(8);
        }
        if (megaphone.canSnooze() || megaphone.hasSecondaryButton()) {
            this.secondaryButton.setVisibility(0);
            if (megaphone.canSnooze()) {
                this.secondaryButton.setOnClickListener(new View.OnClickListener(megaphone) { // from class: org.thoughtcrime.securesms.megaphone.BasicMegaphoneView$$ExternalSyntheticLambda1
                    public final /* synthetic */ Megaphone f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        BasicMegaphoneView.m2293$r8$lambda$cb7o7gzlAsp3jl7Ij2sOkxCng8(MegaphoneActionController.this, this.f$1, view);
                    }
                });
                return;
            }
            this.secondaryButton.setText(megaphone.getSecondaryButtonText().resolve(getContext()));
            this.secondaryButton.setOnClickListener(new View.OnClickListener(megaphoneActionController) { // from class: org.thoughtcrime.securesms.megaphone.BasicMegaphoneView$$ExternalSyntheticLambda2
                public final /* synthetic */ MegaphoneActionController f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    BasicMegaphoneView.$r8$lambda$wb9mZxwut4xSvSHJkGy84rZhCso(Megaphone.this, this.f$1, view);
                }
            });
            return;
        }
        this.secondaryButton.setVisibility(8);
    }

    public static /* synthetic */ void lambda$present$0(Megaphone megaphone, MegaphoneActionController megaphoneActionController, View view) {
        if (megaphone.getButtonClickListener() != null) {
            megaphone.getButtonClickListener().onEvent(megaphone, megaphoneActionController);
        }
    }

    public static /* synthetic */ void lambda$present$1(MegaphoneActionController megaphoneActionController, Megaphone megaphone, View view) {
        megaphoneActionController.onMegaphoneSnooze(megaphone.getEvent());
        if (megaphone.getSnoozeListener() != null) {
            megaphone.getSnoozeListener().onEvent(megaphone, megaphoneActionController);
        }
    }

    public static /* synthetic */ void lambda$present$2(Megaphone megaphone, MegaphoneActionController megaphoneActionController, View view) {
        if (megaphone.getSecondaryButtonClickListener() != null) {
            megaphone.getSecondaryButtonClickListener().onEvent(megaphone, megaphoneActionController);
        }
    }
}
