package org.thoughtcrime.securesms.megaphone;

/* loaded from: classes4.dex */
public interface MegaphoneSchedule {
    boolean shouldDisplay(int i, long j, long j2, long j3);
}
