package org.thoughtcrime.securesms.messageprocessingalarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import j$.util.Optional;
import java.util.Locale;
import java.util.Objects;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.PushNotificationReceiveJob;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes.dex */
public final class MessageProcessReceiver extends BroadcastReceiver {
    public static final String BROADCAST_ACTION;
    private static final String TAG = Log.tag(MessageProcessReceiver.class);

    @Override // android.content.BroadcastReceiver
    public void onReceive(Context context, Intent intent) {
        String str = TAG;
        Log.i(str, String.format("onReceive(%s)", intent.getAction()));
        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
            Log.i(str, "Starting Alarm because of boot receiver");
            startOrUpdateAlarm(context);
        } else if (!BROADCAST_ACTION.equals(intent.getAction())) {
        } else {
            if (ApplicationDependencies.getAppForegroundObserver().isForegrounded()) {
                Log.i(str, "App is foregrounded");
                return;
            }
            long backgroundMessageProcessForegroundDelay = FeatureFlags.getBackgroundMessageProcessForegroundDelay();
            long j = 200 + backgroundMessageProcessForegroundDelay;
            Log.i(str, String.format(Locale.US, "Starting PushNotificationReceiveJob asynchronously with %d delay before foreground shown", Long.valueOf(backgroundMessageProcessForegroundDelay)));
            BroadcastReceiver.PendingResult goAsync = goAsync();
            Handler handler = new Handler(Looper.getMainLooper());
            Objects.requireNonNull(goAsync);
            handler.postDelayed(new Runnable(goAsync) { // from class: org.thoughtcrime.securesms.messageprocessingalarm.MessageProcessReceiver$$ExternalSyntheticLambda0
                public final /* synthetic */ BroadcastReceiver.PendingResult f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.f$0.finish();
                }
            }, j);
            SignalExecutors.BOUNDED.submit(new Runnable(backgroundMessageProcessForegroundDelay, j) { // from class: org.thoughtcrime.securesms.messageprocessingalarm.MessageProcessReceiver$$ExternalSyntheticLambda1
                public final /* synthetic */ long f$0;
                public final /* synthetic */ long f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r3;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    MessageProcessReceiver.lambda$onReceive$0(this.f$0, this.f$1);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$onReceive$0(long j, long j2) {
        String str = TAG;
        Log.i(str, "Running PushNotificationReceiveJob");
        Optional<JobTracker.JobState> runSynchronously = ApplicationDependencies.getJobManager().runSynchronously(PushNotificationReceiveJob.withDelayedForegroundService(j), j2);
        StringBuilder sb = new StringBuilder();
        sb.append("PushNotificationReceiveJob ended: ");
        sb.append(runSynchronously.isPresent() ? runSynchronously.get().toString() : "Job did not complete");
        Log.i(str, sb.toString());
    }

    public static void startOrUpdateAlarm(Context context) {
        Intent intent = new Intent(context, MessageProcessReceiver.class);
        intent.setAction(BROADCAST_ACTION);
        PendingIntent broadcast = PendingIntent.getBroadcast(context, 123, intent, 134217728);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService("alarm");
        long backgroundMessageProcessInterval = FeatureFlags.getBackgroundMessageProcessInterval();
        if (backgroundMessageProcessInterval < 0) {
            alarmManager.cancel(broadcast);
            Log.i(TAG, "Alarm cancelled");
            return;
        }
        alarmManager.setRepeating(2, SystemClock.elapsedRealtime() + backgroundMessageProcessInterval, backgroundMessageProcessInterval, broadcast);
        String str = TAG;
        Log.i(str, "Alarm scheduled to repeat at interval " + backgroundMessageProcessInterval);
    }
}
