package org.thoughtcrime.securesms;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import j$.util.Optional;
import j$.util.function.Consumer;
import java.io.IOException;
import java.lang.ref.WeakReference;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.components.ContactFilterView;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes.dex */
public abstract class ContactSelectionActivity extends PassphraseRequiredActivity implements SwipeRefreshLayout.OnRefreshListener, ContactSelectionListFragment.OnContactSelectedListener, ContactSelectionListFragment.ScrollCallback {
    public static final String EXTRA_LAYOUT_RES_ID;
    private static final String TAG = Log.tag(ContactSelectionActivity.class);
    private ContactFilterView contactFilterView;
    protected ContactSelectionListFragment contactsFragment;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private Toolbar toolbar;

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        if (!getIntent().hasExtra("display_mode")) {
            getIntent().putExtra("display_mode", Util.isDefaultSmsProvider(this) ? 31 : 29);
        }
        setContentView(getIntent().getIntExtra(EXTRA_LAYOUT_RES_ID, R.layout.contact_selection_activity));
        initializeContactFilterView();
        initializeToolbar();
        initializeResources();
        initializeSearch();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    public Toolbar getToolbar() {
        return this.toolbar;
    }

    public ContactFilterView getContactFilterView() {
        return this.contactFilterView;
    }

    private void initializeContactFilterView() {
        this.contactFilterView = (ContactFilterView) findViewById(R.id.contact_filter_edit_text);
    }

    private void initializeToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.toolbar = toolbar;
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setIcon(null);
        getSupportActionBar().setLogo(null);
    }

    private void initializeResources() {
        ContactSelectionListFragment contactSelectionListFragment = (ContactSelectionListFragment) getSupportFragmentManager().findFragmentById(R.id.contact_selection_list_fragment);
        this.contactsFragment = contactSelectionListFragment;
        contactSelectionListFragment.setOnRefreshListener(this);
    }

    private void initializeSearch() {
        this.contactFilterView.setOnFilterChangedListener(new ContactFilterView.OnFilterChangedListener() { // from class: org.thoughtcrime.securesms.ContactSelectionActivity$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.components.ContactFilterView.OnFilterChangedListener
            public final void onFilterChanged(String str) {
                ContactSelectionActivity.$r8$lambda$yHeWwczjqDFRG8ixvi4_JnIV89A(ContactSelectionActivity.this, str);
            }
        });
    }

    public /* synthetic */ void lambda$initializeSearch$0(String str) {
        this.contactsFragment.setQueryFilter(str);
    }

    @Override // androidx.swiperefreshlayout.widget.SwipeRefreshLayout.OnRefreshListener
    public void onRefresh() {
        new RefreshDirectoryTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, getApplicationContext());
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, Consumer<Boolean> consumer) {
        consumer.accept(Boolean.TRUE);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.ScrollCallback
    public void onBeginScroll() {
        hideKeyboard();
    }

    private void hideKeyboard() {
        ServiceUtil.getInputMethodManager(this).hideSoftInputFromWindow(this.toolbar.getWindowToken(), 0);
        this.toolbar.clearFocus();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes.dex */
    public static class RefreshDirectoryTask extends AsyncTask<Context, Void, Void> {
        private final WeakReference<ContactSelectionActivity> activity;

        private RefreshDirectoryTask(ContactSelectionActivity contactSelectionActivity) {
            this.activity = new WeakReference<>(contactSelectionActivity);
        }

        public Void doInBackground(Context... contextArr) {
            try {
                ContactDiscovery.refreshAll(contextArr[0], true);
                return null;
            } catch (IOException e) {
                Log.w(ContactSelectionActivity.TAG, e);
                return null;
            }
        }

        public void onPostExecute(Void r2) {
            ContactSelectionActivity contactSelectionActivity = this.activity.get();
            if (contactSelectionActivity != null && !contactSelectionActivity.isFinishing()) {
                contactSelectionActivity.contactFilterView.clear();
                contactSelectionActivity.contactsFragment.resetQueryFilter();
            }
        }
    }
}
