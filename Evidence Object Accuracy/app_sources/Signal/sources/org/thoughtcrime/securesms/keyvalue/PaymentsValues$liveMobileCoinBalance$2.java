package org.thoughtcrime.securesms.keyvalue;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.payments.Balance;
import org.thoughtcrime.securesms.payments.MobileCoinLedgerWrapper;

/* compiled from: PaymentsValues.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u0010\u0012\f\u0012\n \u0003*\u0004\u0018\u00010\u00020\u00020\u0001H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/payments/Balance;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PaymentsValues$liveMobileCoinBalance$2 extends Lambda implements Function0<LiveData<Balance>> {
    final /* synthetic */ PaymentsValues this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PaymentsValues$liveMobileCoinBalance$2(PaymentsValues paymentsValues) {
        super(0);
        this.this$0 = paymentsValues;
    }

    /* renamed from: invoke$lambda-0 */
    public static final Balance m2010invoke$lambda0(MobileCoinLedgerWrapper mobileCoinLedgerWrapper) {
        Intrinsics.checkNotNullParameter(mobileCoinLedgerWrapper, "obj");
        return mobileCoinLedgerWrapper.getBalance();
    }

    @Override // kotlin.jvm.functions.Function0
    public final LiveData<Balance> invoke() {
        return Transformations.map(this.this$0.getLiveMobileCoinLedger(), new Function() { // from class: org.thoughtcrime.securesms.keyvalue.PaymentsValues$liveMobileCoinBalance$2$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return PaymentsValues$liveMobileCoinBalance$2.m2010invoke$lambda0((MobileCoinLedgerWrapper) obj);
            }
        });
    }
}
