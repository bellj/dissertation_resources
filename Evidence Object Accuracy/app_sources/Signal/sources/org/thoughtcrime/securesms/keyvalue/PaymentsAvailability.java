package org.thoughtcrime.securesms.keyvalue;

/* loaded from: classes4.dex */
public enum PaymentsAvailability {
    NOT_IN_REGION(false, false),
    DISABLED_REMOTELY(false, false),
    REGISTRATION_AVAILABLE(false, true),
    WITHDRAW_ONLY(true, true),
    WITHDRAW_AND_SEND(true, true);
    
    private final boolean isEnabled;
    private final boolean showPaymentsMenu;

    PaymentsAvailability(boolean z, boolean z2) {
        this.showPaymentsMenu = z2;
        this.isEnabled = z;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public boolean showPaymentsMenu() {
        return this.showPaymentsMenu;
    }

    public boolean isSendAllowed() {
        return this == WITHDRAW_AND_SEND;
    }

    public boolean canRegister() {
        return this == REGISTRATION_AVAILABLE;
    }
}
