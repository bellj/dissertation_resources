package org.thoughtcrime.securesms.keyvalue;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: SignalStoreValueDelegates.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0015\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0007H\u0010¢\u0006\u0002\b\u000bJ\u001d\u0010\f\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u000e\u001a\u00020\u0002H\u0010¢\u0006\u0002\b\u000fR\u000e\u0010\u0005\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/BlobValue;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "", "key", "", "default", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Ljava/lang/String;[BLorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "getValue", "values", "getValue$Signal_Android_websiteProdRelease", "setValue", "", DraftDatabase.DRAFT_VALUE, "setValue$Signal_Android_websiteProdRelease", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BlobValue extends SignalStoreValueDelegate<byte[]> {

    /* renamed from: default */
    private final byte[] f4default;
    private final String key;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BlobValue(String str, byte[] bArr, KeyValueStore keyValueStore) {
        super(keyValueStore, null);
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(bArr, "default");
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
        this.key = str;
        this.f4default = bArr;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public byte[] getValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        byte[] blob = keyValueStore.getBlob(this.key, this.f4default);
        Intrinsics.checkNotNullExpressionValue(blob, "values.getBlob(key, default)");
        return blob;
    }

    public void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, byte[] bArr) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        Intrinsics.checkNotNullParameter(bArr, DraftDatabase.DRAFT_VALUE);
        keyValueStore.beginWrite().putBlob(this.key, bArr).apply();
    }
}
