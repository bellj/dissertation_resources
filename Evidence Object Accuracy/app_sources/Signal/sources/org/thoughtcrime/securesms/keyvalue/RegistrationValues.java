package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public final class RegistrationValues extends SignalStoreValues {
    private static final String HAS_UPLOADED_PROFILE;
    private static final String PIN_REQUIRED;
    private static final String REGISTRATION_COMPLETE;

    public RegistrationValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public synchronized void onFirstEverAppLaunch() {
        getStore().beginWrite().putBoolean(HAS_UPLOADED_PROFILE, false).putBoolean(REGISTRATION_COMPLETE, false).putBoolean(PIN_REQUIRED, true).commit();
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public synchronized void clearRegistrationComplete() {
        onFirstEverAppLaunch();
    }

    public synchronized void setRegistrationComplete() {
        getStore().beginWrite().putBoolean(REGISTRATION_COMPLETE, true).commit();
    }

    public synchronized boolean pinWasRequiredAtRegistration() {
        return getStore().getBoolean(PIN_REQUIRED, false);
    }

    public synchronized boolean isRegistrationComplete() {
        return getStore().getBoolean(REGISTRATION_COMPLETE, true);
    }

    public boolean hasUploadedProfile() {
        return getBoolean(HAS_UPLOADED_PROFILE, true);
    }

    public void markHasUploadedProfile() {
        putBoolean(HAS_UPLOADED_PROFILE, true);
    }

    public void clearHasUploadedProfile() {
        putBoolean(HAS_UPLOADED_PROFILE, false);
    }
}
