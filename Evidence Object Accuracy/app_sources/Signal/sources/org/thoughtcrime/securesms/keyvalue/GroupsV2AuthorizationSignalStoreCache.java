package org.thoughtcrime.securesms.keyvalue;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.zkgroup.InvalidInputException;
import org.signal.libsignal.zkgroup.auth.AuthCredentialWithPniResponse;
import org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponse;
import org.thoughtcrime.securesms.database.model.databaseprotos.TemporalAuthCredentialResponses;
import org.thoughtcrime.securesms.groups.GroupsV2Authorization;

/* loaded from: classes4.dex */
public final class GroupsV2AuthorizationSignalStoreCache implements GroupsV2Authorization.ValueCache {
    private static final String ACI_PNI_PREFIX;
    private static final int ACI_PNI_VERSION;
    private static final String TAG = Log.tag(GroupsV2AuthorizationSignalStoreCache.class);
    private final String key;
    private final KeyValueStore store;

    public static GroupsV2AuthorizationSignalStoreCache createAciCache(KeyValueStore keyValueStore) {
        if (keyValueStore.containsKey(ACI_PNI_PREFIX)) {
            keyValueStore.beginWrite().remove(ACI_PNI_PREFIX).commit();
        }
        return new GroupsV2AuthorizationSignalStoreCache(keyValueStore, "gv2:auth_token_cache:3");
    }

    private GroupsV2AuthorizationSignalStoreCache(KeyValueStore keyValueStore, String str) {
        this.store = keyValueStore;
        this.key = str;
    }

    @Override // org.thoughtcrime.securesms.groups.GroupsV2Authorization.ValueCache
    public void clear() {
        this.store.beginWrite().remove(this.key).commit();
        Log.i(TAG, "Cleared local response cache");
    }

    @Override // org.thoughtcrime.securesms.groups.GroupsV2Authorization.ValueCache
    public Map<Long, AuthCredentialWithPniResponse> read() {
        byte[] blob = this.store.getBlob(this.key, null);
        if (blob == null) {
            Log.i(TAG, "No credentials responses are cached locally");
            return Collections.emptyMap();
        }
        try {
            TemporalAuthCredentialResponses parseFrom = TemporalAuthCredentialResponses.parseFrom(blob);
            HashMap hashMap = new HashMap(parseFrom.getCredentialResponseCount());
            for (TemporalAuthCredentialResponse temporalAuthCredentialResponse : parseFrom.getCredentialResponseList()) {
                hashMap.put(Long.valueOf(temporalAuthCredentialResponse.getDate()), new AuthCredentialWithPniResponse(temporalAuthCredentialResponse.getAuthCredentialResponse().toByteArray()));
            }
            Log.i(TAG, String.format(Locale.US, "Loaded %d credentials from local storage", Integer.valueOf(hashMap.size())));
            return hashMap;
        } catch (InvalidProtocolBufferException | InvalidInputException e) {
            throw new AssertionError(e);
        }
    }

    @Override // org.thoughtcrime.securesms.groups.GroupsV2Authorization.ValueCache
    public void write(Map<Long, AuthCredentialWithPniResponse> map) {
        TemporalAuthCredentialResponses.Builder newBuilder = TemporalAuthCredentialResponses.newBuilder();
        for (Map.Entry<Long, AuthCredentialWithPniResponse> entry : map.entrySet()) {
            newBuilder.addCredentialResponse(TemporalAuthCredentialResponse.newBuilder().setDate(entry.getKey().longValue()).setAuthCredentialResponse(ByteString.copyFrom(entry.getValue().serialize())));
        }
        this.store.beginWrite().putBlob(this.key, newBuilder.build().toByteArray()).commit();
        Log.i(TAG, String.format(Locale.US, "Written %d credentials to local storage", Integer.valueOf(map.size())));
    }
}
