package org.thoughtcrime.securesms.keyvalue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes4.dex */
public final class PhoneNumberPrivacyValues extends SignalStoreValues {
    private static final Collection<CertificateType> BOTH_CERTIFICATES;
    public static final String LISTING_MODE;
    private static final Collection<CertificateType> PRIVACY_CERTIFICATE;
    private static final Collection<CertificateType> REGULAR_CERTIFICATE;
    public static final String SHARING_MODE;

    /* loaded from: classes4.dex */
    public enum PhoneNumberSharingMode {
        EVERYONE,
        CONTACTS,
        NOBODY
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    static {
        CertificateType certificateType = CertificateType.UUID_AND_E164;
        REGULAR_CERTIFICATE = Collections.singletonList(certificateType);
        CertificateType certificateType2 = CertificateType.UUID_ONLY;
        PRIVACY_CERTIFICATE = Collections.singletonList(certificateType2);
        BOTH_CERTIFICATES = Collections.unmodifiableCollection(Arrays.asList(certificateType, certificateType2));
    }

    public PhoneNumberPrivacyValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Arrays.asList(SHARING_MODE, LISTING_MODE);
    }

    public PhoneNumberSharingMode getPhoneNumberSharingMode() {
        if (!FeatureFlags.phoneNumberPrivacy()) {
            return PhoneNumberSharingMode.EVERYONE;
        }
        return PhoneNumberSharingMode.values()[getInteger(SHARING_MODE, PhoneNumberSharingMode.EVERYONE.ordinal())];
    }

    public void setPhoneNumberSharingMode(PhoneNumberSharingMode phoneNumberSharingMode) {
        putInteger(SHARING_MODE, phoneNumberSharingMode.ordinal());
    }

    public PhoneNumberListingMode getPhoneNumberListingMode() {
        if (!FeatureFlags.phoneNumberPrivacy()) {
            return PhoneNumberListingMode.LISTED;
        }
        return PhoneNumberListingMode.values()[getInteger(LISTING_MODE, PhoneNumberListingMode.LISTED.ordinal())];
    }

    public void setPhoneNumberListingMode(PhoneNumberListingMode phoneNumberListingMode) {
        putInteger(LISTING_MODE, phoneNumberListingMode.ordinal());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode;

        static {
            int[] iArr = new int[PhoneNumberSharingMode.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode = iArr;
            try {
                iArr[PhoneNumberSharingMode.EVERYONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[PhoneNumberSharingMode.CONTACTS.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[PhoneNumberSharingMode.NOBODY.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public Collection<CertificateType> getRequiredCertificateTypes() {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$PhoneNumberPrivacyValues$PhoneNumberSharingMode[getPhoneNumberSharingMode().ordinal()];
        if (i == 1) {
            return REGULAR_CERTIFICATE;
        }
        if (i == 2) {
            return BOTH_CERTIFICATES;
        }
        if (i == 3) {
            return PRIVACY_CERTIFICATE;
        }
        throw new AssertionError();
    }

    public Collection<CertificateType> getAllCertificateTypes() {
        return FeatureFlags.phoneNumberPrivacy() ? BOTH_CERTIFICATES : REGULAR_CERTIFICATE;
    }

    /* loaded from: classes4.dex */
    public enum PhoneNumberListingMode {
        LISTED,
        UNLISTED;

        public boolean isDiscoverable() {
            return this == LISTED;
        }

        public boolean isUnlisted() {
            return this == UNLISTED;
        }
    }
}
