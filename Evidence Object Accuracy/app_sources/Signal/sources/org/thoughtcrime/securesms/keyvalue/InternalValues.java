package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* loaded from: classes4.dex */
public final class InternalValues extends SignalStoreValues {
    public static final String ALLOW_CENSORSHIP_SETTING;
    public static final String CALLING_AUDIO_PROCESSING_METHOD;
    public static final String CALLING_BANDWIDTH_MODE;
    public static final String CALLING_DISABLE_TELECOM;
    public static final String CALLING_SERVER;
    public static final String DELAY_RESENDS;
    public static final String DISABLE_STORAGE_SERVICE;
    public static final String FORCE_BUILT_IN_EMOJI;
    public static final String GV2_DISABLE_AUTOMIGRATE_INITIATION;
    public static final String GV2_DISABLE_AUTOMIGRATE_NOTIFICATION;
    public static final String GV2_DO_NOT_CREATE_GV2;
    public static final String GV2_FORCE_INVITES;
    public static final String GV2_IGNORE_P2P_CHANGES;
    public static final String GV2_IGNORE_SERVER_CHANGES;
    public static final String RECIPIENT_DETAILS;
    public static final String REMOVE_SENDER_KEY_MINIMUM;
    public static final String SHAKE_TO_REPORT;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public InternalValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public synchronized boolean gv2DoNotCreateGv2Groups() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(GV2_DO_NOT_CREATE_GV2, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean gv2ForceInvites() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(GV2_FORCE_INVITES, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean gv2IgnoreServerChanges() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(GV2_IGNORE_SERVER_CHANGES, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean gv2IgnoreP2PChanges() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(GV2_IGNORE_P2P_CHANGES, false)) {
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (getBoolean(org.thoughtcrime.securesms.keyvalue.InternalValues.RECIPIENT_DETAILS, true) != false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean recipientDetails() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = org.thoughtcrime.securesms.util.FeatureFlags.internalUser()     // Catch: all -> 0x0014
            r1 = 1
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "internal.recipient_details"
            boolean r0 = r2.getBoolean(r0, r1)     // Catch: all -> 0x0014
            if (r0 == 0) goto L_0x0011
            goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            monitor-exit(r2)
            return r1
        L_0x0014:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyvalue.InternalValues.recipientDetails():boolean");
    }

    public synchronized boolean allowChangingCensorshipSetting() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(ALLOW_CENSORSHIP_SETTING, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean forceBuiltInEmoji() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(FORCE_BUILT_IN_EMOJI, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean removeSenderKeyMinimum() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(REMOVE_SENDER_KEY_MINIMUM, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean delayResends() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(DELAY_RESENDS, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean disableGv1AutoMigrateInitiation() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(GV2_DISABLE_AUTOMIGRATE_INITIATION, false)) {
                z = true;
            }
        }
        return z;
    }

    public synchronized boolean disableGv1AutoMigrateNotification() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(GV2_DISABLE_AUTOMIGRATE_NOTIFICATION, false)) {
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        if (getBoolean(org.thoughtcrime.securesms.keyvalue.InternalValues.SHAKE_TO_REPORT, true) != false) goto L_0x0012;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean shakeToReport() {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = org.thoughtcrime.securesms.util.FeatureFlags.internalUser()     // Catch: all -> 0x0014
            r1 = 1
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "internal.shake_to_report"
            boolean r0 = r2.getBoolean(r0, r1)     // Catch: all -> 0x0014
            if (r0 == 0) goto L_0x0011
            goto L_0x0012
        L_0x0011:
            r1 = 0
        L_0x0012:
            monitor-exit(r2)
            return r1
        L_0x0014:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyvalue.InternalValues.shakeToReport():boolean");
    }

    public synchronized boolean storageServiceDisabled() {
        boolean z;
        z = false;
        if (FeatureFlags.internalUser()) {
            if (getBoolean(DISABLE_STORAGE_SERVICE, false)) {
                z = true;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0022  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0023  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String groupCallingServer() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = org.thoughtcrime.securesms.util.FeatureFlags.internalUser()     // Catch: all -> 0x0027
            r1 = 0
            if (r0 == 0) goto L_0x000f
            java.lang.String r0 = "internal.calling_server"
            java.lang.String r0 = r3.getString(r0, r1)     // Catch: all -> 0x0027
            goto L_0x0010
        L_0x000f:
            r0 = r1
        L_0x0010:
            if (r0 == 0) goto L_0x001f
            java.lang.String[] r2 = org.thoughtcrime.securesms.BuildConfig.SIGNAL_SFU_INTERNAL_URLS     // Catch: all -> 0x0027
            java.util.List r2 = java.util.Arrays.asList(r2)     // Catch: all -> 0x0027
            boolean r2 = r2.contains(r0)     // Catch: all -> 0x0027
            if (r2 != 0) goto L_0x001f
            goto L_0x0020
        L_0x001f:
            r1 = r0
        L_0x0020:
            if (r1 == 0) goto L_0x0023
            goto L_0x0025
        L_0x0023:
            java.lang.String r1 = "https://sfu.voip.signal.org"
        L_0x0025:
            monitor-exit(r3)
            return r1
        L_0x0027:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.keyvalue.InternalValues.groupCallingServer():java.lang.String");
    }

    public synchronized CallManager.AudioProcessingMethod callingAudioProcessingMethod() {
        if (FeatureFlags.internalUser()) {
            return CallManager.AudioProcessingMethod.values()[getInteger(CALLING_AUDIO_PROCESSING_METHOD, CallManager.AudioProcessingMethod.Default.ordinal())];
        }
        return CallManager.AudioProcessingMethod.Default;
    }

    public synchronized CallManager.BandwidthMode callingBandwidthMode() {
        if (FeatureFlags.internalUser()) {
            return CallManager.BandwidthMode.values()[getInteger(CALLING_BANDWIDTH_MODE, CallManager.BandwidthMode.NORMAL.ordinal())];
        }
        return CallManager.BandwidthMode.NORMAL;
    }

    public synchronized boolean callingDisableTelecom() {
        if (!FeatureFlags.internalUser()) {
            return false;
        }
        return getBoolean(CALLING_DISABLE_TELECOM, false);
    }
}
