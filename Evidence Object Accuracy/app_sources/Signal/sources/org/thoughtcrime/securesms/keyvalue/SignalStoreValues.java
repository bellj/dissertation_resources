package org.thoughtcrime.securesms.keyvalue;

import com.google.protobuf.InvalidProtocolBufferException;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import j$.util.stream.Stream;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.StringSerializer;
import org.thoughtcrime.securesms.database.model.databaseprotos.SignalStoreList;

/* loaded from: classes.dex */
public abstract class SignalStoreValues {
    private final KeyValueStore store;

    abstract List<String> getKeysToIncludeInBackup();

    abstract void onFirstEverAppLaunch();

    public SignalStoreValues(KeyValueStore keyValueStore) {
        this.store = keyValueStore;
    }

    public KeyValueStore getStore() {
        return this.store;
    }

    public String getString(String str, String str2) {
        return this.store.getString(str, str2);
    }

    public int getInteger(String str, int i) {
        return this.store.getInteger(str, i);
    }

    public long getLong(String str, long j) {
        return this.store.getLong(str, j);
    }

    public boolean getBoolean(String str, boolean z) {
        return this.store.getBoolean(str, z);
    }

    float getFloat(String str, float f) {
        return this.store.getFloat(str, f);
    }

    public byte[] getBlob(String str, byte[] bArr) {
        return this.store.getBlob(str, bArr);
    }

    public <T> List<T> getList(String str, StringSerializer<T> stringSerializer) {
        byte[] blob = getBlob(str, null);
        if (blob == null) {
            return Collections.emptyList();
        }
        try {
            Stream stream = Collection$EL.stream(SignalStoreList.parseFrom(blob).getContentsList());
            Objects.requireNonNull(stringSerializer);
            return (List) stream.map(new Function() { // from class: org.thoughtcrime.securesms.keyvalue.SignalStoreValues$$ExternalSyntheticLambda1
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return StringSerializer.this.deserialize((String) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).collect(Collectors.toList());
        } catch (InvalidProtocolBufferException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void putBlob(String str, byte[] bArr) {
        this.store.beginWrite().putBlob(str, bArr).apply();
    }

    public void putBoolean(String str, boolean z) {
        this.store.beginWrite().putBoolean(str, z).apply();
    }

    void putFloat(String str, float f) {
        this.store.beginWrite().putFloat(str, f).apply();
    }

    public void putInteger(String str, int i) {
        this.store.beginWrite().putInteger(str, i).apply();
    }

    public void putLong(String str, long j) {
        this.store.beginWrite().putLong(str, j).apply();
    }

    public void putString(String str, String str2) {
        this.store.beginWrite().putString(str, str2).apply();
    }

    public <T> void putList(String str, List<T> list, StringSerializer<T> stringSerializer) {
        SignalStoreList.Builder newBuilder = SignalStoreList.newBuilder();
        Stream stream = Collection$EL.stream(list);
        Objects.requireNonNull(stringSerializer);
        putBlob(str, newBuilder.addAllContents((Iterable) stream.map(new Function() { // from class: org.thoughtcrime.securesms.keyvalue.SignalStoreValues$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StringSerializer.this.serialize(obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList())).build().toByteArray());
    }

    public void remove(String str) {
        this.store.beginWrite().remove(str).apply();
    }
}
