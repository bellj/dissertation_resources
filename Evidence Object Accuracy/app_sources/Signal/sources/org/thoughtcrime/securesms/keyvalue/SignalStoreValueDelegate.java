package org.thoughtcrime.securesms.keyvalue;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.reflect.KProperty;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: SignalStoreValueDelegates.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u000f\b\u0004\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J$\u0010\u0006\u001a\u00028\u00002\b\u0010\u0007\u001a\u0004\u0018\u00010\u00022\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\tH\u0002¢\u0006\u0002\u0010\nJ\u0017\u0010\u0006\u001a\u00028\u00002\u0006\u0010\u000b\u001a\u00020\u0004H ¢\u0006\u0004\b\f\u0010\rJ,\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0007\u001a\u0004\u0018\u00010\u00022\n\u0010\b\u001a\u0006\u0012\u0002\b\u00030\t2\u0006\u0010\u0010\u001a\u00028\u0000H\u0002¢\u0006\u0002\u0010\u0011J\u001f\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00028\u0000H ¢\u0006\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000\u0001\u0007\u0014\u0015\u0016\u0017\u0018\u0019\u001a¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "T", "", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "getValue", "thisRef", "property", "Lkotlin/reflect/KProperty;", "(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;", "values", "getValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)Ljava/lang/Object;", "setValue", "", DraftDatabase.DRAFT_VALUE, "(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V", "setValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;Ljava/lang/Object;)V", "Lorg/thoughtcrime/securesms/keyvalue/LongValue;", "Lorg/thoughtcrime/securesms/keyvalue/BooleanValue;", "Lorg/thoughtcrime/securesms/keyvalue/StringValue;", "Lorg/thoughtcrime/securesms/keyvalue/IntValue;", "Lorg/thoughtcrime/securesms/keyvalue/FloatValue;", "Lorg/thoughtcrime/securesms/keyvalue/BlobValue;", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueEnumValue;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class SignalStoreValueDelegate<T> {
    private final KeyValueStore store;

    public /* synthetic */ SignalStoreValueDelegate(KeyValueStore keyValueStore, DefaultConstructorMarker defaultConstructorMarker) {
        this(keyValueStore);
    }

    public abstract T getValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore);

    public abstract void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, T t);

    private SignalStoreValueDelegate(KeyValueStore keyValueStore) {
        this.store = keyValueStore;
    }

    public final T getValue(Object obj, KProperty<?> kProperty) {
        Intrinsics.checkNotNullParameter(kProperty, "property");
        return getValue$Signal_Android_websiteProdRelease(this.store);
    }

    public final void setValue(Object obj, KProperty<?> kProperty, T t) {
        Intrinsics.checkNotNullParameter(kProperty, "property");
        setValue$Signal_Android_websiteProdRelease(this.store, t);
    }
}
