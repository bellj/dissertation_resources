package org.thoughtcrime.securesms.keyvalue;

/* loaded from: classes4.dex */
interface KeyValueReader {
    boolean containsKey(String str);

    byte[] getBlob(String str, byte[] bArr);

    boolean getBoolean(String str, boolean z);

    float getFloat(String str, float f);

    int getInteger(String str, int i);

    long getLong(String str, long j);

    String getString(String str, String str2);
}
