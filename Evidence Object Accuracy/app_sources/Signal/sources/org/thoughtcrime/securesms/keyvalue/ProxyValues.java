package org.thoughtcrime.securesms.keyvalue;

import java.util.Arrays;
import java.util.List;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.internal.configuration.SignalProxy;

/* loaded from: classes.dex */
public final class ProxyValues extends SignalStoreValues {
    private static final String KEY_HOST;
    private static final String KEY_PORT;
    private static final String KEY_PROXY_ENABLED;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public ProxyValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Arrays.asList(KEY_PROXY_ENABLED, KEY_HOST, KEY_PORT);
    }

    public void enableProxy(SignalProxy signalProxy) {
        if (!Util.isEmpty(signalProxy.getHost())) {
            getStore().beginWrite().putBoolean(KEY_PROXY_ENABLED, true).putString(KEY_HOST, signalProxy.getHost()).putInteger(KEY_PORT, signalProxy.getPort()).apply();
            return;
        }
        throw new IllegalArgumentException("Empty host!");
    }

    public void disableProxy() {
        putBoolean(KEY_PROXY_ENABLED, false);
    }

    public boolean isProxyEnabled() {
        return getBoolean(KEY_PROXY_ENABLED, false);
    }

    public void setProxy(SignalProxy signalProxy) {
        if (signalProxy != null) {
            getStore().beginWrite().putString(KEY_HOST, signalProxy.getHost()).putInteger(KEY_PORT, signalProxy.getPort()).apply();
        } else {
            getStore().beginWrite().remove(KEY_HOST).remove(KEY_PORT).apply();
        }
    }

    public SignalProxy getProxy() {
        String string = getString(KEY_HOST, null);
        int integer = getInteger(KEY_PORT, 0);
        if (string != null) {
            return new SignalProxy(string, integer);
        }
        return null;
    }

    public String getProxyHost() {
        SignalProxy proxy = getProxy();
        if (proxy != null) {
            return proxy.getHost();
        }
        return null;
    }
}
