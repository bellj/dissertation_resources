package org.thoughtcrime.securesms.keyvalue;

import androidx.preference.PreferenceDataStore;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.database.KeyValueDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* loaded from: classes.dex */
public final class SignalStore {
    private static volatile SignalStore instance;
    private final AccountValues accountValues;
    private final CertificateValues certificateValues;
    private final ChatColorsValues chatColorsValues;
    private final DonationsValues donationsValues;
    private final EmojiValues emojiValues;
    private final ImageEditorValues imageEditorValues;
    private final InternalValues internalValues;
    private final KbsValues kbsValues;
    private final MiscellaneousValues misc;
    private final NotificationProfileValues notificationProfileValues;
    private final OnboardingValues onboardingValues;
    private final PaymentsValues paymentsValues;
    private final PhoneNumberPrivacyValues phoneNumberPrivacyValues;
    private final PinValues pinValues;
    private final ProxyValues proxyValues;
    private final RateLimitValues rateLimitValues;
    private final RegistrationValues registrationValues;
    private final ReleaseChannelValues releaseChannelValues;
    private final RemoteConfigValues remoteConfigValues;
    private final SettingsValues settingsValues;
    private final StorageServiceValues storageServiceValues;
    private KeyValueStore store;
    private final StoryValues storyValues;
    private final TooltipValues tooltipValues;
    private final UiHints uiHints;
    private final WallpaperValues wallpaperValues;

    private static SignalStore getInstance() {
        if (instance == null) {
            synchronized (SignalStore.class) {
                if (instance == null) {
                    instance = new SignalStore(new KeyValueStore(KeyValueDatabase.getInstance(ApplicationDependencies.getApplication())));
                }
            }
        }
        return instance;
    }

    private SignalStore(KeyValueStore keyValueStore) {
        this.store = keyValueStore;
        this.accountValues = new AccountValues(keyValueStore);
        this.kbsValues = new KbsValues(keyValueStore);
        this.registrationValues = new RegistrationValues(keyValueStore);
        this.pinValues = new PinValues(keyValueStore);
        this.remoteConfigValues = new RemoteConfigValues(keyValueStore);
        this.storageServiceValues = new StorageServiceValues(keyValueStore);
        this.uiHints = new UiHints(keyValueStore);
        this.tooltipValues = new TooltipValues(keyValueStore);
        this.misc = new MiscellaneousValues(keyValueStore);
        this.internalValues = new InternalValues(keyValueStore);
        this.emojiValues = new EmojiValues(keyValueStore);
        this.settingsValues = new SettingsValues(keyValueStore);
        this.certificateValues = new CertificateValues(keyValueStore);
        this.phoneNumberPrivacyValues = new PhoneNumberPrivacyValues(keyValueStore);
        this.onboardingValues = new OnboardingValues(keyValueStore);
        this.wallpaperValues = new WallpaperValues(keyValueStore);
        this.paymentsValues = new PaymentsValues(keyValueStore);
        this.donationsValues = new DonationsValues(keyValueStore);
        this.proxyValues = new ProxyValues(keyValueStore);
        this.rateLimitValues = new RateLimitValues(keyValueStore);
        this.chatColorsValues = new ChatColorsValues(keyValueStore);
        this.imageEditorValues = new ImageEditorValues(keyValueStore);
        this.notificationProfileValues = new NotificationProfileValues(keyValueStore);
        this.releaseChannelValues = new ReleaseChannelValues(keyValueStore);
        this.storyValues = new StoryValues(keyValueStore);
    }

    public static void onFirstEverAppLaunch() {
        account().onFirstEverAppLaunch();
        kbsValues().onFirstEverAppLaunch();
        registrationValues().onFirstEverAppLaunch();
        pinValues().onFirstEverAppLaunch();
        remoteConfigValues().onFirstEverAppLaunch();
        storageService().onFirstEverAppLaunch();
        uiHints().onFirstEverAppLaunch();
        tooltips().onFirstEverAppLaunch();
        misc().onFirstEverAppLaunch();
        internalValues().onFirstEverAppLaunch();
        emojiValues().onFirstEverAppLaunch();
        settings().onFirstEverAppLaunch();
        certificateValues().onFirstEverAppLaunch();
        phoneNumberPrivacy().onFirstEverAppLaunch();
        onboarding().onFirstEverAppLaunch();
        wallpaper().onFirstEverAppLaunch();
        paymentsValues().onFirstEverAppLaunch();
        donationsValues().onFirstEverAppLaunch();
        proxy().onFirstEverAppLaunch();
        rateLimit().onFirstEverAppLaunch();
        chatColorsValues().onFirstEverAppLaunch();
        imageEditorValues().onFirstEverAppLaunch();
        notificationProfileValues().onFirstEverAppLaunch();
        releaseChannelValues().onFirstEverAppLaunch();
        storyValues().onFirstEverAppLaunch();
    }

    public static List<String> getKeysToIncludeInBackup() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(account().getKeysToIncludeInBackup());
        arrayList.addAll(kbsValues().getKeysToIncludeInBackup());
        arrayList.addAll(registrationValues().getKeysToIncludeInBackup());
        arrayList.addAll(pinValues().getKeysToIncludeInBackup());
        arrayList.addAll(remoteConfigValues().getKeysToIncludeInBackup());
        arrayList.addAll(storageService().getKeysToIncludeInBackup());
        arrayList.addAll(uiHints().getKeysToIncludeInBackup());
        arrayList.addAll(tooltips().getKeysToIncludeInBackup());
        arrayList.addAll(misc().getKeysToIncludeInBackup());
        arrayList.addAll(internalValues().getKeysToIncludeInBackup());
        arrayList.addAll(emojiValues().getKeysToIncludeInBackup());
        arrayList.addAll(settings().getKeysToIncludeInBackup());
        arrayList.addAll(certificateValues().getKeysToIncludeInBackup());
        arrayList.addAll(phoneNumberPrivacy().getKeysToIncludeInBackup());
        arrayList.addAll(onboarding().getKeysToIncludeInBackup());
        arrayList.addAll(wallpaper().getKeysToIncludeInBackup());
        arrayList.addAll(paymentsValues().getKeysToIncludeInBackup());
        arrayList.addAll(donationsValues().getKeysToIncludeInBackup());
        arrayList.addAll(proxy().getKeysToIncludeInBackup());
        arrayList.addAll(rateLimit().getKeysToIncludeInBackup());
        arrayList.addAll(chatColorsValues().getKeysToIncludeInBackup());
        arrayList.addAll(imageEditorValues().getKeysToIncludeInBackup());
        arrayList.addAll(notificationProfileValues().getKeysToIncludeInBackup());
        arrayList.addAll(releaseChannelValues().getKeysToIncludeInBackup());
        arrayList.addAll(storyValues().getKeysToIncludeInBackup());
        return arrayList;
    }

    public static void resetCache() {
        getInstance().store.resetCache();
    }

    public static void onPostBackupRestore() {
        getInstance().store.resetCache();
    }

    public static AccountValues account() {
        return getInstance().accountValues;
    }

    public static KbsValues kbsValues() {
        return getInstance().kbsValues;
    }

    public static RegistrationValues registrationValues() {
        return getInstance().registrationValues;
    }

    public static PinValues pinValues() {
        return getInstance().pinValues;
    }

    public static RemoteConfigValues remoteConfigValues() {
        return getInstance().remoteConfigValues;
    }

    public static StorageServiceValues storageService() {
        return getInstance().storageServiceValues;
    }

    public static UiHints uiHints() {
        return getInstance().uiHints;
    }

    public static TooltipValues tooltips() {
        return getInstance().tooltipValues;
    }

    public static MiscellaneousValues misc() {
        return getInstance().misc;
    }

    public static InternalValues internalValues() {
        return getInstance().internalValues;
    }

    public static EmojiValues emojiValues() {
        return getInstance().emojiValues;
    }

    public static SettingsValues settings() {
        return getInstance().settingsValues;
    }

    public static CertificateValues certificateValues() {
        return getInstance().certificateValues;
    }

    public static PhoneNumberPrivacyValues phoneNumberPrivacy() {
        return getInstance().phoneNumberPrivacyValues;
    }

    public static OnboardingValues onboarding() {
        return getInstance().onboardingValues;
    }

    public static WallpaperValues wallpaper() {
        return getInstance().wallpaperValues;
    }

    public static PaymentsValues paymentsValues() {
        return getInstance().paymentsValues;
    }

    public static DonationsValues donationsValues() {
        return getInstance().donationsValues;
    }

    public static ProxyValues proxy() {
        return getInstance().proxyValues;
    }

    public static RateLimitValues rateLimit() {
        return getInstance().rateLimitValues;
    }

    public static ChatColorsValues chatColorsValues() {
        return getInstance().chatColorsValues;
    }

    public static ImageEditorValues imageEditorValues() {
        return getInstance().imageEditorValues;
    }

    public static NotificationProfileValues notificationProfileValues() {
        return getInstance().notificationProfileValues;
    }

    public static ReleaseChannelValues releaseChannelValues() {
        return getInstance().releaseChannelValues;
    }

    public static StoryValues storyValues() {
        return getInstance().storyValues;
    }

    public static GroupsV2AuthorizationSignalStoreCache groupsV2AciAuthorizationCache() {
        return GroupsV2AuthorizationSignalStoreCache.createAciCache(getStore());
    }

    public static PreferenceDataStore getPreferenceDataStore() {
        return new SignalPreferenceDataStore(getStore());
    }

    public static void blockUntilAllWritesFinished() {
        getStore().blockUntilAllWritesFinished();
    }

    private static KeyValueStore getStore() {
        return getInstance().store;
    }

    public static void inject(KeyValueStore keyValueStore) {
        instance = new SignalStore(keyValueStore);
    }
}
