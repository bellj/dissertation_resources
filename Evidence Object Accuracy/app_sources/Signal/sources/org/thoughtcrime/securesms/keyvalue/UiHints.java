package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public class UiHints extends SignalStoreValues {
    private static final String HAS_CONFIRMED_DELETE_FOR_EVERYONE_ONCE;
    private static final String HAS_SEEN_GROUP_SETTINGS_MENU_TOAST;

    public UiHints(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
        markHasSeenGroupSettingsMenuToast();
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public void markHasSeenGroupSettingsMenuToast() {
        putBoolean(HAS_SEEN_GROUP_SETTINGS_MENU_TOAST, true);
    }

    public boolean hasSeenGroupSettingsMenuToast() {
        return getBoolean(HAS_SEEN_GROUP_SETTINGS_MENU_TOAST, false);
    }

    public void markHasConfirmedDeleteForEveryoneOnce() {
        putBoolean(HAS_CONFIRMED_DELETE_FOR_EVERYONE_ONCE, true);
    }

    public boolean hasConfirmedDeleteForEveryoneOnce() {
        return getBoolean(HAS_CONFIRMED_DELETE_FOR_EVERYONE_ONCE, false);
    }
}
