package org.thoughtcrime.securesms.keyvalue;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: SignalStoreValueDelegates.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0002\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0017\u0010\t\u001a\u00020\u00022\u0006\u0010\n\u001a\u00020\u0007H\u0010¢\u0006\u0004\b\u000b\u0010\fJ\u001d\u0010\r\u001a\u00020\u000e2\u0006\u0010\n\u001a\u00020\u00072\u0006\u0010\u000f\u001a\u00020\u0002H\u0010¢\u0006\u0002\b\u0010R\u000e\u0010\u0005\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/BooleanValue;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "", "key", "", "default", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Ljava/lang/String;ZLorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "getValue", "values", "getValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)Ljava/lang/Boolean;", "setValue", "", DraftDatabase.DRAFT_VALUE, "setValue$Signal_Android_websiteProdRelease", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BooleanValue extends SignalStoreValueDelegate<Boolean> {

    /* renamed from: default */
    private final boolean f5default;
    private final String key;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BooleanValue(String str, boolean z, KeyValueStore keyValueStore) {
        super(keyValueStore, null);
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
        this.key = str;
        this.f5default = z;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public /* bridge */ /* synthetic */ void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, Object obj) {
        setValue$Signal_Android_websiteProdRelease(keyValueStore, ((Boolean) obj).booleanValue());
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public Boolean getValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        return Boolean.valueOf(keyValueStore.getBoolean(this.key, this.f5default));
    }

    public void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, boolean z) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        keyValueStore.beginWrite().putBoolean(this.key, z).apply();
    }
}
