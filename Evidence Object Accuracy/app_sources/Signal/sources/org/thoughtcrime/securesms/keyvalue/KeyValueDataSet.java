package org.thoughtcrime.securesms.keyvalue;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* loaded from: classes4.dex */
public class KeyValueDataSet implements KeyValueReader {
    private final Map<String, Class> types = new HashMap();
    private final Map<String, Object> values = new HashMap();

    public void putBlob(String str, byte[] bArr) {
        this.values.put(str, bArr);
        this.types.put(str, byte[].class);
    }

    public void putBoolean(String str, boolean z) {
        this.values.put(str, Boolean.valueOf(z));
        this.types.put(str, Boolean.class);
    }

    public void putFloat(String str, float f) {
        this.values.put(str, Float.valueOf(f));
        this.types.put(str, Float.class);
    }

    public void putInteger(String str, int i) {
        this.values.put(str, Integer.valueOf(i));
        this.types.put(str, Integer.class);
    }

    public void putLong(String str, long j) {
        this.values.put(str, Long.valueOf(j));
        this.types.put(str, Long.class);
    }

    public void putString(String str, String str2) {
        this.values.put(str, str2);
        this.types.put(str, String.class);
    }

    public void putAll(KeyValueDataSet keyValueDataSet) {
        this.values.putAll(keyValueDataSet.values);
        this.types.putAll(keyValueDataSet.types);
    }

    public void removeAll(Collection<String> collection) {
        for (String str : collection) {
            this.values.remove(str);
            this.types.remove(str);
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public byte[] getBlob(String str, byte[] bArr) {
        return containsKey(str) ? (byte[]) readValueAsType(str, byte[].class, true) : bArr;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public boolean getBoolean(String str, boolean z) {
        return containsKey(str) ? ((Boolean) readValueAsType(str, Boolean.class, false)).booleanValue() : z;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public float getFloat(String str, float f) {
        return containsKey(str) ? ((Float) readValueAsType(str, Float.class, false)).floatValue() : f;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public int getInteger(String str, int i) {
        return containsKey(str) ? ((Integer) readValueAsType(str, Integer.class, false)).intValue() : i;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public long getLong(String str, long j) {
        return containsKey(str) ? ((Long) readValueAsType(str, Long.class, false)).longValue() : j;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public String getString(String str, String str2) {
        return containsKey(str) ? (String) readValueAsType(str, String.class, true) : str2;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public boolean containsKey(String str) {
        return this.values.containsKey(str);
    }

    public Map<String, Object> getValues() {
        return this.values;
    }

    public Class getType(String str) {
        return this.types.get(str);
    }

    private <E> E readValueAsType(String str, Class<E> cls, boolean z) {
        Object obj = this.values.get(str);
        if ((obj == null && z) || (obj != null && obj.getClass() == cls)) {
            return cls.cast(obj);
        }
        throw new IllegalArgumentException("Type mismatch!");
    }
}
