package org.thoughtcrime.securesms.keyvalue;

import com.google.protobuf.InvalidProtocolBufferException;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.ChatColor;

/* compiled from: ChatColorsValues.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 !2\u00020\u0001:\u0001!B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u001e0\u001dH\u0014J\b\u0010\u001f\u001a\u00020 H\u0014R(\u0010\u0007\u001a\u0004\u0018\u00010\u00062\b\u0010\u0005\u001a\u0004\u0018\u00010\u00068F@FX\u000e¢\u0006\f\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR$\u0010\r\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\f8B@BX\u000e¢\u0006\f\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0012\u001a\u00020\u00138G¢\u0006\u0006\u001a\u0004\b\u0012\u0010\u0014R$\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u00138F@FX\u000e¢\u0006\f\u001a\u0004\b\u0016\u0010\u0014\"\u0004\b\u0017\u0010\u0018R$\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u00138F@FX\u000e¢\u0006\f\u001a\u0004\b\u001a\u0010\u0014\"\u0004\b\u001b\u0010\u0018¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/ChatColorsValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", DraftDatabase.DRAFT_VALUE, "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "chatColors", "getChatColors", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;", "setChatColors", "(Lorg/thoughtcrime/securesms/conversation/colors/ChatColors;)V", "Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "chatColorsId", "getChatColorsId", "()Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;", "setChatColorsId", "(Lorg/thoughtcrime/securesms/conversation/colors/ChatColors$Id;)V", "hasChatColors", "", "()Z", "shouldShowAutoTooltip", "getShouldShowAutoTooltip", "setShouldShowAutoTooltip", "(Z)V", "shouldShowGradientTooltip", "getShouldShowGradientTooltip", "setShouldShowGradientTooltip", "getKeysToIncludeInBackup", "", "", "onFirstEverAppLaunch", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatColorsValues extends SignalStoreValues {
    public static final Companion Companion = new Companion(null);
    private static final String KEY_CHAT_COLORS;
    private static final String KEY_CHAT_COLORS_AUTO_TOOLTIP;
    private static final String KEY_CHAT_COLORS_GRADIENT_TOOLTIP;
    private static final String KEY_CHAT_COLORS_ID;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChatColorsValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: ChatColorsValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/ChatColorsValues$Companion;", "", "()V", "KEY_CHAT_COLORS", "", "KEY_CHAT_COLORS_AUTO_TOOLTIP", "KEY_CHAT_COLORS_GRADIENT_TOOLTIP", "KEY_CHAT_COLORS_ID", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.mutableListOf(KEY_CHAT_COLORS, KEY_CHAT_COLORS_ID, KEY_CHAT_COLORS_AUTO_TOOLTIP, KEY_CHAT_COLORS_GRADIENT_TOOLTIP);
    }

    public final boolean getShouldShowAutoTooltip() {
        return getBoolean(KEY_CHAT_COLORS_AUTO_TOOLTIP, true);
    }

    public final void setShouldShowAutoTooltip(boolean z) {
        putBoolean(KEY_CHAT_COLORS_AUTO_TOOLTIP, z);
    }

    public final boolean getShouldShowGradientTooltip() {
        return getBoolean(KEY_CHAT_COLORS_GRADIENT_TOOLTIP, true);
    }

    public final void setShouldShowGradientTooltip(boolean z) {
        putBoolean(KEY_CHAT_COLORS_GRADIENT_TOOLTIP, z);
    }

    public final boolean hasChatColors() {
        return getChatColors() != null;
    }

    public final ChatColors getChatColors() {
        byte[] blob = getBlob(KEY_CHAT_COLORS, null);
        if (blob == null) {
            return null;
        }
        try {
            ChatColors.Companion companion = ChatColors.Companion;
            ChatColors.Id chatColorsId = getChatColorsId();
            ChatColor parseFrom = ChatColor.parseFrom(blob);
            Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(bytes)");
            return companion.forChatColor(chatColorsId, parseFrom);
        } catch (InvalidProtocolBufferException unused) {
            return null;
        }
    }

    public final void setChatColors(ChatColors chatColors) {
        if (chatColors != null) {
            putBlob(KEY_CHAT_COLORS, chatColors.serialize().toByteArray());
            setChatColorsId(chatColors.getId());
            return;
        }
        remove(KEY_CHAT_COLORS);
    }

    private final ChatColors.Id getChatColorsId() {
        return ChatColors.Id.Companion.forLongValue(getLong(KEY_CHAT_COLORS_ID, ChatColors.Id.NotSet.INSTANCE.getLongValue()));
    }

    private final void setChatColorsId(ChatColors.Id id) {
        putLong(KEY_CHAT_COLORS_ID, id.getLongValue());
    }
}
