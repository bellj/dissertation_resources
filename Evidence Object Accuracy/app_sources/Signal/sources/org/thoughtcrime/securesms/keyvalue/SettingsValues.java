package org.thoughtcrime.securesms.keyvalue;

import android.content.Context;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import java.util.Arrays;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.webrtc.CallBandwidthMode;

/* loaded from: classes.dex */
public final class SettingsValues extends SignalStoreValues {
    public static final String BACKUPS_ENABLED;
    private static final String CALL_BANDWIDTH_MODE;
    public static final String CALL_NOTIFICATIONS_ENABLED;
    public static final String CALL_RINGTONE;
    public static final String CALL_VIBRATE_ENABLED;
    private static final String CENSORSHIP_CIRCUMVENTION_ENABLED;
    private static final String DEFAULT_SMS;
    public static final String ENTER_KEY_SENDS;
    public static final String KEEP_MESSAGES_DURATION;
    public static final String LANGUAGE;
    public static final String LINK_PREVIEWS;
    public static final String MESSAGE_FONT_SIZE;
    public static final String MESSAGE_IN_CHAT_SOUNDS_ENABLED;
    public static final String MESSAGE_LED_BLINK_PATTERN;
    public static final String MESSAGE_LED_COLOR;
    public static final String MESSAGE_NOTIFICATIONS_ENABLED;
    public static final String MESSAGE_NOTIFICATION_PRIVACY;
    public static final String MESSAGE_NOTIFICATION_SOUND;
    public static final String MESSAGE_REPEAT_ALERTS;
    public static final String MESSAGE_VIBRATE_ENABLED;
    public static final String NOTIFY_WHEN_CONTACT_JOINS_SIGNAL;
    public static final String PREFER_SYSTEM_CONTACT_PHOTOS;
    public static final String PREFER_SYSTEM_EMOJI;
    private static final String SENT_MEDIA_QUALITY;
    private static final String SIGNAL_BACKUP_DIRECTORY;
    private static final String SIGNAL_LATEST_BACKUP_DIRECTORY;
    public static final String SMS_DELIVERY_REPORTS_ENABLED;
    private static final String TAG = Log.tag(SettingsValues.class);
    public static final String THEME;
    public static final String THREAD_TRIM_ENABLED;
    public static final String THREAD_TRIM_LENGTH;
    private static final String UNIVERSAL_EXPIRE_TIMER;
    public static final String WIFI_CALLING_COMPATIBILITY_MODE_ENABLED;
    private final SingleLiveEvent<String> onConfigurationSettingChanged = new SingleLiveEvent<>();

    public SettingsValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
        if (!getStore().containsKey(LINK_PREVIEWS)) {
            getStore().beginWrite().putBoolean(LINK_PREVIEWS, true).apply();
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Arrays.asList(LINK_PREVIEWS, KEEP_MESSAGES_DURATION, PREFER_SYSTEM_CONTACT_PHOTOS, CALL_BANDWIDTH_MODE, THREAD_TRIM_LENGTH, THREAD_TRIM_ENABLED, LANGUAGE, THEME, MESSAGE_FONT_SIZE, PREFER_SYSTEM_EMOJI, ENTER_KEY_SENDS, BACKUPS_ENABLED, MESSAGE_NOTIFICATIONS_ENABLED, MESSAGE_NOTIFICATION_SOUND, MESSAGE_VIBRATE_ENABLED, MESSAGE_LED_COLOR, MESSAGE_LED_BLINK_PATTERN, MESSAGE_IN_CHAT_SOUNDS_ENABLED, MESSAGE_REPEAT_ALERTS, MESSAGE_NOTIFICATION_PRIVACY, CALL_NOTIFICATIONS_ENABLED, CALL_RINGTONE, CALL_VIBRATE_ENABLED, NOTIFY_WHEN_CONTACT_JOINS_SIGNAL, UNIVERSAL_EXPIRE_TIMER, SENT_MEDIA_QUALITY);
    }

    public LiveData<String> getOnConfigurationSettingChanged() {
        return this.onConfigurationSettingChanged;
    }

    public boolean isLinkPreviewsEnabled() {
        return getBoolean(LINK_PREVIEWS, false);
    }

    public void setLinkPreviewsEnabled(boolean z) {
        putBoolean(LINK_PREVIEWS, z);
    }

    public KeepMessagesDuration getKeepMessagesDuration() {
        return KeepMessagesDuration.fromId(getInteger(KEEP_MESSAGES_DURATION, 0));
    }

    public void setKeepMessagesForDuration(KeepMessagesDuration keepMessagesDuration) {
        putInteger(KEEP_MESSAGES_DURATION, keepMessagesDuration.getId());
    }

    public boolean isTrimByLengthEnabled() {
        return getBoolean(THREAD_TRIM_ENABLED, false);
    }

    public void setThreadTrimByLengthEnabled(boolean z) {
        putBoolean(THREAD_TRIM_ENABLED, z);
    }

    public int getThreadTrimLength() {
        return getInteger(THREAD_TRIM_LENGTH, 500);
    }

    public void setThreadTrimLength(int i) {
        putInteger(THREAD_TRIM_LENGTH, i);
    }

    public void setSignalBackupDirectory(Uri uri) {
        putString(SIGNAL_BACKUP_DIRECTORY, uri.toString());
        putString(SIGNAL_LATEST_BACKUP_DIRECTORY, uri.toString());
    }

    public void setPreferSystemContactPhotos(boolean z) {
        putBoolean(PREFER_SYSTEM_CONTACT_PHOTOS, z);
    }

    public boolean isPreferSystemContactPhotos() {
        return getBoolean(PREFER_SYSTEM_CONTACT_PHOTOS, false);
    }

    public Uri getSignalBackupDirectory() {
        return getUri(SIGNAL_BACKUP_DIRECTORY);
    }

    public Uri getLatestSignalBackupDirectory() {
        return getUri(SIGNAL_LATEST_BACKUP_DIRECTORY);
    }

    public void clearSignalBackupDirectory() {
        putString(SIGNAL_BACKUP_DIRECTORY, null);
    }

    public void setCallBandwidthMode(CallBandwidthMode callBandwidthMode) {
        putInteger(CALL_BANDWIDTH_MODE, callBandwidthMode.getCode());
    }

    public CallBandwidthMode getCallBandwidthMode() {
        return CallBandwidthMode.fromCode(getInteger(CALL_BANDWIDTH_MODE, CallBandwidthMode.HIGH_ALWAYS.getCode()));
    }

    public Theme getTheme() {
        return Theme.deserialize(getString(THEME, TextSecurePreferences.getTheme(ApplicationDependencies.getApplication())));
    }

    public void setTheme(Theme theme) {
        putString(THEME, theme.serialize());
        this.onConfigurationSettingChanged.postValue(THEME);
    }

    public int getMessageFontSize() {
        return getInteger(MESSAGE_FONT_SIZE, TextSecurePreferences.getMessageBodyTextSize(ApplicationDependencies.getApplication()));
    }

    public int getMessageQuoteFontSize(Context context) {
        int messageFontSize = getMessageFontSize();
        int[] intArray = context.getResources().getIntArray(R.array.pref_message_font_size_values);
        int[] intArray2 = context.getResources().getIntArray(R.array.pref_message_font_quote_size_values);
        int binarySearch = Arrays.binarySearch(intArray, messageFontSize);
        if (binarySearch < 0) {
            int i = Integer.MAX_VALUE;
            int i2 = 0;
            for (int i3 = 0; i3 < intArray.length; i3++) {
                int abs = Math.abs(intArray[i3] - messageFontSize);
                if (abs < i) {
                    i2 = i3;
                    i = abs;
                }
            }
            int i4 = intArray[i2];
            String str = TAG;
            Log.w(str, "Using non-standard font size of " + messageFontSize + ". Closest match was " + i4 + ". Updating.");
            setMessageFontSize(i4);
            binarySearch = Arrays.binarySearch(intArray, i4);
        }
        return intArray2[binarySearch];
    }

    public void setMessageFontSize(int i) {
        putInteger(MESSAGE_FONT_SIZE, i);
    }

    public String getLanguage() {
        return TextSecurePreferences.getLanguage(ApplicationDependencies.getApplication());
    }

    public void setLanguage(String str) {
        TextSecurePreferences.setLanguage(ApplicationDependencies.getApplication(), str);
        this.onConfigurationSettingChanged.postValue(LANGUAGE);
    }

    public boolean isPreferSystemEmoji() {
        return getBoolean(PREFER_SYSTEM_EMOJI, TextSecurePreferences.isSystemEmojiPreferred(ApplicationDependencies.getApplication()));
    }

    public void setPreferSystemEmoji(boolean z) {
        putBoolean(PREFER_SYSTEM_EMOJI, z);
    }

    public boolean isEnterKeySends() {
        return getBoolean(ENTER_KEY_SENDS, TextSecurePreferences.isEnterSendsEnabled(ApplicationDependencies.getApplication()));
    }

    public void setEnterKeySends(boolean z) {
        putBoolean(ENTER_KEY_SENDS, z);
    }

    public boolean isBackupEnabled() {
        return getBoolean(BACKUPS_ENABLED, TextSecurePreferences.isBackupEnabled(ApplicationDependencies.getApplication()));
    }

    public void setBackupEnabled(boolean z) {
        putBoolean(BACKUPS_ENABLED, z);
    }

    public boolean isSmsDeliveryReportsEnabled() {
        return getBoolean(SMS_DELIVERY_REPORTS_ENABLED, TextSecurePreferences.isSmsDeliveryReportsEnabled(ApplicationDependencies.getApplication()));
    }

    public void setSmsDeliveryReportsEnabled(boolean z) {
        putBoolean(SMS_DELIVERY_REPORTS_ENABLED, z);
    }

    public boolean isWifiCallingCompatibilityModeEnabled() {
        return getBoolean(WIFI_CALLING_COMPATIBILITY_MODE_ENABLED, TextSecurePreferences.isWifiSmsEnabled(ApplicationDependencies.getApplication()));
    }

    public void setWifiCallingCompatibilityModeEnabled(boolean z) {
        putBoolean(WIFI_CALLING_COMPATIBILITY_MODE_ENABLED, z);
    }

    public void setMessageNotificationsEnabled(boolean z) {
        putBoolean(MESSAGE_NOTIFICATIONS_ENABLED, z);
    }

    public boolean isMessageNotificationsEnabled() {
        return getBoolean(MESSAGE_NOTIFICATIONS_ENABLED, TextSecurePreferences.isNotificationsEnabled(ApplicationDependencies.getApplication()));
    }

    public void setMessageNotificationSound(Uri uri) {
        putString(MESSAGE_NOTIFICATION_SOUND, uri.toString());
    }

    public Uri getMessageNotificationSound() {
        String string = getString(MESSAGE_NOTIFICATION_SOUND, TextSecurePreferences.getNotificationRingtone(ApplicationDependencies.getApplication()).toString());
        if (string.startsWith("file:")) {
            string = Settings.System.DEFAULT_NOTIFICATION_URI.toString();
        }
        return Uri.parse(string);
    }

    public boolean isMessageVibrateEnabled() {
        return getBoolean(MESSAGE_VIBRATE_ENABLED, TextSecurePreferences.isNotificationVibrateEnabled(ApplicationDependencies.getApplication()));
    }

    public void setMessageVibrateEnabled(boolean z) {
        putBoolean(MESSAGE_VIBRATE_ENABLED, z);
    }

    public String getMessageLedColor() {
        return getString(MESSAGE_LED_COLOR, TextSecurePreferences.getNotificationLedColor(ApplicationDependencies.getApplication()));
    }

    public void setMessageLedColor(String str) {
        putString(MESSAGE_LED_COLOR, str);
    }

    public String getMessageLedBlinkPattern() {
        return getString(MESSAGE_LED_BLINK_PATTERN, TextSecurePreferences.getNotificationLedPattern(ApplicationDependencies.getApplication()));
    }

    public void setMessageLedBlinkPattern(String str) {
        putString(MESSAGE_LED_BLINK_PATTERN, str);
    }

    public boolean isMessageNotificationsInChatSoundsEnabled() {
        return getBoolean(MESSAGE_IN_CHAT_SOUNDS_ENABLED, TextSecurePreferences.isInThreadNotifications(ApplicationDependencies.getApplication()));
    }

    public void setMessageNotificationsInChatSoundsEnabled(boolean z) {
        putBoolean(MESSAGE_IN_CHAT_SOUNDS_ENABLED, z);
    }

    public int getMessageNotificationsRepeatAlerts() {
        return getInteger(MESSAGE_REPEAT_ALERTS, TextSecurePreferences.getRepeatAlertsCount(ApplicationDependencies.getApplication()));
    }

    public void setMessageNotificationsRepeatAlerts(int i) {
        putInteger(MESSAGE_REPEAT_ALERTS, i);
    }

    public NotificationPrivacyPreference getMessageNotificationsPrivacy() {
        return new NotificationPrivacyPreference(getString(MESSAGE_NOTIFICATION_PRIVACY, TextSecurePreferences.getNotificationPrivacy(ApplicationDependencies.getApplication()).toString()));
    }

    public void setMessageNotificationsPrivacy(NotificationPrivacyPreference notificationPrivacyPreference) {
        putString(MESSAGE_NOTIFICATION_PRIVACY, notificationPrivacyPreference.toString());
    }

    public boolean isCallNotificationsEnabled() {
        return getBoolean(CALL_NOTIFICATIONS_ENABLED, TextSecurePreferences.isCallNotificationsEnabled(ApplicationDependencies.getApplication()));
    }

    public void setCallNotificationsEnabled(boolean z) {
        putBoolean(CALL_NOTIFICATIONS_ENABLED, z);
    }

    public Uri getCallRingtone() {
        String string = getString(CALL_RINGTONE, TextSecurePreferences.getCallNotificationRingtone(ApplicationDependencies.getApplication()).toString());
        if (string != null && string.startsWith("file:")) {
            string = Settings.System.DEFAULT_RINGTONE_URI.toString();
        }
        return Uri.parse(string);
    }

    public void setCallRingtone(Uri uri) {
        putString(CALL_RINGTONE, uri.toString());
    }

    public boolean isCallVibrateEnabled() {
        return getBoolean(CALL_VIBRATE_ENABLED, TextSecurePreferences.isCallNotificationVibrateEnabled(ApplicationDependencies.getApplication()));
    }

    public void setCallVibrateEnabled(boolean z) {
        putBoolean(CALL_VIBRATE_ENABLED, z);
    }

    public boolean isNotifyWhenContactJoinsSignal() {
        return getBoolean(NOTIFY_WHEN_CONTACT_JOINS_SIGNAL, TextSecurePreferences.isNewContactsNotificationEnabled(ApplicationDependencies.getApplication()));
    }

    public void setNotifyWhenContactJoinsSignal(boolean z) {
        putBoolean(NOTIFY_WHEN_CONTACT_JOINS_SIGNAL, z);
    }

    public void setDefaultSms(boolean z) {
        if (z != getBoolean(DEFAULT_SMS, false) && SignalStore.registrationValues().isRegistrationComplete()) {
            Log.i(TAG, "Default SMS state changed! Scheduling a storage sync.");
            putBoolean(DEFAULT_SMS, z);
            SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.keyvalue.SettingsValues$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    SettingsValues.lambda$setDefaultSms$0();
                }
            });
        }
    }

    public static /* synthetic */ void lambda$setDefaultSms$0() {
        SignalDatabase.recipients().markNeedsSync(Recipient.self().getId());
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    public void setUniversalExpireTimer(int i) {
        putInteger(UNIVERSAL_EXPIRE_TIMER, i);
    }

    public int getUniversalExpireTimer() {
        return getInteger(UNIVERSAL_EXPIRE_TIMER, 0);
    }

    public void setSentMediaQuality(SentMediaQuality sentMediaQuality) {
        putInteger(SENT_MEDIA_QUALITY, sentMediaQuality.getCode());
    }

    public SentMediaQuality getSentMediaQuality() {
        return SentMediaQuality.fromCode(getInteger(SENT_MEDIA_QUALITY, SentMediaQuality.STANDARD.getCode()));
    }

    public CensorshipCircumventionEnabled getCensorshipCircumventionEnabled() {
        return CensorshipCircumventionEnabled.deserialize(getInteger(CENSORSHIP_CIRCUMVENTION_ENABLED, CensorshipCircumventionEnabled.DEFAULT.serialize()));
    }

    public void setCensorshipCircumventionEnabled(boolean z) {
        String str = TAG;
        Log.i(str, "Changing censorship circumvention state to: " + z, new Throwable());
        putInteger(CENSORSHIP_CIRCUMVENTION_ENABLED, (z ? CensorshipCircumventionEnabled.ENABLED : CensorshipCircumventionEnabled.DISABLED).serialize());
    }

    private Uri getUri(String str) {
        String string = getString(str, "");
        if (TextUtils.isEmpty(string)) {
            return null;
        }
        return Uri.parse(string);
    }

    /* loaded from: classes4.dex */
    public enum CensorshipCircumventionEnabled {
        DEFAULT(0),
        ENABLED(1),
        DISABLED(2);
        
        private final int value;

        CensorshipCircumventionEnabled(int i) {
            this.value = i;
        }

        public static CensorshipCircumventionEnabled deserialize(int i) {
            if (i == 0) {
                return DEFAULT;
            }
            if (i == 1) {
                return ENABLED;
            }
            if (i == 2) {
                return DISABLED;
            }
            throw new IllegalArgumentException("Bad value: " + i);
        }

        public int serialize() {
            return this.value;
        }
    }

    /* loaded from: classes4.dex */
    public enum Theme {
        SYSTEM("system"),
        LIGHT("light"),
        DARK("dark");
        
        private final String value;

        Theme(String str) {
            this.value = str;
        }

        public String serialize() {
            return this.value;
        }

        public static Theme deserialize(String str) {
            str.hashCode();
            char c = 65535;
            switch (str.hashCode()) {
                case -887328209:
                    if (str.equals("system")) {
                        c = 0;
                        break;
                    }
                    break;
                case 3075958:
                    if (str.equals("dark")) {
                        c = 1;
                        break;
                    }
                    break;
                case 102970646:
                    if (str.equals("light")) {
                        c = 2;
                        break;
                    }
                    break;
            }
            switch (c) {
                case 0:
                    return SYSTEM;
                case 1:
                    return DARK;
                case 2:
                    return LIGHT;
                default:
                    throw new IllegalArgumentException("Unrecognized value " + str);
            }
        }
    }
}
