package org.thoughtcrime.securesms.keyvalue;

import java.lang.String;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: SignalStoreValueDelegates.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0002\u0018\u0000*\n\b\u0000\u0010\u0001*\u0004\u0018\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0002\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0017\u0010\n\u001a\u00028\u00002\u0006\u0010\u000b\u001a\u00020\u0007H\u0010¢\u0006\u0004\b\f\u0010\rJ\u001f\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00028\u0000H\u0010¢\u0006\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0005\u001a\u00028\u0000X\u0004¢\u0006\u0004\n\u0002\u0010\tR\u000e\u0010\u0004\u001a\u00020\u0002X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StringValue;", "T", "", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "key", "default", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "Ljava/lang/String;", "getValue", "values", "getValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)Ljava/lang/String;", "setValue", "", DraftDatabase.DRAFT_VALUE, "setValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;Ljava/lang/String;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StringValue<T extends String> extends SignalStoreValueDelegate<T> {

    /* renamed from: default */
    private final T f10default;
    private final String key;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StringValue(String str, T t, KeyValueStore keyValueStore) {
        super(keyValueStore, null);
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
        this.key = str;
        this.f10default = t;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.keyvalue.StringValue<T extends java.lang.String> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public /* bridge */ /* synthetic */ void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, Object obj) {
        setValue$Signal_Android_websiteProdRelease(keyValueStore, (KeyValueStore) ((String) obj));
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public T getValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        return (T) keyValueStore.getString(this.key, this.f10default);
    }

    public void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, T t) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        keyValueStore.beginWrite().putString(this.key, t).apply();
    }
}
