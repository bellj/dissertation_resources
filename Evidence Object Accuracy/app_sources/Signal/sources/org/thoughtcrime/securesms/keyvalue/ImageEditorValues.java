package org.thoughtcrime.securesms.keyvalue;

import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ImageEditorValues.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\b\b\u0000\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006J\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\bJ\u0006\u0010\n\u001a\u00020\u0006J\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\bJ\u000e\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rH\u0016J\u0006\u0010\u000f\u001a\u00020\u0006J\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\bJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u000e\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u0006J\u000e\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0016\u001a\u00020\u0006J\u000e\u0010\u0017\u001a\u00020\u00122\u0006\u0010\u0018\u001a\u00020\u0006¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/ImageEditorValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "getBlurPercentage", "", "getBlurWidthRange", "Lkotlin/Pair;", "", "getHighlighterPercentage", "getHighlighterWidthRange", "getKeysToIncludeInBackup", "", "", "getMarkerPercentage", "getMarkerWidthRange", "onFirstEverAppLaunch", "", "setBlurPercentage", "blurPercentage", "setHighlighterPercentage", "highlighterPercentage", "setMarkerPercentage", "markerPercentage", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ImageEditorValues extends SignalStoreValues {
    public static final Companion Companion = new Companion(null);
    private static final String KEY_IMAGE_EDITOR_BLUR_WIDTH;
    private static final String KEY_IMAGE_EDITOR_HIGHLIGHTER_WIDTH;
    private static final String KEY_IMAGE_EDITOR_MARKER_WIDTH;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ImageEditorValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: ImageEditorValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/ImageEditorValues$Companion;", "", "()V", "KEY_IMAGE_EDITOR_BLUR_WIDTH", "", "KEY_IMAGE_EDITOR_HIGHLIGHTER_WIDTH", "KEY_IMAGE_EDITOR_MARKER_WIDTH", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{KEY_IMAGE_EDITOR_MARKER_WIDTH, KEY_IMAGE_EDITOR_HIGHLIGHTER_WIDTH, KEY_IMAGE_EDITOR_BLUR_WIDTH});
    }

    public final void setMarkerPercentage(int i) {
        putInteger(KEY_IMAGE_EDITOR_MARKER_WIDTH, i);
    }

    public final void setHighlighterPercentage(int i) {
        putInteger(KEY_IMAGE_EDITOR_HIGHLIGHTER_WIDTH, i);
    }

    public final void setBlurPercentage(int i) {
        putInteger(KEY_IMAGE_EDITOR_BLUR_WIDTH, i);
    }

    public final int getMarkerPercentage() {
        return getInteger(KEY_IMAGE_EDITOR_MARKER_WIDTH, 0);
    }

    public final int getHighlighterPercentage() {
        return getInteger(KEY_IMAGE_EDITOR_HIGHLIGHTER_WIDTH, 0);
    }

    public final int getBlurPercentage() {
        return getInteger(KEY_IMAGE_EDITOR_BLUR_WIDTH, 0);
    }

    public final Pair<Float, Float> getMarkerWidthRange() {
        return new Pair<>(Float.valueOf(0.01f), Float.valueOf(0.05f));
    }

    public final Pair<Float, Float> getHighlighterWidthRange() {
        return new Pair<>(Float.valueOf(0.03f), Float.valueOf(0.08f));
    }

    public final Pair<Float, Float> getBlurWidthRange() {
        return new Pair<>(Float.valueOf(0.052f), Float.valueOf(0.092f));
    }
}
