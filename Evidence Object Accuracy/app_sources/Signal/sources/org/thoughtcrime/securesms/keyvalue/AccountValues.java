package org.thoughtcrime.securesms.keyvalue;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.security.SecureRandom;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import org.signal.core.util.logging.Log;
import org.signal.libsignal.protocol.IdentityKey;
import org.signal.libsignal.protocol.IdentityKeyPair;
import org.signal.libsignal.protocol.ecc.Curve;
import org.signal.libsignal.protocol.util.Medium;
import org.signal.libsignal.zkgroup.profiles.ProfileKey;
import org.thoughtcrime.securesms.crypto.IdentityKeyUtil;
import org.thoughtcrime.securesms.crypto.MasterCipher;
import org.thoughtcrime.securesms.crypto.MasterSecretUtil;
import org.thoughtcrime.securesms.crypto.ProfileKeyUtil;
import org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.KeyValueStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.api.push.ServiceIds;

/* compiled from: AccountValues.kt */
@Metadata(d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0014\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0000\u0018\u0000 _2\u00020\u0001:\u0001_B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010?\u001a\u00020@2\u0006\u0010A\u001a\u00020BH\u0002J\u0006\u0010C\u001a\u00020@J\u0006\u0010D\u001a\u00020@J\u000e\u0010E\u001a\b\u0012\u0004\u0012\u00020\u001a0FH\u0016J\u0006\u0010G\u001a\u00020HJ\u0006\u0010I\u001a\u00020\u001fJ\u0006\u0010J\u001a\u00020\u001fJ\u0010\u0010K\u001a\u00020@2\u0006\u0010A\u001a\u00020BH\u0002J\u0010\u0010L\u001a\u00020@2\u0006\u0010A\u001a\u00020BH\u0002J\b\u0010M\u001a\u00020@H\u0016J\u0006\u0010N\u001a\u00020\u0006J\u0006\u0010O\u001a\u000203J\u000e\u0010P\u001a\u00020@2\u0006\u0010Q\u001a\u00020\u001aJ\u000e\u0010R\u001a\u00020@2\u0006\u0010Q\u001a\u00020\u001aJ\u000e\u0010S\u001a\u00020@2\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010T\u001a\u00020@2\u0006\u0010\u0019\u001a\u00020\u001aJ\u000e\u0010U\u001a\u00020@2\u0006\u0010\u001d\u001a\u00020\u001aJ\u000e\u0010V\u001a\u00020@2\u0006\u0010W\u001a\u00020\nJ\u000e\u0010X\u001a\u00020@2\u0006\u00102\u001a\u000203J\u000e\u0010Y\u001a\u00020@2\u0006\u0010Z\u001a\u00020\u001fJ\u000e\u0010[\u001a\u00020@2\u0006\u0010=\u001a\u00020\u001aJ\u0014\u0010\\\u001a\u00020\u001f*\u00020]2\u0006\u0010^\u001a\u00020\u001aH\u0002R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u00068F¢\u0006\u0006\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\r\u001a\u00020\u000e8G¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000fR+\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u00118F@FX\u0002¢\u0006\u0012\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u001a8F¢\u0006\u0006\u001a\u0004\b\u001b\u0010\u001cR\u0013\u0010\u001d\u001a\u0004\u0018\u00010\u001a8F¢\u0006\u0006\u001a\u0004\b\u001e\u0010\u001cR+\u0010 \u001a\u00020\u001f2\u0006\u0010\u0010\u001a\u00020\u001f8G@FX\u0002¢\u0006\u0012\n\u0004\b%\u0010\u0018\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R(\u0010'\u001a\u0004\u0018\u00010\u001a2\b\u0010&\u001a\u0004\u0018\u00010\u001a8F@FX\u000e¢\u0006\f\u001a\u0004\b(\u0010\u001c\"\u0004\b)\u0010*R\u0011\u0010+\u001a\u00020,8F¢\u0006\u0006\u001a\u0004\b-\u0010.R\u0011\u0010/\u001a\u00020\u001f8F¢\u0006\u0006\u001a\u0004\b/\u0010\"R\u0011\u00100\u001a\u00020\u001f8F¢\u0006\u0006\u001a\u0004\b0\u0010\"R\u0011\u00101\u001a\u00020\u001f8F¢\u0006\u0006\u001a\u0004\b1\u0010\"R\u0013\u00102\u001a\u0004\u0018\u0001038F¢\u0006\u0006\u001a\u0004\b4\u00105R\u0011\u00106\u001a\u00020\n8F¢\u0006\u0006\u001a\u0004\b7\u0010\fR\u0013\u00108\u001a\u00020\u000e8G¢\u0006\b\n\u0000\u001a\u0004\b8\u0010\u000fR+\u00109\u001a\u00020\u00112\u0006\u0010\u0010\u001a\u00020\u00118F@FX\u0002¢\u0006\u0012\n\u0004\b<\u0010\u0018\u001a\u0004\b:\u0010\u0014\"\u0004\b;\u0010\u0016R\u0013\u0010=\u001a\u0004\u0018\u00010\u001a8F¢\u0006\u0006\u001a\u0004\b>\u0010\u001c¨\u0006`"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/AccountValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "aci", "Lorg/whispersystems/signalservice/api/push/ACI;", "getAci", "()Lorg/whispersystems/signalservice/api/push/ACI;", "aciIdentityKey", "Lorg/signal/libsignal/protocol/IdentityKeyPair;", "getAciIdentityKey", "()Lorg/signal/libsignal/protocol/IdentityKeyPair;", "aciPreKeys", "Lorg/thoughtcrime/securesms/crypto/storage/PreKeyMetadataStore;", "()Lorg/thoughtcrime/securesms/crypto/storage/PreKeyMetadataStore;", "<set-?>", "", "deviceId", "getDeviceId", "()I", "setDeviceId", "(I)V", "deviceId$delegate", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "deviceName", "", "getDeviceName", "()Ljava/lang/String;", CdsDatabase.E164, "getE164", "", "fcmEnabled", "isFcmEnabled", "()Z", "setFcmEnabled", "(Z)V", "fcmEnabled$delegate", DraftDatabase.DRAFT_VALUE, "fcmToken", "getFcmToken", "setFcmToken", "(Ljava/lang/String;)V", "fcmTokenLastSetTime", "", "getFcmTokenLastSetTime", "()J", "isLinkedDevice", "isPrimaryDevice", "isRegistered", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "getPni", "()Lorg/whispersystems/signalservice/api/push/PNI;", "pniIdentityKey", "getPniIdentityKey", "pniPreKeys", "registrationId", "getRegistrationId", "setRegistrationId", "registrationId$delegate", "servicePassword", "getServicePassword", "clearLocalCredentials", "", "context", "Landroid/content/Context;", "generateAciIdentityKeyIfNecessary", "generatePniIdentityKeyIfNecessary", "getKeysToIncludeInBackup", "", "getServiceIds", "Lorg/whispersystems/signalservice/api/push/ServiceIds;", "hasAciIdentityKey", "hasPniIdentityKey", "migrateFromSharedPrefsV1", "migrateFromSharedPrefsV2", "onFirstEverAppLaunch", "requireAci", "requirePni", "restoreLegacyIdentityPrivateKeyFromBackup", "base64", "restoreLegacyIdentityPublicKeyFromBackup", "setAci", "setDeviceName", "setE164", "setIdentityKeysFromPrimaryDevice", "aciKeys", "setPni", "setRegistered", RecipientDatabase.REGISTERED, "setServicePassword", "hasStringData", "Landroid/content/SharedPreferences;", "key", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes.dex */
public final class AccountValues extends SignalStoreValues {
    static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues.class, "registrationId", "getRegistrationId()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues.class, "fcmEnabled", "isFcmEnabled()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues.class, "deviceId", "getDeviceId()I", 0))};
    public static final Companion Companion = new Companion(null);
    public static final String KEY_ACI;
    private static final String KEY_ACI_ACTIVE_SIGNED_PREKEY_ID;
    private static final String KEY_ACI_IDENTITY_PRIVATE_KEY;
    private static final String KEY_ACI_IDENTITY_PUBLIC_KEY;
    private static final String KEY_ACI_NEXT_ONE_TIME_PREKEY_ID;
    private static final String KEY_ACI_NEXT_SIGNED_PREKEY_ID;
    private static final String KEY_ACI_SIGNED_PREKEY_FAILURE_COUNT;
    private static final String KEY_ACI_SIGNED_PREKEY_REGISTERED;
    private static final String KEY_DEVICE_ID;
    private static final String KEY_DEVICE_NAME;
    public static final String KEY_E164;
    private static final String KEY_FCM_ENABLED;
    private static final String KEY_FCM_TOKEN;
    private static final String KEY_FCM_TOKEN_LAST_SET_TIME;
    private static final String KEY_FCM_TOKEN_VERSION;
    public static final String KEY_IS_REGISTERED;
    public static final String KEY_PNI;
    private static final String KEY_PNI_ACTIVE_SIGNED_PREKEY_ID;
    private static final String KEY_PNI_IDENTITY_PRIVATE_KEY;
    private static final String KEY_PNI_IDENTITY_PUBLIC_KEY;
    private static final String KEY_PNI_NEXT_ONE_TIME_PREKEY_ID;
    private static final String KEY_PNI_NEXT_SIGNED_PREKEY_ID;
    private static final String KEY_PNI_SIGNED_PREKEY_FAILURE_COUNT;
    private static final String KEY_PNI_SIGNED_PREKEY_REGISTERED;
    private static final String KEY_REGISTRATION_ID;
    private static final String KEY_SERVICE_PASSWORD;
    private static final String TAG = Log.tag(AccountValues.class);
    private final PreKeyMetadataStore aciPreKeys;
    private final SignalStoreValueDelegate deviceId$delegate;
    private final SignalStoreValueDelegate fcmEnabled$delegate;
    private final PreKeyMetadataStore pniPreKeys;
    private final SignalStoreValueDelegate registrationId$delegate;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AccountValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
        if (!keyValueStore.containsKey(KEY_ACI)) {
            Application application = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
            migrateFromSharedPrefsV1(application);
        }
        if (!keyValueStore.containsKey(KEY_ACI_IDENTITY_PUBLIC_KEY)) {
            Application application2 = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application2, "getApplication()");
            migrateFromSharedPrefsV2(application2);
        }
        this.registrationId$delegate = SignalStoreValueDelegatesKt.integerValue(this, KEY_REGISTRATION_ID, 0);
        this.aciPreKeys = new PreKeyMetadataStore(this) { // from class: org.thoughtcrime.securesms.keyvalue.AccountValues$aciPreKeys$1
            static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$aciPreKeys$1.class, "nextSignedPreKeyId", "getNextSignedPreKeyId()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$aciPreKeys$1.class, "activeSignedPreKeyId", "getActiveSignedPreKeyId()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$aciPreKeys$1.class, "isSignedPreKeyRegistered", "isSignedPreKeyRegistered()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$aciPreKeys$1.class, "signedPreKeyFailureCount", "getSignedPreKeyFailureCount()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$aciPreKeys$1.class, "nextOneTimePreKeyId", "getNextOneTimePreKeyId()I", 0))};
            private final SignalStoreValueDelegate activeSignedPreKeyId$delegate;
            private final SignalStoreValueDelegate isSignedPreKeyRegistered$delegate;
            private final SignalStoreValueDelegate nextOneTimePreKeyId$delegate;
            private final SignalStoreValueDelegate nextSignedPreKeyId$delegate;
            private final SignalStoreValueDelegate signedPreKeyFailureCount$delegate;

            /* access modifiers changed from: package-private */
            {
                this.nextSignedPreKeyId$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.aci_next_signed_prekey_id", new SecureRandom().nextInt(Medium.MAX_VALUE));
                this.activeSignedPreKeyId$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.aci_active_signed_prekey_id", -1);
                this.isSignedPreKeyRegistered$delegate = SignalStoreValueDelegatesKt.booleanValue(r3, "account.aci_signed_prekey_registered", false);
                this.signedPreKeyFailureCount$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.aci_signed_prekey_failure_count", 0);
                this.nextOneTimePreKeyId$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.aci_next_one_time_prekey_id", new SecureRandom().nextInt(Medium.MAX_VALUE));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getNextSignedPreKeyId() {
                return ((Number) this.nextSignedPreKeyId$delegate.getValue(this, $$delegatedProperties[0])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setNextSignedPreKeyId(int i) {
                this.nextSignedPreKeyId$delegate.setValue(this, $$delegatedProperties[0], Integer.valueOf(i));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getActiveSignedPreKeyId() {
                return ((Number) this.activeSignedPreKeyId$delegate.getValue(this, $$delegatedProperties[1])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setActiveSignedPreKeyId(int i) {
                this.activeSignedPreKeyId$delegate.setValue(this, $$delegatedProperties[1], Integer.valueOf(i));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public boolean isSignedPreKeyRegistered() {
                return ((Boolean) this.isSignedPreKeyRegistered$delegate.getValue(this, $$delegatedProperties[2])).booleanValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setSignedPreKeyRegistered(boolean z) {
                this.isSignedPreKeyRegistered$delegate.setValue(this, $$delegatedProperties[2], Boolean.valueOf(z));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getSignedPreKeyFailureCount() {
                return ((Number) this.signedPreKeyFailureCount$delegate.getValue(this, $$delegatedProperties[3])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setSignedPreKeyFailureCount(int i) {
                this.signedPreKeyFailureCount$delegate.setValue(this, $$delegatedProperties[3], Integer.valueOf(i));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getNextOneTimePreKeyId() {
                return ((Number) this.nextOneTimePreKeyId$delegate.getValue(this, $$delegatedProperties[4])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setNextOneTimePreKeyId(int i) {
                this.nextOneTimePreKeyId$delegate.setValue(this, $$delegatedProperties[4], Integer.valueOf(i));
            }
        };
        this.pniPreKeys = new PreKeyMetadataStore(this) { // from class: org.thoughtcrime.securesms.keyvalue.AccountValues$pniPreKeys$1
            static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$pniPreKeys$1.class, "nextSignedPreKeyId", "getNextSignedPreKeyId()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$pniPreKeys$1.class, "activeSignedPreKeyId", "getActiveSignedPreKeyId()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$pniPreKeys$1.class, "isSignedPreKeyRegistered", "isSignedPreKeyRegistered()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$pniPreKeys$1.class, "signedPreKeyFailureCount", "getSignedPreKeyFailureCount()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(AccountValues$pniPreKeys$1.class, "nextOneTimePreKeyId", "getNextOneTimePreKeyId()I", 0))};
            private final SignalStoreValueDelegate activeSignedPreKeyId$delegate;
            private final SignalStoreValueDelegate isSignedPreKeyRegistered$delegate;
            private final SignalStoreValueDelegate nextOneTimePreKeyId$delegate;
            private final SignalStoreValueDelegate nextSignedPreKeyId$delegate;
            private final SignalStoreValueDelegate signedPreKeyFailureCount$delegate;

            /* access modifiers changed from: package-private */
            {
                this.nextSignedPreKeyId$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.pni_next_signed_prekey_id", new SecureRandom().nextInt(Medium.MAX_VALUE));
                this.activeSignedPreKeyId$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.pni_active_signed_prekey_id", -1);
                this.isSignedPreKeyRegistered$delegate = SignalStoreValueDelegatesKt.booleanValue(r3, "account.pni_signed_prekey_registered", false);
                this.signedPreKeyFailureCount$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.pni_signed_prekey_failure_count", 0);
                this.nextOneTimePreKeyId$delegate = SignalStoreValueDelegatesKt.integerValue(r3, "account.pni_next_one_time_prekey_id", new SecureRandom().nextInt(Medium.MAX_VALUE));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getNextSignedPreKeyId() {
                return ((Number) this.nextSignedPreKeyId$delegate.getValue(this, $$delegatedProperties[0])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setNextSignedPreKeyId(int i) {
                this.nextSignedPreKeyId$delegate.setValue(this, $$delegatedProperties[0], Integer.valueOf(i));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getActiveSignedPreKeyId() {
                return ((Number) this.activeSignedPreKeyId$delegate.getValue(this, $$delegatedProperties[1])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setActiveSignedPreKeyId(int i) {
                this.activeSignedPreKeyId$delegate.setValue(this, $$delegatedProperties[1], Integer.valueOf(i));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public boolean isSignedPreKeyRegistered() {
                return ((Boolean) this.isSignedPreKeyRegistered$delegate.getValue(this, $$delegatedProperties[2])).booleanValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setSignedPreKeyRegistered(boolean z) {
                this.isSignedPreKeyRegistered$delegate.setValue(this, $$delegatedProperties[2], Boolean.valueOf(z));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getSignedPreKeyFailureCount() {
                return ((Number) this.signedPreKeyFailureCount$delegate.getValue(this, $$delegatedProperties[3])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setSignedPreKeyFailureCount(int i) {
                this.signedPreKeyFailureCount$delegate.setValue(this, $$delegatedProperties[3], Integer.valueOf(i));
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public int getNextOneTimePreKeyId() {
                return ((Number) this.nextOneTimePreKeyId$delegate.getValue(this, $$delegatedProperties[4])).intValue();
            }

            @Override // org.thoughtcrime.securesms.crypto.storage.PreKeyMetadataStore
            public void setNextOneTimePreKeyId(int i) {
                this.nextOneTimePreKeyId$delegate.setValue(this, $$delegatedProperties[4], Integer.valueOf(i));
            }
        };
        this.fcmEnabled$delegate = SignalStoreValueDelegatesKt.booleanValue(this, KEY_FCM_ENABLED, false);
        this.deviceId$delegate = SignalStoreValueDelegatesKt.integerValue(this, KEY_DEVICE_ID, 1);
    }

    /* compiled from: AccountValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b \b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0002R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\u0010\u0010\u0002R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\u0016\u0010\u0002R\u0016\u0010\u0017\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\u0018\u0010\u0002R\u000e\u0010\u0019\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\"\u001a\n #*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/AccountValues$Companion;", "", "()V", "KEY_ACI", "", "getKEY_ACI$annotations", "KEY_ACI_ACTIVE_SIGNED_PREKEY_ID", "KEY_ACI_IDENTITY_PRIVATE_KEY", "KEY_ACI_IDENTITY_PUBLIC_KEY", "KEY_ACI_NEXT_ONE_TIME_PREKEY_ID", "KEY_ACI_NEXT_SIGNED_PREKEY_ID", "KEY_ACI_SIGNED_PREKEY_FAILURE_COUNT", "KEY_ACI_SIGNED_PREKEY_REGISTERED", "KEY_DEVICE_ID", "KEY_DEVICE_NAME", "KEY_E164", "getKEY_E164$annotations", "KEY_FCM_ENABLED", "KEY_FCM_TOKEN", "KEY_FCM_TOKEN_LAST_SET_TIME", "KEY_FCM_TOKEN_VERSION", "KEY_IS_REGISTERED", "getKEY_IS_REGISTERED$annotations", "KEY_PNI", "getKEY_PNI$annotations", "KEY_PNI_ACTIVE_SIGNED_PREKEY_ID", "KEY_PNI_IDENTITY_PRIVATE_KEY", "KEY_PNI_IDENTITY_PUBLIC_KEY", "KEY_PNI_NEXT_ONE_TIME_PREKEY_ID", "KEY_PNI_NEXT_SIGNED_PREKEY_ID", "KEY_PNI_SIGNED_PREKEY_FAILURE_COUNT", "KEY_PNI_SIGNED_PREKEY_REGISTERED", "KEY_REGISTRATION_ID", "KEY_SERVICE_PASSWORD", "TAG", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ void getKEY_ACI$annotations() {
        }

        public static /* synthetic */ void getKEY_E164$annotations() {
        }

        public static /* synthetic */ void getKEY_IS_REGISTERED$annotations() {
        }

        public static /* synthetic */ void getKEY_PNI$annotations() {
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{KEY_ACI_IDENTITY_PUBLIC_KEY, KEY_ACI_IDENTITY_PRIVATE_KEY, KEY_PNI_IDENTITY_PUBLIC_KEY, KEY_PNI_IDENTITY_PRIVATE_KEY});
    }

    public final ACI getAci() {
        return ACI.parseOrNull(getString(KEY_ACI, null));
    }

    public final ACI requireAci() {
        ACI parseOrThrow = ACI.parseOrThrow(getString(KEY_ACI, null));
        Intrinsics.checkNotNullExpressionValue(parseOrThrow, "parseOrThrow(getString(KEY_ACI, null))");
        return parseOrThrow;
    }

    public final void setAci(ACI aci) {
        Intrinsics.checkNotNullParameter(aci, "aci");
        putString(KEY_ACI, aci.toString());
    }

    public final PNI getPni() {
        return PNI.parseOrNull(getString(KEY_PNI, null));
    }

    public final PNI requirePni() {
        PNI parseOrThrow = PNI.parseOrThrow(getString(KEY_PNI, null));
        Intrinsics.checkNotNullExpressionValue(parseOrThrow, "parseOrThrow(getString(KEY_PNI, null))");
        return parseOrThrow;
    }

    public final void setPni(PNI pni) {
        Intrinsics.checkNotNullParameter(pni, RecipientDatabase.PNI_COLUMN);
        putString(KEY_PNI, pni.toString());
    }

    public final ServiceIds getServiceIds() {
        return new ServiceIds(requireAci(), getPni());
    }

    public final String getE164() {
        return getString(KEY_E164, null);
    }

    public final void setE164(String str) {
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        putString(KEY_E164, str);
    }

    public final String getServicePassword() {
        return getString(KEY_SERVICE_PASSWORD, null);
    }

    public final void setServicePassword(String str) {
        Intrinsics.checkNotNullParameter(str, "servicePassword");
        putString(KEY_SERVICE_PASSWORD, str);
    }

    public final int getRegistrationId() {
        return ((Number) this.registrationId$delegate.getValue(this, $$delegatedProperties[0])).intValue();
    }

    public final void setRegistrationId(int i) {
        this.registrationId$delegate.setValue(this, $$delegatedProperties[0], Integer.valueOf(i));
    }

    public final IdentityKeyPair getAciIdentityKey() {
        if (getStore().containsKey(KEY_ACI_IDENTITY_PUBLIC_KEY)) {
            return new IdentityKeyPair(new IdentityKey(getBlob(KEY_ACI_IDENTITY_PUBLIC_KEY, null)), Curve.decodePrivatePoint(getBlob(KEY_ACI_IDENTITY_PRIVATE_KEY, null)));
        }
        throw new IllegalArgumentException("Not yet set!".toString());
    }

    public final IdentityKeyPair getPniIdentityKey() {
        if (getStore().containsKey(KEY_PNI_IDENTITY_PUBLIC_KEY)) {
            return new IdentityKeyPair(new IdentityKey(getBlob(KEY_PNI_IDENTITY_PUBLIC_KEY, null)), Curve.decodePrivatePoint(getBlob(KEY_PNI_IDENTITY_PRIVATE_KEY, null)));
        }
        throw new IllegalArgumentException("Not yet set!".toString());
    }

    public final boolean hasAciIdentityKey() {
        return getStore().containsKey(KEY_ACI_IDENTITY_PUBLIC_KEY);
    }

    public final void generateAciIdentityKeyIfNecessary() {
        synchronized (this) {
            if (getStore().containsKey(KEY_ACI_IDENTITY_PUBLIC_KEY)) {
                Log.w(TAG, "Tried to generate an ANI identity, but one was already set!", new Throwable());
                return;
            }
            Log.i(TAG, "Generating a new ACI identity key pair.");
            IdentityKeyPair generateIdentityKeyPair = IdentityKeyUtil.generateIdentityKeyPair();
            Intrinsics.checkNotNullExpressionValue(generateIdentityKeyPair, "generateIdentityKeyPair()");
            getStore().beginWrite().putBlob(KEY_ACI_IDENTITY_PUBLIC_KEY, generateIdentityKeyPair.getPublicKey().serialize()).putBlob(KEY_ACI_IDENTITY_PRIVATE_KEY, generateIdentityKeyPair.getPrivateKey().serialize()).commit();
            Unit unit = Unit.INSTANCE;
        }
    }

    public final boolean hasPniIdentityKey() {
        return getStore().containsKey(KEY_PNI_IDENTITY_PUBLIC_KEY);
    }

    public final void generatePniIdentityKeyIfNecessary() {
        synchronized (this) {
            if (getStore().containsKey(KEY_PNI_IDENTITY_PUBLIC_KEY)) {
                Log.w(TAG, "Tried to generate a PNI identity, but one was already set!", new Throwable());
                return;
            }
            Log.i(TAG, "Generating a new PNI identity key pair.");
            IdentityKeyPair generateIdentityKeyPair = IdentityKeyUtil.generateIdentityKeyPair();
            Intrinsics.checkNotNullExpressionValue(generateIdentityKeyPair, "generateIdentityKeyPair()");
            getStore().beginWrite().putBlob(KEY_PNI_IDENTITY_PUBLIC_KEY, generateIdentityKeyPair.getPublicKey().serialize()).putBlob(KEY_PNI_IDENTITY_PRIVATE_KEY, generateIdentityKeyPair.getPrivateKey().serialize()).commit();
            Unit unit = Unit.INSTANCE;
        }
    }

    public final void setIdentityKeysFromPrimaryDevice(IdentityKeyPair identityKeyPair) {
        Intrinsics.checkNotNullParameter(identityKeyPair, "aciKeys");
        synchronized (this) {
            if (isLinkedDevice()) {
                getStore().beginWrite().putBlob(KEY_ACI_IDENTITY_PUBLIC_KEY, identityKeyPair.getPublicKey().serialize()).putBlob(KEY_ACI_IDENTITY_PRIVATE_KEY, identityKeyPair.getPrivateKey().serialize()).commit();
                Unit unit = Unit.INSTANCE;
            } else {
                throw new IllegalArgumentException("Must be a linked device!".toString());
            }
        }
    }

    public final void restoreLegacyIdentityPublicKeyFromBackup(String str) {
        Intrinsics.checkNotNullParameter(str, "base64");
        Log.w(TAG, "Restoring legacy identity public key from backup.");
        putBlob(KEY_ACI_IDENTITY_PUBLIC_KEY, Base64.decode(str));
    }

    public final void restoreLegacyIdentityPrivateKeyFromBackup(String str) {
        Intrinsics.checkNotNullParameter(str, "base64");
        Log.w(TAG, "Restoring legacy identity private key from backup.");
        putBlob(KEY_ACI_IDENTITY_PRIVATE_KEY, Base64.decode(str));
    }

    public final PreKeyMetadataStore aciPreKeys() {
        return this.aciPreKeys;
    }

    public final PreKeyMetadataStore pniPreKeys() {
        return this.pniPreKeys;
    }

    public final boolean isFcmEnabled() {
        return ((Boolean) this.fcmEnabled$delegate.getValue(this, $$delegatedProperties[1])).booleanValue();
    }

    public final void setFcmEnabled(boolean z) {
        this.fcmEnabled$delegate.setValue(this, $$delegatedProperties[1], Boolean.valueOf(z));
    }

    public final String getFcmToken() {
        if (getInteger(KEY_FCM_TOKEN_VERSION, 0) == Util.getCanonicalVersionCode()) {
            return getString(KEY_FCM_TOKEN, null);
        }
        return null;
    }

    public final void setFcmToken(String str) {
        getStore().beginWrite().putString(KEY_FCM_TOKEN, str).putInteger(KEY_FCM_TOKEN_VERSION, Util.getCanonicalVersionCode()).putLong(KEY_FCM_TOKEN_LAST_SET_TIME, System.currentTimeMillis()).apply();
    }

    public final long getFcmTokenLastSetTime() {
        return getLong(KEY_FCM_TOKEN_LAST_SET_TIME, 0);
    }

    public final boolean isRegistered() {
        return getBoolean(KEY_IS_REGISTERED, false);
    }

    public final void setRegistered(boolean z) {
        String str = TAG;
        Log.i(str, "Setting push registered: " + z, new Throwable());
        boolean isRegistered = isRegistered();
        putBoolean(KEY_IS_REGISTERED, z);
        ApplicationDependencies.getIncomingMessageObserver().notifyRegistrationChanged();
        if (isRegistered != z) {
            Recipient.self().live().refresh();
        }
        if (isRegistered && !z) {
            Application application = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
            clearLocalCredentials(application);
        }
    }

    public final String getDeviceName() {
        return getString(KEY_DEVICE_NAME, null);
    }

    public final void setDeviceName(String str) {
        Intrinsics.checkNotNullParameter(str, "deviceName");
        putString(KEY_DEVICE_NAME, str);
    }

    public final int getDeviceId() {
        return ((Number) this.deviceId$delegate.getValue(this, $$delegatedProperties[2])).intValue();
    }

    public final void setDeviceId(int i) {
        this.deviceId$delegate.setValue(this, $$delegatedProperties[2], Integer.valueOf(i));
    }

    public final boolean isPrimaryDevice() {
        return getDeviceId() == 1;
    }

    public final boolean isLinkedDevice() {
        return !isPrimaryDevice();
    }

    private final void clearLocalCredentials(Context context) {
        putString(KEY_SERVICE_PASSWORD, Util.getSecret(18));
        ProfileKey createNew = ProfileKeyUtil.createNew();
        Intrinsics.checkNotNullExpressionValue(createNew, "createNew()");
        Recipient self = Recipient.self();
        Intrinsics.checkNotNullExpressionValue(self, "self()");
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = self.getId();
        Intrinsics.checkNotNullExpressionValue(id, "self.id");
        recipients.setProfileKey(id, createNew);
        ApplicationDependencies.getGroupsV2Authorization().clear();
    }

    private final void migrateFromSharedPrefsV1(Context context) {
        Log.i(TAG, "[V1] Migrating account values from shared prefs.");
        putString(KEY_ACI, TextSecurePreferences.getStringPreference(context, "pref_local_uuid", null));
        putString(KEY_E164, TextSecurePreferences.getStringPreference(context, "pref_local_number", null));
        putString(KEY_SERVICE_PASSWORD, TextSecurePreferences.getStringPreference(context, "pref_gcm_password", null));
        putBoolean(KEY_IS_REGISTERED, TextSecurePreferences.getBooleanPreference(context, "pref_gcm_registered", false));
        putInteger(KEY_REGISTRATION_ID, TextSecurePreferences.getIntegerPreference(context, "pref_local_registration_id", 0));
        putBoolean(KEY_FCM_ENABLED, !TextSecurePreferences.getBooleanPreference(context, "pref_gcm_disabled", false));
        putString(KEY_FCM_TOKEN, TextSecurePreferences.getStringPreference(context, "pref_gcm_registration_id", null));
        putInteger(KEY_FCM_TOKEN_VERSION, TextSecurePreferences.getIntegerPreference(context, "pref_gcm_registration_id_version", 0));
        putLong(KEY_FCM_TOKEN_LAST_SET_TIME, TextSecurePreferences.getLongPreference(context, "pref_gcm_registration_id_last_set_time", 0));
    }

    private final void migrateFromSharedPrefsV2(Context context) {
        String str = TAG;
        Log.i(str, "[V2] Migrating account values from shared prefs.");
        SharedPreferences sharedPreferences = context.getSharedPreferences(MasterSecretUtil.PREFERENCES_NAME, 0);
        Intrinsics.checkNotNullExpressionValue(sharedPreferences, "context.getSharedPrefere…ecureSMS-Preferences\", 0)");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Intrinsics.checkNotNullExpressionValue(defaultSharedPreferences, "getDefaultSharedPreferences(context)");
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        Intrinsics.checkNotNullExpressionValue(beginWrite, "store.beginWrite()");
        if (hasStringData(sharedPreferences, "pref_identity_public_v3")) {
            Log.i(str, "Migrating modern identity key.");
            String string = sharedPreferences.getString("pref_identity_public_v3", null);
            Intrinsics.checkNotNull(string);
            byte[] decode = Base64.decode(string);
            Intrinsics.checkNotNullExpressionValue(decode, "decode(masterSecretPrefs…tity_public_v3\", null)!!)");
            String string2 = sharedPreferences.getString("pref_identity_private_v3", null);
            Intrinsics.checkNotNull(string2);
            byte[] decode2 = Base64.decode(string2);
            Intrinsics.checkNotNullExpressionValue(decode2, "decode(masterSecretPrefs…ity_private_v3\", null)!!)");
            beginWrite.putBlob(KEY_ACI_IDENTITY_PUBLIC_KEY, decode).putBlob(KEY_ACI_IDENTITY_PRIVATE_KEY, decode2);
        } else if (hasStringData(sharedPreferences, "pref_identity_public_curve25519")) {
            Log.i(str, "Migrating legacy identity key.");
            MasterCipher masterCipher = new MasterCipher(KeyCachingService.getMasterSecret(context));
            String string3 = sharedPreferences.getString("pref_identity_public_curve25519", null);
            Intrinsics.checkNotNull(string3);
            byte[] decode3 = Base64.decode(string3);
            Intrinsics.checkNotNullExpressionValue(decode3, "decode(masterSecretPrefs…lic_curve25519\", null)!!)");
            String string4 = sharedPreferences.getString("pref_identity_private_curve25519", null);
            Intrinsics.checkNotNull(string4);
            beginWrite.putBlob(KEY_ACI_IDENTITY_PUBLIC_KEY, decode3).putBlob(KEY_ACI_IDENTITY_PRIVATE_KEY, masterCipher.decryptKey(Base64.decode(string4)).serialize());
        } else {
            Log.w(str, "No pre-existing identity key! No migration.");
        }
        beginWrite.putInteger(KEY_ACI_NEXT_SIGNED_PREKEY_ID, defaultSharedPreferences.getInt("pref_next_signed_pre_key_id", new SecureRandom().nextInt(Medium.MAX_VALUE))).putInteger(KEY_ACI_ACTIVE_SIGNED_PREKEY_ID, defaultSharedPreferences.getInt("pref_active_signed_pre_key_id", -1)).putInteger(KEY_ACI_NEXT_ONE_TIME_PREKEY_ID, defaultSharedPreferences.getInt("pref_next_pre_key_id", new SecureRandom().nextInt(Medium.MAX_VALUE))).putInteger(KEY_ACI_SIGNED_PREKEY_FAILURE_COUNT, defaultSharedPreferences.getInt("pref_signed_prekey_failure_count", 0)).putBoolean(KEY_ACI_SIGNED_PREKEY_REGISTERED, defaultSharedPreferences.getBoolean("pref_signed_prekey_registered", false)).commit();
        sharedPreferences.edit().remove("pref_identity_public_v3").remove("pref_identity_private_v3").remove("pref_identity_public_curve25519").remove("pref_identity_private_curve25519").commit();
        defaultSharedPreferences.edit().remove("pref_local_uuid").remove("pref_identity_public_v3").remove("pref_next_signed_pre_key_id").remove("pref_active_signed_pre_key_id").remove("pref_signed_prekey_failure_count").remove("pref_signed_prekey_registered").remove("pref_next_pre_key_id").remove("pref_gcm_password").remove("pref_gcm_registered").remove("pref_local_registration_id").remove("pref_gcm_disabled").remove("pref_gcm_registration_id").remove("pref_gcm_registration_id_version").remove("pref_gcm_registration_id_last_set_time").commit();
    }

    private final boolean hasStringData(SharedPreferences sharedPreferences, String str) {
        return sharedPreferences.getString(str, null) != null;
    }
}
