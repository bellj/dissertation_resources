package org.thoughtcrime.securesms.keyvalue;

import java.io.IOException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.lock.PinHashing;
import org.thoughtcrime.securesms.util.JsonUtils;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.kbs.MasterKey;
import org.whispersystems.signalservice.internal.contacts.entities.TokenResponse;

/* loaded from: classes4.dex */
public final class KbsValues extends SignalStoreValues {
    private static final String LAST_CREATE_FAILED_TIMESTAMP;
    private static final String LOCK_LOCAL_PIN_HASH;
    private static final String MASTER_KEY;
    public static final String OPTED_OUT;
    private static final String PIN;
    private static final String PIN_FORGOTTEN_OR_SKIPPED;
    private static final String TOKEN_RESPONSE;
    public static final String V2_LOCK_ENABLED;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public KbsValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public void clearRegistrationLockAndPin() {
        getStore().beginWrite().remove(V2_LOCK_ENABLED).remove(TOKEN_RESPONSE).remove(LOCK_LOCAL_PIN_HASH).remove(PIN).remove(LAST_CREATE_FAILED_TIMESTAMP).remove(OPTED_OUT).commit();
    }

    public synchronized void setKbsMasterKey(KbsPinData kbsPinData, String str) {
        MasterKey masterKey = kbsPinData.getMasterKey();
        try {
            getStore().beginWrite().putString(TOKEN_RESPONSE, JsonUtils.toJson(kbsPinData.getTokenResponse())).putBlob(MASTER_KEY, masterKey.serialize()).putString(LOCK_LOCAL_PIN_HASH, PinHashing.localPinHash(str)).putString(PIN, str).putLong(LAST_CREATE_FAILED_TIMESTAMP, -1).putBoolean(OPTED_OUT, false).commit();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    public synchronized void setPinIfNotPresent(String str) {
        if (getStore().getString(PIN, null) == null) {
            getStore().beginWrite().putString(PIN, str).commit();
        }
    }

    public synchronized void setV2RegistrationLockEnabled(boolean z) {
        putBoolean(V2_LOCK_ENABLED, z);
    }

    public synchronized boolean isV2RegistrationLockEnabled() {
        return getBoolean(V2_LOCK_ENABLED, false);
    }

    public synchronized void onPinCreateFailure() {
        putLong(LAST_CREATE_FAILED_TIMESTAMP, System.currentTimeMillis());
    }

    public synchronized boolean lastPinCreateFailed() {
        return getLong(LAST_CREATE_FAILED_TIMESTAMP, -1) > 0;
    }

    public synchronized MasterKey getOrCreateMasterKey() {
        byte[] blob;
        blob = getStore().getBlob(MASTER_KEY, null);
        if (blob == null) {
            getStore().beginWrite().putBlob(MASTER_KEY, MasterKey.createNew(new SecureRandom()).serialize()).commit();
            blob = getBlob(MASTER_KEY, null);
        }
        return new MasterKey(blob);
    }

    public synchronized MasterKey getPinBackedMasterKey() {
        if (!isV2RegistrationLockEnabled()) {
            return null;
        }
        return getMasterKey();
    }

    private synchronized MasterKey getMasterKey() {
        MasterKey masterKey;
        masterKey = null;
        byte[] blob = getBlob(MASTER_KEY, null);
        if (blob != null) {
            masterKey = new MasterKey(blob);
        }
        return masterKey;
    }

    public String getRegistrationLockToken() {
        MasterKey pinBackedMasterKey = getPinBackedMasterKey();
        if (pinBackedMasterKey == null) {
            return null;
        }
        return pinBackedMasterKey.deriveRegistrationLock();
    }

    public synchronized String getPin() {
        return getString(PIN, null);
    }

    public synchronized String getLocalPinHash() {
        return getString(LOCK_LOCAL_PIN_HASH, null);
    }

    public synchronized boolean hasPin() {
        return getLocalPinHash() != null;
    }

    public synchronized boolean isPinForgottenOrSkipped() {
        return getBoolean(PIN_FORGOTTEN_OR_SKIPPED, false);
    }

    public synchronized void setPinForgottenOrSkipped(boolean z) {
        putBoolean(PIN_FORGOTTEN_OR_SKIPPED, z);
    }

    public synchronized void optOut() {
        getStore().beginWrite().putBoolean(OPTED_OUT, true).remove(TOKEN_RESPONSE).putBlob(MASTER_KEY, MasterKey.createNew(new SecureRandom()).serialize()).remove(LOCK_LOCAL_PIN_HASH).remove(PIN).putLong(LAST_CREATE_FAILED_TIMESTAMP, -1).commit();
    }

    public synchronized boolean hasOptedOut() {
        return getBoolean(OPTED_OUT, false);
    }

    public synchronized TokenResponse getRegistrationLockTokenResponse() {
        String string = getStore().getString(TOKEN_RESPONSE, null);
        if (string == null) {
            return null;
        }
        try {
            return (TokenResponse) JsonUtils.fromJson(string, TokenResponse.class);
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
