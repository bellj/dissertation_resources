package org.thoughtcrime.securesms.keyvalue;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ReleaseChannelValues.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u0012\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0000\u0018\u0000 .2\u00020\u0001:\u0001.B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010'\u001a\b\u0012\u0004\u0012\u00020)0(H\u0014J\b\u0010*\u001a\u00020+H\u0014J\u000e\u0010,\u001a\u00020+2\u0006\u0010-\u001a\u00020$R+\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR+\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u000e8F@FX\u0002¢\u0006\u0012\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R+\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0005\u001a\u00020\u00158F@FX\u0002¢\u0006\u0012\n\u0004\b\u001b\u0010\r\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR+\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u0005\u001a\u00020\u001c8F@FX\u0002¢\u0006\u0012\n\u0004\b\"\u0010\r\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R\u0013\u0010#\u001a\u0004\u0018\u00010$8F¢\u0006\u0006\u001a\u0004\b%\u0010&¨\u0006/"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/ReleaseChannelValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "<set-?>", "", "hasMetConversationRequirement", "getHasMetConversationRequirement", "()Z", "setHasMetConversationRequirement", "(Z)V", "hasMetConversationRequirement$delegate", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "", "highestVersionNoteReceived", "getHighestVersionNoteReceived", "()I", "setHighestVersionNoteReceived", "(I)V", "highestVersionNoteReceived$delegate", "", "nextScheduledCheck", "getNextScheduledCheck", "()J", "setNextScheduledCheck", "(J)V", "nextScheduledCheck$delegate", "", "previousManifestMd5", "getPreviousManifestMd5", "()[B", "setPreviousManifestMd5", "([B)V", "previousManifestMd5$delegate", "releaseChannelRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getReleaseChannelRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getKeysToIncludeInBackup", "", "", "onFirstEverAppLaunch", "", "setReleaseChannelRecipientId", ContactRepository.ID_COLUMN, "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ReleaseChannelValues extends SignalStoreValues {
    static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(ReleaseChannelValues.class, "nextScheduledCheck", "getNextScheduledCheck()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(ReleaseChannelValues.class, "previousManifestMd5", "getPreviousManifestMd5()[B", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(ReleaseChannelValues.class, "highestVersionNoteReceived", "getHighestVersionNoteReceived()I", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(ReleaseChannelValues.class, "hasMetConversationRequirement", "getHasMetConversationRequirement()Z", 0))};
    public static final Companion Companion = new Companion(null);
    private static final String KEY_HIGHEST_VERSION_NOTE_RECEIVED;
    private static final String KEY_MET_CONVERSATION_REQUIREMENT;
    private static final String KEY_NEXT_SCHEDULED_CHECK;
    private static final String KEY_PREVIOUS_MANIFEST_MD5;
    private static final String KEY_RELEASE_CHANNEL_RECIPIENT_ID;
    private final SignalStoreValueDelegate hasMetConversationRequirement$delegate = SignalStoreValueDelegatesKt.booleanValue(this, KEY_MET_CONVERSATION_REQUIREMENT, false);
    private final SignalStoreValueDelegate highestVersionNoteReceived$delegate = SignalStoreValueDelegatesKt.integerValue(this, KEY_HIGHEST_VERSION_NOTE_RECEIVED, 0);
    private final SignalStoreValueDelegate nextScheduledCheck$delegate = SignalStoreValueDelegatesKt.longValue(this, KEY_NEXT_SCHEDULED_CHECK, 0);
    private final SignalStoreValueDelegate previousManifestMd5$delegate = SignalStoreValueDelegatesKt.blobValue(this, KEY_PREVIOUS_MANIFEST_MD5, new byte[0]);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ReleaseChannelValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: ReleaseChannelValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/ReleaseChannelValues$Companion;", "", "()V", "KEY_HIGHEST_VERSION_NOTE_RECEIVED", "", "KEY_MET_CONVERSATION_REQUIREMENT", "KEY_NEXT_SCHEDULED_CHECK", "KEY_PREVIOUS_MANIFEST_MD5", "KEY_RELEASE_CHANNEL_RECIPIENT_ID", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsJVMKt.listOf(KEY_RELEASE_CHANNEL_RECIPIENT_ID);
    }

    public final RecipientId getReleaseChannelRecipientId() {
        String string = getString(KEY_RELEASE_CHANNEL_RECIPIENT_ID, "");
        Intrinsics.checkNotNullExpressionValue(string, ContactRepository.ID_COLUMN);
        if (string.length() == 0) {
            return null;
        }
        return RecipientId.from(string);
    }

    public final void setReleaseChannelRecipientId(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        putString(KEY_RELEASE_CHANNEL_RECIPIENT_ID, recipientId.serialize());
    }

    public final long getNextScheduledCheck() {
        return ((Number) this.nextScheduledCheck$delegate.getValue(this, $$delegatedProperties[0])).longValue();
    }

    public final void setNextScheduledCheck(long j) {
        this.nextScheduledCheck$delegate.setValue(this, $$delegatedProperties[0], Long.valueOf(j));
    }

    public final byte[] getPreviousManifestMd5() {
        return (byte[]) this.previousManifestMd5$delegate.getValue(this, $$delegatedProperties[1]);
    }

    public final void setPreviousManifestMd5(byte[] bArr) {
        Intrinsics.checkNotNullParameter(bArr, "<set-?>");
        this.previousManifestMd5$delegate.setValue(this, $$delegatedProperties[1], bArr);
    }

    public final int getHighestVersionNoteReceived() {
        return ((Number) this.highestVersionNoteReceived$delegate.getValue(this, $$delegatedProperties[2])).intValue();
    }

    public final void setHighestVersionNoteReceived(int i) {
        this.highestVersionNoteReceived$delegate.setValue(this, $$delegatedProperties[2], Integer.valueOf(i));
    }

    public final boolean getHasMetConversationRequirement() {
        return ((Boolean) this.hasMetConversationRequirement$delegate.getValue(this, $$delegatedProperties[3])).booleanValue();
    }

    public final void setHasMetConversationRequirement(boolean z) {
        this.hasMetConversationRequirement$delegate.setValue(this, $$delegatedProperties[3], Boolean.valueOf(z));
    }
}
