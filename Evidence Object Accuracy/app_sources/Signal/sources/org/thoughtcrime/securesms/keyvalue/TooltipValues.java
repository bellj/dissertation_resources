package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public class TooltipValues extends SignalStoreValues {
    private static final String BLUR_HUD_ICON;
    private static final String BUBBLE_OPT_OUT;
    private static final int GROUP_CALLING_MAX_TOOLTIP_DISPLAY_COUNT;
    private static final String GROUP_CALL_SPEAKER_VIEW;
    private static final String GROUP_CALL_TOOLTIP_DISPLAY_COUNT;
    private static final String MULTI_FORWARD_DIALOG;

    public TooltipValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
        markMultiForwardDialogSeen();
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public boolean hasSeenBlurHudIconTooltip() {
        return getBoolean(BLUR_HUD_ICON, false);
    }

    public void markBlurHudIconTooltipSeen() {
        putBoolean(BLUR_HUD_ICON, true);
    }

    public boolean hasSeenGroupCallSpeakerView() {
        return getBoolean(GROUP_CALL_SPEAKER_VIEW, false);
    }

    public void markGroupCallSpeakerViewSeen() {
        putBoolean(GROUP_CALL_SPEAKER_VIEW, true);
    }

    public boolean shouldShowGroupCallingTooltip() {
        return getInteger(GROUP_CALL_TOOLTIP_DISPLAY_COUNT, 0) < 3;
    }

    public void markGroupCallingTooltipSeen() {
        putInteger(GROUP_CALL_TOOLTIP_DISPLAY_COUNT, getInteger(GROUP_CALL_TOOLTIP_DISPLAY_COUNT, 0) + 1);
    }

    public void markGroupCallingLobbyEntered() {
        putInteger(GROUP_CALL_TOOLTIP_DISPLAY_COUNT, Integer.MAX_VALUE);
    }

    public boolean showMultiForwardDialog() {
        return getBoolean(MULTI_FORWARD_DIALOG, true);
    }

    public void markMultiForwardDialogSeen() {
        putBoolean(MULTI_FORWARD_DIALOG, false);
    }

    public boolean hasSeenBubbleOptOutTooltip() {
        return getBoolean(BUBBLE_OPT_OUT, false);
    }

    public void markBubbleOptOutTooltipSeen() {
        putBoolean(BUBBLE_OPT_OUT, true);
    }
}
