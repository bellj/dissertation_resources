package org.thoughtcrime.securesms.keyvalue;

import androidx.preference.PreferenceDataStore;

/* loaded from: classes4.dex */
public class SignalPreferenceDataStore extends PreferenceDataStore {
    private final KeyValueStore store;

    public SignalPreferenceDataStore(KeyValueStore keyValueStore) {
        this.store = keyValueStore;
    }

    @Override // androidx.preference.PreferenceDataStore
    public void putString(String str, String str2) {
        this.store.beginWrite().putString(str, str2).apply();
    }

    @Override // androidx.preference.PreferenceDataStore
    public void putInt(String str, int i) {
        this.store.beginWrite().putInteger(str, i).apply();
    }

    @Override // androidx.preference.PreferenceDataStore
    public void putLong(String str, long j) {
        this.store.beginWrite().putLong(str, j).apply();
    }

    @Override // androidx.preference.PreferenceDataStore
    public void putFloat(String str, float f) {
        this.store.beginWrite().putFloat(str, f).apply();
    }

    @Override // androidx.preference.PreferenceDataStore
    public void putBoolean(String str, boolean z) {
        this.store.beginWrite().putBoolean(str, z).apply();
    }

    @Override // androidx.preference.PreferenceDataStore
    public String getString(String str, String str2) {
        return this.store.getString(str, str2);
    }

    @Override // androidx.preference.PreferenceDataStore
    public int getInt(String str, int i) {
        return this.store.getInteger(str, i);
    }

    @Override // androidx.preference.PreferenceDataStore
    public long getLong(String str, long j) {
        return this.store.getLong(str, j);
    }

    @Override // androidx.preference.PreferenceDataStore
    public float getFloat(String str, float f) {
        return this.store.getFloat(str, f);
    }

    @Override // androidx.preference.PreferenceDataStore
    public boolean getBoolean(String str, boolean z) {
        return this.store.getBoolean(str, z);
    }
}
