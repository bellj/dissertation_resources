package org.thoughtcrime.securesms.keyvalue;

import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;

/* JADX WARN: Init of enum ONE_YEAR can be incorrect */
/* JADX WARN: Init of enum SIX_MONTHS can be incorrect */
/* JADX WARN: Init of enum THIRTY_DAYS can be incorrect */
/* loaded from: classes4.dex */
public enum KeepMessagesDuration {
    FOREVER(0, R.string.preferences_storage__forever, Long.MAX_VALUE),
    ONE_YEAR(1, R.string.preferences_storage__one_year, r1.toMillis(365)),
    SIX_MONTHS(2, R.string.preferences_storage__six_months, r1.toMillis(183)),
    THIRTY_DAYS(3, R.string.preferences_storage__thirty_days, r1.toMillis(30));
    
    private final long duration;
    private final int id;
    private final int stringResource;

    static {
        TimeUnit timeUnit = TimeUnit.DAYS;
    }

    KeepMessagesDuration(int i, int i2, long j) {
        this.id = i;
        this.stringResource = i2;
        this.duration = j;
    }

    public int getId() {
        return this.id;
    }

    public int getStringResource() {
        return this.stringResource;
    }

    public long getDuration() {
        return this.duration;
    }

    public static KeepMessagesDuration fromId(int i) {
        return values()[i];
    }
}
