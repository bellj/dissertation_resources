package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.lock.SignalPinReminders;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public final class PinValues extends SignalStoreValues {
    private static final String KEYBOARD_TYPE;
    private static final String LAST_SUCCESSFUL_ENTRY;
    private static final String NEXT_INTERVAL;
    public static final String PIN_REMINDERS_ENABLED;
    private static final String PIN_STATE;
    private static final String TAG = Log.tag(PinValues.class);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public PinValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.singletonList(PIN_REMINDERS_ENABLED);
    }

    public void onEntrySuccess(String str) {
        long nextInterval = SignalPinReminders.getNextInterval(getCurrentInterval());
        String str2 = TAG;
        Log.i(str2, "onEntrySuccess() nextInterval: " + nextInterval);
        getStore().beginWrite().putLong(LAST_SUCCESSFUL_ENTRY, System.currentTimeMillis()).putLong(NEXT_INTERVAL, nextInterval).apply();
        SignalStore.kbsValues().setPinIfNotPresent(str);
    }

    public void onEntrySuccessWithWrongGuess(String str) {
        long previousInterval = SignalPinReminders.getPreviousInterval(getCurrentInterval());
        String str2 = TAG;
        Log.i(str2, "onEntrySuccessWithWrongGuess() nextInterval: " + previousInterval);
        getStore().beginWrite().putLong(LAST_SUCCESSFUL_ENTRY, System.currentTimeMillis()).putLong(NEXT_INTERVAL, previousInterval).apply();
        SignalStore.kbsValues().setPinIfNotPresent(str);
    }

    public void onEntrySkipWithWrongGuess() {
        long previousInterval = SignalPinReminders.getPreviousInterval(getCurrentInterval());
        String str = TAG;
        Log.i(str, "onEntrySkipWithWrongGuess() nextInterval: " + previousInterval);
        putLong(NEXT_INTERVAL, previousInterval);
    }

    public void resetPinReminders() {
        long j = SignalPinReminders.INITIAL_INTERVAL;
        String str = TAG;
        Log.i(str, "resetPinReminders() nextInterval: " + j, new Throwable());
        getStore().beginWrite().putLong(NEXT_INTERVAL, j).putLong(LAST_SUCCESSFUL_ENTRY, System.currentTimeMillis()).apply();
    }

    public long getCurrentInterval() {
        return getLong(NEXT_INTERVAL, TextSecurePreferences.getRegistrationLockNextReminderInterval(ApplicationDependencies.getApplication()));
    }

    public long getLastSuccessfulEntryTime() {
        return getLong(LAST_SUCCESSFUL_ENTRY, TextSecurePreferences.getRegistrationLockLastReminderTime(ApplicationDependencies.getApplication()));
    }

    public void setKeyboardType(PinKeyboardType pinKeyboardType) {
        putString(KEYBOARD_TYPE, pinKeyboardType.getCode());
    }

    public void setPinRemindersEnabled(boolean z) {
        putBoolean(PIN_REMINDERS_ENABLED, z);
    }

    public boolean arePinRemindersEnabled() {
        return getBoolean(PIN_REMINDERS_ENABLED, true);
    }

    public PinKeyboardType getKeyboardType() {
        return PinKeyboardType.fromCode(getStore().getString(KEYBOARD_TYPE, null));
    }

    public void setNextReminderIntervalToAtMost(long j) {
        if (getStore().getLong(NEXT_INTERVAL, 0) > j) {
            putLong(NEXT_INTERVAL, j);
        }
    }

    public void setPinState(String str) {
        getStore().beginWrite().putString(PIN_STATE, str).commit();
    }

    public String getPinState() {
        return getString(PIN_STATE, null);
    }
}
