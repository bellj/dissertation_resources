package org.thoughtcrime.securesms.keyvalue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import org.json.JSONObject;
import org.signal.core.util.StringSerializer;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.StorySend;

/* compiled from: StoryValues.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\t\n\u0002\b\u0013\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0000\u0018\u0000 /2\u00020\u0001:\u0002/0B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010$\u001a\b\u0012\u0004\u0012\u00020&0%H\u0014J\u0014\u0010'\u001a\b\u0012\u0004\u0012\u00020)0(2\u0006\u0010*\u001a\u00020\u0011J\b\u0010+\u001a\u00020,H\u0014J\u000e\u0010-\u001a\u00020,2\u0006\u0010.\u001a\u00020)R+\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR+\u0010\u000e\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b\u0010\u0010\r\u001a\u0004\b\u000e\u0010\t\"\u0004\b\u000f\u0010\u000bR+\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0005\u001a\u00020\u00118F@FX\u0002¢\u0006\u0012\n\u0004\b\u0017\u0010\r\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R+\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b\u001b\u0010\r\u001a\u0004\b\u0019\u0010\t\"\u0004\b\u001a\u0010\u000bR+\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b\u001f\u0010\r\u001a\u0004\b\u001d\u0010\t\"\u0004\b\u001e\u0010\u000bR+\u0010 \u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b#\u0010\r\u001a\u0004\b!\u0010\t\"\u0004\b\"\u0010\u000b¨\u00061"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StoryValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "<set-?>", "", "hasDownloadedOnboardingStory", "getHasDownloadedOnboardingStory", "()Z", "setHasDownloadedOnboardingStory", "(Z)V", "hasDownloadedOnboardingStory$delegate", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "isFeatureDisabled", "setFeatureDisabled", "isFeatureDisabled$delegate", "", "lastFontVersionCheck", "getLastFontVersionCheck", "()J", "setLastFontVersionCheck", "(J)V", "lastFontVersionCheck$delegate", "userHasBeenNotifiedAboutStories", "getUserHasBeenNotifiedAboutStories", "setUserHasBeenNotifiedAboutStories", "userHasBeenNotifiedAboutStories$delegate", "userHasSeenFirstNavView", "getUserHasSeenFirstNavView", "setUserHasSeenFirstNavView", "userHasSeenFirstNavView$delegate", "userHasSeenOnboardingStory", "getUserHasSeenOnboardingStory", "setUserHasSeenOnboardingStory", "userHasSeenOnboardingStory$delegate", "getKeysToIncludeInBackup", "", "", "getLatestActiveStorySendTimestamps", "", "Lorg/thoughtcrime/securesms/keyvalue/StorySend;", "activeCutoffTimestamp", "onFirstEverAppLaunch", "", "setLatestStorySend", "storySend", "Companion", "StorySendSerializer", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StoryValues extends SignalStoreValues {
    static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoryValues.class, "isFeatureDisabled", "isFeatureDisabled()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoryValues.class, "lastFontVersionCheck", "getLastFontVersionCheck()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoryValues.class, "userHasBeenNotifiedAboutStories", "getUserHasBeenNotifiedAboutStories()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoryValues.class, "userHasSeenFirstNavView", "getUserHasSeenFirstNavView()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoryValues.class, "hasDownloadedOnboardingStory", "getHasDownloadedOnboardingStory()Z", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(StoryValues.class, "userHasSeenOnboardingStory", "getUserHasSeenOnboardingStory()Z", 0))};
    public static final Companion Companion = new Companion(null);
    private static final String HAS_DOWNLOADED_ONBOARDING_STORY;
    private static final String LAST_FONT_VERSION_CHECK;
    private static final String LATEST_STORY_SENDS;
    private static final String MANUAL_FEATURE_DISABLE;
    private static final String USER_HAS_ADDED_TO_A_STORY;
    private static final String USER_HAS_SEEN_FIRST_NAV_VIEW;
    private static final String USER_HAS_SEEN_ONBOARDING_STORY;
    private final SignalStoreValueDelegate hasDownloadedOnboardingStory$delegate = SignalStoreValueDelegatesKt.booleanValue(this, HAS_DOWNLOADED_ONBOARDING_STORY, false);
    private final SignalStoreValueDelegate isFeatureDisabled$delegate = SignalStoreValueDelegatesKt.booleanValue(this, MANUAL_FEATURE_DISABLE, false);
    private final SignalStoreValueDelegate lastFontVersionCheck$delegate = SignalStoreValueDelegatesKt.longValue(this, LAST_FONT_VERSION_CHECK, 0);
    private final SignalStoreValueDelegate userHasBeenNotifiedAboutStories$delegate = SignalStoreValueDelegatesKt.booleanValue(this, USER_HAS_ADDED_TO_A_STORY, false);
    private final SignalStoreValueDelegate userHasSeenFirstNavView$delegate = SignalStoreValueDelegatesKt.booleanValue(this, USER_HAS_SEEN_FIRST_NAV_VIEW, false);
    private final SignalStoreValueDelegate userHasSeenOnboardingStory$delegate = SignalStoreValueDelegatesKt.booleanValue(this, USER_HAS_SEEN_ONBOARDING_STORY, false);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public StoryValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: StoryValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StoryValues$Companion;", "", "()V", "HAS_DOWNLOADED_ONBOARDING_STORY", "", "LAST_FONT_VERSION_CHECK", "LATEST_STORY_SENDS", "MANUAL_FEATURE_DISABLE", "USER_HAS_ADDED_TO_A_STORY", "USER_HAS_SEEN_FIRST_NAV_VIEW", "USER_HAS_SEEN_ONBOARDING_STORY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.mutableListOf(MANUAL_FEATURE_DISABLE, USER_HAS_ADDED_TO_A_STORY, USER_HAS_SEEN_FIRST_NAV_VIEW, HAS_DOWNLOADED_ONBOARDING_STORY);
    }

    public final boolean isFeatureDisabled() {
        return ((Boolean) this.isFeatureDisabled$delegate.getValue(this, $$delegatedProperties[0])).booleanValue();
    }

    public final void setFeatureDisabled(boolean z) {
        this.isFeatureDisabled$delegate.setValue(this, $$delegatedProperties[0], Boolean.valueOf(z));
    }

    public final long getLastFontVersionCheck() {
        return ((Number) this.lastFontVersionCheck$delegate.getValue(this, $$delegatedProperties[1])).longValue();
    }

    public final void setLastFontVersionCheck(long j) {
        this.lastFontVersionCheck$delegate.setValue(this, $$delegatedProperties[1], Long.valueOf(j));
    }

    public final boolean getUserHasBeenNotifiedAboutStories() {
        return ((Boolean) this.userHasBeenNotifiedAboutStories$delegate.getValue(this, $$delegatedProperties[2])).booleanValue();
    }

    public final void setUserHasBeenNotifiedAboutStories(boolean z) {
        this.userHasBeenNotifiedAboutStories$delegate.setValue(this, $$delegatedProperties[2], Boolean.valueOf(z));
    }

    public final boolean getUserHasSeenFirstNavView() {
        return ((Boolean) this.userHasSeenFirstNavView$delegate.getValue(this, $$delegatedProperties[3])).booleanValue();
    }

    public final void setUserHasSeenFirstNavView(boolean z) {
        this.userHasSeenFirstNavView$delegate.setValue(this, $$delegatedProperties[3], Boolean.valueOf(z));
    }

    public final boolean getHasDownloadedOnboardingStory() {
        return ((Boolean) this.hasDownloadedOnboardingStory$delegate.getValue(this, $$delegatedProperties[4])).booleanValue();
    }

    public final void setHasDownloadedOnboardingStory(boolean z) {
        this.hasDownloadedOnboardingStory$delegate.setValue(this, $$delegatedProperties[4], Boolean.valueOf(z));
    }

    public final boolean getUserHasSeenOnboardingStory() {
        return ((Boolean) this.userHasSeenOnboardingStory$delegate.getValue(this, $$delegatedProperties[5])).booleanValue();
    }

    public final void setUserHasSeenOnboardingStory(boolean z) {
        this.userHasSeenOnboardingStory$delegate.setValue(this, $$delegatedProperties[5], Boolean.valueOf(z));
    }

    public final void setLatestStorySend(StorySend storySend) {
        Intrinsics.checkNotNullParameter(storySend, "storySend");
        synchronized (this) {
            StorySendSerializer storySendSerializer = StorySendSerializer.INSTANCE;
            List list = getList(LATEST_STORY_SENDS, storySendSerializer);
            Intrinsics.checkNotNullExpressionValue(list, "getList(LATEST_STORY_SENDS, StorySendSerializer)");
            putList(LATEST_STORY_SENDS, CollectionsKt___CollectionsKt.plus((Collection) CollectionsKt__CollectionsJVMKt.listOf(storySend), (Iterable) CollectionsKt___CollectionsKt.take(list, 1)), storySendSerializer);
            Unit unit = Unit.INSTANCE;
        }
    }

    public final List<StorySend> getLatestActiveStorySendTimestamps(long j) {
        List list = getList(LATEST_STORY_SENDS, StorySendSerializer.INSTANCE);
        Intrinsics.checkNotNullExpressionValue(list, "getList(LATEST_STORY_SENDS, StorySendSerializer)");
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (((StorySend) obj).getTimestamp() >= j) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* compiled from: StoryValues.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0002\bÂ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0002H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StoryValues$StorySendSerializer;", "Lorg/signal/core/util/StringSerializer;", "Lorg/thoughtcrime/securesms/keyvalue/StorySend;", "()V", "deserialize", "data", "", "serialize", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class StorySendSerializer implements StringSerializer<StorySend> {
        public static final StorySendSerializer INSTANCE = new StorySendSerializer();

        private StorySendSerializer() {
        }

        public String serialize(StorySend storySend) {
            Intrinsics.checkNotNullParameter(storySend, "data");
            String str = null;
            JSONObject put = new JSONObject().put("timestamp", storySend.getTimestamp()).put("groupId", storySend.getIdentifier() instanceof StorySend.Identifier.Group ? ((StorySend.Identifier.Group) storySend.getIdentifier()).getGroupId().toString() : null);
            if (storySend.getIdentifier() instanceof StorySend.Identifier.DistributionList) {
                str = ((StorySend.Identifier.DistributionList) storySend.getIdentifier()).getDistributionListId().serialize();
            }
            String jSONObject = put.put("distributionListId", str).toString();
            Intrinsics.checkNotNullExpressionValue(jSONObject, "JSONObject()\n        .pu…null)\n        .toString()");
            return jSONObject;
        }

        public StorySend deserialize(String str) {
            StorySend.Identifier identifier;
            Intrinsics.checkNotNullParameter(str, "data");
            JSONObject jSONObject = new JSONObject(str);
            long j = jSONObject.getLong("timestamp");
            if (jSONObject.has("groupId")) {
                GroupId parse = GroupId.parse(jSONObject.getString("groupId"));
                Intrinsics.checkNotNullExpressionValue(parse, "parse(group)");
                identifier = new StorySend.Identifier.Group(parse);
            } else {
                DistributionListId from = DistributionListId.from(jSONObject.getString("distributionListId"));
                Intrinsics.checkNotNullExpressionValue(from, "from(distributionListId)");
                identifier = new StorySend.Identifier.DistributionList(from);
            }
            return new StorySend(j, identifier);
        }
    }
}
