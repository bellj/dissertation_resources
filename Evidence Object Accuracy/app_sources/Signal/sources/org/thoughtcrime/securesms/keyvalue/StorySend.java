package org.thoughtcrime.securesms.keyvalue;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListId;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: StorySend.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u0000 \u00152\u00020\u0001:\u0002\u0015\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StorySend;", "", "timestamp", "", "identifier", "Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier;", "(JLorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier;)V", "getIdentifier", "()Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier;", "getTimestamp", "()J", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "Identifier", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class StorySend {
    public static final Companion Companion = new Companion(null);
    private final Identifier identifier;
    private final long timestamp;

    public static /* synthetic */ StorySend copy$default(StorySend storySend, long j, Identifier identifier, int i, Object obj) {
        if ((i & 1) != 0) {
            j = storySend.timestamp;
        }
        if ((i & 2) != 0) {
            identifier = storySend.identifier;
        }
        return storySend.copy(j, identifier);
    }

    @JvmStatic
    public static final StorySend newSend(Recipient recipient) {
        return Companion.newSend(recipient);
    }

    public final long component1() {
        return this.timestamp;
    }

    public final Identifier component2() {
        return this.identifier;
    }

    public final StorySend copy(long j, Identifier identifier) {
        Intrinsics.checkNotNullParameter(identifier, "identifier");
        return new StorySend(j, identifier);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StorySend)) {
            return false;
        }
        StorySend storySend = (StorySend) obj;
        return this.timestamp == storySend.timestamp && Intrinsics.areEqual(this.identifier, storySend.identifier);
    }

    public int hashCode() {
        return (SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.timestamp) * 31) + this.identifier.hashCode();
    }

    public String toString() {
        return "StorySend(timestamp=" + this.timestamp + ", identifier=" + this.identifier + ')';
    }

    public StorySend(long j, Identifier identifier) {
        Intrinsics.checkNotNullParameter(identifier, "identifier");
        this.timestamp = j;
        this.identifier = identifier;
    }

    public final long getTimestamp() {
        return this.timestamp;
    }

    public final Identifier getIdentifier() {
        return this.identifier;
    }

    /* compiled from: StorySend.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StorySend$Companion;", "", "()V", "newSend", "Lorg/thoughtcrime/securesms/keyvalue/StorySend;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final StorySend newSend(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            if (recipient.isGroup()) {
                long currentTimeMillis = System.currentTimeMillis();
                GroupId requireGroupId = recipient.requireGroupId();
                Intrinsics.checkNotNullExpressionValue(requireGroupId, "recipient.requireGroupId()");
                return new StorySend(currentTimeMillis, new Identifier.Group(requireGroupId));
            }
            long currentTimeMillis2 = System.currentTimeMillis();
            DistributionListId requireDistributionListId = recipient.requireDistributionListId();
            Intrinsics.checkNotNullExpressionValue(requireDistributionListId, "recipient.requireDistributionListId()");
            return new StorySend(currentTimeMillis2, new Identifier.DistributionList(requireDistributionListId));
        }
    }

    /* compiled from: StorySend.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier;", "", "()V", "matches", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "DistributionList", "Group", "Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier$Group;", "Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier$DistributionList;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Identifier {
        public /* synthetic */ Identifier(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public abstract boolean matches(Recipient recipient);

        /* compiled from: StorySend.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier$Group;", "Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "(Lorg/thoughtcrime/securesms/groups/GroupId;)V", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "matches", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Group extends Identifier {
            private final GroupId groupId;

            public static /* synthetic */ Group copy$default(Group group, GroupId groupId, int i, Object obj) {
                if ((i & 1) != 0) {
                    groupId = group.groupId;
                }
                return group.copy(groupId);
            }

            public final GroupId component1() {
                return this.groupId;
            }

            public final Group copy(GroupId groupId) {
                Intrinsics.checkNotNullParameter(groupId, "groupId");
                return new Group(groupId);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof Group) && Intrinsics.areEqual(this.groupId, ((Group) obj).groupId);
            }

            public int hashCode() {
                return this.groupId.hashCode();
            }

            public String toString() {
                return "Group(groupId=" + this.groupId + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Group(GroupId groupId) {
                super(null);
                Intrinsics.checkNotNullParameter(groupId, "groupId");
                this.groupId = groupId;
            }

            public final GroupId getGroupId() {
                return this.groupId;
            }

            @Override // org.thoughtcrime.securesms.keyvalue.StorySend.Identifier
            public boolean matches(Recipient recipient) {
                Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
                return Intrinsics.areEqual(recipient.getGroupId().orElse(null), this.groupId);
            }
        }

        private Identifier() {
        }

        /* compiled from: StorySend.kt */
        @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\u0010\u0010\u000f\u001a\u00020\n2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier$DistributionList;", "Lorg/thoughtcrime/securesms/keyvalue/StorySend$Identifier;", "distributionListId", "Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "(Lorg/thoughtcrime/securesms/database/model/DistributionListId;)V", "getDistributionListId", "()Lorg/thoughtcrime/securesms/database/model/DistributionListId;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "matches", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DistributionList extends Identifier {
            private final DistributionListId distributionListId;

            public static /* synthetic */ DistributionList copy$default(DistributionList distributionList, DistributionListId distributionListId, int i, Object obj) {
                if ((i & 1) != 0) {
                    distributionListId = distributionList.distributionListId;
                }
                return distributionList.copy(distributionListId);
            }

            public final DistributionListId component1() {
                return this.distributionListId;
            }

            public final DistributionList copy(DistributionListId distributionListId) {
                Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
                return new DistributionList(distributionListId);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof DistributionList) && Intrinsics.areEqual(this.distributionListId, ((DistributionList) obj).distributionListId);
            }

            public int hashCode() {
                return this.distributionListId.hashCode();
            }

            public String toString() {
                return "DistributionList(distributionListId=" + this.distributionListId + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public DistributionList(DistributionListId distributionListId) {
                super(null);
                Intrinsics.checkNotNullParameter(distributionListId, "distributionListId");
                this.distributionListId = distributionListId;
            }

            public final DistributionListId getDistributionListId() {
                return this.distributionListId;
            }

            @Override // org.thoughtcrime.securesms.keyvalue.StorySend.Identifier
            public boolean matches(Recipient recipient) {
                Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
                return Intrinsics.areEqual(recipient.getDistributionListId().orElse(null), this.distributionListId);
            }
        }
    }
}
