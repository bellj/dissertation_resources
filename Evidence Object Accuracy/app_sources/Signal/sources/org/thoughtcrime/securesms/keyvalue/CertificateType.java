package org.thoughtcrime.securesms.keyvalue;

/* loaded from: classes4.dex */
public enum CertificateType {
    UUID_AND_E164,
    UUID_ONLY
}
