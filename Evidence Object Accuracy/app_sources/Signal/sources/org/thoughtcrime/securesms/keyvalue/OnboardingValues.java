package org.thoughtcrime.securesms.keyvalue;

import android.content.Context;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public final class OnboardingValues extends SignalStoreValues {
    private static final String SHOW_ADD_PHOTO;
    private static final String SHOW_APPEARANCE;
    private static final String SHOW_INVITE_FRIENDS;
    private static final String SHOW_NEW_GROUP;
    private static final String SHOW_SMS;

    public boolean shouldShowSms() {
        return false;
    }

    public OnboardingValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
        putBoolean(SHOW_NEW_GROUP, true);
        putBoolean(SHOW_INVITE_FRIENDS, true);
        putBoolean(SHOW_SMS, true);
        putBoolean(SHOW_APPEARANCE, true);
        putBoolean(SHOW_ADD_PHOTO, true);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public void clearAll() {
        setShowNewGroup(false);
        setShowInviteFriends(false);
        setShowSms(false);
        setShowAppearance(false);
        setShowAddPhoto(false);
    }

    public boolean hasOnboarding(Context context) {
        return shouldShowNewGroup() || shouldShowInviteFriends() || shouldShowSms() || shouldShowAppearance() || shouldShowAddPhoto();
    }

    public void setShowNewGroup(boolean z) {
        putBoolean(SHOW_NEW_GROUP, z);
    }

    public boolean shouldShowNewGroup() {
        return getBoolean(SHOW_NEW_GROUP, false);
    }

    public void setShowInviteFriends(boolean z) {
        putBoolean(SHOW_INVITE_FRIENDS, z);
    }

    public boolean shouldShowInviteFriends() {
        return getBoolean(SHOW_INVITE_FRIENDS, false);
    }

    public void setShowSms(boolean z) {
        putBoolean(SHOW_SMS, z);
    }

    public void setShowAppearance(boolean z) {
        putBoolean(SHOW_APPEARANCE, z);
    }

    public boolean shouldShowAppearance() {
        return getBoolean(SHOW_APPEARANCE, false);
    }

    public void setShowAddPhoto(boolean z) {
        putBoolean(SHOW_ADD_PHOTO, z);
    }

    public boolean shouldShowAddPhoto() {
        return getBoolean(SHOW_ADD_PHOTO, false);
    }
}
