package org.thoughtcrime.securesms.keyvalue;

import android.content.Context;
import android.net.Uri;
import com.google.protobuf.InvalidProtocolBufferException;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.model.databaseprotos.Wallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaper;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperFactory;
import org.thoughtcrime.securesms.wallpaper.WallpaperStorage;

/* loaded from: classes4.dex */
public final class WallpaperValues extends SignalStoreValues {
    private static final String KEY_WALLPAPER;
    private static final String TAG = Log.tag(WallpaperValues.class);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public WallpaperValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public void setWallpaper(Context context, ChatWallpaper chatWallpaper) {
        Wallpaper currentWallpaper = getCurrentWallpaper();
        Uri parse = (currentWallpaper == null || !currentWallpaper.hasFile()) ? null : Uri.parse(currentWallpaper.getFile().getUri());
        if (chatWallpaper != null) {
            putBlob(KEY_WALLPAPER, chatWallpaper.serialize().toByteArray());
        } else {
            getStore().beginWrite().remove(KEY_WALLPAPER).apply();
        }
        if (parse != null) {
            WallpaperStorage.onWallpaperDeselected(context, parse);
        }
    }

    public ChatWallpaper getWallpaper() {
        Wallpaper currentWallpaper = getCurrentWallpaper();
        if (currentWallpaper != null) {
            return ChatWallpaperFactory.create(currentWallpaper);
        }
        return null;
    }

    public boolean hasWallpaperSet() {
        return getStore().getBlob(KEY_WALLPAPER, null) != null;
    }

    public void setDimInDarkTheme(boolean z) {
        Wallpaper currentWallpaper = getCurrentWallpaper();
        if (currentWallpaper != null) {
            putBlob(KEY_WALLPAPER, currentWallpaper.toBuilder().setDimLevelInDarkTheme(z ? 0.2f : 0.0f).build().toByteArray());
            return;
        }
        throw new IllegalStateException("No wallpaper currently set!");
    }

    public Uri getWallpaperUri() {
        Wallpaper currentWallpaper = getCurrentWallpaper();
        if (currentWallpaper == null || !currentWallpaper.hasFile()) {
            return null;
        }
        return Uri.parse(currentWallpaper.getFile().getUri());
    }

    private Wallpaper getCurrentWallpaper() {
        byte[] blob = getBlob(KEY_WALLPAPER, null);
        if (blob != null) {
            try {
                return Wallpaper.parseFrom(blob);
            } catch (InvalidProtocolBufferException unused) {
                Log.w(TAG, "Invalid proto stored for wallpaper!");
            }
        }
        return null;
    }
}
