package org.thoughtcrime.securesms.keyvalue;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.keyvalue.KeyValueStore;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class EmojiValues extends SignalStoreValues {
    public static final List<String> DEFAULT_REACTIONS_LIST = Arrays.asList("❤️", "👍", "👎", "😂", "😮", "😢");
    private static final String JUMBO_EMOJI_DOWNLOAD;
    private static final String LAST_SEARCH_CHECK;
    private static final String NEXT_SCHEDULED_CHECK;
    public static final String NO_LANGUAGE;
    private static final String PREFIX;
    private static final String REACTIONS_LIST;
    private static final String SEARCH_LANGUAGE;
    private static final String SEARCH_VERSION;

    public EmojiValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
        putInteger(SEARCH_VERSION, 0);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.singletonList(REACTIONS_LIST);
    }

    public long getNextScheduledImageCheck() {
        return getStore().getLong(NEXT_SCHEDULED_CHECK, 0);
    }

    public void setNextScheduledImageCheck(long j) {
        putLong(NEXT_SCHEDULED_CHECK, j);
    }

    public void setPreferredVariation(String str) {
        String canonicalRepresentation = EmojiUtil.getCanonicalRepresentation(str);
        if (canonicalRepresentation.equals(str)) {
            KeyValueStore.Writer beginWrite = getStore().beginWrite();
            beginWrite.remove(PREFIX + canonicalRepresentation).apply();
            return;
        }
        putString(PREFIX + canonicalRepresentation, str);
    }

    public String getPreferredVariation(String str) {
        String canonicalRepresentation = EmojiUtil.getCanonicalRepresentation(str);
        return getString(PREFIX + canonicalRepresentation, str);
    }

    public List<String> getReactions() {
        List<String> rawReactions = getRawReactions();
        ArrayList arrayList = new ArrayList(DEFAULT_REACTIONS_LIST.size());
        int i = 0;
        while (true) {
            List<String> list = DEFAULT_REACTIONS_LIST;
            if (i >= list.size()) {
                return arrayList;
            }
            if (rawReactions.size() <= i || !EmojiUtil.isEmoji(rawReactions.get(i))) {
                arrayList.add(list.get(i));
            } else {
                arrayList.add(rawReactions.get(i));
            }
            i++;
        }
    }

    public List<String> getRawReactions() {
        String string = getString(REACTIONS_LIST, "");
        if (TextUtils.isEmpty(string)) {
            return Collections.emptyList();
        }
        return Arrays.asList(string.split(","));
    }

    public void setReactions(List<String> list) {
        putString(REACTIONS_LIST, Util.join(list, ","));
    }

    public void onSearchIndexUpdated(int i, String str) {
        getStore().beginWrite().putInteger(SEARCH_VERSION, i).putString(SEARCH_LANGUAGE, str).apply();
    }

    public boolean hasSearchIndex() {
        return getSearchVersion() > 0 && getSearchLanguage() != null;
    }

    public int getSearchVersion() {
        return getInteger(SEARCH_VERSION, 0);
    }

    public String getSearchLanguage() {
        return getString(SEARCH_LANGUAGE, null);
    }

    public long getLastSearchIndexCheck() {
        return getLong(LAST_SEARCH_CHECK, 0);
    }

    public void setLastSearchIndexCheck(long j) {
        putLong(LAST_SEARCH_CHECK, j);
    }

    public void addJumboEmojiSheet(int i, String str) {
        HashSet<String> jumboEmojiSheets = getJumboEmojiSheets(i);
        jumboEmojiSheets.add(str);
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        beginWrite.putString(JUMBO_EMOJI_DOWNLOAD + i, Util.join(jumboEmojiSheets, ",")).apply();
    }

    public HashSet<String> getJumboEmojiSheets(int i) {
        KeyValueStore store = getStore();
        return new HashSet<>(Arrays.asList(store.getString(JUMBO_EMOJI_DOWNLOAD + i, "").split(",")));
    }

    public void clearJumboEmojiSheets(int i) {
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        beginWrite.remove(JUMBO_EMOJI_DOWNLOAD + i).apply();
    }
}
