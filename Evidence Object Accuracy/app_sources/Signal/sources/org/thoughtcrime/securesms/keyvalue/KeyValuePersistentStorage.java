package org.thoughtcrime.securesms.keyvalue;

import java.util.Collection;

/* loaded from: classes4.dex */
public interface KeyValuePersistentStorage {
    KeyValueDataSet getDataSet();

    void writeDataSet(KeyValueDataSet keyValueDataSet, Collection<String> collection);
}
