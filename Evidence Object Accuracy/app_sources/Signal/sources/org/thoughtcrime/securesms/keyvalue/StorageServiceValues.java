package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;
import org.whispersystems.signalservice.api.storage.SignalStorageManifest;
import org.whispersystems.signalservice.api.storage.StorageKey;
import org.whispersystems.signalservice.api.util.Preconditions;

/* loaded from: classes4.dex */
public class StorageServiceValues extends SignalStoreValues {
    private static final String LAST_SYNC_TIME;
    private static final String MANIFEST;
    private static final String NEEDS_ACCOUNT_RESTORE;
    private static final String SYNC_STORAGE_KEY;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public StorageServiceValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public synchronized StorageKey getOrCreateStorageKey() {
        if (getStore().containsKey(SYNC_STORAGE_KEY)) {
            return new StorageKey(getBlob(SYNC_STORAGE_KEY, null));
        }
        return SignalStore.kbsValues().getOrCreateMasterKey().deriveStorageServiceKey();
    }

    public long getLastSyncTime() {
        return getLong(LAST_SYNC_TIME, 0);
    }

    public void onSyncCompleted() {
        putLong(LAST_SYNC_TIME, System.currentTimeMillis());
    }

    public boolean needsAccountRestore() {
        return getBoolean(NEEDS_ACCOUNT_RESTORE, false);
    }

    public void setNeedsAccountRestore(boolean z) {
        putBoolean(NEEDS_ACCOUNT_RESTORE, z);
    }

    public void setManifest(SignalStorageManifest signalStorageManifest) {
        putBlob(MANIFEST, signalStorageManifest.serialize());
    }

    public SignalStorageManifest getManifest() {
        byte[] blob = getBlob(MANIFEST, null);
        if (blob != null) {
            return SignalStorageManifest.deserialize(blob);
        }
        return SignalStorageManifest.EMPTY;
    }

    public synchronized void setStorageKeyFromPrimary(StorageKey storageKey) {
        Preconditions.checkState(SignalStore.account().isLinkedDevice(), "Can only set storage key directly on linked devices");
        putBlob(SYNC_STORAGE_KEY, storageKey.serialize());
    }

    public void clearStorageKeyFromPrimary() {
        Preconditions.checkState(SignalStore.account().isLinkedDevice(), "Can only clear storage key directly on linked devices");
        remove(SYNC_STORAGE_KEY);
    }
}
