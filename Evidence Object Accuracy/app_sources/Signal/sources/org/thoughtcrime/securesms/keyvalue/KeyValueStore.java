package org.thoughtcrime.securesms.keyvalue;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.webrtc.SurfaceTextureEglRenderer$$ExternalSyntheticLambda0;

/* loaded from: classes4.dex */
public final class KeyValueStore implements KeyValueReader {
    private static final String TAG = Log.tag(KeyValueStore.class);
    private KeyValueDataSet dataSet;
    private final ExecutorService executor = SignalExecutors.newCachedSingleThreadExecutor("signal-KeyValueStore");
    private final KeyValuePersistentStorage storage;

    public KeyValueStore(KeyValuePersistentStorage keyValuePersistentStorage) {
        this.storage = keyValuePersistentStorage;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized byte[] getBlob(String str, byte[] bArr) {
        initializeIfNecessary();
        return this.dataSet.getBlob(str, bArr);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized boolean getBoolean(String str, boolean z) {
        initializeIfNecessary();
        return this.dataSet.getBoolean(str, z);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized float getFloat(String str, float f) {
        initializeIfNecessary();
        return this.dataSet.getFloat(str, f);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized int getInteger(String str, int i) {
        initializeIfNecessary();
        return this.dataSet.getInteger(str, i);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized long getLong(String str, long j) {
        initializeIfNecessary();
        return this.dataSet.getLong(str, j);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized String getString(String str, String str2) {
        initializeIfNecessary();
        return this.dataSet.getString(str, str2);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.KeyValueReader
    public synchronized boolean containsKey(String str) {
        initializeIfNecessary();
        return this.dataSet.containsKey(str);
    }

    public Writer beginWrite() {
        return new Writer();
    }

    synchronized KeyValueReader beginRead() {
        KeyValueDataSet keyValueDataSet;
        initializeIfNecessary();
        keyValueDataSet = new KeyValueDataSet();
        keyValueDataSet.putAll(this.dataSet);
        return keyValueDataSet;
    }

    public synchronized void blockUntilAllWritesFinished() {
        CountDownLatch countDownLatch = new CountDownLatch(1);
        this.executor.execute(new SurfaceTextureEglRenderer$$ExternalSyntheticLambda0(countDownLatch));
        try {
            countDownLatch.await();
        } catch (InterruptedException unused) {
            Log.w(TAG, "Failed to wait for all writes.");
        }
    }

    public synchronized void resetCache() {
        this.dataSet = null;
        initializeIfNecessary();
    }

    public synchronized void write(KeyValueDataSet keyValueDataSet, Collection<String> collection) {
        initializeIfNecessary();
        this.dataSet.putAll(keyValueDataSet);
        this.dataSet.removeAll(collection);
        this.executor.execute(new Runnable(keyValueDataSet, collection) { // from class: org.thoughtcrime.securesms.keyvalue.KeyValueStore$$ExternalSyntheticLambda0
            public final /* synthetic */ KeyValueDataSet f$1;
            public final /* synthetic */ Collection f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                KeyValueStore.this.lambda$write$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$write$0(KeyValueDataSet keyValueDataSet, Collection collection) {
        this.storage.writeDataSet(keyValueDataSet, collection);
    }

    private void initializeIfNecessary() {
        if (this.dataSet == null) {
            this.dataSet = this.storage.getDataSet();
        }
    }

    /* loaded from: classes4.dex */
    public class Writer {
        private final KeyValueDataSet dataSet = new KeyValueDataSet();
        private final Set<String> removes = new HashSet();

        Writer() {
            KeyValueStore.this = r1;
        }

        public Writer putBlob(String str, byte[] bArr) {
            this.dataSet.putBlob(str, bArr);
            return this;
        }

        public Writer putBoolean(String str, boolean z) {
            this.dataSet.putBoolean(str, z);
            return this;
        }

        public Writer putFloat(String str, float f) {
            this.dataSet.putFloat(str, f);
            return this;
        }

        public Writer putInteger(String str, int i) {
            this.dataSet.putInteger(str, i);
            return this;
        }

        public Writer putLong(String str, long j) {
            this.dataSet.putLong(str, j);
            return this;
        }

        public Writer putString(String str, String str2) {
            this.dataSet.putString(str, str2);
            return this;
        }

        public Writer remove(String str) {
            this.removes.add(str);
            return this;
        }

        public void apply() {
            for (String str : this.removes) {
                if (this.dataSet.containsKey(str)) {
                    throw new IllegalStateException("Tried to remove a key while also setting it!");
                }
            }
            KeyValueStore.this.write(this.dataSet, this.removes);
        }

        public void commit() {
            apply();
            KeyValueStore.this.blockUntilAllWritesFinished();
        }
    }
}
