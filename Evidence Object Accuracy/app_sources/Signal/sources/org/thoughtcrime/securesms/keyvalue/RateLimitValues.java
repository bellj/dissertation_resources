package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;
import org.greenrobot.eventbus.EventBus;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.ratelimit.RecaptchaRequiredEvent;

/* loaded from: classes4.dex */
public final class RateLimitValues extends SignalStoreValues {
    private static final String KEY_CHALLENGE;
    private static final String KEY_NEEDS_RECAPTCHA;
    private static final String TAG = Log.tag(RateLimitValues.class);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public RateLimitValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public void markNeedsRecaptcha(String str) {
        Log.i(TAG, "markNeedsRecaptcha()");
        putBoolean(KEY_NEEDS_RECAPTCHA, true);
        putString(KEY_CHALLENGE, str);
        EventBus.getDefault().post(new RecaptchaRequiredEvent());
    }

    public void onProofAccepted() {
        Log.i(TAG, "onProofAccepted()", new Throwable());
        putBoolean(KEY_NEEDS_RECAPTCHA, false);
        remove(KEY_CHALLENGE);
    }

    public boolean needsRecaptcha() {
        return getBoolean(KEY_NEEDS_RECAPTCHA, false);
    }

    public String getChallenge() {
        return getString(KEY_CHALLENGE, "");
    }
}
