package org.thoughtcrime.securesms.keyvalue;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import io.reactivex.rxjava3.subjects.Subject;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.SetsKt___SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;
import kotlin.text.StringsKt__StringsKt;
import org.signal.core.util.logging.Log;
import org.signal.donations.StripeApi;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.model.databaseprotos.BadgeList;
import org.thoughtcrime.securesms.jobs.SubscriptionReceiptRequestResponseJob;
import org.thoughtcrime.securesms.keyvalue.KeyValueStore;
import org.thoughtcrime.securesms.payments.currency.CurrencyUtil;
import org.thoughtcrime.securesms.subscription.LevelUpdateOperation;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.IdempotencyKey;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;
import org.whispersystems.signalservice.internal.util.JsonUtil;

/* compiled from: DonationsValues.kt */
@Metadata(d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u000b\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\b\u0003\n\u0002\u0010\"\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0019\b\u0000\u0018\u0000 f2\u00020\u0001:\u0001fB\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u00105\u001a\u0002062\u0006\u00107\u001a\u00020#H\u0002J\u0006\u00108\u001a\u000206J\u0006\u00109\u001a\u000206J\u0006\u0010:\u001a\u000206J\u0006\u0010;\u001a\u00020\u0015J\b\u0010<\u001a\u0004\u0018\u00010=J\b\u0010>\u001a\u0004\u0018\u00010=J\u000e\u0010?\u001a\b\u0012\u0004\u0012\u00020#0@H\u0014J\u0006\u0010A\u001a\u00020*J\u0006\u0010B\u001a\u00020*J\u000e\u0010C\u001a\b\u0012\u0004\u0012\u00020#0DH\u0002J\u0010\u0010E\u001a\u0004\u0018\u00010F2\u0006\u00107\u001a\u00020#J\u0006\u0010G\u001a\u00020\u0007J\b\u0010H\u001a\u0004\u0018\u00010IJ\u0010\u0010H\u001a\u0004\u0018\u00010I2\u0006\u0010J\u001a\u00020\u0007J\u0006\u0010K\u001a\u00020\u0007J\u0006\u0010L\u001a\u00020\u0015J\b\u0010M\u001a\u0004\u0018\u00010NJ\u0006\u0010O\u001a\u00020\u0015J\u0006\u0010P\u001a\u00020\u0015J\u0006\u0010Q\u001a\u000206J\u0006\u0010R\u001a\u000206J\b\u0010S\u001a\u000206H\u0014J\u0006\u0010T\u001a\u00020IJ\u000e\u0010U\u001a\u0002062\u0006\u0010V\u001a\u00020\u0015J\u0010\u0010W\u001a\u0002062\b\u0010X\u001a\u0004\u0018\u00010=J\u0010\u0010Y\u001a\u0002062\b\u0010X\u001a\u0004\u0018\u00010=J\u000e\u0010Z\u001a\u0002062\u0006\u0010[\u001a\u00020*J\u000e\u0010\\\u001a\u0002062\u0006\u0010[\u001a\u00020*J\u000e\u0010]\u001a\u0002062\u0006\u0010^\u001a\u00020FJ\u000e\u0010_\u001a\u0002062\u0006\u0010J\u001a\u00020\u0007J\u000e\u0010`\u001a\u0002062\u0006\u0010a\u001a\u00020IJ\u0010\u0010b\u001a\u0002062\b\u0010c\u001a\u0004\u0018\u00010NJ\b\u0010d\u001a\u000206H\u0007J\b\u0010e\u001a\u000206H\u0007R!\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068FX\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR!\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068FX\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000b\u001a\u0004\b\r\u0010\tR!\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00108BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u000b\u001a\u0004\b\u0011\u0010\u0012R$\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00158F@FX\u000e¢\u0006\f\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR+\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001b\u001a\u00020\u00158G@FX\u0002¢\u0006\u0012\n\u0004\b\u001e\u0010\u001f\u001a\u0004\b\u001c\u0010\u0018\"\u0004\b\u001d\u0010\u001aR!\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00070\u00108BX\u0002¢\u0006\f\n\u0004\b\"\u0010\u000b\u001a\u0004\b!\u0010\u0012R/\u0010$\u001a\u0004\u0018\u00010#2\b\u0010\u001b\u001a\u0004\u0018\u00010#8F@FX\u0002¢\u0006\u0012\n\u0004\b)\u0010\u001f\u001a\u0004\b%\u0010&\"\u0004\b'\u0010(R+\u0010+\u001a\u00020*2\u0006\u0010\u001b\u001a\u00020*8F@FX\u0002¢\u0006\u0012\n\u0004\b0\u0010\u001f\u001a\u0004\b,\u0010-\"\u0004\b.\u0010/R+\u00101\u001a\u00020*2\u0006\u0010\u001b\u001a\u00020*8F@FX\u0002¢\u0006\u0012\n\u0004\b4\u0010\u001f\u001a\u0004\b2\u0010-\"\u0004\b3\u0010/¨\u0006g"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/DonationsValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "observableOneTimeCurrency", "Lio/reactivex/rxjava3/core/Observable;", "Ljava/util/Currency;", "getObservableOneTimeCurrency", "()Lio/reactivex/rxjava3/core/Observable;", "observableOneTimeCurrency$delegate", "Lkotlin/Lazy;", "observableSubscriptionCurrency", "getObservableSubscriptionCurrency", "observableSubscriptionCurrency$delegate", "oneTimeCurrencyPublisher", "Lio/reactivex/rxjava3/subjects/Subject;", "getOneTimeCurrencyPublisher", "()Lio/reactivex/rxjava3/subjects/Subject;", "oneTimeCurrencyPublisher$delegate", DraftDatabase.DRAFT_VALUE, "", "shouldCancelSubscriptionBeforeNextSubscribeAttempt", "getShouldCancelSubscriptionBeforeNextSubscribeAttempt", "()Z", "setShouldCancelSubscriptionBeforeNextSubscribeAttempt", "(Z)V", "<set-?>", "showCantProcessDialog", "setShowCantProcessDialog", "showCantProcessDialog$delegate", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "subscriptionCurrencyPublisher", "getSubscriptionCurrencyPublisher", "subscriptionCurrencyPublisher$delegate", "", "unexpectedSubscriptionCancelationReason", "getUnexpectedSubscriptionCancelationReason", "()Ljava/lang/String;", "setUnexpectedSubscriptionCancelationReason", "(Ljava/lang/String;)V", "unexpectedSubscriptionCancelationReason$delegate", "", "unexpectedSubscriptionCancelationTimestamp", "getUnexpectedSubscriptionCancelationTimestamp", "()J", "setUnexpectedSubscriptionCancelationTimestamp", "(J)V", "unexpectedSubscriptionCancelationTimestamp$delegate", "unexpectedSubscriptionCancelationWatermark", "getUnexpectedSubscriptionCancelationWatermark", "setUnexpectedSubscriptionCancelationWatermark", "unexpectedSubscriptionCancelationWatermark$delegate", "addLevelToHistory", "", "level", "clearLevelOperations", "clearSubscriptionRedemptionFailed", "clearUserManuallyCancelled", "getDisplayBadgesOnProfile", "getExpiredBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getExpiredGiftBadge", "getKeysToIncludeInBackup", "", "getLastEndOfPeriod", "getLastKeepAliveLaunchTime", "getLevelHistory", "", "getLevelOperation", "Lorg/thoughtcrime/securesms/subscription/LevelUpdateOperation;", "getOneTimeCurrency", "getSubscriber", "Lorg/thoughtcrime/securesms/subscription/Subscriber;", "currency", "getSubscriptionCurrency", "getSubscriptionRedemptionFailed", "getUnexpectedSubscriptionCancelationChargeFailure", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$ChargeFailure;", "isLikelyASustainer", "isUserManuallyCancelled", "markSubscriptionRedemptionFailed", "markUserManuallyCancelled", "onFirstEverAppLaunch", "requireSubscriber", "setDisplayBadgesOnProfile", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "setExpiredBadge", "badge", "setExpiredGiftBadge", "setLastEndOfPeriod", "timestamp", "setLastKeepAliveLaunchTime", "setLevelOperation", "levelUpdateOperation", "setOneTimeCurrency", "setSubscriber", "subscriber", "setUnexpectedSubscriptionCancelationChargeFailure", "chargeFailure", "updateLocalStateForLocalSubscribe", "updateLocalStateForManualCancellation", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationsValues extends SignalStoreValues {
    static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(DonationsValues.class, "unexpectedSubscriptionCancelationReason", "getUnexpectedSubscriptionCancelationReason()Ljava/lang/String;", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(DonationsValues.class, "unexpectedSubscriptionCancelationTimestamp", "getUnexpectedSubscriptionCancelationTimestamp()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(DonationsValues.class, "unexpectedSubscriptionCancelationWatermark", "getUnexpectedSubscriptionCancelationWatermark()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(DonationsValues.class, "showCantProcessDialog", "showCantProcessDialog()Z", 0))};
    public static final Companion Companion = new Companion(null);
    private static final String DISPLAY_BADGES_ON_PROFILE;
    private static final String EXPIRED_BADGE;
    private static final String EXPIRED_GIFT_BADGE;
    private static final String KEY_CURRENCY_CODE_ONE_TIME;
    private static final String KEY_LAST_END_OF_PERIOD_SECONDS;
    private static final String KEY_LAST_KEEP_ALIVE_LAUNCH;
    private static final String KEY_LEVEL_HISTORY;
    private static final String KEY_LEVEL_OPERATION_PREFIX;
    private static final String KEY_SUBSCRIBER_ID_PREFIX;
    private static final String KEY_SUBSCRIPTION_CURRENCY_CODE;
    private static final String SHOULD_CANCEL_SUBSCRIPTION_BEFORE_NEXT_SUBSCRIBE_ATTEMPT;
    private static final String SHOW_CANT_PROCESS_DIALOG;
    private static final String SUBSCRIPTION_CANCELATION_CHARGE_FAILURE;
    private static final String SUBSCRIPTION_CANCELATION_REASON;
    private static final String SUBSCRIPTION_CANCELATION_TIMESTAMP;
    private static final String SUBSCRIPTION_CANCELATION_WATERMARK;
    private static final String SUBSCRIPTION_REDEMPTION_FAILED;
    private static final String TAG = Log.tag(DonationsValues.class);
    private static final String USER_MANUALLY_CANCELLED;
    private final Lazy observableOneTimeCurrency$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Subject<Currency>>(this) { // from class: org.thoughtcrime.securesms.keyvalue.DonationsValues$observableOneTimeCurrency$2
        final /* synthetic */ DonationsValues this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Subject<Currency> invoke() {
            return this.this$0.getOneTimeCurrencyPublisher();
        }
    });
    private final Lazy observableSubscriptionCurrency$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Subject<Currency>>(this) { // from class: org.thoughtcrime.securesms.keyvalue.DonationsValues$observableSubscriptionCurrency$2
        final /* synthetic */ DonationsValues this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Subject<Currency> invoke() {
            return this.this$0.getSubscriptionCurrencyPublisher();
        }
    });
    private final Lazy oneTimeCurrencyPublisher$delegate = LazyKt__LazyJVMKt.lazy(new Function0<BehaviorSubject<Currency>>(this) { // from class: org.thoughtcrime.securesms.keyvalue.DonationsValues$oneTimeCurrencyPublisher$2
        final /* synthetic */ DonationsValues this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final BehaviorSubject<Currency> invoke() {
            return BehaviorSubject.createDefault(this.this$0.getOneTimeCurrency());
        }
    });
    private final SignalStoreValueDelegate showCantProcessDialog$delegate = SignalStoreValueDelegatesKt.booleanValue(this, SHOW_CANT_PROCESS_DIALOG, true);
    private final Lazy subscriptionCurrencyPublisher$delegate = LazyKt__LazyJVMKt.lazy(new Function0<BehaviorSubject<Currency>>(this) { // from class: org.thoughtcrime.securesms.keyvalue.DonationsValues$subscriptionCurrencyPublisher$2
        final /* synthetic */ DonationsValues this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final BehaviorSubject<Currency> invoke() {
            return BehaviorSubject.createDefault(this.this$0.getSubscriptionCurrency());
        }
    });
    private final SignalStoreValueDelegate unexpectedSubscriptionCancelationReason$delegate = SignalStoreValueDelegatesKt.stringValue(this, SUBSCRIPTION_CANCELATION_REASON, null);
    private final SignalStoreValueDelegate unexpectedSubscriptionCancelationTimestamp$delegate = SignalStoreValueDelegatesKt.longValue(this, SUBSCRIPTION_CANCELATION_TIMESTAMP, 0);
    private final SignalStoreValueDelegate unexpectedSubscriptionCancelationWatermark$delegate = SignalStoreValueDelegatesKt.longValue(this, SUBSCRIPTION_CANCELATION_WATERMARK, 0);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DonationsValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: DonationsValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0014\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0015\u001a\n \u0016*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/DonationsValues$Companion;", "", "()V", "DISPLAY_BADGES_ON_PROFILE", "", "EXPIRED_BADGE", "EXPIRED_GIFT_BADGE", "KEY_CURRENCY_CODE_ONE_TIME", "KEY_LAST_END_OF_PERIOD_SECONDS", "KEY_LAST_KEEP_ALIVE_LAUNCH", "KEY_LEVEL_HISTORY", "KEY_LEVEL_OPERATION_PREFIX", "KEY_SUBSCRIBER_ID_PREFIX", "KEY_SUBSCRIPTION_CURRENCY_CODE", "SHOULD_CANCEL_SUBSCRIPTION_BEFORE_NEXT_SUBSCRIBE_ATTEMPT", "SHOW_CANT_PROCESS_DIALOG", "SUBSCRIPTION_CANCELATION_CHARGE_FAILURE", "SUBSCRIPTION_CANCELATION_REASON", "SUBSCRIPTION_CANCELATION_TIMESTAMP", "SUBSCRIPTION_CANCELATION_WATERMARK", "SUBSCRIPTION_REDEMPTION_FAILED", "TAG", "kotlin.jvm.PlatformType", "USER_MANUALLY_CANCELLED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.mutableListOf(KEY_CURRENCY_CODE_ONE_TIME, KEY_LAST_KEEP_ALIVE_LAUNCH, KEY_LAST_END_OF_PERIOD_SECONDS, SHOULD_CANCEL_SUBSCRIPTION_BEFORE_NEXT_SUBSCRIBE_ATTEMPT, SUBSCRIPTION_CANCELATION_REASON, SUBSCRIPTION_CANCELATION_TIMESTAMP, SUBSCRIPTION_CANCELATION_WATERMARK, SHOW_CANT_PROCESS_DIALOG);
    }

    public final Subject<Currency> getSubscriptionCurrencyPublisher() {
        Object value = this.subscriptionCurrencyPublisher$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-subscriptionCurrencyPublisher>(...)");
        return (Subject) value;
    }

    public final Observable<Currency> getObservableSubscriptionCurrency() {
        return (Observable) this.observableSubscriptionCurrency$delegate.getValue();
    }

    public final Subject<Currency> getOneTimeCurrencyPublisher() {
        Object value = this.oneTimeCurrencyPublisher$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-oneTimeCurrencyPublisher>(...)");
        return (Subject) value;
    }

    public final Observable<Currency> getObservableOneTimeCurrency() {
        return (Observable) this.observableOneTimeCurrency$delegate.getValue();
    }

    public final Currency getSubscriptionCurrency() {
        Currency currency = null;
        String string = getString(KEY_SUBSCRIPTION_CURRENCY_CODE, null);
        if (string == null) {
            Currency currencyByLocale = CurrencyUtil.getCurrencyByLocale(Locale.getDefault());
            if (currencyByLocale == null) {
                String e164 = SignalStore.account().getE164();
                if (e164 != null) {
                    currency = CurrencyUtil.getCurrencyByE164(e164);
                }
            } else {
                currency = currencyByLocale;
            }
        } else {
            currency = CurrencyUtil.getCurrencyByCurrencyCode(string);
        }
        if (currency != null) {
            List<String> supportedCurrencyCodes = StripeApi.Validation.INSTANCE.getSupportedCurrencyCodes();
            String currencyCode = currency.getCurrencyCode();
            Intrinsics.checkNotNullExpressionValue(currencyCode, "currency.currencyCode");
            Locale locale = Locale.ROOT;
            Intrinsics.checkNotNullExpressionValue(locale, "ROOT");
            String upperCase = currencyCode.toUpperCase(locale);
            Intrinsics.checkNotNullExpressionValue(upperCase, "this as java.lang.String).toUpperCase(locale)");
            if (supportedCurrencyCodes.contains(upperCase)) {
                return currency;
            }
        }
        Currency instance = Currency.getInstance("USD");
        Intrinsics.checkNotNullExpressionValue(instance, "{\n      Currency.getInstance(\"USD\")\n    }");
        return instance;
    }

    public final Currency getOneTimeCurrency() {
        String string = getString(KEY_CURRENCY_CODE_ONE_TIME, null);
        if (string == null) {
            Currency subscriptionCurrency = getSubscriptionCurrency();
            setOneTimeCurrency(subscriptionCurrency);
            return subscriptionCurrency;
        }
        Currency instance = Currency.getInstance(string);
        Intrinsics.checkNotNullExpressionValue(instance, "{\n      Currency.getInst…ce(oneTimeCurrency)\n    }");
        return instance;
    }

    public final void setOneTimeCurrency(Currency currency) {
        Intrinsics.checkNotNullParameter(currency, "currency");
        putString(KEY_CURRENCY_CODE_ONE_TIME, currency.getCurrencyCode());
        getOneTimeCurrencyPublisher().onNext(currency);
    }

    public final Subscriber getSubscriber(Currency currency) {
        Intrinsics.checkNotNullParameter(currency, "currency");
        String currencyCode = currency.getCurrencyCode();
        byte[] blob = getBlob(KEY_SUBSCRIBER_ID_PREFIX + currencyCode, null);
        if (blob == null) {
            return null;
        }
        SubscriberId fromBytes = SubscriberId.fromBytes(blob);
        Intrinsics.checkNotNullExpressionValue(fromBytes, "fromBytes(subscriberIdBytes)");
        Intrinsics.checkNotNullExpressionValue(currencyCode, "currencyCode");
        return new Subscriber(fromBytes, currencyCode);
    }

    public final Subscriber getSubscriber() {
        return getSubscriber(getSubscriptionCurrency());
    }

    public final Subscriber requireSubscriber() {
        Subscriber subscriber = getSubscriber();
        if (subscriber != null) {
            return subscriber;
        }
        throw new Exception("Subscriber ID is not set.");
    }

    public final void setSubscriber(Subscriber subscriber) {
        Intrinsics.checkNotNullParameter(subscriber, "subscriber");
        String str = TAG;
        Log.i(str, "Setting subscriber for currency " + subscriber.getCurrencyCode(), new Exception(), true);
        String currencyCode = subscriber.getCurrencyCode();
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        beginWrite.putBlob(KEY_SUBSCRIBER_ID_PREFIX + currencyCode, subscriber.getSubscriberId().getBytes()).putString(KEY_SUBSCRIPTION_CURRENCY_CODE, currencyCode).apply();
        getSubscriptionCurrencyPublisher().onNext(Currency.getInstance(currencyCode));
    }

    public final LevelUpdateOperation getLevelOperation(String str) {
        Intrinsics.checkNotNullParameter(str, "level");
        byte[] blob = getBlob(KEY_LEVEL_OPERATION_PREFIX + str, null);
        if (blob == null) {
            return null;
        }
        IdempotencyKey fromBytes = IdempotencyKey.fromBytes(blob);
        Intrinsics.checkNotNullExpressionValue(fromBytes, "fromBytes(idempotencyKey)");
        return new LevelUpdateOperation(fromBytes, str);
    }

    public final void setLevelOperation(LevelUpdateOperation levelUpdateOperation) {
        Intrinsics.checkNotNullParameter(levelUpdateOperation, "levelUpdateOperation");
        addLevelToHistory(levelUpdateOperation.getLevel());
        putBlob(KEY_LEVEL_OPERATION_PREFIX + levelUpdateOperation.getLevel(), levelUpdateOperation.getIdempotencyKey().getBytes());
    }

    private final Set<String> getLevelHistory() {
        String string = getString(KEY_LEVEL_HISTORY, "");
        Intrinsics.checkNotNullExpressionValue(string, "getString(KEY_LEVEL_HISTORY, \"\")");
        return CollectionsKt___CollectionsKt.toSet(StringsKt__StringsKt.split$default((CharSequence) string, new String[]{","}, false, 0, 6, (Object) null));
    }

    private final void addLevelToHistory(String str) {
        putString(KEY_LEVEL_HISTORY, CollectionsKt___CollectionsKt.joinToString$default(SetsKt___SetsKt.plus(getLevelHistory(), str), ",", null, null, 0, null, null, 62, null));
    }

    public final void clearLevelOperations() {
        Set<String> levelHistory = getLevelHistory();
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        Intrinsics.checkNotNullExpressionValue(beginWrite, "store.beginWrite()");
        Iterator<String> it = levelHistory.iterator();
        while (it.hasNext()) {
            beginWrite.remove(KEY_LEVEL_OPERATION_PREFIX + it.next());
        }
        beginWrite.apply();
    }

    public final void setExpiredBadge(Badge badge) {
        if (badge != null) {
            putBlob(EXPIRED_BADGE, Badges.toDatabaseBadge(badge).toByteArray());
        } else {
            remove(EXPIRED_BADGE);
        }
    }

    public final Badge getExpiredBadge() {
        byte[] blob = getBlob(EXPIRED_BADGE, null);
        if (blob == null) {
            return null;
        }
        BadgeList.Badge parseFrom = BadgeList.Badge.parseFrom(blob);
        Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(badgeBytes)");
        return Badges.fromDatabaseBadge(parseFrom);
    }

    public final void setExpiredGiftBadge(Badge badge) {
        if (badge != null) {
            putBlob(EXPIRED_GIFT_BADGE, Badges.toDatabaseBadge(badge).toByteArray());
        } else {
            remove(EXPIRED_GIFT_BADGE);
        }
    }

    public final Badge getExpiredGiftBadge() {
        byte[] blob = getBlob(EXPIRED_GIFT_BADGE, null);
        if (blob == null) {
            return null;
        }
        BadgeList.Badge parseFrom = BadgeList.Badge.parseFrom(blob);
        Intrinsics.checkNotNullExpressionValue(parseFrom, "parseFrom(badgeBytes)");
        return Badges.fromDatabaseBadge(parseFrom);
    }

    public final long getLastKeepAliveLaunchTime() {
        return getLong(KEY_LAST_KEEP_ALIVE_LAUNCH, 0);
    }

    public final void setLastKeepAliveLaunchTime(long j) {
        putLong(KEY_LAST_KEEP_ALIVE_LAUNCH, j);
    }

    public final long getLastEndOfPeriod() {
        return getLong(KEY_LAST_END_OF_PERIOD_SECONDS, 0);
    }

    public final void setLastEndOfPeriod(long j) {
        putLong(KEY_LAST_END_OF_PERIOD_SECONDS, j);
    }

    public final boolean isLikelyASustainer() {
        return TimeUnit.SECONDS.toMillis(getLastEndOfPeriod()) > System.currentTimeMillis();
    }

    public final boolean isUserManuallyCancelled() {
        return getBoolean(USER_MANUALLY_CANCELLED, false);
    }

    public final void markUserManuallyCancelled() {
        putBoolean(USER_MANUALLY_CANCELLED, true);
    }

    public final void clearUserManuallyCancelled() {
        remove(USER_MANUALLY_CANCELLED);
    }

    public final void setDisplayBadgesOnProfile(boolean z) {
        putBoolean(DISPLAY_BADGES_ON_PROFILE, z);
    }

    public final boolean getDisplayBadgesOnProfile() {
        return getBoolean(DISPLAY_BADGES_ON_PROFILE, false);
    }

    public final boolean getSubscriptionRedemptionFailed() {
        return getBoolean(SUBSCRIPTION_REDEMPTION_FAILED, false);
    }

    public final void markSubscriptionRedemptionFailed() {
        Log.w(TAG, "markSubscriptionRedemptionFailed()", new Throwable(), true);
        putBoolean(SUBSCRIPTION_REDEMPTION_FAILED, true);
    }

    public final void clearSubscriptionRedemptionFailed() {
        putBoolean(SUBSCRIPTION_REDEMPTION_FAILED, false);
    }

    public final void setUnexpectedSubscriptionCancelationChargeFailure(ActiveSubscription.ChargeFailure chargeFailure) {
        if (chargeFailure == null) {
            remove(SUBSCRIPTION_CANCELATION_CHARGE_FAILURE);
        } else {
            putString(SUBSCRIPTION_CANCELATION_CHARGE_FAILURE, JsonUtil.toJson(chargeFailure));
        }
    }

    public final ActiveSubscription.ChargeFailure getUnexpectedSubscriptionCancelationChargeFailure() {
        String string = getString(SUBSCRIPTION_CANCELATION_CHARGE_FAILURE, null);
        if (string == null || string.length() == 0) {
            return null;
        }
        return (ActiveSubscription.ChargeFailure) JsonUtil.fromJson(string, ActiveSubscription.ChargeFailure.class);
    }

    public final String getUnexpectedSubscriptionCancelationReason() {
        return (String) this.unexpectedSubscriptionCancelationReason$delegate.getValue(this, $$delegatedProperties[0]);
    }

    public final void setUnexpectedSubscriptionCancelationReason(String str) {
        this.unexpectedSubscriptionCancelationReason$delegate.setValue(this, $$delegatedProperties[0], str);
    }

    public final long getUnexpectedSubscriptionCancelationTimestamp() {
        return ((Number) this.unexpectedSubscriptionCancelationTimestamp$delegate.getValue(this, $$delegatedProperties[1])).longValue();
    }

    public final void setUnexpectedSubscriptionCancelationTimestamp(long j) {
        this.unexpectedSubscriptionCancelationTimestamp$delegate.setValue(this, $$delegatedProperties[1], Long.valueOf(j));
    }

    public final long getUnexpectedSubscriptionCancelationWatermark() {
        return ((Number) this.unexpectedSubscriptionCancelationWatermark$delegate.getValue(this, $$delegatedProperties[2])).longValue();
    }

    public final void setUnexpectedSubscriptionCancelationWatermark(long j) {
        this.unexpectedSubscriptionCancelationWatermark$delegate.setValue(this, $$delegatedProperties[2], Long.valueOf(j));
    }

    public final void setShowCantProcessDialog(boolean z) {
        this.showCantProcessDialog$delegate.setValue(this, $$delegatedProperties[3], Boolean.valueOf(z));
    }

    public final boolean showCantProcessDialog() {
        return ((Boolean) this.showCantProcessDialog$delegate.getValue(this, $$delegatedProperties[3])).booleanValue();
    }

    public final boolean getShouldCancelSubscriptionBeforeNextSubscribeAttempt() {
        return getBoolean(SHOULD_CANCEL_SUBSCRIPTION_BEFORE_NEXT_SUBSCRIBE_ATTEMPT, false);
    }

    public final void setShouldCancelSubscriptionBeforeNextSubscribeAttempt(boolean z) {
        putBoolean(SHOULD_CANCEL_SUBSCRIPTION_BEFORE_NEXT_SUBSCRIBE_ATTEMPT, z);
    }

    public final void updateLocalStateForManualCancellation() {
        Object obj = SubscriptionReceiptRequestResponseJob.MUTEX;
        Intrinsics.checkNotNullExpressionValue(obj, "MUTEX");
        synchronized (obj) {
            String str = TAG;
            Log.d(str, "[updateLocalStateForManualCancellation] Clearing donation values.");
            setLastEndOfPeriod(0);
            clearLevelOperations();
            markUserManuallyCancelled();
            setShouldCancelSubscriptionBeforeNextSubscribeAttempt(false);
            setUnexpectedSubscriptionCancelationChargeFailure(null);
            setUnexpectedSubscriptionCancelationReason(null);
            setUnexpectedSubscriptionCancelationTimestamp(0);
            Badge expiredBadge = getExpiredBadge();
            if (expiredBadge != null && expiredBadge.isSubscription()) {
                Log.d(str, "[updateLocalStateForManualCancellation] Clearing expired badge.");
                setExpiredBadge(null);
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    public final void updateLocalStateForLocalSubscribe() {
        Object obj = SubscriptionReceiptRequestResponseJob.MUTEX;
        Intrinsics.checkNotNullExpressionValue(obj, "MUTEX");
        synchronized (obj) {
            String str = TAG;
            Log.d(str, "[updateLocalStateForLocalSubscribe] Clearing donation values.");
            clearUserManuallyCancelled();
            clearLevelOperations();
            setShouldCancelSubscriptionBeforeNextSubscribeAttempt(false);
            setUnexpectedSubscriptionCancelationChargeFailure(null);
            setUnexpectedSubscriptionCancelationReason(null);
            setUnexpectedSubscriptionCancelationTimestamp(0);
            Badge expiredBadge = getExpiredBadge();
            if (expiredBadge != null && expiredBadge.isSubscription()) {
                Log.d(str, "[updateLocalStateForLocalSubscribe] Clearing expired badge.");
                setExpiredBadge(null);
            }
            Unit unit = Unit.INSTANCE;
        }
    }
}
