package org.thoughtcrime.securesms.keyvalue;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.MutablePropertyReference1Impl;
import kotlin.jvm.internal.Reflection;
import kotlin.reflect.KProperty;

/* compiled from: NotificationProfileValues.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\t\n\u0002\b\u0017\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0000\u0018\u0000 *2\u00020\u0001:\u0001*B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u000e\u0010%\u001a\b\u0012\u0004\u0012\u00020'0&H\u0014J\b\u0010(\u001a\u00020)H\u0014R+\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u00068F@FX\u0002¢\u0006\u0012\n\u0004\b\f\u0010\r\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR+\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u000e8F@FX\u0002¢\u0006\u0012\n\u0004\b\u0014\u0010\r\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R+\u0010\u0015\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u000e8F@FX\u0002¢\u0006\u0012\n\u0004\b\u0018\u0010\r\u001a\u0004\b\u0016\u0010\u0011\"\u0004\b\u0017\u0010\u0013R+\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u000e8F@FX\u0002¢\u0006\u0012\n\u0004\b\u001c\u0010\r\u001a\u0004\b\u001a\u0010\u0011\"\u0004\b\u001b\u0010\u0013R+\u0010\u001d\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u000e8F@FX\u0002¢\u0006\u0012\n\u0004\b \u0010\r\u001a\u0004\b\u001e\u0010\u0011\"\u0004\b\u001f\u0010\u0013R+\u0010!\u001a\u00020\u000e2\u0006\u0010\u0005\u001a\u00020\u000e8F@FX\u0002¢\u0006\u0012\n\u0004\b$\u0010\r\u001a\u0004\b\"\u0010\u0011\"\u0004\b#\u0010\u0013¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/NotificationProfileValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "<set-?>", "", "hasSeenTooltip", "getHasSeenTooltip", "()Z", "setHasSeenTooltip", "(Z)V", "hasSeenTooltip$delegate", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "", "lastProfilePopup", "getLastProfilePopup", "()J", "setLastProfilePopup", "(J)V", "lastProfilePopup$delegate", "lastProfilePopupTime", "getLastProfilePopupTime", "setLastProfilePopupTime", "lastProfilePopupTime$delegate", "manuallyDisabledAt", "getManuallyDisabledAt", "setManuallyDisabledAt", "manuallyDisabledAt$delegate", "manuallyEnabledProfile", "getManuallyEnabledProfile", "setManuallyEnabledProfile", "manuallyEnabledProfile$delegate", "manuallyEnabledUntil", "getManuallyEnabledUntil", "setManuallyEnabledUntil", "manuallyEnabledUntil$delegate", "getKeysToIncludeInBackup", "", "", "onFirstEverAppLaunch", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileValues extends SignalStoreValues {
    static final /* synthetic */ KProperty<Object>[] $$delegatedProperties = {Reflection.mutableProperty1(new MutablePropertyReference1Impl(NotificationProfileValues.class, "manuallyEnabledProfile", "getManuallyEnabledProfile()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(NotificationProfileValues.class, "manuallyEnabledUntil", "getManuallyEnabledUntil()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(NotificationProfileValues.class, "manuallyDisabledAt", "getManuallyDisabledAt()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(NotificationProfileValues.class, "lastProfilePopup", "getLastProfilePopup()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(NotificationProfileValues.class, "lastProfilePopupTime", "getLastProfilePopupTime()J", 0)), Reflection.mutableProperty1(new MutablePropertyReference1Impl(NotificationProfileValues.class, "hasSeenTooltip", "getHasSeenTooltip()Z", 0))};
    public static final Companion Companion = new Companion(null);
    private static final String KEY_LAST_PROFILE_POPUP;
    private static final String KEY_LAST_PROFILE_POPUP_TIME;
    public static final String KEY_MANUALLY_DISABLED_AT;
    public static final String KEY_MANUALLY_ENABLED_PROFILE;
    public static final String KEY_MANUALLY_ENABLED_UNTIL;
    private static final String KEY_SEEN_TOOLTIP;
    private final SignalStoreValueDelegate hasSeenTooltip$delegate = SignalStoreValueDelegatesKt.booleanValue(this, KEY_SEEN_TOOLTIP, false);
    private final SignalStoreValueDelegate lastProfilePopup$delegate = SignalStoreValueDelegatesKt.longValue(this, KEY_LAST_PROFILE_POPUP, 0);
    private final SignalStoreValueDelegate lastProfilePopupTime$delegate = SignalStoreValueDelegatesKt.longValue(this, KEY_LAST_PROFILE_POPUP_TIME, 0);
    private final SignalStoreValueDelegate manuallyDisabledAt$delegate = SignalStoreValueDelegatesKt.longValue(this, KEY_MANUALLY_DISABLED_AT, 0);
    private final SignalStoreValueDelegate manuallyEnabledProfile$delegate = SignalStoreValueDelegatesKt.longValue(this, KEY_MANUALLY_ENABLED_PROFILE, 0);
    private final SignalStoreValueDelegate manuallyEnabledUntil$delegate = SignalStoreValueDelegatesKt.longValue(this, KEY_MANUALLY_ENABLED_UNTIL, 0);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public NotificationProfileValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: NotificationProfileValues.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\u0002R\u0016\u0010\b\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\t\u0010\u0002R\u0016\u0010\n\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\u000b\u0010\u0002R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/NotificationProfileValues$Companion;", "", "()V", "KEY_LAST_PROFILE_POPUP", "", "KEY_LAST_PROFILE_POPUP_TIME", "KEY_MANUALLY_DISABLED_AT", "getKEY_MANUALLY_DISABLED_AT$annotations", "KEY_MANUALLY_ENABLED_PROFILE", "getKEY_MANUALLY_ENABLED_PROFILE$annotations", "KEY_MANUALLY_ENABLED_UNTIL", "getKEY_MANUALLY_ENABLED_UNTIL$annotations", "KEY_SEEN_TOOLTIP", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ void getKEY_MANUALLY_DISABLED_AT$annotations() {
        }

        public static /* synthetic */ void getKEY_MANUALLY_ENABLED_PROFILE$annotations() {
        }

        public static /* synthetic */ void getKEY_MANUALLY_ENABLED_UNTIL$annotations() {
        }

        private Companion() {
        }
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.mutableListOf(KEY_SEEN_TOOLTIP);
    }

    public final long getManuallyEnabledProfile() {
        return ((Number) this.manuallyEnabledProfile$delegate.getValue(this, $$delegatedProperties[0])).longValue();
    }

    public final void setManuallyEnabledProfile(long j) {
        this.manuallyEnabledProfile$delegate.setValue(this, $$delegatedProperties[0], Long.valueOf(j));
    }

    public final long getManuallyEnabledUntil() {
        return ((Number) this.manuallyEnabledUntil$delegate.getValue(this, $$delegatedProperties[1])).longValue();
    }

    public final void setManuallyEnabledUntil(long j) {
        this.manuallyEnabledUntil$delegate.setValue(this, $$delegatedProperties[1], Long.valueOf(j));
    }

    public final long getManuallyDisabledAt() {
        return ((Number) this.manuallyDisabledAt$delegate.getValue(this, $$delegatedProperties[2])).longValue();
    }

    public final void setManuallyDisabledAt(long j) {
        this.manuallyDisabledAt$delegate.setValue(this, $$delegatedProperties[2], Long.valueOf(j));
    }

    public final long getLastProfilePopup() {
        return ((Number) this.lastProfilePopup$delegate.getValue(this, $$delegatedProperties[3])).longValue();
    }

    public final void setLastProfilePopup(long j) {
        this.lastProfilePopup$delegate.setValue(this, $$delegatedProperties[3], Long.valueOf(j));
    }

    public final long getLastProfilePopupTime() {
        return ((Number) this.lastProfilePopupTime$delegate.getValue(this, $$delegatedProperties[4])).longValue();
    }

    public final void setLastProfilePopupTime(long j) {
        this.lastProfilePopupTime$delegate.setValue(this, $$delegatedProperties[4], Long.valueOf(j));
    }

    public final boolean getHasSeenTooltip() {
        return ((Boolean) this.hasSeenTooltip$delegate.getValue(this, $$delegatedProperties[5])).booleanValue();
    }

    public final void setHasSeenTooltip(boolean z) {
        this.hasSeenTooltip$delegate.setValue(this, $$delegatedProperties[5], Boolean.valueOf(z));
    }
}
