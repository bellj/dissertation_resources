package org.thoughtcrime.securesms.keyvalue;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.LongSerializer;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: SignalStoreValueDelegates.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0002\b\u0004\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B+\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0017\u0010\f\u001a\u00028\u00002\u0006\u0010\r\u001a\u00020\tH\u0010¢\u0006\u0004\b\u000e\u0010\u000fJ\u001f\u0010\u0010\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00028\u0000H\u0010¢\u0006\u0004\b\u0013\u0010\u0014R\u0010\u0010\u0005\u001a\u00028\u0000X\u0004¢\u0006\u0004\n\u0002\u0010\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/KeyValueEnumValue;", "T", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "key", "", "default", "serializer", "Lorg/signal/core/util/LongSerializer;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Ljava/lang/String;Ljava/lang/Object;Lorg/signal/core/util/LongSerializer;Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "Ljava/lang/Object;", "getValue", "values", "getValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)Ljava/lang/Object;", "setValue", "", DraftDatabase.DRAFT_VALUE, "setValue$Signal_Android_websiteProdRelease", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;Ljava/lang/Object;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class KeyValueEnumValue<T> extends SignalStoreValueDelegate<T> {

    /* renamed from: default */
    private final T f8default;
    private final String key;
    private final LongSerializer<T> serializer;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public KeyValueEnumValue(String str, T t, LongSerializer<T> longSerializer, KeyValueStore keyValueStore) {
        super(keyValueStore, null);
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(longSerializer, "serializer");
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
        this.key = str;
        this.f8default = t;
        this.serializer = longSerializer;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public T getValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        if (keyValueStore.containsKey(this.key)) {
            return this.serializer.deserialize(Long.valueOf(keyValueStore.getLong(this.key, 0)));
        }
        return this.f8default;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValueDelegate
    public void setValue$Signal_Android_websiteProdRelease(KeyValueStore keyValueStore, T t) {
        Intrinsics.checkNotNullParameter(keyValueStore, "values");
        keyValueStore.beginWrite().putLong(this.key, this.serializer.serialize(t).longValue()).apply();
    }
}
