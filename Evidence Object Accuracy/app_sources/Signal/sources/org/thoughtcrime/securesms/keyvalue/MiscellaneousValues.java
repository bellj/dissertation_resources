package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;

/* loaded from: classes.dex */
public final class MiscellaneousValues extends SignalStoreValues {
    private static final String CDS_TOKEN;
    private static final String CENSORSHIP_LAST_CHECK_TIME;
    private static final String CENSORSHIP_SERVICE_REACHABLE;
    private static final String CHANGE_NUMBER_LOCK;
    private static final String CLIENT_DEPRECATED;
    private static final String HAS_EVER_HAD_AN_AVATAR;
    private static final String LAST_FCM_FOREGROUND_TIME;
    private static final String LAST_GV1_ROUTINE_MIGRATION_TIME;
    private static final String LAST_GV2_PROFILE_CHECK_TIME;
    private static final String LAST_PREKEY_REFRESH_TIME;
    private static final String LAST_PROFILE_REFRESH_TIME;
    private static final String MESSAGE_REQUEST_ENABLE_TIME;
    private static final String OLD_DEVICE_TRANSFER_LOCKED;
    private static final String USERNAME_SHOW_REMINDER;

    public MiscellaneousValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
        putLong(MESSAGE_REQUEST_ENABLE_TIME, 0);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public long getLastPrekeyRefreshTime() {
        return getLong(LAST_PREKEY_REFRESH_TIME, 0);
    }

    public void setLastPrekeyRefreshTime(long j) {
        putLong(LAST_PREKEY_REFRESH_TIME, j);
    }

    public long getMessageRequestEnableTime() {
        return getLong(MESSAGE_REQUEST_ENABLE_TIME, 0);
    }

    public long getLastProfileRefreshTime() {
        return getLong(LAST_PROFILE_REFRESH_TIME, 0);
    }

    public void setLastProfileRefreshTime(long j) {
        putLong(LAST_PROFILE_REFRESH_TIME, j);
    }

    public long getLastGv1RoutineMigrationTime() {
        return getLong(LAST_GV1_ROUTINE_MIGRATION_TIME, 0);
    }

    public void setLastGv1RoutineMigrationTime(long j) {
        putLong(LAST_GV1_ROUTINE_MIGRATION_TIME, j);
    }

    public void hideUsernameReminder() {
        putBoolean(USERNAME_SHOW_REMINDER, false);
    }

    public boolean shouldShowUsernameReminder() {
        return getBoolean(USERNAME_SHOW_REMINDER, true);
    }

    public boolean isClientDeprecated() {
        return getBoolean(CLIENT_DEPRECATED, false);
    }

    public void markClientDeprecated() {
        putBoolean(CLIENT_DEPRECATED, true);
    }

    public void clearClientDeprecated() {
        putBoolean(CLIENT_DEPRECATED, false);
    }

    public boolean isOldDeviceTransferLocked() {
        return getBoolean(OLD_DEVICE_TRANSFER_LOCKED, false);
    }

    public void markOldDeviceTransferLocked() {
        putBoolean(OLD_DEVICE_TRANSFER_LOCKED, true);
    }

    public void clearOldDeviceTransferLocked() {
        putBoolean(OLD_DEVICE_TRANSFER_LOCKED, false);
    }

    public boolean hasEverHadAnAvatar() {
        return getBoolean(HAS_EVER_HAD_AN_AVATAR, false);
    }

    public void markHasEverHadAnAvatar() {
        putBoolean(HAS_EVER_HAD_AN_AVATAR, true);
    }

    public boolean isChangeNumberLocked() {
        return getBoolean(CHANGE_NUMBER_LOCK, false);
    }

    public void lockChangeNumber() {
        putBoolean(CHANGE_NUMBER_LOCK, true);
    }

    public void unlockChangeNumber() {
        putBoolean(CHANGE_NUMBER_LOCK, false);
    }

    public long getLastCensorshipServiceReachabilityCheckTime() {
        return getLong(CENSORSHIP_LAST_CHECK_TIME, 0);
    }

    public void setLastCensorshipServiceReachabilityCheckTime(long j) {
        putLong(CENSORSHIP_LAST_CHECK_TIME, j);
    }

    public boolean isServiceReachableWithoutCircumvention() {
        return getBoolean(CENSORSHIP_SERVICE_REACHABLE, false);
    }

    public void setServiceReachableWithoutCircumvention(boolean z) {
        putBoolean(CENSORSHIP_SERVICE_REACHABLE, z);
    }

    public long getLastGv2ProfileCheckTime() {
        return getLong(LAST_GV2_PROFILE_CHECK_TIME, 0);
    }

    public void setLastGv2ProfileCheckTime(long j) {
        putLong(LAST_GV2_PROFILE_CHECK_TIME, j);
    }

    public byte[] getCdsToken() {
        return getBlob(CDS_TOKEN, null);
    }

    public void setCdsToken(byte[] bArr) {
        getStore().beginWrite().putBlob(CDS_TOKEN, bArr).commit();
    }

    public long getLastFcmForegroundServiceTime() {
        return getLong(LAST_FCM_FOREGROUND_TIME, 0);
    }

    public void setLastFcmForegroundServiceTime(long j) {
        putLong(LAST_FCM_FOREGROUND_TIME, j);
    }
}
