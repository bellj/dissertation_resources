package org.thoughtcrime.securesms.keyvalue;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.LongSerializer;

/* compiled from: SignalStoreValueDelegates.kt */
@Metadata(d1 = {"\u0000<\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0012\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\u001a\"\u0010\u0000\u001a\b\u0012\u0004\u0012\u00020\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0002H\u0000\u001a\"\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\bH\u0000\u001a;\u0010\t\u001a\b\u0012\u0004\u0012\u0002H\n0\u0001\"\u0004\b\u0000\u0010\n*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\n0\fH\u0000¢\u0006\u0002\u0010\r\u001a\"\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u000fH\u0000\u001a\"\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0011H\u0000\u001a\"\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0013H\u0000\u001a3\u0010\u0014\u001a\b\u0012\u0004\u0012\u0002H\n0\u0001\"\n\b\u0000\u0010\n*\u0004\u0018\u00010\u0005*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u0002H\nH\u0000¢\u0006\u0002\u0010\u0015¨\u0006\u0016"}, d2 = {"blobValue", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "key", "", "default", "booleanValue", "", "enumValue", "T", "serializer", "Lorg/signal/core/util/LongSerializer;", "(Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;Ljava/lang/String;Ljava/lang/Object;Lorg/signal/core/util/LongSerializer;)Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "floatValue", "", "integerValue", "", "longValue", "", "stringValue", "(Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;Ljava/lang/String;Ljava/lang/String;)Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValueDelegate;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SignalStoreValueDelegatesKt {
    public static final SignalStoreValueDelegate<Long> longValue(SignalStoreValues signalStoreValues, String str, long j) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new LongValue(str, j, store);
    }

    public static final SignalStoreValueDelegate<Boolean> booleanValue(SignalStoreValues signalStoreValues, String str, boolean z) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new BooleanValue(str, z, store);
    }

    public static final <T extends String> SignalStoreValueDelegate<T> stringValue(SignalStoreValues signalStoreValues, String str, T t) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new StringValue(str, t, store);
    }

    public static final SignalStoreValueDelegate<Integer> integerValue(SignalStoreValues signalStoreValues, String str, int i) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new IntValue(str, i, store);
    }

    public static final SignalStoreValueDelegate<Float> floatValue(SignalStoreValues signalStoreValues, String str, float f) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new FloatValue(str, f, store);
    }

    public static final SignalStoreValueDelegate<byte[]> blobValue(SignalStoreValues signalStoreValues, String str, byte[] bArr) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(bArr, "default");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new BlobValue(str, bArr, store);
    }

    public static final <T> SignalStoreValueDelegate<T> enumValue(SignalStoreValues signalStoreValues, String str, T t, LongSerializer<T> longSerializer) {
        Intrinsics.checkNotNullParameter(signalStoreValues, "<this>");
        Intrinsics.checkNotNullParameter(str, "key");
        Intrinsics.checkNotNullParameter(longSerializer, "serializer");
        KeyValueStore store = signalStoreValues.getStore();
        Intrinsics.checkNotNullExpressionValue(store, "this.store");
        return new KeyValueEnumValue(str, t, longSerializer, store);
    }
}
