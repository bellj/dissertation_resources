package org.thoughtcrime.securesms.keyvalue;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.google.protobuf.InvalidProtocolBufferException;
import com.mobilecoin.lib.Mnemonics;
import com.mobilecoin.lib.exceptions.BadMnemonicException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.KeyValueStore;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.payments.Balance;
import org.thoughtcrime.securesms.payments.Entropy;
import org.thoughtcrime.securesms.payments.GeographicalRestrictions;
import org.thoughtcrime.securesms.payments.Mnemonic;
import org.thoughtcrime.securesms.payments.MobileCoinLedgerWrapper;
import org.thoughtcrime.securesms.payments.currency.CurrencyUtil;
import org.thoughtcrime.securesms.payments.proto.MobileCoinLedger;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.payments.Money;

/* compiled from: PaymentsValues.kt */
@Metadata(d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0014\b\u0000\u0018\u0000 F2\u00020\u0001:\u0002FGB\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\"\u001a\u00020\u0007J\b\u0010#\u001a\u00020\u0007H\u0002J\u0006\u0010$\u001a\u00020%J\u0006\u0010&\u001a\u00020%J\u0006\u0010'\u001a\u00020%J\u0006\u0010(\u001a\u00020%J\u0006\u0010)\u001a\u00020%J\u000e\u0010*\u001a\b\u0012\u0004\u0012\u00020,0+H\u0016J\u0006\u0010-\u001a\u00020.J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\rJ\u0006\u0010/\u001a\u00020\u000eJ\u0006\u00100\u001a\u00020\u0013J\u0006\u00101\u001a\u00020.J\b\u00102\u001a\u00020%H\u0016J\u0010\u00103\u001a\u0002042\u0006\u00105\u001a\u00020,H\u0007J\u000e\u00106\u001a\u00020%2\u0006\u0010\"\u001a\u00020\u0007J\u0018\u00107\u001a\u00020%2\u0006\u00108\u001a\u00020.2\b\u00109\u001a\u0004\u0018\u00010\u001bJ\u000e\u0010:\u001a\u00020%2\u0006\u0010;\u001a\u00020\u0013J\u0010\u0010<\u001a\u00020%2\u0006\u0010=\u001a\u00020.H\u0007J\u000e\u0010>\u001a\u00020%2\u0006\u0010?\u001a\u00020.J\u0006\u0010@\u001a\u00020.J\u0006\u0010A\u001a\u00020.J\u0006\u0010B\u001a\u00020.J\u0006\u0010C\u001a\u00020.J\u0006\u0010D\u001a\u00020.J\u0006\u0010?\u001a\u00020.J\b\u0010E\u001a\u00020.H\u0002R!\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00068BX\u0002¢\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR!\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8BX\u0002¢\u0006\f\n\u0004\b\u0011\u0010\u000b\u001a\u0004\b\u000f\u0010\u0010R!\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u00068BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u000b\u001a\u0004\b\u0014\u0010\tR\u0011\u0010\u0016\u001a\u00020\u00178F¢\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0013\u0010\u001a\u001a\u0004\u0018\u00010\u001b8F¢\u0006\u0006\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u001e\u001a\u00020\u001f8F¢\u0006\u0006\u001a\u0004\b \u0010!¨\u0006H"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/PaymentsValues;", "Lorg/thoughtcrime/securesms/keyvalue/SignalStoreValues;", "store", "Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;", "(Lorg/thoughtcrime/securesms/keyvalue/KeyValueStore;)V", "liveCurrentCurrency", "Landroidx/lifecycle/MutableLiveData;", "Ljava/util/Currency;", "getLiveCurrentCurrency", "()Landroidx/lifecycle/MutableLiveData;", "liveCurrentCurrency$delegate", "Lkotlin/Lazy;", "liveMobileCoinBalance", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/payments/Balance;", "getLiveMobileCoinBalance", "()Landroidx/lifecycle/LiveData;", "liveMobileCoinBalance$delegate", "liveMobileCoinLedger", "Lorg/thoughtcrime/securesms/payments/MobileCoinLedgerWrapper;", "getLiveMobileCoinLedger", "liveMobileCoinLedger$delegate", "paymentsAvailability", "Lorg/thoughtcrime/securesms/keyvalue/PaymentsAvailability;", "getPaymentsAvailability", "()Lorg/thoughtcrime/securesms/keyvalue/PaymentsAvailability;", "paymentsEntropy", "Lorg/thoughtcrime/securesms/payments/Entropy;", "getPaymentsEntropy", "()Lorg/thoughtcrime/securesms/payments/Entropy;", "paymentsMnemonic", "Lorg/thoughtcrime/securesms/payments/Mnemonic;", "getPaymentsMnemonic", "()Lorg/thoughtcrime/securesms/payments/Mnemonic;", "currentCurrency", "determineCurrency", "dismissAboutMobileCoinInfoCard", "", "dismissAddingToYourWalletInfoCard", "dismissCashingOutInfoCard", "dismissRecoveryPhraseInfoCard", "dismissUpdatePinInfoCard", "getKeysToIncludeInBackup", "", "", "hasPaymentsEntropy", "", "mobileCoinLatestBalance", "mobileCoinLatestFullLedger", "mobileCoinPaymentsEnabled", "onFirstEverAppLaunch", "restoreWallet", "Lorg/thoughtcrime/securesms/keyvalue/PaymentsValues$WalletRestoreResult;", "mnemonic", "setCurrentCurrency", "setEnabledAndEntropy", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "entropy", "setMobileCoinFullLedger", "ledger", "setMobileCoinPaymentsEnabled", "isMobileCoinPaymentsEnabled", "setUserConfirmedMnemonic", "userConfirmedMnemonic", "showAboutMobileCoinInfoCard", "showAddingToYourWalletInfoCard", "showCashingOutInfoCard", "showRecoveryPhraseInfoCard", "showUpdatePinInfoCard", "userHasLargeBalance", "Companion", "WalletRestoreResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PaymentsValues extends SignalStoreValues {
    public static final Companion Companion = new Companion(null);
    private static final String DEFAULT_CURRENCY_CODE;
    private static final Money.MobileCoin LARGE_BALANCE_THRESHOLD = Money.mobileCoin(BigDecimal.valueOf(500L));
    private static final String MOB_LEDGER;
    public static final String MOB_PAYMENTS_ENABLED;
    private static final String PAYMENTS_CURRENT_CURRENCY;
    private static final String PAYMENTS_ENTROPY;
    private static final String SHOW_ABOUT_MOBILE_COIN_INFO_CARD;
    private static final String SHOW_ADDING_TO_YOUR_WALLET_INFO_CARD;
    private static final String SHOW_CASHING_OUT_INFO_CARD;
    private static final String SHOW_RECOVERY_PHRASE_INFO_CARD;
    private static final String SHOW_UPDATE_PIN_INFO_CARD;
    private static final String TAG = Log.tag(PaymentsValues.class);
    private static final String USER_CONFIRMED_MNEMONIC;
    private final Lazy liveCurrentCurrency$delegate = LazyKt__LazyJVMKt.lazy(new Function0<MutableLiveData<Currency>>(this) { // from class: org.thoughtcrime.securesms.keyvalue.PaymentsValues$liveCurrentCurrency$2
        final /* synthetic */ PaymentsValues this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final MutableLiveData<Currency> invoke() {
            return new MutableLiveData<>(this.this$0.currentCurrency());
        }
    });
    private final Lazy liveMobileCoinBalance$delegate = LazyKt__LazyJVMKt.lazy(new PaymentsValues$liveMobileCoinBalance$2(this));
    private final Lazy liveMobileCoinLedger$delegate = LazyKt__LazyJVMKt.lazy(new Function0<MutableLiveData<MobileCoinLedgerWrapper>>(this) { // from class: org.thoughtcrime.securesms.keyvalue.PaymentsValues$liveMobileCoinLedger$2
        final /* synthetic */ PaymentsValues this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final MutableLiveData<MobileCoinLedgerWrapper> invoke() {
            return new MutableLiveData<>(this.this$0.mobileCoinLatestFullLedger());
        }
    });

    /* compiled from: PaymentsValues.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/PaymentsValues$WalletRestoreResult;", "", "(Ljava/lang/String;I)V", "ENTROPY_CHANGED", "ENTROPY_UNCHANGED", "MNEMONIC_ERROR", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum WalletRestoreResult {
        ENTROPY_CHANGED,
        ENTROPY_UNCHANGED,
        MNEMONIC_ERROR
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PaymentsValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
        Intrinsics.checkNotNullParameter(keyValueStore, "store");
    }

    /* compiled from: PaymentsValues.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\t\u001a\u00020\u00048\u0006XT¢\u0006\b\n\u0000\u0012\u0004\b\n\u0010\u0002R\u000e\u0010\u000b\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\n \u0007*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/keyvalue/PaymentsValues$Companion;", "", "()V", "DEFAULT_CURRENCY_CODE", "", "LARGE_BALANCE_THRESHOLD", "Lorg/whispersystems/signalservice/api/payments/Money$MobileCoin;", "kotlin.jvm.PlatformType", "MOB_LEDGER", "MOB_PAYMENTS_ENABLED", "getMOB_PAYMENTS_ENABLED$annotations", "PAYMENTS_CURRENT_CURRENCY", "PAYMENTS_ENTROPY", "SHOW_ABOUT_MOBILE_COIN_INFO_CARD", "SHOW_ADDING_TO_YOUR_WALLET_INFO_CARD", "SHOW_CASHING_OUT_INFO_CARD", "SHOW_RECOVERY_PHRASE_INFO_CARD", "SHOW_UPDATE_PIN_INFO_CARD", "TAG", "USER_CONFIRMED_MNEMONIC", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public static /* synthetic */ void getMOB_PAYMENTS_ENABLED$annotations() {
        }

        private Companion() {
        }
    }

    private final MutableLiveData<Currency> getLiveCurrentCurrency() {
        return (MutableLiveData) this.liveCurrentCurrency$delegate.getValue();
    }

    public final MutableLiveData<MobileCoinLedgerWrapper> getLiveMobileCoinLedger() {
        return (MutableLiveData) this.liveMobileCoinLedger$delegate.getValue();
    }

    private final LiveData<Balance> getLiveMobileCoinBalance() {
        Object value = this.liveMobileCoinBalance$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-liveMobileCoinBalance>(...)");
        return (LiveData) value;
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return CollectionsKt__CollectionsKt.listOf((Object[]) new String[]{PAYMENTS_ENTROPY, MOB_PAYMENTS_ENABLED, MOB_LEDGER, PAYMENTS_CURRENT_CURRENCY, DEFAULT_CURRENCY_CODE, USER_CONFIRMED_MNEMONIC, SHOW_ABOUT_MOBILE_COIN_INFO_CARD, SHOW_ADDING_TO_YOUR_WALLET_INFO_CARD, SHOW_CASHING_OUT_INFO_CARD, SHOW_RECOVERY_PHRASE_INFO_CARD, SHOW_UPDATE_PIN_INFO_CARD});
    }

    public final boolean userConfirmedMnemonic() {
        return getStore().getBoolean(USER_CONFIRMED_MNEMONIC, false);
    }

    public final void setUserConfirmedMnemonic(boolean z) {
        getStore().beginWrite().putBoolean(USER_CONFIRMED_MNEMONIC, z).commit();
    }

    public final boolean mobileCoinPaymentsEnabled() {
        return getBoolean(MOB_PAYMENTS_ENABLED, false);
    }

    public final PaymentsAvailability getPaymentsAvailability() {
        if (!SignalStore.account().isRegistered()) {
            return PaymentsAvailability.NOT_IN_REGION;
        }
        if (FeatureFlags.payments()) {
            if (mobileCoinPaymentsEnabled()) {
                if (GeographicalRestrictions.e164Allowed(SignalStore.account().getE164())) {
                    return PaymentsAvailability.WITHDRAW_AND_SEND;
                }
                return PaymentsAvailability.WITHDRAW_ONLY;
            } else if (GeographicalRestrictions.e164Allowed(SignalStore.account().getE164())) {
                return PaymentsAvailability.REGISTRATION_AVAILABLE;
            } else {
                return PaymentsAvailability.NOT_IN_REGION;
            }
        } else if (mobileCoinPaymentsEnabled()) {
            return PaymentsAvailability.WITHDRAW_ONLY;
        } else {
            return PaymentsAvailability.DISABLED_REMOTELY;
        }
    }

    public final void setMobileCoinPaymentsEnabled(boolean z) {
        if (mobileCoinPaymentsEnabled() != z) {
            if (z) {
                Entropy paymentsEntropy = getPaymentsEntropy();
                if (paymentsEntropy == null) {
                    paymentsEntropy = Entropy.generateNew();
                    Log.i(TAG, "Generated new payments entropy");
                }
                getStore().beginWrite().putBlob(PAYMENTS_ENTROPY, paymentsEntropy.getBytes()).putBoolean(MOB_PAYMENTS_ENABLED, true).putString(PAYMENTS_CURRENT_CURRENCY, currentCurrency().getCurrencyCode()).commit();
            } else {
                getStore().beginWrite().putBoolean(MOB_PAYMENTS_ENABLED, false).putBoolean(USER_CONFIRMED_MNEMONIC, false).commit();
            }
            RecipientDatabase recipients = SignalDatabase.Companion.recipients();
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            recipients.markNeedsSync(id);
            StorageSyncHelper.scheduleSyncForDataChange();
        }
    }

    public final Mnemonic getPaymentsMnemonic() {
        Entropy paymentsEntropy = getPaymentsEntropy();
        if (paymentsEntropy != null) {
            Mnemonic asMnemonic = paymentsEntropy.asMnemonic();
            Intrinsics.checkNotNullExpressionValue(asMnemonic, "paymentsEntropy.asMnemonic()");
            return asMnemonic;
        }
        throw new IllegalStateException("Entropy has not been set");
    }

    public final boolean hasPaymentsEntropy() {
        return getPaymentsEntropy() != null;
    }

    public final Entropy getPaymentsEntropy() {
        return Entropy.fromBytes(getStore().getBlob(PAYMENTS_ENTROPY, null));
    }

    public final Balance mobileCoinLatestBalance() {
        Balance balance = mobileCoinLatestFullLedger().getBalance();
        Intrinsics.checkNotNullExpressionValue(balance, "mobileCoinLatestFullLedger().balance");
        return balance;
    }

    public final LiveData<MobileCoinLedgerWrapper> liveMobileCoinLedger() {
        return getLiveMobileCoinLedger();
    }

    public final LiveData<Balance> liveMobileCoinBalance() {
        return getLiveMobileCoinBalance();
    }

    public final void setCurrentCurrency(Currency currency) {
        Intrinsics.checkNotNullParameter(currency, "currentCurrency");
        getStore().beginWrite().putString(PAYMENTS_CURRENT_CURRENCY, currency.getCurrencyCode()).commit();
        getLiveCurrentCurrency().postValue(currency);
    }

    public final Currency currentCurrency() {
        String string = getStore().getString(PAYMENTS_CURRENT_CURRENCY, null);
        if (string == null) {
            return determineCurrency();
        }
        Currency instance = Currency.getInstance(string);
        Intrinsics.checkNotNullExpressionValue(instance, "getInstance(currencyCode)");
        return instance;
    }

    public final MutableLiveData<Currency> liveCurrentCurrency() {
        return getLiveCurrentCurrency();
    }

    public final boolean showAboutMobileCoinInfoCard() {
        return getStore().getBoolean(SHOW_ABOUT_MOBILE_COIN_INFO_CARD, true);
    }

    public final boolean showAddingToYourWalletInfoCard() {
        return getStore().getBoolean(SHOW_ADDING_TO_YOUR_WALLET_INFO_CARD, true);
    }

    public final boolean showCashingOutInfoCard() {
        return getStore().getBoolean(SHOW_CASHING_OUT_INFO_CARD, true);
    }

    public final boolean showRecoveryPhraseInfoCard() {
        if (userHasLargeBalance()) {
            return getStore().getBoolean(SHOW_CASHING_OUT_INFO_CARD, true);
        }
        return false;
    }

    public final boolean showUpdatePinInfoCard() {
        if (!userHasLargeBalance() || !SignalStore.kbsValues().hasPin() || SignalStore.kbsValues().hasOptedOut() || SignalStore.pinValues().getKeyboardType() != PinKeyboardType.NUMERIC) {
            return false;
        }
        return getStore().getBoolean(SHOW_CASHING_OUT_INFO_CARD, true);
    }

    public final void dismissAboutMobileCoinInfoCard() {
        getStore().beginWrite().putBoolean(SHOW_ABOUT_MOBILE_COIN_INFO_CARD, false).apply();
    }

    public final void dismissAddingToYourWalletInfoCard() {
        getStore().beginWrite().putBoolean(SHOW_ADDING_TO_YOUR_WALLET_INFO_CARD, false).apply();
    }

    public final void dismissCashingOutInfoCard() {
        getStore().beginWrite().putBoolean(SHOW_CASHING_OUT_INFO_CARD, false).apply();
    }

    public final void dismissRecoveryPhraseInfoCard() {
        getStore().beginWrite().putBoolean(SHOW_RECOVERY_PHRASE_INFO_CARD, false).apply();
    }

    public final void dismissUpdatePinInfoCard() {
        getStore().beginWrite().putBoolean(SHOW_UPDATE_PIN_INFO_CARD, false).apply();
    }

    public final void setMobileCoinFullLedger(MobileCoinLedgerWrapper mobileCoinLedgerWrapper) {
        Intrinsics.checkNotNullParameter(mobileCoinLedgerWrapper, "ledger");
        getStore().beginWrite().putBlob(MOB_LEDGER, mobileCoinLedgerWrapper.serialize()).commit();
        getLiveMobileCoinLedger().postValue(mobileCoinLedgerWrapper);
    }

    public final MobileCoinLedgerWrapper mobileCoinLatestFullLedger() {
        byte[] blob = getStore().getBlob(MOB_LEDGER, null);
        if (blob == null) {
            return new MobileCoinLedgerWrapper(MobileCoinLedger.getDefaultInstance());
        }
        try {
            return new MobileCoinLedgerWrapper(MobileCoinLedger.parseFrom(blob));
        } catch (InvalidProtocolBufferException e) {
            Log.w(TAG, "Bad cached ledger, clearing", e);
            setMobileCoinFullLedger(new MobileCoinLedgerWrapper(MobileCoinLedger.getDefaultInstance()));
            throw new AssertionError(e);
        }
    }

    private final Currency determineCurrency() {
        String e164 = SignalStore.account().getE164();
        if (e164 == null) {
            e164 = "";
        }
        Object firstNonNull = Util.firstNonNull(CurrencyUtil.getCurrencyByE164(e164), CurrencyUtil.getCurrencyByLocale(Locale.getDefault()), Currency.getInstance(DEFAULT_CURRENCY_CODE));
        Intrinsics.checkNotNullExpressionValue(firstNonNull, "firstNonNull(\n      Curr…AULT_CURRENCY_CODE)\n    )");
        return (Currency) firstNonNull;
    }

    public final void setEnabledAndEntropy(boolean z, Entropy entropy) {
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        Intrinsics.checkNotNullExpressionValue(beginWrite, "store.beginWrite()");
        if (entropy != null) {
            beginWrite.putBlob(PAYMENTS_ENTROPY, entropy.getBytes());
        }
        beginWrite.putBoolean(MOB_PAYMENTS_ENABLED, z).commit();
    }

    public final WalletRestoreResult restoreWallet(String str) {
        Intrinsics.checkNotNullParameter(str, "mnemonic");
        try {
            byte[] bip39EntropyFromMnemonic = Mnemonics.bip39EntropyFromMnemonic(str);
            Intrinsics.checkNotNullExpressionValue(bip39EntropyFromMnemonic, "{\n      Mnemonics.bip39E…mMnemonic(mnemonic)\n    }");
            Entropy paymentsEntropy = getPaymentsEntropy();
            if (paymentsEntropy == null || !Arrays.equals(paymentsEntropy.getBytes(), bip39EntropyFromMnemonic)) {
                getStore().beginWrite().putBlob(PAYMENTS_ENTROPY, bip39EntropyFromMnemonic).putBoolean(MOB_PAYMENTS_ENABLED, true).remove(MOB_LEDGER).putBoolean(USER_CONFIRMED_MNEMONIC, true).commit();
                getLiveMobileCoinLedger().postValue(new MobileCoinLedgerWrapper(MobileCoinLedger.getDefaultInstance()));
                StorageSyncHelper.scheduleSyncForDataChange();
                return WalletRestoreResult.ENTROPY_CHANGED;
            }
            setMobileCoinPaymentsEnabled(true);
            setUserConfirmedMnemonic(true);
            return WalletRestoreResult.ENTROPY_UNCHANGED;
        } catch (BadMnemonicException unused) {
            return WalletRestoreResult.MNEMONIC_ERROR;
        }
    }

    private final boolean userHasLargeBalance() {
        return mobileCoinLatestBalance().getFullAmount().requireMobileCoin().greaterThan(LARGE_BALANCE_THRESHOLD);
    }
}
