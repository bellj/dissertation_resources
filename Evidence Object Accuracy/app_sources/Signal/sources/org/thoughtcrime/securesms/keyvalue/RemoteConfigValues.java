package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class RemoteConfigValues extends SignalStoreValues {
    private static final String CURRENT_CONFIG;
    private static final String LAST_FETCH_TIME;
    private static final String PENDING_CONFIG;
    private static final String TAG = Log.tag(RemoteConfigValues.class);

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public RemoteConfigValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public String getCurrentConfig() {
        return getString(CURRENT_CONFIG, null);
    }

    public void setCurrentConfig(String str) {
        putString(CURRENT_CONFIG, str);
    }

    public String getPendingConfig() {
        return getString(PENDING_CONFIG, getCurrentConfig());
    }

    public void setPendingConfig(String str) {
        putString(PENDING_CONFIG, str);
    }

    public long getLastFetchTime() {
        return getLong(LAST_FETCH_TIME, 0);
    }

    public void setLastFetchTime(long j) {
        putLong(LAST_FETCH_TIME, j);
    }
}
