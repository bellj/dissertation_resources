package org.thoughtcrime.securesms.keyvalue;

import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.keyvalue.KeyValueStore;

/* loaded from: classes4.dex */
public final class CertificateValues extends SignalStoreValues {
    private static final String UD_CERTIFICATE_UUID_AND_E164;
    private static final String UD_CERTIFICATE_UUID_ONLY;

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public void onFirstEverAppLaunch() {
    }

    public CertificateValues(KeyValueStore keyValueStore) {
        super(keyValueStore);
    }

    @Override // org.thoughtcrime.securesms.keyvalue.SignalStoreValues
    public List<String> getKeysToIncludeInBackup() {
        return Collections.emptyList();
    }

    public void setUnidentifiedAccessCertificate(CertificateType certificateType, byte[] bArr) {
        KeyValueStore.Writer beginWrite = getStore().beginWrite();
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType[certificateType.ordinal()];
        if (i == 1) {
            beginWrite.putBlob(UD_CERTIFICATE_UUID_AND_E164, bArr);
        } else if (i == 2) {
            beginWrite.putBlob(UD_CERTIFICATE_UUID_ONLY, bArr);
        } else {
            throw new AssertionError();
        }
        beginWrite.commit();
    }

    /* renamed from: org.thoughtcrime.securesms.keyvalue.CertificateValues$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType;

        static {
            int[] iArr = new int[CertificateType.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType = iArr;
            try {
                iArr[CertificateType.UUID_AND_E164.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType[CertificateType.UUID_ONLY.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public byte[] getUnidentifiedAccessCertificate(CertificateType certificateType) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyvalue$CertificateType[certificateType.ordinal()];
        if (i == 1) {
            return getBlob(UD_CERTIFICATE_UUID_AND_E164, null);
        }
        if (i == 2) {
            return getBlob(UD_CERTIFICATE_UUID_ONLY, null);
        }
        throw new AssertionError();
    }
}
