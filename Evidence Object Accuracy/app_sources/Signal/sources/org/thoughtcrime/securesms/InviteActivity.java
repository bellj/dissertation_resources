package org.thoughtcrime.securesms;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import j$.util.Optional;
import java.util.concurrent.ExecutionException;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.components.ContactFilterView;
import org.thoughtcrime.securesms.contacts.SelectedContact;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.sms.OutgoingTextMessage;
import org.thoughtcrime.securesms.util.DynamicNoActionBarInviteTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.task.ProgressDialogAsyncTask;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* loaded from: classes.dex */
public class InviteActivity extends PassphraseRequiredActivity implements ContactSelectionListFragment.OnContactSelectedListener {
    static final /* synthetic */ boolean $assertionsDisabled;
    private ContactSelectionListFragment contactsFragment;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarInviteTheme();
    private EditText inviteText;
    private Animation slideInAnimation;
    private Animation slideOutAnimation;
    private Button smsSendButton;
    private ViewGroup smsSendFrame;

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onPreCreate() {
        super.onPreCreate();
        this.dynamicTheme.onCreate(this);
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        getIntent().putExtra("display_mode", 2);
        getIntent().putExtra("selection_limits", SelectionLimits.NO_LIMITS);
        getIntent().putExtra("hide_count", true);
        getIntent().putExtra("refreshable", false);
        setContentView(R.layout.invite_activity);
        initializeAppBar();
        initializeResources();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    private void initializeAppBar() {
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.AndroidManifest__invite_friends);
    }

    private void initializeResources() {
        this.slideInAnimation = loadAnimation(R.anim.slide_from_bottom);
        this.slideOutAnimation = loadAnimation(R.anim.slide_to_bottom);
        View findViewById = findViewById(R.id.share_button);
        TextView textView = (TextView) findViewById(R.id.share_text);
        View findViewById2 = findViewById(R.id.sms_button);
        this.inviteText = (EditText) findViewById(R.id.invite_text);
        this.smsSendFrame = (ViewGroup) findViewById(R.id.sms_send_frame);
        this.smsSendButton = (Button) findViewById(R.id.send_sms_button);
        this.contactsFragment = (ContactSelectionListFragment) getSupportFragmentManager().findFragmentById(R.id.contact_selection_list_fragment);
        this.inviteText.setText(getString(R.string.InviteActivity_lets_switch_to_signal, new Object[]{getString(R.string.install_url)}));
        this.inviteText.addTextChangedListener(new AfterTextChanged(new Consumer(findViewById2, findViewById) { // from class: org.thoughtcrime.securesms.InviteActivity$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;
            public final /* synthetic */ View f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                InviteActivity.$r8$lambda$TuaC1DKazN2qsXseuGjHR3euoSI(this.f$0, this.f$1, (Editable) obj);
            }
        }));
        updateSmsButtonText(this.contactsFragment.getSelectedContacts().size());
        ((Button) findViewById(R.id.cancel_sms_button)).setOnClickListener(new SmsCancelClickListener());
        this.smsSendButton.setOnClickListener(new SmsSendClickListener());
        ((ContactFilterView) findViewById(R.id.contact_filter_edit_text)).setOnFilterChangedListener(new ContactFilterChangedListener());
        if (Util.isDefaultSmsProvider(this)) {
            findViewById.setOnClickListener(new ShareClickListener());
            findViewById2.setOnClickListener(new SmsClickListener());
            return;
        }
        findViewById2.setVisibility(8);
        textView.setText(R.string.InviteActivity_share);
        findViewById.setOnClickListener(new ShareClickListener());
    }

    public static /* synthetic */ void lambda$initializeResources$0(View view, View view2, Editable editable) {
        boolean z = editable.length() > 0;
        view.setEnabled(z);
        view2.setEnabled(z);
        float f = 1.0f;
        view.animate().alpha(z ? 1.0f : 0.5f);
        ViewPropertyAnimator animate = view2.animate();
        if (!z) {
            f = 0.5f;
        }
        animate.alpha(f);
    }

    private Animation loadAnimation(int i) {
        Animation loadAnimation = AnimationUtils.loadAnimation(this, i);
        loadAnimation.setInterpolator(new FastOutSlowInInterpolator());
        return loadAnimation;
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, j$.util.function.Consumer<Boolean> consumer) {
        updateSmsButtonText(this.contactsFragment.getSelectedContacts().size() + 1);
        consumer.accept(Boolean.TRUE);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
        updateSmsButtonText(this.contactsFragment.getSelectedContacts().size());
    }

    public void sendSmsInvites() {
        new SendSmsInvitesAsyncTask(this, this.inviteText.getText().toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, (SelectedContact[]) this.contactsFragment.getSelectedContacts().toArray(new SelectedContact[0]));
    }

    private void updateSmsButtonText(int i) {
        boolean z = true;
        this.smsSendButton.setText(getResources().getString(R.string.InviteActivity_send_sms, Integer.valueOf(i)));
        Button button = this.smsSendButton;
        if (i <= 0) {
            z = false;
        }
        button.setEnabled(z);
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
        if (this.smsSendFrame.getVisibility() == 0) {
            cancelSmsSelection();
        } else {
            super.onBackPressed();
        }
    }

    @Override // androidx.appcompat.app.AppCompatActivity
    public boolean onSupportNavigateUp() {
        if (this.smsSendFrame.getVisibility() != 0) {
            return super.onSupportNavigateUp();
        }
        cancelSmsSelection();
        return false;
    }

    public void cancelSmsSelection() {
        this.contactsFragment.reset();
        updateSmsButtonText(this.contactsFragment.getSelectedContacts().size());
        ViewUtil.animateOut(this.smsSendFrame, this.slideOutAnimation, 8);
    }

    /* loaded from: classes.dex */
    public class ShareClickListener implements View.OnClickListener {
        private ShareClickListener() {
            InviteActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.SEND");
            intent.putExtra("android.intent.extra.TEXT", InviteActivity.this.inviteText.getText().toString());
            intent.setType("text/plain");
            if (intent.resolveActivity(InviteActivity.this.getPackageManager()) != null) {
                InviteActivity inviteActivity = InviteActivity.this;
                inviteActivity.startActivity(Intent.createChooser(intent, inviteActivity.getString(R.string.InviteActivity_invite_to_signal)));
                return;
            }
            Toast.makeText(InviteActivity.this, (int) R.string.InviteActivity_no_app_to_share_to, 1).show();
        }
    }

    /* loaded from: classes.dex */
    public class SmsClickListener implements View.OnClickListener {
        private SmsClickListener() {
            InviteActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            ViewUtil.animateIn(InviteActivity.this.smsSendFrame, InviteActivity.this.slideInAnimation);
        }
    }

    /* loaded from: classes.dex */
    public class SmsCancelClickListener implements View.OnClickListener {
        private SmsCancelClickListener() {
            InviteActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            InviteActivity.this.cancelSmsSelection();
        }
    }

    /* loaded from: classes.dex */
    public class SmsSendClickListener implements View.OnClickListener {
        private SmsSendClickListener() {
            InviteActivity.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            new AlertDialog.Builder(InviteActivity.this).setTitle(InviteActivity.this.getResources().getQuantityString(R.plurals.InviteActivity_send_sms_invites, InviteActivity.this.contactsFragment.getSelectedContacts().size(), Integer.valueOf(InviteActivity.this.contactsFragment.getSelectedContacts().size()))).setMessage(InviteActivity.this.inviteText.getText().toString()).setPositiveButton(R.string.yes, new InviteActivity$SmsSendClickListener$$ExternalSyntheticLambda0(this)).setNegativeButton(R.string.no, new InviteActivity$SmsSendClickListener$$ExternalSyntheticLambda1()).show();
        }

        public /* synthetic */ void lambda$onClick$0(DialogInterface dialogInterface, int i) {
            InviteActivity.this.sendSmsInvites();
        }
    }

    /* loaded from: classes.dex */
    public class ContactFilterChangedListener implements ContactFilterView.OnFilterChangedListener {
        private ContactFilterChangedListener() {
            InviteActivity.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.ContactFilterView.OnFilterChangedListener
        public void onFilterChanged(String str) {
            InviteActivity.this.contactsFragment.setQueryFilter(str);
        }
    }

    /* loaded from: classes.dex */
    public class SendSmsInvitesAsyncTask extends ProgressDialogAsyncTask<SelectedContact, Void, Void> {
        private final String message;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        SendSmsInvitesAsyncTask(Context context, String str) {
            super(context, (int) R.string.InviteActivity_sending, (int) R.string.InviteActivity_sending);
            InviteActivity.this = r1;
            this.message = str;
        }

        public Void doInBackground(SelectedContact... selectedContactArr) {
            Context context = getContext();
            if (context == null) {
                return null;
            }
            for (SelectedContact selectedContact : selectedContactArr) {
                Recipient resolved = Recipient.resolved(selectedContact.getOrCreateRecipientId(context));
                MessageSender.send(context, new OutgoingTextMessage(resolved, this.message, resolved.getDefaultSubscriptionId().orElse(-1).intValue()), -1L, true, (String) null, (MessageDatabase.InsertListener) null);
                if (resolved.getContactUri() != null) {
                    SignalDatabase.recipients().setHasSentInvite(resolved.getId());
                }
            }
            return null;
        }

        public void onPostExecute(Void r4) {
            super.onPostExecute((SendSmsInvitesAsyncTask) r4);
            Context context = getContext();
            if (context != null) {
                ViewUtil.animateOut(InviteActivity.this.smsSendFrame, InviteActivity.this.slideOutAnimation, 8).addListener(new ListenableFuture.Listener<Boolean>() { // from class: org.thoughtcrime.securesms.InviteActivity.SendSmsInvitesAsyncTask.1
                    @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
                    public void onFailure(ExecutionException executionException) {
                    }

                    public void onSuccess(Boolean bool) {
                        InviteActivity.this.contactsFragment.reset();
                    }
                });
                Toast.makeText(context, (int) R.string.InviteActivity_invitations_sent, 1).show();
            }
        }
    }
}
