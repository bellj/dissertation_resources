package org.thoughtcrime.securesms.gcm;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.messages.BackgroundMessageRetriever;
import org.thoughtcrime.securesms.messages.RestStrategy;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes.dex */
public class FcmJobService extends JobService {
    private static final int ID;
    private static final String TAG = Log.tag(FcmJobService.class);

    public static void schedule(Context context) {
        ServiceUtil.getJobScheduler(context).schedule(new JobInfo.Builder(ID, new ComponentName(context, FcmJobService.class)).setRequiredNetworkType(1).setBackoffCriteria(0, 0).setPersisted(true).build());
    }

    @Override // android.app.job.JobService
    public boolean onStartJob(JobParameters jobParameters) {
        String str = TAG;
        Log.d(str, "onStartJob()");
        if (BackgroundMessageRetriever.shouldIgnoreFetch()) {
            Log.i(str, "App is foregrounded. No need to run.");
            return false;
        }
        SignalExecutors.UNBOUNDED.execute(new Runnable(jobParameters) { // from class: org.thoughtcrime.securesms.gcm.FcmJobService$$ExternalSyntheticLambda0
            public final /* synthetic */ JobParameters f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                FcmJobService.this.lambda$onStartJob$0(this.f$1);
            }
        });
        return true;
    }

    public /* synthetic */ void lambda$onStartJob$0(JobParameters jobParameters) {
        if (ApplicationDependencies.getBackgroundMessageRetriever().retrieveMessages(getApplicationContext(), new RestStrategy(), new RestStrategy())) {
            Log.i(TAG, "Successfully retrieved messages.");
            jobFinished(jobParameters, false);
            return;
        }
        Log.w(TAG, "Failed to retrieve messages. Scheduling a retry.");
        jobFinished(jobParameters, true);
    }

    @Override // android.app.job.JobService
    public boolean onStopJob(JobParameters jobParameters) {
        Log.d(TAG, "onStopJob()");
        return TextSecurePreferences.getNeedsMessagePull(getApplicationContext());
    }
}
