package org.thoughtcrime.securesms.gcm;

import android.content.Context;
import android.os.Build;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.FcmRefreshJob;
import org.thoughtcrime.securesms.jobs.SubmitRateLimitPushChallengeJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.PushChallengeRequest;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.NetworkUtil;

/* loaded from: classes4.dex */
public class FcmReceiveService extends FirebaseMessagingService {
    private static final long FCM_FOREGROUND_INTERVAL = TimeUnit.MINUTES.toMillis(3);
    private static final String TAG = Log.tag(FcmReceiveService.class);

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.i(TAG, String.format(Locale.US, "onMessageReceived() ID: %s, Delay: %d, Priority: %d, Original Priority: %d, Network: %s", remoteMessage.getMessageId(), Long.valueOf(System.currentTimeMillis() - remoteMessage.getSentTime()), Integer.valueOf(remoteMessage.getPriority()), Integer.valueOf(remoteMessage.getOriginalPriority()), NetworkUtil.getNetworkStatus(this)));
        String str = remoteMessage.getData().get("challenge");
        String str2 = remoteMessage.getData().get("rateLimitChallenge");
        if (str != null) {
            handleRegistrationPushChallenge(str);
        } else if (str2 != null) {
            handleRateLimitPushChallenge(str2);
        } else {
            handleReceivedNotification(ApplicationDependencies.getApplication(), remoteMessage);
        }
    }

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void onDeletedMessages() {
        Log.w(TAG, "onDeleteMessages() -- Messages may have been dropped. Doing a normal message fetch.");
        handleReceivedNotification(ApplicationDependencies.getApplication(), null);
    }

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void onNewToken(String str) {
        String str2 = TAG;
        Log.i(str2, "onNewToken()");
        if (!SignalStore.account().isRegistered()) {
            Log.i(str2, "Got a new FCM token, but the user isn't registered.");
        } else {
            ApplicationDependencies.getJobManager().add(new FcmRefreshJob());
        }
    }

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void onMessageSent(String str) {
        String str2 = TAG;
        Log.i(str2, "onMessageSent()" + str);
    }

    @Override // com.google.firebase.messaging.FirebaseMessagingService
    public void onSendError(String str, Exception exc) {
        Log.w(TAG, "onSendError()", exc);
    }

    private static void handleReceivedNotification(Context context, RemoteMessage remoteMessage) {
        boolean z;
        boolean z2 = false;
        try {
            long currentTimeMillis = System.currentTimeMillis() - SignalStore.misc().getLastFcmForegroundServiceTime();
            String str = TAG;
            Locale locale = Locale.US;
            Object[] objArr = new Object[4];
            int i = Build.VERSION.SDK_INT;
            objArr[0] = Integer.valueOf(i);
            objArr[1] = Boolean.valueOf(FeatureFlags.useFcmForegroundService());
            objArr[2] = remoteMessage != null ? Integer.valueOf(remoteMessage.getPriority()) : "n/a";
            objArr[3] = Long.valueOf(currentTimeMillis);
            Log.d(str, String.format(locale, "[handleReceivedNotification] API: %s, FeatureFlag: %s, RemoteMessagePriority: %s, TimeSinceLastRefresh: %s ms", objArr));
            if (!FeatureFlags.useFcmForegroundService() || i < 31 || remoteMessage == null || remoteMessage.getPriority() != 1 || currentTimeMillis <= FCM_FOREGROUND_INTERVAL) {
                z = FcmFetchManager.enqueue(context, false);
            } else {
                z = FcmFetchManager.enqueue(context, true);
                SignalStore.misc().setLastFcmForegroundServiceTime(System.currentTimeMillis());
            }
            z2 = z;
        } catch (Exception e) {
            Log.w(TAG, "Failed to start service.", e);
        }
        if (!z2) {
            Log.w(TAG, "Failed to start service. Falling back to legacy approach.");
            FcmFetchManager.retrieveMessages(context);
        }
    }

    private static void handleRegistrationPushChallenge(String str) {
        Log.d(TAG, "Got a registration push challenge.");
        PushChallengeRequest.postChallengeResponse(str);
    }

    private static void handleRateLimitPushChallenge(String str) {
        Log.d(TAG, "Got a rate limit push challenge.");
        ApplicationDependencies.getJobManager().add(new SubmitRateLimitPushChallengeJob(str));
    }
}
