package org.thoughtcrime.securesms.gcm;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class FcmFetchBackgroundService extends Service {
    private static final String TAG = Log.tag(FcmFetchBackgroundService.class);

    @Override // android.app.Service
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override // android.app.Service
    public int onStartCommand(Intent intent, int i, int i2) {
        return 1;
    }

    @Override // android.app.Service
    public void onDestroy() {
        Log.i(TAG, "onDestroy()");
    }
}
