package org.thoughtcrime.securesms.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.PushNotificationReceiveJob;
import org.thoughtcrime.securesms.messages.RestStrategy;
import org.thoughtcrime.securesms.util.concurrent.SerialMonoLifoExecutor;

/* compiled from: FcmFetchManager.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000bH\u0007J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u0012\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u000eH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/gcm/FcmFetchManager;", "", "()V", "EXECUTOR", "Lorg/thoughtcrime/securesms/util/concurrent/SerialMonoLifoExecutor;", "TAG", "", "kotlin.jvm.PlatformType", "activeCount", "", "startedForeground", "", "enqueue", "context", "Landroid/content/Context;", "foreground", "fetch", "", "retrieveMessages", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class FcmFetchManager {
    private static final SerialMonoLifoExecutor EXECUTOR = new SerialMonoLifoExecutor(SignalExecutors.UNBOUNDED);
    public static final FcmFetchManager INSTANCE = new FcmFetchManager();
    private static final String TAG = Log.tag(FcmFetchManager.class);
    private static volatile int activeCount;
    private static volatile boolean startedForeground;

    private FcmFetchManager() {
    }

    @JvmStatic
    public static final boolean enqueue(Context context, boolean z) {
        Intrinsics.checkNotNullParameter(context, "context");
        synchronized (INSTANCE) {
            try {
                if (z) {
                    Log.i(TAG, "Starting in the foreground.");
                    ContextCompat.startForegroundService(context, new Intent(context, FcmFetchForegroundService.class));
                    startedForeground = true;
                } else {
                    Log.i(TAG, "Starting in the background.");
                    context.startService(new Intent(context, FcmFetchBackgroundService.class));
                }
                if (EXECUTOR.enqueue(new Runnable(context) { // from class: org.thoughtcrime.securesms.gcm.FcmFetchManager$$ExternalSyntheticLambda0
                    public final /* synthetic */ Context f$0;

                    {
                        this.f$0 = r1;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        FcmFetchManager.$r8$lambda$hd4wQdmtdgFHyPcrmi4n2jP9Rxw(this.f$0);
                    }
                })) {
                    Log.i(TAG, "Already have one running and one enqueued. Ignoring.");
                } else {
                    activeCount++;
                    Log.i(TAG, "Incrementing active count to " + activeCount);
                }
                Unit unit = Unit.INSTANCE;
            } catch (IllegalStateException e) {
                Log.w(TAG, "Failed to start service!", e);
                return false;
            }
        }
        return true;
    }

    /* renamed from: enqueue$lambda-1$lambda-0 */
    public static final void m1798enqueue$lambda1$lambda0(Context context) {
        Intrinsics.checkNotNullParameter(context, "$context");
        INSTANCE.fetch(context);
    }

    private final void fetch(Context context) {
        retrieveMessages(context);
        synchronized (this) {
            activeCount--;
            if (activeCount <= 0) {
                Log.i(TAG, "No more active. Stopping.");
                context.stopService(new Intent(context, FcmFetchBackgroundService.class));
                if (startedForeground) {
                    context.startService(FcmFetchForegroundService.Companion.buildStopIntent(context));
                    startedForeground = false;
                }
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    @JvmStatic
    public static final void retrieveMessages(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (ApplicationDependencies.getBackgroundMessageRetriever().retrieveMessages(context, new RestStrategy(), new RestStrategy())) {
            Log.i(TAG, "Successfully retrieved messages.");
            return;
        }
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            String str = TAG;
            Log.w(str, "[API " + i + "] Failed to retrieve messages. Scheduling on the system JobScheduler (API " + i + ").");
            FcmJobService.schedule(context);
            return;
        }
        String str2 = TAG;
        Log.w(str2, "[API " + i + "] Failed to retrieve messages. Scheduling on JobManager (API " + i + ").");
        ApplicationDependencies.getJobManager().add(new PushNotificationReceiveJob());
    }
}
