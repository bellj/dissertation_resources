package org.thoughtcrime.securesms.gcm;

import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public final class FcmUtil {
    private static final String TAG = Log.tag(FcmUtil.class);

    /* JADX WARNING: Removed duplicated region for block: B:10:0x002f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static j$.util.Optional<java.lang.String> getToken(android.content.Context r3) {
        /*
            com.google.firebase.FirebaseApp.initializeApp(r3)
            r3 = 0
            com.google.firebase.messaging.FirebaseMessaging r0 = com.google.firebase.messaging.FirebaseMessaging.getInstance()     // Catch: InterruptedException -> 0x0020, ExecutionException -> 0x0013
            com.google.android.gms.tasks.Task r0 = r0.getToken()     // Catch: InterruptedException -> 0x0020, ExecutionException -> 0x0013
            java.lang.Object r0 = com.google.android.gms.tasks.Tasks.await(r0)     // Catch: InterruptedException -> 0x0020, ExecutionException -> 0x0013
            java.lang.String r0 = (java.lang.String) r0     // Catch: InterruptedException -> 0x0020, ExecutionException -> 0x0013
            goto L_0x0028
        L_0x0013:
            r0 = move-exception
            java.lang.String r1 = org.thoughtcrime.securesms.gcm.FcmUtil.TAG
            java.lang.Throwable r0 = r0.getCause()
            java.lang.String r2 = "Failed to get the token."
            org.signal.core.util.logging.Log.w(r1, r2, r0)
            goto L_0x0027
        L_0x0020:
            java.lang.String r0 = org.thoughtcrime.securesms.gcm.FcmUtil.TAG
            java.lang.String r1 = "Was interrupted while waiting for the token."
            org.signal.core.util.logging.Log.w(r0, r1)
        L_0x0027:
            r0 = r3
        L_0x0028:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x002f
            goto L_0x0030
        L_0x002f:
            r3 = r0
        L_0x0030:
            j$.util.Optional r3 = j$.util.Optional.ofNullable(r3)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.gcm.FcmUtil.getToken(android.content.Context):j$.util.Optional");
    }
}
