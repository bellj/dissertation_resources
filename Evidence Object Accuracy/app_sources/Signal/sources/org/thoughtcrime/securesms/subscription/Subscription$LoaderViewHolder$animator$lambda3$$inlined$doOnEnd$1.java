package org.thoughtcrime.securesms.subscription;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: Animator.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016J\u0010\u0010\u0006\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"androidx/core/animation/AnimatorKt$addListener$listener$1", "Landroid/animation/Animator$AnimatorListener;", "Landroid/animation/Animator;", "animator", "", "onAnimationRepeat", "onAnimationEnd", "onAnimationCancel", "onAnimationStart", "core-ktx_release"}, k = 1, mv = {1, 6, 0})
/* renamed from: org.thoughtcrime.securesms.subscription.Subscription$LoaderViewHolder$animator$lambda-3$$inlined$doOnEnd$1 */
/* loaded from: classes3.dex */
public final class Subscription$LoaderViewHolder$animator$lambda3$$inlined$doOnEnd$1 implements Animator.AnimatorListener {
    final /* synthetic */ View $itemView$inlined;
    final /* synthetic */ AnimatorSet $this_apply$inlined;

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationCancel(Animator animator) {
        Intrinsics.checkNotNullParameter(animator, "animator");
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationRepeat(Animator animator) {
        Intrinsics.checkNotNullParameter(animator, "animator");
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationStart(Animator animator) {
        Intrinsics.checkNotNullParameter(animator, "animator");
    }

    public Subscription$LoaderViewHolder$animator$lambda3$$inlined$doOnEnd$1(View view, AnimatorSet animatorSet) {
        this.$itemView$inlined = view;
        this.$this_apply$inlined = animatorSet;
    }

    @Override // android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        Intrinsics.checkNotNullParameter(animator, "animator");
        if (this.$itemView$inlined.isAttachedToWindow()) {
            this.$this_apply$inlined.start();
        }
    }
}
