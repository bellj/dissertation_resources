package org.thoughtcrime.securesms.subscription;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;

/* compiled from: Subscriber.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscriber;", "", "subscriberId", "Lorg/whispersystems/signalservice/api/subscriptions/SubscriberId;", "currencyCode", "", "(Lorg/whispersystems/signalservice/api/subscriptions/SubscriberId;Ljava/lang/String;)V", "getCurrencyCode", "()Ljava/lang/String;", "getSubscriberId", "()Lorg/whispersystems/signalservice/api/subscriptions/SubscriberId;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Subscriber {
    private final String currencyCode;
    private final SubscriberId subscriberId;

    public static /* synthetic */ Subscriber copy$default(Subscriber subscriber, SubscriberId subscriberId, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            subscriberId = subscriber.subscriberId;
        }
        if ((i & 2) != 0) {
            str = subscriber.currencyCode;
        }
        return subscriber.copy(subscriberId, str);
    }

    public final SubscriberId component1() {
        return this.subscriberId;
    }

    public final String component2() {
        return this.currencyCode;
    }

    public final Subscriber copy(SubscriberId subscriberId, String str) {
        Intrinsics.checkNotNullParameter(subscriberId, "subscriberId");
        Intrinsics.checkNotNullParameter(str, "currencyCode");
        return new Subscriber(subscriberId, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Subscriber)) {
            return false;
        }
        Subscriber subscriber = (Subscriber) obj;
        return Intrinsics.areEqual(this.subscriberId, subscriber.subscriberId) && Intrinsics.areEqual(this.currencyCode, subscriber.currencyCode);
    }

    public int hashCode() {
        return (this.subscriberId.hashCode() * 31) + this.currencyCode.hashCode();
    }

    public String toString() {
        return "Subscriber(subscriberId=" + this.subscriberId + ", currencyCode=" + this.currencyCode + ')';
    }

    public Subscriber(SubscriberId subscriberId, String str) {
        Intrinsics.checkNotNullParameter(subscriberId, "subscriberId");
        Intrinsics.checkNotNullParameter(str, "currencyCode");
        this.subscriberId = subscriberId;
        this.currencyCode = str;
    }

    public final SubscriberId getSubscriberId() {
        return this.subscriberId;
    }

    public final String getCurrencyCode() {
        return this.currencyCode;
    }
}
