package org.thoughtcrime.securesms.subscription;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import java.util.Currency;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: Subscription.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\t\b\b\u0018\u0000 !2\u00020\u0001:\u0005!\"#$%B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0006HÆ\u0003J\u000f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\t0\bHÆ\u0003J\t\u0010\u001a\u001a\u00020\u000bHÆ\u0003JA\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\b\b\u0002\u0010\n\u001a\u00020\u000bHÆ\u0001J\u0013\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001f\u001a\u00020\u000bHÖ\u0001J\t\u0010 \u001a\u00020\u0003HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0010R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscription;", "", ContactRepository.ID_COLUMN, "", "name", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "prices", "", "Lorg/signal/core/util/money/FiatMoney;", "level", "", "(Ljava/lang/String;Ljava/lang/String;Lorg/thoughtcrime/securesms/badges/models/Badge;Ljava/util/Set;I)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getId", "()Ljava/lang/String;", "getLevel", "()I", "getName", "getPrices", "()Ljava/util/Set;", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "toString", "Companion", "LoaderModel", "LoaderViewHolder", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class Subscription {
    public static final Companion Companion = new Companion(null);
    private final Badge badge;
    private final String id;
    private final int level;
    private final String name;
    private final Set<FiatMoney> prices;

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: org.thoughtcrime.securesms.subscription.Subscription */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ Subscription copy$default(Subscription subscription, String str, String str2, Badge badge, Set set, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = subscription.id;
        }
        if ((i2 & 2) != 0) {
            str2 = subscription.name;
        }
        if ((i2 & 4) != 0) {
            badge = subscription.badge;
        }
        if ((i2 & 8) != 0) {
            set = subscription.prices;
        }
        if ((i2 & 16) != 0) {
            i = subscription.level;
        }
        return subscription.copy(str, str2, badge, set, i);
    }

    public final String component1() {
        return this.id;
    }

    public final String component2() {
        return this.name;
    }

    public final Badge component3() {
        return this.badge;
    }

    public final Set<FiatMoney> component4() {
        return this.prices;
    }

    public final int component5() {
        return this.level;
    }

    public final Subscription copy(String str, String str2, Badge badge, Set<? extends FiatMoney> set, int i) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str2, "name");
        Intrinsics.checkNotNullParameter(badge, "badge");
        Intrinsics.checkNotNullParameter(set, "prices");
        return new Subscription(str, str2, badge, set, i);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Subscription)) {
            return false;
        }
        Subscription subscription = (Subscription) obj;
        return Intrinsics.areEqual(this.id, subscription.id) && Intrinsics.areEqual(this.name, subscription.name) && Intrinsics.areEqual(this.badge, subscription.badge) && Intrinsics.areEqual(this.prices, subscription.prices) && this.level == subscription.level;
    }

    public int hashCode() {
        return (((((((this.id.hashCode() * 31) + this.name.hashCode()) * 31) + this.badge.hashCode()) * 31) + this.prices.hashCode()) * 31) + this.level;
    }

    public String toString() {
        return "Subscription(id=" + this.id + ", name=" + this.name + ", badge=" + this.badge + ", prices=" + this.prices + ", level=" + this.level + ')';
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: java.util.Set<? extends org.signal.core.util.money.FiatMoney> */
    /* JADX WARN: Multi-variable type inference failed */
    public Subscription(String str, String str2, Badge badge, Set<? extends FiatMoney> set, int i) {
        Intrinsics.checkNotNullParameter(str, ContactRepository.ID_COLUMN);
        Intrinsics.checkNotNullParameter(str2, "name");
        Intrinsics.checkNotNullParameter(badge, "badge");
        Intrinsics.checkNotNullParameter(set, "prices");
        this.id = str;
        this.name = str2;
        this.badge = badge;
        this.prices = set;
        this.level = i;
    }

    public final String getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final Badge getBadge() {
        return this.badge;
    }

    public final Set<FiatMoney> getPrices() {
        return this.prices;
    }

    public final int getLevel() {
        return this.level;
    }

    /* compiled from: Subscription.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscription$Companion;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* renamed from: register$lambda-0 */
        public static final MappingViewHolder m3216register$lambda0(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new ViewHolder(view);
        }

        public final void register(MappingAdapter mappingAdapter) {
            Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
            mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Subscription$Companion$$ExternalSyntheticLambda0(), R.layout.subscription_preference));
            mappingAdapter.registerFactory(LoaderModel.class, new LayoutFactory(new Subscription$Companion$$ExternalSyntheticLambda1(), R.layout.subscription_preference_loader));
        }

        /* renamed from: register$lambda-1 */
        public static final MappingViewHolder m3217register$lambda1(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new LoaderViewHolder(view);
        }
    }

    /* compiled from: Subscription.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscription$LoaderModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "()V", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class LoaderModel extends PreferenceModel<LoaderModel> {
        public boolean areItemsTheSame(LoaderModel loaderModel) {
            Intrinsics.checkNotNullParameter(loaderModel, "newItem");
            return true;
        }

        public LoaderModel() {
            super(null, null, null, null, false, 31, null);
        }
    }

    /* compiled from: Subscription.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u00012\u00020\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016J\b\u0010\f\u001a\u00020\nH\u0016J\b\u0010\r\u001a\u00020\nH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscription$LoaderViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/subscription/Subscription$LoaderModel;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "animator", "Landroid/animation/Animator;", "bind", "", "model", "onAttachedToWindow", "onDetachedFromWindow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class LoaderViewHolder extends MappingViewHolder<LoaderModel> implements DefaultLifecycleObserver {
        private final Animator animator;

        public void bind(LoaderModel loaderModel) {
            Intrinsics.checkNotNullParameter(loaderModel, "model");
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
        }

        @Override // androidx.lifecycle.FullLifecycleObserver
        public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
            DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public LoaderViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", 0.8f, 0.25f);
            ofFloat.setDuration(1000L);
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "alpha", 0.25f, 0.8f);
            ofFloat2.setDuration(300L);
            animatorSet.playSequentially(ofFloat, ofFloat2);
            animatorSet.addListener(new Subscription$LoaderViewHolder$animator$lambda3$$inlined$doOnEnd$1(view, animatorSet));
            this.animator = animatorSet;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onAttachedToWindow() {
            if (this.animator.isStarted()) {
                this.animator.resume();
            } else {
                this.animator.start();
            }
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onDetachedFromWindow() {
            this.animator.pause();
        }
    }

    /* compiled from: Subscription.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\u0000\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BU\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0006\u0010\t\u001a\u00020\u0007\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011¢\u0006\u0002\u0010\u0012J\u0010\u0010\u001f\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u0000H\u0016J\u0010\u0010!\u001a\u00020\u00072\u0006\u0010 \u001a\u00020\u0000H\u0016J\u0012\u0010\"\u001a\u0004\u0018\u00010#2\u0006\u0010 \u001a\u00020\u0000H\u0016R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0015R\u0014\u0010\n\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0015R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\t\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u0015¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscription$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "activePrice", "Lorg/signal/core/util/money/FiatMoney;", "subscription", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "isSelected", "", "isActive", "willRenew", "isEnabled", "onClick", "Lkotlin/Function0;", "", "renewalTimestamp", "", "selectedCurrency", "Ljava/util/Currency;", "(Lorg/signal/core/util/money/FiatMoney;Lorg/thoughtcrime/securesms/subscription/Subscription;ZZZZLkotlin/jvm/functions/Function0;JLjava/util/Currency;)V", "getActivePrice", "()Lorg/signal/core/util/money/FiatMoney;", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getRenewalTimestamp", "()J", "getSelectedCurrency", "()Ljava/util/Currency;", "getSubscription", "()Lorg/thoughtcrime/securesms/subscription/Subscription;", "getWillRenew", "areContentsTheSame", "newItem", "areItemsTheSame", "getChangePayload", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final FiatMoney activePrice;
        private final boolean isActive;
        private final boolean isEnabled;
        private final boolean isSelected;
        private final Function0<Unit> onClick;
        private final long renewalTimestamp;
        private final Currency selectedCurrency;
        private final Subscription subscription;
        private final boolean willRenew;

        public final FiatMoney getActivePrice() {
            return this.activePrice;
        }

        public final Subscription getSubscription() {
            return this.subscription;
        }

        public final boolean isSelected() {
            return this.isSelected;
        }

        public final boolean isActive() {
            return this.isActive;
        }

        public final boolean getWillRenew() {
            return this.willRenew;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public boolean isEnabled() {
            return this.isEnabled;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        public final long getRenewalTimestamp() {
            return this.renewalTimestamp;
        }

        public final Currency getSelectedCurrency() {
            return this.selectedCurrency;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(FiatMoney fiatMoney, Subscription subscription, boolean z, boolean z2, boolean z3, boolean z4, Function0<Unit> function0, long j, Currency currency) {
            super(null, null, null, null, z4, 15, null);
            Intrinsics.checkNotNullParameter(subscription, "subscription");
            Intrinsics.checkNotNullParameter(function0, "onClick");
            Intrinsics.checkNotNullParameter(currency, "selectedCurrency");
            this.activePrice = fiatMoney;
            this.subscription = subscription;
            this.isSelected = z;
            this.isActive = z2;
            this.willRenew = z3;
            this.isEnabled = z4;
            this.onClick = function0;
            this.renewalTimestamp = j;
            this.selectedCurrency = currency;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.subscription.getId(), model.subscription.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(model.subscription, this.subscription) && model.isSelected == this.isSelected && model.isActive == this.isActive && model.renewalTimestamp == this.renewalTimestamp && model.willRenew == this.willRenew && Intrinsics.areEqual(model.selectedCurrency, this.selectedCurrency);
        }

        public Object getChangePayload(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            if (Intrinsics.areEqual(model.subscription.getBadge(), this.subscription.getBadge())) {
                return Unit.INSTANCE;
            }
            return null;
        }
    }

    /* compiled from: Subscription.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/Subscription$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/subscription/Subscription$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "check", "Landroid/widget/ImageView;", "price", "Landroid/widget/TextView;", "tagline", MultiselectForwardFragment.DIALOG_TITLE, "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes3.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badge;
        private final ImageView check;
        private final TextView price;
        private final TextView tagline;
        private final TextView title;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.badge)");
            this.badge = (BadgeImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.title);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.title)");
            this.title = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.tagline);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.tagline)");
            this.tagline = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.price);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.id.price)");
            this.price = (TextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.check);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.check)");
            this.check = (ImageView) findViewById5;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setEnabled(model.isEnabled());
            this.itemView.setOnClickListener(new Subscription$ViewHolder$$ExternalSyntheticLambda0(model));
            this.itemView.setSelected(model.isSelected());
            if (this.payload.isEmpty()) {
                this.badge.setBadge(model.getSubscription().getBadge());
                this.badge.setClickable(false);
            }
            this.title.setText(model.getSubscription().getName());
            this.tagline.setText(this.context.getString(R.string.Subscription__earn_a_s_badge, model.getSubscription().getBadge().getName()));
            Resources resources = this.context.getResources();
            FiatMoney activePrice = model.getActivePrice();
            if (activePrice == null) {
                for (FiatMoney fiatMoney : model.getSubscription().getPrices()) {
                    if (Intrinsics.areEqual(fiatMoney.getCurrency(), model.getSelectedCurrency())) {
                        activePrice = fiatMoney;
                    }
                }
                throw new NoSuchElementException("Collection contains no element matching the predicate.");
            }
            String format = FiatMoneyUtil.format(resources, activePrice, FiatMoneyUtil.formatOptions());
            Intrinsics.checkNotNullExpressionValue(format, "format(\n        context.…l.formatOptions()\n      )");
            if (model.isActive() && model.getWillRenew()) {
                this.price.setText(this.context.getString(R.string.Subscription__s_per_month_dot_renews_s, format, DateUtils.formatDateWithYear(Locale.getDefault(), model.getRenewalTimestamp())));
            } else if (model.isActive()) {
                this.price.setText(this.context.getString(R.string.Subscription__s_per_month_dot_expires_s, format, DateUtils.formatDateWithYear(Locale.getDefault(), model.getRenewalTimestamp())));
            } else {
                this.price.setText(this.context.getString(R.string.Subscription__s_per_month, format));
            }
            ViewExtensionsKt.setVisible(this.check, model.isActive());
        }

        /* renamed from: bind$lambda-0 */
        public static final void m3218bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke();
        }
    }
}
