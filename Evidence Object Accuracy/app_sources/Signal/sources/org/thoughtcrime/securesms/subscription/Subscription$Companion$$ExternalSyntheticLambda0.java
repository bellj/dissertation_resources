package org.thoughtcrime.securesms.subscription;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.subscription.Subscription;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class Subscription$Companion$$ExternalSyntheticLambda0 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return Subscription.Companion.m3216register$lambda0((View) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
