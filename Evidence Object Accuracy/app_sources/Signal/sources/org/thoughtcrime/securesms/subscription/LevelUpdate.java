package org.thoughtcrime.securesms.subscription;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: LevelUpdate.kt */
@Metadata(bv = {}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u000f\u0010\u0010J\u000e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002R:\u0010\b\u001a&\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00020\u0002 \u0007*\u0012\u0012\f\u0012\n \u0007*\u0004\u0018\u00010\u00020\u0002\u0018\u00010\u00060\u00068\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b\b\u0010\tR(\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00020\n8\u0006@\u0006X\u000e¢\u0006\u0012\n\u0004\b\u0003\u0010\u000b\u001a\u0004\b\u0003\u0010\f\"\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/LevelUpdate;", "", "", "isProcessing", "", "updateProcessingState", "Lio/reactivex/rxjava3/subjects/BehaviorSubject;", "kotlin.jvm.PlatformType", "isProcessingSubject", "Lio/reactivex/rxjava3/subjects/BehaviorSubject;", "Lio/reactivex/rxjava3/core/Observable;", "Lio/reactivex/rxjava3/core/Observable;", "()Lio/reactivex/rxjava3/core/Observable;", "setProcessing", "(Lio/reactivex/rxjava3/core/Observable;)V", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes3.dex */
public final class LevelUpdate {
    public static final LevelUpdate INSTANCE = new LevelUpdate();
    private static Observable<Boolean> isProcessing;
    private static BehaviorSubject<Boolean> isProcessingSubject;

    private LevelUpdate() {
    }

    static {
        INSTANCE = new LevelUpdate();
        BehaviorSubject<Boolean> createDefault = BehaviorSubject.createDefault(Boolean.FALSE);
        isProcessingSubject = createDefault;
        Intrinsics.checkNotNullExpressionValue(createDefault, "isProcessingSubject");
        isProcessing = createDefault;
    }

    public final Observable<Boolean> isProcessing() {
        return isProcessing;
    }

    public final void setProcessing(Observable<Boolean> observable) {
        Intrinsics.checkNotNullParameter(observable, "<set-?>");
        isProcessing = observable;
    }

    public final void updateProcessingState(boolean z) {
        isProcessingSubject.onNext(Boolean.valueOf(z));
    }
}
