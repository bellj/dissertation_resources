package org.thoughtcrime.securesms.subscription;

import android.view.View;
import org.thoughtcrime.securesms.subscription.Subscription;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes3.dex */
public final /* synthetic */ class Subscription$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Subscription.Model f$0;

    public /* synthetic */ Subscription$ViewHolder$$ExternalSyntheticLambda0(Subscription.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        Subscription.ViewHolder.m3218bind$lambda0(this.f$0, view);
    }
}
