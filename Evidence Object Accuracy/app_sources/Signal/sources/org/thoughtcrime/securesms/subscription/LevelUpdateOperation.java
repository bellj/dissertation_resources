package org.thoughtcrime.securesms.subscription;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.whispersystems.signalservice.api.subscriptions.IdempotencyKey;

/* compiled from: LevelUpdateOperation.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/subscription/LevelUpdateOperation;", "", "idempotencyKey", "Lorg/whispersystems/signalservice/api/subscriptions/IdempotencyKey;", "level", "", "(Lorg/whispersystems/signalservice/api/subscriptions/IdempotencyKey;Ljava/lang/String;)V", "getIdempotencyKey", "()Lorg/whispersystems/signalservice/api/subscriptions/IdempotencyKey;", "getLevel", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes3.dex */
public final class LevelUpdateOperation {
    private final IdempotencyKey idempotencyKey;
    private final String level;

    public static /* synthetic */ LevelUpdateOperation copy$default(LevelUpdateOperation levelUpdateOperation, IdempotencyKey idempotencyKey, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            idempotencyKey = levelUpdateOperation.idempotencyKey;
        }
        if ((i & 2) != 0) {
            str = levelUpdateOperation.level;
        }
        return levelUpdateOperation.copy(idempotencyKey, str);
    }

    public final IdempotencyKey component1() {
        return this.idempotencyKey;
    }

    public final String component2() {
        return this.level;
    }

    public final LevelUpdateOperation copy(IdempotencyKey idempotencyKey, String str) {
        Intrinsics.checkNotNullParameter(idempotencyKey, "idempotencyKey");
        Intrinsics.checkNotNullParameter(str, "level");
        return new LevelUpdateOperation(idempotencyKey, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LevelUpdateOperation)) {
            return false;
        }
        LevelUpdateOperation levelUpdateOperation = (LevelUpdateOperation) obj;
        return Intrinsics.areEqual(this.idempotencyKey, levelUpdateOperation.idempotencyKey) && Intrinsics.areEqual(this.level, levelUpdateOperation.level);
    }

    public int hashCode() {
        return (this.idempotencyKey.hashCode() * 31) + this.level.hashCode();
    }

    public String toString() {
        return "LevelUpdateOperation(idempotencyKey=" + this.idempotencyKey + ", level=" + this.level + ')';
    }

    public LevelUpdateOperation(IdempotencyKey idempotencyKey, String str) {
        Intrinsics.checkNotNullParameter(idempotencyKey, "idempotencyKey");
        Intrinsics.checkNotNullParameter(str, "level");
        this.idempotencyKey = idempotencyKey;
        this.level = str;
    }

    public final IdempotencyKey getIdempotencyKey() {
        return this.idempotencyKey;
    }

    public final String getLevel() {
        return this.level;
    }
}
