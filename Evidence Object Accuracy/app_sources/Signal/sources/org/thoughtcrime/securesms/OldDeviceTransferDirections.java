package org.thoughtcrime.securesms;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;

/* loaded from: classes.dex */
public class OldDeviceTransferDirections {
    private OldDeviceTransferDirections() {
    }

    public static NavDirections actionDirectlyToOldDeviceTransferInstructions() {
        return new ActionOnlyNavDirections(R.id.action_directly_to_oldDeviceTransferInstructions);
    }
}
