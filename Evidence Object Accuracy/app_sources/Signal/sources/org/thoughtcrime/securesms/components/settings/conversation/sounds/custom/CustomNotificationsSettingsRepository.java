package org.thoughtcrime.securesms.components.settings.conversation.sounds.custom;

import android.content.Context;
import android.net.Uri;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.concurrent.SerialExecutor;

/* compiled from: CustomNotificationsSettingsRepository.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0003J\u0010\u0010\f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0003J\u001c\u0010\r\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\t0\u000fJ\u0018\u0010\u0010\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0016\u0010\u0013\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015J\u0016\u0010\u0016\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0017\u001a\u00020\u0018J\u0018\u0010\u0019\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0016\u0010\u001a\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u0015R\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "executor", "Lorg/thoughtcrime/securesms/util/concurrent/SerialExecutor;", "createCustomNotificationChannel", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "deleteCustomNotificationChannel", "ensureCustomChannelConsistency", "onComplete", "Lkotlin/Function0;", "setCallSound", "sound", "Landroid/net/Uri;", "setCallingVibrate", "vibrateState", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "setHasCustomNotifications", "hasCustomNotifications", "", "setMessageSound", "setMessageVibrate", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomNotificationsSettingsRepository {
    private final Context context;
    private final SerialExecutor executor = new SerialExecutor(SignalExecutors.BOUNDED);

    public CustomNotificationsSettingsRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    public final void ensureCustomChannelConsistency(RecipientId recipientId, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function0, "onComplete");
        this.executor.execute(new Runnable(recipientId, function0) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ Function0 f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomNotificationsSettingsRepository.m1237ensureCustomChannelConsistency$lambda0(CustomNotificationsSettingsRepository.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: ensureCustomChannelConsistency$lambda-0 */
    public static final void m1237ensureCustomChannelConsistency$lambda0(CustomNotificationsSettingsRepository customNotificationsSettingsRepository, RecipientId recipientId, Function0 function0) {
        Intrinsics.checkNotNullParameter(customNotificationsSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(function0, "$onComplete");
        if (NotificationChannels.supported()) {
            NotificationChannels.ensureCustomChannelConsistency(customNotificationsSettingsRepository.context);
            Recipient resolved = Recipient.resolved(recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
            RecipientDatabase recipients = SignalDatabase.Companion.recipients();
            if (resolved.getNotificationChannel() != null) {
                Uri messageRingtone = NotificationChannels.getMessageRingtone(customNotificationsSettingsRepository.context, resolved);
                RecipientId id = resolved.getId();
                Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
                if (Intrinsics.areEqual(messageRingtone, Uri.EMPTY)) {
                    messageRingtone = null;
                }
                recipients.setMessageRingtone(id, messageRingtone);
                RecipientId id2 = resolved.getId();
                Intrinsics.checkNotNullExpressionValue(id2, "recipient.id");
                recipients.setMessageVibrate(id2, RecipientDatabase.VibrateState.Companion.fromBoolean(NotificationChannels.getMessageVibrate(customNotificationsSettingsRepository.context, resolved)));
            }
        }
        function0.invoke();
    }

    public final void setHasCustomNotifications(RecipientId recipientId, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.executor.execute(new Runnable(z, this, recipientId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ boolean f$0;
            public final /* synthetic */ CustomNotificationsSettingsRepository f$1;
            public final /* synthetic */ RecipientId f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomNotificationsSettingsRepository.m1240setHasCustomNotifications$lambda1(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: setHasCustomNotifications$lambda-1 */
    public static final void m1240setHasCustomNotifications$lambda1(boolean z, CustomNotificationsSettingsRepository customNotificationsSettingsRepository, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(customNotificationsSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        if (z) {
            customNotificationsSettingsRepository.createCustomNotificationChannel(recipientId);
        } else {
            customNotificationsSettingsRepository.deleteCustomNotificationChannel(recipientId);
        }
    }

    public final void setMessageVibrate(RecipientId recipientId, RecipientDatabase.VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(vibrateState, "vibrateState");
        this.executor.execute(new Runnable(vibrateState, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ RecipientDatabase.VibrateState f$1;
            public final /* synthetic */ CustomNotificationsSettingsRepository f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomNotificationsSettingsRepository.m1242setMessageVibrate$lambda2(RecipientId.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: setMessageVibrate$lambda-2 */
    public static final void m1242setMessageVibrate$lambda2(RecipientId recipientId, RecipientDatabase.VibrateState vibrateState, CustomNotificationsSettingsRepository customNotificationsSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(vibrateState, "$vibrateState");
        Intrinsics.checkNotNullParameter(customNotificationsSettingsRepository, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = resolved.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        recipients.setMessageVibrate(id, vibrateState);
        NotificationChannels.updateMessageVibrate(customNotificationsSettingsRepository.context, resolved, vibrateState);
    }

    public final void setCallingVibrate(RecipientId recipientId, RecipientDatabase.VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(vibrateState, "vibrateState");
        this.executor.execute(new Runnable(vibrateState) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ RecipientDatabase.VibrateState f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomNotificationsSettingsRepository.m1239setCallingVibrate$lambda3(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: setCallingVibrate$lambda-3 */
    public static final void m1239setCallingVibrate$lambda3(RecipientId recipientId, RecipientDatabase.VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(vibrateState, "$vibrateState");
        SignalDatabase.Companion.recipients().setCallVibrate(recipientId, vibrateState);
    }

    public final void setMessageSound(RecipientId recipientId, Uri uri) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.executor.execute(new Runnable(uri, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Uri f$1;
            public final /* synthetic */ CustomNotificationsSettingsRepository f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomNotificationsSettingsRepository.m1241setMessageSound$lambda4(RecipientId.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: setMessageSound$lambda-4 */
    public static final void m1241setMessageSound$lambda4(RecipientId recipientId, Uri uri, CustomNotificationsSettingsRepository customNotificationsSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(customNotificationsSettingsRepository, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        Uri messageNotificationSound = SignalStore.settings().getMessageNotificationSound();
        Intrinsics.checkNotNullExpressionValue(messageNotificationSound, "settings().messageNotificationSound");
        if (Intrinsics.areEqual(messageNotificationSound, uri)) {
            uri = null;
        } else if (uri == null) {
            uri = Uri.EMPTY;
        }
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = resolved.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        recipients.setMessageRingtone(id, uri);
        NotificationChannels.updateMessageRingtone(customNotificationsSettingsRepository.context, resolved, uri);
    }

    public final void setCallSound(RecipientId recipientId, Uri uri) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.executor.execute(new Runnable(uri, recipientId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ Uri f$0;
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CustomNotificationsSettingsRepository.m1238setCallSound$lambda5(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: setCallSound$lambda-5 */
    public static final void m1238setCallSound$lambda5(Uri uri, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Uri callRingtone = SignalStore.settings().getCallRingtone();
        Intrinsics.checkNotNullExpressionValue(callRingtone, "settings().callRingtone");
        if (Intrinsics.areEqual(callRingtone, uri)) {
            uri = null;
        } else if (uri == null) {
            uri = Uri.EMPTY;
        }
        SignalDatabase.Companion.recipients().setCallRingtone(recipientId, uri);
    }

    private final void createCustomNotificationChannel(RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        String createChannelFor = NotificationChannels.createChannelFor(this.context, resolved);
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = resolved.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        recipients.setNotificationChannel(id, createChannelFor);
    }

    private final void deleteCustomNotificationChannel(RecipientId recipientId) {
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = resolved.getId();
        Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
        recipients.setNotificationChannel(id, null);
        NotificationChannels.deleteChannelFor(this.context, resolved);
    }
}
