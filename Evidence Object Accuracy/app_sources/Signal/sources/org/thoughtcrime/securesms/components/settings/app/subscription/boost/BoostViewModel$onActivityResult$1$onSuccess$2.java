package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.app.Application;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: BoostViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "throwable", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class BoostViewModel$onActivityResult$1$onSuccess$2 extends Lambda implements Function1<Throwable, Unit> {
    final /* synthetic */ BoostViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BoostViewModel$onActivityResult$1$onSuccess$2(BoostViewModel boostViewModel) {
        super(1);
        this.this$0 = boostViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final BoostState m952invoke$lambda0(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.READY, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    public final void invoke(Throwable th) {
        DonationError donationError;
        Intrinsics.checkNotNullParameter(th, "throwable");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$onActivityResult$1$onSuccess$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$onActivityResult$1$onSuccess$2.m952invoke$lambda0((BoostState) obj);
            }
        });
        if (th instanceof DonationError) {
            donationError = (DonationError) th;
        } else {
            Log.w(BoostViewModel.TAG, "Failed to complete payment or redemption", th, true);
            donationError = DonationError.Companion.genericBadgeRedemptionFailure(DonationErrorSource.BOOST);
        }
        DonationError.Companion companion = DonationError.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        companion.routeDonationError(application, donationError);
    }
}
