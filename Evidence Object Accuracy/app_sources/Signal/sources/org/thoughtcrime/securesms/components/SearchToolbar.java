package org.thoughtcrime.securesms.components;

import android.animation.Animator;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;

/* loaded from: classes4.dex */
public class SearchToolbar extends LinearLayout {
    private SearchListener listener;
    private MenuItem searchItem;
    private float x;
    private float y;

    /* loaded from: classes4.dex */
    public interface SearchListener {
        void onSearchClosed();

        void onSearchTextChange(String str);
    }

    public SearchToolbar(Context context) {
        super(context);
        initialize();
    }

    public SearchToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public SearchToolbar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    private void initialize() {
        LinearLayout.inflate(getContext(), R.layout.search_toolbar, this);
        setOrientation(1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_arrow_left_24);
        toolbar.setNavigationIcon(drawable);
        toolbar.setCollapseIcon(drawable);
        toolbar.inflateMenu(R.menu.conversation_list_search);
        MenuItem findItem = toolbar.getMenu().findItem(R.id.action_filter_search);
        this.searchItem = findItem;
        SearchView searchView = (SearchView) findItem.getActionView();
        EditText editText = (EditText) searchView.findViewById(R.id.search_src_text);
        searchView.setSubmitButtonEnabled(false);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        if (editText != null) {
            editText.setHint(R.string.SearchToolbar_search);
        } else {
            searchView.setQueryHint(getResources().getString(R.string.SearchToolbar_search));
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() { // from class: org.thoughtcrime.securesms.components.SearchToolbar.1
            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextSubmit(String str) {
                if (SearchToolbar.this.listener == null) {
                    return true;
                }
                SearchToolbar.this.listener.onSearchTextChange(str);
                return true;
            }

            @Override // androidx.appcompat.widget.SearchView.OnQueryTextListener
            public boolean onQueryTextChange(String str) {
                return onQueryTextSubmit(str);
            }
        });
        this.searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() { // from class: org.thoughtcrime.securesms.components.SearchToolbar.2
            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                return true;
            }

            @Override // android.view.MenuItem.OnActionExpandListener
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                SearchToolbar.this.hide();
                return true;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.SearchToolbar$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SearchToolbar.m487$r8$lambda$_iifJ5xFeAvENDpZG_EI_bwWgs(SearchToolbar.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$initialize$0(View view) {
        hide();
    }

    public void display(float f, float f2) {
        if (getVisibility() != 0) {
            this.x = f;
            this.y = f2;
            this.searchItem.expandActionView();
            if (Build.VERSION.SDK_INT >= 21) {
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this, (int) f, (int) f2, 0.0f, (float) getWidth());
                createCircularReveal.setDuration(400);
                setVisibility(0);
                createCircularReveal.start();
                return;
            }
            setVisibility(0);
        }
    }

    public void collapse() {
        this.searchItem.collapseActionView();
    }

    public void hide() {
        if (getVisibility() == 0) {
            SearchListener searchListener = this.listener;
            if (searchListener != null) {
                searchListener.onSearchClosed();
            }
            if (Build.VERSION.SDK_INT >= 21) {
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this, (int) this.x, (int) this.y, (float) getWidth(), 0.0f);
                createCircularReveal.setDuration(400);
                createCircularReveal.addListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.SearchToolbar.3
                    @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        SearchToolbar.this.setVisibility(4);
                    }
                });
                createCircularReveal.start();
                return;
            }
            setVisibility(4);
        }
    }

    public boolean isVisible() {
        return getVisibility() == 0;
    }

    public void setListener(SearchListener searchListener) {
        this.listener = searchListener;
    }
}
