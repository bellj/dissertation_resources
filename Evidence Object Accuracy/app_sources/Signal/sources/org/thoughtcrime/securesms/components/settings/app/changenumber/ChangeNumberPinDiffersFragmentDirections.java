package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;

/* loaded from: classes4.dex */
public class ChangeNumberPinDiffersFragmentDirections {
    private ChangeNumberPinDiffersFragmentDirections() {
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }
}
