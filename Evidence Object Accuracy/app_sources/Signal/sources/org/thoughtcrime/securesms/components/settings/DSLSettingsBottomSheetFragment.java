package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EdgeEffect;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;

/* compiled from: DSLSettingsBottomSheetFragment.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b&\u0018\u00002\u00020\u0001:\u0001\"B/\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\u0014\b\u0002\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005\u0012\b\b\u0002\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H&J&\u0010\u0018\u001a\u0004\u0018\u00010\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u001a\u0010 \u001a\u00020\u00152\u0006\u0010!\u001a\u00020\u00192\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\b\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001e\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@BX.¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "layoutId", "", "layoutManagerProducer", "Lkotlin/Function1;", "Landroid/content/Context;", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "peekHeightPercentage", "", "(ILkotlin/jvm/functions/Function1;F)V", "getLayoutManagerProducer", "()Lkotlin/jvm/functions/Function1;", "getPeekHeightPercentage", "()F", "<set-?>", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "getRecyclerView", "()Landroidx/recyclerview/widget/RecyclerView;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "EdgeEffectFactory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DSLSettingsBottomSheetFragment extends FixedRoundedCornerBottomSheetDialogFragment {
    private final int layoutId;
    private final Function1<Context, RecyclerView.LayoutManager> layoutManagerProducer;
    private final float peekHeightPercentage;
    private RecyclerView recyclerView;

    public DSLSettingsBottomSheetFragment() {
        this(0, null, 0.0f, 7, null);
    }

    public abstract void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter);

    public /* synthetic */ DSLSettingsBottomSheetFragment(int i, Function1 function1, float f, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? R.layout.dsl_settings_bottom_sheet : i, (i2 & 2) != 0 ? AnonymousClass1.INSTANCE : function1, (i2 & 4) != 0 ? 1.0f : f);
    }

    public final Function1<Context, RecyclerView.LayoutManager> getLayoutManagerProducer() {
        return this.layoutManagerProducer;
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super android.content.Context, ? extends androidx.recyclerview.widget.RecyclerView$LayoutManager> */
    /* JADX WARN: Multi-variable type inference failed */
    public DSLSettingsBottomSheetFragment(int i, Function1<? super Context, ? extends RecyclerView.LayoutManager> function1, float f) {
        Intrinsics.checkNotNullParameter(function1, "layoutManagerProducer");
        this.layoutId = i;
        this.layoutManagerProducer = function1;
        this.peekHeightPercentage = f;
    }

    public final RecyclerView getRecyclerView() {
        RecyclerView recyclerView = this.recyclerView;
        if (recyclerView != null) {
            return recyclerView;
        }
        Intrinsics.throwUninitializedPropertyAccessException("recyclerView");
        return null;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(this.layoutId, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.recycler)");
        this.recyclerView = (RecyclerView) findViewById;
        getRecyclerView().setEdgeEffectFactory(new EdgeEffectFactory());
        DSLSettingsAdapter dSLSettingsAdapter = new DSLSettingsAdapter();
        RecyclerView recyclerView = getRecyclerView();
        Function1<Context, RecyclerView.LayoutManager> function1 = this.layoutManagerProducer;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        recyclerView.setLayoutManager(function1.invoke(requireContext));
        getRecyclerView().setAdapter(dSLSettingsAdapter);
        getRecyclerView().setOverScrollMode(1);
        bindAdapter(dSLSettingsAdapter);
    }

    /* compiled from: DSLSettingsBottomSheetFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0014¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment$EdgeEffectFactory;", "Landroidx/recyclerview/widget/RecyclerView$EdgeEffectFactory;", "()V", "createEdgeEffect", "Landroid/widget/EdgeEffect;", "view", "Landroidx/recyclerview/widget/RecyclerView;", "direction", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class EdgeEffectFactory extends RecyclerView.EdgeEffectFactory {
        @Override // androidx.recyclerview.widget.RecyclerView.EdgeEffectFactory
        public EdgeEffect createEdgeEffect(RecyclerView recyclerView, int i) {
            Intrinsics.checkNotNullParameter(recyclerView, "view");
            EdgeEffect createEdgeEffect = super.createEdgeEffect(recyclerView, i);
            Intrinsics.checkNotNullExpressionValue(createEdgeEffect, "super.createEdgeEffect(view, direction)");
            if (Build.VERSION.SDK_INT > 21) {
                createEdgeEffect.setColor(ContextCompat.getColor(recyclerView.getContext(), R.color.settings_ripple_color));
            }
            return createEdgeEffect;
        }
    }
}
