package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.Context;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.CdsDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.CertificateType;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.pin.KbsRepository;
import org.thoughtcrime.securesms.pin.KeyBackupSystemWrongPinException;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.whispersystems.signalservice.api.KbsPinData;
import org.whispersystems.signalservice.api.KeyBackupSystemNoDataException;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;
import org.whispersystems.signalservice.internal.push.WhoAmIResponse;

/* compiled from: ChangeNumberRepository.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001e\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0007J\"\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\b2\u0006\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u000bJ2\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00130\u000f0\b2\u0006\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u000b2\u0006\u0010\u0014\u001a\u00020\u000b2\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "accountManager", "Lorg/whispersystems/signalservice/api/SignalServiceAccountManager;", "changeLocalNumber", "Lio/reactivex/rxjava3/core/Single;", "", CdsDatabase.E164, "", RecipientDatabase.PNI_COLUMN, "Lorg/whispersystems/signalservice/api/push/PNI;", "changeNumber", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "code", "newE164", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse;", "pin", "tokenData", "Lorg/thoughtcrime/securesms/pin/TokenData;", "rotateCertificates", "whoAmI", "Lorg/whispersystems/signalservice/internal/push/WhoAmIResponse;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberRepository {
    private final SignalServiceAccountManager accountManager;
    private final Context context;

    /* compiled from: ChangeNumberRepository.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CertificateType.values().length];
            iArr[CertificateType.UUID_AND_E164.ordinal()] = 1;
            iArr[CertificateType.UUID_ONLY.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public ChangeNumberRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
        SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
        Intrinsics.checkNotNullExpressionValue(signalServiceAccountManager, "getSignalServiceAccountManager()");
        this.accountManager = signalServiceAccountManager;
    }

    /* renamed from: changeNumber$lambda-0 */
    public static final ServiceResponse m612changeNumber$lambda0(ChangeNumberRepository changeNumberRepository, String str, String str2) {
        Intrinsics.checkNotNullParameter(changeNumberRepository, "this$0");
        Intrinsics.checkNotNullParameter(str, "$code");
        Intrinsics.checkNotNullParameter(str2, "$newE164");
        return changeNumberRepository.accountManager.changeNumber(str, str2, null);
    }

    public final Single<ServiceResponse<VerifyAccountResponse>> changeNumber(String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "code");
        Intrinsics.checkNotNullParameter(str2, "newE164");
        Single<ServiceResponse<VerifyAccountResponse>> subscribeOn = Single.fromCallable(new Callable(str, str2) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ChangeNumberRepository.m612changeNumber$lambda0(ChangeNumberRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { accountMa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    public final Single<ServiceResponse<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse>> changeNumber(String str, String str2, String str3, TokenData tokenData) {
        Intrinsics.checkNotNullParameter(str, "code");
        Intrinsics.checkNotNullParameter(str2, "newE164");
        Intrinsics.checkNotNullParameter(str3, "pin");
        Intrinsics.checkNotNullParameter(tokenData, "tokenData");
        Single<ServiceResponse<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse>> subscribeOn = Single.fromCallable(new Callable(str3, tokenData, this, str, str2) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$0;
            public final /* synthetic */ TokenData f$1;
            public final /* synthetic */ ChangeNumberRepository f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String f$4;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ChangeNumberRepository.m613changeNumber$lambda1(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      try…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: changeNumber$lambda-1 */
    public static final ServiceResponse m613changeNumber$lambda1(String str, TokenData tokenData, ChangeNumberRepository changeNumberRepository, String str2, String str3) {
        Intrinsics.checkNotNullParameter(str, "$pin");
        Intrinsics.checkNotNullParameter(tokenData, "$tokenData");
        Intrinsics.checkNotNullParameter(changeNumberRepository, "this$0");
        Intrinsics.checkNotNullParameter(str2, "$code");
        Intrinsics.checkNotNullParameter(str3, "$newE164");
        try {
            KbsPinData restoreMasterKey = KbsRepository.restoreMasterKey(str, tokenData.getEnclave(), tokenData.getBasicAuth(), tokenData.getTokenResponse());
            Intrinsics.checkNotNull(restoreMasterKey);
            String deriveRegistrationLock = restoreMasterKey.getMasterKey().deriveRegistrationLock();
            Intrinsics.checkNotNullExpressionValue(deriveRegistrationLock, "kbsData.masterKey.deriveRegistrationLock()");
            ServiceResponse<VerifyAccountResponse> changeNumber = changeNumberRepository.accountManager.changeNumber(str2, str3, deriveRegistrationLock);
            Intrinsics.checkNotNullExpressionValue(changeNumber, "accountManager.changeNum…ewE164, registrationLock)");
            return VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse.Companion.from(changeNumber, restoreMasterKey);
        } catch (IOException e) {
            return ServiceResponse.forExecutionError(e);
        } catch (KeyBackupSystemWrongPinException e2) {
            return ServiceResponse.forExecutionError(e2);
        } catch (KeyBackupSystemNoDataException e3) {
            return ServiceResponse.forExecutionError(e3);
        }
    }

    /* renamed from: whoAmI$lambda-2 */
    public static final WhoAmIResponse m615whoAmI$lambda2() {
        return ApplicationDependencies.getSignalServiceAccountManager().getWhoAmI();
    }

    public final Single<WhoAmIResponse> whoAmI() {
        Single<WhoAmIResponse> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberRepository$$ExternalSyntheticLambda3
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ChangeNumberRepository.m615whoAmI$lambda2();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { Applicati…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    public final Single<Unit> changeLocalNumber(String str, PNI pni) {
        Intrinsics.checkNotNullParameter(str, CdsDatabase.E164);
        Intrinsics.checkNotNullParameter(pni, RecipientDatabase.PNI_COLUMN);
        byte[] storageServiceId = Recipient.self().getStorageServiceId();
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        companion.recipients().updateSelfPhone(str);
        if (MessageDigest.isEqual(storageServiceId, Recipient.self().getStorageServiceId())) {
            Log.w(ChangeNumberRepositoryKt.TAG, "Self storage id was not rotated, attempting to rotate again");
            RecipientDatabase recipients = companion.recipients();
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            recipients.rotateStorageId(id);
            Recipient.self().live().refresh();
            StorageSyncHelper.scheduleSyncForDataChange();
            if (MessageDigest.isEqual(storageServiceId, Recipient.self().getStorageServiceId())) {
                Log.w(ChangeNumberRepositoryKt.TAG, "Second attempt also failed to rotate storage id");
            }
        }
        RecipientDatabase recipients2 = companion.recipients();
        RecipientId id2 = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id2, "self().id");
        recipients2.setPni(id2, pni);
        SignalStore.account().setE164(str);
        SignalStore.account().setPni(pni);
        ApplicationDependencies.closeConnections();
        ApplicationDependencies.getIncomingMessageObserver();
        return rotateCertificates();
    }

    private final Single<Unit> rotateCertificates() {
        Collection<CertificateType> allCertificateTypes = SignalStore.phoneNumberPrivacy().getAllCertificateTypes();
        String str = ChangeNumberRepositoryKt.TAG;
        Log.i(str, "Rotating these certificates " + allCertificateTypes);
        Single<Unit> subscribeOn = Single.fromCallable(new Callable(allCertificateTypes, this) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Collection f$0;
            public final /* synthetic */ ChangeNumberRepository f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ChangeNumberRepository.m614rotateCertificates$lambda3(this.f$0, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      for…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: rotateCertificates$lambda-3 */
    public static final Unit m614rotateCertificates$lambda3(Collection collection, ChangeNumberRepository changeNumberRepository) {
        int i;
        byte[] bArr;
        Intrinsics.checkNotNullParameter(changeNumberRepository, "this$0");
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            CertificateType certificateType = (CertificateType) it.next();
            if (certificateType == null) {
                i = -1;
            } else {
                i = WhenMappings.$EnumSwitchMapping$0[certificateType.ordinal()];
            }
            if (i == 1) {
                bArr = changeNumberRepository.accountManager.getSenderCertificate();
            } else if (i == 2) {
                bArr = changeNumberRepository.accountManager.getSenderCertificateForPhoneNumberPrivacy();
            } else {
                throw new AssertionError();
            }
            String str = ChangeNumberRepositoryKt.TAG;
            Log.i(str, "Successfully got " + certificateType + " certificate");
            SignalStore.certificateValues().setUnidentifiedAccessCertificate(certificateType, bArr);
        }
        return Unit.INSTANCE;
    }
}
