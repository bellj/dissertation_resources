package org.thoughtcrime.securesms.components.webrtc;

import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.SetUtil;
import org.thoughtcrime.securesms.events.CallParticipant;

/* loaded from: classes4.dex */
public final class CallParticipantListUpdate {
    private final Set<Wrapper> added;
    private final Set<Wrapper> removed;

    CallParticipantListUpdate(Set<Wrapper> set, Set<Wrapper> set2) {
        this.added = set;
        this.removed = set2;
    }

    public Set<Wrapper> getAdded() {
        return this.added;
    }

    public Set<Wrapper> getRemoved() {
        return this.removed;
    }

    public boolean hasNoChanges() {
        return this.added.isEmpty() && this.removed.isEmpty();
    }

    public boolean hasSingleChange() {
        return this.added.size() + this.removed.size() == 1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || CallParticipantListUpdate.class != obj.getClass()) {
            return false;
        }
        CallParticipantListUpdate callParticipantListUpdate = (CallParticipantListUpdate) obj;
        if (!this.added.equals(callParticipantListUpdate.added) || !this.removed.equals(callParticipantListUpdate.removed)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Objects.hash(this.added, this.removed);
    }

    public static CallParticipantListUpdate computeDeltaUpdate(List<CallParticipant> list, List<CallParticipant> list2) {
        Set set = (Set) Stream.of(list).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantListUpdate$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return CallParticipantListUpdate.lambda$computeDeltaUpdate$0((CallParticipant) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantListUpdate$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CallParticipantListUpdate.createWrapper((CallParticipant) obj);
            }
        }).collect(Collectors.toSet());
        Set set2 = (Set) Stream.of(list2).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantListUpdate$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return CallParticipantListUpdate.lambda$computeDeltaUpdate$1((CallParticipant) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantListUpdate$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CallParticipantListUpdate.createWrapper((CallParticipant) obj);
            }
        }).collect(Collectors.toSet());
        return new CallParticipantListUpdate(SetUtil.difference(set2, set), SetUtil.difference(set, set2));
    }

    public static /* synthetic */ boolean lambda$computeDeltaUpdate$0(CallParticipant callParticipant) {
        return callParticipant.getCallParticipantId().getDemuxId() != -1;
    }

    public static /* synthetic */ boolean lambda$computeDeltaUpdate$1(CallParticipant callParticipant) {
        return callParticipant.getCallParticipantId().getDemuxId() != -1;
    }

    public static Wrapper createWrapper(CallParticipant callParticipant) {
        return new Wrapper(callParticipant);
    }

    /* loaded from: classes4.dex */
    public static final class Wrapper {
        private final CallParticipant callParticipant;

        private Wrapper(CallParticipant callParticipant) {
            this.callParticipant = callParticipant;
        }

        public CallParticipant getCallParticipant() {
            return this.callParticipant;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || Wrapper.class != obj.getClass()) {
                return false;
            }
            return this.callParticipant.getCallParticipantId().equals(((Wrapper) obj).callParticipant.getCallParticipantId());
        }

        public int hashCode() {
            return Objects.hash(this.callParticipant.getCallParticipantId());
        }
    }
}
