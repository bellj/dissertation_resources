package org.thoughtcrime.securesms.components.settings.app.internal;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceDataStore;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.jobs.StoryOnboardingDownloadJob;
import org.thoughtcrime.securesms.keyvalue.InternalValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: InternalSettingsViewModel.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001,B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u000e\u001a\u00020\u000fJ\b\u0010\n\u001a\u00020\tH\u0002J\u0006\u0010\u0010\u001a\u00020\u000fJ\b\u0010\u0011\u001a\u00020\u000fH\u0002J\u000e\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u001c\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\u001e\u001a\u00020\u001fJ\u000e\u0010 \u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\"J\u000e\u0010#\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u0010\u0010$\u001a\u00020\u000f2\b\u0010%\u001a\u0004\u0018\u00010&J\u000e\u0010'\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010(\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010)\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010*\u001a\u00020\u000f2\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010+\u001a\u00020\u000fR\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsRepository;)V", "preferenceDataStore", "Landroidx/preference/PreferenceDataStore;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "addSampleReleaseNote", "", "onClearOnboardingState", "refresh", "setAllowCensorshipSetting", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "setDelayResends", "setDisableAutoMigrationInitiation", "setDisableAutoMigrationNotification", "setDisableStorageService", "setGv2DoNotCreateGv2Groups", "setGv2ForceInvites", "setGv2IgnoreP2PChanges", "setGv2IgnoreServerChanges", "setInternalCallingAudioProcessingMethod", "method", "Lorg/signal/ringrtc/CallManager$AudioProcessingMethod;", "setInternalCallingBandwidthMode", "bandwidthMode", "Lorg/signal/ringrtc/CallManager$BandwidthMode;", "setInternalCallingDisableTelecom", "setInternalGroupCallingServer", "server", "", "setRemoveSenderKeyMinimum", "setSeeMoreUserDetails", "setShakeToReport", "setUseBuiltInEmoji", "toggleStories", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternalSettingsViewModel extends ViewModel {
    private final PreferenceDataStore preferenceDataStore;
    private final InternalSettingsRepository repository;
    private final LiveData<InternalSettingsState> state;
    private final Store<InternalSettingsState> store;

    public InternalSettingsViewModel(InternalSettingsRepository internalSettingsRepository) {
        Intrinsics.checkNotNullParameter(internalSettingsRepository, "repository");
        this.repository = internalSettingsRepository;
        PreferenceDataStore preferenceDataStore = SignalStore.getPreferenceDataStore();
        Intrinsics.checkNotNullExpressionValue(preferenceDataStore, "getPreferenceDataStore()");
        this.preferenceDataStore = preferenceDataStore;
        Store<InternalSettingsState> store = new Store<>(getState());
        this.store = store;
        internalSettingsRepository.getEmojiVersionInfo(new Function1<EmojiFiles.Version, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsViewModel.1
            final /* synthetic */ InternalSettingsViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(EmojiFiles.Version version) {
                invoke(version);
                return Unit.INSTANCE;
            }

            /* renamed from: invoke$lambda-0 */
            public static final InternalSettingsState m671invoke$lambda0(EmojiFiles.Version version, InternalSettingsState internalSettingsState) {
                Intrinsics.checkNotNullExpressionValue(internalSettingsState, "it");
                return internalSettingsState.copy((r38 & 1) != 0 ? internalSettingsState.seeMoreUserDetails : false, (r38 & 2) != 0 ? internalSettingsState.shakeToReport : false, (r38 & 4) != 0 ? internalSettingsState.gv2doNotCreateGv2Groups : false, (r38 & 8) != 0 ? internalSettingsState.gv2forceInvites : false, (r38 & 16) != 0 ? internalSettingsState.gv2ignoreServerChanges : false, (r38 & 32) != 0 ? internalSettingsState.gv2ignoreP2PChanges : false, (r38 & 64) != 0 ? internalSettingsState.disableAutoMigrationInitiation : false, (r38 & 128) != 0 ? internalSettingsState.disableAutoMigrationNotification : false, (r38 & 256) != 0 ? internalSettingsState.allowCensorshipSetting : false, (r38 & 512) != 0 ? internalSettingsState.callingServer : null, (r38 & 1024) != 0 ? internalSettingsState.callingAudioProcessingMethod : null, (r38 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? internalSettingsState.callingBandwidthMode : null, (r38 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? internalSettingsState.callingDisableTelecom : false, (r38 & 8192) != 0 ? internalSettingsState.useBuiltInEmojiSet : false, (r38 & 16384) != 0 ? internalSettingsState.emojiVersion : version, (r38 & 32768) != 0 ? internalSettingsState.removeSenderKeyMinimium : false, (r38 & 65536) != 0 ? internalSettingsState.delayResends : false, (r38 & 131072) != 0 ? internalSettingsState.disableStorageService : false, (r38 & 262144) != 0 ? internalSettingsState.disableStories : false, (r38 & 524288) != 0 ? internalSettingsState.canClearOnboardingState : false);
            }

            public final void invoke(EmojiFiles.Version version) {
                this.this$0.store.update(new InternalSettingsViewModel$1$$ExternalSyntheticLambda0(version));
            }
        });
        LiveData<InternalSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    /* renamed from: getState */
    public final LiveData<InternalSettingsState> m670getState() {
        return this.state;
    }

    public final void setSeeMoreUserDetails(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.RECIPIENT_DETAILS, z);
        refresh();
    }

    public final void setShakeToReport(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.SHAKE_TO_REPORT, z);
        refresh();
    }

    public final void setDisableStorageService(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.DISABLE_STORAGE_SERVICE, z);
        refresh();
    }

    public final void setGv2DoNotCreateGv2Groups(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.GV2_DO_NOT_CREATE_GV2, z);
        refresh();
    }

    public final void setGv2ForceInvites(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.GV2_FORCE_INVITES, z);
        refresh();
    }

    public final void setGv2IgnoreServerChanges(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.GV2_IGNORE_SERVER_CHANGES, z);
        refresh();
    }

    public final void setGv2IgnoreP2PChanges(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.GV2_IGNORE_P2P_CHANGES, z);
        refresh();
    }

    public final void setDisableAutoMigrationInitiation(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.GV2_DISABLE_AUTOMIGRATE_INITIATION, z);
        refresh();
    }

    public final void setDisableAutoMigrationNotification(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.GV2_DISABLE_AUTOMIGRATE_NOTIFICATION, z);
        refresh();
    }

    public final void setAllowCensorshipSetting(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.ALLOW_CENSORSHIP_SETTING, z);
        refresh();
    }

    public final void setUseBuiltInEmoji(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.FORCE_BUILT_IN_EMOJI, z);
        refresh();
    }

    public final void setRemoveSenderKeyMinimum(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.REMOVE_SENDER_KEY_MINIMUM, z);
        refresh();
    }

    public final void setDelayResends(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.DELAY_RESENDS, z);
        refresh();
    }

    public final void setInternalGroupCallingServer(String str) {
        this.preferenceDataStore.putString(InternalValues.CALLING_SERVER, str);
        refresh();
    }

    public final void setInternalCallingAudioProcessingMethod(CallManager.AudioProcessingMethod audioProcessingMethod) {
        Intrinsics.checkNotNullParameter(audioProcessingMethod, "method");
        this.preferenceDataStore.putInt(InternalValues.CALLING_AUDIO_PROCESSING_METHOD, audioProcessingMethod.ordinal());
        refresh();
    }

    public final void setInternalCallingBandwidthMode(CallManager.BandwidthMode bandwidthMode) {
        Intrinsics.checkNotNullParameter(bandwidthMode, "bandwidthMode");
        this.preferenceDataStore.putInt(InternalValues.CALLING_BANDWIDTH_MODE, bandwidthMode.ordinal());
        refresh();
    }

    public final void setInternalCallingDisableTelecom(boolean z) {
        this.preferenceDataStore.putBoolean(InternalValues.CALLING_DISABLE_TELECOM, z);
        refresh();
    }

    public final void toggleStories() {
        boolean z = !SignalStore.storyValues().isFeatureDisabled();
        SignalStore.storyValues().setFeatureDisabled(z);
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return InternalSettingsViewModel.m669toggleStories$lambda0(InternalSettingsViewModel.this, this.f$1, (InternalSettingsState) obj);
            }
        });
    }

    /* renamed from: toggleStories$lambda-0 */
    public static final InternalSettingsState m669toggleStories$lambda0(InternalSettingsViewModel internalSettingsViewModel, boolean z, InternalSettingsState internalSettingsState) {
        Intrinsics.checkNotNullParameter(internalSettingsViewModel, "this$0");
        return r0.copy((r38 & 1) != 0 ? r0.seeMoreUserDetails : false, (r38 & 2) != 0 ? r0.shakeToReport : false, (r38 & 4) != 0 ? r0.gv2doNotCreateGv2Groups : false, (r38 & 8) != 0 ? r0.gv2forceInvites : false, (r38 & 16) != 0 ? r0.gv2ignoreServerChanges : false, (r38 & 32) != 0 ? r0.gv2ignoreP2PChanges : false, (r38 & 64) != 0 ? r0.disableAutoMigrationInitiation : false, (r38 & 128) != 0 ? r0.disableAutoMigrationNotification : false, (r38 & 256) != 0 ? r0.allowCensorshipSetting : false, (r38 & 512) != 0 ? r0.callingServer : null, (r38 & 1024) != 0 ? r0.callingAudioProcessingMethod : null, (r38 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r0.callingBandwidthMode : null, (r38 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r0.callingDisableTelecom : false, (r38 & 8192) != 0 ? r0.useBuiltInEmojiSet : false, (r38 & 16384) != 0 ? r0.emojiVersion : null, (r38 & 32768) != 0 ? r0.removeSenderKeyMinimium : false, (r38 & 65536) != 0 ? r0.delayResends : false, (r38 & 131072) != 0 ? r0.disableStorageService : false, (r38 & 262144) != 0 ? r0.disableStories : z, (r38 & 524288) != 0 ? internalSettingsViewModel.getState().canClearOnboardingState : false);
    }

    public final void addSampleReleaseNote() {
        this.repository.addSampleReleaseNote();
    }

    private final void refresh() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return InternalSettingsViewModel.m668refresh$lambda1(InternalSettingsViewModel.this, (InternalSettingsState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-1 */
    public static final InternalSettingsState m668refresh$lambda1(InternalSettingsViewModel internalSettingsViewModel, InternalSettingsState internalSettingsState) {
        Intrinsics.checkNotNullParameter(internalSettingsViewModel, "this$0");
        return r1.copy((r38 & 1) != 0 ? r1.seeMoreUserDetails : false, (r38 & 2) != 0 ? r1.shakeToReport : false, (r38 & 4) != 0 ? r1.gv2doNotCreateGv2Groups : false, (r38 & 8) != 0 ? r1.gv2forceInvites : false, (r38 & 16) != 0 ? r1.gv2ignoreServerChanges : false, (r38 & 32) != 0 ? r1.gv2ignoreP2PChanges : false, (r38 & 64) != 0 ? r1.disableAutoMigrationInitiation : false, (r38 & 128) != 0 ? r1.disableAutoMigrationNotification : false, (r38 & 256) != 0 ? r1.allowCensorshipSetting : false, (r38 & 512) != 0 ? r1.callingServer : null, (r38 & 1024) != 0 ? r1.callingAudioProcessingMethod : null, (r38 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r1.callingBandwidthMode : null, (r38 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r1.callingDisableTelecom : false, (r38 & 8192) != 0 ? r1.useBuiltInEmojiSet : false, (r38 & 16384) != 0 ? r1.emojiVersion : internalSettingsState.getEmojiVersion(), (r38 & 32768) != 0 ? r1.removeSenderKeyMinimium : false, (r38 & 65536) != 0 ? r1.delayResends : false, (r38 & 131072) != 0 ? r1.disableStorageService : false, (r38 & 262144) != 0 ? r1.disableStories : false, (r38 & 524288) != 0 ? internalSettingsViewModel.getState().canClearOnboardingState : false);
    }

    private final InternalSettingsState getState() {
        boolean recipientDetails = SignalStore.internalValues().recipientDetails();
        boolean shakeToReport = SignalStore.internalValues().shakeToReport();
        boolean gv2DoNotCreateGv2Groups = SignalStore.internalValues().gv2DoNotCreateGv2Groups();
        boolean gv2ForceInvites = SignalStore.internalValues().gv2ForceInvites();
        boolean gv2IgnoreServerChanges = SignalStore.internalValues().gv2IgnoreServerChanges();
        boolean gv2IgnoreP2PChanges = SignalStore.internalValues().gv2IgnoreP2PChanges();
        boolean disableGv1AutoMigrateInitiation = SignalStore.internalValues().disableGv1AutoMigrateInitiation();
        boolean disableGv1AutoMigrateNotification = SignalStore.internalValues().disableGv1AutoMigrateNotification();
        boolean allowChangingCensorshipSetting = SignalStore.internalValues().allowChangingCensorshipSetting();
        String groupCallingServer = SignalStore.internalValues().groupCallingServer();
        Intrinsics.checkNotNullExpressionValue(groupCallingServer, "internalValues().groupCallingServer()");
        CallManager.AudioProcessingMethod callingAudioProcessingMethod = SignalStore.internalValues().callingAudioProcessingMethod();
        Intrinsics.checkNotNullExpressionValue(callingAudioProcessingMethod, "internalValues().callingAudioProcessingMethod()");
        CallManager.BandwidthMode callingBandwidthMode = SignalStore.internalValues().callingBandwidthMode();
        Intrinsics.checkNotNullExpressionValue(callingBandwidthMode, "internalValues().callingBandwidthMode()");
        return new InternalSettingsState(recipientDetails, shakeToReport, gv2DoNotCreateGv2Groups, gv2ForceInvites, gv2IgnoreServerChanges, gv2IgnoreP2PChanges, disableGv1AutoMigrateInitiation, disableGv1AutoMigrateNotification, allowChangingCensorshipSetting, groupCallingServer, callingAudioProcessingMethod, callingBandwidthMode, SignalStore.internalValues().callingDisableTelecom(), SignalStore.internalValues().forceBuiltInEmoji(), null, SignalStore.internalValues().removeSenderKeyMinimum(), SignalStore.internalValues().delayResends(), SignalStore.internalValues().storageServiceDisabled(), SignalStore.storyValues().isFeatureDisabled(), SignalStore.storyValues().getHasDownloadedOnboardingStory() && Stories.isFeatureEnabled());
    }

    public final void onClearOnboardingState() {
        SignalStore.storyValues().setHasDownloadedOnboardingStory(false);
        SignalStore.storyValues().setUserHasSeenOnboardingStory(false);
        refresh();
        StoryOnboardingDownloadJob.Companion.enqueueIfNeeded();
    }

    /* compiled from: InternalSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final InternalSettingsRepository repository;

        public Factory(InternalSettingsRepository internalSettingsRepository) {
            Intrinsics.checkNotNullParameter(internalSettingsRepository, "repository");
            this.repository = internalSettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new InternalSettingsViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
