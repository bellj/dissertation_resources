package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import org.thoughtcrime.securesms.components.viewpager.HackyViewPager;

/* loaded from: classes4.dex */
public class ControllableViewPager extends HackyViewPager {
    public ControllableViewPager(Context context) {
        super(context);
    }

    public ControllableViewPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // org.thoughtcrime.securesms.components.viewpager.HackyViewPager, androidx.viewpager.widget.ViewPager, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && super.onTouchEvent(motionEvent);
    }

    @Override // org.thoughtcrime.securesms.components.viewpager.HackyViewPager, androidx.viewpager.widget.ViewPager, android.view.ViewGroup
    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return isEnabled() && super.onInterceptTouchEvent(motionEvent);
    }
}
