package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B'\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0016\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\f¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/LearnMoreTextPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getTitle", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LearnMoreTextPreference extends PreferenceModel<LearnMoreTextPreference> {
    private final Function0<Unit> onClick;
    private final DSLSettingsText summary;
    private final DSLSettingsText title;

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getSummary() {
        return this.summary;
    }

    public final Function0<Unit> getOnClick() {
        return this.onClick;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public LearnMoreTextPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, Function0<Unit> function0) {
        super(null, null, null, null, false, 31, null);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.title = dSLSettingsText;
        this.summary = dSLSettingsText2;
        this.onClick = function0;
    }
}
