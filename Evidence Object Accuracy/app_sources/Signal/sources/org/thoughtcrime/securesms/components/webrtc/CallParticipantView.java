package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewKt;
import androidx.core.widget.ImageViewCompat;
import androidx.transition.TransitionManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import j$.util.function.Consumer;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.webrtc.EglBase;
import org.webrtc.RendererCommon;

/* loaded from: classes4.dex */
public class CallParticipantView extends ConstraintLayout {
    private static final long DELAY_SHOWING_MISSING_MEDIA_KEYS = TimeUnit.SECONDS.toMillis(5);
    private static final FallbackPhotoProvider FALLBACK_PHOTO_PROVIDER = new FallbackPhotoProvider();
    private static final int LARGE_AVATAR = ViewUtil.dpToPx(R.styleable.AppCompatTheme_tooltipForegroundColor);
    private static final int SMALL_AVATAR = ViewUtil.dpToPx(96);
    private AudioIndicatorView audioIndicator;
    private AvatarImageView avatar;
    private AppCompatImageView backgroundAvatar;
    private BadgeImageView badge;
    private ContactPhoto contactPhoto;
    private AppCompatImageView infoIcon;
    private EmojiTextView infoMessage;
    private boolean infoMode;
    private Button infoMoreInfo;
    private View infoOverlay;
    private Runnable missingMediaKeysUpdater;
    private ImageView pipAvatar;
    private BadgeImageView pipBadge;
    private RecipientId recipientId;
    private TextureViewRenderer renderer;
    private View rendererFrame;

    public CallParticipantView(Context context) {
        super(context);
        onFinishInflate();
    }

    public CallParticipantView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CallParticipantView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.backgroundAvatar = (AppCompatImageView) findViewById(R.id.call_participant_background_avatar);
        this.avatar = (AvatarImageView) findViewById(R.id.call_participant_item_avatar);
        this.pipAvatar = (ImageView) findViewById(R.id.call_participant_item_pip_avatar);
        this.rendererFrame = findViewById(R.id.call_participant_renderer_frame);
        this.renderer = (TextureViewRenderer) findViewById(R.id.call_participant_renderer);
        this.audioIndicator = (AudioIndicatorView) findViewById(R.id.call_participant_audio_indicator);
        this.infoOverlay = findViewById(R.id.call_participant_info_overlay);
        this.infoIcon = (AppCompatImageView) findViewById(R.id.call_participant_info_icon);
        this.infoMessage = (EmojiTextView) findViewById(R.id.call_participant_info_message);
        this.infoMoreInfo = (Button) findViewById(R.id.call_participant_info_more_info);
        this.badge = (BadgeImageView) findViewById(R.id.call_participant_item_badge);
        this.pipBadge = (BadgeImageView) findViewById(R.id.call_participant_item_pip_badge);
        this.avatar.setFallbackPhotoProvider(FALLBACK_PHOTO_PROVIDER);
        useLargeAvatar();
    }

    public void setMirror(boolean z) {
        this.renderer.setMirror(z);
    }

    public void setScalingType(RendererCommon.ScalingType scalingType) {
        this.renderer.setScalingType(scalingType);
    }

    public void setScalingType(RendererCommon.ScalingType scalingType, RendererCommon.ScalingType scalingType2) {
        this.renderer.setScalingType(scalingType, scalingType2);
    }

    public void setCallParticipant(CallParticipant callParticipant) {
        RecipientId recipientId = this.recipientId;
        boolean z = true;
        boolean z2 = recipientId == null || !recipientId.equals(callParticipant.getRecipient().getId());
        this.recipientId = callParticipant.getRecipient().getId();
        boolean z3 = callParticipant.getRecipient().isBlocked() || isMissingMediaKeys(callParticipant);
        this.infoMode = z3;
        int i = 8;
        if (z3) {
            this.rendererFrame.setVisibility(8);
            this.renderer.setVisibility(8);
            this.renderer.attachBroadcastVideoSink(null);
            this.audioIndicator.setVisibility(8);
            this.avatar.setVisibility(8);
            this.badge.setVisibility(8);
            this.pipAvatar.setVisibility(8);
            this.pipBadge.setVisibility(8);
            this.infoOverlay.setVisibility(0);
            ImageViewCompat.setImageTintList(this.infoIcon, ContextCompat.getColorStateList(getContext(), R.color.core_white));
            if (callParticipant.getRecipient().isBlocked()) {
                this.infoIcon.setImageResource(R.drawable.ic_block_tinted_24);
                this.infoMessage.setText(getContext().getString(R.string.CallParticipantView__s_is_blocked, callParticipant.getRecipient().getShortDisplayName(getContext())));
                this.infoMoreInfo.setOnClickListener(new View.OnClickListener(callParticipant) { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantView$$ExternalSyntheticLambda0
                    public final /* synthetic */ CallParticipant f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        CallParticipantView.$r8$lambda$6Zry1VHvQW42tPKuqb_Dn2j2q08(CallParticipantView.this, this.f$1, view);
                    }
                });
            } else {
                this.infoIcon.setImageResource(R.drawable.ic_error_solid_24);
                this.infoMessage.setText(getContext().getString(R.string.CallParticipantView__cant_receive_audio_video_from_s, callParticipant.getRecipient().getShortDisplayName(getContext())));
                this.infoMoreInfo.setOnClickListener(new View.OnClickListener(callParticipant) { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantView$$ExternalSyntheticLambda1
                    public final /* synthetic */ CallParticipant f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        CallParticipantView.$r8$lambda$JM8vMK8XCPLrduaeHoCA75taPf0(CallParticipantView.this, this.f$1, view);
                    }
                });
            }
        } else {
            this.infoOverlay.setVisibility(8);
            if ((!callParticipant.isVideoEnabled() && !callParticipant.isScreenSharing()) || !callParticipant.isForwardingVideo()) {
                z = false;
            }
            this.rendererFrame.setVisibility(z ? 0 : 8);
            TextureViewRenderer textureViewRenderer = this.renderer;
            if (z) {
                i = 0;
            }
            textureViewRenderer.setVisibility(i);
            if (callParticipant.isVideoEnabled()) {
                callParticipant.getVideoSink().getLockableEglBase().performWithValidEglBase(new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantView$$ExternalSyntheticLambda2
                    @Override // j$.util.function.Consumer
                    public final void accept(Object obj) {
                        CallParticipantView.m1274$r8$lambda$ObwWTng4HomGROvlL3spPFFKs(CallParticipantView.this, (EglBase) obj);
                    }

                    @Override // j$.util.function.Consumer
                    public /* synthetic */ Consumer andThen(Consumer consumer) {
                        return Consumer.CC.$default$andThen(this, consumer);
                    }
                });
                this.renderer.attachBroadcastVideoSink(callParticipant.getVideoSink());
            } else {
                this.renderer.attachBroadcastVideoSink(null);
            }
            this.audioIndicator.setVisibility(0);
            this.audioIndicator.bind(callParticipant.isMicrophoneEnabled(), callParticipant.getAudioLevel());
        }
        if (z2 || !Objects.equals(this.contactPhoto, callParticipant.getRecipient().getContactPhoto())) {
            this.avatar.setAvatarUsingProfile(callParticipant.getRecipient());
            this.badge.setBadgeFromRecipient(callParticipant.getRecipient());
            AvatarUtil.loadBlurredIconIntoImageView(callParticipant.getRecipient(), this.backgroundAvatar);
            setPipAvatar(callParticipant.getRecipient());
            this.pipBadge.setBadgeFromRecipient(callParticipant.getRecipient());
            this.contactPhoto = callParticipant.getRecipient().getContactPhoto();
        }
    }

    public /* synthetic */ void lambda$setCallParticipant$0(CallParticipant callParticipant, View view) {
        showBlockedDialog(callParticipant.getRecipient());
    }

    public /* synthetic */ void lambda$setCallParticipant$1(CallParticipant callParticipant, View view) {
        showNoMediaKeysDialog(callParticipant.getRecipient());
    }

    public /* synthetic */ void lambda$setCallParticipant$2(EglBase eglBase) {
        this.renderer.init(eglBase);
    }

    private boolean isMissingMediaKeys(CallParticipant callParticipant) {
        Runnable runnable = this.missingMediaKeysUpdater;
        if (runnable != null) {
            ThreadUtil.cancelRunnableOnMain(runnable);
            this.missingMediaKeysUpdater = null;
        }
        if (callParticipant.isMediaKeysReceived()) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis() - callParticipant.getAddedToCallTime();
        long j = DELAY_SHOWING_MISSING_MEDIA_KEYS;
        if (currentTimeMillis > j) {
            return true;
        }
        CallParticipantView$$ExternalSyntheticLambda3 callParticipantView$$ExternalSyntheticLambda3 = new Runnable(callParticipant) { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantView$$ExternalSyntheticLambda3
            public final /* synthetic */ CallParticipant f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                CallParticipantView.$r8$lambda$bGTjhzRfT7xGg7ay7Yqsb3qUVI8(CallParticipantView.this, this.f$1);
            }
        };
        this.missingMediaKeysUpdater = callParticipantView$$ExternalSyntheticLambda3;
        ThreadUtil.runOnMainDelayed(callParticipantView$$ExternalSyntheticLambda3, j - currentTimeMillis);
        return false;
    }

    public /* synthetic */ void lambda$isMissingMediaKeys$3(CallParticipant callParticipant) {
        if (this.recipientId.equals(callParticipant.getRecipient().getId())) {
            setCallParticipant(callParticipant);
        }
    }

    public void setRenderInPip(boolean z) {
        int i = 8;
        if (this.infoMode) {
            this.infoMessage.setVisibility(z ? 8 : 0);
            Button button = this.infoMoreInfo;
            if (!z) {
                i = 0;
            }
            button.setVisibility(i);
            return;
        }
        this.avatar.setVisibility(z ? 8 : 0);
        this.badge.setVisibility(z ? 8 : 0);
        this.pipAvatar.setVisibility(z ? 0 : 8);
        BadgeImageView badgeImageView = this.pipBadge;
        if (z) {
            i = 0;
        }
        badgeImageView.setVisibility(i);
    }

    public void hideAvatar() {
        this.avatar.setAlpha(0.0f);
        this.badge.setAlpha(0.0f);
    }

    public void showAvatar() {
        this.avatar.setAlpha(1.0f);
        this.badge.setAlpha(1.0f);
    }

    public void useLargeAvatar() {
        changeAvatarParams(LARGE_AVATAR);
    }

    public void useSmallAvatar() {
        changeAvatarParams(SMALL_AVATAR);
    }

    public void setBottomInset(int i) {
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.webrtc_audio_indicator_margin) + i;
        if (ViewKt.getMarginBottom(this.audioIndicator) != dimensionPixelSize) {
            TransitionManager.beginDelayedTransition(this);
            ViewUtil.setBottomMargin(this.audioIndicator, dimensionPixelSize);
        }
    }

    public void releaseRenderer() {
        this.renderer.release();
    }

    private void changeAvatarParams(int i) {
        ViewGroup.LayoutParams layoutParams = this.avatar.getLayoutParams();
        if (layoutParams.height != i) {
            layoutParams.height = i;
            layoutParams.width = i;
            this.avatar.setLayoutParams(layoutParams);
        }
    }

    private void setPipAvatar(Recipient recipient) {
        ContactPhoto contactPhoto;
        if (recipient.isSelf()) {
            contactPhoto = new ProfileContactPhoto(Recipient.self());
        } else {
            contactPhoto = recipient.getContactPhoto();
        }
        FallbackContactPhoto fallbackContactPhoto = recipient.getFallbackContactPhoto(FALLBACK_PHOTO_PROVIDER);
        GlideApp.with(this).load((Object) contactPhoto).fallback(fallbackContactPhoto.asCallCard(getContext())).error(fallbackContactPhoto.asCallCard(getContext())).diskCacheStrategy(DiskCacheStrategy.ALL).into(this.pipAvatar);
        this.pipAvatar.setScaleType(contactPhoto == null ? ImageView.ScaleType.CENTER_INSIDE : ImageView.ScaleType.CENTER_CROP);
        this.pipAvatar.setBackground(recipient.getChatColors().getChatBubbleMask());
    }

    private void showBlockedDialog(Recipient recipient) {
        new AlertDialog.Builder(getContext()).setTitle(getContext().getString(R.string.CallParticipantView__s_is_blocked, recipient.getShortDisplayName(getContext()))).setMessage(R.string.CallParticipantView__you_wont_receive_their_audio_or_video).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    private void showNoMediaKeysDialog(Recipient recipient) {
        new AlertDialog.Builder(getContext()).setTitle(getContext().getString(R.string.CallParticipantView__cant_receive_audio_and_video_from_s, recipient.getShortDisplayName(getContext()))).setMessage(R.string.CallParticipantView__this_may_be_Because_they_have_not_verified_your_safety_number_change).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
    }

    /* loaded from: classes4.dex */
    public static final class FallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        private FallbackPhotoProvider() {
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForLocalNumber() {
            return super.getPhotoForRecipientWithoutName();
        }

        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForRecipientWithoutName() {
            ResourceContactPhoto resourceContactPhoto = new ResourceContactPhoto(R.drawable.ic_profile_outline_120);
            resourceContactPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
            return resourceContactPhoto;
        }
    }
}
