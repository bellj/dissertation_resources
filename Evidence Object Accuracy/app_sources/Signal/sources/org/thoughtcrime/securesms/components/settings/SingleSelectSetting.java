package org.thoughtcrime.securesms.components.settings;

import android.text.TextUtils;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.RadioButton;
import android.widget.TextView;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class SingleSelectSetting {

    /* loaded from: classes4.dex */
    public interface SingleSelectSelectionChangedListener {
        void onSelectionChanged(Object obj);
    }

    /* loaded from: classes4.dex */
    public static class ViewHolder extends MappingViewHolder<Item> {
        private final RadioButton radio = ((RadioButton) findViewById(R.id.single_select_item_radio));
        protected final SingleSelectSelectionChangedListener selectionChangedListener;
        private final TextView summaryText = ((TextView) findViewById(R.id.single_select_item_summary));
        protected final CheckedTextView text = ((CheckedTextView) findViewById(R.id.single_select_item_text));

        public ViewHolder(View view, SingleSelectSelectionChangedListener singleSelectSelectionChangedListener) {
            super(view);
            this.selectionChangedListener = singleSelectSelectionChangedListener;
        }

        public void bind(Item item) {
            this.radio.setChecked(item.isSelected);
            this.text.setText(item.text);
            if (!TextUtils.isEmpty(item.summaryText)) {
                this.summaryText.setText(item.summaryText);
                this.summaryText.setVisibility(0);
            } else {
                this.summaryText.setVisibility(8);
            }
            this.itemView.setOnClickListener(new SingleSelectSetting$ViewHolder$$ExternalSyntheticLambda0(this, item));
        }

        public /* synthetic */ void lambda$bind$0(Item item, View view) {
            this.selectionChangedListener.onSelectionChanged(item.item);
        }
    }

    /* loaded from: classes4.dex */
    public static class Item implements MappingModel<Item> {
        private final boolean isSelected;
        private final Object item;
        private final String summaryText;
        private final String text;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Item item) {
            return MappingModel.CC.$default$getChangePayload(this, item);
        }

        public <T> Item(T t, String str, String str2, boolean z) {
            this.item = t;
            this.summaryText = str2;
            this.text = str == null ? t.toString() : str;
            this.isSelected = z;
        }

        public Object getItem() {
            return this.item;
        }

        public String getText() {
            return this.text;
        }

        public String getSummaryText() {
            return this.summaryText;
        }

        public boolean isSelected() {
            return this.isSelected;
        }

        public boolean areItemsTheSame(Item item) {
            return this.item.equals(item.item);
        }

        public boolean areContentsTheSame(Item item) {
            return Objects.equals(this.text, item.text) && Objects.equals(this.summaryText, item.summaryText) && this.isSelected == item.isSelected;
        }
    }
}
