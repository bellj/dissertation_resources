package org.thoughtcrime.securesms.components;

import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InputPanel$SlideToCancel$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ SettableFuture f$0;

    public /* synthetic */ InputPanel$SlideToCancel$$ExternalSyntheticLambda0(SettableFuture settableFuture) {
        this.f$0 = settableFuture;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.set(null);
    }
}
