package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$getConfiguration$1;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileDetailsFragment$getConfiguration$1$4$1$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfileDetailsFragment f$0;
    public final /* synthetic */ RecipientId f$1;

    public /* synthetic */ NotificationProfileDetailsFragment$getConfiguration$1$4$1$$ExternalSyntheticLambda0(NotificationProfileDetailsFragment notificationProfileDetailsFragment, RecipientId recipientId) {
        this.f$0 = notificationProfileDetailsFragment;
        this.f$1 = recipientId;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfileDetailsFragment$getConfiguration$1.AnonymousClass4.AnonymousClass1.m760invoke$lambda1$lambda0(this.f$0, this.f$1, view);
    }
}
