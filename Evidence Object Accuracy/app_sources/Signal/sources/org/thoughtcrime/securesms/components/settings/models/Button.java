package org.thoughtcrime.securesms.components.settings.models;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import com.google.android.material.button.MaterialButton;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: Button.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Button;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Button {
    public static final Button INSTANCE = new Button();

    private Button() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m1249register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.Primary.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.models.Button$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Button.m1249register$lambda0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_button_primary));
        mappingAdapter.registerFactory(Model.Tonal.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.models.Button$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Button.m1250register$lambda1((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_button_tonal));
        mappingAdapter.registerFactory(Model.SecondaryNoOutline.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.models.Button$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return Button.m1251register$lambda2((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_button_secondary));
    }

    /* renamed from: register$lambda-1 */
    public static final MappingViewHolder m1250register$lambda1(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    /* renamed from: register$lambda-2 */
    public static final MappingViewHolder m1251register$lambda2(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    /* compiled from: Button.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002:\u0003\u000f\u0010\u0011B1\b\u0004\u0012\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u0001\u0003\u0012\u0013\u0014¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Button$Model;", "T", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZLkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "Primary", "SecondaryNoOutline", "Tonal", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model$Primary;", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model$Tonal;", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model$SecondaryNoOutline;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class Model<T extends Model<T>> extends PreferenceModel<T> {
        private final Function0<Unit> onClick;

        public /* synthetic */ Model(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0 function0, DefaultConstructorMarker defaultConstructorMarker) {
            this(dSLSettingsText, dSLSettingsIcon, z, function0);
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        private Model(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0<Unit> function0) {
            super(dSLSettingsText, null, dSLSettingsIcon, null, z, 10, null);
            this.onClick = function0;
        }

        /* compiled from: Button.kt */
        @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B/\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0002\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Button$Model$Primary;", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZLkotlin/jvm/functions/Function0;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Primary extends Model<Primary> {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Primary(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0<Unit> function0) {
                super(dSLSettingsText, dSLSettingsIcon, z, function0, null);
                Intrinsics.checkNotNullParameter(function0, "onClick");
            }
        }

        /* compiled from: Button.kt */
        @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B/\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0002\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Button$Model$Tonal;", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZLkotlin/jvm/functions/Function0;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Tonal extends Model<Tonal> {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Tonal(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0<Unit> function0) {
                super(dSLSettingsText, dSLSettingsIcon, z, function0, null);
                Intrinsics.checkNotNullParameter(function0, "onClick");
            }
        }

        /* compiled from: Button.kt */
        @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B/\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0002\u0010\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Button$Model$SecondaryNoOutline;", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZLkotlin/jvm/functions/Function0;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SecondaryNoOutline extends Model<SecondaryNoOutline> {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public SecondaryNoOutline(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0<Unit> function0) {
                super(dSLSettingsText, dSLSettingsIcon, z, function0, null);
                Intrinsics.checkNotNullParameter(function0, "onClick");
            }
        }
    }

    /* compiled from: Button.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Button$ViewHolder;", "T", "Lorg/thoughtcrime/securesms/components/settings/models/Button$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "button", "Lcom/google/android/material/button/MaterialButton;", "bind", "", "model", "(Lorg/thoughtcrime/securesms/components/settings/models/Button$Model;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder<T extends Model<T>> extends MappingViewHolder<T> {
        private final MaterialButton button;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.button);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.button)");
            this.button = (MaterialButton) findViewById;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.models.Button$ViewHolder<T extends org.thoughtcrime.securesms.components.settings.models.Button$Model<T>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public /* bridge */ /* synthetic */ void bind(Object obj) {
            bind((ViewHolder<T>) ((Model) obj));
        }

        public void bind(T t) {
            CharSequence charSequence;
            Intrinsics.checkNotNullParameter(t, "model");
            MaterialButton materialButton = this.button;
            DSLSettingsText title = t.getTitle();
            Drawable drawable = null;
            if (title != null) {
                Context context = this.context;
                Intrinsics.checkNotNullExpressionValue(context, "context");
                charSequence = title.resolve(context);
            } else {
                charSequence = null;
            }
            materialButton.setText(charSequence);
            this.button.setOnClickListener(new Button$ViewHolder$$ExternalSyntheticLambda0(t));
            MaterialButton materialButton2 = this.button;
            DSLSettingsIcon icon = t.getIcon();
            if (icon != null) {
                Context context2 = this.context;
                Intrinsics.checkNotNullExpressionValue(context2, "context");
                drawable = icon.resolve(context2);
            }
            materialButton2.setIcon(drawable);
            this.button.setEnabled(t.isEnabled());
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1252bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke();
        }
    }
}
