package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class TypingIndicatorView extends LinearLayout {
    private static final long CYCLE_DURATION;
    private static final long DOT_DURATION;
    private static final long DURATION;
    private static final float MIN_ALPHA;
    private static final float MIN_SCALE;
    private static final long POST_DELAY;
    private static final long PRE_DELAY;
    private View dot1;
    private View dot2;
    private View dot3;
    private boolean isActive;
    private long startTime;

    public TypingIndicatorView(Context context) {
        super(context);
        initialize(null);
    }

    public TypingIndicatorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(attributeSet);
    }

    private void initialize(AttributeSet attributeSet) {
        LinearLayout.inflate(getContext(), R.layout.typing_indicator_view, this);
        setWillNotDraw(false);
        this.dot1 = findViewById(R.id.typing_dot1);
        this.dot2 = findViewById(R.id.typing_dot2);
        this.dot3 = findViewById(R.id.typing_dot3);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.TypingIndicatorView, 0, 0);
            int color = obtainStyledAttributes.getColor(0, -1);
            obtainStyledAttributes.recycle();
            setDotTint(color);
        }
    }

    public void setDotTint(int i) {
        this.dot1.getBackground().setColorFilter(i, PorterDuff.Mode.MULTIPLY);
        this.dot2.getBackground().setColorFilter(i, PorterDuff.Mode.MULTIPLY);
        this.dot3.getBackground().setColorFilter(i, PorterDuff.Mode.MULTIPLY);
    }

    @Override // android.widget.LinearLayout, android.view.View
    protected void onDraw(Canvas canvas) {
        if (!this.isActive) {
            super.onDraw(canvas);
            return;
        }
        long currentTimeMillis = (System.currentTimeMillis() - this.startTime) % CYCLE_DURATION;
        render(this.dot1, currentTimeMillis, 0);
        render(this.dot2, currentTimeMillis, 150);
        render(this.dot3, currentTimeMillis, DURATION);
        super.onDraw(canvas);
        postInvalidate();
    }

    private void render(View view, long j, long j2) {
        long j3 = DOT_DURATION + j2;
        long j4 = j2 + DURATION;
        if (j < j2 || j > j3) {
            renderDefault(view);
        } else if (j < j4) {
            renderFadeIn(view, j, j2);
        } else {
            renderFadeOut(view, j, j4);
        }
    }

    private void renderDefault(View view) {
        view.setAlpha(MIN_ALPHA);
        view.setScaleX(MIN_SCALE);
        view.setScaleY(MIN_SCALE);
    }

    private void renderFadeIn(View view, long j, long j2) {
        float f = ((float) (j - j2)) / 300.0f;
        view.setAlpha((0.6f * f) + MIN_ALPHA);
        float f2 = (f * 0.25f) + MIN_SCALE;
        view.setScaleX(f2);
        view.setScaleY(f2);
    }

    private void renderFadeOut(View view, long j, long j2) {
        float f = ((float) (j - j2)) / 300.0f;
        view.setAlpha(1.0f - (0.6f * f));
        float f2 = 1.0f - (f * 0.25f);
        view.setScaleX(f2);
        view.setScaleY(f2);
    }

    public void startAnimation() {
        this.isActive = true;
        this.startTime = System.currentTimeMillis();
        postInvalidate();
    }

    public void stopAnimation() {
        this.isActive = false;
    }
}
