package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.graphics.drawable.IconCompat;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.notifications.NotificationIds;

/* compiled from: DonationErrorNotifications.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000eB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\"\u0010\t\u001a\u00020\u0004*\u00020\n2\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fH\u0002¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorNotifications;", "", "()V", "displayErrorNotification", "", "context", "Landroid/content/Context;", "donationError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", "addAction", "Landroidx/core/app/NotificationCompat$Builder;", "errorAction", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "Landroid/app/PendingIntent;", "NotificationCallback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationErrorNotifications {
    public static final DonationErrorNotifications INSTANCE = new DonationErrorNotifications();

    private DonationErrorNotifications() {
    }

    public final void displayErrorNotification(Context context, DonationError donationError) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(donationError, "donationError");
        DonationErrorParams create = DonationErrorParams.Companion.create(context, donationError, NotificationCallback.INSTANCE);
        NotificationCompat.Builder contentText = new NotificationCompat.Builder(context, NotificationChannels.FAILURES).setSmallIcon(R.drawable.ic_notification).setContentTitle(context.getString(create.getTitle())).setContentText(context.getString(create.getMessage()));
        if (create.getPositiveAction() != null) {
            DonationErrorNotifications donationErrorNotifications = INSTANCE;
            Intrinsics.checkNotNullExpressionValue(contentText, "");
            donationErrorNotifications.addAction(contentText, context, create.getPositiveAction());
        }
        if (create.getNegativeAction() != null) {
            DonationErrorNotifications donationErrorNotifications2 = INSTANCE;
            Intrinsics.checkNotNullExpressionValue(contentText, "");
            donationErrorNotifications2.addAction(contentText, context, create.getNegativeAction());
        }
        Notification build = contentText.build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder(context, Notific… }\n      }\n      .build()");
        NotificationManagerCompat.from(context).notify(NotificationIds.DONOR_BADGE_FAILURE, build);
    }

    private final void addAction(NotificationCompat.Builder builder, Context context, DonationErrorParams.ErrorAction<PendingIntent> errorAction) {
        builder.addAction(new NotificationCompat.Action.Builder((IconCompat) null, context.getString(errorAction.getLabel()), errorAction.getAction().invoke()).build());
    }

    /* compiled from: DonationErrorNotifications.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\bÂ\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0003J&\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH\u0002J\u0018\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0016\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0016\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorNotifications$NotificationCallback;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$Callback;", "Landroid/app/PendingIntent;", "()V", "createAction", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "context", "Landroid/content/Context;", EmojiSearchDatabase.LABEL, "", "actionIntent", "Landroid/content/Intent;", "onCancel", "onContactSupport", "onGoToGooglePay", "onLearnMore", "onOk", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotificationCallback implements DonationErrorParams.Callback<PendingIntent> {
        public static final NotificationCallback INSTANCE = new NotificationCallback();

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<PendingIntent> onCancel(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return null;
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<PendingIntent> onOk(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return null;
        }

        private NotificationCallback() {
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<PendingIntent> onLearnMore(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return createAction(context, R.string.DeclineCode__learn_more, new Intent("android.intent.action.VIEW", Uri.parse(context.getString(R.string.donation_decline_code_error_url))));
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<PendingIntent> onGoToGooglePay(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return createAction(context, R.string.DeclineCode__go_to_google_pay, new Intent("android.intent.action.VIEW", Uri.parse(context.getString(R.string.google_pay_url))));
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<PendingIntent> onContactSupport(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return createAction(context, R.string.Subscription__contact_support, AppSettingsActivity.Companion.help(context, 7));
        }

        private final DonationErrorParams.ErrorAction<PendingIntent> createAction(Context context, int i, Intent intent) {
            return new DonationErrorParams.ErrorAction<>(i, new DonationErrorNotifications$NotificationCallback$createAction$1(context, intent));
        }
    }
}
