package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/LongClickPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/LongClickPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LongClickPreferenceViewHolder extends PreferenceViewHolder<LongClickPreference> {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public LongClickPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
    }

    public void bind(LongClickPreference longClickPreference) {
        Intrinsics.checkNotNullParameter(longClickPreference, "model");
        super.bind((LongClickPreferenceViewHolder) longClickPreference);
        this.itemView.setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.components.settings.LongClickPreferenceViewHolder$$ExternalSyntheticLambda0
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return LongClickPreferenceViewHolder.m530$r8$lambda$Scx3GPN50T3WRsIPorNJvD9g(LongClickPreference.this, view);
            }
        });
    }

    /* renamed from: bind$lambda-0 */
    public static final boolean m531bind$lambda0(LongClickPreference longClickPreference, View view) {
        Intrinsics.checkNotNullParameter(longClickPreference, "$model");
        longClickPreference.getOnLongClick().invoke();
        return true;
    }
}
