package org.thoughtcrime.securesms.components.settings.app.subscription.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.GooglePayButton;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GooglePayButton$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ GooglePayButton.ViewHolder f$0;
    public final /* synthetic */ GooglePayButton.Model f$1;

    public /* synthetic */ GooglePayButton$ViewHolder$$ExternalSyntheticLambda0(GooglePayButton.ViewHolder viewHolder, GooglePayButton.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GooglePayButton.ViewHolder.m990bind$lambda0(this.f$0, this.f$1, view);
    }
}
