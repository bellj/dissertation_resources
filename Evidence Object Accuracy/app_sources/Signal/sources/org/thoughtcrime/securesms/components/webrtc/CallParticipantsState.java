package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.OptionalLong;
import com.annimon.stream.function.LongUnaryOperator;
import com.annimon.stream.function.Supplier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsJvmKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.WebRtcControls;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;

/* compiled from: CallParticipantsState.kt */
@Metadata(d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b0\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\b\b\u0018\u0000 U2\u00020\u0001:\u0002UVB¡\u0001\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\f\u0012\b\b\u0002\u0010\r\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0010\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0012\u0012\b\b\u0002\u0010\u0013\u001a\u00020\u0014\u0012\b\b\u0002\u0010\u0015\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u000e\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u0012\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a¢\u0006\u0002\u0010\u001cJ\t\u00109\u001a\u00020\u0003HÆ\u0003J\t\u0010:\u001a\u00020\u0012HÆ\u0003J\t\u0010;\u001a\u00020\u0014HÂ\u0003J\t\u0010<\u001a\u00020\u000eHÆ\u0003J\t\u0010=\u001a\u00020\u000eHÆ\u0003J\t\u0010>\u001a\u00020\u0018HÆ\u0003J\u000f\u0010?\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aHÆ\u0003J\t\u0010@\u001a\u00020\u0005HÆ\u0003J\t\u0010A\u001a\u00020\u0007HÂ\u0003J\t\u0010B\u001a\u00020\tHÆ\u0003J\t\u0010C\u001a\u00020\tHÆ\u0003J\t\u0010D\u001a\u00020\fHÆ\u0003J\t\u0010E\u001a\u00020\u000eHÆ\u0003J\t\u0010F\u001a\u00020\u000eHÂ\u0003J\t\u0010G\u001a\u00020\u000eHÆ\u0003J¥\u0001\u0010H\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u000e2\b\b\u0002\u0010\u0010\u001a\u00020\u000e2\b\b\u0002\u0010\u0011\u001a\u00020\u00122\b\b\u0002\u0010\u0013\u001a\u00020\u00142\b\b\u0002\u0010\u0015\u001a\u00020\u000e2\b\b\u0002\u0010\u0016\u001a\u00020\u000e2\b\b\u0002\u0010\u0017\u001a\u00020\u00182\u000e\b\u0002\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001aHÆ\u0001J\u0013\u0010I\u001a\u00020\u000e2\b\u0010J\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0010\u0010K\u001a\u0004\u0018\u00010L2\u0006\u0010M\u001a\u00020NJ\u0010\u0010O\u001a\u0004\u0018\u00010L2\u0006\u0010M\u001a\u00020NJ\u0010\u0010P\u001a\u0004\u0018\u00010L2\u0006\u0010M\u001a\u00020NJ\t\u0010Q\u001a\u00020RHÖ\u0001J\u0006\u0010S\u001a\u00020\u000eJ\t\u0010T\u001a\u00020LHÖ\u0001R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\t0\u001a¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u000e\u0010\u0013\u001a\u00020\u0014X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010$\u001a\b\u0012\u0004\u0012\u00020\t0\u001a8F¢\u0006\u0006\u001a\u0004\b%\u0010\u001fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'R\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001b0\u001a¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001fR\u0011\u0010)\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b)\u0010*R\u0011\u0010\u0015\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010*R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010*R\u0011\u0010+\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b+\u0010*R\u0011\u0010,\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b,\u0010*R\u0011\u0010\u0010\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010*R\u0017\u0010-\u001a\b\u0012\u0004\u0012\u00020\t0\u001a8F¢\u0006\u0006\u001a\u0004\b.\u0010\u001fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b/\u0010#R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b0\u00101R\u0011\u00102\u001a\u00020\u00128F¢\u0006\u0006\u001a\u0004\b3\u00104R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b5\u00104R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0016\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b6\u0010*R\u0011\u0010\u0017\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\b7\u00108R\u000e\u0010\u000f\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006W"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState;", "", "callState", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "groupCallState", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "remoteParticipants", "Lorg/thoughtcrime/securesms/service/webrtc/collections/ParticipantCollection;", "localParticipant", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "focusedParticipant", "localRenderState", "Lorg/thoughtcrime/securesms/components/webrtc/WebRtcLocalRenderState;", "isInPipMode", "", "showVideoForOutgoing", "isViewingFocusedParticipant", "remoteDevicesCount", "Lcom/annimon/stream/OptionalLong;", "foldableState", "Lorg/thoughtcrime/securesms/components/webrtc/WebRtcControls$FoldableState;", "isInOutgoingRingingMode", "ringGroup", "ringerRecipient", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "groupMembers", "", "Lorg/thoughtcrime/securesms/groups/ui/GroupMemberEntry$FullMember;", "(Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;Lorg/thoughtcrime/securesms/service/webrtc/collections/ParticipantCollection;Lorg/thoughtcrime/securesms/events/CallParticipant;Lorg/thoughtcrime/securesms/events/CallParticipant;Lorg/thoughtcrime/securesms/components/webrtc/WebRtcLocalRenderState;ZZZLcom/annimon/stream/OptionalLong;Lorg/thoughtcrime/securesms/components/webrtc/WebRtcControls$FoldableState;ZZLorg/thoughtcrime/securesms/recipients/Recipient;Ljava/util/List;)V", "allRemoteParticipants", "getAllRemoteParticipants", "()Ljava/util/List;", "getCallState", "()Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "getFocusedParticipant", "()Lorg/thoughtcrime/securesms/events/CallParticipant;", "gridParticipants", "getGridParticipants", "getGroupCallState", "()Lorg/thoughtcrime/securesms/events/WebRtcViewModel$GroupCallState;", "getGroupMembers", "isFolded", "()Z", "isIncomingRing", "isLargeVideoGroup", "listParticipants", "getListParticipants", "getLocalParticipant", "getLocalRenderState", "()Lorg/thoughtcrime/securesms/components/webrtc/WebRtcLocalRenderState;", "participantCount", "getParticipantCount", "()Lcom/annimon/stream/OptionalLong;", "getRemoteDevicesCount", "getRingGroup", "getRingerRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "getIncomingRingingGroupDescription", "", "context", "Landroid/content/Context;", "getOutgoingRingingGroupDescription", "getPreJoinGroupDescription", "hashCode", "", "needsNewRequestSizes", "toString", "Companion", "SelectedPage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CallParticipantsState {
    public static final Companion Companion = new Companion(null);
    public static final long MAX_OUTGOING_GROUP_RING_DURATION = TimeUnit.MINUTES.toMillis(1);
    private static final int SMALL_GROUP_MAX;
    public static final CallParticipantsState STARTING_STATE = new CallParticipantsState(null, null, null, null, null, null, false, false, false, null, null, false, false, null, null, 32767, null);
    private final List<CallParticipant> allRemoteParticipants;
    private final WebRtcViewModel.State callState;
    private final CallParticipant focusedParticipant;
    private final WebRtcControls.FoldableState foldableState;
    private final WebRtcViewModel.GroupCallState groupCallState;
    private final List<GroupMemberEntry.FullMember> groupMembers;
    private final boolean isFolded;
    private final boolean isInOutgoingRingingMode;
    private final boolean isInPipMode;
    private final boolean isIncomingRing;
    private final boolean isLargeVideoGroup;
    private final boolean isViewingFocusedParticipant;
    private final CallParticipant localParticipant;
    private final WebRtcLocalRenderState localRenderState;
    private final OptionalLong remoteDevicesCount;
    private final ParticipantCollection remoteParticipants;
    private final boolean ringGroup;
    private final Recipient ringerRecipient;
    private final boolean showVideoForOutgoing;

    /* compiled from: CallParticipantsState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState$SelectedPage;", "", "(Ljava/lang/String;I)V", "GRID", "FOCUSED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum SelectedPage {
        GRID,
        FOCUSED
    }

    public CallParticipantsState() {
        this(null, null, null, null, null, null, false, false, false, null, null, false, false, null, null, 32767, null);
    }

    /* renamed from: _get_participantCount_$lambda-0 */
    public static final long m1276_get_participantCount_$lambda0(boolean z, long j) {
        return j + (z ? 1 : 0);
    }

    private final WebRtcControls.FoldableState component11() {
        return this.foldableState;
    }

    private final ParticipantCollection component3() {
        return this.remoteParticipants;
    }

    private final boolean component8() {
        return this.showVideoForOutgoing;
    }

    public static /* synthetic */ CallParticipantsState copy$default(CallParticipantsState callParticipantsState, WebRtcViewModel.State state, WebRtcViewModel.GroupCallState groupCallState, ParticipantCollection participantCollection, CallParticipant callParticipant, CallParticipant callParticipant2, WebRtcLocalRenderState webRtcLocalRenderState, boolean z, boolean z2, boolean z3, OptionalLong optionalLong, WebRtcControls.FoldableState foldableState, boolean z4, boolean z5, Recipient recipient, List list, int i, Object obj) {
        return callParticipantsState.copy((i & 1) != 0 ? callParticipantsState.callState : state, (i & 2) != 0 ? callParticipantsState.groupCallState : groupCallState, (i & 4) != 0 ? callParticipantsState.remoteParticipants : participantCollection, (i & 8) != 0 ? callParticipantsState.localParticipant : callParticipant, (i & 16) != 0 ? callParticipantsState.focusedParticipant : callParticipant2, (i & 32) != 0 ? callParticipantsState.localRenderState : webRtcLocalRenderState, (i & 64) != 0 ? callParticipantsState.isInPipMode : z, (i & 128) != 0 ? callParticipantsState.showVideoForOutgoing : z2, (i & 256) != 0 ? callParticipantsState.isViewingFocusedParticipant : z3, (i & 512) != 0 ? callParticipantsState.remoteDevicesCount : optionalLong, (i & 1024) != 0 ? callParticipantsState.foldableState : foldableState, (i & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? callParticipantsState.isInOutgoingRingingMode : z4, (i & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? callParticipantsState.ringGroup : z5, (i & 8192) != 0 ? callParticipantsState.ringerRecipient : recipient, (i & 16384) != 0 ? callParticipantsState.groupMembers : list);
    }

    @JvmStatic
    public static final CallParticipantsState setExpanded(CallParticipantsState callParticipantsState, boolean z) {
        return Companion.setExpanded(callParticipantsState, z);
    }

    @JvmStatic
    public static final CallParticipantsState update(CallParticipantsState callParticipantsState, List<GroupMemberEntry.FullMember> list) {
        return Companion.update(callParticipantsState, list);
    }

    @JvmStatic
    public static final CallParticipantsState update(CallParticipantsState callParticipantsState, SelectedPage selectedPage) {
        return Companion.update(callParticipantsState, selectedPage);
    }

    @JvmStatic
    public static final CallParticipantsState update(CallParticipantsState callParticipantsState, WebRtcControls.FoldableState foldableState) {
        return Companion.update(callParticipantsState, foldableState);
    }

    @JvmStatic
    public static final CallParticipantsState update(CallParticipantsState callParticipantsState, WebRtcViewModel webRtcViewModel, boolean z) {
        return Companion.update(callParticipantsState, webRtcViewModel, z);
    }

    @JvmStatic
    public static final CallParticipantsState update(CallParticipantsState callParticipantsState, WebRtcEphemeralState webRtcEphemeralState) {
        return Companion.update(callParticipantsState, webRtcEphemeralState);
    }

    @JvmStatic
    public static final CallParticipantsState update(CallParticipantsState callParticipantsState, boolean z) {
        return Companion.update(callParticipantsState, z);
    }

    public final WebRtcViewModel.State component1() {
        return this.callState;
    }

    public final OptionalLong component10() {
        return this.remoteDevicesCount;
    }

    public final boolean component12() {
        return this.isInOutgoingRingingMode;
    }

    public final boolean component13() {
        return this.ringGroup;
    }

    public final Recipient component14() {
        return this.ringerRecipient;
    }

    public final List<GroupMemberEntry.FullMember> component15() {
        return this.groupMembers;
    }

    public final WebRtcViewModel.GroupCallState component2() {
        return this.groupCallState;
    }

    public final CallParticipant component4() {
        return this.localParticipant;
    }

    public final CallParticipant component5() {
        return this.focusedParticipant;
    }

    public final WebRtcLocalRenderState component6() {
        return this.localRenderState;
    }

    public final boolean component7() {
        return this.isInPipMode;
    }

    public final boolean component9() {
        return this.isViewingFocusedParticipant;
    }

    public final CallParticipantsState copy(WebRtcViewModel.State state, WebRtcViewModel.GroupCallState groupCallState, ParticipantCollection participantCollection, CallParticipant callParticipant, CallParticipant callParticipant2, WebRtcLocalRenderState webRtcLocalRenderState, boolean z, boolean z2, boolean z3, OptionalLong optionalLong, WebRtcControls.FoldableState foldableState, boolean z4, boolean z5, Recipient recipient, List<GroupMemberEntry.FullMember> list) {
        Intrinsics.checkNotNullParameter(state, "callState");
        Intrinsics.checkNotNullParameter(groupCallState, "groupCallState");
        Intrinsics.checkNotNullParameter(participantCollection, "remoteParticipants");
        Intrinsics.checkNotNullParameter(callParticipant, "localParticipant");
        Intrinsics.checkNotNullParameter(callParticipant2, "focusedParticipant");
        Intrinsics.checkNotNullParameter(webRtcLocalRenderState, "localRenderState");
        Intrinsics.checkNotNullParameter(optionalLong, "remoteDevicesCount");
        Intrinsics.checkNotNullParameter(foldableState, "foldableState");
        Intrinsics.checkNotNullParameter(recipient, "ringerRecipient");
        Intrinsics.checkNotNullParameter(list, "groupMembers");
        return new CallParticipantsState(state, groupCallState, participantCollection, callParticipant, callParticipant2, webRtcLocalRenderState, z, z2, z3, optionalLong, foldableState, z4, z5, recipient, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CallParticipantsState)) {
            return false;
        }
        CallParticipantsState callParticipantsState = (CallParticipantsState) obj;
        return this.callState == callParticipantsState.callState && this.groupCallState == callParticipantsState.groupCallState && Intrinsics.areEqual(this.remoteParticipants, callParticipantsState.remoteParticipants) && Intrinsics.areEqual(this.localParticipant, callParticipantsState.localParticipant) && Intrinsics.areEqual(this.focusedParticipant, callParticipantsState.focusedParticipant) && this.localRenderState == callParticipantsState.localRenderState && this.isInPipMode == callParticipantsState.isInPipMode && this.showVideoForOutgoing == callParticipantsState.showVideoForOutgoing && this.isViewingFocusedParticipant == callParticipantsState.isViewingFocusedParticipant && Intrinsics.areEqual(this.remoteDevicesCount, callParticipantsState.remoteDevicesCount) && Intrinsics.areEqual(this.foldableState, callParticipantsState.foldableState) && this.isInOutgoingRingingMode == callParticipantsState.isInOutgoingRingingMode && this.ringGroup == callParticipantsState.ringGroup && Intrinsics.areEqual(this.ringerRecipient, callParticipantsState.ringerRecipient) && Intrinsics.areEqual(this.groupMembers, callParticipantsState.groupMembers);
    }

    public int hashCode() {
        int hashCode = ((((((((((this.callState.hashCode() * 31) + this.groupCallState.hashCode()) * 31) + this.remoteParticipants.hashCode()) * 31) + this.localParticipant.hashCode()) * 31) + this.focusedParticipant.hashCode()) * 31) + this.localRenderState.hashCode()) * 31;
        boolean z = this.isInPipMode;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.showVideoForOutgoing;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.isViewingFocusedParticipant;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int hashCode2 = (((((i9 + i10) * 31) + this.remoteDevicesCount.hashCode()) * 31) + this.foldableState.hashCode()) * 31;
        boolean z4 = this.isInOutgoingRingingMode;
        if (z4) {
            z4 = true;
        }
        int i13 = z4 ? 1 : 0;
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = (hashCode2 + i13) * 31;
        boolean z5 = this.ringGroup;
        if (!z5) {
            i = z5 ? 1 : 0;
        }
        return ((((i16 + i) * 31) + this.ringerRecipient.hashCode()) * 31) + this.groupMembers.hashCode();
    }

    public String toString() {
        return "CallParticipantsState(callState=" + this.callState + ", groupCallState=" + this.groupCallState + ", remoteParticipants=" + this.remoteParticipants + ", localParticipant=" + this.localParticipant + ", focusedParticipant=" + this.focusedParticipant + ", localRenderState=" + this.localRenderState + ", isInPipMode=" + this.isInPipMode + ", showVideoForOutgoing=" + this.showVideoForOutgoing + ", isViewingFocusedParticipant=" + this.isViewingFocusedParticipant + ", remoteDevicesCount=" + this.remoteDevicesCount + ", foldableState=" + this.foldableState + ", isInOutgoingRingingMode=" + this.isInOutgoingRingingMode + ", ringGroup=" + this.ringGroup + ", ringerRecipient=" + this.ringerRecipient + ", groupMembers=" + this.groupMembers + ')';
    }

    public CallParticipantsState(WebRtcViewModel.State state, WebRtcViewModel.GroupCallState groupCallState, ParticipantCollection participantCollection, CallParticipant callParticipant, CallParticipant callParticipant2, WebRtcLocalRenderState webRtcLocalRenderState, boolean z, boolean z2, boolean z3, OptionalLong optionalLong, WebRtcControls.FoldableState foldableState, boolean z4, boolean z5, Recipient recipient, List<GroupMemberEntry.FullMember> list) {
        Intrinsics.checkNotNullParameter(state, "callState");
        Intrinsics.checkNotNullParameter(groupCallState, "groupCallState");
        Intrinsics.checkNotNullParameter(participantCollection, "remoteParticipants");
        Intrinsics.checkNotNullParameter(callParticipant, "localParticipant");
        Intrinsics.checkNotNullParameter(callParticipant2, "focusedParticipant");
        Intrinsics.checkNotNullParameter(webRtcLocalRenderState, "localRenderState");
        Intrinsics.checkNotNullParameter(optionalLong, "remoteDevicesCount");
        Intrinsics.checkNotNullParameter(foldableState, "foldableState");
        Intrinsics.checkNotNullParameter(recipient, "ringerRecipient");
        Intrinsics.checkNotNullParameter(list, "groupMembers");
        this.callState = state;
        this.groupCallState = groupCallState;
        this.remoteParticipants = participantCollection;
        this.localParticipant = callParticipant;
        this.focusedParticipant = callParticipant2;
        this.localRenderState = webRtcLocalRenderState;
        this.isInPipMode = z;
        this.showVideoForOutgoing = z2;
        this.isViewingFocusedParticipant = z3;
        this.remoteDevicesCount = optionalLong;
        this.foldableState = foldableState;
        this.isInOutgoingRingingMode = z4;
        this.ringGroup = z5;
        this.ringerRecipient = recipient;
        this.groupMembers = list;
        List<CallParticipant> allParticipants = participantCollection.getAllParticipants();
        Intrinsics.checkNotNullExpressionValue(allParticipants, "remoteParticipants.allParticipants");
        this.allRemoteParticipants = allParticipants;
        this.isFolded = foldableState.isFolded();
        boolean z6 = true;
        this.isLargeVideoGroup = allParticipants.size() > 6;
        this.isIncomingRing = state != WebRtcViewModel.State.CALL_INCOMING ? false : z6;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CallParticipantsState(org.thoughtcrime.securesms.events.WebRtcViewModel.State r17, org.thoughtcrime.securesms.events.WebRtcViewModel.GroupCallState r18, org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection r19, org.thoughtcrime.securesms.events.CallParticipant r20, org.thoughtcrime.securesms.events.CallParticipant r21, org.thoughtcrime.securesms.components.webrtc.WebRtcLocalRenderState r22, boolean r23, boolean r24, boolean r25, com.annimon.stream.OptionalLong r26, org.thoughtcrime.securesms.components.webrtc.WebRtcControls.FoldableState r27, boolean r28, boolean r29, org.thoughtcrime.securesms.recipients.Recipient r30, java.util.List r31, int r32, kotlin.jvm.internal.DefaultConstructorMarker r33) {
        /*
        // Method dump skipped, instructions count: 207
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.<init>(org.thoughtcrime.securesms.events.WebRtcViewModel$State, org.thoughtcrime.securesms.events.WebRtcViewModel$GroupCallState, org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection, org.thoughtcrime.securesms.events.CallParticipant, org.thoughtcrime.securesms.events.CallParticipant, org.thoughtcrime.securesms.components.webrtc.WebRtcLocalRenderState, boolean, boolean, boolean, com.annimon.stream.OptionalLong, org.thoughtcrime.securesms.components.webrtc.WebRtcControls$FoldableState, boolean, boolean, org.thoughtcrime.securesms.recipients.Recipient, java.util.List, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final WebRtcViewModel.State getCallState() {
        return this.callState;
    }

    public final WebRtcViewModel.GroupCallState getGroupCallState() {
        return this.groupCallState;
    }

    public final CallParticipant getLocalParticipant() {
        return this.localParticipant;
    }

    public final CallParticipant getFocusedParticipant() {
        return this.focusedParticipant;
    }

    public final WebRtcLocalRenderState getLocalRenderState() {
        return this.localRenderState;
    }

    public final boolean isInPipMode() {
        return this.isInPipMode;
    }

    public final boolean isViewingFocusedParticipant() {
        return this.isViewingFocusedParticipant;
    }

    public final OptionalLong getRemoteDevicesCount() {
        return this.remoteDevicesCount;
    }

    public final boolean isInOutgoingRingingMode() {
        return this.isInOutgoingRingingMode;
    }

    public final boolean getRingGroup() {
        return this.ringGroup;
    }

    public final Recipient getRingerRecipient() {
        return this.ringerRecipient;
    }

    public final List<GroupMemberEntry.FullMember> getGroupMembers() {
        return this.groupMembers;
    }

    public final List<CallParticipant> getAllRemoteParticipants() {
        return this.allRemoteParticipants;
    }

    public final boolean isFolded() {
        return this.isFolded;
    }

    public final boolean isLargeVideoGroup() {
        return this.isLargeVideoGroup;
    }

    public final boolean isIncomingRing() {
        return this.isIncomingRing;
    }

    public final List<CallParticipant> getGridParticipants() {
        List<CallParticipant> gridParticipants = this.remoteParticipants.getGridParticipants();
        Intrinsics.checkNotNullExpressionValue(gridParticipants, "remoteParticipants.gridParticipants");
        return gridParticipants;
    }

    public final List<CallParticipant> getListParticipants() {
        ArrayList arrayList = new ArrayList();
        if (!this.isViewingFocusedParticipant || this.allRemoteParticipants.size() <= 1) {
            List<CallParticipant> listParticipants = this.remoteParticipants.getListParticipants();
            Intrinsics.checkNotNullExpressionValue(listParticipants, "remoteParticipants.listParticipants");
            arrayList.addAll(listParticipants);
        } else {
            arrayList.addAll(this.allRemoteParticipants);
            arrayList.remove(this.focusedParticipant);
        }
        if (this.foldableState.isFlat()) {
            arrayList.add(CallParticipant.EMPTY);
        }
        CollectionsKt___CollectionsJvmKt.reverse(arrayList);
        return arrayList;
    }

    public final OptionalLong getParticipantCount() {
        boolean z = this.groupCallState == WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED;
        OptionalLong or = this.remoteDevicesCount.map(new LongUnaryOperator(z) { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantsState$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.LongUnaryOperator
            public final long applyAsLong(long j) {
                return CallParticipantsState.m1276_get_participantCount_$lambda0(this.f$0, j);
            }
        }).or(new Supplier(z) { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantsState$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Supplier
            public final Object get() {
                return CallParticipantsState.m1277_get_participantCount_$lambda1(this.f$0);
            }
        });
        Intrinsics.checkNotNullExpressionValue(or, "remoteDevicesCount.map {…se OptionalLong.empty() }");
        return or;
    }

    /* renamed from: _get_participantCount_$lambda-1 */
    public static final OptionalLong m1277_get_participantCount_$lambda1(boolean z) {
        return z ? OptionalLong.of(1) : OptionalLong.empty();
    }

    public final String getPreJoinGroupDescription(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.callState != WebRtcViewModel.State.CALL_PRE_JOIN || this.groupCallState.isIdle()) {
            return null;
        }
        if (this.remoteParticipants.isEmpty()) {
            Companion companion = Companion;
            boolean z = this.ringGroup;
            return companion.describeGroupMembers(context, z ? R.string.WebRtcCallView__signal_will_ring_s : R.string.WebRtcCallView__s_will_be_notified, z ? R.string.WebRtcCallView__signal_will_ring_s_and_s : R.string.WebRtcCallView__s_and_s_will_be_notified, z ? R.plurals.WebRtcCallView__signal_will_ring_s_s_and_d_others : R.plurals.WebRtcCallView__s_s_and_d_others_will_be_notified, this.groupMembers);
        }
        int size = this.remoteParticipants.size();
        if (size == 0) {
            return context.getString(R.string.WebRtcCallView__no_one_else_is_here);
        }
        if (size == 1) {
            return context.getString(this.remoteParticipants.get(0).isSelf() ? R.string.WebRtcCallView__s_are_in_this_call : R.string.WebRtcCallView__s_is_in_this_call, this.remoteParticipants.get(0).getShortRecipientDisplayName(context));
        } else if (size == 2) {
            return context.getString(R.string.WebRtcCallView__s_and_s_are_in_this_call, this.remoteParticipants.get(0).getShortRecipientDisplayName(context), this.remoteParticipants.get(1).getShortRecipientDisplayName(context));
        } else {
            int size2 = this.remoteParticipants.size() - 2;
            return context.getResources().getQuantityString(R.plurals.WebRtcCallView__s_s_and_d_others_are_in_this_call, size2, this.remoteParticipants.get(0).getShortRecipientDisplayName(context), this.remoteParticipants.get(1).getShortRecipientDisplayName(context), Integer.valueOf(size2));
        }
    }

    public final String getOutgoingRingingGroupDescription(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.callState == WebRtcViewModel.State.CALL_CONNECTED && this.groupCallState == WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED && this.isInOutgoingRingingMode) {
            return Companion.describeGroupMembers(context, R.string.WebRtcCallView__ringing_s, R.string.WebRtcCallView__ringing_s_and_s, R.plurals.WebRtcCallView__ringing_s_s_and_d_others, this.groupMembers);
        }
        return null;
    }

    public final String getIncomingRingingGroupDescription(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (this.callState != WebRtcViewModel.State.CALL_INCOMING || this.groupCallState != WebRtcViewModel.GroupCallState.RINGING || !this.ringerRecipient.hasServiceId()) {
            return null;
        }
        String shortDisplayName = this.ringerRecipient.getShortDisplayName(context);
        Intrinsics.checkNotNullExpressionValue(shortDisplayName, "ringerRecipient.getShortDisplayName(context)");
        List<GroupMemberEntry.FullMember> list = this.groupMembers;
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            boolean z = false;
            if (!it.hasNext()) {
                break;
            }
            Object next = it.next();
            GroupMemberEntry.FullMember fullMember = (GroupMemberEntry.FullMember) next;
            if (fullMember.getMember().isSelf() || Intrinsics.areEqual(this.ringerRecipient.requireServiceId(), fullMember.getMember().getServiceId().orElse(null))) {
                z = true;
            }
            if (!z) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        if (size == 0) {
            return context.getString(R.string.WebRtcCallView__s_is_calling_you, shortDisplayName);
        }
        if (size == 1) {
            return context.getString(R.string.WebRtcCallView__s_is_calling_you_and_s, shortDisplayName, ((GroupMemberEntry.FullMember) arrayList.get(0)).getMember().getShortDisplayName(context));
        }
        if (size == 2) {
            return context.getString(R.string.WebRtcCallView__s_is_calling_you_s_and_s, shortDisplayName, ((GroupMemberEntry.FullMember) arrayList.get(0)).getMember().getShortDisplayName(context), ((GroupMemberEntry.FullMember) arrayList.get(1)).getMember().getShortDisplayName(context));
        }
        int size2 = arrayList.size() - 2;
        return context.getResources().getQuantityString(R.plurals.WebRtcCallView__s_is_calling_you_s_s_and_d_others, size2, shortDisplayName, ((GroupMemberEntry.FullMember) arrayList.get(0)).getMember().getShortDisplayName(context), ((GroupMemberEntry.FullMember) arrayList.get(1)).getMember().getShortDisplayName(context), Integer.valueOf(size2));
    }

    public final boolean needsNewRequestSizes() {
        if (!this.groupCallState.isNotIdle()) {
            return false;
        }
        List<CallParticipant> list = this.allRemoteParticipants;
        if ((list instanceof Collection) && list.isEmpty()) {
            return false;
        }
        for (CallParticipant callParticipant : list) {
            if (callParticipant.getVideoSink().needsNewRequestingSize()) {
                return true;
            }
        }
        return false;
    }

    /* compiled from: CallParticipantsState.kt */
    @Metadata(d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J<\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\r\u001a\u00020\u00062\b\b\u0001\u0010\u000e\u001a\u00020\u00062\b\b\u0001\u0010\u000f\u001a\u00020\u00062\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0002J`\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\b2\b\b\u0002\u0010\u0016\u001a\u00020\u00172\b\b\u0002\u0010\u0018\u001a\u00020\u00192\b\b\u0002\u0010\u001a\u001a\u00020\u00192\b\b\u0002\u0010\u001b\u001a\u00020\u00192\b\b\u0002\u0010\u001c\u001a\u00020\u001d2\b\b\u0002\u0010\u001e\u001a\u00020\u00062\b\b\u0002\u0010\u001f\u001a\u00020\u00192\b\b\u0002\u0010 \u001a\u00020\u0019H\u0002J\u0016\u0010!\u001a\u00020\u00172\f\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00170\u0011H\u0002J\u0018\u0010#\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010$\u001a\u00020\u0019H\u0007J\u0018\u0010%\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010\u0018\u001a\u00020\u0019H\u0007J\u001e\u0010%\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\f\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011H\u0007J\u0018\u0010%\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010'\u001a\u00020(H\u0007J\u0018\u0010%\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010)\u001a\u00020*H\u0007J \u0010%\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020\u0019H\u0007J\u0018\u0010%\u001a\u00020\b2\u0006\u0010\u0015\u001a\u00020\b2\u0006\u0010.\u001a\u00020/H\u0007R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u00020\b8\u0006X\u0004¢\u0006\u0002\n\u0000¨\u00060"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState$Companion;", "", "()V", "MAX_OUTGOING_GROUP_RING_DURATION", "", "SMALL_GROUP_MAX", "", "STARTING_STATE", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState;", "describeGroupMembers", "", "context", "Landroid/content/Context;", "oneParticipant", "twoParticipants", "multipleParticipants", "members", "", "Lorg/thoughtcrime/securesms/groups/ui/GroupMemberEntry$FullMember;", "determineLocalRenderMode", "Lorg/thoughtcrime/securesms/components/webrtc/WebRtcLocalRenderState;", "oldState", "localParticipant", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "isInPip", "", "showVideoForOutgoing", "isNonIdleGroupCall", "callState", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel$State;", "numberOfRemoteParticipants", "isViewingFocusedParticipant", "isExpanded", "getFocusedParticipant", "participants", "setExpanded", "expanded", "update", "groupMembers", "selectedPage", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsState$SelectedPage;", "foldableState", "Lorg/thoughtcrime/securesms/components/webrtc/WebRtcControls$FoldableState;", "webRtcViewModel", "Lorg/thoughtcrime/securesms/events/WebRtcViewModel;", "enableVideo", "ephemeralState", "Lorg/thoughtcrime/securesms/service/webrtc/state/WebRtcEphemeralState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
            if (r22.getState() != org.thoughtcrime.securesms.events.WebRtcViewModel.State.CALL_OUTGOING) goto L_0x0028;
         */
        @kotlin.jvm.JvmStatic
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final org.thoughtcrime.securesms.components.webrtc.CallParticipantsState update(org.thoughtcrime.securesms.components.webrtc.CallParticipantsState r21, org.thoughtcrime.securesms.events.WebRtcViewModel r22, boolean r23) {
            /*
                r20 = this;
                java.lang.String r0 = "oldState"
                r13 = r21
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r13, r0)
                java.lang.String r0 = "webRtcViewModel"
                r15 = r22
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r15, r0)
                boolean r0 = org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.access$getShowVideoForOutgoing$p(r21)
                r1 = 1
                r2 = 0
                if (r23 == 0) goto L_0x0020
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r0 = r22.getState()
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r3 = org.thoughtcrime.securesms.events.WebRtcViewModel.State.CALL_OUTGOING
                if (r0 != r3) goto L_0x0028
                r0 = 1
                goto L_0x0029
            L_0x0020:
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r3 = r22.getState()
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r4 = org.thoughtcrime.securesms.events.WebRtcViewModel.State.CALL_OUTGOING
                if (r3 == r4) goto L_0x0029
            L_0x0028:
                r0 = 0
            L_0x0029:
                boolean r3 = r21.isInOutgoingRingingMode()
                if (r3 == 0) goto L_0x004b
                long r3 = r22.getCallConnectedTime()
                long r5 = org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.MAX_OUTGOING_GROUP_RING_DURATION
                long r3 = r3 + r5
                long r5 = java.lang.System.currentTimeMillis()
                int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r7 <= 0) goto L_0x007b
                java.util.List r3 = r22.getRemoteParticipants()
                int r3 = r3.size()
                if (r3 != 0) goto L_0x007b
            L_0x0048:
                r19 = 1
                goto L_0x007d
            L_0x004b:
                boolean r3 = r21.getRingGroup()
                if (r3 == 0) goto L_0x007b
                long r3 = r22.getCallConnectedTime()
                long r5 = org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.MAX_OUTGOING_GROUP_RING_DURATION
                long r3 = r3 + r5
                long r5 = java.lang.System.currentTimeMillis()
                int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r7 <= 0) goto L_0x007b
                java.util.List r3 = r22.getRemoteParticipants()
                int r3 = r3.size()
                if (r3 != 0) goto L_0x007b
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r3 = r21.getCallState()
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r4 = org.thoughtcrime.securesms.events.WebRtcViewModel.State.CALL_OUTGOING
                if (r3 != r4) goto L_0x007b
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r3 = r22.getState()
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r4 = org.thoughtcrime.securesms.events.WebRtcViewModel.State.CALL_CONNECTED
                if (r3 != r4) goto L_0x007b
                goto L_0x0048
            L_0x007b:
                r19 = 0
            L_0x007d:
                org.thoughtcrime.securesms.events.CallParticipant r3 = r22.getLocalParticipant()
                r4 = 0
                org.thoughtcrime.securesms.events.WebRtcViewModel$GroupCallState r1 = r22.getGroupState()
                boolean r6 = r1.isNotIdle()
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r7 = r22.getState()
                java.util.List r1 = r22.getRemoteParticipants()
                int r8 = r1.size()
                r9 = 0
                r10 = 0
                r11 = 388(0x184, float:5.44E-43)
                r12 = 0
                r1 = r20
                r2 = r21
                r5 = r0
                org.thoughtcrime.securesms.components.webrtc.WebRtcLocalRenderState r7 = determineLocalRenderMode$default(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12)
                org.thoughtcrime.securesms.events.WebRtcViewModel$State r2 = r22.getState()
                org.thoughtcrime.securesms.events.WebRtcViewModel$GroupCallState r3 = r22.getGroupState()
                org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection r1 = org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.access$getRemoteParticipants$p(r21)
                java.util.List r4 = r22.getRemoteParticipants()
                org.thoughtcrime.securesms.service.webrtc.collections.ParticipantCollection r1 = r1.getNext(r4)
                r4 = r1
                org.thoughtcrime.securesms.events.CallParticipant r5 = r22.getLocalParticipant()
                java.util.List r6 = r22.getRemoteParticipants()
                r9 = r20
                org.thoughtcrime.securesms.events.CallParticipant r6 = r9.getFocusedParticipant(r6)
                com.annimon.stream.OptionalLong r11 = r22.getRemoteDevicesCount()
                boolean r14 = r22.shouldRingGroup()
                org.thoughtcrime.securesms.recipients.Recipient r15 = r22.getRingerRecipient()
                java.lang.String r8 = "getNext(webRtcViewModel.remoteParticipants)"
                kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r8)
                r8 = 0
                r16 = 0
                r17 = 17728(0x4540, float:2.4842E-41)
                r18 = 0
                r1 = r21
                r9 = r0
                r13 = r19
                org.thoughtcrime.securesms.components.webrtc.CallParticipantsState r0 = org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.copy$default(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.webrtc.CallParticipantsState.Companion.update(org.thoughtcrime.securesms.components.webrtc.CallParticipantsState, org.thoughtcrime.securesms.events.WebRtcViewModel, boolean):org.thoughtcrime.securesms.components.webrtc.CallParticipantsState");
        }

        @JvmStatic
        public final CallParticipantsState update(CallParticipantsState callParticipantsState, boolean z) {
            Intrinsics.checkNotNullParameter(callParticipantsState, "oldState");
            return CallParticipantsState.copy$default(callParticipantsState, null, null, null, null, null, determineLocalRenderMode$default(this, callParticipantsState, null, z, false, false, null, 0, false, false, 506, null), z, false, false, null, null, false, false, null, null, 32671, null);
        }

        @JvmStatic
        public final CallParticipantsState setExpanded(CallParticipantsState callParticipantsState, boolean z) {
            Intrinsics.checkNotNullParameter(callParticipantsState, "oldState");
            return CallParticipantsState.copy$default(callParticipantsState, null, null, null, null, null, determineLocalRenderMode$default(this, callParticipantsState, null, false, false, false, null, 0, false, z, 254, null), false, false, false, null, null, false, false, null, null, 32735, null);
        }

        @JvmStatic
        public final CallParticipantsState update(CallParticipantsState callParticipantsState, SelectedPage selectedPage) {
            Intrinsics.checkNotNullParameter(callParticipantsState, "oldState");
            Intrinsics.checkNotNullParameter(selectedPage, "selectedPage");
            SelectedPage selectedPage2 = SelectedPage.FOCUSED;
            return CallParticipantsState.copy$default(callParticipantsState, null, null, null, null, null, determineLocalRenderMode$default(this, callParticipantsState, null, false, false, false, null, 0, selectedPage == selectedPage2, false, 382, null), false, false, selectedPage == selectedPage2, null, null, false, false, null, null, 32479, null);
        }

        @JvmStatic
        public final CallParticipantsState update(CallParticipantsState callParticipantsState, WebRtcControls.FoldableState foldableState) {
            Intrinsics.checkNotNullParameter(callParticipantsState, "oldState");
            Intrinsics.checkNotNullParameter(foldableState, "foldableState");
            return CallParticipantsState.copy$default(callParticipantsState, null, null, null, null, null, determineLocalRenderMode$default(this, callParticipantsState, null, false, false, false, null, 0, false, false, 510, null), false, false, false, null, foldableState, false, false, null, null, 31711, null);
        }

        @JvmStatic
        public final CallParticipantsState update(CallParticipantsState callParticipantsState, List<GroupMemberEntry.FullMember> list) {
            Intrinsics.checkNotNullParameter(callParticipantsState, "oldState");
            Intrinsics.checkNotNullParameter(list, "groupMembers");
            return CallParticipantsState.copy$default(callParticipantsState, null, null, null, null, null, null, false, false, false, null, null, false, false, null, list, 16383, null);
        }

        /* renamed from: update$lambda-0 */
        public static final CallParticipant m1278update$lambda0(WebRtcEphemeralState webRtcEphemeralState, CallParticipant callParticipant) {
            Intrinsics.checkNotNullParameter(webRtcEphemeralState, "$ephemeralState");
            Intrinsics.checkNotNullExpressionValue(callParticipant, "p");
            return CallParticipant.copy$default(callParticipant, null, null, null, null, null, false, false, false, 0, webRtcEphemeralState.getRemoteAudioLevels().get(callParticipant.getCallParticipantId()), false, 0, false, null, 15871, null);
        }

        @JvmStatic
        public final CallParticipantsState update(CallParticipantsState callParticipantsState, WebRtcEphemeralState webRtcEphemeralState) {
            Intrinsics.checkNotNullParameter(callParticipantsState, "oldState");
            Intrinsics.checkNotNullParameter(webRtcEphemeralState, "ephemeralState");
            ParticipantCollection map = callParticipantsState.remoteParticipants.map(new CallParticipantsState$Companion$$ExternalSyntheticLambda0(webRtcEphemeralState));
            Intrinsics.checkNotNullExpressionValue(map, "oldState.remoteParticipa…s[p.callParticipantId]) }");
            return CallParticipantsState.copy$default(callParticipantsState, null, null, map, CallParticipant.copy$default(callParticipantsState.getLocalParticipant(), null, null, null, null, null, false, false, false, 0, webRtcEphemeralState.getLocalAudioLevel(), false, 0, false, null, 15871, null), CallParticipant.copy$default(callParticipantsState.getFocusedParticipant(), null, null, null, null, null, false, false, false, 0, webRtcEphemeralState.getRemoteAudioLevels().get(callParticipantsState.getFocusedParticipant().getCallParticipantId()), false, 0, false, null, 15871, null), null, false, false, false, null, null, false, false, null, null, 32739, null);
        }

        static /* synthetic */ WebRtcLocalRenderState determineLocalRenderMode$default(Companion companion, CallParticipantsState callParticipantsState, CallParticipant callParticipant, boolean z, boolean z2, boolean z3, WebRtcViewModel.State state, int i, boolean z4, boolean z5, int i2, Object obj) {
            boolean z6;
            CallParticipant localParticipant = (i2 & 2) != 0 ? callParticipantsState.getLocalParticipant() : callParticipant;
            boolean isInPipMode = (i2 & 4) != 0 ? callParticipantsState.isInPipMode() : z;
            if ((i2 & 8) != 0) {
                z6 = callParticipantsState.showVideoForOutgoing;
            } else {
                z6 = z2;
            }
            return companion.determineLocalRenderMode(callParticipantsState, localParticipant, isInPipMode, z6, (i2 & 16) != 0 ? callParticipantsState.getGroupCallState().isNotIdle() : z3, (i2 & 32) != 0 ? callParticipantsState.getCallState() : state, (i2 & 64) != 0 ? callParticipantsState.getAllRemoteParticipants().size() : i, (i2 & 128) != 0 ? callParticipantsState.isViewingFocusedParticipant() : z4, (i2 & 256) != 0 ? callParticipantsState.getLocalRenderState() == WebRtcLocalRenderState.EXPANDED : z5);
        }

        private final WebRtcLocalRenderState determineLocalRenderMode(CallParticipantsState callParticipantsState, CallParticipant callParticipant, boolean z, boolean z2, boolean z3, WebRtcViewModel.State state, int i, boolean z4, boolean z5) {
            WebRtcLocalRenderState webRtcLocalRenderState;
            boolean z6 = (i == 0 || !z) && (z3 || callParticipant.isVideoEnabled());
            WebRtcLocalRenderState webRtcLocalRenderState2 = WebRtcLocalRenderState.GONE;
            if (z5 && (callParticipant.isVideoEnabled() || z3)) {
                return WebRtcLocalRenderState.EXPANDED;
            }
            if (z6 || z2) {
                if (state == WebRtcViewModel.State.CALL_CONNECTED || state == WebRtcViewModel.State.CALL_RECONNECTING) {
                    webRtcLocalRenderState = (z4 || i > 1) ? WebRtcLocalRenderState.SMALLER_RECTANGLE : i == 1 ? WebRtcLocalRenderState.SMALL_RECTANGLE : callParticipant.isVideoEnabled() ? WebRtcLocalRenderState.LARGE : WebRtcLocalRenderState.LARGE_NO_VIDEO;
                } else if (state == WebRtcViewModel.State.CALL_INCOMING || state == WebRtcViewModel.State.CALL_DISCONNECTED) {
                    return webRtcLocalRenderState2;
                } else {
                    webRtcLocalRenderState = callParticipant.isVideoEnabled() ? WebRtcLocalRenderState.LARGE : WebRtcLocalRenderState.LARGE_NO_VIDEO;
                }
                return webRtcLocalRenderState;
            } else if (state == WebRtcViewModel.State.CALL_PRE_JOIN) {
                return WebRtcLocalRenderState.LARGE_NO_VIDEO;
            } else {
                return webRtcLocalRenderState2;
            }
        }

        public final String describeGroupMembers(Context context, int i, int i2, int i3, List<GroupMemberEntry.FullMember> list) {
            ArrayList arrayList = new ArrayList();
            Iterator<T> it = list.iterator();
            while (true) {
                boolean z = false;
                if (!it.hasNext()) {
                    break;
                }
                Object next = it.next();
                GroupMemberEntry.FullMember fullMember = (GroupMemberEntry.FullMember) next;
                if (fullMember.getMember().isSelf() || fullMember.getMember().isBlocked()) {
                    z = true;
                }
                if (!z) {
                    arrayList.add(next);
                }
            }
            int size = arrayList.size();
            if (size == 0) {
                return "";
            }
            if (size == 1) {
                String string = context.getString(i, ((GroupMemberEntry.FullMember) arrayList.get(0)).getMember().getShortDisplayName(context));
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(\n     …ayName(context)\n        )");
                return string;
            } else if (size != 2) {
                int size2 = arrayList.size() - 2;
                String quantityString = context.getResources().getQuantityString(i3, size2, ((GroupMemberEntry.FullMember) arrayList.get(0)).getMember().getShortDisplayName(context), ((GroupMemberEntry.FullMember) arrayList.get(1)).getMember().getShortDisplayName(context), Integer.valueOf(size2));
                Intrinsics.checkNotNullExpressionValue(quantityString, "{\n          val others =…ers\n          )\n        }");
                return quantityString;
            } else {
                String string2 = context.getString(i2, ((GroupMemberEntry.FullMember) arrayList.get(0)).getMember().getShortDisplayName(context), ((GroupMemberEntry.FullMember) arrayList.get(1)).getMember().getShortDisplayName(context));
                Intrinsics.checkNotNullExpressionValue(string2, "context.getString(\n     …ayName(context)\n        )");
                return string2;
            }
        }

        private final CallParticipant getFocusedParticipant(List<CallParticipant> list) {
            Object obj;
            List list2 = CollectionsKt___CollectionsKt.sortedWith(list, new CallParticipantsState$Companion$getFocusedParticipant$$inlined$sortedByDescending$1());
            if (list2.isEmpty()) {
                return CallParticipant.EMPTY;
            }
            Iterator it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (((CallParticipant) obj).isScreenSharing()) {
                    break;
                }
            }
            CallParticipant callParticipant = (CallParticipant) obj;
            return callParticipant == null ? (CallParticipant) list2.get(0) : callParticipant;
        }
    }
}
