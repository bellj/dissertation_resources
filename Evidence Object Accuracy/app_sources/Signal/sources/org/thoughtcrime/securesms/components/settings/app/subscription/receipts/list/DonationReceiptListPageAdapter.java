package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import androidx.fragment.app.Fragment;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;

/* compiled from: DonationReceiptListPageAdapter.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\u0007H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageAdapter;", "Landroidx/viewpager2/adapter/FragmentStateAdapter;", "fragment", "Landroidx/fragment/app/Fragment;", "(Landroidx/fragment/app/Fragment;)V", "createFragment", "position", "", "getItemCount", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListPageAdapter extends FragmentStateAdapter {
    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return 4;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DonationReceiptListPageAdapter(Fragment fragment) {
        super(fragment);
        Intrinsics.checkNotNullParameter(fragment, "fragment");
    }

    @Override // androidx.viewpager2.adapter.FragmentStateAdapter
    public Fragment createFragment(int i) {
        if (i == 0) {
            return DonationReceiptListPageFragment.Companion.create(null);
        }
        if (i == 1) {
            return DonationReceiptListPageFragment.Companion.create(DonationReceiptRecord.Type.RECURRING);
        }
        if (i == 2) {
            return DonationReceiptListPageFragment.Companion.create(DonationReceiptRecord.Type.BOOST);
        }
        if (i == 3) {
            return DonationReceiptListPageFragment.Companion.create(DonationReceiptRecord.Type.GIFT);
        }
        throw new IllegalStateException(("Unsupported position " + i).toString());
    }
}
