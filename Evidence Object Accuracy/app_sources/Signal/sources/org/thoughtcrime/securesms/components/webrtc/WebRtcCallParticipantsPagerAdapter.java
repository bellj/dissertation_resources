package org.thoughtcrime.securesms.components.webrtc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.webrtc.RendererCommon;

/* loaded from: classes4.dex */
public class WebRtcCallParticipantsPagerAdapter extends ListAdapter<WebRtcCallParticipantsPage, ViewHolder> {
    private static final int VIEW_TYPE_MULTI;
    private static final int VIEW_TYPE_SINGLE;
    private final Runnable onPageClicked;

    public WebRtcCallParticipantsPagerAdapter(Runnable runnable) {
        super(new DiffCallback());
        this.onPageClicked = runnable;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        recyclerView.setOverScrollMode(2);
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ViewHolder viewHolder;
        if (i == 0) {
            viewHolder = new MultipleParticipantViewHolder((CallParticipantsLayout) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.webrtc_call_participants_layout, viewGroup, false));
        } else if (i == 1) {
            viewHolder = new SingleParticipantViewHolder((CallParticipantView) LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.call_participant_item, viewGroup, false));
        } else {
            throw new IllegalArgumentException("Unsupported viewType: " + i);
        }
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallParticipantsPagerAdapter$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallParticipantsPagerAdapter.m1280$r8$lambda$JF7Hcp3XEdjumtEynk321VkwcU(WebRtcCallParticipantsPagerAdapter.this, view);
            }
        });
        return viewHolder;
    }

    public /* synthetic */ void lambda$onCreateViewHolder$0(View view) {
        this.onPageClicked.run();
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(getItem(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return getItem(i).isSpeaker() ? 1 : 0;
    }

    /* loaded from: classes4.dex */
    public static abstract class ViewHolder extends RecyclerView.ViewHolder {
        abstract void bind(WebRtcCallParticipantsPage webRtcCallParticipantsPage);

        public ViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static class MultipleParticipantViewHolder extends ViewHolder {
        private final CallParticipantsLayout callParticipantsLayout;

        private MultipleParticipantViewHolder(CallParticipantsLayout callParticipantsLayout) {
            super(callParticipantsLayout);
            this.callParticipantsLayout = callParticipantsLayout;
        }

        void bind(WebRtcCallParticipantsPage webRtcCallParticipantsPage) {
            this.callParticipantsLayout.update(webRtcCallParticipantsPage.getCallParticipants(), webRtcCallParticipantsPage.getFocusedParticipant(), webRtcCallParticipantsPage.isRenderInPip(), webRtcCallParticipantsPage.isPortrait(), webRtcCallParticipantsPage.isIncomingRing(), webRtcCallParticipantsPage.getNavBarBottomInset(), webRtcCallParticipantsPage.getLayoutStrategy());
        }
    }

    /* loaded from: classes4.dex */
    public static class SingleParticipantViewHolder extends ViewHolder {
        private final CallParticipantView callParticipantView;

        private SingleParticipantViewHolder(CallParticipantView callParticipantView) {
            super(callParticipantView);
            this.callParticipantView = callParticipantView;
            ViewGroup.LayoutParams layoutParams = callParticipantView.getLayoutParams();
            layoutParams.height = -1;
            layoutParams.width = -1;
            callParticipantView.setLayoutParams(layoutParams);
        }

        void bind(WebRtcCallParticipantsPage webRtcCallParticipantsPage) {
            CallParticipant callParticipant = webRtcCallParticipantsPage.getCallParticipants().get(0);
            this.callParticipantView.setCallParticipant(callParticipant);
            this.callParticipantView.setRenderInPip(webRtcCallParticipantsPage.isRenderInPip());
            if (callParticipant.isScreenSharing()) {
                this.callParticipantView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
            } else {
                this.callParticipantView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static final class DiffCallback extends DiffUtil.ItemCallback<WebRtcCallParticipantsPage> {
        private DiffCallback() {
        }

        public boolean areItemsTheSame(WebRtcCallParticipantsPage webRtcCallParticipantsPage, WebRtcCallParticipantsPage webRtcCallParticipantsPage2) {
            return webRtcCallParticipantsPage.isSpeaker() == webRtcCallParticipantsPage2.isSpeaker();
        }

        public boolean areContentsTheSame(WebRtcCallParticipantsPage webRtcCallParticipantsPage, WebRtcCallParticipantsPage webRtcCallParticipantsPage2) {
            return webRtcCallParticipantsPage.equals(webRtcCallParticipantsPage2);
        }
    }
}
