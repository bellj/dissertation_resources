package org.thoughtcrime.securesms.components.emoji;

import android.text.InputFilter;
import android.text.Spannable;
import android.text.Spanned;
import android.text.TextUtils;
import android.widget.TextView;

/* loaded from: classes4.dex */
public class EmojiFilter implements InputFilter {
    private boolean jumboEmoji;
    private TextView view;

    public EmojiFilter(TextView textView, boolean z) {
        this.view = textView;
        this.jumboEmoji = z;
    }

    @Override // android.text.InputFilter
    public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
        char[] cArr = new char[i2 - i];
        TextUtils.getChars(charSequence, i, i2, cArr, 0);
        Spannable emojify = EmojiProvider.emojify(new String(cArr), this.view, this.jumboEmoji);
        if ((charSequence instanceof Spanned) && emojify != null) {
            TextUtils.copySpansFrom((Spanned) charSequence, i, i2, null, emojify, 0);
        }
        return emojify;
    }
}
