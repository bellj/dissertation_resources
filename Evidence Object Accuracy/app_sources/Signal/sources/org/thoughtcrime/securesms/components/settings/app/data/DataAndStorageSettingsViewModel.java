package org.thoughtcrime.securesms.components.settings.app.data;

import android.content.SharedPreferences;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.thoughtcrime.securesms.webrtc.CallBandwidthMode;

/* compiled from: DataAndStorageSettingsViewModel.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u001dB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\n\u001a\u00020\tH\u0002J\b\u0010\u000e\u001a\u00020\u000fH\u0002J\u0006\u0010\u0010\u001a\u00020\u000fJ\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u0014\u0010\u0014\u001a\u00020\u000f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016J\u0014\u0010\u0018\u001a\u00020\u000f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016J\u000e\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\u001a\u001a\u00020\u001bJ\u0014\u0010\u001c\u001a\u00020\u000f2\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00170\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "sharedPreferences", "Landroid/content/SharedPreferences;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsRepository;", "(Landroid/content/SharedPreferences;Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsRepository;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getStateAndCopyStorageUsage", "", "refresh", "setCallBandwidthMode", "callBandwidthMode", "Lorg/thoughtcrime/securesms/webrtc/CallBandwidthMode;", "setMobileAutoDownloadValues", "resultSet", "", "", "setRoamingAutoDownloadValues", "setSentMediaQuality", "sentMediaQuality", "Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "setWifiAutoDownloadValues", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DataAndStorageSettingsViewModel extends ViewModel {
    private final DataAndStorageSettingsRepository repository;
    private final SharedPreferences sharedPreferences;
    private final LiveData<DataAndStorageSettingsState> state;
    private final Store<DataAndStorageSettingsState> store;

    public DataAndStorageSettingsViewModel(SharedPreferences sharedPreferences, DataAndStorageSettingsRepository dataAndStorageSettingsRepository) {
        Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
        Intrinsics.checkNotNullParameter(dataAndStorageSettingsRepository, "repository");
        this.sharedPreferences = sharedPreferences;
        this.repository = dataAndStorageSettingsRepository;
        Store<DataAndStorageSettingsState> store = new Store<>(getState());
        this.store = store;
        LiveData<DataAndStorageSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    /* renamed from: getState */
    public final LiveData<DataAndStorageSettingsState> m649getState() {
        return this.state;
    }

    public final void refresh() {
        this.repository.getTotalStorageUse(new DataAndStorageSettingsViewModel$refresh$1(this));
    }

    public final void setMobileAutoDownloadValues(Set<String> set) {
        Intrinsics.checkNotNullParameter(set, "resultSet");
        this.sharedPreferences.edit().putStringSet(TextSecurePreferences.MEDIA_DOWNLOAD_MOBILE_PREF, set).apply();
        getStateAndCopyStorageUsage();
    }

    public final void setWifiAutoDownloadValues(Set<String> set) {
        Intrinsics.checkNotNullParameter(set, "resultSet");
        this.sharedPreferences.edit().putStringSet(TextSecurePreferences.MEDIA_DOWNLOAD_WIFI_PREF, set).apply();
        getStateAndCopyStorageUsage();
    }

    public final void setRoamingAutoDownloadValues(Set<String> set) {
        Intrinsics.checkNotNullParameter(set, "resultSet");
        this.sharedPreferences.edit().putStringSet(TextSecurePreferences.MEDIA_DOWNLOAD_ROAMING_PREF, set).apply();
        getStateAndCopyStorageUsage();
    }

    public final void setCallBandwidthMode(CallBandwidthMode callBandwidthMode) {
        Intrinsics.checkNotNullParameter(callBandwidthMode, "callBandwidthMode");
        SignalStore.settings().setCallBandwidthMode(callBandwidthMode);
        ApplicationDependencies.getSignalCallManager().bandwidthModeUpdate();
        getStateAndCopyStorageUsage();
    }

    public final void setSentMediaQuality(SentMediaQuality sentMediaQuality) {
        Intrinsics.checkNotNullParameter(sentMediaQuality, "sentMediaQuality");
        SignalStore.settings().setSentMediaQuality(sentMediaQuality);
        getStateAndCopyStorageUsage();
    }

    private final void getStateAndCopyStorageUsage() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DataAndStorageSettingsViewModel.m648getStateAndCopyStorageUsage$lambda0(DataAndStorageSettingsViewModel.this, (DataAndStorageSettingsState) obj);
            }
        });
    }

    /* renamed from: getStateAndCopyStorageUsage$lambda-0 */
    public static final DataAndStorageSettingsState m648getStateAndCopyStorageUsage$lambda0(DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel, DataAndStorageSettingsState dataAndStorageSettingsState) {
        Intrinsics.checkNotNullParameter(dataAndStorageSettingsViewModel, "this$0");
        return r1.copy((r18 & 1) != 0 ? r1.totalStorageUse : dataAndStorageSettingsState.getTotalStorageUse(), (r18 & 2) != 0 ? r1.mobileAutoDownloadValues : null, (r18 & 4) != 0 ? r1.wifiAutoDownloadValues : null, (r18 & 8) != 0 ? r1.roamingAutoDownloadValues : null, (r18 & 16) != 0 ? r1.callBandwidthMode : null, (r18 & 32) != 0 ? r1.isProxyEnabled : false, (r18 & 64) != 0 ? dataAndStorageSettingsViewModel.getState().sentMediaQuality : null);
    }

    public final DataAndStorageSettingsState getState() {
        Set<String> mobileMediaDownloadAllowed = TextSecurePreferences.getMobileMediaDownloadAllowed(ApplicationDependencies.getApplication());
        Intrinsics.checkNotNullExpressionValue(mobileMediaDownloadAllowed, "getMobileMediaDownloadAl…es.getApplication()\n    )");
        Set<String> wifiMediaDownloadAllowed = TextSecurePreferences.getWifiMediaDownloadAllowed(ApplicationDependencies.getApplication());
        Intrinsics.checkNotNullExpressionValue(wifiMediaDownloadAllowed, "getWifiMediaDownloadAllo…es.getApplication()\n    )");
        Set<String> roamingMediaDownloadAllowed = TextSecurePreferences.getRoamingMediaDownloadAllowed(ApplicationDependencies.getApplication());
        Intrinsics.checkNotNullExpressionValue(roamingMediaDownloadAllowed, "getRoamingMediaDownloadA…es.getApplication()\n    )");
        CallBandwidthMode callBandwidthMode = SignalStore.settings().getCallBandwidthMode();
        Intrinsics.checkNotNullExpressionValue(callBandwidthMode, "settings().callBandwidthMode");
        boolean isProxyEnabled = SignalStore.proxy().isProxyEnabled();
        SentMediaQuality sentMediaQuality = SignalStore.settings().getSentMediaQuality();
        Intrinsics.checkNotNullExpressionValue(sentMediaQuality, "settings().sentMediaQuality");
        return new DataAndStorageSettingsState(0, mobileMediaDownloadAllowed, wifiMediaDownloadAllowed, roamingMediaDownloadAllowed, callBandwidthMode, isProxyEnabled, sentMediaQuality);
    }

    /* compiled from: DataAndStorageSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "sharedPreferences", "Landroid/content/SharedPreferences;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsRepository;", "(Landroid/content/SharedPreferences;Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DataAndStorageSettingsRepository repository;
        private final SharedPreferences sharedPreferences;

        public Factory(SharedPreferences sharedPreferences, DataAndStorageSettingsRepository dataAndStorageSettingsRepository) {
            Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
            Intrinsics.checkNotNullParameter(dataAndStorageSettingsRepository, "repository");
            this.sharedPreferences = sharedPreferences;
            this.repository = dataAndStorageSettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new DataAndStorageSettingsViewModel(this.sharedPreferences, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
