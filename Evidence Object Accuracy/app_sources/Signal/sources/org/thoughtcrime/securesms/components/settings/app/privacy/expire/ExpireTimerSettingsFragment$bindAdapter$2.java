package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import kotlin.Metadata;
import kotlin.jvm.internal.PropertyReference1Impl;

/* compiled from: ExpireTimerSettingsFragment.kt */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
/* synthetic */ class ExpireTimerSettingsFragment$bindAdapter$2 extends PropertyReference1Impl {
    public static final ExpireTimerSettingsFragment$bindAdapter$2 INSTANCE = new ExpireTimerSettingsFragment$bindAdapter$2();

    ExpireTimerSettingsFragment$bindAdapter$2() {
        super(ExpireTimerSettingsState.class, "saveState", "getSaveState()Lorg/thoughtcrime/securesms/util/livedata/ProcessState;", 0);
    }

    @Override // kotlin.jvm.internal.PropertyReference1Impl, kotlin.reflect.KProperty1
    public Object get(Object obj) {
        return ((ExpireTimerSettingsState) obj).getSaveState();
    }
}
