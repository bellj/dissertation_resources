package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.appcompat.widget.AppCompatImageButton;

/* loaded from: classes4.dex */
public class RepeatableImageKey extends AppCompatImageButton {
    private KeyEventListener listener;

    /* loaded from: classes4.dex */
    public interface KeyEventListener {
        void onKeyEvent();
    }

    public RepeatableImageKey(Context context) {
        super(context);
        init();
    }

    public RepeatableImageKey(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    public RepeatableImageKey(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init();
    }

    private void init() {
        setOnClickListener(new RepeaterClickListener());
        setOnTouchListener(new RepeaterTouchListener());
    }

    public void setOnKeyEventListener(KeyEventListener keyEventListener) {
        this.listener = keyEventListener;
    }

    public void notifyListener() {
        KeyEventListener keyEventListener = this.listener;
        if (keyEventListener != null) {
            keyEventListener.onKeyEvent();
        }
    }

    /* loaded from: classes4.dex */
    public class RepeaterClickListener implements View.OnClickListener {
        private RepeaterClickListener() {
            RepeatableImageKey.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            RepeatableImageKey.this.notifyListener();
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class Repeater implements Runnable {
        private Repeater() {
            RepeatableImageKey.this = r1;
        }

        @Override // java.lang.Runnable
        public void run() {
            RepeatableImageKey.this.notifyListener();
            RepeatableImageKey.this.postDelayed(this, (long) ViewConfiguration.getKeyRepeatDelay());
        }
    }

    /* loaded from: classes4.dex */
    public class RepeaterTouchListener implements View.OnTouchListener {
        private final Repeater repeater;

        RepeaterTouchListener() {
            RepeatableImageKey.this = r3;
            this.repeater = new Repeater();
        }

        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            if (action == 0) {
                view.postDelayed(this.repeater, (long) ViewConfiguration.getKeyRepeatTimeout());
                RepeatableImageKey.this.performHapticFeedback(3);
                return false;
            } else if (action != 1 && action != 3) {
                return false;
            } else {
                view.removeCallbacks(this.repeater);
                return false;
            }
        }
    }
}
