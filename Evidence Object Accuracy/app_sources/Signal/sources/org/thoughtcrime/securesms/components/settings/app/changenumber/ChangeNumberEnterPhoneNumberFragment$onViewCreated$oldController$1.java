package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.view.View;
import android.widget.ScrollView;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.LabeledEditText;
import org.thoughtcrime.securesms.registration.fragments.CountryPickerFragmentArgs;
import org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberEnterPhoneNumberFragment.kt */
@Metadata(d1 = {"\u0000)\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\b\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000bH\u0016J\u0010\u0010\f\u001a\u00020\u00032\u0006\u0010\r\u001a\u00020\u000eH\u0016¨\u0006\u000f"}, d2 = {"org/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberEnterPhoneNumberFragment$onViewCreated$oldController$1", "Lorg/thoughtcrime/securesms/registration/util/RegistrationNumberInputController$Callbacks;", "onNumberFocused", "", "onNumberInputDone", "view", "Landroid/view/View;", "onNumberInputNext", "onPickCountry", "setCountry", "countryCode", "", "setNationalNumber", "number", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberEnterPhoneNumberFragment$onViewCreated$oldController$1 implements RegistrationNumberInputController.Callbacks {
    final /* synthetic */ ChangeNumberEnterPhoneNumberFragment this$0;

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onNumberInputDone(View view) {
        Intrinsics.checkNotNullParameter(view, "view");
    }

    public ChangeNumberEnterPhoneNumberFragment$onViewCreated$oldController$1(ChangeNumberEnterPhoneNumberFragment changeNumberEnterPhoneNumberFragment) {
        this.this$0 = changeNumberEnterPhoneNumberFragment;
    }

    /* renamed from: onNumberFocused$lambda-0 */
    public static final void m589onNumberFocused$lambda0(ChangeNumberEnterPhoneNumberFragment changeNumberEnterPhoneNumberFragment) {
        Intrinsics.checkNotNullParameter(changeNumberEnterPhoneNumberFragment, "this$0");
        ScrollView scrollView = changeNumberEnterPhoneNumberFragment.scrollView;
        LabeledEditText labeledEditText = null;
        if (scrollView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrollView");
            scrollView = null;
        }
        LabeledEditText labeledEditText2 = changeNumberEnterPhoneNumberFragment.oldNumber;
        if (labeledEditText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("oldNumber");
        } else {
            labeledEditText = labeledEditText2;
        }
        scrollView.smoothScrollTo(0, labeledEditText.getBottom());
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onNumberFocused() {
        ScrollView scrollView = this.this$0.scrollView;
        if (scrollView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("scrollView");
            scrollView = null;
        }
        scrollView.postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterPhoneNumberFragment$onViewCreated$oldController$1$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ChangeNumberEnterPhoneNumberFragment$onViewCreated$oldController$1.m589onNumberFocused$lambda0(ChangeNumberEnterPhoneNumberFragment.this);
            }
        }, 250);
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onNumberInputNext(View view) {
        Intrinsics.checkNotNullParameter(view, "view");
        LabeledEditText labeledEditText = this.this$0.newNumberCountryCode;
        if (labeledEditText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("newNumberCountryCode");
            labeledEditText = null;
        }
        labeledEditText.requestFocus();
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void onPickCountry(View view) {
        Intrinsics.checkNotNullParameter(view, "view");
        CountryPickerFragmentArgs build = new CountryPickerFragmentArgs.Builder().setResultKey("old_number_country").build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder().setResultKey(O…R_COUNTRY_SELECT).build()");
        SafeNavigation.safeNavigate(FragmentKt.findNavController(this.this$0), (int) R.id.action_enterPhoneNumberChangeFragment_to_countryPickerFragment, build.toBundle());
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void setNationalNumber(String str) {
        Intrinsics.checkNotNullParameter(str, "number");
        ChangeNumberViewModel changeNumberViewModel = this.this$0.viewModel;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        changeNumberViewModel.setOldNationalNumber(str);
    }

    @Override // org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController.Callbacks
    public void setCountry(int i) {
        ChangeNumberViewModel changeNumberViewModel = this.this$0.viewModel;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        ChangeNumberViewModel.setOldCountry$default(changeNumberViewModel, i, null, 2, null);
    }
}
