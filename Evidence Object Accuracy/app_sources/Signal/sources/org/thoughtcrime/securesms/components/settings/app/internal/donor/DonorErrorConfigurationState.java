package org.thoughtcrime.securesms.components.settings.app.internal.donor;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.donations.StripeDeclineCode;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.UnexpectedSubscriptionCancellation;

/* compiled from: DonorErrorConfigurationState.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B9\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\u0002\u0010\nJ\u000f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\tHÆ\u0003J=\u0010\u0017\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tHÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001J\t\u0010\u001d\u001a\u00020\u001eHÖ\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationState;", "", "badges", "", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "selectedBadge", "selectedUnexpectedSubscriptionCancellation", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation;", "selectedStripeDeclineCode", "Lorg/signal/donations/StripeDeclineCode$Code;", "(Ljava/util/List;Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation;Lorg/signal/donations/StripeDeclineCode$Code;)V", "getBadges", "()Ljava/util/List;", "getSelectedBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getSelectedStripeDeclineCode", "()Lorg/signal/donations/StripeDeclineCode$Code;", "getSelectedUnexpectedSubscriptionCancellation", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonorErrorConfigurationState {
    private final List<Badge> badges;
    private final Badge selectedBadge;
    private final StripeDeclineCode.Code selectedStripeDeclineCode;
    private final UnexpectedSubscriptionCancellation selectedUnexpectedSubscriptionCancellation;

    public DonorErrorConfigurationState() {
        this(null, null, null, null, 15, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DonorErrorConfigurationState copy$default(DonorErrorConfigurationState donorErrorConfigurationState, List list, Badge badge, UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation, StripeDeclineCode.Code code, int i, Object obj) {
        if ((i & 1) != 0) {
            list = donorErrorConfigurationState.badges;
        }
        if ((i & 2) != 0) {
            badge = donorErrorConfigurationState.selectedBadge;
        }
        if ((i & 4) != 0) {
            unexpectedSubscriptionCancellation = donorErrorConfigurationState.selectedUnexpectedSubscriptionCancellation;
        }
        if ((i & 8) != 0) {
            code = donorErrorConfigurationState.selectedStripeDeclineCode;
        }
        return donorErrorConfigurationState.copy(list, badge, unexpectedSubscriptionCancellation, code);
    }

    public final List<Badge> component1() {
        return this.badges;
    }

    public final Badge component2() {
        return this.selectedBadge;
    }

    public final UnexpectedSubscriptionCancellation component3() {
        return this.selectedUnexpectedSubscriptionCancellation;
    }

    public final StripeDeclineCode.Code component4() {
        return this.selectedStripeDeclineCode;
    }

    public final DonorErrorConfigurationState copy(List<Badge> list, Badge badge, UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation, StripeDeclineCode.Code code) {
        Intrinsics.checkNotNullParameter(list, "badges");
        return new DonorErrorConfigurationState(list, badge, unexpectedSubscriptionCancellation, code);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DonorErrorConfigurationState)) {
            return false;
        }
        DonorErrorConfigurationState donorErrorConfigurationState = (DonorErrorConfigurationState) obj;
        return Intrinsics.areEqual(this.badges, donorErrorConfigurationState.badges) && Intrinsics.areEqual(this.selectedBadge, donorErrorConfigurationState.selectedBadge) && this.selectedUnexpectedSubscriptionCancellation == donorErrorConfigurationState.selectedUnexpectedSubscriptionCancellation && this.selectedStripeDeclineCode == donorErrorConfigurationState.selectedStripeDeclineCode;
    }

    public int hashCode() {
        int hashCode = this.badges.hashCode() * 31;
        Badge badge = this.selectedBadge;
        int i = 0;
        int hashCode2 = (hashCode + (badge == null ? 0 : badge.hashCode())) * 31;
        UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation = this.selectedUnexpectedSubscriptionCancellation;
        int hashCode3 = (hashCode2 + (unexpectedSubscriptionCancellation == null ? 0 : unexpectedSubscriptionCancellation.hashCode())) * 31;
        StripeDeclineCode.Code code = this.selectedStripeDeclineCode;
        if (code != null) {
            i = code.hashCode();
        }
        return hashCode3 + i;
    }

    public String toString() {
        return "DonorErrorConfigurationState(badges=" + this.badges + ", selectedBadge=" + this.selectedBadge + ", selectedUnexpectedSubscriptionCancellation=" + this.selectedUnexpectedSubscriptionCancellation + ", selectedStripeDeclineCode=" + this.selectedStripeDeclineCode + ')';
    }

    public DonorErrorConfigurationState(List<Badge> list, Badge badge, UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation, StripeDeclineCode.Code code) {
        Intrinsics.checkNotNullParameter(list, "badges");
        this.badges = list;
        this.selectedBadge = badge;
        this.selectedUnexpectedSubscriptionCancellation = unexpectedSubscriptionCancellation;
        this.selectedStripeDeclineCode = code;
    }

    public /* synthetic */ DonorErrorConfigurationState(List list, Badge badge, UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation, StripeDeclineCode.Code code, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 2) != 0 ? null : badge, (i & 4) != 0 ? null : unexpectedSubscriptionCancellation, (i & 8) != 0 ? null : code);
    }

    public final List<Badge> getBadges() {
        return this.badges;
    }

    public final Badge getSelectedBadge() {
        return this.selectedBadge;
    }

    public final UnexpectedSubscriptionCancellation getSelectedUnexpectedSubscriptionCancellation() {
        return this.selectedUnexpectedSubscriptionCancellation;
    }

    public final StripeDeclineCode.Code getSelectedStripeDeclineCode() {
        return this.selectedStripeDeclineCode;
    }
}
