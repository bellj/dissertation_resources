package org.thoughtcrime.securesms.components.settings.conversation;

import android.database.Cursor;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ConversationSettingsState.kt */
@Metadata(d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b#\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001By\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u0011\u0012\b\b\u0002\u0010\u0012\u001a\u00020\r\u0012\b\b\u0002\u0010\u0013\u001a\u00020\r\u0012\u0006\u0010\u0014\u001a\u00020\u0015¢\u0006\u0002\u0010\u0016J\t\u0010)\u001a\u00020\u0003HÆ\u0003J\t\u0010*\u001a\u00020\rHÂ\u0003J\t\u0010+\u001a\u00020\u0015HÂ\u0003J\t\u0010,\u001a\u00020\u0005HÆ\u0003J\t\u0010-\u001a\u00020\u0007HÆ\u0003J\t\u0010.\u001a\u00020\tHÆ\u0003J\t\u0010/\u001a\u00020\u000bHÆ\u0003J\t\u00100\u001a\u00020\rHÆ\u0003J\u000b\u00101\u001a\u0004\u0018\u00010\u000fHÆ\u0003J\u000f\u00102\u001a\b\u0012\u0004\u0012\u00020\u00030\u0011HÆ\u0003J\t\u00103\u001a\u00020\rHÆ\u0003J\u00104\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u000e\b\u0002\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u00112\b\b\u0002\u0010\u0012\u001a\u00020\r2\b\b\u0002\u0010\u0013\u001a\u00020\r2\b\b\u0002\u0010\u0014\u001a\u00020\u0015HÆ\u0001J\u0013\u00105\u001a\u00020\r2\b\u00106\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00107\u001a\u00020\u000bHÖ\u0001J\u0006\u00108\u001a\u000209J\u0006\u0010:\u001a\u00020;J\t\u0010<\u001a\u00020=HÖ\u0001J\u001a\u0010>\u001a\u00020?2\u0012\u0010@\u001a\u000e\u0012\u0004\u0012\u000209\u0012\u0004\u0012\u00020?0AJ\u001a\u0010B\u001a\u00020?2\u0012\u0010@\u001a\u000e\u0012\u0004\u0012\u00020;\u0012\u0004\u0012\u00020?0AR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u0012\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001aR\u0011\u0010\u001e\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001aR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u000f¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00030\u0011¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u000e\u0010\u0013\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(¨\u0006C"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsState;", "", "threadId", "", "storyViewState", "Lorg/thoughtcrime/securesms/database/model/StoryViewState;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "buttonStripState", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;", "disappearingMessagesLifespan", "", "canModifyBlockedState", "", "sharedMedia", "Landroid/database/Cursor;", "sharedMediaIds", "", "displayInternalRecipientDetails", "sharedMediaLoaded", "specificSettingsState", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;", "(JLorg/thoughtcrime/securesms/database/model/StoryViewState;Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;IZLandroid/database/Cursor;Ljava/util/List;ZZLorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;)V", "getButtonStripState", "()Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;", "getCanModifyBlockedState", "()Z", "getDisappearingMessagesLifespan", "()I", "getDisplayInternalRecipientDetails", "isLoaded", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getSharedMedia", "()Landroid/database/Cursor;", "getSharedMediaIds", "()Ljava/util/List;", "getStoryViewState", "()Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "getThreadId", "()J", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "requireGroupSettingsState", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState$GroupSettingsState;", "requireRecipientSettingsState", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState$RecipientSettingsState;", "toString", "", "withGroupSettingsState", "", "consumer", "Lkotlin/Function1;", "withRecipientSettingsState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationSettingsState {
    private final ButtonStripPreference.State buttonStripState;
    private final boolean canModifyBlockedState;
    private final int disappearingMessagesLifespan;
    private final boolean displayInternalRecipientDetails;
    private final boolean isLoaded;
    private final Recipient recipient;
    private final Cursor sharedMedia;
    private final List<Long> sharedMediaIds;
    private final boolean sharedMediaLoaded;
    private final SpecificSettingsState specificSettingsState;
    private final StoryViewState storyViewState;
    private final long threadId;

    private final boolean component10() {
        return this.sharedMediaLoaded;
    }

    private final SpecificSettingsState component11() {
        return this.specificSettingsState;
    }

    public final long component1() {
        return this.threadId;
    }

    public final StoryViewState component2() {
        return this.storyViewState;
    }

    public final Recipient component3() {
        return this.recipient;
    }

    public final ButtonStripPreference.State component4() {
        return this.buttonStripState;
    }

    public final int component5() {
        return this.disappearingMessagesLifespan;
    }

    public final boolean component6() {
        return this.canModifyBlockedState;
    }

    public final Cursor component7() {
        return this.sharedMedia;
    }

    public final List<Long> component8() {
        return this.sharedMediaIds;
    }

    public final boolean component9() {
        return this.displayInternalRecipientDetails;
    }

    public final ConversationSettingsState copy(long j, StoryViewState storyViewState, Recipient recipient, ButtonStripPreference.State state, int i, boolean z, Cursor cursor, List<Long> list, boolean z2, boolean z3, SpecificSettingsState specificSettingsState) {
        Intrinsics.checkNotNullParameter(storyViewState, "storyViewState");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(state, "buttonStripState");
        Intrinsics.checkNotNullParameter(list, "sharedMediaIds");
        Intrinsics.checkNotNullParameter(specificSettingsState, "specificSettingsState");
        return new ConversationSettingsState(j, storyViewState, recipient, state, i, z, cursor, list, z2, z3, specificSettingsState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ConversationSettingsState)) {
            return false;
        }
        ConversationSettingsState conversationSettingsState = (ConversationSettingsState) obj;
        return this.threadId == conversationSettingsState.threadId && this.storyViewState == conversationSettingsState.storyViewState && Intrinsics.areEqual(this.recipient, conversationSettingsState.recipient) && Intrinsics.areEqual(this.buttonStripState, conversationSettingsState.buttonStripState) && this.disappearingMessagesLifespan == conversationSettingsState.disappearingMessagesLifespan && this.canModifyBlockedState == conversationSettingsState.canModifyBlockedState && Intrinsics.areEqual(this.sharedMedia, conversationSettingsState.sharedMedia) && Intrinsics.areEqual(this.sharedMediaIds, conversationSettingsState.sharedMediaIds) && this.displayInternalRecipientDetails == conversationSettingsState.displayInternalRecipientDetails && this.sharedMediaLoaded == conversationSettingsState.sharedMediaLoaded && Intrinsics.areEqual(this.specificSettingsState, conversationSettingsState.specificSettingsState);
    }

    public int hashCode() {
        int m = ((((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId) * 31) + this.storyViewState.hashCode()) * 31) + this.recipient.hashCode()) * 31) + this.buttonStripState.hashCode()) * 31) + this.disappearingMessagesLifespan) * 31;
        boolean z = this.canModifyBlockedState;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (m + i2) * 31;
        Cursor cursor = this.sharedMedia;
        int hashCode = (((i5 + (cursor == null ? 0 : cursor.hashCode())) * 31) + this.sharedMediaIds.hashCode()) * 31;
        boolean z2 = this.displayInternalRecipientDetails;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (hashCode + i6) * 31;
        boolean z3 = this.sharedMediaLoaded;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return ((i9 + i) * 31) + this.specificSettingsState.hashCode();
    }

    public String toString() {
        return "ConversationSettingsState(threadId=" + this.threadId + ", storyViewState=" + this.storyViewState + ", recipient=" + this.recipient + ", buttonStripState=" + this.buttonStripState + ", disappearingMessagesLifespan=" + this.disappearingMessagesLifespan + ", canModifyBlockedState=" + this.canModifyBlockedState + ", sharedMedia=" + this.sharedMedia + ", sharedMediaIds=" + this.sharedMediaIds + ", displayInternalRecipientDetails=" + this.displayInternalRecipientDetails + ", sharedMediaLoaded=" + this.sharedMediaLoaded + ", specificSettingsState=" + this.specificSettingsState + ')';
    }

    public ConversationSettingsState(long j, StoryViewState storyViewState, Recipient recipient, ButtonStripPreference.State state, int i, boolean z, Cursor cursor, List<Long> list, boolean z2, boolean z3, SpecificSettingsState specificSettingsState) {
        Intrinsics.checkNotNullParameter(storyViewState, "storyViewState");
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        Intrinsics.checkNotNullParameter(state, "buttonStripState");
        Intrinsics.checkNotNullParameter(list, "sharedMediaIds");
        Intrinsics.checkNotNullParameter(specificSettingsState, "specificSettingsState");
        this.threadId = j;
        this.storyViewState = storyViewState;
        this.recipient = recipient;
        this.buttonStripState = state;
        this.disappearingMessagesLifespan = i;
        this.canModifyBlockedState = z;
        this.sharedMedia = cursor;
        this.sharedMediaIds = list;
        this.displayInternalRecipientDetails = z2;
        this.sharedMediaLoaded = z3;
        this.specificSettingsState = specificSettingsState;
        this.isLoaded = !Intrinsics.areEqual(recipient, Recipient.UNKNOWN) && z3 && specificSettingsState.isLoaded();
    }

    public final long getThreadId() {
        return this.threadId;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ConversationSettingsState(long r19, org.thoughtcrime.securesms.database.model.StoryViewState r21, org.thoughtcrime.securesms.recipients.Recipient r22, org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.State r23, int r24, boolean r25, android.database.Cursor r26, java.util.List r27, boolean r28, boolean r29, org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState r30, int r31, kotlin.jvm.internal.DefaultConstructorMarker r32) {
        /*
            r18 = this;
            r0 = r31
            r1 = r0 & 1
            if (r1 == 0) goto L_0x000a
            r1 = -1
            r4 = r1
            goto L_0x000c
        L_0x000a:
            r4 = r19
        L_0x000c:
            r1 = r0 & 2
            if (r1 == 0) goto L_0x0014
            org.thoughtcrime.securesms.database.model.StoryViewState r1 = org.thoughtcrime.securesms.database.model.StoryViewState.NONE
            r6 = r1
            goto L_0x0016
        L_0x0014:
            r6 = r21
        L_0x0016:
            r1 = r0 & 4
            if (r1 == 0) goto L_0x0023
            org.thoughtcrime.securesms.recipients.Recipient r1 = org.thoughtcrime.securesms.recipients.Recipient.UNKNOWN
            java.lang.String r2 = "UNKNOWN"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
            r7 = r1
            goto L_0x0025
        L_0x0023:
            r7 = r22
        L_0x0025:
            r1 = r0 & 8
            if (r1 == 0) goto L_0x003b
            org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$State r1 = new org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$State
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 127(0x7f, float:1.78E-43)
            r17 = 0
            r8 = r1
            r8.<init>(r9, r10, r11, r12, r13, r14, r15, r16, r17)
            goto L_0x003d
        L_0x003b:
            r8 = r23
        L_0x003d:
            r1 = r0 & 16
            r2 = 0
            if (r1 == 0) goto L_0x0044
            r9 = 0
            goto L_0x0046
        L_0x0044:
            r9 = r24
        L_0x0046:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x004c
            r10 = 0
            goto L_0x004e
        L_0x004c:
            r10 = r25
        L_0x004e:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0055
            r1 = 0
            r11 = r1
            goto L_0x0057
        L_0x0055:
            r11 = r26
        L_0x0057:
            r1 = r0 & 128(0x80, float:1.794E-43)
            if (r1 == 0) goto L_0x0061
            java.util.List r1 = kotlin.collections.CollectionsKt.emptyList()
            r12 = r1
            goto L_0x0063
        L_0x0061:
            r12 = r27
        L_0x0063:
            r1 = r0 & 256(0x100, float:3.59E-43)
            if (r1 == 0) goto L_0x0069
            r13 = 0
            goto L_0x006b
        L_0x0069:
            r13 = r28
        L_0x006b:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto L_0x0071
            r14 = 0
            goto L_0x0073
        L_0x0071:
            r14 = r29
        L_0x0073:
            r3 = r18
            r15 = r30
            r3.<init>(r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsState.<init>(long, org.thoughtcrime.securesms.database.model.StoryViewState, org.thoughtcrime.securesms.recipients.Recipient, org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$State, int, boolean, android.database.Cursor, java.util.List, boolean, boolean, org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final StoryViewState getStoryViewState() {
        return this.storyViewState;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final ButtonStripPreference.State getButtonStripState() {
        return this.buttonStripState;
    }

    public final int getDisappearingMessagesLifespan() {
        return this.disappearingMessagesLifespan;
    }

    public final boolean getCanModifyBlockedState() {
        return this.canModifyBlockedState;
    }

    public final Cursor getSharedMedia() {
        return this.sharedMedia;
    }

    public final List<Long> getSharedMediaIds() {
        return this.sharedMediaIds;
    }

    public final boolean getDisplayInternalRecipientDetails() {
        return this.displayInternalRecipientDetails;
    }

    public final boolean isLoaded() {
        return this.isLoaded;
    }

    public final void withRecipientSettingsState(Function1<? super SpecificSettingsState.RecipientSettingsState, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SpecificSettingsState specificSettingsState = this.specificSettingsState;
        if (specificSettingsState instanceof SpecificSettingsState.RecipientSettingsState) {
            function1.invoke(specificSettingsState);
        }
    }

    public final void withGroupSettingsState(Function1<? super SpecificSettingsState.GroupSettingsState, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SpecificSettingsState specificSettingsState = this.specificSettingsState;
        if (specificSettingsState instanceof SpecificSettingsState.GroupSettingsState) {
            function1.invoke(specificSettingsState);
        }
    }

    public final SpecificSettingsState.RecipientSettingsState requireRecipientSettingsState() {
        return this.specificSettingsState.requireRecipientSettingsState();
    }

    public final SpecificSettingsState.GroupSettingsState requireGroupSettingsState() {
        return this.specificSettingsState.requireGroupSettingsState();
    }
}
