package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.airbnb.lottie.SimpleColorFilter;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class ConversationScrollToView extends FrameLayout {
    private final ImageView scrollButton;
    private final TextView unreadCount;

    public ConversationScrollToView(Context context) {
        this(context, null);
    }

    public ConversationScrollToView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ConversationScrollToView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.conversation_scroll_to, this);
        this.unreadCount = (TextView) findViewById(R.id.conversation_scroll_to_count);
        ImageView imageView = (ImageView) findViewById(R.id.conversation_scroll_to_button);
        this.scrollButton = imageView;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ConversationScrollToView);
            imageView.setImageResource(obtainStyledAttributes.getResourceId(0, 0));
            obtainStyledAttributes.recycle();
        }
    }

    public void setWallpaperEnabled(boolean z) {
        if (z) {
            this.scrollButton.setBackgroundResource(R.drawable.scroll_to_bottom_background_wallpaper);
        } else {
            this.scrollButton.setBackgroundResource(R.drawable.scroll_to_bottom_background_normal);
        }
    }

    public void setUnreadCountBackgroundTint(int i) {
        this.unreadCount.getBackground().setColorFilter(new SimpleColorFilter(i));
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.scrollButton.setOnClickListener(onClickListener);
    }

    public void setUnreadCount(int i) {
        this.unreadCount.setText(formatUnreadCount(i));
        this.unreadCount.setVisibility(i > 0 ? 0 : 8);
    }

    private CharSequence formatUnreadCount(int i) {
        return i > 999 ? "999+" : String.valueOf(i);
    }
}
