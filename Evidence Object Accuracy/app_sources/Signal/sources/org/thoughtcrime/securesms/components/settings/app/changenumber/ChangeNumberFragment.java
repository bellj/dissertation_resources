package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberFragment.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberFragment extends LoggingFragment {
    public ChangeNumberFragment() {
        super(R.layout.fragment_change_phone_number);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberFragment.$r8$lambda$H3czuyZLakk7L_k9BMjmEhv47nc(ChangeNumberFragment.this, view2);
            }
        });
        view.findViewById(R.id.change_phone_number_continue).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberFragment.m592$r8$lambda$ZEnbu3yyD40Hm1BdcWRsg1Diig(ChangeNumberFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m593onViewCreated$lambda0(ChangeNumberFragment changeNumberFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberFragment, "this$0");
        FragmentKt.findNavController(changeNumberFragment).navigateUp();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m594onViewCreated$lambda1(ChangeNumberFragment changeNumberFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberFragment, "this$0");
        SafeNavigation.safeNavigate(FragmentKt.findNavController(changeNumberFragment), (int) R.id.action_changePhoneNumberFragment_to_enterPhoneNumberChangeFragment);
    }
}
