package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import com.annimon.stream.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012^\u0010\u0002\u001aZ\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00040\u0004\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007 \u0006*,\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u0005 \u0006*\n\u0012\u0004\u0012\u00020\u0005\u0018\u00010\u00040\u0004\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00070\u0007\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\b"}, d2 = {"<anonymous>", "", "<name for destructuring parameter 0>", "Lkotlin/Pair;", "", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "kotlin.jvm.PlatformType", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$refresh$6 extends Lambda implements Function1<Pair<? extends List<? extends Subscription>, ? extends ActiveSubscription>, Unit> {
    final /* synthetic */ SubscribeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SubscribeViewModel$refresh$6(SubscribeViewModel subscribeViewModel) {
        super(1);
        this.this$0 = subscribeViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Pair<? extends List<? extends Subscription>, ? extends ActiveSubscription> pair) {
        invoke((Pair<? extends List<Subscription>, ActiveSubscription>) pair);
        return Unit.INSTANCE;
    }

    public final void invoke(Pair<? extends List<Subscription>, ActiveSubscription> pair) {
        this.this$0.store.update(new Function((List) pair.component1(), this.this$0, pair.component2()) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$refresh$6$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;
            public final /* synthetic */ SubscribeViewModel f$1;
            public final /* synthetic */ ActiveSubscription f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$refresh$6.m1074invoke$lambda0(this.f$0, this.f$1, this.f$2, (SubscribeState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final SubscribeState m1074invoke$lambda0(List list, SubscribeViewModel subscribeViewModel, ActiveSubscription activeSubscription, SubscribeState subscribeState) {
        Intrinsics.checkNotNullParameter(subscribeViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        Intrinsics.checkNotNullExpressionValue(list, "subs");
        Subscription selectedSubscription = subscribeState.getSelectedSubscription();
        if (selectedSubscription == null) {
            Intrinsics.checkNotNullExpressionValue(activeSubscription, "active");
            selectedSubscription = subscribeViewModel.resolveSelectedSubscription(activeSubscription, list);
        }
        return SubscribeState.copy$default(subscribeState, null, list, selectedSubscription, activeSubscription, false, (subscribeState.getStage() == SubscribeState.Stage.INIT || subscribeState.getStage() == SubscribeState.Stage.FAILURE) ? SubscribeState.Stage.READY : subscribeState.getStage(), false, 81, null);
    }
}
