package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class EmojiPageView extends RecyclerView implements EmojiPageViewGridAdapter.VariationSelectorListener {
    private AdapterFactory adapterFactory;
    private LinearLayoutManager layoutManager;
    private EmojiVariationSelectorPopup popup;
    private RecyclerView.OnItemTouchListener scrollDisabler;
    private EmojiPageViewGridAdapter.VariationSelectorListener variationSelectorListener;

    /* loaded from: classes4.dex */
    public interface AdapterFactory {
        EmojiPageViewGridAdapter create();
    }

    public EmojiPageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public EmojiPageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public EmojiPageView(Context context, EmojiEventListener emojiEventListener, EmojiPageViewGridAdapter.VariationSelectorListener variationSelectorListener, boolean z) {
        super(context);
        initialize(emojiEventListener, variationSelectorListener, z);
    }

    public EmojiPageView(Context context, EmojiEventListener emojiEventListener, EmojiPageViewGridAdapter.VariationSelectorListener variationSelectorListener, boolean z, LinearLayoutManager linearLayoutManager, int i, int i2) {
        super(context);
        initialize(emojiEventListener, variationSelectorListener, z, linearLayoutManager, i, i2);
    }

    public void initialize(EmojiEventListener emojiEventListener, EmojiPageViewGridAdapter.VariationSelectorListener variationSelectorListener, boolean z) {
        initialize(emojiEventListener, variationSelectorListener, z, new GridLayoutManager(getContext(), 8), R.layout.emoji_display_item_grid, R.layout.emoji_text_display_item_grid);
    }

    public void initialize(EmojiEventListener emojiEventListener, EmojiPageViewGridAdapter.VariationSelectorListener variationSelectorListener, boolean z, LinearLayoutManager linearLayoutManager, int i, int i2) {
        this.variationSelectorListener = variationSelectorListener;
        this.layoutManager = linearLayoutManager;
        this.scrollDisabler = new ScrollDisabler();
        this.popup = new EmojiVariationSelectorPopup(getContext(), emojiEventListener);
        this.adapterFactory = new AdapterFactory(emojiEventListener, z, i, i2) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageView$$ExternalSyntheticLambda0
            public final /* synthetic */ EmojiEventListener f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ int f$3;
            public final /* synthetic */ int f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageView.AdapterFactory
            public final EmojiPageViewGridAdapter create() {
                return EmojiPageView.$r8$lambda$of41Ex4bQanbOzBxfp2oZltYCgQ(EmojiPageView.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        };
        LinearLayoutManager linearLayoutManager2 = this.layoutManager;
        if (linearLayoutManager2 instanceof GridLayoutManager) {
            final GridLayoutManager gridLayoutManager = (GridLayoutManager) linearLayoutManager2;
            gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageView.1
                @Override // androidx.recyclerview.widget.GridLayoutManager.SpanSizeLookup
                public int getSpanSize(int i3) {
                    if (EmojiPageView.this.getAdapter() == null) {
                        return 1;
                    }
                    Optional<MappingModel<?>> model = EmojiPageView.this.getAdapter().getModel(i3);
                    if (!model.isPresent()) {
                        return 1;
                    }
                    if ((model.get() instanceof EmojiPageViewGridAdapter.EmojiHeader) || (model.get() instanceof EmojiPageViewGridAdapter.EmojiNoResultsModel)) {
                        return gridLayoutManager.getSpanCount();
                    }
                    return 1;
                }
            });
        }
        setLayoutManager(linearLayoutManager);
        addItemDecoration(new EmojiItemDecoration(z, DrawableUtil.tint(ContextUtil.requireDrawable(getContext(), R.drawable.triangle_bottom_right_corner), ContextCompat.getColor(getContext(), R.color.signal_button_secondary_text_disabled))));
    }

    public /* synthetic */ EmojiPageViewGridAdapter lambda$initialize$0(EmojiEventListener emojiEventListener, boolean z, int i, int i2) {
        return new EmojiPageViewGridAdapter(this.popup, emojiEventListener, this, z, i, i2);
    }

    public void presentForEmojiKeyboard() {
        setPadding(getPaddingLeft(), getPaddingTop(), getPaddingRight(), getPaddingBottom() + ViewUtil.dpToPx(56));
        setClipToPadding(false);
    }

    public void onSelected() {
        if (getAdapter() != null) {
            getAdapter().notifyDataSetChanged();
        }
    }

    public void setList(List<MappingModel<?>> list, Runnable runnable) {
        EmojiPageViewGridAdapter create = this.adapterFactory.create();
        setAdapter(create);
        create.submitList(list, runnable);
    }

    @Override // android.view.View
    protected void onVisibilityChanged(View view, int i) {
        if (i != 0) {
            this.popup.dismiss();
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView, android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        if (this.layoutManager instanceof GridLayoutManager) {
            ((GridLayoutManager) this.layoutManager).setSpanCount(Math.max(((i - getPaddingStart()) - getPaddingEnd()) / getContext().getResources().getDimensionPixelOffset(R.dimen.emoji_drawer_item_width), 1));
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.VariationSelectorListener
    public void onVariationSelectorStateChanged(boolean z) {
        if (z) {
            addOnItemTouchListener(this.scrollDisabler);
        } else {
            post(new Runnable() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageView$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    EmojiPageView.$r8$lambda$QGYt8NOMAX8NHNzzpHYzJ3KjB0U(EmojiPageView.this);
                }
            });
        }
        EmojiPageViewGridAdapter.VariationSelectorListener variationSelectorListener = this.variationSelectorListener;
        if (variationSelectorListener != null) {
            variationSelectorListener.onVariationSelectorStateChanged(z);
        }
    }

    public /* synthetic */ void lambda$onVariationSelectorStateChanged$1() {
        removeOnItemTouchListener(this.scrollDisabler);
    }

    public void setRecyclerNestedScrollingEnabled(boolean z) {
        setNestedScrollingEnabled(z);
    }

    public void smoothScrollToPositionTop(int i) {
        if (Math.abs(this.layoutManager.findFirstCompletelyVisibleItemPosition() - i) < 475) {
            AnonymousClass2 r0 = new LinearSmoothScroller(getContext()) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageView.2
                @Override // androidx.recyclerview.widget.LinearSmoothScroller
                protected int getVerticalSnapPreference() {
                    return -1;
                }
            };
            r0.setTargetPosition(i);
            this.layoutManager.startSmoothScroll(r0);
            return;
        }
        this.layoutManager.scrollToPositionWithOffset(i, 0);
    }

    @Override // androidx.recyclerview.widget.RecyclerView
    public EmojiPageViewGridAdapter getAdapter() {
        return (EmojiPageViewGridAdapter) super.getAdapter();
    }

    /* loaded from: classes4.dex */
    public static class ScrollDisabler implements RecyclerView.OnItemTouchListener {
        @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
        public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
            return true;
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
        public void onRequestDisallowInterceptTouchEvent(boolean z) {
        }

        @Override // androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
        public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
        }

        private ScrollDisabler() {
        }
    }
}
