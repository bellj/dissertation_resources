package org.thoughtcrime.securesms.components.voice;

import android.hardware.Sensor;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* compiled from: VoiceNoteProximityWakeLockManager.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002\"#B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\b\u0010\u0018\u001a\u00020\u0017H\u0002J\u0010\u0010\u0019\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001c\u001a\u00020\u00152\u0006\u0010\u001a\u001a\u00020\u001bH\u0016J\u0010\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u001e\u001a\u00020\u001fH\u0002J\u0006\u0010 \u001a\u00020\u0015J\u0006\u0010!\u001a\u00020\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\u0007\u001a\u00060\bR\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u00060\nR\u00020\u0000X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0018\u00010\u0012R\u00020\u0013X\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager;", "Landroidx/lifecycle/DefaultLifecycleObserver;", "activity", "Landroidx/fragment/app/FragmentActivity;", "mediaController", "Landroid/support/v4/media/session/MediaControllerCompat;", "(Landroidx/fragment/app/FragmentActivity;Landroid/support/v4/media/session/MediaControllerCompat;)V", "hardwareSensorEventListener", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager$HardwareSensorEventListener;", "mediaControllerCallback", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager$MediaControllerCallback;", "proximitySensor", "Landroid/hardware/Sensor;", "sensorManager", "Landroid/hardware/SensorManager;", "startTime", "", "wakeLock", "Landroid/os/PowerManager$WakeLock;", "Landroid/os/PowerManager;", "cleanUpWakeLock", "", "isActivityResumed", "", "isPlayerActive", "onPause", "owner", "Landroidx/lifecycle/LifecycleOwner;", "onResume", "sendNewStreamTypeToPlayer", "newStreamType", "", "unregisterCallbacksAndRelease", "unregisterFromLifecycle", "HardwareSensorEventListener", "MediaControllerCallback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNoteProximityWakeLockManager implements DefaultLifecycleObserver {
    private final FragmentActivity activity;
    private final HardwareSensorEventListener hardwareSensorEventListener;
    private final MediaControllerCompat mediaController;
    private final MediaControllerCallback mediaControllerCallback;
    private final Sensor proximitySensor;
    private final SensorManager sensorManager;
    private long startTime;
    private final PowerManager.WakeLock wakeLock;

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public VoiceNoteProximityWakeLockManager(FragmentActivity fragmentActivity, MediaControllerCompat mediaControllerCompat) {
        Intrinsics.checkNotNullParameter(fragmentActivity, "activity");
        Intrinsics.checkNotNullParameter(mediaControllerCompat, "mediaController");
        this.activity = fragmentActivity;
        this.mediaController = mediaControllerCompat;
        this.wakeLock = Build.VERSION.SDK_INT >= 21 ? ServiceUtil.getPowerManager(fragmentActivity.getApplicationContext()).newWakeLock(32, VoiceNoteProximityWakeLockManagerKt.TAG) : null;
        SensorManager sensorManager = ServiceUtil.getSensorManager(fragmentActivity);
        Intrinsics.checkNotNullExpressionValue(sensorManager, "getSensorManager(activity)");
        this.sensorManager = sensorManager;
        Sensor defaultSensor = sensorManager.getDefaultSensor(8);
        this.proximitySensor = defaultSensor;
        this.mediaControllerCallback = new MediaControllerCallback();
        this.hardwareSensorEventListener = new HardwareSensorEventListener();
        this.startTime = -1;
        if (defaultSensor != null) {
            fragmentActivity.getLifecycle().addObserver(this);
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onResume(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        if (this.proximitySensor != null) {
            this.mediaController.registerCallback(this.mediaControllerCallback);
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        Intrinsics.checkNotNullParameter(lifecycleOwner, "owner");
        if (this.proximitySensor != null) {
            unregisterCallbacksAndRelease();
        }
    }

    public final void unregisterCallbacksAndRelease() {
        this.mediaController.unregisterCallback(this.mediaControllerCallback);
        cleanUpWakeLock();
    }

    public final void unregisterFromLifecycle() {
        if (this.proximitySensor != null) {
            this.activity.getLifecycle().removeObserver(this);
        }
    }

    public final boolean isActivityResumed() {
        return this.activity.getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.RESUMED);
    }

    public final boolean isPlayerActive() {
        return this.mediaController.getPlaybackState().getState() == 6 || this.mediaController.getPlaybackState().getState() == 3;
    }

    public final void cleanUpWakeLock() {
        this.startTime = -1;
        this.sensorManager.unregisterListener(this.hardwareSensorEventListener);
        PowerManager.WakeLock wakeLock = this.wakeLock;
        boolean z = true;
        if (wakeLock == null || !wakeLock.isHeld()) {
            z = false;
        }
        if (z) {
            Log.d(VoiceNoteProximityWakeLockManagerKt.TAG, "[cleanUpWakeLock] Releasing wake lock.");
            this.wakeLock.release();
        }
        sendNewStreamTypeToPlayer(3);
    }

    public final void sendNewStreamTypeToPlayer(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt(VoiceNotePlaybackService.ACTION_SET_AUDIO_STREAM, i);
        this.mediaController.sendCommand(VoiceNotePlaybackService.ACTION_SET_AUDIO_STREAM, bundle, null);
    }

    /* compiled from: VoiceNoteProximityWakeLockManager.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager$MediaControllerCallback;", "Landroid/support/v4/media/session/MediaControllerCompat$Callback;", "(Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager;)V", "onPlaybackStateChanged", "", "state", "Landroid/support/v4/media/session/PlaybackStateCompat;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class MediaControllerCallback extends MediaControllerCompat.Callback {
        public MediaControllerCallback() {
            VoiceNoteProximityWakeLockManager.this = r1;
        }

        @Override // android.support.v4.media.session.MediaControllerCompat.Callback
        public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) {
            Intrinsics.checkNotNullParameter(playbackStateCompat, "state");
            if (VoiceNoteProximityWakeLockManager.this.isActivityResumed()) {
                if (!VoiceNoteProximityWakeLockManager.this.isPlayerActive()) {
                    Log.d(VoiceNoteProximityWakeLockManagerKt.TAG, "[onPlaybackStateChanged] Player became inactive. Cleaning up wake lock.");
                    VoiceNoteProximityWakeLockManager.this.cleanUpWakeLock();
                } else if (VoiceNoteProximityWakeLockManager.this.startTime == -1) {
                    String str = VoiceNoteProximityWakeLockManagerKt.TAG;
                    Log.d(str, "[onPlaybackStateChanged] Player became active with start time " + VoiceNoteProximityWakeLockManager.this.startTime + ", registering sensor listener.");
                    VoiceNoteProximityWakeLockManager.this.startTime = System.currentTimeMillis();
                    VoiceNoteProximityWakeLockManager.this.sensorManager.registerListener(VoiceNoteProximityWakeLockManager.this.hardwareSensorEventListener, VoiceNoteProximityWakeLockManager.this.proximitySensor, 3);
                } else {
                    Log.d(VoiceNoteProximityWakeLockManagerKt.TAG, "[onPlaybackStateChanged] Player became active without start time, skipping sensor registration");
                }
            }
        }
    }

    /* compiled from: VoiceNoteProximityWakeLockManager.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u000bH\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager$HardwareSensorEventListener;", "Landroid/hardware/SensorEventListener;", "(Lorg/thoughtcrime/securesms/components/voice/VoiceNoteProximityWakeLockManager;)V", "onAccuracyChanged", "", "sensor", "Landroid/hardware/Sensor;", "accuracy", "", "onSensorChanged", "event", "Landroid/hardware/SensorEvent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class HardwareSensorEventListener implements SensorEventListener {
        @Override // android.hardware.SensorEventListener
        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        public HardwareSensorEventListener() {
            VoiceNoteProximityWakeLockManager.this = r1;
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
        /* JADX WARNING: Removed duplicated region for block: B:32:0x00a1  */
        @Override // android.hardware.SensorEventListener
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onSensorChanged(android.hardware.SensorEvent r6) {
            /*
                r5 = this;
                java.lang.String r0 = "event"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r0 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                long r0 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getStartTime$p(r0)
                r2 = -1
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 == 0) goto L_0x00c4
                long r0 = java.lang.System.currentTimeMillis()
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r2 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                long r2 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getStartTime$p(r2)
                long r0 = r0 - r2
                r2 = 500(0x1f4, double:2.47E-321)
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 <= 0) goto L_0x00c4
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r0 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                boolean r0 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$isActivityResumed(r0)
                if (r0 == 0) goto L_0x00c4
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r0 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                boolean r0 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$isPlayerActive(r0)
                if (r0 == 0) goto L_0x00c4
                android.hardware.Sensor r0 = r6.sensor
                int r0 = r0.getType()
                r1 = 8
                if (r0 == r1) goto L_0x003e
                goto L_0x00c4
            L_0x003e:
                float[] r6 = r6.values
                r0 = 0
                r6 = r6[r0]
                r1 = 1084227584(0x40a00000, float:5.0)
                int r1 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
                if (r1 >= 0) goto L_0x0063
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r1 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                android.hardware.Sensor r1 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getProximitySensor$p(r1)
                if (r1 == 0) goto L_0x005a
                float r1 = r1.getMaximumRange()
                java.lang.Float r1 = java.lang.Float.valueOf(r1)
                goto L_0x005b
            L_0x005a:
                r1 = 0
            L_0x005b:
                boolean r6 = kotlin.jvm.internal.Intrinsics.areEqual(r6, r1)
                if (r6 != 0) goto L_0x0063
                r6 = 0
                goto L_0x0064
            L_0x0063:
                r6 = 3
            L_0x0064:
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r1 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$sendNewStreamTypeToPlayer(r1, r6)
                r1 = 1
                if (r6 != 0) goto L_0x00a1
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                android.os.PowerManager$WakeLock r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getWakeLock$p(r6)
                if (r6 == 0) goto L_0x007b
                boolean r6 = r6.isHeld()
                if (r6 != 0) goto L_0x007b
                r0 = 1
            L_0x007b:
                if (r0 == 0) goto L_0x0097
                java.lang.String r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManagerKt.access$getTAG$p()
                java.lang.String r0 = "[onSensorChanged] Acquiring wakelock"
                org.signal.core.util.logging.Log.d(r6, r0)
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                android.os.PowerManager$WakeLock r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getWakeLock$p(r6)
                java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.MINUTES
                r1 = 30
                long r0 = r0.toMillis(r1)
                r6.acquire(r0)
            L_0x0097:
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                long r0 = java.lang.System.currentTimeMillis()
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$setStartTime$p(r6, r0)
                goto L_0x00c4
            L_0x00a1:
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                android.os.PowerManager$WakeLock r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getWakeLock$p(r6)
                if (r6 == 0) goto L_0x00b0
                boolean r6 = r6.isHeld()
                if (r6 != r1) goto L_0x00b0
                r0 = 1
            L_0x00b0:
                if (r0 == 0) goto L_0x00c4
                java.lang.String r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManagerKt.access$getTAG$p()
                java.lang.String r0 = "[onSensorChanged] Releasing wakelock"
                org.signal.core.util.logging.Log.d(r6, r0)
                org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.this
                android.os.PowerManager$WakeLock r6 = org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.access$getWakeLock$p(r6)
                r6.release()
            L_0x00c4:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.voice.VoiceNoteProximityWakeLockManager.HardwareSensorEventListener.onSensorChanged(android.hardware.SensorEvent):void");
        }
    }
}
