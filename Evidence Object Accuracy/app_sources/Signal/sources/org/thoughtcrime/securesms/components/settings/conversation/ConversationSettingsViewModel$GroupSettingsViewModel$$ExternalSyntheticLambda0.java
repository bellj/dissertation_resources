package org.thoughtcrime.securesms.components.settings.conversation;

import io.reactivex.rxjava3.functions.Consumer;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel f$0;

    public /* synthetic */ ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda0(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel) {
        this.f$0 = groupSettingsViewModel;
    }

    @Override // io.reactivex.rxjava3.functions.Consumer
    public final void accept(Object obj) {
        ConversationSettingsViewModel.GroupSettingsViewModel.m1129_init_$lambda1(this.f$0, (StoryViewState) obj);
    }
}
