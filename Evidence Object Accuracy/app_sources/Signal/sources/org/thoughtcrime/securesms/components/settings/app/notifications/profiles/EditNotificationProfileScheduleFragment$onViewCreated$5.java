package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.content.Context;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import j$.time.DayOfWeek;
import j$.time.LocalTime;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: EditNotificationProfileScheduleFragment.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "schedule", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class EditNotificationProfileScheduleFragment$onViewCreated$5 extends Lambda implements Function1<NotificationProfileSchedule, Unit> {
    final /* synthetic */ Map<CheckedTextView, DayOfWeek> $days;
    final /* synthetic */ SwitchMaterial $enableToggle;
    final /* synthetic */ TextView $endTime;
    final /* synthetic */ CircularProgressMaterialButton $next;
    final /* synthetic */ TextView $startTime;
    final /* synthetic */ View $view;
    final /* synthetic */ EditNotificationProfileScheduleFragment this$0;

    /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.Map<android.widget.CheckedTextView, ? extends j$.time.DayOfWeek> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public EditNotificationProfileScheduleFragment$onViewCreated$5(SwitchMaterial switchMaterial, Map<CheckedTextView, ? extends DayOfWeek> map, TextView textView, View view, TextView textView2, EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, CircularProgressMaterialButton circularProgressMaterialButton) {
        super(1);
        this.$enableToggle = switchMaterial;
        this.$days = map;
        this.$startTime = textView;
        this.$view = view;
        this.$endTime = textView2;
        this.this$0 = editNotificationProfileScheduleFragment;
        this.$next = circularProgressMaterialButton;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(NotificationProfileSchedule notificationProfileSchedule) {
        invoke(notificationProfileSchedule);
        return Unit.INSTANCE;
    }

    public final void invoke(NotificationProfileSchedule notificationProfileSchedule) {
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        this.$enableToggle.setChecked(notificationProfileSchedule.getEnabled());
        this.$enableToggle.setEnabled(true);
        for (Map.Entry<CheckedTextView, DayOfWeek> entry : this.$days.entrySet()) {
            CheckedTextView key = entry.getKey();
            key.setChecked(notificationProfileSchedule.getDaysEnabled().contains(entry.getValue()));
            key.setEnabled(notificationProfileSchedule.getEnabled());
        }
        TextView textView = this.$startTime;
        LocalTime startTime = notificationProfileSchedule.startTime();
        Context context = this.$view.getContext();
        Intrinsics.checkNotNullExpressionValue(context, "view.context");
        textView.setText(EditNotificationProfileScheduleFragmentKt.formatTime(startTime, context));
        this.$startTime.setOnClickListener(new View.OnClickListener(notificationProfileSchedule) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$onViewCreated$5$$ExternalSyntheticLambda0
            public final /* synthetic */ NotificationProfileSchedule f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EditNotificationProfileScheduleFragment$onViewCreated$5.m749invoke$lambda1(EditNotificationProfileScheduleFragment.this, this.f$1, view);
            }
        });
        this.$startTime.setEnabled(notificationProfileSchedule.getEnabled());
        TextView textView2 = this.$endTime;
        LocalTime endTime = notificationProfileSchedule.endTime();
        Context context2 = this.$view.getContext();
        Intrinsics.checkNotNullExpressionValue(context2, "view.context");
        textView2.setText(EditNotificationProfileScheduleFragmentKt.formatTime(endTime, context2));
        this.$endTime.setOnClickListener(new View.OnClickListener(notificationProfileSchedule) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$onViewCreated$5$$ExternalSyntheticLambda1
            public final /* synthetic */ NotificationProfileSchedule f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EditNotificationProfileScheduleFragment$onViewCreated$5.m750invoke$lambda2(EditNotificationProfileScheduleFragment.this, this.f$1, view);
            }
        });
        this.$endTime.setEnabled(notificationProfileSchedule.getEnabled());
        if (this.this$0.getCreateMode()) {
            this.$next.setText(notificationProfileSchedule.getEnabled() ? R.string.EditNotificationProfileSchedule__next : R.string.EditNotificationProfileSchedule__skip);
        } else {
            this.$next.setText(R.string.EditNotificationProfileSchedule__save);
        }
        this.$next.setEnabled(true);
    }

    /* renamed from: invoke$lambda-1 */
    public static final void m749invoke$lambda1(EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, NotificationProfileSchedule notificationProfileSchedule, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "$schedule");
        editNotificationProfileScheduleFragment.showTimeSelector(true, notificationProfileSchedule.startTime());
    }

    /* renamed from: invoke$lambda-2 */
    public static final void m750invoke$lambda2(EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, NotificationProfileSchedule notificationProfileSchedule, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "$schedule");
        editNotificationProfileScheduleFragment.showTimeSelector(false, notificationProfileSchedule.endTime());
    }
}
