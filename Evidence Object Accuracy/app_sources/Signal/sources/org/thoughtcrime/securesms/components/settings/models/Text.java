package org.thoughtcrime.securesms.components.settings.models;

import android.content.Context;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: Text.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\b\b\u0018\u0000 \u00102\u00020\u0001:\u0003\u0010\u0011\u0012B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Text;", "", DraftDatabase.Draft.TEXT, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;)V", "getText", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Text {
    public static final Companion Companion = new Companion(null);
    private final DSLSettingsText text;

    public static /* synthetic */ Text copy$default(Text text, DSLSettingsText dSLSettingsText, int i, Object obj) {
        if ((i & 1) != 0) {
            dSLSettingsText = text.text;
        }
        return text.copy(dSLSettingsText);
    }

    public final DSLSettingsText component1() {
        return this.text;
    }

    public final Text copy(DSLSettingsText dSLSettingsText) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.Draft.TEXT);
        return new Text(dSLSettingsText);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof Text) && Intrinsics.areEqual(this.text, ((Text) obj).text);
    }

    public int hashCode() {
        return this.text.hashCode();
    }

    public String toString() {
        return "Text(text=" + this.text + ')';
    }

    public Text(DSLSettingsText dSLSettingsText) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.Draft.TEXT);
        this.text = dSLSettingsText;
    }

    public final DSLSettingsText getText() {
        return this.text;
    }

    /* compiled from: Text.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Text$Companion;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* renamed from: register$lambda-0 */
        public static final MappingViewHolder m1257register$lambda0(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new ViewHolder(view);
        }

        public final void register(MappingAdapter mappingAdapter) {
            Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
            mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Text$Companion$$ExternalSyntheticLambda0(), R.layout.dsl_text_preference));
        }
    }

    /* compiled from: Text.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Text$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "paddableText", "Lorg/thoughtcrime/securesms/components/settings/models/Text;", "(Lorg/thoughtcrime/securesms/components/settings/models/Text;)V", "getPaddableText", "()Lorg/thoughtcrime/securesms/components/settings/models/Text;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Text paddableText;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Text text) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(text, "paddableText");
            this.paddableText = text;
        }

        public final Text getPaddableText() {
            return this.paddableText;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(model.paddableText, this.paddableText);
        }
    }

    /* compiled from: Text.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/Text$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/models/Text$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", DraftDatabase.Draft.TEXT, "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final TextView text;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.title);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.title)");
            this.text = (TextView) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            TextView textView = this.text;
            DSLSettingsText text = model.getPaddableText().getText();
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setText(text.resolve(context));
            CharSequence text2 = this.text.getText();
            Spanned spanned = text2 instanceof Spanned ? (Spanned) text2 : null;
            boolean z = false;
            ClickableSpan[] clickableSpanArr = spanned != null ? (ClickableSpan[]) spanned.getSpans(0, this.text.getText().length(), ClickableSpan.class) : null;
            if (clickableSpanArr != null) {
                if (!(clickableSpanArr.length == 0)) {
                    z = true;
                }
            }
            if (z) {
                this.text.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                this.text.setMovementMethod(null);
            }
        }
    }
}
