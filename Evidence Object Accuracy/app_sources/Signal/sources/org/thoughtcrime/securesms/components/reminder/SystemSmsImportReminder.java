package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import org.thoughtcrime.securesms.DatabaseMigrationActivity;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.service.ApplicationMigrationService;

/* loaded from: classes4.dex */
public class SystemSmsImportReminder extends Reminder {
    public SystemSmsImportReminder(final Context context) {
        super(context.getString(R.string.reminder_header_sms_import_title), context.getString(R.string.reminder_header_sms_import_text));
        SystemSmsImportReminder$$ExternalSyntheticLambda0 systemSmsImportReminder$$ExternalSyntheticLambda0 = new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.SystemSmsImportReminder$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SystemSmsImportReminder.$r8$lambda$uweprfw0eYl8jBysfLNA_LOzDvo(this.f$0, view);
            }
        };
        AnonymousClass1 r1 = new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.reminder.SystemSmsImportReminder.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ApplicationMigrationService.setDatabaseImported(context);
            }
        };
        setOkListener(systemSmsImportReminder$$ExternalSyntheticLambda0);
        setDismissListener(r1);
    }

    public static /* synthetic */ void lambda$new$0(Context context, View view) {
        Intent intent = new Intent(context, ApplicationMigrationService.class);
        intent.setAction(ApplicationMigrationService.MIGRATE_DATABASE);
        context.startService(intent);
        Intent clearTop = MainActivity.clearTop(context);
        Intent intent2 = new Intent(context, DatabaseMigrationActivity.class);
        intent2.putExtra("next_intent", clearTop);
        context.startActivity(intent2);
    }

    public static boolean isEligible(Context context) {
        return !ApplicationMigrationService.isDatabaseImported(context);
    }
}
