package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import android.content.Context;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.installations.FirebaseInstallations;
import j$.util.Optional;
import java.io.IOException;
import java.util.concurrent.ExecutionException;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceConfigurationUpdateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.whispersystems.signalservice.api.SignalServiceAccountManager;
import org.whispersystems.signalservice.api.push.exceptions.AuthorizationFailedException;

/* compiled from: AdvancedPrivacySettingsRepository.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u000bB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001a\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\bJ\u0006\u0010\n\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "disablePushMessages", "", "consumer", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository$DisablePushMessagesResult;", "syncShowSealedSenderIconState", "DisablePushMessagesResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AdvancedPrivacySettingsRepository {
    private final Context context;

    /* compiled from: AdvancedPrivacySettingsRepository.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository$DisablePushMessagesResult;", "", "(Ljava/lang/String;I)V", "SUCCESS", "NETWORK_ERROR", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum DisablePushMessagesResult {
        SUCCESS,
        NETWORK_ERROR
    }

    public AdvancedPrivacySettingsRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final void disablePushMessages(Function1<? super DisablePushMessagesResult, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsRepository$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                AdvancedPrivacySettingsRepository.m847disablePushMessages$lambda0(Function1.this);
            }
        });
    }

    /* renamed from: disablePushMessages$lambda-0 */
    public static final void m847disablePushMessages$lambda0(Function1 function1) {
        DisablePushMessagesResult disablePushMessagesResult;
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        try {
            SignalServiceAccountManager signalServiceAccountManager = ApplicationDependencies.getSignalServiceAccountManager();
            Intrinsics.checkNotNullExpressionValue(signalServiceAccountManager, "getSignalServiceAccountManager()");
            try {
                signalServiceAccountManager.setGcmId(Optional.empty());
            } catch (AuthorizationFailedException e) {
                Log.w(AdvancedPrivacySettingsRepositoryKt.TAG, e);
            }
            if (SignalStore.account().isFcmEnabled()) {
                Tasks.await(FirebaseInstallations.getInstance().delete());
            }
            disablePushMessagesResult = DisablePushMessagesResult.SUCCESS;
        } catch (IOException e2) {
            Log.w(AdvancedPrivacySettingsRepositoryKt.TAG, e2);
            disablePushMessagesResult = DisablePushMessagesResult.NETWORK_ERROR;
        } catch (InterruptedException e3) {
            Log.w(AdvancedPrivacySettingsRepositoryKt.TAG, "Interrupted while deleting", e3);
            disablePushMessagesResult = DisablePushMessagesResult.NETWORK_ERROR;
        } catch (ExecutionException e4) {
            Log.w(AdvancedPrivacySettingsRepositoryKt.TAG, "Error deleting", e4.getCause());
            disablePushMessagesResult = DisablePushMessagesResult.NETWORK_ERROR;
        }
        function1.invoke(disablePushMessagesResult);
    }

    public final void syncShowSealedSenderIconState() {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                AdvancedPrivacySettingsRepository.m848syncShowSealedSenderIconState$lambda1(AdvancedPrivacySettingsRepository.this);
            }
        });
    }

    /* renamed from: syncShowSealedSenderIconState$lambda-1 */
    public static final void m848syncShowSealedSenderIconState$lambda1(AdvancedPrivacySettingsRepository advancedPrivacySettingsRepository) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsRepository, "this$0");
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
        ApplicationDependencies.getJobManager().add(new MultiDeviceConfigurationUpdateJob(TextSecurePreferences.isReadReceiptsEnabled(advancedPrivacySettingsRepository.context), TextSecurePreferences.isTypingIndicatorsEnabled(advancedPrivacySettingsRepository.context), TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(advancedPrivacySettingsRepository.context), SignalStore.settings().isLinkPreviewsEnabled()));
    }
}
