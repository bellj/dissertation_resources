package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.content.Context;
import java.io.IOException;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.ThreadDatabase;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.OutgoingExpirationUpdateMessage;
import org.thoughtcrime.securesms.mms.OutgoingMediaMessage;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.sms.MessageSender;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;

/* compiled from: ExpireTimerSettingsRepository.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0003J3\u0010\u000b\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000e2\u0018\u0010\u000f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u0011\u0012\u0004\u0012\u00020\f0\u0010ø\u0001\u0000J\u001c\u0010\u0012\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\f0\u0014R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u0002\u0004\n\u0002\b\u0019¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "getContext", "()Landroid/content/Context;", "getThreadId", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "setExpiration", "", "newExpirationTime", "", "consumer", "Lkotlin/Function1;", "Lkotlin/Result;", "setUniversalExpireTimerSeconds", "onDone", "Lkotlin/Function0;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsRepository {
    private final Context context;

    public ExpireTimerSettingsRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final Context getContext() {
        return this.context;
    }

    public final void setExpiration(RecipientId recipientId, int i, Function1<? super Result<Integer>, Unit> function1) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(this, i, function1) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ ExpireTimerSettingsRepository f$1;
            public final /* synthetic */ int f$2;
            public final /* synthetic */ Function1 f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ExpireTimerSettingsRepository.m865$r8$lambda$L6lFiGPnSr9EKhhvIVXJm6njE(RecipientId.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: setExpiration$lambda-0 */
    public static final void m866setExpiration$lambda0(RecipientId recipientId, ExpireTimerSettingsRepository expireTimerSettingsRepository, int i, Function1 function1) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(expireTimerSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (!resolved.getGroupId().isPresent() || !resolved.getGroupId().get().isPush()) {
            SignalDatabase.Companion.recipients().setExpireMessages(recipientId, i);
            MessageSender.send(expireTimerSettingsRepository.context, (OutgoingMediaMessage) new OutgoingExpirationUpdateMessage(Recipient.resolved(recipientId), System.currentTimeMillis(), ((long) i) * 1000), expireTimerSettingsRepository.getThreadId(recipientId), false, (String) null, (MessageDatabase.InsertListener) null);
            Result.Companion companion = Result.Companion;
            function1.invoke(Result.m152boximpl(Result.m153constructorimpl(Integer.valueOf(i))));
            return;
        }
        try {
            GroupManager.updateGroupTimer(expireTimerSettingsRepository.context, resolved.getGroupId().get().requirePush(), i);
            Result.Companion companion2 = Result.Companion;
            function1.invoke(Result.m152boximpl(Result.m153constructorimpl(Integer.valueOf(i))));
        } catch (IOException e) {
            Log.w(ExpireTimerSettingsRepositoryKt.TAG, e);
            Result.Companion companion3 = Result.Companion;
            function1.invoke(Result.m152boximpl(Result.m153constructorimpl(ResultKt.createFailure(e))));
        } catch (GroupChangeException e2) {
            Log.w(ExpireTimerSettingsRepositoryKt.TAG, e2);
            Result.Companion companion4 = Result.Companion;
            function1.invoke(Result.m152boximpl(Result.m153constructorimpl(ResultKt.createFailure(e2))));
        }
    }

    public final void setUniversalExpireTimerSeconds(int i, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(function0, "onDone");
        SignalExecutors.BOUNDED.execute(new Runnable(i, function0) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;
            public final /* synthetic */ Function0 f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ExpireTimerSettingsRepository.$r8$lambda$Pcz0ZYsXXmwEYf4hKOnqxJHvLZo(this.f$0, this.f$1);
            }
        });
    }

    /* renamed from: setUniversalExpireTimerSeconds$lambda-1 */
    public static final void m867setUniversalExpireTimerSeconds$lambda1(int i, Function0 function0) {
        Intrinsics.checkNotNullParameter(function0, "$onDone");
        SignalStore.settings().setUniversalExpireTimer(i);
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
        function0.invoke();
    }

    private final long getThreadId(RecipientId recipientId) {
        ThreadDatabase threads = SignalDatabase.Companion.threads();
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        return threads.getOrCreateThreadIdFor(resolved);
    }
}
