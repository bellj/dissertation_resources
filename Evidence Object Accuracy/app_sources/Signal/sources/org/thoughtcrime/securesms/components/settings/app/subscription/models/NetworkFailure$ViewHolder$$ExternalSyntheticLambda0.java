package org.thoughtcrime.securesms.components.settings.app.subscription.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.NetworkFailure;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NetworkFailure$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NetworkFailure.Model f$0;

    public /* synthetic */ NetworkFailure$ViewHolder$$ExternalSyntheticLambda0(NetworkFailure.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NetworkFailure.ViewHolder.m992bind$lambda0(this.f$0, view);
    }
}
