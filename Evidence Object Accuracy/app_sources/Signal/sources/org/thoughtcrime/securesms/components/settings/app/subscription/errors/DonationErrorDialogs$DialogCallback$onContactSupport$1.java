package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import android.content.Context;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;

/* compiled from: DonationErrorDialogs.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class DonationErrorDialogs$DialogCallback$onContactSupport$1 extends Lambda implements Function0<Unit> {
    final /* synthetic */ Context $context;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DonationErrorDialogs$DialogCallback$onContactSupport$1(Context context) {
        super(0);
        this.$context = context;
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        Context context = this.$context;
        context.startActivity(AppSettingsActivity.Companion.help(context, 7));
    }
}
