package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.view.View;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.RegistrationNavigationActivity;

/* loaded from: classes4.dex */
public class PushRegistrationReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    public PushRegistrationReminder(Context context) {
        super(context.getString(R.string.reminder_header_push_title), context.getString(R.string.reminder_header_push_text));
        setOkListener(new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.PushRegistrationReminder$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PushRegistrationReminder.$r8$lambda$83UNjHwhchyB7pz8wPvH0q8Ksh0(this.f$0, view);
            }
        });
    }

    public static /* synthetic */ void lambda$new$0(Context context, View view) {
        context.startActivity(RegistrationNavigationActivity.newIntentForReRegistration(context));
    }

    public static boolean isEligible(Context context) {
        return !SignalStore.account().isRegistered();
    }
}
