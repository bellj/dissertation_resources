package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.database.Cursor;
import android.view.View;
import j$.util.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ThreadPhotoRailView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.SharedMediaPreference;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: SharedMediaPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/SharedMediaPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SharedMediaPreference {
    public static final SharedMediaPreference INSTANCE = new SharedMediaPreference();

    private SharedMediaPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.SharedMediaPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SharedMediaPreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_shared_media));
    }

    /* compiled from: SharedMediaPreference.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\u000b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0018\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\b¢\u0006\u0002\u0010\fJ\u0010\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u0000H\u0016J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R#\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/SharedMediaPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "mediaCursor", "Landroid/database/Cursor;", "mediaIds", "", "", "onMediaRecordClick", "Lkotlin/Function2;", "Lorg/thoughtcrime/securesms/database/MediaDatabase$MediaRecord;", "", "", "(Landroid/database/Cursor;Ljava/util/List;Lkotlin/jvm/functions/Function2;)V", "getMediaCursor", "()Landroid/database/Cursor;", "getMediaIds", "()Ljava/util/List;", "getOnMediaRecordClick", "()Lkotlin/jvm/functions/Function2;", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Cursor mediaCursor;
        private final List<Long> mediaIds;
        private final Function2<MediaDatabase.MediaRecord, Boolean, Unit> onMediaRecordClick;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        public final Cursor getMediaCursor() {
            return this.mediaCursor;
        }

        public final List<Long> getMediaIds() {
            return this.mediaIds;
        }

        public final Function2<MediaDatabase.MediaRecord, Boolean, Unit> getOnMediaRecordClick() {
            return this.onMediaRecordClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.database.MediaDatabase$MediaRecord, ? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Cursor cursor, List<Long> list, Function2<? super MediaDatabase.MediaRecord, ? super Boolean, Unit> function2) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(cursor, "mediaCursor");
            Intrinsics.checkNotNullParameter(list, "mediaIds");
            Intrinsics.checkNotNullParameter(function2, "onMediaRecordClick");
            this.mediaCursor = cursor;
            this.mediaIds = list;
            this.onMediaRecordClick = function2;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(this.mediaIds, model.mediaIds);
        }
    }

    /* compiled from: SharedMediaPreference.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/SharedMediaPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/SharedMediaPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "rail", "Lorg/thoughtcrime/securesms/components/ThreadPhotoRailView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ThreadPhotoRailView rail;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.rail_view);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.rail_view)");
            this.rail = (ThreadPhotoRailView) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            ThreadPhotoRailView threadPhotoRailView = this.rail;
            threadPhotoRailView.setCursor(GlideApp.with(threadPhotoRailView), model.getMediaCursor());
            this.rail.setListener(new SharedMediaPreference$ViewHolder$$ExternalSyntheticLambda0(model, this));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1215bind$lambda0(Model model, ViewHolder viewHolder, MediaDatabase.MediaRecord mediaRecord) {
            Intrinsics.checkNotNullParameter(model, "$model");
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Function2<MediaDatabase.MediaRecord, Boolean, Unit> onMediaRecordClick = model.getOnMediaRecordClick();
            Intrinsics.checkNotNullExpressionValue(mediaRecord, "it");
            onMediaRecordClick.invoke(mediaRecord, Boolean.valueOf(ViewUtil.isLtr(viewHolder.rail)));
        }
    }
}
