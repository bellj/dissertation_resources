package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.LayerDrawable;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: DSLSettingsIcon.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00072\u00020\u0001:\u0004\u0007\b\t\nB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H&\u0001\u0003\u000b\f\r¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "", "()V", "resolve", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "Companion", "FromDrawable", "FromResource", "FromResourceWithBackground", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$FromResource;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$FromResourceWithBackground;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$FromDrawable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DSLSettingsIcon {
    public static final Companion Companion = new Companion(null);

    public /* synthetic */ DSLSettingsIcon(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    @JvmStatic
    public static final DSLSettingsIcon from(int i, int i2) {
        return Companion.from(i, i2);
    }

    @JvmStatic
    public static final DSLSettingsIcon from(int i, int i2, int i3, int i4, int i5) {
        return Companion.from(i, i2, i3, i4, i5);
    }

    @JvmStatic
    public static final DSLSettingsIcon from(Drawable drawable) {
        return Companion.from(drawable);
    }

    public abstract Drawable resolve(Context context);

    private DSLSettingsIcon() {
    }

    /* compiled from: DSLSettingsIcon.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0019\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\u0006\u001a\u00020\u0003HÂ\u0003J\t\u0010\u0007\u001a\u00020\u0003HÂ\u0003J\u001d\u0010\b\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u0003HÖ\u0001J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$FromResource;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "iconId", "", "iconTintId", "(II)V", "component1", "component2", "copy", "equals", "", "other", "", "hashCode", "resolve", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FromResource extends DSLSettingsIcon {
        private final int iconId;
        private final int iconTintId;

        private final int component1() {
            return this.iconId;
        }

        private final int component2() {
            return this.iconTintId;
        }

        public static /* synthetic */ FromResource copy$default(FromResource fromResource, int i, int i2, int i3, Object obj) {
            if ((i3 & 1) != 0) {
                i = fromResource.iconId;
            }
            if ((i3 & 2) != 0) {
                i2 = fromResource.iconTintId;
            }
            return fromResource.copy(i, i2);
        }

        public final FromResource copy(int i, int i2) {
            return new FromResource(i, i2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FromResource)) {
                return false;
            }
            FromResource fromResource = (FromResource) obj;
            return this.iconId == fromResource.iconId && this.iconTintId == fromResource.iconTintId;
        }

        public int hashCode() {
            return (this.iconId * 31) + this.iconTintId;
        }

        public String toString() {
            return "FromResource(iconId=" + this.iconId + ", iconTintId=" + this.iconTintId + ')';
        }

        public FromResource(int i, int i2) {
            super(null);
            this.iconId = i;
            this.iconTintId = i2;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsIcon
        public Drawable resolve(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            Drawable drawable = ContextCompat.getDrawable(context, this.iconId);
            if (drawable != null) {
                if (this.iconTintId != -1) {
                    drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(context, this.iconTintId), PorterDuff.Mode.SRC_IN));
                }
                Intrinsics.checkNotNullExpressionValue(drawable, "requireNotNull(ContextCo…ode.SRC_IN)\n      }\n    }");
                return drawable;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* compiled from: DSLSettingsIcon.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\f\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B7\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\t\u001a\u00020\u0003HÂ\u0003J\t\u0010\n\u001a\u00020\u0003HÂ\u0003J\t\u0010\u000b\u001a\u00020\u0003HÂ\u0003J\t\u0010\f\u001a\u00020\u0003HÂ\u0003J\t\u0010\r\u001a\u00020\u0003HÂ\u0003J;\u0010\u000e\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0003\u0010\u0004\u001a\u00020\u00032\b\b\u0003\u0010\u0005\u001a\u00020\u00032\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0003\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÖ\u0001J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u000e\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$FromResourceWithBackground;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "iconId", "", "iconTintId", "backgroundId", "backgroundTint", "insetPx", "(IIIII)V", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "", "hashCode", "resolve", "Landroid/graphics/drawable/Drawable;", "context", "Landroid/content/Context;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FromResourceWithBackground extends DSLSettingsIcon {
        private final int backgroundId;
        private final int backgroundTint;
        private final int iconId;
        private final int iconTintId;
        private final int insetPx;

        private final int component1() {
            return this.iconId;
        }

        private final int component2() {
            return this.iconTintId;
        }

        private final int component3() {
            return this.backgroundId;
        }

        private final int component4() {
            return this.backgroundTint;
        }

        private final int component5() {
            return this.insetPx;
        }

        public static /* synthetic */ FromResourceWithBackground copy$default(FromResourceWithBackground fromResourceWithBackground, int i, int i2, int i3, int i4, int i5, int i6, Object obj) {
            if ((i6 & 1) != 0) {
                i = fromResourceWithBackground.iconId;
            }
            if ((i6 & 2) != 0) {
                i2 = fromResourceWithBackground.iconTintId;
            }
            if ((i6 & 4) != 0) {
                i3 = fromResourceWithBackground.backgroundId;
            }
            if ((i6 & 8) != 0) {
                i4 = fromResourceWithBackground.backgroundTint;
            }
            if ((i6 & 16) != 0) {
                i5 = fromResourceWithBackground.insetPx;
            }
            return fromResourceWithBackground.copy(i, i2, i3, i4, i5);
        }

        public final FromResourceWithBackground copy(int i, int i2, int i3, int i4, int i5) {
            return new FromResourceWithBackground(i, i2, i3, i4, i5);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FromResourceWithBackground)) {
                return false;
            }
            FromResourceWithBackground fromResourceWithBackground = (FromResourceWithBackground) obj;
            return this.iconId == fromResourceWithBackground.iconId && this.iconTintId == fromResourceWithBackground.iconTintId && this.backgroundId == fromResourceWithBackground.backgroundId && this.backgroundTint == fromResourceWithBackground.backgroundTint && this.insetPx == fromResourceWithBackground.insetPx;
        }

        public int hashCode() {
            return (((((((this.iconId * 31) + this.iconTintId) * 31) + this.backgroundId) * 31) + this.backgroundTint) * 31) + this.insetPx;
        }

        public String toString() {
            return "FromResourceWithBackground(iconId=" + this.iconId + ", iconTintId=" + this.iconTintId + ", backgroundId=" + this.backgroundId + ", backgroundTint=" + this.backgroundTint + ", insetPx=" + this.insetPx + ')';
        }

        public FromResourceWithBackground(int i, int i2, int i3, int i4, int i5) {
            super(null);
            this.iconId = i;
            this.iconTintId = i2;
            this.backgroundId = i3;
            this.backgroundTint = i4;
            this.insetPx = i5;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsIcon
        public Drawable resolve(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            Drawable resolve = new FromResource(this.iconId, this.iconTintId).resolve(context);
            int i = this.insetPx;
            return new LayerDrawable(new Drawable[]{new FromResource(this.backgroundId, this.backgroundTint).resolve(context), new InsetDrawable(resolve, i, i, i, i)});
        }
    }

    /* compiled from: DSLSettingsIcon.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0005\u001a\u00020\u0003HÂ\u0003J\u0013\u0010\u0006\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nHÖ\u0003J\t\u0010\u000b\u001a\u00020\fHÖ\u0001J\u0010\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$FromDrawable;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "drawable", "Landroid/graphics/drawable/Drawable;", "(Landroid/graphics/drawable/Drawable;)V", "component1", "copy", "equals", "", "other", "", "hashCode", "", "resolve", "context", "Landroid/content/Context;", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FromDrawable extends DSLSettingsIcon {
        private final Drawable drawable;

        private final Drawable component1() {
            return this.drawable;
        }

        public static /* synthetic */ FromDrawable copy$default(FromDrawable fromDrawable, Drawable drawable, int i, Object obj) {
            if ((i & 1) != 0) {
                drawable = fromDrawable.drawable;
            }
            return fromDrawable.copy(drawable);
        }

        public final FromDrawable copy(Drawable drawable) {
            Intrinsics.checkNotNullParameter(drawable, "drawable");
            return new FromDrawable(drawable);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return (obj instanceof FromDrawable) && Intrinsics.areEqual(this.drawable, ((FromDrawable) obj).drawable);
        }

        public int hashCode() {
            return this.drawable.hashCode();
        }

        public String toString() {
            return "FromDrawable(drawable=" + this.drawable + ')';
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public FromDrawable(Drawable drawable) {
            super(null);
            Intrinsics.checkNotNullParameter(drawable, "drawable");
            this.drawable = drawable;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsIcon
        public Drawable resolve(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return this.drawable;
        }
    }

    /* compiled from: DSLSettingsIcon.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007J\u001c\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0007\u001a\u00020\b2\b\b\u0003\u0010\t\u001a\u00020\bH\u0007J:\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\b2\b\b\u0001\u0010\n\u001a\u00020\b2\b\b\u0001\u0010\u000b\u001a\u00020\b2\b\b\u0003\u0010\f\u001a\u00020\bH\u0007¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon$Companion;", "", "()V", "from", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "drawable", "Landroid/graphics/drawable/Drawable;", "iconId", "", "iconTintId", "backgroundId", "backgroundTint", "insetPx", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final DSLSettingsIcon from(int i, int i2, int i3, int i4, int i5) {
            return new FromResourceWithBackground(i, i2, i3, i4, i5);
        }

        public static /* synthetic */ DSLSettingsIcon from$default(Companion companion, int i, int i2, int i3, Object obj) {
            if ((i3 & 2) != 0) {
                i2 = R.color.signal_icon_tint_primary;
            }
            return companion.from(i, i2);
        }

        @JvmStatic
        public final DSLSettingsIcon from(int i, int i2) {
            return new FromResource(i, i2);
        }

        @JvmStatic
        public final DSLSettingsIcon from(Drawable drawable) {
            Intrinsics.checkNotNullParameter(drawable, "drawable");
            return new FromDrawable(drawable);
        }
    }
}
