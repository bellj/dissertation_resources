package org.thoughtcrime.securesms.components.settings.app.notifications.manual.models;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.Group;
import com.airbnb.lottie.SimpleColorFilter;
import j$.time.LocalDateTime;
import j$.time.LocalTime;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: NotificationProfileSelection.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001:\u0004\n\u000b\f\rB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection;", "", "()V", "TOGGLE_EXPANSION", "", "UPDATE_TIMESLOT", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Entry", "EntryViewHolder", "New", "NewViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileSelection {
    public static final NotificationProfileSelection INSTANCE = new NotificationProfileSelection();
    private static final int TOGGLE_EXPANSION;
    private static final int UPDATE_TIMESLOT;

    private NotificationProfileSelection() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(New.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new NotificationProfileSelection.NewViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.new_notification_profile_pref));
        mappingAdapter.registerFactory(Entry.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new NotificationProfileSelection.EntryViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.notification_profile_entry_pref));
    }

    /* compiled from: NotificationProfileSelection.kt */
    @Metadata(bv = {}, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0001\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\u000b\u0012\u0006\u0010\u0011\u001a\u00020\u0010\u0012\u0006\u0010\u0015\u001a\u00020\u0003\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0012\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b\u0012\u0012\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b\u0012\u0018\u0010$\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u001c0#\u0012\u0012\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b\u0012\u0012\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b¢\u0006\u0004\b,\u0010-J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0000H\u0016J\u0010\u0010\u0005\u001a\u00020\u00032\u0006\u0010\u0002\u001a\u00020\u0000H\u0016J\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0002\u001a\u00020\u0000H\u0016R\u0017\u0010\b\u001a\u00020\u00038\u0006¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\b\u0010\nR\u001a\u0010\f\u001a\u00020\u000b8\u0016X\u0004¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\u0011\u001a\u00020\u00108\u0006¢\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\u0015\u001a\u00020\u00038\u0006¢\u0006\f\n\u0004\b\u0015\u0010\t\u001a\u0004\b\u0015\u0010\nR\u0017\u0010\u0017\u001a\u00020\u00168\u0006¢\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0019\u0010\u001aR#\u0010\u001d\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b8\u0006¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R#\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b8\u0006¢\u0006\f\n\u0004\b!\u0010\u001e\u001a\u0004\b\"\u0010 R)\u0010$\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020\u001c0#8\u0006¢\u0006\f\n\u0004\b$\u0010%\u001a\u0004\b&\u0010'R#\u0010(\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b8\u0006¢\u0006\f\n\u0004\b(\u0010\u001e\u001a\u0004\b)\u0010 R#\u0010*\u001a\u000e\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u001c0\u001b8\u0006¢\u0006\f\n\u0004\b*\u0010\u001e\u001a\u0004\b+\u0010 ¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection$Entry;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "newItem", "", "areItemsTheSame", "areContentsTheSame", "", "getChangePayload", "isOn", "Z", "()Z", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "notificationProfile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "getNotificationProfile", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "isExpanded", "j$/time/LocalDateTime", "timeSlotB", "Lj$/time/LocalDateTime;", "getTimeSlotB", "()Lj$/time/LocalDateTime;", "Lkotlin/Function1;", "", "onRowClick", "Lkotlin/jvm/functions/Function1;", "getOnRowClick", "()Lkotlin/jvm/functions/Function1;", "onTimeSlotAClick", "getOnTimeSlotAClick", "Lkotlin/Function2;", "onTimeSlotBClick", "Lkotlin/jvm/functions/Function2;", "getOnTimeSlotBClick", "()Lkotlin/jvm/functions/Function2;", "onViewSettingsClick", "getOnViewSettingsClick", "onToggleClick", "getOnToggleClick", "<init>", "(ZLorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;ZLj$/time/LocalDateTime;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function2;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes4.dex */
    public static final class Entry extends PreferenceModel<Entry> {
        private final boolean isExpanded;
        private final boolean isOn;
        private final NotificationProfile notificationProfile;
        private final Function1<NotificationProfile, Unit> onRowClick;
        private final Function1<NotificationProfile, Unit> onTimeSlotAClick;
        private final Function2<NotificationProfile, LocalDateTime, Unit> onTimeSlotBClick;
        private final Function1<NotificationProfile, Unit> onToggleClick;
        private final Function1<NotificationProfile, Unit> onViewSettingsClick;
        private final DSLSettingsText summary;
        private final LocalDateTime timeSlotB;

        public final boolean isOn() {
            return this.isOn;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsText getSummary() {
            return this.summary;
        }

        public final NotificationProfile getNotificationProfile() {
            return this.notificationProfile;
        }

        public final boolean isExpanded() {
            return this.isExpanded;
        }

        public final LocalDateTime getTimeSlotB() {
            return this.timeSlotB;
        }

        public final Function1<NotificationProfile, Unit> getOnRowClick() {
            return this.onRowClick;
        }

        public final Function1<NotificationProfile, Unit> getOnTimeSlotAClick() {
            return this.onTimeSlotAClick;
        }

        public final Function2<NotificationProfile, LocalDateTime, Unit> getOnTimeSlotBClick() {
            return this.onTimeSlotBClick;
        }

        public final Function1<NotificationProfile, Unit> getOnViewSettingsClick() {
            return this.onViewSettingsClick;
        }

        public final Function1<NotificationProfile, Unit> getOnToggleClick() {
            return this.onToggleClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r23v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r24v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r25v0, resolved type: kotlin.jvm.functions.Function2<? super org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, ? super j$.time.LocalDateTime, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r26v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r27v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Entry(boolean z, DSLSettingsText dSLSettingsText, NotificationProfile notificationProfile, boolean z2, LocalDateTime localDateTime, Function1<? super NotificationProfile, Unit> function1, Function1<? super NotificationProfile, Unit> function12, Function2<? super NotificationProfile, ? super LocalDateTime, Unit> function2, Function1<? super NotificationProfile, Unit> function13, Function1<? super NotificationProfile, Unit> function14) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(dSLSettingsText, "summary");
            Intrinsics.checkNotNullParameter(notificationProfile, "notificationProfile");
            Intrinsics.checkNotNullParameter(localDateTime, "timeSlotB");
            Intrinsics.checkNotNullParameter(function1, "onRowClick");
            Intrinsics.checkNotNullParameter(function12, "onTimeSlotAClick");
            Intrinsics.checkNotNullParameter(function2, "onTimeSlotBClick");
            Intrinsics.checkNotNullParameter(function13, "onViewSettingsClick");
            Intrinsics.checkNotNullParameter(function14, "onToggleClick");
            this.isOn = z;
            this.summary = dSLSettingsText;
            this.notificationProfile = notificationProfile;
            this.isExpanded = z2;
            this.timeSlotB = localDateTime;
            this.onRowClick = function1;
            this.onTimeSlotAClick = function12;
            this.onTimeSlotBClick = function2;
            this.onViewSettingsClick = function13;
            this.onToggleClick = function14;
        }

        public boolean areItemsTheSame(Entry entry) {
            Intrinsics.checkNotNullParameter(entry, "newItem");
            return this.notificationProfile.getId() == entry.notificationProfile.getId();
        }

        public boolean areContentsTheSame(Entry entry) {
            Intrinsics.checkNotNullParameter(entry, "newItem");
            return super.areContentsTheSame(entry) && this.isOn == entry.isOn && Intrinsics.areEqual(this.notificationProfile, entry.notificationProfile) && this.isExpanded == entry.isExpanded && Intrinsics.areEqual(this.timeSlotB, entry.timeSlotB);
        }

        public Object getChangePayload(Entry entry) {
            Intrinsics.checkNotNullParameter(entry, "newItem");
            if (!Intrinsics.areEqual(this.notificationProfile, entry.notificationProfile) || this.isExpanded == entry.isExpanded) {
                return (!Intrinsics.areEqual(this.notificationProfile, entry.notificationProfile) || Intrinsics.areEqual(this.timeSlotB, entry.timeSlotB)) ? null : 1;
            }
            return 0;
        }
    }

    /* compiled from: NotificationProfileSelection.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0016J\u0010\u0010\u0014\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0002H\u0002R\u000e\u0010\u0006\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection$EntryViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection$Entry;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "chevron", "expansion", "Landroidx/constraintlayout/widget/Group;", DraftDatabase.Draft.IMAGE, "Lorg/thoughtcrime/securesms/components/emoji/EmojiImageView;", "name", "Landroid/widget/TextView;", "status", "timeSlotA", "timeSlotB", "viewSettings", "bind", "", "model", "presentStatus", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EntryViewHolder extends MappingViewHolder<Entry> {
        private final View chevron;
        private final Group expansion;
        private final EmojiImageView image;
        private final TextView name;
        private final TextView status;
        private final TextView timeSlotA;
        private final TextView timeSlotB;
        private final View viewSettings;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public EntryViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.notification_preference_image);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.notification_preference_image)");
            this.image = (EmojiImageView) findViewById;
            View findViewById2 = findViewById(R.id.notification_preference_chevron);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.notification_preference_chevron)");
            this.chevron = findViewById2;
            View findViewById3 = findViewById(R.id.notification_preference_name);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.notification_preference_name)");
            this.name = (TextView) findViewById3;
            View findViewById4 = findViewById(R.id.notification_preference_status);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.notification_preference_status)");
            this.status = (TextView) findViewById4;
            View findViewById5 = findViewById(R.id.notification_preference_expanded);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.notifi…tion_preference_expanded)");
            this.expansion = (Group) findViewById5;
            View findViewById6 = findViewById(R.id.notification_preference_1hr);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.notification_preference_1hr)");
            this.timeSlotA = (TextView) findViewById6;
            View findViewById7 = findViewById(R.id.notification_preference_6pm);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "findViewById(R.id.notification_preference_6pm)");
            this.timeSlotB = (TextView) findViewById7;
            View findViewById8 = findViewById(R.id.notification_preference_view_settings);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "findViewById(R.id.notifi…preference_view_settings)");
            this.viewSettings = findViewById8;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m721bind$lambda0(Entry entry, View view) {
            Intrinsics.checkNotNullParameter(entry, "$model");
            entry.getOnRowClick().invoke(entry.getNotificationProfile());
        }

        public void bind(Entry entry) {
            Intrinsics.checkNotNullParameter(entry, "model");
            this.itemView.setOnClickListener(new NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda0(entry));
            this.chevron.setOnClickListener(new NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda1(entry));
            this.chevron.setRotation(entry.isExpanded() ? 180.0f : 0.0f);
            this.timeSlotA.setOnClickListener(new NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda2(entry));
            this.timeSlotB.setOnClickListener(new NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda3(entry));
            this.viewSettings.setOnClickListener(new NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda4(entry));
            ViewExtensionsKt.setVisible(this.expansion, entry.isExpanded());
            TextView textView = this.timeSlotB;
            Context context = this.context;
            LocalTime from = LocalTime.from(entry.getTimeSlotB());
            Intrinsics.checkNotNullExpressionValue(from, "from(model.timeSlotB)");
            Context context2 = this.context;
            Intrinsics.checkNotNullExpressionValue(context2, "context");
            textView.setText(context.getString(R.string.NotificationProfileSelection__until_s, JavaTimeExtensionsKt.formatHours(from, context2)));
            if (!this.payload.contains(0) && !this.payload.contains(1)) {
                this.image.getBackground().setColorFilter(new SimpleColorFilter(entry.getNotificationProfile().getColor().colorInt()));
                if (entry.getNotificationProfile().getEmoji().length() > 0) {
                    this.image.setImageEmoji(entry.getNotificationProfile().getEmoji());
                } else {
                    this.image.setImageResource(R.drawable.ic_moon_24);
                }
                this.name.setText(entry.getNotificationProfile().getName());
                presentStatus(entry);
                TextView textView2 = this.timeSlotB;
                Context context3 = this.context;
                LocalTime from2 = LocalTime.from(entry.getTimeSlotB());
                Intrinsics.checkNotNullExpressionValue(from2, "from(model.timeSlotB)");
                Context context4 = this.context;
                Intrinsics.checkNotNullExpressionValue(context4, "context");
                textView2.setText(context3.getString(R.string.NotificationProfileSelection__until_s, JavaTimeExtensionsKt.formatHours(from2, context4)));
                this.itemView.setSelected(entry.isOn());
            }
        }

        /* renamed from: bind$lambda-1 */
        public static final void m722bind$lambda1(Entry entry, View view) {
            Intrinsics.checkNotNullParameter(entry, "$model");
            entry.getOnToggleClick().invoke(entry.getNotificationProfile());
        }

        /* renamed from: bind$lambda-2 */
        public static final void m723bind$lambda2(Entry entry, View view) {
            Intrinsics.checkNotNullParameter(entry, "$model");
            entry.getOnTimeSlotAClick().invoke(entry.getNotificationProfile());
        }

        /* renamed from: bind$lambda-3 */
        public static final void m724bind$lambda3(Entry entry, View view) {
            Intrinsics.checkNotNullParameter(entry, "$model");
            entry.getOnTimeSlotBClick().invoke(entry.getNotificationProfile(), entry.getTimeSlotB());
        }

        /* renamed from: bind$lambda-4 */
        public static final void m725bind$lambda4(Entry entry, View view) {
            Intrinsics.checkNotNullParameter(entry, "$model");
            entry.getOnViewSettingsClick().invoke(entry.getNotificationProfile());
        }

        private final void presentStatus(Entry entry) {
            this.status.setEnabled(entry.isOn());
            TextView textView = this.status;
            DSLSettingsText summary = entry.getSummary();
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setText(summary.resolve(context));
        }
    }

    /* compiled from: NotificationProfileSelection.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0000H\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection$New;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "onClick", "Lkotlin/Function0;", "", "(Lkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class New extends PreferenceModel<New> {
        private final Function0<Unit> onClick;

        public boolean areItemsTheSame(New r2) {
            Intrinsics.checkNotNullParameter(r2, "newItem");
            return true;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public New(Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.onClick = function0;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }
    }

    /* compiled from: NotificationProfileSelection.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection$NewViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/models/NotificationProfileSelection$New;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NewViewHolder extends MappingViewHolder<New> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public NewViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        /* renamed from: bind$lambda-0 */
        public static final void m726bind$lambda0(New r0, View view) {
            Intrinsics.checkNotNullParameter(r0, "$model");
            r0.getOnClick().invoke();
        }

        public void bind(New r3) {
            Intrinsics.checkNotNullParameter(r3, "model");
            this.itemView.setOnClickListener(new NotificationProfileSelection$NewViewHolder$$ExternalSyntheticLambda0(r3));
        }
    }
}
