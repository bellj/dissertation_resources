package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.database.model.IdentityRecord;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$6$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ IdentityRecord f$0;

    public /* synthetic */ ConversationSettingsViewModel$RecipientSettingsViewModel$6$$ExternalSyntheticLambda0(IdentityRecord identityRecord) {
        this.f$0 = identityRecord;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.RecipientSettingsViewModel.AnonymousClass6.m1159invoke$lambda0(this.f$0, (ConversationSettingsState) obj);
    }
}
