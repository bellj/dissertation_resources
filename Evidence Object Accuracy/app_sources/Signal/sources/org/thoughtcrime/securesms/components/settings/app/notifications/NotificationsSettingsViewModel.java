package org.thoughtcrime.securesms.components.settings.app.notifications;

import android.content.SharedPreferences;
import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: NotificationsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0001&B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\b\u001a\u00020\u0007H\u0002J\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0010\u0010\u0010\u001a\u00020\r2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u000e\u0010\u0013\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0014\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0015\u001a\u00020\r2\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\r2\u0006\u0010\u0019\u001a\u00020\u0017J\u000e\u0010\u001a\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\u001cJ\u000e\u0010\u001d\u001a\u00020\r2\u0006\u0010\u001e\u001a\u00020\u0017J\u000e\u0010\u001f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010 \u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fJ\u0010\u0010!\u001a\u00020\r2\b\u0010\"\u001a\u0004\u0018\u00010\u0012J\u000e\u0010#\u001a\u00020\r2\u0006\u0010$\u001a\u00020\u001cJ\u000e\u0010%\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "sharedPreferences", "Landroid/content/SharedPreferences;", "(Landroid/content/SharedPreferences;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "setCallNotificationsEnabled", "", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "setCallRingtone", "ringtone", "Landroid/net/Uri;", "setCallVibrateEnabled", "setMessageNotificationInChatSoundsEnabled", "setMessageNotificationLedBlink", "blink", "", "setMessageNotificationLedColor", NotificationProfileDatabase.NotificationProfileTable.COLOR, "setMessageNotificationPriority", "priority", "", "setMessageNotificationPrivacy", "preference", "setMessageNotificationVibration", "setMessageNotificationsEnabled", "setMessageNotificationsSound", "sound", "setMessageRepeatAlerts", "repeats", "setNotifyWhenContactJoinsSignal", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationsSettingsViewModel extends ViewModel {
    private final SharedPreferences sharedPreferences;
    private final LiveData<NotificationsSettingsState> state;
    private final Store<NotificationsSettingsState> store;

    public NotificationsSettingsViewModel(SharedPreferences sharedPreferences) {
        Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
        this.sharedPreferences = sharedPreferences;
        if (NotificationChannels.supported()) {
            SignalStore.settings().setMessageNotificationSound(NotificationChannels.getMessageRingtone(ApplicationDependencies.getApplication()));
            SignalStore.settings().setMessageVibrateEnabled(NotificationChannels.getMessageVibrate(ApplicationDependencies.getApplication()));
        }
        Store<NotificationsSettingsState> store = new Store<>(getState());
        this.store = store;
        LiveData<NotificationsSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    /* renamed from: getState */
    public final LiveData<NotificationsSettingsState> m712getState() {
        return this.state;
    }

    public final void setMessageNotificationsEnabled(boolean z) {
        SignalStore.settings().setMessageNotificationsEnabled(z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda10
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m708setMessageNotificationsEnabled$lambda0(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationsEnabled$lambda-0 */
    public static final NotificationsSettingsState m708setMessageNotificationsEnabled$lambda0(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationsSound(Uri uri) {
        if (uri == null) {
            uri = Uri.EMPTY;
        }
        SignalStore.settings().setMessageNotificationSound(uri);
        NotificationChannels.updateMessageRingtone(ApplicationDependencies.getApplication(), uri);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m709setMessageNotificationsSound$lambda1(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationsSound$lambda-1 */
    public static final NotificationsSettingsState m709setMessageNotificationsSound$lambda1(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationVibration(boolean z) {
        SignalStore.settings().setMessageVibrateEnabled(z);
        NotificationChannels.updateMessageVibrate(ApplicationDependencies.getApplication(), z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda12
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m707setMessageNotificationVibration$lambda2(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationVibration$lambda-2 */
    public static final NotificationsSettingsState m707setMessageNotificationVibration$lambda2(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationLedColor(String str) {
        Intrinsics.checkNotNullParameter(str, NotificationProfileDatabase.NotificationProfileTable.COLOR);
        SignalStore.settings().setMessageLedColor(str);
        NotificationChannels.updateMessagesLedColor(ApplicationDependencies.getApplication(), str);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m704setMessageNotificationLedColor$lambda3(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationLedColor$lambda-3 */
    public static final NotificationsSettingsState m704setMessageNotificationLedColor$lambda3(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationLedBlink(String str) {
        Intrinsics.checkNotNullParameter(str, "blink");
        SignalStore.settings().setMessageLedBlinkPattern(str);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m703setMessageNotificationLedBlink$lambda4(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationLedBlink$lambda-4 */
    public static final NotificationsSettingsState m703setMessageNotificationLedBlink$lambda4(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationInChatSoundsEnabled(boolean z) {
        SignalStore.settings().setMessageNotificationsInChatSoundsEnabled(z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m702setMessageNotificationInChatSoundsEnabled$lambda5(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationInChatSoundsEnabled$lambda-5 */
    public static final NotificationsSettingsState m702setMessageNotificationInChatSoundsEnabled$lambda5(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageRepeatAlerts(int i) {
        SignalStore.settings().setMessageNotificationsRepeatAlerts(i);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m710setMessageRepeatAlerts$lambda6(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageRepeatAlerts$lambda-6 */
    public static final NotificationsSettingsState m710setMessageRepeatAlerts$lambda6(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationPrivacy(String str) {
        Intrinsics.checkNotNullParameter(str, "preference");
        SignalStore.settings().setMessageNotificationsPrivacy(new NotificationPrivacyPreference(str));
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m706setMessageNotificationPrivacy$lambda7(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationPrivacy$lambda-7 */
    public static final NotificationsSettingsState m706setMessageNotificationPrivacy$lambda7(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setMessageNotificationPriority(int i) {
        this.sharedPreferences.edit().putString(TextSecurePreferences.NOTIFICATION_PRIORITY_PREF, String.valueOf(i)).apply();
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m705setMessageNotificationPriority$lambda8(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setMessageNotificationPriority$lambda-8 */
    public static final NotificationsSettingsState m705setMessageNotificationPriority$lambda8(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setCallNotificationsEnabled(boolean z) {
        SignalStore.settings().setCallNotificationsEnabled(z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m699setCallNotificationsEnabled$lambda9(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setCallNotificationsEnabled$lambda-9 */
    public static final NotificationsSettingsState m699setCallNotificationsEnabled$lambda9(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setCallRingtone(Uri uri) {
        SettingsValues settingsValues = SignalStore.settings();
        if (uri == null) {
            uri = Uri.EMPTY;
            Intrinsics.checkNotNullExpressionValue(uri, "EMPTY");
        }
        settingsValues.setCallRingtone(uri);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda8
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m700setCallRingtone$lambda10(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setCallRingtone$lambda-10 */
    public static final NotificationsSettingsState m700setCallRingtone$lambda10(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setCallVibrateEnabled(boolean z) {
        SignalStore.settings().setCallVibrateEnabled(z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda7
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m701setCallVibrateEnabled$lambda11(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setCallVibrateEnabled$lambda-11 */
    public static final NotificationsSettingsState m701setCallVibrateEnabled$lambda11(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    public final void setNotifyWhenContactJoinsSignal(boolean z) {
        SignalStore.settings().setNotifyWhenContactJoinsSignal(z);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationsSettingsViewModel.m711setNotifyWhenContactJoinsSignal$lambda12(NotificationsSettingsViewModel.this, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: setNotifyWhenContactJoinsSignal$lambda-12 */
    public static final NotificationsSettingsState m711setNotifyWhenContactJoinsSignal$lambda12(NotificationsSettingsViewModel notificationsSettingsViewModel, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(notificationsSettingsViewModel, "this$0");
        return notificationsSettingsViewModel.getState();
    }

    private final NotificationsSettingsState getState() {
        boolean isMessageNotificationsEnabled = SignalStore.settings().isMessageNotificationsEnabled();
        Uri messageNotificationSound = SignalStore.settings().getMessageNotificationSound();
        Intrinsics.checkNotNullExpressionValue(messageNotificationSound, "settings().messageNotificationSound");
        boolean isMessageVibrateEnabled = SignalStore.settings().isMessageVibrateEnabled();
        String messageLedColor = SignalStore.settings().getMessageLedColor();
        Intrinsics.checkNotNullExpressionValue(messageLedColor, "settings().messageLedColor");
        String messageLedBlinkPattern = SignalStore.settings().getMessageLedBlinkPattern();
        Intrinsics.checkNotNullExpressionValue(messageLedBlinkPattern, "settings().messageLedBlinkPattern");
        boolean isMessageNotificationsInChatSoundsEnabled = SignalStore.settings().isMessageNotificationsInChatSoundsEnabled();
        int messageNotificationsRepeatAlerts = SignalStore.settings().getMessageNotificationsRepeatAlerts();
        String notificationPrivacyPreference = SignalStore.settings().getMessageNotificationsPrivacy().toString();
        Intrinsics.checkNotNullExpressionValue(notificationPrivacyPreference, "settings().messageNotificationsPrivacy.toString()");
        MessageNotificationsState messageNotificationsState = new MessageNotificationsState(isMessageNotificationsEnabled, messageNotificationSound, isMessageVibrateEnabled, messageLedColor, messageLedBlinkPattern, isMessageNotificationsInChatSoundsEnabled, messageNotificationsRepeatAlerts, notificationPrivacyPreference, TextSecurePreferences.getNotificationPriority(ApplicationDependencies.getApplication()));
        boolean isCallNotificationsEnabled = SignalStore.settings().isCallNotificationsEnabled();
        Uri callRingtone = SignalStore.settings().getCallRingtone();
        Intrinsics.checkNotNullExpressionValue(callRingtone, "settings().callRingtone");
        return new NotificationsSettingsState(messageNotificationsState, new CallNotificationsState(isCallNotificationsEnabled, callRingtone, SignalStore.settings().isCallVibrateEnabled()), SignalStore.settings().isNotifyWhenContactJoinsSignal());
    }

    /* compiled from: NotificationsSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "sharedPreferences", "Landroid/content/SharedPreferences;", "(Landroid/content/SharedPreferences;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final SharedPreferences sharedPreferences;

        public Factory(SharedPreferences sharedPreferences) {
            Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
            this.sharedPreferences = sharedPreferences;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new NotificationsSettingsViewModel(this.sharedPreferences));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
