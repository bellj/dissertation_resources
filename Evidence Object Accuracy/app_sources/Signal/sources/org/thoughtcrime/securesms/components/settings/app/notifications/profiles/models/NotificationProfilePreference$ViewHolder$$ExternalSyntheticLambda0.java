package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfilePreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfilePreference.Model f$0;

    public /* synthetic */ NotificationProfilePreference$ViewHolder$$ExternalSyntheticLambda0(NotificationProfilePreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfilePreference.ViewHolder.m817bind$lambda0(this.f$0, view);
    }
}
