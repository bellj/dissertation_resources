package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.text.Annotation;
import android.text.Editable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import androidx.core.content.ContextCompat;
import androidx.core.view.inputmethod.EditorInfoCompat;
import androidx.core.view.inputmethod.InputConnectionCompat;
import androidx.core.view.inputmethod.InputContentInfoCompat;
import java.util.List;
import java.util.Objects;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.InputPanel;
import org.thoughtcrime.securesms.components.emoji.EmojiEditText;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.mention.MentionDeleter;
import org.thoughtcrime.securesms.components.mention.MentionRendererDelegate;
import org.thoughtcrime.securesms.components.mention.MentionValidatorWatcher;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.database.MentionUtil;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class ComposeText extends EmojiEditText {
    private CursorPositionChangedListener cursorPositionChangedListener;
    private CharSequence hint;
    private InputPanel.MediaListener mediaListener;
    private MentionQueryChangedListener mentionQueryChangedListener;
    private MentionRendererDelegate mentionRendererDelegate;
    private MentionValidatorWatcher mentionValidatorWatcher;
    private SpannableString subHint;

    /* loaded from: classes4.dex */
    public interface CursorPositionChangedListener {
        void onCursorPositionChanged(int i, int i2);
    }

    /* loaded from: classes4.dex */
    public interface MentionQueryChangedListener {
        void onQueryChanged(String str);
    }

    public ComposeText(Context context) {
        super(context);
        initialize();
    }

    public ComposeText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public ComposeText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    public CharSequence getTextTrimmed() {
        Editable text = getText();
        if (text == null) {
            return "";
        }
        return StringUtil.trimSequence(text);
    }

    @Override // android.widget.TextView, android.view.View
    protected void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (getLayout() != null && !TextUtils.isEmpty(this.hint)) {
            if (!TextUtils.isEmpty(this.subHint)) {
                setHintWithChecks(new SpannableStringBuilder().append(ellipsizeToWidth(this.hint)).append((CharSequence) "\n").append(ellipsizeToWidth(this.subHint)));
            } else {
                setHintWithChecks(ellipsizeToWidth(this.hint));
            }
            super.onMeasure(i, i2);
        }
    }

    @Override // android.widget.TextView
    protected void onSelectionChanged(int i, int i2) {
        super.onSelectionChanged(i, i2);
        if (getText() != null) {
            if (!changeSelectionForPartialMentions(getText(), i, i2)) {
                if (i == i2) {
                    doAfterCursorChange(getText());
                } else {
                    updateQuery(null);
                }
            } else {
                return;
            }
        }
        CursorPositionChangedListener cursorPositionChangedListener = this.cursorPositionChangedListener;
        if (cursorPositionChangedListener != null) {
            cursorPositionChangedListener.onCursorPositionChanged(i, i2);
        }
    }

    @Override // android.widget.TextView, android.view.View
    protected void onDraw(Canvas canvas) {
        if (!(getText() == null || getLayout() == null)) {
            int save = canvas.save();
            int height = (((getLayout().getHeight() - getBottom()) - getTop()) - getCompoundPaddingBottom()) - getCompoundPaddingTop();
            canvas.clipRect(((float) (getCompoundPaddingLeft() + getScrollX())) - 10.0f, getScrollY() == 0 ? 0.0f : (float) (getExtendedPaddingTop() + getScrollY()), ((float) (((getRight() - getLeft()) - getCompoundPaddingRight()) + getScrollX())) + 10.0f, (float) (((getBottom() - getTop()) + getScrollY()) - (getScrollY() == height ? 0 : getExtendedPaddingBottom())));
            canvas.translate((float) getTotalPaddingLeft(), (float) getTotalPaddingTop());
            try {
                this.mentionRendererDelegate.draw(canvas, getText(), getLayout());
            } finally {
                canvas.restoreToCount(save);
            }
        }
        super.onDraw(canvas);
    }

    private CharSequence ellipsizeToWidth(CharSequence charSequence) {
        return TextUtils.ellipsize(charSequence, getPaint(), (float) ((getWidth() - getPaddingLeft()) - getPaddingRight()), TextUtils.TruncateAt.END);
    }

    public void setHint(String str, CharSequence charSequence) {
        this.hint = str;
        if (charSequence != null) {
            SpannableString spannableString = new SpannableString(charSequence);
            this.subHint = spannableString;
            spannableString.setSpan(new RelativeSizeSpan(0.5f), 0, charSequence.length(), 18);
        } else {
            this.subHint = null;
        }
        if (this.subHint != null) {
            setHintWithChecks(new SpannableStringBuilder().append(ellipsizeToWidth(this.hint)).append((CharSequence) "\n").append(ellipsizeToWidth(this.subHint)));
        } else {
            setHintWithChecks(ellipsizeToWidth(this.hint));
        }
        setHintWithChecks(str);
    }

    public void appendInvite(String str) {
        if (getText() != null) {
            if (!TextUtils.isEmpty(getText()) && !getText().toString().equals(" ")) {
                append(" ");
            }
            append(str);
            setSelection(getText().length());
        }
    }

    public void setCursorPositionChangedListener(CursorPositionChangedListener cursorPositionChangedListener) {
        this.cursorPositionChangedListener = cursorPositionChangedListener;
    }

    public void setMentionQueryChangedListener(MentionQueryChangedListener mentionQueryChangedListener) {
        this.mentionQueryChangedListener = mentionQueryChangedListener;
    }

    public void setMentionValidator(MentionValidatorWatcher.MentionValidator mentionValidator) {
        this.mentionValidatorWatcher.setMentionValidator(mentionValidator);
    }

    private boolean isLandscape() {
        return getResources().getConfiguration().orientation == 2;
    }

    public void setMessageSendType(MessageSendType messageSendType) {
        boolean isPreferSystemEmoji = SignalStore.settings().isPreferSystemEmoji();
        int imeOptions = (getImeOptions() & -256) | 4;
        int inputType = getInputType();
        String str = null;
        if (isLandscape()) {
            setImeActionLabel(getContext().getString(messageSendType.getComposeHintRes()), 4);
        } else {
            setImeActionLabel(null, 0);
        }
        if (isPreferSystemEmoji) {
            inputType = (inputType & -4081) | 64;
        }
        setImeOptions(imeOptions);
        String string = getContext().getString(messageSendType.getComposeHintRes());
        if (messageSendType.getSimName() != null) {
            str = getContext().getString(R.string.conversation_activity__from_sim_name, messageSendType.getSimName());
        }
        setHint(string, str);
        setInputType(inputType);
    }

    @Override // androidx.appcompat.widget.AppCompatEditText, android.widget.TextView, android.view.View
    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        if (SignalStore.settings().isEnterKeySends()) {
            editorInfo.imeOptions &= -1073741825;
        }
        if (Build.VERSION.SDK_INT < 21) {
            return onCreateInputConnection;
        }
        if (this.mediaListener == null) {
            return onCreateInputConnection;
        }
        if (onCreateInputConnection == null) {
            return null;
        }
        EditorInfoCompat.setContentMimeTypes(editorInfo, new String[]{MediaUtil.IMAGE_JPEG, MediaUtil.IMAGE_PNG, MediaUtil.IMAGE_GIF});
        return InputConnectionCompat.createWrapper(onCreateInputConnection, editorInfo, new CommitContentListener(this.mediaListener));
    }

    public void setMediaListener(InputPanel.MediaListener mediaListener) {
        this.mediaListener = mediaListener;
    }

    public boolean hasMentions() {
        Editable text = getText();
        if (text != null) {
            return !MentionAnnotation.getMentionAnnotations(text).isEmpty();
        }
        return false;
    }

    public List<Mention> getMentions() {
        return MentionAnnotation.getMentionsFromAnnotations(getText());
    }

    private void initialize() {
        if (TextSecurePreferences.isIncognitoKeyboardEnabled(getContext())) {
            setImeOptions(getImeOptions() | 16777216);
        }
        this.mentionRendererDelegate = new MentionRendererDelegate(getContext(), ContextCompat.getColor(getContext(), R.color.conversation_mention_background_color));
        addTextChangedListener(new MentionDeleter());
        MentionValidatorWatcher mentionValidatorWatcher = new MentionValidatorWatcher();
        this.mentionValidatorWatcher = mentionValidatorWatcher;
        addTextChangedListener(mentionValidatorWatcher);
    }

    private void setHintWithChecks(CharSequence charSequence) {
        if (getLayout() != null && !Objects.equals(getHint(), charSequence)) {
            setHint(charSequence);
        }
    }

    private boolean changeSelectionForPartialMentions(Spanned spanned, int i, int i2) {
        Annotation[] annotationArr = (Annotation[]) spanned.getSpans(0, spanned.length(), Annotation.class);
        for (Annotation annotation : annotationArr) {
            if (MentionAnnotation.isMentionAnnotation(annotation)) {
                int spanStart = spanned.getSpanStart(annotation);
                int spanEnd = spanned.getSpanEnd(annotation);
                boolean z = i > spanStart && i < spanEnd;
                boolean z2 = i2 > spanStart && i2 < spanEnd;
                if (z || z2) {
                    if (i == i2) {
                        setSelection(spanEnd, spanEnd);
                    } else {
                        if (z) {
                            i = spanStart;
                        }
                        if (z2) {
                            i2 = spanEnd;
                        }
                        setSelection(i, i2);
                    }
                    return true;
                }
            }
        }
        return false;
    }

    private void doAfterCursorChange(Editable editable) {
        if (enoughToFilter(editable)) {
            performFiltering(editable);
        } else {
            updateQuery(null);
        }
    }

    private void performFiltering(Editable editable) {
        int selectionEnd = getSelectionEnd();
        updateQuery(editable.subSequence(findQueryStart(editable, selectionEnd), selectionEnd).toString());
    }

    private void updateQuery(String str) {
        MentionQueryChangedListener mentionQueryChangedListener = this.mentionQueryChangedListener;
        if (mentionQueryChangedListener != null) {
            mentionQueryChangedListener.onQueryChanged(str);
        }
    }

    private boolean enoughToFilter(Editable editable) {
        int selectionEnd = getSelectionEnd();
        if (selectionEnd >= 0 && findQueryStart(editable, selectionEnd) != -1) {
            return true;
        }
        return false;
    }

    public void replaceTextWithMention(String str, RecipientId recipientId) {
        Editable text = getText();
        if (text != null) {
            clearComposingText();
            int selectionEnd = getSelectionEnd();
            text.replace(findQueryStart(text, selectionEnd) - 1, selectionEnd, createReplacementToken(str, recipientId));
        }
    }

    private CharSequence createReplacementToken(CharSequence charSequence, RecipientId recipientId) {
        SpannableStringBuilder append = new SpannableStringBuilder().append(MentionUtil.MENTION_STARTER);
        if (charSequence instanceof Spanned) {
            SpannableString spannableString = new SpannableString(((Object) charSequence) + " ");
            TextUtils.copySpansFrom((Spanned) charSequence, 0, charSequence.length(), Object.class, spannableString, 0);
            append.append((CharSequence) spannableString);
        } else {
            append.append(charSequence).append((CharSequence) " ");
        }
        append.setSpan(MentionAnnotation.mentionAnnotationForRecipientId(recipientId), 0, append.length() - 1, 33);
        return append;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0021, code lost:
        if (r5.charAt(r6) != '@') goto L_0x0026;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0025, code lost:
        return r6 + 1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private int findQueryStart(java.lang.CharSequence r5, int r6) {
        /*
            r4 = this;
            r0 = -1
            if (r6 != 0) goto L_0x0004
            return r0
        L_0x0004:
            int r6 = r6 + -1
        L_0x0006:
            r1 = 64
            if (r6 < 0) goto L_0x001b
            char r2 = r5.charAt(r6)
            if (r2 == r1) goto L_0x001b
            char r2 = r5.charAt(r6)
            r3 = 32
            if (r2 == r3) goto L_0x001b
            int r6 = r6 + -1
            goto L_0x0006
        L_0x001b:
            if (r6 < 0) goto L_0x0026
            char r5 = r5.charAt(r6)
            if (r5 != r1) goto L_0x0026
            int r6 = r6 + 1
            return r6
        L_0x0026:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.ComposeText.findQueryStart(java.lang.CharSequence, int):int");
    }

    /* loaded from: classes4.dex */
    private static class CommitContentListener implements InputConnectionCompat.OnCommitContentListener {
        private static final String TAG = Log.tag(CommitContentListener.class);
        private final InputPanel.MediaListener mediaListener;

        private CommitContentListener(InputPanel.MediaListener mediaListener) {
            this.mediaListener = mediaListener;
        }

        @Override // androidx.core.view.inputmethod.InputConnectionCompat.OnCommitContentListener
        public boolean onCommitContent(InputContentInfoCompat inputContentInfoCompat, int i, Bundle bundle) {
            if (Build.VERSION.SDK_INT >= 25 && (i & 1) != 0) {
                try {
                    inputContentInfoCompat.requestPermission();
                } catch (Exception e) {
                    Log.w(TAG, e);
                    return false;
                }
            }
            if (inputContentInfoCompat.getDescription().getMimeTypeCount() <= 0) {
                return false;
            }
            this.mediaListener.onMediaSelected(inputContentInfoCompat.getContentUri(), inputContentInfoCompat.getDescription().getMimeType(0));
            return true;
        }
    }
}
