package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: SoundsAndNotificationsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SoundsAndNotificationsSettingsViewModel$channelConsistencyCheck$2 extends Lambda implements Function0<Unit> {
    final /* synthetic */ SoundsAndNotificationsSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SoundsAndNotificationsSettingsViewModel$channelConsistencyCheck$2(SoundsAndNotificationsSettingsViewModel soundsAndNotificationsSettingsViewModel) {
        super(0);
        this.this$0 = soundsAndNotificationsSettingsViewModel;
    }

    /* renamed from: invoke$lambda-0 */
    public static final SoundsAndNotificationsSettingsState m1229invoke$lambda0(SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(soundsAndNotificationsSettingsState, "s");
        return SoundsAndNotificationsSettingsState.copy$default(soundsAndNotificationsSettingsState, null, 0, null, false, false, true, 31, null);
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsViewModel$channelConsistencyCheck$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SoundsAndNotificationsSettingsViewModel$channelConsistencyCheck$2.m1229invoke$lambda0((SoundsAndNotificationsSettingsState) obj);
            }
        });
    }
}
