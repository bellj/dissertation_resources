package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.livedata.LiveDataExtensionsKt;
import org.thoughtcrime.securesms.util.livedata.ProcessState;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: ExpireTimerSettingsFragment.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00152\u00020\u0001:\u0001\u0015B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0002J\u0010\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\rH\u0016J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0002J\u001a\u0010\u0012\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "save", "Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel;", "adjustListPaddingForSaveButton", "", "view", "Landroid/view/View;", "bindAdapter", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsState;", "onViewCreated", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    public static final String FOR_RESULT_VALUE;
    private CircularProgressMaterialButton save;
    private ExpireTimerSettingsViewModel viewModel;

    public ExpireTimerSettingsFragment() {
        super(R.string.PrivacySettingsFragment__disappearing_messages, 0, R.layout.expire_timer_settings_fragment, null, 10, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.timer_select_fragment_save);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.timer_select_fragment_save)");
        CircularProgressMaterialButton circularProgressMaterialButton = (CircularProgressMaterialButton) findViewById;
        this.save = circularProgressMaterialButton;
        if (circularProgressMaterialButton == null) {
            Intrinsics.throwUninitializedPropertyAccessException("save");
            circularProgressMaterialButton = null;
        }
        circularProgressMaterialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ExpireTimerSettingsFragment.m861$r8$lambda$VGekOBY6Ol3dUtthHctunDjJGc(ExpireTimerSettingsFragment.this, view2);
            }
        });
        adjustListPaddingForSaveButton(view);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m864onViewCreated$lambda0(ExpireTimerSettingsFragment expireTimerSettingsFragment, View view) {
        Intrinsics.checkNotNullParameter(expireTimerSettingsFragment, "this$0");
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel = expireTimerSettingsFragment.viewModel;
        if (expireTimerSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            expireTimerSettingsViewModel = null;
        }
        expireTimerSettingsViewModel.save();
    }

    private final void adjustListPaddingForSaveButton(View view) {
        View findViewById = view.findViewById(R.id.recycler);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.recycler)");
        RecyclerView recyclerView = (RecyclerView) findViewById;
        recyclerView.setPadding(recyclerView.getPaddingLeft(), recyclerView.getPaddingTop(), recyclerView.getPaddingRight(), ViewUtil.dpToPx(80));
        recyclerView.setClipToPadding(false);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ViewModelStoreOwner viewModelStoreOwner = NavHostFragment.findNavController(this).getViewModelStoreOwner(R.id.app_settings_expire_timer);
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ViewModel viewModel = new ViewModelProvider(viewModelStoreOwner, new ExpireTimerSettingsViewModel.Factory(requireContext, ExpireTimerSettingsFragmentKt.toConfig(getArguments()))).get(ExpireTimerSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "provider.get(ExpireTimer…ngsViewModel::class.java)");
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel = (ExpireTimerSettingsViewModel) viewModel;
        this.viewModel = expireTimerSettingsViewModel;
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel2 = null;
        if (expireTimerSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            expireTimerSettingsViewModel = null;
        }
        expireTimerSettingsViewModel.getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ ExpireTimerSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ExpireTimerSettingsFragment.$r8$lambda$6T1u8zTU5rdvGl_F3NPZwVUNrrQ(DSLSettingsAdapter.this, this.f$1, (ExpireTimerSettingsState) obj);
            }
        });
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel3 = this.viewModel;
        if (expireTimerSettingsViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            expireTimerSettingsViewModel2 = expireTimerSettingsViewModel3;
        }
        LiveDataExtensionsKt.distinctUntilChanged(expireTimerSettingsViewModel2.getState(), ExpireTimerSettingsFragment$bindAdapter$2.INSTANCE).observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$$ExternalSyntheticLambda2
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ExpireTimerSettingsFragment.m860$r8$lambda$FBeDafeA4zJKlW9aBiTtqu1Bg(ExpireTimerSettingsFragment.this, (ExpireTimerSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m862bindAdapter$lambda1(DSLSettingsAdapter dSLSettingsAdapter, ExpireTimerSettingsFragment expireTimerSettingsFragment, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(expireTimerSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "state");
        dSLSettingsAdapter.submitList(expireTimerSettingsFragment.getConfiguration(expireTimerSettingsState).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m863bindAdapter$lambda2(ExpireTimerSettingsFragment expireTimerSettingsFragment, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullParameter(expireTimerSettingsFragment, "this$0");
        ProcessState<Integer> saveState = expireTimerSettingsState.getSaveState();
        CircularProgressMaterialButton circularProgressMaterialButton = null;
        CircularProgressMaterialButton circularProgressMaterialButton2 = null;
        CircularProgressMaterialButton circularProgressMaterialButton3 = null;
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel = null;
        if (saveState instanceof ProcessState.Working) {
            CircularProgressMaterialButton circularProgressMaterialButton4 = expireTimerSettingsFragment.save;
            if (circularProgressMaterialButton4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("save");
            } else {
                circularProgressMaterialButton2 = circularProgressMaterialButton4;
            }
            circularProgressMaterialButton2.setSpinning();
        } else if (saveState instanceof ProcessState.Success) {
            if (expireTimerSettingsState.isGroupCreate()) {
                expireTimerSettingsFragment.requireActivity().setResult(-1, new Intent().putExtra(FOR_RESULT_VALUE, ((Number) ((ProcessState.Success) saveState).getResult()).intValue()));
            }
            CircularProgressMaterialButton circularProgressMaterialButton5 = expireTimerSettingsFragment.save;
            if (circularProgressMaterialButton5 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("save");
            } else {
                circularProgressMaterialButton3 = circularProgressMaterialButton5;
            }
            circularProgressMaterialButton3.setClickable(false);
            expireTimerSettingsFragment.requireActivity().onNavigateUp();
        } else if (saveState instanceof ProcessState.Failure) {
            Throwable throwable = ((ProcessState.Failure) saveState).getThrowable();
            GroupChangeFailureReason fromException = throwable != null ? GroupChangeFailureReason.fromException(throwable) : null;
            if (fromException == null) {
                fromException = GroupChangeFailureReason.OTHER;
            }
            Toast.makeText(expireTimerSettingsFragment.getContext(), GroupErrors.getUserDisplayMessage(fromException), 1).show();
            ExpireTimerSettingsViewModel expireTimerSettingsViewModel2 = expireTimerSettingsFragment.viewModel;
            if (expireTimerSettingsViewModel2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            } else {
                expireTimerSettingsViewModel = expireTimerSettingsViewModel2;
            }
            expireTimerSettingsViewModel.resetError();
        } else {
            CircularProgressMaterialButton circularProgressMaterialButton6 = expireTimerSettingsFragment.save;
            if (circularProgressMaterialButton6 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("save");
            } else {
                circularProgressMaterialButton = circularProgressMaterialButton6;
            }
            circularProgressMaterialButton.cancelSpinning();
        }
    }

    private final DSLConfiguration getConfiguration(ExpireTimerSettingsState expireTimerSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(expireTimerSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1
            final /* synthetic */ ExpireTimerSettingsState $state;
            final /* synthetic */ ExpireTimerSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0098: INVOKE  
                  (r19v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText : 0x007c: INVOKE  (r2v13 org.thoughtcrime.securesms.components.settings.DSLSettingsText A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion : 0x0078: SGET  (r1v16 org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion A[REMOVE]) =  org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion)
                  (wrap: java.lang.String : 0x006c: CHECK_CAST (r2v12 java.lang.String A[REMOVE]) = (java.lang.String) (wrap: java.lang.Object : 0x0068: INVOKE  (r2v11 java.lang.Object A[REMOVE]) = (r1v13 'pair' kotlin.Pair) type: VIRTUAL call: kotlin.Pair.component1():java.lang.Object))
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] : 0x007a: NEW_ARRAY  (r3v10 org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] A[REMOVE]) = (0 int) type: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[])
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion.from(java.lang.CharSequence, org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[]):org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (null org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  false
                  (wrap: boolean : ?: TERNARY(r5v1 boolean A[REMOVE]) = ((wrap: int : 0x0082: INVOKE  (r1v17 int A[REMOVE]) = 
                  (r13v0 'expireTimerSettingsState2' org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState.getCurrentTimer():int) == (r8v1 'intValue' int)) ? true : false)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1 : 0x008d: CONSTRUCTOR  (r6v1 org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r14v0 'expireTimerSettingsFragment' org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment)
                  (r8v1 'intValue' int)
                 call: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment, int):void type: CONSTRUCTOR)
                  (6 int)
                  (null java.lang.Object)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.radioPref$default(org.thoughtcrime.securesms.components.settings.DSLConfiguration, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsText, boolean, boolean, kotlin.jvm.functions.Function0, int, java.lang.Object):void in method: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x008d: CONSTRUCTOR  (r6v1 org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r14v0 'expireTimerSettingsFragment' org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment)
                  (r8v1 'intValue' int)
                 call: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment, int):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 16 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 22 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r19) {
                /*
                    r18 = this;
                    r0 = r18
                    r9 = r19
                    java.lang.String r1 = "$this$configure"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r9, r1)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r1 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState r2 = r0.$state
                    boolean r2 = r2.isForRecipient()
                    if (r2 == 0) goto L_0x0017
                    r2 = 2131952600(0x7f1303d8, float:1.9541647E38)
                    goto L_0x001a
                L_0x0017:
                    r2 = 2131952599(0x7f1303d7, float:1.9541645E38)
                L_0x001a:
                    r10 = 0
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r3 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r10]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r1 = r1.from(r2, r3)
                    r11 = 0
                    r12 = 1
                    org.thoughtcrime.securesms.components.settings.DSLConfiguration.textPref$default(r9, r11, r1, r12, r11)
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment r1 = r0.this$0
                    android.content.res.Resources r1 = r1.getResources()
                    r2 = 2130903041(0x7f030001, float:1.7412889E38)
                    java.lang.String[] r1 = r1.getStringArray(r2)
                    java.lang.String r2 = "resources.getStringArray…SettingsFragment__labels)"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment r2 = r0.this$0
                    android.content.res.Resources r2 = r2.getResources()
                    r3 = 2130903042(0x7f030002, float:1.741289E38)
                    int[] r2 = r2.getIntArray(r3)
                    java.lang.String r3 = "resources.getIntArray(R.…SettingsFragment__values)"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
                    java.lang.Integer[] r2 = kotlin.collections.ArraysKt.toTypedArray(r2)
                    java.util.List r1 = kotlin.collections.ArraysKt.zip(r1, r2)
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState r13 = r0.$state
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment r14 = r0.this$0
                    java.util.Iterator r15 = r1.iterator()
                    r16 = 1
                L_0x005c:
                    boolean r1 = r15.hasNext()
                    if (r1 == 0) goto L_0x00aa
                    java.lang.Object r1 = r15.next()
                    kotlin.Pair r1 = (kotlin.Pair) r1
                    java.lang.Object r2 = r1.component1()
                    java.lang.String r2 = (java.lang.String) r2
                    java.lang.Object r1 = r1.component2()
                    java.lang.Number r1 = (java.lang.Number) r1
                    int r8 = r1.intValue()
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r1 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r3 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r10]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r2 = r1.from(r2, r3)
                    r3 = 0
                    r4 = 0
                    int r1 = r13.getCurrentTimer()
                    if (r1 != r8) goto L_0x008a
                    r5 = 1
                    goto L_0x008b
                L_0x008a:
                    r5 = 0
                L_0x008b:
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1 r6 = new org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$1$1
                    r6.<init>(r14, r8)
                    r7 = 6
                    r17 = 0
                    r1 = r19
                    r11 = r8
                    r8 = r17
                    org.thoughtcrime.securesms.components.settings.DSLConfiguration.radioPref$default(r1, r2, r3, r4, r5, r6, r7, r8)
                    if (r16 == 0) goto L_0x00a6
                    int r1 = r13.getCurrentTimer()
                    if (r1 == r11) goto L_0x00a6
                    r16 = 1
                    goto L_0x00a8
                L_0x00a6:
                    r16 = 0
                L_0x00a8:
                    r11 = 0
                    goto L_0x005c
                L_0x00aa:
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r1 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    r2 = 2131952595(0x7f1303d3, float:1.9541637E38)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r3 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r10]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r2 = r1.from(r2, r3)
                    if (r16 == 0) goto L_0x00d4
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment r3 = r0.this$0
                    android.content.Context r3 = r3.requireContext()
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState r4 = r0.$state
                    int r4 = r4.getCurrentTimer()
                    java.lang.String r3 = org.thoughtcrime.securesms.util.ExpirationUtil.getExpirationDisplayValue(r3, r4)
                    java.lang.String r4 = "getExpirationDisplayValu…xt(), state.currentTimer)"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r3, r4)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r4 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r10]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r1 = r1.from(r3, r4)
                    r3 = r1
                    goto L_0x00d5
                L_0x00d4:
                    r3 = 0
                L_0x00d5:
                    r4 = 0
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$2 r6 = new org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1$2
                    org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment r1 = r0.this$0
                    r6.<init>(r1)
                    r7 = 4
                    r8 = 0
                    r1 = r19
                    r5 = r16
                    org.thoughtcrime.securesms.components.settings.DSLConfiguration.radioPref$default(r1, r2, r3, r4, r5, r6, r7, r8)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    /* compiled from: ExpireTimerSettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsFragment$Companion;", "", "()V", "FOR_RESULT_VALUE", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
