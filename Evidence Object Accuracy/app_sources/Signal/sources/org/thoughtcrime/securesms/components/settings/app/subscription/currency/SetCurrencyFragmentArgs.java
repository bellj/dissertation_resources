package org.thoughtcrime.securesms.components.settings.app.subscription.currency;

import android.os.Bundle;
import java.util.Arrays;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class SetCurrencyFragmentArgs {
    private final HashMap arguments;

    private SetCurrencyFragmentArgs() {
        this.arguments = new HashMap();
    }

    private SetCurrencyFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static SetCurrencyFragmentArgs fromBundle(Bundle bundle) {
        SetCurrencyFragmentArgs setCurrencyFragmentArgs = new SetCurrencyFragmentArgs();
        bundle.setClassLoader(SetCurrencyFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("isBoost")) {
            setCurrencyFragmentArgs.arguments.put("isBoost", Boolean.valueOf(bundle.getBoolean("isBoost")));
            if (bundle.containsKey("supportedCurrencyCodes")) {
                String[] stringArray = bundle.getStringArray("supportedCurrencyCodes");
                if (stringArray != null) {
                    setCurrencyFragmentArgs.arguments.put("supportedCurrencyCodes", stringArray);
                    return setCurrencyFragmentArgs;
                }
                throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
            }
            throw new IllegalArgumentException("Required argument \"supportedCurrencyCodes\" is missing and does not have an android:defaultValue");
        }
        throw new IllegalArgumentException("Required argument \"isBoost\" is missing and does not have an android:defaultValue");
    }

    public boolean getIsBoost() {
        return ((Boolean) this.arguments.get("isBoost")).booleanValue();
    }

    public String[] getSupportedCurrencyCodes() {
        return (String[]) this.arguments.get("supportedCurrencyCodes");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("isBoost")) {
            bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
        }
        if (this.arguments.containsKey("supportedCurrencyCodes")) {
            bundle.putStringArray("supportedCurrencyCodes", (String[]) this.arguments.get("supportedCurrencyCodes"));
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SetCurrencyFragmentArgs setCurrencyFragmentArgs = (SetCurrencyFragmentArgs) obj;
        if (this.arguments.containsKey("isBoost") == setCurrencyFragmentArgs.arguments.containsKey("isBoost") && getIsBoost() == setCurrencyFragmentArgs.getIsBoost() && this.arguments.containsKey("supportedCurrencyCodes") == setCurrencyFragmentArgs.arguments.containsKey("supportedCurrencyCodes")) {
            return getSupportedCurrencyCodes() == null ? setCurrencyFragmentArgs.getSupportedCurrencyCodes() == null : getSupportedCurrencyCodes().equals(setCurrencyFragmentArgs.getSupportedCurrencyCodes());
        }
        return false;
    }

    public int hashCode() {
        return (((getIsBoost() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getSupportedCurrencyCodes());
    }

    public String toString() {
        return "SetCurrencyFragmentArgs{isBoost=" + getIsBoost() + ", supportedCurrencyCodes=" + getSupportedCurrencyCodes() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(SetCurrencyFragmentArgs setCurrencyFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(setCurrencyFragmentArgs.arguments);
        }

        public Builder(boolean z, String[] strArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("isBoost", Boolean.valueOf(z));
            if (strArr != null) {
                hashMap.put("supportedCurrencyCodes", strArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        public SetCurrencyFragmentArgs build() {
            return new SetCurrencyFragmentArgs(this.arguments);
        }

        public Builder setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        public Builder setSupportedCurrencyCodes(String[] strArr) {
            if (strArr != null) {
                this.arguments.put("supportedCurrencyCodes", strArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }

        public String[] getSupportedCurrencyCodes() {
            return (String[]) this.arguments.get("supportedCurrencyCodes");
        }
    }
}
