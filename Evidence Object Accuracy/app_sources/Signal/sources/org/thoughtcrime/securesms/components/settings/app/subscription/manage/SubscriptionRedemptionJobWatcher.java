package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.functions.Function;
import j$.util.Optional;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.DonationReceiptRedemptionJob;
import org.thoughtcrime.securesms.jobs.SubscriptionReceiptRequestResponseJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* compiled from: SubscriptionRedemptionJobWatcher.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0006\u0010\u0007J\u0012\u0010\u0005\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00030\u0002¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/SubscriptionRedemptionJobWatcher;", "", "Lio/reactivex/rxjava3/core/Observable;", "j$/util/Optional", "Lorg/thoughtcrime/securesms/jobmanager/JobTracker$JobState;", "watch", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class SubscriptionRedemptionJobWatcher {
    public static final SubscriptionRedemptionJobWatcher INSTANCE = new SubscriptionRedemptionJobWatcher();

    private SubscriptionRedemptionJobWatcher() {
    }

    public final Observable<Optional<JobTracker.JobState>> watch() {
        Observable<Optional<JobTracker.JobState>> distinctUntilChanged = Observable.interval(0, 5, TimeUnit.SECONDS).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.SubscriptionRedemptionJobWatcher$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SubscriptionRedemptionJobWatcher.m983watch$lambda2((Long) obj);
            }
        }).distinctUntilChanged();
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "interval(0, 5, TimeUnit.… }.distinctUntilChanged()");
        return distinctUntilChanged;
    }

    /* renamed from: watch$lambda-2 */
    public static final Optional m983watch$lambda2(Long l) {
        JobTracker.JobState firstMatchingJobState = ApplicationDependencies.getJobManager().getFirstMatchingJobState(new JobTracker.JobFilter() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.SubscriptionRedemptionJobWatcher$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobFilter
            public final boolean matches(Job job) {
                return SubscriptionRedemptionJobWatcher.m984watch$lambda2$lambda0(job);
            }
        });
        firstMatchingJobState = ApplicationDependencies.getJobManager().getFirstMatchingJobState(new JobTracker.JobFilter() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.SubscriptionRedemptionJobWatcher$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobFilter
            public final boolean matches(Job job) {
                return SubscriptionRedemptionJobWatcher.m985watch$lambda2$lambda1(job);
            }
        });
        if (firstMatchingJobState == null) {
        }
        if (firstMatchingJobState != null || !SignalStore.donationsValues().getSubscriptionRedemptionFailed()) {
            return Optional.ofNullable(firstMatchingJobState);
        }
        return Optional.of(JobTracker.JobState.FAILURE);
    }

    /* renamed from: watch$lambda-2$lambda-0 */
    public static final boolean m984watch$lambda2$lambda0(Job job) {
        Intrinsics.checkNotNullParameter(job, "it");
        return Intrinsics.areEqual(job.getFactoryKey(), DonationReceiptRedemptionJob.KEY) && Intrinsics.areEqual(job.getParameters().getQueue(), DonationReceiptRedemptionJob.SUBSCRIPTION_QUEUE);
    }

    /* renamed from: watch$lambda-2$lambda-1 */
    public static final boolean m985watch$lambda2$lambda1(Job job) {
        Intrinsics.checkNotNullParameter(job, "it");
        return Intrinsics.areEqual(job.getFactoryKey(), SubscriptionReceiptRequestResponseJob.KEY) && Intrinsics.areEqual(job.getParameters().getQueue(), DonationReceiptRedemptionJob.SUBSCRIPTION_QUEUE);
    }
}
