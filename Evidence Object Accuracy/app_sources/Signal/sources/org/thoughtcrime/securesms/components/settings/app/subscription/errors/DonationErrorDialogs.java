package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams;

/* compiled from: DonationErrorDialogs.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001:\u0001\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorDialogs;", "", "()V", "show", "Landroid/content/DialogInterface;", "context", "Landroid/content/Context;", "throwable", "", "callback", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorDialogs$DialogCallback;", "DialogCallback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationErrorDialogs {
    public static final DonationErrorDialogs INSTANCE = new DonationErrorDialogs();

    private DonationErrorDialogs() {
    }

    public final DialogInterface show(Context context, Throwable th, DialogCallback dialogCallback) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(dialogCallback, "callback");
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(context);
        materialAlertDialogBuilder.setOnDismissListener((DialogInterface.OnDismissListener) new DialogInterface.OnDismissListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                DonationErrorDialogs.m959show$lambda0(DonationErrorDialogs.DialogCallback.this, dialogInterface);
            }
        });
        DonationErrorParams create = DonationErrorParams.Companion.create(context, th, dialogCallback);
        materialAlertDialogBuilder.setTitle(create.getTitle()).setMessage(create.getMessage());
        if (create.getPositiveAction() != null) {
            materialAlertDialogBuilder.setPositiveButton(create.getPositiveAction().getLabel(), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs$$ExternalSyntheticLambda1
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DonationErrorDialogs.m960show$lambda1(DonationErrorParams.this, dialogInterface, i);
                }
            });
        }
        if (create.getNegativeAction() != null) {
            materialAlertDialogBuilder.setNegativeButton(create.getNegativeAction().getLabel(), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs$$ExternalSyntheticLambda2
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    DonationErrorDialogs.m961show$lambda2(DonationErrorParams.this, dialogInterface, i);
                }
            });
        }
        AlertDialog show = materialAlertDialogBuilder.show();
        Intrinsics.checkNotNullExpressionValue(show, "builder.show()");
        return show;
    }

    /* renamed from: show$lambda-0 */
    public static final void m959show$lambda0(DialogCallback dialogCallback, DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogCallback, "$callback");
        dialogCallback.onDialogDismissed();
    }

    /* renamed from: show$lambda-1 */
    public static final void m960show$lambda1(DonationErrorParams donationErrorParams, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(donationErrorParams, "$params");
        donationErrorParams.getPositiveAction().getAction().invoke();
    }

    /* renamed from: show$lambda-2 */
    public static final void m961show$lambda2(DonationErrorParams donationErrorParams, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(donationErrorParams, "$params");
        donationErrorParams.getNegativeAction().getAction().invoke();
    }

    /* compiled from: DonationErrorDialogs.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u0016\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\u0018\u0010\u0004\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0016\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\t\u001a\u00020\u0002H\u0016J\u0018\u0010\n\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\u000b\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016J\u0018\u0010\f\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u00052\u0006\u0010\u0006\u001a\u00020\u0007H\u0016¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorDialogs$DialogCallback;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$Callback;", "", "()V", "onCancel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorParams$ErrorAction;", "context", "Landroid/content/Context;", "onContactSupport", "onDialogDismissed", "onGoToGooglePay", "onLearnMore", "onOk", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static class DialogCallback implements DonationErrorParams.Callback<Unit> {
        public void onDialogDismissed() {
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<Unit> onCancel(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return new DonationErrorParams.ErrorAction<>(17039360, DonationErrorDialogs$DialogCallback$onCancel$1.INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<Unit> onOk(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return new DonationErrorParams.ErrorAction<>(17039370, DonationErrorDialogs$DialogCallback$onOk$1.INSTANCE);
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<Unit> onGoToGooglePay(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return new DonationErrorParams.ErrorAction<>(R.string.DeclineCode__go_to_google_pay, new DonationErrorDialogs$DialogCallback$onGoToGooglePay$1(context));
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<Unit> onLearnMore(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return new DonationErrorParams.ErrorAction<>(R.string.DeclineCode__learn_more, new DonationErrorDialogs$DialogCallback$onLearnMore$1(context));
        }

        @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorParams.Callback
        public DonationErrorParams.ErrorAction<Unit> onContactSupport(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return new DonationErrorParams.ErrorAction<>(R.string.Subscription__contact_support, new DonationErrorDialogs$DialogCallback$onContactSupport$1(context));
        }
    }
}
