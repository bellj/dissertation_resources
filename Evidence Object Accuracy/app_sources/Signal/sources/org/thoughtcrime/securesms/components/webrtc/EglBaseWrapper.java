package org.thoughtcrime.securesms.components.webrtc;

import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.webrtc.EglBase;

/* compiled from: EglBaseWrapper.kt */
@Metadata(bv = {}, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0013\b\u0002\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0004¢\u0006\u0004\b\u0013\u0010\u0014B\t\b\u0016¢\u0006\u0004\b\u0013\u0010\u0015J\b\u0010\u0003\u001a\u00020\u0002H\u0002J\u0006\u0010\u0005\u001a\u00020\u0004J\u0014\u0010\b\u001a\u00020\u00022\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00040\u0006R\u0019\u0010\t\u001a\u0004\u0018\u00010\u00048\u0006¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u000e\u001a\u00020\r8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u000e\u0010\u000fR\u0016\u0010\u0011\u001a\u00020\u00108\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/EglBaseWrapper;", "", "", "releaseEglBase", "Lorg/webrtc/EglBase;", "require", "j$/util/function/Consumer", "consumer", "performWithValidEglBase", "eglBase", "Lorg/webrtc/EglBase;", "getEglBase", "()Lorg/webrtc/EglBase;", "Ljava/util/concurrent/locks/Lock;", "lock", "Ljava/util/concurrent/locks/Lock;", "", "isReleased", "Z", "<init>", "(Lorg/webrtc/EglBase;)V", "()V", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EglBaseWrapper {
    public static final Companion Companion = new Companion(null);
    public static final String OUTGOING_PLACEHOLDER;
    private static EglBaseWrapper eglBaseWrapper;
    private static final Set<Object> holders = new LinkedHashSet();
    private final EglBase eglBase;
    private volatile boolean isReleased;
    private final Lock lock;

    public /* synthetic */ EglBaseWrapper(EglBase eglBase, DefaultConstructorMarker defaultConstructorMarker) {
        this(eglBase);
    }

    @JvmStatic
    public static final EglBaseWrapper acquireEglBase(Object obj) {
        return Companion.acquireEglBase(obj);
    }

    @JvmStatic
    public static final void forceRelease() {
        Companion.forceRelease();
    }

    @JvmStatic
    public static final void releaseEglBase(Object obj) {
        Companion.releaseEglBase(obj);
    }

    @JvmStatic
    public static final void replaceHolder(Object obj, Object obj2) {
        Companion.replaceHolder(obj, obj2);
    }

    private EglBaseWrapper(EglBase eglBase) {
        this.eglBase = eglBase;
        this.lock = new ReentrantLock();
    }

    public final EglBase getEglBase() {
        return this.eglBase;
    }

    public EglBaseWrapper() {
        this(null);
    }

    public final EglBase require() {
        EglBase eglBase = this.eglBase;
        if (eglBase != null) {
            return eglBase;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0068, code lost:
        if (kotlin.jvm.internal.Intrinsics.areEqual(((org.webrtc.EglBase10.Context) r1).getRawContext(), javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT) == false) goto L_0x006a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006c A[Catch: all -> 0x007f, TryCatch #0 {all -> 0x007f, blocks: (B:10:0x0022, B:12:0x0026, B:15:0x0036, B:17:0x0047, B:21:0x0058, B:23:0x005c, B:26:0x006c, B:27:0x0071, B:30:0x0077, B:31:0x007e), top: B:35:0x0022 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void performWithValidEglBase(j$.util.function.Consumer<org.webrtc.EglBase> r6) {
        /*
            r5 = this;
            java.lang.String r0 = "consumer"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r6, r0)
            boolean r0 = r5.isReleased
            java.lang.String r1 = "Tried to use a released EglBase"
            if (r0 == 0) goto L_0x0018
            java.lang.String r6 = org.thoughtcrime.securesms.components.webrtc.EglBaseWrapperKt.access$getTAG$p()
            java.lang.Exception r0 = new java.lang.Exception
            r0.<init>()
            org.signal.core.util.logging.Log.d(r6, r1, r0)
            return
        L_0x0018:
            org.webrtc.EglBase r0 = r5.eglBase
            if (r0 != 0) goto L_0x001d
            return
        L_0x001d:
            java.util.concurrent.locks.Lock r0 = r5.lock
            r0.lock()
            boolean r2 = r5.isReleased     // Catch: all -> 0x007f
            if (r2 == 0) goto L_0x0036
            java.lang.String r6 = org.thoughtcrime.securesms.components.webrtc.EglBaseWrapperKt.access$getTAG$p()     // Catch: all -> 0x007f
            java.lang.Exception r2 = new java.lang.Exception     // Catch: all -> 0x007f
            r2.<init>()     // Catch: all -> 0x007f
            org.signal.core.util.logging.Log.d(r6, r1, r2)     // Catch: all -> 0x007f
            r0.unlock()
            return
        L_0x0036:
            org.webrtc.EglBase r1 = r5.eglBase     // Catch: all -> 0x007f
            org.webrtc.EglBase$Context r1 = r1.getEglBaseContext()     // Catch: all -> 0x007f
            java.lang.String r2 = "eglBase.eglBaseContext"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r1, r2)     // Catch: all -> 0x007f
            boolean r2 = r1 instanceof org.webrtc.EglBase14.Context     // Catch: all -> 0x007f
            r3 = 1
            r4 = 0
            if (r2 == 0) goto L_0x0058
            org.webrtc.EglBase14$Context r1 = (org.webrtc.EglBase14.Context) r1     // Catch: all -> 0x007f
            android.opengl.EGLContext r1 = r1.getRawContext()     // Catch: all -> 0x007f
            android.opengl.EGLContext r2 = android.opengl.EGL14.EGL_NO_CONTEXT     // Catch: all -> 0x007f
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual(r1, r2)     // Catch: all -> 0x007f
            if (r1 != 0) goto L_0x0056
            goto L_0x006a
        L_0x0056:
            r3 = 0
            goto L_0x006a
        L_0x0058:
            boolean r2 = r1 instanceof org.webrtc.EglBase10.Context     // Catch: all -> 0x007f
            if (r2 == 0) goto L_0x0077
            org.webrtc.EglBase10$Context r1 = (org.webrtc.EglBase10.Context) r1     // Catch: all -> 0x007f
            javax.microedition.khronos.egl.EGLContext r1 = r1.getRawContext()     // Catch: all -> 0x007f
            javax.microedition.khronos.egl.EGLContext r2 = javax.microedition.khronos.egl.EGL10.EGL_NO_CONTEXT     // Catch: all -> 0x007f
            boolean r1 = kotlin.jvm.internal.Intrinsics.areEqual(r1, r2)     // Catch: all -> 0x007f
            if (r1 != 0) goto L_0x0056
        L_0x006a:
            if (r3 == 0) goto L_0x0071
            org.webrtc.EglBase r1 = r5.eglBase     // Catch: all -> 0x007f
            r6.accept(r1)     // Catch: all -> 0x007f
        L_0x0071:
            kotlin.Unit r6 = kotlin.Unit.INSTANCE     // Catch: all -> 0x007f
            r0.unlock()
            return
        L_0x0077:
            java.lang.IllegalStateException r6 = new java.lang.IllegalStateException     // Catch: all -> 0x007f
            java.lang.String r1 = "Unknown context"
            r6.<init>(r1)     // Catch: all -> 0x007f
            throw r6     // Catch: all -> 0x007f
        L_0x007f:
            r6 = move-exception
            r0.unlock()
            goto L_0x0085
        L_0x0084:
            throw r6
        L_0x0085:
            goto L_0x0084
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.webrtc.EglBaseWrapper.performWithValidEglBase(j$.util.function.Consumer):void");
    }

    public final void releaseEglBase() {
        if (!this.isReleased && this.eglBase != null) {
            Lock lock = this.lock;
            lock.lock();
            try {
                if (!this.isReleased) {
                    this.isReleased = true;
                    this.eglBase.release();
                    Unit unit = Unit.INSTANCE;
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /* compiled from: EglBaseWrapper.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u0001H\u0007J\b\u0010\u000b\u001a\u00020\fH\u0007J\u0010\u0010\r\u001a\u00020\f2\u0006\u0010\n\u001a\u00020\u0001H\u0007J\u0018\u0010\u000e\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u00012\u0006\u0010\u0010\u001a\u00020\u0001H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00010\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/EglBaseWrapper$Companion;", "", "()V", EglBaseWrapper.OUTGOING_PLACEHOLDER, "", "eglBaseWrapper", "Lorg/thoughtcrime/securesms/components/webrtc/EglBaseWrapper;", "holders", "", "acquireEglBase", "holder", "forceRelease", "", "releaseEglBase", "replaceHolder", "currentHolder", "newHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final EglBaseWrapper acquireEglBase(Object obj) {
            Intrinsics.checkNotNullParameter(obj, "holder");
            EglBaseWrapper eglBaseWrapper = EglBaseWrapper.eglBaseWrapper;
            if (eglBaseWrapper == null) {
                eglBaseWrapper = new EglBaseWrapper(EglBase.CC.create(), null);
            }
            EglBaseWrapper.eglBaseWrapper = eglBaseWrapper;
            EglBaseWrapper.holders.add(obj);
            String str = EglBaseWrapperKt.TAG;
            Log.d(str, "Acquire EGL " + EglBaseWrapper.eglBaseWrapper + " with holder: " + obj);
            return eglBaseWrapper;
        }

        @JvmStatic
        public final void releaseEglBase(Object obj) {
            Intrinsics.checkNotNullParameter(obj, "holder");
            String str = EglBaseWrapperKt.TAG;
            Log.d(str, "Release EGL with holder: " + obj);
            EglBaseWrapper.holders.remove(obj);
            if (EglBaseWrapper.holders.isEmpty()) {
                Log.d(EglBaseWrapperKt.TAG, "Holders empty, release EGL Base");
                EglBaseWrapper eglBaseWrapper = EglBaseWrapper.eglBaseWrapper;
                if (eglBaseWrapper != null) {
                    eglBaseWrapper.releaseEglBase();
                }
                EglBaseWrapper.eglBaseWrapper = null;
            }
        }

        @JvmStatic
        public final void replaceHolder(Object obj, Object obj2) {
            Intrinsics.checkNotNullParameter(obj, "currentHolder");
            Intrinsics.checkNotNullParameter(obj2, "newHolder");
            if (!Intrinsics.areEqual(obj, obj2)) {
                String str = EglBaseWrapperKt.TAG;
                Log.d(str, "Replace holder " + obj + " with " + obj2);
                EglBaseWrapper.holders.add(obj2);
                EglBaseWrapper.holders.remove(obj);
            }
        }

        @JvmStatic
        public final void forceRelease() {
            EglBaseWrapper eglBaseWrapper = EglBaseWrapper.eglBaseWrapper;
            if (eglBaseWrapper != null) {
                eglBaseWrapper.releaseEglBase();
            }
            EglBaseWrapper.eglBaseWrapper = null;
            EglBaseWrapper.holders.clear();
        }
    }
}
