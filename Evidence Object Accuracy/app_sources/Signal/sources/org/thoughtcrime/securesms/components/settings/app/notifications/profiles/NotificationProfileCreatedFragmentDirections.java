package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class NotificationProfileCreatedFragmentDirections {
    private NotificationProfileCreatedFragmentDirections() {
    }

    public static ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment(long j) {
        return new ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment(j);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_notificationProfileCreatedFragment_to_notificationProfileDetailsFragment;
        }

        private ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment = (ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment) obj;
            return this.arguments.containsKey("profileId") == actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment.arguments.containsKey("profileId") && getProfileId() == actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment.getProfileId() && getActionId() == actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment.getActionId();
        }

        public int hashCode() {
            return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + "}";
        }
    }
}
