package org.thoughtcrime.securesms.components.webrtc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class CallToastPopupWindow extends PopupWindow {
    private static final long DURATION = TimeUnit.SECONDS.toMillis(3);
    private final ViewGroup parent;

    public static void show(ViewGroup viewGroup) {
        new CallToastPopupWindow(viewGroup).show();
    }

    private CallToastPopupWindow(ViewGroup viewGroup) {
        super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.call_toast_popup_window, viewGroup, false), -1, ViewUtil.dpToPx(94));
        this.parent = viewGroup;
        setAnimationStyle(R.style.PopupAnimation);
    }

    public void show() {
        showAtLocation(this.parent, 8388659, 0, 0);
        measureChild();
        update();
        getContentView().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.CallToastPopupWindow$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                CallToastPopupWindow.this.dismiss();
            }
        }, DURATION);
    }

    private void measureChild() {
        getContentView().measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    }
}
