package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ConversationSearchBottomBar extends ConstraintLayout {
    private EventListener eventListener;
    private View progressWheel;
    private View searchDown;
    private TextView searchPositionText;
    private View searchUp;

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onSearchMoveDownPressed();

        void onSearchMoveUpPressed();
    }

    public ConversationSearchBottomBar(Context context) {
        super(context);
    }

    public ConversationSearchBottomBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.searchUp = findViewById(R.id.conversation_search_up);
        this.searchDown = findViewById(R.id.conversation_search_down);
        this.searchPositionText = (TextView) findViewById(R.id.conversation_search_position);
        this.progressWheel = findViewById(R.id.conversation_search_progress_wheel);
    }

    public void setData(int i, int i2) {
        this.progressWheel.setVisibility(8);
        this.searchUp.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ConversationSearchBottomBar$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationSearchBottomBar.m477$r8$lambda$vJSzdwsIzpMp0GGASJJX0sgIVU(ConversationSearchBottomBar.this, view);
            }
        });
        this.searchDown.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ConversationSearchBottomBar$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ConversationSearchBottomBar.m476$r8$lambda$nFRrD5nHme6zH2AOibk2rLtzcM(ConversationSearchBottomBar.this, view);
            }
        });
        boolean z = false;
        if (i2 > 0) {
            this.searchPositionText.setText(getResources().getString(R.string.ConversationActivity_search_position, Integer.valueOf(i + 1), Integer.valueOf(i2)));
        } else {
            this.searchPositionText.setText(R.string.ConversationActivity_no_results);
        }
        setViewEnabled(this.searchUp, i < i2 - 1);
        View view = this.searchDown;
        if (i > 0) {
            z = true;
        }
        setViewEnabled(view, z);
    }

    public /* synthetic */ void lambda$setData$0(View view) {
        EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onSearchMoveUpPressed();
        }
    }

    public /* synthetic */ void lambda$setData$1(View view) {
        EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onSearchMoveDownPressed();
        }
    }

    public void showLoading() {
        this.progressWheel.setVisibility(0);
    }

    private void setViewEnabled(View view, boolean z) {
        view.setEnabled(z);
        view.setAlpha(z ? 1.0f : 0.25f);
    }

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }
}
