package org.thoughtcrime.securesms.components.settings.app.internal.donor;

import androidx.lifecycle.ViewModel;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.functions.Function3;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.donations.StripeDeclineCode;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.UnexpectedSubscriptionCancellation;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.SubscriptionReceiptRequestResponseJob;
import org.thoughtcrime.securesms.keyvalue.DonationsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.rx.RxStore;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: DonorErrorConfigurationViewModel.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\f\u001a\u00020\rJ\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0007H\u0002J\u0010\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0007H\u0002J\u0010\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0007H\u0002J\u0010\u0010\u0012\u001a\u00020\u000f2\u0006\u0010\u0005\u001a\u00020\u0007H\u0002J\b\u0010\u0013\u001a\u00020\u000fH\u0014J\u0006\u0010\u0014\u001a\u00020\rJ\u000e\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0016\u001a\u00020\u0017J\u000e\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u0017J\u000e\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u0017R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Lio/reactivex/rxjava3/core/Flowable;", "Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationState;", "getState", "()Lio/reactivex/rxjava3/core/Flowable;", "store", "Lorg/thoughtcrime/securesms/util/rx/RxStore;", "clear", "Lio/reactivex/rxjava3/core/Completable;", "handleBoostExpiration", "", "handleGiftExpiration", "handleSubscriptionExpiration", "handleSubscriptionPaymentFailure", "onCleared", "save", "setSelectedBadge", "badgeIndex", "", "setSelectedUnexpectedSubscriptionCancellation", "unexpectedSubscriptionCancellationIndex", "setStripeDeclineCode", "stripeDeclineCodeIndex", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonorErrorConfigurationViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final Flowable<DonorErrorConfigurationState> state;
    private final RxStore<DonorErrorConfigurationState> store;

    public DonorErrorConfigurationViewModel() {
        RxStore<DonorErrorConfigurationState> rxStore = new RxStore<>(new DonorErrorConfigurationState(null, null, null, null, 15, null), null, 2, null);
        this.store = rxStore;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        this.state = rxStore.getStateFlowable();
        Single subscribeOn = ApplicationDependencies.getDonationsService().getGiftBadges(Locale.getDefault()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonorErrorConfigurationViewModel.m681_init_$lambda0((ServiceResponse) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonorErrorConfigurationViewModel.m682_init_$lambda2((Map) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "getDonationsService()\n  …scribeOn(Schedulers.io())");
        Single subscribeOn2 = ApplicationDependencies.getDonationsService().getBoostBadge(Locale.getDefault()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonorErrorConfigurationViewModel.m683_init_$lambda3((ServiceResponse) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonorErrorConfigurationViewModel.m684_init_$lambda4((SignalServiceProfile.Badge) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn2, "getDonationsService()\n  …scribeOn(Schedulers.io())");
        Single subscribeOn3 = ApplicationDependencies.getDonationsService().getSubscriptionLevels(Locale.getDefault()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonorErrorConfigurationViewModel.m685_init_$lambda5((ServiceResponse) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonorErrorConfigurationViewModel.m686_init_$lambda7((SubscriptionLevels) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn3, "getDonationsService()\n  …scribeOn(Schedulers.io())");
        Disposable subscribe = Single.zip(subscribeOn, subscribeOn2, subscribeOn3, new Function3() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Function3
            public final Object apply(Object obj, Object obj2, Object obj3) {
                return DonorErrorConfigurationViewModel.m687_init_$lambda8((List) obj, (List) obj2, (List) obj3);
            }
        }).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonorErrorConfigurationViewModel.m688_init_$lambda9(DonorErrorConfigurationViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "zip(giftBadges, boostBad…(badges = badges) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final Flowable<DonorErrorConfigurationState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final SingleSource m681_init_$lambda0(ServiceResponse serviceResponse) {
        return serviceResponse.flattenResult();
    }

    /* renamed from: _init_$lambda-2 */
    public static final List m682_init_$lambda2(Map map) {
        Collection<SignalServiceProfile.Badge> values = map.values();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(values, 10));
        for (SignalServiceProfile.Badge badge : values) {
            Intrinsics.checkNotNullExpressionValue(badge, "it");
            arrayList.add(Badges.fromServiceBadge(badge));
        }
        return arrayList;
    }

    /* renamed from: _init_$lambda-3 */
    public static final SingleSource m683_init_$lambda3(ServiceResponse serviceResponse) {
        return serviceResponse.flattenResult();
    }

    /* renamed from: _init_$lambda-4 */
    public static final List m684_init_$lambda4(SignalServiceProfile.Badge badge) {
        Intrinsics.checkNotNullExpressionValue(badge, "it");
        return CollectionsKt__CollectionsJVMKt.listOf(Badges.fromServiceBadge(badge));
    }

    /* renamed from: _init_$lambda-5 */
    public static final SingleSource m685_init_$lambda5(ServiceResponse serviceResponse) {
        return serviceResponse.flattenResult();
    }

    /* renamed from: _init_$lambda-7 */
    public static final List m686_init_$lambda7(SubscriptionLevels subscriptionLevels) {
        Collection<SubscriptionLevels.Level> values = subscriptionLevels.getLevels().values();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(values, 10));
        for (SubscriptionLevels.Level level : values) {
            SignalServiceProfile.Badge badge = level.getBadge();
            Intrinsics.checkNotNullExpressionValue(badge, "it.badge");
            arrayList.add(Badges.fromServiceBadge(badge));
        }
        return arrayList;
    }

    /* renamed from: _init_$lambda-8 */
    public static final List m687_init_$lambda8(List list, List list2, List list3) {
        Intrinsics.checkNotNullExpressionValue(list, "g");
        Intrinsics.checkNotNullExpressionValue(list2, "b");
        List list4 = CollectionsKt___CollectionsKt.plus((Collection) list, (Iterable) list2);
        Intrinsics.checkNotNullExpressionValue(list3, "s");
        return CollectionsKt___CollectionsKt.plus((Collection) list4, (Iterable) list3);
    }

    /* renamed from: _init_$lambda-9 */
    public static final void m688_init_$lambda9(DonorErrorConfigurationViewModel donorErrorConfigurationViewModel, List list) {
        Intrinsics.checkNotNullParameter(donorErrorConfigurationViewModel, "this$0");
        donorErrorConfigurationViewModel.store.update(new Function1<DonorErrorConfigurationState, DonorErrorConfigurationState>(list) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$2$1
            final /* synthetic */ List<Badge> $badges;

            /* access modifiers changed from: package-private */
            {
                this.$badges = r1;
            }

            public final DonorErrorConfigurationState invoke(DonorErrorConfigurationState donorErrorConfigurationState) {
                Intrinsics.checkNotNullParameter(donorErrorConfigurationState, "it");
                List<Badge> list2 = this.$badges;
                Intrinsics.checkNotNullExpressionValue(list2, "badges");
                return DonorErrorConfigurationState.copy$default(donorErrorConfigurationState, list2, null, null, null, 14, null);
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void setSelectedBadge(int i) {
        this.store.update(new Function1<DonorErrorConfigurationState, DonorErrorConfigurationState>(i) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$setSelectedBadge$1
            final /* synthetic */ int $badgeIndex;

            /* access modifiers changed from: package-private */
            {
                this.$badgeIndex = r1;
            }

            public final DonorErrorConfigurationState invoke(DonorErrorConfigurationState donorErrorConfigurationState) {
                Intrinsics.checkNotNullParameter(donorErrorConfigurationState, "it");
                int i2 = this.$badgeIndex;
                boolean z = false;
                if (i2 >= 0 && i2 < donorErrorConfigurationState.getBadges().size()) {
                    z = true;
                }
                return DonorErrorConfigurationState.copy$default(donorErrorConfigurationState, null, z ? donorErrorConfigurationState.getBadges().get(this.$badgeIndex) : null, null, null, 13, null);
            }
        });
    }

    public final void setSelectedUnexpectedSubscriptionCancellation(int i) {
        this.store.update(new Function1<DonorErrorConfigurationState, DonorErrorConfigurationState>(i) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$setSelectedUnexpectedSubscriptionCancellation$1
            final /* synthetic */ int $unexpectedSubscriptionCancellationIndex;

            /* access modifiers changed from: package-private */
            {
                this.$unexpectedSubscriptionCancellationIndex = r1;
            }

            public final DonorErrorConfigurationState invoke(DonorErrorConfigurationState donorErrorConfigurationState) {
                Intrinsics.checkNotNullParameter(donorErrorConfigurationState, "it");
                int i2 = this.$unexpectedSubscriptionCancellationIndex;
                boolean z = false;
                if (i2 >= 0 && i2 < UnexpectedSubscriptionCancellation.values().length) {
                    z = true;
                }
                return DonorErrorConfigurationState.copy$default(donorErrorConfigurationState, null, null, z ? UnexpectedSubscriptionCancellation.values()[this.$unexpectedSubscriptionCancellationIndex] : null, null, 11, null);
            }
        });
    }

    public final void setStripeDeclineCode(int i) {
        this.store.update(new Function1<DonorErrorConfigurationState, DonorErrorConfigurationState>(i) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$setStripeDeclineCode$1
            final /* synthetic */ int $stripeDeclineCodeIndex;

            /* access modifiers changed from: package-private */
            {
                this.$stripeDeclineCodeIndex = r1;
            }

            public final DonorErrorConfigurationState invoke(DonorErrorConfigurationState donorErrorConfigurationState) {
                Intrinsics.checkNotNullParameter(donorErrorConfigurationState, "it");
                int i2 = this.$stripeDeclineCodeIndex;
                boolean z = false;
                if (i2 >= 0 && i2 < StripeDeclineCode.Code.values().length) {
                    z = true;
                }
                return DonorErrorConfigurationState.copy$default(donorErrorConfigurationState, null, null, null, z ? StripeDeclineCode.Code.values()[this.$stripeDeclineCodeIndex] : null, 7, null);
            }
        });
    }

    public final Completable save() {
        Completable andThen = clear().andThen(Completable.fromAction(new Action(this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda8
            public final /* synthetic */ DonorErrorConfigurationViewModel f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonorErrorConfigurationViewModel.m690save$lambda11(DonorErrorConfigurationState.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io()));
        Intrinsics.checkNotNullExpressionValue(andThen, "clear().andThen(saveState)");
        return andThen;
    }

    /* renamed from: save$lambda-11 */
    public static final void m690save$lambda11(DonorErrorConfigurationState donorErrorConfigurationState, DonorErrorConfigurationViewModel donorErrorConfigurationViewModel) {
        Intrinsics.checkNotNullParameter(donorErrorConfigurationState, "$snapshot");
        Intrinsics.checkNotNullParameter(donorErrorConfigurationViewModel, "this$0");
        Object obj = SubscriptionReceiptRequestResponseJob.MUTEX;
        Intrinsics.checkNotNullExpressionValue(obj, "MUTEX");
        synchronized (obj) {
            Badge selectedBadge = donorErrorConfigurationState.getSelectedBadge();
            boolean z = true;
            if (selectedBadge != null && selectedBadge.isGift()) {
                donorErrorConfigurationViewModel.handleGiftExpiration(donorErrorConfigurationState);
            } else {
                Badge selectedBadge2 = donorErrorConfigurationState.getSelectedBadge();
                if (selectedBadge2 != null && selectedBadge2.isBoost()) {
                    donorErrorConfigurationViewModel.handleBoostExpiration(donorErrorConfigurationState);
                } else {
                    Badge selectedBadge3 = donorErrorConfigurationState.getSelectedBadge();
                    if (selectedBadge3 == null || !selectedBadge3.isSubscription()) {
                        z = false;
                    }
                    if (z) {
                        donorErrorConfigurationViewModel.handleSubscriptionExpiration(donorErrorConfigurationState);
                    } else {
                        donorErrorConfigurationViewModel.handleSubscriptionPaymentFailure(donorErrorConfigurationState);
                    }
                }
            }
            Unit unit = Unit.INSTANCE;
        }
    }

    public final Completable clear() {
        Completable fromAction = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationViewModel$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonorErrorConfigurationViewModel.m689clear$lambda13(DonorErrorConfigurationViewModel.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromAction, "fromAction {\n      synch…l\n        )\n      }\n    }");
        return fromAction;
    }

    /* renamed from: clear$lambda-13 */
    public static final void m689clear$lambda13(DonorErrorConfigurationViewModel donorErrorConfigurationViewModel) {
        Intrinsics.checkNotNullParameter(donorErrorConfigurationViewModel, "this$0");
        Object obj = SubscriptionReceiptRequestResponseJob.MUTEX;
        Intrinsics.checkNotNullExpressionValue(obj, "MUTEX");
        synchronized (obj) {
            SignalStore.donationsValues().setExpiredBadge(null);
            SignalStore.donationsValues().setExpiredGiftBadge(null);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationReason(null);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationTimestamp(0);
            SignalStore.donationsValues().setUnexpectedSubscriptionCancelationChargeFailure(null);
            Unit unit = Unit.INSTANCE;
        }
        donorErrorConfigurationViewModel.store.update(DonorErrorConfigurationViewModel$clear$1$2.INSTANCE);
    }

    private final void handleBoostExpiration(DonorErrorConfigurationState donorErrorConfigurationState) {
        SignalStore.donationsValues().setExpiredBadge(donorErrorConfigurationState.getSelectedBadge());
    }

    private final void handleGiftExpiration(DonorErrorConfigurationState donorErrorConfigurationState) {
        SignalStore.donationsValues().setExpiredGiftBadge(donorErrorConfigurationState.getSelectedBadge());
    }

    private final void handleSubscriptionExpiration(DonorErrorConfigurationState donorErrorConfigurationState) {
        SignalStore.donationsValues().setExpiredBadge(donorErrorConfigurationState.getSelectedBadge());
        handleSubscriptionPaymentFailure(donorErrorConfigurationState);
    }

    private final void handleSubscriptionPaymentFailure(DonorErrorConfigurationState donorErrorConfigurationState) {
        DonationsValues donationsValues = SignalStore.donationsValues();
        UnexpectedSubscriptionCancellation selectedUnexpectedSubscriptionCancellation = donorErrorConfigurationState.getSelectedUnexpectedSubscriptionCancellation();
        ActiveSubscription.ChargeFailure chargeFailure = null;
        donationsValues.setUnexpectedSubscriptionCancelationReason(selectedUnexpectedSubscriptionCancellation != null ? selectedUnexpectedSubscriptionCancellation.getStatus() : null);
        SignalStore.donationsValues().setUnexpectedSubscriptionCancelationTimestamp(System.currentTimeMillis());
        DonationsValues donationsValues2 = SignalStore.donationsValues();
        StripeDeclineCode.Code selectedStripeDeclineCode = donorErrorConfigurationState.getSelectedStripeDeclineCode();
        if (selectedStripeDeclineCode != null) {
            chargeFailure = new ActiveSubscription.ChargeFailure(selectedStripeDeclineCode.getCode(), "Test Charge Failure", "Test Network Status", "Test Network Reason", "Test");
        }
        donationsValues2.setUnexpectedSubscriptionCancelationChargeFailure(chargeFailure);
    }
}
