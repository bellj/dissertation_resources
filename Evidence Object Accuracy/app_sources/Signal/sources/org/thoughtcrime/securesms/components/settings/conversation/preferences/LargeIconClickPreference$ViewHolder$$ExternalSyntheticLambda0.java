package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LargeIconClickPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LargeIconClickPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ LargeIconClickPreference.Model f$0;

    public /* synthetic */ LargeIconClickPreference$ViewHolder$$ExternalSyntheticLambda0(LargeIconClickPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        LargeIconClickPreference.ViewHolder.m1209bind$lambda0(this.f$0, view);
    }
}
