package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.lifecycle.Lifecycle;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.logsubmit.SubmitDebugLogActivity;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.internal.push.WhoAmIResponse;

/* compiled from: ChangeNumberLockActivity.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0003\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\b\u0010\u000b\u001a\u00020\nH\u0016J\b\u0010\f\u001a\u00020\nH\u0002J\u001a\u0010\r\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\u0010\u0010\u0012\u001a\u00020\n2\u0006\u0010\u0013\u001a\u00020\u0014H\u0002J\b\u0010\u0015\u001a\u00020\nH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberLockActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "()V", "changeNumberRepository", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberRepository;", "disposables", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "dynamicTheme", "Lorg/thoughtcrime/securesms/util/DynamicTheme;", "checkWhoAmI", "", "onBackPressed", "onChangeStatusConfirmed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "onFailedToGetChangeNumberStatus", "error", "", "onResume", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberLockActivity extends PassphraseRequiredActivity {
    public static final Companion Companion = new Companion(null);
    private ChangeNumberRepository changeNumberRepository;
    private final LifecycleDisposable disposables = new LifecycleDisposable();
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    @JvmStatic
    public static final Intent createIntent(Context context) {
        return Companion.createIntent(context);
    }

    @Override // androidx.activity.ComponentActivity, android.app.Activity
    public void onBackPressed() {
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        this.dynamicTheme.onCreate(this);
        LifecycleDisposable lifecycleDisposable = this.disposables;
        Lifecycle lifecycle = getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        setContentView(R.layout.activity_change_number_lock);
        Context applicationContext = getApplicationContext();
        Intrinsics.checkNotNullExpressionValue(applicationContext, "applicationContext");
        this.changeNumberRepository = new ChangeNumberRepository(applicationContext);
        checkWhoAmI();
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        this.dynamicTheme.onResume(this);
    }

    private final void checkWhoAmI() {
        LifecycleDisposable lifecycleDisposable = this.disposables;
        ChangeNumberRepository changeNumberRepository = this.changeNumberRepository;
        if (changeNumberRepository == null) {
            Intrinsics.throwUninitializedPropertyAccessException("changeNumberRepository");
            changeNumberRepository = null;
        }
        Single observeOn = changeNumberRepository.whoAmI().flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberLockActivity.m596checkWhoAmI$lambda1(ChangeNumberLockActivity.this, (WhoAmIResponse) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        ChangeNumberLockActivity$checkWhoAmI$2 changeNumberLockActivity$checkWhoAmI$2 = new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$checkWhoAmI$2
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "p0");
                ((ChangeNumberLockActivity) this.receiver).onFailedToGetChangeNumberStatus(th);
            }
        };
        Intrinsics.checkNotNullExpressionValue(observeOn, "observeOn(AndroidSchedulers.mainThread())");
        lifecycleDisposable.add(SubscribersKt.subscribeBy(observeOn, changeNumberLockActivity$checkWhoAmI$2, new Function1<Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$checkWhoAmI$3
            final /* synthetic */ ChangeNumberLockActivity this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                invoke(bool);
                return Unit.INSTANCE;
            }

            public final void invoke(Boolean bool) {
                this.this$0.onChangeStatusConfirmed();
            }
        }));
    }

    /* renamed from: checkWhoAmI$lambda-1 */
    public static final SingleSource m596checkWhoAmI$lambda1(ChangeNumberLockActivity changeNumberLockActivity, WhoAmIResponse whoAmIResponse) {
        Intrinsics.checkNotNullParameter(changeNumberLockActivity, "this$0");
        if (Objects.equals(whoAmIResponse.getNumber(), SignalStore.account().getE164())) {
            Log.i(ChangeNumberLockActivityKt.TAG, "Local and remote numbers match, nothing needs to be done.");
            return Single.just(Boolean.FALSE);
        }
        String str = ChangeNumberLockActivityKt.TAG;
        Log.i(str, "Local (" + SignalStore.account().getE164() + ") and remote (" + whoAmIResponse.getNumber() + ") numbers do not match, updating local.");
        ChangeNumberRepository changeNumberRepository = changeNumberLockActivity.changeNumberRepository;
        if (changeNumberRepository == null) {
            Intrinsics.throwUninitializedPropertyAccessException("changeNumberRepository");
            changeNumberRepository = null;
        }
        String number = whoAmIResponse.getNumber();
        Intrinsics.checkNotNullExpressionValue(number, "whoAmI.number");
        PNI parseOrThrow = PNI.parseOrThrow(whoAmIResponse.getPni());
        Intrinsics.checkNotNullExpressionValue(parseOrThrow, "parseOrThrow(whoAmI.pni)");
        return changeNumberRepository.changeLocalNumber(number, parseOrThrow).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberLockActivity.m597checkWhoAmI$lambda1$lambda0((Unit) obj);
            }
        });
    }

    /* renamed from: checkWhoAmI$lambda-1$lambda-0 */
    public static final Boolean m597checkWhoAmI$lambda1$lambda0(Unit unit) {
        return Boolean.TRUE;
    }

    public final void onChangeStatusConfirmed() {
        SignalStore.misc().unlockChangeNumber();
        MaterialAlertDialogBuilder title = new MaterialAlertDialogBuilder(this).setTitle(R.string.ChangeNumberLockActivity__change_status_confirmed);
        String e164 = SignalStore.account().getE164();
        Intrinsics.checkNotNull(e164);
        title.setMessage((CharSequence) getString(R.string.ChangeNumberLockActivity__your_number_has_been_confirmed_as_s, new Object[]{PhoneNumberFormatter.prettyPrint(e164)})).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$$ExternalSyntheticLambda5
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChangeNumberLockActivity.m598onChangeStatusConfirmed$lambda2(ChangeNumberLockActivity.this, dialogInterface, i);
            }
        }).setCancelable(false).show();
    }

    /* renamed from: onChangeStatusConfirmed$lambda-2 */
    public static final void m598onChangeStatusConfirmed$lambda2(ChangeNumberLockActivity changeNumberLockActivity, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(changeNumberLockActivity, "this$0");
        changeNumberLockActivity.startActivity(MainActivity.clearTop(changeNumberLockActivity));
        changeNumberLockActivity.finish();
    }

    public final void onFailedToGetChangeNumberStatus(Throwable th) {
        Log.w(ChangeNumberLockActivityKt.TAG, "Unable to determine status of change number", th);
        new MaterialAlertDialogBuilder(this).setTitle(R.string.ChangeNumberLockActivity__change_status_unconfirmed).setMessage((CharSequence) getString(R.string.ChangeNumberLockActivity__we_could_not_determine_the_status_of_your_change_number_request, new Object[]{th.getClass().getSimpleName()})).setPositiveButton(R.string.ChangeNumberLockActivity__retry, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChangeNumberLockActivity.m599onFailedToGetChangeNumberStatus$lambda3(ChangeNumberLockActivity.this, dialogInterface, i);
            }
        }).setNegativeButton(R.string.ChangeNumberLockActivity__leave, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChangeNumberLockActivity.m600onFailedToGetChangeNumberStatus$lambda4(ChangeNumberLockActivity.this, dialogInterface, i);
            }
        }).setNeutralButton(R.string.ChangeNumberLockActivity__submit_debug_log, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChangeNumberLockActivity.m601onFailedToGetChangeNumberStatus$lambda5(ChangeNumberLockActivity.this, dialogInterface, i);
            }
        }).setCancelable(false).show();
    }

    /* renamed from: onFailedToGetChangeNumberStatus$lambda-3 */
    public static final void m599onFailedToGetChangeNumberStatus$lambda3(ChangeNumberLockActivity changeNumberLockActivity, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(changeNumberLockActivity, "this$0");
        changeNumberLockActivity.checkWhoAmI();
    }

    /* renamed from: onFailedToGetChangeNumberStatus$lambda-4 */
    public static final void m600onFailedToGetChangeNumberStatus$lambda4(ChangeNumberLockActivity changeNumberLockActivity, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(changeNumberLockActivity, "this$0");
        changeNumberLockActivity.finish();
    }

    /* renamed from: onFailedToGetChangeNumberStatus$lambda-5 */
    public static final void m601onFailedToGetChangeNumberStatus$lambda5(ChangeNumberLockActivity changeNumberLockActivity, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(changeNumberLockActivity, "this$0");
        changeNumberLockActivity.startActivity(new Intent(changeNumberLockActivity, SubmitDebugLogActivity.class));
        changeNumberLockActivity.finish();
    }

    /* compiled from: ChangeNumberLockActivity.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberLockActivity$Companion;", "", "()V", "createIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Intent createIntent(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intent intent = new Intent(context, ChangeNumberLockActivity.class);
            intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
            return intent;
        }
    }
}
