package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import com.annimon.stream.function.Function;
import java.math.BigDecimal;
import java.util.Currency;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.money.FiatMoney;

/* compiled from: BoostViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "currency", "Ljava/util/Currency;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostViewModel$refresh$4 extends Lambda implements Function1<Currency, Unit> {
    final /* synthetic */ BoostViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BoostViewModel$refresh$4(BoostViewModel boostViewModel) {
        super(1);
        this.this$0 = boostViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Currency currency) {
        invoke(currency);
        return Unit.INSTANCE;
    }

    public final void invoke(Currency currency) {
        Intrinsics.checkNotNullParameter(currency, "currency");
        this.this$0.store.update(new Function(currency) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$refresh$4$$ExternalSyntheticLambda0
            public final /* synthetic */ Currency f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$refresh$4.m956invoke$lambda0(this.f$0, (BoostState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final BoostState m956invoke$lambda0(Currency currency, BoostState boostState) {
        Intrinsics.checkNotNullParameter(currency, "$currency");
        FiatMoney fiatMoney = new FiatMoney(BigDecimal.ZERO, currency);
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : currency, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : fiatMoney, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : null, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }
}
