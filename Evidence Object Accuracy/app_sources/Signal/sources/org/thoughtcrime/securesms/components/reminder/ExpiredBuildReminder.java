package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.view.View;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class ExpiredBuildReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    public ExpiredBuildReminder(Context context) {
        super(null, context.getString(R.string.ExpiredBuildReminder_this_version_of_signal_has_expired));
        setOkListener(new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.ExpiredBuildReminder$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ExpiredBuildReminder.$r8$lambda$XPhWeNPgkUmivj_PvQyjlxloUAg(this.f$0, view);
            }
        });
        addAction(new Reminder.Action(context.getString(R.string.ExpiredBuildReminder_update_now), R.id.reminder_action_update_now));
    }

    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public List<Reminder.Action> getActions() {
        return super.getActions();
    }

    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public Reminder.Importance getImportance() {
        return Reminder.Importance.TERMINAL;
    }

    public static boolean isEligible() {
        return SignalStore.misc().isClientDeprecated();
    }
}
