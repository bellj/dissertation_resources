package org.thoughtcrime.securesms.components;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public abstract class FullScreenDialogFragment extends DialogFragment {
    protected Toolbar toolbar;

    protected abstract int getDialogLayoutResource();

    protected abstract int getTitle();

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.Signal_DayNight_Dialog_FullScreen);
    }

    @Override // androidx.fragment.app.Fragment
    public final View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.full_screen_dialog_fragment, viewGroup, false);
        layoutInflater.inflate(getDialogLayoutResource(), (ViewGroup) inflate.findViewById(R.id.full_screen_dialog_content), true);
        this.toolbar = (Toolbar) inflate.findViewById(R.id.full_screen_dialog_toolbar);
        if (getTitle() != -1) {
            this.toolbar.setTitle(getTitle());
        }
        this.toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.FullScreenDialogFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                FullScreenDialogFragment.m478$r8$lambda$ziSe5Mn8s1edENcw1fozJYhP6Y(FullScreenDialogFragment.this, view);
            }
        });
        return inflate;
    }

    public /* synthetic */ void lambda$onCreateView$0(View view) {
        onNavigateUp();
    }

    protected void onNavigateUp() {
        dismissAllowingStateLoss();
    }
}
