package org.thoughtcrime.securesms.components.emoji;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EmojiPageViewGridAdapter$$ExternalSyntheticLambda3 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return new MappingViewHolder.SimpleViewHolder((View) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
