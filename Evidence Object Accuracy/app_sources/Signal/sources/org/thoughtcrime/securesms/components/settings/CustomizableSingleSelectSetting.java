package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import androidx.constraintlayout.widget.Group;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.SingleSelectSetting;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class CustomizableSingleSelectSetting {

    /* loaded from: classes4.dex */
    public interface CustomizableSingleSelectionListener extends SingleSelectSetting.SingleSelectSelectionChangedListener {
        void onCustomizeClicked(Item item);
    }

    /* loaded from: classes4.dex */
    public static class ViewHolder extends MappingViewHolder<Item> {
        private final View customize = findViewById(R.id.customizable_single_select_customize);
        private final Group customizeGroup = ((Group) findViewById(R.id.customizable_single_select_customize_group));
        private final SingleSelectSetting.ViewHolder delegate;
        private final CustomizableSingleSelectionListener selectionListener;

        public ViewHolder(View view, CustomizableSingleSelectionListener customizableSingleSelectionListener) {
            super(view);
            this.selectionListener = customizableSingleSelectionListener;
            this.delegate = new SingleSelectSetting.ViewHolder(view, customizableSingleSelectionListener);
        }

        public void bind(Item item) {
            this.delegate.bind(item.singleSelectItem);
            this.customizeGroup.setVisibility(item.singleSelectItem.isSelected() ? 0 : 8);
            this.customize.setOnClickListener(new CustomizableSingleSelectSetting$ViewHolder$$ExternalSyntheticLambda0(this, item));
        }

        public /* synthetic */ void lambda$bind$0(Item item, View view) {
            this.selectionListener.onCustomizeClicked(item);
        }
    }

    /* loaded from: classes4.dex */
    public static class Item implements MappingModel<Item> {
        private final Object customValue;
        private final SingleSelectSetting.Item singleSelectItem;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Item item) {
            return MappingModel.CC.$default$getChangePayload(this, item);
        }

        public <T> Item(T t, String str, boolean z, Object obj, String str2) {
            this.customValue = obj;
            this.singleSelectItem = new SingleSelectSetting.Item(t, str, str2, z);
        }

        public Object getCustomValue() {
            return this.customValue;
        }

        public boolean areItemsTheSame(Item item) {
            return this.singleSelectItem.areItemsTheSame(item.singleSelectItem);
        }

        public boolean areContentsTheSame(Item item) {
            return this.singleSelectItem.areContentsTheSame(item.singleSelectItem) && Objects.equals(this.customValue, item.customValue);
        }
    }
}
