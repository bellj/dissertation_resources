package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: ActiveSubscriptionPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ActiveSubscriptionPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ActiveSubscriptionPreference {
    public static final ActiveSubscriptionPreference INSTANCE = new ActiveSubscriptionPreference();

    private ActiveSubscriptionPreference() {
    }

    /* compiled from: ActiveSubscriptionPreference.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0000H\u0016J\u0010\u0010\u001f\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0000H\u0016R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ActiveSubscriptionPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "price", "Lorg/signal/core/util/money/FiatMoney;", "subscription", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "renewalTimestamp", "", "redemptionState", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;", "activeSubscription", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$Subscription;", "onContactSupport", "Lkotlin/Function0;", "", "(Lorg/signal/core/util/money/FiatMoney;Lorg/thoughtcrime/securesms/subscription/Subscription;JLorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$Subscription;Lkotlin/jvm/functions/Function0;)V", "getActiveSubscription", "()Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$Subscription;", "getOnContactSupport", "()Lkotlin/jvm/functions/Function0;", "getPrice", "()Lorg/signal/core/util/money/FiatMoney;", "getRedemptionState", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;", "getRenewalTimestamp", "()J", "getSubscription", "()Lorg/thoughtcrime/securesms/subscription/Subscription;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final ActiveSubscription.Subscription activeSubscription;
        private final Function0<Unit> onContactSupport;
        private final FiatMoney price;
        private final ManageDonationsState.SubscriptionRedemptionState redemptionState;
        private final long renewalTimestamp;
        private final Subscription subscription;

        public /* synthetic */ Model(FiatMoney fiatMoney, Subscription subscription, long j, ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState, ActiveSubscription.Subscription subscription2, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(fiatMoney, subscription, (i & 4) != 0 ? -1 : j, subscriptionRedemptionState, subscription2, function0);
        }

        public final FiatMoney getPrice() {
            return this.price;
        }

        public final Subscription getSubscription() {
            return this.subscription;
        }

        public final long getRenewalTimestamp() {
            return this.renewalTimestamp;
        }

        public final ManageDonationsState.SubscriptionRedemptionState getRedemptionState() {
            return this.redemptionState;
        }

        public final ActiveSubscription.Subscription getActiveSubscription() {
            return this.activeSubscription;
        }

        public final Function0<Unit> getOnContactSupport() {
            return this.onContactSupport;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(FiatMoney fiatMoney, Subscription subscription, long j, ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState, ActiveSubscription.Subscription subscription2, Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(fiatMoney, "price");
            Intrinsics.checkNotNullParameter(subscription, "subscription");
            Intrinsics.checkNotNullParameter(subscriptionRedemptionState, "redemptionState");
            Intrinsics.checkNotNullParameter(subscription2, "activeSubscription");
            Intrinsics.checkNotNullParameter(function0, "onContactSupport");
            this.price = fiatMoney;
            this.subscription = subscription;
            this.renewalTimestamp = j;
            this.redemptionState = subscriptionRedemptionState;
            this.activeSubscription = subscription2;
            this.onContactSupport = function0;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.subscription.getId(), model.subscription.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(this.subscription, model.subscription) && this.renewalTimestamp == model.renewalTimestamp && this.redemptionState == model.redemptionState && FiatMoney.equals(this.price, model.price) && Intrinsics.areEqual(this.activeSubscription, model.activeSubscription);
        }
    }

    /* compiled from: ActiveSubscriptionPreference.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0016J\u0010\u0010\u0019\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\b\u0010\u001a\u001a\u00020\u0017H\u0002J\u0010\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010\u001c\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002J\u0010\u0010\u001d\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0002H\u0002R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u000e\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0014\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\r¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ActiveSubscriptionPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ActiveSubscriptionPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "getBadge", "()Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "expiry", "Landroid/widget/TextView;", "getExpiry", "()Landroid/widget/TextView;", "price", "getPrice", "progress", "Landroid/widget/ProgressBar;", "getProgress", "()Landroid/widget/ProgressBar;", MultiselectForwardFragment.DIALOG_TITLE, "getTitle", "bind", "", "model", "presentFailureState", "presentInProgressState", "presentPaymentFailureState", "presentRedemptionFailureState", "presentRenewalState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badge;
        private final TextView expiry;
        private final TextView price;
        private final ProgressBar progress;
        private final TextView title;

        /* compiled from: ActiveSubscriptionPreference.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[ManageDonationsState.SubscriptionRedemptionState.values().length];
                iArr[ManageDonationsState.SubscriptionRedemptionState.NONE.ordinal()] = 1;
                iArr[ManageDonationsState.SubscriptionRedemptionState.IN_PROGRESS.ordinal()] = 2;
                iArr[ManageDonationsState.SubscriptionRedemptionState.FAILED.ordinal()] = 3;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.my_support_badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.my_support_badge)");
            this.badge = (BadgeImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.my_support_title);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.my_support_title)");
            this.title = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.my_support_price);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.my_support_price)");
            this.price = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.my_support_expiry);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.id.my_support_expiry)");
            this.expiry = (TextView) findViewById4;
            View findViewById5 = view.findViewById(R.id.my_support_progress);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.my_support_progress)");
            this.progress = (ProgressBar) findViewById5;
        }

        public final BadgeImageView getBadge() {
            return this.badge;
        }

        public final TextView getTitle() {
            return this.title;
        }

        public final TextView getPrice() {
            return this.price;
        }

        public final TextView getExpiry() {
            return this.expiry;
        }

        public final ProgressBar getProgress() {
            return this.progress;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.badge.setBadge(model.getSubscription().getBadge());
            this.title.setText(model.getSubscription().getName());
            TextView textView = this.price;
            Context context = this.context;
            textView.setText(context.getString(R.string.MySupportPreference__s_per_month, FiatMoneyUtil.format(context.getResources(), model.getPrice(), FiatMoneyUtil.formatOptions())));
            this.expiry.setMovementMethod(LinkMovementMethod.getInstance());
            int i = WhenMappings.$EnumSwitchMapping$0[model.getRedemptionState().ordinal()];
            if (i == 1) {
                presentRenewalState(model);
            } else if (i == 2) {
                presentInProgressState();
            } else if (i == 3) {
                presentFailureState(model);
            }
        }

        private final void presentRenewalState(Model model) {
            this.expiry.setText(this.context.getString(R.string.MySupportPreference__renews_s, DateUtils.formatDateWithYear(Locale.getDefault(), model.getRenewalTimestamp())));
            this.badge.setAlpha(1.0f);
            ViewExtensionsKt.setVisible(this.progress, false);
        }

        private final void presentInProgressState() {
            this.expiry.setText(this.context.getString(R.string.MySupportPreference__processing_transaction));
            this.badge.setAlpha(0.2f);
            ViewExtensionsKt.setVisible(this.progress, true);
        }

        private final void presentFailureState(Model model) {
            if (model.getActiveSubscription().isFailedPayment() || SignalStore.donationsValues().getShouldCancelSubscriptionBeforeNextSubscribeAttempt()) {
                presentPaymentFailureState(model);
            } else {
                presentRedemptionFailureState(model);
            }
        }

        private final void presentPaymentFailureState(Model model) {
            String string = this.context.getString(R.string.MySupportPreference__please_contact_support);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…__please_contact_support)");
            this.expiry.setText(SpanUtil.clickSubstring(this.context.getString(R.string.DonationsErrors__error_processing_payment_s, string), string, new ActiveSubscriptionPreference$ViewHolder$$ExternalSyntheticLambda1(model), ContextCompat.getColor(this.context, R.color.signal_accent_primary)));
            this.badge.setAlpha(0.2f);
            ViewExtensionsKt.setVisible(this.progress, false);
        }

        /* renamed from: presentPaymentFailureState$lambda-0 */
        public static final void m964presentPaymentFailureState$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnContactSupport().invoke();
        }

        private final void presentRedemptionFailureState(Model model) {
            String string = this.context.getString(R.string.MySupportPreference__please_contact_support);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…__please_contact_support)");
            this.expiry.setText(SpanUtil.clickSubstring(this.context.getString(R.string.MySupportPreference__couldnt_add_badge_s, string), string, new ActiveSubscriptionPreference$ViewHolder$$ExternalSyntheticLambda0(model), ContextCompat.getColor(this.context, R.color.signal_accent_primary)));
            this.badge.setAlpha(0.2f);
            ViewExtensionsKt.setVisible(this.progress, false);
        }

        /* renamed from: presentRedemptionFailureState$lambda-1 */
        public static final void m965presentRedemptionFailureState$lambda1(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnContactSupport().invoke();
        }
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m962register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ActiveSubscriptionPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ActiveSubscriptionPreference.$r8$lambda$k5BhFH17G_UTVle_N20uvnck6DE((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.my_support_preference));
    }
}
