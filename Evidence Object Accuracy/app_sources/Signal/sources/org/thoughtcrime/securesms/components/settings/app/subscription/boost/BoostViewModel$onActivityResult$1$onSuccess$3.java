package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.subjects.PublishSubject;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState;

/* compiled from: BoostViewModel.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class BoostViewModel$onActivityResult$1$onSuccess$3 extends Lambda implements Function0<Unit> {
    final /* synthetic */ BoostViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BoostViewModel$onActivityResult$1$onSuccess$3(BoostViewModel boostViewModel) {
        super(0);
        this.this$0 = boostViewModel;
    }

    /* renamed from: invoke$lambda-0 */
    public static final BoostState m953invoke$lambda0(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.READY, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$onActivityResult$1$onSuccess$3$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$onActivityResult$1$onSuccess$3.m953invoke$lambda0((BoostState) obj);
            }
        });
        PublishSubject publishSubject = this.this$0.eventPublisher;
        Badge boostBadge = ((BoostState) this.this$0.store.getState()).getBoostBadge();
        Intrinsics.checkNotNull(boostBadge);
        publishSubject.onNext(new DonationEvent.PaymentConfirmationSuccess(boostBadge));
    }
}
