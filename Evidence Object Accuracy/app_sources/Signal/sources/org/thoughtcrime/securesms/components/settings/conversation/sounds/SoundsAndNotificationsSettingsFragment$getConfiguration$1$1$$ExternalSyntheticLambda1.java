package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ SoundsAndNotificationsSettingsFragment f$0;

    public /* synthetic */ SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda1(SoundsAndNotificationsSettingsFragment soundsAndNotificationsSettingsFragment) {
        this.f$0 = soundsAndNotificationsSettingsFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        SoundsAndNotificationsSettingsFragment$getConfiguration$1.AnonymousClass1.m1218invoke$lambda0(this.f$0, dialogInterface, i);
    }
}
