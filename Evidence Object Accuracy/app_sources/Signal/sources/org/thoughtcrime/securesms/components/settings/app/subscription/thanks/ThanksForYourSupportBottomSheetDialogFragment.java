package org.thoughtcrime.securesms.components.settings.app.subscription.thanks;

import android.animation.Animator;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.fragment.FragmentKt;
import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.BadgeRepository;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: ThanksForYourSupportBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B\u0005¢\u0006\u0002\u0010\u0002J&\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u001a\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u00102\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\b\u0010\u001d\u001a\u00020\u0018H\u0002J\b\u0010\u001e\u001a\u00020\u0018H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nXD¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/thanks/ThanksForYourSupportBottomSheetDialogFragment;", "Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "()V", "badgeRepository", "Lorg/thoughtcrime/securesms/badges/BadgeRepository;", "controlState", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/thanks/ThanksForYourSupportBottomSheetDialogFragment$ControlState;", "heading", "Landroid/widget/TextView;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "switch", "Lcom/google/android/material/switchmaterial/SwitchMaterial;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "", "dialog", "Landroid/content/DialogInterface;", "onViewCreated", "view", "presentBoostCopy", "presentSubscriptionCopy", "Companion", "ControlState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ThanksForYourSupportBottomSheetDialogFragment extends FixedRoundedCornerBottomSheetDialogFragment {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ThanksForYourSupportBottomSheetDialogFragment.class);
    private BadgeRepository badgeRepository;
    private ControlState controlState;
    private TextView heading;
    private final float peekHeightPercentage = 1.0f;

    /* renamed from: switch */
    private SwitchMaterial f1switch;

    /* compiled from: ThanksForYourSupportBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/thanks/ThanksForYourSupportBottomSheetDialogFragment$ControlState;", "", "(Ljava/lang/String;I)V", "FEATURE", "DISPLAY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private enum ControlState {
        FEATURE,
        DISPLAY
    }

    @Override // org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment
    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        return layoutInflater.inflate(R.layout.thanks_for_your_support_bottom_sheet_dialog_fragment, viewGroup, false);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        boolean z;
        Intrinsics.checkNotNullParameter(view, "view");
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        this.badgeRepository = new BadgeRepository(requireContext);
        View findViewById = view.findViewById(R.id.thanks_bottom_sheet_badge);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.thanks_bottom_sheet_badge)");
        BadgeImageView badgeImageView = (BadgeImageView) findViewById;
        View findViewById2 = view.findViewById(R.id.thanks_bottom_sheet_lottie);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.thanks_bottom_sheet_lottie)");
        LottieAnimationView lottieAnimationView = (LottieAnimationView) findViewById2;
        View findViewById3 = view.findViewById(R.id.thanks_bottom_sheet_badge_name);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.t…_bottom_sheet_badge_name)");
        View findViewById4 = view.findViewById(R.id.thanks_bottom_sheet_done);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.thanks_bottom_sheet_done)");
        MaterialButton materialButton = (MaterialButton) findViewById4;
        View findViewById5 = view.findViewById(R.id.thanks_bottom_sheet_control_text);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.t…ottom_sheet_control_text)");
        TextView textView = (TextView) findViewById5;
        View findViewById6 = view.findViewById(R.id.thanks_bottom_sheet_featured_note);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.t…ttom_sheet_featured_note)");
        View findViewById7 = view.findViewById(R.id.thanks_bottom_sheet_subhead);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.t…nks_bottom_sheet_subhead)");
        TextView textView2 = (TextView) findViewById7;
        View findViewById8 = view.findViewById(R.id.thanks_bottom_sheet_heading);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.t…nks_bottom_sheet_heading)");
        this.heading = (TextView) findViewById8;
        View findViewById9 = view.findViewById(R.id.thanks_bottom_sheet_switch);
        Intrinsics.checkNotNullExpressionValue(findViewById9, "view.findViewById(R.id.thanks_bottom_sheet_switch)");
        this.f1switch = (SwitchMaterial) findViewById9;
        ThanksForYourSupportBottomSheetDialogFragmentArgs fromBundle = ThanksForYourSupportBottomSheetDialogFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        badgeImageView.setBadge(fromBundle.getBadge());
        ((TextView) findViewById3).setText(fromBundle.getBadge().getName());
        if (fromBundle.getBadge().isBoost()) {
            List<Badge> badges = Recipient.self().getBadges();
            Intrinsics.checkNotNullExpressionValue(badges, "self().badges");
            if (!(badges instanceof Collection) || !badges.isEmpty()) {
                for (Badge badge : badges) {
                    if (!badge.isBoost()) {
                        z = true;
                        break;
                    }
                }
            }
            z = false;
            if (z) {
                textView2.setText(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__youve_earned_a_boost_badge_display);
            } else {
                textView2.setText(new SpannableStringBuilder(getString(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__youve_earned_a_boost_badge_display)).append((CharSequence) " ").append((CharSequence) getString(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__you_can_also)).append((CharSequence) " ").append(SpanUtil.clickable(getString(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__become_a_montly_sustainer), ContextCompat.getColor(requireContext(), R.color.signal_accent_primary), new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.thanks.ThanksForYourSupportBottomSheetDialogFragment$$ExternalSyntheticLambda0
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        ThanksForYourSupportBottomSheetDialogFragment.$r8$lambda$DTmUoHR_QiL7sPNrZ4T4P7VzYFI(ThanksForYourSupportBottomSheetDialogFragment.this, view2);
                    }
                })));
            }
        } else {
            textView2.setText(getString(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__youve_earned_s_badge_display, fromBundle.getBadge().getName()));
        }
        List<Badge> badges2 = Recipient.self().getBadges();
        Intrinsics.checkNotNullExpressionValue(badges2, "self().badges");
        ArrayList arrayList = new ArrayList();
        for (Object obj : badges2) {
            if (!Intrinsics.areEqual(((Badge) obj).getId(), fromBundle.getBadge().getId())) {
                arrayList.add(obj);
            }
        }
        boolean z2 = !arrayList.isEmpty();
        boolean displayBadgesOnProfile = SignalStore.donationsValues().getDisplayBadgesOnProfile();
        SwitchMaterial switchMaterial = null;
        if (z2 && displayBadgesOnProfile) {
            SwitchMaterial switchMaterial2 = this.f1switch;
            if (switchMaterial2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("switch");
            } else {
                switchMaterial = switchMaterial2;
            }
            switchMaterial.setChecked(false);
            textView.setText(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__make_featured_badge);
            ViewExtensionsKt.setVisible(findViewById6, true);
            this.controlState = ControlState.FEATURE;
        } else if (!z2 || displayBadgesOnProfile) {
            SwitchMaterial switchMaterial3 = this.f1switch;
            if (switchMaterial3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("switch");
            } else {
                switchMaterial = switchMaterial3;
            }
            switchMaterial.setChecked(true);
            textView.setText(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__display_on_profile);
            ViewExtensionsKt.setVisible(findViewById6, false);
            this.controlState = ControlState.DISPLAY;
        } else {
            SwitchMaterial switchMaterial4 = this.f1switch;
            if (switchMaterial4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("switch");
            } else {
                switchMaterial = switchMaterial4;
            }
            switchMaterial.setChecked(false);
            textView.setText(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__display_on_profile);
            ViewExtensionsKt.setVisible(findViewById6, false);
            this.controlState = ControlState.DISPLAY;
        }
        if (fromBundle.getIsBoost()) {
            presentBoostCopy();
            badgeImageView.setVisibility(4);
            ViewExtensionsKt.setVisible(lottieAnimationView, true);
            lottieAnimationView.playAnimation();
            lottieAnimationView.addAnimatorListener(new AnimationCompleteListener(lottieAnimationView) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.thanks.ThanksForYourSupportBottomSheetDialogFragment$onViewCreated$3
                final /* synthetic */ LottieAnimationView $lottie;

                /* access modifiers changed from: package-private */
                {
                    this.$lottie = r1;
                }

                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    this.$lottie.removeAnimatorListener(this);
                    this.$lottie.setMinAndMaxFrame(30, 91);
                    this.$lottie.setRepeatMode(1);
                    this.$lottie.setRepeatCount(-1);
                    this.$lottie.setFrame(30);
                    this.$lottie.playAnimation();
                }
            });
        } else {
            presentSubscriptionCopy();
            ViewExtensionsKt.setVisible(lottieAnimationView, false);
        }
        materialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.thanks.ThanksForYourSupportBottomSheetDialogFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ThanksForYourSupportBottomSheetDialogFragment.m1077$r8$lambda$_ynmfHuJ3FWa33K06fnyhBx_tA(ThanksForYourSupportBottomSheetDialogFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1078onViewCreated$lambda1(ThanksForYourSupportBottomSheetDialogFragment thanksForYourSupportBottomSheetDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(thanksForYourSupportBottomSheetDialogFragment, "this$0");
        thanksForYourSupportBottomSheetDialogFragment.requireActivity().finish();
        FragmentActivity requireActivity = thanksForYourSupportBottomSheetDialogFragment.requireActivity();
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = thanksForYourSupportBottomSheetDialogFragment.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        requireActivity.startActivity(companion.subscriptions(requireContext));
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m1079onViewCreated$lambda3(ThanksForYourSupportBottomSheetDialogFragment thanksForYourSupportBottomSheetDialogFragment, View view) {
        Intrinsics.checkNotNullParameter(thanksForYourSupportBottomSheetDialogFragment, "this$0");
        thanksForYourSupportBottomSheetDialogFragment.dismissAllowingStateLoss();
    }

    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    public void onDismiss(DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(dialogInterface, "dialog");
        SwitchMaterial switchMaterial = this.f1switch;
        if (switchMaterial == null) {
            Intrinsics.throwUninitializedPropertyAccessException("switch");
            switchMaterial = null;
        }
        boolean isChecked = switchMaterial.isChecked();
        ThanksForYourSupportBottomSheetDialogFragmentArgs fromBundle = ThanksForYourSupportBottomSheetDialogFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        ControlState controlState = this.controlState;
        if (controlState == null) {
            Intrinsics.throwUninitializedPropertyAccessException("controlState");
            controlState = null;
        }
        if (controlState == ControlState.DISPLAY) {
            BadgeRepository badgeRepository = this.badgeRepository;
            if (badgeRepository == null) {
                Intrinsics.throwUninitializedPropertyAccessException("badgeRepository");
                badgeRepository = null;
            }
            SubscribersKt.subscribeBy$default(BadgeRepository.setVisibilityForAllBadges$default(badgeRepository, isChecked, null, 2, null), ThanksForYourSupportBottomSheetDialogFragment$onDismiss$1.INSTANCE, (Function0) null, 2, (Object) null);
        } else if (isChecked) {
            BadgeRepository badgeRepository2 = this.badgeRepository;
            if (badgeRepository2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("badgeRepository");
                badgeRepository2 = null;
            }
            Badge badge = fromBundle.getBadge();
            Intrinsics.checkNotNullExpressionValue(badge, "args.badge");
            SubscribersKt.subscribeBy$default(badgeRepository2.setFeaturedBadge(badge), ThanksForYourSupportBottomSheetDialogFragment$onDismiss$2.INSTANCE, (Function0) null, 2, (Object) null);
        }
        if (fromBundle.getIsBoost()) {
            FragmentKt.findNavController(this).popBackStack();
            return;
        }
        requireActivity().finish();
        FragmentActivity requireActivity = requireActivity();
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        requireActivity.startActivity(companion.manageSubscriptions(requireContext));
    }

    private final void presentBoostCopy() {
        TextView textView = this.heading;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("heading");
            textView = null;
        }
        textView.setText(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__thanks_for_the_boost);
    }

    private final void presentSubscriptionCopy() {
        TextView textView = this.heading;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("heading");
            textView = null;
        }
        textView.setText(R.string.SubscribeThanksForYourSupportBottomSheetDialogFragment__thanks_for_your_support);
    }

    /* compiled from: ThanksForYourSupportBottomSheetDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/thanks/ThanksForYourSupportBottomSheetDialogFragment$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
