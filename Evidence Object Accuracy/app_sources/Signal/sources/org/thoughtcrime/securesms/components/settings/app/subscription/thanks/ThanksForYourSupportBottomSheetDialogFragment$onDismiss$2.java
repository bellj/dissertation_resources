package org.thoughtcrime.securesms.components.settings.app.subscription.thanks;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.logging.Log;

/* compiled from: ThanksForYourSupportBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ThanksForYourSupportBottomSheetDialogFragment$onDismiss$2 extends Lambda implements Function1<Throwable, Unit> {
    public static final ThanksForYourSupportBottomSheetDialogFragment$onDismiss$2 INSTANCE = new ThanksForYourSupportBottomSheetDialogFragment$onDismiss$2();

    ThanksForYourSupportBottomSheetDialogFragment$onDismiss$2() {
        super(1);
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    public final void invoke(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "it");
        Log.w(ThanksForYourSupportBottomSheetDialogFragment.TAG, "Failure while updating featured badge", th);
    }
}
