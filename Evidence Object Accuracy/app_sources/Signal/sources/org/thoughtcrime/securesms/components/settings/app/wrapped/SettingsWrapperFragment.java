package org.thoughtcrime.securesms.components.settings.app.wrapped;

import android.os.Bundle;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: SettingsWrapperFragment.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001:\u0001\u0013B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\b\u001a\u00020\u0001H&J\b\u0010\t\u001a\u00020\nH\u0002J\u001a\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\n2\b\b\u0001\u0010\u0011\u001a\u00020\u0012R\u001e\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004@BX.¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/wrapped/SettingsWrapperFragment;", "Landroidx/fragment/app/Fragment;", "()V", "<set-?>", "Landroidx/appcompat/widget/Toolbar;", "toolbar", "getToolbar", "()Landroidx/appcompat/widget/Toolbar;", "getFragment", "onBackPressed", "", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "setTitle", "titleId", "", "OnBackPressed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class SettingsWrapperFragment extends Fragment {
    private Toolbar toolbar;

    public abstract Fragment getFragment();

    public SettingsWrapperFragment() {
        super(R.layout.settings_wrapper_fragment);
    }

    public final Toolbar getToolbar() {
        Toolbar toolbar = this.toolbar;
        if (toolbar != null) {
            return toolbar;
        }
        Intrinsics.throwUninitializedPropertyAccessException("toolbar");
        return null;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        this.toolbar = (Toolbar) findViewById;
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.wrapped.SettingsWrapperFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SettingsWrapperFragment.m1080$r8$lambda$IXmMUQCNWlV8p0EOikoB8lIRBk(SettingsWrapperFragment.this, view2);
            }
        });
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressed());
        getChildFragmentManager().beginTransaction().replace(R.id.wrapped_fragment, getFragment()).commit();
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1081onViewCreated$lambda0(SettingsWrapperFragment settingsWrapperFragment, View view) {
        Intrinsics.checkNotNullParameter(settingsWrapperFragment, "this$0");
        settingsWrapperFragment.onBackPressed();
    }

    public final void setTitle(int i) {
        getToolbar().setTitle(i);
    }

    public final void onBackPressed() {
        if (!getChildFragmentManager().popBackStackImmediate()) {
            requireActivity().onNavigateUp();
        }
    }

    /* compiled from: SettingsWrapperFragment.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/wrapped/SettingsWrapperFragment$OnBackPressed;", "Landroidx/activity/OnBackPressedCallback;", "(Lorg/thoughtcrime/securesms/components/settings/app/wrapped/SettingsWrapperFragment;)V", "handleOnBackPressed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private final class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            SettingsWrapperFragment.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            SettingsWrapperFragment.this.onBackPressed();
        }
    }
}
