package org.thoughtcrime.securesms.components.settings.conversation;

import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: GroupCapacityResult.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t¢\u0006\u0002\u0010\nJ\u000e\u0010\f\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00030\u0005J\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fJ\u0006\u0010\u0011\u001a\u00020\u000fR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u000bR\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/GroupCapacityResult;", "", "selfId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "members", "", "selectionLimits", "Lorg/thoughtcrime/securesms/groups/SelectionLimits;", "isAnnouncementGroup", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/util/List;Lorg/thoughtcrime/securesms/groups/SelectionLimits;Z)V", "()Z", "getMembers", "getMembersWithoutSelf", "getRemainingCapacity", "", "getSelectionLimit", "getSelectionWarning", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupCapacityResult {
    private final boolean isAnnouncementGroup;
    private final List<RecipientId> members;
    private final SelectionLimits selectionLimits;
    private final RecipientId selfId;

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
    /* JADX WARN: Multi-variable type inference failed */
    public GroupCapacityResult(RecipientId recipientId, List<? extends RecipientId> list, SelectionLimits selectionLimits, boolean z) {
        Intrinsics.checkNotNullParameter(recipientId, "selfId");
        Intrinsics.checkNotNullParameter(list, "members");
        Intrinsics.checkNotNullParameter(selectionLimits, "selectionLimits");
        this.selfId = recipientId;
        this.members = list;
        this.selectionLimits = selectionLimits;
        this.isAnnouncementGroup = z;
    }

    public final boolean isAnnouncementGroup() {
        return this.isAnnouncementGroup;
    }

    public final List<RecipientId> getMembers() {
        return this.members;
    }

    public final int getSelectionLimit() {
        if (!this.selectionLimits.hasHardLimit()) {
            return Integer.MAX_VALUE;
        }
        return this.selectionLimits.getHardLimit() - (this.members.indexOf(this.selfId) != -1 ? 1 : 0);
    }

    public final int getSelectionWarning() {
        if (!this.selectionLimits.hasRecommendedLimit()) {
            return Integer.MAX_VALUE;
        }
        return this.selectionLimits.getRecommendedLimit() - (this.members.indexOf(this.selfId) != -1 ? 1 : 0);
    }

    public final int getRemainingCapacity() {
        return this.selectionLimits.getHardLimit() - this.members.size();
    }

    public final List<RecipientId> getMembersWithoutSelf() {
        ArrayList arrayList = new ArrayList(this.members.size());
        for (RecipientId recipientId : this.members) {
            if (!Intrinsics.areEqual(recipientId, this.selfId)) {
                arrayList.add(recipientId);
            }
        }
        return arrayList;
    }
}
