package org.thoughtcrime.securesms.components.settings.app.appearance;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;

/* compiled from: AppearanceSettingsState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0007HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/appearance/AppearanceSettingsState;", "", "theme", "Lorg/thoughtcrime/securesms/keyvalue/SettingsValues$Theme;", "messageFontSize", "", "language", "", "(Lorg/thoughtcrime/securesms/keyvalue/SettingsValues$Theme;ILjava/lang/String;)V", "getLanguage", "()Ljava/lang/String;", "getMessageFontSize", "()I", "getTheme", "()Lorg/thoughtcrime/securesms/keyvalue/SettingsValues$Theme;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppearanceSettingsState {
    private final String language;
    private final int messageFontSize;
    private final SettingsValues.Theme theme;

    public static /* synthetic */ AppearanceSettingsState copy$default(AppearanceSettingsState appearanceSettingsState, SettingsValues.Theme theme, int i, String str, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            theme = appearanceSettingsState.theme;
        }
        if ((i2 & 2) != 0) {
            i = appearanceSettingsState.messageFontSize;
        }
        if ((i2 & 4) != 0) {
            str = appearanceSettingsState.language;
        }
        return appearanceSettingsState.copy(theme, i, str);
    }

    public final SettingsValues.Theme component1() {
        return this.theme;
    }

    public final int component2() {
        return this.messageFontSize;
    }

    public final String component3() {
        return this.language;
    }

    public final AppearanceSettingsState copy(SettingsValues.Theme theme, int i, String str) {
        Intrinsics.checkNotNullParameter(theme, "theme");
        Intrinsics.checkNotNullParameter(str, "language");
        return new AppearanceSettingsState(theme, i, str);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AppearanceSettingsState)) {
            return false;
        }
        AppearanceSettingsState appearanceSettingsState = (AppearanceSettingsState) obj;
        return this.theme == appearanceSettingsState.theme && this.messageFontSize == appearanceSettingsState.messageFontSize && Intrinsics.areEqual(this.language, appearanceSettingsState.language);
    }

    public int hashCode() {
        return (((this.theme.hashCode() * 31) + this.messageFontSize) * 31) + this.language.hashCode();
    }

    public String toString() {
        return "AppearanceSettingsState(theme=" + this.theme + ", messageFontSize=" + this.messageFontSize + ", language=" + this.language + ')';
    }

    public AppearanceSettingsState(SettingsValues.Theme theme, int i, String str) {
        Intrinsics.checkNotNullParameter(theme, "theme");
        Intrinsics.checkNotNullParameter(str, "language");
        this.theme = theme;
        this.messageFontSize = i;
        this.language = str;
    }

    public final SettingsValues.Theme getTheme() {
        return this.theme;
    }

    public final int getMessageFontSize() {
        return this.messageFontSize;
    }

    public final String getLanguage() {
        return this.language;
    }
}
