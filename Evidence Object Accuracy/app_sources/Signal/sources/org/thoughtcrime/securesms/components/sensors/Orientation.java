package org.thoughtcrime.securesms.components.sensors;

import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;

/* loaded from: classes4.dex */
public enum Orientation {
    PORTRAIT_BOTTOM_EDGE(0),
    LANDSCAPE_LEFT_EDGE(90),
    LANDSCAPE_RIGHT_EDGE(SubsamplingScaleImageView.ORIENTATION_270);
    
    private final int degrees;

    Orientation(int i) {
        this.degrees = i;
    }

    public int getDegrees() {
        return this.degrees;
    }

    public static Orientation fromDegrees(int i) {
        Orientation[] values = values();
        for (Orientation orientation : values) {
            if (orientation.degrees == i) {
                return orientation;
            }
        }
        return PORTRAIT_BOTTOM_EDGE;
    }
}
