package org.thoughtcrime.securesms.components.settings.app.notifications;

import kotlin.Metadata;
import org.signal.core.util.logging.Log;

/* compiled from: NotificationsSettingsFragment.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"CALL_RINGTONE_SELECT", "", "MESSAGE_SOUND_SELECT", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationsSettingsFragmentKt {
    private static final int CALL_RINGTONE_SELECT;
    private static final int MESSAGE_SOUND_SELECT;
    private static final String TAG = Log.tag(NotificationsSettingsFragment.class);
}
