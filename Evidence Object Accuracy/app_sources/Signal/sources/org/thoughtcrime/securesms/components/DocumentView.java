package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.pnikosis.materialishprogress.ProgressWheel;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.util.Util;
import org.whispersystems.signalservice.api.util.OptionalUtil;

/* loaded from: classes.dex */
public class DocumentView extends FrameLayout {
    private static final String TAG = Log.tag(DocumentView.class);
    private final View container;
    private final AnimatingToggle controlToggle;
    private final TextView document;
    private Slide documentSlide;
    private final ImageView downloadButton;
    private SlideClickListener downloadListener;
    private final ProgressWheel downloadProgress;
    private final TextView fileName;
    private final TextView fileSize;
    private final ViewGroup iconContainer;
    private SlideClickListener viewListener;

    public DocumentView(Context context) {
        this(context, null);
    }

    public DocumentView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DocumentView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.document_view, this);
        this.container = findViewById(R.id.document_container);
        this.iconContainer = (ViewGroup) findViewById(R.id.icon_container);
        this.controlToggle = (AnimatingToggle) findViewById(R.id.control_toggle);
        ImageView imageView = (ImageView) findViewById(R.id.download);
        this.downloadButton = imageView;
        ProgressWheel progressWheel = (ProgressWheel) findViewById(R.id.download_progress);
        this.downloadProgress = progressWheel;
        TextView textView = (TextView) findViewById(R.id.file_name);
        this.fileName = textView;
        TextView textView2 = (TextView) findViewById(R.id.file_size);
        this.fileSize = textView2;
        this.document = (TextView) findViewById(R.id.document);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.DocumentView, 0, 0);
            int i2 = obtainStyledAttributes.getInt(2, -16777216);
            int i3 = obtainStyledAttributes.getInt(0, -16777216);
            int i4 = obtainStyledAttributes.getInt(1, -1);
            obtainStyledAttributes.recycle();
            textView.setTextColor(i2);
            textView2.setTextColor(i3);
            imageView.setColorFilter(i4, PorterDuff.Mode.MULTIPLY);
            progressWheel.setBarColor(i4);
        }
    }

    public void setDownloadClickListener(SlideClickListener slideClickListener) {
        this.downloadListener = slideClickListener;
    }

    public void setDocumentClickListener(SlideClickListener slideClickListener) {
        this.viewListener = slideClickListener;
    }

    public void setDocument(Slide slide, boolean z) {
        if (z && slide.isPendingDownload()) {
            this.controlToggle.displayQuick(this.downloadButton);
            this.downloadButton.setOnClickListener(new DownloadClickedListener(slide));
            if (this.downloadProgress.isSpinning()) {
                this.downloadProgress.stopSpinning();
            }
        } else if (!z || slide.getTransferState() != 1) {
            this.controlToggle.displayQuick(this.iconContainer);
            if (this.downloadProgress.isSpinning()) {
                this.downloadProgress.stopSpinning();
            }
        } else {
            this.controlToggle.displayQuick(this.downloadProgress);
            this.downloadProgress.spin();
        }
        this.documentSlide = slide;
        this.fileName.setText((CharSequence) OptionalUtil.or(slide.getFileName(), slide.getCaption()).orElse(getContext().getString(R.string.DocumentView_unnamed_file)));
        this.fileSize.setText(Util.getPrettyFileSize(slide.getFileSize()));
        this.document.setText(slide.getFileType(getContext()).orElse("").toLowerCase());
        setOnClickListener(new OpenClickedListener(slide));
    }

    @Override // android.view.View
    public void setFocusable(boolean z) {
        super.setFocusable(z);
        this.downloadButton.setFocusable(z);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        super.setClickable(z);
        this.downloadButton.setClickable(z);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.downloadButton.setEnabled(z);
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventAsync(PartProgressEvent partProgressEvent) {
        Slide slide = this.documentSlide;
        if (slide != null && partProgressEvent.attachment.equals(slide.asAttachment())) {
            this.downloadProgress.setInstantProgress(((float) partProgressEvent.progress) / ((float) partProgressEvent.total));
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class DownloadClickedListener implements View.OnClickListener {
        private final Slide slide;

        private DownloadClickedListener(Slide slide) {
            DocumentView.this = r1;
            this.slide = slide;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (DocumentView.this.downloadListener != null) {
                DocumentView.this.downloadListener.onClick(view, this.slide);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class OpenClickedListener implements View.OnClickListener {
        private final Slide slide;

        private OpenClickedListener(Slide slide) {
            DocumentView.this = r1;
            this.slide = slide;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (!this.slide.isPendingDownload() && !this.slide.isInProgress() && DocumentView.this.viewListener != null) {
                DocumentView.this.viewListener.onClick(view, this.slide);
            }
        }
    }
}
