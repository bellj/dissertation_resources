package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.core.widget.TextViewCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.registration.RegistrationNavigationActivity;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: AdvancedPrivacySettingsFragment.kt */
@Metadata(d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\r\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001&B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\b\u0010\"\u001a\u00020\u0017H\u0016J\b\u0010#\u001a\u00020\u0017H\u0016J\b\u0010$\u001a\u00020\u0017H\u0002J\b\u0010%\u001a\u00020\u0017H\u0002R\u0014\u0010\u0003\u001a\b\u0018\u00010\u0004R\u00020\u0000X\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0011\u001a\u00020\f8FX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\u0010\u001a\u0004\b\u0012\u0010\u000eR\u000e\u0010\u0014\u001a\u00020\u0015X.¢\u0006\u0002\n\u0000¨\u0006'"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "networkReceiver", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsFragment$NetworkReceiver;", "progressDialog", "Landroid/app/ProgressDialog;", "getProgressDialog", "()Landroid/app/ProgressDialog;", "setProgressDialog", "(Landroid/app/ProgressDialog;)V", "sealedSenderSummary", "", "getSealedSenderSummary", "()Ljava/lang/CharSequence;", "sealedSenderSummary$delegate", "Lkotlin/Lazy;", "statusIcon", "getStatusIcon", "statusIcon$delegate", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsState;", "getPushToggleSummary", "", "isPushEnabled", "", "onPause", "onResume", "registerNetworkReceiver", "unregisterNetworkReceiver", "NetworkReceiver", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AdvancedPrivacySettingsFragment extends DSLSettingsFragment {
    private NetworkReceiver networkReceiver;
    private ProgressDialog progressDialog;
    private final Lazy sealedSenderSummary$delegate = LazyKt__LazyJVMKt.lazy(new AdvancedPrivacySettingsFragment$sealedSenderSummary$2(this));
    private final Lazy statusIcon$delegate = LazyKt__LazyJVMKt.lazy(new Function0<CharSequence>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$statusIcon$2
        final /* synthetic */ AdvancedPrivacySettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final CharSequence invoke() {
            Drawable drawable = ContextCompat.getDrawable(this.this$0.requireContext(), R.drawable.ic_unidentified_delivery);
            if (drawable != null) {
                Intrinsics.checkNotNullExpressionValue(drawable, "requireNotNull(\n      Co…ed_delivery\n      )\n    )");
                drawable.setBounds(0, 0, ViewUtil.dpToPx(20), ViewUtil.dpToPx(20));
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_text_primary_dialog), PorterDuff.Mode.SRC_IN));
                return SpanUtil.buildImageSpan(drawable);
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    });
    private AdvancedPrivacySettingsViewModel viewModel;

    public AdvancedPrivacySettingsFragment() {
        super(R.string.preferences__advanced, 0, 0, null, 14, null);
    }

    public final CharSequence getSealedSenderSummary() {
        Object value = this.sealedSenderSummary$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-sealedSenderSummary>(...)");
        return (CharSequence) value;
    }

    public final ProgressDialog getProgressDialog() {
        return this.progressDialog;
    }

    public final void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

    public final CharSequence getStatusIcon() {
        Object value = this.statusIcon$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-statusIcon>(...)");
        return (CharSequence) value;
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel = this.viewModel;
        if (advancedPrivacySettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            advancedPrivacySettingsViewModel = null;
        }
        advancedPrivacySettingsViewModel.refresh();
        registerNetworkReceiver();
    }

    @Override // androidx.fragment.app.Fragment
    public void onPause() {
        super.onPause();
        unregisterNetworkReceiver();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        AdvancedPrivacySettingsRepository advancedPrivacySettingsRepository = new AdvancedPrivacySettingsRepository(requireContext);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        Intrinsics.checkNotNullExpressionValue(defaultSharedPreferences, "preferences");
        ViewModel viewModel = new ViewModelProvider(this, new AdvancedPrivacySettingsViewModel.Factory(defaultSharedPreferences, advancedPrivacySettingsRepository)).get(AdvancedPrivacySettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ngsViewModel::class.java]");
        AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel = (AdvancedPrivacySettingsViewModel) viewModel;
        this.viewModel = advancedPrivacySettingsViewModel;
        AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel2 = null;
        if (advancedPrivacySettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            advancedPrivacySettingsViewModel = null;
        }
        advancedPrivacySettingsViewModel.m854getState().observe(getViewLifecycleOwner(), new Observer(dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DSLSettingsAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AdvancedPrivacySettingsFragment.m840$r8$lambda$SnhdmfMok2bcPFTPOsyT4lOr4(AdvancedPrivacySettingsFragment.this, this.f$1, (AdvancedPrivacySettingsState) obj);
            }
        });
        AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel3 = this.viewModel;
        if (advancedPrivacySettingsViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            advancedPrivacySettingsViewModel2 = advancedPrivacySettingsViewModel3;
        }
        advancedPrivacySettingsViewModel2.getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AdvancedPrivacySettingsFragment.$r8$lambda$thhIoc6R69DaPiKM22iUUxOPLUI(AdvancedPrivacySettingsFragment.this, (AdvancedPrivacySettingsViewModel.Event) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m841bindAdapter$lambda0(AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment, DSLSettingsAdapter dSLSettingsAdapter, AdvancedPrivacySettingsState advancedPrivacySettingsState) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        if (advancedPrivacySettingsState.getShowProgressSpinner()) {
            ProgressDialog progressDialog = advancedPrivacySettingsFragment.progressDialog;
            boolean z = false;
            if (progressDialog != null && !progressDialog.isShowing()) {
                z = true;
            }
            if (z) {
                advancedPrivacySettingsFragment.progressDialog = ProgressDialog.show(advancedPrivacySettingsFragment.requireContext(), null, null, true);
            }
        } else {
            ProgressDialog progressDialog2 = advancedPrivacySettingsFragment.progressDialog;
            if (progressDialog2 != null) {
                progressDialog2.hide();
            }
        }
        Intrinsics.checkNotNullExpressionValue(advancedPrivacySettingsState, "it");
        dSLSettingsAdapter.submitList(advancedPrivacySettingsFragment.getConfiguration(advancedPrivacySettingsState).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m842bindAdapter$lambda1(AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment, AdvancedPrivacySettingsViewModel.Event event) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsFragment, "this$0");
        if (event == AdvancedPrivacySettingsViewModel.Event.DISABLE_PUSH_FAILED) {
            Toast.makeText(advancedPrivacySettingsFragment.requireContext(), (int) R.string.ApplicationPreferencesActivity_error_connecting_to_server, 1).show();
        }
    }

    private final DSLConfiguration getConfiguration(AdvancedPrivacySettingsState advancedPrivacySettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, advancedPrivacySettingsState) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1
            final /* synthetic */ AdvancedPrivacySettingsState $state;
            final /* synthetic */ AdvancedPrivacySettingsFragment this$0;

            /* compiled from: AdvancedPrivacySettingsFragment.kt */
            @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes4.dex */
            public /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[CensorshipCircumventionState.values().length];
                    iArr[CensorshipCircumventionState.AVAILABLE.ordinal()] = 1;
                    iArr[CensorshipCircumventionState.AVAILABLE_MANUALLY_DISABLED.ordinal()] = 2;
                    iArr[CensorshipCircumventionState.AVAILABLE_AUTOMATICALLY_ENABLED.ordinal()] = 3;
                    iArr[CensorshipCircumventionState.UNAVAILABLE_CONNECTED.ordinal()] = 4;
                    iArr[CensorshipCircumventionState.UNAVAILABLE_NO_INTERNET.ordinal()] = 5;
                    $EnumSwitchMapping$0 = iArr;
                }
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$state = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                int i;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.preferences__signal_messages_and_calls, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from2 = companion.from(AdvancedPrivacySettingsFragment.access$getPushToggleSummary(this.this$0, this.$state.isPushEnabled()), new DSLSettingsText.Modifier[0]);
                boolean isPushEnabled = this.$state.isPushEnabled();
                final AdvancedPrivacySettingsState advancedPrivacySettingsState2 = this.$state;
                final AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment = this.this$0;
                dSLConfiguration.switchPref(from, (r16 & 2) != 0 ? null : from2, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, isPushEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        if (advancedPrivacySettingsState2.isPushEnabled()) {
                            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(advancedPrivacySettingsFragment.requireContext());
                            AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment2 = advancedPrivacySettingsFragment;
                            materialAlertDialogBuilder.setMessage(R.string.ApplicationPreferencesActivity_disable_signal_messages_and_calls_by_unregistering);
                            materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
                            materialAlertDialogBuilder.setPositiveButton(17039370, (DialogInterface.OnClickListener) new AdvancedPrivacySettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda0(advancedPrivacySettingsFragment2));
                            Drawable drawable = ContextCompat.getDrawable(materialAlertDialogBuilder.getContext(), R.drawable.ic_info_outline);
                            if (drawable != null) {
                                drawable.setBounds(0, 0, ViewUtil.dpToPx(32), ViewUtil.dpToPx(32));
                                TextView textView = new TextView(materialAlertDialogBuilder.getContext());
                                int dpToPx = ViewUtil.dpToPx(16);
                                textView.setText(R.string.ApplicationPreferencesActivity_disable_signal_messages_and_calls);
                                textView.setPadding(dpToPx, dpToPx, dpToPx, dpToPx);
                                textView.setCompoundDrawablePadding(dpToPx / 2);
                                TextViewCompat.setTextAppearance(textView, R.style.TextAppearance_Signal_Title2_MaterialDialog);
                                TextViewCompat.setCompoundDrawablesRelative(textView, drawable, null, null, null);
                                materialAlertDialogBuilder.setCustomTitle((View) textView).show();
                                return;
                            }
                            throw new IllegalArgumentException("Required value was null.".toString());
                        }
                        AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment3 = advancedPrivacySettingsFragment;
                        advancedPrivacySettingsFragment3.startActivity(RegistrationNavigationActivity.newIntentForReRegistration(advancedPrivacySettingsFragment3.requireContext()));
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-1$lambda-0  reason: not valid java name */
                    public static final void m843invoke$lambda1$lambda0(AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment2, DialogInterface dialogInterface, int i2) {
                        Intrinsics.checkNotNullParameter(advancedPrivacySettingsFragment2, "this$0");
                        AdvancedPrivacySettingsViewModel access$getViewModel$p = AdvancedPrivacySettingsFragment.access$getViewModel$p(advancedPrivacySettingsFragment2);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.disablePushMessages();
                    }
                });
                DSLSettingsText from3 = companion.from(R.string.preferences_advanced__always_relay_calls, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from4 = companion.from(R.string.preferences_advanced__relay_all_calls_through_the_signal_server_to_avoid_revealing_your_ip_address, new DSLSettingsText.Modifier[0]);
                boolean alwaysRelayCalls = this.$state.getAlwaysRelayCalls();
                final AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment2 = this.this$0;
                final AdvancedPrivacySettingsState advancedPrivacySettingsState3 = this.$state;
                dSLConfiguration.switchPref(from3, (r16 & 2) != 0 ? null : from4, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, alwaysRelayCalls, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        AdvancedPrivacySettingsViewModel access$getViewModel$p = AdvancedPrivacySettingsFragment.access$getViewModel$p(advancedPrivacySettingsFragment2);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setAlwaysRelayCalls(!advancedPrivacySettingsState3.getAlwaysRelayCalls());
                    }
                });
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.preferences_communication__category_censorship_circumvention);
                int i2 = WhenMappings.$EnumSwitchMapping$0[this.$state.getCensorshipCircumventionState().ordinal()];
                if (i2 == 1) {
                    i = R.string.preferences_communication__censorship_circumvention_if_enabled_signal_will_attempt_to_circumvent_censorship;
                } else if (i2 == 2) {
                    i = R.string.preferences_communication__censorship_circumvention_you_have_manually_disabled;
                } else if (i2 == 3) {
                    i = R.string.preferences_communication__censorship_circumvention_has_been_activated_based_on_your_accounts_phone_number;
                } else if (i2 == 4) {
                    i = R.string.preferences_communication__censorship_circumvention_is_not_necessary_you_are_already_connected;
                } else if (i2 == 5) {
                    i = R.string.preferences_communication__censorship_circumvention_can_only_be_activated_when_connected_to_the_internet;
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                DSLSettingsText from5 = companion.from(R.string.preferences_communication__censorship_circumvention, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from6 = companion.from(i, new DSLSettingsText.Modifier[0]);
                boolean censorshipCircumventionEnabled = this.$state.getCensorshipCircumventionEnabled();
                boolean available = this.$state.getCensorshipCircumventionState().getAvailable();
                final AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment3 = this.this$0;
                final AdvancedPrivacySettingsState advancedPrivacySettingsState4 = this.$state;
                dSLConfiguration.switchPref(from5, (r16 & 2) != 0 ? null : from6, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : available, censorshipCircumventionEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        AdvancedPrivacySettingsViewModel access$getViewModel$p = AdvancedPrivacySettingsFragment.access$getViewModel$p(advancedPrivacySettingsFragment3);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setCensorshipCircumventionEnabled(!advancedPrivacySettingsState4.getCensorshipCircumventionEnabled());
                    }
                });
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.preferences_communication__category_sealed_sender);
                SpannableStringBuilder append = new SpannableStringBuilder(this.this$0.getString(R.string.AdvancedPrivacySettingsFragment__show_status_icon)).append((CharSequence) " ").append(this.this$0.getStatusIcon());
                Intrinsics.checkNotNullExpressionValue(append, "SpannableStringBuilder(g…      .append(statusIcon)");
                DSLSettingsText from7 = companion.from(append, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from8 = companion.from(R.string.AdvancedPrivacySettingsFragment__show_an_icon, new DSLSettingsText.Modifier[0]);
                boolean showSealedSenderStatusIcon = this.$state.getShowSealedSenderStatusIcon();
                final AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment4 = this.this$0;
                final AdvancedPrivacySettingsState advancedPrivacySettingsState5 = this.$state;
                dSLConfiguration.switchPref(from7, (r16 & 2) != 0 ? null : from8, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, showSealedSenderStatusIcon, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1.4
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        AdvancedPrivacySettingsViewModel access$getViewModel$p = AdvancedPrivacySettingsFragment.access$getViewModel$p(advancedPrivacySettingsFragment4);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setShowStatusIconForSealedSender(!advancedPrivacySettingsState5.getShowSealedSenderStatusIcon());
                    }
                });
                DSLSettingsText from9 = companion.from(R.string.preferences_communication__sealed_sender_allow_from_anyone, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from10 = companion.from(R.string.preferences_communication__sealed_sender_allow_from_anyone_description, new DSLSettingsText.Modifier[0]);
                boolean allowSealedSenderFromAnyone = this.$state.getAllowSealedSenderFromAnyone();
                final AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment5 = this.this$0;
                final AdvancedPrivacySettingsState advancedPrivacySettingsState6 = this.$state;
                dSLConfiguration.switchPref(from9, (r16 & 2) != 0 ? null : from10, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, allowSealedSenderFromAnyone, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1.5
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        AdvancedPrivacySettingsViewModel access$getViewModel$p = AdvancedPrivacySettingsFragment.access$getViewModel$p(advancedPrivacySettingsFragment5);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setAllowSealedSenderFromAnyone(!advancedPrivacySettingsState6.getAllowSealedSenderFromAnyone());
                    }
                });
                DSLConfiguration.textPref$default(dSLConfiguration, null, companion.from(AdvancedPrivacySettingsFragment.access$getSealedSenderSummary(this.this$0), new DSLSettingsText.Modifier[0]), 1, null);
            }
        });
    }

    public final String getPushToggleSummary(boolean z) {
        if (z) {
            String e164 = SignalStore.account().getE164();
            Intrinsics.checkNotNull(e164);
            String prettyPrint = PhoneNumberFormatter.prettyPrint(e164);
            Intrinsics.checkNotNullExpressionValue(prettyPrint, "{\n      PhoneNumberForma…e.account().e164!!)\n    }");
            return prettyPrint;
        }
        String string = getString(R.string.preferences__free_private_messages_and_calls);
        Intrinsics.checkNotNullExpressionValue(string, "{\n      getString(R.stri…messages_and_calls)\n    }");
        return string;
    }

    private final void registerNetworkReceiver() {
        Context context = getContext();
        if (context != null && this.networkReceiver == null) {
            NetworkReceiver networkReceiver = new NetworkReceiver();
            this.networkReceiver = networkReceiver;
            context.registerReceiver(networkReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    private final void unregisterNetworkReceiver() {
        NetworkReceiver networkReceiver;
        Context context = getContext();
        if (context != null && (networkReceiver = this.networkReceiver) != null) {
            context.unregisterReceiver(networkReceiver);
            this.networkReceiver = null;
        }
    }

    /* compiled from: AdvancedPrivacySettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsFragment$NetworkReceiver;", "Landroid/content/BroadcastReceiver;", "(Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsFragment;)V", "onReceive", "", "context", "Landroid/content/Context;", "intent", "Landroid/content/Intent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class NetworkReceiver extends BroadcastReceiver {
        public NetworkReceiver() {
            AdvancedPrivacySettingsFragment.this = r1;
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(intent, "intent");
            AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel = AdvancedPrivacySettingsFragment.this.viewModel;
            if (advancedPrivacySettingsViewModel == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                advancedPrivacySettingsViewModel = null;
            }
            advancedPrivacySettingsViewModel.refresh();
        }
    }
}
