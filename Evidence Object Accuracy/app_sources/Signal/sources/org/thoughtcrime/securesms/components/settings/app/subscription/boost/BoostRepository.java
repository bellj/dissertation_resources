package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.util.PlatformCurrencyUtil;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.services.DonationsService;

/* compiled from: BoostRepository.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\u001e\u0010\b\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\t0\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostRepository;", "", "donationsService", "Lorg/whispersystems/signalservice/api/services/DonationsService;", "(Lorg/whispersystems/signalservice/api/services/DonationsService;)V", "getBoostBadge", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "getBoosts", "", "Ljava/util/Currency;", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostRepository {
    private final DonationsService donationsService;

    public BoostRepository(DonationsService donationsService) {
        Intrinsics.checkNotNullParameter(donationsService, "donationsService");
        this.donationsService = donationsService;
    }

    public final Single<Map<Currency, List<Boost>>> getBoosts() {
        Single<Map<Currency, List<Boost>>> map = this.donationsService.getBoostAmounts().subscribeOn(Schedulers.io()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return BoostRepository.m936$r8$lambda$79iFTvN8KavhAjFtOqhRvWMkL4((Map) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "donationsService.boostAm…t, currency)) } }\n      }");
        return map;
    }

    /* renamed from: getBoosts$lambda-4 */
    public static final Map m937getBoosts$lambda4(Map map) {
        Intrinsics.checkNotNullExpressionValue(map, MediaSendActivityResult.EXTRA_RESULT);
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        for (Map.Entry entry : map.entrySet()) {
            if (PlatformCurrencyUtil.INSTANCE.getAvailableCurrencyCodes().contains(entry.getKey())) {
                linkedHashMap.put(entry.getKey(), entry.getValue());
            }
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(linkedHashMap.size()));
        for (Map.Entry entry2 : linkedHashMap.entrySet()) {
            linkedHashMap2.put(Currency.getInstance((String) entry2.getKey()), entry2.getValue());
        }
        LinkedHashMap linkedHashMap3 = new LinkedHashMap(MapsKt__MapsJVMKt.mapCapacity(linkedHashMap2.size()));
        for (Map.Entry entry3 : linkedHashMap2.entrySet()) {
            Object key = entry3.getKey();
            Currency currency = (Currency) entry3.getKey();
            List<BigDecimal> list = (List) entry3.getValue();
            ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
            for (BigDecimal bigDecimal : list) {
                arrayList.add(new Boost(new FiatMoney(bigDecimal, currency)));
            }
            linkedHashMap3.put(key, arrayList);
        }
        return linkedHashMap3;
    }

    public final Single<Badge> getBoostBadge() {
        Single<Badge> map = this.donationsService.getBoostBadge(Locale.getDefault()).subscribeOn(Schedulers.io()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return Badges.fromServiceBadge((SignalServiceProfile.Badge) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "donationsService.getBoos…Badges::fromServiceBadge)");
        return map;
    }
}
