package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.InternalPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ InternalPreference.Model f$0;

    public /* synthetic */ InternalPreference$ViewHolder$$ExternalSyntheticLambda0(InternalPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        InternalPreference.ViewHolder.m1207bind$lambda0(this.f$0, view);
    }
}
