package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantListUpdate;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class CallParticipantsListUpdatePopupWindow extends PopupWindow {
    private static final long DURATION = TimeUnit.SECONDS.toMillis(2);
    private final AvatarImageView avatarImageView;
    private final BadgeImageView badgeImageView;
    private final TextView descriptionTextView;
    private boolean isEnabled = true;
    private final ViewGroup parent;
    private final Set<CallParticipantListUpdate.Wrapper> pendingAdditions = new HashSet();
    private final Set<CallParticipantListUpdate.Wrapper> pendingRemovals = new HashSet();

    private static int getManyMemberDescriptionResourceId(boolean z) {
        return z ? R.string.CallParticipantsListUpdatePopupWindow__s_s_and_d_others_joined : R.string.CallParticipantsListUpdatePopupWindow__s_s_and_d_others_left;
    }

    private static int getOneMemberDescriptionResourceId(boolean z) {
        return z ? R.string.CallParticipantsListUpdatePopupWindow__s_joined : R.string.CallParticipantsListUpdatePopupWindow__s_left;
    }

    private static int getThreeMemberDescriptionResourceId(boolean z) {
        return z ? R.string.CallParticipantsListUpdatePopupWindow__s_s_and_s_joined : R.string.CallParticipantsListUpdatePopupWindow__s_s_and_s_left;
    }

    private static int getTwoMemberDescriptionResourceId(boolean z) {
        return z ? R.string.CallParticipantsListUpdatePopupWindow__s_and_s_joined : R.string.CallParticipantsListUpdatePopupWindow__s_and_s_left;
    }

    public CallParticipantsListUpdatePopupWindow(ViewGroup viewGroup) {
        super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.call_participant_list_update, viewGroup, false), -1, ViewUtil.dpToPx(94));
        this.parent = viewGroup;
        this.avatarImageView = (AvatarImageView) getContentView().findViewById(R.id.avatar);
        this.badgeImageView = (BadgeImageView) getContentView().findViewById(R.id.badge);
        this.descriptionTextView = (TextView) getContentView().findViewById(R.id.description);
        setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantsListUpdatePopupWindow$$ExternalSyntheticLambda1
            @Override // android.widget.PopupWindow.OnDismissListener
            public final void onDismiss() {
                CallParticipantsListUpdatePopupWindow.this.showPending();
            }
        });
        setAnimationStyle(R.style.PopupAnimation);
    }

    public void addCallParticipantListUpdate(CallParticipantListUpdate callParticipantListUpdate) {
        this.pendingAdditions.addAll(callParticipantListUpdate.getAdded());
        this.pendingAdditions.removeAll(callParticipantListUpdate.getRemoved());
        this.pendingRemovals.addAll(callParticipantListUpdate.getRemoved());
        this.pendingRemovals.removeAll(callParticipantListUpdate.getAdded());
        if (!isShowing()) {
            showPending();
        }
    }

    public void setEnabled(boolean z) {
        this.isEnabled = z;
        if (!z) {
            dismiss();
        }
    }

    public void showPending() {
        if (!this.pendingAdditions.isEmpty()) {
            showAdditions();
        } else if (!this.pendingRemovals.isEmpty()) {
            showRemovals();
        }
    }

    private void showAdditions() {
        setAvatar(getNextRecipient(this.pendingAdditions.iterator()));
        setDescription(this.pendingAdditions, true);
        this.pendingAdditions.clear();
        show();
    }

    private void showRemovals() {
        setAvatar(getNextRecipient(this.pendingRemovals.iterator()));
        setDescription(this.pendingRemovals, false);
        this.pendingRemovals.clear();
        show();
    }

    private void show() {
        if (this.isEnabled) {
            showAtLocation(this.parent, 8388659, 0, 0);
            measureChild();
            update();
            getContentView().postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.CallParticipantsListUpdatePopupWindow$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    CallParticipantsListUpdatePopupWindow.this.dismiss();
                }
            }, DURATION);
        }
    }

    private void measureChild() {
        getContentView().measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
    }

    private void setAvatar(Recipient recipient) {
        this.avatarImageView.setAvatarUsingProfile(recipient);
        this.badgeImageView.setBadgeFromRecipient(recipient);
        this.avatarImageView.setVisibility(recipient == null ? 8 : 0);
    }

    private void setDescription(Set<CallParticipantListUpdate.Wrapper> set, boolean z) {
        if (set.isEmpty()) {
            this.descriptionTextView.setText("");
        } else {
            setDescriptionForRecipients(set, z);
        }
    }

    private void setDescriptionForRecipients(Set<CallParticipantListUpdate.Wrapper> set, boolean z) {
        String str;
        Iterator<CallParticipantListUpdate.Wrapper> it = set.iterator();
        Context context = getContentView().getContext();
        int size = set.size();
        if (size != 0) {
            if (size == 1) {
                str = context.getString(getOneMemberDescriptionResourceId(z), getNextDisplayName(it));
            } else if (size == 2) {
                str = context.getString(getTwoMemberDescriptionResourceId(z), getNextDisplayName(it), getNextDisplayName(it));
            } else if (size != 3) {
                str = context.getString(getManyMemberDescriptionResourceId(z), getNextDisplayName(it), getNextDisplayName(it), Integer.valueOf(set.size() - 2));
            } else {
                str = context.getString(getThreeMemberDescriptionResourceId(z), getNextDisplayName(it), getNextDisplayName(it), getNextDisplayName(it));
            }
            this.descriptionTextView.setText(str);
            return;
        }
        throw new IllegalArgumentException("Recipients must contain 1 or more entries");
    }

    private Recipient getNextRecipient(Iterator<CallParticipantListUpdate.Wrapper> it) {
        return it.next().getCallParticipant().getRecipient();
    }

    private String getNextDisplayName(Iterator<CallParticipantListUpdate.Wrapper> it) {
        return it.next().getCallParticipant().getRecipientDisplayName(getContentView().getContext());
    }
}
