package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import org.thoughtcrime.securesms.components.emoji.EmojiEditText;

/* loaded from: classes4.dex */
public class SelectionAwareEmojiEditText extends EmojiEditText {
    private OnSelectionChangedListener onSelectionChangedListener;

    /* loaded from: classes4.dex */
    public interface OnSelectionChangedListener {
        void onSelectionChanged(int i, int i2);
    }

    public SelectionAwareEmojiEditText(Context context) {
        super(context);
    }

    public SelectionAwareEmojiEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public SelectionAwareEmojiEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setOnSelectionChangedListener(OnSelectionChangedListener onSelectionChangedListener) {
        this.onSelectionChangedListener = onSelectionChangedListener;
    }

    @Override // android.widget.TextView
    protected void onSelectionChanged(int i, int i2) {
        OnSelectionChangedListener onSelectionChangedListener = this.onSelectionChangedListener;
        if (onSelectionChangedListener != null) {
            onSelectionChangedListener.onSelectionChanged(i, i2);
        }
        super.onSelectionChanged(i, i2);
    }
}
