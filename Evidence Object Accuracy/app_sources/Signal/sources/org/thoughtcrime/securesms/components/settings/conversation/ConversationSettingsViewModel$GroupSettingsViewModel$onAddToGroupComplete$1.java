package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsEvent;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.groups.v2.GroupAddMembersResult;

/* compiled from: ConversationSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroupComplete$1 extends Lambda implements Function1<GroupAddMembersResult, Unit> {
    final /* synthetic */ Function0<Unit> $onComplete;
    final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroupComplete$1(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel, Function0<Unit> function0) {
        super(1);
        this.this$0 = groupSettingsViewModel;
        this.$onComplete = function0;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GroupAddMembersResult groupAddMembersResult) {
        invoke(groupAddMembersResult);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m1148invoke$lambda0(Function0 function0) {
        Intrinsics.checkNotNullParameter(function0, "$onComplete");
        function0.invoke();
    }

    public final void invoke(GroupAddMembersResult groupAddMembersResult) {
        Intrinsics.checkNotNullParameter(groupAddMembersResult, "it");
        ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroupComplete$1$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroupComplete$1.m1148invoke$lambda0(Function0.this);
            }
        });
        if (groupAddMembersResult instanceof GroupAddMembersResult.Success) {
            GroupAddMembersResult.Success success = (GroupAddMembersResult.Success) groupAddMembersResult;
            if (!success.getNewMembersInvited().isEmpty()) {
                this.this$0.getInternalEvents().postValue(new ConversationSettingsEvent.ShowGroupInvitesSentDialog(success.getNewMembersInvited()));
            }
            if (success.getNumberOfMembersAdded() > 0) {
                this.this$0.getInternalEvents().postValue(new ConversationSettingsEvent.ShowMembersAdded(success.getNumberOfMembersAdded()));
            }
        } else if (groupAddMembersResult instanceof GroupAddMembersResult.Failure) {
            this.this$0.getInternalEvents().postValue(new ConversationSettingsEvent.ShowAddMembersToGroupError(((GroupAddMembersResult.Failure) groupAddMembersResult).getReason()));
        }
    }
}
