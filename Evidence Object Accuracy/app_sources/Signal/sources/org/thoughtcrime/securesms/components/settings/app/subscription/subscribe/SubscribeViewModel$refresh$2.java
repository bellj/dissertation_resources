package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$refresh$2 extends Lambda implements Function1<Throwable, Unit> {
    public static final SubscribeViewModel$refresh$2 INSTANCE = new SubscribeViewModel$refresh$2();

    SubscribeViewModel$refresh$2() {
        super(1);
    }

    public final void invoke(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "it");
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }
}
