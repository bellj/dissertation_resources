package org.thoughtcrime.securesms.components.settings.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.models.TextInput;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class TextInput$MultilineViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ TextInput.MultilineModel f$0;
    public final /* synthetic */ TextInput.MultilineViewHolder f$1;

    public /* synthetic */ TextInput$MultilineViewHolder$$ExternalSyntheticLambda1(TextInput.MultilineModel multilineModel, TextInput.MultilineViewHolder multilineViewHolder) {
        this.f$0 = multilineModel;
        this.f$1 = multilineViewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        TextInput.MultilineViewHolder.m1262bind$lambda3(this.f$0, this.f$1, view);
    }
}
