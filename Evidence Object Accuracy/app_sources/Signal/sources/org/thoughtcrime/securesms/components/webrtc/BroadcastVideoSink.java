package org.thoughtcrime.securesms.components.webrtc;

import android.graphics.Point;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.util.Objects;
import java.util.WeakHashMap;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSink;

/* loaded from: classes4.dex */
public class BroadcastVideoSink implements VideoSink {
    public static final int DEVICE_ROTATION_IGNORE;
    private RequestedSize currentlyRequestedMaxSize;
    private int deviceOrientationDegrees;
    private final EglBaseWrapper eglBase;
    private boolean forceRotate;
    private final WeakHashMap<Object, Point> requestingSizes;
    private boolean rotateToRightSide;
    private boolean rotateWithDevice;
    private final WeakHashMap<VideoSink, Boolean> sinks;

    public BroadcastVideoSink() {
        this(new EglBaseWrapper(), false, true, 0);
    }

    public BroadcastVideoSink(EglBaseWrapper eglBaseWrapper, boolean z, boolean z2, int i) {
        this.eglBase = eglBaseWrapper;
        this.sinks = new WeakHashMap<>();
        this.requestingSizes = new WeakHashMap<>();
        this.deviceOrientationDegrees = i;
        this.rotateToRightSide = false;
        this.forceRotate = z;
        this.rotateWithDevice = z2;
    }

    public EglBaseWrapper getLockableEglBase() {
        return this.eglBase;
    }

    public synchronized void addSink(VideoSink videoSink) {
        this.sinks.put(videoSink, Boolean.TRUE);
    }

    public synchronized void removeSink(VideoSink videoSink) {
        this.sinks.remove(videoSink);
    }

    public void setForceRotate(boolean z) {
        this.forceRotate = z;
    }

    public void setRotateWithDevice(boolean z) {
        this.rotateWithDevice = z;
    }

    public void setRotateToRightSide(boolean z) {
        this.rotateToRightSide = z;
    }

    public void setDeviceOrientationDegrees(int i) {
        this.deviceOrientationDegrees = i;
    }

    @Override // org.webrtc.VideoSink
    public synchronized void onFrame(VideoFrame videoFrame) {
        int calculateRotation;
        boolean z = true;
        int i = 0;
        boolean z2 = this.deviceOrientationDegrees == -1;
        if (videoFrame.getRotatedHeight() >= videoFrame.getRotatedWidth()) {
            z = false;
        }
        if (!z2 && ((z || this.forceRotate) && (calculateRotation = calculateRotation()) > 0)) {
            if (this.rotateWithDevice) {
                i = videoFrame.getRotation();
            }
            videoFrame = new VideoFrame(videoFrame.getBuffer(), (calculateRotation + i) % 360, videoFrame.getTimestampNs());
        }
        for (VideoSink videoSink : this.sinks.keySet()) {
            videoSink.onFrame(videoFrame);
        }
    }

    private int calculateRotation() {
        int i;
        boolean z = this.forceRotate;
        if (z && ((i = this.deviceOrientationDegrees) == 0 || i == 180)) {
            return 0;
        }
        if (this.rotateWithDevice) {
            if (z) {
                return this.deviceOrientationDegrees;
            }
            int i2 = this.deviceOrientationDegrees;
            if (i2 == 0 || i2 == 180) {
                return SubsamplingScaleImageView.ORIENTATION_270;
            }
            return i2;
        } else if (this.rotateToRightSide) {
            return 90;
        } else {
            return SubsamplingScaleImageView.ORIENTATION_270;
        }
    }

    public void putRequestingSize(Object obj, Point point) {
        if (point.x != 0 && point.y != 0) {
            synchronized (this.requestingSizes) {
                this.requestingSizes.put(obj, point);
            }
        }
    }

    public void removeRequestingSize(Object obj) {
        synchronized (this.requestingSizes) {
            this.requestingSizes.remove(obj);
        }
    }

    public RequestedSize getMaxRequestingSize() {
        int i;
        int i2;
        synchronized (this.requestingSizes) {
            i = 0;
            i2 = 0;
            for (Point point : this.requestingSizes.values()) {
                int i3 = point.x;
                if (i < i3) {
                    i2 = point.y;
                    i = i3;
                }
            }
        }
        return new RequestedSize(i, i2);
    }

    public void setCurrentlyRequestedMaxSize(RequestedSize requestedSize) {
        this.currentlyRequestedMaxSize = requestedSize;
    }

    public boolean needsNewRequestingSize() {
        return !getMaxRequestingSize().equals(this.currentlyRequestedMaxSize);
    }

    /* loaded from: classes4.dex */
    public static final class RequestedSize {
        private final int height;
        private final int width;

        private RequestedSize(int i, int i2) {
            this.width = i;
            this.height = i2;
        }

        public int getWidth() {
            return this.width;
        }

        public int getHeight() {
            return this.height;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || RequestedSize.class != obj.getClass()) {
                return false;
            }
            RequestedSize requestedSize = (RequestedSize) obj;
            if (this.width == requestedSize.width && this.height == requestedSize.height) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return Objects.hash(Integer.valueOf(this.width), Integer.valueOf(this.height));
        }
    }
}
