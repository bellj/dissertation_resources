package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Space;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;

/* loaded from: classes4.dex */
public final class ReminderView extends FrameLayout {
    private OnActionClickListener actionClickListener;
    private RecyclerView actionsRecycler;
    private View background;
    private ImageButton closeButton;
    private ViewGroup container;
    private OnDismissListener dismissListener;
    private ProgressBar progressBar;
    private TextView progressText;
    private Space space;
    private TextView text;
    private TextView title;

    /* loaded from: classes4.dex */
    public interface OnActionClickListener {
        void onActionClick(int i);
    }

    /* loaded from: classes4.dex */
    public interface OnDismissListener {
        void onDismiss();
    }

    public ReminderView(Context context) {
        super(context);
        initialize();
    }

    public ReminderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public ReminderView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    private void initialize() {
        LayoutInflater.from(getContext()).inflate(R.layout.reminder_header, (ViewGroup) this, true);
        this.progressBar = (ProgressBar) findViewById(R.id.reminder_progress);
        this.progressText = (TextView) findViewById(R.id.reminder_progress_text);
        this.container = (ViewGroup) findViewById(R.id.container);
        this.background = findViewById(R.id.background);
        this.closeButton = (ImageButton) findViewById(R.id.cancel);
        this.title = (TextView) findViewById(R.id.reminder_title);
        this.text = (TextView) findViewById(R.id.reminder_text);
        this.space = (Space) findViewById(R.id.reminder_space);
        this.actionsRecycler = (RecyclerView) findViewById(R.id.reminder_actions);
    }

    public void showReminder(final Reminder reminder) {
        if (!TextUtils.isEmpty(reminder.getTitle())) {
            this.title.setText(reminder.getTitle());
            this.title.setVisibility(0);
            this.space.setVisibility(8);
        } else {
            this.title.setText("");
            this.title.setVisibility(8);
            this.space.setVisibility(0);
        }
        if (!reminder.isDismissable()) {
            this.space.setVisibility(8);
        }
        this.text.setText(reminder.getText());
        int i = AnonymousClass2.$SwitchMap$org$thoughtcrime$securesms$components$reminder$Reminder$Importance[reminder.getImportance().ordinal()];
        if (i == 1) {
            this.background.setBackgroundResource(R.drawable.reminder_background_normal);
            this.title.setTextColor(ContextCompat.getColor(getContext(), R.color.signal_text_primary));
            this.text.setTextColor(ContextCompat.getColor(getContext(), R.color.signal_text_primary));
        } else if (i == 2) {
            this.background.setBackgroundResource(R.drawable.reminder_background_error);
            this.title.setTextColor(ContextCompat.getColor(getContext(), R.color.core_black));
            this.text.setTextColor(ContextCompat.getColor(getContext(), R.color.core_black));
        } else if (i == 3) {
            this.background.setBackgroundResource(R.drawable.reminder_background_terminal);
            this.title.setTextColor(ContextCompat.getColor(getContext(), R.color.signal_button_primary_text));
            this.text.setTextColor(ContextCompat.getColor(getContext(), R.color.signal_button_primary_text));
        } else {
            throw new IllegalStateException();
        }
        if (reminder.getOkListener() != null) {
            setOnClickListener(reminder.getOkListener());
        }
        this.closeButton.setVisibility(reminder.isDismissable() ? 0 : 8);
        this.closeButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.reminder.ReminderView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ReminderView.this.hide();
                if (reminder.getDismissListener() != null) {
                    reminder.getDismissListener().onClick(view);
                }
                if (ReminderView.this.dismissListener != null) {
                    ReminderView.this.dismissListener.onDismiss();
                }
            }
        });
        if (reminder.getImportance() == Reminder.Importance.NORMAL) {
            this.closeButton.setColorFilter(ContextCompat.getColor(getContext(), R.color.signal_text_primary));
        }
        int progress = reminder.getProgress();
        if (progress != -1) {
            this.progressBar.setProgress(progress);
            this.progressBar.setVisibility(0);
            this.progressText.setText(getContext().getString(R.string.reminder_header_progress, Integer.valueOf(progress)));
            this.progressText.setVisibility(0);
        } else {
            this.progressBar.setVisibility(8);
            this.progressText.setVisibility(8);
        }
        List<Reminder.Action> actions = reminder.getActions();
        if (actions.isEmpty()) {
            this.text.setPadding(0, 0, 0, (int) DimensionUnit.DP.toPixels(16.0f));
            this.actionsRecycler.setVisibility(8);
        } else {
            this.text.setPadding(0, 0, 0, 0);
            this.actionsRecycler.setVisibility(0);
            this.actionsRecycler.setAdapter(new ReminderActionsAdapter(reminder.getImportance(), actions, new OnActionClickListener() { // from class: org.thoughtcrime.securesms.components.reminder.ReminderView$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.components.reminder.ReminderView.OnActionClickListener
                public final void onActionClick(int i2) {
                    ReminderView.this.handleActionClicked(i2);
                }
            }));
        }
        this.container.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: org.thoughtcrime.securesms.components.reminder.ReminderView$2 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$components$reminder$Reminder$Importance;

        static {
            int[] iArr = new int[Reminder.Importance.values().length];
            $SwitchMap$org$thoughtcrime$securesms$components$reminder$Reminder$Importance = iArr;
            try {
                iArr[Reminder.Importance.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$reminder$Reminder$Importance[Reminder.Importance.ERROR.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$reminder$Reminder$Importance[Reminder.Importance.TERMINAL.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public void handleActionClicked(int i) {
        OnActionClickListener onActionClickListener = this.actionClickListener;
        if (onActionClickListener != null) {
            onActionClickListener.onActionClick(i);
        }
    }

    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.dismissListener = onDismissListener;
    }

    public void setOnActionClickListener(OnActionClickListener onActionClickListener) {
        this.actionClickListener = onActionClickListener;
    }

    public void requestDismiss() {
        this.closeButton.performClick();
    }

    public void hide() {
        this.container.setVisibility(8);
    }
}
