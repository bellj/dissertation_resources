package org.thoughtcrime.securesms.components.registration;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;

/* loaded from: classes4.dex */
public class PulsingFloatingActionButton extends FloatingActionButton {
    private boolean pulsing;

    public PulsingFloatingActionButton(Context context) {
        super(context);
    }

    public PulsingFloatingActionButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PulsingFloatingActionButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void startPulse(long j) {
        if (!this.pulsing) {
            this.pulsing = true;
            pulse(j);
        }
    }

    public void stopPulse() {
        this.pulsing = false;
    }

    public void pulse(final long j) {
        if (this.pulsing) {
            animate().scaleX(1.2f).scaleY(1.2f).setDuration(150).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.registration.PulsingFloatingActionButton.1
                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    PulsingFloatingActionButton.this.clearAnimation();
                    PulsingFloatingActionButton.this.animate().scaleX(1.0f).scaleY(1.0f).setDuration(150).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.registration.PulsingFloatingActionButton.1.1
                        public /* synthetic */ void lambda$onAnimationEnd$0(long j2) {
                            PulsingFloatingActionButton.this.pulse(j2);
                        }

                        @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                        public void onAnimationEnd(Animator animator2) {
                            AnonymousClass1 r4 = AnonymousClass1.this;
                            PulsingFloatingActionButton.this.postDelayed(new PulsingFloatingActionButton$1$1$$ExternalSyntheticLambda0(this, j), j);
                        }
                    }).start();
                }
            }).start();
        }
    }
}
