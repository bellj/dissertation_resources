package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: CustomExpireTimerSelectorView.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0001\u0013B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\f\u001a\u00020\u0007J\u0015\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\u0010J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectorView;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "unitPicker", "Landroid/widget/NumberPicker;", "valuePicker", "getTimer", "setTimer", "", "timer", "(Ljava/lang/Integer;)V", "unitChange", "newValue", "TimerUnit", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomExpireTimerSelectorView extends LinearLayout {
    private final NumberPicker unitPicker;
    private final NumberPicker valuePicker;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public CustomExpireTimerSelectorView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public CustomExpireTimerSelectorView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ CustomExpireTimerSelectorView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CustomExpireTimerSelectorView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        setOrientation(0);
        setGravity(17);
        LinearLayout.inflate(context, R.layout.custom_expire_timer_selector_view, this);
        View findViewById = findViewById(R.id.custom_expire_timer_selector_value);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.custom…ire_timer_selector_value)");
        NumberPicker numberPicker = (NumberPicker) findViewById;
        this.valuePicker = numberPicker;
        View findViewById2 = findViewById(R.id.custom_expire_timer_selector_unit);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.custom…pire_timer_selector_unit)");
        NumberPicker numberPicker2 = (NumberPicker) findViewById2;
        this.unitPicker = numberPicker2;
        TimerUnit.Companion companion = TimerUnit.Companion;
        numberPicker.setMinValue(companion.get(1).getMinValue());
        numberPicker.setMaxValue(companion.get(1).getMaxValue());
        numberPicker2.setMinValue(0);
        numberPicker2.setMaxValue(4);
        numberPicker2.setValue(1);
        numberPicker2.setWrapSelectorWheel(false);
        numberPicker2.setLongClickable(false);
        numberPicker2.setDisplayedValues(context.getResources().getStringArray(R.array.CustomExpireTimerSelectorView__unit_labels));
        numberPicker2.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.CustomExpireTimerSelectorView$$ExternalSyntheticLambda0
            @Override // android.widget.NumberPicker.OnValueChangeListener
            public final void onValueChange(NumberPicker numberPicker3, int i2, int i3) {
                CustomExpireTimerSelectorView.m859_init_$lambda0(CustomExpireTimerSelectorView.this, numberPicker3, i2, i3);
            }
        });
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m859_init_$lambda0(CustomExpireTimerSelectorView customExpireTimerSelectorView, NumberPicker numberPicker, int i, int i2) {
        Intrinsics.checkNotNullParameter(customExpireTimerSelectorView, "this$0");
        customExpireTimerSelectorView.unitChange(i2);
    }

    public final void setTimer(Integer num) {
        TimerUnit timerUnit;
        if (num != null && num.intValue() != 0) {
            TimerUnit[] values = TimerUnit.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    timerUnit = null;
                    break;
                }
                timerUnit = values[i];
                if (((long) num.intValue()) / timerUnit.getValueMultiplier() < ((long) timerUnit.getMaxValue())) {
                    break;
                }
                i++;
            }
            if (timerUnit != null) {
                this.valuePicker.setValue((int) (((long) num.intValue()) / timerUnit.getValueMultiplier()));
                this.unitPicker.setValue(ArraysKt___ArraysKt.indexOf(TimerUnit.values(), timerUnit));
                unitChange(this.unitPicker.getValue());
            }
        }
    }

    public final int getTimer() {
        return this.valuePicker.getValue() * ((int) TimerUnit.Companion.get(this.unitPicker.getValue()).getValueMultiplier());
    }

    private final void unitChange(int i) {
        TimerUnit timerUnit = TimerUnit.values()[i];
        this.valuePicker.setMinValue(timerUnit.getMinValue());
        this.valuePicker.setMaxValue(timerUnit.getMaxValue());
    }

    /* compiled from: CustomExpireTimerSelectorView.kt */
    /* JADX WARN: Init of enum DAYS can be incorrect */
    /* JADX WARN: Init of enum WEEKS can be incorrect */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\r\b\u0001\u0018\u0000 \u00122\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0012B\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectorView$TimerUnit;", "", "minValue", "", "maxValue", "valueMultiplier", "", "(Ljava/lang/String;IIIJ)V", "getMaxValue", "()I", "getMinValue", "getValueMultiplier", "()J", "SECONDS", "MINUTES", "HOURS", "DAYS", "WEEKS", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum TimerUnit {
        SECONDS(1, 59, TimeUnit.SECONDS.toSeconds(1)),
        MINUTES(1, 59, TimeUnit.MINUTES.toSeconds(1)),
        HOURS(1, 23, TimeUnit.HOURS.toSeconds(1)),
        DAYS(1, 6, r1.toSeconds(1)),
        WEEKS(1, 4, r1.toSeconds(7));
        
        public static final Companion Companion = new Companion(null);
        private final int maxValue;
        private final int minValue;
        private final long valueMultiplier;

        TimerUnit(int i, int i2, long j) {
            this.minValue = i;
            this.maxValue = i2;
            this.valueMultiplier = j;
        }

        public final int getMaxValue() {
            return this.maxValue;
        }

        public final int getMinValue() {
            return this.minValue;
        }

        public final long getValueMultiplier() {
            return this.valueMultiplier;
        }

        static {
            TimeUnit timeUnit = TimeUnit.DAYS;
            Companion = new Companion(null);
        }

        /* compiled from: CustomExpireTimerSelectorView.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectorView$TimerUnit$Companion;", "", "()V", "get", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectorView$TimerUnit;", DraftDatabase.DRAFT_VALUE, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final TimerUnit get(int i) {
                return TimerUnit.values()[i];
            }
        }
    }
}
