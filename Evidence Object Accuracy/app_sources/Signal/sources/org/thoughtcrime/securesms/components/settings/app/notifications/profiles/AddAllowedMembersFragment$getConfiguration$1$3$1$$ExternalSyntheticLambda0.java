package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$getConfiguration$1;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AddAllowedMembersFragment$getConfiguration$1$3$1$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AddAllowedMembersFragment f$0;
    public final /* synthetic */ RecipientId f$1;

    public /* synthetic */ AddAllowedMembersFragment$getConfiguration$1$3$1$$ExternalSyntheticLambda0(AddAllowedMembersFragment addAllowedMembersFragment, RecipientId recipientId) {
        this.f$0 = addAllowedMembersFragment;
        this.f$1 = recipientId;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AddAllowedMembersFragment$getConfiguration$1.AnonymousClass3.AnonymousClass1.m728invoke$lambda1$lambda0(this.f$0, this.f$1, view);
    }
}
