package org.thoughtcrime.securesms.components.settings.conversation.permissions;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class PermissionsSettingsFragmentArgs {
    private final HashMap arguments;

    private PermissionsSettingsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private PermissionsSettingsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static PermissionsSettingsFragmentArgs fromBundle(Bundle bundle) {
        PermissionsSettingsFragmentArgs permissionsSettingsFragmentArgs = new PermissionsSettingsFragmentArgs();
        bundle.setClassLoader(PermissionsSettingsFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("group_id")) {
            throw new IllegalArgumentException("Required argument \"group_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(Parcelable.class) || Serializable.class.isAssignableFrom(Parcelable.class)) {
            permissionsSettingsFragmentArgs.arguments.put("group_id", (Parcelable) bundle.get("group_id"));
            return permissionsSettingsFragmentArgs;
        } else {
            throw new UnsupportedOperationException(Parcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public Parcelable getGroupId() {
        return (Parcelable) this.arguments.get("group_id");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("group_id")) {
            Parcelable parcelable = (Parcelable) this.arguments.get("group_id");
            if (Parcelable.class.isAssignableFrom(Parcelable.class) || parcelable == null) {
                bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelable));
            } else if (Serializable.class.isAssignableFrom(Parcelable.class)) {
                bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelable));
            } else {
                throw new UnsupportedOperationException(Parcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        PermissionsSettingsFragmentArgs permissionsSettingsFragmentArgs = (PermissionsSettingsFragmentArgs) obj;
        if (this.arguments.containsKey("group_id") != permissionsSettingsFragmentArgs.arguments.containsKey("group_id")) {
            return false;
        }
        return getGroupId() == null ? permissionsSettingsFragmentArgs.getGroupId() == null : getGroupId().equals(permissionsSettingsFragmentArgs.getGroupId());
    }

    public int hashCode() {
        return 31 + (getGroupId() != null ? getGroupId().hashCode() : 0);
    }

    public String toString() {
        return "PermissionsSettingsFragmentArgs{groupId=" + getGroupId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(PermissionsSettingsFragmentArgs permissionsSettingsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(permissionsSettingsFragmentArgs.arguments);
        }

        public Builder(Parcelable parcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("group_id", parcelable);
        }

        public PermissionsSettingsFragmentArgs build() {
            return new PermissionsSettingsFragmentArgs(this.arguments);
        }

        public Builder setGroupId(Parcelable parcelable) {
            this.arguments.put("group_id", parcelable);
            return this;
        }

        public Parcelable getGroupId() {
            return (Parcelable) this.arguments.get("group_id");
        }
    }
}
