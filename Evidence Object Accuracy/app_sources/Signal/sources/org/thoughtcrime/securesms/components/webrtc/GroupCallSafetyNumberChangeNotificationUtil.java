package org.thoughtcrime.securesms.components.webrtc;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.WebRtcCallActivity;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class GroupCallSafetyNumberChangeNotificationUtil {
    public static final String GROUP_CALLING_NOTIFICATION_TAG;

    private GroupCallSafetyNumberChangeNotificationUtil() {
    }

    public static void showNotification(Context context, Recipient recipient) {
        Intent intent = new Intent(context, WebRtcCallActivity.class);
        intent.setFlags(SQLiteDatabase.ENABLE_WRITE_AHEAD_LOGGING);
        NotificationManagerCompat.from(context).notify(GROUP_CALLING_NOTIFICATION_TAG, recipient.hashCode(), new NotificationCompat.Builder(context, NotificationChannels.CALLS).setSmallIcon(R.drawable.ic_notification).setContentTitle(recipient.getDisplayName(context)).setContentText(context.getString(R.string.GroupCallSafetyNumberChangeNotification__someone_has_joined_this_call_with_a_safety_number_that_has_changed)).setStyle(new NotificationCompat.BigTextStyle().bigText(context.getString(R.string.GroupCallSafetyNumberChangeNotification__someone_has_joined_this_call_with_a_safety_number_that_has_changed))).setContentIntent(PendingIntent.getActivity(context, 0, intent, 0)).build());
    }

    public static void cancelNotification(Context context, Recipient recipient) {
        NotificationManagerCompat.from(context).cancel(GROUP_CALLING_NOTIFICATION_TAG, recipient.hashCode());
    }
}
