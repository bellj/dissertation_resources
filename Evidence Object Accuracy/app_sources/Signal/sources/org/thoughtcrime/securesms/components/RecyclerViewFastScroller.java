package org.thoughtcrime.securesms.components;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class RecyclerViewFastScroller extends LinearLayout {
    private static final int BUBBLE_ANIMATION_DURATION;
    private static final int TRACK_SNAP_RANGE;
    private final TextView bubble;
    private ObjectAnimator currentAnimator;
    private final View handle;
    private int height;
    private final RecyclerView.OnScrollListener onScrollListener;
    private RecyclerView recyclerView;

    /* loaded from: classes4.dex */
    public interface FastScrollAdapter {
        CharSequence getBubbleText(int i);
    }

    public RecyclerViewFastScroller(Context context) {
        this(context, null);
    }

    public RecyclerViewFastScroller(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.onScrollListener = new RecyclerView.OnScrollListener() { // from class: org.thoughtcrime.securesms.components.RecyclerViewFastScroller.1
            @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                if (!RecyclerViewFastScroller.this.handle.isSelected()) {
                    int computeVerticalScrollOffset = recyclerView.computeVerticalScrollOffset();
                    int max = Math.max(recyclerView.computeVerticalScrollRange() - recyclerView.computeVerticalScrollExtent(), 1);
                    RecyclerViewFastScroller.this.setBubbleAndHandlePosition(((float) Util.clamp(computeVerticalScrollOffset, 0, max)) / ((float) max));
                }
            }
        };
        setOrientation(0);
        setClipChildren(false);
        setScrollContainer(true);
        LinearLayout.inflate(context, R.layout.recycler_view_fast_scroller, this);
        this.bubble = (TextView) findViewById(R.id.fastscroller_bubble);
        this.handle = findViewById(R.id.fastscroller_handle);
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.height = i2;
    }

    @Override // android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action != 2) {
                    if (action != 3) {
                        return super.onTouchEvent(motionEvent);
                    }
                }
            }
            this.handle.setSelected(false);
            hideBubble();
            return true;
        } else if (motionEvent.getX() < this.handle.getX() - ((float) this.handle.getPaddingLeft()) || motionEvent.getY() < this.handle.getY() - ((float) this.handle.getPaddingTop()) || motionEvent.getY() > this.handle.getY() + ((float) this.handle.getHeight()) + ((float) this.handle.getPaddingBottom())) {
            return false;
        } else {
            ObjectAnimator objectAnimator = this.currentAnimator;
            if (objectAnimator != null) {
                objectAnimator.cancel();
            }
            if (this.bubble.getVisibility() != 0) {
                showBubble();
            }
            this.handle.setSelected(true);
        }
        float y = motionEvent.getY();
        setBubbleAndHandlePosition(y / ((float) this.height));
        setRecyclerViewPosition(y);
        return true;
    }

    public void setRecyclerView(final RecyclerView recyclerView) {
        RecyclerView recyclerView2 = this.recyclerView;
        if (recyclerView2 != null) {
            recyclerView2.removeOnScrollListener(this.onScrollListener);
        }
        this.recyclerView = recyclerView;
        if (recyclerView != null) {
            recyclerView.addOnScrollListener(this.onScrollListener);
            recyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() { // from class: org.thoughtcrime.securesms.components.RecyclerViewFastScroller.2
                @Override // android.view.ViewTreeObserver.OnPreDrawListener
                public boolean onPreDraw() {
                    recyclerView.getViewTreeObserver().removeOnPreDrawListener(this);
                    if (RecyclerViewFastScroller.this.handle.isSelected()) {
                        return true;
                    }
                    float computeVerticalScrollOffset = ((float) recyclerView.computeVerticalScrollOffset()) / (((float) recyclerView.computeVerticalScrollRange()) - ((float) RecyclerViewFastScroller.this.height));
                    RecyclerViewFastScroller recyclerViewFastScroller = RecyclerViewFastScroller.this;
                    recyclerViewFastScroller.setBubbleAndHandlePosition(((float) recyclerViewFastScroller.height) * computeVerticalScrollOffset);
                    return true;
                }
            });
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        RecyclerView recyclerView = this.recyclerView;
        if (recyclerView != null) {
            recyclerView.removeOnScrollListener(this.onScrollListener);
        }
    }

    private void setRecyclerViewPosition(float f) {
        RecyclerView recyclerView = this.recyclerView;
        if (recyclerView != null) {
            int itemCount = recyclerView.getAdapter().getItemCount();
            float f2 = 0.0f;
            if (this.handle.getY() != 0.0f) {
                float y = this.handle.getY() + ((float) this.handle.getHeight());
                int i = this.height;
                f2 = y >= ((float) (i + -5)) ? 1.0f : f / ((float) i);
            }
            int clamp = Util.clamp((int) (f2 * ((float) itemCount)), 0, itemCount - 1);
            ((LinearLayoutManager) this.recyclerView.getLayoutManager()).scrollToPositionWithOffset(clamp, 0);
            this.bubble.setText(((FastScrollAdapter) this.recyclerView.getAdapter()).getBubbleText(clamp));
        }
    }

    public void setBubbleAndHandlePosition(float f) {
        int height = this.handle.getHeight();
        int height2 = this.bubble.getHeight();
        int i = this.height;
        int clamp = Util.clamp((int) (((float) (i - height)) * f), 0, i - height);
        this.handle.setY((float) clamp);
        TextView textView = this.bubble;
        textView.setY((float) Util.clamp(((clamp - height2) - textView.getPaddingBottom()) + height, 0, this.height - height2));
    }

    private void showBubble() {
        this.bubble.setVisibility(0);
        ObjectAnimator objectAnimator = this.currentAnimator;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.bubble, "alpha", 0.0f, 1.0f).setDuration(100L);
        this.currentAnimator = duration;
        duration.start();
    }

    private void hideBubble() {
        ObjectAnimator objectAnimator = this.currentAnimator;
        if (objectAnimator != null) {
            objectAnimator.cancel();
        }
        ObjectAnimator duration = ObjectAnimator.ofFloat(this.bubble, "alpha", 1.0f, 0.0f).setDuration(100L);
        this.currentAnimator = duration;
        duration.addListener(new AnimatorListenerAdapter() { // from class: org.thoughtcrime.securesms.components.RecyclerViewFastScroller.3
            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                super.onAnimationEnd(animator);
                RecyclerViewFastScroller.this.bubble.setVisibility(4);
                RecyclerViewFastScroller.this.currentAnimator = null;
            }

            @Override // android.animation.AnimatorListenerAdapter, android.animation.Animator.AnimatorListener
            public void onAnimationCancel(Animator animator) {
                super.onAnimationCancel(animator);
                RecyclerViewFastScroller.this.bubble.setVisibility(4);
                RecyclerViewFastScroller.this.currentAnimator = null;
            }
        });
        this.currentAnimator.start();
    }
}
