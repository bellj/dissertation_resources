package org.thoughtcrime.securesms.components.settings.app.chats;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ChatsSettingsFragmentDirections {
    private ChatsSettingsFragmentDirections() {
    }

    public static NavDirections actionChatsSettingsFragmentToBackupsPreferenceFragment() {
        return new ActionOnlyNavDirections(R.id.action_chatsSettingsFragment_to_backupsPreferenceFragment);
    }

    public static NavDirections actionChatsSettingsFragmentToSmsSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_chatsSettingsFragment_to_smsSettingsFragment);
    }

    public static NavDirections actionChatsSettingsFragmentToEditReactionsFragment() {
        return new ActionOnlyNavDirections(R.id.action_chatsSettingsFragment_to_editReactionsFragment);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }
}
