package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\b&\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00028\u0000H\u0017¢\u0006\u0002\u0010\u0015R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\u00020\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\u00020\rX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000f¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "T", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "iconEndView", "Landroid/widget/ImageView;", "iconView", "getIconView", "()Landroid/widget/ImageView;", "summaryView", "Landroid/widget/TextView;", "getSummaryView", "()Landroid/widget/TextView;", "titleView", "getTitleView", "bind", "", "model", "(Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class PreferenceViewHolder<T extends PreferenceModel<T>> extends MappingViewHolder<T> {
    private final ImageView iconEndView;
    private final ImageView iconView;
    private final TextView summaryView;
    private final TextView titleView;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
        View findViewById = view.findViewById(R.id.icon);
        Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.icon)");
        this.iconView = (ImageView) findViewById;
        this.iconEndView = (ImageView) view.findViewById(R.id.icon_end);
        View findViewById2 = view.findViewById(R.id.title);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.title)");
        this.titleView = (TextView) findViewById2;
        View findViewById3 = view.findViewById(R.id.summary);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.summary)");
        this.summaryView = (TextView) findViewById3;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.PreferenceViewHolder<T extends org.thoughtcrime.securesms.components.settings.PreferenceModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
    public /* bridge */ /* synthetic */ void bind(Object obj) {
        bind((PreferenceViewHolder<T>) ((PreferenceModel) obj));
    }

    public final ImageView getIconView() {
        return this.iconView;
    }

    public final TextView getTitleView() {
        return this.titleView;
    }

    public final TextView getSummaryView() {
        return this.summaryView;
    }

    public void bind(T t) {
        Drawable drawable;
        Drawable drawable2;
        CharSequence charSequence;
        CharSequence charSequence2;
        CharSequence charSequence3;
        Intrinsics.checkNotNullParameter(t, "model");
        boolean z = false;
        for (View view : CollectionsKt__CollectionsKt.listOf((Object[]) new View[]{this.itemView, this.titleView, this.summaryView})) {
            view.setEnabled(t.isEnabled());
        }
        DSLSettingsIcon icon = t.getIcon();
        if (icon != null) {
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            drawable = icon.resolve(context);
        } else {
            drawable = null;
        }
        this.iconView.setImageDrawable(drawable);
        ViewExtensionsKt.setVisible(this.iconView, drawable != null);
        DSLSettingsIcon iconEnd = t.getIconEnd();
        if (iconEnd != null) {
            Context context2 = this.context;
            Intrinsics.checkNotNullExpressionValue(context2, "context");
            drawable2 = iconEnd.resolve(context2);
        } else {
            drawable2 = null;
        }
        ImageView imageView = this.iconEndView;
        if (imageView != null) {
            imageView.setImageDrawable(drawable2);
        }
        ImageView imageView2 = this.iconEndView;
        if (imageView2 != null) {
            ViewExtensionsKt.setVisible(imageView2, drawable2 != null);
        }
        DSLSettingsText title = t.getTitle();
        if (title != null) {
            Context context3 = this.context;
            Intrinsics.checkNotNullExpressionValue(context3, "context");
            charSequence = title.resolve(context3);
        } else {
            charSequence = null;
        }
        if (charSequence != null) {
            TextView textView = this.titleView;
            DSLSettingsText title2 = t.getTitle();
            if (title2 != null) {
                Context context4 = this.context;
                Intrinsics.checkNotNullExpressionValue(context4, "context");
                charSequence3 = title2.resolve(context4);
            } else {
                charSequence3 = null;
            }
            textView.setText(charSequence3);
            this.titleView.setVisibility(0);
        } else {
            this.titleView.setVisibility(8);
        }
        DSLSettingsText summary = t.getSummary();
        if (summary != null) {
            Context context5 = this.context;
            Intrinsics.checkNotNullExpressionValue(context5, "context");
            charSequence2 = summary.resolve(context5);
        } else {
            charSequence2 = null;
        }
        if (charSequence2 != null) {
            this.summaryView.setText(charSequence2);
            this.summaryView.setVisibility(0);
            CharSequence text = this.summaryView.getText();
            Spanned spanned = text instanceof Spanned ? (Spanned) text : null;
            ClickableSpan[] clickableSpanArr = spanned != null ? (ClickableSpan[]) spanned.getSpans(0, this.summaryView.getText().length(), ClickableSpan.class) : null;
            if (clickableSpanArr != null) {
                if (!(clickableSpanArr.length == 0)) {
                    z = true;
                }
            }
            if (z) {
                this.summaryView.setMovementMethod(LinkMovementMethod.getInstance());
            } else {
                this.summaryView.setMovementMethod(null);
            }
        } else {
            this.summaryView.setVisibility(8);
            this.summaryView.setMovementMethod(null);
        }
    }
}
