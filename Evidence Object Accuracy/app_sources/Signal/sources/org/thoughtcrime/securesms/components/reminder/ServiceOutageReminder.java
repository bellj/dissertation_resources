package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class ServiceOutageReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    public ServiceOutageReminder(Context context) {
        super(null, context.getString(R.string.reminder_header_service_outage_text));
    }

    public static boolean isEligible(Context context) {
        return TextSecurePreferences.getServiceOutage(context);
    }

    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public Reminder.Importance getImportance() {
        return Reminder.Importance.ERROR;
    }
}
