package org.thoughtcrime.securesms.components.webrtc;

import android.os.Handler;
import android.os.Looper;
import android.util.Pair;
import androidx.arch.core.util.Function;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.signal.core.util.ThreadUtil;
import org.thoughtcrime.securesms.components.sensors.DeviceOrientationMonitor;
import org.thoughtcrime.securesms.components.sensors.Orientation;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsState;
import org.thoughtcrime.securesms.components.webrtc.WebRtcControls;
import org.thoughtcrime.securesms.database.identity.IdentityRecordList;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.NetworkUtil;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* loaded from: classes4.dex */
public class WebRtcCallViewModel extends ViewModel {
    private boolean answerWithVideoAvailable;
    private long callConnectedTime;
    private final SingleLiveEvent<CallParticipantListUpdate> callParticipantListUpdate;
    private boolean callStarting;
    private boolean canDisplayPopupIfNeeded;
    private boolean canDisplayTooltipIfNeeded;
    private boolean canEnterPipMode;
    private final LiveData<Integer> controlsRotation;
    private final LiveData<WebRtcControls> controlsWithFoldableState;
    private final MutableLiveData<Long> elapsed;
    private final Handler elapsedTimeHandler;
    private final Runnable elapsedTimeRunnable;
    private final MutableLiveData<WebRtcEphemeralState> ephemeralState;
    private final SingleLiveEvent<Event> events;
    private final MutableLiveData<WebRtcControls.FoldableState> foldableState;
    private final LiveData<Integer> groupMemberCount;
    private final Observer<List<GroupMemberEntry.FullMember>> groupMemberStateUpdater;
    private final LiveData<List<GroupMemberEntry.FullMember>> groupMembers;
    private final LiveData<List<GroupMemberEntry.FullMember>> groupMembersChanged;
    private final LiveData<Recipient> groupRecipient;
    private boolean hasEnabledLocalVideo;
    private final MutableLiveData<Collection<RecipientId>> identityChangedRecipients;
    private final MutableLiveData<Boolean> isInPipMode;
    private final MutableLiveData<Boolean> isLandscapeEnabled;
    private final MutableLiveData<LiveRecipient> liveRecipient;
    private final MutableLiveData<Boolean> microphoneEnabled;
    private final LiveData<Orientation> orientation;
    private final DefaultValueLiveData<CallParticipantsState> participantsState;
    private List<CallParticipant> previousParticipantsList;
    private final LiveData<WebRtcControls> realWebRtcControls;
    private final WebRtcCallRepository repository;
    private final LiveData<SafetyNumberChangeEvent> safetyNumberChangeEvent;
    private final LiveData<Boolean> shouldShowSpeakerHint;
    private boolean showScreenShareTip;
    private final Runnable stopOutgoingRingingMode;
    private boolean switchOnFirstScreenShare;
    private boolean wasInOutgoingRingingMode;
    private final MutableLiveData<WebRtcControls> webRtcControls;

    /* synthetic */ WebRtcCallViewModel(DeviceOrientationMonitor deviceOrientationMonitor, AnonymousClass1 r2) {
        this(deviceOrientationMonitor);
    }

    public static /* synthetic */ SafetyNumberChangeEvent lambda$new$0(boolean z, Collection collection) {
        return new SafetyNumberChangeEvent(z, collection, null);
    }

    public static /* synthetic */ LiveData lambda$new$1(Recipient recipient) {
        return Transformations.distinctUntilChanged(new LiveGroup(recipient.requireGroupId()).getFullMembers());
    }

    public /* synthetic */ void lambda$new$2(List list) {
        DefaultValueLiveData<CallParticipantsState> defaultValueLiveData = this.participantsState;
        defaultValueLiveData.setValue(CallParticipantsState.update(defaultValueLiveData.getValue(), list));
    }

    private WebRtcCallViewModel(DeviceOrientationMonitor deviceOrientationMonitor) {
        this.microphoneEnabled = new MutableLiveData<>(Boolean.TRUE);
        MutableLiveData<Boolean> mutableLiveData = new MutableLiveData<>(Boolean.FALSE);
        this.isInPipMode = mutableLiveData;
        MutableLiveData<WebRtcControls> mutableLiveData2 = new MutableLiveData<>(WebRtcControls.NONE);
        this.webRtcControls = mutableLiveData2;
        MutableLiveData<WebRtcControls.FoldableState> mutableLiveData3 = new MutableLiveData<>(WebRtcControls.FoldableState.flat());
        this.foldableState = mutableLiveData3;
        LiveData<WebRtcControls> combineLatest = LiveDataUtil.combineLatest(mutableLiveData3, mutableLiveData2, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return WebRtcCallViewModel.this.updateControlsFoldableState((WebRtcControls.FoldableState) obj, (WebRtcControls) obj2);
            }
        });
        this.controlsWithFoldableState = combineLatest;
        this.realWebRtcControls = LiveDataUtil.combineLatest(mutableLiveData, combineLatest, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return WebRtcCallViewModel.this.getRealWebRtcControls(((Boolean) obj).booleanValue(), (WebRtcControls) obj2);
            }
        });
        this.events = new SingleLiveEvent<>();
        this.elapsed = new MutableLiveData<>(-1L);
        MutableLiveData<LiveRecipient> mutableLiveData4 = new MutableLiveData<>(Recipient.UNKNOWN.live());
        this.liveRecipient = mutableLiveData4;
        DefaultValueLiveData<CallParticipantsState> defaultValueLiveData = new DefaultValueLiveData<>(CallParticipantsState.STARTING_STATE);
        this.participantsState = defaultValueLiveData;
        this.callParticipantListUpdate = new SingleLiveEvent<>();
        MutableLiveData<Collection<RecipientId>> mutableLiveData5 = new MutableLiveData<>(Collections.emptyList());
        this.identityChangedRecipients = mutableLiveData5;
        this.safetyNumberChangeEvent = LiveDataUtil.combineLatest(mutableLiveData, mutableLiveData5, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return WebRtcCallViewModel.lambda$new$0(((Boolean) obj).booleanValue(), (Collection) obj2);
            }
        });
        LiveData<Recipient> filter = LiveDataUtil.filter(Transformations.switchMap(mutableLiveData4, new WebRtcCallViewModel$$ExternalSyntheticLambda5()), new Predicate() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return ((Recipient) obj).isActiveGroup();
            }
        });
        this.groupRecipient = filter;
        LiveData<List<GroupMemberEntry.FullMember>> switchMap = Transformations.switchMap(filter, new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda7
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return WebRtcCallViewModel.lambda$new$1((Recipient) obj);
            }
        });
        this.groupMembers = switchMap;
        this.groupMembersChanged = LiveDataUtil.skip(switchMap, 1);
        this.groupMemberCount = Transformations.map(switchMap, new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda8
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Integer.valueOf(((List) obj).size());
            }
        });
        this.shouldShowSpeakerHint = Transformations.map(defaultValueLiveData, new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda9
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return Boolean.valueOf(WebRtcCallViewModel.this.shouldShowSpeakerHint((CallParticipantsState) obj));
            }
        });
        MutableLiveData<Boolean> mutableLiveData6 = new MutableLiveData<>();
        this.isLandscapeEnabled = mutableLiveData6;
        WebRtcCallViewModel$$ExternalSyntheticLambda10 webRtcCallViewModel$$ExternalSyntheticLambda10 = new Observer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda10
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                WebRtcCallViewModel.this.lambda$new$2((List) obj);
            }
        };
        this.groupMemberStateUpdater = webRtcCallViewModel$$ExternalSyntheticLambda10;
        this.ephemeralState = new MutableLiveData<>();
        this.elapsedTimeHandler = new Handler(Looper.getMainLooper());
        this.elapsedTimeRunnable = new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda11
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallViewModel.this.handleTick();
            }
        };
        this.stopOutgoingRingingMode = new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallViewModel.this.stopOutgoingRingingMode();
            }
        };
        this.canDisplayTooltipIfNeeded = true;
        this.canDisplayPopupIfNeeded = true;
        this.hasEnabledLocalVideo = false;
        this.wasInOutgoingRingingMode = false;
        this.callConnectedTime = -1;
        this.answerWithVideoAvailable = false;
        this.canEnterPipMode = false;
        this.previousParticipantsList = Collections.emptyList();
        this.callStarting = false;
        this.switchOnFirstScreenShare = true;
        this.showScreenShareTip = true;
        this.repository = new WebRtcCallRepository(ApplicationDependencies.getApplication());
        LiveData<Orientation> orientation = deviceOrientationMonitor.getOrientation();
        this.orientation = orientation;
        this.controlsRotation = LiveDataUtil.combineLatest(Transformations.distinctUntilChanged(mutableLiveData6), Transformations.distinctUntilChanged(orientation), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return Integer.valueOf(WebRtcCallViewModel.this.resolveRotation(((Boolean) obj).booleanValue(), (Orientation) obj2));
            }
        });
        switchMap.observeForever(webRtcCallViewModel$$ExternalSyntheticLambda10);
    }

    public LiveData<Integer> getControlsRotation() {
        return this.controlsRotation;
    }

    public LiveData<Orientation> getOrientation() {
        return Transformations.distinctUntilChanged(this.orientation);
    }

    public LiveData<Pair<Orientation, Boolean>> getOrientationAndLandscapeEnabled() {
        return LiveDataUtil.combineLatest(this.orientation, this.isLandscapeEnabled, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda12
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return new Pair((Orientation) obj, (Boolean) obj2);
            }
        });
    }

    public LiveData<Boolean> getMicrophoneEnabled() {
        return Transformations.distinctUntilChanged(this.microphoneEnabled);
    }

    public LiveData<WebRtcControls> getWebRtcControls() {
        return this.realWebRtcControls;
    }

    public LiveRecipient getRecipient() {
        return this.liveRecipient.getValue();
    }

    public void setRecipient(Recipient recipient) {
        this.liveRecipient.setValue(recipient.live());
    }

    public void setFoldableState(WebRtcControls.FoldableState foldableState) {
        this.foldableState.postValue(foldableState);
        ThreadUtil.runOnMain(new Runnable(foldableState) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda13
            public final /* synthetic */ WebRtcControls.FoldableState f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallViewModel.this.lambda$setFoldableState$3(this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$setFoldableState$3(WebRtcControls.FoldableState foldableState) {
        DefaultValueLiveData<CallParticipantsState> defaultValueLiveData = this.participantsState;
        defaultValueLiveData.setValue(CallParticipantsState.update(defaultValueLiveData.getValue(), foldableState));
    }

    public LiveData<Event> getEvents() {
        return this.events;
    }

    public /* synthetic */ Long lambda$getCallTime$4(Long l) {
        long j = -1;
        if (this.callConnectedTime != -1) {
            j = l.longValue();
        }
        return Long.valueOf(j);
    }

    public LiveData<Long> getCallTime() {
        return Transformations.map(this.elapsed, new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda16
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return WebRtcCallViewModel.this.lambda$getCallTime$4((Long) obj);
            }
        });
    }

    public LiveData<CallParticipantsState> getCallParticipantsState() {
        return this.participantsState;
    }

    public LiveData<CallParticipantListUpdate> getCallParticipantListUpdate() {
        return this.callParticipantListUpdate;
    }

    public LiveData<SafetyNumberChangeEvent> getSafetyNumberChangeEvent() {
        return this.safetyNumberChangeEvent;
    }

    public LiveData<List<GroupMemberEntry.FullMember>> getGroupMembersChanged() {
        return this.groupMembersChanged;
    }

    public LiveData<Integer> getGroupMemberCount() {
        return this.groupMemberCount;
    }

    public LiveData<Boolean> shouldShowSpeakerHint() {
        return this.shouldShowSpeakerHint;
    }

    public LiveData<WebRtcEphemeralState> getEphemeralState() {
        return this.ephemeralState;
    }

    public boolean canEnterPipMode() {
        return this.canEnterPipMode;
    }

    public boolean isAnswerWithVideoAvailable() {
        return this.answerWithVideoAvailable;
    }

    public boolean isCallStarting() {
        return this.callStarting;
    }

    public void setIsInPipMode(boolean z) {
        this.isInPipMode.setValue(Boolean.valueOf(z));
        DefaultValueLiveData<CallParticipantsState> defaultValueLiveData = this.participantsState;
        defaultValueLiveData.setValue(CallParticipantsState.update(defaultValueLiveData.getValue(), z));
    }

    public void setIsLandscapeEnabled(boolean z) {
        this.isLandscapeEnabled.postValue(Boolean.valueOf(z));
    }

    public void setIsViewingFocusedParticipant(CallParticipantsState.SelectedPage selectedPage) {
        if (selectedPage == CallParticipantsState.SelectedPage.FOCUSED) {
            SignalStore.tooltips().markGroupCallSpeakerViewSeen();
        }
        CallParticipantsState value = this.participantsState.getValue();
        if (this.showScreenShareTip && value.getFocusedParticipant().isScreenSharing() && value.isViewingFocusedParticipant() && selectedPage == CallParticipantsState.SelectedPage.GRID) {
            this.showScreenShareTip = false;
            this.events.setValue(new Event.ShowSwipeToSpeakerHint());
        }
        DefaultValueLiveData<CallParticipantsState> defaultValueLiveData = this.participantsState;
        defaultValueLiveData.setValue(CallParticipantsState.update(defaultValueLiveData.getValue(), selectedPage));
    }

    public void onLocalPictureInPictureClicked() {
        CallParticipantsState value = this.participantsState.getValue();
        if (value.getGroupCallState() == WebRtcViewModel.GroupCallState.IDLE) {
            DefaultValueLiveData<CallParticipantsState> defaultValueLiveData = this.participantsState;
            defaultValueLiveData.setValue(CallParticipantsState.setExpanded(defaultValueLiveData.getValue(), value.getLocalRenderState() != WebRtcLocalRenderState.EXPANDED));
        }
    }

    public void onDismissedVideoTooltip() {
        this.canDisplayTooltipIfNeeded = false;
    }

    public void updateFromWebRtcViewModel(WebRtcViewModel webRtcViewModel, boolean z) {
        this.canEnterPipMode = !webRtcViewModel.getState().isPreJoinOrNetworkUnavailable();
        if (this.callStarting && webRtcViewModel.getState().isPassedPreJoin()) {
            this.callStarting = false;
        }
        CallParticipant localParticipant = webRtcViewModel.getLocalParticipant();
        this.microphoneEnabled.setValue(Boolean.valueOf(localParticipant.isMicrophoneEnabled()));
        CallParticipantsState value = this.participantsState.getValue();
        boolean isScreenSharing = value.getFocusedParticipant().isScreenSharing();
        CallParticipantsState update = CallParticipantsState.update(value, webRtcViewModel, z);
        this.participantsState.setValue(update);
        if (this.switchOnFirstScreenShare && !isScreenSharing && update.getFocusedParticipant().isScreenSharing()) {
            this.switchOnFirstScreenShare = false;
            this.events.setValue(new Event.SwitchToSpeaker());
        }
        if (webRtcViewModel.getGroupState().isConnected()) {
            if (!containsPlaceholders(this.previousParticipantsList)) {
                this.callParticipantListUpdate.setValue(CallParticipantListUpdate.computeDeltaUpdate(this.previousParticipantsList, webRtcViewModel.getRemoteParticipants()));
            }
            this.previousParticipantsList = webRtcViewModel.getRemoteParticipants();
            this.identityChangedRecipients.setValue(webRtcViewModel.getIdentityChangedParticipants());
        }
        updateWebRtcControls(webRtcViewModel.getState(), webRtcViewModel.getGroupState(), localParticipant.getCameraState().isEnabled(), webRtcViewModel.isRemoteVideoEnabled(), webRtcViewModel.isRemoteVideoOffer(), localParticipant.isMoreThanOneCameraAvailable(), Util.hasItems(webRtcViewModel.getRemoteParticipants()), webRtcViewModel.getActiveDevice(), webRtcViewModel.getAvailableDevices(), webRtcViewModel.getRemoteDevicesCount().orElse(0), webRtcViewModel.getParticipantLimit());
        if (update.isInOutgoingRingingMode()) {
            cancelTimer();
            if (!this.wasInOutgoingRingingMode) {
                this.elapsedTimeHandler.postDelayed(this.stopOutgoingRingingMode, CallParticipantsState.MAX_OUTGOING_GROUP_RING_DURATION);
            }
            this.wasInOutgoingRingingMode = true;
        } else {
            WebRtcViewModel.State state = webRtcViewModel.getState();
            WebRtcViewModel.State state2 = WebRtcViewModel.State.CALL_CONNECTED;
            if (state == state2 && this.callConnectedTime == -1) {
                this.callConnectedTime = this.wasInOutgoingRingingMode ? System.currentTimeMillis() : webRtcViewModel.getCallConnectedTime();
                startTimer();
            } else if (webRtcViewModel.getState() != state2 || webRtcViewModel.getGroupState().isNotIdleOrConnected()) {
                cancelTimer();
                this.callConnectedTime = -1;
            }
        }
        if (localParticipant.getCameraState().isEnabled()) {
            this.canDisplayTooltipIfNeeded = false;
            this.hasEnabledLocalVideo = true;
            this.events.setValue(new Event.DismissVideoTooltip());
        }
        if (this.canDisplayTooltipIfNeeded && webRtcViewModel.isRemoteVideoEnabled() && !this.hasEnabledLocalVideo) {
            this.canDisplayTooltipIfNeeded = false;
            this.events.setValue(new Event.ShowVideoTooltip());
        }
        if (this.canDisplayPopupIfNeeded && webRtcViewModel.isCellularConnection() && NetworkUtil.isConnectedWifi(ApplicationDependencies.getApplication())) {
            this.canDisplayPopupIfNeeded = false;
            this.events.setValue(new Event.ShowWifiToCellularPopup());
        } else if (!webRtcViewModel.isCellularConnection()) {
            this.canDisplayPopupIfNeeded = true;
        }
    }

    public void updateFromEphemeralState(WebRtcEphemeralState webRtcEphemeralState) {
        this.ephemeralState.setValue(webRtcEphemeralState);
    }

    public int resolveRotation(boolean z, Orientation orientation) {
        if (z) {
            return 0;
        }
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$components$sensors$Orientation[orientation.ordinal()];
        if (i == 1) {
            return 90;
        }
        if (i == 2) {
            return -90;
        }
        if (i == 3) {
            return 0;
        }
        throw new AssertionError();
    }

    private boolean containsPlaceholders(List<CallParticipant> list) {
        return Stream.of(list).anyMatch(new Predicate() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda14
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return WebRtcCallViewModel.lambda$containsPlaceholders$5((CallParticipant) obj);
            }
        });
    }

    public static /* synthetic */ boolean lambda$containsPlaceholders$5(CallParticipant callParticipant) {
        return callParticipant.getCallParticipantId().getDemuxId() == -1;
    }

    private void updateWebRtcControls(WebRtcViewModel.State state, WebRtcViewModel.GroupCallState groupCallState, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, SignalAudioManager.AudioDevice audioDevice, Set<SignalAudioManager.AudioDevice> set, long j, Long l) {
        WebRtcControls.CallState callState;
        WebRtcControls.GroupCallState groupCallState2;
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[state.ordinal()]) {
            case 1:
                callState = WebRtcControls.CallState.PRE_JOIN;
                break;
            case 2:
                callState = WebRtcControls.CallState.INCOMING;
                this.answerWithVideoAvailable = z3;
                break;
            case 3:
            case 4:
                callState = WebRtcControls.CallState.OUTGOING;
                break;
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                callState = WebRtcControls.CallState.ENDING;
                break;
            case 11:
                callState = WebRtcControls.CallState.INCOMING;
                break;
            case 12:
                callState = WebRtcControls.CallState.ERROR;
                break;
            case 13:
                callState = WebRtcControls.CallState.RECONNECTING;
                break;
            default:
                callState = WebRtcControls.CallState.ONGOING;
                break;
        }
        switch (AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[groupCallState.ordinal()]) {
            case 1:
                groupCallState2 = WebRtcControls.GroupCallState.DISCONNECTED;
                break;
            case 2:
            case 3:
                if (l != null && j >= l.longValue()) {
                    groupCallState2 = WebRtcControls.GroupCallState.FULL;
                    break;
                } else {
                    groupCallState2 = WebRtcControls.GroupCallState.CONNECTING;
                    break;
                }
            case 4:
            case 5:
            case 6:
                groupCallState2 = WebRtcControls.GroupCallState.CONNECTED;
                break;
            default:
                groupCallState2 = WebRtcControls.GroupCallState.NONE;
                break;
        }
        this.webRtcControls.setValue(new WebRtcControls(z, z2 || z3, z4, Boolean.TRUE.equals(this.isInPipMode.getValue()), z5, callState, groupCallState2, l, WebRtcControls.FoldableState.flat(), audioDevice, set));
    }

    /* renamed from: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$components$sensors$Orientation;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State;

        static {
            int[] iArr = new int[WebRtcViewModel.GroupCallState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState = iArr;
            try {
                iArr[WebRtcViewModel.GroupCallState.DISCONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.RECONNECTING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTED.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINING.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[WebRtcViewModel.State.values().length];
            $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State = iArr2;
            try {
                iArr2[WebRtcViewModel.State.CALL_PRE_JOIN.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_INCOMING.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_OUTGOING.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_RINGING.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_ACCEPTED_ELSEWHERE.ordinal()] = 5;
            } catch (NoSuchFieldError unused11) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_DECLINED_ELSEWHERE.ordinal()] = 6;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_ONGOING_ELSEWHERE.ordinal()] = 7;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_NEEDS_PERMISSION.ordinal()] = 8;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_BUSY.ordinal()] = 9;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_DISCONNECTED.ordinal()] = 10;
            } catch (NoSuchFieldError unused16) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_DISCONNECTED_GLARE.ordinal()] = 11;
            } catch (NoSuchFieldError unused17) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.NETWORK_FAILURE.ordinal()] = 12;
            } catch (NoSuchFieldError unused18) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$State[WebRtcViewModel.State.CALL_RECONNECTING.ordinal()] = 13;
            } catch (NoSuchFieldError unused19) {
            }
            int[] iArr3 = new int[Orientation.values().length];
            $SwitchMap$org$thoughtcrime$securesms$components$sensors$Orientation = iArr3;
            try {
                iArr3[Orientation.LANDSCAPE_LEFT_EDGE.ordinal()] = 1;
            } catch (NoSuchFieldError unused20) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$sensors$Orientation[Orientation.LANDSCAPE_RIGHT_EDGE.ordinal()] = 2;
            } catch (NoSuchFieldError unused21) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$sensors$Orientation[Orientation.PORTRAIT_BOTTOM_EDGE.ordinal()] = 3;
            } catch (NoSuchFieldError unused22) {
            }
        }
    }

    public WebRtcControls updateControlsFoldableState(WebRtcControls.FoldableState foldableState, WebRtcControls webRtcControls) {
        return webRtcControls.withFoldableState(foldableState);
    }

    public WebRtcControls getRealWebRtcControls(boolean z, WebRtcControls webRtcControls) {
        return z ? WebRtcControls.PIP : webRtcControls;
    }

    public boolean shouldShowSpeakerHint(CallParticipantsState callParticipantsState) {
        return !callParticipantsState.isInPipMode() && callParticipantsState.getRemoteDevicesCount().orElse(0) > 1 && callParticipantsState.getGroupCallState().isConnected() && !SignalStore.tooltips().hasSeenGroupCallSpeakerView();
    }

    private void startTimer() {
        cancelTimer();
        this.elapsedTimeHandler.removeCallbacks(this.stopOutgoingRingingMode);
        this.elapsedTimeHandler.post(this.elapsedTimeRunnable);
    }

    public void stopOutgoingRingingMode() {
        if (this.callConnectedTime == -1) {
            this.callConnectedTime = System.currentTimeMillis();
            startTimer();
        }
    }

    public void handleTick() {
        if (this.callConnectedTime != -1) {
            this.elapsed.postValue(Long.valueOf((System.currentTimeMillis() - this.callConnectedTime) / 1000));
            this.elapsedTimeHandler.postDelayed(this.elapsedTimeRunnable, 1000);
        }
    }

    private void cancelTimer() {
        this.elapsedTimeHandler.removeCallbacks(this.elapsedTimeRunnable);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        cancelTimer();
        this.groupMembers.removeObserver(this.groupMemberStateUpdater);
    }

    public void startCall(boolean z) {
        this.callStarting = true;
        Recipient recipient = getRecipient().get();
        if (recipient.isGroup()) {
            this.repository.getIdentityRecords(recipient, new Consumer(z) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel$$ExternalSyntheticLambda15
                public final /* synthetic */ boolean f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.core.util.Consumer
                public final void accept(Object obj) {
                    WebRtcCallViewModel.this.lambda$startCall$6(this.f$1, (IdentityRecordList) obj);
                }
            });
        } else {
            this.events.postValue(new Event.StartCall(z));
        }
    }

    public /* synthetic */ void lambda$startCall$6(boolean z, IdentityRecordList identityRecordList) {
        if (identityRecordList.isUntrusted(false) || identityRecordList.isUnverified(false)) {
            List<IdentityRecord> unverifiedRecords = identityRecordList.getUnverifiedRecords();
            unverifiedRecords.addAll(identityRecordList.getUntrustedRecords());
            this.events.postValue(new Event.ShowGroupCallSafetyNumberChange(unverifiedRecords));
            return;
        }
        this.events.postValue(new Event.StartCall(z));
    }

    /* loaded from: classes4.dex */
    public static abstract class Event {
        /* synthetic */ Event(AnonymousClass1 r1) {
            this();
        }

        private Event() {
        }

        /* loaded from: classes4.dex */
        public static class ShowVideoTooltip extends Event {
            public ShowVideoTooltip() {
                super(null);
            }
        }

        /* loaded from: classes4.dex */
        public static class DismissVideoTooltip extends Event {
            public DismissVideoTooltip() {
                super(null);
            }
        }

        /* loaded from: classes4.dex */
        public static class ShowWifiToCellularPopup extends Event {
            public ShowWifiToCellularPopup() {
                super(null);
            }
        }

        /* loaded from: classes4.dex */
        public static class StartCall extends Event {
            private final boolean isVideoCall;

            public StartCall(boolean z) {
                super(null);
                this.isVideoCall = z;
            }

            public boolean isVideoCall() {
                return this.isVideoCall;
            }
        }

        /* loaded from: classes4.dex */
        public static class ShowGroupCallSafetyNumberChange extends Event {
            private final List<IdentityRecord> identityRecords;

            public ShowGroupCallSafetyNumberChange(List<IdentityRecord> list) {
                super(null);
                this.identityRecords = list;
            }

            public List<IdentityRecord> getIdentityRecords() {
                return this.identityRecords;
            }
        }

        /* loaded from: classes4.dex */
        public static class SwitchToSpeaker extends Event {
            public SwitchToSpeaker() {
                super(null);
            }
        }

        /* loaded from: classes4.dex */
        public static class ShowSwipeToSpeakerHint extends Event {
            public ShowSwipeToSpeakerHint() {
                super(null);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static class SafetyNumberChangeEvent {
        private final boolean isInPipMode;
        private final Collection<RecipientId> recipientIds;

        /* synthetic */ SafetyNumberChangeEvent(boolean z, Collection collection, AnonymousClass1 r3) {
            this(z, collection);
        }

        private SafetyNumberChangeEvent(boolean z, Collection<RecipientId> collection) {
            this.isInPipMode = z;
            this.recipientIds = collection;
        }

        public boolean isInPipMode() {
            return this.isInPipMode;
        }

        public Collection<RecipientId> getRecipientIds() {
            return this.recipientIds;
        }
    }

    /* loaded from: classes4.dex */
    public static class Factory implements ViewModelProvider.Factory {
        private final DeviceOrientationMonitor deviceOrientationMonitor;

        public Factory(DeviceOrientationMonitor deviceOrientationMonitor) {
            this.deviceOrientationMonitor = deviceOrientationMonitor;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            T cast = cls.cast(new WebRtcCallViewModel(this.deviceOrientationMonitor, null));
            Objects.requireNonNull(cast);
            return cast;
        }
    }
}
