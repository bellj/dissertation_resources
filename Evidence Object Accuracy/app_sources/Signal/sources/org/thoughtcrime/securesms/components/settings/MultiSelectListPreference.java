package org.thoughtcrime.securesms.components.settings;

import java.util.Arrays;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B?\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\r0\f¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u001a\u001a\u00020\u0000H\u0016R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u000fR\u0019\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\n\n\u0002\u0010\u0012\u001a\u0004\b\u0010\u0010\u0011R\u001d\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/MultiSelectListPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "isEnabled", "", "listItems", "", "", "selected", "", "onSelected", "Lkotlin/Function1;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Z[Ljava/lang/String;[ZLkotlin/jvm/functions/Function1;)V", "()Z", "getListItems", "()[Ljava/lang/String;", "[Ljava/lang/String;", "getOnSelected", "()Lkotlin/jvm/functions/Function1;", "getSelected", "()[Z", "getTitle", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "areContentsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiSelectListPreference extends PreferenceModel<MultiSelectListPreference> {
    private final boolean isEnabled;
    private final String[] listItems;
    private final Function1<boolean[], Unit> onSelected;
    private final boolean[] selected;
    private final DSLSettingsText title;

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public final String[] getListItems() {
        return this.listItems;
    }

    public final boolean[] getSelected() {
        return this.selected;
    }

    public final Function1<boolean[], Unit> getOnSelected() {
        return this.onSelected;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r14v0, resolved type: kotlin.jvm.functions.Function1<? super boolean[], kotlin.Unit> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MultiSelectListPreference(DSLSettingsText dSLSettingsText, boolean z, String[] strArr, boolean[] zArr, Function1<? super boolean[], Unit> function1) {
        super(dSLSettingsText, null, null, null, z, 14, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(strArr, "listItems");
        Intrinsics.checkNotNullParameter(zArr, "selected");
        Intrinsics.checkNotNullParameter(function1, "onSelected");
        this.title = dSLSettingsText;
        this.isEnabled = z;
        this.listItems = strArr;
        this.selected = zArr;
        this.onSelected = function1;
    }

    public boolean areContentsTheSame(MultiSelectListPreference multiSelectListPreference) {
        Intrinsics.checkNotNullParameter(multiSelectListPreference, "newItem");
        return super.areContentsTheSame(multiSelectListPreference) && Arrays.equals(this.listItems, multiSelectListPreference.listItems) && Arrays.equals(this.selected, multiSelectListPreference.selected);
    }
}
