package org.thoughtcrime.securesms.components.voice;

import android.os.Bundle;
import android.os.ResultReceiver;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.util.Util;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;

/* compiled from: VoiceNotePlaybackController.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00132\u00020\u0001:\u0001\u0013B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J4\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackController;", "Lcom/google/android/exoplayer2/ext/mediasession/MediaSessionConnector$CommandReceiver;", "player", "Lcom/google/android/exoplayer2/SimpleExoPlayer;", "voiceNotePlaybackParameters", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackParameters;", "(Lcom/google/android/exoplayer2/SimpleExoPlayer;Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackParameters;)V", "onCommand", "", "p", "Lcom/google/android/exoplayer2/Player;", "controlDispatcher", "Lcom/google/android/exoplayer2/ControlDispatcher;", "command", "", "extras", "Landroid/os/Bundle;", "cb", "Landroid/os/ResultReceiver;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNotePlaybackController implements MediaSessionConnector.CommandReceiver {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(VoiceNoteMediaController.class);
    private final SimpleExoPlayer player;
    private final VoiceNotePlaybackParameters voiceNotePlaybackParameters;

    public VoiceNotePlaybackController(SimpleExoPlayer simpleExoPlayer, VoiceNotePlaybackParameters voiceNotePlaybackParameters) {
        Intrinsics.checkNotNullParameter(simpleExoPlayer, "player");
        Intrinsics.checkNotNullParameter(voiceNotePlaybackParameters, "voiceNotePlaybackParameters");
        this.player = simpleExoPlayer;
        this.voiceNotePlaybackParameters = voiceNotePlaybackParameters;
    }

    /* compiled from: VoiceNotePlaybackController.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackController$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.CommandReceiver
    public boolean onCommand(Player player, ControlDispatcher controlDispatcher, String str, Bundle bundle, ResultReceiver resultReceiver) {
        AudioAttributes audioAttributes;
        Intrinsics.checkNotNullParameter(player, "p");
        Intrinsics.checkNotNullParameter(controlDispatcher, "controlDispatcher");
        Intrinsics.checkNotNullParameter(str, "command");
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("[onCommand] Received player command ");
        sb.append(str);
        sb.append(" (extras? ");
        sb.append(bundle != null);
        sb.append(')');
        Log.d(str2, sb.toString());
        if (Intrinsics.areEqual(str, VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED)) {
            float f = 1.0f;
            if (bundle != null) {
                f = bundle.getFloat(VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED, 1.0f);
            }
            this.player.setPlaybackParameters(new PlaybackParameters(f));
            this.voiceNotePlaybackParameters.setSpeed(f);
            return true;
        } else if (!Intrinsics.areEqual(str, VoiceNotePlaybackService.ACTION_SET_AUDIO_STREAM)) {
            return false;
        } else {
            int i = bundle != null ? bundle.getInt(VoiceNotePlaybackService.ACTION_SET_AUDIO_STREAM, 3) : 3;
            if (i != Util.getStreamTypeForAudioUsage(this.player.getAudioAttributes().usage)) {
                if (i == 0) {
                    audioAttributes = new AudioAttributes.Builder().setContentType(1).setUsage(2).build();
                } else if (i == 3) {
                    audioAttributes = new AudioAttributes.Builder().setContentType(2).setUsage(1).build();
                } else {
                    throw new AssertionError();
                }
                Intrinsics.checkNotNullExpressionValue(audioAttributes, "when (newStreamType) {\n …ssertionError()\n        }");
                this.player.setPlayWhenReady(false);
                this.player.setAudioAttributes(audioAttributes, false);
                if (i == 0) {
                    this.player.setPlayWhenReady(true);
                }
            }
            return true;
        }
    }
}
