package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.ScaleAnimation;
import android.widget.LinearLayout;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

/* loaded from: classes4.dex */
public class HidingLinearLayout extends LinearLayout {
    public HidingLinearLayout(Context context) {
        super(context);
    }

    public HidingLinearLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public HidingLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void hide() {
        if (isEnabled() && getVisibility() != 8) {
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(new ScaleAnimation(1.0f, 0.5f, 1.0f, 1.0f, 1, 1.0f, 1, 0.5f));
            animationSet.addAnimation(new AlphaAnimation(1.0f, 0.0f));
            animationSet.setDuration(100);
            animationSet.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.components.HidingLinearLayout.1
                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationRepeat(Animation animation) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationStart(Animation animation) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationEnd(Animation animation) {
                    HidingLinearLayout.this.setVisibility(8);
                }
            });
            animateWith(animationSet);
        }
    }

    public void show() {
        if (isEnabled() && getVisibility() != 0) {
            setVisibility(0);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(new ScaleAnimation(0.5f, 1.0f, 1.0f, 1.0f, 1, 1.0f, 1, 0.5f));
            animationSet.addAnimation(new AlphaAnimation(0.0f, 1.0f));
            animationSet.setDuration(100);
            animateWith(animationSet);
        }
    }

    private void animateWith(Animation animation) {
        animation.setDuration(150);
        animation.setInterpolator(new FastOutSlowInInterpolator());
        startAnimation(animation);
    }

    public void disable() {
        setVisibility(8);
        setEnabled(false);
    }
}
