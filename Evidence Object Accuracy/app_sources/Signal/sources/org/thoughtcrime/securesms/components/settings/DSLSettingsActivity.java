package org.thoughtcrime.securesms.components.settings;

import android.os.Bundle;
import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* compiled from: DSLSettingsActivity.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0016\u0018\u0000 \u00152\u00020\u0001:\u0002\u0015\u0016B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0014J\b\u0010\u0012\u001a\u00020\u0011H\u0016J\b\u0010\u0013\u001a\u00020\rH\u0014J\b\u0010\u0014\u001a\u00020\rH\u0014R\u0014\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001e\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b@BX.¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "()V", "dynamicTheme", "Lorg/thoughtcrime/securesms/util/DynamicTheme;", "getDynamicTheme", "()Lorg/thoughtcrime/securesms/util/DynamicTheme;", "<set-?>", "Landroidx/navigation/NavController;", "navController", "getNavController", "()Landroidx/navigation/NavController;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "onNavigateUp", "onResume", "onWillFinish", "Companion", "OnBackPressed", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class DSLSettingsActivity extends PassphraseRequiredActivity {
    public static final String ARG_NAV_GRAPH;
    public static final String ARG_START_BUNDLE;
    public static final Companion Companion = new Companion(null);
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();
    private NavController navController;

    protected void onWillFinish() {
    }

    protected DynamicTheme getDynamicTheme() {
        return this.dynamicTheme;
    }

    public final NavController getNavController() {
        NavController navController = this.navController;
        if (navController != null) {
            return navController;
        }
        Intrinsics.throwUninitializedPropertyAccessException("navController");
        return null;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        setContentView(R.layout.dsl_settings_activity);
        if (bundle == null) {
            int intExtra = getIntent().getIntExtra(ARG_NAV_GRAPH, -1);
            if (intExtra != -1) {
                NavHostFragment create = NavHostFragment.create(intExtra, getIntent().getBundleExtra(ARG_START_BUNDLE));
                Intrinsics.checkNotNullExpressionValue(create, "create(navGraphId, inten…eExtra(ARG_START_BUNDLE))");
                getSupportFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, create).commitNow();
                NavController navController = create.getNavController();
                Intrinsics.checkNotNullExpressionValue(navController, "fragment.navController");
                this.navController = navController;
            } else {
                throw new IllegalStateException("No navgraph id was passed to activity");
            }
        } else {
            Fragment findFragmentById = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
            if (findFragmentById != null) {
                NavController navController2 = ((NavHostFragment) findFragmentById).getNavController();
                Intrinsics.checkNotNullExpressionValue(navController2, "fragment.navController");
                this.navController = navController2;
            } else {
                throw new NullPointerException("null cannot be cast to non-null type androidx.navigation.fragment.NavHostFragment");
            }
        }
        getDynamicTheme().onCreate(this);
        getOnBackPressedDispatcher().addCallback(this, new OnBackPressed());
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        getDynamicTheme().onResume(this);
    }

    @Override // android.app.Activity
    public boolean onNavigateUp() {
        if (Navigation.findNavController(this, R.id.nav_host_fragment).popBackStack()) {
            return false;
        }
        onWillFinish();
        finish();
        return true;
    }

    /* compiled from: DSLSettingsActivity.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsActivity$Companion;", "", "()V", "ARG_NAV_GRAPH", "", "ARG_START_BUNDLE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    /* compiled from: DSLSettingsActivity.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsActivity$OnBackPressed;", "Landroidx/activity/OnBackPressedCallback;", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsActivity;)V", "handleOnBackPressed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public final class OnBackPressed extends OnBackPressedCallback {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnBackPressed() {
            super(true);
            DSLSettingsActivity.this = r1;
        }

        @Override // androidx.activity.OnBackPressedCallback
        public void handleOnBackPressed() {
            DSLSettingsActivity.this.onNavigateUp();
        }
    }
}
