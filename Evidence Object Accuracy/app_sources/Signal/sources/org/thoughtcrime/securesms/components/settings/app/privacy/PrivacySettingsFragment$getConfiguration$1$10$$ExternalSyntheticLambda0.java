package org.thoughtcrime.securesms.components.settings.app.privacy;

import mobi.upod.timedurationpicker.TimeDurationPicker;
import mobi.upod.timedurationpicker.TimeDurationPickerDialog;
import org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PrivacySettingsFragment$getConfiguration$1$10$$ExternalSyntheticLambda0 implements TimeDurationPickerDialog.OnDurationSetListener {
    public final /* synthetic */ PrivacySettingsFragment f$0;

    public /* synthetic */ PrivacySettingsFragment$getConfiguration$1$10$$ExternalSyntheticLambda0(PrivacySettingsFragment privacySettingsFragment) {
        this.f$0 = privacySettingsFragment;
    }

    @Override // mobi.upod.timedurationpicker.TimeDurationPickerDialog.OnDurationSetListener
    public final void onDurationSet(TimeDurationPicker timeDurationPicker, long j) {
        PrivacySettingsFragment$getConfiguration$1.AnonymousClass10.m827invoke$lambda0(this.f$0, timeDurationPicker, j);
    }
}
