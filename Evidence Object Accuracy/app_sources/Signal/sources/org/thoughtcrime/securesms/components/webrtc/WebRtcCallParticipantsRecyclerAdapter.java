package org.thoughtcrime.securesms.components.webrtc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.webrtc.RendererCommon;

/* loaded from: classes4.dex */
public class WebRtcCallParticipantsRecyclerAdapter extends ListAdapter<CallParticipant, ViewHolder> {
    private static final int EMPTY;
    private static final int PARTICIPANT;

    public WebRtcCallParticipantsRecyclerAdapter() {
        super(new DiffCallback());
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 0) {
            return new ParticipantViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.webrtc_call_participant_recycler_item, viewGroup, false));
        }
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.webrtc_call_participant_recycler_empty_item, viewGroup, false));
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(getItem(i));
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemViewType(int i) {
        return getItem(i) == CallParticipant.EMPTY ? 1 : 0;
    }

    /* loaded from: classes4.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        void bind(CallParticipant callParticipant) {
        }

        ViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static class ParticipantViewHolder extends ViewHolder {
        private final CallParticipantView callParticipantView;

        ParticipantViewHolder(View view) {
            super(view);
            CallParticipantView callParticipantView = (CallParticipantView) view.findViewById(R.id.call_participant);
            this.callParticipantView = callParticipantView;
            View findViewById = callParticipantView.findViewById(R.id.call_participant_audio_indicator);
            int pixels = (int) DimensionUnit.DP.toPixels(8.0f);
            ViewUtil.setLeftMargin(findViewById, pixels);
            ViewUtil.setBottomMargin(findViewById, pixels);
        }

        void bind(CallParticipant callParticipant) {
            this.callParticipantView.setCallParticipant(callParticipant);
            this.callParticipantView.setRenderInPip(true);
            this.callParticipantView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL);
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class DiffCallback extends DiffUtil.ItemCallback<CallParticipant> {
        private DiffCallback() {
        }

        public boolean areItemsTheSame(CallParticipant callParticipant, CallParticipant callParticipant2) {
            return callParticipant.getRecipient().equals(callParticipant2.getRecipient());
        }

        public boolean areContentsTheSame(CallParticipant callParticipant, CallParticipant callParticipant2) {
            return callParticipant.equals(callParticipant2);
        }
    }
}
