package org.thoughtcrime.securesms.components.settings.app.privacy;

import android.content.SharedPreferences;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: PrivacySettingsViewModel.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001(B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\n\u001a\u00020\tH\u0002J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0006\u0010\u0010\u001a\u00020\u000fJ\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0015\u001a\u00020\u0016J\u000e\u0010\u0017\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u0018\u001a\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u001aJ\u000e\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\u001c\u001a\u00020\u001dJ\u000e\u0010\u001e\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010\u001f\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010 \u001a\u00020\u000f2\u0006\u0010!\u001a\u00020\"J\u000e\u0010#\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u000e\u0010$\u001a\u00020\u000f2\u0006\u0010%\u001a\u00020\u0013J\u000e\u0010&\u001a\u00020\u000f2\u0006\u0010\u0012\u001a\u00020\u0013J\u0010\u0010'\u001a\u00020\t2\u0006\u0010\u0007\u001a\u00020\tH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "sharedPreferences", "Landroid/content/SharedPreferences;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsRepository;", "(Landroid/content/SharedPreferences;Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsRepository;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "refresh", "", "refreshBlockedCount", "setIncognitoKeyboard", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "setObsoletePasswordTimeout", "minutes", "", "setObsoletePasswordTimeoutEnabled", "setPhoneNumberListingMode", "phoneNumberListingMode", "Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberListingMode;", "setPhoneNumberSharingMode", "phoneNumberSharingMode", "Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberSharingMode;", "setReadReceiptsEnabled", "setScreenLockEnabled", "setScreenLockTimeout", "seconds", "", "setScreenSecurityEnabled", "setStoriesEnabled", "isStoriesEnabled", "setTypingIndicatorsEnabled", "updateState", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PrivacySettingsViewModel extends ViewModel {
    private final PrivacySettingsRepository repository;
    private final SharedPreferences sharedPreferences;
    private final LiveData<PrivacySettingsState> state;
    private final Store<PrivacySettingsState> store;

    public PrivacySettingsViewModel(SharedPreferences sharedPreferences, PrivacySettingsRepository privacySettingsRepository) {
        Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
        Intrinsics.checkNotNullParameter(privacySettingsRepository, "repository");
        this.sharedPreferences = sharedPreferences;
        this.repository = privacySettingsRepository;
        Store<PrivacySettingsState> store = new Store<>(getState());
        this.store = store;
        LiveData<PrivacySettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    /* renamed from: getState */
    public final LiveData<PrivacySettingsState> m837getState() {
        return this.state;
    }

    public final void refreshBlockedCount() {
        this.repository.getBlockedCount(new PrivacySettingsViewModel$refreshBlockedCount$1(this));
        this.repository.getPrivateStories(new PrivacySettingsViewModel$refreshBlockedCount$2(this));
    }

    public final void setReadReceiptsEnabled(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.READ_RECEIPTS_PREF, z).apply();
        this.repository.syncReadReceiptState();
        refresh();
    }

    public final void setTypingIndicatorsEnabled(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.TYPING_INDICATORS, z).apply();
        this.repository.syncTypingIndicatorsState();
        refresh();
    }

    public final void setScreenLockEnabled(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.SCREEN_LOCK, z).apply();
        refresh();
    }

    public final void setScreenLockTimeout(long j) {
        TextSecurePreferences.setScreenLockTimeout(ApplicationDependencies.getApplication(), j);
        refresh();
    }

    public final void setScreenSecurityEnabled(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.SCREEN_SECURITY_PREF, z).apply();
        refresh();
    }

    public final void setPhoneNumberSharingMode(PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode) {
        Intrinsics.checkNotNullParameter(phoneNumberSharingMode, "phoneNumberSharingMode");
        SignalStore.phoneNumberPrivacy().setPhoneNumberSharingMode(phoneNumberSharingMode);
        StorageSyncHelper.scheduleSyncForDataChange();
        refresh();
    }

    public final void setPhoneNumberListingMode(PhoneNumberPrivacyValues.PhoneNumberListingMode phoneNumberListingMode) {
        Intrinsics.checkNotNullParameter(phoneNumberListingMode, "phoneNumberListingMode");
        SignalStore.phoneNumberPrivacy().setPhoneNumberListingMode(phoneNumberListingMode);
        StorageSyncHelper.scheduleSyncForDataChange();
        ApplicationDependencies.getJobManager().startChain(new RefreshAttributesJob()).then(new RefreshOwnProfileJob()).enqueue();
        refresh();
    }

    public final void setIncognitoKeyboard(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.INCOGNITO_KEYBORAD_PREF, z).apply();
        refresh();
    }

    public final void setObsoletePasswordTimeoutEnabled(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.PASSPHRASE_TIMEOUT_PREF, z).apply();
        refresh();
    }

    public final void setObsoletePasswordTimeout(int i) {
        TextSecurePreferences.setPassphraseTimeoutInterval(ApplicationDependencies.getApplication(), i);
        refresh();
    }

    public final void setStoriesEnabled(boolean z) {
        SignalStore.storyValues().setFeatureDisabled(!z);
        refresh();
    }

    public final void refresh() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PrivacySettingsViewModel.this.updateState((PrivacySettingsState) obj);
            }
        });
    }

    private final PrivacySettingsState getState() {
        boolean isReadReceiptsEnabled = TextSecurePreferences.isReadReceiptsEnabled(ApplicationDependencies.getApplication());
        boolean isTypingIndicatorsEnabled = TextSecurePreferences.isTypingIndicatorsEnabled(ApplicationDependencies.getApplication());
        boolean isScreenLockEnabled = TextSecurePreferences.isScreenLockEnabled(ApplicationDependencies.getApplication());
        long screenLockTimeout = TextSecurePreferences.getScreenLockTimeout(ApplicationDependencies.getApplication());
        boolean isScreenSecurityEnabled = TextSecurePreferences.isScreenSecurityEnabled(ApplicationDependencies.getApplication());
        boolean isIncognitoKeyboardEnabled = TextSecurePreferences.isIncognitoKeyboardEnabled(ApplicationDependencies.getApplication());
        PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode = SignalStore.phoneNumberPrivacy().getPhoneNumberSharingMode();
        PhoneNumberPrivacyValues.PhoneNumberListingMode phoneNumberListingMode = SignalStore.phoneNumberPrivacy().getPhoneNumberListingMode();
        Intrinsics.checkNotNullExpressionValue(phoneNumberSharingMode, "phoneNumberSharingMode");
        Intrinsics.checkNotNullExpressionValue(phoneNumberListingMode, "phoneNumberListingMode");
        return new PrivacySettingsState(0, phoneNumberSharingMode, phoneNumberListingMode, isReadReceiptsEnabled, isTypingIndicatorsEnabled, isScreenLockEnabled, screenLockTimeout, isScreenSecurityEnabled, isIncognitoKeyboardEnabled, !TextSecurePreferences.isPasswordDisabled(ApplicationDependencies.getApplication()), TextSecurePreferences.isPassphraseTimeoutEnabled(ApplicationDependencies.getApplication()), TextSecurePreferences.getPassphraseTimeoutInterval(ApplicationDependencies.getApplication()), SignalStore.settings().getUniversalExpireTimer(), CollectionsKt__CollectionsKt.emptyList(), !SignalStore.storyValues().isFeatureDisabled());
    }

    public final PrivacySettingsState updateState(PrivacySettingsState privacySettingsState) {
        return r0.copy((r33 & 1) != 0 ? r0.blockedCount : privacySettingsState.getBlockedCount(), (r33 & 2) != 0 ? r0.seeMyPhoneNumber : null, (r33 & 4) != 0 ? r0.findMeByPhoneNumber : null, (r33 & 8) != 0 ? r0.readReceipts : false, (r33 & 16) != 0 ? r0.typingIndicators : false, (r33 & 32) != 0 ? r0.screenLock : false, (r33 & 64) != 0 ? r0.screenLockActivityTimeout : 0, (r33 & 128) != 0 ? r0.screenSecurity : false, (r33 & 256) != 0 ? r0.incognitoKeyboard : false, (r33 & 512) != 0 ? r0.isObsoletePasswordEnabled : false, (r33 & 1024) != 0 ? r0.isObsoletePasswordTimeoutEnabled : false, (r33 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r0.obsoletePasswordTimeout : 0, (r33 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r0.universalExpireTimer : 0, (r33 & 8192) != 0 ? r0.privateStories : privacySettingsState.getPrivateStories(), (r33 & 16384) != 0 ? getState().isStoriesEnabled : false);
    }

    /* compiled from: PrivacySettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "sharedPreferences", "Landroid/content/SharedPreferences;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsRepository;", "(Landroid/content/SharedPreferences;Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final PrivacySettingsRepository repository;
        private final SharedPreferences sharedPreferences;

        public Factory(SharedPreferences sharedPreferences, PrivacySettingsRepository privacySettingsRepository) {
            Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
            Intrinsics.checkNotNullParameter(privacySettingsRepository, "repository");
            this.sharedPreferences = sharedPreferences;
            this.repository = privacySettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new PrivacySettingsViewModel(this.sharedPreferences, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
