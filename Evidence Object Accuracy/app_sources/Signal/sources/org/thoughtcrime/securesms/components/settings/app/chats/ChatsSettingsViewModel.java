package org.thoughtcrime.securesms.components.settings.app.chats;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.BackupUtil;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.ThrottledDebouncer;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: ChatsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u0016B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0014\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0015\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsRepository;)V", "refreshDebouncer", "Lorg/thoughtcrime/securesms/util/ThrottledDebouncer;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "refresh", "", "setEnterKeySends", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "setGenerateLinkPreviewsEnabled", "setUseAddressBook", "setUseSystemEmoji", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatsSettingsViewModel extends ViewModel {
    private final ThrottledDebouncer refreshDebouncer = new ThrottledDebouncer(500);
    private final ChatsSettingsRepository repository;
    private final LiveData<ChatsSettingsState> state;
    private final Store<ChatsSettingsState> store;

    public ChatsSettingsViewModel(ChatsSettingsRepository chatsSettingsRepository) {
        Intrinsics.checkNotNullParameter(chatsSettingsRepository, "repository");
        this.repository = chatsSettingsRepository;
        Store<ChatsSettingsState> store = new Store<>(new ChatsSettingsState(SignalStore.settings().isLinkPreviewsEnabled(), SignalStore.settings().isPreferSystemContactPhotos(), SignalStore.settings().isPreferSystemEmoji(), SignalStore.settings().isEnterKeySends(), SignalStore.settings().isBackupEnabled() && BackupUtil.canUserAccessBackupDirectory(ApplicationDependencies.getApplication())));
        this.store = store;
        LiveData<ChatsSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<ChatsSettingsState> getState() {
        return this.state;
    }

    /* renamed from: setGenerateLinkPreviewsEnabled$lambda-0 */
    public static final ChatsSettingsState m636setGenerateLinkPreviewsEnabled$lambda0(boolean z, ChatsSettingsState chatsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(chatsSettingsState, "it");
        return ChatsSettingsState.copy$default(chatsSettingsState, z, false, false, false, false, 30, null);
    }

    public final void setGenerateLinkPreviewsEnabled(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatsSettingsViewModel.m636setGenerateLinkPreviewsEnabled$lambda0(this.f$0, (ChatsSettingsState) obj);
            }
        });
        SignalStore.settings().setLinkPreviewsEnabled(z);
        this.repository.syncLinkPreviewsState();
    }

    /* renamed from: setUseAddressBook$lambda-1 */
    public static final ChatsSettingsState m637setUseAddressBook$lambda1(boolean z, ChatsSettingsState chatsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(chatsSettingsState, "it");
        return ChatsSettingsState.copy$default(chatsSettingsState, false, z, false, false, false, 29, null);
    }

    public final void setUseAddressBook(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatsSettingsViewModel.m637setUseAddressBook$lambda1(this.f$0, (ChatsSettingsState) obj);
            }
        });
        this.refreshDebouncer.publish(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                ChatsSettingsViewModel.m638setUseAddressBook$lambda2();
            }
        });
        SignalStore.settings().setPreferSystemContactPhotos(z);
        this.repository.syncPreferSystemContactPhotos();
    }

    /* renamed from: setUseAddressBook$lambda-2 */
    public static final void m638setUseAddressBook$lambda2() {
        ConversationUtil.refreshRecipientShortcuts();
    }

    /* renamed from: setUseSystemEmoji$lambda-3 */
    public static final ChatsSettingsState m639setUseSystemEmoji$lambda3(boolean z, ChatsSettingsState chatsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(chatsSettingsState, "it");
        return ChatsSettingsState.copy$default(chatsSettingsState, false, false, z, false, false, 27, null);
    }

    public final void setUseSystemEmoji(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatsSettingsViewModel.m639setUseSystemEmoji$lambda3(this.f$0, (ChatsSettingsState) obj);
            }
        });
        SignalStore.settings().setPreferSystemEmoji(z);
    }

    /* renamed from: setEnterKeySends$lambda-4 */
    public static final ChatsSettingsState m635setEnterKeySends$lambda4(boolean z, ChatsSettingsState chatsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(chatsSettingsState, "it");
        return ChatsSettingsState.copy$default(chatsSettingsState, false, false, false, z, false, 23, null);
    }

    public final void setEnterKeySends(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel$$ExternalSyntheticLambda5
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ChatsSettingsViewModel.m635setEnterKeySends$lambda4(this.f$0, (ChatsSettingsState) obj);
            }
        });
        SignalStore.settings().setEnterKeySends(z);
    }

    public final void refresh() {
        boolean z = SignalStore.settings().isBackupEnabled() && BackupUtil.canUserAccessBackupDirectory(ApplicationDependencies.getApplication());
        if (this.store.getState().getChatBackupsEnabled() != z) {
            this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel$$ExternalSyntheticLambda4
                public final /* synthetic */ boolean f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ChatsSettingsViewModel.m634refresh$lambda5(this.f$0, (ChatsSettingsState) obj);
                }
            });
        }
    }

    /* renamed from: refresh$lambda-5 */
    public static final ChatsSettingsState m634refresh$lambda5(boolean z, ChatsSettingsState chatsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(chatsSettingsState, "it");
        return ChatsSettingsState.copy$default(chatsSettingsState, false, false, false, false, z, 15, null);
    }

    /* compiled from: ChatsSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final ChatsSettingsRepository repository;

        public Factory(ChatsSettingsRepository chatsSettingsRepository) {
            Intrinsics.checkNotNullParameter(chatsSettingsRepository, "repository");
            this.repository = chatsSettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ChatsSettingsViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
