package org.thoughtcrime.securesms.components.voice;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Process;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import androidx.core.content.ContextCompat;
import androidx.media.MediaBrowserServiceCompat;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.device.DeviceInfo;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoSize;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageId;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceViewedUpdateJob;
import org.thoughtcrime.securesms.jobs.SendViewedReceiptJob;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.service.KeyCachingService;

/* loaded from: classes4.dex */
public class VoiceNotePlaybackService extends MediaBrowserServiceCompat {
    public static final String ACTION_NEXT_PLAYBACK_SPEED;
    public static final String ACTION_SET_AUDIO_STREAM;
    private static final String EMPTY_ROOT_ID;
    private static final int LOAD_MORE_THRESHOLD;
    private static final long SUPPORTED_ACTIONS;
    private static final String TAG = Log.tag(VoiceNotePlaybackService.class);
    private BecomingNoisyReceiver becomingNoisyReceiver;
    private boolean isForegroundService;
    private KeyClearedReceiver keyClearedReceiver;
    private MediaSessionCompat mediaSession;
    private MediaSessionConnector mediaSessionConnector;
    private VoiceNotePlayer player;
    private VoiceNoteNotificationManager voiceNoteNotificationManager;
    private VoiceNotePlaybackParameters voiceNotePlaybackParameters;
    private VoiceNotePlaybackPreparer voiceNotePlaybackPreparer;

    @Override // androidx.media.MediaBrowserServiceCompat, android.app.Service
    public void onCreate() {
        super.onCreate();
        MediaSessionCompat mediaSessionCompat = new MediaSessionCompat(this, TAG);
        this.mediaSession = mediaSessionCompat;
        this.voiceNotePlaybackParameters = new VoiceNotePlaybackParameters(mediaSessionCompat);
        this.mediaSessionConnector = new MediaSessionConnector(this.mediaSession);
        this.becomingNoisyReceiver = new BecomingNoisyReceiver(this, this.mediaSession.getSessionToken());
        this.keyClearedReceiver = new KeyClearedReceiver(this, this.mediaSession.getSessionToken());
        this.player = new VoiceNotePlayer(this);
        this.voiceNoteNotificationManager = new VoiceNoteNotificationManager(this, this.mediaSession.getSessionToken(), new VoiceNoteNotificationManagerListener());
        this.voiceNotePlaybackPreparer = new VoiceNotePlaybackPreparer(this, this.player, this.voiceNotePlaybackParameters);
        this.player.addListener((Player.Listener) new VoiceNotePlayerEventListener());
        this.mediaSessionConnector.setPlayer(this.player);
        this.mediaSessionConnector.setEnabledPlaybackActions(SUPPORTED_ACTIONS);
        this.mediaSessionConnector.setPlaybackPreparer(this.voiceNotePlaybackPreparer);
        this.mediaSessionConnector.setQueueNavigator(new VoiceNoteQueueNavigator(this.mediaSession));
        this.mediaSessionConnector.registerCustomCommandReceiver(new VoiceNotePlaybackController(this.player.getInternalPlayer(), this.voiceNotePlaybackParameters));
        setSessionToken(this.mediaSession.getSessionToken());
        this.mediaSession.setActive(true);
        this.keyClearedReceiver.register();
    }

    @Override // android.app.Service
    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        this.player.stop();
        this.player.clearMediaItems();
    }

    @Override // android.app.Service
    public void onDestroy() {
        super.onDestroy();
        this.mediaSession.setActive(false);
        this.mediaSession.release();
        this.becomingNoisyReceiver.unregister();
        this.keyClearedReceiver.unregister();
        this.player.release();
    }

    @Override // androidx.media.MediaBrowserServiceCompat
    public MediaBrowserServiceCompat.BrowserRoot onGetRoot(String str, int i, Bundle bundle) {
        if (i == Process.myUid()) {
            return new MediaBrowserServiceCompat.BrowserRoot(EMPTY_ROOT_ID, null);
        }
        return null;
    }

    @Override // androidx.media.MediaBrowserServiceCompat
    public void onLoadChildren(String str, MediaBrowserServiceCompat.Result<List<MediaBrowserCompat.MediaItem>> result) {
        result.sendResult(Collections.emptyList());
    }

    /* loaded from: classes4.dex */
    private class VoiceNotePlayerEventListener implements Player.Listener {
        public /* bridge */ /* synthetic */ void onAudioSessionIdChanged(int i) {
            Player.Listener.CC.$default$onAudioSessionIdChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onAvailableCommandsChanged(Player.Commands commands) {
            Player.Listener.CC.$default$onAvailableCommandsChanged(this, commands);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.text.TextOutput
        public /* bridge */ /* synthetic */ void onCues(List list) {
            Player.Listener.CC.$default$onCues(this, list);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
        public /* bridge */ /* synthetic */ void onDeviceInfoChanged(DeviceInfo deviceInfo) {
            Player.Listener.CC.$default$onDeviceInfoChanged(this, deviceInfo);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
        public /* bridge */ /* synthetic */ void onDeviceVolumeChanged(int i, boolean z) {
            Player.Listener.CC.$default$onDeviceVolumeChanged(this, i, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onEvents(Player player, Player.Events events) {
            Player.Listener.CC.$default$onEvents(this, player, events);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onIsLoadingChanged(boolean z) {
            Player.Listener.CC.$default$onIsLoadingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onIsPlayingChanged(boolean z) {
            Player.Listener.CC.$default$onIsPlayingChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onLoadingChanged(boolean z) {
            Player.EventListener.CC.$default$onLoadingChanged(this, z);
        }

        public /* bridge */ /* synthetic */ void onMaxSeekToPreviousPositionChanged(int i) {
            Player.EventListener.CC.$default$onMaxSeekToPreviousPositionChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onMediaItemTransition(MediaItem mediaItem, int i) {
            Player.Listener.CC.$default$onMediaItemTransition(this, mediaItem, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
            Player.Listener.CC.$default$onMediaMetadataChanged(this, mediaMetadata);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.metadata.MetadataOutput
        public /* bridge */ /* synthetic */ void onMetadata(Metadata metadata) {
            Player.Listener.CC.$default$onMetadata(this, metadata);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            Player.Listener.CC.$default$onPlaybackParametersChanged(this, playbackParameters);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
            Player.Listener.CC.$default$onPlaybackSuppressionReasonChanged(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlayerErrorChanged(PlaybackException playbackException) {
            Player.Listener.CC.$default$onPlayerErrorChanged(this, playbackException);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onPlayerStateChanged(boolean z, int i) {
            Player.EventListener.CC.$default$onPlayerStateChanged(this, z, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onPlaylistMetadataChanged(MediaMetadata mediaMetadata) {
            Player.Listener.CC.$default$onPlaylistMetadataChanged(this, mediaMetadata);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onPositionDiscontinuity(int i) {
            Player.EventListener.CC.$default$onPositionDiscontinuity(this, i);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* bridge */ /* synthetic */ void onRenderedFirstFrame() {
            Player.Listener.CC.$default$onRenderedFirstFrame(this);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onRepeatModeChanged(int i) {
            Player.Listener.CC.$default$onRepeatModeChanged(this, i);
        }

        public /* bridge */ /* synthetic */ void onSeekBackIncrementChanged(long j) {
            Player.Listener.CC.$default$onSeekBackIncrementChanged(this, j);
        }

        public /* bridge */ /* synthetic */ void onSeekForwardIncrementChanged(long j) {
            Player.Listener.CC.$default$onSeekForwardIncrementChanged(this, j);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onSeekProcessed() {
            Player.EventListener.CC.$default$onSeekProcessed(this);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onShuffleModeEnabledChanged(boolean z) {
            Player.Listener.CC.$default$onShuffleModeEnabledChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* bridge */ /* synthetic */ void onSkipSilenceEnabledChanged(boolean z) {
            Player.Listener.CC.$default$onSkipSilenceEnabledChanged(this, z);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onStaticMetadataChanged(List list) {
            Player.EventListener.CC.$default$onStaticMetadataChanged(this, list);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* bridge */ /* synthetic */ void onSurfaceSizeChanged(int i, int i2) {
            Player.Listener.CC.$default$onSurfaceSizeChanged(this, i, i2);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onTimelineChanged(Timeline timeline, int i) {
            Player.Listener.CC.$default$onTimelineChanged(this, timeline, i);
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public /* bridge */ /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            Player.Listener.CC.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
        }

        @Override // com.google.android.exoplayer2.video.VideoListener
        @Deprecated
        public /* bridge */ /* synthetic */ void onVideoSizeChanged(int i, int i2, int i3, float f) {
            VideoListener.CC.$default$onVideoSizeChanged(this, i, i2, i3, f);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
        public /* bridge */ /* synthetic */ void onVideoSizeChanged(VideoSize videoSize) {
            Player.Listener.CC.$default$onVideoSizeChanged(this, videoSize);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public /* bridge */ /* synthetic */ void onVolumeChanged(float f) {
            Player.Listener.CC.$default$onVolumeChanged(this, f);
        }

        private VoiceNotePlayerEventListener() {
            VoiceNotePlaybackService.this = r1;
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPlayWhenReadyChanged(boolean z, int i) {
            onPlaybackStateChanged(z, VoiceNotePlaybackService.this.player.getPlaybackState());
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPlaybackStateChanged(int i) {
            onPlaybackStateChanged(VoiceNotePlaybackService.this.player.getPlayWhenReady(), i);
        }

        private void onPlaybackStateChanged(boolean z, int i) {
            if (i == 2 || i == 3) {
                VoiceNotePlaybackService.this.voiceNoteNotificationManager.showNotification(VoiceNotePlaybackService.this.player);
                if (!z) {
                    VoiceNotePlaybackService.this.stopForeground(false);
                    VoiceNotePlaybackService.this.isForegroundService = false;
                    VoiceNotePlaybackService.this.becomingNoisyReceiver.unregister();
                    return;
                }
                VoiceNotePlaybackService.this.sendViewedReceiptForCurrentWindowIndex();
                VoiceNotePlaybackService.this.becomingNoisyReceiver.register();
                return;
            }
            VoiceNotePlaybackService.this.becomingNoisyReceiver.unregister();
            VoiceNotePlaybackService.this.voiceNoteNotificationManager.hideNotification();
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPositionDiscontinuity(Player.PositionInfo positionInfo, Player.PositionInfo positionInfo2, int i) {
            int i2 = positionInfo2.windowIndex;
            if (i2 != -1) {
                boolean z = false;
                if (i == 0) {
                    VoiceNotePlaybackService.this.sendViewedReceiptForCurrentWindowIndex();
                    MediaItem currentMediaItem = VoiceNotePlaybackService.this.player.getCurrentMediaItem();
                    if (!(currentMediaItem == null || currentMediaItem.playbackProperties == null)) {
                        String str = VoiceNotePlaybackService.TAG;
                        Log.d(str, "onPositionDiscontinuity: current window uri: " + currentMediaItem.playbackProperties.uri);
                    }
                    PlaybackParameters playbackParametersForWindowPosition = VoiceNotePlaybackService.this.getPlaybackParametersForWindowPosition(i2);
                    if ((playbackParametersForWindowPosition != null ? playbackParametersForWindowPosition.speed : 1.0f) != VoiceNotePlaybackService.this.player.getPlaybackParameters().speed) {
                        VoiceNotePlaybackService.this.player.setPlayWhenReady(false);
                        if (playbackParametersForWindowPosition != null) {
                            VoiceNotePlaybackService.this.player.setPlaybackParameters(playbackParametersForWindowPosition);
                        }
                        VoiceNotePlaybackService.this.player.seekTo(i2, 1);
                        VoiceNotePlaybackService.this.player.setPlayWhenReady(true);
                    }
                }
                if (i2 < 2 || i2 + 2 >= VoiceNotePlaybackService.this.player.getMediaItemCount()) {
                    z = true;
                }
                if (z && i2 % 2 == 0) {
                    VoiceNotePlaybackService.this.voiceNotePlaybackPreparer.loadMoreVoiceNotes();
                }
            }
        }

        @Override // com.google.android.exoplayer2.Player.EventListener
        public void onPlayerError(PlaybackException playbackException) {
            Log.w(VoiceNotePlaybackService.TAG, "ExoPlayer error occurred:", playbackException);
        }

        @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
        public void onAudioAttributesChanged(AudioAttributes audioAttributes) {
            int i = audioAttributes.usage == 2 ? 0 : 3;
            String str = VoiceNotePlaybackService.TAG;
            Log.i(str, "onAudioAttributesChanged: Setting audio stream to " + i);
            VoiceNotePlaybackService.this.mediaSession.setPlaybackToLocal(i);
        }
    }

    public PlaybackParameters getPlaybackParametersForWindowPosition(int i) {
        if (isAudioMessage(i)) {
            return this.voiceNotePlaybackParameters.getParameters();
        }
        return null;
    }

    private boolean isAudioMessage(int i) {
        return i % 2 == 0;
    }

    public void sendViewedReceiptForCurrentWindowIndex() {
        MediaItem currentMediaItem;
        MediaItem.PlaybackProperties playbackProperties;
        if (this.player.getPlaybackState() == 3 && this.player.getPlayWhenReady() && this.player.getCurrentWindowIndex() != -1 && (currentMediaItem = this.player.getCurrentMediaItem()) != null && (playbackProperties = currentMediaItem.playbackProperties) != null && playbackProperties.uri.getScheme().equals("content")) {
            SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackService$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    VoiceNotePlaybackService.lambda$sendViewedReceiptForCurrentWindowIndex$0(MediaItem.this);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$sendViewedReceiptForCurrentWindowIndex$0(MediaItem mediaItem) {
        Bundle bundle = mediaItem.mediaMetadata.extras;
        if (bundle != null) {
            long j = bundle.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_ID);
            RecipientId from = RecipientId.from(bundle.getString(VoiceNoteMediaItemFactory.EXTRA_INDIVIDUAL_RECIPIENT_ID));
            MessageDatabase.MarkedMessageInfo incomingMessageViewed = SignalDatabase.mms().setIncomingMessageViewed(j);
            if (incomingMessageViewed != null) {
                ApplicationDependencies.getJobManager().add(new SendViewedReceiptJob(incomingMessageViewed.getThreadId(), from, incomingMessageViewed.getSyncMessageId().getTimetamp(), new MessageId(j, true)));
                MultiDeviceViewedUpdateJob.enqueue(Collections.singletonList(incomingMessageViewed.getSyncMessageId()));
            }
        }
    }

    /* loaded from: classes4.dex */
    private class VoiceNoteNotificationManagerListener implements PlayerNotificationManager.NotificationListener {
        private VoiceNoteNotificationManagerListener() {
            VoiceNotePlaybackService.this = r1;
        }

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.NotificationListener
        public void onNotificationPosted(int i, Notification notification, boolean z) {
            if (z && !VoiceNotePlaybackService.this.isForegroundService) {
                ContextCompat.startForegroundService(VoiceNotePlaybackService.this.getApplicationContext(), new Intent(VoiceNotePlaybackService.this.getApplicationContext(), VoiceNotePlaybackService.class));
                VoiceNotePlaybackService.this.startForeground(i, notification);
                VoiceNotePlaybackService.this.isForegroundService = true;
            }
        }

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.NotificationListener
        public void onNotificationCancelled(int i, boolean z) {
            VoiceNotePlaybackService.this.stopForeground(true);
            VoiceNotePlaybackService.this.isForegroundService = false;
            VoiceNotePlaybackService.this.stopSelf();
        }
    }

    /* loaded from: classes4.dex */
    private static class KeyClearedReceiver extends BroadcastReceiver {
        private static final IntentFilter KEY_CLEARED_FILTER = new IntentFilter(KeyCachingService.CLEAR_KEY_EVENT);
        private final Context context;
        private final MediaControllerCompat controller;
        private boolean registered;

        private KeyClearedReceiver(Context context, MediaSessionCompat.Token token) {
            this.context = context;
            this.controller = new MediaControllerCompat(context, token);
        }

        void register() {
            if (!this.registered) {
                this.context.registerReceiver(this, KEY_CLEARED_FILTER);
                this.registered = true;
            }
        }

        void unregister() {
            if (this.registered) {
                this.context.unregisterReceiver(this);
                this.registered = false;
            }
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            this.controller.getTransportControls().stop();
        }
    }

    /* loaded from: classes4.dex */
    public static class BecomingNoisyReceiver extends BroadcastReceiver {
        private static final IntentFilter NOISY_INTENT_FILTER = new IntentFilter("android.media.AUDIO_BECOMING_NOISY");
        private final Context context;
        private final MediaControllerCompat controller;
        private boolean registered;

        private BecomingNoisyReceiver(Context context, MediaSessionCompat.Token token) {
            this.context = context;
            this.controller = new MediaControllerCompat(context, token);
        }

        void register() {
            if (!this.registered) {
                this.context.registerReceiver(this, NOISY_INTENT_FILTER);
                this.registered = true;
            }
        }

        void unregister() {
            if (this.registered) {
                this.context.unregisterReceiver(this);
                this.registered = false;
            }
        }

        @Override // android.content.BroadcastReceiver
        public void onReceive(Context context, Intent intent) {
            if ("android.media.AUDIO_BECOMING_NOISY".equals(intent.getAction())) {
                this.controller.getTransportControls().pause();
            }
        }
    }
}
