package org.thoughtcrime.securesms.components.settings.app.subscription.thanks;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.badges.models.Badge;

/* loaded from: classes4.dex */
public class ThanksForYourSupportBottomSheetDialogFragmentArgs {
    private final HashMap arguments;

    private ThanksForYourSupportBottomSheetDialogFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ThanksForYourSupportBottomSheetDialogFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ThanksForYourSupportBottomSheetDialogFragmentArgs fromBundle(Bundle bundle) {
        ThanksForYourSupportBottomSheetDialogFragmentArgs thanksForYourSupportBottomSheetDialogFragmentArgs = new ThanksForYourSupportBottomSheetDialogFragmentArgs();
        bundle.setClassLoader(ThanksForYourSupportBottomSheetDialogFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("badge")) {
            throw new IllegalArgumentException("Required argument \"badge\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(Badge.class) || Serializable.class.isAssignableFrom(Badge.class)) {
            Badge badge = (Badge) bundle.get("badge");
            if (badge != null) {
                thanksForYourSupportBottomSheetDialogFragmentArgs.arguments.put("badge", badge);
                if (bundle.containsKey("isBoost")) {
                    thanksForYourSupportBottomSheetDialogFragmentArgs.arguments.put("isBoost", Boolean.valueOf(bundle.getBoolean("isBoost")));
                } else {
                    thanksForYourSupportBottomSheetDialogFragmentArgs.arguments.put("isBoost", Boolean.FALSE);
                }
                return thanksForYourSupportBottomSheetDialogFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public Badge getBadge() {
        return (Badge) this.arguments.get("badge");
    }

    public boolean getIsBoost() {
        return ((Boolean) this.arguments.get("isBoost")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("badge")) {
            Badge badge = (Badge) this.arguments.get("badge");
            if (Parcelable.class.isAssignableFrom(Badge.class) || badge == null) {
                bundle.putParcelable("badge", (Parcelable) Parcelable.class.cast(badge));
            } else if (Serializable.class.isAssignableFrom(Badge.class)) {
                bundle.putSerializable("badge", (Serializable) Serializable.class.cast(badge));
            } else {
                throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("isBoost")) {
            bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
        } else {
            bundle.putBoolean("isBoost", false);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ThanksForYourSupportBottomSheetDialogFragmentArgs thanksForYourSupportBottomSheetDialogFragmentArgs = (ThanksForYourSupportBottomSheetDialogFragmentArgs) obj;
        if (this.arguments.containsKey("badge") != thanksForYourSupportBottomSheetDialogFragmentArgs.arguments.containsKey("badge")) {
            return false;
        }
        if (getBadge() == null ? thanksForYourSupportBottomSheetDialogFragmentArgs.getBadge() == null : getBadge().equals(thanksForYourSupportBottomSheetDialogFragmentArgs.getBadge())) {
            return this.arguments.containsKey("isBoost") == thanksForYourSupportBottomSheetDialogFragmentArgs.arguments.containsKey("isBoost") && getIsBoost() == thanksForYourSupportBottomSheetDialogFragmentArgs.getIsBoost();
        }
        return false;
    }

    public int hashCode() {
        return (((getBadge() != null ? getBadge().hashCode() : 0) + 31) * 31) + (getIsBoost() ? 1 : 0);
    }

    public String toString() {
        return "ThanksForYourSupportBottomSheetDialogFragmentArgs{badge=" + getBadge() + ", isBoost=" + getIsBoost() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ThanksForYourSupportBottomSheetDialogFragmentArgs thanksForYourSupportBottomSheetDialogFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(thanksForYourSupportBottomSheetDialogFragmentArgs.arguments);
        }

        public Builder(Badge badge) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (badge != null) {
                hashMap.put("badge", badge);
                return;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ThanksForYourSupportBottomSheetDialogFragmentArgs build() {
            return new ThanksForYourSupportBottomSheetDialogFragmentArgs(this.arguments);
        }

        public Builder setBadge(Badge badge) {
            if (badge != null) {
                this.arguments.put("badge", badge);
                return this;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public Builder setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        public Badge getBadge() {
            return (Badge) this.arguments.get("badge");
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }
    }
}
