package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: NotificationProfileNamePreset.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileNamePreset;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileNamePreset {
    public static final NotificationProfileNamePreset INSTANCE = new NotificationProfileNamePreset();

    private NotificationProfileNamePreset() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m814register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileNamePreset$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return NotificationProfileNamePreset.m814register$lambda0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.about_preset_item));
    }

    /* compiled from: NotificationProfileNamePreset.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B+\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\tJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0000H\u0016J\u0010\u0010\u0013\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileNamePreset$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "emoji", "", "bodyResource", "", "onClick", "Lkotlin/Function1;", "", "(Ljava/lang/String;ILkotlin/jvm/functions/Function1;)V", "getBodyResource", "()I", "getEmoji", "()Ljava/lang/String;", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final int bodyResource;
        private final String emoji;
        private final Function1<Model, Unit> onClick;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileNamePreset$Model, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        public Model(String str, int i, Function1<? super Model, Unit> function1) {
            Intrinsics.checkNotNullParameter(str, "emoji");
            Intrinsics.checkNotNullParameter(function1, "onClick");
            this.emoji = str;
            this.bodyResource = i;
            this.onClick = function1;
        }

        public final int getBodyResource() {
            return this.bodyResource;
        }

        public final String getEmoji() {
            return this.emoji;
        }

        public final Function1<Model, Unit> getOnClick() {
            return this.onClick;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.bodyResource == model.bodyResource;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return areItemsTheSame(model) && Intrinsics.areEqual(this.emoji, model.emoji);
        }
    }

    /* compiled from: NotificationProfileNamePreset.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileNamePreset$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileNamePreset$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "body", "Landroid/widget/TextView;", "getBody", "()Landroid/widget/TextView;", "emoji", "Landroid/widget/ImageView;", "getEmoji", "()Landroid/widget/ImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final TextView body;
        private final ImageView emoji;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.about_preset_emoji);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.about_preset_emoji)");
            this.emoji = (ImageView) findViewById;
            View findViewById2 = findViewById(R.id.about_preset_body);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.about_preset_body)");
            this.body = (TextView) findViewById2;
        }

        public final ImageView getEmoji() {
            return this.emoji;
        }

        public final TextView getBody() {
            return this.body;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m815bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke(model);
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setOnClickListener(new NotificationProfileNamePreset$ViewHolder$$ExternalSyntheticLambda0(model));
            this.emoji.setImageDrawable(EmojiUtil.convertToDrawable(this.context, model.getEmoji()));
            this.body.setText(model.getBodyResource());
        }
    }
}
