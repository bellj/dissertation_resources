package org.thoughtcrime.securesms.components;

import android.animation.Animator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.value.LottieFrameInfo;
import com.airbnb.lottie.value.SimpleLottieValueCallback;
import j$.util.Optional;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.components.PlaybackSpeedToggleTextView;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.ExpiringMessageManager;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.SignalLocalMetrics;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionInfoCompat;
import org.thoughtcrime.securesms.util.dualsim.SubscriptionManagerCompat;

/* loaded from: classes4.dex */
public class ConversationItemFooter extends ConstraintLayout {
    private TextView audioDuration;
    private TextView dateView;
    private DeliveryStatusView deliveryStatusView;
    private boolean hasShrunkDate;
    private ImageView insecureIndicatorView;
    private boolean isOutgoing;
    private OnTouchDelegateChangedListener onTouchDelegateChangedListener;
    private boolean onlyShowSendingStatus;
    private PlaybackSpeedToggleTextView playbackSpeedToggleTextView;
    private long previousMessageId;
    private LottieAnimationView revealDot;
    private TextView simView;
    private final Rect speedToggleHitRect = new Rect();
    private ExpirationTimerView timerView;
    private final int touchTargetSize = ViewUtil.dpToPx(48);

    /* loaded from: classes4.dex */
    public interface OnTouchDelegateChangedListener {
        void onTouchDelegateChanged(Rect rect, View view);
    }

    public ConversationItemFooter(Context context) {
        super(context);
        init(null);
    }

    public ConversationItemFooter(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public ConversationItemFooter(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = attributeSet != null ? getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ConversationItemFooter, 0, 0) : null;
        int i = R.layout.conversation_item_footer_outgoing;
        boolean z = true;
        if (obtainStyledAttributes != null) {
            if (obtainStyledAttributes.getInt(1, 0) != 0) {
                z = false;
            }
            this.isOutgoing = z;
            if (!z) {
                i = R.layout.conversation_item_footer_incoming;
            }
        } else {
            this.isOutgoing = true;
        }
        ViewGroup.inflate(getContext(), i, this);
        this.dateView = (TextView) findViewById(R.id.footer_date);
        this.simView = (TextView) findViewById(R.id.footer_sim_info);
        this.timerView = (ExpirationTimerView) findViewById(R.id.footer_expiration_timer);
        this.insecureIndicatorView = (ImageView) findViewById(R.id.footer_insecure_indicator);
        this.deliveryStatusView = (DeliveryStatusView) findViewById(R.id.footer_delivery_status);
        this.audioDuration = (TextView) findViewById(R.id.footer_audio_duration);
        this.revealDot = (LottieAnimationView) findViewById(R.id.footer_revealed_dot);
        this.playbackSpeedToggleTextView = (PlaybackSpeedToggleTextView) findViewById(R.id.footer_audio_playback_speed_toggle);
        if (obtainStyledAttributes != null) {
            setTextColor(obtainStyledAttributes.getInt(3, getResources().getColor(R.color.core_white)));
            setIconColor(obtainStyledAttributes.getInt(0, getResources().getColor(R.color.core_white)));
            setRevealDotColor(obtainStyledAttributes.getInt(2, getResources().getColor(R.color.core_white)));
            obtainStyledAttributes.recycle();
        }
        this.dateView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() { // from class: org.thoughtcrime.securesms.components.ConversationItemFooter$$ExternalSyntheticLambda2
            @Override // android.view.View.OnLayoutChangeListener
            public final void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
                ConversationItemFooter.$r8$lambda$d5PnJPbZBbI3VDf4m7nfbYewyV4(ConversationItemFooter.this, view, i2, i3, i4, i5, i6, i7, i8, i9);
            }
        });
    }

    public /* synthetic */ void lambda$init$0(View view, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        if (i5 != i || i7 != i3) {
            notifyTouchDelegateChanged(getPlaybackSpeedToggleTouchDelegateRect(), this.playbackSpeedToggleTextView);
        }
    }

    public void setOnTouchDelegateChangedListener(OnTouchDelegateChangedListener onTouchDelegateChangedListener) {
        this.onTouchDelegateChangedListener = onTouchDelegateChangedListener;
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.timerView.stopAnimation();
    }

    public void setMessageRecord(MessageRecord messageRecord, Locale locale) {
        presentDate(messageRecord, locale);
        presentSimInfo(messageRecord);
        presentTimer(messageRecord);
        presentInsecureIndicator(messageRecord);
        presentDeliveryStatus(messageRecord);
        presentAudioDuration(messageRecord);
    }

    public void setAudioDuration(long j, long j2) {
        long max = Math.max(0L, TimeUnit.MILLISECONDS.toSeconds(j - j2));
        this.audioDuration.setText(getResources().getString(R.string.AudioView_duration, Long.valueOf(max / 60), Long.valueOf(max % 60)));
    }

    public void setPlaybackSpeedListener(PlaybackSpeedToggleTextView.PlaybackSpeedListener playbackSpeedListener) {
        this.playbackSpeedToggleTextView.setPlaybackSpeedListener(playbackSpeedListener);
    }

    public void setAudioPlaybackSpeed(float f, boolean z) {
        if (z) {
            showPlaybackSpeedToggle();
        } else {
            hidePlaybackSpeedToggle();
        }
        this.playbackSpeedToggleTextView.setCurrentSpeed(f);
    }

    public void setTextColor(int i) {
        this.dateView.setTextColor(i);
        this.simView.setTextColor(i);
        this.audioDuration.setTextColor(i);
    }

    public void setIconColor(int i) {
        this.timerView.setColorFilter(i, PorterDuff.Mode.SRC_IN);
        this.insecureIndicatorView.setColorFilter(i);
        this.deliveryStatusView.setTint(i);
    }

    public void setRevealDotColor(int i) {
        this.revealDot.addValueCallback(new KeyPath("**"), (KeyPath) LottieProperty.COLOR_FILTER, (SimpleLottieValueCallback<KeyPath>) new SimpleLottieValueCallback(i) { // from class: org.thoughtcrime.securesms.components.ConversationItemFooter$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.airbnb.lottie.value.SimpleLottieValueCallback
            public final Object getValue(LottieFrameInfo lottieFrameInfo) {
                return ConversationItemFooter.m475$r8$lambda$RBhIhvUK3S_s4FwfGrGJR7cik4(this.f$0, lottieFrameInfo);
            }
        });
    }

    public static /* synthetic */ ColorFilter lambda$setRevealDotColor$1(int i, LottieFrameInfo lottieFrameInfo) {
        return new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN);
    }

    public void setOnlyShowSendingStatus(boolean z, MessageRecord messageRecord) {
        this.onlyShowSendingStatus = z;
        presentDeliveryStatus(messageRecord);
    }

    public void enableBubbleBackground(int i, Integer num) {
        setBackgroundResource(i);
        if (num != null) {
            getBackground().setColorFilter(num.intValue(), PorterDuff.Mode.MULTIPLY);
        } else {
            getBackground().clearColorFilter();
        }
    }

    public void disableBubbleBackground() {
        setBackground(null);
    }

    public Projection getProjection(ViewGroup viewGroup) {
        if (getVisibility() == 0) {
            return Projection.relativeToParent(viewGroup, this, new Projection.Corners((float) ViewUtil.dpToPx(11)));
        }
        return null;
    }

    public TextView getDateView() {
        return this.dateView;
    }

    private void notifyTouchDelegateChanged(Rect rect, View view) {
        OnTouchDelegateChangedListener onTouchDelegateChangedListener = this.onTouchDelegateChangedListener;
        if (onTouchDelegateChangedListener != null) {
            onTouchDelegateChangedListener.onTouchDelegateChanged(rect, view);
        }
    }

    private void showPlaybackSpeedToggle() {
        if (!this.hasShrunkDate) {
            this.hasShrunkDate = true;
            this.playbackSpeedToggleTextView.animate().alpha(1.0f).scaleX(1.0f).scaleY(1.0f).setDuration(150).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.ConversationItemFooter.1
                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    ConversationItemFooter.this.playbackSpeedToggleTextView.setClickable(true);
                }
            });
            if (this.isOutgoing) {
                this.dateView.setMaxWidth(ViewUtil.dpToPx(32));
                return;
            }
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this);
            constraintSet.constrainMaxWidth(R.id.date_and_expiry_wrapper, ViewUtil.dpToPx(40));
            constraintSet.applyTo(this);
        }
    }

    private void hidePlaybackSpeedToggle() {
        if (this.hasShrunkDate) {
            this.hasShrunkDate = false;
            this.playbackSpeedToggleTextView.animate().alpha(0.0f).scaleX(0.5f).scaleY(0.5f).setDuration(150).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.ConversationItemFooter.2
                @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
                public void onAnimationEnd(Animator animator) {
                    ConversationItemFooter.this.playbackSpeedToggleTextView.setClickable(false);
                    ConversationItemFooter.this.playbackSpeedToggleTextView.clearRequestedSpeed();
                }
            });
            if (this.isOutgoing) {
                this.dateView.setMaxWidth(Integer.MAX_VALUE);
                return;
            }
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this);
            constraintSet.constrainMaxWidth(R.id.date_and_expiry_wrapper, -1);
            constraintSet.applyTo(this);
        }
    }

    private Rect getPlaybackSpeedToggleTouchDelegateRect() {
        this.playbackSpeedToggleTextView.getHitRect(this.speedToggleHitRect);
        int width = (this.touchTargetSize - this.speedToggleHitRect.width()) / 2;
        int height = (this.touchTargetSize - this.speedToggleHitRect.height()) / 2;
        Rect rect = this.speedToggleHitRect;
        rect.top -= height;
        rect.left -= width;
        rect.right += width;
        rect.bottom += height;
        return rect;
    }

    private void presentDate(MessageRecord messageRecord, Locale locale) {
        int i;
        this.dateView.forceLayout();
        if (messageRecord.isFailed()) {
            if (messageRecord.hasFailedWithNetworkFailures()) {
                i = R.string.ConversationItem_error_network_not_delivered;
            } else {
                i = (!messageRecord.getRecipient().isPushGroup() || !messageRecord.isIdentityMismatchFailure()) ? R.string.ConversationItem_error_not_sent_tap_for_details : R.string.ConversationItem_error_partially_not_delivered;
            }
            this.dateView.setText(i);
        } else if (messageRecord.isPendingInsecureSmsFallback()) {
            this.dateView.setText(R.string.ConversationItem_click_to_approve_unencrypted);
        } else if (messageRecord.isRateLimited()) {
            this.dateView.setText(R.string.ConversationItem_send_paused);
        } else {
            this.dateView.setText(DateUtils.getSimpleRelativeTimeSpanString(getContext(), locale, messageRecord.getTimestamp()));
        }
    }

    private void presentSimInfo(MessageRecord messageRecord) {
        SubscriptionManagerCompat subscriptionManagerCompat = new SubscriptionManagerCompat(getContext());
        if (messageRecord.isPush() || messageRecord.getSubscriptionId() == -1 || !Permissions.hasAll(getContext(), "android.permission.READ_PHONE_STATE") || !subscriptionManagerCompat.isMultiSim()) {
            this.simView.setVisibility(8);
            return;
        }
        Optional<SubscriptionInfoCompat> activeSubscriptionInfo = subscriptionManagerCompat.getActiveSubscriptionInfo(messageRecord.getSubscriptionId());
        if (activeSubscriptionInfo.isPresent() && messageRecord.isOutgoing()) {
            this.simView.setText(getContext().getString(R.string.ConversationItem_from_s, activeSubscriptionInfo.get().getDisplayName()));
            this.simView.setVisibility(0);
        } else if (activeSubscriptionInfo.isPresent()) {
            this.simView.setText(getContext().getString(R.string.ConversationItem_to_s, activeSubscriptionInfo.get().getDisplayName()));
            this.simView.setVisibility(0);
        } else {
            this.simView.setVisibility(8);
        }
    }

    private void presentTimer(MessageRecord messageRecord) {
        if (messageRecord.getExpiresIn() <= 0 || messageRecord.isPending()) {
            this.timerView.setVisibility(8);
            return;
        }
        this.timerView.setVisibility(0);
        this.timerView.setPercentComplete(0.0f);
        if (messageRecord.getExpireStarted() > 0) {
            this.timerView.setExpirationTime(messageRecord.getExpireStarted(), messageRecord.getExpiresIn());
            this.timerView.startAnimation();
            if (messageRecord.getExpireStarted() + messageRecord.getExpiresIn() <= System.currentTimeMillis()) {
                ApplicationDependencies.getExpiringMessageManager().checkSchedule();
            }
        } else if (!messageRecord.isOutgoing() && !messageRecord.isMediaPending()) {
            SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.ConversationItemFooter$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    ConversationItemFooter.$r8$lambda$CVKPnZ6CziAGDrJ3fG7PR639Uds(MessageRecord.this);
                }
            });
        }
    }

    public static /* synthetic */ void lambda$presentTimer$2(MessageRecord messageRecord) {
        ExpiringMessageManager expiringMessageManager = ApplicationDependencies.getExpiringMessageManager();
        long id = messageRecord.getId();
        boolean isMms = messageRecord.isMms();
        if (isMms) {
            SignalDatabase.mms().markExpireStarted(id);
        } else {
            SignalDatabase.sms().markExpireStarted(id);
        }
        expiringMessageManager.scheduleDeletion(id, isMms, messageRecord.getExpiresIn());
    }

    private void presentInsecureIndicator(MessageRecord messageRecord) {
        this.insecureIndicatorView.setVisibility(messageRecord.isSecure() ? 8 : 0);
    }

    private void presentDeliveryStatus(MessageRecord messageRecord) {
        long buildMessageId = buildMessageId(messageRecord);
        if (this.previousMessageId == buildMessageId && this.deliveryStatusView.isPending() && !messageRecord.isPending()) {
            if (messageRecord.getRecipient().isGroup()) {
                SignalLocalMetrics.GroupMessageSend.onUiUpdated(messageRecord.getId());
            } else {
                SignalLocalMetrics.IndividualMessageSend.onUiUpdated(messageRecord.getId());
            }
        }
        this.previousMessageId = buildMessageId;
        if (messageRecord.isFailed() || messageRecord.isPendingInsecureSmsFallback()) {
            this.deliveryStatusView.setNone();
        } else if (this.onlyShowSendingStatus) {
            if (!messageRecord.isOutgoing() || !messageRecord.isPending()) {
                this.deliveryStatusView.setNone();
            } else {
                this.deliveryStatusView.setPending();
            }
        } else if (!messageRecord.isOutgoing()) {
            this.deliveryStatusView.setNone();
        } else if (messageRecord.isPending()) {
            this.deliveryStatusView.setPending();
        } else if (messageRecord.isRemoteRead()) {
            this.deliveryStatusView.setRead();
        } else if (messageRecord.isDelivered()) {
            this.deliveryStatusView.setDelivered();
        } else {
            this.deliveryStatusView.setSent();
        }
    }

    private void presentAudioDuration(MessageRecord messageRecord) {
        if (!messageRecord.isMms()) {
            hideAudioDurationViews();
        } else if (((MmsMessageRecord) messageRecord).getSlideDeck().getAudioSlide() != null) {
            showAudioDurationViews();
            if (messageRecord.getViewedReceiptCount() > 0 || (messageRecord.isOutgoing() && Objects.equals(messageRecord.getRecipient(), Recipient.self()))) {
                this.revealDot.setProgress(1.0f);
            } else {
                this.revealDot.setProgress(0.0f);
            }
        } else {
            hideAudioDurationViews();
        }
    }

    private void showAudioDurationViews() {
        this.audioDuration.setVisibility(0);
        this.revealDot.setVisibility(0);
        this.playbackSpeedToggleTextView.setVisibility(0);
    }

    private void hideAudioDurationViews() {
        this.audioDuration.setVisibility(8);
        this.revealDot.setVisibility(8);
        this.playbackSpeedToggleTextView.setVisibility(8);
    }

    private long buildMessageId(MessageRecord messageRecord) {
        return messageRecord.isMms() ? -messageRecord.getId() : messageRecord.getId();
    }
}
