package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$IntRef;
import org.signal.core.util.logging.Log;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u0018\u0000 \t2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\tB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/RadioListPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/RadioListPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RadioListPreferenceViewHolder extends PreferenceViewHolder<RadioListPreference> {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(RadioListPreference.class);

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RadioListPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
    }

    public void bind(RadioListPreference radioListPreference) {
        Intrinsics.checkNotNullParameter(radioListPreference, "model");
        super.bind((RadioListPreferenceViewHolder) radioListPreference);
        if (radioListPreference.getSelected() >= 0) {
            getSummaryView().setVisibility(0);
            getSummaryView().setText(radioListPreference.getListItems()[radioListPreference.getSelected()]);
        } else {
            getSummaryView().setVisibility(8);
            String str = TAG;
            Log.w(str, "Detected a radio list without a default selection: " + radioListPreference.getDialogTitle());
        }
        this.itemView.setOnClickListener(new View.OnClickListener(radioListPreference) { // from class: org.thoughtcrime.securesms.components.settings.RadioListPreferenceViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ RadioListPreference f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RadioListPreferenceViewHolder.m538$r8$lambda$bNhpmIQ_B7wGajxB9iJit5ptwI(RadioListPreferenceViewHolder.this, this.f$1, view);
            }
        });
    }

    /* renamed from: bind$lambda-3 */
    public static final void m539bind$lambda3(RadioListPreferenceViewHolder radioListPreferenceViewHolder, RadioListPreference radioListPreference, View view) {
        Intrinsics.checkNotNullParameter(radioListPreferenceViewHolder, "this$0");
        Intrinsics.checkNotNullParameter(radioListPreference, "$model");
        Ref$IntRef ref$IntRef = new Ref$IntRef();
        ref$IntRef.element = -1;
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(radioListPreferenceViewHolder.context);
        DSLSettingsText dialogTitle = radioListPreference.getDialogTitle();
        Context context = radioListPreferenceViewHolder.context;
        Intrinsics.checkNotNullExpressionValue(context, "context");
        MaterialAlertDialogBuilder singleChoiceItems = materialAlertDialogBuilder.setTitle(dialogTitle.resolve(context)).setSingleChoiceItems((CharSequence[]) radioListPreference.getListItems(), radioListPreference.getSelected(), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(ref$IntRef) { // from class: org.thoughtcrime.securesms.components.settings.RadioListPreferenceViewHolder$$ExternalSyntheticLambda1
            public final /* synthetic */ Ref$IntRef f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                RadioListPreferenceViewHolder.m537$r8$lambda$ASb0aZjwdwwnZNwHqa4BsOxutk(RadioListPreference.this, this.f$1, dialogInterface, i);
            }
        });
        Intrinsics.checkNotNullExpressionValue(singleChoiceItems, "MaterialAlertDialogBuild…s()\n          }\n        }");
        if (radioListPreference.getConfirmAction()) {
            singleChoiceItems.setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(ref$IntRef) { // from class: org.thoughtcrime.securesms.components.settings.RadioListPreferenceViewHolder$$ExternalSyntheticLambda2
                public final /* synthetic */ Ref$IntRef f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    RadioListPreferenceViewHolder.$r8$lambda$9pLL0tabsc1ISjcUyZ5H9S7gdyY(RadioListPreference.this, this.f$1, dialogInterface, i);
                }
            }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.RadioListPreferenceViewHolder$$ExternalSyntheticLambda3
                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    RadioListPreferenceViewHolder.$r8$lambda$b9PMBR6XYRq2EUD2649NtoyGhdI(dialogInterface, i);
                }
            }).show();
        } else {
            singleChoiceItems.show();
        }
    }

    /* renamed from: bind$lambda-3$lambda-0 */
    public static final void m540bind$lambda3$lambda0(RadioListPreference radioListPreference, Ref$IntRef ref$IntRef, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(radioListPreference, "$model");
        Intrinsics.checkNotNullParameter(ref$IntRef, "$selection");
        if (radioListPreference.getConfirmAction()) {
            ref$IntRef.element = i;
            return;
        }
        radioListPreference.getOnSelected().invoke(Integer.valueOf(i));
        dialogInterface.dismiss();
    }

    /* renamed from: bind$lambda-3$lambda-1 */
    public static final void m541bind$lambda3$lambda1(RadioListPreference radioListPreference, Ref$IntRef ref$IntRef, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(radioListPreference, "$model");
        Intrinsics.checkNotNullParameter(ref$IntRef, "$selection");
        radioListPreference.getOnSelected().invoke(Integer.valueOf(ref$IntRef.element));
        dialogInterface.dismiss();
    }

    /* renamed from: bind$lambda-3$lambda-2 */
    public static final void m542bind$lambda3$lambda2(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    /* compiled from: DSLSettingsAdapter.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/RadioListPreferenceViewHolder$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
