package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.graphics.drawable.Drawable;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiParser;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.emoji.ObsoleteEmoji;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class EmojiUtil {
    private static final Pattern EMOJI_PATTERN = Pattern.compile("^(?:(?:[©®‼⁉™ℹ↔-↙↩-↪⌚-⌛⌨⏏⏩-⏳⏸-⏺Ⓜ▪-▫▶◀◻-◾☀-☄☎☑☔-☕☘☝☠☢-☣☦☪☮-☯☸-☺♈-♓♠♣♥-♦♨♻♿⚒-⚔⚖-⚗⚙⚛-⚜⚠-⚡⚪-⚫⚰-⚱⚽-⚾⛄-⛅⛈⛎-⛏⛑⛓-⛔⛩-⛪⛰-⛵⛷-⛺⛽✂✅✈-✍✏✒✔✖✝✡✨✳-✴❄❇❌❎❓-❕❗❣-❤➕-➗➡➰➿⤴-⤵⬅-⬇⬛-⬜⭐⭕〰〽㊗㊙🀄🃏🅰-🅱🅾-🅿🆎🆑-🆚🈁-🈂🈚🈯🈲-🈺🉐-🉑‍🌀-🗿😀-🙏🚀-🛿🤀-🧿󠀠-󠁿]|‍[♀♂]|[🇦-🇿]{2}|.[⃠⃣️]+)+)+$");
    private static final String EMOJI_REGEX;

    private EmojiUtil() {
    }

    public static Set<String> getAllRepresentations(String str) {
        HashSet hashSet = new HashSet();
        hashSet.add(str);
        for (ObsoleteEmoji obsoleteEmoji : EmojiSource.getLatest().getObsolete()) {
            if (obsoleteEmoji.getObsolete().equals(str)) {
                hashSet.add(obsoleteEmoji.getReplaceWith());
            } else if (obsoleteEmoji.getReplaceWith().equals(str)) {
                hashSet.add(obsoleteEmoji.getObsolete());
            }
        }
        return hashSet;
    }

    public static String getCanonicalRepresentation(String str) {
        String str2 = EmojiSource.getLatest().getVariationsToCanonical().get(str);
        return str2 != null ? str2 : str;
    }

    public static boolean isCanonicallyEqual(String str, String str2) {
        return getCanonicalRepresentation(str).equals(getCanonicalRepresentation(str2));
    }

    public static Drawable convertToDrawable(Context context, String str) {
        if (Util.isEmpty(str)) {
            return null;
        }
        return EmojiProvider.getEmojiDrawable(context, str);
    }

    public static boolean isEmoji(String str) {
        if (Util.isEmpty(str) || StringUtil.getGraphemeCount(str) != 1) {
            return false;
        }
        EmojiParser.CandidateList candidates = EmojiProvider.getCandidates(str);
        if ((candidates == null || candidates.size() <= 0) && !EMOJI_PATTERN.matcher(str).matches()) {
            return false;
        }
        return true;
    }

    public static String stripEmoji(String str) {
        return str == null ? str : str.replaceAll(EMOJI_REGEX, "");
    }
}
