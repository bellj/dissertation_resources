package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Annotation;
import android.text.Layout;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextDirectionHeuristic;
import android.text.TextDirectionHeuristics;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.text.style.CharacterStyle;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewKt;
import androidx.core.widget.TextViewCompat;
import j$.util.DesugarArrays;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.Comparator;
import java.util.List;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import org.signal.core.util.StringUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiProvider;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiParser;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.mention.MentionRendererDelegate;
import org.thoughtcrime.securesms.emoji.JumboEmoji;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class EmojiTextView extends AppCompatTextView {
    private static final char ELLIPSIS;
    private static final float JUMBOMOJI_SCALE;
    private boolean forceCustom;
    private boolean forceJumboEmoji;
    private boolean isJumbomoji;
    private int lastLineWidth;
    private int maxLength;
    private boolean measureLastLine;
    private MentionRendererDelegate mentionRendererDelegate;
    private float originalFontSize;
    private CharSequence overflowText;
    private TextView.BufferType previousBufferType;
    private CharSequence previousOverflowText;
    private CharSequence previousText;
    private TransformationMethod previousTransformationMethod;
    private boolean renderMentions;
    private final boolean scaleEmojis;
    private boolean sizeChangeInProgress;
    private TextDirectionHeuristic textDirection;
    private boolean useSystemEmoji;

    public EmojiTextView(Context context) {
        this(context, null);
    }

    public EmojiTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public EmojiTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.lastLineWidth = -1;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.EmojiTextView, 0, 0);
        this.scaleEmojis = obtainStyledAttributes.getBoolean(5, false);
        this.maxLength = obtainStyledAttributes.getInteger(2, -1);
        this.forceCustom = obtainStyledAttributes.getBoolean(0, false);
        this.renderMentions = obtainStyledAttributes.getBoolean(3, true);
        this.measureLastLine = obtainStyledAttributes.getBoolean(4, false);
        this.forceJumboEmoji = obtainStyledAttributes.getBoolean(1, false);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, new int[]{16842901});
        this.originalFontSize = (float) obtainStyledAttributes2.getDimensionPixelSize(0, 0);
        obtainStyledAttributes2.recycle();
        if (this.renderMentions) {
            this.mentionRendererDelegate = new MentionRendererDelegate(getContext(), ContextCompat.getColor(getContext(), R.color.transparent_black_20));
        }
        this.textDirection = getLayoutDirection() == 0 ? TextDirectionHeuristics.FIRSTSTRONG_RTL : TextDirectionHeuristics.ANYRTL_LTR;
    }

    @Override // android.widget.TextView, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.renderMentions && (getText() instanceof Spanned) && getLayout() != null) {
            int save = canvas.save();
            canvas.translate((float) getTotalPaddingLeft(), (float) getTotalPaddingTop());
            try {
                this.mentionRendererDelegate.draw(canvas, (Spanned) getText(), getLayout());
            } finally {
                canvas.restoreToCount(save);
            }
        }
        super.onDraw(canvas);
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        EmojiParser.CandidateList candidates = isInEditMode() ? null : EmojiProvider.getCandidates(charSequence);
        boolean z = true;
        if (this.scaleEmojis && candidates != null && candidates.allEmojis && (candidates.hasJumboForAll() || JumboEmoji.canDownloadJumbo(getContext()))) {
            int size = candidates.size();
            float f = size <= 5 ? 1.8f : 1.0f;
            if (size <= 4) {
                f += JUMBOMOJI_SCALE;
            }
            if (size <= 2) {
                f += JUMBOMOJI_SCALE;
            }
            this.isJumbomoji = f > 1.0f;
            super.setTextSize(0, this.originalFontSize * f);
        } else if (this.scaleEmojis) {
            this.isJumbomoji = false;
            super.setTextSize(0, this.originalFontSize);
        }
        if (!unchanged(charSequence, this.overflowText, bufferType)) {
            this.previousText = charSequence;
            this.previousOverflowText = this.overflowText;
            this.previousBufferType = bufferType;
            this.useSystemEmoji = useSystemEmoji();
            this.previousTransformationMethod = getTransformationMethod();
            if (this.useSystemEmoji || candidates == null || candidates.size() == 0) {
                super.setText(new SpannableStringBuilder((CharSequence) Optional.ofNullable(charSequence).orElse("")), TextView.BufferType.SPANNABLE);
            } else {
                if (!this.isJumbomoji && !this.forceJumboEmoji) {
                    z = false;
                }
                super.setText(new SpannableStringBuilder(EmojiProvider.emojify(candidates, charSequence, this, z)), TextView.BufferType.SPANNABLE);
            }
            if (getText() != null && getText().length() > 0 && isEllipsizedAtEnd()) {
                if (this.maxLength > 0) {
                    ellipsizeAnyTextForMaxLength();
                } else if (getMaxLines() > 0) {
                    ellipsizeEmojiTextForMaxLines();
                }
            }
            if (getLayoutParams() != null && getLayoutParams().width == -2) {
                requestLayout();
            }
        }
    }

    private boolean isEllipsizedAtEnd() {
        return getEllipsize() == TextUtils.TruncateAt.END || (getMaxLines() > 0 && getMaxLines() < Integer.MAX_VALUE) || this.maxLength > 0;
    }

    @Override // androidx.appcompat.widget.AppCompatTextView, android.widget.TextView, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(applyWidthMeasureRoundingFix(i), i2);
        CharSequence text = getText();
        if (getLayout() == null || !this.measureLastLine || text == null || text.length() == 0) {
            this.lastLineWidth = -1;
            return;
        }
        Layout layout = getLayout();
        CharSequence text2 = layout.getText();
        int lineStart = layout.getLineStart(layout.getLineCount() - 1);
        if ((getLayoutDirection() != 0 || !this.textDirection.isRtl(text2, 0, text2.length())) && (getLayoutDirection() != 1 || this.textDirection.isRtl(text2, 0, text2.length()))) {
            this.lastLineWidth = (int) getPaint().measureText(text2, lineStart, text2.length());
        } else {
            this.lastLineWidth = getMeasuredWidth();
        }
    }

    private int applyWidthMeasureRoundingFix(int i) {
        CharSequence text;
        if (Build.VERSION.SDK_INT < 30 || getLetterSpacing() <= 0.0f || (text = getText()) == null) {
            return i;
        }
        int mode = View.MeasureSpec.getMode(i);
        int size = View.MeasureSpec.getSize(i);
        int desiredWidth = ((int) (hasMetricAffectingSpan(text) ? Layout.getDesiredWidth(text, getPaint()) : getLongestLineWidth(text))) + getPaddingLeft() + getPaddingRight();
        return (mode != Integer.MIN_VALUE || desiredWidth >= size) ? i : View.MeasureSpec.makeMeasureSpec(desiredWidth + 1, 1073741824);
    }

    private boolean hasMetricAffectingSpan(CharSequence charSequence) {
        if ((charSequence instanceof Spanned) && ((Spanned) charSequence).nextSpanTransition(-1, charSequence.length(), CharacterStyle.class) != charSequence.length()) {
            return true;
        }
        return false;
    }

    private float getLongestLineWidth(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return 0.0f;
        }
        return ((Float) DesugarArrays.stream(charSequence.toString().split("\n")).limit(getMaxLines() > 0 ? (long) getMaxLines() : Long.MAX_VALUE).map(new Function() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiTextView$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiTextView.this.lambda$getLongestLineWidth$0((String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).max(new Comparator() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiTextView$$ExternalSyntheticLambda1
            @Override // java.util.Comparator
            public final int compare(Object obj, Object obj2) {
                return Float.compare(((Float) obj).floatValue(), ((Float) obj2).floatValue());
            }
        }).orElse(Float.valueOf(0.0f))).floatValue();
    }

    public /* synthetic */ Float lambda$getLongestLineWidth$0(String str) {
        return Float.valueOf(getPaint().measureText(str, 0, str.length()));
    }

    public int getLastLineWidth() {
        return this.lastLineWidth;
    }

    @Override // android.widget.TextView
    public boolean isSingleLine() {
        return getLayout() != null && getLayout().getLineCount() == 1;
    }

    public boolean isJumbomoji() {
        return this.isJumbomoji;
    }

    public void setOverflowText(CharSequence charSequence) {
        this.overflowText = charSequence;
        setText(this.previousText, TextView.BufferType.SPANNABLE);
    }

    public void setForceCustomEmoji(boolean z) {
        if (this.forceCustom != z) {
            this.forceCustom = z;
            setText(this.previousText, TextView.BufferType.SPANNABLE);
        }
    }

    private void ellipsizeAnyTextForMaxLength() {
        if (this.maxLength > 0) {
            boolean z = true;
            if (getText().length() > this.maxLength + 1) {
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                CharSequence subSequence = getText().subSequence(0, this.maxLength);
                if (subSequence instanceof Spanned) {
                    Spanned spanned = (Spanned) subSequence;
                    int i = this.maxLength;
                    List<Annotation> mentionAnnotations = MentionAnnotation.getMentionAnnotations(spanned, i - 1, i);
                    if (!mentionAnnotations.isEmpty()) {
                        subSequence = subSequence.subSequence(0, spanned.getSpanStart(mentionAnnotations.get(0)));
                    }
                }
                spannableStringBuilder.append(subSequence).append(ELLIPSIS).append(Util.emptyIfNull(this.overflowText));
                EmojiParser.CandidateList candidates = isInEditMode() ? null : EmojiProvider.getCandidates(spannableStringBuilder);
                if (this.useSystemEmoji || candidates == null || candidates.size() == 0) {
                    super.setText(spannableStringBuilder, TextView.BufferType.SPANNABLE);
                    return;
                }
                if (!this.isJumbomoji && !this.forceJumboEmoji) {
                    z = false;
                }
                super.setText(EmojiProvider.emojify(candidates, spannableStringBuilder, this, z), TextView.BufferType.SPANNABLE);
            }
        }
    }

    private void ellipsizeEmojiTextForMaxLines() {
        EmojiTextView$$ExternalSyntheticLambda2 emojiTextView$$ExternalSyntheticLambda2 = new Runnable() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiTextView$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                EmojiTextView.this.lambda$ellipsizeEmojiTextForMaxLines$1();
            }
        };
        if (getLayout() != null) {
            emojiTextView$$ExternalSyntheticLambda2.run();
        } else {
            ViewKt.doOnPreDraw(this, new Function1(emojiTextView$$ExternalSyntheticLambda2) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiTextView$$ExternalSyntheticLambda3
                public final /* synthetic */ Runnable f$0;

                {
                    this.f$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function1
                public final Object invoke(Object obj) {
                    return EmojiTextView.lambda$ellipsizeEmojiTextForMaxLines$2(this.f$0, (View) obj);
                }
            });
        }
    }

    public /* synthetic */ void lambda$ellipsizeEmojiTextForMaxLines$1() {
        float f;
        int maxLines = TextViewCompat.getMaxLines(this);
        if (maxLines <= 0 && this.maxLength < 0) {
            return;
        }
        if (getLineCount() > maxLines) {
            boolean z = true;
            int i = maxLines - 1;
            int lineStart = getLayout().getLineStart(i);
            int i2 = this.maxLength;
            if (i2 <= 0 || lineStart <= i2) {
                CharSequence subSequence = getText().subSequence(lineStart, getLayout().getLineEnd(i));
                if (this.overflowText != null) {
                    TextPaint paint = getPaint();
                    CharSequence charSequence = this.overflowText;
                    f = paint.measureText(charSequence, 0, charSequence.length());
                } else {
                    f = 0.0f;
                }
                CharSequence trim = StringUtil.trim(TextUtils.ellipsize(subSequence, getPaint(), ((float) getWidth()) - f, TextUtils.TruncateAt.END));
                SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
                spannableStringBuilder.append(getText().subSequence(0, lineStart)).append(trim.subSequence(0, trim.length())).append((CharSequence) Optional.ofNullable(this.overflowText).orElse(""));
                EmojiParser.CandidateList candidates = isInEditMode() ? null : EmojiProvider.getCandidates(spannableStringBuilder);
                if (!this.isJumbomoji && !this.forceJumboEmoji) {
                    z = false;
                }
                super.setText(EmojiProvider.emojify(candidates, spannableStringBuilder, this, z), TextView.BufferType.SPANNABLE);
                return;
            }
            ellipsizeAnyTextForMaxLength();
        } else if (this.maxLength > 0) {
            ellipsizeAnyTextForMaxLength();
        }
    }

    public static /* synthetic */ Unit lambda$ellipsizeEmojiTextForMaxLines$2(Runnable runnable, View view) {
        runnable.run();
        return Unit.INSTANCE;
    }

    private boolean unchanged(CharSequence charSequence, CharSequence charSequence2, TextView.BufferType bufferType) {
        return Util.equals(this.previousText, charSequence) && Util.equals(this.previousOverflowText, charSequence2) && Util.equals(this.previousBufferType, bufferType) && this.useSystemEmoji == useSystemEmoji() && !this.sizeChangeInProgress && this.previousTransformationMethod == getTransformationMethod();
    }

    private boolean useSystemEmoji() {
        return !this.forceCustom && SignalStore.settings().isPreferSystemEmoji();
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (!this.sizeChangeInProgress) {
            this.sizeChangeInProgress = true;
            setText(this.previousText, this.previousBufferType);
            this.sizeChangeInProgress = false;
        }
    }

    @Override // android.widget.TextView, android.graphics.drawable.Drawable.Callback, android.view.View
    public void invalidateDrawable(Drawable drawable) {
        if (drawable instanceof EmojiProvider.EmojiDrawable) {
            invalidate();
        } else {
            super.invalidateDrawable(drawable);
        }
    }

    @Override // android.widget.TextView
    public void setTextSize(float f) {
        setTextSize(2, f);
    }

    @Override // androidx.appcompat.widget.AppCompatTextView, android.widget.TextView
    public void setTextSize(int i, float f) {
        this.originalFontSize = TypedValue.applyDimension(i, f, getResources().getDisplayMetrics());
        super.setTextSize(i, f);
    }

    public void setMentionBackgroundTint(int i) {
        if (this.renderMentions) {
            this.mentionRendererDelegate.setTint(i);
        }
    }
}
