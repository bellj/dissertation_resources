package org.thoughtcrime.securesms.components.settings.app.privacy;

import android.text.SpannableStringBuilder;
import android.view.View;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: PrivacySettingsFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Landroid/text/SpannableStringBuilder;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class PrivacySettingsFragment$incognitoSummary$2 extends Lambda implements Function0<SpannableStringBuilder> {
    final /* synthetic */ PrivacySettingsFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PrivacySettingsFragment$incognitoSummary$2(PrivacySettingsFragment privacySettingsFragment) {
        super(0);
        this.this$0 = privacySettingsFragment;
    }

    @Override // kotlin.jvm.functions.Function0
    public final SpannableStringBuilder invoke() {
        return new SpannableStringBuilder(this.this$0.getString(R.string.preferences__this_setting_is_not_a_guarantee)).append((CharSequence) " ").append(SpanUtil.learnMore(this.this$0.requireContext(), ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_text_primary), new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$incognitoSummary$2$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PrivacySettingsFragment$incognitoSummary$2.m830invoke$lambda0(PrivacySettingsFragment.this, view);
            }
        }));
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m830invoke$lambda0(PrivacySettingsFragment privacySettingsFragment, View view) {
        Intrinsics.checkNotNullParameter(privacySettingsFragment, "this$0");
        CommunicationActions.openBrowserLink(privacySettingsFragment.requireContext(), privacySettingsFragment.getString(R.string.preferences__incognito_keyboard_learn_more));
    }
}
