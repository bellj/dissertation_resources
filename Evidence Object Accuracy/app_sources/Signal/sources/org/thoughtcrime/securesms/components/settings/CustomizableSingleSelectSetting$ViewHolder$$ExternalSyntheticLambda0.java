package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.CustomizableSingleSelectSetting;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CustomizableSingleSelectSetting$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CustomizableSingleSelectSetting.ViewHolder f$0;
    public final /* synthetic */ CustomizableSingleSelectSetting.Item f$1;

    public /* synthetic */ CustomizableSingleSelectSetting$ViewHolder$$ExternalSyntheticLambda0(CustomizableSingleSelectSetting.ViewHolder viewHolder, CustomizableSingleSelectSetting.Item item) {
        this.f$0 = viewHolder;
        this.f$1 = item;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
