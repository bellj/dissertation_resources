package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import java.text.SimpleDateFormat;
import java.util.Locale;
import okhttp3.HttpUrl;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.ImageSlide;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlidesClickedListener;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class LinkPreviewView extends FrameLayout {
    private static final int TYPE_COMPOSE;
    private static final int TYPE_CONVERSATION;
    private View closeButton;
    private CloseClickedListener closeClickedListener;
    private ViewGroup container;
    private CornerMask cornerMask;
    private int defaultRadius;
    private TextView description;
    private View divider;
    private TextView noPreview;
    private TextView site;
    private View spinner;
    private OutlinedThumbnailView thumbnail;
    private TextView title;
    private int type;

    /* loaded from: classes4.dex */
    public interface CloseClickedListener {
        void onCloseClicked();
    }

    public LinkPreviewView(Context context) {
        super(context);
        init(null);
    }

    public LinkPreviewView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        FrameLayout.inflate(getContext(), R.layout.link_preview, this);
        this.container = (ViewGroup) findViewById(R.id.linkpreview_container);
        this.thumbnail = (OutlinedThumbnailView) findViewById(R.id.linkpreview_thumbnail);
        this.title = (TextView) findViewById(R.id.linkpreview_title);
        this.description = (TextView) findViewById(R.id.linkpreview_description);
        this.site = (TextView) findViewById(R.id.linkpreview_site);
        this.divider = findViewById(R.id.linkpreview_divider);
        this.spinner = findViewById(R.id.linkpreview_progress_wheel);
        this.closeButton = findViewById(R.id.linkpreview_close);
        this.noPreview = (TextView) findViewById(R.id.linkpreview_no_preview);
        this.defaultRadius = getResources().getDimensionPixelSize(R.dimen.thumbnail_default_radius);
        this.cornerMask = new CornerMask(this);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.LinkPreviewView, 0, 0);
            this.type = obtainStyledAttributes.getInt(0, 0);
            obtainStyledAttributes.recycle();
        }
        if (this.type == 1) {
            this.container.setBackgroundColor(0);
            this.container.setPadding(0, 0, 0, 0);
            this.divider.setVisibility(0);
            this.closeButton.setVisibility(0);
            this.title.setMaxLines(2);
            this.description.setMaxLines(2);
            this.closeButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.LinkPreviewView$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    LinkPreviewView.this.lambda$init$0(view);
                }
            });
        }
        setWillNotDraw(false);
    }

    public /* synthetic */ void lambda$init$0(View view) {
        CloseClickedListener closeClickedListener = this.closeClickedListener;
        if (closeClickedListener != null) {
            closeClickedListener.onCloseClicked();
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (this.type != 1) {
            this.cornerMask.mask(canvas);
        }
    }

    public void setLoading() {
        this.title.setVisibility(8);
        this.site.setVisibility(8);
        this.description.setVisibility(8);
        this.thumbnail.setVisibility(8);
        this.spinner.setVisibility(0);
        this.noPreview.setVisibility(4);
    }

    public void setNoPreview(LinkPreviewRepository.Error error) {
        this.title.setVisibility(8);
        this.site.setVisibility(8);
        this.thumbnail.setVisibility(8);
        this.spinner.setVisibility(8);
        this.noPreview.setVisibility(0);
        this.noPreview.setText(getLinkPreviewErrorString(error));
    }

    public void setLinkPreview(GlideRequests glideRequests, LinkPreview linkPreview, boolean z) {
        setLinkPreview(glideRequests, linkPreview, z, true);
    }

    public void setLinkPreview(GlideRequests glideRequests, LinkPreview linkPreview, boolean z, boolean z2) {
        HttpUrl parse;
        this.spinner.setVisibility(8);
        this.noPreview.setVisibility(8);
        if (!Util.isEmpty(linkPreview.getTitle())) {
            this.title.setText(linkPreview.getTitle());
            this.title.setVisibility(0);
        } else {
            this.title.setVisibility(8);
        }
        if (!z2 || Util.isEmpty(linkPreview.getDescription())) {
            this.description.setVisibility(8);
        } else {
            this.description.setText(linkPreview.getDescription());
            this.description.setVisibility(0);
        }
        String str = null;
        if (!Util.isEmpty(linkPreview.getUrl()) && (parse = HttpUrl.parse(linkPreview.getUrl())) != null) {
            str = parse.topPrivateDomain();
        }
        boolean z3 = true;
        if (str != null && linkPreview.getDate() > 0) {
            this.site.setText(getContext().getString(R.string.LinkPreviewView_domain_date, str, formatDate(linkPreview.getDate())));
            this.site.setVisibility(0);
        } else if (str != null) {
            this.site.setText(str);
            this.site.setVisibility(0);
        } else if (linkPreview.getDate() > 0) {
            this.site.setText(formatDate(linkPreview.getDate()));
            this.site.setVisibility(0);
        } else {
            this.site.setVisibility(8);
        }
        if (!z || !linkPreview.getThumbnail().isPresent()) {
            this.thumbnail.setVisibility(8);
            return;
        }
        this.thumbnail.setVisibility(0);
        OutlinedThumbnailView outlinedThumbnailView = this.thumbnail;
        ImageSlide imageSlide = new ImageSlide(getContext(), linkPreview.getThumbnail().get());
        if (this.type != 0) {
            z3 = false;
        }
        outlinedThumbnailView.setImageResource(glideRequests, (Slide) imageSlide, z3, false);
        this.thumbnail.showDownloadText(false);
    }

    public void setCorners(int i, int i2) {
        if (ViewUtil.isRtl(this)) {
            this.cornerMask.setRadii(i2, i, 0, 0);
            OutlinedThumbnailView outlinedThumbnailView = this.thumbnail;
            int i3 = this.defaultRadius;
            outlinedThumbnailView.setCorners(i3, i2, i3, i3);
        } else {
            this.cornerMask.setRadii(i, i2, 0, 0);
            OutlinedThumbnailView outlinedThumbnailView2 = this.thumbnail;
            int i4 = this.defaultRadius;
            outlinedThumbnailView2.setCorners(i, i4, i4, i4);
        }
        postInvalidate();
    }

    public void setCloseClickedListener(CloseClickedListener closeClickedListener) {
        this.closeClickedListener = closeClickedListener;
    }

    public void setDownloadClickedListener(SlidesClickedListener slidesClickedListener) {
        this.thumbnail.setDownloadClickListener(slidesClickedListener);
    }

    private static int getLinkPreviewErrorString(LinkPreviewRepository.Error error) {
        return error == LinkPreviewRepository.Error.GROUP_LINK_INACTIVE ? R.string.LinkPreviewView_this_group_link_is_not_active : R.string.LinkPreviewView_no_link_preview_available;
    }

    private static String formatDate(long j) {
        return new SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()).format(Long.valueOf(j));
    }
}
