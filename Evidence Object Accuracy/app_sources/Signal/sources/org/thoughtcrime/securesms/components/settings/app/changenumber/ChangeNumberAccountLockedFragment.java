package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;

/* compiled from: ChangeNumberAccountLockedFragment.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0014J\b\u0010\u0005\u001a\u00020\u0006H\u0014J\u001a\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\t2\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0016¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberAccountLockedFragment;", "Lorg/thoughtcrime/securesms/registration/fragments/BaseAccountLockedFragment;", "()V", "getViewModel", "Lorg/thoughtcrime/securesms/registration/viewmodel/BaseRegistrationViewModel;", "onNext", "", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberAccountLockedFragment extends BaseAccountLockedFragment {
    public ChangeNumberAccountLockedFragment() {
        super(R.layout.fragment_change_number_account_locked);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberAccountLockedFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberAccountLockedFragment.$r8$lambda$c7tEluGdZpO9ZDWXisr1utnog3k(ChangeNumberAccountLockedFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m578onViewCreated$lambda0(ChangeNumberAccountLockedFragment changeNumberAccountLockedFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberAccountLockedFragment, "this$0");
        FragmentKt.findNavController(changeNumberAccountLockedFragment).navigateUp();
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment
    protected BaseRegistrationViewModel getViewModel() {
        return ChangeNumberUtil.getViewModel(this);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseAccountLockedFragment
    protected void onNext() {
        FragmentKt.findNavController(this).navigateUp();
    }
}
