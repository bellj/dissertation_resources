package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import java.util.Collections;
import java.util.Set;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.webrtc.audio.SignalAudioManager;

/* loaded from: classes4.dex */
public final class WebRtcControls {
    public static final WebRtcControls NONE = new WebRtcControls();
    public static final WebRtcControls PIP = new WebRtcControls(false, false, false, true, false, CallState.NONE, GroupCallState.NONE, null, FoldableState.flat(), SignalAudioManager.AudioDevice.NONE, Collections.emptySet());
    private final SignalAudioManager.AudioDevice activeDevice;
    private final Set<SignalAudioManager.AudioDevice> availableDevices;
    private final CallState callState;
    private final FoldableState foldableState;
    private final GroupCallState groupCallState;
    private final boolean hasAtLeastOneRemote;
    private final boolean isInPipMode;
    private final boolean isLocalVideoEnabled;
    private final boolean isMoreThanOneCameraAvailable;
    private final boolean isRemoteVideoEnabled;
    private final Long participantLimit;

    private WebRtcControls() {
        this(false, false, false, false, false, CallState.NONE, GroupCallState.NONE, null, FoldableState.flat(), SignalAudioManager.AudioDevice.NONE, Collections.emptySet());
    }

    public WebRtcControls(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, CallState callState, GroupCallState groupCallState, Long l, FoldableState foldableState, SignalAudioManager.AudioDevice audioDevice, Set<SignalAudioManager.AudioDevice> set) {
        this.isLocalVideoEnabled = z;
        this.isRemoteVideoEnabled = z2;
        this.isMoreThanOneCameraAvailable = z3;
        this.isInPipMode = z4;
        this.hasAtLeastOneRemote = z5;
        this.callState = callState;
        this.groupCallState = groupCallState;
        this.participantLimit = l;
        this.foldableState = foldableState;
        this.activeDevice = audioDevice;
        this.availableDevices = set;
    }

    public WebRtcControls withFoldableState(FoldableState foldableState) {
        return new WebRtcControls(this.isLocalVideoEnabled, this.isRemoteVideoEnabled, this.isMoreThanOneCameraAvailable, this.isInPipMode, this.hasAtLeastOneRemote, this.callState, this.groupCallState, this.participantLimit, foldableState, this.activeDevice, this.availableDevices);
    }

    public boolean displayErrorControls() {
        return isError();
    }

    public boolean displayStartCallControls() {
        return isPreJoin();
    }

    public boolean adjustForFold() {
        return this.foldableState.isFolded();
    }

    public int getFold() {
        return this.foldableState.getFoldPoint();
    }

    public int getStartCallButtonText() {
        if (!isGroupCall()) {
            return R.string.WebRtcCallView__start_call;
        }
        if (this.groupCallState == GroupCallState.FULL) {
            return R.string.WebRtcCallView__call_is_full;
        }
        return this.hasAtLeastOneRemote ? R.string.WebRtcCallView__join_call : R.string.WebRtcCallView__start_call;
    }

    public boolean isStartCallEnabled() {
        return this.groupCallState != GroupCallState.FULL;
    }

    public boolean displayGroupCallFull() {
        return this.groupCallState == GroupCallState.FULL;
    }

    public String getGroupCallFullMessage(Context context) {
        Long l = this.participantLimit;
        return l != null ? context.getString(R.string.WebRtcCallView__the_maximum_number_of_d_participants_has_been_Reached_for_this_call, l) : "";
    }

    public boolean displayGroupMembersButton() {
        return (this.groupCallState.isAtLeast(GroupCallState.CONNECTING) && this.hasAtLeastOneRemote) || this.groupCallState.isAtLeast(GroupCallState.FULL);
    }

    public boolean displayEndCall() {
        return isAtLeastOutgoing() || this.callState == CallState.RECONNECTING;
    }

    public boolean displayMuteAudio() {
        return isPreJoin() || isAtLeastOutgoing();
    }

    public boolean displayVideoToggle() {
        return isPreJoin() || isAtLeastOutgoing();
    }

    public boolean displayAudioToggle() {
        return (isPreJoin() || isAtLeastOutgoing()) && (!this.isLocalVideoEnabled || enableHeadsetInAudioToggle());
    }

    public boolean displayCameraToggle() {
        return (isPreJoin() || isAtLeastOutgoing()) && this.isLocalVideoEnabled && this.isMoreThanOneCameraAvailable;
    }

    public boolean displayRemoteVideoRecycler() {
        return isOngoing();
    }

    public boolean displayAnswerWithoutVideo() {
        return isIncoming() && this.isRemoteVideoEnabled;
    }

    public boolean displayIncomingCallButtons() {
        return isIncoming();
    }

    public boolean enableHandsetInAudioToggle() {
        return !this.isLocalVideoEnabled;
    }

    public boolean enableHeadsetInAudioToggle() {
        return this.availableDevices.contains(SignalAudioManager.AudioDevice.BLUETOOTH);
    }

    public boolean isFadeOutEnabled() {
        return isAtLeastOutgoing() && this.isRemoteVideoEnabled && this.callState != CallState.RECONNECTING;
    }

    public boolean displaySmallOngoingCallButtons() {
        return isAtLeastOutgoing() && displayAudioToggle() && displayCameraToggle();
    }

    public boolean displayLargeOngoingCallButtons() {
        return isAtLeastOutgoing() && (!displayAudioToggle() || !displayCameraToggle());
    }

    public boolean displayTopViews() {
        return !this.isInPipMode;
    }

    /* renamed from: org.thoughtcrime.securesms.components.webrtc.WebRtcControls$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$webrtc$audio$SignalAudioManager$AudioDevice;

        static {
            int[] iArr = new int[SignalAudioManager.AudioDevice.values().length];
            $SwitchMap$org$thoughtcrime$securesms$webrtc$audio$SignalAudioManager$AudioDevice = iArr;
            try {
                iArr[SignalAudioManager.AudioDevice.SPEAKER_PHONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$webrtc$audio$SignalAudioManager$AudioDevice[SignalAudioManager.AudioDevice.BLUETOOTH.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
        }
    }

    public WebRtcAudioOutput getAudioOutput() {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$webrtc$audio$SignalAudioManager$AudioDevice[this.activeDevice.ordinal()];
        if (i == 1) {
            return WebRtcAudioOutput.SPEAKER;
        }
        if (i != 2) {
            return WebRtcAudioOutput.HANDSET;
        }
        return WebRtcAudioOutput.HEADSET;
    }

    public boolean showSmallHeader() {
        return isAtLeastOutgoing();
    }

    public boolean showFullScreenShade() {
        return isPreJoin() || isIncoming();
    }

    public boolean displayRingToggle() {
        return FeatureFlags.groupCallRinging() && isPreJoin() && isGroupCall() && !this.hasAtLeastOneRemote;
    }

    private boolean isError() {
        return this.callState == CallState.ERROR;
    }

    private boolean isPreJoin() {
        return this.callState == CallState.PRE_JOIN;
    }

    private boolean isOngoing() {
        return this.callState == CallState.ONGOING;
    }

    private boolean isIncoming() {
        return this.callState == CallState.INCOMING;
    }

    private boolean isAtLeastOutgoing() {
        return this.callState.isAtLeast(CallState.OUTGOING);
    }

    private boolean isGroupCall() {
        return this.groupCallState != GroupCallState.NONE;
    }

    /* loaded from: classes4.dex */
    public enum CallState {
        NONE,
        ERROR,
        PRE_JOIN,
        RECONNECTING,
        INCOMING,
        OUTGOING,
        ONGOING,
        ENDING;

        boolean isAtLeast(CallState callState) {
            return compareTo(callState) >= 0;
        }
    }

    /* loaded from: classes4.dex */
    public enum GroupCallState {
        NONE,
        DISCONNECTED,
        RECONNECTING,
        CONNECTING,
        FULL,
        CONNECTED;

        boolean isAtLeast(GroupCallState groupCallState) {
            return compareTo(groupCallState) >= 0;
        }
    }

    /* loaded from: classes4.dex */
    public static final class FoldableState {
        private static final int NOT_SET;
        private final int foldPoint;

        public FoldableState(int i) {
            this.foldPoint = i;
        }

        public boolean isFolded() {
            return this.foldPoint != -1;
        }

        public boolean isFlat() {
            return this.foldPoint == -1;
        }

        public int getFoldPoint() {
            return this.foldPoint;
        }

        public static FoldableState folded(int i) {
            return new FoldableState(i);
        }

        public static FoldableState flat() {
            return new FoldableState(-1);
        }
    }
}
