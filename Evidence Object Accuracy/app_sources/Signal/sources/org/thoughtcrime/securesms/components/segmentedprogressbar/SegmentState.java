package org.thoughtcrime.securesms.components.segmentedprogressbar;

import kotlin.Metadata;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;

/* compiled from: SegmentState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J\t\u0010\t\u001a\u00020\u0003HÆ\u0003J\t\u0010\n\u001a\u00020\u0003HÆ\u0003J\u001d\u0010\u000b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentState;", "", "position", "", "duration", "(JJ)V", "getDuration", "()J", "getPosition", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SegmentState {
    private final long duration;
    private final long position;

    public static /* synthetic */ SegmentState copy$default(SegmentState segmentState, long j, long j2, int i, Object obj) {
        if ((i & 1) != 0) {
            j = segmentState.position;
        }
        if ((i & 2) != 0) {
            j2 = segmentState.duration;
        }
        return segmentState.copy(j, j2);
    }

    public final long component1() {
        return this.position;
    }

    public final long component2() {
        return this.duration;
    }

    public final SegmentState copy(long j, long j2) {
        return new SegmentState(j, j2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SegmentState)) {
            return false;
        }
        SegmentState segmentState = (SegmentState) obj;
        return this.position == segmentState.position && this.duration == segmentState.duration;
    }

    public int hashCode() {
        return (SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.position) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.duration);
    }

    public String toString() {
        return "SegmentState(position=" + this.position + ", duration=" + this.duration + ')';
    }

    public SegmentState(long j, long j2) {
        this.position = j;
        this.duration = j2;
    }

    public final long getPosition() {
        return this.position;
    }

    public final long getDuration() {
        return this.duration;
    }
}
