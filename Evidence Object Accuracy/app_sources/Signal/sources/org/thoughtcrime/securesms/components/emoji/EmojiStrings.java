package org.thoughtcrime.securesms.components.emoji;

/* loaded from: classes4.dex */
public final class EmojiStrings {
    public static final String AUDIO;
    public static final String BUST_IN_SILHOUETTE;
    public static final String FILE;
    public static final String GIF;
    public static final String GIFT;
    public static final String PHOTO;
    public static final String STICKER;
    public static final String VIDEO;
}
