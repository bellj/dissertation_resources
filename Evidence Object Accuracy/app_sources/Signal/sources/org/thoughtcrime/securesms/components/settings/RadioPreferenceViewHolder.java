package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import android.widget.RadioButton;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/RadioPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/RadioPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "radioButton", "Landroid/widget/RadioButton;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RadioPreferenceViewHolder extends PreferenceViewHolder<RadioPreference> {
    private final RadioButton radioButton;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RadioPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
        View findViewById = view.findViewById(R.id.radio_widget);
        Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.radio_widget)");
        this.radioButton = (RadioButton) findViewById;
    }

    public void bind(RadioPreference radioPreference) {
        Intrinsics.checkNotNullParameter(radioPreference, "model");
        super.bind((RadioPreferenceViewHolder) radioPreference);
        this.radioButton.setChecked(radioPreference.isChecked());
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.RadioPreferenceViewHolder$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                RadioPreferenceViewHolder.m543$r8$lambda$ooK6v08tp73YEVL81LjMqCtdk(RadioPreference.this, view);
            }
        });
    }

    /* renamed from: bind$lambda-0 */
    public static final void m544bind$lambda0(RadioPreference radioPreference, View view) {
        Intrinsics.checkNotNullParameter(radioPreference, "$model");
        radioPreference.getOnClick().invoke();
    }
}
