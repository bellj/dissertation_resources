package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes4.dex */
public class RecentEmojiPageModel implements EmojiPageModel {
    private static final int EMOJI_LRU_SIZE;
    public static final String KEY;
    private static final String TAG = Log.tag(RecentEmojiPageModel.class);
    private final String preferenceName;
    private final SharedPreferences prefs;
    private final LinkedHashSet<String> recentlyUsed = getPersistedCache();

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public int getIconAttr() {
        return R.attr.emoji_category_recent;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public String getKey() {
        return KEY;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public Uri getSpriteUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public boolean isDynamic() {
        return true;
    }

    public static boolean hasRecents(Context context, String str) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains(str);
    }

    public RecentEmojiPageModel(Context context, String str) {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);
        this.preferenceName = str;
    }

    private LinkedHashSet<String> getPersistedCache() {
        try {
            return (LinkedHashSet) JsonUtils.getMapper().readValue(this.prefs.getString(this.preferenceName, "[]"), TypeFactory.defaultInstance().constructCollectionType(LinkedHashSet.class, String.class));
        } catch (IOException e) {
            Log.w(TAG, e);
            return new LinkedHashSet<>();
        }
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<String> getEmoji() {
        ArrayList arrayList = new ArrayList(this.recentlyUsed);
        Collections.reverse(arrayList);
        return arrayList;
    }

    public static /* synthetic */ Emoji lambda$getDisplayEmoji$0(String str) {
        return new Emoji(str);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<Emoji> getDisplayEmoji() {
        return Stream.of(getEmoji()).map(new Function() { // from class: org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return RecentEmojiPageModel.$r8$lambda$QqOdbCZcLem7I8qni3f1PZbTx4s((String) obj);
            }
        }).toList();
    }

    public void onCodePointSelected(String str) {
        this.recentlyUsed.remove(str);
        this.recentlyUsed.add(str);
        if (this.recentlyUsed.size() > 50) {
            Iterator<String> it = this.recentlyUsed.iterator();
            it.next();
            it.remove();
        }
        SignalExecutors.BOUNDED.execute(new Runnable(new LinkedHashSet(this.recentlyUsed)) { // from class: org.thoughtcrime.securesms.components.emoji.RecentEmojiPageModel$$ExternalSyntheticLambda1
            public final /* synthetic */ LinkedHashSet f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                RecentEmojiPageModel.$r8$lambda$WCuGRYUpKKa0hAI41zD_4mH0a0U(RecentEmojiPageModel.this, this.f$1);
            }
        });
    }

    public /* synthetic */ void lambda$onCodePointSelected$1(LinkedHashSet linkedHashSet) {
        try {
            this.prefs.edit().putString(this.preferenceName, JsonUtils.toJson(linkedHashSet)).apply();
        } catch (IOException e) {
            Log.w(TAG, e);
        }
    }
}
