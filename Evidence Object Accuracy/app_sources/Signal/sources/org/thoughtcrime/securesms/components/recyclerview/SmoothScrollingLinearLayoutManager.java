package org.thoughtcrime.securesms.components.recyclerview;

import android.content.Context;
import android.util.DisplayMetrics;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;

/* loaded from: classes4.dex */
public class SmoothScrollingLinearLayoutManager extends LinearLayoutManager {
    @Override // androidx.recyclerview.widget.LinearLayoutManager, androidx.recyclerview.widget.RecyclerView.LayoutManager
    public boolean supportsPredictiveItemAnimations() {
        return false;
    }

    public SmoothScrollingLinearLayoutManager(Context context, boolean z) {
        super(context, 1, z);
    }

    public void smoothScrollToPosition(Context context, int i, final float f) {
        AnonymousClass1 r0 = new LinearSmoothScroller(context) { // from class: org.thoughtcrime.securesms.components.recyclerview.SmoothScrollingLinearLayoutManager.1
            @Override // androidx.recyclerview.widget.LinearSmoothScroller
            protected int getVerticalSnapPreference() {
                return 1;
            }

            @Override // androidx.recyclerview.widget.LinearSmoothScroller
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return f / ((float) displayMetrics.densityDpi);
            }
        };
        r0.setTargetPosition(i);
        startSmoothScroll(r0);
    }
}
