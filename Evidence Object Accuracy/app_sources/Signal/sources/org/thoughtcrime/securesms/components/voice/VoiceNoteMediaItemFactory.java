package org.thoughtcrime.securesms.components.voice;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.media.MediaDescriptionCompat;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import java.util.Locale;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.database.model.MmsMessageRecord;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.preferences.widgets.NotificationPrivacyPreference;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.DateUtils;

/* loaded from: classes4.dex */
public class VoiceNoteMediaItemFactory {
    public static final Uri END_URI = Uri.parse("file:///android_asset/sounds/state-change_confirm-up.ogg");
    public static final String EXTRA_AVATAR_RECIPIENT_ID;
    public static final String EXTRA_COLOR;
    public static final String EXTRA_INDIVIDUAL_RECIPIENT_ID;
    public static final String EXTRA_MESSAGE_ID;
    public static final String EXTRA_MESSAGE_POSITION;
    public static final String EXTRA_MESSAGE_TIMESTAMP;
    public static final String EXTRA_THREAD_ID;
    public static final String EXTRA_THREAD_RECIPIENT_ID;
    public static final Uri NEXT_URI = Uri.parse("file:///android_asset/sounds/state-change_confirm-down.ogg");
    private static final String TAG = Log.tag(VoiceNoteMediaItemFactory.class);

    private VoiceNoteMediaItemFactory() {
    }

    public static MediaItem buildMediaItem(Context context, long j, Uri uri) {
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(j);
        if (recipientForThreadId == null) {
            recipientForThreadId = Recipient.UNKNOWN;
        }
        return buildMediaItem(context, recipientForThreadId, Recipient.self(), Recipient.self(), 0, j, -1, System.currentTimeMillis(), uri);
    }

    public static MediaItem buildMediaItem(Context context, MessageRecord messageRecord) {
        int messagePositionInConversation = SignalDatabase.mmsSms().getMessagePositionInConversation(messageRecord.getThreadId(), messageRecord.getDateReceived());
        Recipient recipientForThreadId = SignalDatabase.threads().getRecipientForThreadId(messageRecord.getThreadId());
        Objects.requireNonNull(recipientForThreadId);
        Recipient self = messageRecord.isOutgoing() ? Recipient.self() : messageRecord.getIndividualRecipient();
        Recipient recipient = recipientForThreadId.isGroup() ? recipientForThreadId : self;
        AudioSlide audioSlide = ((MmsMessageRecord) messageRecord).getSlideDeck().getAudioSlide();
        if (audioSlide == null) {
            Log.w(TAG, "Message does not have an audio slide. Can't play this voice note.");
            return null;
        }
        Uri uri = audioSlide.getUri();
        if (uri != null) {
            return buildMediaItem(context, recipientForThreadId, recipient, self, messagePositionInConversation, messageRecord.getThreadId(), messageRecord.getId(), messageRecord.getDateReceived(), uri);
        }
        Log.w(TAG, "Audio slide does not have a URI. Can't play this voice note.");
        return null;
    }

    private static MediaItem buildMediaItem(Context context, Recipient recipient, Recipient recipient2, Recipient recipient3, int i, long j, long j2, long j3, Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_THREAD_RECIPIENT_ID, recipient.getId().serialize());
        bundle.putString(EXTRA_AVATAR_RECIPIENT_ID, recipient2.getId().serialize());
        bundle.putString(EXTRA_INDIVIDUAL_RECIPIENT_ID, recipient3.getId().serialize());
        bundle.putLong(EXTRA_MESSAGE_POSITION, (long) i);
        bundle.putLong(EXTRA_THREAD_ID, j);
        bundle.putLong(EXTRA_COLOR, (long) recipient.getChatColors().asSingleColor());
        bundle.putLong(EXTRA_MESSAGE_ID, j2);
        bundle.putLong(EXTRA_MESSAGE_TIMESTAMP, j3);
        NotificationPrivacyPreference messageNotificationsPrivacy = SignalStore.settings().getMessageNotificationsPrivacy();
        String title = getTitle(context, recipient3, recipient, messageNotificationsPrivacy);
        String string = messageNotificationsPrivacy.isDisplayContact() ? context.getString(R.string.VoiceNoteMediaItemFactory__voice_message, DateUtils.formatDateWithoutDayOfWeek(Locale.getDefault(), j3)) : null;
        return new MediaItem.Builder().setUri(uri).setMediaMetadata(new MediaMetadata.Builder().setTitle(title).setSubtitle(string).setExtras(bundle).build()).setTag(new MediaDescriptionCompat.Builder().setMediaUri(uri).setTitle(title).setSubtitle(string).setExtras(bundle).build()).build();
    }

    public static String getTitle(Context context, Recipient recipient, Recipient recipient2, NotificationPrivacyPreference notificationPrivacyPreference) {
        if (notificationPrivacyPreference == null) {
            notificationPrivacyPreference = new NotificationPrivacyPreference("all");
        }
        if (notificationPrivacyPreference.isDisplayContact() && recipient2.isGroup()) {
            return context.getString(R.string.VoiceNoteMediaItemFactory__s_to_s, recipient.getDisplayName(context), recipient2.getDisplayName(context));
        }
        if (!notificationPrivacyPreference.isDisplayContact()) {
            return context.getString(R.string.MessageNotifier_signal_message);
        }
        if (recipient.isSelf()) {
            return context.getString(R.string.note_to_self);
        }
        return recipient.getDisplayName(context);
    }

    public static MediaItem buildNextVoiceNoteMediaItem(MediaItem mediaItem) {
        return cloneMediaItem(mediaItem, "next", NEXT_URI);
    }

    public static MediaItem buildEndVoiceNoteMediaItem(MediaItem mediaItem) {
        return cloneMediaItem(mediaItem, NotificationProfileDatabase.NotificationProfileScheduleTable.END, END_URI);
    }

    private static MediaItem cloneMediaItem(MediaItem mediaItem, String str, Uri uri) {
        MediaItem.PlaybackProperties playbackProperties = mediaItem.playbackProperties;
        MediaDescriptionCompat mediaDescriptionCompat = null;
        MediaDescriptionCompat mediaDescriptionCompat2 = playbackProperties != null ? (MediaDescriptionCompat) playbackProperties.tag : null;
        MediaItem.Builder uri2 = mediaItem.buildUpon().setMediaId(str).setUri(uri);
        if (mediaDescriptionCompat2 != null) {
            mediaDescriptionCompat = new MediaDescriptionCompat.Builder().setMediaUri(uri).setTitle(mediaDescriptionCompat2.getTitle()).setSubtitle(mediaDescriptionCompat2.getSubtitle()).setExtras(mediaDescriptionCompat2.getExtras()).build();
        }
        return uri2.setTag(mediaDescriptionCompat).build();
    }
}
