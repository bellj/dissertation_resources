package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class ArcProgressBar extends View {
    private static final int DEFAULT_BACKGROUND_COLOR;
    private static final int DEFAULT_FOREGROUND_COLOR;
    private static final float DEFAULT_PROGRESS;
    private static final boolean DEFAULT_ROUNDED_ENDS;
    private static final float DEFAULT_START_ANGLE;
    private static final float DEFAULT_SWEEP_ANGLE;
    private static final int DEFAULT_WIDTH;
    private static final String PROGRESS;
    private static final String SUPER;
    private final Paint arcBackgroundPaint;
    private final Paint arcForegroundPaint;
    private final RectF arcRect;
    private final float arcStartAngle;
    private final float arcSweepAngle;
    private float progress;
    private final float width;

    public ArcProgressBar(Context context) {
        this(context, null);
    }

    public ArcProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ArcProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.arcRect = new RectF();
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.ArcProgressBar, i, 0);
        float dimensionPixelSize = (float) obtainStyledAttributes.getDimensionPixelSize(6, 10);
        this.width = dimensionPixelSize;
        this.progress = obtainStyledAttributes.getFloat(2, 0.0f);
        Paint createPaint = createPaint(dimensionPixelSize, obtainStyledAttributes.getColor(0, DEFAULT_BACKGROUND_COLOR));
        this.arcBackgroundPaint = createPaint;
        Paint createPaint2 = createPaint(dimensionPixelSize, obtainStyledAttributes.getColor(1, -1));
        this.arcForegroundPaint = createPaint2;
        this.arcStartAngle = obtainStyledAttributes.getFloat(4, 0.0f);
        float f = obtainStyledAttributes.getFloat(5, DEFAULT_SWEEP_ANGLE);
        this.arcSweepAngle = f;
        if (obtainStyledAttributes.getBoolean(3, true)) {
            createPaint2.setStrokeCap(Paint.Cap.ROUND);
            if (f <= DEFAULT_SWEEP_ANGLE) {
                createPaint.setStrokeCap(Paint.Cap.ROUND);
            }
        }
        obtainStyledAttributes.recycle();
    }

    private static Paint createPaint(float f, int i) {
        Paint paint = new Paint();
        paint.setStrokeWidth(f);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        paint.setColor(i);
        return paint;
    }

    public void setProgress(float f) {
        if (this.progress != f) {
            this.progress = f;
            invalidate();
        }
    }

    @Override // android.view.View
    protected Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SUPER, onSaveInstanceState);
        bundle.putFloat(PROGRESS, this.progress);
        return bundle;
    }

    @Override // android.view.View
    protected void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable.getClass() == Bundle.class) {
            Bundle bundle = (Bundle) parcelable;
            super.onRestoreInstanceState(bundle.getParcelable(SUPER));
            this.progress = (float) bundle.getLong(PROGRESS);
            return;
        }
        throw new IllegalStateException("Expected");
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        float f = this.width / 2.0f;
        float f2 = f + 0.0f;
        this.arcRect.set(f2, f2, ((float) getWidth()) - f, ((float) getHeight()) - f);
        canvas.drawArc(this.arcRect, this.arcStartAngle, this.arcSweepAngle, false, this.arcBackgroundPaint);
        canvas.drawArc(this.arcRect, this.arcStartAngle, this.arcSweepAngle * Util.clamp(this.progress, 0.0f, 1.0f), false, this.arcForegroundPaint);
    }
}
