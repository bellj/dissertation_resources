package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import android.content.Intent;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;
import org.thoughtcrime.securesms.jobs.MultiDeviceSubscriptionSyncRequestJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.subscription.LevelUpdate;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;
import org.thoughtcrime.securesms.util.PlatformCurrencyUtil;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;

/* compiled from: SubscribeViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u0000 G2\u00020\u0001:\u0002GHB\u001f\u0012\u0006\u0010\"\u001a\u00020!\u0012\u0006\u0010%\u001a\u00020$\u0012\u0006\u0010'\u001a\u00020\u0017¢\u0006\u0004\bE\u0010FJ\u0010\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0002J \u0010\u000b\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0007\u001a\u00020\u00062\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u0002J\b\u0010\r\u001a\u00020\fH\u0002J\b\u0010\u000e\u001a\u00020\u0004H\u0014J\b\u0010\u0010\u001a\u0004\u0018\u00010\u000fJ\u000e\u0010\u0012\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\bJ\u0006\u0010\u0013\u001a\u00020\u0004J\u0006\u0010\u0014\u001a\u00020\u0004J\u0006\u0010\u0015\u001a\u00020\u0004J\u0006\u0010\u0016\u001a\u00020\u0004J \u0010\u001c\u001a\u00020\u00042\u0006\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u00172\b\u0010\u001b\u001a\u0004\u0018\u00010\u001aJ\u0006\u0010\u001d\u001a\u00020\u0004J\u0006\u0010\u001e\u001a\u00020\u0004J\u000e\u0010 \u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\tR\u0014\u0010\"\u001a\u00020!8\u0002X\u0004¢\u0006\u0006\n\u0004\b\"\u0010#R\u0014\u0010%\u001a\u00020$8\u0002X\u0004¢\u0006\u0006\n\u0004\b%\u0010&R\u0014\u0010'\u001a\u00020\u00178\u0002X\u0004¢\u0006\u0006\n\u0004\b'\u0010(R\u001a\u0010+\u001a\b\u0012\u0004\u0012\u00020*0)8\u0002X\u0004¢\u0006\u0006\n\u0004\b+\u0010,R\u001a\u0010/\u001a\b\u0012\u0004\u0012\u00020.0-8\u0002X\u0004¢\u0006\u0006\n\u0004\b/\u00100R\u0014\u00102\u001a\u0002018\u0002X\u0004¢\u0006\u0006\n\u0004\b2\u00103R\u0014\u00105\u001a\u0002048\u0002X\u0004¢\u0006\u0006\n\u0004\b5\u00106R\u001d\u00108\u001a\b\u0012\u0004\u0012\u00020*078\u0006¢\u0006\f\n\u0004\b8\u00109\u001a\u0004\b:\u0010;R\u001d\u0010=\u001a\b\u0012\u0004\u0012\u00020.0<8\u0006¢\u0006\f\n\u0004\b=\u0010>\u001a\u0004\b?\u0010@R\u0018\u0010A\u001a\u0004\u0018\u00010\t8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\bA\u0010BR8\u0010D\u001a&\u0012\f\u0012\n C*\u0004\u0018\u00010\u00060\u0006 C*\u0012\u0012\f\u0012\n C*\u0004\u0018\u00010\u00060\u0006\u0018\u00010-0-8\u0002X\u0004¢\u0006\u0006\n\u0004\bD\u00100¨\u0006I"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeViewModel;", "Landroidx/lifecycle/ViewModel;", "", "throwable", "", "handleSubscriptionDataLoadFailure", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "activeSubscription", "", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "subscriptions", "resolveSelectedSubscription", "Lio/reactivex/rxjava3/core/Completable;", "cancelActiveSubscriptionIfNecessary", "onCleared", "Lorg/signal/core/util/money/FiatMoney;", "getPriceOfSelectedSubscription", "", "getSelectableCurrencyCodes", "retry", "refresh", "refreshActiveSubscription", "cancel", "", "requestCode", "resultCode", "Landroid/content/Intent;", "data", "onActivityResult", "updateSubscription", "requestTokenFromGooglePay", "subscription", "setSelectedSubscription", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "fetchTokenRequestCode", "I", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "eventPublisher", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Lio/reactivex/rxjava3/disposables/Disposable;", "networkDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "Landroidx/lifecycle/LiveData;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/core/Observable;", "events", "Lio/reactivex/rxjava3/core/Observable;", "getEvents", "()Lio/reactivex/rxjava3/core/Observable;", "subscriptionToPurchase", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "kotlin.jvm.PlatformType", "activeSubscriptionSubject", "<init>", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;I)V", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class SubscribeViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(SubscribeViewModel.class);
    private final PublishSubject<ActiveSubscription> activeSubscriptionSubject;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final DonationPaymentRepository donationPaymentRepository;
    private final PublishSubject<DonationEvent> eventPublisher;
    private final Observable<DonationEvent> events;
    private final int fetchTokenRequestCode;
    private final Disposable networkDisposable;
    private final LiveData<SubscribeState> state;
    private final Store<SubscribeState> store;
    private Subscription subscriptionToPurchase;
    private final SubscriptionsRepository subscriptionsRepository;

    public SubscribeViewModel(SubscriptionsRepository subscriptionsRepository, DonationPaymentRepository donationPaymentRepository, int i) {
        Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "donationPaymentRepository");
        this.subscriptionsRepository = subscriptionsRepository;
        this.donationPaymentRepository = donationPaymentRepository;
        this.fetchTokenRequestCode = i;
        Store<SubscribeState> store = new Store<>(new SubscribeState(SignalStore.donationsValues().getSubscriptionCurrency(), null, null, null, false, null, false, 126, null));
        this.store = store;
        PublishSubject<DonationEvent> create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.eventPublisher = create;
        LiveData<SubscribeState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Observable<DonationEvent> observeOn = create.observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "eventPublisher.observeOn…dSchedulers.mainThread())");
        this.events = observeOn;
        this.activeSubscriptionSubject = PublishSubject.create();
        Disposable subscribe = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SubscribeViewModel.m1050_init_$lambda0(SubscribeViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "InternetConnectionObserv…retry()\n        }\n      }");
        this.networkDisposable = subscribe;
    }

    public final LiveData<SubscribeState> getState() {
        return this.state;
    }

    public final Observable<DonationEvent> getEvents() {
        return this.events;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1050_init_$lambda0(SubscribeViewModel subscribeViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(subscribeViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
        if (bool.booleanValue()) {
            subscribeViewModel.retry();
        }
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.networkDisposable.dispose();
        this.disposables.dispose();
    }

    public final FiatMoney getPriceOfSelectedSubscription() {
        Set<FiatMoney> prices;
        Subscription selectedSubscription = this.store.getState().getSelectedSubscription();
        if (selectedSubscription == null || (prices = selectedSubscription.getPrices()) == null) {
            return null;
        }
        for (FiatMoney fiatMoney : prices) {
            if (Intrinsics.areEqual(fiatMoney.getCurrency(), this.store.getState().getCurrencySelection())) {
                return fiatMoney;
            }
        }
        throw new NoSuchElementException("Collection contains no element matching the predicate.");
    }

    public final List<String> getSelectableCurrencyCodes() {
        Set<FiatMoney> prices;
        Subscription subscription = (Subscription) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) this.store.getState().getSubscriptions()));
        if (subscription == null || (prices = subscription.getPrices()) == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(prices, 10));
        for (FiatMoney fiatMoney : prices) {
            arrayList.add(fiatMoney.getCurrency().getCurrencyCode());
        }
        return arrayList;
    }

    public final void retry() {
        if (!this.disposables.isDisposed() && this.store.getState().getStage() == SubscribeState.Stage.FAILURE) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda5
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SubscribeViewModel.m1058retry$lambda3((SubscribeState) obj);
                }
            });
            refresh();
        }
    }

    /* renamed from: retry$lambda-3 */
    public static final SubscribeState m1058retry$lambda3(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.INIT, false, 95, null);
    }

    public final void refresh() {
        this.disposables.clear();
        Observable<Currency> observableSubscriptionCurrency = SignalStore.donationsValues().getObservableSubscriptionCurrency();
        Single<List<Subscription>> subscriptions = this.subscriptionsRepository.getSubscriptions();
        refreshActiveSubscription();
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy$default(LevelUpdate.INSTANCE.isProcessing(), (Function1) null, (Function0) null, new SubscribeViewModel$refresh$1(this), 3, (Object) null));
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(subscriptions, SubscribeViewModel$refresh$2.INSTANCE, new Function1<List<? extends Subscription>, Unit>(observableSubscriptionCurrency, this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$refresh$3
            final /* synthetic */ Observable<Currency> $currency;
            final /* synthetic */ SubscribeViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.$currency = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(List<? extends Subscription> list) {
                invoke((List<Subscription>) list);
                return Unit.INSTANCE;
            }

            public final void invoke(List<Subscription> list) {
                Intrinsics.checkNotNullParameter(list, "subscriptions");
                if (!list.isEmpty()) {
                    Set<FiatMoney> prices = list.get(0).getPrices();
                    ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(prices, 10));
                    for (FiatMoney fiatMoney : prices) {
                        arrayList.add(fiatMoney.getCurrency());
                    }
                    if (!arrayList.contains(SignalStore.donationsValues().getSubscriptionCurrency())) {
                        String str = SubscribeViewModel.TAG;
                        Log.w(str, "Unsupported currency selection. Defaulting to USD. " + this.$currency + " isn't supported.");
                        Currency usd = PlatformCurrencyUtil.INSTANCE.getUSD();
                        Subscriber subscriber = SignalStore.donationsValues().getSubscriber(usd);
                        if (subscriber == null) {
                            SubscriberId generate = SubscriberId.generate();
                            Intrinsics.checkNotNullExpressionValue(generate, "generate()");
                            String currencyCode = usd.getCurrencyCode();
                            Intrinsics.checkNotNullExpressionValue(currencyCode, "usd.currencyCode");
                            subscriber = new Subscriber(generate, currencyCode);
                        }
                        SignalStore.donationsValues().setSubscriber(subscriber);
                        this.this$0.donationPaymentRepository.scheduleSyncForAccountRecordChange();
                    }
                }
            }
        }));
        CompositeDisposable compositeDisposable = this.disposables;
        Observable combineLatest = Observable.combineLatest(subscriptions.toObservable(), this.activeSubscriptionSubject, new BiFunction() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return new Pair((List) obj, (ActiveSubscription) obj2);
            }
        });
        SubscribeViewModel$refresh$5 subscribeViewModel$refresh$5 = new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$refresh$5
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "p0");
                ((SubscribeViewModel) this.receiver).handleSubscriptionDataLoadFailure(th);
            }
        };
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(allSubscri…scriptionSubject, ::Pair)");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(combineLatest, subscribeViewModel$refresh$5, (Function0) null, new SubscribeViewModel$refresh$6(this), 2, (Object) null));
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe = observableSubscriptionCurrency.subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SubscribeViewModel.m1055refresh$lambda5(SubscribeViewModel.this, (Currency) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "currency.subscribe { sel…tion = selection) }\n    }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe);
    }

    /* renamed from: refresh$lambda-5 */
    public static final void m1055refresh$lambda5(SubscribeViewModel subscribeViewModel, Currency currency) {
        Intrinsics.checkNotNullParameter(subscribeViewModel, "this$0");
        subscribeViewModel.store.update(new Function(currency) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda10
            public final /* synthetic */ Currency f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel.m1056refresh$lambda5$lambda4(this.f$0, (SubscribeState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-5$lambda-4 */
    public static final SubscribeState m1056refresh$lambda5$lambda4(Currency currency, SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        Intrinsics.checkNotNullExpressionValue(currency, "selection");
        return SubscribeState.copy$default(subscribeState, currency, null, null, null, false, null, false, 126, null);
    }

    public final void handleSubscriptionDataLoadFailure(Throwable th) {
        Log.w(TAG, "Could not load subscription data", th);
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda9
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel.m1054handleSubscriptionDataLoadFailure$lambda6((SubscribeState) obj);
            }
        });
    }

    /* renamed from: handleSubscriptionDataLoadFailure$lambda-6 */
    public static final SubscribeState m1054handleSubscriptionDataLoadFailure$lambda6(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.FAILURE, false, 95, null);
    }

    public final void refreshActiveSubscription() {
        SubscribersKt.subscribeBy(this.subscriptionsRepository.getActiveSubscription(), new Function1<Throwable, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$refreshActiveSubscription$1
            final /* synthetic */ SubscribeViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
                invoke(th);
                return Unit.INSTANCE;
            }

            public final void invoke(Throwable th) {
                Intrinsics.checkNotNullParameter(th, "it");
                this.this$0.activeSubscriptionSubject.onNext(ActiveSubscription.EMPTY);
            }
        }, new Function1<ActiveSubscription, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$refreshActiveSubscription$2
            final /* synthetic */ SubscribeViewModel this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(ActiveSubscription activeSubscription) {
                invoke(activeSubscription);
                return Unit.INSTANCE;
            }

            public final void invoke(ActiveSubscription activeSubscription) {
                Intrinsics.checkNotNullParameter(activeSubscription, "it");
                this.this$0.activeSubscriptionSubject.onNext(activeSubscription);
            }
        });
    }

    public final Subscription resolveSelectedSubscription(ActiveSubscription activeSubscription, List<Subscription> list) {
        Object obj;
        boolean z;
        if (!activeSubscription.isActive()) {
            return (Subscription) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) list));
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Subscription) obj).getLevel() == activeSubscription.getActiveSubscription().getLevel()) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (Subscription) obj;
    }

    public final Completable cancelActiveSubscriptionIfNecessary() {
        Completable flatMapCompletable = Single.just(Boolean.valueOf(SignalStore.donationsValues().getShouldCancelSubscriptionBeforeNextSubscribeAttempt())).flatMapCompletable(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel.m1052cancelActiveSubscriptionIfNecessary$lambda9(SubscribeViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapCompletable, "just(SignalStore.donatio….complete()\n      }\n    }");
        return flatMapCompletable;
    }

    /* renamed from: cancelActiveSubscriptionIfNecessary$lambda-9 */
    public static final CompletableSource m1052cancelActiveSubscriptionIfNecessary$lambda9(SubscribeViewModel subscribeViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(subscribeViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "it");
        if (bool.booleanValue()) {
            return subscribeViewModel.donationPaymentRepository.cancelActiveSubscription().doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda8
                @Override // io.reactivex.rxjava3.functions.Action
                public final void run() {
                    SubscribeViewModel.m1053cancelActiveSubscriptionIfNecessary$lambda9$lambda8();
                }
            });
        }
        return Completable.complete();
    }

    /* renamed from: cancelActiveSubscriptionIfNecessary$lambda-9$lambda-8 */
    public static final void m1053cancelActiveSubscriptionIfNecessary$lambda9$lambda8() {
        SignalStore.donationsValues().updateLocalStateForManualCancellation();
        MultiDeviceSubscriptionSyncRequestJob.Companion.enqueue();
    }

    /* renamed from: cancel$lambda-10 */
    public static final SubscribeState m1051cancel$lambda10(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.CANCELLING, false, 95, null);
    }

    public final void cancel() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda6
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel.m1051cancel$lambda10((SubscribeState) obj);
            }
        });
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(this.donationPaymentRepository.cancelActiveSubscription(), new SubscribeViewModel$cancel$2(this), new SubscribeViewModel$cancel$3(this)));
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        Subscription subscription = this.subscriptionToPurchase;
        this.subscriptionToPurchase = null;
        this.donationPaymentRepository.onActivityResult(i, i2, intent, this.fetchTokenRequestCode, new SubscribeViewModel$onActivityResult$1(subscription, this));
    }

    /* renamed from: updateSubscription$lambda-11 */
    public static final SubscribeState m1060updateSubscription$lambda11(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.PAYMENT_PIPELINE, false, 95, null);
    }

    public final void updateSubscription() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel.m1060updateSubscription$lambda11((SubscribeState) obj);
            }
        });
        Completable cancelActiveSubscriptionIfNecessary = cancelActiveSubscriptionIfNecessary();
        DonationPaymentRepository donationPaymentRepository = this.donationPaymentRepository;
        Subscription selectedSubscription = this.store.getState().getSelectedSubscription();
        Intrinsics.checkNotNull(selectedSubscription);
        Completable andThen = cancelActiveSubscriptionIfNecessary.andThen(donationPaymentRepository.setSubscriptionLevel(String.valueOf(selectedSubscription.getLevel())));
        Intrinsics.checkNotNullExpressionValue(andThen, "cancelActiveSubscription…tion!!.level.toString()))");
        SubscribersKt.subscribeBy(andThen, new SubscribeViewModel$updateSubscription$2(this), new SubscribeViewModel$updateSubscription$3(this));
    }

    public final void requestTokenFromGooglePay() {
        SubscribeState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        SubscribeState subscribeState = state;
        if (subscribeState.getSelectedSubscription() != null) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda3
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SubscribeViewModel.m1057requestTokenFromGooglePay$lambda12((SubscribeState) obj);
                }
            });
            Currency currencySelection = subscribeState.getCurrencySelection();
            this.subscriptionToPurchase = subscribeState.getSelectedSubscription();
            DonationPaymentRepository donationPaymentRepository = this.donationPaymentRepository;
            for (FiatMoney fiatMoney : subscribeState.getSelectedSubscription().getPrices()) {
                if (Intrinsics.areEqual(fiatMoney.getCurrency(), currencySelection)) {
                    donationPaymentRepository.requestTokenFromGooglePay(fiatMoney, subscribeState.getSelectedSubscription().getName(), this.fetchTokenRequestCode);
                    return;
                }
            }
            throw new NoSuchElementException("Collection contains no element matching the predicate.");
        }
    }

    /* renamed from: requestTokenFromGooglePay$lambda-12 */
    public static final SubscribeState m1057requestTokenFromGooglePay$lambda12(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.TOKEN_REQUEST, false, 95, null);
    }

    /* renamed from: setSelectedSubscription$lambda-14 */
    public static final SubscribeState m1059setSelectedSubscription$lambda14(Subscription subscription, SubscribeState subscribeState) {
        Intrinsics.checkNotNullParameter(subscription, "$subscription");
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, subscription, null, false, null, false, 123, null);
    }

    public final void setSelectedSubscription(Subscription subscription) {
        Intrinsics.checkNotNullParameter(subscription, "subscription");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$$ExternalSyntheticLambda11
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel.m1059setSelectedSubscription$lambda14(Subscription.this, (SubscribeState) obj);
            }
        });
    }

    /* compiled from: SubscribeViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "fetchTokenRequestCode", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;I)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DonationPaymentRepository donationPaymentRepository;
        private final int fetchTokenRequestCode;
        private final SubscriptionsRepository subscriptionsRepository;

        public Factory(SubscriptionsRepository subscriptionsRepository, DonationPaymentRepository donationPaymentRepository, int i) {
            Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
            Intrinsics.checkNotNullParameter(donationPaymentRepository, "donationPaymentRepository");
            this.subscriptionsRepository = subscriptionsRepository;
            this.donationPaymentRepository = donationPaymentRepository;
            this.fetchTokenRequestCode = i;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new SubscribeViewModel(this.subscriptionsRepository, this.donationPaymentRepository, this.fetchTokenRequestCode));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }

    /* compiled from: SubscribeViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
