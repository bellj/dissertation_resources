package org.thoughtcrime.securesms.components;

import android.app.Dialog;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.DialogPreference;
import androidx.preference.PreferenceDialogFragmentCompat;
import java.net.URI;
import java.net.URISyntaxException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class CustomDefaultPreference extends DialogPreference {
    private static final String TAG = Log.tag(CustomDefaultPreference.class);
    private final String customPreference = getKey();
    private final String customToggle;
    private String defaultValue;
    private final int inputType;
    private CustomDefaultPreferenceDialogFragmentCompat.CustomPreferenceValidator validator;

    public CustomDefaultPreference(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16843296, R.attr.custom_pref_toggle});
        this.inputType = obtainStyledAttributes.getInt(0, 0);
        this.customToggle = obtainStyledAttributes.getString(1);
        this.validator = new CustomDefaultPreferenceDialogFragmentCompat.NullValidator();
        obtainStyledAttributes.recycle();
        setPersistent(false);
        setDialogLayoutResource(R.layout.custom_default_preference_dialog);
    }

    public CustomDefaultPreference setValidator(CustomDefaultPreferenceDialogFragmentCompat.CustomPreferenceValidator customPreferenceValidator) {
        this.validator = customPreferenceValidator;
        return this;
    }

    public CustomDefaultPreference setDefaultValue(String str) {
        this.defaultValue = str;
        setSummary(getSummary());
        return this;
    }

    @Override // androidx.preference.Preference
    public String getSummary() {
        if (isCustom()) {
            return getContext().getString(R.string.CustomDefaultPreference_using_custom, getPrettyPrintValue(getCustomValue()));
        }
        return getContext().getString(R.string.CustomDefaultPreference_using_default, getPrettyPrintValue(getDefaultValue()));
    }

    public String getPrettyPrintValue(String str) {
        return TextUtils.isEmpty(str) ? getContext().getString(R.string.CustomDefaultPreference_none) : str;
    }

    public boolean isCustom() {
        return TextSecurePreferences.getBooleanPreference(getContext(), this.customToggle, false);
    }

    public void setCustom(boolean z) {
        TextSecurePreferences.setBooleanPreference(getContext(), this.customToggle, z);
    }

    public String getCustomValue() {
        return TextSecurePreferences.getStringPreference(getContext(), this.customPreference, "");
    }

    public void setCustomValue(String str) {
        TextSecurePreferences.setStringPreference(getContext(), this.customPreference, str);
    }

    private String getDefaultValue() {
        return this.defaultValue;
    }

    /* loaded from: classes4.dex */
    public static class CustomDefaultPreferenceDialogFragmentCompat extends PreferenceDialogFragmentCompat {
        private static final String INPUT_TYPE;
        private EditText customText;
        private TextView defaultLabel;
        private Spinner spinner;

        /* loaded from: classes4.dex */
        public interface CustomPreferenceValidator {
            boolean isValid(String str);
        }

        public static CustomDefaultPreferenceDialogFragmentCompat newInstance(String str) {
            CustomDefaultPreferenceDialogFragmentCompat customDefaultPreferenceDialogFragmentCompat = new CustomDefaultPreferenceDialogFragmentCompat();
            Bundle bundle = new Bundle(1);
            bundle.putString("key", str);
            customDefaultPreferenceDialogFragmentCompat.setArguments(bundle);
            return customDefaultPreferenceDialogFragmentCompat;
        }

        @Override // androidx.preference.PreferenceDialogFragmentCompat
        public void onBindDialogView(View view) {
            Log.i(CustomDefaultPreference.TAG, "onBindDialogView");
            super.onBindDialogView(view);
            CustomDefaultPreference customDefaultPreference = (CustomDefaultPreference) getPreference();
            this.spinner = (Spinner) view.findViewById(R.id.default_or_custom);
            this.defaultLabel = (TextView) view.findViewById(R.id.default_label);
            EditText editText = (EditText) view.findViewById(R.id.custom_edit);
            this.customText = editText;
            editText.setInputType(customDefaultPreference.inputType);
            this.customText.addTextChangedListener(new TextValidator());
            this.customText.setText(customDefaultPreference.getCustomValue());
            this.spinner.setOnItemSelectedListener(new SelectionLister());
            this.defaultLabel.setText(customDefaultPreference.getPrettyPrintValue(customDefaultPreference.defaultValue));
        }

        @Override // androidx.preference.PreferenceDialogFragmentCompat, androidx.fragment.app.DialogFragment
        public Dialog onCreateDialog(Bundle bundle) {
            Dialog onCreateDialog = super.onCreateDialog(bundle);
            if (((CustomDefaultPreference) getPreference()).isCustom()) {
                this.spinner.setSelection(1, true);
            } else {
                this.spinner.setSelection(0, true);
            }
            return onCreateDialog;
        }

        @Override // androidx.preference.PreferenceDialogFragmentCompat
        public void onDialogClosed(boolean z) {
            CustomDefaultPreference customDefaultPreference = (CustomDefaultPreference) getPreference();
            if (z) {
                Spinner spinner = this.spinner;
                if (spinner != null) {
                    boolean z2 = true;
                    if (spinner.getSelectedItemPosition() != 1) {
                        z2 = false;
                    }
                    customDefaultPreference.setCustom(z2);
                }
                EditText editText = this.customText;
                if (editText != null) {
                    customDefaultPreference.setCustomValue(editText.getText().toString());
                }
                customDefaultPreference.setSummary(customDefaultPreference.getSummary());
            }
        }

        /* loaded from: classes4.dex */
        private static class NullValidator implements CustomPreferenceValidator {
            @Override // org.thoughtcrime.securesms.components.CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.CustomPreferenceValidator
            public boolean isValid(String str) {
                return true;
            }

            private NullValidator() {
            }
        }

        /* loaded from: classes4.dex */
        private class TextValidator implements TextWatcher {
            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            private TextValidator() {
                CustomDefaultPreferenceDialogFragmentCompat.this = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                CustomDefaultPreference customDefaultPreference = (CustomDefaultPreference) CustomDefaultPreferenceDialogFragmentCompat.this.getPreference();
                if (CustomDefaultPreferenceDialogFragmentCompat.this.spinner.getSelectedItemPosition() == 1) {
                    ((AlertDialog) CustomDefaultPreferenceDialogFragmentCompat.this.getDialog()).getButton(-1).setEnabled(customDefaultPreference.validator.isValid(editable.toString()));
                }
            }
        }

        /* loaded from: classes4.dex */
        public static class UriValidator implements CustomPreferenceValidator {
            @Override // org.thoughtcrime.securesms.components.CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.CustomPreferenceValidator
            public boolean isValid(String str) {
                if (TextUtils.isEmpty(str)) {
                    return true;
                }
                try {
                    new URI(str);
                    return true;
                } catch (URISyntaxException unused) {
                    return false;
                }
            }
        }

        /* loaded from: classes4.dex */
        public static class HostnameValidator implements CustomPreferenceValidator {
            @Override // org.thoughtcrime.securesms.components.CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.CustomPreferenceValidator
            public boolean isValid(String str) {
                if (TextUtils.isEmpty(str)) {
                    return true;
                }
                try {
                    new URI(null, str, null, null);
                    return true;
                } catch (URISyntaxException unused) {
                    return false;
                }
            }
        }

        /* loaded from: classes4.dex */
        public static class PortValidator implements CustomPreferenceValidator {
            @Override // org.thoughtcrime.securesms.components.CustomDefaultPreference.CustomDefaultPreferenceDialogFragmentCompat.CustomPreferenceValidator
            public boolean isValid(String str) {
                try {
                    Integer.parseInt(str);
                    return true;
                } catch (NumberFormatException unused) {
                    return false;
                }
            }
        }

        /* loaded from: classes4.dex */
        private class SelectionLister implements AdapterView.OnItemSelectedListener {
            private SelectionLister() {
                CustomDefaultPreferenceDialogFragmentCompat.this = r1;
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                CustomDefaultPreference customDefaultPreference = (CustomDefaultPreference) CustomDefaultPreferenceDialogFragmentCompat.this.getPreference();
                Button button = ((AlertDialog) CustomDefaultPreferenceDialogFragmentCompat.this.getDialog()).getButton(-1);
                int i2 = 8;
                boolean z = false;
                CustomDefaultPreferenceDialogFragmentCompat.this.defaultLabel.setVisibility(i == 0 ? 0 : 8);
                EditText editText = CustomDefaultPreferenceDialogFragmentCompat.this.customText;
                if (i != 0) {
                    i2 = 0;
                }
                editText.setVisibility(i2);
                if (i == 0 || customDefaultPreference.validator.isValid(CustomDefaultPreferenceDialogFragmentCompat.this.customText.getText().toString())) {
                    z = true;
                }
                button.setEnabled(z);
            }

            @Override // android.widget.AdapterView.OnItemSelectedListener
            public void onNothingSelected(AdapterView<?> adapterView) {
                CustomDefaultPreferenceDialogFragmentCompat.this.defaultLabel.setVisibility(0);
                CustomDefaultPreferenceDialogFragmentCompat.this.customText.setVisibility(8);
            }
        }
    }
}
