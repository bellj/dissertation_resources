package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.GlideApp;

/* loaded from: classes4.dex */
public class TooltipPopup extends PopupWindow {
    public static final int POSITION_ABOVE;
    public static final int POSITION_BELOW;
    public static final int POSITION_END;
    private static final int POSITION_LEFT;
    private static final int POSITION_RIGHT;
    public static final int POSITION_START;
    private final View anchor;
    private final ImageView arrow;
    private final int position;
    private final int startMargin;

    public static Builder forTarget(View view) {
        return new Builder(view);
    }

    private TooltipPopup(View view, int i, int i2, String str, int i3, int i4, Object obj, PopupWindow.OnDismissListener onDismissListener) {
        super(LayoutInflater.from(view.getContext()).inflate(R.layout.tooltip, (ViewGroup) null), -2, -2);
        this.anchor = view;
        this.position = getRtlPosition(view.getContext(), i);
        this.startMargin = i2;
        if (i == 0) {
            this.arrow = (ImageView) getContentView().findViewById(R.id.tooltip_arrow_bottom);
        } else if (i == 1) {
            this.arrow = (ImageView) getContentView().findViewById(R.id.tooltip_arrow_top);
        } else if (i == 2) {
            this.arrow = (ImageView) getContentView().findViewById(R.id.tooltip_arrow_end);
        } else if (i == 3) {
            this.arrow = (ImageView) getContentView().findViewById(R.id.tooltip_arrow_start);
        } else {
            throw new AssertionError("Invalid position!");
        }
        this.arrow.setVisibility(0);
        TextView textView = (TextView) getContentView().findViewById(R.id.tooltip_text);
        textView.setText(str);
        if (i4 != 0) {
            textView.setTextColor(i4);
        }
        View findViewById = getContentView().findViewById(R.id.tooltip_bubble);
        if (i3 == 0) {
            findViewById.getBackground().setColorFilter(ContextCompat.getColor(view.getContext(), R.color.tooltip_default_color), PorterDuff.Mode.MULTIPLY);
            this.arrow.setColorFilter(ContextCompat.getColor(view.getContext(), R.color.tooltip_default_color), PorterDuff.Mode.MULTIPLY);
        } else {
            findViewById.getBackground().setColorFilter(i3, PorterDuff.Mode.MULTIPLY);
            this.arrow.setColorFilter(i3, PorterDuff.Mode.MULTIPLY);
        }
        if (obj != null) {
            ImageView imageView = (ImageView) getContentView().findViewById(R.id.tooltip_icon);
            imageView.setVisibility(0);
            GlideApp.with(view.getContext()).load(obj).into(imageView);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation(10.0f);
        }
        getContentView().setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.TooltipPopup$$ExternalSyntheticLambda5
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                TooltipPopup.this.lambda$new$0(view2);
            }
        });
        setOnDismissListener(onDismissListener);
        setBackgroundDrawable(null);
        setOutsideTouchable(true);
    }

    public /* synthetic */ void lambda$new$0(View view) {
        dismiss();
    }

    public void show() {
        int i;
        if (this.anchor.getWidth() == 0 && this.anchor.getHeight() == 0) {
            this.anchor.post(new Runnable() { // from class: org.thoughtcrime.securesms.components.TooltipPopup$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    TooltipPopup.this.show();
                }
            });
            return;
        }
        int i2 = 0;
        getContentView().measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        int dimensionPixelOffset = this.anchor.getContext().getResources().getDimensionPixelOffset(R.dimen.tooltip_popup_margin);
        int i3 = this.position;
        if (i3 == 0) {
            dimensionPixelOffset = -(this.anchor.getHeight() + getContentView().getMeasuredHeight() + dimensionPixelOffset);
            onLayout(new Runnable() { // from class: org.thoughtcrime.securesms.components.TooltipPopup$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    TooltipPopup.this.lambda$show$1();
                }
            });
        } else if (i3 == 1) {
            onLayout(new Runnable() { // from class: org.thoughtcrime.securesms.components.TooltipPopup$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    TooltipPopup.this.lambda$show$2();
                }
            });
        } else if (i3 == 4) {
            i2 = (-getContentView().getMeasuredWidth()) - dimensionPixelOffset;
            dimensionPixelOffset = -((getContentView().getMeasuredHeight() / 2) + (this.anchor.getHeight() / 2));
            onLayout(new Runnable() { // from class: org.thoughtcrime.securesms.components.TooltipPopup$$ExternalSyntheticLambda3
                @Override // java.lang.Runnable
                public final void run() {
                    TooltipPopup.this.lambda$show$3();
                }
            });
        } else if (i3 == 5) {
            i2 = this.anchor.getWidth() + dimensionPixelOffset;
            dimensionPixelOffset = -((getContentView().getMeasuredHeight() / 2) + (this.anchor.getHeight() / 2));
            onLayout(new Runnable() { // from class: org.thoughtcrime.securesms.components.TooltipPopup$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    TooltipPopup.this.lambda$show$4();
                }
            });
        } else {
            throw new AssertionError("Invalid tooltip position!");
        }
        int i4 = this.position;
        if (i4 == 0) {
            i2 += this.startMargin;
        } else if (i4 != 1) {
            if (i4 != 4) {
                if (i4 == 5) {
                    i2 -= this.startMargin;
                }
                showAsDropDown(this.anchor, i2, dimensionPixelOffset);
            }
            i = this.startMargin;
            i2 += i;
            showAsDropDown(this.anchor, i2, dimensionPixelOffset);
        }
        i = this.startMargin;
        i2 += i;
        showAsDropDown(this.anchor, i2, dimensionPixelOffset);
    }

    public /* synthetic */ void lambda$show$1() {
        setArrowHorizontalPosition(this.arrow, this.anchor);
    }

    public /* synthetic */ void lambda$show$2() {
        setArrowHorizontalPosition(this.arrow, this.anchor);
    }

    public /* synthetic */ void lambda$show$3() {
        setArrowVerticalPosition(this.arrow, this.anchor);
    }

    public /* synthetic */ void lambda$show$4() {
        setArrowVerticalPosition(this.arrow, this.anchor);
    }

    private void onLayout(final Runnable runnable) {
        getContentView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() { // from class: org.thoughtcrime.securesms.components.TooltipPopup.1
            @Override // android.view.ViewTreeObserver.OnGlobalLayoutListener
            public void onGlobalLayout() {
                TooltipPopup.this.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                runnable.run();
            }
        });
    }

    private static void setArrowHorizontalPosition(View view, View view2) {
        view.setTranslationX((float) ((getAbsolutePosition(view2)[0] + (view2.getWidth() / 2)) - (getAbsolutePosition(view)[0] + (view.getWidth() / 2))));
    }

    private static void setArrowVerticalPosition(View view, View view2) {
        view.setTranslationY((float) ((getAbsolutePosition(view2)[1] + (view2.getHeight() / 2)) - (getAbsolutePosition(view)[1] + (view.getHeight() / 2))));
    }

    private static int[] getAbsolutePosition(View view) {
        int[] iArr = new int[2];
        view.getLocationOnScreen(iArr);
        return iArr;
    }

    private static int getRtlPosition(Context context, int i) {
        if (i == 0 || i == 1) {
            return i;
        }
        return context.getResources().getConfiguration().getLayoutDirection() == 1 ? i == 2 ? 5 : 4 : i == 2 ? 4 : 5;
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final View anchor;
        private int backgroundTint;
        private PopupWindow.OnDismissListener dismissListener;
        private Object iconGlideModel;
        private int setStartMargin;
        private int textColor;
        private int textResId;

        private Builder(View view) {
            this.anchor = view;
        }

        public Builder setBackgroundTint(int i) {
            this.backgroundTint = i;
            return this;
        }

        public Builder setTextColor(int i) {
            this.textColor = i;
            return this;
        }

        public Builder setText(int i) {
            this.textResId = i;
            return this;
        }

        public Builder setIconGlideModel(Object obj) {
            this.iconGlideModel = obj;
            return this;
        }

        public Builder setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
            this.dismissListener = onDismissListener;
            return this;
        }

        public Builder setStartMargin(int i) {
            this.setStartMargin = i;
            return this;
        }

        public TooltipPopup show(int i) {
            TooltipPopup tooltipPopup = new TooltipPopup(this.anchor, i, this.setStartMargin, this.anchor.getContext().getString(this.textResId), this.backgroundTint, this.textColor, this.iconGlideModel, this.dismissListener);
            tooltipPopup.show();
            return tooltipPopup;
        }
    }
}
