package org.thoughtcrime.securesms.components.settings.app.chats;

import kotlin.Metadata;

/* compiled from: ChatsSettingsState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0014\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J;\u0010\u0014\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00032\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\nR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\n¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsState;", "", "generateLinkPreviews", "", "useAddressBook", "useSystemEmoji", "enterKeySends", "chatBackupsEnabled", "(ZZZZZ)V", "getChatBackupsEnabled", "()Z", "getEnterKeySends", "getGenerateLinkPreviews", "getUseAddressBook", "getUseSystemEmoji", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatsSettingsState {
    private final boolean chatBackupsEnabled;
    private final boolean enterKeySends;
    private final boolean generateLinkPreviews;
    private final boolean useAddressBook;
    private final boolean useSystemEmoji;

    public static /* synthetic */ ChatsSettingsState copy$default(ChatsSettingsState chatsSettingsState, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, int i, Object obj) {
        if ((i & 1) != 0) {
            z = chatsSettingsState.generateLinkPreviews;
        }
        if ((i & 2) != 0) {
            z2 = chatsSettingsState.useAddressBook;
        }
        if ((i & 4) != 0) {
            z3 = chatsSettingsState.useSystemEmoji;
        }
        if ((i & 8) != 0) {
            z4 = chatsSettingsState.enterKeySends;
        }
        if ((i & 16) != 0) {
            z5 = chatsSettingsState.chatBackupsEnabled;
        }
        return chatsSettingsState.copy(z, z2, z3, z4, z5);
    }

    public final boolean component1() {
        return this.generateLinkPreviews;
    }

    public final boolean component2() {
        return this.useAddressBook;
    }

    public final boolean component3() {
        return this.useSystemEmoji;
    }

    public final boolean component4() {
        return this.enterKeySends;
    }

    public final boolean component5() {
        return this.chatBackupsEnabled;
    }

    public final ChatsSettingsState copy(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        return new ChatsSettingsState(z, z2, z3, z4, z5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ChatsSettingsState)) {
            return false;
        }
        ChatsSettingsState chatsSettingsState = (ChatsSettingsState) obj;
        return this.generateLinkPreviews == chatsSettingsState.generateLinkPreviews && this.useAddressBook == chatsSettingsState.useAddressBook && this.useSystemEmoji == chatsSettingsState.useSystemEmoji && this.enterKeySends == chatsSettingsState.enterKeySends && this.chatBackupsEnabled == chatsSettingsState.chatBackupsEnabled;
    }

    public int hashCode() {
        boolean z = this.generateLinkPreviews;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.useAddressBook;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.useSystemEmoji;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.enterKeySends;
        if (z4) {
            z4 = true;
        }
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = (i13 + i14) * 31;
        boolean z5 = this.chatBackupsEnabled;
        if (!z5) {
            i = z5 ? 1 : 0;
        }
        return i17 + i;
    }

    public String toString() {
        return "ChatsSettingsState(generateLinkPreviews=" + this.generateLinkPreviews + ", useAddressBook=" + this.useAddressBook + ", useSystemEmoji=" + this.useSystemEmoji + ", enterKeySends=" + this.enterKeySends + ", chatBackupsEnabled=" + this.chatBackupsEnabled + ')';
    }

    public ChatsSettingsState(boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        this.generateLinkPreviews = z;
        this.useAddressBook = z2;
        this.useSystemEmoji = z3;
        this.enterKeySends = z4;
        this.chatBackupsEnabled = z5;
    }

    public final boolean getGenerateLinkPreviews() {
        return this.generateLinkPreviews;
    }

    public final boolean getUseAddressBook() {
        return this.useAddressBook;
    }

    public final boolean getUseSystemEmoji() {
        return this.useSystemEmoji;
    }

    public final boolean getEnterKeySends() {
        return this.enterKeySends;
    }

    public final boolean getChatBackupsEnabled() {
        return this.chatBackupsEnabled;
    }
}
