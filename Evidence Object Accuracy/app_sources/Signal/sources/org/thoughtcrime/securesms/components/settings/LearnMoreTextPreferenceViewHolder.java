package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/LearnMoreTextPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/LearnMoreTextPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LearnMoreTextPreferenceViewHolder extends PreferenceViewHolder<LearnMoreTextPreference> {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public LearnMoreTextPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
    }

    public void bind(LearnMoreTextPreference learnMoreTextPreference) {
        Intrinsics.checkNotNullParameter(learnMoreTextPreference, "model");
        super.bind((LearnMoreTextPreferenceViewHolder) learnMoreTextPreference);
        ((LearnMoreTextView) getTitleView()).setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.LearnMoreTextPreferenceViewHolder$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                LearnMoreTextPreferenceViewHolder.$r8$lambda$ElSbXVIEzmj1Fq6mHKdHHbhsXOw(LearnMoreTextPreference.this, view);
            }
        });
        ((LearnMoreTextView) getSummaryView()).setOnLinkClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.LearnMoreTextPreferenceViewHolder$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                LearnMoreTextPreferenceViewHolder.m527$r8$lambda$KJhW0yqfmNC6f1jANDAHbv8BZ8(LearnMoreTextPreference.this, view);
            }
        });
    }

    /* renamed from: bind$lambda-0 */
    public static final void m528bind$lambda0(LearnMoreTextPreference learnMoreTextPreference, View view) {
        Intrinsics.checkNotNullParameter(learnMoreTextPreference, "$model");
        learnMoreTextPreference.getOnClick().invoke();
    }

    /* renamed from: bind$lambda-1 */
    public static final void m529bind$lambda1(LearnMoreTextPreference learnMoreTextPreference, View view) {
        Intrinsics.checkNotNullParameter(learnMoreTextPreference, "$model");
        learnMoreTextPreference.getOnClick().invoke();
    }
}
