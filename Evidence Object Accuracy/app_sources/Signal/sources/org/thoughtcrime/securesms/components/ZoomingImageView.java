package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.widget.FrameLayout;
import androidx.exifinterface.media.ExifInterface;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestListener;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.davemorrissey.labs.subscaleview.decoder.DecoderFactory;
import com.github.chrisbanes.photoview.PhotoView;
import java.io.IOException;
import java.io.InputStream;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.subsampling.AttachmentBitmapDecoder;
import org.thoughtcrime.securesms.components.subsampling.AttachmentRegionDecoder;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.PartAuthority;
import org.thoughtcrime.securesms.util.ActionRequestListener;
import org.thoughtcrime.securesms.util.BitmapDecodingException;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ZoomingImageView extends FrameLayout {
    private static final float LARGE_IMAGES_ZOOM_LEVEL_MAX;
    private static final float LARGE_IMAGES_ZOOM_LEVEL_MID;
    private static final float SMALL_IMAGES_ZOOM_LEVEL_MAX;
    private static final float SMALL_IMAGES_ZOOM_LEVEL_MID;
    private static final String TAG = Log.tag(ZoomingImageView.class);
    private static final float ZOOM_LEVEL_MIN;
    private static final int ZOOM_TRANSITION_DURATION;
    private final PhotoView photoView;
    private final SubsamplingScaleImageView subsamplingImageView;

    public ZoomingImageView(Context context) {
        this(context, null);
    }

    public ZoomingImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ZoomingImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.zooming_image_view, this);
        PhotoView photoView = (PhotoView) findViewById(R.id.image_view);
        this.photoView = photoView;
        SubsamplingScaleImageView subsamplingScaleImageView = (SubsamplingScaleImageView) findViewById(R.id.subsampling_image_view);
        this.subsamplingImageView = subsamplingScaleImageView;
        photoView.setZoomTransitionDuration(300);
        photoView.setScaleLevels(ZOOM_LEVEL_MIN, 3.0f, SMALL_IMAGES_ZOOM_LEVEL_MAX);
        subsamplingScaleImageView.setDoubleTapZoomDuration(300);
        subsamplingScaleImageView.setDoubleTapZoomScale(LARGE_IMAGES_ZOOM_LEVEL_MID);
        subsamplingScaleImageView.setMaxScale(LARGE_IMAGES_ZOOM_LEVEL_MAX);
        photoView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ZoomingImageView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ZoomingImageView.m500$r8$lambda$pPpT7reEQ42hDXZK9VeVSQxtMk(ZoomingImageView.this, view);
            }
        });
        subsamplingScaleImageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ZoomingImageView$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ZoomingImageView.$r8$lambda$uVjOXIncjKByMD8RWrzmvzdG_dM(ZoomingImageView.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$new$0(View view) {
        callOnClick();
    }

    public /* synthetic */ void lambda$new$1(View view) {
        callOnClick();
    }

    public void setImageUri(GlideRequests glideRequests, Uri uri, String str, Runnable runnable) {
        Context context = getContext();
        int maxTextureSize = BitmapUtil.getMaxTextureSize();
        String str2 = TAG;
        Log.i(str2, "Max texture size: " + maxTextureSize);
        SimpleTask.run(ViewUtil.getActivityLifecycle(this), new SimpleTask.BackgroundTask(str, context, uri) { // from class: org.thoughtcrime.securesms.components.ZoomingImageView$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$0;
            public final /* synthetic */ Context f$1;
            public final /* synthetic */ Uri f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return ZoomingImageView.m498$r8$lambda$73nRffTn1KgdXSMa1CXb3zLdvw(this.f$0, this.f$1, this.f$2);
            }
        }, new SimpleTask.ForegroundTask(maxTextureSize, glideRequests, uri, runnable) { // from class: org.thoughtcrime.securesms.components.ZoomingImageView$$ExternalSyntheticLambda3
            public final /* synthetic */ int f$1;
            public final /* synthetic */ GlideRequests f$2;
            public final /* synthetic */ Uri f$3;
            public final /* synthetic */ Runnable f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                ZoomingImageView.m499$r8$lambda$UN_9gmFYS3xfnT_3y8twsAbtJs(ZoomingImageView.this, this.f$1, this.f$2, this.f$3, this.f$4, (Pair) obj);
            }
        });
    }

    public static /* synthetic */ Pair lambda$setImageUri$2(String str, Context context, Uri uri) {
        if (MediaUtil.isGif(str)) {
            return null;
        }
        try {
            return BitmapUtil.getDimensions(PartAuthority.getAttachmentStream(context, uri));
        } catch (IOException | BitmapDecodingException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public /* synthetic */ void lambda$setImageUri$3(int i, GlideRequests glideRequests, Uri uri, Runnable runnable, Pair pair) {
        String str;
        String str2 = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Dimensions: ");
        if (pair == null) {
            str = "(null)";
        } else {
            str = pair.first + ", " + pair.second;
        }
        sb.append(str);
        Log.i(str2, sb.toString());
        if (pair == null || (((Integer) pair.first).intValue() <= i && ((Integer) pair.second).intValue() <= i)) {
            Log.i(str2, "Loading in standard image view...");
            setImageViewUri(glideRequests, uri, runnable);
            return;
        }
        Log.i(str2, "Loading in subsampling image view...");
        setSubsamplingImageViewUri(uri);
        runnable.run();
    }

    private void setImageViewUri(GlideRequests glideRequests, Uri uri, Runnable runnable) {
        this.photoView.setVisibility(0);
        this.subsamplingImageView.setVisibility(8);
        glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).diskCacheStrategy(DiskCacheStrategy.NONE).dontTransform().override(Integer.MIN_VALUE, Integer.MIN_VALUE).addListener((RequestListener<Drawable>) ActionRequestListener.onEither(runnable)).into(this.photoView);
    }

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0022 */
    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: android.net.Uri */
    /* JADX DEBUG: Multi-variable search result rejected for r0v4, resolved type: com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r5v2, types: [com.davemorrissey.labs.subscaleview.ImageSource] */
    /* JADX WARN: Type inference failed for: r5v3 */
    private void setSubsamplingImageViewUri(Uri uri) {
        Uri uri2;
        this.subsamplingImageView.setBitmapDecoderFactory(new AttachmentBitmapDecoderFactory());
        this.subsamplingImageView.setRegionDecoderFactory(new AttachmentRegionDecoderFactory());
        this.subsamplingImageView.setVisibility(0);
        this.photoView.setVisibility(8);
        try {
            InputStream attachmentStream = PartAuthority.getAttachmentStream(getContext(), uri);
            int exifOrientation = BitmapUtil.getExifOrientation(new ExifInterface(attachmentStream));
            attachmentStream.close();
            if (exifOrientation == 6) {
                this.subsamplingImageView.setOrientation(90);
                uri2 = uri;
            } else if (exifOrientation == 3) {
                this.subsamplingImageView.setOrientation(SubsamplingScaleImageView.ORIENTATION_180);
                uri2 = uri;
            } else if (exifOrientation == 8) {
                this.subsamplingImageView.setOrientation(SubsamplingScaleImageView.ORIENTATION_270);
                uri2 = uri;
            } else {
                this.subsamplingImageView.setOrientation(0);
                uri2 = uri;
            }
        } catch (IOException e) {
            Log.w(TAG, e);
            uri2 = uri;
        }
        SubsamplingScaleImageView subsamplingScaleImageView = this.subsamplingImageView;
        uri = ImageSource.uri(uri2);
        subsamplingScaleImageView.setImage(uri);
    }

    public void cleanup() {
        this.photoView.setImageDrawable(null);
        this.subsamplingImageView.recycle();
    }

    /* loaded from: classes4.dex */
    public static class AttachmentBitmapDecoderFactory implements DecoderFactory<AttachmentBitmapDecoder> {
        private AttachmentBitmapDecoderFactory() {
        }

        @Override // com.davemorrissey.labs.subscaleview.decoder.DecoderFactory
        public AttachmentBitmapDecoder make() throws IllegalAccessException, InstantiationException {
            return new AttachmentBitmapDecoder();
        }
    }

    /* loaded from: classes4.dex */
    public static class AttachmentRegionDecoderFactory implements DecoderFactory<AttachmentRegionDecoder> {
        private AttachmentRegionDecoderFactory() {
        }

        @Override // com.davemorrissey.labs.subscaleview.decoder.DecoderFactory
        public AttachmentRegionDecoder make() throws IllegalAccessException, InstantiationException {
            return new AttachmentRegionDecoder();
        }
    }
}
