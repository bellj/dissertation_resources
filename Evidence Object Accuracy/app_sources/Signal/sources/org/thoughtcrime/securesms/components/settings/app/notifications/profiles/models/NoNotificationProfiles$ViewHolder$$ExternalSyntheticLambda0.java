package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NoNotificationProfiles;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NoNotificationProfiles$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NoNotificationProfiles.Model f$0;

    public /* synthetic */ NoNotificationProfiles$ViewHolder$$ExternalSyntheticLambda0(NoNotificationProfiles.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NoNotificationProfiles.ViewHolder.m811bind$lambda0(this.f$0, view);
    }
}
