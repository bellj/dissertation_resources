package org.thoughtcrime.securesms.components.registration;

import android.content.Context;
import android.graphics.PorterDuff;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class VerificationPinKeyboard extends FrameLayout {
    private ImageView failureView;
    private KeyboardView keyboardView;
    private OnKeyPressListener listener;
    private ImageView lockedView;
    private ProgressBar progressBar;
    private ImageView successView;

    /* loaded from: classes4.dex */
    public interface OnKeyPressListener {
        void onKeyPress(int i);
    }

    public VerificationPinKeyboard(Context context) {
        super(context);
        initialize();
    }

    public VerificationPinKeyboard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public VerificationPinKeyboard(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    public VerificationPinKeyboard(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        initialize();
    }

    private void initialize() {
        FrameLayout.inflate(getContext(), R.layout.verification_pin_keyboard_view, this);
        this.keyboardView = (KeyboardView) findViewById(R.id.keyboard_view);
        this.progressBar = (ProgressBar) findViewById(R.id.progress);
        this.successView = (ImageView) findViewById(R.id.success);
        this.failureView = (ImageView) findViewById(R.id.failure);
        this.lockedView = (ImageView) findViewById(R.id.locked);
        this.keyboardView.setPreviewEnabled(false);
        this.keyboardView.setKeyboard(new Keyboard(getContext(), R.xml.pin_keyboard));
        this.keyboardView.setOnKeyboardActionListener(new KeyboardView.OnKeyboardActionListener() { // from class: org.thoughtcrime.securesms.components.registration.VerificationPinKeyboard.1
            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void onKey(int i, int[] iArr) {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void onRelease(int i) {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void onText(CharSequence charSequence) {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void swipeDown() {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void swipeLeft() {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void swipeRight() {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void swipeUp() {
            }

            @Override // android.inputmethodservice.KeyboardView.OnKeyboardActionListener
            public void onPress(int i) {
                if (VerificationPinKeyboard.this.listener != null) {
                    VerificationPinKeyboard.this.listener.onKeyPress(i);
                }
            }
        });
        displayKeyboard();
    }

    public void setOnKeyPressListener(OnKeyPressListener onKeyPressListener) {
        this.listener = onKeyPressListener;
    }

    public void displayKeyboard() {
        this.keyboardView.setVisibility(0);
        this.progressBar.setVisibility(8);
        this.successView.setVisibility(8);
        this.failureView.setVisibility(8);
        this.lockedView.setVisibility(8);
    }

    public void displayProgress() {
        this.keyboardView.setVisibility(4);
        this.progressBar.setVisibility(0);
        this.successView.setVisibility(8);
        this.failureView.setVisibility(8);
        this.lockedView.setVisibility(8);
    }

    public ListenableFuture<Boolean> displaySuccess() {
        final SettableFuture settableFuture = new SettableFuture();
        this.keyboardView.setVisibility(4);
        this.progressBar.setVisibility(8);
        this.failureView.setVisibility(8);
        this.lockedView.setVisibility(8);
        this.successView.getBackground().setColorFilter(getResources().getColor(R.color.green_500), PorterDuff.Mode.SRC_IN);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(new OvershootInterpolator());
        scaleAnimation.setDuration(800);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.components.registration.VerificationPinKeyboard.2
            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationRepeat(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationStart(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                settableFuture.set(Boolean.TRUE);
            }
        });
        ViewUtil.animateIn(this.successView, scaleAnimation);
        return settableFuture;
    }

    public ListenableFuture<Boolean> displayFailure() {
        final SettableFuture settableFuture = new SettableFuture();
        this.keyboardView.setVisibility(4);
        this.progressBar.setVisibility(8);
        this.failureView.setVisibility(8);
        this.lockedView.setVisibility(8);
        this.failureView.getBackground().setColorFilter(getResources().getColor(R.color.red_500), PorterDuff.Mode.SRC_IN);
        this.failureView.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 30.0f, 0.0f, 0.0f);
        translateAnimation.setDuration(50);
        translateAnimation.setRepeatCount(7);
        translateAnimation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.components.registration.VerificationPinKeyboard.3
            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationRepeat(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationStart(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                settableFuture.set(Boolean.TRUE);
            }
        });
        this.failureView.startAnimation(translateAnimation);
        return settableFuture;
    }

    public ListenableFuture<Boolean> displayLocked() {
        final SettableFuture settableFuture = new SettableFuture();
        this.keyboardView.setVisibility(4);
        this.progressBar.setVisibility(8);
        this.failureView.setVisibility(8);
        this.lockedView.setVisibility(8);
        this.lockedView.getBackground().setColorFilter(getResources().getColor(R.color.green_500), PorterDuff.Mode.SRC_IN);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.0f, 1.0f, 0.0f, 1.0f, 1, 0.5f, 1, 0.5f);
        scaleAnimation.setInterpolator(new OvershootInterpolator());
        scaleAnimation.setDuration(800);
        scaleAnimation.setAnimationListener(new Animation.AnimationListener() { // from class: org.thoughtcrime.securesms.components.registration.VerificationPinKeyboard.4
            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationRepeat(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationStart(Animation animation) {
            }

            @Override // android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                settableFuture.set(Boolean.TRUE);
            }
        });
        ViewUtil.animateIn(this.lockedView, scaleAnimation);
        return settableFuture;
    }
}
