package org.thoughtcrime.securesms.components.settings;

import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.models.AsyncSwitch;
import org.thoughtcrime.securesms.components.settings.models.Button;
import org.thoughtcrime.securesms.components.settings.models.Space;
import org.thoughtcrime.securesms.components.settings.models.Text;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0018\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J6\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000e\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010J\\\u0010\u0011\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u00102\u0010\b\u0002\u0010\u0016\u001a\n\u0012\u0004\u0012\u00020\f\u0018\u00010\u0010J\u0012\u0010\u0017\u001a\u00020\b2\n\u0010\u0018\u001a\u0006\u0012\u0002\b\u00030\u0005J\u0006\u0010\u0019\u001a\u00020\bJ$\u0010\u001a\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\b\u0001\u0010\u001b\u001a\u00020\u001cJ,\u0010\u001d\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010J>\u0010\u001e\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\b0\u0010JG\u0010\u001f\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\"0!2\u0006\u0010#\u001a\u00020$2\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020$\u0012\u0004\u0012\u00020\b0&¢\u0006\u0002\u0010'J\u000e\u0010(\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ&\u0010)\u001a\u00020\b2\u0006\u0010*\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010Jg\u0010+\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010,\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\"0!2\u0006\u0010#\u001a\u00020\u001c2\b\b\u0002\u0010-\u001a\u00020\f2\u0012\u0010%\u001a\u000e\u0012\u0004\u0012\u00020\u001c\u0012\u0004\u0012\u00020\b0&¢\u0006\u0002\u0010.J:\u0010/\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010J2\u00100\u001a\u00020\b2\u0006\u0010*\u001a\u00020\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010J\u000e\u00101\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u001cJ\u000e\u00101\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0010\u00102\u001a\u00020\b2\b\b\u0001\u00103\u001a\u00020\u001cJF\u00104\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u00142\b\b\u0002\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010J\u001e\u00105\u001a\u00020\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\n2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\nJ\u0006\u00106\u001a\u000207J&\u00108\u001a\u00020\b2\u0006\u0010*\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\b0\u0010R&\u0010\u0003\u001a\u001a\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00050\u0004j\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u0005`\u0006X\u0004¢\u0006\u0002\n\u0000¨\u00069"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "", "()V", "children", "Ljava/util/ArrayList;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "Lkotlin/collections/ArrayList;", "asyncSwitchPref", "", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "isEnabled", "", "isChecked", "isProcessing", "onClick", "Lkotlin/Function0;", "clickPref", "summary", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "iconEnd", "onLongClick", "customPref", "customPreference", "dividerPref", "externalLinkPref", "linkId", "", "learnMoreTextPref", "longClickPref", "multiSelectPref", "listItems", "", "", "selected", "", "onSelected", "Lkotlin/Function1;", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Z[Ljava/lang/String;[ZLkotlin/jvm/functions/Function1;)V", "noPadTextPref", "primaryButton", DraftDatabase.Draft.TEXT, "radioListPref", "dialogTitle", "confirmAction", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Z[Ljava/lang/String;IZLkotlin/jvm/functions/Function1;)V", "radioPref", "secondaryButtonNoOutline", "sectionHeaderPref", "space", "pixels", "switchPref", "textPref", "toMappingModelList", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModelList;", "tonalButton", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DSLConfiguration {
    private final ArrayList<MappingModel<?>> children = new ArrayList<>();

    public final void customPref(MappingModel<?> mappingModel) {
        Intrinsics.checkNotNullParameter(mappingModel, "customPreference");
        this.children.add(mappingModel);
    }

    public final void radioListPref(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, DSLSettingsText dSLSettingsText2, boolean z, String[] strArr, int i, boolean z2, Function1<? super Integer, Unit> function1) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(dSLSettingsText2, "dialogTitle");
        Intrinsics.checkNotNullParameter(strArr, "listItems");
        Intrinsics.checkNotNullParameter(function1, "onSelected");
        this.children.add(new RadioListPreference(dSLSettingsText, dSLSettingsIcon, z, dSLSettingsText2, strArr, i, function1, z2));
    }

    public final void multiSelectPref(DSLSettingsText dSLSettingsText, boolean z, String[] strArr, boolean[] zArr, Function1<? super boolean[], Unit> function1) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(strArr, "listItems");
        Intrinsics.checkNotNullParameter(zArr, "selected");
        Intrinsics.checkNotNullParameter(function1, "onSelected");
        this.children.add(new MultiSelectListPreference(dSLSettingsText, z, strArr, zArr, function1));
    }

    public final void asyncSwitchPref(DSLSettingsText dSLSettingsText, boolean z, boolean z2, boolean z3, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new AsyncSwitch.Model(dSLSettingsText, z, z2, z3, function0));
    }

    public final void switchPref(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, boolean z, boolean z2, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new SwitchPreference(dSLSettingsText, dSLSettingsText2, dSLSettingsIcon, z, z2, function0));
    }

    public static /* synthetic */ void radioPref$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, boolean z, boolean z2, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            dSLSettingsText2 = null;
        }
        dSLConfiguration.radioPref(dSLSettingsText, dSLSettingsText2, (i & 4) != 0 ? true : z, z2, function0);
    }

    public final void radioPref(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, boolean z, boolean z2, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new RadioPreference(dSLSettingsText, dSLSettingsText2, z, z2, function0));
    }

    public final void clickPref(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, DSLSettingsIcon dSLSettingsIcon2, boolean z, Function0<Unit> function0, Function0<Boolean> function02) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new ClickPreference(dSLSettingsText, dSLSettingsText2, dSLSettingsIcon, dSLSettingsIcon2, z, function0, function02));
    }

    public final void longClickPref(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onLongClick");
        this.children.add(new LongClickPreference(dSLSettingsText, dSLSettingsText2, dSLSettingsIcon, z, function0));
    }

    public static /* synthetic */ void externalLinkPref$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            dSLSettingsIcon = null;
        }
        dSLConfiguration.externalLinkPref(dSLSettingsText, dSLSettingsIcon, i);
    }

    public final void externalLinkPref(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, int i) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        this.children.add(new ExternalLinkPreference(dSLSettingsText, dSLSettingsIcon, i));
    }

    public final void dividerPref() {
        this.children.add(new DividerPreference());
    }

    public final void sectionHeaderPref(DSLSettingsText dSLSettingsText) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        this.children.add(new SectionHeaderPreference(dSLSettingsText));
    }

    public final void sectionHeaderPref(int i) {
        this.children.add(new SectionHeaderPreference(DSLSettingsText.Companion.from(i, new DSLSettingsText.Modifier[0])));
    }

    public final void noPadTextPref(DSLSettingsText dSLSettingsText) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        this.children.add(new Text.Model(new Text(dSLSettingsText)));
    }

    public final void space(int i) {
        this.children.add(new Space.Model(new Space(i)));
    }

    public static /* synthetic */ void primaryButton$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, boolean z, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            z = true;
        }
        dSLConfiguration.primaryButton(dSLSettingsText, z, function0);
    }

    public final void primaryButton(DSLSettingsText dSLSettingsText, boolean z, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.Draft.TEXT);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new Button.Model.Primary(dSLSettingsText, null, z, function0));
    }

    public static /* synthetic */ void tonalButton$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, boolean z, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            z = true;
        }
        dSLConfiguration.tonalButton(dSLSettingsText, z, function0);
    }

    public final void tonalButton(DSLSettingsText dSLSettingsText, boolean z, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.Draft.TEXT);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new Button.Model.Tonal(dSLSettingsText, null, z, function0));
    }

    public static /* synthetic */ void secondaryButtonNoOutline$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0 function0, int i, Object obj) {
        if ((i & 2) != 0) {
            dSLSettingsIcon = null;
        }
        if ((i & 4) != 0) {
            z = true;
        }
        dSLConfiguration.secondaryButtonNoOutline(dSLSettingsText, dSLSettingsIcon, z, function0);
    }

    public final void secondaryButtonNoOutline(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.Draft.TEXT);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new Button.Model.SecondaryNoOutline(dSLSettingsText, dSLSettingsIcon, z, function0));
    }

    public static /* synthetic */ void textPref$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, int i, Object obj) {
        if ((i & 1) != 0) {
            dSLSettingsText = null;
        }
        if ((i & 2) != 0) {
            dSLSettingsText2 = null;
        }
        dSLConfiguration.textPref(dSLSettingsText, dSLSettingsText2);
    }

    public final void textPref(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2) {
        this.children.add(new TextPreference(dSLSettingsText, dSLSettingsText2));
    }

    public static /* synthetic */ void learnMoreTextPref$default(DSLConfiguration dSLConfiguration, DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, Function0 function0, int i, Object obj) {
        if ((i & 1) != 0) {
            dSLSettingsText = null;
        }
        if ((i & 2) != 0) {
            dSLSettingsText2 = null;
        }
        dSLConfiguration.learnMoreTextPref(dSLSettingsText, dSLSettingsText2, function0);
    }

    public final void learnMoreTextPref(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.children.add(new LearnMoreTextPreference(dSLSettingsText, dSLSettingsText2, function0));
    }

    public final MappingModelList toMappingModelList() {
        MappingModelList mappingModelList = new MappingModelList();
        mappingModelList.addAll(this.children);
        return mappingModelList;
    }
}
