package org.thoughtcrime.securesms.components.settings.app;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AppSettingsFragment$SubscriptionPreferenceViewHolder$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ AppSettingsFragment.SubscriptionPreference f$0;

    public /* synthetic */ AppSettingsFragment$SubscriptionPreferenceViewHolder$$ExternalSyntheticLambda1(AppSettingsFragment.SubscriptionPreference subscriptionPreference) {
        this.f$0 = subscriptionPreference;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return AppSettingsFragment.SubscriptionPreferenceViewHolder.m553bind$lambda1(this.f$0, view);
    }
}
