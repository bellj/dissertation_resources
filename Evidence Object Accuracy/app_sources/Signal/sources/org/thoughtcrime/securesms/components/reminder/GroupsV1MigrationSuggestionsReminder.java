package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class GroupsV1MigrationSuggestionsReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    public GroupsV1MigrationSuggestionsReminder(Context context, List<RecipientId> list) {
        super(null, context.getResources().getQuantityString(R.plurals.GroupsV1MigrationSuggestionsReminder_members_couldnt_be_added_to_the_new_group, list.size(), Integer.valueOf(list.size())));
        addAction(new Reminder.Action(context.getResources().getQuantityString(R.plurals.GroupsV1MigrationSuggestionsReminder_add_members, list.size()), R.id.reminder_action_gv1_suggestion_add_members));
        addAction(new Reminder.Action(context.getResources().getString(R.string.GroupsV1MigrationSuggestionsReminder_no_thanks), R.id.reminder_action_gv1_suggestion_no_thanks));
    }
}
