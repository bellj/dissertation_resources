package org.thoughtcrime.securesms.components.settings.app.subscription.currency;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.keyvalue.DonationsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;

/* compiled from: SetCurrencyViewModel.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0002\u0012\u0013B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u000e\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\u000eX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyViewModel;", "Landroidx/lifecycle/ViewModel;", "isOneTime", "", "supportedCurrencyCodes", "", "", "(ZLjava/util/List;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "setSelectedCurrency", "", "selectedCurrencyCode", "CurrencyComparator", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SetCurrencyViewModel extends ViewModel {
    private final boolean isOneTime;
    private final LiveData<SetCurrencyState> state;
    private final Store<SetCurrencyState> store;

    public SetCurrencyViewModel(boolean z, List<String> list) {
        String str;
        Intrinsics.checkNotNullParameter(list, "supportedCurrencyCodes");
        this.isOneTime = z;
        if (z) {
            str = SignalStore.donationsValues().getOneTimeCurrency().getCurrencyCode();
        } else {
            str = SignalStore.donationsValues().getSubscriptionCurrency().getCurrencyCode();
        }
        Intrinsics.checkNotNullExpressionValue(str, "if (isOneTime) {\n       …cy().currencyCode\n      }");
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (String str2 : list) {
            arrayList.add(Currency.getInstance(str2));
        }
        Store<SetCurrencyState> store = new Store<>(new SetCurrencyState(str, CollectionsKt___CollectionsKt.sortedWith(arrayList, new CurrencyComparator(StringsKt__StringsKt.split$default((CharSequence) BuildConfig.DEFAULT_CURRENCIES, new String[]{","}, false, 0, 6, (Object) null)))));
        this.store = store;
        LiveData<SetCurrencyState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<SetCurrencyState> getState() {
        return this.state;
    }

    /* renamed from: setSelectedCurrency$lambda-0 */
    public static final SetCurrencyState m958setSelectedCurrency$lambda0(String str, SetCurrencyState setCurrencyState) {
        Intrinsics.checkNotNullParameter(str, "$selectedCurrencyCode");
        Intrinsics.checkNotNullExpressionValue(setCurrencyState, "it");
        return SetCurrencyState.copy$default(setCurrencyState, str, null, 2, null);
    }

    public final void setSelectedCurrency(String str) {
        Intrinsics.checkNotNullParameter(str, "selectedCurrencyCode");
        this.store.update(new Function(str) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SetCurrencyViewModel.m958setSelectedCurrency$lambda0(this.f$0, (SetCurrencyState) obj);
            }
        });
        if (this.isOneTime) {
            DonationsValues donationsValues = SignalStore.donationsValues();
            Currency instance = Currency.getInstance(str);
            Intrinsics.checkNotNullExpressionValue(instance, "getInstance(selectedCurrencyCode)");
            donationsValues.setOneTimeCurrency(instance);
            return;
        }
        Currency instance2 = Currency.getInstance(str);
        DonationsValues donationsValues2 = SignalStore.donationsValues();
        Intrinsics.checkNotNullExpressionValue(instance2, "currency");
        Subscriber subscriber = donationsValues2.getSubscriber(instance2);
        if (subscriber != null) {
            SignalStore.donationsValues().setSubscriber(subscriber);
            return;
        }
        DonationsValues donationsValues3 = SignalStore.donationsValues();
        SubscriberId generate = SubscriberId.generate();
        Intrinsics.checkNotNullExpressionValue(generate, "generate()");
        String currencyCode = instance2.getCurrencyCode();
        Intrinsics.checkNotNullExpressionValue(currencyCode, "currency.currencyCode");
        donationsValues3.setSubscriber(new Subscriber(generate, currencyCode));
    }

    /* compiled from: SetCurrencyViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0004\b\u0007\u0018\u0000 \f2\u0012\u0012\u0004\u0012\u00020\u00020\u0001j\b\u0012\u0004\u0012\u00020\u0002`\u0003:\u0001\fB\u0013\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0018\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyViewModel$CurrencyComparator;", "Ljava/util/Comparator;", "Ljava/util/Currency;", "Lkotlin/Comparator;", "defaults", "", "", "(Ljava/util/List;)V", "compare", "", "o1", "o2", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CurrencyComparator implements Comparator<Currency> {
        public static final Companion Companion = new Companion(null);
        private static final String USD;
        private final List<String> defaults;

        public CurrencyComparator(List<String> list) {
            Intrinsics.checkNotNullParameter(list, "defaults");
            this.defaults = list;
        }

        /* compiled from: SetCurrencyViewModel.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyViewModel$CurrencyComparator$Companion;", "", "()V", CurrencyComparator.USD, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }
        }

        public int compare(Currency currency, Currency currency2) {
            Intrinsics.checkNotNullParameter(currency, "o1");
            Intrinsics.checkNotNullParameter(currency2, "o2");
            boolean contains = this.defaults.contains(currency.getCurrencyCode());
            boolean contains2 = this.defaults.contains(currency2.getCurrencyCode());
            if (Intrinsics.areEqual(currency.getCurrencyCode(), currency2.getCurrencyCode())) {
                return 0;
            }
            if (!Intrinsics.areEqual(currency.getCurrencyCode(), USD)) {
                if (Intrinsics.areEqual(currency2.getCurrencyCode(), USD)) {
                    return 1;
                }
                if (contains && contains2) {
                    String displayName = currency.getDisplayName(Locale.getDefault());
                    String displayName2 = currency2.getDisplayName(Locale.getDefault());
                    Intrinsics.checkNotNullExpressionValue(displayName2, "o2.getDisplayName(Locale.getDefault())");
                    return displayName.compareTo(displayName2);
                } else if (!contains) {
                    if (contains2) {
                        return 1;
                    }
                    String displayName3 = currency.getDisplayName(Locale.getDefault());
                    String displayName4 = currency2.getDisplayName(Locale.getDefault());
                    Intrinsics.checkNotNullExpressionValue(displayName4, "o2.getDisplayName(Locale.getDefault())");
                    return displayName3.compareTo(displayName4);
                }
            }
            return -1;
        }
    }

    /* compiled from: SetCurrencyViewModel.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J%\u0010\b\u001a\u0002H\t\"\b\b\u0000\u0010\t*\u00020\n2\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u0002H\t0\fH\u0016¢\u0006\u0002\u0010\rR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "isOneTime", "", "supportedCurrencyCodes", "", "", "(ZLjava/util/List;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final boolean isOneTime;
        private final List<String> supportedCurrencyCodes;

        public Factory(boolean z, List<String> list) {
            Intrinsics.checkNotNullParameter(list, "supportedCurrencyCodes");
            this.isOneTime = z;
            this.supportedCurrencyCodes = list;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new SetCurrencyViewModel(this.isOneTime, this.supportedCurrencyCodes));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
