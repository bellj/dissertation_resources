package org.thoughtcrime.securesms.components.emoji;

import android.graphics.Bitmap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.thoughtcrime.securesms.components.emoji.EmojiProvider;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EmojiProvider$1$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ AtomicBoolean f$0;
    public final /* synthetic */ EmojiProvider.EmojiDrawable f$1;
    public final /* synthetic */ Bitmap f$2;
    public final /* synthetic */ Runnable f$3;

    public /* synthetic */ EmojiProvider$1$$ExternalSyntheticLambda0(AtomicBoolean atomicBoolean, EmojiProvider.EmojiDrawable emojiDrawable, Bitmap bitmap, Runnable runnable) {
        this.f$0 = atomicBoolean;
        this.f$1 = emojiDrawable;
        this.f$2 = bitmap;
        this.f$3 = runnable;
    }

    @Override // java.lang.Runnable
    public final void run() {
        EmojiProvider.AnonymousClass1.lambda$onSuccess$0(this.f$0, this.f$1, this.f$2, this.f$3);
    }
}
