package org.thoughtcrime.securesms.components.settings.app;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import j$.util.Optional;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversationlist.model.UnreadPayments;
import org.thoughtcrime.securesms.conversationlist.model.UnreadPaymentsLiveData;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: AppSettingsViewModel.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u000e\u001a\u00020\u000fR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00070\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "selfLiveData", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "unreadPaymentsLiveData", "Lorg/thoughtcrime/securesms/conversationlist/model/UnreadPaymentsLiveData;", "refreshExpiredGiftBadge", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppSettingsViewModel extends ViewModel {
    private final LiveData<Recipient> selfLiveData;
    private final LiveData<AppSettingsState> state;
    private final Store<AppSettingsState> store;
    private final UnreadPaymentsLiveData unreadPaymentsLiveData;

    /* JADX DEBUG: Type inference failed for r2v3. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, java.lang.Object, androidx.lifecycle.LiveData<Input> */
    public AppSettingsViewModel() {
        Recipient self = Recipient.self();
        Intrinsics.checkNotNullExpressionValue(self, "self()");
        Store<AppSettingsState> store = new Store<>(new AppSettingsState(self, 0, SignalStore.donationsValues().getExpiredGiftBadge() != null));
        this.store = store;
        UnreadPaymentsLiveData unreadPaymentsLiveData = new UnreadPaymentsLiveData();
        this.unreadPaymentsLiveData = unreadPaymentsLiveData;
        LiveData liveData = Recipient.self().live().getLiveData();
        Intrinsics.checkNotNullExpressionValue(liveData, "self().live().liveData");
        this.selfLiveData = liveData;
        LiveData<AppSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        store.update(unreadPaymentsLiveData, new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return AppSettingsViewModel.m556_init_$lambda1((Optional) obj, (AppSettingsState) obj2);
            }
        });
        store.update(liveData, new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return AppSettingsViewModel.m557_init_$lambda2((Recipient) obj, (AppSettingsState) obj2);
            }
        });
    }

    public final LiveData<AppSettingsState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-1 */
    public static final AppSettingsState m556_init_$lambda1(Optional optional, AppSettingsState appSettingsState) {
        Intrinsics.checkNotNullExpressionValue(appSettingsState, "state");
        Object orElse = optional.map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsViewModel$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return AppSettingsViewModel.m558lambda1$lambda0((UnreadPayments) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(0);
        Intrinsics.checkNotNullExpressionValue(orElse, "payments.map { it.unreadCount }.orElse(0)");
        return AppSettingsState.copy$default(appSettingsState, null, ((Number) orElse).intValue(), false, 5, null);
    }

    /* renamed from: lambda-1$lambda-0 */
    public static final Integer m558lambda1$lambda0(UnreadPayments unreadPayments) {
        return Integer.valueOf(unreadPayments.getUnreadCount());
    }

    /* renamed from: _init_$lambda-2 */
    public static final AppSettingsState m557_init_$lambda2(Recipient recipient, AppSettingsState appSettingsState) {
        Intrinsics.checkNotNullExpressionValue(appSettingsState, "state");
        Intrinsics.checkNotNullExpressionValue(recipient, "self");
        return AppSettingsState.copy$default(appSettingsState, recipient, 0, false, 6, null);
    }

    /* renamed from: refreshExpiredGiftBadge$lambda-3 */
    public static final AppSettingsState m559refreshExpiredGiftBadge$lambda3(AppSettingsState appSettingsState) {
        Intrinsics.checkNotNullExpressionValue(appSettingsState, "it");
        return AppSettingsState.copy$default(appSettingsState, null, 0, SignalStore.donationsValues().getExpiredGiftBadge() != null, 3, null);
    }

    public final void refreshExpiredGiftBadge() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AppSettingsViewModel.m559refreshExpiredGiftBadge$lambda3((AppSettingsState) obj);
            }
        });
    }
}
