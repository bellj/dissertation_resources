package org.thoughtcrime.securesms.components.settings.app.subscription;

import android.content.Intent;
import io.reactivex.rxjava3.subjects.Subject;
import kotlin.Metadata;

/* compiled from: DonationPaymentComponent.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bf\u0018\u00002\u00020\u0001:\u0001\u000bR\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005R\u0018\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007X¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\n¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "getDonationPaymentRepository", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "googlePayResultPublisher", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent$GooglePayResult;", "getGooglePayResultPublisher", "()Lio/reactivex/rxjava3/subjects/Subject;", "GooglePayResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface DonationPaymentComponent {
    DonationPaymentRepository getDonationPaymentRepository();

    Subject<GooglePayResult> getGooglePayResultPublisher();

    /* compiled from: DonationPaymentComponent.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001B\u001f\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007R\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent$GooglePayResult;", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "(IILandroid/content/Intent;)V", "getData", "()Landroid/content/Intent;", "getRequestCode", "()I", "getResultCode", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GooglePayResult {
        private final Intent data;
        private final int requestCode;
        private final int resultCode;

        public GooglePayResult(int i, int i2, Intent intent) {
            this.requestCode = i;
            this.resultCode = i2;
            this.data = intent;
        }

        public final Intent getData() {
            return this.data;
        }

        public final int getRequestCode() {
            return this.requestCode;
        }

        public final int getResultCode() {
            return this.resultCode;
        }
    }
}
