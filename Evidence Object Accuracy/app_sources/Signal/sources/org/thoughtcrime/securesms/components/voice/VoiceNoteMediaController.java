package org.thoughtcrime.securesms.components.voice;

import android.content.ComponentName;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.media.MediaBrowserCompat;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import androidx.arch.core.util.Function;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import j$.util.Optional;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* loaded from: classes4.dex */
public class VoiceNoteMediaController implements DefaultLifecycleObserver {
    public static final String EXTRA_MESSAGE_ID;
    public static final String EXTRA_PLAY_SINGLE;
    public static final String EXTRA_PROGRESS;
    public static final String EXTRA_THREAD_ID;
    private static final String TAG = Log.tag(VoiceNoteMediaController.class);
    private FragmentActivity activity;
    private MediaBrowserCompat mediaBrowser;
    private final MediaControllerCompatCallback mediaControllerCompatCallback = new MediaControllerCompatCallback();
    private ProgressEventHandler progressEventHandler;
    private MutableLiveData<VoiceNotePlaybackState> voiceNotePlaybackState = new MutableLiveData<>(VoiceNotePlaybackState.NONE);
    private LiveData<Optional<VoiceNotePlayerView.State>> voiceNotePlayerViewState;
    private VoiceNoteProximityWakeLockManager voiceNoteProximityWakeLockManager;

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
    }

    public VoiceNoteMediaController(FragmentActivity fragmentActivity) {
        this.activity = fragmentActivity;
        this.mediaBrowser = new MediaBrowserCompat(fragmentActivity, new ComponentName(fragmentActivity, VoiceNotePlaybackService.class), new ConnectionCallback(), null);
        fragmentActivity.getLifecycle().addObserver(this);
        this.voiceNotePlayerViewState = Transformations.switchMap(this.voiceNotePlaybackState, new Function() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController$$ExternalSyntheticLambda2
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return VoiceNoteMediaController.lambda$new$2(FragmentActivity.this, (VoiceNotePlaybackState) obj);
            }
        });
    }

    public static /* synthetic */ LiveData lambda$new$2(FragmentActivity fragmentActivity, VoiceNotePlaybackState voiceNotePlaybackState) {
        if (!(voiceNotePlaybackState.getClipType() instanceof VoiceNotePlaybackState.ClipType.Message)) {
            return new DefaultValueLiveData(Optional.empty());
        }
        VoiceNotePlaybackState.ClipType.Message message = (VoiceNotePlaybackState.ClipType.Message) voiceNotePlaybackState.getClipType();
        return Transformations.map(LiveDataUtil.combineLatest(Recipient.live(message.getSenderId()).getLiveDataResolved(), Recipient.live(message.getThreadRecipientId()).getLiveDataResolved(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return VoiceNoteMediaItemFactory.getTitle(FragmentActivity.this, (Recipient) obj, (Recipient) obj2, null);
            }
        }), new Function(message) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNoteMediaController$$ExternalSyntheticLambda1
            public final /* synthetic */ VoiceNotePlaybackState.ClipType.Message f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return VoiceNoteMediaController.lambda$new$1(VoiceNotePlaybackState.this, this.f$1, (String) obj);
            }
        });
    }

    public static /* synthetic */ Optional lambda$new$1(VoiceNotePlaybackState voiceNotePlaybackState, VoiceNotePlaybackState.ClipType.Message message, String str) {
        return Optional.of(new VoiceNotePlayerView.State(voiceNotePlaybackState.getUri(), message.getMessageId(), message.getThreadId(), !voiceNotePlaybackState.isPlaying(), message.getSenderId(), message.getThreadRecipientId(), message.getMessagePosition(), message.getTimestamp(), str, voiceNotePlaybackState.getPlayheadPositionMillis(), voiceNotePlaybackState.getTrackDuration(), voiceNotePlaybackState.getSpeed()));
    }

    public LiveData<VoiceNotePlaybackState> getVoiceNotePlaybackState() {
        return this.voiceNotePlaybackState;
    }

    public LiveData<Optional<VoiceNotePlayerView.State>> getVoiceNotePlayerViewState() {
        return this.voiceNotePlayerViewState;
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onResume(LifecycleOwner lifecycleOwner) {
        this.mediaBrowser.disconnect();
        this.mediaBrowser.connect();
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onPause(LifecycleOwner lifecycleOwner) {
        clearProgressEventHandler();
        if (MediaControllerCompat.getMediaController(this.activity) != null) {
            MediaControllerCompat.getMediaController(this.activity).unregisterCallback(this.mediaControllerCompatCallback);
        }
        this.mediaBrowser.disconnect();
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onDestroy(LifecycleOwner lifecycleOwner) {
        VoiceNoteProximityWakeLockManager voiceNoteProximityWakeLockManager = this.voiceNoteProximityWakeLockManager;
        if (voiceNoteProximityWakeLockManager != null) {
            voiceNoteProximityWakeLockManager.unregisterCallbacksAndRelease();
            this.voiceNoteProximityWakeLockManager.unregisterFromLifecycle();
            this.voiceNoteProximityWakeLockManager = null;
        }
        this.activity.getLifecycle().removeObserver(this);
        this.activity = null;
    }

    public static boolean isPlayerActive(PlaybackStateCompat playbackStateCompat) {
        return playbackStateCompat.getState() == 6 || playbackStateCompat.getState() == 3;
    }

    private static boolean isPlayerPaused(PlaybackStateCompat playbackStateCompat) {
        return playbackStateCompat.getState() == 2;
    }

    public static boolean isPlayerStopped(PlaybackStateCompat playbackStateCompat) {
        return playbackStateCompat.getState() <= 1;
    }

    private MediaControllerCompat getMediaController() {
        FragmentActivity fragmentActivity = this.activity;
        if (fragmentActivity != null) {
            return MediaControllerCompat.getMediaController(fragmentActivity);
        }
        return null;
    }

    public void startConsecutivePlayback(Uri uri, long j, double d) {
        startPlayback(uri, j, -1, d, false);
    }

    public void startSinglePlayback(Uri uri, long j, double d) {
        startPlayback(uri, j, -1, d, true);
    }

    public void startSinglePlaybackForDraft(Uri uri, long j, double d) {
        startPlayback(uri, -1, j, d, true);
    }

    private void startPlayback(Uri uri, long j, long j2, double d, boolean z) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called startPlayback before controller was set. (" + getActivityName() + ")");
        } else if (isCurrentTrack(uri)) {
            long j3 = getMediaController().getMetadata().getLong("android.media.metadata.DURATION");
            MediaControllerCompat.TransportControls transportControls = getMediaController().getTransportControls();
            double d2 = (double) j3;
            Double.isNaN(d2);
            transportControls.seekTo((long) (d2 * d));
            getMediaController().getTransportControls().play();
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong(EXTRA_MESSAGE_ID, j);
            bundle.putLong(EXTRA_THREAD_ID, j2);
            bundle.putDouble(EXTRA_PROGRESS, d);
            bundle.putBoolean(EXTRA_PLAY_SINGLE, z);
            getMediaController().getTransportControls().playFromUri(uri, bundle);
        }
    }

    public void resumePlayback(Uri uri, long j) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called resumePlayback before controller was set. (" + getActivityName() + ")");
        } else if (isCurrentTrack(uri)) {
            getMediaController().getTransportControls().play();
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong(EXTRA_MESSAGE_ID, j);
            bundle.putLong(EXTRA_THREAD_ID, -1);
            bundle.putDouble(EXTRA_PROGRESS, 0.0d);
            bundle.putBoolean(EXTRA_PLAY_SINGLE, true);
            getMediaController().getTransportControls().playFromUri(uri, bundle);
        }
    }

    public void pausePlayback(Uri uri) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called pausePlayback(uri) before controller was set. (" + getActivityName() + ")");
        } else if (isCurrentTrack(uri)) {
            getMediaController().getTransportControls().pause();
        }
    }

    public void pausePlayback() {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called pausePlayback before controller was set. (" + getActivityName() + ")");
            return;
        }
        getMediaController().getTransportControls().pause();
    }

    public void seekToPosition(Uri uri, double d) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called seekToPosition before controller was set. (" + getActivityName() + ")");
        } else if (isCurrentTrack(uri)) {
            long j = getMediaController().getMetadata().getLong("android.media.metadata.DURATION");
            getMediaController().getTransportControls().pause();
            MediaControllerCompat.TransportControls transportControls = getMediaController().getTransportControls();
            double d2 = (double) j;
            Double.isNaN(d2);
            transportControls.seekTo((long) (d2 * d));
            getMediaController().getTransportControls().play();
        }
    }

    public void stopPlaybackAndReset(Uri uri) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called stopPlaybackAndReset before controller was set. (" + getActivityName() + ")");
        } else if (isCurrentTrack(uri)) {
            getMediaController().getTransportControls().stop();
        }
    }

    public void setPlaybackSpeed(Uri uri, float f) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called setPlaybackSpeed before controller was set. (" + getActivityName() + ")");
        } else if (isCurrentTrack(uri)) {
            Bundle bundle = new Bundle();
            bundle.putFloat(VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED, f);
            getMediaController().sendCommand(VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED, bundle, null);
        }
    }

    private boolean isCurrentTrack(Uri uri) {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called isCurrentTrack before controller was set. (" + getActivityName() + ")");
            return false;
        }
        MediaMetadataCompat metadata = getMediaController().getMetadata();
        if (metadata == null || !Objects.equals(metadata.getDescription().getMediaUri(), uri)) {
            return false;
        }
        return true;
    }

    public void notifyProgressEventHandler() {
        if (getMediaController() == null) {
            String str = TAG;
            Log.w(str, "Called notifyProgressEventHandler before controller was set. (" + getActivityName() + ")");
        } else if (this.progressEventHandler == null && this.activity != null) {
            ProgressEventHandler progressEventHandler = new ProgressEventHandler(getMediaController(), this.voiceNotePlaybackState);
            this.progressEventHandler = progressEventHandler;
            progressEventHandler.sendEmptyMessage(0);
        }
    }

    public void clearProgressEventHandler() {
        if (this.progressEventHandler != null) {
            this.progressEventHandler = null;
        }
    }

    private String getActivityName() {
        FragmentActivity fragmentActivity = this.activity;
        if (fragmentActivity == null) {
            return "Activity is null";
        }
        return fragmentActivity.getLocalClassName();
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public final class ConnectionCallback extends MediaBrowserCompat.ConnectionCallback {
        private ConnectionCallback() {
            VoiceNoteMediaController.this = r1;
        }

        @Override // android.support.v4.media.MediaBrowserCompat.ConnectionCallback
        public void onConnected() {
            MediaControllerCompat mediaControllerCompat = new MediaControllerCompat(VoiceNoteMediaController.this.activity, VoiceNoteMediaController.this.mediaBrowser.getSessionToken());
            MediaControllerCompat.setMediaController(VoiceNoteMediaController.this.activity, mediaControllerCompat);
            MediaMetadataCompat metadata = mediaControllerCompat.getMetadata();
            if (VoiceNoteMediaController.canExtractPlaybackInformationFromMetadata(metadata)) {
                VoiceNotePlaybackState extractStateFromMetadata = VoiceNoteMediaController.extractStateFromMetadata(mediaControllerCompat, metadata, null);
                if (extractStateFromMetadata != null) {
                    VoiceNoteMediaController.this.voiceNotePlaybackState.postValue(extractStateFromMetadata);
                } else {
                    VoiceNoteMediaController.this.voiceNotePlaybackState.postValue(VoiceNotePlaybackState.NONE);
                }
            }
            cleanUpOldProximityWakeLockManager();
            VoiceNoteMediaController voiceNoteMediaController = VoiceNoteMediaController.this;
            voiceNoteMediaController.voiceNoteProximityWakeLockManager = new VoiceNoteProximityWakeLockManager(voiceNoteMediaController.activity, mediaControllerCompat);
            mediaControllerCompat.registerCallback(VoiceNoteMediaController.this.mediaControllerCompatCallback);
            VoiceNoteMediaController.this.mediaControllerCompatCallback.onPlaybackStateChanged(mediaControllerCompat.getPlaybackState());
        }

        @Override // android.support.v4.media.MediaBrowserCompat.ConnectionCallback
        public void onConnectionSuspended() {
            Log.d(VoiceNoteMediaController.TAG, "Voice note MediaBrowser connection suspended.");
            cleanUpOldProximityWakeLockManager();
        }

        @Override // android.support.v4.media.MediaBrowserCompat.ConnectionCallback
        public void onConnectionFailed() {
            Log.d(VoiceNoteMediaController.TAG, "Voice note MediaBrowser connection failed.");
            cleanUpOldProximityWakeLockManager();
        }

        private void cleanUpOldProximityWakeLockManager() {
            if (VoiceNoteMediaController.this.voiceNoteProximityWakeLockManager != null) {
                Log.d(VoiceNoteMediaController.TAG, "Session reconnected, cleaning up old wake lock manager");
                VoiceNoteMediaController.this.voiceNoteProximityWakeLockManager.unregisterCallbacksAndRelease();
                VoiceNoteMediaController.this.voiceNoteProximityWakeLockManager.unregisterFromLifecycle();
                VoiceNoteMediaController.this.voiceNoteProximityWakeLockManager = null;
            }
        }
    }

    public static boolean canExtractPlaybackInformationFromMetadata(MediaMetadataCompat mediaMetadataCompat) {
        return (mediaMetadataCompat == null || mediaMetadataCompat.getDescription() == null || mediaMetadataCompat.getDescription().getMediaUri() == null) ? false : true;
    }

    public static VoiceNotePlaybackState extractStateFromMetadata(MediaControllerCompat mediaControllerCompat, MediaMetadataCompat mediaMetadataCompat, VoiceNotePlaybackState voiceNotePlaybackState) {
        Uri mediaUri = mediaMetadataCompat.getDescription().getMediaUri();
        Objects.requireNonNull(mediaUri);
        boolean z = mediaUri.equals(VoiceNoteMediaItemFactory.NEXT_URI) || mediaUri.equals(VoiceNoteMediaItemFactory.END_URI);
        long position = mediaControllerCompat.getPlaybackState().getPosition();
        long j = mediaMetadataCompat.getLong("android.media.metadata.DURATION");
        Bundle extras = mediaControllerCompat.getExtras();
        float f = extras != null ? extras.getFloat(VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED, 1.0f) : 1.0f;
        if (voiceNotePlaybackState != null && mediaUri.equals(voiceNotePlaybackState.getUri())) {
            if (position < 0 && voiceNotePlaybackState.getPlayheadPositionMillis() >= 0) {
                position = voiceNotePlaybackState.getPlayheadPositionMillis();
            }
            if (j <= 0 && voiceNotePlaybackState.getTrackDuration() > 0) {
                j = voiceNotePlaybackState.getTrackDuration();
            }
        }
        if (j <= 0 || position < 0 || position > j) {
            return null;
        }
        return new VoiceNotePlaybackState(mediaUri, position, j, z, f, isPlayerActive(mediaControllerCompat.getPlaybackState()), getClipType(mediaMetadataCompat.getBundle()));
    }

    public static VoiceNotePlaybackState constructPlaybackState(MediaControllerCompat mediaControllerCompat, VoiceNotePlaybackState voiceNotePlaybackState) {
        MediaMetadataCompat metadata = mediaControllerCompat.getMetadata();
        if (isPlayerActive(mediaControllerCompat.getPlaybackState()) && canExtractPlaybackInformationFromMetadata(metadata)) {
            return extractStateFromMetadata(mediaControllerCompat, metadata, voiceNotePlaybackState);
        }
        if (!isPlayerPaused(mediaControllerCompat.getPlaybackState()) || metadata == null) {
            return VoiceNotePlaybackState.NONE;
        }
        long position = mediaControllerCompat.getPlaybackState().getPosition();
        long j = metadata.getLong("android.media.metadata.DURATION");
        if (voiceNotePlaybackState == null || position >= j) {
            return VoiceNotePlaybackState.NONE;
        }
        return voiceNotePlaybackState.asPaused();
    }

    private static VoiceNotePlaybackState.ClipType getClipType(Bundle bundle) {
        long j;
        long j2;
        long j3;
        RecipientId recipientId;
        RecipientId recipientId2;
        long j4;
        RecipientId recipientId3 = RecipientId.UNKNOWN;
        if (bundle != null) {
            long j5 = bundle.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_ID, -1);
            long j6 = bundle.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_POSITION, -1);
            long j7 = bundle.getLong(VoiceNoteMediaItemFactory.EXTRA_THREAD_ID, -1);
            long j8 = bundle.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_TIMESTAMP, -1);
            String string = bundle.getString(VoiceNoteMediaItemFactory.EXTRA_INDIVIDUAL_RECIPIENT_ID);
            RecipientId from = string != null ? RecipientId.from(string) : recipientId3;
            String string2 = bundle.getString(VoiceNoteMediaItemFactory.EXTRA_THREAD_RECIPIENT_ID);
            if (string2 != null) {
                recipientId3 = RecipientId.from(string2);
            }
            recipientId = recipientId3;
            j4 = j5;
            j3 = j6;
            j2 = j7;
            j = j8;
            recipientId2 = from;
        } else {
            recipientId2 = recipientId3;
            recipientId = recipientId2;
            j4 = -1;
            j3 = -1;
            j2 = -1;
            j = -1;
        }
        if (j4 != -1) {
            return new VoiceNotePlaybackState.ClipType.Message(j4, recipientId2, recipientId, j3, j2, j);
        }
        return VoiceNotePlaybackState.ClipType.Draft.INSTANCE;
    }

    /* loaded from: classes4.dex */
    public static class ProgressEventHandler extends Handler {
        private final MediaControllerCompat mediaController;
        private final MutableLiveData<VoiceNotePlaybackState> voiceNotePlaybackState;

        private ProgressEventHandler(MediaControllerCompat mediaControllerCompat, MutableLiveData<VoiceNotePlaybackState> mutableLiveData) {
            super(Looper.getMainLooper());
            this.mediaController = mediaControllerCompat;
            this.voiceNotePlaybackState = mutableLiveData;
        }

        @Override // android.os.Handler
        public void handleMessage(Message message) {
            VoiceNotePlaybackState constructPlaybackState = VoiceNoteMediaController.constructPlaybackState(this.mediaController, this.voiceNotePlaybackState.getValue());
            if (constructPlaybackState != null) {
                this.voiceNotePlaybackState.postValue(constructPlaybackState);
            }
            if (VoiceNoteMediaController.isPlayerActive(this.mediaController.getPlaybackState())) {
                sendEmptyMessageDelayed(0, 50);
            }
        }
    }

    /* loaded from: classes4.dex */
    public final class MediaControllerCompatCallback extends MediaControllerCompat.Callback {
        private MediaControllerCompatCallback() {
            VoiceNoteMediaController.this = r1;
        }

        @Override // android.support.v4.media.session.MediaControllerCompat.Callback
        public void onPlaybackStateChanged(PlaybackStateCompat playbackStateCompat) {
            if (VoiceNoteMediaController.isPlayerActive(playbackStateCompat)) {
                VoiceNoteMediaController.this.notifyProgressEventHandler();
                return;
            }
            VoiceNoteMediaController.this.clearProgressEventHandler();
            if (VoiceNoteMediaController.isPlayerStopped(playbackStateCompat)) {
                VoiceNoteMediaController.this.voiceNotePlaybackState.postValue(VoiceNotePlaybackState.NONE);
            }
        }
    }
}
