package org.thoughtcrime.securesms.components.menu;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: SignalContextMenu.kt */
@Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\r\u0018\u00002\u00020\u0001:\u0003)*+BY\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\u0002\u0010\u0012J\b\u0010(\u001a\u00020\u0000H\u0002R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u001a\u001a\u00020\u001b¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u000e\u0010\u001e\u001a\u00020\u001fX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b&\u0010'¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu;", "Landroid/widget/PopupWindow;", "anchor", "Landroid/view/View;", "container", "Landroid/view/ViewGroup;", "items", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "baseOffsetX", "", "baseOffsetY", "horizontalPosition", "Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;", "verticalPosition", "Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;", "onDismiss", "Ljava/lang/Runnable;", "(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/List;IILorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;Ljava/lang/Runnable;)V", "getAnchor", "()Landroid/view/View;", "getBaseOffsetX", "()I", "getBaseOffsetY", "getContainer", "()Landroid/view/ViewGroup;", "context", "Landroid/content/Context;", "getContext", "()Landroid/content/Context;", "contextMenuList", "Lorg/thoughtcrime/securesms/components/menu/ContextMenuList;", "getHorizontalPosition", "()Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;", "getItems", "()Ljava/util/List;", "getOnDismiss", "()Ljava/lang/Runnable;", "getVerticalPosition", "()Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;", "show", "Builder", "HorizontalPosition", "VerticalPosition", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SignalContextMenu extends PopupWindow {
    private final View anchor;
    private final int baseOffsetX;
    private final int baseOffsetY;
    private final ViewGroup container;
    private final Context context;
    private final ContextMenuList contextMenuList;
    private final HorizontalPosition horizontalPosition;
    private final List<ActionItem> items;
    private final Runnable onDismiss;
    private final VerticalPosition verticalPosition;

    /* compiled from: SignalContextMenu.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;", "", "(Ljava/lang/String;I)V", "START", "END", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum HorizontalPosition {
        START,
        END
    }

    /* compiled from: SignalContextMenu.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;", "", "(Ljava/lang/String;I)V", "ABOVE", "BELOW", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum VerticalPosition {
        ABOVE,
        BELOW
    }

    /* compiled from: SignalContextMenu.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[HorizontalPosition.values().length];
            iArr[HorizontalPosition.START.ordinal()] = 1;
            iArr[HorizontalPosition.END.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public /* synthetic */ SignalContextMenu(View view, ViewGroup viewGroup, List list, int i, int i2, HorizontalPosition horizontalPosition, VerticalPosition verticalPosition, Runnable runnable, DefaultConstructorMarker defaultConstructorMarker) {
        this(view, viewGroup, list, i, i2, horizontalPosition, verticalPosition, runnable);
    }

    public final View getAnchor() {
        return this.anchor;
    }

    public final ViewGroup getContainer() {
        return this.container;
    }

    public final List<ActionItem> getItems() {
        return this.items;
    }

    public final int getBaseOffsetX() {
        return this.baseOffsetX;
    }

    public final int getBaseOffsetY() {
        return this.baseOffsetY;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    /* synthetic */ SignalContextMenu(android.view.View r13, android.view.ViewGroup r14, java.util.List r15, int r16, int r17, org.thoughtcrime.securesms.components.menu.SignalContextMenu.HorizontalPosition r18, org.thoughtcrime.securesms.components.menu.SignalContextMenu.VerticalPosition r19, java.lang.Runnable r20, int r21, kotlin.jvm.internal.DefaultConstructorMarker r22) {
        /*
            r12 = this;
            r0 = r21
            r1 = r0 & 8
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r7 = 0
            goto L_0x000b
        L_0x0009:
            r7 = r16
        L_0x000b:
            r1 = r0 & 16
            if (r1 == 0) goto L_0x0011
            r8 = 0
            goto L_0x0013
        L_0x0011:
            r8 = r17
        L_0x0013:
            r1 = r0 & 32
            if (r1 == 0) goto L_0x001b
            org.thoughtcrime.securesms.components.menu.SignalContextMenu$HorizontalPosition r1 = org.thoughtcrime.securesms.components.menu.SignalContextMenu.HorizontalPosition.START
            r9 = r1
            goto L_0x001d
        L_0x001b:
            r9 = r18
        L_0x001d:
            r1 = r0 & 64
            if (r1 == 0) goto L_0x0025
            org.thoughtcrime.securesms.components.menu.SignalContextMenu$VerticalPosition r1 = org.thoughtcrime.securesms.components.menu.SignalContextMenu.VerticalPosition.BELOW
            r10 = r1
            goto L_0x0027
        L_0x0025:
            r10 = r19
        L_0x0027:
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x002e
            r0 = 0
            r11 = r0
            goto L_0x0030
        L_0x002e:
            r11 = r20
        L_0x0030:
            r3 = r12
            r4 = r13
            r5 = r14
            r6 = r15
            r3.<init>(r4, r5, r6, r7, r8, r9, r10, r11)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.menu.SignalContextMenu.<init>(android.view.View, android.view.ViewGroup, java.util.List, int, int, org.thoughtcrime.securesms.components.menu.SignalContextMenu$HorizontalPosition, org.thoughtcrime.securesms.components.menu.SignalContextMenu$VerticalPosition, java.lang.Runnable, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final HorizontalPosition getHorizontalPosition() {
        return this.horizontalPosition;
    }

    public final VerticalPosition getVerticalPosition() {
        return this.verticalPosition;
    }

    public final Runnable getOnDismiss() {
        return this.onDismiss;
    }

    private SignalContextMenu(View view, ViewGroup viewGroup, List<ActionItem> list, int i, int i2, HorizontalPosition horizontalPosition, VerticalPosition verticalPosition, Runnable runnable) {
        super(LayoutInflater.from(view.getContext()).inflate(R.layout.signal_context_menu, (ViewGroup) null), -2, -2);
        this.anchor = view;
        this.container = viewGroup;
        this.items = list;
        this.baseOffsetX = i;
        this.baseOffsetY = i2;
        this.horizontalPosition = horizontalPosition;
        this.verticalPosition = verticalPosition;
        this.onDismiss = runnable;
        Context context = view.getContext();
        Intrinsics.checkNotNullExpressionValue(context, "anchor.context");
        this.context = context;
        View findViewById = getContentView().findViewById(R.id.signal_context_menu_list);
        Intrinsics.checkNotNullExpressionValue(findViewById, "contentView.findViewById…signal_context_menu_list)");
        ContextMenuList contextMenuList = new ContextMenuList((RecyclerView) findViewById, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.menu.SignalContextMenu$contextMenuList$1
            final /* synthetic */ SignalContextMenu this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                this.this$0.dismiss();
            }
        });
        this.contextMenuList = contextMenuList;
        setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.signal_context_menu_background));
        setInputMethodMode(2);
        setFocusable(true);
        if (runnable != null) {
            setOnDismissListener(new PopupWindow.OnDismissListener() { // from class: org.thoughtcrime.securesms.components.menu.SignalContextMenu$$ExternalSyntheticLambda1
                @Override // android.widget.PopupWindow.OnDismissListener
                public final void onDismiss() {
                    SignalContextMenu.m515_init_$lambda0(SignalContextMenu.this);
                }
            });
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation(20.0f);
        }
        contextMenuList.setItems(list);
    }

    public final Context getContext() {
        return this.context;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m515_init_$lambda0(SignalContextMenu signalContextMenu) {
        Intrinsics.checkNotNullParameter(signalContextMenu, "this$0");
        signalContextMenu.onDismiss.run();
    }

    public final SignalContextMenu show() {
        int i;
        int i2;
        if (this.anchor.getWidth() == 0 || this.anchor.getHeight() == 0) {
            this.anchor.post(new Runnable() { // from class: org.thoughtcrime.securesms.components.menu.SignalContextMenu$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    SignalContextMenu.this.show();
                }
            });
            return this;
        }
        getContentView().measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        Rect rect = new Rect(this.anchor.getLeft(), this.anchor.getTop(), this.anchor.getRight(), this.anchor.getBottom());
        if (!Intrinsics.areEqual(this.anchor.getParent(), this.container)) {
            this.container.offsetDescendantRectToMyCoords(this.anchor, rect);
        }
        int measuredHeight = rect.bottom + getContentView().getMeasuredHeight() + this.baseOffsetY;
        int measuredHeight2 = (rect.top - getContentView().getMeasuredHeight()) - this.baseOffsetY;
        int height = this.container.getHeight();
        float y = this.container.getY();
        if (this.verticalPosition == VerticalPosition.ABOVE && ((float) measuredHeight2) > y) {
            i = -(rect.height() + getContentView().getMeasuredHeight() + this.baseOffsetY);
            this.contextMenuList.setItems(CollectionsKt___CollectionsKt.reversed(this.items));
        } else if (measuredHeight < height) {
            i = this.baseOffsetY;
        } else if (((float) measuredHeight2) > y) {
            i = -(rect.height() + getContentView().getMeasuredHeight() + this.baseOffsetY);
            this.contextMenuList.setItems(CollectionsKt___CollectionsKt.reversed(this.items));
        } else {
            i = -((rect.height() / 2) + (getContentView().getMeasuredHeight() / 2) + this.baseOffsetY);
        }
        int i3 = WhenMappings.$EnumSwitchMapping$0[this.horizontalPosition.ordinal()];
        if (i3 != 1) {
            if (i3 != 2) {
                throw new NoWhenBranchMatchedException();
            } else if (ViewUtil.isLtr(this.context)) {
                i2 = -((this.baseOffsetX + getContentView().getMeasuredWidth()) - rect.width());
            } else {
                i2 = this.baseOffsetX - rect.width();
            }
        } else if (ViewUtil.isLtr(this.context)) {
            i2 = this.baseOffsetX;
        } else {
            i2 = -(this.baseOffsetX + getContentView().getMeasuredWidth());
        }
        showAsDropDown(this.anchor, i2, i);
        return this;
    }

    /* compiled from: SignalContextMenu.kt */
    @Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0011\u001a\u00020\u00002\u0006\u0010&\u001a\u00020\u0012J\u000e\u0010\u0017\u001a\u00020\u00002\u0006\u0010&\u001a\u00020\u0012J\u000e\u0010\u001a\u001a\u00020\u00002\u0006\u0010\u001a\u001a\u00020\u001bJ\u000e\u0010'\u001a\u00020\u00002\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010(\u001a\u00020\u00002\u0006\u0010 \u001a\u00020!J\u0014\u0010)\u001a\u00020*2\f\u0010+\u001a\b\u0012\u0004\u0012\u00020-0,R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\u0011\u001a\u00020\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001a\u0010\u0017\u001a\u00020\u0012X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0014\"\u0004\b\u0019\u0010\u0016R\u001c\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u001c\u0010\u001d\"\u0004\b\u001e\u0010\u001fR\u001a\u0010 \u001a\u00020!X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%¨\u0006."}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$Builder;", "", "anchor", "Landroid/view/View;", "container", "Landroid/view/ViewGroup;", "(Landroid/view/View;Landroid/view/ViewGroup;)V", "getAnchor", "()Landroid/view/View;", "getContainer", "()Landroid/view/ViewGroup;", "horizontalPosition", "Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;", "getHorizontalPosition", "()Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;", "setHorizontalPosition", "(Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$HorizontalPosition;)V", "offsetX", "", "getOffsetX", "()I", "setOffsetX", "(I)V", "offsetY", "getOffsetY", "setOffsetY", "onDismiss", "Ljava/lang/Runnable;", "getOnDismiss", "()Ljava/lang/Runnable;", "setOnDismiss", "(Ljava/lang/Runnable;)V", "verticalPosition", "Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;", "getVerticalPosition", "()Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;", "setVerticalPosition", "(Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu$VerticalPosition;)V", "offsetPx", "preferredHorizontalPosition", "preferredVerticalPosition", "show", "Lorg/thoughtcrime/securesms/components/menu/SignalContextMenu;", "items", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Builder {
        private final View anchor;
        private final ViewGroup container;
        private HorizontalPosition horizontalPosition = HorizontalPosition.START;
        private int offsetX;
        private int offsetY;
        private Runnable onDismiss;
        private VerticalPosition verticalPosition = VerticalPosition.BELOW;

        public Builder(View view, ViewGroup viewGroup) {
            Intrinsics.checkNotNullParameter(view, "anchor");
            Intrinsics.checkNotNullParameter(viewGroup, "container");
            this.anchor = view;
            this.container = viewGroup;
        }

        public final View getAnchor() {
            return this.anchor;
        }

        public final ViewGroup getContainer() {
            return this.container;
        }

        public final Runnable getOnDismiss() {
            return this.onDismiss;
        }

        public final void setOnDismiss(Runnable runnable) {
            this.onDismiss = runnable;
        }

        public final int getOffsetX() {
            return this.offsetX;
        }

        public final void setOffsetX(int i) {
            this.offsetX = i;
        }

        public final int getOffsetY() {
            return this.offsetY;
        }

        public final void setOffsetY(int i) {
            this.offsetY = i;
        }

        public final HorizontalPosition getHorizontalPosition() {
            return this.horizontalPosition;
        }

        public final void setHorizontalPosition(HorizontalPosition horizontalPosition) {
            Intrinsics.checkNotNullParameter(horizontalPosition, "<set-?>");
            this.horizontalPosition = horizontalPosition;
        }

        public final VerticalPosition getVerticalPosition() {
            return this.verticalPosition;
        }

        public final void setVerticalPosition(VerticalPosition verticalPosition) {
            Intrinsics.checkNotNullParameter(verticalPosition, "<set-?>");
            this.verticalPosition = verticalPosition;
        }

        public final Builder onDismiss(Runnable runnable) {
            Intrinsics.checkNotNullParameter(runnable, "onDismiss");
            this.onDismiss = runnable;
            return this;
        }

        public final Builder offsetX(int i) {
            this.offsetX = i;
            return this;
        }

        public final Builder offsetY(int i) {
            this.offsetY = i;
            return this;
        }

        public final Builder preferredHorizontalPosition(HorizontalPosition horizontalPosition) {
            Intrinsics.checkNotNullParameter(horizontalPosition, "horizontalPosition");
            this.horizontalPosition = horizontalPosition;
            return this;
        }

        public final Builder preferredVerticalPosition(VerticalPosition verticalPosition) {
            Intrinsics.checkNotNullParameter(verticalPosition, "verticalPosition");
            this.verticalPosition = verticalPosition;
            return this;
        }

        public final SignalContextMenu show(List<ActionItem> list) {
            Intrinsics.checkNotNullParameter(list, "items");
            return new SignalContextMenu(this.anchor, this.container, list, this.offsetX, this.offsetY, this.horizontalPosition, this.verticalPosition, this.onDismiss, null).show();
        }
    }
}
