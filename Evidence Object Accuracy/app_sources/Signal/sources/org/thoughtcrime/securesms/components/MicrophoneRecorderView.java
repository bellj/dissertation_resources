package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.permissions.Permissions;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class MicrophoneRecorderView extends FrameLayout implements View.OnTouchListener {
    public static final int ANIMATION_DURATION;
    private FloatingRecordButton floatingRecordButton;
    private Listener listener;
    private LockDropTarget lockDropTarget;
    private State state = State.NOT_RUNNING;

    /* loaded from: classes4.dex */
    public interface Listener {
        void onRecordCanceled();

        void onRecordLocked();

        void onRecordMoved(float f, float f2);

        void onRecordPermissionRequired();

        void onRecordPressed();

        void onRecordReleased();
    }

    /* loaded from: classes4.dex */
    public enum State {
        NOT_RUNNING,
        RUNNING_HELD,
        RUNNING_LOCKED
    }

    public MicrophoneRecorderView(Context context) {
        super(context);
    }

    public MicrophoneRecorderView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        this.floatingRecordButton = new FloatingRecordButton(getContext(), (ImageView) findViewById(R.id.quick_audio_fab));
        this.lockDropTarget = new LockDropTarget(getContext(), findViewById(R.id.lock_drop_target));
        findViewById(R.id.quick_audio_toggle).setOnTouchListener(this);
    }

    public void cancelAction() {
        State state = this.state;
        State state2 = State.NOT_RUNNING;
        if (state != state2) {
            this.state = state2;
            hideUi();
            Listener listener = this.listener;
            if (listener != null) {
                listener.onRecordCanceled();
            }
        }
    }

    public boolean isRecordingLocked() {
        return this.state == State.RUNNING_LOCKED;
    }

    private void lockAction() {
        if (this.state == State.RUNNING_HELD) {
            this.state = State.RUNNING_LOCKED;
            hideUi();
            Listener listener = this.listener;
            if (listener != null) {
                listener.onRecordLocked();
            }
        }
    }

    public void unlockAction() {
        if (this.state == State.RUNNING_LOCKED) {
            this.state = State.NOT_RUNNING;
            hideUi();
            Listener listener = this.listener;
            if (listener != null) {
                listener.onRecordReleased();
            }
        }
    }

    private void hideUi() {
        this.floatingRecordButton.hide();
        this.lockDropTarget.hide();
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action != 2) {
                    if (action != 3) {
                        return false;
                    }
                } else if (this.state != State.RUNNING_HELD) {
                    return false;
                } else {
                    this.floatingRecordButton.moveTo(motionEvent.getX(), motionEvent.getY());
                    Listener listener = this.listener;
                    if (listener != null) {
                        listener.onRecordMoved(this.floatingRecordButton.lastOffsetX, motionEvent.getRawX());
                    }
                    if (this.floatingRecordButton.lastOffsetY > ((float) getResources().getDimensionPixelSize(R.dimen.recording_voice_lock_target))) {
                        return false;
                    }
                    lockAction();
                    return false;
                }
            }
            if (this.state != State.RUNNING_HELD) {
                return false;
            }
            this.state = State.NOT_RUNNING;
            hideUi();
            Listener listener2 = this.listener;
            if (listener2 == null) {
                return false;
            }
            listener2.onRecordReleased();
            return false;
        } else if (!Permissions.hasAll(getContext(), "android.permission.RECORD_AUDIO")) {
            Listener listener3 = this.listener;
            if (listener3 == null) {
                return false;
            }
            listener3.onRecordPermissionRequired();
            return false;
        } else if (this.state != State.NOT_RUNNING) {
            return false;
        } else {
            this.state = State.RUNNING_HELD;
            this.floatingRecordButton.display(motionEvent.getX(), motionEvent.getY());
            this.lockDropTarget.display();
            Listener listener4 = this.listener;
            if (listener4 == null) {
                return false;
            }
            listener4.onRecordPressed();
            return false;
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    /* loaded from: classes4.dex */
    public static class FloatingRecordButton {
        private float lastOffsetX;
        private float lastOffsetY;
        private final ImageView recordButtonFab;
        private float startPositionX;
        private float startPositionY;

        FloatingRecordButton(Context context, ImageView imageView) {
            this.recordButtonFab = imageView;
            imageView.getBackground().setColorFilter(context.getResources().getColor(R.color.red_500), PorterDuff.Mode.SRC_IN);
        }

        void display(float f, float f2) {
            this.startPositionX = f;
            this.startPositionY = f2;
            this.recordButtonFab.setVisibility(0);
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(new TranslateAnimation(0, 0.0f, 0, 0.0f, 0, 0.0f, 0, 0.0f));
            animationSet.addAnimation(new ScaleAnimation(0.5f, 1.0f, 0.5f, 1.0f, 1, 0.5f, 1, 0.5f));
            animationSet.setDuration(200);
            animationSet.setInterpolator(new OvershootInterpolator());
            this.recordButtonFab.startAnimation(animationSet);
        }

        void moveTo(float f, float f2) {
            this.lastOffsetX = getXOffset(f);
            this.lastOffsetY = getYOffset(f2);
            if (Math.abs(this.lastOffsetX) > Math.abs(this.lastOffsetY)) {
                this.lastOffsetY = 0.0f;
            } else {
                this.lastOffsetX = 0.0f;
            }
            this.recordButtonFab.setTranslationX(this.lastOffsetX);
            this.recordButtonFab.setTranslationY(this.lastOffsetY);
        }

        void hide() {
            this.recordButtonFab.setTranslationX(0.0f);
            this.recordButtonFab.setTranslationY(0.0f);
            if (this.recordButtonFab.getVisibility() == 0) {
                AnimationSet animationSet = new AnimationSet(false);
                ScaleAnimation scaleAnimation = new ScaleAnimation(1.0f, 0.5f, 1.0f, 0.5f, 1, 0.5f, 1, 0.5f);
                TranslateAnimation translateAnimation = new TranslateAnimation(0, this.lastOffsetX, 0, 0.0f, 0, this.lastOffsetY, 0, 0.0f);
                scaleAnimation.setInterpolator(new AnticipateOvershootInterpolator(1.5f));
                translateAnimation.setInterpolator(new DecelerateInterpolator());
                animationSet.addAnimation(scaleAnimation);
                animationSet.addAnimation(translateAnimation);
                animationSet.setDuration(200);
                animationSet.setInterpolator(new AnticipateOvershootInterpolator(1.5f));
                this.recordButtonFab.setVisibility(8);
                this.recordButtonFab.clearAnimation();
                this.recordButtonFab.startAnimation(animationSet);
            }
        }

        private float getXOffset(float f) {
            if (ViewUtil.isLtr(this.recordButtonFab)) {
                return -Math.max(0.0f, this.startPositionX - f);
            }
            return Math.max(0.0f, f - this.startPositionX);
        }

        private float getYOffset(float f) {
            return Math.min(0.0f, f - this.startPositionY);
        }
    }

    /* loaded from: classes4.dex */
    public static class LockDropTarget {
        private final int dropTargetPosition;
        private final View lockDropTarget;

        LockDropTarget(Context context, View view) {
            this.lockDropTarget = view;
            this.dropTargetPosition = context.getResources().getDimensionPixelSize(R.dimen.recording_voice_lock_target);
        }

        void display() {
            this.lockDropTarget.setScaleX(1.0f);
            this.lockDropTarget.setScaleY(1.0f);
            this.lockDropTarget.setAlpha(0.0f);
            this.lockDropTarget.setTranslationY(0.0f);
            this.lockDropTarget.setVisibility(0);
            this.lockDropTarget.animate().setStartDelay(400).setDuration(200).setInterpolator(new DecelerateInterpolator()).translationY((float) this.dropTargetPosition).alpha(1.0f).start();
        }

        void hide() {
            this.lockDropTarget.animate().setStartDelay(0).setDuration(200).setInterpolator(new LinearInterpolator()).scaleX(0.0f).scaleY(0.0f).start();
        }
    }
}
