package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.content.ClipData;
import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blocked.BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: BioTextPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001:\u0006\u0007\b\t\n\u000b\fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "BioTextPreferenceModel", "BioTextViewHolder", "GroupModel", "GroupViewHolder", "RecipientModel", "RecipientViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BioTextPreference {
    public static final BioTextPreference INSTANCE = new BioTextPreference();

    private BioTextPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(RecipientModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new BioTextPreference.RecipientViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_bio_preference_item));
        mappingAdapter.registerFactory(GroupModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new BioTextPreference.GroupViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_bio_preference_item));
    }

    /* compiled from: BioTextPreference.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b&\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&J\u0012\u0010\b\u001a\u0004\u0018\u00010\t2\u0006\u0010\u0006\u001a\u00020\u0007H&J\n\u0010\n\u001a\u0004\u0018\u00010\tH&¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextPreferenceModel;", "T", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "()V", "getHeadlineText", "", "context", "Landroid/content/Context;", "getSubhead1Text", "", "getSubhead2Text", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class BioTextPreferenceModel<T extends BioTextPreferenceModel<T>> extends PreferenceModel<T> {
        public abstract CharSequence getHeadlineText(Context context);

        public abstract String getSubhead1Text(Context context);

        public abstract String getSubhead2Text();

        public BioTextPreferenceModel() {
            super(null, null, null, null, false, 31, null);
        }
    }

    /* compiled from: BioTextPreference.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0000H\u0016J\u0010\u0010\b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0000H\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0012\u0010\r\u001a\u0004\u0018\u00010\u000e2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\n\u0010\u000f\u001a\u0004\u0018\u00010\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$RecipientModel;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextPreferenceModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;)V", "areContentsTheSame", "", "newItem", "areItemsTheSame", "getHeadlineText", "", "context", "Landroid/content/Context;", "getSubhead1Text", "", "getSubhead2Text", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientModel extends BioTextPreferenceModel<RecipientModel> {
        private final Recipient recipient;

        public RecipientModel(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.recipient = recipient;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference.BioTextPreferenceModel
        public CharSequence getHeadlineText(Context context) {
            String str;
            Intrinsics.checkNotNullParameter(context, "context");
            if (this.recipient.isSelf()) {
                str = context.getString(R.string.note_to_self);
            } else {
                str = this.recipient.getDisplayNameOrUsername(context);
            }
            Intrinsics.checkNotNullExpressionValue(str, "if (recipient.isSelf) {\n…Username(context)\n      }");
            if (!this.recipient.showVerified()) {
                return str;
            }
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(str);
            SpanUtil.appendCenteredImageSpan(spannableStringBuilder, ContextUtil.requireDrawable(context, R.drawable.ic_official_28), 28, 28);
            return spannableStringBuilder;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference.BioTextPreferenceModel
        public String getSubhead1Text(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            if (this.recipient.isReleaseNotes()) {
                return context.getString(R.string.ReleaseNotes__signal_release_notes_and_news);
            }
            return this.recipient.getCombinedAboutAndEmoji();
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference.BioTextPreferenceModel
        public String getSubhead2Text() {
            return (String) this.recipient.getE164().map(new BlockedUsersAdapter$ViewHolder$$ExternalSyntheticLambda1()).orElse(null);
        }

        public boolean areContentsTheSame(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return super.areContentsTheSame(recipientModel) && recipientModel.recipient.hasSameContent(this.recipient);
        }

        public boolean areItemsTheSame(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "newItem");
            return Intrinsics.areEqual(recipientModel.recipient.getId(), this.recipient.getId());
        }
    }

    /* compiled from: BioTextPreference.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\u0002\u0010\u0005J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0010\u0010\f\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0000H\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\n\u0010\u0012\u001a\u0004\u0018\u00010\u0003H\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0007¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$GroupModel;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextPreferenceModel;", "groupTitle", "", "groupMembershipDescription", "(Ljava/lang/String;Ljava/lang/String;)V", "getGroupMembershipDescription", "()Ljava/lang/String;", "getGroupTitle", "areContentsTheSame", "", "newItem", "areItemsTheSame", "getHeadlineText", "", "context", "Landroid/content/Context;", "getSubhead1Text", "getSubhead2Text", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupModel extends BioTextPreferenceModel<GroupModel> {
        private final String groupMembershipDescription;
        private final String groupTitle;

        public boolean areItemsTheSame(GroupModel groupModel) {
            Intrinsics.checkNotNullParameter(groupModel, "newItem");
            return true;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference.BioTextPreferenceModel
        public String getSubhead2Text() {
            return null;
        }

        public final String getGroupTitle() {
            return this.groupTitle;
        }

        public final String getGroupMembershipDescription() {
            return this.groupMembershipDescription;
        }

        public GroupModel(String str, String str2) {
            Intrinsics.checkNotNullParameter(str, "groupTitle");
            this.groupTitle = str;
            this.groupMembershipDescription = str2;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference.BioTextPreferenceModel
        public CharSequence getHeadlineText(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return this.groupTitle;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference.BioTextPreferenceModel
        public String getSubhead1Text(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return this.groupMembershipDescription;
        }

        public boolean areContentsTheSame(GroupModel groupModel) {
            Intrinsics.checkNotNullParameter(groupModel, "newItem");
            return super.areContentsTheSame(groupModel) && Intrinsics.areEqual(this.groupTitle, groupModel.groupTitle) && Intrinsics.areEqual(this.groupMembershipDescription, groupModel.groupMembershipDescription);
        }
    }

    /* compiled from: BioTextPreference.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\b\"\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00022\b\u0012\u0004\u0012\u0002H\u00010\u0003B\r\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0010R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextViewHolder;", "T", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextPreferenceModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "headline", "Landroid/widget/TextView;", "subhead1", "subhead2", "getSubhead2", "()Landroid/widget/TextView;", "bind", "", "model", "(Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextPreferenceModel;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class BioTextViewHolder<T extends BioTextPreferenceModel<T>> extends MappingViewHolder<T> {
        private final TextView headline;
        private final TextView subhead1;
        private final TextView subhead2;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BioTextViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.bio_preference_headline);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.….bio_preference_headline)");
            this.headline = (TextView) findViewById;
            View findViewById2 = view.findViewById(R.id.bio_preference_subhead_1);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…bio_preference_subhead_1)");
            this.subhead1 = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.bio_preference_subhead_2);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.…bio_preference_subhead_2)");
            this.subhead2 = (TextView) findViewById3;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference$BioTextViewHolder<T extends org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference$BioTextPreferenceModel<T>> */
        /* JADX WARN: Multi-variable type inference failed */
        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public /* bridge */ /* synthetic */ void bind(Object obj) {
            bind((BioTextViewHolder<T>) ((BioTextPreferenceModel) obj));
        }

        protected final TextView getSubhead2() {
            return this.subhead2;
        }

        public void bind(T t) {
            Intrinsics.checkNotNullParameter(t, "model");
            TextView textView = this.headline;
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setText(t.getHeadlineText(context));
            Context context2 = this.context;
            Intrinsics.checkNotNullExpressionValue(context2, "context");
            String subhead1Text = t.getSubhead1Text(context2);
            this.subhead1.setText(subhead1Text);
            int i = 8;
            this.subhead1.setVisibility(subhead1Text == null ? 8 : 0);
            String subhead2Text = t.getSubhead2Text();
            this.subhead2.setText(subhead2Text);
            TextView textView2 = this.subhead2;
            if (subhead2Text != null) {
                i = 0;
            }
            textView2.setVisibility(i);
        }
    }

    /* compiled from: BioTextPreference.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$RecipientViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$RecipientModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientViewHolder extends BioTextViewHolder<RecipientModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RecipientViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public void bind(RecipientModel recipientModel) {
            Intrinsics.checkNotNullParameter(recipientModel, "model");
            super.bind((RecipientViewHolder) recipientModel);
            String subhead2Text = recipientModel.getSubhead2Text();
            if (!(subhead2Text == null || subhead2Text.length() == 0)) {
                getSubhead2().setOnLongClickListener(new BioTextPreference$RecipientViewHolder$$ExternalSyntheticLambda0(this));
            } else {
                getSubhead2().setOnLongClickListener(null);
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final boolean m1195bind$lambda0(RecipientViewHolder recipientViewHolder, View view) {
            Intrinsics.checkNotNullParameter(recipientViewHolder, "this$0");
            ServiceUtil.getClipboardManager(recipientViewHolder.context).setPrimaryClip(ClipData.newPlainText(recipientViewHolder.context.getString(R.string.ConversationSettingsFragment__phone_number), recipientViewHolder.getSubhead2().getText().toString()));
            Toast.makeText(recipientViewHolder.context, (int) R.string.ConversationSettingsFragment__copied_phone_number_to_clipboard, 0).show();
            return true;
        }
    }

    /* compiled from: BioTextPreference.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$GroupViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$BioTextViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/BioTextPreference$GroupModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupViewHolder extends BioTextViewHolder<GroupModel> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }
    }
}
