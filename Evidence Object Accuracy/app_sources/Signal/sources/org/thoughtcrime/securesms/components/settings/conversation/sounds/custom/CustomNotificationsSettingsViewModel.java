package org.thoughtcrime.securesms.components.settings.conversation.sounds.custom;

import android.net.Uri;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: CustomNotificationsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001\u001cB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000e\u001a\u00020\u000fJ\u0010\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u000e\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J\u000e\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u0018J\u0010\u0010\u0019\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u000e\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\u001b\u001a\u00020\u0015R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsRepository;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "channelConsistencyCheck", "", "setCallSound", "uri", "Landroid/net/Uri;", "setCallVibrate", "callVibrateState", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "setHasCustomNotifications", "hasCustomNotifications", "", "setMessageSound", "setMessageVibrate", "messageVibrateState", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomNotificationsSettingsViewModel extends ViewModel {
    private final RecipientId recipientId;
    private final CustomNotificationsSettingsRepository repository;
    private final LiveData<CustomNotificationsSettingsState> state;
    private final Store<CustomNotificationsSettingsState> store;

    /* compiled from: CustomNotificationsSettingsViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[RecipientDatabase.VibrateState.values().length];
            iArr[RecipientDatabase.VibrateState.DEFAULT.ordinal()] = 1;
            iArr[RecipientDatabase.VibrateState.ENABLED.ordinal()] = 2;
            iArr[RecipientDatabase.VibrateState.DISABLED.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX DEBUG: Type inference failed for r1v4. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public CustomNotificationsSettingsViewModel(RecipientId recipientId, CustomNotificationsSettingsRepository customNotificationsSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(customNotificationsSettingsRepository, "repository");
        this.recipientId = recipientId;
        this.repository = customNotificationsSettingsRepository;
        Store<CustomNotificationsSettingsState> store = new Store<>(new CustomNotificationsSettingsState(false, null, false, false, null, false, null, null, null, false, 1023, null));
        this.store = store;
        LiveData<CustomNotificationsSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        store.update(Recipient.live(recipientId).getLiveData(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return CustomNotificationsSettingsViewModel.m1243_init_$lambda0((Recipient) obj, (CustomNotificationsSettingsState) obj2);
            }
        });
    }

    public final LiveData<CustomNotificationsSettingsState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final CustomNotificationsSettingsState m1243_init_$lambda0(Recipient recipient, CustomNotificationsSettingsState customNotificationsSettingsState) {
        boolean z;
        boolean z2 = NotificationChannels.supported() && recipient.getNotificationChannel() != null;
        boolean z3 = (!NotificationChannels.supported() || z2) && customNotificationsSettingsState.isInitialLoadComplete();
        Uri messageRingtone = recipient.getMessageRingtone();
        RecipientDatabase.VibrateState messageVibrate = recipient.getMessageVibrate();
        int i = WhenMappings.$EnumSwitchMapping$0[recipient.getMessageVibrate().ordinal()];
        if (i == 1) {
            z = SignalStore.settings().isMessageVibrateEnabled();
        } else if (i == 2) {
            z = true;
        } else if (i == 3) {
            z = false;
        } else {
            throw new NoWhenBranchMatchedException();
        }
        boolean z4 = recipient.isRegistered() && (!recipient.isGroup() || FeatureFlags.groupCallRinging());
        Uri callRingtone = recipient.getCallRingtone();
        RecipientDatabase.VibrateState callVibrate = recipient.getCallVibrate();
        Intrinsics.checkNotNullExpressionValue(customNotificationsSettingsState, "state");
        Intrinsics.checkNotNullExpressionValue(messageVibrate, "messageVibrate");
        Intrinsics.checkNotNullExpressionValue(callVibrate, "callVibrate");
        return customNotificationsSettingsState.copy((r22 & 1) != 0 ? customNotificationsSettingsState.isInitialLoadComplete : false, (r22 & 2) != 0 ? customNotificationsSettingsState.recipient : recipient, (r22 & 4) != 0 ? customNotificationsSettingsState.hasCustomNotifications : z2, (r22 & 8) != 0 ? customNotificationsSettingsState.controlsEnabled : z3, (r22 & 16) != 0 ? customNotificationsSettingsState.messageVibrateState : messageVibrate, (r22 & 32) != 0 ? customNotificationsSettingsState.messageVibrateEnabled : z, (r22 & 64) != 0 ? customNotificationsSettingsState.messageSound : messageRingtone, (r22 & 128) != 0 ? customNotificationsSettingsState.callVibrateState : callVibrate, (r22 & 256) != 0 ? customNotificationsSettingsState.callSound : callRingtone, (r22 & 512) != 0 ? customNotificationsSettingsState.showCallingOptions : z4);
    }

    public final void setHasCustomNotifications(boolean z) {
        this.repository.setHasCustomNotifications(this.recipientId, z);
    }

    public final void setMessageVibrate(RecipientDatabase.VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(vibrateState, "messageVibrateState");
        this.repository.setMessageVibrate(this.recipientId, vibrateState);
    }

    public final void setMessageSound(Uri uri) {
        this.repository.setMessageSound(this.recipientId, uri);
    }

    public final void setCallVibrate(RecipientDatabase.VibrateState vibrateState) {
        Intrinsics.checkNotNullParameter(vibrateState, "callVibrateState");
        this.repository.setCallingVibrate(this.recipientId, vibrateState);
    }

    public final void setCallSound(Uri uri) {
        this.repository.setCallSound(this.recipientId, uri);
    }

    /* renamed from: channelConsistencyCheck$lambda-1 */
    public static final CustomNotificationsSettingsState m1244channelConsistencyCheck$lambda1(CustomNotificationsSettingsState customNotificationsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(customNotificationsSettingsState, "it");
        return customNotificationsSettingsState.copy((r22 & 1) != 0 ? customNotificationsSettingsState.isInitialLoadComplete : false, (r22 & 2) != 0 ? customNotificationsSettingsState.recipient : null, (r22 & 4) != 0 ? customNotificationsSettingsState.hasCustomNotifications : false, (r22 & 8) != 0 ? customNotificationsSettingsState.controlsEnabled : false, (r22 & 16) != 0 ? customNotificationsSettingsState.messageVibrateState : null, (r22 & 32) != 0 ? customNotificationsSettingsState.messageVibrateEnabled : false, (r22 & 64) != 0 ? customNotificationsSettingsState.messageSound : null, (r22 & 128) != 0 ? customNotificationsSettingsState.callVibrateState : null, (r22 & 256) != 0 ? customNotificationsSettingsState.callSound : null, (r22 & 512) != 0 ? customNotificationsSettingsState.showCallingOptions : false);
    }

    public final void channelConsistencyCheck() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CustomNotificationsSettingsViewModel.m1244channelConsistencyCheck$lambda1((CustomNotificationsSettingsState) obj);
            }
        });
        this.repository.ensureCustomChannelConsistency(this.recipientId, new CustomNotificationsSettingsViewModel$channelConsistencyCheck$2(this));
    }

    /* compiled from: CustomNotificationsSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;
        private final CustomNotificationsSettingsRepository repository;

        public Factory(RecipientId recipientId, CustomNotificationsSettingsRepository customNotificationsSettingsRepository) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(customNotificationsSettingsRepository, "repository");
            this.recipientId = recipientId;
            this.repository = customNotificationsSettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new CustomNotificationsSettingsViewModel(this.recipientId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
