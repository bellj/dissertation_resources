package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import androidx.core.content.ContextCompat;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.SimpleEmojiTextView;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class FromTextView extends SimpleEmojiTextView {
    private static final String TAG = Log.tag(FromTextView.class);

    public FromTextView(Context context) {
        super(context);
    }

    public FromTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setText(Recipient recipient) {
        setText(recipient, true);
    }

    public void setText(Recipient recipient, boolean z) {
        setText(recipient, z, (String) null);
    }

    public void setText(Recipient recipient, boolean z, String str) {
        setText(recipient, recipient.getDisplayNameOrUsername(getContext()), z, str);
    }

    public void setText(Recipient recipient, CharSequence charSequence, boolean z, String str) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        if (recipient.isSelf()) {
            spannableStringBuilder.append((CharSequence) getContext().getString(R.string.note_to_self));
        } else {
            spannableStringBuilder.append(charSequence);
        }
        if (str != null) {
            spannableStringBuilder.append((CharSequence) str);
        }
        if (recipient.showVerified()) {
            Drawable requireDrawable = ContextUtil.requireDrawable(getContext(), R.drawable.ic_official_20);
            requireDrawable.setBounds(0, 0, ViewUtil.dpToPx(20), ViewUtil.dpToPx(20));
            spannableStringBuilder.append((CharSequence) " ").append(SpanUtil.buildCenteredImageSpan(requireDrawable));
        }
        setText(spannableStringBuilder);
        if (recipient.isBlocked()) {
            setCompoundDrawablesRelativeWithIntrinsicBounds(R.drawable.ic_block_grey600_18dp, 0, 0, 0);
        } else if (recipient.isMuted()) {
            setCompoundDrawablesRelativeWithIntrinsicBounds(getMuted(), (Drawable) null, (Drawable) null, (Drawable) null);
        } else {
            setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    private Drawable getMuted() {
        Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.ic_bell_disabled_16);
        Objects.requireNonNull(drawable);
        drawable.setBounds(0, 0, ViewUtil.dpToPx(18), ViewUtil.dpToPx(18));
        drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(getContext(), R.color.signal_icon_tint_secondary), PorterDuff.Mode.SRC_IN));
        return drawable;
    }
}
