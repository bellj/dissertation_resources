package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.subscription.LevelUpdate;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: ManageDonationsViewModel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 \u00142\u00020\u0001:\u0002\u0014\u0015B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0010\u001a\u00020\u0011H\u0014J\u0006\u0010\u0012\u001a\u00020\u0011J\u0006\u0010\u0013\u001a\u00020\u0011R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsViewModel;", "Landroidx/lifecycle/ViewModel;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "networkDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "refresh", "retry", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ManageDonationsViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(ManageDonationsViewModel.class);
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final Disposable networkDisposable;
    private final LiveData<ManageDonationsState> state;
    private final Store<ManageDonationsState> store;
    private final SubscriptionsRepository subscriptionsRepository;

    /* JADX DEBUG: Type inference failed for r0v6. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public ManageDonationsViewModel(SubscriptionsRepository subscriptionsRepository) {
        Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
        this.subscriptionsRepository = subscriptionsRepository;
        Store<ManageDonationsState> store = new Store<>(new ManageDonationsState(null, null, null, null, 15, null));
        this.store = store;
        LiveData<ManageDonationsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        store.update(Recipient.self().live().getLiveDataResolved(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ManageDonationsViewModel.m969_init_$lambda0((Recipient) obj, (ManageDonationsState) obj2);
            }
        });
        Disposable subscribe = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ManageDonationsViewModel.m970_init_$lambda1(ManageDonationsViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "InternetConnectionObserv…retry()\n        }\n      }");
        this.networkDisposable = subscribe;
    }

    public final LiveData<ManageDonationsState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final ManageDonationsState m969_init_$lambda0(Recipient recipient, ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "state");
        return ManageDonationsState.copy$default(manageDonationsState, recipient.getFeaturedBadge(), null, null, null, 14, null);
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m970_init_$lambda1(ManageDonationsViewModel manageDonationsViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(manageDonationsViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
        if (bool.booleanValue()) {
            manageDonationsViewModel.retry();
        }
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void retry() {
        if (!this.disposables.isDisposed() && Intrinsics.areEqual(this.store.getState().getTransactionState(), ManageDonationsState.TransactionState.NetworkFailure.INSTANCE)) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ManageDonationsViewModel.m973retry$lambda2((ManageDonationsState) obj);
                }
            });
            refresh();
        }
    }

    /* renamed from: retry$lambda-2 */
    public static final ManageDonationsState m973retry$lambda2(ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "it");
        return ManageDonationsState.copy$default(manageDonationsState, null, ManageDonationsState.TransactionState.Init.INSTANCE, null, null, 13, null);
    }

    public final void refresh() {
        this.disposables.clear();
        Observable<Boolean> distinctUntilChanged = LevelUpdate.INSTANCE.isProcessing().distinctUntilChanged();
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "LevelUpdate.isProcessing.distinctUntilChanged()");
        Single<ActiveSubscription> activeSubscription = this.subscriptionsRepository.getActiveSubscription();
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy$default(SubscriptionRedemptionJobWatcher.INSTANCE.watch(), (Function1) null, (Function0) null, new ManageDonationsViewModel$refresh$1(this), 3, (Object) null));
        CompositeDisposable compositeDisposable = this.disposables;
        Observable<R> flatMapSingle = distinctUntilChanged.flatMapSingle(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel.m971refresh$lambda4(Single.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapSingle, "levelUpdateOperationEdge…ction(it) }\n      }\n    }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(flatMapSingle, new ManageDonationsViewModel$refresh$3(this), (Function0) null, new ManageDonationsViewModel$refresh$4(this), 2, (Object) null));
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy(this.subscriptionsRepository.getSubscriptions(), ManageDonationsViewModel$refresh$5.INSTANCE, new ManageDonationsViewModel$refresh$6(this)));
    }

    /* renamed from: refresh$lambda-4 */
    public static final SingleSource m971refresh$lambda4(Single single, Boolean bool) {
        Intrinsics.checkNotNullParameter(single, "$activeSubscription");
        Intrinsics.checkNotNullExpressionValue(bool, "isProcessing");
        if (bool.booleanValue()) {
            return Single.just(ManageDonationsState.TransactionState.InTransaction.INSTANCE);
        }
        return single.map(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel.m972refresh$lambda4$lambda3((ActiveSubscription) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-4$lambda-3 */
    public static final ManageDonationsState.TransactionState.NotInTransaction m972refresh$lambda4$lambda3(ActiveSubscription activeSubscription) {
        Intrinsics.checkNotNullExpressionValue(activeSubscription, "it");
        return new ManageDonationsState.TransactionState.NotInTransaction(activeSubscription);
    }

    /* compiled from: ManageDonationsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "subscriptionsRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final SubscriptionsRepository subscriptionsRepository;

        public Factory(SubscriptionsRepository subscriptionsRepository) {
            Intrinsics.checkNotNullParameter(subscriptionsRepository, "subscriptionsRepository");
            this.subscriptionsRepository = subscriptionsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ManageDonationsViewModel(this.subscriptionsRepository));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }

    /* compiled from: ManageDonationsViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
