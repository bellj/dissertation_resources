package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class EditNotificationProfileScheduleFragmentArgs {
    private final HashMap arguments;

    private EditNotificationProfileScheduleFragmentArgs() {
        this.arguments = new HashMap();
    }

    private EditNotificationProfileScheduleFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static EditNotificationProfileScheduleFragmentArgs fromBundle(Bundle bundle) {
        EditNotificationProfileScheduleFragmentArgs editNotificationProfileScheduleFragmentArgs = new EditNotificationProfileScheduleFragmentArgs();
        bundle.setClassLoader(EditNotificationProfileScheduleFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("profileId")) {
            editNotificationProfileScheduleFragmentArgs.arguments.put("profileId", Long.valueOf(bundle.getLong("profileId")));
            if (bundle.containsKey("createMode")) {
                editNotificationProfileScheduleFragmentArgs.arguments.put("createMode", Boolean.valueOf(bundle.getBoolean("createMode")));
                return editNotificationProfileScheduleFragmentArgs;
            }
            throw new IllegalArgumentException("Required argument \"createMode\" is missing and does not have an android:defaultValue");
        }
        throw new IllegalArgumentException("Required argument \"profileId\" is missing and does not have an android:defaultValue");
    }

    public long getProfileId() {
        return ((Long) this.arguments.get("profileId")).longValue();
    }

    public boolean getCreateMode() {
        return ((Boolean) this.arguments.get("createMode")).booleanValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("profileId")) {
            bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
        }
        if (this.arguments.containsKey("createMode")) {
            bundle.putBoolean("createMode", ((Boolean) this.arguments.get("createMode")).booleanValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        EditNotificationProfileScheduleFragmentArgs editNotificationProfileScheduleFragmentArgs = (EditNotificationProfileScheduleFragmentArgs) obj;
        return this.arguments.containsKey("profileId") == editNotificationProfileScheduleFragmentArgs.arguments.containsKey("profileId") && getProfileId() == editNotificationProfileScheduleFragmentArgs.getProfileId() && this.arguments.containsKey("createMode") == editNotificationProfileScheduleFragmentArgs.arguments.containsKey("createMode") && getCreateMode() == editNotificationProfileScheduleFragmentArgs.getCreateMode();
    }

    public int hashCode() {
        return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + (getCreateMode() ? 1 : 0);
    }

    public String toString() {
        return "EditNotificationProfileScheduleFragmentArgs{profileId=" + getProfileId() + ", createMode=" + getCreateMode() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(EditNotificationProfileScheduleFragmentArgs editNotificationProfileScheduleFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(editNotificationProfileScheduleFragmentArgs.arguments);
        }

        public Builder(long j, boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
            hashMap.put("createMode", Boolean.valueOf(z));
        }

        public EditNotificationProfileScheduleFragmentArgs build() {
            return new EditNotificationProfileScheduleFragmentArgs(this.arguments);
        }

        public Builder setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public Builder setCreateMode(boolean z) {
            this.arguments.put("createMode", Boolean.valueOf(z));
            return this;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean getCreateMode() {
            return ((Boolean) this.arguments.get("createMode")).booleanValue();
        }
    }
}
