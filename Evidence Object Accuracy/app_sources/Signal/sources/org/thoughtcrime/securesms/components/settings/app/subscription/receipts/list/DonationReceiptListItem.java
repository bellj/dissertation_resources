package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: DonationReceiptListItem.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00040\b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListItem;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "onClick", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListItem$Model;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListItem {
    public static final DonationReceiptListItem INSTANCE = new DonationReceiptListItem();

    private DonationReceiptListItem() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m1021register$lambda0(Function1 function1, View view) {
        Intrinsics.checkNotNullParameter(function1, "$onClick");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view, function1);
    }

    public final void register(MappingAdapter mappingAdapter, Function1<? super Model, Unit> function1) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        Intrinsics.checkNotNullParameter(function1, "onClick");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListItem$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return DonationReceiptListItem.m1021register$lambda0(Function1.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.donation_receipt_list_item));
    }

    /* compiled from: DonationReceiptListItem.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListItem$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "record", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getRecord", "()Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final Badge badge;
        private final DonationReceiptRecord record;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        public Model(DonationReceiptRecord donationReceiptRecord, Badge badge) {
            Intrinsics.checkNotNullParameter(donationReceiptRecord, "record");
            this.record = donationReceiptRecord;
            this.badge = badge;
        }

        public final DonationReceiptRecord getRecord() {
            return this.record;
        }

        public final Badge getBadge() {
            return this.badge;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.record, model.record) && Intrinsics.areEqual(this.badge, model.badge);
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.record.getId() == model.record.getId();
        }
    }

    /* compiled from: DonationReceiptListItem.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B!\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListItem$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListItem$Model;", "itemView", "Landroid/view/View;", "onClick", "Lkotlin/Function1;", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function1;)V", "badgeView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "dateView", "Landroid/widget/TextView;", "moneyView", "typeView", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final BadgeImageView badgeView;
        private final TextView dateView;
        private final TextView moneyView;
        private final Function1<Model, Unit> onClick;
        private final TextView typeView;

        /* compiled from: DonationReceiptListItem.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[DonationReceiptRecord.Type.values().length];
                iArr[DonationReceiptRecord.Type.RECURRING.ordinal()] = 1;
                iArr[DonationReceiptRecord.Type.BOOST.ordinal()] = 2;
                iArr[DonationReceiptRecord.Type.GIFT.ordinal()] = 3;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListItem$Model, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view, Function1<? super Model, Unit> function1) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function1, "onClick");
            this.onClick = function1;
            View findViewById = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.badge)");
            this.badgeView = (BadgeImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.date);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.date)");
            this.dateView = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.type);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.type)");
            this.typeView = (TextView) findViewById3;
            View findViewById4 = view.findViewById(R.id.money);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.id.money)");
            this.moneyView = (TextView) findViewById4;
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1022bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.onClick.invoke(model);
        }

        public void bind(Model model) {
            int i;
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setOnClickListener(new DonationReceiptListItem$ViewHolder$$ExternalSyntheticLambda0(this, model));
            this.badgeView.setBadge(model.getBadge());
            this.dateView.setText(DateUtils.formatDate(Locale.getDefault(), model.getRecord().getTimestamp()));
            TextView textView = this.typeView;
            int i2 = WhenMappings.$EnumSwitchMapping$0[model.getRecord().getType().ordinal()];
            if (i2 == 1) {
                i = R.string.DonationReceiptListFragment__recurring;
            } else if (i2 == 2) {
                i = R.string.DonationReceiptListFragment__one_time;
            } else if (i2 == 3) {
                i = R.string.DonationReceiptListFragment__gift;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            textView.setText(i);
            this.moneyView.setText(FiatMoneyUtil.format(this.context.getResources(), model.getRecord().getAmount()));
        }
    }
}
