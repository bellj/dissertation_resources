package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsRepository;
import org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* compiled from: AdvancedPrivacySettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository$DisablePushMessagesResult;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AdvancedPrivacySettingsViewModel$disablePushMessages$2 extends Lambda implements Function1<AdvancedPrivacySettingsRepository.DisablePushMessagesResult, Unit> {
    final /* synthetic */ AdvancedPrivacySettingsViewModel this$0;

    /* compiled from: AdvancedPrivacySettingsViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[AdvancedPrivacySettingsRepository.DisablePushMessagesResult.values().length];
            iArr[AdvancedPrivacySettingsRepository.DisablePushMessagesResult.SUCCESS.ordinal()] = 1;
            iArr[AdvancedPrivacySettingsRepository.DisablePushMessagesResult.NETWORK_ERROR.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AdvancedPrivacySettingsViewModel$disablePushMessages$2(AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel) {
        super(1);
        this.this$0 = advancedPrivacySettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(AdvancedPrivacySettingsRepository.DisablePushMessagesResult disablePushMessagesResult) {
        invoke(disablePushMessagesResult);
        return Unit.INSTANCE;
    }

    public final void invoke(AdvancedPrivacySettingsRepository.DisablePushMessagesResult disablePushMessagesResult) {
        Intrinsics.checkNotNullParameter(disablePushMessagesResult, "it");
        int i = WhenMappings.$EnumSwitchMapping$0[disablePushMessagesResult.ordinal()];
        if (i == 1) {
            SignalStore.account().setRegistered(false);
            SignalStore.registrationValues().clearRegistrationComplete();
            SignalStore.registrationValues().clearHasUploadedProfile();
        } else if (i == 2) {
            this.this$0.singleEvents.postValue(AdvancedPrivacySettingsViewModel.Event.DISABLE_PUSH_FAILED);
        }
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsViewModel$disablePushMessages$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AdvancedPrivacySettingsViewModel$disablePushMessages$2.m855invoke$lambda0(AdvancedPrivacySettingsViewModel.this, (AdvancedPrivacySettingsState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final AdvancedPrivacySettingsState m855invoke$lambda0(AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel, AdvancedPrivacySettingsState advancedPrivacySettingsState) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsViewModel, "this$0");
        return AdvancedPrivacySettingsState.copy$default(advancedPrivacySettingsViewModel.getState(), false, false, null, false, false, false, false, 63, null);
    }
}
