package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import org.thoughtcrime.securesms.components.ThreadPhotoRailView;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.SharedMediaPreference;
import org.thoughtcrime.securesms.database.MediaDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SharedMediaPreference$ViewHolder$$ExternalSyntheticLambda0 implements ThreadPhotoRailView.OnItemClickedListener {
    public final /* synthetic */ SharedMediaPreference.Model f$0;
    public final /* synthetic */ SharedMediaPreference.ViewHolder f$1;

    public /* synthetic */ SharedMediaPreference$ViewHolder$$ExternalSyntheticLambda0(SharedMediaPreference.Model model, SharedMediaPreference.ViewHolder viewHolder) {
        this.f$0 = model;
        this.f$1 = viewHolder;
    }

    @Override // org.thoughtcrime.securesms.components.ThreadPhotoRailView.OnItemClickedListener
    public final void onItemClicked(MediaDatabase.MediaRecord mediaRecord) {
        SharedMediaPreference.ViewHolder.m1215bind$lambda0(this.f$0, this.f$1, mediaRecord);
    }
}
