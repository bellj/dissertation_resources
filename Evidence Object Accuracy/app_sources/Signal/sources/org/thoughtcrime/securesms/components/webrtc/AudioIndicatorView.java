package org.thoughtcrime.securesms.components.webrtc;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;

/* compiled from: AudioIndicatorView.kt */
@Metadata(d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 %2\u00020\u0001:\u0001%B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u0010J\u001a\u0010\u001c\u001a\u00020\u00142\b\u0010\u001d\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u001e\u001a\u00020\bH\u0002J\u0010\u0010\u001f\u001a\u00020\u00192\u0006\u0010 \u001a\u00020!H\u0014J\u001c\u0010\"\u001a\u00020\u0019*\u00020!2\u0006\u0010#\u001a\u00020\b2\u0006\u0010$\u001a\u00020\bH\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u000e¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/AudioIndicatorView;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "barPadding", "", "barPaint", "Landroid/graphics/Paint;", "barRadius", "barRect", "Landroid/graphics/RectF;", "barWidth", "lastAudioLevel", "Lorg/thoughtcrime/securesms/events/CallParticipant$AudioLevel;", "micMuted", "Landroid/view/View;", "middleBarAnimation", "Landroid/animation/ValueAnimator;", "showAudioLevel", "", "sideBarAnimation", "bind", "", "microphoneEnabled", "level", "createAnimation", "current", "finalHeight", "onDraw", "canvas", "Landroid/graphics/Canvas;", "drawBar", "xOffset", MediaPreviewActivity.SIZE_EXTRA, "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AudioIndicatorView extends FrameLayout {
    public static final Companion Companion = new Companion(null);
    private static final float SIDE_BAR_SHRINK_FACTOR;
    private final float barPadding;
    private final Paint barPaint;
    private final float barRadius;
    private final RectF barRect = new RectF();
    private final float barWidth;
    private CallParticipant.AudioLevel lastAudioLevel;
    private final View micMuted;
    private ValueAnimator middleBarAnimation;
    private boolean showAudioLevel;
    private ValueAnimator sideBarAnimation;

    /* compiled from: AudioIndicatorView.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CallParticipant.AudioLevel.values().length];
            iArr[CallParticipant.AudioLevel.LOWEST.ordinal()] = 1;
            iArr[CallParticipant.AudioLevel.LOW.ordinal()] = 2;
            iArr[CallParticipant.AudioLevel.MEDIUM.ordinal()] = 3;
            iArr[CallParticipant.AudioLevel.HIGH.ordinal()] = 4;
            iArr[CallParticipant.AudioLevel.HIGHEST.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AudioIndicatorView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(attributeSet, "attrs");
        Paint paint = new Paint(1);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(-1);
        this.barPaint = paint;
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        this.barWidth = dimensionUnit.toPixels(4.0f);
        this.barRadius = dimensionUnit.toPixels(32.0f);
        this.barPadding = dimensionUnit.toPixels(4.0f);
        FrameLayout.inflate(context, R.layout.audio_indicator_view, this);
        setWillNotDraw(false);
        View findViewById = findViewById(R.id.mic_muted);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.mic_muted)");
        this.micMuted = findViewById;
    }

    /* compiled from: AudioIndicatorView.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/AudioIndicatorView$Companion;", "", "()V", "SIDE_BAR_SHRINK_FACTOR", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final void bind(boolean z, CallParticipant.AudioLevel audioLevel) {
        float f;
        ViewExtensionsKt.setVisible(this.micMuted, !z);
        boolean z2 = this.showAudioLevel;
        boolean z3 = z && audioLevel != null;
        this.showAudioLevel = z3;
        if (z3) {
            Intrinsics.checkNotNull(audioLevel);
            int i = WhenMappings.$EnumSwitchMapping$0[audioLevel.ordinal()];
            if (i == 1) {
                f = 0.2f;
            } else if (i == 2) {
                f = 0.4f;
            } else if (i == 3) {
                f = 0.6f;
            } else if (i == 4) {
                f = 0.8f;
            } else if (i == 5) {
                f = 1.0f;
            } else {
                throw new NoWhenBranchMatchedException();
            }
            ValueAnimator valueAnimator = this.middleBarAnimation;
            if (valueAnimator != null) {
                valueAnimator.end();
            }
            ValueAnimator createAnimation = createAnimation(this.middleBarAnimation, ((float) getHeight()) * f);
            this.middleBarAnimation = createAnimation;
            if (createAnimation != null) {
                createAnimation.start();
            }
            ValueAnimator valueAnimator2 = this.sideBarAnimation;
            if (valueAnimator2 != null) {
                valueAnimator2.end();
            }
            float height = ((float) getHeight()) * f;
            if (audioLevel != CallParticipant.AudioLevel.LOWEST) {
                height *= SIDE_BAR_SHRINK_FACTOR;
            }
            ValueAnimator createAnimation2 = createAnimation(this.sideBarAnimation, height);
            this.sideBarAnimation = createAnimation2;
            if (createAnimation2 != null) {
                createAnimation2.start();
            }
        }
        if (!(this.showAudioLevel == z2 && audioLevel == this.lastAudioLevel)) {
            invalidate();
        }
        this.lastAudioLevel = audioLevel;
    }

    private final ValueAnimator createAnimation(ValueAnimator valueAnimator, float f) {
        Float f2 = null;
        Object animatedValue = valueAnimator != null ? valueAnimator.getAnimatedValue() : null;
        if (animatedValue instanceof Float) {
            f2 = (Float) animatedValue;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(f2 != null ? f2.floatValue() : 0.0f, f);
        ofFloat.setDuration(200L);
        ofFloat.setInterpolator(new DecelerateInterpolator());
        Intrinsics.checkNotNullExpressionValue(ofFloat, "ofFloat(currentHeight, f…erateInterpolator()\n    }");
        return ofFloat;
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        super.onDraw(canvas);
        ValueAnimator valueAnimator = this.middleBarAnimation;
        Float f = null;
        Object animatedValue = valueAnimator != null ? valueAnimator.getAnimatedValue() : null;
        Float f2 = animatedValue instanceof Float ? (Float) animatedValue : null;
        ValueAnimator valueAnimator2 = this.sideBarAnimation;
        Object animatedValue2 = valueAnimator2 != null ? valueAnimator2.getAnimatedValue() : null;
        if (animatedValue2 instanceof Float) {
            f = (Float) animatedValue2;
        }
        if (this.showAudioLevel && f2 != null && f != null) {
            float f3 = (float) 2;
            float width = (((float) getWidth()) - ((((float) 3) * this.barWidth) + (this.barPadding * f3))) / f3;
            drawBar(canvas, width, f.floatValue());
            drawBar(canvas, this.barPadding + this.barWidth + width, f2.floatValue());
            drawBar(canvas, (f3 * (this.barPadding + this.barWidth)) + width, f.floatValue());
            ValueAnimator valueAnimator3 = this.middleBarAnimation;
            boolean z = true;
            if (!(valueAnimator3 != null && valueAnimator3.isRunning())) {
                ValueAnimator valueAnimator4 = this.sideBarAnimation;
                if (valueAnimator4 == null || !valueAnimator4.isRunning()) {
                    z = false;
                }
                if (!z) {
                    return;
                }
            }
            invalidate();
        }
    }

    private final void drawBar(Canvas canvas, float f, float f2) {
        float height = (((float) canvas.getHeight()) - f2) / ((float) 2);
        this.barRect.set(f, height, this.barWidth + f, ((float) canvas.getHeight()) - height);
        RectF rectF = this.barRect;
        float f3 = this.barRadius;
        canvas.drawRoundRect(rectF, f3, f3, this.barPaint);
    }
}
