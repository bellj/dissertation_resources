package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class DeliveryStatusView extends FrameLayout {
    private static final String TAG = Log.tag(DeliveryStatusView.class);
    private final ImageView deliveredIndicator;
    private final ImageView pendingIndicator;
    private final ImageView readIndicator;
    private final RotateAnimation rotationAnimation;
    private final ImageView sentIndicator;

    public DeliveryStatusView(Context context) {
        this(context, null);
    }

    public DeliveryStatusView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public DeliveryStatusView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.delivery_status_view, this);
        this.deliveredIndicator = (ImageView) findViewById(R.id.delivered_indicator);
        this.sentIndicator = (ImageView) findViewById(R.id.sent_indicator);
        this.pendingIndicator = (ImageView) findViewById(R.id.pending_indicator);
        this.readIndicator = (ImageView) findViewById(R.id.read_indicator);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 360.0f, 1, 0.5f, 1, 0.5f);
        this.rotationAnimation = rotateAnimation;
        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setDuration(1500);
        rotateAnimation.setRepeatCount(-1);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.DeliveryStatusView, 0, 0);
            setTint(obtainStyledAttributes.getColor(0, getResources().getColor(R.color.core_white)));
            obtainStyledAttributes.recycle();
        }
    }

    public void setNone() {
        setVisibility(8);
    }

    public boolean isPending() {
        return this.pendingIndicator.getVisibility() == 0;
    }

    public void setPending() {
        setVisibility(0);
        this.pendingIndicator.setVisibility(0);
        this.pendingIndicator.startAnimation(this.rotationAnimation);
        this.sentIndicator.setVisibility(8);
        this.deliveredIndicator.setVisibility(8);
        this.readIndicator.setVisibility(8);
    }

    public void setSent() {
        setVisibility(0);
        this.pendingIndicator.setVisibility(8);
        this.pendingIndicator.clearAnimation();
        this.sentIndicator.setVisibility(0);
        this.deliveredIndicator.setVisibility(8);
        this.readIndicator.setVisibility(8);
    }

    public void setDelivered() {
        setVisibility(0);
        this.pendingIndicator.setVisibility(8);
        this.pendingIndicator.clearAnimation();
        this.sentIndicator.setVisibility(8);
        this.deliveredIndicator.setVisibility(0);
        this.readIndicator.setVisibility(8);
    }

    public void setRead() {
        setVisibility(0);
        this.pendingIndicator.setVisibility(8);
        this.pendingIndicator.clearAnimation();
        this.sentIndicator.setVisibility(8);
        this.deliveredIndicator.setVisibility(8);
        this.readIndicator.setVisibility(0);
    }

    public void setTint(int i) {
        this.pendingIndicator.setColorFilter(i);
        this.deliveredIndicator.setColorFilter(i);
        this.sentIndicator.setColorFilter(i);
        this.readIndicator.setColorFilter(i);
    }
}
