package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NotificationProfileDetailsFragmentArgs {
    private final HashMap arguments;

    private NotificationProfileDetailsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private NotificationProfileDetailsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static NotificationProfileDetailsFragmentArgs fromBundle(Bundle bundle) {
        NotificationProfileDetailsFragmentArgs notificationProfileDetailsFragmentArgs = new NotificationProfileDetailsFragmentArgs();
        bundle.setClassLoader(NotificationProfileDetailsFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("profileId")) {
            notificationProfileDetailsFragmentArgs.arguments.put("profileId", Long.valueOf(bundle.getLong("profileId")));
            return notificationProfileDetailsFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"profileId\" is missing and does not have an android:defaultValue");
    }

    public long getProfileId() {
        return ((Long) this.arguments.get("profileId")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("profileId")) {
            bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NotificationProfileDetailsFragmentArgs notificationProfileDetailsFragmentArgs = (NotificationProfileDetailsFragmentArgs) obj;
        return this.arguments.containsKey("profileId") == notificationProfileDetailsFragmentArgs.arguments.containsKey("profileId") && getProfileId() == notificationProfileDetailsFragmentArgs.getProfileId();
    }

    public int hashCode() {
        return 31 + ((int) (getProfileId() ^ (getProfileId() >>> 32)));
    }

    public String toString() {
        return "NotificationProfileDetailsFragmentArgs{profileId=" + getProfileId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(NotificationProfileDetailsFragmentArgs notificationProfileDetailsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(notificationProfileDetailsFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public NotificationProfileDetailsFragmentArgs build() {
            return new NotificationProfileDetailsFragmentArgs(this.arguments);
        }

        public Builder setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }
    }
}
