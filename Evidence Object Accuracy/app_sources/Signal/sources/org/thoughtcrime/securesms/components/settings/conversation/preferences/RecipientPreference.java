package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import android.widget.TextView;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: RecipientPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RecipientPreference {
    public static final RecipientPreference INSTANCE = new RecipientPreference();

    private RecipientPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new RecipientPreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.group_recipient_list_item));
    }

    /* compiled from: RecipientPreference.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\n\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B)\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u0010\b\u0002\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007¢\u0006\u0002\u0010\tJ\u0010\u0010\u000f\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0000H\u0016J\u0010\u0010\u0011\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\nR\u0019\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isAdmin", "", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;ZLkotlin/jvm/functions/Function0;)V", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final boolean isAdmin;
        private final Function0<Unit> onClick;
        private final Recipient recipient;

        public /* synthetic */ Model(Recipient recipient, boolean z, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(recipient, (i & 2) != 0 ? false : z, (i & 4) != 0 ? null : function0);
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final boolean isAdmin() {
            return this.isAdmin;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Recipient recipient, boolean z, Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.recipient = recipient;
            this.isAdmin = z;
            this.onClick = function0;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.recipient.getId(), model.recipient.getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && this.recipient.hasSameContent(model.recipient) && this.isAdmin == model.isAdmin;
        }
    }

    /* compiled from: RecipientPreference.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0002H\u0016R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", RecipientDatabase.ABOUT, "Landroid/widget/TextView;", "admin", "avatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "name", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final TextView about;
        private final View admin;
        private final AvatarImageView avatar;
        private final BadgeImageView badge;
        private final TextView name;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.recipient_avatar);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.recipient_avatar)");
            this.avatar = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.recipient_name);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.recipient_name)");
            this.name = (TextView) findViewById2;
            this.about = (TextView) view.findViewById(R.id.recipient_about);
            this.admin = view.findViewById(R.id.admin);
            View findViewById3 = view.findViewById(R.id.recipient_badge);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.recipient_badge)");
            this.badge = (BadgeImageView) findViewById3;
        }

        public void bind(Model model) {
            String str;
            Intrinsics.checkNotNullParameter(model, "model");
            if (model.getOnClick() != null) {
                this.itemView.setOnClickListener(new RecipientPreference$ViewHolder$$ExternalSyntheticLambda0(model));
            } else {
                this.itemView.setOnClickListener(null);
            }
            this.avatar.setRecipient(model.getRecipient());
            this.badge.setBadgeFromRecipient(model.getRecipient());
            TextView textView = this.name;
            if (model.getRecipient().isSelf()) {
                str = this.context.getString(R.string.Recipient_you);
            } else {
                str = model.getRecipient().getDisplayName(this.context);
            }
            textView.setText(str);
            String combinedAboutAndEmoji = model.getRecipient().getCombinedAboutAndEmoji();
            if (combinedAboutAndEmoji == null || combinedAboutAndEmoji.length() == 0) {
                TextView textView2 = this.about;
                if (textView2 != null) {
                    textView2.setVisibility(8);
                }
            } else {
                TextView textView3 = this.about;
                if (textView3 != null) {
                    textView3.setText(model.getRecipient().getCombinedAboutAndEmoji());
                }
                TextView textView4 = this.about;
                if (textView4 != null) {
                    textView4.setVisibility(0);
                }
            }
            View view = this.admin;
            if (view != null) {
                ViewExtensionsKt.setVisible(view, model.isAdmin());
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1213bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke();
        }
    }
}
