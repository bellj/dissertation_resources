package org.thoughtcrime.securesms.components.settings.conversation.permissions;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsEvents;
import org.thoughtcrime.securesms.groups.GroupAccessControl;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: PermissionsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u001fB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018J\u000e\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u0018J\u000e\u0010\u001b\u001a\u00020\u00162\u0006\u0010\u001c\u001a\u00020\u0018J\f\u0010\u001d\u001a\u00020\u001e*\u00020\u0018H\u0002R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00110\b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000bR\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00110\u0014X\u0004¢\u0006\u0002\n\u0000¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsRepository;", "(Lorg/thoughtcrime/securesms/groups/GroupId;Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsRepository;)V", "events", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsEvents;", "getEvents", "()Landroidx/lifecycle/LiveData;", "internalEvents", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "liveGroup", "Lorg/thoughtcrime/securesms/groups/LiveGroup;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsState;", "getState", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "setAnnouncementGroup", "", "announcementGroup", "", "setNonAdminCanAddMembers", "nonAdminCanAddMembers", "setNonAdminCanEditGroupInfo", "nonAdminCanEditGroupInfo", "asGroupAccessControl", "Lorg/thoughtcrime/securesms/groups/GroupAccessControl;", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PermissionsSettingsViewModel extends ViewModel {
    private final LiveData<PermissionsSettingsEvents> events;
    private final GroupId groupId;
    private final SingleLiveEvent<PermissionsSettingsEvents> internalEvents;
    private final LiveGroup liveGroup;
    private final PermissionsSettingsRepository repository;
    private final LiveData<PermissionsSettingsState> state;
    private final Store<PermissionsSettingsState> store;

    /* JADX DEBUG: Type inference failed for r9v2. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
    /* JADX DEBUG: Type inference failed for r9v3. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.groups.GroupAccessControl>, androidx.lifecycle.LiveData<Input> */
    /* JADX DEBUG: Type inference failed for r9v4. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.groups.GroupAccessControl>, androidx.lifecycle.LiveData<Input> */
    /* JADX DEBUG: Type inference failed for r9v5. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
    public PermissionsSettingsViewModel(GroupId groupId, PermissionsSettingsRepository permissionsSettingsRepository) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(permissionsSettingsRepository, "repository");
        this.groupId = groupId;
        this.repository = permissionsSettingsRepository;
        Store<PermissionsSettingsState> store = new Store<>(new PermissionsSettingsState(false, false, false, false, 15, null));
        this.store = store;
        LiveGroup liveGroup = new LiveGroup(groupId);
        this.liveGroup = liveGroup;
        SingleLiveEvent<PermissionsSettingsEvents> singleLiveEvent = new SingleLiveEvent<>();
        this.internalEvents = singleLiveEvent;
        LiveData<PermissionsSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        this.events = singleLiveEvent;
        store.update(liveGroup.isSelfAdmin(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return PermissionsSettingsViewModel.m1186_init_$lambda0((Boolean) obj, (PermissionsSettingsState) obj2);
            }
        });
        store.update(liveGroup.getMembershipAdditionAccessControl(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return PermissionsSettingsViewModel.m1187_init_$lambda1((GroupAccessControl) obj, (PermissionsSettingsState) obj2);
            }
        });
        store.update(liveGroup.getAttributesAccessControl(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda2
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return PermissionsSettingsViewModel.m1188_init_$lambda2((GroupAccessControl) obj, (PermissionsSettingsState) obj2);
            }
        });
        store.update(liveGroup.isAnnouncementGroup(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return PermissionsSettingsViewModel.m1189_init_$lambda3((Boolean) obj, (PermissionsSettingsState) obj2);
            }
        });
    }

    public final LiveData<PermissionsSettingsState> getState() {
        return this.state;
    }

    public final LiveData<PermissionsSettingsEvents> getEvents() {
        return this.events;
    }

    /* renamed from: _init_$lambda-0 */
    public static final PermissionsSettingsState m1186_init_$lambda0(Boolean bool, PermissionsSettingsState permissionsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(permissionsSettingsState, "state");
        Intrinsics.checkNotNullExpressionValue(bool, "isSelfAdmin");
        return PermissionsSettingsState.copy$default(permissionsSettingsState, bool.booleanValue(), false, false, false, 14, null);
    }

    /* renamed from: _init_$lambda-1 */
    public static final PermissionsSettingsState m1187_init_$lambda1(GroupAccessControl groupAccessControl, PermissionsSettingsState permissionsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(permissionsSettingsState, "state");
        return PermissionsSettingsState.copy$default(permissionsSettingsState, false, groupAccessControl == GroupAccessControl.ALL_MEMBERS, false, false, 13, null);
    }

    /* renamed from: _init_$lambda-2 */
    public static final PermissionsSettingsState m1188_init_$lambda2(GroupAccessControl groupAccessControl, PermissionsSettingsState permissionsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(permissionsSettingsState, "state");
        return PermissionsSettingsState.copy$default(permissionsSettingsState, false, false, groupAccessControl == GroupAccessControl.ALL_MEMBERS, false, 11, null);
    }

    /* renamed from: _init_$lambda-3 */
    public static final PermissionsSettingsState m1189_init_$lambda3(Boolean bool, PermissionsSettingsState permissionsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(permissionsSettingsState, "state");
        Intrinsics.checkNotNullExpressionValue(bool, "isAnnouncementGroup");
        return PermissionsSettingsState.copy$default(permissionsSettingsState, false, false, false, bool.booleanValue(), 7, null);
    }

    public final void setNonAdminCanAddMembers(boolean z) {
        this.repository.applyMembershipRightsChange(this.groupId, asGroupAccessControl(z), new GroupChangeErrorCallback() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda6
            @Override // org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback
            public final void onError(GroupChangeFailureReason groupChangeFailureReason) {
                PermissionsSettingsViewModel.m1191setNonAdminCanAddMembers$lambda4(PermissionsSettingsViewModel.this, groupChangeFailureReason);
            }
        });
    }

    /* renamed from: setNonAdminCanAddMembers$lambda-4 */
    public static final void m1191setNonAdminCanAddMembers$lambda4(PermissionsSettingsViewModel permissionsSettingsViewModel, GroupChangeFailureReason groupChangeFailureReason) {
        Intrinsics.checkNotNullParameter(permissionsSettingsViewModel, "this$0");
        Intrinsics.checkNotNullParameter(groupChangeFailureReason, "reason");
        permissionsSettingsViewModel.internalEvents.postValue(new PermissionsSettingsEvents.GroupChangeError(groupChangeFailureReason));
    }

    public final void setNonAdminCanEditGroupInfo(boolean z) {
        this.repository.applyAttributesRightsChange(this.groupId, asGroupAccessControl(z), new GroupChangeErrorCallback() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback
            public final void onError(GroupChangeFailureReason groupChangeFailureReason) {
                PermissionsSettingsViewModel.m1192setNonAdminCanEditGroupInfo$lambda5(PermissionsSettingsViewModel.this, groupChangeFailureReason);
            }
        });
    }

    /* renamed from: setNonAdminCanEditGroupInfo$lambda-5 */
    public static final void m1192setNonAdminCanEditGroupInfo$lambda5(PermissionsSettingsViewModel permissionsSettingsViewModel, GroupChangeFailureReason groupChangeFailureReason) {
        Intrinsics.checkNotNullParameter(permissionsSettingsViewModel, "this$0");
        Intrinsics.checkNotNullParameter(groupChangeFailureReason, "reason");
        permissionsSettingsViewModel.internalEvents.postValue(new PermissionsSettingsEvents.GroupChangeError(groupChangeFailureReason));
    }

    public final void setAnnouncementGroup(boolean z) {
        this.repository.applyAnnouncementGroupChange(this.groupId, z, new GroupChangeErrorCallback() { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsViewModel$$ExternalSyntheticLambda4
            @Override // org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback
            public final void onError(GroupChangeFailureReason groupChangeFailureReason) {
                PermissionsSettingsViewModel.m1190setAnnouncementGroup$lambda6(PermissionsSettingsViewModel.this, groupChangeFailureReason);
            }
        });
    }

    /* renamed from: setAnnouncementGroup$lambda-6 */
    public static final void m1190setAnnouncementGroup$lambda6(PermissionsSettingsViewModel permissionsSettingsViewModel, GroupChangeFailureReason groupChangeFailureReason) {
        Intrinsics.checkNotNullParameter(permissionsSettingsViewModel, "this$0");
        Intrinsics.checkNotNullParameter(groupChangeFailureReason, "reason");
        permissionsSettingsViewModel.internalEvents.postValue(new PermissionsSettingsEvents.GroupChangeError(groupChangeFailureReason));
    }

    private final GroupAccessControl asGroupAccessControl(boolean z) {
        if (z) {
            return GroupAccessControl.ALL_MEMBERS;
        }
        return GroupAccessControl.ONLY_ADMINS;
    }

    /* compiled from: PermissionsSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsRepository;", "(Lorg/thoughtcrime/securesms/groups/GroupId;Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final GroupId groupId;
        private final PermissionsSettingsRepository repository;

        public Factory(GroupId groupId, PermissionsSettingsRepository permissionsSettingsRepository) {
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(permissionsSettingsRepository, "repository");
            this.groupId = groupId;
            this.repository = permissionsSettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new PermissionsSettingsViewModel(this.groupId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
