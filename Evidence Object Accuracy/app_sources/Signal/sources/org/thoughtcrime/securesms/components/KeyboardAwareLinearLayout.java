package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowInsets;
import androidx.appcompat.widget.LinearLayoutCompat;
import java.lang.reflect.Field;
import java.util.HashSet;
import java.util.Set;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class KeyboardAwareLinearLayout extends LinearLayoutCompat {
    private static final String TAG = Log.tag(KeyboardAwareLinearLayout.class);
    private final int defaultCustomKeyboardSize;
    private final DisplayMetrics displayMetrics;
    private final Set<OnKeyboardHiddenListener> hiddenListeners;
    private boolean isBubble;
    private boolean keyboardOpen;
    private final int minCustomKeyboardSize;
    private final int minCustomKeyboardTopMarginLandscape;
    private final int minCustomKeyboardTopMarginLandscapeBubble;
    private final int minCustomKeyboardTopMarginPortrait;
    private final int minKeyboardSize;
    private final Rect rect;
    private int rotation;
    private final Set<OnKeyboardShownListener> shownListeners;
    private final int statusBarHeight;
    private int viewInset;

    /* loaded from: classes4.dex */
    public interface OnKeyboardHiddenListener {
        void onKeyboardHidden();
    }

    /* loaded from: classes4.dex */
    public interface OnKeyboardShownListener {
        void onKeyboardShown();
    }

    public KeyboardAwareLinearLayout(Context context) {
        this(context, null);
    }

    public KeyboardAwareLinearLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public KeyboardAwareLinearLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.rect = new Rect();
        this.hiddenListeners = new HashSet();
        this.shownListeners = new HashSet();
        this.displayMetrics = new DisplayMetrics();
        this.keyboardOpen = false;
        this.rotation = 0;
        this.isBubble = false;
        this.minKeyboardSize = getResources().getDimensionPixelSize(R.dimen.min_keyboard_size);
        this.minCustomKeyboardSize = getResources().getDimensionPixelSize(R.dimen.min_custom_keyboard_size);
        this.defaultCustomKeyboardSize = getResources().getDimensionPixelSize(R.dimen.default_custom_keyboard_size);
        this.minCustomKeyboardTopMarginPortrait = getResources().getDimensionPixelSize(R.dimen.min_custom_keyboard_top_margin_portrait);
        this.minCustomKeyboardTopMarginLandscape = getResources().getDimensionPixelSize(R.dimen.min_custom_keyboard_top_margin_portrait);
        this.minCustomKeyboardTopMarginLandscapeBubble = getResources().getDimensionPixelSize(R.dimen.min_custom_keyboard_top_margin_landscape_bubble);
        this.statusBarHeight = ViewUtil.getStatusBarHeight(this);
        this.viewInset = getViewInset();
    }

    @Override // androidx.appcompat.widget.LinearLayoutCompat, android.view.View
    public void onMeasure(int i, int i2) {
        updateRotation();
        updateKeyboardState();
        super.onMeasure(i, i2);
    }

    public void setIsBubble(boolean z) {
        this.isBubble = z;
    }

    private void updateRotation() {
        int i = this.rotation;
        int deviceRotation = getDeviceRotation();
        this.rotation = deviceRotation;
        if (i != deviceRotation) {
            Log.i(TAG, "rotation changed");
            onKeyboardClose();
        }
    }

    private void updateKeyboardState() {
        lambda$updateKeyboardState$0(Integer.MAX_VALUE);
    }

    /* renamed from: updateKeyboardState */
    public void lambda$updateKeyboardState$0(int i) {
        if (this.viewInset == 0 && Build.VERSION.SDK_INT >= 21) {
            this.viewInset = getViewInset();
        }
        getWindowVisibleDisplayFrame(this.rect);
        int availableHeight = getAvailableHeight() - this.rect.bottom;
        if (availableHeight > this.minKeyboardSize) {
            if (getKeyboardHeight() != availableHeight) {
                if (isLandscape()) {
                    setKeyboardLandscapeHeight(availableHeight);
                } else {
                    setKeyboardPortraitHeight(availableHeight);
                }
            }
            if (!this.keyboardOpen) {
                onKeyboardOpen(availableHeight);
            }
        } else if (!this.keyboardOpen) {
        } else {
            if (i == availableHeight) {
                onKeyboardClose();
            } else {
                postDelayed(new Runnable(availableHeight) { // from class: org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout$$ExternalSyntheticLambda0
                    public final /* synthetic */ int f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        KeyboardAwareLinearLayout.this.lambda$updateKeyboardState$0(this.f$1);
                    }
                }, 100);
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onAttachedToWindow() {
        int i;
        super.onAttachedToWindow();
        this.rotation = getDeviceRotation();
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23 && getRootWindowInsets() != null) {
            WindowInsets rootWindowInsets = getRootWindowInsets();
            if (i2 >= 30) {
                i = rootWindowInsets.getInsets(WindowInsets.Type.navigationBars()).bottom;
            } else {
                i = rootWindowInsets.getStableInsetBottom();
            }
            if (i != 0) {
                int i3 = this.viewInset;
                if (i3 == 0 || i3 == this.statusBarHeight) {
                    String str = TAG;
                    Log.i(str, "Updating view inset based on WindowInsets. viewInset: " + this.viewInset + " windowInset: " + i);
                    this.viewInset = i;
                }
            }
        }
    }

    private int getViewInset() {
        try {
            Field declaredField = View.class.getDeclaredField("mAttachInfo");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(this);
            if (obj != null) {
                Field declaredField2 = obj.getClass().getDeclaredField("mStableInsets");
                declaredField2.setAccessible(true);
                Rect rect = (Rect) declaredField2.get(obj);
                if (rect != null) {
                    return rect.bottom;
                }
            }
        } catch (IllegalAccessException | NoSuchFieldException unused) {
        }
        return this.statusBarHeight;
    }

    private int getAvailableHeight() {
        int height = getRootView().getHeight() - this.viewInset;
        int width = getRootView().getWidth();
        return (!isLandscape() || height <= width) ? height : width;
    }

    protected void onKeyboardOpen(int i) {
        String str = TAG;
        Log.i(str, "onKeyboardOpen(" + i + ")");
        this.keyboardOpen = true;
        notifyShownListeners();
    }

    protected void onKeyboardClose() {
        Log.i(TAG, "onKeyboardClose()");
        this.keyboardOpen = false;
        notifyHiddenListeners();
    }

    public boolean isKeyboardOpen() {
        return this.keyboardOpen;
    }

    public int getKeyboardHeight() {
        return isLandscape() ? getKeyboardLandscapeHeight() : getKeyboardPortraitHeight();
    }

    public boolean isLandscape() {
        int deviceRotation = getDeviceRotation();
        return deviceRotation == 1 || deviceRotation == 3;
    }

    private int getDeviceRotation() {
        if (Build.VERSION.SDK_INT >= 30) {
            getContext().getDisplay().getRealMetrics(this.displayMetrics);
        } else {
            ServiceUtil.getWindowManager(getContext()).getDefaultDisplay().getRealMetrics(this.displayMetrics);
        }
        DisplayMetrics displayMetrics = this.displayMetrics;
        return displayMetrics.widthPixels > displayMetrics.heightPixels ? 1 : 0;
    }

    private int getKeyboardLandscapeHeight() {
        if (this.isBubble) {
            return getRootView().getHeight() - this.minCustomKeyboardTopMarginLandscapeBubble;
        }
        return Util.clamp(PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("keyboard_height_landscape", this.defaultCustomKeyboardSize), this.minCustomKeyboardSize, getRootView().getHeight() - this.minCustomKeyboardTopMarginLandscape);
    }

    private int getKeyboardPortraitHeight() {
        if (!this.isBubble) {
            return Util.clamp(PreferenceManager.getDefaultSharedPreferences(getContext()).getInt("keyboard_height_portrait", this.defaultCustomKeyboardSize), this.minCustomKeyboardSize, getRootView().getHeight() - this.minCustomKeyboardTopMarginPortrait);
        }
        int height = getRootView().getHeight();
        double d = (double) height;
        Double.isNaN(d);
        return height - ((int) (d * 0.45d));
    }

    private void setKeyboardPortraitHeight(int i) {
        if (!this.isBubble) {
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt("keyboard_height_portrait", i).apply();
        }
    }

    private void setKeyboardLandscapeHeight(int i) {
        if (!this.isBubble) {
            PreferenceManager.getDefaultSharedPreferences(getContext()).edit().putInt("keyboard_height_landscape", i).apply();
        }
    }

    public void postOnKeyboardClose(final Runnable runnable) {
        if (this.keyboardOpen) {
            addOnKeyboardHiddenListener(new OnKeyboardHiddenListener() { // from class: org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.1
                @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardHiddenListener
                public void onKeyboardHidden() {
                    KeyboardAwareLinearLayout.this.removeOnKeyboardHiddenListener(this);
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public void postOnKeyboardOpen(final Runnable runnable) {
        if (!this.keyboardOpen) {
            addOnKeyboardShownListener(new OnKeyboardShownListener() { // from class: org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.2
                @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
                public void onKeyboardShown() {
                    KeyboardAwareLinearLayout.this.removeOnKeyboardShownListener(this);
                    runnable.run();
                }
            });
        } else {
            runnable.run();
        }
    }

    public void addOnKeyboardHiddenListener(OnKeyboardHiddenListener onKeyboardHiddenListener) {
        this.hiddenListeners.add(onKeyboardHiddenListener);
    }

    public void removeOnKeyboardHiddenListener(OnKeyboardHiddenListener onKeyboardHiddenListener) {
        this.hiddenListeners.remove(onKeyboardHiddenListener);
    }

    public void addOnKeyboardShownListener(OnKeyboardShownListener onKeyboardShownListener) {
        this.shownListeners.add(onKeyboardShownListener);
    }

    public void removeOnKeyboardShownListener(OnKeyboardShownListener onKeyboardShownListener) {
        this.shownListeners.remove(onKeyboardShownListener);
    }

    private void notifyHiddenListeners() {
        for (OnKeyboardHiddenListener onKeyboardHiddenListener : new HashSet(this.hiddenListeners)) {
            onKeyboardHiddenListener.onKeyboardHidden();
        }
    }

    private void notifyShownListeners() {
        for (OnKeyboardShownListener onKeyboardShownListener : new HashSet(this.shownListeners)) {
            onKeyboardShownListener.onKeyboardShown();
        }
    }
}
