package org.thoughtcrime.securesms.components.voice;

import kotlin.Metadata;

/* compiled from: VoiceNoteMediaControllerOwner.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u00002\u00020\u0001R\u0012\u0010\u0002\u001a\u00020\u0003X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaControllerOwner;", "", "voiceNoteMediaController", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaController;", "getVoiceNoteMediaController", "()Lorg/thoughtcrime/securesms/components/voice/VoiceNoteMediaController;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface VoiceNoteMediaControllerOwner {
    VoiceNoteMediaController getVoiceNoteMediaController();
}
