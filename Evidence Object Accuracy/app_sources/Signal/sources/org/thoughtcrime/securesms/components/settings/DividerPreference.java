package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DividerPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "()V", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DividerPreference extends PreferenceModel<DividerPreference> {
    public boolean areItemsTheSame(DividerPreference dividerPreference) {
        Intrinsics.checkNotNullParameter(dividerPreference, "newItem");
        return true;
    }

    public DividerPreference() {
        super(null, null, null, null, false, 31, null);
    }
}
