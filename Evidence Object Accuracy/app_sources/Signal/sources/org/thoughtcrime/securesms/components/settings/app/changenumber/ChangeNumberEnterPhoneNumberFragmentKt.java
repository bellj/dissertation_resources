package org.thoughtcrime.securesms.components.settings.app.changenumber;

import kotlin.Metadata;

/* compiled from: ChangeNumberEnterPhoneNumberFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0003"}, d2 = {"NEW_NUMBER_COUNTRY_SELECT", "", "OLD_NUMBER_COUNTRY_SELECT", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberEnterPhoneNumberFragmentKt {
    private static final String NEW_NUMBER_COUNTRY_SELECT;
    private static final String OLD_NUMBER_COUNTRY_SELECT;
}
