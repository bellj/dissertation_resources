package org.thoughtcrime.securesms.components.settings.app.data;

import java.util.Set;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.webrtc.CallBandwidthMode;

/* compiled from: DataAndStorageSettingsState.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\"\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001BO\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\u000e¢\u0006\u0002\u0010\u000fJ\t\u0010\u001b\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010\u001f\u001a\u00020\nHÆ\u0003J\t\u0010 \u001a\u00020\fHÆ\u0003J\t\u0010!\u001a\u00020\u000eHÆ\u0003Ja\u0010\"\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\u000eHÆ\u0001J\u0013\u0010#\u001a\u00020\f2\b\u0010$\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010%\u001a\u00020&HÖ\u0001J\t\u0010'\u001a\u00020\u0006HÖ\u0001R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0012R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014¨\u0006("}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsState;", "", "totalStorageUse", "", "mobileAutoDownloadValues", "", "", "wifiAutoDownloadValues", "roamingAutoDownloadValues", "callBandwidthMode", "Lorg/thoughtcrime/securesms/webrtc/CallBandwidthMode;", "isProxyEnabled", "", "sentMediaQuality", "Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "(JLjava/util/Set;Ljava/util/Set;Ljava/util/Set;Lorg/thoughtcrime/securesms/webrtc/CallBandwidthMode;ZLorg/thoughtcrime/securesms/mms/SentMediaQuality;)V", "getCallBandwidthMode", "()Lorg/thoughtcrime/securesms/webrtc/CallBandwidthMode;", "()Z", "getMobileAutoDownloadValues", "()Ljava/util/Set;", "getRoamingAutoDownloadValues", "getSentMediaQuality", "()Lorg/thoughtcrime/securesms/mms/SentMediaQuality;", "getTotalStorageUse", "()J", "getWifiAutoDownloadValues", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DataAndStorageSettingsState {
    private final CallBandwidthMode callBandwidthMode;
    private final boolean isProxyEnabled;
    private final Set<String> mobileAutoDownloadValues;
    private final Set<String> roamingAutoDownloadValues;
    private final SentMediaQuality sentMediaQuality;
    private final long totalStorageUse;
    private final Set<String> wifiAutoDownloadValues;

    public final long component1() {
        return this.totalStorageUse;
    }

    public final Set<String> component2() {
        return this.mobileAutoDownloadValues;
    }

    public final Set<String> component3() {
        return this.wifiAutoDownloadValues;
    }

    public final Set<String> component4() {
        return this.roamingAutoDownloadValues;
    }

    public final CallBandwidthMode component5() {
        return this.callBandwidthMode;
    }

    public final boolean component6() {
        return this.isProxyEnabled;
    }

    public final SentMediaQuality component7() {
        return this.sentMediaQuality;
    }

    public final DataAndStorageSettingsState copy(long j, Set<String> set, Set<String> set2, Set<String> set3, CallBandwidthMode callBandwidthMode, boolean z, SentMediaQuality sentMediaQuality) {
        Intrinsics.checkNotNullParameter(set, "mobileAutoDownloadValues");
        Intrinsics.checkNotNullParameter(set2, "wifiAutoDownloadValues");
        Intrinsics.checkNotNullParameter(set3, "roamingAutoDownloadValues");
        Intrinsics.checkNotNullParameter(callBandwidthMode, "callBandwidthMode");
        Intrinsics.checkNotNullParameter(sentMediaQuality, "sentMediaQuality");
        return new DataAndStorageSettingsState(j, set, set2, set3, callBandwidthMode, z, sentMediaQuality);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DataAndStorageSettingsState)) {
            return false;
        }
        DataAndStorageSettingsState dataAndStorageSettingsState = (DataAndStorageSettingsState) obj;
        return this.totalStorageUse == dataAndStorageSettingsState.totalStorageUse && Intrinsics.areEqual(this.mobileAutoDownloadValues, dataAndStorageSettingsState.mobileAutoDownloadValues) && Intrinsics.areEqual(this.wifiAutoDownloadValues, dataAndStorageSettingsState.wifiAutoDownloadValues) && Intrinsics.areEqual(this.roamingAutoDownloadValues, dataAndStorageSettingsState.roamingAutoDownloadValues) && this.callBandwidthMode == dataAndStorageSettingsState.callBandwidthMode && this.isProxyEnabled == dataAndStorageSettingsState.isProxyEnabled && this.sentMediaQuality == dataAndStorageSettingsState.sentMediaQuality;
    }

    public int hashCode() {
        int m = ((((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.totalStorageUse) * 31) + this.mobileAutoDownloadValues.hashCode()) * 31) + this.wifiAutoDownloadValues.hashCode()) * 31) + this.roamingAutoDownloadValues.hashCode()) * 31) + this.callBandwidthMode.hashCode()) * 31;
        boolean z = this.isProxyEnabled;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return ((m + i) * 31) + this.sentMediaQuality.hashCode();
    }

    public String toString() {
        return "DataAndStorageSettingsState(totalStorageUse=" + this.totalStorageUse + ", mobileAutoDownloadValues=" + this.mobileAutoDownloadValues + ", wifiAutoDownloadValues=" + this.wifiAutoDownloadValues + ", roamingAutoDownloadValues=" + this.roamingAutoDownloadValues + ", callBandwidthMode=" + this.callBandwidthMode + ", isProxyEnabled=" + this.isProxyEnabled + ", sentMediaQuality=" + this.sentMediaQuality + ')';
    }

    public DataAndStorageSettingsState(long j, Set<String> set, Set<String> set2, Set<String> set3, CallBandwidthMode callBandwidthMode, boolean z, SentMediaQuality sentMediaQuality) {
        Intrinsics.checkNotNullParameter(set, "mobileAutoDownloadValues");
        Intrinsics.checkNotNullParameter(set2, "wifiAutoDownloadValues");
        Intrinsics.checkNotNullParameter(set3, "roamingAutoDownloadValues");
        Intrinsics.checkNotNullParameter(callBandwidthMode, "callBandwidthMode");
        Intrinsics.checkNotNullParameter(sentMediaQuality, "sentMediaQuality");
        this.totalStorageUse = j;
        this.mobileAutoDownloadValues = set;
        this.wifiAutoDownloadValues = set2;
        this.roamingAutoDownloadValues = set3;
        this.callBandwidthMode = callBandwidthMode;
        this.isProxyEnabled = z;
        this.sentMediaQuality = sentMediaQuality;
    }

    public final long getTotalStorageUse() {
        return this.totalStorageUse;
    }

    public final Set<String> getMobileAutoDownloadValues() {
        return this.mobileAutoDownloadValues;
    }

    public final Set<String> getWifiAutoDownloadValues() {
        return this.wifiAutoDownloadValues;
    }

    public final Set<String> getRoamingAutoDownloadValues() {
        return this.roamingAutoDownloadValues;
    }

    public final CallBandwidthMode getCallBandwidthMode() {
        return this.callBandwidthMode;
    }

    public final boolean isProxyEnabled() {
        return this.isProxyEnabled;
    }

    public final SentMediaQuality getSentMediaQuality() {
        return this.sentMediaQuality;
    }
}
