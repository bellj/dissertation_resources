package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileAddMembers;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileAddMembers$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfileAddMembers.Model f$0;

    public /* synthetic */ NotificationProfileAddMembers$ViewHolder$$ExternalSyntheticLambda0(NotificationProfileAddMembers.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfileAddMembers.ViewHolder.m813bind$lambda0(this.f$0, view);
    }
}
