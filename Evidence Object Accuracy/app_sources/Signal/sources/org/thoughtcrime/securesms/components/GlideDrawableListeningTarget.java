package org.thoughtcrime.securesms.components;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class GlideDrawableListeningTarget extends DrawableImageViewTarget {
    private final SettableFuture<Boolean> loaded;

    public GlideDrawableListeningTarget(ImageView imageView, SettableFuture<Boolean> settableFuture) {
        super(imageView);
        this.loaded = settableFuture;
    }

    @Override // com.bumptech.glide.request.target.DrawableImageViewTarget
    public void setResource(Drawable drawable) {
        super.setResource(drawable);
        this.loaded.set(Boolean.TRUE);
    }

    @Override // com.bumptech.glide.request.target.ImageViewTarget, com.bumptech.glide.request.target.BaseTarget, com.bumptech.glide.request.target.Target
    public void onLoadFailed(Drawable drawable) {
        super.onLoadFailed(drawable);
        this.loaded.set(Boolean.TRUE);
    }
}
