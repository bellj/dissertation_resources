package org.thoughtcrime.securesms.components.webrtc.participantslist;

import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.function.LongConsumer;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import java.util.ArrayList;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsState;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallViewModel;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* loaded from: classes4.dex */
public class CallParticipantsListDialog extends BottomSheetDialogFragment {
    private CallParticipantsListAdapter adapter;
    private RecyclerView participantList;

    public static void show(FragmentManager fragmentManager) {
        new CallParticipantsListDialog().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    public static void dismiss(FragmentManager fragmentManager) {
        Fragment findFragmentByTag = fragmentManager.findFragmentByTag(BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        if (findFragmentByTag instanceof CallParticipantsListDialog) {
            ((CallParticipantsListDialog) findFragmentByTag).dismissAllowingStateLoss();
        }
    }

    @Override // androidx.fragment.app.DialogFragment
    public void show(FragmentManager fragmentManager, String str) {
        BottomSheetUtil.show(fragmentManager, str, this);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        setStyle(0, R.style.Theme_Signal_RoundedBottomSheet);
        super.onCreate(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        RecyclerView recyclerView = (RecyclerView) LayoutInflater.from(new ContextThemeWrapper(layoutInflater.getContext(), (int) R.style.TextSecure_DarkTheme)).inflate(R.layout.call_participants_list_dialog, viewGroup, false);
        this.participantList = recyclerView;
        return recyclerView;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        initializeList();
        ((WebRtcCallViewModel) ViewModelProviders.of(requireActivity()).get(WebRtcCallViewModel.class)).getCallParticipantsState().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.components.webrtc.participantslist.CallParticipantsListDialog$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CallParticipantsListDialog.this.updateList((CallParticipantsState) obj);
            }
        });
    }

    private void initializeList() {
        this.adapter = new CallParticipantsListAdapter();
        this.participantList.setLayoutManager(new LinearLayoutManager(requireContext()));
        this.participantList.setAdapter(this.adapter);
    }

    public void updateList(CallParticipantsState callParticipantsState) {
        ArrayList arrayList = new ArrayList();
        callParticipantsState.getParticipantCount().executeIfPresent(new LongConsumer(arrayList, callParticipantsState.getGroupCallState() == WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED, callParticipantsState) { // from class: org.thoughtcrime.securesms.components.webrtc.participantslist.CallParticipantsListDialog$$ExternalSyntheticLambda1
            public final /* synthetic */ List f$0;
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ CallParticipantsState f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // com.annimon.stream.function.LongConsumer
            public final void accept(long j) {
                CallParticipantsListDialog.lambda$updateList$0(this.f$0, this.f$1, this.f$2, j);
            }
        });
        this.adapter.submitList(arrayList);
    }

    public static /* synthetic */ void lambda$updateList$0(List list, boolean z, CallParticipantsState callParticipantsState, long j) {
        list.add(new CallParticipantsListHeader((int) j));
        if (z) {
            list.add(new CallParticipantViewState(callParticipantsState.getLocalParticipant()));
        }
        for (CallParticipant callParticipant : callParticipantsState.getAllRemoteParticipants()) {
            list.add(new CallParticipantViewState(callParticipant));
        }
    }
}
