package org.thoughtcrime.securesms.components.segmentedprogressbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.viewpager.widget.ViewPager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.IntIterator;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.segmentedprogressbar.Segment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;

/* compiled from: SegmentedProgressBar.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0014\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010$\n\u0002\b\u000e\n\u0002\u0010\u0007\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 n2\u00020\u00012\u00020\u00022\u00020\u0003:\u0001nB\u000f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006B\u0017\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tB\u001f\b\u0016\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\u000b¢\u0006\u0002\u0010\fJ\u0018\u0010N\u001a\u0002072\u0006\u0010O\u001a\u00020<2\u0006\u0010P\u001a\u00020\u0012H\u0002J\b\u0010Q\u001a\u00020RH\u0002J\u0018\u0010S\u001a\u00020R2\u0006\u0010T\u001a\u00020\u000b2\u0006\u0010U\u001a\u00020\u0010H\u0002J\u0006\u0010V\u001a\u00020RJ\u0012\u0010W\u001a\u00020R2\b\u0010X\u001a\u0004\u0018\u00010YH\u0014J\u0010\u0010Z\u001a\u00020R2\u0006\u0010[\u001a\u00020\u0012H\u0002J\u0010\u0010\\\u001a\u00020R2\u0006\u0010]\u001a\u00020\u000bH\u0016J \u0010^\u001a\u00020R2\u0006\u0010_\u001a\u00020\u000b2\u0006\u0010`\u001a\u0002072\u0006\u0010a\u001a\u00020\u000bH\u0016J\u0010\u0010b\u001a\u00020R2\u0006\u0010_\u001a\u00020\u000bH\u0016J\u001c\u0010c\u001a\u00020\u00102\b\u0010d\u001a\u0004\u0018\u00010\u00012\b\u0010e\u001a\u0004\u0018\u00010fH\u0016J\u0006\u0010g\u001a\u00020RJ\u0006\u0010h\u001a\u00020RJ\u0006\u0010i\u001a\u00020RJ\u0006\u0010j\u001a\u00020RJ\u000e\u0010k\u001a\u00020R2\u0006\u0010_\u001a\u00020\u000bJ\u000e\u0010l\u001a\u00020R2\u0006\u0010T\u001a\u00020\u000bJ\u0006\u0010m\u001a\u00020RR\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001e\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u000e\u0010\u001d\u001a\u00020\u001eX\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u001f\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001cR\u001e\u0010!\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001cR$\u0010$\u001a\u00020\u000b2\u0006\u0010#\u001a\u00020\u000b@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b%\u0010\u001c\"\u0004\b&\u0010'R<\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00120(2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00120(@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b*\u0010+\"\u0004\b,\u0010-R\u001e\u0010.\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b/\u0010\u001cR\u001e\u00100\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b1\u0010\u001cR\u001e\u00102\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b3\u0010\u001cR\u001e\u00104\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000b@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b5\u0010\u001cR\u0011\u00106\u001a\u0002078F¢\u0006\u0006\u001a\u0004\b8\u00109R\u0014\u0010:\u001a\b\u0012\u0004\u0012\u00020<0;X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010=\u001a\u0004\u0018\u00010<8BX\u0004¢\u0006\u0006\u001a\u0004\b>\u0010?R\u0014\u0010@\u001a\u00020\u000b8BX\u0004¢\u0006\u0006\u001a\u0004\bA\u0010\u001cR\u0011\u0010B\u001a\u00020\u00108F¢\u0006\u0006\u001a\u0004\bC\u0010DR\u001e\u0010E\u001a\u00020\u00122\u0006\u0010\u0019\u001a\u00020\u0012@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\bF\u0010GR(\u0010I\u001a\u0004\u0018\u00010H2\b\u0010#\u001a\u0004\u0018\u00010H@GX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\bJ\u0010K\"\u0004\bL\u0010M¨\u0006o"}, d2 = {"Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBar;", "Landroid/view/View;", "Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;", "Landroid/view/View$OnTouchListener;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "corners", "", "isPaused", "", "lastFrameTimeMillis", "", "listener", "Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBarListener;", "getListener", "()Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBarListener;", "setListener", "(Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBarListener;)V", "<set-?>", "margin", "getMargin", "()I", "path", "Landroid/graphics/Path;", "radius", "getRadius", "segmentBackgroundColor", "getSegmentBackgroundColor", DraftDatabase.DRAFT_VALUE, "segmentCount", "getSegmentCount", "setSegmentCount", "(I)V", "", "segmentDurations", "getSegmentDurations", "()Ljava/util/Map;", "setSegmentDurations", "(Ljava/util/Map;)V", "segmentSelectedBackgroundColor", "getSegmentSelectedBackgroundColor", "segmentSelectedStrokeColor", "getSegmentSelectedStrokeColor", "segmentStrokeColor", "getSegmentStrokeColor", "segmentStrokeWidth", "getSegmentStrokeWidth", "segmentWidth", "", "getSegmentWidth", "()F", "segments", "", "Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment;", "selectedSegment", "getSelectedSegment", "()Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment;", "selectedSegmentIndex", "getSelectedSegmentIndex", "strokeApplicable", "getStrokeApplicable", "()Z", "timePerSegmentMs", "getTimePerSegmentMs", "()J", "Landroidx/viewpager/widget/ViewPager;", "viewPager", "getViewPager", "()Landroidx/viewpager/widget/ViewPager;", "setViewPager", "(Landroidx/viewpager/widget/ViewPager;)V", "getSegmentProgressPercentage", "segment", "timeSinceLastFrameMillis", "initSegments", "", "loadSegment", "offset", "userAction", "next", "onDraw", "canvas", "Landroid/graphics/Canvas;", "onFrame", "frameTimeMillis", "onPageScrollStateChanged", "state", "onPageScrolled", "position", "positionOffset", "positionOffsetPixels", "onPageSelected", "onTouch", "p0", "p1", "Landroid/view/MotionEvent;", "pause", "previous", "reset", "restartSegment", "setPosition", "skip", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SegmentedProgressBar extends View implements ViewPager.OnPageChangeListener, View.OnTouchListener {
    public static final Companion Companion = new Companion(null);
    private static final long MILLIS_PER_FRAME = TimeUnit.MILLISECONDS.toMillis(17);
    private final float[] corners = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
    private boolean isPaused;
    private long lastFrameTimeMillis;
    private SegmentedProgressBarListener listener;
    private int margin = getResources().getDimensionPixelSize(R.dimen.segmentedprogressbar_default_segment_margin);
    private final Path path = new Path();
    private int radius = getResources().getDimensionPixelSize(R.dimen.segmentedprogressbar_default_corner_radius);
    private int segmentBackgroundColor = -1;
    private int segmentCount = getResources().getInteger(R.integer.segmentedprogressbar_default_segments_count);
    private Map<Integer, Long> segmentDurations = MapsKt__MapsKt.emptyMap();
    private int segmentSelectedBackgroundColor;
    private int segmentSelectedStrokeColor;
    private int segmentStrokeColor;
    private int segmentStrokeWidth = getResources().getDimensionPixelSize(R.dimen.segmentedprogressbar_default_segment_stroke_width);
    private List<Segment> segments;
    private long timePerSegmentMs;
    private ViewPager viewPager;

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageScrollStateChanged(int i) {
    }

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageScrolled(int i, float f, int i2) {
    }

    /* compiled from: SegmentedProgressBar.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBar$Companion;", "", "()V", "MILLIS_PER_FRAME", "", "getMILLIS_PER_FRAME", "()J", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final long getMILLIS_PER_FRAME() {
            return SegmentedProgressBar.MILLIS_PER_FRAME;
        }
    }

    public final int getSegmentCount() {
        return this.segmentCount;
    }

    public final void setSegmentCount(int i) {
        this.segmentCount = i;
        initSegments();
    }

    public final Map<Integer, Long> getSegmentDurations() {
        return this.segmentDurations;
    }

    public final void setSegmentDurations(Map<Integer, Long> map) {
        Intrinsics.checkNotNullParameter(map, DraftDatabase.DRAFT_VALUE);
        this.segmentDurations = map;
        initSegments();
    }

    public final int getMargin() {
        return this.margin;
    }

    public final int getRadius() {
        return this.radius;
    }

    public final int getSegmentStrokeWidth() {
        return this.segmentStrokeWidth;
    }

    public final int getSegmentBackgroundColor() {
        return this.segmentBackgroundColor;
    }

    public final int getSegmentSelectedBackgroundColor() {
        return this.segmentSelectedBackgroundColor;
    }

    public final int getSegmentStrokeColor() {
        return this.segmentStrokeColor;
    }

    public final int getSegmentSelectedStrokeColor() {
        return this.segmentSelectedStrokeColor;
    }

    public final long getTimePerSegmentMs() {
        return this.timePerSegmentMs;
    }

    private final Segment getSelectedSegment() {
        Object obj;
        boolean z;
        Iterator<T> it = this.segments.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((Segment) obj).getAnimationState() == Segment.AnimationState.ANIMATING) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        return (Segment) obj;
    }

    private final int getSelectedSegmentIndex() {
        return CollectionsKt___CollectionsKt.indexOf((List<? extends Segment>) ((List<? extends Object>) this.segments), getSelectedSegment());
    }

    public final boolean getStrokeApplicable() {
        return this.segmentStrokeWidth * 4 <= getMeasuredHeight();
    }

    public final float getSegmentWidth() {
        int measuredWidth = getMeasuredWidth();
        int i = this.margin;
        int i2 = this.segmentCount;
        return ((float) (measuredWidth - (i * (i2 - 1)))) / ((float) i2);
    }

    public final ViewPager getViewPager() {
        return this.viewPager;
    }

    public final void setViewPager(ViewPager viewPager) {
        this.viewPager = viewPager;
        if (viewPager == null) {
            if (viewPager != null) {
                viewPager.removeOnPageChangeListener(this);
            }
            ViewPager viewPager2 = this.viewPager;
            if (viewPager2 != null) {
                viewPager2.setOnTouchListener(null);
                return;
            }
            return;
        }
        if (viewPager != null) {
            viewPager.addOnPageChangeListener(this);
        }
        ViewPager viewPager3 = this.viewPager;
        if (viewPager3 != null) {
            viewPager3.setOnTouchListener(this);
        }
    }

    public final SegmentedProgressBarListener getListener() {
        return this.listener;
    }

    public final void setListener(SegmentedProgressBarListener segmentedProgressBarListener) {
        this.listener = segmentedProgressBarListener;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SegmentedProgressBar(Context context) {
        super(context);
        Intrinsics.checkNotNullParameter(context, "context");
        Context context2 = getContext();
        Intrinsics.checkNotNullExpressionValue(context2, "context");
        this.segmentSelectedBackgroundColor = UtilsKt.getThemeColor(context2, R.attr.colorAccent);
        this.segmentStrokeColor = -16777216;
        this.segmentSelectedStrokeColor = -16777216;
        this.timePerSegmentMs = (long) getResources().getInteger(R.integer.segmentedprogressbar_default_time_per_segment_ms);
        this.segments = new ArrayList();
        setLayerType(1, null);
        this.isPaused = true;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SegmentedProgressBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(attributeSet, "attrs");
        Context context2 = getContext();
        Intrinsics.checkNotNullExpressionValue(context2, "context");
        this.segmentSelectedBackgroundColor = UtilsKt.getThemeColor(context2, R.attr.colorAccent);
        this.segmentStrokeColor = -16777216;
        this.segmentSelectedStrokeColor = -16777216;
        this.timePerSegmentMs = (long) getResources().getInteger(R.integer.segmentedprogressbar_default_time_per_segment_ms);
        this.segments = new ArrayList();
        setLayerType(1, null);
        this.isPaused = true;
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.SegmentedProgressBar, 0, 0);
        Intrinsics.checkNotNullExpressionValue(obtainStyledAttributes, "context.theme.obtainStyl…gmentedProgressBar, 0, 0)");
        setSegmentCount(obtainStyledAttributes.getInt(8, this.segmentCount));
        this.margin = obtainStyledAttributes.getDimensionPixelSize(2, this.margin);
        this.radius = obtainStyledAttributes.getDimensionPixelSize(1, this.radius);
        this.segmentStrokeWidth = obtainStyledAttributes.getDimensionPixelSize(6, this.segmentStrokeWidth);
        this.segmentBackgroundColor = obtainStyledAttributes.getColor(0, this.segmentBackgroundColor);
        this.segmentSelectedBackgroundColor = obtainStyledAttributes.getColor(3, this.segmentSelectedBackgroundColor);
        this.segmentStrokeColor = obtainStyledAttributes.getColor(5, this.segmentStrokeColor);
        this.segmentSelectedStrokeColor = obtainStyledAttributes.getColor(4, this.segmentSelectedStrokeColor);
        this.timePerSegmentMs = (long) obtainStyledAttributes.getInt(7, (int) this.timePerSegmentMs);
        obtainStyledAttributes.recycle();
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SegmentedProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(attributeSet, "attrs");
        Context context2 = getContext();
        Intrinsics.checkNotNullExpressionValue(context2, "context");
        this.segmentSelectedBackgroundColor = UtilsKt.getThemeColor(context2, R.attr.colorAccent);
        this.segmentStrokeColor = -16777216;
        this.segmentSelectedStrokeColor = -16777216;
        this.timePerSegmentMs = (long) getResources().getInteger(R.integer.segmentedprogressbar_default_time_per_segment_ms);
        this.segments = new ArrayList();
        setLayerType(1, null);
        this.isPaused = true;
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int i = 0;
        for (Object obj : this.segments) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            Pair<List<RectF>, List<Paint>> drawingComponents = UtilsKt.getDrawingComponents(this, (Segment) obj, i);
            if (i == 0) {
                Iterator<Integer> it = ArraysKt___ArraysKt.getIndices(this.corners).iterator();
                while (it.hasNext()) {
                    this.corners[((IntIterator) it).nextInt()] = 0.0f;
                }
                float[] fArr = this.corners;
                int i3 = this.radius;
                fArr[0] = (float) i3;
                fArr[1] = (float) i3;
                fArr[6] = (float) i3;
                fArr[7] = (float) i3;
            } else if (i == CollectionsKt__CollectionsKt.getLastIndex(this.segments)) {
                Iterator<Integer> it2 = ArraysKt___ArraysKt.getIndices(this.corners).iterator();
                while (it2.hasNext()) {
                    this.corners[((IntIterator) it2).nextInt()] = 0.0f;
                }
                float[] fArr2 = this.corners;
                int i4 = this.radius;
                fArr2[2] = (float) i4;
                fArr2[3] = (float) i4;
                fArr2[4] = (float) i4;
                fArr2[5] = (float) i4;
            }
            int i5 = 0;
            for (Object obj2 : drawingComponents.getFirst()) {
                int i6 = i5 + 1;
                if (i5 < 0) {
                    CollectionsKt__CollectionsKt.throwIndexOverflow();
                }
                RectF rectF = (RectF) obj2;
                if (i == 0 || i == CollectionsKt__CollectionsKt.getLastIndex(this.segments)) {
                    this.path.reset();
                    this.path.addRoundRect(rectF, this.corners, Path.Direction.CW);
                    if (canvas != null) {
                        canvas.drawPath(this.path, drawingComponents.getSecond().get(i5));
                    }
                } else if (canvas != null) {
                    canvas.drawRect(rectF, drawingComponents.getSecond().get(i5));
                }
                i5 = i6;
            }
            i = i2;
        }
        onFrame(System.currentTimeMillis());
    }

    public final void start() {
        pause();
        if (getSelectedSegment() == null) {
            next();
            return;
        }
        this.isPaused = false;
        invalidate();
    }

    public final void pause() {
        this.isPaused = true;
        this.lastFrameTimeMillis = 0;
    }

    public final void reset() {
        List<Segment> list = this.segments;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (Segment segment : list) {
            segment.setAnimationState(Segment.AnimationState.IDLE);
            arrayList.add(Unit.INSTANCE);
        }
        invalidate();
    }

    public final void next() {
        loadSegment(1, true);
    }

    public final void previous() {
        loadSegment(-1, true);
    }

    public final void restartSegment() {
        loadSegment(0, true);
    }

    public final void skip(int i) {
        loadSegment(i, true);
    }

    public final void setPosition(int i) {
        loadSegment(i - getSelectedSegmentIndex(), true);
    }

    private final void loadSegment(int i, boolean z) {
        int i2 = CollectionsKt___CollectionsKt.indexOf((List<? extends Segment>) ((List<? extends Object>) this.segments), getSelectedSegment());
        int i3 = i2 + i;
        if (z) {
            if (!(i3 >= 0 && i3 < this.segmentCount)) {
                if (i3 >= this.segmentCount) {
                    SegmentedProgressBarListener segmentedProgressBarListener = this.listener;
                    if (segmentedProgressBarListener != null) {
                        segmentedProgressBarListener.onFinished();
                        return;
                    }
                    return;
                }
                loadSegment(0, false);
                return;
            }
        }
        List<Segment> list = this.segments;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        int i4 = 0;
        for (Object obj : list) {
            int i5 = i4 + 1;
            if (i4 < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            Segment segment = (Segment) obj;
            if (i > 0) {
                if (i4 < i3) {
                    segment.setAnimationState(Segment.AnimationState.ANIMATED);
                }
            } else if (i < 0) {
                if (i4 > i3 - 1) {
                    segment.setAnimationState(Segment.AnimationState.IDLE);
                }
            } else if (i == 0 && i4 == i3) {
                segment.setAnimationState(Segment.AnimationState.IDLE);
            }
            arrayList.add(Unit.INSTANCE);
            i4 = i5;
        }
        Segment segment2 = (Segment) CollectionsKt___CollectionsKt.getOrNull(this.segments, i3);
        if (segment2 != null) {
            pause();
            segment2.setAnimationState(Segment.AnimationState.ANIMATING);
            this.isPaused = false;
            invalidate();
            SegmentedProgressBarListener segmentedProgressBarListener2 = this.listener;
            if (segmentedProgressBarListener2 != null) {
                segmentedProgressBarListener2.onPage(i2, getSelectedSegmentIndex());
            }
            ViewPager viewPager = this.viewPager;
            if (viewPager != null) {
                viewPager.setCurrentItem(getSelectedSegmentIndex());
                return;
            }
            return;
        }
        pause();
        SegmentedProgressBarListener segmentedProgressBarListener3 = this.listener;
        if (segmentedProgressBarListener3 != null) {
            segmentedProgressBarListener3.onFinished();
        }
    }

    private final float getSegmentProgressPercentage(Segment segment, long j) {
        Float onRequestSegmentProgressPercentage;
        if (segment.getAnimationDurationMillis() > 0) {
            return segment.getAnimationProgressPercentage() + (((float) j) / ((float) segment.getAnimationDurationMillis()));
        }
        SegmentedProgressBarListener segmentedProgressBarListener = this.listener;
        if (segmentedProgressBarListener == null || (onRequestSegmentProgressPercentage = segmentedProgressBarListener.onRequestSegmentProgressPercentage()) == null) {
            return 0.0f;
        }
        return onRequestSegmentProgressPercentage.floatValue();
    }

    private final void initSegments() {
        this.segments.clear();
        List<Segment> list = this.segments;
        int i = this.segmentCount;
        ArrayList arrayList = new ArrayList(i);
        for (int i2 = 0; i2 < i; i2++) {
            Long l = this.segmentDurations.get(Integer.valueOf(i2));
            arrayList.add(new Segment(l != null ? l.longValue() : this.timePerSegmentMs));
        }
        list.addAll(arrayList);
        invalidate();
        reset();
    }

    private final void onFrame(long j) {
        if (!this.isPaused) {
            long j2 = this.lastFrameTimeMillis;
            this.lastFrameTimeMillis = j;
            Segment selectedSegment = getSelectedSegment();
            if (selectedSegment == null) {
                loadSegment(1, false);
            } else if (j2 > 0) {
                selectedSegment.setAnimationProgressPercentage(getSegmentProgressPercentage(selectedSegment, j - j2));
                if (selectedSegment.getAnimationProgressPercentage() >= 1.0f) {
                    loadSegment(1, false);
                } else {
                    invalidate();
                }
            } else {
                invalidate();
            }
        }
    }

    @Override // androidx.viewpager.widget.ViewPager.OnPageChangeListener
    public void onPageSelected(int i) {
        setPosition(i);
    }

    @Override // android.view.View.OnTouchListener
    public boolean onTouch(View view, MotionEvent motionEvent) {
        Integer valueOf = motionEvent != null ? Integer.valueOf(motionEvent.getAction()) : null;
        if (valueOf != null && valueOf.intValue() == 0) {
            pause();
            return false;
        } else if (valueOf == null || valueOf.intValue() != 1) {
            return false;
        } else {
            start();
            return false;
        }
    }
}
