package org.thoughtcrime.securesms.components.voice;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.widget.Toast;
import com.annimon.stream.Stream;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.MediaMetadata;
import com.google.android.exoplayer2.PlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.AudioAttributes;
import com.google.android.exoplayer2.device.DeviceInfo;
import com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector;
import com.google.android.exoplayer2.metadata.Metadata;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.video.VideoListener;
import com.google.android.exoplayer2.video.VideoSize;
import j$.util.Collection$EL;
import j$.util.function.Function;
import j$.util.function.Predicate;
import j$.util.stream.Collectors;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.NoSuchMessageException;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.MessageRecord;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public final class VoiceNotePlaybackPreparer implements MediaSessionConnector.PlaybackPreparer {
    private static final Executor EXECUTOR = Executors.newSingleThreadExecutor();
    private static final long LIMIT;
    private static final String TAG = Log.tag(VoiceNotePlaybackPreparer.class);
    private boolean canLoadMore;
    private final Context context;
    private Uri latestUri = Uri.EMPTY;
    private final Player player;
    private final VoiceNotePlaybackParameters voiceNotePlaybackParameters;

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.PlaybackPreparer
    public long getSupportedPrepareActions() {
        return 8192;
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.CommandReceiver
    public boolean onCommand(Player player, ControlDispatcher controlDispatcher, String str, Bundle bundle, ResultReceiver resultReceiver) {
        return false;
    }

    public VoiceNotePlaybackPreparer(Context context, Player player, VoiceNotePlaybackParameters voiceNotePlaybackParameters) {
        this.context = context;
        this.player = player;
        this.voiceNotePlaybackParameters = voiceNotePlaybackParameters;
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.PlaybackPreparer
    public void onPrepare(boolean z) {
        Log.w(TAG, "Requested playback from IDLE state. Ignoring.");
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.PlaybackPreparer
    public void onPrepareFromMediaId(String str, boolean z, Bundle bundle) {
        throw new UnsupportedOperationException("VoiceNotePlaybackPreparer does not support onPrepareFromMediaId");
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.PlaybackPreparer
    public void onPrepareFromSearch(String str, boolean z, Bundle bundle) {
        throw new UnsupportedOperationException("VoiceNotePlaybackPreparer does not support onPrepareFromSearch");
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.MediaSessionConnector.PlaybackPreparer
    public void onPrepareFromUri(Uri uri, boolean z, Bundle bundle) {
        String str = TAG;
        Log.d(str, "onPrepareFromUri: " + uri);
        if (bundle != null) {
            long j = bundle.getLong(VoiceNoteMediaController.EXTRA_MESSAGE_ID);
            long j2 = bundle.getLong(VoiceNoteMediaController.EXTRA_THREAD_ID);
            double d = bundle.getDouble(VoiceNoteMediaController.EXTRA_PROGRESS, 0.0d);
            boolean z2 = bundle.getBoolean(VoiceNoteMediaController.EXTRA_PLAY_SINGLE, false);
            this.canLoadMore = false;
            this.latestUri = uri;
            SimpleTask.run(EXECUTOR, new SimpleTask.BackgroundTask(z2, j, j2, uri) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda7
                public final /* synthetic */ boolean f$1;
                public final /* synthetic */ long f$2;
                public final /* synthetic */ long f$3;
                public final /* synthetic */ Uri f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r5;
                    this.f$4 = r7;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return VoiceNotePlaybackPreparer.$r8$lambda$niu1qibZ8_kyYI1A1XPdhI8nGCM(VoiceNotePlaybackPreparer.this, this.f$1, this.f$2, this.f$3, this.f$4);
                }
            }, new SimpleTask.ForegroundTask(uri, d, z2) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda8
                public final /* synthetic */ Uri f$1;
                public final /* synthetic */ double f$2;
                public final /* synthetic */ boolean f$3;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r5;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    VoiceNotePlaybackPreparer.$r8$lambda$DTSLVkqsz53pO3s6XfkksXCUB6I(VoiceNotePlaybackPreparer.this, this.f$1, this.f$2, this.f$3, (List) obj);
                }
            });
        }
    }

    public /* synthetic */ List lambda$onPrepareFromUri$0(boolean z, long j, long j2, Uri uri) {
        if (!z) {
            return lambda$loadMoreVoiceNotes$3(j);
        }
        if (j != -1) {
            return loadMediaItemsForSinglePlayback(j);
        }
        return loadMediaItemsForDraftPlayback(j2, uri);
    }

    public /* synthetic */ void lambda$onPrepareFromUri$2(Uri uri, final double d, boolean z, List list) {
        this.player.clearMediaItems();
        if (Util.hasItems(list) && Objects.equals(this.latestUri, uri)) {
            applyDescriptionsToQueue(list);
            final int max = Math.max(0, indexOfPlayerMediaItemByUri(uri));
            this.player.addListener((Player.Listener) new Player.Listener() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer.1
                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
                public /* bridge */ /* synthetic */ void onAudioAttributesChanged(AudioAttributes audioAttributes) {
                    Player.Listener.CC.$default$onAudioAttributesChanged(this, audioAttributes);
                }

                public /* bridge */ /* synthetic */ void onAudioSessionIdChanged(int i) {
                    Player.Listener.CC.$default$onAudioSessionIdChanged(this, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onAvailableCommandsChanged(Player.Commands commands) {
                    Player.Listener.CC.$default$onAvailableCommandsChanged(this, commands);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.text.TextOutput
                public /* bridge */ /* synthetic */ void onCues(List list2) {
                    Player.Listener.CC.$default$onCues(this, list2);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
                public /* bridge */ /* synthetic */ void onDeviceInfoChanged(DeviceInfo deviceInfo) {
                    Player.Listener.CC.$default$onDeviceInfoChanged(this, deviceInfo);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.device.DeviceListener
                public /* bridge */ /* synthetic */ void onDeviceVolumeChanged(int i, boolean z2) {
                    Player.Listener.CC.$default$onDeviceVolumeChanged(this, i, z2);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onEvents(Player player, Player.Events events) {
                    Player.Listener.CC.$default$onEvents(this, player, events);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onIsLoadingChanged(boolean z2) {
                    Player.Listener.CC.$default$onIsLoadingChanged(this, z2);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onIsPlayingChanged(boolean z2) {
                    Player.Listener.CC.$default$onIsPlayingChanged(this, z2);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                @Deprecated
                public /* bridge */ /* synthetic */ void onLoadingChanged(boolean z2) {
                    Player.EventListener.CC.$default$onLoadingChanged(this, z2);
                }

                public /* bridge */ /* synthetic */ void onMaxSeekToPreviousPositionChanged(int i) {
                    Player.EventListener.CC.$default$onMaxSeekToPreviousPositionChanged(this, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onMediaItemTransition(MediaItem mediaItem, int i) {
                    Player.Listener.CC.$default$onMediaItemTransition(this, mediaItem, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onMediaMetadataChanged(MediaMetadata mediaMetadata) {
                    Player.Listener.CC.$default$onMediaMetadataChanged(this, mediaMetadata);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.metadata.MetadataOutput
                public /* bridge */ /* synthetic */ void onMetadata(Metadata metadata) {
                    Player.Listener.CC.$default$onMetadata(this, metadata);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlayWhenReadyChanged(boolean z2, int i) {
                    Player.Listener.CC.$default$onPlayWhenReadyChanged(this, z2, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
                    Player.Listener.CC.$default$onPlaybackParametersChanged(this, playbackParameters);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlaybackStateChanged(int i) {
                    Player.Listener.CC.$default$onPlaybackStateChanged(this, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlaybackSuppressionReasonChanged(int i) {
                    Player.Listener.CC.$default$onPlaybackSuppressionReasonChanged(this, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlayerError(PlaybackException playbackException) {
                    Player.Listener.CC.$default$onPlayerError(this, playbackException);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlayerErrorChanged(PlaybackException playbackException) {
                    Player.Listener.CC.$default$onPlayerErrorChanged(this, playbackException);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                @Deprecated
                public /* bridge */ /* synthetic */ void onPlayerStateChanged(boolean z2, int i) {
                    Player.EventListener.CC.$default$onPlayerStateChanged(this, z2, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPlaylistMetadataChanged(MediaMetadata mediaMetadata) {
                    Player.Listener.CC.$default$onPlaylistMetadataChanged(this, mediaMetadata);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                @Deprecated
                public /* bridge */ /* synthetic */ void onPositionDiscontinuity(int i) {
                    Player.EventListener.CC.$default$onPositionDiscontinuity(this, i);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onPositionDiscontinuity(Player.PositionInfo positionInfo, Player.PositionInfo positionInfo2, int i) {
                    Player.Listener.CC.$default$onPositionDiscontinuity(this, positionInfo, positionInfo2, i);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
                public /* bridge */ /* synthetic */ void onRenderedFirstFrame() {
                    Player.Listener.CC.$default$onRenderedFirstFrame(this);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onRepeatModeChanged(int i) {
                    Player.Listener.CC.$default$onRepeatModeChanged(this, i);
                }

                public /* bridge */ /* synthetic */ void onSeekBackIncrementChanged(long j) {
                    Player.Listener.CC.$default$onSeekBackIncrementChanged(this, j);
                }

                public /* bridge */ /* synthetic */ void onSeekForwardIncrementChanged(long j) {
                    Player.Listener.CC.$default$onSeekForwardIncrementChanged(this, j);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                @Deprecated
                public /* bridge */ /* synthetic */ void onSeekProcessed() {
                    Player.EventListener.CC.$default$onSeekProcessed(this);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onShuffleModeEnabledChanged(boolean z2) {
                    Player.Listener.CC.$default$onShuffleModeEnabledChanged(this, z2);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
                public /* bridge */ /* synthetic */ void onSkipSilenceEnabledChanged(boolean z2) {
                    Player.Listener.CC.$default$onSkipSilenceEnabledChanged(this, z2);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                @Deprecated
                public /* bridge */ /* synthetic */ void onStaticMetadataChanged(List list2) {
                    Player.EventListener.CC.$default$onStaticMetadataChanged(this, list2);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
                public /* bridge */ /* synthetic */ void onSurfaceSizeChanged(int i, int i2) {
                    Player.Listener.CC.$default$onSurfaceSizeChanged(this, i, i2);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public /* bridge */ /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
                    Player.Listener.CC.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
                }

                @Override // com.google.android.exoplayer2.video.VideoListener
                @Deprecated
                public /* bridge */ /* synthetic */ void onVideoSizeChanged(int i, int i2, int i3, float f) {
                    VideoListener.CC.$default$onVideoSizeChanged(this, i, i2, i3, f);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.video.VideoListener
                public /* bridge */ /* synthetic */ void onVideoSizeChanged(VideoSize videoSize) {
                    Player.Listener.CC.$default$onVideoSizeChanged(this, videoSize);
                }

                @Override // com.google.android.exoplayer2.Player.Listener, com.google.android.exoplayer2.audio.AudioListener
                public /* bridge */ /* synthetic */ void onVolumeChanged(float f) {
                    Player.Listener.CC.$default$onVolumeChanged(this, f);
                }

                @Override // com.google.android.exoplayer2.Player.EventListener
                public void onTimelineChanged(Timeline timeline, int i) {
                    if (timeline.getWindowCount() >= max) {
                        VoiceNotePlaybackPreparer.this.player.setPlayWhenReady(false);
                        VoiceNotePlaybackPreparer.this.player.setPlaybackParameters(VoiceNotePlaybackPreparer.this.voiceNotePlaybackParameters.getParameters());
                        Player player = VoiceNotePlaybackPreparer.this.player;
                        int i2 = max;
                        double duration = (double) VoiceNotePlaybackPreparer.this.player.getDuration();
                        double d2 = d;
                        Double.isNaN(duration);
                        player.seekTo(i2, (long) (duration * d2));
                        VoiceNotePlaybackPreparer.this.player.setPlayWhenReady(true);
                        VoiceNotePlaybackPreparer.this.player.removeListener((Player.Listener) this);
                    }
                }
            });
            this.player.prepare();
            this.canLoadMore = !z;
        } else if (Objects.equals(this.latestUri, uri)) {
            Log.w(TAG, "Requested playback but no voice notes could be found.");
            ThreadUtil.postToMain(new Runnable() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda2
                @Override // java.lang.Runnable
                public final void run() {
                    VoiceNotePlaybackPreparer.m1264$r8$lambda$BLnZdNk1RylcGO1sGxbQnisMI(VoiceNotePlaybackPreparer.this);
                }
            });
        }
    }

    public /* synthetic */ void lambda$onPrepareFromUri$1() {
        Toast.makeText(this.context, (int) R.string.VoiceNotePlaybackPreparer__failed_to_play_voice_message, 0).show();
    }

    private void applyDescriptionsToQueue(List<MediaItem> list) {
        int i;
        MediaItem mediaItemAt;
        MediaItem.PlaybackProperties playbackProperties;
        for (MediaItem mediaItem : list) {
            MediaItem.PlaybackProperties playbackProperties2 = mediaItem.playbackProperties;
            if (playbackProperties2 != null) {
                int indexOfPlayerMediaItemByUri = indexOfPlayerMediaItemByUri(playbackProperties2.uri);
                MediaItem buildNextVoiceNoteMediaItem = VoiceNoteMediaItemFactory.buildNextVoiceNoteMediaItem(mediaItem);
                int currentWindowIndex = this.player.getCurrentWindowIndex();
                if (indexOfPlayerMediaItemByUri != -1) {
                    if (currentWindowIndex != indexOfPlayerMediaItemByUri) {
                        this.player.removeMediaItem(indexOfPlayerMediaItemByUri);
                        this.player.addMediaItem(indexOfPlayerMediaItemByUri, mediaItem);
                    }
                    int i2 = indexOfPlayerMediaItemByUri + 1;
                    if (currentWindowIndex != i2) {
                        if (this.player.getMediaItemCount() > 1) {
                            this.player.removeMediaItem(i2);
                        }
                        this.player.addMediaItem(i2, buildNextVoiceNoteMediaItem);
                    }
                } else {
                    int indexAfter = indexAfter(mediaItem);
                    this.player.addMediaItem(indexAfter, buildNextVoiceNoteMediaItem);
                    this.player.addMediaItem(indexAfter, mediaItem);
                }
            }
        }
        int mediaItemCount = this.player.getMediaItemCount();
        if (mediaItemCount > 0 && (playbackProperties = (mediaItemAt = this.player.getMediaItemAt((i = mediaItemCount - 1))).playbackProperties) != null && Objects.equals(playbackProperties.uri, VoiceNoteMediaItemFactory.NEXT_URI)) {
            this.player.removeMediaItem(i);
            if (this.player.getMediaItemCount() > 1) {
                this.player.addMediaItem(i, VoiceNoteMediaItemFactory.buildEndVoiceNoteMediaItem(mediaItemAt));
            }
        }
    }

    private int indexOfPlayerMediaItemByUri(Uri uri) {
        for (int i = 0; i < this.player.getMediaItemCount(); i++) {
            MediaItem.PlaybackProperties playbackProperties = this.player.getMediaItemAt(i).playbackProperties;
            if (playbackProperties != null && playbackProperties.uri.equals(uri)) {
                return i;
            }
        }
        return -1;
    }

    private int indexAfter(MediaItem mediaItem) {
        int mediaItemCount = this.player.getMediaItemCount();
        long j = mediaItem.mediaMetadata.extras.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_ID);
        for (int i = 0; i < mediaItemCount; i++) {
            if (this.player.getMediaItemAt(i).mediaMetadata.extras.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_ID) > j) {
                return i;
            }
        }
        return mediaItemCount;
    }

    public void loadMoreVoiceNotes() {
        MediaItem currentMediaItem;
        if (this.canLoadMore && (currentMediaItem = this.player.getCurrentMediaItem()) != null) {
            SimpleTask.run(EXECUTOR, new SimpleTask.BackgroundTask(currentMediaItem.mediaMetadata.extras.getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_ID)) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda5
                public final /* synthetic */ long f$1;

                {
                    this.f$1 = r2;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return VoiceNotePlaybackPreparer.$r8$lambda$51JOaQlep6EFj8dz24Ow9FCSwnM(VoiceNotePlaybackPreparer.this, this.f$1);
                }
            }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda6
                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    VoiceNotePlaybackPreparer.$r8$lambda$wlUCZwc6Bn_NKF1P2V8y74IrUMM(VoiceNotePlaybackPreparer.this, (List) obj);
                }
            });
        }
    }

    public /* synthetic */ void lambda$loadMoreVoiceNotes$4(List list) {
        if (Util.hasItems(list) && this.canLoadMore) {
            applyDescriptionsToQueue(list);
        }
    }

    private List<MediaItem> loadMediaItemsForSinglePlayback(long j) {
        try {
            MessageRecord messageRecord = SignalDatabase.mms().getMessageRecord(j);
            if (!MessageRecordUtil.hasAudio(messageRecord)) {
                Log.w(TAG, "Message does not contain audio.");
                return Collections.emptyList();
            }
            MediaItem buildMediaItem = VoiceNoteMediaItemFactory.buildMediaItem(this.context, messageRecord);
            if (buildMediaItem == null) {
                return Collections.emptyList();
            }
            return Collections.singletonList(buildMediaItem);
        } catch (NoSuchMessageException e) {
            Log.w(TAG, "Could not find message.", e);
            return Collections.emptyList();
        }
    }

    private List<MediaItem> loadMediaItemsForDraftPlayback(long j, Uri uri) {
        return Collections.singletonList(VoiceNoteMediaItemFactory.buildMediaItem(this.context, j, uri));
    }

    /* renamed from: loadMediaItemsForConsecutivePlayback */
    public List<MediaItem> lambda$loadMoreVoiceNotes$3(long j) {
        try {
            return (List) Collection$EL.stream(buildFilteredMessageRecordList(SignalDatabase.mmsSms().getMessagesAfterVoiceNoteInclusive(j, LIMIT))).map(new Function() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda3
                @Override // j$.util.function.Function
                public /* synthetic */ Function andThen(Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return VoiceNotePlaybackPreparer.m1265$r8$lambda$ClwpJhLkjLMyQkNAHRFr3P9hw(VoiceNotePlaybackPreparer.this, (MessageRecord) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ Function compose(Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda4
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return VoiceNotePlaybackPreparer$$ExternalSyntheticBackport0.m((MediaItem) obj);
                }
            }).collect(Collectors.toList());
        } catch (NoSuchMessageException e) {
            Log.w(TAG, "Could not find message.", e);
            return Collections.emptyList();
        }
    }

    public /* synthetic */ MediaItem lambda$loadMediaItemsForConsecutivePlayback$5(MessageRecord messageRecord) {
        return VoiceNoteMediaItemFactory.buildMediaItem(this.context, messageRecord);
    }

    private static List<MessageRecord> buildFilteredMessageRecordList(List<MessageRecord> list) {
        return Stream.of(list).takeWhile(new com.annimon.stream.function.Predicate() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackPreparer$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MessageRecordUtil.hasAudio((MessageRecord) obj);
            }
        }).toList();
    }
}
