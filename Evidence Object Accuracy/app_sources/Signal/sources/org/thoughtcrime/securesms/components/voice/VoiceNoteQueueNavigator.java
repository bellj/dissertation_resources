package org.thoughtcrime.securesms.components.voice;

import android.support.v4.media.MediaDescriptionCompat;
import android.support.v4.media.session.MediaSessionCompat;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator;

/* loaded from: classes4.dex */
final class VoiceNoteQueueNavigator extends TimelineQueueNavigator {
    private static final MediaDescriptionCompat EMPTY = new MediaDescriptionCompat.Builder().build();

    public VoiceNoteQueueNavigator(MediaSessionCompat mediaSessionCompat) {
        super(mediaSessionCompat);
    }

    @Override // com.google.android.exoplayer2.ext.mediasession.TimelineQueueNavigator
    public MediaDescriptionCompat getMediaDescription(Player player, int i) {
        MediaItem.PlaybackProperties playbackProperties;
        MediaItem mediaItemAt = (i < 0 || i >= player.getMediaItemCount()) ? null : player.getMediaItemAt(i);
        if (mediaItemAt == null || (playbackProperties = mediaItemAt.playbackProperties) == null) {
            return EMPTY;
        }
        MediaDescriptionCompat mediaDescriptionCompat = (MediaDescriptionCompat) playbackProperties.tag;
        if (mediaDescriptionCompat == null) {
            return EMPTY;
        }
        return mediaDescriptionCompat;
    }
}
