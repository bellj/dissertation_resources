package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SubscribeFragment$getConfiguration$1$5$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ SubscribeFragment f$0;

    public /* synthetic */ SubscribeFragment$getConfiguration$1$5$$ExternalSyntheticLambda0(SubscribeFragment subscribeFragment) {
        this.f$0 = subscribeFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        SubscribeFragment$getConfiguration$1.AnonymousClass5.m1042invoke$lambda0(this.f$0, dialogInterface, i);
    }
}
