package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "throwable", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$cancel$2 extends Lambda implements Function1<Throwable, Unit> {
    final /* synthetic */ SubscribeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SubscribeViewModel$cancel$2(SubscribeViewModel subscribeViewModel) {
        super(1);
        this.this$0 = subscribeViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    public final void invoke(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "throwable");
        this.this$0.eventPublisher.onNext(new DonationEvent.SubscriptionCancellationFailed(th));
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$cancel$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$cancel$2.m1061invoke$lambda0((SubscribeState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final SubscribeState m1061invoke$lambda0(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.READY, false, 95, null);
    }
}
