package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.app.ShareCompat$IntentBuilder;
import androidx.core.view.ViewKt;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.button.MaterialButton;
import java.io.ByteArrayOutputStream;
import java.util.Locale;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.concurrent.SimpleTask;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel;
import org.thoughtcrime.securesms.components.settings.models.SplashImage;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.payments.FiatMoneyUtil;
import org.thoughtcrime.securesms.providers.BlobProvider;
import org.thoughtcrime.securesms.util.DateUtils;
import org.thoughtcrime.securesms.util.MediaUtil;

/* compiled from: DonationReceiptDetailFragment.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00192\u00020\u0001:\u0001\u0019B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u001a\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0002J\u0010\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0018\u0010\u0018\u001a\u00020\f2\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "progressDialog", "Landroid/app/ProgressDialog;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "record", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "subscriptionName", "", "openShareSheet", "uri", "Landroid/net/Uri;", "renderPng", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptDetailFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    private static final int DONATION_RECEIPT_WIDTH;
    private static final String TAG = Log.tag(DonationReceiptDetailFragment.class);
    private ProgressDialog progressDialog;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(DonationReceiptDetailViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$viewModel$2
        final /* synthetic */ DonationReceiptDetailFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new DonationReceiptDetailViewModel.Factory(DonationReceiptDetailFragmentArgs.fromBundle(this.this$0.requireArguments()).getId(), new DonationReceiptDetailRepository());
        }
    });

    /* compiled from: DonationReceiptDetailFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DonationReceiptRecord.Type.values().length];
            iArr[DonationReceiptRecord.Type.RECURRING.ordinal()] = 1;
            iArr[DonationReceiptRecord.Type.BOOST.ordinal()] = 2;
            iArr[DonationReceiptRecord.Type.GIFT.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public DonationReceiptDetailFragment() {
        super(0, 0, R.layout.donation_receipt_detail_fragment, null, 11, null);
    }

    private final DonationReceiptDetailViewModel getViewModel() {
        return (DonationReceiptDetailViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        SplashImage.INSTANCE.register(dSLSettingsAdapter);
        View findViewById = requireView().findViewById(R.id.share_png);
        Intrinsics.checkNotNullExpressionValue(findViewById, "requireView().findViewById(R.id.share_png)");
        MaterialButton materialButton = (MaterialButton) findViewById;
        materialButton.setEnabled(false);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this, materialButton) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DonationReceiptDetailFragment f$1;
            public final /* synthetic */ MaterialButton f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DonationReceiptDetailFragment.m994$r8$lambda$mUt9iw4D2ZZi5CXRo1Zlc5CyCM(DSLSettingsAdapter.this, this.f$1, this.f$2, (DonationReceiptDetailState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m995bindAdapter$lambda1(DSLSettingsAdapter dSLSettingsAdapter, DonationReceiptDetailFragment donationReceiptDetailFragment, MaterialButton materialButton, DonationReceiptDetailState donationReceiptDetailState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(donationReceiptDetailFragment, "this$0");
        Intrinsics.checkNotNullParameter(materialButton, "$sharePngButton");
        if (donationReceiptDetailState.getDonationReceiptRecord() != null) {
            dSLSettingsAdapter.submitList(donationReceiptDetailFragment.getConfiguration(donationReceiptDetailState.getDonationReceiptRecord(), donationReceiptDetailState.getSubscriptionName()).toMappingModelList());
        }
        if (donationReceiptDetailState.getDonationReceiptRecord() != null && donationReceiptDetailState.getSubscriptionName() != null) {
            materialButton.setEnabled(true);
            materialButton.setOnClickListener(new View.OnClickListener(donationReceiptDetailState) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ DonationReceiptDetailState f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    DonationReceiptDetailFragment.$r8$lambda$fXp8vzPiyTjulm_3truf9lKFURo(DonationReceiptDetailFragment.this, this.f$1, view);
                }
            });
        }
    }

    /* renamed from: bindAdapter$lambda-1$lambda-0 */
    public static final void m996bindAdapter$lambda1$lambda0(DonationReceiptDetailFragment donationReceiptDetailFragment, DonationReceiptDetailState donationReceiptDetailState, View view) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailFragment, "this$0");
        donationReceiptDetailFragment.renderPng(donationReceiptDetailState.getDonationReceiptRecord(), donationReceiptDetailState.getSubscriptionName());
    }

    private final void renderPng(DonationReceiptRecord donationReceiptRecord, String str) {
        String str2;
        ProgressDialog progressDialog = new ProgressDialog(requireContext());
        this.progressDialog = progressDialog;
        progressDialog.show();
        String formatDateWithDayOfWeek = DateUtils.formatDateWithDayOfWeek(Locale.getDefault(), System.currentTimeMillis());
        Intrinsics.checkNotNullExpressionValue(formatDateWithDayOfWeek, "formatDateWithDayOfWeek(…stem.currentTimeMillis())");
        String format = FiatMoneyUtil.format(getResources(), donationReceiptRecord.getAmount());
        Intrinsics.checkNotNullExpressionValue(format, "format(resources, record.amount)");
        int i = WhenMappings.$EnumSwitchMapping$0[donationReceiptRecord.getType().ordinal()];
        if (i == 1) {
            str2 = getString(R.string.DonationReceiptDetailsFragment__s_dash_s, str, getString(R.string.DonationReceiptListFragment__recurring));
            Intrinsics.checkNotNullExpressionValue(str2, "getString(R.string.Donat…ListFragment__recurring))");
        } else if (i == 2) {
            str2 = getString(R.string.DonationReceiptListFragment__one_time);
            Intrinsics.checkNotNullExpressionValue(str2, "getString(R.string.Donat…ptListFragment__one_time)");
        } else if (i == 3) {
            str2 = getString(R.string.DonationReceiptListFragment__gift);
            Intrinsics.checkNotNullExpressionValue(str2, "getString(R.string.Donat…eceiptListFragment__gift)");
        } else {
            throw new NoWhenBranchMatchedException();
        }
        String formatDate = DateUtils.formatDate(Locale.getDefault(), donationReceiptRecord.getTimestamp());
        Intrinsics.checkNotNullExpressionValue(formatDate, "formatDate(Locale.getDefault(), record.timestamp)");
        SimpleTask.run(getViewLifecycleOwner().getLifecycle(), new SimpleTask.BackgroundTask(formatDateWithDayOfWeek, format, str2, formatDate) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ String f$3;
            public final /* synthetic */ String f$4;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return DonationReceiptDetailFragment.$r8$lambda$yio3CNbCw9KDup5oc9SOAbgLryY(DonationReceiptDetailFragment.this, this.f$1, this.f$2, this.f$3, this.f$4);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$$ExternalSyntheticLambda3
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                DonationReceiptDetailFragment.m993$r8$lambda$Gm_1RZLJmjxEZOWeHKfgtH8ttc(DonationReceiptDetailFragment.this, (Uri) obj);
            }
        });
    }

    /* renamed from: renderPng$lambda-2 */
    public static final Uri m997renderPng$lambda2(DonationReceiptDetailFragment donationReceiptDetailFragment, String str, String str2, String str3, String str4) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "$today");
        Intrinsics.checkNotNullParameter(str2, "$amount");
        Intrinsics.checkNotNullParameter(str3, "$type");
        Intrinsics.checkNotNullParameter(str4, "$datePaid");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        View inflate = LayoutInflater.from(donationReceiptDetailFragment.requireContext()).inflate(R.layout.donation_receipt_png, (ViewGroup) null);
        ((TextView) inflate.findViewById(R.id.date)).setText(str);
        ((TextView) inflate.findViewById(R.id.amount)).setText(str2);
        ((TextView) inflate.findViewById(R.id.donation_type)).setText(str3);
        ((TextView) inflate.findViewById(R.id.date_paid)).setText(str4);
        inflate.measure(View.MeasureSpec.makeMeasureSpec(DONATION_RECEIPT_WIDTH, 1073741824), View.MeasureSpec.makeMeasureSpec(0, 0));
        inflate.layout(0, 0, inflate.getMeasuredWidth(), inflate.getMeasuredHeight());
        Intrinsics.checkNotNullExpressionValue(inflate, "view");
        ViewKt.drawToBitmap$default(inflate, null, 1, null).compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return BlobProvider.getInstance().forData(byteArrayOutputStream.toByteArray()).withMimeType(MediaUtil.IMAGE_PNG).withFileName("Signal-Donation-Receipt.png").createForSingleSessionInMemory();
    }

    /* renamed from: renderPng$lambda-3 */
    public static final void m998renderPng$lambda3(DonationReceiptDetailFragment donationReceiptDetailFragment, Uri uri) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailFragment, "this$0");
        ProgressDialog progressDialog = donationReceiptDetailFragment.progressDialog;
        if (progressDialog == null) {
            Intrinsics.throwUninitializedPropertyAccessException("progressDialog");
            progressDialog = null;
        }
        progressDialog.dismiss();
        Intrinsics.checkNotNullExpressionValue(uri, "it");
        donationReceiptDetailFragment.openShareSheet(uri);
    }

    private final void openShareSheet(Uri uri) {
        Intent addFlags = new ShareCompat$IntentBuilder(requireContext()).setStream(uri).setType(Intent.normalizeMimeType(MediaUtil.IMAGE_PNG)).createChooserIntent().addFlags(1);
        Intrinsics.checkNotNullExpressionValue(addFlags, "IntentBuilder(requireCon…RANT_READ_URI_PERMISSION)");
        try {
            startActivity(addFlags);
        } catch (ActivityNotFoundException e) {
            Log.w(TAG, "No activity existed to share the media.", e);
            Toast.makeText(requireContext(), (int) R.string.MediaPreviewActivity_cant_find_an_app_able_to_share_this_media, 1).show();
        }
    }

    private final DSLConfiguration getConfiguration(DonationReceiptRecord donationReceiptRecord, String str) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, donationReceiptRecord, str) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailFragment$getConfiguration$1
            final /* synthetic */ DonationReceiptRecord $record;
            final /* synthetic */ String $subscriptionName;
            final /* synthetic */ DonationReceiptDetailFragment this$0;

            /* compiled from: DonationReceiptDetailFragment.kt */
            @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes4.dex */
            public /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[DonationReceiptRecord.Type.values().length];
                    iArr[DonationReceiptRecord.Type.RECURRING.ordinal()] = 1;
                    iArr[DonationReceiptRecord.Type.BOOST.ordinal()] = 2;
                    iArr[DonationReceiptRecord.Type.GIFT.ordinal()] = 3;
                    $EnumSwitchMapping$0 = iArr;
                }
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$record = r2;
                this.$subscriptionName = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                String str2;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new SplashImage.Model(R.drawable.ic_signal_logo_type, 0, 2, null));
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                String format = FiatMoneyUtil.format(this.this$0.getResources(), this.$record.getAmount());
                Intrinsics.checkNotNullExpressionValue(format, "format(resources, record.amount)");
                DSLConfiguration.textPref$default(dSLConfiguration, companion.from(format, new DSLSettingsText.TextAppearanceModifier(R.style.Signal_Text_Giant), DSLSettingsText.CenterModifier.INSTANCE), null, 2, null);
                dSLConfiguration.dividerPref();
                DSLSettingsText from = companion.from(R.string.DonationReceiptDetailsFragment__donation_type, new DSLSettingsText.Modifier[0]);
                int i = WhenMappings.$EnumSwitchMapping$0[this.$record.getType().ordinal()];
                if (i == 1) {
                    DonationReceiptDetailFragment donationReceiptDetailFragment = this.this$0;
                    str2 = donationReceiptDetailFragment.getString(R.string.DonationReceiptDetailsFragment__s_dash_s, this.$subscriptionName, donationReceiptDetailFragment.getString(R.string.DonationReceiptListFragment__recurring));
                } else if (i == 2) {
                    str2 = this.this$0.getString(R.string.DonationReceiptListFragment__one_time);
                } else if (i == 3) {
                    str2 = this.this$0.getString(R.string.DonationReceiptListFragment__gift);
                } else {
                    throw new NoWhenBranchMatchedException();
                }
                Intrinsics.checkNotNullExpressionValue(str2, "when (record.type) {\n   …agment__gift)\n          }");
                dSLConfiguration.textPref(from, companion.from(str2, new DSLSettingsText.Modifier[0]));
                DSLSettingsText from2 = companion.from(R.string.DonationReceiptDetailsFragment__date_paid, new DSLSettingsText.Modifier[0]);
                String formatDateWithYear = DateUtils.formatDateWithYear(Locale.getDefault(), this.$record.getTimestamp());
                Intrinsics.checkNotNullExpressionValue(formatDateWithYear, "formatDateWithYear(Local…tDefault(), it.timestamp)");
                dSLConfiguration.textPref(from2, companion.from(formatDateWithYear, new DSLSettingsText.Modifier[0]));
            }
        });
    }

    /* compiled from: DonationReceiptDetailFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailFragment$Companion;", "", "()V", "DONATION_RECEIPT_WIDTH", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
