package org.thoughtcrime.securesms.components.emoji.parsing;

import java.util.HashMap;
import java.util.Map;

/* loaded from: classes4.dex */
public class EmojiTree {
    private static final char TERMINATOR;
    private final EmojiTreeNode root = new EmojiTreeNode();

    public void add(String str, EmojiDrawInfo emojiDrawInfo) {
        EmojiTreeNode emojiTreeNode = this.root;
        char[] charArray = str.toCharArray();
        for (char c : charArray) {
            if (!emojiTreeNode.hasChild(c)) {
                emojiTreeNode.addChild(c);
            }
            emojiTreeNode = emojiTreeNode.getChild(c);
        }
        emojiTreeNode.setEmoji(emojiDrawInfo);
    }

    public Matches isEmoji(CharSequence charSequence, int i, int i2) {
        if (charSequence == null) {
            return Matches.POSSIBLY;
        }
        EmojiTreeNode emojiTreeNode = this.root;
        while (i < i2) {
            char charAt = charSequence.charAt(i);
            if (!emojiTreeNode.hasChild(charAt)) {
                return Matches.IMPOSSIBLE;
            }
            emojiTreeNode = emojiTreeNode.getChild(charAt);
            i++;
        }
        if (emojiTreeNode.isEndOfEmoji()) {
            return Matches.EXACTLY;
        }
        if (charSequence.charAt(i2 - 1) == 65039 || !emojiTreeNode.hasChild(TERMINATOR) || !emojiTreeNode.getChild(TERMINATOR).isEndOfEmoji()) {
            return Matches.POSSIBLY;
        }
        return Matches.EXACTLY;
    }

    public EmojiDrawInfo getEmoji(CharSequence charSequence, int i, int i2) {
        EmojiTreeNode emojiTreeNode = this.root;
        while (i < i2) {
            char charAt = charSequence.charAt(i);
            if (!emojiTreeNode.hasChild(charAt)) {
                return null;
            }
            emojiTreeNode = emojiTreeNode.getChild(charAt);
            i++;
        }
        if (emojiTreeNode.getEmoji() != null) {
            return emojiTreeNode.getEmoji();
        }
        if (charSequence.charAt(i2 - 1) == 65039 || !emojiTreeNode.hasChild(TERMINATOR)) {
            return null;
        }
        return emojiTreeNode.getChild(TERMINATOR).getEmoji();
    }

    /* loaded from: classes4.dex */
    public static class EmojiTreeNode {
        private Map<Character, EmojiTreeNode> children;
        private EmojiDrawInfo emoji;

        private EmojiTreeNode() {
            this.children = new HashMap();
        }

        public void setEmoji(EmojiDrawInfo emojiDrawInfo) {
            this.emoji = emojiDrawInfo;
        }

        public EmojiDrawInfo getEmoji() {
            return this.emoji;
        }

        boolean hasChild(char c) {
            return this.children.containsKey(Character.valueOf(c));
        }

        void addChild(char c) {
            this.children.put(Character.valueOf(c), new EmojiTreeNode());
        }

        EmojiTreeNode getChild(char c) {
            return this.children.get(Character.valueOf(c));
        }

        boolean isEndOfEmoji() {
            return this.emoji != null;
        }
    }

    /* loaded from: classes4.dex */
    public enum Matches {
        EXACTLY,
        POSSIBLY,
        IMPOSSIBLE;

        public boolean exactMatch() {
            return this == EXACTLY;
        }

        public boolean impossibleMatch() {
            return this == IMPOSSIBLE;
        }
    }
}
