package org.thoughtcrime.securesms.components.settings.conversation;

import io.reactivex.rxjava3.functions.Consumer;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda2 implements Consumer {
    public final /* synthetic */ ConversationSettingsViewModel.RecipientSettingsViewModel f$0;

    public /* synthetic */ ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda2(ConversationSettingsViewModel.RecipientSettingsViewModel recipientSettingsViewModel) {
        this.f$0 = recipientSettingsViewModel;
    }

    @Override // io.reactivex.rxjava3.functions.Consumer
    public final void accept(Object obj) {
        ConversationSettingsViewModel.RecipientSettingsViewModel.m1151_init_$lambda1(this.f$0, (StoryViewState) obj);
    }
}
