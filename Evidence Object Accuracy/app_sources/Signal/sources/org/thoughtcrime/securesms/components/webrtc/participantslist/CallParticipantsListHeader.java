package org.thoughtcrime.securesms.components.webrtc.participantslist;

import android.content.Context;
import android.content.res.Resources;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* loaded from: classes4.dex */
public class CallParticipantsListHeader implements MappingModel<CallParticipantsListHeader> {
    private int participantCount;

    public boolean areItemsTheSame(CallParticipantsListHeader callParticipantsListHeader) {
        return true;
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(CallParticipantsListHeader callParticipantsListHeader) {
        return MappingModel.CC.$default$getChangePayload(this, callParticipantsListHeader);
    }

    public CallParticipantsListHeader(int i) {
        this.participantCount = i;
    }

    public String getHeader(Context context) {
        Resources resources = context.getResources();
        int i = this.participantCount;
        return resources.getQuantityString(R.plurals.CallParticipantsListDialog_in_this_call_d_people, i, Integer.valueOf(i));
    }

    public boolean areContentsTheSame(CallParticipantsListHeader callParticipantsListHeader) {
        return this.participantCount == callParticipantsListHeader.participantCount;
    }
}
