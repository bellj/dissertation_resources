package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class EmojiImageView extends AppCompatImageView {
    private final boolean forceJumboEmoji;

    public EmojiImageView(Context context) {
        this(context, null);
    }

    public EmojiImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public EmojiImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.EmojiImageView, 0, 0);
        this.forceJumboEmoji = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
    }

    public void setImageEmoji(CharSequence charSequence) {
        if (isInEditMode()) {
            setImageResource(R.drawable.ic_emoji);
        } else {
            setImageDrawable(EmojiProvider.getEmojiDrawable(getContext(), charSequence, this.forceJumboEmoji));
        }
    }
}
