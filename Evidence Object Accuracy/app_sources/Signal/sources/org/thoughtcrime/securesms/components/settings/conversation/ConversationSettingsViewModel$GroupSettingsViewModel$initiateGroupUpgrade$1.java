package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsEvent;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ConversationSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "it", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ConversationSettingsViewModel$GroupSettingsViewModel$initiateGroupUpgrade$1 extends Lambda implements Function1<RecipientId, Unit> {
    final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ConversationSettingsViewModel$GroupSettingsViewModel$initiateGroupUpgrade$1(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel) {
        super(1);
        this.this$0 = groupSettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(RecipientId recipientId) {
        invoke(recipientId);
        return Unit.INSTANCE;
    }

    public final void invoke(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "it");
        this.this$0.getInternalEvents().postValue(new ConversationSettingsEvent.InitiateGroupMigration(recipientId));
    }
}
