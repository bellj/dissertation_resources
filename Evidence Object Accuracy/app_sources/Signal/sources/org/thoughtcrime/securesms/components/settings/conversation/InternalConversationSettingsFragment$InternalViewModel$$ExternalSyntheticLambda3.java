package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda3 implements Function {
    public final /* synthetic */ Recipient f$0;

    public /* synthetic */ InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda3(Recipient recipient) {
        this.f$0 = recipient;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return InternalConversationSettingsFragment.InternalViewModel.m1166onRecipientChanged$lambda3(this.f$0, (InternalConversationSettingsFragment.InternalState) obj);
    }
}
