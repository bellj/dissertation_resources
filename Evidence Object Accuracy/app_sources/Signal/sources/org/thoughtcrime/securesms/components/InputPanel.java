package org.thoughtcrime.securesms.components;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.Optional;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.components.InputPanel;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.components.LinkPreviewView;
import org.thoughtcrime.securesms.components.MicrophoneRecorderView;
import org.thoughtcrime.securesms.components.emoji.EmojiEventListener;
import org.thoughtcrime.securesms.components.emoji.EmojiToggle;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.conversation.ConversationStickerSuggestionAdapter;
import org.thoughtcrime.securesms.conversation.VoiceNoteDraftView;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.model.StickerRecord;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.linkpreview.LinkPreview;
import org.thoughtcrime.securesms.linkpreview.LinkPreviewRepository;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.AssertedSuccessListener;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class InputPanel extends LinearLayout implements MicrophoneRecorderView.Listener, KeyboardAwareLinearLayout.OnKeyboardShownListener, EmojiEventListener, ConversationStickerSuggestionAdapter.EventListener {
    private static final int FADE_TIME;
    private static final long QUOTE_REVEAL_DURATION_MILLIS;
    private static final String TAG = Log.tag(InputPanel.class);
    private AnimatingToggle buttonToggle;
    private ViewGroup composeContainer;
    private ComposeText composeText;
    private boolean emojiVisible;
    private boolean hideForBlockedState;
    private boolean hideForGroupState;
    private boolean hideForMessageRequestState;
    private boolean hideForSearch;
    private boolean hideForSelection;
    private LinkPreviewView linkPreview;
    private Listener listener;
    private EmojiToggle mediaKeyboard;
    private MicrophoneRecorderView microphoneRecorderView;
    private ImageButton quickAudioToggle;
    private ImageButton quickCameraToggle;
    private ValueAnimator quoteAnimator;
    private QuoteView quoteView;
    private View recordLockCancel;
    private RecordTime recordTime;
    private View recordingContainer;
    private SendButton sendButton;
    private SlideToCancel slideToCancel;
    private RecyclerView stickerSuggestion;
    private ConversationStickerSuggestionAdapter stickerSuggestionAdapter;
    private VoiceNoteDraftView voiceNoteDraftView;

    /* loaded from: classes4.dex */
    public interface Listener extends VoiceNoteDraftView.Listener {
        void onEmojiToggle();

        void onLinkPreviewCanceled();

        void onRecorderCanceled();

        void onRecorderFinished();

        void onRecorderLocked();

        void onRecorderPermissionRequired();

        void onRecorderStarted();

        void onStickerSuggestionSelected(StickerRecord stickerRecord);
    }

    /* loaded from: classes4.dex */
    public interface MediaListener {
        void onMediaSelected(Uri uri, String str);
    }

    public InputPanel(Context context) {
        super(context);
    }

    public InputPanel(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public InputPanel(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        View findViewById = findViewById(R.id.quote_dismiss);
        this.composeContainer = (ViewGroup) findViewById(R.id.compose_bubble);
        this.stickerSuggestion = (RecyclerView) findViewById(R.id.input_panel_sticker_suggestion);
        this.quoteView = (QuoteView) findViewById(R.id.quote_view);
        this.linkPreview = (LinkPreviewView) findViewById(R.id.link_preview);
        this.mediaKeyboard = (EmojiToggle) findViewById(R.id.emoji_toggle);
        this.composeText = (ComposeText) findViewById(R.id.embedded_text_editor);
        this.quickCameraToggle = (ImageButton) findViewById(R.id.quick_camera_toggle);
        this.quickAudioToggle = (ImageButton) findViewById(R.id.quick_audio_toggle);
        this.buttonToggle = (AnimatingToggle) findViewById(R.id.button_toggle);
        this.sendButton = (SendButton) findViewById(R.id.send_button);
        this.recordingContainer = findViewById(R.id.recording_container);
        this.recordLockCancel = findViewById(R.id.record_cancel);
        this.voiceNoteDraftView = (VoiceNoteDraftView) findViewById(R.id.voice_note_draft_view);
        this.slideToCancel = new SlideToCancel(findViewById(R.id.slide_to_cancel));
        MicrophoneRecorderView microphoneRecorderView = (MicrophoneRecorderView) findViewById(R.id.recorder_view);
        this.microphoneRecorderView = microphoneRecorderView;
        microphoneRecorderView.setListener(this);
        this.recordTime = new RecordTime((TextView) findViewById(R.id.record_time), findViewById(R.id.microphone), TimeUnit.HOURS.toSeconds(1), new Runnable() { // from class: org.thoughtcrime.securesms.components.InputPanel$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                InputPanel.this.lambda$onFinishInflate$0();
            }
        });
        this.recordLockCancel.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.InputPanel$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InputPanel.this.lambda$onFinishInflate$1(view);
            }
        });
        if (SignalStore.settings().isPreferSystemEmoji()) {
            this.mediaKeyboard.setVisibility(8);
            this.emojiVisible = false;
        } else {
            this.mediaKeyboard.setVisibility(0);
            this.emojiVisible = true;
        }
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.InputPanel$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InputPanel.this.lambda$onFinishInflate$2(view);
            }
        });
        this.linkPreview.setCloseClickedListener(new LinkPreviewView.CloseClickedListener() { // from class: org.thoughtcrime.securesms.components.InputPanel$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.components.LinkPreviewView.CloseClickedListener
            public final void onCloseClicked() {
                InputPanel.this.lambda$onFinishInflate$3();
            }
        });
        this.stickerSuggestionAdapter = new ConversationStickerSuggestionAdapter(GlideApp.with(this), this);
        this.stickerSuggestion.setLayoutManager(new LinearLayoutManager(getContext(), 0, false));
        this.stickerSuggestion.setAdapter(this.stickerSuggestionAdapter);
    }

    public /* synthetic */ void lambda$onFinishInflate$0() {
        this.microphoneRecorderView.cancelAction();
    }

    public /* synthetic */ void lambda$onFinishInflate$1(View view) {
        this.microphoneRecorderView.cancelAction();
    }

    public /* synthetic */ void lambda$onFinishInflate$2(View view) {
        clearQuote();
    }

    public /* synthetic */ void lambda$onFinishInflate$3() {
        Listener listener = this.listener;
        if (listener != null) {
            listener.onLinkPreviewCanceled();
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
        this.mediaKeyboard.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.InputPanel$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                InputPanel.Listener.this.onEmojiToggle();
            }
        });
        this.voiceNoteDraftView.setListener(listener);
    }

    public void setMediaListener(MediaListener mediaListener) {
        this.composeText.setMediaListener(mediaListener);
    }

    public void setQuote(GlideRequests glideRequests, long j, Recipient recipient, CharSequence charSequence, SlideDeck slideDeck, QuoteModel.Type type) {
        this.quoteView.setQuote(glideRequests, j, recipient, charSequence, false, slideDeck, null, type);
        int measuredHeight = this.quoteView.getVisibility() == 0 ? this.quoteView.getMeasuredHeight() : 0;
        this.quoteView.setVisibility(0);
        int width = this.composeContainer.getWidth();
        if (this.quoteView.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) this.quoteView.getLayoutParams();
            width -= marginLayoutParams.leftMargin + marginLayoutParams.rightMargin;
        }
        this.quoteView.measure(View.MeasureSpec.makeMeasureSpec(width, Integer.MIN_VALUE), 0);
        ValueAnimator valueAnimator = this.quoteAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        QuoteView quoteView = this.quoteView;
        ValueAnimator createHeightAnimator = createHeightAnimator(quoteView, measuredHeight, quoteView.getMeasuredHeight(), null);
        this.quoteAnimator = createHeightAnimator;
        createHeightAnimator.start();
        if (this.linkPreview.getVisibility() == 0) {
            int readDimen = readDimen(R.dimen.message_corner_collapse_radius);
            this.linkPreview.setCorners(readDimen, readDimen);
        }
    }

    public void clearQuote() {
        ValueAnimator valueAnimator = this.quoteAnimator;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        QuoteView quoteView = this.quoteView;
        ValueAnimator createHeightAnimator = createHeightAnimator(quoteView, quoteView.getMeasuredHeight(), 0, new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.InputPanel.1
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                InputPanel.this.quoteView.dismiss();
                if (InputPanel.this.linkPreview.getVisibility() == 0) {
                    int readDimen = InputPanel.this.readDimen(R.dimen.message_corner_radius);
                    InputPanel.this.linkPreview.setCorners(readDimen, readDimen);
                }
            }
        });
        this.quoteAnimator = createHeightAnimator;
        createHeightAnimator.start();
    }

    private static ValueAnimator createHeightAnimator(View view, int i, int i2, AnimationCompleteListener animationCompleteListener) {
        ValueAnimator duration = ValueAnimator.ofInt(i, i2).setDuration(QUOTE_REVEAL_DURATION_MILLIS);
        duration.addUpdateListener(new ValueAnimator.AnimatorUpdateListener(view) { // from class: org.thoughtcrime.securesms.components.InputPanel$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.animation.ValueAnimator.AnimatorUpdateListener
            public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                InputPanel.lambda$createHeightAnimator$5(this.f$0, valueAnimator);
            }
        });
        if (animationCompleteListener != null) {
            duration.addListener(animationCompleteListener);
        }
        return duration;
    }

    public static /* synthetic */ void lambda$createHeightAnimator$5(View view, ValueAnimator valueAnimator) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = ((Integer) valueAnimator.getAnimatedValue()).intValue();
        view.setLayoutParams(layoutParams);
    }

    public boolean hasSaveableContent() {
        return getQuote().isPresent() || this.voiceNoteDraftView.getDraft() != null;
    }

    public Optional<QuoteModel> getQuote() {
        if (this.quoteView.getQuoteId() <= 0 || this.quoteView.getVisibility() != 0) {
            return Optional.empty();
        }
        return Optional.of(new QuoteModel(this.quoteView.getQuoteId(), this.quoteView.getAuthor().getId(), this.quoteView.getBody().toString(), false, this.quoteView.getAttachments(), this.quoteView.getMentions(), this.quoteView.getQuoteType()));
    }

    public void setLinkPreviewLoading() {
        this.linkPreview.setVisibility(0);
        this.linkPreview.setLoading();
    }

    public void setLinkPreviewNoPreview(LinkPreviewRepository.Error error) {
        this.linkPreview.setVisibility(0);
        this.linkPreview.setNoPreview(error);
    }

    public void setLinkPreview(GlideRequests glideRequests, Optional<LinkPreview> optional) {
        int i;
        if (optional.isPresent()) {
            this.linkPreview.setVisibility(0);
            this.linkPreview.setLinkPreview(glideRequests, optional.get(), true);
        } else {
            this.linkPreview.setVisibility(8);
        }
        if (this.quoteView.getVisibility() == 0) {
            i = readDimen(R.dimen.message_corner_collapse_radius);
        } else {
            i = readDimen(R.dimen.message_corner_radius);
        }
        this.linkPreview.setCorners(i, i);
    }

    public void clickOnComposeInput() {
        this.composeText.performClick();
    }

    public void setMediaKeyboard(MediaKeyboard mediaKeyboard) {
        this.mediaKeyboard.attach(mediaKeyboard);
    }

    public void setStickerSuggestions(List<StickerRecord> list) {
        this.stickerSuggestion.setVisibility(list.isEmpty() ? 8 : 0);
        this.stickerSuggestionAdapter.setStickers(list);
    }

    public void showMediaKeyboardToggle(boolean z) {
        this.emojiVisible = z;
        this.mediaKeyboard.setVisibility(z ? 0 : 8);
    }

    public void setMediaKeyboardToggleMode(KeyboardPage keyboardPage) {
        this.mediaKeyboard.setStickerMode(keyboardPage);
    }

    public boolean isStickerMode() {
        return this.mediaKeyboard.isStickerMode();
    }

    public View getMediaKeyboardToggleAnchorView() {
        return this.mediaKeyboard;
    }

    public MediaKeyboard.MediaKeyboardListener getMediaKeyboardListener() {
        return this.mediaKeyboard;
    }

    public void setWallpaperEnabled(boolean z) {
        int i;
        int i2;
        int i3;
        if (z) {
            i = getContext().getResources().getColor(R.color.signal_colorOnSurface);
            i2 = getContext().getResources().getColor(R.color.signal_colorOnSurface);
            i3 = getContext().getResources().getColor(R.color.signal_colorOnSurfaceVariant);
            setBackground(null);
            ViewGroup viewGroup = this.composeContainer;
            Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.compose_background_wallpaper);
            Objects.requireNonNull(drawable);
            viewGroup.setBackground(drawable);
            this.quickAudioToggle.setColorFilter(i);
            this.quickCameraToggle.setColorFilter(i);
        } else {
            i = getContext().getResources().getColor(R.color.signal_colorOnSurface);
            i2 = getContext().getResources().getColor(R.color.signal_colorOnSurface);
            i3 = getContext().getResources().getColor(R.color.signal_colorOnSurfaceVariant);
            setBackground(new ColorDrawable(getContext().getResources().getColor(R.color.signal_colorSurface)));
            ViewGroup viewGroup2 = this.composeContainer;
            Drawable drawable2 = ContextCompat.getDrawable(getContext(), R.drawable.compose_background);
            Objects.requireNonNull(drawable2);
            viewGroup2.setBackground(drawable2);
        }
        this.mediaKeyboard.setColorFilter(i);
        this.quickAudioToggle.setColorFilter(i);
        this.quickCameraToggle.setColorFilter(i);
        this.composeText.setTextColor(i2);
        this.composeText.setHintTextColor(i3);
        this.quoteView.setWallpaperEnabled(z);
    }

    public void setHideForMessageRequestState(boolean z) {
        this.hideForMessageRequestState = z;
        updateVisibility();
    }

    public void setHideForGroupState(boolean z) {
        this.hideForGroupState = z;
        updateVisibility();
    }

    public void setHideForBlockedState(boolean z) {
        this.hideForBlockedState = z;
        updateVisibility();
    }

    public void setHideForSearch(boolean z) {
        this.hideForSearch = z;
        updateVisibility();
    }

    public void setHideForSelection(boolean z) {
        this.hideForSelection = z;
        updateVisibility();
    }

    @Override // org.thoughtcrime.securesms.components.MicrophoneRecorderView.Listener
    public void onRecordPermissionRequired() {
        Listener listener = this.listener;
        if (listener != null) {
            listener.onRecorderPermissionRequired();
        }
    }

    @Override // org.thoughtcrime.securesms.components.MicrophoneRecorderView.Listener
    public void onRecordPressed() {
        Listener listener = this.listener;
        if (listener != null) {
            listener.onRecorderStarted();
        }
        this.recordTime.display();
        this.slideToCancel.display();
        if (this.emojiVisible) {
            fadeOut(this.mediaKeyboard);
        }
        fadeOut(this.composeText);
        fadeOut(this.quickCameraToggle);
        fadeOut(this.quickAudioToggle);
        fadeOut(this.buttonToggle);
    }

    @Override // org.thoughtcrime.securesms.components.MicrophoneRecorderView.Listener
    public void onRecordReleased() {
        long onRecordHideEvent = onRecordHideEvent();
        if (this.listener != null) {
            String str = TAG;
            Log.d(str, "Elapsed time: " + onRecordHideEvent);
            if (onRecordHideEvent > 1000) {
                this.listener.onRecorderFinished();
                return;
            }
            Toast.makeText(getContext(), (int) R.string.InputPanel_tap_and_hold_to_record_a_voice_message_release_to_send, 1).show();
            this.listener.onRecorderCanceled();
        }
    }

    @Override // org.thoughtcrime.securesms.components.MicrophoneRecorderView.Listener
    public void onRecordMoved(float f, float f2) {
        this.slideToCancel.moveTo(f);
        float width = f2 / ((float) this.recordingContainer.getWidth());
        if ((ViewUtil.isLtr(this) && ((double) width) <= 0.5d) || (ViewUtil.isRtl(this) && ((double) width) >= 0.6d)) {
            this.microphoneRecorderView.cancelAction();
        }
    }

    @Override // org.thoughtcrime.securesms.components.MicrophoneRecorderView.Listener
    public void onRecordCanceled() {
        onRecordHideEvent();
        Listener listener = this.listener;
        if (listener != null) {
            listener.onRecorderCanceled();
        }
    }

    @Override // org.thoughtcrime.securesms.components.MicrophoneRecorderView.Listener
    public void onRecordLocked() {
        this.slideToCancel.hide();
        this.recordLockCancel.setVisibility(0);
        fadeIn(this.buttonToggle);
        Listener listener = this.listener;
        if (listener != null) {
            listener.onRecorderLocked();
        }
    }

    public void onPause() {
        this.microphoneRecorderView.cancelAction();
    }

    public Observer<VoiceNotePlaybackState> getPlaybackStateObserver() {
        return this.voiceNoteDraftView.getPlaybackStateObserver();
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        this.composeText.setEnabled(z);
        this.mediaKeyboard.setEnabled(z);
        this.quickAudioToggle.setEnabled(z);
        this.quickCameraToggle.setEnabled(z);
    }

    private long onRecordHideEvent() {
        this.recordLockCancel.setVisibility(8);
        ListenableFuture<Void> hide = this.slideToCancel.hide();
        long hide2 = this.recordTime.hide();
        hide.addListener(new AssertedSuccessListener<Void>() { // from class: org.thoughtcrime.securesms.components.InputPanel.2
            public void onSuccess(Void r1) {
                InputPanel.this.fadeInNormalComposeViews();
            }
        });
        return hide2;
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
    public void onKeyboardShown() {
        this.mediaKeyboard.setToMedia();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onKeyEvent(KeyEvent keyEvent) {
        this.composeText.dispatchKeyEvent(keyEvent);
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiEventListener
    public void onEmojiSelected(String str) {
        this.composeText.insertEmoji(str);
    }

    @Override // org.thoughtcrime.securesms.conversation.ConversationStickerSuggestionAdapter.EventListener
    public void onStickerSuggestionClicked(StickerRecord stickerRecord) {
        Listener listener = this.listener;
        if (listener != null) {
            listener.onStickerSuggestionSelected(stickerRecord);
        }
    }

    public int readDimen(int i) {
        return getResources().getDimensionPixelSize(i);
    }

    public boolean isRecordingInLockedMode() {
        return this.microphoneRecorderView.isRecordingLocked();
    }

    public void releaseRecordingLock() {
        this.microphoneRecorderView.unlockAction();
    }

    public void setVoiceNoteDraft(DraftDatabase.Draft draft) {
        if (draft != null) {
            this.voiceNoteDraftView.setDraft(draft);
            this.voiceNoteDraftView.setVisibility(0);
            hideNormalComposeViews();
            this.buttonToggle.displayQuick(this.sendButton);
            return;
        }
        this.voiceNoteDraftView.clearDraft();
        ViewUtil.fadeOut(this.voiceNoteDraftView, FADE_TIME);
        fadeInNormalComposeViews();
    }

    public DraftDatabase.Draft getVoiceNoteDraft() {
        return this.voiceNoteDraftView.getDraft();
    }

    private void hideNormalComposeViews() {
        if (this.emojiVisible) {
            this.mediaKeyboard.animate().cancel();
            this.mediaKeyboard.setAlpha(0.0f);
        }
        for (View view : Arrays.asList(this.composeText, this.quickCameraToggle, this.quickAudioToggle)) {
            view.animate().cancel();
            view.setAlpha(0.0f);
        }
    }

    public void fadeInNormalComposeViews() {
        if (this.emojiVisible) {
            fadeIn(this.mediaKeyboard);
        }
        fadeIn(this.composeText);
        fadeIn(this.quickCameraToggle);
        fadeIn(this.quickAudioToggle);
        fadeIn(this.buttonToggle);
    }

    private void fadeIn(View view) {
        view.animate().alpha(1.0f).setDuration(QUOTE_REVEAL_DURATION_MILLIS).start();
    }

    private void fadeOut(View view) {
        view.animate().alpha(0.0f).setDuration(QUOTE_REVEAL_DURATION_MILLIS).start();
    }

    private void updateVisibility() {
        if (this.hideForGroupState || this.hideForBlockedState || this.hideForSearch || this.hideForSelection || this.hideForMessageRequestState) {
            setVisibility(8);
        } else {
            setVisibility(0);
        }
    }

    /* loaded from: classes4.dex */
    public static class SlideToCancel {
        private final View slideToCancelView;

        SlideToCancel(View view) {
            this.slideToCancelView = view;
        }

        public void display() {
            ViewUtil.fadeIn(this.slideToCancelView, InputPanel.FADE_TIME);
        }

        public ListenableFuture<Void> hide() {
            SettableFuture settableFuture = new SettableFuture();
            AnimationSet animationSet = new AnimationSet(true);
            animationSet.addAnimation(new TranslateAnimation(0, this.slideToCancelView.getTranslationX(), 0, 0.0f, 1, 0.0f, 1, 0.0f));
            animationSet.addAnimation(new AlphaAnimation(1.0f, 0.0f));
            animationSet.setDuration(200);
            animationSet.setFillBefore(true);
            animationSet.setFillAfter(false);
            this.slideToCancelView.postDelayed(new InputPanel$SlideToCancel$$ExternalSyntheticLambda0(settableFuture), 200);
            this.slideToCancelView.setVisibility(8);
            this.slideToCancelView.startAnimation(animationSet);
            return settableFuture;
        }

        void moveTo(float f) {
            TranslateAnimation translateAnimation = new TranslateAnimation(0, f, 0, f, 1, 0.0f, 1, 0.0f);
            translateAnimation.setDuration(0);
            translateAnimation.setFillAfter(true);
            translateAnimation.setFillBefore(true);
            this.slideToCancelView.startAnimation(translateAnimation);
        }
    }

    /* loaded from: classes4.dex */
    public static class RecordTime implements Runnable {
        private final long limitSeconds;
        private final View microphone;
        private final Runnable onLimitHit;
        private final TextView recordTimeView;
        private long startTime;

        private RecordTime(TextView textView, View view, long j, Runnable runnable) {
            this.recordTimeView = textView;
            this.microphone = view;
            this.limitSeconds = j;
            this.onLimitHit = runnable;
        }

        public void display() {
            this.startTime = System.currentTimeMillis();
            this.recordTimeView.setText(DateUtils.formatElapsedTime(0));
            ViewUtil.fadeIn(this.recordTimeView, InputPanel.FADE_TIME);
            ThreadUtil.runOnMainDelayed(this, TimeUnit.SECONDS.toMillis(1));
            this.microphone.setVisibility(0);
            this.microphone.startAnimation(pulseAnimation());
        }

        public long hide() {
            long currentTimeMillis = System.currentTimeMillis() - this.startTime;
            this.startTime = 0;
            ViewUtil.fadeOut(this.recordTimeView, InputPanel.FADE_TIME, 4);
            this.microphone.clearAnimation();
            ViewUtil.fadeOut(this.microphone, InputPanel.FADE_TIME, 4);
            return currentTimeMillis;
        }

        @Override // java.lang.Runnable
        public void run() {
            long j = this.startTime;
            if (j > 0) {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - j);
                if (seconds >= this.limitSeconds) {
                    this.onLimitHit.run();
                    return;
                }
                this.recordTimeView.setText(DateUtils.formatElapsedTime(seconds));
                ThreadUtil.runOnMainDelayed(this, TimeUnit.SECONDS.toMillis(1));
            }
        }

        private static Animation pulseAnimation() {
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setInterpolator(pulseInterpolator());
            alphaAnimation.setRepeatCount(-1);
            alphaAnimation.setDuration(1000);
            return alphaAnimation;
        }

        private static Interpolator pulseInterpolator() {
            return new InputPanel$RecordTime$$ExternalSyntheticLambda0();
        }

        public static /* synthetic */ float lambda$pulseInterpolator$0(float f) {
            float f2 = f * 5.0f;
            if (f2 > 1.0f) {
                f2 = 4.0f - f2;
            }
            return Math.max(0.0f, Math.min(1.0f, f2));
        }
    }
}
