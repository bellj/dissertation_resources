package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail;

import android.os.Bundle;
import java.util.HashMap;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* loaded from: classes4.dex */
public class DonationReceiptDetailFragmentArgs {
    private final HashMap arguments;

    private DonationReceiptDetailFragmentArgs() {
        this.arguments = new HashMap();
    }

    private DonationReceiptDetailFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static DonationReceiptDetailFragmentArgs fromBundle(Bundle bundle) {
        DonationReceiptDetailFragmentArgs donationReceiptDetailFragmentArgs = new DonationReceiptDetailFragmentArgs();
        bundle.setClassLoader(DonationReceiptDetailFragmentArgs.class.getClassLoader());
        if (bundle.containsKey(ContactRepository.ID_COLUMN)) {
            donationReceiptDetailFragmentArgs.arguments.put(ContactRepository.ID_COLUMN, Long.valueOf(bundle.getLong(ContactRepository.ID_COLUMN)));
            return donationReceiptDetailFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"id\" is missing and does not have an android:defaultValue");
    }

    public long getId() {
        return ((Long) this.arguments.get(ContactRepository.ID_COLUMN)).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey(ContactRepository.ID_COLUMN)) {
            bundle.putLong(ContactRepository.ID_COLUMN, ((Long) this.arguments.get(ContactRepository.ID_COLUMN)).longValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        DonationReceiptDetailFragmentArgs donationReceiptDetailFragmentArgs = (DonationReceiptDetailFragmentArgs) obj;
        return this.arguments.containsKey(ContactRepository.ID_COLUMN) == donationReceiptDetailFragmentArgs.arguments.containsKey(ContactRepository.ID_COLUMN) && getId() == donationReceiptDetailFragmentArgs.getId();
    }

    public int hashCode() {
        return 31 + ((int) (getId() ^ (getId() >>> 32)));
    }

    public String toString() {
        return "DonationReceiptDetailFragmentArgs{id=" + getId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(DonationReceiptDetailFragmentArgs donationReceiptDetailFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(donationReceiptDetailFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put(ContactRepository.ID_COLUMN, Long.valueOf(j));
        }

        public DonationReceiptDetailFragmentArgs build() {
            return new DonationReceiptDetailFragmentArgs(this.arguments);
        }

        public Builder setId(long j) {
            this.arguments.put(ContactRepository.ID_COLUMN, Long.valueOf(j));
            return this;
        }

        public long getId() {
            return ((Long) this.arguments.get(ContactRepository.ID_COLUMN)).longValue();
        }
    }
}
