package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$4$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ List f$0;

    public /* synthetic */ ConversationSettingsViewModel$RecipientSettingsViewModel$4$$ExternalSyntheticLambda0(List list) {
        this.f$0 = list;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.RecipientSettingsViewModel.AnonymousClass4.m1157invoke$lambda0(this.f$0, (ConversationSettingsState) obj);
    }
}
