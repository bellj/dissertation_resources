package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState;

/* compiled from: ManageDonationsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "throwable", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ManageDonationsViewModel$refresh$3 extends Lambda implements Function1<Throwable, Unit> {
    final /* synthetic */ ManageDonationsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ManageDonationsViewModel$refresh$3(ManageDonationsViewModel manageDonationsViewModel) {
        super(1);
        this.this$0 = manageDonationsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    public final void invoke(Throwable th) {
        Intrinsics.checkNotNullParameter(th, "throwable");
        Log.w(ManageDonationsViewModel.TAG, "Error retrieving subscription transaction state", th);
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$refresh$3$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel$refresh$3.m977invoke$lambda0((ManageDonationsState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final ManageDonationsState m977invoke$lambda0(ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "it");
        return ManageDonationsState.copy$default(manageDonationsState, null, ManageDonationsState.TransactionState.NetworkFailure.INSTANCE, null, null, 13, null);
    }
}
