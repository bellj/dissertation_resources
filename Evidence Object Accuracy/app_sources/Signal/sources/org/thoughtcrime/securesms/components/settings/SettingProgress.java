package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public final class SettingProgress {

    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Item> {
        public void bind(Item item) {
        }

        public ViewHolder(View view) {
            super(view);
        }
    }

    /* loaded from: classes4.dex */
    public static final class Item implements MappingModel<Item> {
        private final int id;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Item item) {
            return MappingModel.CC.$default$getChangePayload(this, item);
        }

        public Item() {
            this(0);
        }

        public Item(int i) {
            this.id = i;
        }

        public boolean areItemsTheSame(Item item) {
            return this.id == item.id;
        }

        public boolean areContentsTheSame(Item item) {
            return areItemsTheSame(item);
        }
    }
}
