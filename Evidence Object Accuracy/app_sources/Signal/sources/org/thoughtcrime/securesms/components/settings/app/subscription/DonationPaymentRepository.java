package org.thoughtcrime.securesms.components.settings.app.subscription;

import android.app.Activity;
import android.content.Intent;
import com.google.android.gms.wallet.PaymentData;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableEmitter;
import io.reactivex.rxjava3.core.CompletableObserver;
import io.reactivex.rxjava3.core.CompletableOnSubscribe;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.text.CharsKt__CharJVMKt;
import okhttp3.OkHttpClient;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.signal.donations.GooglePayApi;
import org.signal.donations.GooglePayPaymentSource;
import org.signal.donations.StripeApi;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SearchDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.Job;
import org.thoughtcrime.securesms.jobmanager.JobManager;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.BoostReceiptRequestResponseJob;
import org.thoughtcrime.securesms.jobs.SubscriptionReceiptRequestResponseJob;
import org.thoughtcrime.securesms.keyvalue.DonationsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.subscription.LevelUpdate;
import org.thoughtcrime.securesms.subscription.LevelUpdateOperation;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.util.Environment;
import org.thoughtcrime.securesms.util.ProfileUtil;
import org.whispersystems.signalservice.api.profiles.ProfileAndCredential;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.subscriptions.IdempotencyKey;
import org.whispersystems.signalservice.api.subscriptions.SubscriberId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionClientSecret;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: DonationPaymentRepository.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\u0018\u0000 62\u00020\u00012\u00020\u0002:\u00016B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0006\u0010\n\u001a\u00020\u000bJ:\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J0\u0010\u0019\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0006\u0010\u0017\u001a\u00020\u0018J\u000e\u0010\u001a\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010J\u0006\u0010\u001b\u001a\u00020\u000bJ\u001e\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00120\u001d2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u001e\u001a\u00020\u0018H\u0016J\u000e\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020 0\u001dH\u0016J\u0016\u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\u001d2\u0006\u0010#\u001a\u00020\u0016H\u0002J0\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020'2\b\u0010)\u001a\u0004\u0018\u00010*2\u0006\u0010+\u001a\u00020'2\u0006\u0010,\u001a\u00020-J\u001e\u0010.\u001a\u00020%2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010/\u001a\u00020\u00162\u0006\u0010&\u001a\u00020'J\u0006\u00100\u001a\u00020%J\b\u00101\u001a\u00020%H\u0002J\u0010\u00102\u001a\u00020\u000b2\u0006\u00103\u001a\u00020\u0016H\u0016J\u000e\u00104\u001a\u00020\u000b2\u0006\u0010#\u001a\u00020\u0016J\u000e\u00105\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u0014R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u00067"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "Lorg/signal/donations/StripeApi$PaymentIntentFetcher;", "Lorg/signal/donations/StripeApi$SetupIntentHelper;", "activity", "Landroid/app/Activity;", "(Landroid/app/Activity;)V", "googlePayApi", "Lorg/signal/donations/GooglePayApi;", "stripeApi", "Lorg/signal/donations/StripeApi;", "cancelActiveSubscription", "Lio/reactivex/rxjava3/core/Completable;", "confirmPayment", "price", "Lorg/signal/core/util/money/FiatMoney;", "paymentData", "Lcom/google/android/gms/wallet/PaymentData;", "paymentIntent", "Lorg/signal/donations/StripeApi$PaymentIntent;", "badgeRecipient", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "additionalMessage", "", "badgeLevel", "", "continuePayment", "continueSubscriptionSetup", "ensureSubscriberId", "fetchPaymentIntent", "Lio/reactivex/rxjava3/core/Single;", "level", "fetchSetupIntent", "Lorg/signal/donations/StripeApi$SetupIntent;", "getOrCreateLevelUpdateOperation", "Lorg/thoughtcrime/securesms/subscription/LevelUpdateOperation;", "subscriptionLevel", "onActivityResult", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "expectedRequestCode", "paymentsRequestCallback", "Lorg/signal/donations/GooglePayApi$PaymentRequestCallback;", "requestTokenFromGooglePay", EmojiSearchDatabase.LABEL, "scheduleSyncForAccountRecordChange", "scheduleSyncForAccountRecordChangeSync", "setDefaultPaymentMethod", "paymentMethodId", "setSubscriptionLevel", "verifyRecipientIsAllowedToReceiveAGift", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationPaymentRepository implements StripeApi.PaymentIntentFetcher, StripeApi.SetupIntentHelper {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(DonationPaymentRepository.class);
    private final GooglePayApi googlePayApi;
    private final StripeApi stripeApi;

    /* compiled from: DonationPaymentRepository.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[JobTracker.JobState.values().length];
            iArr[JobTracker.JobState.SUCCESS.ordinal()] = 1;
            iArr[JobTracker.JobState.FAILURE.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public DonationPaymentRepository(Activity activity) {
        Intrinsics.checkNotNullParameter(activity, "activity");
        Environment.Donations donations = Environment.Donations.INSTANCE;
        this.googlePayApi = new GooglePayApi(activity, new StripeApi.Gateway(donations.getSTRIPE_CONFIGURATION()), donations.getGOOGLE_PAY_CONFIGURATION());
        StripeApi.Configuration stripe_configuration = donations.getSTRIPE_CONFIGURATION();
        OkHttpClient okHttpClient = ApplicationDependencies.getOkHttpClient();
        Intrinsics.checkNotNullExpressionValue(okHttpClient, "getOkHttpClient()");
        this.stripeApi = new StripeApi(stripe_configuration, this, this, okHttpClient);
    }

    public final void scheduleSyncForAccountRecordChange() {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda25
            @Override // java.lang.Runnable
            public final void run() {
                DonationPaymentRepository.m905scheduleSyncForAccountRecordChange$lambda0(DonationPaymentRepository.this);
            }
        });
    }

    /* renamed from: scheduleSyncForAccountRecordChange$lambda-0 */
    public static final void m905scheduleSyncForAccountRecordChange$lambda0(DonationPaymentRepository donationPaymentRepository) {
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "this$0");
        donationPaymentRepository.scheduleSyncForAccountRecordChangeSync();
    }

    private final void scheduleSyncForAccountRecordChangeSync() {
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
    }

    public final void requestTokenFromGooglePay(FiatMoney fiatMoney, String str, int i) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
        Log.d(TAG, "Requesting a token from google pay...");
        this.googlePayApi.requestPayment(fiatMoney, str, i);
    }

    public final void onActivityResult(int i, int i2, Intent intent, int i3, GooglePayApi.PaymentRequestCallback paymentRequestCallback) {
        Intrinsics.checkNotNullParameter(paymentRequestCallback, "paymentsRequestCallback");
        Log.d(TAG, "Processing possible google pay result...");
        this.googlePayApi.onActivityResult(i, i2, intent, i3, paymentRequestCallback);
    }

    public final Completable verifyRecipientIsAllowedToReceiveAGift(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "badgeRecipient");
        Completable subscribeOn = Completable.fromAction(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonationPaymentRepository.m914verifyRecipientIsAllowedToReceiveAGift$lambda1(RecipientId.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Log.d…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: verifyRecipientIsAllowedToReceiveAGift$lambda-1 */
    public static final void m914verifyRecipientIsAllowedToReceiveAGift$lambda1(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "$badgeRecipient");
        String str = TAG;
        Log.d(str, "Verifying badge recipient " + recipientId, true);
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(badgeRecipient)");
        if (resolved.isSelf()) {
            Log.d(str, "Cannot send a gift to self.", true);
            throw DonationError.GiftRecipientVerificationError.SelectedRecipientDoesNotSupportGifts.INSTANCE;
        } else if (resolved.isGroup() || resolved.isDistributionList() || resolved.getRegistered() != RecipientDatabase.RegisteredState.REGISTERED) {
            Log.w(str, "Invalid badge recipient " + recipientId + ". Verification failed.", true);
            throw DonationError.GiftRecipientVerificationError.SelectedRecipientIsInvalid.INSTANCE;
        } else {
            try {
                ProfileAndCredential retrieveProfileSync = ProfileUtil.retrieveProfileSync(ApplicationDependencies.getApplication(), resolved, SignalServiceProfile.RequestType.PROFILE_AND_CREDENTIAL);
                Intrinsics.checkNotNullExpressionValue(retrieveProfileSync, "retrieveProfileSync(Appl…e.PROFILE_AND_CREDENTIAL)");
                if (retrieveProfileSync.getProfile().getCapabilities().isGiftBadges()) {
                    Log.d(str, "Badge recipient supports gifting. Verification successful.", true);
                } else {
                    Log.w(str, "Badge recipient does not support gifting. Verification failed.", true);
                    throw DonationError.GiftRecipientVerificationError.SelectedRecipientDoesNotSupportGifts.INSTANCE;
                }
            } catch (IOException e) {
                Log.w(TAG, "Failed to retrieve profile for recipient.", e, true);
                throw new DonationError.GiftRecipientVerificationError.FailedToFetchProfile(e);
            }
        }
    }

    public final Completable continuePayment(FiatMoney fiatMoney, PaymentData paymentData, RecipientId recipientId, String str, long j) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        Intrinsics.checkNotNullParameter(paymentData, "paymentData");
        Intrinsics.checkNotNullParameter(recipientId, "badgeRecipient");
        String str2 = TAG;
        Log.d(str2, "Creating payment intent for " + fiatMoney + SearchDatabase.SNIPPET_WRAP, true);
        Completable subscribeOn = this.stripeApi.createPaymentIntent(fiatMoney, j).onErrorResumeNext(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda10
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m893continuePayment$lambda2(RecipientId.this, (Throwable) obj);
            }
        }).flatMapCompletable(new Function(fiatMoney, this, paymentData, str, j) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ FiatMoney f$1;
            public final /* synthetic */ DonationPaymentRepository f$2;
            public final /* synthetic */ PaymentData f$3;
            public final /* synthetic */ String f$4;
            public final /* synthetic */ long f$5;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m894continuePayment$lambda3(RecipientId.this, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, (StripeApi.CreatePaymentIntentResult) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "stripeApi.createPaymentI…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: continuePayment$lambda-2 */
    public static final SingleSource m893continuePayment$lambda2(RecipientId recipientId, Throwable th) {
        Intrinsics.checkNotNullParameter(recipientId, "$badgeRecipient");
        if (th instanceof DonationError) {
            return Single.error(th);
        }
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(badgeRecipient)");
        DonationErrorSource donationErrorSource = resolved.isSelf() ? DonationErrorSource.BOOST : DonationErrorSource.GIFT;
        DonationError.Companion companion = DonationError.Companion;
        Intrinsics.checkNotNullExpressionValue(th, "it");
        return Single.error(companion.getPaymentSetupError(donationErrorSource, th));
    }

    /* renamed from: continuePayment$lambda-3 */
    public static final CompletableSource m894continuePayment$lambda3(RecipientId recipientId, FiatMoney fiatMoney, DonationPaymentRepository donationPaymentRepository, PaymentData paymentData, String str, long j, StripeApi.CreatePaymentIntentResult createPaymentIntentResult) {
        Intrinsics.checkNotNullParameter(recipientId, "$badgeRecipient");
        Intrinsics.checkNotNullParameter(fiatMoney, "$price");
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "this$0");
        Intrinsics.checkNotNullParameter(paymentData, "$paymentData");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(badgeRecipient)");
        DonationErrorSource donationErrorSource = resolved.isSelf() ? DonationErrorSource.BOOST : DonationErrorSource.GIFT;
        String str2 = TAG;
        Log.d(str2, "Created payment intent for " + fiatMoney + '.', true);
        if (createPaymentIntentResult instanceof StripeApi.CreatePaymentIntentResult.AmountIsTooSmall) {
            return Completable.error(DonationError.Companion.oneTimeDonationAmountTooSmall(donationErrorSource));
        }
        if (createPaymentIntentResult instanceof StripeApi.CreatePaymentIntentResult.AmountIsTooLarge) {
            return Completable.error(DonationError.Companion.oneTimeDonationAmountTooLarge(donationErrorSource));
        }
        if (createPaymentIntentResult instanceof StripeApi.CreatePaymentIntentResult.CurrencyIsNotSupported) {
            return Completable.error(DonationError.Companion.invalidCurrencyForOneTimeDonation(donationErrorSource));
        }
        if (createPaymentIntentResult instanceof StripeApi.CreatePaymentIntentResult.Success) {
            return donationPaymentRepository.confirmPayment(fiatMoney, paymentData, ((StripeApi.CreatePaymentIntentResult.Success) createPaymentIntentResult).getPaymentIntent(), recipientId, str, j);
        }
        throw new NoWhenBranchMatchedException();
    }

    public final Completable continueSubscriptionSetup(PaymentData paymentData) {
        Intrinsics.checkNotNullParameter(paymentData, "paymentData");
        Log.d(TAG, "Continuing subscription setup...", true);
        Completable flatMapCompletable = this.stripeApi.createSetupIntent().flatMapCompletable(new Function(paymentData) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ PaymentData f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m895continueSubscriptionSetup$lambda5(DonationPaymentRepository.this, this.f$1, (StripeApi.CreateSetupIntentResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMapCompletable, "stripeApi.createSetupInt…, true)\n        }\n      }");
        return flatMapCompletable;
    }

    /* renamed from: continueSubscriptionSetup$lambda-5 */
    public static final CompletableSource m895continueSubscriptionSetup$lambda5(DonationPaymentRepository donationPaymentRepository, PaymentData paymentData, StripeApi.CreateSetupIntentResult createSetupIntentResult) {
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "this$0");
        Intrinsics.checkNotNullParameter(paymentData, "$paymentData");
        Log.d(TAG, "Retrieved SetupIntent, confirming...", true);
        return donationPaymentRepository.stripeApi.confirmSetupIntent(new GooglePayPaymentSource(paymentData), createSetupIntentResult.getSetupIntent()).doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonationPaymentRepository.m896continueSubscriptionSetup$lambda5$lambda4();
            }
        });
    }

    /* renamed from: continueSubscriptionSetup$lambda-5$lambda-4 */
    public static final void m896continueSubscriptionSetup$lambda5$lambda4() {
        Log.d(TAG, "Confirmed SetupIntent...", true);
    }

    public final Completable cancelActiveSubscription() {
        Log.d(TAG, "Canceling active subscription...", true);
        Completable doOnComplete = ApplicationDependencies.getDonationsService().cancelSubscription(SignalStore.donationsValues().requireSubscriber().getSubscriberId()).subscribeOn(Schedulers.io()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).ignoreElement().doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda14
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonationPaymentRepository.m889cancelActiveSubscription$lambda6();
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnComplete, "getDonationsService()\n  …e subscription.\", true) }");
        return doOnComplete;
    }

    /* renamed from: cancelActiveSubscription$lambda-6 */
    public static final void m889cancelActiveSubscription$lambda6() {
        Log.d(TAG, "Cancelled active subscription.", true);
    }

    public final Completable ensureSubscriberId() {
        SubscriberId subscriberId;
        Log.d(TAG, "Ensuring SubscriberId exists on Signal service...", true);
        Subscriber subscriber = SignalStore.donationsValues().getSubscriber();
        if (subscriber == null || (subscriberId = subscriber.getSubscriberId()) == null) {
            subscriberId = SubscriberId.generate();
        }
        Completable doOnComplete = ApplicationDependencies.getDonationsService().putSubscription(subscriberId).subscribeOn(Schedulers.io()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).ignoreElement().doOnComplete(new Action(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ DonationPaymentRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonationPaymentRepository.m897ensureSubscriberId$lambda7(SubscriberId.this, this.f$1);
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnComplete, "getDonationsService()\n  …ecordChangeSync()\n      }");
        return doOnComplete;
    }

    /* renamed from: ensureSubscriberId$lambda-7 */
    public static final void m897ensureSubscriberId$lambda7(SubscriberId subscriberId, DonationPaymentRepository donationPaymentRepository) {
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "this$0");
        Log.d(TAG, "Successfully set SubscriberId exists on Signal service.", true);
        DonationsValues donationsValues = SignalStore.donationsValues();
        Intrinsics.checkNotNullExpressionValue(subscriberId, "subscriberId");
        String currencyCode = SignalStore.donationsValues().getSubscriptionCurrency().getCurrencyCode();
        Intrinsics.checkNotNullExpressionValue(currencyCode, "donationsValues().getSub…onCurrency().currencyCode");
        donationsValues.setSubscriber(new Subscriber(subscriberId, currencyCode));
        donationPaymentRepository.scheduleSyncForAccountRecordChangeSync();
    }

    private final Completable confirmPayment(FiatMoney fiatMoney, PaymentData paymentData, StripeApi.PaymentIntent paymentIntent, RecipientId recipientId, String str, long j) {
        boolean areEqual = Intrinsics.areEqual(recipientId, Recipient.self().getId());
        DonationErrorSource donationErrorSource = areEqual ? DonationErrorSource.BOOST : DonationErrorSource.GIFT;
        Log.d(TAG, "Confirming payment intent...", true);
        Completable andThen = this.stripeApi.confirmPaymentIntent(new GooglePayPaymentSource(paymentData), paymentIntent).onErrorResumeNext(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m892confirmPayment$lambda8(DonationErrorSource.this, (Throwable) obj);
            }
        }).andThen(Completable.create(new CompletableOnSubscribe(areEqual, fiatMoney, paymentIntent, recipientId, str, j, donationErrorSource) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ boolean f$0;
            public final /* synthetic */ FiatMoney f$1;
            public final /* synthetic */ StripeApi.PaymentIntent f$2;
            public final /* synthetic */ RecipientId f$3;
            public final /* synthetic */ String f$4;
            public final /* synthetic */ long f$5;
            public final /* synthetic */ DonationErrorSource f$6;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
                this.f$6 = r8;
            }

            @Override // io.reactivex.rxjava3.core.CompletableOnSubscribe
            public final void subscribe(CompletableEmitter completableEmitter) {
                DonationPaymentRepository.m890confirmPayment$lambda11(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, completableEmitter);
            }
        }));
        Intrinsics.checkNotNullExpressionValue(andThen, "confirmPayment.andThen(waitOnRedemption)");
        return andThen;
    }

    /* renamed from: confirmPayment$lambda-8 */
    public static final CompletableSource m892confirmPayment$lambda8(DonationErrorSource donationErrorSource, Throwable th) {
        Intrinsics.checkNotNullParameter(donationErrorSource, "$donationErrorSource");
        DonationError.Companion companion = DonationError.Companion;
        Intrinsics.checkNotNullExpressionValue(th, "it");
        return Completable.error(companion.getPaymentSetupError(donationErrorSource, th));
    }

    /* renamed from: confirmPayment$lambda-11 */
    public static final void m890confirmPayment$lambda11(boolean z, FiatMoney fiatMoney, StripeApi.PaymentIntent paymentIntent, RecipientId recipientId, String str, long j, DonationErrorSource donationErrorSource, CompletableEmitter completableEmitter) {
        DonationReceiptRecord donationReceiptRecord;
        JobManager.Chain chain;
        String str2;
        Intrinsics.checkNotNullParameter(fiatMoney, "$price");
        Intrinsics.checkNotNullParameter(paymentIntent, "$paymentIntent");
        Intrinsics.checkNotNullParameter(recipientId, "$badgeRecipient");
        Intrinsics.checkNotNullParameter(donationErrorSource, "$donationErrorSource");
        if (z) {
            donationReceiptRecord = DonationReceiptRecord.Companion.createForBoost(fiatMoney);
        } else {
            donationReceiptRecord = DonationReceiptRecord.Companion.createForGift(fiatMoney);
        }
        String code = donationReceiptRecord.getType().getCode();
        if (code.length() > 0) {
            StringBuilder sb = new StringBuilder();
            char charAt = code.charAt(0);
            if (Character.isLowerCase(charAt)) {
                Locale locale = Locale.US;
                Intrinsics.checkNotNullExpressionValue(locale, "US");
                str2 = CharsKt__CharJVMKt.titlecase(charAt, locale);
            } else {
                str2 = String.valueOf(charAt);
            }
            sb.append((Object) str2);
            String substring = code.substring(1);
            Intrinsics.checkNotNullExpressionValue(substring, "this as java.lang.String).substring(startIndex)");
            sb.append(substring);
            code = sb.toString();
        }
        String str3 = TAG;
        Log.d(str3, "Confirmed payment intent. Recording " + code + " receipt and submitting badge reimbursement job chain.", true);
        SignalDatabase.Companion.donationReceipts().addReceipt(donationReceiptRecord);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        if (z) {
            chain = BoostReceiptRequestResponseJob.createJobChainForBoost(paymentIntent);
        } else {
            chain = BoostReceiptRequestResponseJob.createJobChainForGift(paymentIntent, recipientId, str, j);
        }
        chain.enqueue(new JobTracker.JobListener(countDownLatch) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ CountDownLatch f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobListener
            public final void onStateChanged(Job job, JobTracker.JobState jobState) {
                DonationPaymentRepository.m891confirmPayment$lambda11$lambda10(Ref$ObjectRef.this, this.f$1, job, jobState);
            }
        });
        try {
            if (countDownLatch.await(10, TimeUnit.SECONDS)) {
                JobTracker.JobState jobState = (JobTracker.JobState) ref$ObjectRef.element;
                int i = jobState == null ? -1 : WhenMappings.$EnumSwitchMapping$0[jobState.ordinal()];
                if (i == 1) {
                    Log.d(str3, code + " request response job chain succeeded.", true);
                    completableEmitter.onComplete();
                } else if (i != 2) {
                    Log.d(str3, code + " request response job chain ignored due to in-progress jobs.", true);
                    completableEmitter.onError(DonationError.Companion.timeoutWaitingForToken(donationErrorSource));
                } else {
                    Log.d(str3, code + " request response job chain failed permanently.", true);
                    completableEmitter.onError(DonationError.Companion.genericBadgeRedemptionFailure(donationErrorSource));
                }
            } else {
                Log.d(str3, code + " job chain timed out waiting for job completion.", true);
                completableEmitter.onError(DonationError.Companion.timeoutWaitingForToken(donationErrorSource));
            }
        } catch (InterruptedException e) {
            String str4 = TAG;
            Log.d(str4, code + " job chain interrupted", e, true);
            completableEmitter.onError(DonationError.Companion.timeoutWaitingForToken(donationErrorSource));
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: org.thoughtcrime.securesms.jobmanager.JobTracker$JobState */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: confirmPayment$lambda-11$lambda-10 */
    public static final void m891confirmPayment$lambda11$lambda10(Ref$ObjectRef ref$ObjectRef, CountDownLatch countDownLatch, Job job, JobTracker.JobState jobState) {
        Intrinsics.checkNotNullParameter(ref$ObjectRef, "$finalJobState");
        Intrinsics.checkNotNullParameter(countDownLatch, "$countDownLatch");
        Intrinsics.checkNotNullParameter(job, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(jobState, "jobState");
        if (jobState.isComplete()) {
            ref$ObjectRef.element = jobState;
            countDownLatch.countDown();
        }
    }

    public final Completable setSubscriptionLevel(String str) {
        Intrinsics.checkNotNullParameter(str, "subscriptionLevel");
        Completable subscribeOn = getOrCreateLevelUpdateOperation(str).flatMapCompletable(new Function(str, this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda12
            public final /* synthetic */ String f$0;
            public final /* synthetic */ DonationPaymentRepository f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m909setSubscriptionLevel$lambda15(this.f$0, this.f$1, (LevelUpdateOperation) obj);
            }
        }).doOnError(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda13
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationPaymentRepository.m913setSubscriptionLevel$lambda16((Throwable) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "getOrCreateLevelUpdateOp…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: setSubscriptionLevel$lambda-15 */
    public static final CompletableSource m909setSubscriptionLevel$lambda15(String str, DonationPaymentRepository donationPaymentRepository, LevelUpdateOperation levelUpdateOperation) {
        Intrinsics.checkNotNullParameter(str, "$subscriptionLevel");
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "this$0");
        Subscriber requireSubscriber = SignalStore.donationsValues().requireSubscriber();
        String str2 = TAG;
        Log.d(str2, "Attempting to set user subscription level to " + str, true);
        return ApplicationDependencies.getDonationsService().updateSubscriptionLevel(requireSubscriber.getSubscriberId(), str, requireSubscriber.getCurrencyCode(), levelUpdateOperation.getIdempotencyKey().serialize(), SubscriptionReceiptRequestResponseJob.MUTEX).flatMapCompletable(new Function(str, donationPaymentRepository) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda19
            public final /* synthetic */ String f$0;
            public final /* synthetic */ DonationPaymentRepository f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m910setSubscriptionLevel$lambda15$lambda12(this.f$0, this.f$1, (ServiceResponse) obj);
            }
        }).andThen(new CompletableSource() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda20
            @Override // io.reactivex.rxjava3.core.CompletableSource
            public final void subscribe(CompletableObserver completableObserver) {
                DonationPaymentRepository.m911setSubscriptionLevel$lambda15$lambda14(completableObserver);
            }
        });
    }

    /* renamed from: setSubscriptionLevel$lambda-15$lambda-12 */
    public static final CompletableSource m910setSubscriptionLevel$lambda15$lambda12(String str, DonationPaymentRepository donationPaymentRepository, ServiceResponse serviceResponse) {
        Intrinsics.checkNotNullParameter(str, "$subscriptionLevel");
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "this$0");
        if (serviceResponse.getStatus() == 200 || serviceResponse.getStatus() == 204) {
            String str2 = TAG;
            Log.d(str2, "Successfully set user subscription to level " + str + " with response code " + serviceResponse.getStatus(), true);
            SignalStore.donationsValues().updateLocalStateForLocalSubscribe();
            donationPaymentRepository.scheduleSyncForAccountRecordChange();
            LevelUpdate.INSTANCE.updateProcessingState(false);
            return Completable.complete();
        }
        if (serviceResponse.getApplicationError().isPresent()) {
            String str3 = TAG;
            Log.w(str3, "Failed to set user subscription to level " + str + " with response code " + serviceResponse.getStatus(), serviceResponse.getApplicationError().get(), true);
            SignalStore.donationsValues().clearLevelOperations();
        } else {
            String str4 = TAG;
            Log.w(str4, "Failed to set user subscription to level " + str, serviceResponse.getExecutionError().orElse(null), true);
        }
        LevelUpdate.INSTANCE.updateProcessingState(false);
        return serviceResponse.flattenResult().ignoreElement();
    }

    /* renamed from: setSubscriptionLevel$lambda-15$lambda-14 */
    public static final void m911setSubscriptionLevel$lambda15$lambda14(CompletableObserver completableObserver) {
        String str = TAG;
        Log.d(str, "Enqueuing request response job chain.", true);
        CountDownLatch countDownLatch = new CountDownLatch(1);
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        SubscriptionReceiptRequestResponseJob.createSubscriptionContinuationJobChain().enqueue(new JobTracker.JobListener(countDownLatch) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda15
            public final /* synthetic */ CountDownLatch f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.jobmanager.JobTracker.JobListener
            public final void onStateChanged(Job job, JobTracker.JobState jobState) {
                DonationPaymentRepository.m912setSubscriptionLevel$lambda15$lambda14$lambda13(Ref$ObjectRef.this, this.f$1, job, jobState);
            }
        });
        try {
            if (countDownLatch.await(10, TimeUnit.SECONDS)) {
                JobTracker.JobState jobState = (JobTracker.JobState) ref$ObjectRef.element;
                int i = jobState == null ? -1 : WhenMappings.$EnumSwitchMapping$0[jobState.ordinal()];
                if (i == 1) {
                    Log.d(str, "Subscription request response job chain succeeded.", true);
                    completableObserver.onComplete();
                } else if (i != 2) {
                    Log.d(str, "Subscription request response job chain ignored due to in-progress jobs.", true);
                    completableObserver.onError(DonationError.Companion.timeoutWaitingForToken(DonationErrorSource.SUBSCRIPTION));
                } else {
                    Log.d(str, "Subscription request response job chain failed permanently.", true);
                    completableObserver.onError(DonationError.Companion.genericBadgeRedemptionFailure(DonationErrorSource.SUBSCRIPTION));
                }
            } else {
                Log.d(str, "Subscription request response job timed out.", true);
                completableObserver.onError(DonationError.Companion.timeoutWaitingForToken(DonationErrorSource.SUBSCRIPTION));
            }
        } catch (InterruptedException e) {
            Log.w(TAG, "Subscription request response interrupted.", e, true);
            completableObserver.onError(DonationError.Companion.timeoutWaitingForToken(DonationErrorSource.SUBSCRIPTION));
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: org.thoughtcrime.securesms.jobmanager.JobTracker$JobState */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: setSubscriptionLevel$lambda-15$lambda-14$lambda-13 */
    public static final void m912setSubscriptionLevel$lambda15$lambda14$lambda13(Ref$ObjectRef ref$ObjectRef, CountDownLatch countDownLatch, Job job, JobTracker.JobState jobState) {
        Intrinsics.checkNotNullParameter(ref$ObjectRef, "$finalJobState");
        Intrinsics.checkNotNullParameter(countDownLatch, "$countDownLatch");
        Intrinsics.checkNotNullParameter(job, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(jobState, "jobState");
        if (jobState.isComplete()) {
            ref$ObjectRef.element = jobState;
            countDownLatch.countDown();
        }
    }

    /* renamed from: setSubscriptionLevel$lambda-16 */
    public static final void m913setSubscriptionLevel$lambda16(Throwable th) {
        LevelUpdate.INSTANCE.updateProcessingState(false);
    }

    private final Single<LevelUpdateOperation> getOrCreateLevelUpdateOperation(String str) {
        Single<LevelUpdateOperation> fromCallable = Single.fromCallable(new Callable(str) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda16
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationPaymentRepository.m904getOrCreateLevelUpdateOperation$lambda17(this.f$0);
            }
        });
        Intrinsics.checkNotNullExpressionValue(fromCallable, "fromCallable {\n    Log.d…UpdateOperation\n    }\n  }");
        return fromCallable;
    }

    /* renamed from: getOrCreateLevelUpdateOperation$lambda-17 */
    public static final LevelUpdateOperation m904getOrCreateLevelUpdateOperation$lambda17(String str) {
        Intrinsics.checkNotNullParameter(str, "$subscriptionLevel");
        String str2 = TAG;
        Log.d(str2, "Retrieving level update operation for " + str);
        LevelUpdateOperation levelOperation = SignalStore.donationsValues().getLevelOperation(str);
        if (levelOperation == null) {
            IdempotencyKey generate = IdempotencyKey.generate();
            Intrinsics.checkNotNullExpressionValue(generate, "generate()");
            LevelUpdateOperation levelUpdateOperation = new LevelUpdateOperation(generate, str);
            SignalStore.donationsValues().setLevelOperation(levelUpdateOperation);
            LevelUpdate.INSTANCE.updateProcessingState(true);
            Log.d(str2, "Created a new operation for " + str);
            return levelUpdateOperation;
        }
        LevelUpdate.INSTANCE.updateProcessingState(true);
        Log.d(str2, "Reusing operation for " + str);
        return levelOperation;
    }

    @Override // org.signal.donations.StripeApi.PaymentIntentFetcher
    public Single<StripeApi.PaymentIntent> fetchPaymentIntent(FiatMoney fiatMoney, long j) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        String str = TAG;
        Log.d(str, "Fetching payment intent from Signal service for " + fiatMoney + "... (Locale.US minimum precision: " + fiatMoney.getMinimumUnitPrecisionString() + ')');
        Single<StripeApi.PaymentIntent> doOnSuccess = ApplicationDependencies.getDonationsService().createDonationIntentWithAmount(fiatMoney.getMinimumUnitPrecisionString(), fiatMoney.getCurrency().getCurrencyCode(), j).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda17
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m898fetchPaymentIntent$lambda18((SubscriptionClientSecret) obj);
            }
        }).doOnSuccess(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda18
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationPaymentRepository.m899fetchPaymentIntent$lambda19((StripeApi.PaymentIntent) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnSuccess, "getDonationsService()\n  …Signal service!\")\n      }");
        return doOnSuccess;
    }

    /* renamed from: fetchPaymentIntent$lambda-18 */
    public static final StripeApi.PaymentIntent m898fetchPaymentIntent$lambda18(SubscriptionClientSecret subscriptionClientSecret) {
        String id = subscriptionClientSecret.getId();
        Intrinsics.checkNotNullExpressionValue(id, "it.id");
        String clientSecret = subscriptionClientSecret.getClientSecret();
        Intrinsics.checkNotNullExpressionValue(clientSecret, "it.clientSecret");
        return new StripeApi.PaymentIntent(id, clientSecret);
    }

    /* renamed from: fetchPaymentIntent$lambda-19 */
    public static final void m899fetchPaymentIntent$lambda19(StripeApi.PaymentIntent paymentIntent) {
        Log.d(TAG, "Got payment intent from Signal service!");
    }

    @Override // org.signal.donations.StripeApi.SetupIntentHelper
    public Single<StripeApi.SetupIntent> fetchSetupIntent() {
        Log.d(TAG, "Fetching setup intent from Signal service...");
        Single<StripeApi.SetupIntent> doOnSuccess = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda21
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationPaymentRepository.m900fetchSetupIntent$lambda20();
            }
        }).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda22
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m901fetchSetupIntent$lambda21((Subscriber) obj);
            }
        }).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda23
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m902fetchSetupIntent$lambda22((SubscriptionClientSecret) obj);
            }
        }).doOnSuccess(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda24
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationPaymentRepository.m903fetchSetupIntent$lambda23((StripeApi.SetupIntent) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnSuccess, "fromCallable { SignalSto…Signal service!\")\n      }");
        return doOnSuccess;
    }

    /* renamed from: fetchSetupIntent$lambda-20 */
    public static final Subscriber m900fetchSetupIntent$lambda20() {
        return SignalStore.donationsValues().requireSubscriber();
    }

    /* renamed from: fetchSetupIntent$lambda-21 */
    public static final SingleSource m901fetchSetupIntent$lambda21(Subscriber subscriber) {
        return ApplicationDependencies.getDonationsService().createSubscriptionPaymentMethod(subscriber.getSubscriberId());
    }

    /* renamed from: fetchSetupIntent$lambda-22 */
    public static final StripeApi.SetupIntent m902fetchSetupIntent$lambda22(SubscriptionClientSecret subscriptionClientSecret) {
        String id = subscriptionClientSecret.getId();
        Intrinsics.checkNotNullExpressionValue(id, "it.id");
        String clientSecret = subscriptionClientSecret.getClientSecret();
        Intrinsics.checkNotNullExpressionValue(clientSecret, "it.clientSecret");
        return new StripeApi.SetupIntent(id, clientSecret);
    }

    /* renamed from: fetchSetupIntent$lambda-23 */
    public static final void m903fetchSetupIntent$lambda23(StripeApi.SetupIntent setupIntent) {
        Log.d(TAG, "Got setup intent from Signal service!");
    }

    @Override // org.signal.donations.StripeApi.SetupIntentHelper
    public Completable setDefaultPaymentMethod(String str) {
        Intrinsics.checkNotNullParameter(str, "paymentMethodId");
        Log.d(TAG, "Setting default payment method via Signal service...");
        Completable doOnComplete = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda7
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationPaymentRepository.m906setDefaultPaymentMethod$lambda24();
            }
        }).flatMap(new Function(str) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationPaymentRepository.m907setDefaultPaymentMethod$lambda25(this.f$0, (Subscriber) obj);
            }
        }).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).ignoreElement().doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                DonationPaymentRepository.m908setDefaultPaymentMethod$lambda26();
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnComplete, "fromCallable {\n      Sig…a Signal service!\")\n    }");
        return doOnComplete;
    }

    /* renamed from: setDefaultPaymentMethod$lambda-24 */
    public static final Subscriber m906setDefaultPaymentMethod$lambda24() {
        return SignalStore.donationsValues().requireSubscriber();
    }

    /* renamed from: setDefaultPaymentMethod$lambda-25 */
    public static final SingleSource m907setDefaultPaymentMethod$lambda25(String str, Subscriber subscriber) {
        Intrinsics.checkNotNullParameter(str, "$paymentMethodId");
        return ApplicationDependencies.getDonationsService().setDefaultPaymentMethodId(subscriber.getSubscriberId(), str);
    }

    /* renamed from: setDefaultPaymentMethod$lambda-26 */
    public static final void m908setDefaultPaymentMethod$lambda26() {
        Log.d(TAG, "Set default payment method via Signal service!");
    }

    /* compiled from: DonationPaymentRepository.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
