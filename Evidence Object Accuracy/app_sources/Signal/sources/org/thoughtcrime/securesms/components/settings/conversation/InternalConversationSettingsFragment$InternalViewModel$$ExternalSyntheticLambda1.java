package org.thoughtcrime.securesms.components.settings.conversation;

import j$.util.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment;
import org.thoughtcrime.securesms.database.GroupDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda1 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return InternalConversationSettingsFragment.InternalViewModel.m1164lambda2$lambda0((GroupDatabase.GroupRecord) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
