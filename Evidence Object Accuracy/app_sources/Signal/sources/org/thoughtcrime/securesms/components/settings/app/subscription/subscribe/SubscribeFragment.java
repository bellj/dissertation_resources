package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgePreview;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.CurrencySelection;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.GooglePayButton;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.NetworkFailure;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel;
import org.thoughtcrime.securesms.components.settings.models.Progress;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.services.DonationsService;

/* compiled from: SubscribeFragment.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0005\u0018\u0000 )2\u00020\u0001:\u0001)B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u0017H\u0016J\b\u0010\u001f\u001a\u00020\u0017H\u0002J\u0010\u0010 \u001a\u00020\u00172\u0006\u0010!\u001a\u00020\"H\u0002J\u0012\u0010#\u001a\u00020\u00172\b\u0010$\u001a\u0004\u0018\u00010%H\u0002J\b\u0010&\u001a\u00020\u0017H\u0016J\b\u0010'\u001a\u00020\u0017H\u0002J\u0010\u0010(\u001a\u00020\u00172\u0006\u0010$\u001a\u00020%H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\u0010\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u0011\u001a\u00020\u00128BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\u0010\u001a\u0004\b\u0013\u0010\u0014¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "donationPaymentComponent", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "errorDialog", "Landroid/content/DialogInterface;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "processingDonationPaymentDialog", "Landroidx/appcompat/app/AlertDialog;", "supportTechSummary", "", "getSupportTechSummary", "()Ljava/lang/CharSequence;", "supportTechSummary$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState;", "onDestroyView", "onGooglePayButtonClicked", "onPaymentConfirmed", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "onPaymentError", "throwable", "", "onResume", "onSubscriptionCancelled", "onSubscriptionFailedToCancel", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeFragment extends DSLSettingsFragment {
    public static final Companion Companion = new Companion(null);
    private static final int FETCH_SUBSCRIPTION_TOKEN_REQUEST_CODE;
    private static final String TAG = Log.tag(SubscribeFragment.class);
    private DonationPaymentComponent donationPaymentComponent;
    private DialogInterface errorDialog;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private AlertDialog processingDonationPaymentDialog;
    private final Lazy supportTechSummary$delegate = LazyKt__LazyJVMKt.lazy(new SubscribeFragment$supportTechSummary$2(this));
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SubscribeViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$viewModel$2
        final /* synthetic */ SubscribeFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            DonationsService donationsService = ApplicationDependencies.getDonationsService();
            Intrinsics.checkNotNullExpressionValue(donationsService, "getDonationsService()");
            SubscriptionsRepository subscriptionsRepository = new SubscriptionsRepository(donationsService);
            DonationPaymentComponent access$getDonationPaymentComponent$p = SubscribeFragment.access$getDonationPaymentComponent$p(this.this$0);
            if (access$getDonationPaymentComponent$p == null) {
                Intrinsics.throwUninitializedPropertyAccessException("donationPaymentComponent");
                access$getDonationPaymentComponent$p = null;
            }
            return new SubscribeViewModel.Factory(subscriptionsRepository, access$getDonationPaymentComponent$p.getDonationPaymentRepository(), MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH);
        }
    });

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        DonationPaymentComponent donationPaymentComponent;
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        donationPaymentComponent = (DonationPaymentComponent) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent");
                    }
                } else if (fragment instanceof DonationPaymentComponent) {
                    donationPaymentComponent = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.donationPaymentComponent = donationPaymentComponent;
            getViewModel().refresh();
            BadgePreview.INSTANCE.register(dSLSettingsAdapter);
            CurrencySelection.INSTANCE.register(dSLSettingsAdapter);
            Subscription.Companion.register(dSLSettingsAdapter);
            GooglePayButton.INSTANCE.register(dSLSettingsAdapter);
            Progress.INSTANCE.register(dSLSettingsAdapter);
            NetworkFailure.INSTANCE.register(dSLSettingsAdapter);
            AlertDialog create = new MaterialAlertDialogBuilder(requireContext()).setView(R.layout.processing_payment_dialog).setCancelable(false).create();
            Intrinsics.checkNotNullExpressionValue(create, "MaterialAlertDialogBuild…le(false)\n      .create()");
            this.processingDonationPaymentDialog = create;
            getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ SubscribeFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    SubscribeFragment.$r8$lambda$IyAfjKFkNQjvVzOKBCxAX8wEyyc(DSLSettingsAdapter.this, this.f$1, (SubscribeState) obj);
                }
            });
            LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
            Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
            Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
            lifecycleDisposable.bindTo(lifecycle);
            LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
            Disposable subscribe = getViewModel().getEvents().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$$ExternalSyntheticLambda2
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    SubscribeFragment.$r8$lambda$oi7YOj4JtAQiE6Nt70DOVsxuOHA(SubscribeFragment.this, (DonationEvent) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.events.subscri….throwable)\n      }\n    }");
            lifecycleDisposable2.plusAssign(subscribe);
            LifecycleDisposable lifecycleDisposable3 = this.lifecycleDisposable;
            DonationPaymentComponent donationPaymentComponent2 = this.donationPaymentComponent;
            if (donationPaymentComponent2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("donationPaymentComponent");
                donationPaymentComponent2 = null;
            }
            Disposable subscribe2 = donationPaymentComponent2.getGooglePayResultPublisher().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$$ExternalSyntheticLambda3
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    SubscribeFragment.$r8$lambda$budJ2pmll7TmZGysWPLPDSfzJqo(SubscribeFragment.this, (DonationPaymentComponent.GooglePayResult) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe2, "donationPaymentComponent…esultCode, it.data)\n    }");
            lifecycleDisposable3.plusAssign(subscribe2);
            LifecycleDisposable lifecycleDisposable4 = this.lifecycleDisposable;
            Disposable subscribe3 = DonationError.Companion.getErrorsForSource(DonationErrorSource.SUBSCRIPTION).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$$ExternalSyntheticLambda4
                @Override // io.reactivex.rxjava3.functions.Consumer
                public final void accept(Object obj) {
                    SubscribeFragment.$r8$lambda$GcbhYvr_u3Htvjsocg_SHJ8y7zQ(SubscribeFragment.this, (DonationError) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(subscribe3, "DonationError\n      .get…or(donationError)\n      }");
            lifecycleDisposable4.plusAssign(subscribe3);
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    public SubscribeFragment() {
        super(0, 0, R.layout.subscribe_fragment, null, 11, null);
    }

    public final CharSequence getSupportTechSummary() {
        Object value = this.supportTechSummary$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-supportTechSummary>(...)");
        return (CharSequence) value;
    }

    public final SubscribeViewModel getViewModel() {
        return (SubscribeViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refreshActiveSubscription();
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m1034bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, SubscribeFragment subscribeFragment, SubscribeState subscribeState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(subscribeFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(subscribeState, "state");
        dSLSettingsAdapter.submitList(subscribeFragment.getConfiguration(subscribeState).toMappingModelList());
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m1035bindAdapter$lambda1(SubscribeFragment subscribeFragment, DonationEvent donationEvent) {
        Intrinsics.checkNotNullParameter(subscribeFragment, "this$0");
        if (donationEvent instanceof DonationEvent.PaymentConfirmationSuccess) {
            subscribeFragment.onPaymentConfirmed(((DonationEvent.PaymentConfirmationSuccess) donationEvent).getBadge());
        } else if (Intrinsics.areEqual(donationEvent, DonationEvent.RequestTokenSuccess.INSTANCE)) {
            Log.w(TAG, "Successfully got request token from Google Pay");
        } else if (Intrinsics.areEqual(donationEvent, DonationEvent.SubscriptionCancelled.INSTANCE)) {
            subscribeFragment.onSubscriptionCancelled();
        } else if (donationEvent instanceof DonationEvent.SubscriptionCancellationFailed) {
            subscribeFragment.onSubscriptionFailedToCancel(((DonationEvent.SubscriptionCancellationFailed) donationEvent).getThrowable());
        }
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m1036bindAdapter$lambda2(SubscribeFragment subscribeFragment, DonationPaymentComponent.GooglePayResult googlePayResult) {
        Intrinsics.checkNotNullParameter(subscribeFragment, "this$0");
        subscribeFragment.getViewModel().onActivityResult(googlePayResult.getRequestCode(), googlePayResult.getResultCode(), googlePayResult.getData());
    }

    /* renamed from: bindAdapter$lambda-3 */
    public static final void m1037bindAdapter$lambda3(SubscribeFragment subscribeFragment, DonationError donationError) {
        Intrinsics.checkNotNullParameter(subscribeFragment, "this$0");
        subscribeFragment.onPaymentError(donationError);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        AlertDialog alertDialog = this.processingDonationPaymentDialog;
        if (alertDialog == null) {
            Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            alertDialog = null;
        }
        alertDialog.dismiss();
    }

    private final DSLConfiguration getConfiguration(SubscribeState subscribeState) {
        AlertDialog alertDialog = null;
        if (subscribeState.getHasInProgressSubscriptionTransaction() || subscribeState.getStage() == SubscribeState.Stage.PAYMENT_PIPELINE) {
            AlertDialog alertDialog2 = this.processingDonationPaymentDialog;
            if (alertDialog2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            } else {
                alertDialog = alertDialog2;
            }
            alertDialog.show();
        } else {
            AlertDialog alertDialog3 = this.processingDonationPaymentDialog;
            if (alertDialog3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("processingDonationPaymentDialog");
            } else {
                alertDialog = alertDialog3;
            }
            alertDialog.hide();
        }
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(subscribeState, this, subscribeState.getStage() == SubscribeState.Stage.READY && !subscribeState.getHasInProgressSubscriptionTransaction()) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$getConfiguration$1
            final /* synthetic */ boolean $areFieldsEnabled;
            final /* synthetic */ SubscribeState $state;
            final /* synthetic */ SubscribeFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
                this.$areFieldsEnabled = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r22) {
                /*
                // Method dump skipped, instructions count: 606
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final void onGooglePayButtonClicked() {
        getViewModel().requestTokenFromGooglePay();
    }

    private final void onPaymentConfirmed(Badge badge) {
        NavController findNavController = FragmentKt.findNavController(this);
        SubscribeFragmentDirections.ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog isBoost = SubscribeFragmentDirections.actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog(badge).setIsBoost(false);
        Intrinsics.checkNotNullExpressionValue(isBoost, "actionSubscribeFragmentT…(badge).setIsBoost(false)");
        SafeNavigation.safeNavigate(findNavController, isBoost);
    }

    private final void onPaymentError(Throwable th) {
        String str = TAG;
        Log.w(str, "onPaymentError", th, true);
        if (this.errorDialog != null) {
            Log.i(str, "Already displaying an error dialog. Skipping.");
            return;
        }
        DonationErrorDialogs donationErrorDialogs = DonationErrorDialogs.INSTANCE;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        this.errorDialog = donationErrorDialogs.show(requireContext, th, new DonationErrorDialogs.DialogCallback(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$onPaymentError$1
            final /* synthetic */ SubscribeFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorDialogs.DialogCallback
            public void onDialogDismissed() {
                FragmentKt.findNavController(this.this$0).popBackStack();
            }
        });
    }

    private final void onSubscriptionCancelled() {
        Snackbar.make(requireView(), (int) R.string.SubscribeFragment__your_subscription_has_been_cancelled, 0).show();
        requireActivity().finish();
        FragmentActivity requireActivity = requireActivity();
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        requireActivity.startActivity(AppSettingsActivity.Companion.home$default(companion, requireContext, null, 2, null));
    }

    private final void onSubscriptionFailedToCancel(Throwable th) {
        Log.w(TAG, "Failed to cancel subscription", th, true);
        new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.DonationsErrors__failed_to_cancel_subscription).setMessage(R.string.DonationsErrors__subscription_cancellation_requires_an_internet_connection).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeFragment$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                SubscribeFragment.$r8$lambda$z8ur9H5QU0OsNamoI7bNDRSSeA8(SubscribeFragment.this, dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: onSubscriptionFailedToCancel$lambda-4 */
    public static final void m1038onSubscriptionFailedToCancel$lambda4(SubscribeFragment subscribeFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(subscribeFragment, "this$0");
        dialogInterface.dismiss();
        FragmentKt.findNavController(subscribeFragment).popBackStack();
    }

    /* compiled from: SubscribeFragment.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeFragment$Companion;", "", "()V", "FETCH_SUBSCRIPTION_TOKEN_REQUEST_CODE", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
