package org.thoughtcrime.securesms.components.settings.conversation.sounds.custom;

import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts$StartActivityForResult;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsViewModel;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.ConversationUtil;
import org.thoughtcrime.securesms.util.RingtoneUtil;

/* compiled from: CustomNotificationsSettingsFragment.kt */
@Metadata(d1 = {"\u0000p\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\u0018\u0010\u001d\u001a\n \u001f*\u0004\u0018\u00010\u001e0\u001e2\u0006\u0010 \u001a\u00020!H\u0002J$\u0010\"\u001a\u00020\t2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010\u001e2\b\u0010&\u001a\u0004\u0018\u00010\u001eH\u0002J&\u0010'\u001a\u00020\u00142\u0006\u0010(\u001a\u00020)2\u0014\u0010*\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u001e\u0012\u0004\u0012\u00020\u00140+H\u0002J\b\u0010,\u001a\u00020\u0014H\u0016J\u001a\u0010-\u001a\u00020\u00142\b\u0010.\u001a\u0004\u0018\u00010\u001e2\u0006\u0010 \u001a\u00020!H\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X.¢\u0006\u0002\n\u0000R\u0014\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X.¢\u0006\u0002\n\u0000R!\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b8BX\u0002¢\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000bR\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\r\u001a\u0004\b\u0010\u0010\u0011¨\u0006/"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "callSoundResultLauncher", "Landroidx/activity/result/ActivityResultLauncher;", "Landroid/content/Intent;", "messageSoundResultLauncher", "vibrateLabels", "", "", "getVibrateLabels", "()[Ljava/lang/String;", "vibrateLabels$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "createFactory", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsViewModel$Factory;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsState;", "getDefaultSound", "Landroid/net/Uri;", "kotlin.jvm.PlatformType", "forCalls", "", "getRingtoneSummary", "context", "Landroid/content/Context;", "ringtone", "defaultNotificationUri", "handleResult", MediaSendActivityResult.EXTRA_RESULT, "Landroidx/activity/result/ActivityResult;", "resultHandler", "Lkotlin/Function1;", "onResume", "requestSound", "current", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomNotificationsSettingsFragment extends DSLSettingsFragment {
    private ActivityResultLauncher<Intent> callSoundResultLauncher;
    private ActivityResultLauncher<Intent> messageSoundResultLauncher;
    private final Lazy vibrateLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$vibrateLabels$2
        final /* synthetic */ CustomNotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.recipient_vibrate_entries);
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(CustomNotificationsSettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<CustomNotificationsSettingsViewModel.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final CustomNotificationsSettingsViewModel.Factory invoke() {
            return CustomNotificationsSettingsFragment.access$createFactory((CustomNotificationsSettingsFragment) this.receiver);
        }
    });

    public CustomNotificationsSettingsFragment() {
        super(R.string.CustomNotificationsDialogFragment__custom_notifications, 0, 0, null, 14, null);
    }

    public final String[] getVibrateLabels() {
        Object value = this.vibrateLabels$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-vibrateLabels>(...)");
        return (String[]) value;
    }

    public final CustomNotificationsSettingsViewModel getViewModel() {
        return (CustomNotificationsSettingsViewModel) this.viewModel$delegate.getValue();
    }

    public final CustomNotificationsSettingsViewModel.Factory createFactory() {
        RecipientId recipientId = CustomNotificationsSettingsFragmentArgs.fromBundle(requireArguments()).getRecipientId();
        Intrinsics.checkNotNullExpressionValue(recipientId, "fromBundle(requireArguments()).recipientId");
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        return new CustomNotificationsSettingsViewModel.Factory(recipientId, new CustomNotificationsSettingsRepository(requireContext));
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().channelConsistencyCheck();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ActivityResultLauncher<Intent> registerForActivityResult = registerForActivityResult(new ActivityResultContracts$StartActivityForResult(), new ActivityResultCallback() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$$ExternalSyntheticLambda0
            @Override // androidx.activity.result.ActivityResultCallback
            public final void onActivityResult(Object obj) {
                CustomNotificationsSettingsFragment.$r8$lambda$ZZ0OWwTYvYmbaNEaTkc6Wz0gnNU(CustomNotificationsSettingsFragment.this, (ActivityResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…l::setMessageSound)\n    }");
        this.messageSoundResultLauncher = registerForActivityResult;
        ActivityResultLauncher<Intent> registerForActivityResult2 = registerForActivityResult(new ActivityResultContracts$StartActivityForResult(), new ActivityResultCallback() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$$ExternalSyntheticLambda1
            @Override // androidx.activity.result.ActivityResultCallback
            public final void onActivityResult(Object obj) {
                CustomNotificationsSettingsFragment.$r8$lambda$6DzVtQPUyOvDwHkWgxexM7gw3OE(CustomNotificationsSettingsFragment.this, (ActivityResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(registerForActivityResult2, "registerForActivityResul…odel::setCallSound)\n    }");
        this.callSoundResultLauncher = registerForActivityResult2;
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ CustomNotificationsSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CustomNotificationsSettingsFragment.$r8$lambda$SwBA57HJ8F29RoIVf_mmI1gB7wc(DSLSettingsAdapter.this, this.f$1, (CustomNotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m1230bindAdapter$lambda0(CustomNotificationsSettingsFragment customNotificationsSettingsFragment, ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter(customNotificationsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(activityResult, MediaSendActivityResult.EXTRA_RESULT);
        customNotificationsSettingsFragment.handleResult(activityResult, new Function1<Uri, Unit>(customNotificationsSettingsFragment.getViewModel()) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$bindAdapter$1$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Uri uri) {
                invoke(uri);
                return Unit.INSTANCE;
            }

            public final void invoke(Uri uri) {
                ((CustomNotificationsSettingsViewModel) this.receiver).setMessageSound(uri);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m1231bindAdapter$lambda1(CustomNotificationsSettingsFragment customNotificationsSettingsFragment, ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter(customNotificationsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(activityResult, MediaSendActivityResult.EXTRA_RESULT);
        customNotificationsSettingsFragment.handleResult(activityResult, new Function1<Uri, Unit>(customNotificationsSettingsFragment.getViewModel()) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$bindAdapter$2$1
            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(Uri uri) {
                invoke(uri);
                return Unit.INSTANCE;
            }

            public final void invoke(Uri uri) {
                ((CustomNotificationsSettingsViewModel) this.receiver).setCallSound(uri);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m1232bindAdapter$lambda2(DSLSettingsAdapter dSLSettingsAdapter, CustomNotificationsSettingsFragment customNotificationsSettingsFragment, CustomNotificationsSettingsState customNotificationsSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(customNotificationsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(customNotificationsSettingsState, "state");
        dSLSettingsAdapter.submitList(customNotificationsSettingsFragment.getConfiguration(customNotificationsSettingsState).toMappingModelList());
    }

    private final void handleResult(ActivityResult activityResult, Function1<? super Uri, Unit> function1) {
        int resultCode = activityResult.getResultCode();
        Intent data = activityResult.getData();
        if (resultCode == -1 && data != null) {
            function1.invoke((Uri) data.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI"));
        }
    }

    private final DSLConfiguration getConfiguration(CustomNotificationsSettingsState customNotificationsSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(customNotificationsSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1
            final /* synthetic */ CustomNotificationsSettingsState $state;
            final /* synthetic */ CustomNotificationsSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.CustomNotificationsDialogFragment__messages);
                if (NotificationChannels.supported()) {
                    DSLSettingsText from = DSLSettingsText.Companion.from(R.string.CustomNotificationsDialogFragment__use_custom_notifications, new DSLSettingsText.Modifier[0]);
                    boolean isInitialLoadComplete = this.$state.isInitialLoadComplete();
                    boolean hasCustomNotifications = this.$state.getHasCustomNotifications();
                    final CustomNotificationsSettingsFragment customNotificationsSettingsFragment = this.this$0;
                    final CustomNotificationsSettingsState customNotificationsSettingsState2 = this.$state;
                    dSLConfiguration.switchPref(from, (r16 & 2) != 0 ? null : null, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : isInitialLoadComplete, hasCustomNotifications, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.1
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            CustomNotificationsSettingsFragment.access$getViewModel(customNotificationsSettingsFragment).setHasCustomNotifications(!customNotificationsSettingsState2.getHasCustomNotifications());
                        }
                    });
                }
                if (Build.VERSION.SDK_INT >= 30) {
                    DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                    DSLSettingsText from2 = companion.from(R.string.CustomNotificationsDialogFragment__customize, new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from3 = companion.from(R.string.CustomNotificationsDialogFragment__change_sound_and_vibration, new DSLSettingsText.Modifier[0]);
                    boolean controlsEnabled = this.$state.getControlsEnabled();
                    final CustomNotificationsSettingsFragment customNotificationsSettingsFragment2 = this.this$0;
                    final CustomNotificationsSettingsState customNotificationsSettingsState3 = this.$state;
                    dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : from3, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : controlsEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.2
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            Context requireContext = customNotificationsSettingsFragment2.requireContext();
                            Recipient recipient = customNotificationsSettingsState3.getRecipient();
                            Intrinsics.checkNotNull(recipient);
                            String notificationChannel = recipient.getNotificationChannel();
                            Intrinsics.checkNotNull(notificationChannel);
                            NotificationChannels.openChannelSettings(requireContext, notificationChannel, ConversationUtil.getShortcutId(customNotificationsSettingsState3.getRecipient()));
                        }
                    }, (r18 & 64) != 0 ? null : null);
                } else {
                    DSLSettingsText.Companion companion2 = DSLSettingsText.Companion;
                    DSLSettingsText from4 = companion2.from(R.string.CustomNotificationsDialogFragment__notification_sound, new DSLSettingsText.Modifier[0]);
                    CustomNotificationsSettingsFragment customNotificationsSettingsFragment3 = this.this$0;
                    Context requireContext = customNotificationsSettingsFragment3.requireContext();
                    Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                    DSLSettingsText from5 = companion2.from(CustomNotificationsSettingsFragment.access$getRingtoneSummary(customNotificationsSettingsFragment3, requireContext, this.$state.getMessageSound(), Settings.System.DEFAULT_NOTIFICATION_URI), new DSLSettingsText.Modifier[0]);
                    boolean controlsEnabled2 = this.$state.getControlsEnabled();
                    final CustomNotificationsSettingsFragment customNotificationsSettingsFragment4 = this.this$0;
                    final CustomNotificationsSettingsState customNotificationsSettingsState4 = this.$state;
                    dSLConfiguration.clickPref(from4, (r18 & 2) != 0 ? null : from5, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : controlsEnabled2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.3
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            CustomNotificationsSettingsFragment.access$requestSound(customNotificationsSettingsFragment4, customNotificationsSettingsState4.getMessageSound(), false);
                        }
                    }, (r18 & 64) != 0 ? null : null);
                    if (NotificationChannels.supported()) {
                        DSLSettingsText from6 = companion2.from(R.string.CustomNotificationsDialogFragment__vibrate, new DSLSettingsText.Modifier[0]);
                        boolean controlsEnabled3 = this.$state.getControlsEnabled();
                        boolean messageVibrateEnabled = this.$state.getMessageVibrateEnabled();
                        final CustomNotificationsSettingsFragment customNotificationsSettingsFragment5 = this.this$0;
                        final CustomNotificationsSettingsState customNotificationsSettingsState5 = this.$state;
                        dSLConfiguration.switchPref(from6, (r16 & 2) != 0 ? null : null, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : controlsEnabled3, messageVibrateEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.4
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                CustomNotificationsSettingsFragment.access$getViewModel(customNotificationsSettingsFragment5).setMessageVibrate(RecipientDatabase.VibrateState.Companion.fromBoolean(!customNotificationsSettingsState5.getMessageVibrateEnabled()));
                            }
                        });
                    } else {
                        DSLSettingsText from7 = companion2.from(R.string.CustomNotificationsDialogFragment__vibrate, new DSLSettingsText.Modifier[0]);
                        boolean controlsEnabled4 = this.$state.getControlsEnabled();
                        String[] access$getVibrateLabels = CustomNotificationsSettingsFragment.access$getVibrateLabels(this.this$0);
                        int id = this.$state.getMessageVibrateState().getId();
                        final CustomNotificationsSettingsFragment customNotificationsSettingsFragment6 = this.this$0;
                        dSLConfiguration.radioListPref(from7, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from7 : null, (r19 & 8) != 0 ? true : controlsEnabled4, access$getVibrateLabels, id, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.5
                            /* Return type fixed from 'java.lang.Object' to match base method */
                            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                            @Override // kotlin.jvm.functions.Function1
                            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                                invoke(num.intValue());
                                return Unit.INSTANCE;
                            }

                            public final void invoke(int i) {
                                CustomNotificationsSettingsFragment.access$getViewModel(customNotificationsSettingsFragment6).setMessageVibrate(RecipientDatabase.VibrateState.Companion.fromId(i));
                            }
                        });
                    }
                }
                if (this.$state.getShowCallingOptions()) {
                    dSLConfiguration.dividerPref();
                    dSLConfiguration.sectionHeaderPref(R.string.CustomNotificationsDialogFragment__call_settings);
                    DSLSettingsText.Companion companion3 = DSLSettingsText.Companion;
                    DSLSettingsText from8 = companion3.from(R.string.CustomNotificationsDialogFragment__ringtone, new DSLSettingsText.Modifier[0]);
                    CustomNotificationsSettingsFragment customNotificationsSettingsFragment7 = this.this$0;
                    Context requireContext2 = customNotificationsSettingsFragment7.requireContext();
                    Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
                    DSLSettingsText from9 = companion3.from(CustomNotificationsSettingsFragment.access$getRingtoneSummary(customNotificationsSettingsFragment7, requireContext2, this.$state.getCallSound(), Settings.System.DEFAULT_RINGTONE_URI), new DSLSettingsText.Modifier[0]);
                    boolean controlsEnabled5 = this.$state.getControlsEnabled();
                    final CustomNotificationsSettingsFragment customNotificationsSettingsFragment8 = this.this$0;
                    final CustomNotificationsSettingsState customNotificationsSettingsState6 = this.$state;
                    dSLConfiguration.clickPref(from8, (r18 & 2) != 0 ? null : from9, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : controlsEnabled5, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.6
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            CustomNotificationsSettingsFragment.access$requestSound(customNotificationsSettingsFragment8, customNotificationsSettingsState6.getCallSound(), true);
                        }
                    }, (r18 & 64) != 0 ? null : null);
                    DSLSettingsText from10 = companion3.from(R.string.CustomNotificationsDialogFragment__vibrate, new DSLSettingsText.Modifier[0]);
                    boolean controlsEnabled6 = this.$state.getControlsEnabled();
                    String[] access$getVibrateLabels2 = CustomNotificationsSettingsFragment.access$getVibrateLabels(this.this$0);
                    int id2 = this.$state.getCallVibrateState().getId();
                    final CustomNotificationsSettingsFragment customNotificationsSettingsFragment9 = this.this$0;
                    dSLConfiguration.radioListPref(from10, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from10 : null, (r19 & 8) != 0 ? true : controlsEnabled6, access$getVibrateLabels2, id2, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsFragment$getConfiguration$1.7
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                            invoke(num.intValue());
                            return Unit.INSTANCE;
                        }

                        public final void invoke(int i) {
                            CustomNotificationsSettingsFragment.access$getViewModel(customNotificationsSettingsFragment9).setCallVibrate(RecipientDatabase.VibrateState.Companion.fromId(i));
                        }
                    });
                }
            }
        });
    }

    public final String getRingtoneSummary(Context context, Uri uri, Uri uri2) {
        if (uri == null || Intrinsics.areEqual(uri, uri2)) {
            String string = context.getString(R.string.CustomNotificationsDialogFragment__default);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…sDialogFragment__default)");
            return string;
        }
        String uri3 = uri.toString();
        Intrinsics.checkNotNullExpressionValue(uri3, "ringtone.toString()");
        if (uri3.length() == 0) {
            String string2 = context.getString(R.string.preferences__silent);
            Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.string.preferences__silent)");
            return string2;
        }
        Ringtone ringtone = RingtoneUtil.getRingtone(requireContext(), uri);
        if (ringtone != null) {
            try {
                String title = ringtone.getTitle(context);
                Intrinsics.checkNotNullExpressionValue(title, "{\n          tone.getTitle(context)\n        }");
                return title;
            } catch (NullPointerException e) {
                Log.w(CustomNotificationsSettingsFragmentKt.TAG, "Could not get correct title for ringtone.", e);
                String string3 = context.getString(R.string.CustomNotificationsDialogFragment__unknown);
                Intrinsics.checkNotNullExpressionValue(string3, "{\n          Log.w(TAG, \"…gment__unknown)\n        }");
                return string3;
            } catch (SecurityException e2) {
                Log.w(CustomNotificationsSettingsFragmentKt.TAG, "Could not get correct title for ringtone.", e2);
                String string4 = context.getString(R.string.CustomNotificationsDialogFragment__unknown);
                Intrinsics.checkNotNullExpressionValue(string4, "{\n          Log.w(TAG, \"…gment__unknown)\n        }");
                return string4;
            }
        } else {
            String string5 = context.getString(R.string.CustomNotificationsDialogFragment__default);
            Intrinsics.checkNotNullExpressionValue(string5, "context.getString(R.stri…sDialogFragment__default)");
            return string5;
        }
    }

    public final void requestSound(Uri uri, boolean z) {
        ActivityResultLauncher<Intent> activityResultLauncher = null;
        int i = 1;
        if (uri == null) {
            uri = getDefaultSound(z);
        } else {
            String uri2 = uri.toString();
            Intrinsics.checkNotNullExpressionValue(uri2, "current.toString()");
            if (uri2.length() == 0) {
                uri = null;
            }
        }
        Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
        intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", true);
        intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", true);
        if (!z) {
            i = 2;
        }
        intent.putExtra("android.intent.extra.ringtone.TYPE", i);
        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", uri);
        if (z) {
            ActivityResultLauncher<Intent> activityResultLauncher2 = this.callSoundResultLauncher;
            if (activityResultLauncher2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("callSoundResultLauncher");
            } else {
                activityResultLauncher = activityResultLauncher2;
            }
            activityResultLauncher.launch(intent);
            return;
        }
        ActivityResultLauncher<Intent> activityResultLauncher3 = this.messageSoundResultLauncher;
        if (activityResultLauncher3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("messageSoundResultLauncher");
        } else {
            activityResultLauncher = activityResultLauncher3;
        }
        activityResultLauncher.launch(intent);
    }

    private final Uri getDefaultSound(boolean z) {
        return z ? Settings.System.DEFAULT_RINGTONE_URI : Settings.System.DEFAULT_NOTIFICATION_URI;
    }
}
