package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;

/* compiled from: DonationReceiptListPageState.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0006HÆ\u0003J#\u0010\r\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0006HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0010\u001a\u00020\u0011HÖ\u0001J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\bR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageState;", "", "records", "", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "isLoaded", "", "(Ljava/util/List;Z)V", "()Z", "getRecords", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListPageState {
    private final boolean isLoaded;
    private final List<DonationReceiptRecord> records;

    public DonationReceiptListPageState() {
        this(null, false, 3, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ DonationReceiptListPageState copy$default(DonationReceiptListPageState donationReceiptListPageState, List list, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            list = donationReceiptListPageState.records;
        }
        if ((i & 2) != 0) {
            z = donationReceiptListPageState.isLoaded;
        }
        return donationReceiptListPageState.copy(list, z);
    }

    public final List<DonationReceiptRecord> component1() {
        return this.records;
    }

    public final boolean component2() {
        return this.isLoaded;
    }

    public final DonationReceiptListPageState copy(List<DonationReceiptRecord> list, boolean z) {
        Intrinsics.checkNotNullParameter(list, "records");
        return new DonationReceiptListPageState(list, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DonationReceiptListPageState)) {
            return false;
        }
        DonationReceiptListPageState donationReceiptListPageState = (DonationReceiptListPageState) obj;
        return Intrinsics.areEqual(this.records, donationReceiptListPageState.records) && this.isLoaded == donationReceiptListPageState.isLoaded;
    }

    public int hashCode() {
        int hashCode = this.records.hashCode() * 31;
        boolean z = this.isLoaded;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "DonationReceiptListPageState(records=" + this.records + ", isLoaded=" + this.isLoaded + ')';
    }

    public DonationReceiptListPageState(List<DonationReceiptRecord> list, boolean z) {
        Intrinsics.checkNotNullParameter(list, "records");
        this.records = list;
        this.isLoaded = z;
    }

    public /* synthetic */ DonationReceiptListPageState(List list, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 2) != 0 ? false : z);
    }

    public final List<DonationReceiptRecord> getRecords() {
        return this.records;
    }

    public final boolean isLoaded() {
        return this.isLoaded;
    }
}
