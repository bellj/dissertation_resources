package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment;
import org.thoughtcrime.securesms.groups.GroupId;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda2 implements Function {
    public final /* synthetic */ Long f$0;
    public final /* synthetic */ GroupId f$1;

    public /* synthetic */ InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda2(Long l, GroupId groupId) {
        this.f$0 = l;
        this.f$1 = groupId;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return InternalConversationSettingsFragment.InternalViewModel.m1165lambda2$lambda1(this.f$0, this.f$1, (InternalConversationSettingsFragment.InternalState) obj);
    }
}
