package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import com.google.android.material.tabs.TabLayout;
import java.util.List;

/* loaded from: classes4.dex */
public class ControllableTabLayout extends TabLayout {
    private NewTabListener newTabListener;
    private List<View> touchables;

    /* loaded from: classes4.dex */
    public interface NewTabListener {
        void onNewTab(TabLayout.Tab tab);
    }

    public ControllableTabLayout(Context context) {
        super(context);
    }

    public ControllableTabLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public ControllableTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        if (isEnabled() && !z) {
            this.touchables = getTouchables();
        }
        for (View view : this.touchables) {
            view.setClickable(z);
        }
        super.setEnabled(z);
    }

    public void setNewTabListener(NewTabListener newTabListener) {
        this.newTabListener = newTabListener;
    }

    @Override // com.google.android.material.tabs.TabLayout
    public TabLayout.Tab newTab() {
        TabLayout.Tab newTab = super.newTab();
        NewTabListener newTabListener = this.newTabListener;
        if (newTabListener != null) {
            newTabListener.onNewTab(newTab);
        }
        return newTab;
    }
}
