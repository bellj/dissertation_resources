package org.thoughtcrime.securesms.components.voice;

import kotlin.Metadata;

/* compiled from: VoiceNotePlayerView.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0003XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"ANIMATE_DURATION", "", "TO_PAUSE", "", "TO_PLAY", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNotePlayerViewKt {
    private static final long ANIMATE_DURATION;
    private static final int TO_PAUSE;
    private static final int TO_PLAY;
}
