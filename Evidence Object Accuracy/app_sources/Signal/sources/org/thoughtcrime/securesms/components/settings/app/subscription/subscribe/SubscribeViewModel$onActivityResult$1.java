package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import android.app.Application;
import com.annimon.stream.function.Function;
import com.google.android.gms.wallet.PaymentData;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.CompletableSource;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.donations.GooglePayApi;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.subscription.Subscription;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0016¨\u0006\n"}, d2 = {"org/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeViewModel$onActivityResult$1", "Lorg/signal/donations/GooglePayApi$PaymentRequestCallback;", "onCancelled", "", "onError", "googlePayException", "Lorg/signal/donations/GooglePayApi$GooglePayException;", "onSuccess", "paymentData", "Lcom/google/android/gms/wallet/PaymentData;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$onActivityResult$1 implements GooglePayApi.PaymentRequestCallback {
    final /* synthetic */ Subscription $subscription;
    final /* synthetic */ SubscribeViewModel this$0;

    public SubscribeViewModel$onActivityResult$1(Subscription subscription, SubscribeViewModel subscribeViewModel) {
        this.$subscription = subscription;
        this.this$0 = subscribeViewModel;
    }

    @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
    public void onSuccess(PaymentData paymentData) {
        Intrinsics.checkNotNullParameter(paymentData, "paymentData");
        if (this.$subscription != null) {
            this.this$0.eventPublisher.onNext(DonationEvent.RequestTokenSuccess.INSTANCE);
            Completable ensureSubscriberId = this.this$0.donationPaymentRepository.ensureSubscriberId();
            Completable continueSubscriptionSetup = this.this$0.donationPaymentRepository.continueSubscriptionSetup(paymentData);
            Completable subscriptionLevel = this.this$0.donationPaymentRepository.setSubscriptionLevel(String.valueOf(this.$subscription.getLevel()));
            this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$onActivityResult$1$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return SubscribeViewModel$onActivityResult$1.m1066onSuccess$lambda0((SubscribeState) obj);
                }
            });
            Completable andThen = ensureSubscriberId.andThen(this.this$0.cancelActiveSubscriptionIfNecessary()).andThen(continueSubscriptionSetup).onErrorResumeNext(new io.reactivex.rxjava3.functions.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$onActivityResult$1$$ExternalSyntheticLambda1
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return SubscribeViewModel$onActivityResult$1.m1067onSuccess$lambda1((Throwable) obj);
                }
            }).andThen(subscriptionLevel);
            Intrinsics.checkNotNullExpressionValue(andThen, "setup.andThen(setLevel)");
            SubscribersKt.subscribeBy(andThen, new SubscribeViewModel$onActivityResult$1$onSuccess$2(this.this$0), new SubscribeViewModel$onActivityResult$1$onSuccess$3(this.this$0, this.$subscription));
            return;
        }
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$onActivityResult$1$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$onActivityResult$1.m1068onSuccess$lambda2((SubscribeState) obj);
            }
        });
    }

    /* renamed from: onSuccess$lambda-0 */
    public static final SubscribeState m1066onSuccess$lambda0(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.PAYMENT_PIPELINE, false, 95, null);
    }

    /* renamed from: onSuccess$lambda-1 */
    public static final CompletableSource m1067onSuccess$lambda1(Throwable th) {
        DonationError.Companion companion = DonationError.Companion;
        DonationErrorSource donationErrorSource = DonationErrorSource.SUBSCRIPTION;
        Intrinsics.checkNotNullExpressionValue(th, "it");
        return Completable.error(companion.getPaymentSetupError(donationErrorSource, th));
    }

    /* renamed from: onSuccess$lambda-2 */
    public static final SubscribeState m1068onSuccess$lambda2(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.READY, false, 95, null);
    }

    /* renamed from: onError$lambda-3 */
    public static final SubscribeState m1065onError$lambda3(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.READY, false, 95, null);
    }

    @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
    public void onError(GooglePayApi.GooglePayException googlePayException) {
        Intrinsics.checkNotNullParameter(googlePayException, "googlePayException");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$onActivityResult$1$$ExternalSyntheticLambda4
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$onActivityResult$1.m1065onError$lambda3((SubscribeState) obj);
            }
        });
        DonationError.Companion companion = DonationError.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        companion.routeDonationError(application, companion.getGooglePayRequestTokenError(DonationErrorSource.SUBSCRIPTION, googlePayException));
    }

    /* renamed from: onCancelled$lambda-4 */
    public static final SubscribeState m1064onCancelled$lambda4(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.READY, false, 95, null);
    }

    @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
    public void onCancelled() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$onActivityResult$1$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$onActivityResult$1.m1064onCancelled$lambda4((SubscribeState) obj);
            }
        });
    }
}
