package org.thoughtcrime.securesms.components.settings.app.data;

import android.content.SharedPreferences;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.preference.PreferenceManager;
import java.util.ArrayList;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsViewModel;
import org.thoughtcrime.securesms.mms.SentMediaQuality;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.webrtc.CallBandwidthMode;

/* compiled from: DataAndStorageSettingsFragment.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0016J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\b\u0010\u001e\u001a\u00020\u0017H\u0016R?\u0010\u0003\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR?\u0010\u000b\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\f\u0010\bR?\u0010\u000e\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0010\u0010\n\u001a\u0004\b\u000f\u0010\bR?\u0010\u0011\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\n\u001a\u0004\b\u0012\u0010\bR\u000e\u0010\u0014\u001a\u00020\u0015X.¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "autoDownloadLabels", "", "", "kotlin.jvm.PlatformType", "getAutoDownloadLabels", "()[Ljava/lang/String;", "autoDownloadLabels$delegate", "Lkotlin/Lazy;", "autoDownloadValues", "getAutoDownloadValues", "autoDownloadValues$delegate", "callBandwidthLabels", "getCallBandwidthLabels", "callBandwidthLabels$delegate", "sentMediaQualityLabels", "getSentMediaQualityLabels", "sentMediaQualityLabels$delegate", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsState;", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DataAndStorageSettingsFragment extends DSLSettingsFragment {
    private final Lazy autoDownloadLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$autoDownloadLabels$2
        final /* synthetic */ DataAndStorageSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_media_download_values);
        }
    });
    private final Lazy autoDownloadValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$autoDownloadValues$2
        final /* synthetic */ DataAndStorageSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_media_download_entries);
        }
    });
    private final Lazy callBandwidthLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$callBandwidthLabels$2
        final /* synthetic */ DataAndStorageSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_data_and_storage_call_bandwidth_values);
        }
    });
    private final Lazy sentMediaQualityLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$sentMediaQualityLabels$2
        final /* synthetic */ DataAndStorageSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return SentMediaQuality.getLabels(this.this$0.requireContext());
        }
    });
    private DataAndStorageSettingsViewModel viewModel;

    public DataAndStorageSettingsFragment() {
        super(R.string.preferences__data_and_storage, 0, 0, null, 14, null);
    }

    public final String[] getAutoDownloadValues() {
        return (String[]) this.autoDownloadValues$delegate.getValue();
    }

    public final String[] getAutoDownloadLabels() {
        return (String[]) this.autoDownloadLabels$delegate.getValue();
    }

    public final String[] getSentMediaQualityLabels() {
        return (String[]) this.sentMediaQualityLabels$delegate.getValue();
    }

    public final String[] getCallBandwidthLabels() {
        return (String[]) this.callBandwidthLabels$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel = this.viewModel;
        if (dataAndStorageSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            dataAndStorageSettingsViewModel = null;
        }
        dataAndStorageSettingsViewModel.refresh();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        DataAndStorageSettingsRepository dataAndStorageSettingsRepository = new DataAndStorageSettingsRepository();
        Intrinsics.checkNotNullExpressionValue(defaultSharedPreferences, "preferences");
        ViewModel viewModel = new ViewModelProvider(this, new DataAndStorageSettingsViewModel.Factory(defaultSharedPreferences, dataAndStorageSettingsRepository)).get(DataAndStorageSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ngsViewModel::class.java]");
        DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel = (DataAndStorageSettingsViewModel) viewModel;
        this.viewModel = dataAndStorageSettingsViewModel;
        if (dataAndStorageSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            dataAndStorageSettingsViewModel = null;
        }
        dataAndStorageSettingsViewModel.m649getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DataAndStorageSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DataAndStorageSettingsFragment.$r8$lambda$mHs8dadhQiW31Y9rjAiE7oQbIC4(DSLSettingsAdapter.this, this.f$1, (DataAndStorageSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m645bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, DataAndStorageSettingsFragment dataAndStorageSettingsFragment, DataAndStorageSettingsState dataAndStorageSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(dataAndStorageSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(dataAndStorageSettingsState, "it");
        dSLSettingsAdapter.submitList(dataAndStorageSettingsFragment.getConfiguration(dataAndStorageSettingsState).toMappingModelList());
    }

    public final DSLConfiguration getConfiguration(DataAndStorageSettingsState dataAndStorageSettingsState) {
        Intrinsics.checkNotNullParameter(dataAndStorageSettingsState, "state");
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(dataAndStorageSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1
            final /* synthetic */ DataAndStorageSettingsState $state;
            final /* synthetic */ DataAndStorageSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.preferences_data_and_storage__manage_storage, new DSLSettingsText.Modifier[0]);
                String prettyFileSize = Util.getPrettyFileSize(this.$state.getTotalStorageUse());
                Intrinsics.checkNotNullExpressionValue(prettyFileSize, "getPrettyFileSize(state.totalStorageUse)");
                DSLSettingsText from2 = companion.from(prettyFileSize, new DSLSettingsText.Modifier[0]);
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : from2, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(dataAndStorageSettingsFragment.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_dataAndStorageSettingsFragment_to_storagePreferenceFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.preferences_chats__media_auto_download);
                DSLSettingsText from3 = companion.from(R.string.preferences_chats__when_using_mobile_data, new DSLSettingsText.Modifier[0]);
                String[] access$getAutoDownloadLabels = DataAndStorageSettingsFragment.access$getAutoDownloadLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getAutoDownloadLabels, "autoDownloadLabels");
                String[] access$getAutoDownloadValues = DataAndStorageSettingsFragment.access$getAutoDownloadValues(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getAutoDownloadValues, "autoDownloadValues");
                DataAndStorageSettingsState dataAndStorageSettingsState2 = this.$state;
                ArrayList arrayList = new ArrayList(access$getAutoDownloadValues.length);
                for (String str : access$getAutoDownloadValues) {
                    arrayList.add(Boolean.valueOf(dataAndStorageSettingsState2.getMobileAutoDownloadValues().contains(str)));
                }
                boolean[] zArr = CollectionsKt___CollectionsKt.toBooleanArray(arrayList);
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment2 = this.this$0;
                dSLConfiguration.multiSelectPref(from3, (r12 & 2) != 0, access$getAutoDownloadLabels, zArr, new Function1<boolean[], Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.3
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(boolean[] zArr2) {
                        invoke(zArr2);
                        return Unit.INSTANCE;
                    }

                    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001f */
                    /* JADX DEBUG: Multi-variable search result rejected for r5v4, resolved type: java.lang.String[] */
                    /* JADX DEBUG: Multi-variable search result rejected for r5v5, resolved type: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsViewModel */
                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r5v3 */
                    public final void invoke(boolean[] zArr2) {
                        DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel;
                        Intrinsics.checkNotNullParameter(zArr2, "it");
                        DataAndStorageSettingsFragment dataAndStorageSettingsFragment3 = dataAndStorageSettingsFragment2;
                        ArrayList arrayList2 = new ArrayList(zArr2.length);
                        int length = zArr2.length;
                        int i = 0;
                        int i2 = 0;
                        while (true) {
                            dataAndStorageSettingsViewModel = 0;
                            if (i >= length) {
                                break;
                            }
                            i2++;
                            if (zArr2[i]) {
                                dataAndStorageSettingsViewModel = DataAndStorageSettingsFragment.access$getAutoDownloadValues(dataAndStorageSettingsFragment3)[i2];
                            }
                            arrayList2.add(dataAndStorageSettingsViewModel == true ? 1 : 0);
                            i++;
                        }
                        Set<String> set = CollectionsKt___CollectionsKt.toSet(CollectionsKt___CollectionsKt.filterNotNull(arrayList2));
                        DataAndStorageSettingsViewModel access$getViewModel$p = DataAndStorageSettingsFragment.access$getViewModel$p(dataAndStorageSettingsFragment2);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                        } else {
                            dataAndStorageSettingsViewModel = access$getViewModel$p;
                        }
                        dataAndStorageSettingsViewModel.setMobileAutoDownloadValues(set);
                    }
                });
                DSLSettingsText from4 = DSLSettingsText.Companion.from(R.string.preferences_chats__when_using_wifi, new DSLSettingsText.Modifier[0]);
                String[] access$getAutoDownloadLabels2 = DataAndStorageSettingsFragment.access$getAutoDownloadLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getAutoDownloadLabels2, "autoDownloadLabels");
                String[] access$getAutoDownloadValues2 = DataAndStorageSettingsFragment.access$getAutoDownloadValues(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getAutoDownloadValues2, "autoDownloadValues");
                DataAndStorageSettingsState dataAndStorageSettingsState3 = this.$state;
                ArrayList arrayList2 = new ArrayList(access$getAutoDownloadValues2.length);
                for (String str2 : access$getAutoDownloadValues2) {
                    arrayList2.add(Boolean.valueOf(dataAndStorageSettingsState3.getWifiAutoDownloadValues().contains(str2)));
                }
                boolean[] zArr2 = CollectionsKt___CollectionsKt.toBooleanArray(arrayList2);
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment3 = this.this$0;
                dSLConfiguration.multiSelectPref(from4, (r12 & 2) != 0, access$getAutoDownloadLabels2, zArr2, new Function1<boolean[], Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.5
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(boolean[] zArr3) {
                        invoke(zArr3);
                        return Unit.INSTANCE;
                    }

                    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001f */
                    /* JADX DEBUG: Multi-variable search result rejected for r5v4, resolved type: java.lang.String[] */
                    /* JADX DEBUG: Multi-variable search result rejected for r5v5, resolved type: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsViewModel */
                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r5v3 */
                    public final void invoke(boolean[] zArr3) {
                        DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel;
                        Intrinsics.checkNotNullParameter(zArr3, "it");
                        DataAndStorageSettingsFragment dataAndStorageSettingsFragment4 = dataAndStorageSettingsFragment3;
                        ArrayList arrayList3 = new ArrayList(zArr3.length);
                        int length = zArr3.length;
                        int i = 0;
                        int i2 = 0;
                        while (true) {
                            dataAndStorageSettingsViewModel = 0;
                            if (i >= length) {
                                break;
                            }
                            i2++;
                            if (zArr3[i]) {
                                dataAndStorageSettingsViewModel = DataAndStorageSettingsFragment.access$getAutoDownloadValues(dataAndStorageSettingsFragment4)[i2];
                            }
                            arrayList3.add(dataAndStorageSettingsViewModel == true ? 1 : 0);
                            i++;
                        }
                        Set<String> set = CollectionsKt___CollectionsKt.toSet(CollectionsKt___CollectionsKt.filterNotNull(arrayList3));
                        DataAndStorageSettingsViewModel access$getViewModel$p = DataAndStorageSettingsFragment.access$getViewModel$p(dataAndStorageSettingsFragment3);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                        } else {
                            dataAndStorageSettingsViewModel = access$getViewModel$p;
                        }
                        dataAndStorageSettingsViewModel.setWifiAutoDownloadValues(set);
                    }
                });
                DSLSettingsText from5 = DSLSettingsText.Companion.from(R.string.preferences_chats__when_roaming, new DSLSettingsText.Modifier[0]);
                String[] access$getAutoDownloadLabels3 = DataAndStorageSettingsFragment.access$getAutoDownloadLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getAutoDownloadLabels3, "autoDownloadLabels");
                String[] access$getAutoDownloadValues3 = DataAndStorageSettingsFragment.access$getAutoDownloadValues(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getAutoDownloadValues3, "autoDownloadValues");
                DataAndStorageSettingsState dataAndStorageSettingsState4 = this.$state;
                ArrayList arrayList3 = new ArrayList(access$getAutoDownloadValues3.length);
                for (String str3 : access$getAutoDownloadValues3) {
                    arrayList3.add(Boolean.valueOf(dataAndStorageSettingsState4.getRoamingAutoDownloadValues().contains(str3)));
                }
                boolean[] zArr3 = CollectionsKt___CollectionsKt.toBooleanArray(arrayList3);
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment4 = this.this$0;
                dSLConfiguration.multiSelectPref(from5, (r12 & 2) != 0, access$getAutoDownloadLabels3, zArr3, new Function1<boolean[], Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.7
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(boolean[] zArr4) {
                        invoke(zArr4);
                        return Unit.INSTANCE;
                    }

                    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:17:0x001f */
                    /* JADX DEBUG: Multi-variable search result rejected for r5v4, resolved type: java.lang.String[] */
                    /* JADX DEBUG: Multi-variable search result rejected for r5v5, resolved type: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsViewModel */
                    /* JADX WARN: Multi-variable type inference failed */
                    /* JADX WARN: Type inference failed for: r5v3 */
                    public final void invoke(boolean[] zArr4) {
                        DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel;
                        Intrinsics.checkNotNullParameter(zArr4, "it");
                        DataAndStorageSettingsFragment dataAndStorageSettingsFragment5 = dataAndStorageSettingsFragment4;
                        ArrayList arrayList4 = new ArrayList(zArr4.length);
                        int length = zArr4.length;
                        int i = 0;
                        int i2 = 0;
                        while (true) {
                            dataAndStorageSettingsViewModel = 0;
                            if (i >= length) {
                                break;
                            }
                            i2++;
                            if (zArr4[i]) {
                                dataAndStorageSettingsViewModel = DataAndStorageSettingsFragment.access$getAutoDownloadValues(dataAndStorageSettingsFragment5)[i2];
                            }
                            arrayList4.add(dataAndStorageSettingsViewModel == true ? 1 : 0);
                            i++;
                        }
                        Set<String> set = CollectionsKt___CollectionsKt.toSet(CollectionsKt___CollectionsKt.filterNotNull(arrayList4));
                        DataAndStorageSettingsViewModel access$getViewModel$p = DataAndStorageSettingsFragment.access$getViewModel$p(dataAndStorageSettingsFragment4);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                        } else {
                            dataAndStorageSettingsViewModel = access$getViewModel$p;
                        }
                        dataAndStorageSettingsViewModel.setRoamingAutoDownloadValues(set);
                    }
                });
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.DataAndStorageSettingsFragment__media_quality);
                DSLSettingsText.Companion companion2 = DSLSettingsText.Companion;
                DSLSettingsText from6 = companion2.from(R.string.DataAndStorageSettingsFragment__sent_media_quality, new DSLSettingsText.Modifier[0]);
                String[] access$getSentMediaQualityLabels = DataAndStorageSettingsFragment.access$getSentMediaQualityLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getSentMediaQualityLabels, "sentMediaQualityLabels");
                int i = ArraysKt___ArraysKt.indexOf(SentMediaQuality.values(), this.$state.getSentMediaQuality());
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment5 = this.this$0;
                dSLConfiguration.radioListPref(from6, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from6 : null, (r19 & 8) != 0, access$getSentMediaQualityLabels, i, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.8
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i2) {
                        DataAndStorageSettingsViewModel access$getViewModel$p = DataAndStorageSettingsFragment.access$getViewModel$p(dataAndStorageSettingsFragment5);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setSentMediaQuality(SentMediaQuality.values()[i2]);
                    }
                });
                DSLConfiguration.textPref$default(dSLConfiguration, null, companion2.from(R.string.DataAndStorageSettingsFragment__sending_high_quality_media_will_use_more_data, new DSLSettingsText.Modifier[0]), 1, null);
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.DataAndStorageSettingsFragment__calls);
                DSLSettingsText from7 = companion2.from(R.string.preferences_data_and_storage__use_less_data_for_calls, new DSLSettingsText.Modifier[0]);
                String[] access$getCallBandwidthLabels = DataAndStorageSettingsFragment.access$getCallBandwidthLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getCallBandwidthLabels, "callBandwidthLabels");
                int abs = Math.abs(this.$state.getCallBandwidthMode().getCode() - 2);
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment6 = this.this$0;
                dSLConfiguration.radioListPref(from7, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from7 : null, (r19 & 8) != 0, access$getCallBandwidthLabels, abs, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.9
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i2) {
                        DataAndStorageSettingsViewModel access$getViewModel$p = DataAndStorageSettingsFragment.access$getViewModel$p(dataAndStorageSettingsFragment6);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        CallBandwidthMode fromCode = CallBandwidthMode.fromCode(Math.abs(i2 - 2));
                        Intrinsics.checkNotNullExpressionValue(fromCode, "fromCode(abs(it - 2))");
                        access$getViewModel$p.setCallBandwidthMode(fromCode);
                    }
                });
                DSLConfiguration.textPref$default(dSLConfiguration, null, companion2.from(R.string.preference_data_and_storage__using_less_data_may_improve_calls_on_bad_networks, new DSLSettingsText.Modifier[0]), 1, null);
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.preferences_proxy);
                DSLSettingsText from8 = companion2.from(R.string.preferences_use_proxy, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from9 = companion2.from(this.$state.isProxyEnabled() ? R.string.preferences_on : R.string.preferences_off, new DSLSettingsText.Modifier[0]);
                final DataAndStorageSettingsFragment dataAndStorageSettingsFragment7 = this.this$0;
                dSLConfiguration.clickPref(from8, (r18 & 2) != 0 ? null : from9, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsFragment$getConfiguration$1.10
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(dataAndStorageSettingsFragment7.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_dataAndStorageSettingsFragment_to_editProxyFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
            }
        });
    }
}
