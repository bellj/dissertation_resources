package org.thoughtcrime.securesms.components;

import android.view.animation.Interpolator;
import org.thoughtcrime.securesms.components.InputPanel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InputPanel$RecordTime$$ExternalSyntheticLambda0 implements Interpolator {
    @Override // android.animation.TimeInterpolator
    public final float getInterpolation(float f) {
        return InputPanel.RecordTime.lambda$pulseInterpolator$0(f);
    }
}
