package org.thoughtcrime.securesms.components.settings.app.help;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: HelpSettingsFragment.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0006\u0010\u0007\u001a\u00020\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/help/HelpSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class HelpSettingsFragment extends DSLSettingsFragment {
    public HelpSettingsFragment() {
        super(R.string.preferences__help, 0, 0, null, 14, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        dSLSettingsAdapter.submitList(getConfiguration().toMappingModelList());
    }

    public final DSLConfiguration getConfiguration() {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.help.HelpSettingsFragment$getConfiguration$1
            final /* synthetic */ HelpSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLConfiguration.externalLinkPref$default(dSLConfiguration, companion.from(R.string.HelpSettingsFragment__support_center, new DSLSettingsText.Modifier[0]), null, R.string.support_center_url, 2, null);
                DSLSettingsText from = companion.from(R.string.HelpSettingsFragment__contact_us, new DSLSettingsText.Modifier[0]);
                final HelpSettingsFragment helpSettingsFragment = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.help.HelpSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(helpSettingsFragment.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_helpSettingsFragment_to_helpFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                dSLConfiguration.dividerPref();
                dSLConfiguration.textPref(companion.from(R.string.HelpSettingsFragment__version, new DSLSettingsText.Modifier[0]), companion.from(BuildConfig.VERSION_NAME, new DSLSettingsText.Modifier[0]));
                DSLSettingsText from2 = companion.from(R.string.HelpSettingsFragment__debug_log, new DSLSettingsText.Modifier[0]);
                final HelpSettingsFragment helpSettingsFragment2 = this.this$0;
                dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.help.HelpSettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(helpSettingsFragment2.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_helpSettingsFragment_to_submitDebugLogActivity);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLConfiguration.externalLinkPref$default(dSLConfiguration, companion.from(R.string.HelpSettingsFragment__terms_amp_privacy_policy, new DSLSettingsText.Modifier[0]), null, R.string.terms_and_privacy_policy_url, 2, null);
                StringBuilder sb = new StringBuilder();
                HelpSettingsFragment helpSettingsFragment3 = this.this$0;
                sb.append(helpSettingsFragment3.getString(R.string.HelpFragment__copyright_signal_messenger));
                sb.append("\n");
                sb.append(helpSettingsFragment3.getString(R.string.HelpFragment__licenced_under_the_gplv3));
                Unit unit = Unit.INSTANCE;
                DSLConfiguration.textPref$default(dSLConfiguration, null, companion.from(sb, new DSLSettingsText.Modifier[0]), 1, null);
            }
        });
    }
}
