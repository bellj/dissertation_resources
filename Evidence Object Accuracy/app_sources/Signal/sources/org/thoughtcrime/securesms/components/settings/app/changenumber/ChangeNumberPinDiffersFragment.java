package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import android.view.View;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts$StartActivityForResult;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;

/* compiled from: ChangeNumberPinDiffersFragment.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberPinDiffersFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberPinDiffersFragment extends LoggingFragment {
    public ChangeNumberPinDiffersFragment() {
        super(R.layout.fragment_change_number_pin_differs);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        view.findViewById(R.id.change_number_pin_differs_keep_old_pin).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberPinDiffersFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberPinDiffersFragment.$r8$lambda$qEzwuRZCT6pw4KTh5Q6gMMtMTG0(ChangeNumberPinDiffersFragment.this, view2);
            }
        });
        ActivityResultLauncher registerForActivityResult = registerForActivityResult(new ActivityResultContracts$StartActivityForResult(), new ActivityResultCallback() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberPinDiffersFragment$$ExternalSyntheticLambda1
            @Override // androidx.activity.result.ActivityResultCallback
            public final void onActivityResult(Object obj) {
                ChangeNumberPinDiffersFragment.m602$r8$lambda$4D81ikYcr8XI8Nz6ct_9fHe0tA(ChangeNumberPinDiffersFragment.this, (ActivityResult) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(registerForActivityResult, "registerForActivityResul…erSuccess()\n      }\n    }");
        view.findViewById(R.id.change_number_pin_differs_update_pin).setOnClickListener(new View.OnClickListener(this) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberPinDiffersFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ ChangeNumberPinDiffersFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberPinDiffersFragment.m603$r8$lambda$SSyyGdpAgFbTFjuf1Nh4cI8bJ0(ActivityResultLauncher.this, this.f$1, view2);
            }
        });
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new ChangeNumberPinDiffersFragment$onViewCreated$3(this));
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m604onViewCreated$lambda0(ChangeNumberPinDiffersFragment changeNumberPinDiffersFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberPinDiffersFragment, "this$0");
        ChangeNumberUtil.INSTANCE.changeNumberSuccess(changeNumberPinDiffersFragment);
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m605onViewCreated$lambda1(ChangeNumberPinDiffersFragment changeNumberPinDiffersFragment, ActivityResult activityResult) {
        Intrinsics.checkNotNullParameter(changeNumberPinDiffersFragment, "this$0");
        if (activityResult.getResultCode() == -1) {
            ChangeNumberUtil.INSTANCE.changeNumberSuccess(changeNumberPinDiffersFragment);
        }
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m606onViewCreated$lambda2(ActivityResultLauncher activityResultLauncher, ChangeNumberPinDiffersFragment changeNumberPinDiffersFragment, View view) {
        Intrinsics.checkNotNullParameter(activityResultLauncher, "$changePin");
        Intrinsics.checkNotNullParameter(changeNumberPinDiffersFragment, "this$0");
        activityResultLauncher.launch(CreateKbsPinActivity.getIntentForPinChangeFromSettings(changeNumberPinDiffersFragment.requireContext()));
    }
}
