package org.thoughtcrime.securesms.components.webrtc.participantslist;

import android.view.View;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.viewholders.RecipientViewHolder;

/* loaded from: classes4.dex */
public class CallParticipantViewHolder extends RecipientViewHolder<CallParticipantViewState> {
    private final View audioMuted = findViewById(R.id.call_participant_audio_muted);
    private final View screenSharing = findViewById(R.id.call_participant_screen_sharing);
    private final View videoMuted = findViewById(R.id.call_participant_video_muted);

    public CallParticipantViewHolder(View view) {
        super(view, null);
    }

    public void bind(CallParticipantViewState callParticipantViewState) {
        super.bind((CallParticipantViewHolder) callParticipantViewState);
        this.videoMuted.setVisibility(callParticipantViewState.getVideoMutedVisibility());
        this.audioMuted.setVisibility(callParticipantViewState.getAudioMutedVisibility());
        this.screenSharing.setVisibility(callParticipantViewState.getScreenSharingVisibility());
    }
}
