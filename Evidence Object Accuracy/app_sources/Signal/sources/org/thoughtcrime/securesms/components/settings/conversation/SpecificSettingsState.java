package org.thoughtcrime.securesms.components.settings.conversation;

import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: ConversationSettingsState.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b6\u0018\u00002\u00020\u0001:\u0002\n\u000bB\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0007H\u0016J\b\u0010\b\u001a\u00020\tH\u0016R\u0012\u0010\u0003\u001a\u00020\u0004X¦\u0004¢\u0006\u0006\u001a\u0004\b\u0003\u0010\u0005\u0001\u0002\t\u0007¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;", "", "()V", "isLoaded", "", "()Z", "requireGroupSettingsState", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState$GroupSettingsState;", "requireRecipientSettingsState", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState$RecipientSettingsState;", "GroupSettingsState", "RecipientSettingsState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class SpecificSettingsState {
    public /* synthetic */ SpecificSettingsState(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    public abstract boolean isLoaded();

    private SpecificSettingsState() {
    }

    /* compiled from: ConversationSettingsState.kt */
    @Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BY\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\t\u0012\b\b\u0002\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\u000f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010\u001e\u001a\u00020\tHÆ\u0003J\t\u0010\u001f\u001a\u00020\tHÆ\u0003J\t\u0010 \u001a\u00020\tHÆ\u0003J\t\u0010!\u001a\u00020\rHÆ\u0003J]\u0010\"\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\rHÆ\u0001J\u0013\u0010#\u001a\u00020\t2\b\u0010$\u001a\u0004\u0018\u00010%HÖ\u0003J\t\u0010&\u001a\u00020'HÖ\u0001J\b\u0010(\u001a\u00020\u0000H\u0016J\t\u0010)\u001a\u00020*HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0010R\u0011\u0010\u000b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0012R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0019\u001a\u00020\tXD¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0012R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0012¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState$RecipientSettingsState;", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;", "identityRecord", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "allGroupsInCommon", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "groupsInCommon", "selfHasGroups", "", "canShowMoreGroupsInCommon", "groupsInCommonExpanded", "contactLinkState", "Lorg/thoughtcrime/securesms/components/settings/conversation/ContactLinkState;", "(Lorg/thoughtcrime/securesms/database/model/IdentityRecord;Ljava/util/List;Ljava/util/List;ZZZLorg/thoughtcrime/securesms/components/settings/conversation/ContactLinkState;)V", "getAllGroupsInCommon", "()Ljava/util/List;", "getCanShowMoreGroupsInCommon", "()Z", "getContactLinkState", "()Lorg/thoughtcrime/securesms/components/settings/conversation/ContactLinkState;", "getGroupsInCommon", "getGroupsInCommonExpanded", "getIdentityRecord", "()Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "isLoaded", "getSelfHasGroups", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "", "hashCode", "", "requireRecipientSettingsState", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientSettingsState extends SpecificSettingsState {
        private final List<Recipient> allGroupsInCommon;
        private final boolean canShowMoreGroupsInCommon;
        private final ContactLinkState contactLinkState;
        private final List<Recipient> groupsInCommon;
        private final boolean groupsInCommonExpanded;
        private final IdentityRecord identityRecord;
        private final boolean isLoaded;
        private final boolean selfHasGroups;

        public RecipientSettingsState() {
            this(null, null, null, false, false, false, null, 127, null);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState$RecipientSettingsState */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ RecipientSettingsState copy$default(RecipientSettingsState recipientSettingsState, IdentityRecord identityRecord, List list, List list2, boolean z, boolean z2, boolean z3, ContactLinkState contactLinkState, int i, Object obj) {
            if ((i & 1) != 0) {
                identityRecord = recipientSettingsState.identityRecord;
            }
            if ((i & 2) != 0) {
                list = recipientSettingsState.allGroupsInCommon;
            }
            if ((i & 4) != 0) {
                list2 = recipientSettingsState.groupsInCommon;
            }
            if ((i & 8) != 0) {
                z = recipientSettingsState.selfHasGroups;
            }
            if ((i & 16) != 0) {
                z2 = recipientSettingsState.canShowMoreGroupsInCommon;
            }
            if ((i & 32) != 0) {
                z3 = recipientSettingsState.groupsInCommonExpanded;
            }
            if ((i & 64) != 0) {
                contactLinkState = recipientSettingsState.contactLinkState;
            }
            return recipientSettingsState.copy(identityRecord, list, list2, z, z2, z3, contactLinkState);
        }

        public final IdentityRecord component1() {
            return this.identityRecord;
        }

        public final List<Recipient> component2() {
            return this.allGroupsInCommon;
        }

        public final List<Recipient> component3() {
            return this.groupsInCommon;
        }

        public final boolean component4() {
            return this.selfHasGroups;
        }

        public final boolean component5() {
            return this.canShowMoreGroupsInCommon;
        }

        public final boolean component6() {
            return this.groupsInCommonExpanded;
        }

        public final ContactLinkState component7() {
            return this.contactLinkState;
        }

        public final RecipientSettingsState copy(IdentityRecord identityRecord, List<? extends Recipient> list, List<? extends Recipient> list2, boolean z, boolean z2, boolean z3, ContactLinkState contactLinkState) {
            Intrinsics.checkNotNullParameter(list, "allGroupsInCommon");
            Intrinsics.checkNotNullParameter(list2, "groupsInCommon");
            Intrinsics.checkNotNullParameter(contactLinkState, "contactLinkState");
            return new RecipientSettingsState(identityRecord, list, list2, z, z2, z3, contactLinkState);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof RecipientSettingsState)) {
                return false;
            }
            RecipientSettingsState recipientSettingsState = (RecipientSettingsState) obj;
            return Intrinsics.areEqual(this.identityRecord, recipientSettingsState.identityRecord) && Intrinsics.areEqual(this.allGroupsInCommon, recipientSettingsState.allGroupsInCommon) && Intrinsics.areEqual(this.groupsInCommon, recipientSettingsState.groupsInCommon) && this.selfHasGroups == recipientSettingsState.selfHasGroups && this.canShowMoreGroupsInCommon == recipientSettingsState.canShowMoreGroupsInCommon && this.groupsInCommonExpanded == recipientSettingsState.groupsInCommonExpanded && this.contactLinkState == recipientSettingsState.contactLinkState;
        }

        public int hashCode() {
            IdentityRecord identityRecord = this.identityRecord;
            int hashCode = (((((identityRecord == null ? 0 : identityRecord.hashCode()) * 31) + this.allGroupsInCommon.hashCode()) * 31) + this.groupsInCommon.hashCode()) * 31;
            boolean z = this.selfHasGroups;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = (hashCode + i2) * 31;
            boolean z2 = this.canShowMoreGroupsInCommon;
            if (z2) {
                z2 = true;
            }
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i5 + i6) * 31;
            boolean z3 = this.groupsInCommonExpanded;
            if (!z3) {
                i = z3 ? 1 : 0;
            }
            return ((i9 + i) * 31) + this.contactLinkState.hashCode();
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState
        public RecipientSettingsState requireRecipientSettingsState() {
            return this;
        }

        public String toString() {
            return "RecipientSettingsState(identityRecord=" + this.identityRecord + ", allGroupsInCommon=" + this.allGroupsInCommon + ", groupsInCommon=" + this.groupsInCommon + ", selfHasGroups=" + this.selfHasGroups + ", canShowMoreGroupsInCommon=" + this.canShowMoreGroupsInCommon + ", groupsInCommonExpanded=" + this.groupsInCommonExpanded + ", contactLinkState=" + this.contactLinkState + ')';
        }

        public final IdentityRecord getIdentityRecord() {
            return this.identityRecord;
        }

        public /* synthetic */ RecipientSettingsState(IdentityRecord identityRecord, List list, List list2, boolean z, boolean z2, boolean z3, ContactLinkState contactLinkState, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : identityRecord, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list2, (i & 8) != 0 ? false : z, (i & 16) != 0 ? false : z2, (i & 32) != 0 ? false : z3, (i & 64) != 0 ? ContactLinkState.NONE : contactLinkState);
        }

        public final List<Recipient> getAllGroupsInCommon() {
            return this.allGroupsInCommon;
        }

        public final List<Recipient> getGroupsInCommon() {
            return this.groupsInCommon;
        }

        public final boolean getSelfHasGroups() {
            return this.selfHasGroups;
        }

        public final boolean getCanShowMoreGroupsInCommon() {
            return this.canShowMoreGroupsInCommon;
        }

        public final boolean getGroupsInCommonExpanded() {
            return this.groupsInCommonExpanded;
        }

        public final ContactLinkState getContactLinkState() {
            return this.contactLinkState;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RecipientSettingsState(IdentityRecord identityRecord, List<? extends Recipient> list, List<? extends Recipient> list2, boolean z, boolean z2, boolean z3, ContactLinkState contactLinkState) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "allGroupsInCommon");
            Intrinsics.checkNotNullParameter(list2, "groupsInCommon");
            Intrinsics.checkNotNullParameter(contactLinkState, "contactLinkState");
            this.identityRecord = identityRecord;
            this.allGroupsInCommon = list;
            this.groupsInCommon = list2;
            this.selfHasGroups = z;
            this.canShowMoreGroupsInCommon = z2;
            this.groupsInCommonExpanded = z3;
            this.contactLinkState = contactLinkState;
            this.isLoaded = true;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState
        public boolean isLoaded() {
            return this.isLoaded;
        }
    }

    /* compiled from: ConversationSettingsState.kt */
    @Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b+\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001BÅ\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\t\u0012\b\b\u0002\u0010\f\u001a\u00020\t\u0012\b\b\u0002\u0010\r\u001a\u00020\t\u0012\b\b\u0002\u0010\u000e\u001a\u00020\t\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u0011\u001a\u00020\t\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0010\u0012\b\b\u0002\u0010\u0013\u001a\u00020\t\u0012\b\b\u0002\u0010\u0014\u001a\u00020\t\u0012\b\b\u0002\u0010\u0015\u001a\u00020\t\u0012\b\b\u0002\u0010\u0016\u001a\u00020\u0010\u0012\b\b\u0002\u0010\u0017\u001a\u00020\u0018\u0012\b\b\u0002\u0010\u0019\u001a\u00020\t¢\u0006\u0002\u0010\u001aJ\t\u0010/\u001a\u00020\u0003HÆ\u0003J\t\u00100\u001a\u00020\u0010HÆ\u0003J\t\u00101\u001a\u00020\tHÂ\u0003J\u000b\u00102\u001a\u0004\u0018\u00010\u0010HÆ\u0003J\t\u00103\u001a\u00020\tHÆ\u0003J\t\u00104\u001a\u00020\tHÂ\u0003J\t\u00105\u001a\u00020\tHÆ\u0003J\t\u00106\u001a\u00020\u0010HÆ\u0003J\t\u00107\u001a\u00020\u0018HÆ\u0003J\t\u00108\u001a\u00020\tHÆ\u0003J\u000f\u00109\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000f\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010;\u001a\u00020\tHÆ\u0003J\t\u0010<\u001a\u00020\tHÆ\u0003J\t\u0010=\u001a\u00020\tHÆ\u0003J\t\u0010>\u001a\u00020\tHÆ\u0003J\t\u0010?\u001a\u00020\tHÆ\u0003J\t\u0010@\u001a\u00020\tHÆ\u0003JË\u0001\u0010A\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\t2\b\b\u0002\u0010\r\u001a\u00020\t2\b\b\u0002\u0010\u000e\u001a\u00020\t2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\t2\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u00102\b\b\u0002\u0010\u0013\u001a\u00020\t2\b\b\u0002\u0010\u0014\u001a\u00020\t2\b\b\u0002\u0010\u0015\u001a\u00020\t2\b\b\u0002\u0010\u0016\u001a\u00020\u00102\b\b\u0002\u0010\u0017\u001a\u00020\u00182\b\b\u0002\u0010\u0019\u001a\u00020\tHÆ\u0001J\u0013\u0010B\u001a\u00020\t2\b\u0010C\u001a\u0004\u0018\u00010DHÖ\u0003J\t\u0010E\u001a\u00020FHÖ\u0001J\b\u0010G\u001a\u00020\u0000H\u0016J\t\u0010H\u001a\u00020\u0010HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u000b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001eR\u0011\u0010\f\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u001eR\u0011\u0010\r\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001eR\u0013\u0010\u0012\u001a\u0004\u0018\u00010\u0010¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u000e\u0010\u0014\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0013\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010&R\u0011\u0010\u0015\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001eR\u0011\u0010\u000e\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001eR\u0011\u0010\u000f\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b)\u0010#R\u000e\u0010\u0011\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0019\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001eR\u0014\u0010*\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001eR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u001eR\u0011\u0010\u0017\u001a\u00020\u0018¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001cR\u0011\u0010\u0016\u001a\u00020\u0010¢\u0006\b\n\u0000\u001a\u0004\b.\u0010#¨\u0006I"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState$GroupSettingsState;", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "allMembers", "", "Lorg/thoughtcrime/securesms/groups/ui/GroupMemberEntry$FullMember;", "members", "isSelfAdmin", "", "canAddToGroup", "canEditGroupAttributes", "canLeave", "canShowMoreGroupMembers", "groupMembersExpanded", "groupTitle", "", "groupTitleLoaded", "groupDescription", "groupDescriptionShouldLinkify", "groupDescriptionLoaded", "groupLinkEnabled", "membershipCountDescription", "legacyGroupState", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;", "isAnnouncementGroup", "(Lorg/thoughtcrime/securesms/groups/GroupId;Ljava/util/List;Ljava/util/List;ZZZZZZLjava/lang/String;ZLjava/lang/String;ZZZLjava/lang/String;Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;Z)V", "getAllMembers", "()Ljava/util/List;", "getCanAddToGroup", "()Z", "getCanEditGroupAttributes", "getCanLeave", "getCanShowMoreGroupMembers", "getGroupDescription", "()Ljava/lang/String;", "getGroupDescriptionShouldLinkify", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId;", "getGroupLinkEnabled", "getGroupMembersExpanded", "getGroupTitle", "isLoaded", "getLegacyGroupState", "()Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;", "getMembers", "getMembershipCountDescription", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "", "hashCode", "", "requireGroupSettingsState", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupSettingsState extends SpecificSettingsState {
        private final List<GroupMemberEntry.FullMember> allMembers;
        private final boolean canAddToGroup;
        private final boolean canEditGroupAttributes;
        private final boolean canLeave;
        private final boolean canShowMoreGroupMembers;
        private final String groupDescription;
        private final boolean groupDescriptionLoaded;
        private final boolean groupDescriptionShouldLinkify;
        private final GroupId groupId;
        private final boolean groupLinkEnabled;
        private final boolean groupMembersExpanded;
        private final String groupTitle;
        private final boolean groupTitleLoaded;
        private final boolean isAnnouncementGroup;
        private final boolean isLoaded;
        private final boolean isSelfAdmin;
        private final LegacyGroupPreference.State legacyGroupState;
        private final List<GroupMemberEntry.FullMember> members;
        private final String membershipCountDescription;

        private final boolean component11() {
            return this.groupTitleLoaded;
        }

        private final boolean component14() {
            return this.groupDescriptionLoaded;
        }

        public final GroupId component1() {
            return this.groupId;
        }

        public final String component10() {
            return this.groupTitle;
        }

        public final String component12() {
            return this.groupDescription;
        }

        public final boolean component13() {
            return this.groupDescriptionShouldLinkify;
        }

        public final boolean component15() {
            return this.groupLinkEnabled;
        }

        public final String component16() {
            return this.membershipCountDescription;
        }

        public final LegacyGroupPreference.State component17() {
            return this.legacyGroupState;
        }

        public final boolean component18() {
            return this.isAnnouncementGroup;
        }

        public final List<GroupMemberEntry.FullMember> component2() {
            return this.allMembers;
        }

        public final List<GroupMemberEntry.FullMember> component3() {
            return this.members;
        }

        public final boolean component4() {
            return this.isSelfAdmin;
        }

        public final boolean component5() {
            return this.canAddToGroup;
        }

        public final boolean component6() {
            return this.canEditGroupAttributes;
        }

        public final boolean component7() {
            return this.canLeave;
        }

        public final boolean component8() {
            return this.canShowMoreGroupMembers;
        }

        public final boolean component9() {
            return this.groupMembersExpanded;
        }

        public final GroupSettingsState copy(GroupId groupId, List<GroupMemberEntry.FullMember> list, List<GroupMemberEntry.FullMember> list2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, String str, boolean z7, String str2, boolean z8, boolean z9, boolean z10, String str3, LegacyGroupPreference.State state, boolean z11) {
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(list, "allMembers");
            Intrinsics.checkNotNullParameter(list2, "members");
            Intrinsics.checkNotNullParameter(str, "groupTitle");
            Intrinsics.checkNotNullParameter(str3, "membershipCountDescription");
            Intrinsics.checkNotNullParameter(state, "legacyGroupState");
            return new GroupSettingsState(groupId, list, list2, z, z2, z3, z4, z5, z6, str, z7, str2, z8, z9, z10, str3, state, z11);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof GroupSettingsState)) {
                return false;
            }
            GroupSettingsState groupSettingsState = (GroupSettingsState) obj;
            return Intrinsics.areEqual(this.groupId, groupSettingsState.groupId) && Intrinsics.areEqual(this.allMembers, groupSettingsState.allMembers) && Intrinsics.areEqual(this.members, groupSettingsState.members) && this.isSelfAdmin == groupSettingsState.isSelfAdmin && this.canAddToGroup == groupSettingsState.canAddToGroup && this.canEditGroupAttributes == groupSettingsState.canEditGroupAttributes && this.canLeave == groupSettingsState.canLeave && this.canShowMoreGroupMembers == groupSettingsState.canShowMoreGroupMembers && this.groupMembersExpanded == groupSettingsState.groupMembersExpanded && Intrinsics.areEqual(this.groupTitle, groupSettingsState.groupTitle) && this.groupTitleLoaded == groupSettingsState.groupTitleLoaded && Intrinsics.areEqual(this.groupDescription, groupSettingsState.groupDescription) && this.groupDescriptionShouldLinkify == groupSettingsState.groupDescriptionShouldLinkify && this.groupDescriptionLoaded == groupSettingsState.groupDescriptionLoaded && this.groupLinkEnabled == groupSettingsState.groupLinkEnabled && Intrinsics.areEqual(this.membershipCountDescription, groupSettingsState.membershipCountDescription) && this.legacyGroupState == groupSettingsState.legacyGroupState && this.isAnnouncementGroup == groupSettingsState.isAnnouncementGroup;
        }

        public int hashCode() {
            int hashCode = ((((this.groupId.hashCode() * 31) + this.allMembers.hashCode()) * 31) + this.members.hashCode()) * 31;
            boolean z = this.isSelfAdmin;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = (hashCode + i2) * 31;
            boolean z2 = this.canAddToGroup;
            if (z2) {
                z2 = true;
            }
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i5 + i6) * 31;
            boolean z3 = this.canEditGroupAttributes;
            if (z3) {
                z3 = true;
            }
            int i10 = z3 ? 1 : 0;
            int i11 = z3 ? 1 : 0;
            int i12 = z3 ? 1 : 0;
            int i13 = (i9 + i10) * 31;
            boolean z4 = this.canLeave;
            if (z4) {
                z4 = true;
            }
            int i14 = z4 ? 1 : 0;
            int i15 = z4 ? 1 : 0;
            int i16 = z4 ? 1 : 0;
            int i17 = (i13 + i14) * 31;
            boolean z5 = this.canShowMoreGroupMembers;
            if (z5) {
                z5 = true;
            }
            int i18 = z5 ? 1 : 0;
            int i19 = z5 ? 1 : 0;
            int i20 = z5 ? 1 : 0;
            int i21 = (i17 + i18) * 31;
            boolean z6 = this.groupMembersExpanded;
            if (z6) {
                z6 = true;
            }
            int i22 = z6 ? 1 : 0;
            int i23 = z6 ? 1 : 0;
            int i24 = z6 ? 1 : 0;
            int hashCode2 = (((i21 + i22) * 31) + this.groupTitle.hashCode()) * 31;
            boolean z7 = this.groupTitleLoaded;
            if (z7) {
                z7 = true;
            }
            int i25 = z7 ? 1 : 0;
            int i26 = z7 ? 1 : 0;
            int i27 = z7 ? 1 : 0;
            int i28 = (hashCode2 + i25) * 31;
            String str = this.groupDescription;
            int hashCode3 = (i28 + (str == null ? 0 : str.hashCode())) * 31;
            boolean z8 = this.groupDescriptionShouldLinkify;
            if (z8) {
                z8 = true;
            }
            int i29 = z8 ? 1 : 0;
            int i30 = z8 ? 1 : 0;
            int i31 = z8 ? 1 : 0;
            int i32 = (hashCode3 + i29) * 31;
            boolean z9 = this.groupDescriptionLoaded;
            if (z9) {
                z9 = true;
            }
            int i33 = z9 ? 1 : 0;
            int i34 = z9 ? 1 : 0;
            int i35 = z9 ? 1 : 0;
            int i36 = (i32 + i33) * 31;
            boolean z10 = this.groupLinkEnabled;
            if (z10) {
                z10 = true;
            }
            int i37 = z10 ? 1 : 0;
            int i38 = z10 ? 1 : 0;
            int i39 = z10 ? 1 : 0;
            int hashCode4 = (((((i36 + i37) * 31) + this.membershipCountDescription.hashCode()) * 31) + this.legacyGroupState.hashCode()) * 31;
            boolean z11 = this.isAnnouncementGroup;
            if (!z11) {
                i = z11 ? 1 : 0;
            }
            return hashCode4 + i;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState
        public GroupSettingsState requireGroupSettingsState() {
            return this;
        }

        public String toString() {
            return "GroupSettingsState(groupId=" + this.groupId + ", allMembers=" + this.allMembers + ", members=" + this.members + ", isSelfAdmin=" + this.isSelfAdmin + ", canAddToGroup=" + this.canAddToGroup + ", canEditGroupAttributes=" + this.canEditGroupAttributes + ", canLeave=" + this.canLeave + ", canShowMoreGroupMembers=" + this.canShowMoreGroupMembers + ", groupMembersExpanded=" + this.groupMembersExpanded + ", groupTitle=" + this.groupTitle + ", groupTitleLoaded=" + this.groupTitleLoaded + ", groupDescription=" + this.groupDescription + ", groupDescriptionShouldLinkify=" + this.groupDescriptionShouldLinkify + ", groupDescriptionLoaded=" + this.groupDescriptionLoaded + ", groupLinkEnabled=" + this.groupLinkEnabled + ", membershipCountDescription=" + this.membershipCountDescription + ", legacyGroupState=" + this.legacyGroupState + ", isAnnouncementGroup=" + this.isAnnouncementGroup + ')';
        }

        public final GroupId getGroupId() {
            return this.groupId;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ GroupSettingsState(org.thoughtcrime.securesms.groups.GroupId r19, java.util.List r20, java.util.List r21, boolean r22, boolean r23, boolean r24, boolean r25, boolean r26, boolean r27, java.lang.String r28, boolean r29, java.lang.String r30, boolean r31, boolean r32, boolean r33, java.lang.String r34, org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference.State r35, boolean r36, int r37, kotlin.jvm.internal.DefaultConstructorMarker r38) {
            /*
            // Method dump skipped, instructions count: 195
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState.GroupSettingsState.<init>(org.thoughtcrime.securesms.groups.GroupId, java.util.List, java.util.List, boolean, boolean, boolean, boolean, boolean, boolean, java.lang.String, boolean, java.lang.String, boolean, boolean, boolean, java.lang.String, org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference$State, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final List<GroupMemberEntry.FullMember> getAllMembers() {
            return this.allMembers;
        }

        public final List<GroupMemberEntry.FullMember> getMembers() {
            return this.members;
        }

        public final boolean isSelfAdmin() {
            return this.isSelfAdmin;
        }

        public final boolean getCanAddToGroup() {
            return this.canAddToGroup;
        }

        public final boolean getCanEditGroupAttributes() {
            return this.canEditGroupAttributes;
        }

        public final boolean getCanLeave() {
            return this.canLeave;
        }

        public final boolean getCanShowMoreGroupMembers() {
            return this.canShowMoreGroupMembers;
        }

        public final boolean getGroupMembersExpanded() {
            return this.groupMembersExpanded;
        }

        public final String getGroupTitle() {
            return this.groupTitle;
        }

        public final String getGroupDescription() {
            return this.groupDescription;
        }

        public final boolean getGroupDescriptionShouldLinkify() {
            return this.groupDescriptionShouldLinkify;
        }

        public final boolean getGroupLinkEnabled() {
            return this.groupLinkEnabled;
        }

        public final String getMembershipCountDescription() {
            return this.membershipCountDescription;
        }

        public final LegacyGroupPreference.State getLegacyGroupState() {
            return this.legacyGroupState;
        }

        public final boolean isAnnouncementGroup() {
            return this.isAnnouncementGroup;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupSettingsState(GroupId groupId, List<GroupMemberEntry.FullMember> list, List<GroupMemberEntry.FullMember> list2, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, String str, boolean z7, String str2, boolean z8, boolean z9, boolean z10, String str3, LegacyGroupPreference.State state, boolean z11) {
            super(null);
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(list, "allMembers");
            Intrinsics.checkNotNullParameter(list2, "members");
            Intrinsics.checkNotNullParameter(str, "groupTitle");
            Intrinsics.checkNotNullParameter(str3, "membershipCountDescription");
            Intrinsics.checkNotNullParameter(state, "legacyGroupState");
            this.groupId = groupId;
            this.allMembers = list;
            this.members = list2;
            this.isSelfAdmin = z;
            this.canAddToGroup = z2;
            this.canEditGroupAttributes = z3;
            this.canLeave = z4;
            this.canShowMoreGroupMembers = z5;
            this.groupMembersExpanded = z6;
            this.groupTitle = str;
            this.groupTitleLoaded = z7;
            this.groupDescription = str2;
            this.groupDescriptionShouldLinkify = z8;
            this.groupDescriptionLoaded = z9;
            this.groupLinkEnabled = z10;
            this.membershipCountDescription = str3;
            this.legacyGroupState = state;
            this.isAnnouncementGroup = z11;
            this.isLoaded = z7 && z9;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState
        public boolean isLoaded() {
            return this.isLoaded;
        }
    }

    public RecipientSettingsState requireRecipientSettingsState() {
        throw new IllegalStateException("Not a recipient settings state".toString());
    }

    public GroupSettingsState requireGroupSettingsState() {
        throw new IllegalStateException("Not a group settings state".toString());
    }
}
