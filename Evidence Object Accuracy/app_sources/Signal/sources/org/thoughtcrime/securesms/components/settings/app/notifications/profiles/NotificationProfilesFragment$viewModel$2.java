package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import androidx.lifecycle.ViewModelProvider;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesViewModel;

/* compiled from: NotificationProfilesFragment.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Landroidx/lifecycle/ViewModelProvider$Factory;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class NotificationProfilesFragment$viewModel$2 extends Lambda implements Function0<ViewModelProvider.Factory> {
    public static final NotificationProfilesFragment$viewModel$2 INSTANCE = new NotificationProfilesFragment$viewModel$2();

    NotificationProfilesFragment$viewModel$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final ViewModelProvider.Factory invoke() {
        return new NotificationProfilesViewModel.Factory();
    }
}
