package org.thoughtcrime.securesms.components.webrtc;

import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public enum WebRtcAudioOutput {
    HANDSET(R.string.WebRtcAudioOutputToggle__phone_earpiece, R.drawable.ic_handset_solid_24),
    SPEAKER(R.string.WebRtcAudioOutputToggle__speaker, R.drawable.ic_speaker_solid_24),
    HEADSET(R.string.WebRtcAudioOutputToggle__bluetooth, R.drawable.ic_speaker_bt_solid_24);
    
    private final int iconRes;
    private final int labelRes;

    WebRtcAudioOutput(int i, int i2) {
        this.labelRes = i;
        this.iconRes = i2;
    }

    public int getIconRes() {
        return this.iconRes;
    }

    public int getLabelRes() {
        return this.labelRes;
    }
}
