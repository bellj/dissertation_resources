package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import androidx.lifecycle.ViewModelProvider;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.services.DonationsService;

/* compiled from: ManageDonationsFragment.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Landroidx/lifecycle/ViewModelProvider$Factory;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ManageDonationsFragment$viewModel$2 extends Lambda implements Function0<ViewModelProvider.Factory> {
    public static final ManageDonationsFragment$viewModel$2 INSTANCE = new ManageDonationsFragment$viewModel$2();

    ManageDonationsFragment$viewModel$2() {
        super(0);
    }

    @Override // kotlin.jvm.functions.Function0
    public final ViewModelProvider.Factory invoke() {
        DonationsService donationsService = ApplicationDependencies.getDonationsService();
        Intrinsics.checkNotNullExpressionValue(donationsService, "getDonationsService()");
        return new ManageDonationsViewModel.Factory(new SubscriptionsRepository(donationsService));
    }
}
