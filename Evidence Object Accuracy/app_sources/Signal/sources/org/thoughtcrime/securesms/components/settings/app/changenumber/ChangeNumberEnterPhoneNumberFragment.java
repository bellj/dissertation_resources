package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentResultListener;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.LabeledEditText;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel;
import org.thoughtcrime.securesms.registration.fragments.CountryPickerFragment;
import org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController;
import org.thoughtcrime.securesms.util.Dialogs;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberEnterPhoneNumberFragment.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\u001a\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX.¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX.¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberEnterPhoneNumberFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "newNumber", "Lorg/thoughtcrime/securesms/components/LabeledEditText;", "newNumberCountryCode", "newNumberCountrySpinner", "Landroid/widget/Spinner;", "oldNumber", "oldNumberCountryCode", "oldNumberCountrySpinner", "scrollView", "Landroid/widget/ScrollView;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel;", "onContinue", "", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberEnterPhoneNumberFragment extends LoggingFragment {
    private LabeledEditText newNumber;
    private LabeledEditText newNumberCountryCode;
    private Spinner newNumberCountrySpinner;
    private LabeledEditText oldNumber;
    private LabeledEditText oldNumberCountryCode;
    private Spinner oldNumberCountrySpinner;
    private ScrollView scrollView;
    private ChangeNumberViewModel viewModel;

    /* compiled from: ChangeNumberEnterPhoneNumberFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[ChangeNumberViewModel.ContinueStatus.values().length];
            iArr[ChangeNumberViewModel.ContinueStatus.CAN_CONTINUE.ordinal()] = 1;
            iArr[ChangeNumberViewModel.ContinueStatus.INVALID_NUMBER.ordinal()] = 2;
            iArr[ChangeNumberViewModel.ContinueStatus.OLD_NUMBER_DOESNT_MATCH.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public ChangeNumberEnterPhoneNumberFragment() {
        super(R.layout.fragment_change_number_enter_phone_number);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        LabeledEditText labeledEditText;
        LabeledEditText labeledEditText2;
        Spinner spinner;
        LabeledEditText labeledEditText3;
        LabeledEditText labeledEditText4;
        Spinner spinner2;
        Intrinsics.checkNotNullParameter(view, "view");
        this.viewModel = ChangeNumberUtil.getViewModel(this);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        toolbar.setTitle(R.string.ChangeNumberEnterPhoneNumberFragment__change_number);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberEnterPhoneNumberFragment.m583$r8$lambda$wzFzuZ_kUAzHj80jeGS5Ay2b7s(ChangeNumberEnterPhoneNumberFragment.this, view2);
            }
        });
        view.findViewById(R.id.change_number_enter_phone_number_continue).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberEnterPhoneNumberFragment.$r8$lambda$nbsxptM7lP8uA_zZHY5J4NKDQBE(ChangeNumberEnterPhoneNumberFragment.this, view2);
            }
        });
        View findViewById2 = view.findViewById(R.id.change_number_enter_phone_number_scroll);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.c…nter_phone_number_scroll)");
        this.scrollView = (ScrollView) findViewById2;
        View findViewById3 = view.findViewById(R.id.change_number_enter_phone_number_old_number_spinner);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.c…umber_old_number_spinner)");
        this.oldNumberCountrySpinner = (Spinner) findViewById3;
        View findViewById4 = view.findViewById(R.id.change_number_enter_phone_number_old_number_country_code);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.c…_old_number_country_code)");
        this.oldNumberCountryCode = (LabeledEditText) findViewById4;
        View findViewById5 = view.findViewById(R.id.change_number_enter_phone_number_old_number_number);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.c…number_old_number_number)");
        this.oldNumber = (LabeledEditText) findViewById5;
        Context requireContext = requireContext();
        LabeledEditText labeledEditText5 = this.oldNumberCountryCode;
        ChangeNumberViewModel changeNumberViewModel = null;
        if (labeledEditText5 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("oldNumberCountryCode");
            labeledEditText = null;
        } else {
            labeledEditText = labeledEditText5;
        }
        LabeledEditText labeledEditText6 = this.oldNumber;
        if (labeledEditText6 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("oldNumber");
            labeledEditText2 = null;
        } else {
            labeledEditText2 = labeledEditText6;
        }
        Spinner spinner3 = this.oldNumberCountrySpinner;
        if (spinner3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("oldNumberCountrySpinner");
            spinner = null;
        } else {
            spinner = spinner3;
        }
        RegistrationNumberInputController registrationNumberInputController = new RegistrationNumberInputController(requireContext, labeledEditText, labeledEditText2, spinner, false, new ChangeNumberEnterPhoneNumberFragment$onViewCreated$oldController$1(this));
        View findViewById6 = view.findViewById(R.id.change_number_enter_phone_number_new_number_spinner);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.c…umber_new_number_spinner)");
        this.newNumberCountrySpinner = (Spinner) findViewById6;
        View findViewById7 = view.findViewById(R.id.change_number_enter_phone_number_new_number_country_code);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.c…_new_number_country_code)");
        this.newNumberCountryCode = (LabeledEditText) findViewById7;
        View findViewById8 = view.findViewById(R.id.change_number_enter_phone_number_new_number_number);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.c…number_new_number_number)");
        this.newNumber = (LabeledEditText) findViewById8;
        Context requireContext2 = requireContext();
        LabeledEditText labeledEditText7 = this.newNumberCountryCode;
        if (labeledEditText7 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("newNumberCountryCode");
            labeledEditText3 = null;
        } else {
            labeledEditText3 = labeledEditText7;
        }
        LabeledEditText labeledEditText8 = this.newNumber;
        if (labeledEditText8 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("newNumber");
            labeledEditText4 = null;
        } else {
            labeledEditText4 = labeledEditText8;
        }
        Spinner spinner4 = this.newNumberCountrySpinner;
        if (spinner4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("newNumberCountrySpinner");
            spinner2 = null;
        } else {
            spinner2 = spinner4;
        }
        RegistrationNumberInputController registrationNumberInputController2 = new RegistrationNumberInputController(requireContext2, labeledEditText3, labeledEditText4, spinner2, true, new ChangeNumberEnterPhoneNumberFragment$onViewCreated$newController$1(this));
        getParentFragmentManager().setFragmentResultListener("old_number_country", this, new FragmentResultListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda2
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                ChangeNumberEnterPhoneNumberFragment.$r8$lambda$MkNjG5RN89K1pHa2T7hqsm5Qlw4(ChangeNumberEnterPhoneNumberFragment.this, str, bundle2);
            }
        });
        getParentFragmentManager().setFragmentResultListener("new_number_country", this, new FragmentResultListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda3
            @Override // androidx.fragment.app.FragmentResultListener
            public final void onFragmentResult(String str, Bundle bundle2) {
                ChangeNumberEnterPhoneNumberFragment.m582$r8$lambda$IHBhrX7UUxdKvr2fB9LQfauSaA(ChangeNumberEnterPhoneNumberFragment.this, str, bundle2);
            }
        });
        ChangeNumberViewModel changeNumberViewModel2 = this.viewModel;
        if (changeNumberViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel2 = null;
        }
        changeNumberViewModel2.getLiveOldNumber().observe(getViewLifecycleOwner(), new ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda4(registrationNumberInputController));
        ChangeNumberViewModel changeNumberViewModel3 = this.viewModel;
        if (changeNumberViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            changeNumberViewModel = changeNumberViewModel3;
        }
        changeNumberViewModel.getLiveNewNumber().observe(getViewLifecycleOwner(), new ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda4(registrationNumberInputController2));
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m584onViewCreated$lambda0(ChangeNumberEnterPhoneNumberFragment changeNumberEnterPhoneNumberFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberEnterPhoneNumberFragment, "this$0");
        FragmentKt.findNavController(changeNumberEnterPhoneNumberFragment).navigateUp();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m585onViewCreated$lambda1(ChangeNumberEnterPhoneNumberFragment changeNumberEnterPhoneNumberFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberEnterPhoneNumberFragment, "this$0");
        changeNumberEnterPhoneNumberFragment.onContinue();
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m586onViewCreated$lambda2(ChangeNumberEnterPhoneNumberFragment changeNumberEnterPhoneNumberFragment, String str, Bundle bundle) {
        Intrinsics.checkNotNullParameter(changeNumberEnterPhoneNumberFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        ChangeNumberViewModel changeNumberViewModel = changeNumberEnterPhoneNumberFragment.viewModel;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        changeNumberViewModel.setOldCountry(bundle.getInt(CountryPickerFragment.KEY_COUNTRY_CODE), bundle.getString(CountryPickerFragment.KEY_COUNTRY));
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m587onViewCreated$lambda3(ChangeNumberEnterPhoneNumberFragment changeNumberEnterPhoneNumberFragment, String str, Bundle bundle) {
        Intrinsics.checkNotNullParameter(changeNumberEnterPhoneNumberFragment, "this$0");
        Intrinsics.checkNotNullParameter(str, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(bundle, "bundle");
        ChangeNumberViewModel changeNumberViewModel = changeNumberEnterPhoneNumberFragment.viewModel;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        changeNumberViewModel.setNewCountry(bundle.getInt(CountryPickerFragment.KEY_COUNTRY_CODE), bundle.getString(CountryPickerFragment.KEY_COUNTRY));
    }

    public final void onContinue() {
        LabeledEditText labeledEditText = this.oldNumberCountryCode;
        ChangeNumberViewModel changeNumberViewModel = null;
        if (labeledEditText == null) {
            Intrinsics.throwUninitializedPropertyAccessException("oldNumberCountryCode");
            labeledEditText = null;
        }
        if (TextUtils.isEmpty(labeledEditText.getText())) {
            Toast.makeText(getContext(), getString(R.string.ChangeNumberEnterPhoneNumberFragment__you_must_specify_your_old_number_country_code), 1).show();
            return;
        }
        LabeledEditText labeledEditText2 = this.oldNumber;
        if (labeledEditText2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("oldNumber");
            labeledEditText2 = null;
        }
        if (TextUtils.isEmpty(labeledEditText2.getText())) {
            Toast.makeText(getContext(), getString(R.string.ChangeNumberEnterPhoneNumberFragment__you_must_specify_your_old_phone_number), 1).show();
            return;
        }
        LabeledEditText labeledEditText3 = this.newNumberCountryCode;
        if (labeledEditText3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("newNumberCountryCode");
            labeledEditText3 = null;
        }
        if (TextUtils.isEmpty(labeledEditText3.getText())) {
            Toast.makeText(getContext(), getString(R.string.ChangeNumberEnterPhoneNumberFragment__you_must_specify_your_new_number_country_code), 1).show();
            return;
        }
        LabeledEditText labeledEditText4 = this.newNumber;
        if (labeledEditText4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("newNumber");
            labeledEditText4 = null;
        }
        if (TextUtils.isEmpty(labeledEditText4.getText())) {
            Toast.makeText(getContext(), getString(R.string.ChangeNumberEnterPhoneNumberFragment__you_must_specify_your_new_phone_number), 1).show();
            return;
        }
        ChangeNumberViewModel changeNumberViewModel2 = this.viewModel;
        if (changeNumberViewModel2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel2 = null;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[changeNumberViewModel2.canContinue().ordinal()];
        if (i == 1) {
            SafeNavigation.safeNavigate(FragmentKt.findNavController(this), (int) R.id.action_enterPhoneNumberChangeFragment_to_changePhoneNumberConfirmFragment);
        } else if (i == 2) {
            Context context = getContext();
            String string = getString(R.string.RegistrationActivity_invalid_number);
            StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
            String string2 = getString(R.string.RegistrationActivity_the_number_you_specified_s_is_invalid);
            Intrinsics.checkNotNullExpressionValue(string2, "getString(R.string.Regis…u_specified_s_is_invalid)");
            Object[] objArr = new Object[1];
            ChangeNumberViewModel changeNumberViewModel3 = this.viewModel;
            if (changeNumberViewModel3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            } else {
                changeNumberViewModel = changeNumberViewModel3;
            }
            objArr[0] = changeNumberViewModel.getNumber().getE164Number();
            String format = String.format(string2, Arrays.copyOf(objArr, 1));
            Intrinsics.checkNotNullExpressionValue(format, "format(format, *args)");
            Dialogs.showAlertDialog(context, string, format);
        } else if (i == 3) {
            new MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.ChangeNumberEnterPhoneNumberFragment__the_phone_number_you_entered_doesnt_match_your_accounts).setPositiveButton(17039370, (DialogInterface.OnClickListener) null).show();
        }
    }
}
