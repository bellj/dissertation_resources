package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class AlertView extends LinearLayout {
    private static final String TAG = Log.tag(AlertView.class);
    private ImageView approvalIndicator;
    private ImageView failedIndicator;

    public AlertView(Context context) {
        this(context, null);
    }

    public AlertView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(attributeSet);
    }

    public AlertView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(attributeSet);
    }

    private void initialize(AttributeSet attributeSet) {
        LinearLayout.inflate(getContext(), R.layout.alert_view, this);
        this.approvalIndicator = (ImageView) findViewById(R.id.pending_approval_indicator);
        this.failedIndicator = (ImageView) findViewById(R.id.sms_failed_indicator);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.AlertView, 0, 0);
            boolean z = obtainStyledAttributes.getBoolean(0, false);
            obtainStyledAttributes.recycle();
            if (z) {
                int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.alertview_small_icon_size);
                this.failedIndicator.getLayoutParams().width = dimensionPixelOffset;
                this.failedIndicator.getLayoutParams().height = dimensionPixelOffset;
                requestLayout();
            }
        }
    }

    public void setNone() {
        setVisibility(8);
    }

    public void setPendingApproval() {
        setVisibility(0);
        this.approvalIndicator.setVisibility(0);
        this.failedIndicator.setVisibility(8);
    }

    public void setFailed() {
        setVisibility(0);
        this.approvalIndicator.setVisibility(8);
        this.failedIndicator.setVisibility(0);
    }

    public void setRateLimited() {
        setVisibility(0);
        this.approvalIndicator.setVisibility(0);
        this.failedIndicator.setVisibility(8);
    }
}
