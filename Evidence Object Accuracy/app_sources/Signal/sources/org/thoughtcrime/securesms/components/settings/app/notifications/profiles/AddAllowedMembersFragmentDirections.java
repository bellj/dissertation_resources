package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class AddAllowedMembersFragmentDirections {
    private AddAllowedMembersFragmentDirections() {
    }

    public static ActionAddAllowedMembersFragmentToSelectRecipientsFragment actionAddAllowedMembersFragmentToSelectRecipientsFragment(long j) {
        return new ActionAddAllowedMembersFragmentToSelectRecipientsFragment(j);
    }

    public static ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment(long j, boolean z) {
        return new ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment(j, z);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionAddAllowedMembersFragmentToSelectRecipientsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_addAllowedMembersFragment_to_selectRecipientsFragment;
        }

        private ActionAddAllowedMembersFragmentToSelectRecipientsFragment(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public ActionAddAllowedMembersFragmentToSelectRecipientsFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public ActionAddAllowedMembersFragmentToSelectRecipientsFragment setCurrentSelection(RecipientId[] recipientIdArr) {
            this.arguments.put("currentSelection", recipientIdArr);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            if (this.arguments.containsKey("currentSelection")) {
                bundle.putParcelableArray("currentSelection", (RecipientId[]) this.arguments.get("currentSelection"));
            } else {
                bundle.putParcelableArray("currentSelection", null);
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public RecipientId[] getCurrentSelection() {
            return (RecipientId[]) this.arguments.get("currentSelection");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionAddAllowedMembersFragmentToSelectRecipientsFragment actionAddAllowedMembersFragmentToSelectRecipientsFragment = (ActionAddAllowedMembersFragmentToSelectRecipientsFragment) obj;
            if (this.arguments.containsKey("profileId") != actionAddAllowedMembersFragmentToSelectRecipientsFragment.arguments.containsKey("profileId") || getProfileId() != actionAddAllowedMembersFragmentToSelectRecipientsFragment.getProfileId() || this.arguments.containsKey("currentSelection") != actionAddAllowedMembersFragmentToSelectRecipientsFragment.arguments.containsKey("currentSelection")) {
                return false;
            }
            if (getCurrentSelection() == null ? actionAddAllowedMembersFragmentToSelectRecipientsFragment.getCurrentSelection() == null : getCurrentSelection().equals(actionAddAllowedMembersFragmentToSelectRecipientsFragment.getCurrentSelection())) {
                return getActionId() == actionAddAllowedMembersFragmentToSelectRecipientsFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return ((((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + Arrays.hashCode(getCurrentSelection())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionAddAllowedMembersFragmentToSelectRecipientsFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + ", currentSelection=" + getCurrentSelection() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_addAllowedMembersFragment_to_editNotificationProfileScheduleFragment;
        }

        private ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment(long j, boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
            hashMap.put("createMode", Boolean.valueOf(z));
        }

        public ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment setCreateMode(boolean z) {
            this.arguments.put("createMode", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            if (this.arguments.containsKey("createMode")) {
                bundle.putBoolean("createMode", ((Boolean) this.arguments.get("createMode")).booleanValue());
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean getCreateMode() {
            return ((Boolean) this.arguments.get("createMode")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment = (ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment) obj;
            return this.arguments.containsKey("profileId") == actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment.arguments.containsKey("profileId") && getProfileId() == actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment.getProfileId() && this.arguments.containsKey("createMode") == actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment.arguments.containsKey("createMode") && getCreateMode() == actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment.getCreateMode() && getActionId() == actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment.getActionId();
        }

        public int hashCode() {
            return ((((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + (getCreateMode() ? 1 : 0)) * 31) + getActionId();
        }

        public String toString() {
            return "ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + ", createMode=" + getCreateMode() + "}";
        }
    }
}
