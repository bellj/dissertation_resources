package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.snackbar.Snackbar;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.List;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersViewModel;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileAddMembers;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileRecipient;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: AddAllowedMembersFragment.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\u001e\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H\u0002J\u001a\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u0010\u0010 \u001a\u00020\u00112\u0006\u0010!\u001a\u00020\"H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u001b\u0010\u000b\u001a\u00020\f8BX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\r\u0010\u000e¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "profileId", "", "getProfileId", "()J", "profileId$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "profile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "undoRemove", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AddAllowedMembersFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy profileId$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Long>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$profileId$2
        final /* synthetic */ AddAllowedMembersFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Long invoke() {
            return Long.valueOf(AddAllowedMembersFragmentArgs.fromBundle(this.this$0.requireArguments()).getProfileId());
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(AddAllowedMembersViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$viewModel$2
        final /* synthetic */ AddAllowedMembersFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new AddAllowedMembersViewModel.Factory(AddAllowedMembersFragment.access$getProfileId(this.this$0));
        }
    });

    public AddAllowedMembersFragment() {
        super(0, 0, R.layout.fragment_add_allowed_members, null, 11, null);
    }

    public final AddAllowedMembersViewModel getViewModel() {
        return (AddAllowedMembersViewModel) this.viewModel$delegate.getValue();
    }

    public final long getProfileId() {
        return ((Number) this.profileId$delegate.getValue()).longValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        ((CircularProgressMaterialButton) view.findViewById(R.id.add_allowed_members_profile_next)).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                AddAllowedMembersFragment.$r8$lambda$LTgm6H0K72DI70pkxaC7EMc4MF8(AddAllowedMembersFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-1$lambda-0 */
    public static final void m727onViewCreated$lambda1$lambda0(AddAllowedMembersFragment addAllowedMembersFragment, View view) {
        Intrinsics.checkNotNullParameter(addAllowedMembersFragment, "this$0");
        NavController findNavController = FragmentKt.findNavController(addAllowedMembersFragment);
        AddAllowedMembersFragmentDirections.ActionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment = AddAllowedMembersFragmentDirections.actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment(addAllowedMembersFragment.getProfileId(), true);
        Intrinsics.checkNotNullExpressionValue(actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment, "actionAddAllowedMembersF…Fragment(profileId, true)");
        SafeNavigation.safeNavigate(findNavController, actionAddAllowedMembersFragmentToEditNotificationProfileScheduleFragment);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        NotificationProfileAddMembers.INSTANCE.register(dSLSettingsAdapter);
        NotificationProfileRecipient.INSTANCE.register(dSLSettingsAdapter);
        this.lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(getViewModel().getProfile(), (Function1) null, (Function0) null, new Function1<AddAllowedMembersViewModel.NotificationProfileAndRecipients, Unit>(dSLSettingsAdapter, this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$bindAdapter$1
            final /* synthetic */ DSLSettingsAdapter $adapter;
            final /* synthetic */ AddAllowedMembersFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$adapter = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(AddAllowedMembersViewModel.NotificationProfileAndRecipients notificationProfileAndRecipients) {
                invoke(notificationProfileAndRecipients);
                return Unit.INSTANCE;
            }

            public final void invoke(AddAllowedMembersViewModel.NotificationProfileAndRecipients notificationProfileAndRecipients) {
                Intrinsics.checkNotNullParameter(notificationProfileAndRecipients, "<name for destructuring parameter 0>");
                this.$adapter.submitList(AddAllowedMembersFragment.access$getConfiguration(this.this$0, notificationProfileAndRecipients.component1(), notificationProfileAndRecipients.component2()).toMappingModelList());
            }
        }, 3, (Object) null));
    }

    public final DSLConfiguration getConfiguration(NotificationProfile notificationProfile, List<? extends Recipient> list) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(notificationProfile, list, this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$getConfiguration$1
            final /* synthetic */ NotificationProfile $profile;
            final /* synthetic */ List<Recipient> $recipients;
            final /* synthetic */ AddAllowedMembersFragment this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$profile = r1;
                this.$recipients = r2;
                this.this$0 = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.AddAllowedMembers__allowed_notifications);
                final AddAllowedMembersFragment addAllowedMembersFragment = this.this$0;
                dSLConfiguration.customPref(new NotificationProfileAddMembers.Model(null, null, new Function2<Long, Set<? extends RecipientId>, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$getConfiguration$1.1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    @Override // kotlin.jvm.functions.Function2
                    public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                        invoke(((Number) obj).longValue(), (Set) obj2);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(long j, Set<? extends RecipientId> set) {
                        Intrinsics.checkNotNullParameter(set, "currentSelection");
                        NavController findNavController = FragmentKt.findNavController(addAllowedMembersFragment);
                        AddAllowedMembersFragmentDirections.ActionAddAllowedMembersFragmentToSelectRecipientsFragment actionAddAllowedMembersFragmentToSelectRecipientsFragment = AddAllowedMembersFragmentDirections.actionAddAllowedMembersFragmentToSelectRecipientsFragment(j);
                        Object[] array = set.toArray(new RecipientId[0]);
                        if (array != null) {
                            AddAllowedMembersFragmentDirections.ActionAddAllowedMembersFragmentToSelectRecipientsFragment currentSelection = actionAddAllowedMembersFragmentToSelectRecipientsFragment.setCurrentSelection((RecipientId[]) array);
                            Intrinsics.checkNotNullExpressionValue(currentSelection, "actionAddAllowedMembersF…Selection.toTypedArray())");
                            SafeNavigation.safeNavigate(findNavController, currentSelection);
                            return;
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                    }
                }, this.$profile.getId(), this.$profile.getAllowedMembers(), 3, null));
                for (Recipient recipient : this.$recipients) {
                    RecipientPreference.Model model = new RecipientPreference.Model(recipient, false, AnonymousClass2.INSTANCE, 2, null);
                    final AddAllowedMembersFragment addAllowedMembersFragment2 = this.this$0;
                    dSLConfiguration.customPref(new NotificationProfileRecipient.Model(model, new Function1<RecipientId, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment$getConfiguration$1.3
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(RecipientId recipientId) {
                            invoke(recipientId);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(final RecipientId recipientId) {
                            Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
                            LifecycleDisposable access$getLifecycleDisposable$p = AddAllowedMembersFragment.access$getLifecycleDisposable$p(addAllowedMembersFragment2);
                            Single<Recipient> removeMember = AddAllowedMembersFragment.access$getViewModel(addAllowedMembersFragment2).removeMember(recipientId);
                            final AddAllowedMembersFragment addAllowedMembersFragment3 = addAllowedMembersFragment2;
                            access$getLifecycleDisposable$p.plusAssign(SubscribersKt.subscribeBy$default(removeMember, (Function1) null, new Function1<Recipient, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersFragment.getConfiguration.1.3.1
                                /* Return type fixed from 'java.lang.Object' to match base method */
                                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                                @Override // kotlin.jvm.functions.Function1
                                public /* bridge */ /* synthetic */ Unit invoke(Recipient recipient2) {
                                    invoke(recipient2);
                                    return Unit.INSTANCE;
                                }

                                public final void invoke(Recipient recipient2) {
                                    Intrinsics.checkNotNullParameter(recipient2, "removed");
                                    View view = addAllowedMembersFragment3.getView();
                                    if (view != null) {
                                        AddAllowedMembersFragment addAllowedMembersFragment4 = addAllowedMembersFragment3;
                                        Snackbar.make(view, addAllowedMembersFragment4.getString(R.string.NotificationProfileDetails__s_removed, recipient2.getDisplayName(addAllowedMembersFragment4.requireContext())), 0).setAction(R.string.NotificationProfileDetails__undo, new AddAllowedMembersFragment$getConfiguration$1$3$1$$ExternalSyntheticLambda0(addAllowedMembersFragment4, recipientId)).show();
                                    }
                                }

                                /* access modifiers changed from: private */
                                /* renamed from: invoke$lambda-1$lambda-0  reason: not valid java name */
                                public static final void m728invoke$lambda1$lambda0(AddAllowedMembersFragment addAllowedMembersFragment4, RecipientId recipientId2, View view) {
                                    Intrinsics.checkNotNullParameter(addAllowedMembersFragment4, "this$0");
                                    Intrinsics.checkNotNullParameter(recipientId2, "$id");
                                    AddAllowedMembersFragment.access$undoRemove(addAllowedMembersFragment4, recipientId2);
                                }
                            }, 1, (Object) null));
                        }
                    }));
                }
            }
        });
    }

    public final void undoRemove(RecipientId recipientId) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().addMember(recipientId).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.addMember(id)\n      .subscribe()");
        lifecycleDisposable.plusAssign(subscribe);
    }
}
