package org.thoughtcrime.securesms.components.settings.conversation;

import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda1 implements LiveDataUtil.Combine {
    @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
    public final Object apply(Object obj, Object obj2) {
        return new ConversationSettingsViewModel.DescriptionState((String) obj, ((Boolean) obj2).booleanValue());
    }
}
