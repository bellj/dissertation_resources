package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.content.Context;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.disposables.Disposable;
import j$.time.DayOfWeek;
import j$.time.LocalTime;
import j$.time.format.TextStyle;
import java.util.List;
import java.util.Locale;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileAddMembers;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileRecipient;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LargeIconClickPreference;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: NotificationProfileDetailsFragment.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\b\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\u000eH\u0002J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\b\u0010\u0018\u001a\u00020\u000eH\u0016J\u001a\u0010\u0019\u001a\u00020\u000e2\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0016J\u0010\u0010\u001e\u001a\u00020\u000e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\f\u0010!\u001a\u00020\"*\u00020#H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "createFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "deleteProfile", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$Valid;", "onDestroyView", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "undoRemove", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "describe", "", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileDetailsFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private Toolbar toolbar;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(NotificationProfileDetailsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return NotificationProfileDetailsFragment.access$createFactory((NotificationProfileDetailsFragment) this.receiver);
        }
    });

    public NotificationProfileDetailsFragment() {
        super(0, 0, 0, null, 15, null);
    }

    public final NotificationProfileDetailsViewModel getViewModel() {
        return (NotificationProfileDetailsViewModel) this.viewModel$delegate.getValue();
    }

    public final ViewModelProvider.Factory createFactory() {
        return new NotificationProfileDetailsViewModel.Factory(NotificationProfileDetailsFragmentArgs.fromBundle(requireArguments()).getProfileId());
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        this.toolbar = toolbar;
        if (toolbar != null) {
            toolbar.inflateMenu(R.menu.notification_profile_details);
        }
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.toolbar = null;
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        NotificationProfilePreference.INSTANCE.register(dSLSettingsAdapter);
        NotificationProfileAddMembers.INSTANCE.register(dSLSettingsAdapter);
        NotificationProfileRecipient.INSTANCE.register(dSLSettingsAdapter);
        LargeIconClickPreference.INSTANCE.register(dSLSettingsAdapter);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DSLSettingsAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                NotificationProfileDetailsFragment.m757$r8$lambda$Lx0lPT6_Jxms9eUXhq4T4Slbso(NotificationProfileDetailsFragment.this, this.f$1, (NotificationProfileDetailsViewModel.State) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-1 */
    public static final void m758bindAdapter$lambda1(NotificationProfileDetailsFragment notificationProfileDetailsFragment, DSLSettingsAdapter dSLSettingsAdapter, NotificationProfileDetailsViewModel.State state) {
        Intrinsics.checkNotNullParameter(notificationProfileDetailsFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        if (state instanceof NotificationProfileDetailsViewModel.State.Valid) {
            Toolbar toolbar = notificationProfileDetailsFragment.toolbar;
            if (toolbar != null) {
                toolbar.setTitle(((NotificationProfileDetailsViewModel.State.Valid) state).getProfile().getName());
            }
            Toolbar toolbar2 = notificationProfileDetailsFragment.toolbar;
            if (toolbar2 != null) {
                toolbar2.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener(state) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$$ExternalSyntheticLambda1
                    public final /* synthetic */ NotificationProfileDetailsViewModel.State f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
                    public final boolean onMenuItemClick(MenuItem menuItem) {
                        return NotificationProfileDetailsFragment.$r8$lambda$wjm3cbG0aaCG0x2JvHjFc5ZFxA8(NotificationProfileDetailsFragment.this, this.f$1, menuItem);
                    }
                });
            }
            Intrinsics.checkNotNullExpressionValue(state, "state");
            dSLSettingsAdapter.submitList(notificationProfileDetailsFragment.getConfiguration((NotificationProfileDetailsViewModel.State.Valid) state).toMappingModelList());
        } else if (!Intrinsics.areEqual(state, NotificationProfileDetailsViewModel.State.NotLoaded.INSTANCE) && Intrinsics.areEqual(state, NotificationProfileDetailsViewModel.State.Invalid.INSTANCE)) {
            notificationProfileDetailsFragment.requireActivity().onBackPressed();
        }
    }

    /* renamed from: bindAdapter$lambda-1$lambda-0 */
    public static final boolean m759bindAdapter$lambda1$lambda0(NotificationProfileDetailsFragment notificationProfileDetailsFragment, NotificationProfileDetailsViewModel.State state, MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(notificationProfileDetailsFragment, "this$0");
        if (menuItem.getItemId() != R.id.action_edit) {
            return false;
        }
        NavController findNavController = FragmentKt.findNavController(notificationProfileDetailsFragment);
        NotificationProfileDetailsFragmentDirections.ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment profileId = NotificationProfileDetailsFragmentDirections.actionNotificationProfileDetailsFragmentToEditNotificationProfileFragment().setProfileId(((NotificationProfileDetailsViewModel.State.Valid) state).getProfile().getId());
        Intrinsics.checkNotNullExpressionValue(profileId, "actionNotificationProfil…ofileId(state.profile.id)");
        SafeNavigation.safeNavigate(findNavController, profileId);
        return true;
    }

    private final DSLConfiguration getConfiguration(NotificationProfileDetailsViewModel.State.Valid valid) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(valid.component1(), valid.component3(), this, valid.component4(), valid.component2()) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$getConfiguration$1
            final /* synthetic */ boolean $expanded;
            final /* synthetic */ boolean $isOn;
            final /* synthetic */ NotificationProfile $profile;
            final /* synthetic */ List<Recipient> $recipients;
            final /* synthetic */ NotificationProfileDetailsFragment this$0;

            /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
            /* JADX WARN: Multi-variable type inference failed */
            /* access modifiers changed from: package-private */
            {
                this.$profile = r1;
                this.$isOn = r2;
                this.this$0 = r3;
                this.$expanded = r4;
                this.$recipients = r5;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r23) {
                /*
                // Method dump skipped, instructions count: 558
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final void deleteProfile() {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().deleteProfile().subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.deleteProfile()\n      .subscribe()");
        lifecycleDisposable.plusAssign(subscribe);
    }

    public final void undoRemove(RecipientId recipientId) {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().addMember(recipientId).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.addMember(id)\n      .subscribe()");
        lifecycleDisposable.plusAssign(subscribe);
    }

    public final String describe(NotificationProfileSchedule notificationProfileSchedule) {
        if (!notificationProfileSchedule.getEnabled()) {
            String string = getString(R.string.NotificationProfileDetails__schedule);
            Intrinsics.checkNotNullExpressionValue(string, "getString(R.string.Notif…ProfileDetails__schedule)");
            return string;
        }
        LocalTime startTime = notificationProfileSchedule.startTime();
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        String formatHours = JavaTimeExtensionsKt.formatHours(startTime, requireContext);
        LocalTime endTime = notificationProfileSchedule.endTime();
        Context requireContext2 = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
        String formatHours2 = JavaTimeExtensionsKt.formatHours(endTime, requireContext2);
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        if (notificationProfileSchedule.getDaysEnabled().size() == 7) {
            sb.append(getString(R.string.NotificationProfileDetails__everyday));
        } else {
            Locale locale = Locale.getDefault();
            Intrinsics.checkNotNullExpressionValue(locale, "getDefault()");
            for (DayOfWeek dayOfWeek : JavaTimeExtensionsKt.orderOfDaysInWeek(locale)) {
                if (notificationProfileSchedule.getDaysEnabled().contains(dayOfWeek)) {
                    if (sb.length() > 0) {
                        sb.append(", ");
                    }
                    sb.append(dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault()));
                }
            }
        }
        String string2 = getString(R.string.NotificationProfileDetails__s_to_s, formatHours, formatHours2);
        if (sb.length() <= 0) {
            z = false;
        }
        if (z) {
            return string2 + '\n' + ((Object) sb);
        }
        Intrinsics.checkNotNullExpressionValue(string2, "hours");
        return string2;
    }
}
