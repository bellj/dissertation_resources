package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.content.Context;
import java.util.Locale;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.DateUtils;

/* compiled from: Utils.kt */
@Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u0004*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/Utils;", "", "()V", "formatMutedUntil", "", "", "context", "Landroid/content/Context;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Utils {
    public static final Utils INSTANCE = new Utils();

    private Utils() {
    }

    public final String formatMutedUntil(long j, Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        if (j == Long.MAX_VALUE) {
            String string = context.getString(R.string.ConversationSettingsFragment__conversation_muted_forever);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      context.getStrin…tion_muted_forever)\n    }");
            return string;
        }
        String string2 = context.getString(R.string.ConversationSettingsFragment__conversation_muted_until_s, DateUtils.getTimeString(context, Locale.getDefault(), j));
        Intrinsics.checkNotNullExpressionValue(string2, "{\n      context.getStrin…lt(), this)\n      )\n    }");
        return string2;
    }
}
