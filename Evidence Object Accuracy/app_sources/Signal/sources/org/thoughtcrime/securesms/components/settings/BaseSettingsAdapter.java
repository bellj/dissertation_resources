package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.CustomizableSingleSelectSetting;
import org.thoughtcrime.securesms.components.settings.SettingHeader;
import org.thoughtcrime.securesms.components.settings.SettingProgress;
import org.thoughtcrime.securesms.components.settings.SingleSelectSetting;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class BaseSettingsAdapter extends MappingAdapter {
    public BaseSettingsAdapter() {
        registerFactory(SettingHeader.Item.class, new BaseSettingsAdapter$$ExternalSyntheticLambda2(), R.layout.base_settings_header_item);
        registerFactory(SettingProgress.Item.class, new Function() { // from class: org.thoughtcrime.securesms.components.settings.BaseSettingsAdapter$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SettingProgress.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.base_settings_progress_item);
    }

    public void configureSingleSelect(SingleSelectSetting.SingleSelectSelectionChangedListener singleSelectSelectionChangedListener) {
        registerFactory(SingleSelectSetting.Item.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.BaseSettingsAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BaseSettingsAdapter.$r8$lambda$oiyPQlciR0Px7XthFxbd9VlJJlc(SingleSelectSetting.SingleSelectSelectionChangedListener.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.single_select_item));
    }

    public static /* synthetic */ MappingViewHolder lambda$configureSingleSelect$0(SingleSelectSetting.SingleSelectSelectionChangedListener singleSelectSelectionChangedListener, View view) {
        return new SingleSelectSetting.ViewHolder(view, singleSelectSelectionChangedListener);
    }

    public void configureCustomizableSingleSelect(CustomizableSingleSelectSetting.CustomizableSingleSelectionListener customizableSingleSelectionListener) {
        registerFactory(CustomizableSingleSelectSetting.Item.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.BaseSettingsAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return BaseSettingsAdapter.$r8$lambda$CrYMeZu6v_oKLy1ZosENnRJl_MQ(CustomizableSingleSelectSetting.CustomizableSingleSelectionListener.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.customizable_single_select_item));
    }

    public static /* synthetic */ MappingViewHolder lambda$configureCustomizableSingleSelect$1(CustomizableSingleSelectSetting.CustomizableSingleSelectionListener customizableSingleSelectionListener, View view) {
        return new CustomizableSingleSelectSetting.ViewHolder(view, customizableSingleSelectionListener);
    }
}
