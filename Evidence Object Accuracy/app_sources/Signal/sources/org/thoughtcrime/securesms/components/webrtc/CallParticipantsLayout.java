package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.cardview.widget.CardView;
import com.google.android.flexbox.FlexboxLayout;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class CallParticipantsLayout extends FlexboxLayout {
    private static final int CORNER_RADIUS = ViewUtil.dpToPx(10);
    private static final int MULTIPLE_PARTICIPANT_SPACING = ViewUtil.dpToPx(3);
    private List<CallParticipant> callParticipants = Collections.emptyList();
    private CallParticipant focusedParticipant = null;
    private boolean isIncomingRing;
    private boolean isPortrait;
    private LayoutStrategy layoutStrategy;
    private int navBarBottomInset;
    private boolean shouldRenderInPip;

    /* loaded from: classes4.dex */
    public interface LayoutStrategy {
        int getFlexDirection();

        void setChildLayoutParams(View view, int i, int i2);

        void setChildScaling(CallParticipant callParticipant, CallParticipantView callParticipantView, boolean z, int i);
    }

    public CallParticipantsLayout(Context context) {
        super(context);
    }

    public CallParticipantsLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CallParticipantsLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void update(List<CallParticipant> list, CallParticipant callParticipant, boolean z, boolean z2, boolean z3, int i, LayoutStrategy layoutStrategy) {
        this.callParticipants = list;
        this.focusedParticipant = callParticipant;
        this.shouldRenderInPip = z;
        this.isPortrait = z2;
        this.isIncomingRing = z3;
        this.navBarBottomInset = i;
        this.layoutStrategy = layoutStrategy;
        setFlexDirection(layoutStrategy.getFlexDirection());
        updateLayout();
    }

    private void updateLayout() {
        int childCount = getChildCount();
        if (!this.shouldRenderInPip || !Util.hasItems(this.callParticipants)) {
            int size = this.callParticipants.size();
            updateChildrenCount(size);
            for (int i = 0; i < size; i++) {
                update(i, size, this.callParticipants.get(i));
            }
        } else {
            updateChildrenCount(1);
            update(0, 1, this.focusedParticipant);
        }
        if (childCount != getChildCount()) {
            updateMarginsForLayout();
        }
    }

    private void updateMarginsForLayout() {
        ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) getLayoutParams();
        if (this.callParticipants.size() <= 1 || this.shouldRenderInPip) {
            marginLayoutParams.setMargins(0, 0, 0, 0);
        } else {
            int i = MULTIPLE_PARTICIPANT_SPACING;
            marginLayoutParams.setMargins(i, ViewUtil.getStatusBarHeight(this), i, 0);
        }
        setLayoutParams(marginLayoutParams);
    }

    private void updateChildrenCount(int i) {
        int childCount = getChildCount();
        if (childCount < i) {
            while (childCount < i) {
                addCallParticipantView();
                childCount++;
            }
        } else if (childCount > i) {
            for (int i2 = i; i2 < childCount; i2++) {
                ((CallParticipantView) getChildAt(i).findViewById(R.id.group_call_participant)).releaseRenderer();
                removeViewAt(i);
            }
        }
    }

    private void update(int i, int i2, CallParticipant callParticipant) {
        View childAt = getChildAt(i);
        CardView cardView = (CardView) childAt.findViewById(R.id.group_call_participant_card_wrapper);
        CallParticipantView callParticipantView = (CallParticipantView) childAt.findViewById(R.id.group_call_participant);
        callParticipantView.setCallParticipant(callParticipant);
        callParticipantView.setRenderInPip(this.shouldRenderInPip);
        this.layoutStrategy.setChildScaling(callParticipant, callParticipantView, this.isPortrait, i2);
        if (i2 > 1) {
            int i3 = MULTIPLE_PARTICIPANT_SPACING;
            childAt.setPadding(i3, i3, i3, i3);
            cardView.setRadius((float) CORNER_RADIUS);
            callParticipantView.setBottomInset(0);
        } else {
            childAt.setPadding(0, 0, 0, 0);
            cardView.setRadius(0.0f);
            callParticipantView.setBottomInset(this.navBarBottomInset);
        }
        if (this.isIncomingRing) {
            callParticipantView.hideAvatar();
        } else {
            callParticipantView.showAvatar();
        }
        if (i2 > 2) {
            callParticipantView.useSmallAvatar();
        } else {
            callParticipantView.useLargeAvatar();
        }
        this.layoutStrategy.setChildLayoutParams(childAt, i, getChildCount());
    }

    private void addCallParticipantView() {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.group_call_participant_item, (ViewGroup) this, false);
        FlexboxLayout.LayoutParams layoutParams = (FlexboxLayout.LayoutParams) inflate.getLayoutParams();
        layoutParams.setAlignSelf(4);
        inflate.setLayoutParams(layoutParams);
        addView(inflate);
    }
}
