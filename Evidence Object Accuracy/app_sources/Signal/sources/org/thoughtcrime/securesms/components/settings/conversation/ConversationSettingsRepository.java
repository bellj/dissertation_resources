package org.thoughtcrime.securesms.components.settings.conversation;

import android.content.Context;
import android.database.Cursor;
import androidx.core.util.Consumer;
import androidx.lifecycle.LiveData;
import com.google.protobuf.ByteString;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableSource;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.util.Optional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.sequences.SequencesKt___SequencesKt;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.signal.storageservice.protos.groups.local.DecryptedGroup;
import org.signal.storageservice.protos.groups.local.DecryptedPendingMember;
import org.thoughtcrime.securesms.contacts.sync.ContactDiscovery;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupProtoUtil;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.groups.v2.GroupAddMembersResult;
import org.thoughtcrime.securesms.groups.v2.GroupManagementRepository;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;

/* compiled from: ConversationSettingsRepository.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010\t\n\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0019\u0012\u0006\u0010.\u001a\u00020-\u0012\b\b\u0002\u00101\u001a\u000200¢\u0006\u0004\b3\u00104J\u0016\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0003\u001a\u00020\u0002H\u0007J\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\b\u001a\u00020\u0007J\"\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000f0\u000eJ\"\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u000f0\u000eJ\u0006\u0010\u0013\u001a\u00020\u0012J\u001a\u0010\u0014\u001a\u00020\u000f2\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u000f0\u000eJ$\u0010\u0016\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0014\u0010\u0010\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0015\u0012\u0004\u0012\u00020\u000f0\u000eJ(\u0010\u0019\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0018\u0010\u0010\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00180\u0017\u0012\u0004\u0012\u00020\u000f0\u000eJ(\u0010\u001a\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0018\u0010\u0010\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u0017\u0012\u0004\u0012\u00020\u000f0\u000eJ\u000e\u0010\u001b\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\fJ\u0016\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u001c\u001a\u00020\u0002J\"\u0010\u001f\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u001e\u0012\u0004\u0012\u00020\u000f0\u000eJ0\u0010\"\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00072\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\f0\u00172\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\u000f0\u000eJ\u0016\u0010\u001d\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\u001c\u001a\u00020\u0002J\u000e\u0010#\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\fJ\u000e\u0010$\u001a\u00020\u000f2\u0006\u0010\r\u001a\u00020\fJ\u000e\u0010#\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u0007J\u000e\u0010$\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u0007J\u0010\u0010&\u001a\u00020\u00122\u0006\u0010%\u001a\u00020\u0018H\u0007J\u0014\u0010+\u001a\b\u0012\u0004\u0012\u00020*0)2\u0006\u0010(\u001a\u00020'J\"\u0010,\u001a\u00020\u000f2\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u000f0\u000eR\u0014\u0010.\u001a\u00020-8\u0002X\u0004¢\u0006\u0006\n\u0004\b.\u0010/R\u0014\u00101\u001a\u0002008\u0002X\u0004¢\u0006\u0006\n\u0004\b1\u00102¨\u00065"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;", "", "", "threadId", "j$/util/Optional", "Landroid/database/Cursor;", "getThreadMedia", "Lorg/thoughtcrime/securesms/groups/GroupId;", "groupId", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "getStoryViewState", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "recipientId", "Lkotlin/Function1;", "", "consumer", "getThreadId", "", "isInternalRecipientDetailsEnabled", "hasGroups", "Lorg/thoughtcrime/securesms/database/model/IdentityRecord;", "getIdentity", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "getGroupsInCommon", "getGroupMembership", "refreshRecipient", "until", "setMuteUntil", "Lorg/thoughtcrime/securesms/components/settings/conversation/GroupCapacityResult;", "getGroupCapacity", "selected", "Lorg/thoughtcrime/securesms/groups/v2/GroupAddMembersResult;", "addMembers", "block", "unblock", RecipientDatabase.TABLE_NAME, "isMessageRequestAccepted", "Lorg/thoughtcrime/securesms/groups/LiveGroup;", "liveGroup", "Landroidx/lifecycle/LiveData;", "", "getMembershipCountDescription", "getExternalPossiblyMigratedGroupRecipientId", "Landroid/content/Context;", "context", "Landroid/content/Context;", "Lorg/thoughtcrime/securesms/groups/v2/GroupManagementRepository;", "groupManagementRepository", "Lorg/thoughtcrime/securesms/groups/v2/GroupManagementRepository;", "<init>", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/groups/v2/GroupManagementRepository;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class ConversationSettingsRepository {
    private final Context context;
    private final GroupManagementRepository groupManagementRepository;

    public ConversationSettingsRepository(Context context, GroupManagementRepository groupManagementRepository) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(groupManagementRepository, "groupManagementRepository");
        this.context = context;
        this.groupManagementRepository = groupManagementRepository;
    }

    public /* synthetic */ ConversationSettingsRepository(Context context, GroupManagementRepository groupManagementRepository, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? new GroupManagementRepository(context) : groupManagementRepository);
    }

    public final Optional<Cursor> getThreadMedia(long j) {
        if (j <= 0) {
            Optional<Cursor> empty = Optional.empty();
            Intrinsics.checkNotNullExpressionValue(empty, "{\n      Optional.empty()\n    }");
            return empty;
        }
        Optional<Cursor> of = Optional.of(SignalDatabase.Companion.media().getGalleryMediaForThread(j, MediaDatabase.Sorting.Newest));
        Intrinsics.checkNotNullExpressionValue(of, "{\n      Optional.of(Sign…se.Sorting.Newest))\n    }");
        return of;
    }

    public final Observable<StoryViewState> getStoryViewState(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Observable<StoryViewState> observeOn = Observable.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda15
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return ConversationSettingsRepository.$r8$lambda$vcoYVW2QBn9S5n2HyY9pE4VN8io(GroupId.this);
            }
        }).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda16
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ConversationSettingsRepository.$r8$lambda$zmD8qC5lKaBm5_JWYsnIO8mdVyk((Optional) obj);
            }
        }).observeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(observeOn, "fromCallable {\n      Sig…bserveOn(Schedulers.io())");
        return observeOn;
    }

    /* renamed from: getStoryViewState$lambda-0 */
    public static final Optional m1108getStoryViewState$lambda0(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        return SignalDatabase.Companion.recipients().getByGroupId(groupId);
    }

    /* renamed from: getStoryViewState$lambda-1 */
    public static final ObservableSource m1109getStoryViewState$lambda1(Optional optional) {
        StoryViewState.Companion companion = StoryViewState.Companion;
        Object obj = optional.get();
        Intrinsics.checkNotNullExpressionValue(obj, "it.get()");
        return companion.getForRecipientId((RecipientId) obj);
    }

    public final void getThreadId(RecipientId recipientId, Function1<? super Long, Unit> function1) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda6
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1092$r8$lambda$Yg7tIAPzvM4AjAiLUj29upowDg(Function1.this, this.f$1);
            }
        });
    }

    /* renamed from: getThreadId$lambda-2 */
    public static final void m1110getThreadId$lambda2(Function1 function1, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        function1.invoke(Long.valueOf(SignalDatabase.Companion.threads().getThreadIdIfExistsFor(recipientId)));
    }

    public final void getThreadId(GroupId groupId, Function1<? super Long, Unit> function1) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda14
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1096$r8$lambda$tjQnRb09p0YnrtkW4r_4QWBZOU(GroupId.this, this.f$1);
            }
        });
    }

    /* renamed from: getThreadId$lambda-3 */
    public static final void m1111getThreadId$lambda3(GroupId groupId, Function1 function1) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        RecipientId id = Recipient.externalGroupExact(groupId).getId();
        Intrinsics.checkNotNullExpressionValue(id, "externalGroupExact(groupId).id");
        function1.invoke(Long.valueOf(SignalDatabase.Companion.threads().getThreadIdIfExistsFor(id)));
    }

    public final boolean isInternalRecipientDetailsEnabled() {
        return SignalStore.internalValues().recipientDetails();
    }

    /* renamed from: hasGroups$lambda-4 */
    public static final void m1112hasGroups$lambda4(Function1 function1) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        function1.invoke(Boolean.valueOf(SignalDatabase.Companion.groups().getActiveGroupCount() > 0));
    }

    public final void hasGroups(Function1<? super Boolean, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda12
            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.$r8$lambda$l7rn1i9jlzTHd3XwLKtr43Lg3KE(Function1.this);
            }
        });
    }

    public final void getIdentity(RecipientId recipientId, Function1<? super IdentityRecord, Unit> function1) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1090$r8$lambda$1xzw5TGqHEFPctHuiigzyaUAGE(Function1.this, this.f$1);
            }
        });
    }

    /* renamed from: getIdentity$lambda-5 */
    public static final void m1107getIdentity$lambda5(Function1 function1, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        if (SignalStore.account().getAci() == null || SignalStore.account().getPni() == null) {
            function1.invoke(null);
        } else {
            function1.invoke(ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecord(recipientId).orElse(null));
        }
    }

    public final void getGroupsInCommon(RecipientId recipientId, Function1<? super List<? extends Recipient>, Unit> function1) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(recipientId, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda17
            public final /* synthetic */ RecipientId f$1;
            public final /* synthetic */ ConversationSettingsRepository f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.$r8$lambda$u2O2mjqhXNKvzb1bzclk2s62EeY(Function1.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: getGroupsInCommon$lambda-7 */
    public static final void m1106getGroupsInCommon$lambda7(Function1 function1, RecipientId recipientId, ConversationSettingsRepository conversationSettingsRepository) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(conversationSettingsRepository, "this$0");
        List<GroupDatabase.GroupRecord> pushGroupsContainingMember = SignalDatabase.Companion.groups().getPushGroupsContainingMember(recipientId);
        Intrinsics.checkNotNullExpressionValue(pushGroupsContainingMember, "SignalDatabase\n         …ainingMember(recipientId)");
        function1.invoke(SequencesKt___SequencesKt.toList(SequencesKt___SequencesKt.sortedWith(SequencesKt___SequencesKt.map(SequencesKt___SequencesKt.map(SequencesKt___SequencesKt.filter(CollectionsKt___CollectionsKt.asSequence(pushGroupsContainingMember), ConversationSettingsRepository$getGroupsInCommon$1$1.INSTANCE), ConversationSettingsRepository$getGroupsInCommon$1$2.INSTANCE), ConversationSettingsRepository$getGroupsInCommon$1$3.INSTANCE), new Comparator(conversationSettingsRepository) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$getGroupsInCommon$lambda-7$$inlined$sortedBy$1
            final /* synthetic */ ConversationSettingsRepository this$0;

            {
                this.this$0 = r1;
            }

            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return ComparisonsKt__ComparisonsKt.compareValues(((Recipient) t).getDisplayName(ConversationSettingsRepository.access$getContext$p(this.this$0)), ((Recipient) t2).getDisplayName(ConversationSettingsRepository.access$getContext$p(this.this$0)));
            }
        })));
    }

    public final void getGroupMembership(RecipientId recipientId, Function1<? super List<? extends RecipientId>, Unit> function1) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1095$r8$lambda$p7HQz0nXH2i2CSUVUgi_TrfpD8(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: getGroupMembership$lambda-8 */
    public static final void m1105getGroupMembership$lambda8(RecipientId recipientId, Function1 function1) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        List<GroupDatabase.GroupRecord> pushGroupsContainingMember = SignalDatabase.Companion.groups().getPushGroupsContainingMember(recipientId);
        Intrinsics.checkNotNullExpressionValue(pushGroupsContainingMember, "groupDatabase.getPushGro…ainingMember(recipientId)");
        ArrayList arrayList = new ArrayList(pushGroupsContainingMember.size());
        for (GroupDatabase.GroupRecord groupRecord : pushGroupsContainingMember) {
            arrayList.add(groupRecord.getRecipientId());
        }
        function1.invoke(arrayList);
    }

    public final void refreshRecipient(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        SignalExecutors.UNBOUNDED.execute(new Runnable(recipientId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ RecipientId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.$r8$lambda$VTy_Y1qNWpNOP11GB3MXr7XvCvU(ConversationSettingsRepository.this, this.f$1);
            }
        });
    }

    /* renamed from: refreshRecipient$lambda-9 */
    public static final void m1113refreshRecipient$lambda9(ConversationSettingsRepository conversationSettingsRepository, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(conversationSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        try {
            Context context = conversationSettingsRepository.context;
            Recipient resolved = Recipient.resolved(recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
            ContactDiscovery.refresh(context, resolved, false);
        } catch (IOException unused) {
            Log.w(ConversationSettingsRepositoryKt.TAG, "Failed to refresh user after adding to contacts.");
        }
    }

    public final void setMuteUntil(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        SignalExecutors.BOUNDED.execute(new Runnable(j) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.$r8$lambda$ZEx_IlIVr2CypLvHyWC8kKQHEws(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: setMuteUntil$lambda-10 */
    public static final void m1114setMuteUntil$lambda10(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        SignalDatabase.Companion.recipients().setMuted(recipientId, j);
    }

    public final void getGroupCapacity(GroupId groupId, Function1<? super GroupCapacityResult, Unit> function1) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ Function1 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1091$r8$lambda$ThgBlAMKtFWmagnsknmJhd7oI(GroupId.this, this.f$1);
            }
        });
    }

    /* renamed from: getGroupCapacity$lambda-12 */
    public static final void m1104getGroupCapacity$lambda12(GroupId groupId, Function1 function1) {
        GroupCapacityResult groupCapacityResult;
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        GroupDatabase.GroupRecord groupRecord = SignalDatabase.Companion.groups().getGroup(groupId).get();
        Intrinsics.checkNotNullExpressionValue(groupRecord, "SignalDatabase.groups.getGroup(groupId).get()");
        GroupDatabase.GroupRecord groupRecord2 = groupRecord;
        if (groupRecord2.isV2Group()) {
            DecryptedGroup decryptedGroup = groupRecord2.requireV2GroupProperties().getDecryptedGroup();
            Intrinsics.checkNotNullExpressionValue(decryptedGroup, "groupRecord.requireV2Gro…operties().decryptedGroup");
            List<DecryptedPendingMember> pendingMembersList = decryptedGroup.getPendingMembersList();
            Intrinsics.checkNotNullExpressionValue(pendingMembersList, "decryptedGroup.pendingMembersList");
            ArrayList<ByteString> arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(pendingMembersList, 10));
            for (DecryptedPendingMember decryptedPendingMember : pendingMembersList) {
                arrayList.add(decryptedPendingMember.getUuid());
            }
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
            for (ByteString byteString : arrayList) {
                arrayList2.add(GroupProtoUtil.uuidByteStringToRecipientId(byteString));
            }
            ArrayList arrayList3 = new ArrayList();
            List<RecipientId> members = groupRecord2.getMembers();
            Intrinsics.checkNotNullExpressionValue(members, "groupRecord.members");
            arrayList3.addAll(members);
            arrayList3.addAll(arrayList2);
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            SelectionLimits groupLimits = FeatureFlags.groupLimits();
            Intrinsics.checkNotNullExpressionValue(groupLimits, "groupLimits()");
            groupCapacityResult = new GroupCapacityResult(id, arrayList3, groupLimits, groupRecord2.isAnnouncementGroup());
        } else {
            RecipientId id2 = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id2, "self().id");
            List<RecipientId> members2 = groupRecord2.getMembers();
            Intrinsics.checkNotNullExpressionValue(members2, "groupRecord.members");
            SelectionLimits groupLimits2 = FeatureFlags.groupLimits();
            Intrinsics.checkNotNullExpressionValue(groupLimits2, "groupLimits()");
            groupCapacityResult = new GroupCapacityResult(id2, members2, groupLimits2, false);
        }
        function1.invoke(groupCapacityResult);
    }

    /* renamed from: addMembers$lambda-13 */
    public static final void m1100addMembers$lambda13(Function1 function1, GroupAddMembersResult groupAddMembersResult) {
        Intrinsics.checkNotNullParameter(function1, "$tmp0");
        function1.invoke(groupAddMembersResult);
    }

    public final void addMembers(GroupId groupId, List<? extends RecipientId> list, Function1<? super GroupAddMembersResult, Unit> function1) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(list, "selected");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        this.groupManagementRepository.addMembers(groupId, list, new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ConversationSettingsRepository.m1097$r8$lambda$w5EgHa7KtsXnGdTHAzNQrJVVNc(Function1.this, (GroupAddMembersResult) obj);
            }
        });
    }

    public final void setMuteUntil(GroupId groupId, long j) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        SignalExecutors.BOUNDED.execute(new Runnable(j) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda13
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1099$r8$lambda$yNaISK827ag8bicAiglo8ESS04(GroupId.this, this.f$1);
            }
        });
    }

    /* renamed from: setMuteUntil$lambda-14 */
    public static final void m1115setMuteUntil$lambda14(GroupId groupId, long j) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        RecipientId id = Recipient.externalGroupExact(groupId).getId();
        Intrinsics.checkNotNullExpressionValue(id, "externalGroupExact(groupId).id");
        SignalDatabase.Companion.recipients().setMuted(id, j);
    }

    public final void block(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        SignalExecutors.BOUNDED.execute(new Runnable(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ ConversationSettingsRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1094$r8$lambda$j4rFCgDfXiKoBc0oSG37iLPSRk(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: block$lambda-15 */
    public static final void m1101block$lambda15(RecipientId recipientId, ConversationSettingsRepository conversationSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(conversationSettingsRepository, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        if (resolved.isGroup()) {
            RecipientUtil.block(conversationSettingsRepository.context, resolved);
        } else {
            RecipientUtil.blockNonGroup(conversationSettingsRepository.context, resolved);
        }
    }

    public final void unblock(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        SignalExecutors.BOUNDED.execute(new Runnable(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ ConversationSettingsRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1098$r8$lambda$wvJJbd5fQZfpQczPsBqhAKHGYE(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: unblock$lambda-16 */
    public static final void m1116unblock$lambda16(RecipientId recipientId, ConversationSettingsRepository conversationSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(conversationSettingsRepository, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        RecipientUtil.unblock(conversationSettingsRepository.context, resolved);
    }

    public final void block(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        SignalExecutors.BOUNDED.execute(new Runnable(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ ConversationSettingsRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.$r8$lambda$NqYLhbFHTXEnAQbSJU_Vv937fh0(GroupId.this, this.f$1);
            }
        });
    }

    /* renamed from: block$lambda-17 */
    public static final void m1102block$lambda17(GroupId groupId, ConversationSettingsRepository conversationSettingsRepository) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(conversationSettingsRepository, "this$0");
        Recipient externalGroupExact = Recipient.externalGroupExact(groupId);
        Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(groupId)");
        RecipientUtil.block(conversationSettingsRepository.context, externalGroupExact);
    }

    public final void unblock(GroupId groupId) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        SignalExecutors.BOUNDED.execute(new Runnable(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ ConversationSettingsRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.m1093$r8$lambda$fqEevwOgWqbBwmR0CzrHhCBNKQ(GroupId.this, this.f$1);
            }
        });
    }

    /* renamed from: unblock$lambda-18 */
    public static final void m1117unblock$lambda18(GroupId groupId, ConversationSettingsRepository conversationSettingsRepository) {
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(conversationSettingsRepository, "this$0");
        Recipient externalGroupExact = Recipient.externalGroupExact(groupId);
        Intrinsics.checkNotNullExpressionValue(externalGroupExact, "externalGroupExact(groupId)");
        RecipientUtil.unblock(conversationSettingsRepository.context, externalGroupExact);
    }

    public final boolean isMessageRequestAccepted(Recipient recipient) {
        Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
        return RecipientUtil.isMessageRequestAccepted(this.context, recipient);
    }

    public final LiveData<String> getMembershipCountDescription(LiveGroup liveGroup) {
        Intrinsics.checkNotNullParameter(liveGroup, "liveGroup");
        LiveData<String> membershipCountDescription = liveGroup.getMembershipCountDescription(this.context.getResources());
        Intrinsics.checkNotNullExpressionValue(membershipCountDescription, "liveGroup.getMembershipC…iption(context.resources)");
        return membershipCountDescription;
    }

    public final void getExternalPossiblyMigratedGroupRecipientId(GroupId groupId, Function1<? super RecipientId, Unit> function1) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(groupId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsRepository$$ExternalSyntheticLambda5
            public final /* synthetic */ GroupId f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsRepository.$r8$lambda$ynKT6fYSVtSX_QVAxuNKbUB95ks(Function1.this, this.f$1);
            }
        });
    }

    /* renamed from: getExternalPossiblyMigratedGroupRecipientId$lambda-19 */
    public static final void m1103getExternalPossiblyMigratedGroupRecipientId$lambda19(Function1 function1, GroupId groupId) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        RecipientId id = Recipient.externalPossiblyMigratedGroup(groupId).getId();
        Intrinsics.checkNotNullExpressionValue(id, "externalPossiblyMigratedGroup(groupId).id");
        function1.invoke(id);
    }
}
