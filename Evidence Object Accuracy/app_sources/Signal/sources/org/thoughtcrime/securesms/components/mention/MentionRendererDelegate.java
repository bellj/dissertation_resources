package org.thoughtcrime.securesms.components.mention;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.Annotation;
import android.text.Layout;
import android.text.Spanned;
import androidx.core.graphics.drawable.DrawableCompat;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.mention.MentionRenderer;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.DrawableUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class MentionRendererDelegate {
    private final Drawable drawable;
    private final Drawable drawableEnd;
    private final Drawable drawableLeft;
    private final Drawable drawableMid;
    private final int horizontalPadding;
    private final MentionRenderer multi;
    private final MentionRenderer single;

    public MentionRendererDelegate(Context context, int i) {
        int dpToPx = ViewUtil.dpToPx(2);
        this.horizontalPadding = dpToPx;
        Drawable tint = DrawableUtil.tint(ContextUtil.requireDrawable(context, R.drawable.mention_text_bg), i);
        this.drawable = tint;
        Drawable tint2 = DrawableUtil.tint(ContextUtil.requireDrawable(context, R.drawable.mention_text_bg_left), i);
        this.drawableLeft = tint2;
        Drawable tint3 = DrawableUtil.tint(ContextUtil.requireDrawable(context, R.drawable.mention_text_bg_mid), i);
        this.drawableMid = tint3;
        Drawable tint4 = DrawableUtil.tint(ContextUtil.requireDrawable(context, R.drawable.mention_text_bg_right), i);
        this.drawableEnd = tint4;
        this.single = new MentionRenderer.SingleLineMentionRenderer(dpToPx, 0, tint);
        this.multi = new MentionRenderer.MultiLineMentionRenderer(dpToPx, 0, tint2, tint3, tint4);
    }

    public void draw(Canvas canvas, Spanned spanned, Layout layout) {
        Annotation[] annotationArr = (Annotation[]) spanned.getSpans(0, spanned.length(), Annotation.class);
        for (Annotation annotation : annotationArr) {
            if (MentionAnnotation.isMentionAnnotation(annotation)) {
                int spanStart = spanned.getSpanStart(annotation);
                int spanEnd = spanned.getSpanEnd(annotation);
                int lineForOffset = layout.getLineForOffset(spanStart);
                int lineForOffset2 = layout.getLineForOffset(spanEnd);
                (lineForOffset == lineForOffset2 ? this.single : this.multi).draw(canvas, layout, lineForOffset, lineForOffset2, (int) (layout.getPrimaryHorizontal(spanStart) + ((float) (layout.getParagraphDirection(lineForOffset) * -1 * this.horizontalPadding))), (int) (layout.getPrimaryHorizontal(spanEnd) + ((float) (layout.getParagraphDirection(lineForOffset2) * this.horizontalPadding))));
            }
        }
    }

    public void setTint(int i) {
        DrawableCompat.setTint(this.drawable, i);
        DrawableCompat.setTint(this.drawableLeft, i);
        DrawableCompat.setTint(this.drawableMid, i);
        DrawableCompat.setTint(this.drawableEnd, i);
    }
}
