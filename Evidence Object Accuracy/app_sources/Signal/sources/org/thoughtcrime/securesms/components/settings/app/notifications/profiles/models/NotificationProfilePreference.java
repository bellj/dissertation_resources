package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import com.airbnb.lottie.SimpleColorFilter;
import com.google.android.material.switchmaterial.SwitchMaterial;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.PreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: NotificationProfilePreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfilePreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfilePreference {
    public static final NotificationProfilePreference INSTANCE = new NotificationProfilePreference();

    private NotificationProfilePreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new NotificationProfilePreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.notification_profile_preference_item));
    }

    /* compiled from: NotificationProfilePreference.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\r\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BK\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\n\u0012\b\b\u0002\u0010\u000b\u001a\u00020\n\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\u0002\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0014R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0014R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0019¨\u0006\u001b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfilePreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", NotificationProfileDatabase.NotificationProfileTable.COLOR, "Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "isOn", "", "showSwitch", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;ZZLkotlin/jvm/functions/Function0;)V", "getColor", "()Lorg/thoughtcrime/securesms/conversation/colors/AvatarColor;", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getShowSwitch", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getTitle", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final AvatarColor color;
        private final DSLSettingsIcon icon;
        private final boolean isOn;
        private final Function0<Unit> onClick;
        private final boolean showSwitch;
        private final DSLSettingsText summary;
        private final DSLSettingsText title;

        public /* synthetic */ Model(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, AvatarColor avatarColor, boolean z, boolean z2, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(dSLSettingsText, dSLSettingsText2, dSLSettingsIcon, avatarColor, (i & 16) != 0 ? false : z, (i & 32) != 0 ? false : z2, function0);
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsText getTitle() {
            return this.title;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsText getSummary() {
            return this.summary;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsIcon getIcon() {
            return this.icon;
        }

        public final AvatarColor getColor() {
            return this.color;
        }

        public final boolean isOn() {
            return this.isOn;
        }

        public final boolean getShowSwitch() {
            return this.showSwitch;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, AvatarColor avatarColor, boolean z, boolean z2, Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(avatarColor, NotificationProfileDatabase.NotificationProfileTable.COLOR);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.title = dSLSettingsText;
            this.summary = dSLSettingsText2;
            this.icon = dSLSettingsIcon;
            this.color = avatarColor;
            this.isOn = z;
            this.showSwitch = z2;
            this.onClick = function0;
        }
    }

    /* compiled from: NotificationProfilePreference.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfilePreference$ViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfilePreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "switchWidget", "Lcom/google/android/material/switchmaterial/SwitchMaterial;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends PreferenceViewHolder<Model> {
        private final SwitchMaterial switchWidget;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.switch_widget);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.switch_widget)");
            this.switchWidget = (SwitchMaterial) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            super.bind((ViewHolder) model);
            this.itemView.setOnClickListener(new NotificationProfilePreference$ViewHolder$$ExternalSyntheticLambda0(model));
            ViewExtensionsKt.setVisible(this.switchWidget, model.getShowSwitch());
            this.switchWidget.setEnabled(model.isEnabled());
            this.switchWidget.setChecked(model.isOn());
            getIconView().getBackground().setColorFilter(new SimpleColorFilter(model.getColor().colorInt()));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m817bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke();
        }
    }
}
