package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileNamePreset;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileNamePreset$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfileNamePreset.Model f$0;

    public /* synthetic */ NotificationProfileNamePreset$ViewHolder$$ExternalSyntheticLambda0(NotificationProfileNamePreset.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfileNamePreset.ViewHolder.m815bind$lambda0(this.f$0, view);
    }
}
