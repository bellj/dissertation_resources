package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class ConversationTypingView extends ConstraintLayout {
    private AvatarImageView avatar1;
    private AvatarImageView avatar2;
    private AvatarImageView avatar3;
    private BadgeImageView badge1;
    private BadgeImageView badge2;
    private BadgeImageView badge3;
    private View bubble;
    private TypingIndicatorView indicator;
    private TextView typistCount;

    public ConversationTypingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.avatar1 = (AvatarImageView) findViewById(R.id.typing_avatar_1);
        this.avatar2 = (AvatarImageView) findViewById(R.id.typing_avatar_2);
        this.avatar3 = (AvatarImageView) findViewById(R.id.typing_avatar_3);
        this.badge1 = (BadgeImageView) findViewById(R.id.typing_badge_1);
        this.badge2 = (BadgeImageView) findViewById(R.id.typing_badge_2);
        this.badge3 = (BadgeImageView) findViewById(R.id.typing_badge_3);
        this.typistCount = (TextView) findViewById(R.id.typing_count);
        this.bubble = findViewById(R.id.typing_bubble);
        this.indicator = (TypingIndicatorView) findViewById(R.id.typing_indicator);
    }

    public void setTypists(GlideRequests glideRequests, List<Recipient> list, boolean z, boolean z2) {
        if (list.isEmpty()) {
            this.indicator.stopAnimation();
            return;
        }
        this.avatar1.setVisibility(8);
        this.avatar2.setVisibility(8);
        this.avatar3.setVisibility(8);
        this.badge1.setVisibility(8);
        this.badge2.setVisibility(8);
        this.badge3.setVisibility(8);
        this.typistCount.setVisibility(8);
        if (z) {
            presentGroupThreadAvatars(glideRequests, list);
        }
        if (z2) {
            this.bubble.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.conversation_item_recv_bubble_color_wallpaper));
            this.typistCount.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.conversation_item_recv_bubble_color_wallpaper), PorterDuff.Mode.SRC_IN);
            this.indicator.setDotTint(ContextCompat.getColor(getContext(), R.color.conversation_typing_indicator_foreground_tint_wallpaper));
        } else {
            this.bubble.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.conversation_item_recv_bubble_color_normal));
            this.typistCount.getBackground().setColorFilter(ContextCompat.getColor(getContext(), R.color.conversation_item_recv_bubble_color_normal), PorterDuff.Mode.SRC_IN);
            this.indicator.setDotTint(ContextCompat.getColor(getContext(), R.color.conversation_typing_indicator_foreground_tint_normal));
        }
        this.indicator.startAnimation();
    }

    private void presentGroupThreadAvatars(GlideRequests glideRequests, List<Recipient> list) {
        this.avatar1.setAvatar(glideRequests, list.get(0), list.size() == 1);
        this.avatar1.setVisibility(0);
        this.badge1.setBadgeFromRecipient(list.get(0), glideRequests);
        this.badge1.setVisibility(0);
        if (list.size() > 1) {
            this.avatar2.setAvatar(glideRequests, list.get(1), false);
            this.avatar2.setVisibility(0);
            this.badge2.setBadgeFromRecipient(list.get(1), glideRequests);
            this.badge2.setVisibility(0);
        }
        if (list.size() == 3) {
            this.avatar3.setAvatar(glideRequests, list.get(2), false);
            this.avatar3.setVisibility(0);
            this.badge3.setBadgeFromRecipient(list.get(2), glideRequests);
            this.badge3.setVisibility(0);
        }
        if (list.size() > 3) {
            this.typistCount.setText(getResources().getString(R.string.ConversationTypingView__plus_d, Integer.valueOf(list.size() - 2)));
            this.typistCount.setVisibility(0);
        }
    }
}
