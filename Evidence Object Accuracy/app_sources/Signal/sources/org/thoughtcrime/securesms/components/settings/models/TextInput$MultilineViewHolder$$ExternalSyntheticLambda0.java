package org.thoughtcrime.securesms.components.settings.models;

import android.text.Editable;
import androidx.core.util.Consumer;
import org.thoughtcrime.securesms.components.settings.models.TextInput;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class TextInput$MultilineViewHolder$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ TextInput.MultilineModel f$0;

    public /* synthetic */ TextInput$MultilineViewHolder$$ExternalSyntheticLambda0(TextInput.MultilineModel multilineModel) {
        this.f$0 = multilineModel;
    }

    @Override // androidx.core.util.Consumer
    public final void accept(Object obj) {
        TextInput.MultilineViewHolder.m1261bind$lambda2(this.f$0, (Editable) obj);
    }
}
