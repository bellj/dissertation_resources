package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.DialogInterface;
import androidx.activity.OnBackPressedCallback;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: ChangeNumberPinDiffersFragment.kt */
@Metadata(d1 = {"\u0000\u0011\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016¨\u0006\u0004"}, d2 = {"org/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberPinDiffersFragment$onViewCreated$3", "Landroidx/activity/OnBackPressedCallback;", "handleOnBackPressed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberPinDiffersFragment$onViewCreated$3 extends OnBackPressedCallback {
    final /* synthetic */ ChangeNumberPinDiffersFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChangeNumberPinDiffersFragment$onViewCreated$3(ChangeNumberPinDiffersFragment changeNumberPinDiffersFragment) {
        super(true);
        this.this$0 = changeNumberPinDiffersFragment;
    }

    @Override // androidx.activity.OnBackPressedCallback
    public void handleOnBackPressed() {
        new MaterialAlertDialogBuilder(this.this$0.requireContext()).setMessage(R.string.ChangeNumberPinDiffersFragment__keep_old_pin_question).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberPinDiffersFragment$onViewCreated$3$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                ChangeNumberPinDiffersFragment$onViewCreated$3.m608handleOnBackPressed$lambda0(ChangeNumberPinDiffersFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    /* renamed from: handleOnBackPressed$lambda-0 */
    public static final void m608handleOnBackPressed$lambda0(ChangeNumberPinDiffersFragment changeNumberPinDiffersFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(changeNumberPinDiffersFragment, "this$0");
        ChangeNumberUtil.INSTANCE.changeNumberSuccess(changeNumberPinDiffersFragment);
    }
}
