package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.BiFunction;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: DonationReceiptListRepository.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u0004¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListRepository;", "", "()V", "getBadges", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptBadge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListRepository {
    public final Single<List<DonationReceiptBadge>> getBadges() {
        Single<R> map = ApplicationDependencies.getDonationsService().getBoostBadge(Locale.getDefault()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationReceiptListRepository.m1029getBadges$lambda0((ServiceResponse) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "getDonationsService().ge…yList()\n        }\n      }");
        Object map2 = ApplicationDependencies.getDonationsService().getSubscriptionLevels(Locale.getDefault()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationReceiptListRepository.m1030getBadges$lambda2((ServiceResponse) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map2, "getDonationsService().ge…yList()\n        }\n      }");
        Single<List<DonationReceiptBadge>> subscribeOn = map.zipWith(map2, new BiFunction() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return DonationReceiptListRepository.m1031getBadges$lambda3((List) obj, (List) obj2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "boostBadges.zipWith(subB…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getBadges$lambda-0 */
    public static final List m1029getBadges$lambda0(ServiceResponse serviceResponse) {
        if (!serviceResponse.getResult().isPresent()) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        DonationReceiptRecord.Type type = DonationReceiptRecord.Type.BOOST;
        Object obj = serviceResponse.getResult().get();
        Intrinsics.checkNotNullExpressionValue(obj, "response.result.get()");
        return CollectionsKt__CollectionsJVMKt.listOf(new DonationReceiptBadge(type, -1, Badges.fromServiceBadge((SignalServiceProfile.Badge) obj)));
    }

    /* renamed from: getBadges$lambda-2 */
    public static final List m1030getBadges$lambda2(ServiceResponse serviceResponse) {
        if (!serviceResponse.getResult().isPresent()) {
            return CollectionsKt__CollectionsKt.emptyList();
        }
        Map<String, SubscriptionLevels.Level> levels = ((SubscriptionLevels) serviceResponse.getResult().get()).getLevels();
        Intrinsics.checkNotNullExpressionValue(levels, "response.result.get().levels");
        ArrayList arrayList = new ArrayList(levels.size());
        for (Map.Entry<String, SubscriptionLevels.Level> entry : levels.entrySet()) {
            String key = entry.getKey();
            Intrinsics.checkNotNullExpressionValue(key, "it.key");
            int parseInt = Integer.parseInt(key);
            SignalServiceProfile.Badge badge = entry.getValue().getBadge();
            Intrinsics.checkNotNullExpressionValue(badge, "it.value.badge");
            arrayList.add(new DonationReceiptBadge(DonationReceiptRecord.Type.RECURRING, parseInt, Badges.fromServiceBadge(badge)));
        }
        return arrayList;
    }

    /* renamed from: getBadges$lambda-3 */
    public static final List m1031getBadges$lambda3(List list, List list2) {
        Intrinsics.checkNotNullExpressionValue(list, "a");
        Intrinsics.checkNotNullExpressionValue(list2, "b");
        return CollectionsKt___CollectionsKt.plus((Collection) list, (Iterable) list2);
    }
}
