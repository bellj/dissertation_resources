package org.thoughtcrime.securesms.components.settings.app.subscription.currency;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import java.util.ArrayList;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyViewModel;
import org.thoughtcrime.securesms.util.fragments.ListenerNotFoundException;

/* compiled from: SetCurrencyFragment.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "donationPaymentComponent", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SetCurrencyFragment extends DSLSettingsBottomSheetFragment {
    private DonationPaymentComponent donationPaymentComponent;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SetCurrencyViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$viewModel$2
        final /* synthetic */ SetCurrencyFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            SetCurrencyFragmentArgs fromBundle = SetCurrencyFragmentArgs.fromBundle(this.this$0.requireArguments());
            Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
            boolean isBoost = fromBundle.getIsBoost();
            String[] supportedCurrencyCodes = fromBundle.getSupportedCurrencyCodes();
            Intrinsics.checkNotNullExpressionValue(supportedCurrencyCodes, "args.supportedCurrencyCodes");
            return new SetCurrencyViewModel.Factory(isBoost, ArraysKt___ArraysKt.toList(supportedCurrencyCodes));
        }
    });

    public SetCurrencyFragment() {
        super(0, null, 0.0f, 7, null);
    }

    public final SetCurrencyViewModel getViewModel() {
        return (SetCurrencyViewModel) this.viewModel$delegate.getValue();
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m957bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, SetCurrencyFragment setCurrencyFragment, SetCurrencyState setCurrencyState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(setCurrencyFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(setCurrencyState, "state");
        dSLSettingsAdapter.submitList(setCurrencyFragment.getConfiguration(setCurrencyState).toMappingModelList());
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        DonationPaymentComponent donationPaymentComponent;
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ArrayList arrayList = new ArrayList();
        try {
            Fragment fragment = getParentFragment();
            while (true) {
                if (fragment == null) {
                    FragmentActivity requireActivity = requireActivity();
                    if (requireActivity != null) {
                        donationPaymentComponent = (DonationPaymentComponent) requireActivity;
                    } else {
                        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent");
                    }
                } else if (fragment instanceof DonationPaymentComponent) {
                    donationPaymentComponent = fragment;
                    break;
                } else {
                    String name = fragment.getClass().getName();
                    Intrinsics.checkNotNullExpressionValue(name, "parent::class.java.name");
                    arrayList.add(name);
                    fragment = fragment.getParentFragment();
                }
            }
            this.donationPaymentComponent = donationPaymentComponent;
            getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ SetCurrencyFragment f$1;

                {
                    this.f$1 = r2;
                }

                @Override // androidx.lifecycle.Observer
                public final void onChanged(Object obj) {
                    SetCurrencyFragment.$r8$lambda$hPmAuVZfhrQ_OzWJIIGG9lexkQI(DSLSettingsAdapter.this, this.f$1, (SetCurrencyState) obj);
                }
            });
        } catch (ClassCastException e) {
            String name2 = requireActivity().getClass().getName();
            Intrinsics.checkNotNullExpressionValue(name2, "requireActivity()::class.java.name");
            arrayList.add(name2);
            throw new ListenerNotFoundException(arrayList, e);
        }
    }

    private final DSLConfiguration getConfiguration(SetCurrencyState setCurrencyState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(setCurrencyState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1
            final /* synthetic */ SetCurrencyState $state;
            final /* synthetic */ SetCurrencyFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x005c: INVOKE  
                  (r16v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (r4v2 'from' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText : ?: TERNARYnull = ((wrap: int : 0x0000: ARITH  (r0v0 int A[REMOVE]) = (r18v0 int) & (2 int)) != (0 int)) ? (null org.thoughtcrime.securesms.components.settings.DSLSettingsText) : (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText : 0x0041: INVOKE  (r5v3 org.thoughtcrime.securesms.components.settings.DSLSettingsText A[REMOVE]) = 
                  (r3v0 'companion' org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion)
                  (r6v1 'currencyCode' java.lang.String)
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] : 0x003f: NEW_ARRAY  (r5v2 org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] A[REMOVE]) = (0 int) type: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[])
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion.from(java.lang.CharSequence, org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[]):org.thoughtcrime.securesms.components.settings.DSLSettingsText))
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsIcon : ?: TERNARYnull = ((wrap: int : 0x0008: ARITH  (r0v1 int A[REMOVE]) = (r18v0 int) & (4 int)) != (0 int)) ? (null org.thoughtcrime.securesms.components.settings.DSLSettingsIcon) : (null org.thoughtcrime.securesms.components.settings.DSLSettingsIcon))
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsIcon : ?: TERNARYnull = ((wrap: int : 0x000f: ARITH  (r0v2 int A[REMOVE]) = (r18v0 int) & (8 int)) != (0 int)) ? (null org.thoughtcrime.securesms.components.settings.DSLSettingsIcon) : (null org.thoughtcrime.securesms.components.settings.DSLSettingsIcon))
                  (wrap: boolean : ?: TERNARYnull = ((wrap: int : 0x0016: ARITH  (r0v3 int A[REMOVE]) = (r18v0 int) & (16 int)) != (0 int)) ? true : false)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1 : 0x004a: CONSTRUCTOR  (r9v0 org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r13v0 'setCurrencyFragment' org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment)
                  (r2v2 'currency' java.util.Currency)
                 call: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment, java.util.Currency):void type: CONSTRUCTOR)
                  (wrap: kotlin.jvm.functions.Function0 : ?: TERNARYnull = ((wrap: int : 0x001e: ARITH  (r0v4 int A[REMOVE]) = (r18v0 int) & (64 int)) != (0 int)) ? (null kotlin.jvm.functions.Function0<java.lang.Boolean>) : (null kotlin.jvm.functions.Function0))
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.clickPref(org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsIcon, org.thoughtcrime.securesms.components.settings.DSLSettingsIcon, boolean, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0):void in method: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x004a: CONSTRUCTOR  (r9v0 org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (r13v0 'setCurrencyFragment' org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment)
                  (r2v2 'currency' java.util.Currency)
                 call: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1.<init>(org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment, java.util.Currency):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 16 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 22 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r16) {
                /*
                    r15 = this;
                    r0 = r15
                    java.lang.String r1 = "$this$configure"
                    r12 = r16
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r12, r1)
                    org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyState r1 = r0.$state
                    java.util.List r1 = r1.getCurrencies()
                    org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment r13 = r0.this$0
                    java.util.Iterator r1 = r1.iterator()
                L_0x0014:
                    boolean r2 = r1.hasNext()
                    if (r2 == 0) goto L_0x0060
                    java.lang.Object r2 = r1.next()
                    java.util.Currency r2 = (java.util.Currency) r2
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r3 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    java.util.Locale r4 = java.util.Locale.getDefault()
                    java.lang.String r4 = r2.getDisplayName(r4)
                    java.lang.String r5 = "currency.getDisplayName(Locale.getDefault())"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r4, r5)
                    r5 = 0
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r6 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r5]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r4 = r3.from(r4, r6)
                    java.lang.String r6 = r2.getCurrencyCode()
                    java.lang.String r7 = "currency.currencyCode"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r6, r7)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r5 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r5]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r5 = r3.from(r6, r5)
                    r6 = 0
                    r7 = 0
                    r8 = 0
                    org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1 r9 = new org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1$1$1
                    r9.<init>(r13, r2)
                    r10 = 0
                    r11 = 92
                    r14 = 0
                    r2 = r16
                    r3 = r4
                    r4 = r5
                    r5 = r6
                    r6 = r7
                    r7 = r8
                    r8 = r9
                    r9 = r10
                    r10 = r11
                    r11 = r14
                    org.thoughtcrime.securesms.components.settings.DSLConfiguration.clickPref$default(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)
                    goto L_0x0014
                L_0x0060:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }
}
