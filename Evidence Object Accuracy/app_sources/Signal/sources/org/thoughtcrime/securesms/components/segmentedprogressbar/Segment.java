package org.thoughtcrime.securesms.components.segmentedprogressbar;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: Segment.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\t\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u0001:\u0001\u0014B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR$\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\r\u001a\u00020\u000e@FX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment;", "", "animationDurationMillis", "", "(J)V", "getAnimationDurationMillis", "()J", "animationProgressPercentage", "", "getAnimationProgressPercentage", "()F", "setAnimationProgressPercentage", "(F)V", DraftDatabase.DRAFT_VALUE, "Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment$AnimationState;", "animationState", "getAnimationState", "()Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment$AnimationState;", "setAnimationState", "(Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment$AnimationState;)V", "AnimationState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Segment {
    private final long animationDurationMillis;
    private float animationProgressPercentage;
    private AnimationState animationState = AnimationState.IDLE;

    /* compiled from: Segment.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment$AnimationState;", "", "(Ljava/lang/String;I)V", "ANIMATED", "ANIMATING", "IDLE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum AnimationState {
        ANIMATED,
        ANIMATING,
        IDLE
    }

    /* compiled from: Segment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[AnimationState.values().length];
            iArr[AnimationState.ANIMATED.ordinal()] = 1;
            iArr[AnimationState.IDLE.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public Segment(long j) {
        this.animationDurationMillis = j;
    }

    public final long getAnimationDurationMillis() {
        return this.animationDurationMillis;
    }

    public final float getAnimationProgressPercentage() {
        return this.animationProgressPercentage;
    }

    public final void setAnimationProgressPercentage(float f) {
        this.animationProgressPercentage = f;
    }

    public final AnimationState getAnimationState() {
        return this.animationState;
    }

    public final void setAnimationState(AnimationState animationState) {
        Intrinsics.checkNotNullParameter(animationState, DraftDatabase.DRAFT_VALUE);
        int i = WhenMappings.$EnumSwitchMapping$0[animationState.ordinal()];
        this.animationProgressPercentage = i != 1 ? i != 2 ? this.animationProgressPercentage : 0.0f : 1.0f;
        this.animationState = animationState;
    }
}
