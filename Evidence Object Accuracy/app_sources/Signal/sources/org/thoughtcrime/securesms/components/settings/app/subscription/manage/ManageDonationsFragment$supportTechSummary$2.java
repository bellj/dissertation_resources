package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import android.text.SpannableStringBuilder;
import android.view.View;
import androidx.core.content.ContextCompat;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ManageDonationsFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Landroid/text/SpannableStringBuilder;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ManageDonationsFragment$supportTechSummary$2 extends Lambda implements Function0<SpannableStringBuilder> {
    final /* synthetic */ ManageDonationsFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ManageDonationsFragment$supportTechSummary$2(ManageDonationsFragment manageDonationsFragment) {
        super(0);
        this.this$0 = manageDonationsFragment;
    }

    @Override // kotlin.jvm.functions.Function0
    public final SpannableStringBuilder invoke() {
        return new SpannableStringBuilder(this.this$0.requireContext().getString(R.string.SubscribeFragment__make_a_recurring_monthly_donation)).append((CharSequence) " ").append(SpanUtil.readMore(this.this$0.requireContext(), ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_button_secondary_text), new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$supportTechSummary$2$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ManageDonationsFragment$supportTechSummary$2.m967invoke$lambda0(ManageDonationsFragment.this, view);
            }
        }));
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m967invoke$lambda0(ManageDonationsFragment manageDonationsFragment, View view) {
        Intrinsics.checkNotNullParameter(manageDonationsFragment, "this$0");
        NavController findNavController = FragmentKt.findNavController(manageDonationsFragment);
        NavDirections actionManageDonationsFragmentToSubscribeLearnMoreBottomSheetDialog = ManageDonationsFragmentDirections.actionManageDonationsFragmentToSubscribeLearnMoreBottomSheetDialog();
        Intrinsics.checkNotNullExpressionValue(actionManageDonationsFragmentToSubscribeLearnMoreBottomSheetDialog, "actionManageDonationsFra…rnMoreBottomSheetDialog()");
        SafeNavigation.safeNavigate(findNavController, actionManageDonationsFragmentToSubscribeLearnMoreBottomSheetDialog);
    }
}
