package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import android.app.Application;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "throwable", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$updateSubscription$2 extends Lambda implements Function1<Throwable, Unit> {
    final /* synthetic */ SubscribeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SubscribeViewModel$updateSubscription$2(SubscribeViewModel subscribeViewModel) {
        super(1);
        this.this$0 = subscribeViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Throwable th) {
        invoke(th);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final SubscribeState m1075invoke$lambda0(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.READY, false, 95, null);
    }

    public final void invoke(Throwable th) {
        DonationError donationError;
        Intrinsics.checkNotNullParameter(th, "throwable");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$updateSubscription$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$updateSubscription$2.m1075invoke$lambda0((SubscribeState) obj);
            }
        });
        if (th instanceof DonationError) {
            donationError = (DonationError) th;
        } else {
            Log.w(SubscribeViewModel.TAG, "Failed to complete payment or redemption", th, true);
            donationError = DonationError.Companion.genericBadgeRedemptionFailure(DonationErrorSource.SUBSCRIPTION);
        }
        DonationError.Companion companion = DonationError.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        companion.routeDonationError(application, donationError);
    }
}
