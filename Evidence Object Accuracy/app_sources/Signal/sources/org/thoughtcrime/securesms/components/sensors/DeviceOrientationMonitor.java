package org.thoughtcrime.securesms.components.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public final class DeviceOrientationMonitor implements DefaultLifecycleObserver {
    private static final float LANDSCAPE_PITCH_MAXIMUM;
    private static final float LANDSCAPE_PITCH_MINIMUM;
    private static final float MAGNITUDE_MAXIMUM;
    private static final float MAGNITUDE_MINIMUM;
    private final float[] accelerometerReading = new float[3];
    private final EventListener eventListener = new EventListener();
    private final float[] magnetometerReading = new float[3];
    private final MutableLiveData<Orientation> orientation = new MutableLiveData<>(Orientation.PORTRAIT_BOTTOM_EDGE);
    private final float[] orientationAngles = new float[3];
    private final float[] rotationMatrix = new float[9];
    private final SensorManager sensorManager;

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onDestroy(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onDestroy(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
        DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
    }

    public DeviceOrientationMonitor(Context context) {
        this.sensorManager = ServiceUtil.getSensorManager(context);
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onStart(LifecycleOwner lifecycleOwner) {
        Sensor defaultSensor = this.sensorManager.getDefaultSensor(1);
        if (defaultSensor != null) {
            this.sensorManager.registerListener(this.eventListener, defaultSensor, 3, 2);
        }
        Sensor defaultSensor2 = this.sensorManager.getDefaultSensor(2);
        if (defaultSensor2 != null) {
            this.sensorManager.registerListener(this.eventListener, defaultSensor2, 3, 2);
        }
    }

    @Override // androidx.lifecycle.FullLifecycleObserver
    public void onStop(LifecycleOwner lifecycleOwner) {
        this.sensorManager.unregisterListener(this.eventListener);
    }

    public LiveData<Orientation> getOrientation() {
        return Transformations.distinctUntilChanged(this.orientation);
    }

    public void updateOrientationAngles() {
        if (!SensorManager.getRotationMatrix(this.rotationMatrix, null, this.accelerometerReading, this.magnetometerReading)) {
            SensorUtil.getRotationMatrixWithoutMagneticSensorData(this.rotationMatrix, this.accelerometerReading);
        }
        SensorManager.getOrientation(this.rotationMatrix, this.orientationAngles);
        float[] fArr = this.orientationAngles;
        float f = fArr[1];
        float f2 = fArr[2];
        float sqrt = (float) Math.sqrt(Math.pow((double) f, 2.0d) + Math.pow((double) f2, 2.0d));
        if (sqrt <= MAGNITUDE_MAXIMUM && sqrt >= MAGNITUDE_MINIMUM) {
            if (f <= LANDSCAPE_PITCH_MINIMUM || f >= LANDSCAPE_PITCH_MAXIMUM) {
                this.orientation.setValue(Orientation.PORTRAIT_BOTTOM_EDGE);
            } else {
                this.orientation.setValue(f2 > 0.0f ? Orientation.LANDSCAPE_RIGHT_EDGE : Orientation.LANDSCAPE_LEFT_EDGE);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public final class EventListener implements SensorEventListener {
        @Override // android.hardware.SensorEventListener
        public void onAccuracyChanged(Sensor sensor, int i) {
        }

        private EventListener() {
            DeviceOrientationMonitor.this = r1;
        }

        @Override // android.hardware.SensorEventListener
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == 1) {
                System.arraycopy(sensorEvent.values, 0, DeviceOrientationMonitor.this.accelerometerReading, 0, DeviceOrientationMonitor.this.accelerometerReading.length);
            } else if (sensorEvent.sensor.getType() == 2) {
                System.arraycopy(sensorEvent.values, 0, DeviceOrientationMonitor.this.magnetometerReading, 0, DeviceOrientationMonitor.this.magnetometerReading.length);
            }
            DeviceOrientationMonitor.this.updateOrientationAngles();
        }
    }
}
