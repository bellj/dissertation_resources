package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda3 implements View.OnClickListener {
    public final /* synthetic */ ButtonStripPreference.Model f$0;

    public /* synthetic */ ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda3(ButtonStripPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ButtonStripPreference.ViewHolder.m1202bind$lambda4(this.f$0, view);
    }
}
