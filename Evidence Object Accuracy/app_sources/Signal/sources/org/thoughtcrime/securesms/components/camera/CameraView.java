package org.thoughtcrime.securesms.components.camera;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.view.OrientationEventListener;
import android.view.ViewGroup;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import j$.util.Optional;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.signal.qr.kitkat.QrCameraView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.BitmapUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class CameraView extends ViewGroup {
    private static final String TAG = Log.tag(CameraView.class);
    private volatile Optional<Camera> camera;
    private volatile int cameraId;
    private volatile int displayOrientation;
    private List<CameraViewListener> listeners;
    private final OnOrientationChange onOrientationChange;
    private int outputOrientation;
    private Camera.Size previewSize;
    private State state;
    private final CameraSurfaceView surface;

    /* loaded from: classes4.dex */
    public interface CameraViewListener {
        void onCameraFail();

        void onCameraStart();

        void onCameraStop();

        void onImageCapture(byte[] bArr);
    }

    /* loaded from: classes4.dex */
    public enum State {
        PAUSED,
        RESUMED,
        ACTIVE
    }

    public CameraView(Context context) {
        this(context, null);
    }

    public CameraView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CameraView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.camera = Optional.empty();
        this.cameraId = 0;
        this.displayOrientation = -1;
        this.state = State.PAUSED;
        this.listeners = Collections.synchronizedList(new LinkedList());
        this.outputOrientation = -1;
        setBackgroundColor(-16777216);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.CameraView);
            int i2 = obtainStyledAttributes.getInt(0, -1);
            if (i2 != -1) {
                this.cameraId = i2;
            } else if (isMultiCamera()) {
                this.cameraId = TextSecurePreferences.getDirectCaptureCameraId(context);
            }
            obtainStyledAttributes.recycle();
        }
        CameraSurfaceView cameraSurfaceView = new CameraSurfaceView(getContext());
        this.surface = cameraSurfaceView;
        this.onOrientationChange = new OnOrientationChange(context.getApplicationContext());
        addView(cameraSurfaceView);
    }

    public void onResume() {
        if (this.state == State.PAUSED) {
            this.state = State.RESUMED;
            Log.i(TAG, "onResume() queued");
            enqueueTask(new SerialAsyncTask<Void>() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.1
                @Override // org.thoughtcrime.securesms.components.camera.CameraView.SerialAsyncTask
                public Void onRunBackground() {
                    try {
                        long currentTimeMillis = System.currentTimeMillis();
                        CameraView cameraView = CameraView.this;
                        cameraView.camera = Optional.ofNullable(Camera.open(cameraView.cameraId));
                        String str = CameraView.TAG;
                        Log.i(str, "camera.open() -> " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                        synchronized (CameraView.this) {
                            CameraView.this.notifyAll();
                        }
                        if (!CameraView.this.camera.isPresent()) {
                            return null;
                        }
                        CameraView cameraView2 = CameraView.this;
                        cameraView2.onCameraReady((Camera) cameraView2.camera.get());
                        return null;
                    } catch (Exception e) {
                        Log.w(CameraView.TAG, e);
                        return null;
                    }
                }

                public void onPostMain(Void r2) {
                    if (!CameraView.this.camera.isPresent()) {
                        Log.w(CameraView.TAG, "tried to open camera but got null");
                        for (CameraViewListener cameraViewListener : CameraView.this.listeners) {
                            cameraViewListener.onCameraFail();
                        }
                        return;
                    }
                    if (CameraView.this.getActivity().getRequestedOrientation() != -1) {
                        CameraView.this.onOrientationChange.enable();
                    }
                    Log.i(CameraView.TAG, "onResume() completed");
                }
            });
        }
    }

    public void onPause() {
        State state = this.state;
        State state2 = State.PAUSED;
        if (state != state2) {
            this.state = state2;
            Log.i(TAG, "onPause() queued");
            enqueueTask(new SerialAsyncTask<Void>() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.2
                private Optional<Camera> cameraToDestroy;

                /* access modifiers changed from: protected */
                @Override // org.thoughtcrime.securesms.components.camera.CameraView.SerialAsyncTask
                public void onPreMain() {
                    this.cameraToDestroy = CameraView.this.camera;
                    CameraView.this.camera = Optional.empty();
                }

                @Override // org.thoughtcrime.securesms.components.camera.CameraView.SerialAsyncTask
                public Void onRunBackground() {
                    if (this.cameraToDestroy.isPresent()) {
                        try {
                            CameraView.this.stopPreview();
                            this.cameraToDestroy.get().setPreviewCallback(null);
                            this.cameraToDestroy.get().release();
                            Log.w(CameraView.TAG, "released old camera instance");
                        } catch (Exception e) {
                            Log.w(CameraView.TAG, e);
                        }
                    }
                    return null;
                }

                public void onPostMain(Void r2) {
                    CameraView.this.onOrientationChange.disable();
                    CameraView.this.displayOrientation = -1;
                    CameraView.this.outputOrientation = -1;
                    CameraView cameraView = CameraView.this;
                    cameraView.removeView(cameraView.surface);
                    CameraView cameraView2 = CameraView.this;
                    cameraView2.addView(cameraView2.surface);
                    Log.i(CameraView.TAG, "onPause() completed");
                }
            });
            for (CameraViewListener cameraViewListener : this.listeners) {
                cameraViewListener.onCameraStop();
            }
        }
    }

    public boolean isStarted() {
        return this.state != State.PAUSED;
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7 = i3 - i;
        int i8 = i4 - i2;
        if (!this.camera.isPresent() || this.previewSize == null) {
            i5 = i7;
            i6 = i8;
        } else if (this.displayOrientation == 90 || this.displayOrientation == 270) {
            Camera.Size size = this.previewSize;
            i5 = size.height;
            i6 = size.width;
        } else {
            Camera.Size size2 = this.previewSize;
            i5 = size2.width;
            i6 = size2.height;
        }
        if (i6 == 0 || i5 == 0) {
            Log.w(TAG, "skipping layout due to zero-width/height preview size");
            return;
        }
        int i9 = i7 * i6;
        int i10 = i8 * i5;
        if (i9 > i10) {
            int i11 = i9 / i5;
            this.surface.layout(0, (i8 - i11) / 2, i7, (i8 + i11) / 2);
            return;
        }
        int i12 = i10 / i6;
        this.surface.layout((i7 - i12) / 2, 0, (i7 + i12) / 2, i8);
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        String str = TAG;
        Log.i(str, "onSizeChanged(" + i3 + "x" + i4 + " -> " + i + "x" + i2 + ")");
        super.onSizeChanged(i, i2, i3, i4);
        if (this.camera.isPresent()) {
            startPreview(this.camera.get().getParameters());
        }
    }

    public void addListener(CameraViewListener cameraViewListener) {
        this.listeners.add(cameraViewListener);
    }

    public void setPreviewCallback(final QrCameraView.PreviewCallback previewCallback) {
        enqueueTask(new PostInitializationTask<Void>() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.3
            public void onPostMain(Void r2) {
                if (CameraView.this.camera.isPresent()) {
                    ((Camera) CameraView.this.camera.get()).setPreviewCallback(new Camera.PreviewCallback() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.3.1
                        @Override // android.hardware.Camera.PreviewCallback
                        public void onPreviewFrame(byte[] bArr, Camera camera) {
                            if (CameraView.this.camera.isPresent()) {
                                int cameraPictureOrientation = CameraView.this.getCameraPictureOrientation();
                                Camera.Size previewSize = camera.getParameters().getPreviewSize();
                                if (bArr != null) {
                                    previewCallback.onPreviewFrame(new QrCameraView.PreviewFrame(bArr, previewSize.width, previewSize.height, cameraPictureOrientation));
                                }
                            }
                        }
                    });
                }
            }
        });
    }

    public boolean isMultiCamera() {
        return Camera.getNumberOfCameras() > 1;
    }

    public boolean isRearCamera() {
        return this.cameraId == 0;
    }

    public void flipCamera() {
        int i = 1;
        if (Camera.getNumberOfCameras() > 1) {
            if (this.cameraId != 0) {
                i = 0;
            }
            this.cameraId = i;
            onPause();
            onResume();
            TextSecurePreferences.setDirectCaptureCameraId(getContext(), this.cameraId);
        }
    }

    public void onCameraReady(final Camera camera) {
        final Camera.Parameters parameters = camera.getParameters();
        parameters.setRecordingHint(true);
        List<String> supportedFocusModes = parameters.getSupportedFocusModes();
        if (supportedFocusModes.contains("continuous-picture")) {
            parameters.setFocusMode("continuous-picture");
        } else if (supportedFocusModes.contains("continuous-video")) {
            parameters.setFocusMode("continuous-video");
        }
        this.displayOrientation = CameraUtils.getCameraDisplayOrientation(getActivity(), getCameraInfo());
        camera.setDisplayOrientation(this.displayOrientation);
        camera.setParameters(parameters);
        enqueueTask(new PostInitializationTask<Void>() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.4
            @Override // org.thoughtcrime.securesms.components.camera.CameraView.SerialAsyncTask
            public Void onRunBackground() {
                try {
                    camera.setPreviewDisplay(CameraView.this.surface.getHolder());
                    CameraView.this.startPreview(parameters);
                    return null;
                } catch (Exception e) {
                    Log.w(CameraView.TAG, "couldn't set preview display", e);
                    return null;
                }
            }
        });
    }

    public void startPreview(Camera.Parameters parameters) {
        if (this.camera.isPresent()) {
            try {
                Camera camera = this.camera.get();
                Camera.Size preferredPreviewSize = getPreferredPreviewSize(parameters);
                if (preferredPreviewSize == null || parameters.getPreviewSize().equals(preferredPreviewSize)) {
                    this.previewSize = parameters.getPreviewSize();
                } else {
                    String str = TAG;
                    Log.i(str, "starting preview with size " + preferredPreviewSize.width + "x" + preferredPreviewSize.height);
                    if (this.state == State.ACTIVE) {
                        stopPreview();
                    }
                    this.previewSize = preferredPreviewSize;
                    parameters.setPreviewSize(preferredPreviewSize.width, preferredPreviewSize.height);
                    camera.setParameters(parameters);
                }
                long currentTimeMillis = System.currentTimeMillis();
                camera.startPreview();
                String str2 = TAG;
                Log.i(str2, "camera.startPreview() -> " + (System.currentTimeMillis() - currentTimeMillis) + "ms");
                this.state = State.ACTIVE;
                ThreadUtil.runOnMain(new Runnable() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.5
                    @Override // java.lang.Runnable
                    public void run() {
                        CameraView.this.requestLayout();
                        for (CameraViewListener cameraViewListener : CameraView.this.listeners) {
                            cameraViewListener.onCameraStart();
                        }
                    }
                });
            } catch (Exception e) {
                Log.w(TAG, e);
            }
        }
    }

    public void stopPreview() {
        if (this.camera.isPresent()) {
            try {
                this.camera.get().stopPreview();
                this.state = State.RESUMED;
            } catch (Exception e) {
                Log.w(TAG, e);
            }
        }
    }

    private Camera.Size getPreferredPreviewSize(Camera.Parameters parameters) {
        return CameraUtils.getPreferredPreviewSize(this.displayOrientation, getMeasuredWidth(), getMeasuredHeight(), parameters);
    }

    public int getCameraPictureOrientation() {
        if (getActivity().getRequestedOrientation() != -1) {
            this.outputOrientation = getCameraPictureRotation(getActivity().getWindowManager().getDefaultDisplay().getOrientation());
        } else if (getCameraInfo().facing == 1) {
            this.outputOrientation = (360 - this.displayOrientation) % 360;
        } else {
            this.outputOrientation = this.displayOrientation;
        }
        return this.outputOrientation;
    }

    private boolean isTroublemaker() {
        if (getCameraInfo().facing != 1 || !"JWR66Y".equals(Build.DISPLAY) || !"yakju".equals(Build.PRODUCT)) {
            return false;
        }
        return true;
    }

    private Camera.CameraInfo getCameraInfo() {
        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
        Camera.getCameraInfo(this.cameraId, cameraInfo);
        return cameraInfo;
    }

    public Activity getActivity() {
        return (Activity) getContext();
    }

    public int getCameraPictureRotation(int i) {
        Camera.CameraInfo cameraInfo = getCameraInfo();
        int i2 = ((i + 45) / 90) * 90;
        if (cameraInfo.facing == 1) {
            return ((cameraInfo.orientation - i2) + 360) % 360;
        }
        return (cameraInfo.orientation + i2) % 360;
    }

    /* loaded from: classes4.dex */
    public class OnOrientationChange extends OrientationEventListener {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public OnOrientationChange(Context context) {
            super(context);
            CameraView.this = r1;
            disable();
        }

        @Override // android.view.OrientationEventListener
        public void onOrientationChanged(int i) {
            int cameraPictureRotation;
            if (CameraView.this.camera.isPresent() && i != -1 && (cameraPictureRotation = CameraView.this.getCameraPictureRotation(i)) != CameraView.this.outputOrientation) {
                CameraView.this.outputOrientation = cameraPictureRotation;
                Camera.Parameters parameters = ((Camera) CameraView.this.camera.get()).getParameters();
                parameters.setRotation(CameraView.this.outputOrientation);
                try {
                    ((Camera) CameraView.this.camera.get()).setParameters(parameters);
                } catch (Exception e) {
                    Log.e(CameraView.TAG, "Exception updating camera parameters in orientation change", e);
                }
            }
        }
    }

    public void takePicture(final Rect rect) {
        if (!this.camera.isPresent() || this.camera.get().getParameters() == null) {
            Log.w(TAG, "camera not in capture-ready state");
        } else {
            this.camera.get().setOneShotPreviewCallback(new Camera.PreviewCallback() { // from class: org.thoughtcrime.securesms.components.camera.CameraView.6
                @Override // android.hardware.Camera.PreviewCallback
                public void onPreviewFrame(byte[] bArr, Camera camera) {
                    int cameraPictureOrientation = CameraView.this.getCameraPictureOrientation();
                    Camera.Size previewSize = camera.getParameters().getPreviewSize();
                    Rect croppedRect = CameraView.this.getCroppedRect(previewSize, rect, cameraPictureOrientation);
                    String str = CameraView.TAG;
                    Log.i(str, "previewSize: " + previewSize.width + "x" + previewSize.height);
                    String str2 = CameraView.TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("data bytes: ");
                    sb.append(bArr.length);
                    Log.i(str2, sb.toString());
                    String str3 = CameraView.TAG;
                    Log.i(str3, "previewFormat: " + camera.getParameters().getPreviewFormat());
                    String str4 = CameraView.TAG;
                    Log.i(str4, "croppingRect: " + croppedRect.toString());
                    String str5 = CameraView.TAG;
                    Log.i(str5, "rotation: " + cameraPictureOrientation);
                    new CaptureTask(previewSize, cameraPictureOrientation, croppedRect).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, bArr);
                }
            });
        }
    }

    public Rect getCroppedRect(Camera.Size size, Rect rect, int i) {
        int i2 = size.width;
        int i3 = size.height;
        int i4 = i % SubsamplingScaleImageView.ORIENTATION_180;
        if (i4 > 0) {
            rotateRect(rect);
        }
        float f = (float) i2;
        float width = f / ((float) rect.width());
        float f2 = (float) i3;
        if (((float) rect.height()) * width > f2) {
            width = f2 / ((float) rect.height());
        }
        float width2 = ((float) rect.width()) * width;
        float height = ((float) rect.height()) * width;
        float f3 = isTroublemaker() ? f - (width2 / 2.0f) : (float) (i2 / 2);
        float f4 = (float) (i3 / 2);
        float f5 = width2 / 2.0f;
        float f6 = height / 2.0f;
        rect.set((int) (f3 - f5), (int) (f4 - f6), (int) (f3 + f5), (int) (f4 + f6));
        if (i4 > 0) {
            rotateRect(rect);
        }
        return rect;
    }

    private void rotateRect(Rect rect) {
        rect.set(rect.top, rect.left, rect.bottom, rect.right);
    }

    private void enqueueTask(SerialAsyncTask serialAsyncTask) {
        AsyncTask.SERIAL_EXECUTOR.execute(serialAsyncTask);
    }

    /* loaded from: classes4.dex */
    public static abstract class SerialAsyncTask<Result> implements Runnable {
        /* renamed from: onPostMain */
        public void lambda$run$0(Result result) {
        }

        public void onPreMain() {
        }

        protected Result onRunBackground() {
            return null;
        }

        protected boolean onWait() {
            return true;
        }

        @Override // java.lang.Runnable
        public final void run() {
            if (!onWait()) {
                Log.w(CameraView.TAG, "skipping task, preconditions not met in onWait()");
                return;
            }
            ThreadUtil.runOnMainSync(new CameraView$SerialAsyncTask$$ExternalSyntheticLambda0(this));
            ThreadUtil.runOnMainSync(new CameraView$SerialAsyncTask$$ExternalSyntheticLambda1(this, onRunBackground()));
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public abstract class PostInitializationTask<Result> extends SerialAsyncTask<Result> {
        private PostInitializationTask() {
            CameraView.this = r1;
        }

        @Override // org.thoughtcrime.securesms.components.camera.CameraView.SerialAsyncTask
        protected boolean onWait() {
            synchronized (CameraView.this) {
                if (!CameraView.this.camera.isPresent()) {
                    return false;
                }
                while (true) {
                    if (CameraView.this.getMeasuredHeight() > 0 && CameraView.this.getMeasuredWidth() > 0 && CameraView.this.surface.isReady()) {
                        return true;
                    }
                    Log.i(CameraView.TAG, String.format("waiting. surface ready? %s", Boolean.valueOf(CameraView.this.surface.isReady())));
                    Util.wait(CameraView.this, 0);
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    private class CaptureTask extends AsyncTask<byte[], Void, byte[]> {
        private final Rect croppingRect;
        private final Camera.Size previewSize;
        private final int rotation;

        public CaptureTask(Camera.Size size, int i, Rect rect) {
            CameraView.this = r1;
            this.previewSize = size;
            this.rotation = i;
            this.croppingRect = rect;
        }

        public byte[] doInBackground(byte[]... bArr) {
            byte[] bArr2 = bArr[0];
            try {
                Camera.Size size = this.previewSize;
                int i = size.width;
                int i2 = size.height;
                int i3 = this.rotation;
                Rect rect = this.croppingRect;
                boolean z = true;
                if (CameraView.this.cameraId != 1) {
                    z = false;
                }
                return BitmapUtil.createFromNV21(bArr2, i, i2, i3, rect, z);
            } catch (IOException e) {
                Log.w(CameraView.TAG, e);
                return null;
            }
        }

        public void onPostExecute(byte[] bArr) {
            if (bArr != null) {
                for (CameraViewListener cameraViewListener : CameraView.this.listeners) {
                    cameraViewListener.onImageCapture(bArr);
                }
            }
        }
    }

    /* loaded from: classes4.dex */
    private static class PreconditionsNotMetException extends Exception {
        private PreconditionsNotMetException() {
        }
    }
}
