package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0010\u001a\u00020\u0000H\u0016R\u0011\u0010\u0007\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\fR\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/RadioPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "isEnabled", "", "isChecked", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;ZZLkotlin/jvm/functions/Function0;)V", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "areContentsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RadioPreference extends PreferenceModel<RadioPreference> {
    private final boolean isChecked;
    private final Function0<Unit> onClick;

    public /* synthetic */ RadioPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, boolean z, boolean z2, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dSLSettingsText, (i & 2) != 0 ? null : dSLSettingsText2, z, z2, function0);
    }

    public final boolean isChecked() {
        return this.isChecked;
    }

    public final Function0<Unit> getOnClick() {
        return this.onClick;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RadioPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, boolean z, boolean z2, Function0<Unit> function0) {
        super(dSLSettingsText, dSLSettingsText2, null, null, z, 12, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.isChecked = z2;
        this.onClick = function0;
    }

    public boolean areContentsTheSame(RadioPreference radioPreference) {
        Intrinsics.checkNotNullParameter(radioPreference, "newItem");
        return super.areContentsTheSame(radioPreference) && this.isChecked == radioPreference.isChecked;
    }
}
