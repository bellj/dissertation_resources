package org.thoughtcrime.securesms.components.menu;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: ActionItem.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B+\b\u0007\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0003\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J1\u0010\u0015\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0003\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "", "iconRes", "", MultiselectForwardFragment.DIALOG_TITLE, "", "tintRes", "action", "Ljava/lang/Runnable;", "(ILjava/lang/CharSequence;ILjava/lang/Runnable;)V", "getAction", "()Ljava/lang/Runnable;", "getIconRes", "()I", "getTintRes", "getTitle", "()Ljava/lang/CharSequence;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ActionItem {
    private final Runnable action;
    private final int iconRes;
    private final int tintRes;
    private final CharSequence title;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public ActionItem(int i, CharSequence charSequence, Runnable runnable) {
        this(i, charSequence, 0, runnable, 4, null);
        Intrinsics.checkNotNullParameter(charSequence, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(runnable, "action");
    }

    public static /* synthetic */ ActionItem copy$default(ActionItem actionItem, int i, CharSequence charSequence, int i2, Runnable runnable, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            i = actionItem.iconRes;
        }
        if ((i3 & 2) != 0) {
            charSequence = actionItem.title;
        }
        if ((i3 & 4) != 0) {
            i2 = actionItem.tintRes;
        }
        if ((i3 & 8) != 0) {
            runnable = actionItem.action;
        }
        return actionItem.copy(i, charSequence, i2, runnable);
    }

    public final int component1() {
        return this.iconRes;
    }

    public final CharSequence component2() {
        return this.title;
    }

    public final int component3() {
        return this.tintRes;
    }

    public final Runnable component4() {
        return this.action;
    }

    public final ActionItem copy(int i, CharSequence charSequence, int i2, Runnable runnable) {
        Intrinsics.checkNotNullParameter(charSequence, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(runnable, "action");
        return new ActionItem(i, charSequence, i2, runnable);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActionItem)) {
            return false;
        }
        ActionItem actionItem = (ActionItem) obj;
        return this.iconRes == actionItem.iconRes && Intrinsics.areEqual(this.title, actionItem.title) && this.tintRes == actionItem.tintRes && Intrinsics.areEqual(this.action, actionItem.action);
    }

    public int hashCode() {
        return (((((this.iconRes * 31) + this.title.hashCode()) * 31) + this.tintRes) * 31) + this.action.hashCode();
    }

    public String toString() {
        return "ActionItem(iconRes=" + this.iconRes + ", title=" + ((Object) this.title) + ", tintRes=" + this.tintRes + ", action=" + this.action + ')';
    }

    public ActionItem(int i, CharSequence charSequence, int i2, Runnable runnable) {
        Intrinsics.checkNotNullParameter(charSequence, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(runnable, "action");
        this.iconRes = i;
        this.title = charSequence;
        this.tintRes = i2;
        this.action = runnable;
    }

    public /* synthetic */ ActionItem(int i, CharSequence charSequence, int i2, Runnable runnable, int i3, DefaultConstructorMarker defaultConstructorMarker) {
        this(i, charSequence, (i3 & 4) != 0 ? R.color.signal_colorOnSurface : i2, runnable);
    }

    public final int getIconRes() {
        return this.iconRes;
    }

    public final CharSequence getTitle() {
        return this.title;
    }

    public final int getTintRes() {
        return this.tintRes;
    }

    public final Runnable getAction() {
        return this.action;
    }
}
