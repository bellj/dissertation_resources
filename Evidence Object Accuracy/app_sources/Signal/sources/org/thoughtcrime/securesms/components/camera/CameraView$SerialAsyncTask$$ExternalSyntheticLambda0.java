package org.thoughtcrime.securesms.components.camera;

import org.thoughtcrime.securesms.components.camera.CameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraView$SerialAsyncTask$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CameraView.SerialAsyncTask f$0;

    public /* synthetic */ CameraView$SerialAsyncTask$$ExternalSyntheticLambda0(CameraView.SerialAsyncTask serialAsyncTask) {
        this.f$0 = serialAsyncTask;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.onPreMain();
    }
}
