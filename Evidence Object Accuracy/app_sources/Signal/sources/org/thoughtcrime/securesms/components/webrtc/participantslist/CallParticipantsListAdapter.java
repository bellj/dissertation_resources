package org.thoughtcrime.securesms.components.webrtc.participantslist;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* loaded from: classes4.dex */
public class CallParticipantsListAdapter extends MappingAdapter {
    public CallParticipantsListAdapter() {
        registerFactory(CallParticipantsListHeader.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.participantslist.CallParticipantsListAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new CallParticipantsListHeaderViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.call_participants_list_header));
        registerFactory(CallParticipantViewState.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.webrtc.participantslist.CallParticipantsListAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new CallParticipantViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.call_participants_list_item));
    }
}
