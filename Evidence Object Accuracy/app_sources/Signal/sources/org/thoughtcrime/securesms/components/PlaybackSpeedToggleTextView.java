package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import androidx.appcompat.widget.AppCompatTextView;
import kotlin.Metadata;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: PlaybackSpeedToggleTextView.kt */
@Metadata(d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0010\u0015\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001)B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\u0006\u0010\u0019\u001a\u00020\u001aJ\b\u0010\u001b\u001a\u00020\fH\u0002J\b\u0010\u001c\u001a\u00020\u0015H\u0002J\b\u0010\u001d\u001a\u00020\u0007H\u0002J\u0012\u0010\u001e\u001a\u00020\u001f2\b\u0010 \u001a\u0004\u0018\u00010!H\u0017J\u000e\u0010\"\u001a\u00020\u001a2\u0006\u0010#\u001a\u00020\u0015J\u0012\u0010$\u001a\u00020\u001a2\b\u0010%\u001a\u0004\u0018\u00010&H\u0016J\b\u0010'\u001a\u00020\u001aH\u0002J\b\u0010(\u001a\u00020\u001aH\u0002R\u000e\u0010\t\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u0016\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\u0004\n\u0002\u0010\rR\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0012\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u000e¢\u0006\u0004\n\u0002\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/components/PlaybackSpeedToggleTextView;", "Landroidx/appcompat/widget/AppCompatTextView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "currentSpeedIndex", "labels", "", "", "[Ljava/lang/String;", "playbackSpeedListener", "Lorg/thoughtcrime/securesms/components/PlaybackSpeedToggleTextView$PlaybackSpeedListener;", "getPlaybackSpeedListener", "()Lorg/thoughtcrime/securesms/components/PlaybackSpeedToggleTextView$PlaybackSpeedListener;", "setPlaybackSpeedListener", "(Lorg/thoughtcrime/securesms/components/PlaybackSpeedToggleTextView$PlaybackSpeedListener;)V", "requestedSpeed", "", "Ljava/lang/Float;", "speeds", "", "clearRequestedSpeed", "", "getCurrentLabel", "getCurrentSpeed", "getNextSpeedIndex", "onTouchEvent", "", "event", "Landroid/view/MotionEvent;", "setCurrentSpeed", "speed", "setOnClickListener", "l", "Landroid/view/View$OnClickListener;", "zoomIn", "zoomOut", "PlaybackSpeedListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PlaybackSpeedToggleTextView extends AppCompatTextView {
    private int currentSpeedIndex;
    private final String[] labels;
    private PlaybackSpeedListener playbackSpeedListener;
    private Float requestedSpeed;
    private final int[] speeds;

    /* compiled from: PlaybackSpeedToggleTextView.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u0007\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/PlaybackSpeedToggleTextView$PlaybackSpeedListener;", "", "onPlaybackSpeedChanged", "", "speed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface PlaybackSpeedListener {
        void onPlaybackSpeedChanged(float f);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public PlaybackSpeedToggleTextView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public PlaybackSpeedToggleTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ PlaybackSpeedToggleTextView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PlaybackSpeedToggleTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        int[] intArray = context.getResources().getIntArray(R.array.PlaybackSpeedToggleTextView__speeds);
        Intrinsics.checkNotNullExpressionValue(intArray, "context.resources.getInt…edToggleTextView__speeds)");
        this.speeds = intArray;
        String[] stringArray = context.getResources().getStringArray(R.array.PlaybackSpeedToggleTextView__speed_labels);
        Intrinsics.checkNotNullExpressionValue(stringArray, "context.resources.getStr…leTextView__speed_labels)");
        this.labels = stringArray;
        setText(getCurrentLabel());
        super.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.PlaybackSpeedToggleTextView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                PlaybackSpeedToggleTextView.m486_init_$lambda0(PlaybackSpeedToggleTextView.this, view);
            }
        });
        setClickable(false);
    }

    public final PlaybackSpeedListener getPlaybackSpeedListener() {
        return this.playbackSpeedListener;
    }

    public final void setPlaybackSpeedListener(PlaybackSpeedListener playbackSpeedListener) {
        this.playbackSpeedListener = playbackSpeedListener;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m486_init_$lambda0(PlaybackSpeedToggleTextView playbackSpeedToggleTextView, View view) {
        Intrinsics.checkNotNullParameter(playbackSpeedToggleTextView, "this$0");
        playbackSpeedToggleTextView.currentSpeedIndex = playbackSpeedToggleTextView.getNextSpeedIndex();
        playbackSpeedToggleTextView.setText(playbackSpeedToggleTextView.getCurrentLabel());
        playbackSpeedToggleTextView.requestedSpeed = Float.valueOf(playbackSpeedToggleTextView.getCurrentSpeed());
        PlaybackSpeedListener playbackSpeedListener = playbackSpeedToggleTextView.playbackSpeedListener;
        if (playbackSpeedListener != null) {
            playbackSpeedListener.onPlaybackSpeedChanged(playbackSpeedToggleTextView.getCurrentSpeed());
        }
    }

    @Override // android.widget.TextView, android.view.View
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (isClickable()) {
            Integer valueOf = motionEvent != null ? Integer.valueOf(motionEvent.getAction()) : null;
            if (valueOf != null && valueOf.intValue() == 0) {
                zoomIn();
            } else if (valueOf != null && valueOf.intValue() == 1) {
                zoomOut();
            } else if (valueOf != null && valueOf.intValue() == 3) {
                zoomOut();
            }
        }
        return super.onTouchEvent(motionEvent);
    }

    public final void clearRequestedSpeed() {
        this.requestedSpeed = null;
    }

    public final void setCurrentSpeed(float f) {
        Float f2;
        if (!(f == getCurrentSpeed()) && ((f2 = this.requestedSpeed) == null || Intrinsics.areEqual(f2, f))) {
            this.requestedSpeed = null;
            int i = ArraysKt___ArraysKt.indexOf(this.speeds, (int) (((float) 100) * f));
            if (i != -1) {
                this.currentSpeedIndex = i;
                setText(getCurrentLabel());
                return;
            }
            throw new IllegalArgumentException("Invalid Speed " + f);
        } else if (Intrinsics.areEqual(this.requestedSpeed, f)) {
            this.requestedSpeed = null;
        }
    }

    private final int getNextSpeedIndex() {
        return (this.currentSpeedIndex + 1) % this.speeds.length;
    }

    private final float getCurrentSpeed() {
        return ((float) this.speeds[this.currentSpeedIndex]) / 100.0f;
    }

    private final String getCurrentLabel() {
        return this.labels[this.currentSpeedIndex];
    }

    private final void zoomIn() {
        animate().setInterpolator(new DecelerateInterpolator()).setDuration(150).scaleX(1.2f).scaleY(1.2f);
    }

    private final void zoomOut() {
        animate().setInterpolator(new DecelerateInterpolator()).setDuration(150).scaleX(1.0f).scaleY(1.0f);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        throw new UnsupportedOperationException();
    }
}
