package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class LabeledEditText extends FrameLayout implements View.OnFocusChangeListener {
    private View border;
    private EditText input;
    private TextView label;
    private ViewGroup textContainer;

    public LabeledEditText(Context context) {
        super(context);
        init(null);
    }

    public LabeledEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        String str;
        FrameLayout.inflate(getContext(), R.layout.labeled_edit_text, this);
        int i = R.layout.labeled_edit_text_default;
        int i2 = -16777216;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.LabeledEditText, 0, 0);
            str = obtainStyledAttributes.getString(1);
            i2 = obtainStyledAttributes.getColor(0, -16777216);
            i = obtainStyledAttributes.getResourceId(2, R.layout.labeled_edit_text_default);
            obtainStyledAttributes.recycle();
        } else {
            str = "";
        }
        this.label = (TextView) findViewById(R.id.label);
        this.border = findViewById(R.id.border);
        this.textContainer = (ViewGroup) findViewById(R.id.text_container);
        FrameLayout.inflate(getContext(), i, this.textContainer);
        this.input = (EditText) findViewById(R.id.input);
        this.label.setText(str);
        this.label.setBackgroundColor(i2);
        if (TextUtils.isEmpty(str)) {
            this.label.setVisibility(4);
        }
        this.input.setOnFocusChangeListener(this);
    }

    public EditText getInput() {
        return this.input;
    }

    public void setText(String str) {
        this.input.setText(str);
    }

    public Editable getText() {
        return this.input.getText();
    }

    @Override // android.view.View.OnFocusChangeListener
    public void onFocusChange(View view, boolean z) {
        this.border.setBackgroundResource(z ? R.drawable.labeled_edit_text_background_active : R.drawable.labeled_edit_text_background_inactive);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.input.setEnabled(z);
    }

    public void focusAndMoveCursorToEndAndOpenKeyboard() {
        ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.input);
    }
}
