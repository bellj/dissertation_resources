package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;

/* loaded from: classes4.dex */
public class ListenableHorizontalScrollView extends HorizontalScrollView {
    private OnScrollListener listener;

    /* loaded from: classes4.dex */
    public interface OnScrollListener {
        void onScroll(int i, int i2);
    }

    public ListenableHorizontalScrollView(Context context) {
        super(context);
    }

    public ListenableHorizontalScrollView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setOnScrollListener(OnScrollListener onScrollListener) {
        this.listener = onScrollListener;
    }

    @Override // android.view.View
    protected void onScrollChanged(int i, int i2, int i3, int i4) {
        OnScrollListener onScrollListener = this.listener;
        if (onScrollListener != null) {
            onScrollListener.onScroll(i, i3);
        }
        super.onScrollChanged(i, i2, i3, i4);
    }
}
