package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class AsciiEmojiView extends View {
    private String emoji;
    private final Paint paint = new Paint(3);

    public AsciiEmojiView(Context context) {
        super(context);
    }

    public AsciiEmojiView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setEmoji(String str) {
        this.emoji = str;
    }

    @Override // android.view.View
    protected void onDraw(Canvas canvas) {
        if (!TextUtils.isEmpty(this.emoji)) {
            float height = ((((float) getHeight()) * 0.75f) - ((float) getPaddingTop())) - ((float) getPaddingBottom());
            this.paint.setTextSize(height);
            this.paint.setColor(ContextCompat.getColor(getContext(), R.color.signal_inverse_primary));
            this.paint.setTextAlign(Paint.Align.CENTER);
            int width = getWidth() / 2;
            int height2 = (int) (((float) (getHeight() / 2)) - ((this.paint.descent() + this.paint.ascent()) / 2.0f));
            float measureText = this.paint.measureText(this.emoji) / ((float) ((getWidth() - getPaddingLeft()) - getPaddingRight()));
            if (measureText > 1.0f) {
                this.paint.setTextSize(height / measureText);
                height2 = (int) (((float) (getHeight() / 2)) - ((this.paint.descent() + this.paint.ascent()) / 2.0f));
            }
            canvas.drawText(this.emoji, (float) width, (float) height2, this.paint);
        }
    }

    @Override // android.view.View
    protected void onMeasure(int i, int i2) {
        super.onMeasure(i, i);
    }
}
