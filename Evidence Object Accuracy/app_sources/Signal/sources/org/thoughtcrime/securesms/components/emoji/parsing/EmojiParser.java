package org.thoughtcrime.securesms.components.emoji.parsing;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiTree;
import org.thoughtcrime.securesms.emoji.JumboEmoji;

/* loaded from: classes4.dex */
public class EmojiParser {
    private final EmojiTree emojiTree;

    public EmojiParser(EmojiTree emojiTree) {
        this.emojiTree = emojiTree;
    }

    public CandidateList findCandidates(CharSequence charSequence) {
        LinkedList linkedList = new LinkedList();
        if (charSequence == null) {
            return new CandidateList(linkedList, false);
        }
        boolean z = charSequence.length() > 0;
        int i = 0;
        while (i < charSequence.length()) {
            int emojiEndPos = getEmojiEndPos(charSequence, i);
            if (emojiEndPos != -1) {
                EmojiDrawInfo emoji = this.emojiTree.getEmoji(charSequence, i, emojiEndPos);
                int i2 = emojiEndPos + 2;
                if (i2 <= charSequence.length() && Fitzpatrick.fitzpatrickFromUnicode(charSequence, emojiEndPos) != null) {
                    emojiEndPos = i2;
                }
                linkedList.add(new Candidate(i, emojiEndPos, emoji));
                i = emojiEndPos - 1;
            } else if (charSequence.charAt(i) != ' ') {
                z = false;
            }
            i++;
        }
        return new CandidateList(linkedList, (!linkedList.isEmpty()) & z);
    }

    private int getEmojiEndPos(CharSequence charSequence, int i) {
        int i2 = -1;
        for (int i3 = i + 1; i3 <= charSequence.length(); i3++) {
            EmojiTree.Matches isEmoji = this.emojiTree.isEmoji(charSequence, i, i3);
            if (isEmoji.exactMatch()) {
                i2 = i3;
            } else if (isEmoji.impossibleMatch()) {
                return i2;
            }
        }
        return i2;
    }

    /* loaded from: classes4.dex */
    public static class Candidate {
        private final EmojiDrawInfo drawInfo;
        private final int endIndex;
        private final int startIndex;

        Candidate(int i, int i2, EmojiDrawInfo emojiDrawInfo) {
            this.startIndex = i;
            this.endIndex = i2;
            this.drawInfo = emojiDrawInfo;
        }

        public EmojiDrawInfo getDrawInfo() {
            return this.drawInfo;
        }

        public int getEndIndex() {
            return this.endIndex;
        }

        public int getStartIndex() {
            return this.startIndex;
        }
    }

    /* loaded from: classes4.dex */
    public static class CandidateList implements Iterable<Candidate> {
        public final boolean allEmojis;
        public final List<Candidate> list;

        public CandidateList(List<Candidate> list, boolean z) {
            this.list = list;
            this.allEmojis = z;
        }

        public int size() {
            return this.list.size();
        }

        public boolean hasJumboForAll() {
            for (Candidate candidate : this.list) {
                if (!JumboEmoji.hasJumboEmoji(candidate.drawInfo)) {
                    return false;
                }
            }
            return true;
        }

        @Override // java.lang.Iterable
        public Iterator<Candidate> iterator() {
            return this.list.iterator();
        }
    }
}
