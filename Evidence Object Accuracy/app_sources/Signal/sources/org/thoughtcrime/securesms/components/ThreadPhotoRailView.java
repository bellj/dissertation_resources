package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.database.Cursor;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.util.MediaUtil;

/* loaded from: classes4.dex */
public class ThreadPhotoRailView extends FrameLayout {
    private OnItemClickedListener listener;
    private final RecyclerView recyclerView;

    /* loaded from: classes4.dex */
    public interface OnItemClickedListener {
        void onItemClicked(MediaDatabase.MediaRecord mediaRecord);
    }

    public ThreadPhotoRailView(Context context) {
        this(context, null);
    }

    public ThreadPhotoRailView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ThreadPhotoRailView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.recipient_preference_photo_rail, this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.photo_list);
        this.recyclerView = recyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(context, 0, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
    }

    public void setListener(OnItemClickedListener onItemClickedListener) {
        this.listener = onItemClickedListener;
        if (this.recyclerView.getAdapter() != null) {
            ((ThreadPhotoRailAdapter) this.recyclerView.getAdapter()).setListener(onItemClickedListener);
        }
    }

    public void setCursor(GlideRequests glideRequests, Cursor cursor) {
        this.recyclerView.setAdapter(new ThreadPhotoRailAdapter(getContext(), glideRequests, cursor, this.listener));
    }

    /* loaded from: classes4.dex */
    public static class ThreadPhotoRailAdapter extends CursorRecyclerViewAdapter<ThreadPhotoViewHolder> {
        private static final String TAG = Log.tag(ThreadPhotoRailAdapter.class);
        private OnItemClickedListener clickedListener;
        private final GlideRequests glideRequests;

        private ThreadPhotoRailAdapter(Context context, GlideRequests glideRequests, Cursor cursor, OnItemClickedListener onItemClickedListener) {
            super(context, cursor);
            this.glideRequests = glideRequests;
            this.clickedListener = onItemClickedListener;
        }

        public ThreadPhotoViewHolder onCreateItemViewHolder(ViewGroup viewGroup, int i) {
            return new ThreadPhotoViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recipient_preference_photo_rail_item, viewGroup, false));
        }

        public void onBindItemViewHolder(ThreadPhotoViewHolder threadPhotoViewHolder, Cursor cursor) {
            ThumbnailView thumbnailView = threadPhotoViewHolder.imageView;
            MediaDatabase.MediaRecord from = MediaDatabase.MediaRecord.from(getContext(), cursor);
            Slide slideForAttachment = MediaUtil.getSlideForAttachment(getContext(), from.getAttachment());
            if (slideForAttachment != null) {
                thumbnailView.setImageResource(this.glideRequests, slideForAttachment, false, false);
            }
            thumbnailView.setOnClickListener(new ThreadPhotoRailView$ThreadPhotoRailAdapter$$ExternalSyntheticLambda0(this, from));
        }

        public /* synthetic */ void lambda$onBindItemViewHolder$0(MediaDatabase.MediaRecord mediaRecord, View view) {
            OnItemClickedListener onItemClickedListener = this.clickedListener;
            if (onItemClickedListener != null) {
                onItemClickedListener.onItemClicked(mediaRecord);
            }
        }

        public void setListener(OnItemClickedListener onItemClickedListener) {
            this.clickedListener = onItemClickedListener;
        }

        /* loaded from: classes4.dex */
        public static class ThreadPhotoViewHolder extends RecyclerView.ViewHolder {
            ThumbnailView imageView;

            ThreadPhotoViewHolder(View view) {
                super(view);
                this.imageView = (ThumbnailView) view.findViewById(R.id.thumbnail);
            }
        }
    }
}
