package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class NotificationProfileCreatedFragmentArgs {
    private final HashMap arguments;

    private NotificationProfileCreatedFragmentArgs() {
        this.arguments = new HashMap();
    }

    private NotificationProfileCreatedFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static NotificationProfileCreatedFragmentArgs fromBundle(Bundle bundle) {
        NotificationProfileCreatedFragmentArgs notificationProfileCreatedFragmentArgs = new NotificationProfileCreatedFragmentArgs();
        bundle.setClassLoader(NotificationProfileCreatedFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("profileId")) {
            notificationProfileCreatedFragmentArgs.arguments.put("profileId", Long.valueOf(bundle.getLong("profileId")));
            return notificationProfileCreatedFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"profileId\" is missing and does not have an android:defaultValue");
    }

    public long getProfileId() {
        return ((Long) this.arguments.get("profileId")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("profileId")) {
            bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        NotificationProfileCreatedFragmentArgs notificationProfileCreatedFragmentArgs = (NotificationProfileCreatedFragmentArgs) obj;
        return this.arguments.containsKey("profileId") == notificationProfileCreatedFragmentArgs.arguments.containsKey("profileId") && getProfileId() == notificationProfileCreatedFragmentArgs.getProfileId();
    }

    public int hashCode() {
        return 31 + ((int) (getProfileId() ^ (getProfileId() >>> 32)));
    }

    public String toString() {
        return "NotificationProfileCreatedFragmentArgs{profileId=" + getProfileId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(NotificationProfileCreatedFragmentArgs notificationProfileCreatedFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(notificationProfileCreatedFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public NotificationProfileCreatedFragmentArgs build() {
            return new NotificationProfileCreatedFragmentArgs(this.arguments);
        }

        public Builder setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }
    }
}
