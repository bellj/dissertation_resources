package org.thoughtcrime.securesms.components.voice;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.ui.PlayerNotificationManager;
import java.util.Objects;
import net.zetetic.database.sqlcipher.SQLiteDatabase;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.conversation.colors.ChatColorsPalette;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.AvatarUtil;

/* loaded from: classes4.dex */
public class VoiceNoteNotificationManager {
    private static final short NOW_PLAYING_NOTIFICATION_ID;
    private final Context context;
    private final MediaControllerCompat controller;
    private final PlayerNotificationManager notificationManager;

    public VoiceNoteNotificationManager(Context context, MediaSessionCompat.Token token, PlayerNotificationManager.NotificationListener notificationListener) {
        this.context = context;
        this.controller = new MediaControllerCompat(context, token);
        PlayerNotificationManager build = new PlayerNotificationManager.Builder(context, 32221, NotificationChannels.VOICE_NOTES).setChannelNameResourceId(R.string.NotificationChannel_voice_notes).setMediaDescriptionAdapter(new DescriptionAdapter()).setNotificationListener(notificationListener).build();
        this.notificationManager = build;
        build.setMediaSessionToken(token);
        build.setSmallIcon(R.drawable.ic_notification);
        build.setColorized(true);
        build.setUseFastForwardAction(false);
        build.setUseRewindAction(false);
        build.setUseStopAction(true);
    }

    public void hideNotification() {
        this.notificationManager.setPlayer(null);
    }

    public void showNotification(Player player) {
        this.notificationManager.setPlayer(player);
    }

    /* loaded from: classes4.dex */
    public final class DescriptionAdapter implements PlayerNotificationManager.MediaDescriptionAdapter {
        private Bitmap cachedBitmap;
        private RecipientId cachedRecipientId;

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter
        public /* bridge */ /* synthetic */ CharSequence getCurrentSubText(Player player) {
            return PlayerNotificationManager.MediaDescriptionAdapter.CC.$default$getCurrentSubText(this, player);
        }

        private DescriptionAdapter() {
            VoiceNoteNotificationManager.this = r1;
        }

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter
        public String getCurrentContentTitle(Player player) {
            if (hasMetadata()) {
                return Objects.toString(VoiceNoteNotificationManager.this.controller.getMetadata().getDescription().getTitle(), null);
            }
            return null;
        }

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter
        public PendingIntent createCurrentContentIntent(Player player) {
            String string;
            if (!hasMetadata() || (string = VoiceNoteNotificationManager.this.controller.getMetadata().getString(VoiceNoteMediaItemFactory.EXTRA_THREAD_RECIPIENT_ID)) == null) {
                return null;
            }
            RecipientId from = RecipientId.from(string);
            int i = (int) VoiceNoteNotificationManager.this.controller.getMetadata().getLong(VoiceNoteMediaItemFactory.EXTRA_MESSAGE_POSITION);
            long j = VoiceNoteNotificationManager.this.controller.getMetadata().getLong(VoiceNoteMediaItemFactory.EXTRA_THREAD_ID);
            int i2 = (int) VoiceNoteNotificationManager.this.controller.getMetadata().getLong(VoiceNoteMediaItemFactory.EXTRA_COLOR);
            if (i2 == 0) {
                i2 = ChatColorsPalette.UNKNOWN_CONTACT.asSingleColor();
            }
            VoiceNoteNotificationManager.this.notificationManager.setColor(i2);
            Intent build = ConversationIntents.createBuilder(VoiceNoteNotificationManager.this.context, from, j).withStartingPosition(i).build();
            build.setFlags(SQLiteDatabase.CREATE_IF_NECESSARY);
            return PendingIntent.getActivity(VoiceNoteNotificationManager.this.context, 0, build, SQLiteDatabase.CREATE_IF_NECESSARY);
        }

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter
        public String getCurrentContentText(Player player) {
            if (hasMetadata()) {
                return Objects.toString(VoiceNoteNotificationManager.this.controller.getMetadata().getDescription().getSubtitle(), null);
            }
            return null;
        }

        @Override // com.google.android.exoplayer2.ui.PlayerNotificationManager.MediaDescriptionAdapter
        public Bitmap getCurrentLargeIcon(Player player, PlayerNotificationManager.BitmapCallback bitmapCallback) {
            Bitmap bitmap;
            if (!hasMetadata() || !SignalStore.settings().getMessageNotificationsPrivacy().isDisplayContact()) {
                this.cachedBitmap = null;
                this.cachedRecipientId = null;
                return null;
            }
            String string = VoiceNoteNotificationManager.this.controller.getMetadata().getString(VoiceNoteMediaItemFactory.EXTRA_AVATAR_RECIPIENT_ID);
            if (string == null) {
                return null;
            }
            RecipientId from = RecipientId.from(string);
            if (Objects.equals(from, this.cachedRecipientId) && (bitmap = this.cachedBitmap) != null) {
                return bitmap;
            }
            this.cachedRecipientId = from;
            SignalExecutors.BOUNDED.execute(new VoiceNoteNotificationManager$DescriptionAdapter$$ExternalSyntheticLambda0(this, bitmapCallback));
            return null;
        }

        public /* synthetic */ void lambda$getCurrentLargeIcon$0(PlayerNotificationManager.BitmapCallback bitmapCallback) {
            try {
                Bitmap bitmapForNotification = AvatarUtil.getBitmapForNotification(VoiceNoteNotificationManager.this.context, Recipient.resolved(this.cachedRecipientId));
                this.cachedBitmap = bitmapForNotification;
                bitmapCallback.onBitmap(bitmapForNotification);
            } catch (Exception unused) {
                this.cachedBitmap = null;
            }
        }

        private boolean hasMetadata() {
            return (VoiceNoteNotificationManager.this.controller.getMetadata() == null || VoiceNoteNotificationManager.this.controller.getMetadata().getDescription() == null) ? false : true;
        }
    }
}
