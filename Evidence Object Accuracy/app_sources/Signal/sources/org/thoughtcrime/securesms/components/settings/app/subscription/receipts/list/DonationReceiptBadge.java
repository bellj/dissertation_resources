package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;

/* compiled from: DonationReceiptBadge.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptBadge;", "", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "level", "", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;ILorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getLevel", "()I", "getType", "()Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptBadge {
    private final Badge badge;
    private final int level;
    private final DonationReceiptRecord.Type type;

    public static /* synthetic */ DonationReceiptBadge copy$default(DonationReceiptBadge donationReceiptBadge, DonationReceiptRecord.Type type, int i, Badge badge, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            type = donationReceiptBadge.type;
        }
        if ((i2 & 2) != 0) {
            i = donationReceiptBadge.level;
        }
        if ((i2 & 4) != 0) {
            badge = donationReceiptBadge.badge;
        }
        return donationReceiptBadge.copy(type, i, badge);
    }

    public final DonationReceiptRecord.Type component1() {
        return this.type;
    }

    public final int component2() {
        return this.level;
    }

    public final Badge component3() {
        return this.badge;
    }

    public final DonationReceiptBadge copy(DonationReceiptRecord.Type type, int i, Badge badge) {
        Intrinsics.checkNotNullParameter(type, "type");
        Intrinsics.checkNotNullParameter(badge, "badge");
        return new DonationReceiptBadge(type, i, badge);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DonationReceiptBadge)) {
            return false;
        }
        DonationReceiptBadge donationReceiptBadge = (DonationReceiptBadge) obj;
        return this.type == donationReceiptBadge.type && this.level == donationReceiptBadge.level && Intrinsics.areEqual(this.badge, donationReceiptBadge.badge);
    }

    public int hashCode() {
        return (((this.type.hashCode() * 31) + this.level) * 31) + this.badge.hashCode();
    }

    public String toString() {
        return "DonationReceiptBadge(type=" + this.type + ", level=" + this.level + ", badge=" + this.badge + ')';
    }

    public DonationReceiptBadge(DonationReceiptRecord.Type type, int i, Badge badge) {
        Intrinsics.checkNotNullParameter(type, "type");
        Intrinsics.checkNotNullParameter(badge, "badge");
        this.type = type;
        this.level = i;
        this.badge = badge;
    }

    public final DonationReceiptRecord.Type getType() {
        return this.type;
    }

    public final int getLevel() {
        return this.level;
    }

    public final Badge getBadge() {
        return this.badge;
    }
}
