package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.lifecycle.Observer;
import org.thoughtcrime.securesms.registration.util.RegistrationNumberInputController;
import org.thoughtcrime.securesms.registration.viewmodel.NumberViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda4 implements Observer {
    public final /* synthetic */ RegistrationNumberInputController f$0;

    public /* synthetic */ ChangeNumberEnterPhoneNumberFragment$$ExternalSyntheticLambda4(RegistrationNumberInputController registrationNumberInputController) {
        this.f$0 = registrationNumberInputController;
    }

    @Override // androidx.lifecycle.Observer
    public final void onChanged(Object obj) {
        this.f$0.updateNumber((NumberViewState) obj);
    }
}
