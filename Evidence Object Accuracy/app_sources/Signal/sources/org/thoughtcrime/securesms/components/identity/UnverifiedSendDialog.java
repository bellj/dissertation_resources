package org.thoughtcrime.securesms.components.identity;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import java.util.List;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.database.IdentityDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes4.dex */
public class UnverifiedSendDialog extends AlertDialog.Builder implements DialogInterface.OnClickListener {
    private final ResendListener resendListener;
    private final List<IdentityRecord> untrustedRecords;

    /* loaded from: classes4.dex */
    public interface ResendListener {
        void onResendMessage();
    }

    public UnverifiedSendDialog(Context context, String str, List<IdentityRecord> list, ResendListener resendListener) {
        super(context);
        this.untrustedRecords = list;
        this.resendListener = resendListener;
        setTitle(R.string.UnverifiedSendDialog_send_message);
        setIcon(R.drawable.ic_warning);
        setMessage(str);
        setPositiveButton(R.string.UnverifiedSendDialog_send, this);
        setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.components.identity.UnverifiedSendDialog$$ExternalSyntheticLambda0
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return UnverifiedSendDialog.$r8$lambda$gADidUwgsxd7uLlQZXuwuLfSxQ0(UnverifiedSendDialog.this);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.components.identity.UnverifiedSendDialog$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                UnverifiedSendDialog.$r8$lambda$vEDJllppA5QfVILFhD5tBaDepWI(UnverifiedSendDialog.this, obj);
            }
        });
    }

    public /* synthetic */ Object lambda$onClick$0() {
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            for (IdentityRecord identityRecord : this.untrustedRecords) {
                ApplicationDependencies.getProtocolStore().aci().identities().setVerified(identityRecord.getRecipientId(), identityRecord.getIdentityKey(), IdentityDatabase.VerifiedStatus.DEFAULT);
            }
            if (acquire == null) {
                return null;
            }
            acquire.close();
            return null;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public /* synthetic */ void lambda$onClick$1(Object obj) {
        this.resendListener.onResendMessage();
    }
}
