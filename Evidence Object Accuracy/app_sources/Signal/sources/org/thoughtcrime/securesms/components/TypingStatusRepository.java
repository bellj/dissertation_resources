package org.thoughtcrime.securesms.components;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.annimon.stream.Collectors;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class TypingStatusRepository {
    private static final long RECIPIENT_TYPING_TIMEOUT = TimeUnit.SECONDS.toMillis(15);
    private static final String TAG = Log.tag(TypingStatusRepository.class);
    private final Map<Long, MutableLiveData<TypingState>> notifiers = new HashMap();
    private final MutableLiveData<Set<Long>> threadsNotifier = new MutableLiveData<>();
    private final Map<Typist, Runnable> timers = new HashMap();
    private final Map<Long, Set<Typist>> typistMap = new HashMap();

    public synchronized void onTypingStarted(Context context, long j, Recipient recipient, int i) {
        if (!recipient.isSelf()) {
            Set<Typist> set = (Set) Util.getOrDefault(this.typistMap, Long.valueOf(j), new LinkedHashSet());
            Typist typist = new Typist(recipient, i, j);
            if (!set.contains(typist)) {
                set.add(typist);
                this.typistMap.put(Long.valueOf(j), set);
                notifyThread(j, set, false);
            }
            Runnable runnable = this.timers.get(typist);
            if (runnable != null) {
                ThreadUtil.cancelRunnableOnMain(runnable);
            }
            TypingStatusRepository$$ExternalSyntheticLambda1 typingStatusRepository$$ExternalSyntheticLambda1 = new Runnable(context, j, recipient, i) { // from class: org.thoughtcrime.securesms.components.TypingStatusRepository$$ExternalSyntheticLambda1
                public final /* synthetic */ Context f$1;
                public final /* synthetic */ long f$2;
                public final /* synthetic */ Recipient f$3;
                public final /* synthetic */ int f$4;

                {
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r5;
                    this.f$4 = r6;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    TypingStatusRepository.$r8$lambda$cBABFQTz1l7J8PgkuUWkxdXiSNI(TypingStatusRepository.this, this.f$1, this.f$2, this.f$3, this.f$4);
                }
            };
            ThreadUtil.runOnMainDelayed(typingStatusRepository$$ExternalSyntheticLambda1, RECIPIENT_TYPING_TIMEOUT);
            this.timers.put(typist, typingStatusRepository$$ExternalSyntheticLambda1);
        }
    }

    public /* synthetic */ void lambda$onTypingStarted$0(Context context, long j, Recipient recipient, int i) {
        onTypingStopped(context, j, recipient, i, false);
    }

    public synchronized void onTypingStopped(Context context, long j, Recipient recipient, int i, boolean z) {
        if (!recipient.isSelf()) {
            Set<Typist> set = (Set) Util.getOrDefault(this.typistMap, Long.valueOf(j), new LinkedHashSet());
            Typist typist = new Typist(recipient, i, j);
            if (set.contains(typist)) {
                set.remove(typist);
                notifyThread(j, set, z);
            }
            if (set.isEmpty()) {
                this.typistMap.remove(Long.valueOf(j));
            }
            Runnable runnable = this.timers.get(typist);
            if (runnable != null) {
                ThreadUtil.cancelRunnableOnMain(runnable);
                this.timers.remove(typist);
            }
        }
    }

    public synchronized LiveData<TypingState> getTypists(long j) {
        MutableLiveData<TypingState> mutableLiveData;
        mutableLiveData = (MutableLiveData) Util.getOrDefault(this.notifiers, Long.valueOf(j), new MutableLiveData());
        this.notifiers.put(Long.valueOf(j), mutableLiveData);
        return mutableLiveData;
    }

    public synchronized LiveData<Set<Long>> getTypingThreads() {
        return this.threadsNotifier;
    }

    public synchronized void clear() {
        TypingState typingState = new TypingState(Collections.emptyList(), false);
        for (MutableLiveData<TypingState> mutableLiveData : this.notifiers.values()) {
            mutableLiveData.postValue(typingState);
        }
        this.notifiers.clear();
        this.typistMap.clear();
        this.timers.clear();
        this.threadsNotifier.postValue(Collections.emptySet());
    }

    private void notifyThread(long j, Set<Typist> set, boolean z) {
        String str = TAG;
        Log.d(str, "notifyThread() threadId: " + j + "  typists: " + set.size() + "  isReplaced: " + z);
        MutableLiveData<TypingState> mutableLiveData = (MutableLiveData) Util.getOrDefault(this.notifiers, Long.valueOf(j), new MutableLiveData());
        this.notifiers.put(Long.valueOf(j), mutableLiveData);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (Typist typist : set) {
            linkedHashSet.add(typist.getAuthor());
        }
        mutableLiveData.postValue(new TypingState(new ArrayList(linkedHashSet), z));
        this.threadsNotifier.postValue((Set) Stream.of(this.typistMap.keySet()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.TypingStatusRepository$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return TypingStatusRepository.$r8$lambda$6YZlac6SiUb4NLttV8Dy7KM4GoI(TypingStatusRepository.this, (Long) obj);
            }
        }).collect(Collectors.toSet()));
    }

    public /* synthetic */ boolean lambda$notifyThread$1(Long l) {
        return !this.typistMap.get(l).isEmpty();
    }

    /* loaded from: classes4.dex */
    public static class TypingState {
        private final boolean replacedByIncomingMessage;
        private final List<Recipient> typists;

        public TypingState(List<Recipient> list, boolean z) {
            this.typists = list;
            this.replacedByIncomingMessage = z;
        }

        public List<Recipient> getTypists() {
            return this.typists;
        }

        public boolean isReplacedByIncomingMessage() {
            return this.replacedByIncomingMessage;
        }
    }

    /* loaded from: classes4.dex */
    public static class Typist {
        private final Recipient author;
        private final int device;
        private final long threadId;

        private Typist(Recipient recipient, int i, long j) {
            this.author = recipient;
            this.device = i;
            this.threadId = j;
        }

        public Recipient getAuthor() {
            return this.author;
        }

        public int getDevice() {
            return this.device;
        }

        public long getThreadId() {
            return this.threadId;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            Typist typist = (Typist) obj;
            if (this.device == typist.device && this.threadId == typist.threadId) {
                return this.author.equals(typist.author);
            }
            return false;
        }

        public int hashCode() {
            long j = this.threadId;
            return (((this.author.hashCode() * 31) + this.device) * 31) + ((int) (j ^ (j >>> 32)));
        }
    }
}
