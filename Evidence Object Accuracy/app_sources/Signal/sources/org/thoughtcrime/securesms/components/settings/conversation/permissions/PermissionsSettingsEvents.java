package org.thoughtcrime.securesms.components.settings.conversation.permissions;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;

/* compiled from: PermissionsSettingsEvents.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0001\u0003B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0001\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsEvents;", "", "()V", "GroupChangeError", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsEvents$GroupChangeError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class PermissionsSettingsEvents {
    public /* synthetic */ PermissionsSettingsEvents(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: PermissionsSettingsEvents.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsEvents$GroupChangeError;", "Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsEvents;", "reason", "Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "(Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;)V", "getReason", "()Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupChangeError extends PermissionsSettingsEvents {
        private final GroupChangeFailureReason reason;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupChangeError(GroupChangeFailureReason groupChangeFailureReason) {
            super(null);
            Intrinsics.checkNotNullParameter(groupChangeFailureReason, "reason");
            this.reason = groupChangeFailureReason;
        }

        public final GroupChangeFailureReason getReason() {
            return this.reason;
        }
    }

    private PermissionsSettingsEvents() {
    }
}
