package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.util.Consumer;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.textfield.TextInputLayout;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.BreakIteratorCompat;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiUtil;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileViewModel;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileNamePreset;
import org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: EditNotificationProfileFragment.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 $2\u00020\u00012\u00020\u0002:\u0001$B\u0005¢\u0006\u0002\u0010\u0003J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u0011H\u0016J\u0010\u0010\u001a\u001a\u00020\u00112\u0006\u0010\u0017\u001a\u00020\u0018H\u0016J\u001a\u0010\u001b\u001a\u00020\u00112\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u0018\u0010 \u001a\u00020\u00112\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0018H\u0002R\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\n\u001a\u00020\u000b8BX\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\r¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "Lorg/thoughtcrime/securesms/reactions/any/ReactWithAnyEmojiBottomSheetDialogFragment$Callback;", "()V", "emojiView", "Landroid/widget/ImageView;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "nameView", "Landroid/widget/EditText;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "createFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "onEmojiSelectedInternal", "emoji", "", "onReactWithAnyEmojiDialogDismissed", "onReactWithAnyEmojiSelected", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "presentCount", "countView", "Landroid/widget/TextView;", "profileName", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EditNotificationProfileFragment extends DSLSettingsFragment implements ReactWithAnyEmojiBottomSheetDialogFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private static final int NOTIFICATION_PROFILE_NAME_LIMIT_DISPLAY_THRESHOLD;
    private static final int NOTIFICATION_PROFILE_NAME_MAX_GLYPHS;
    private ImageView emojiView;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private EditText nameView;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(EditNotificationProfileViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return EditNotificationProfileFragment.access$createFactory((EditNotificationProfileFragment) this.receiver);
        }
    });

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiDialogDismissed() {
    }

    public EditNotificationProfileFragment() {
        super(0, 0, R.layout.fragment_edit_notification_profile, null, 11, null);
    }

    private final EditNotificationProfileViewModel getViewModel() {
        return (EditNotificationProfileViewModel) this.viewModel$delegate.getValue();
    }

    public final ViewModelProvider.Factory createFactory() {
        return new EditNotificationProfileViewModel.Factory(EditNotificationProfileFragmentArgs.fromBundle(requireArguments()).getProfileId());
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileFragment.$r8$lambda$YNyWcuulIZxilfAYFjFTvNNMKFc(EditNotificationProfileFragment.this, view2);
            }
        });
        View findViewById2 = view.findViewById(R.id.edit_notification_profile_title);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.e…tification_profile_title)");
        View findViewById3 = view.findViewById(R.id.edit_notification_profile_count);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.e…tification_profile_count)");
        View findViewById4 = view.findViewById(R.id.edit_notification_profile_save);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.e…otification_profile_save)");
        CircularProgressMaterialButton circularProgressMaterialButton = (CircularProgressMaterialButton) findViewById4;
        View findViewById5 = view.findViewById(R.id.edit_notification_profile_emoji);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.e…tification_profile_emoji)");
        ImageView imageView = (ImageView) findViewById5;
        View findViewById6 = view.findViewById(R.id.edit_notification_profile_name);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.e…otification_profile_name)");
        EditText editText = (EditText) findViewById6;
        View findViewById7 = view.findViewById(R.id.edit_notification_profile_name_wrapper);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.e…ion_profile_name_wrapper)");
        TextInputLayout textInputLayout = (TextInputLayout) findViewById7;
        EditTextUtil.addGraphemeClusterLimitFilter(editText, 32);
        editText.addTextChangedListener(new AfterTextChanged(new Consumer((TextView) findViewById3, textInputLayout) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ TextView f$1;
            public final /* synthetic */ TextInputLayout f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                EditNotificationProfileFragment.$r8$lambda$q848iB43uajzHP0sLFkFw_V_YW0(EditNotificationProfileFragment.this, this.f$1, this.f$2, (Editable) obj);
            }
        }));
        imageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileFragment.$r8$lambda$KZqChyrLBfgON_dIMPT6BrXs1nI(EditNotificationProfileFragment.this, view2);
            }
        });
        view.findViewById(R.id.edit_notification_profile_clear).setOnClickListener(new View.OnClickListener(editText, this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ EditText f$0;
            public final /* synthetic */ EditNotificationProfileFragment f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileFragment.m732$r8$lambda$q930zdLLHhZLt2M4eXMVpSTyXI(this.f$0, this.f$1, view2);
            }
        });
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        circularProgressMaterialButton.setOnClickListener(new View.OnClickListener(editText, textInputLayout, this, circularProgressMaterialButton) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ EditText f$0;
            public final /* synthetic */ TextInputLayout f$1;
            public final /* synthetic */ EditNotificationProfileFragment f$2;
            public final /* synthetic */ CircularProgressMaterialButton f$3;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileFragment.$r8$lambda$zdpkpWrxDBqnZAziDxYrrk8t750(this.f$0, this.f$1, this.f$2, this.f$3, view2);
            }
        });
        this.lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(getViewModel().getInitialState(), (Function1) null, new Function1<EditNotificationProfileViewModel.InitialState, Unit>(circularProgressMaterialButton, this, (TextView) findViewById2, editText) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$onViewCreated$6
            final /* synthetic */ EditText $nameView;
            final /* synthetic */ CircularProgressMaterialButton $saveButton;
            final /* synthetic */ TextView $title;
            final /* synthetic */ EditNotificationProfileFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$saveButton = r1;
                this.this$0 = r2;
                this.$title = r3;
                this.$nameView = r4;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(EditNotificationProfileViewModel.InitialState initialState) {
                invoke(initialState);
                return Unit.INSTANCE;
            }

            public final void invoke(EditNotificationProfileViewModel.InitialState initialState) {
                Intrinsics.checkNotNullParameter(initialState, "initial");
                if (initialState.getCreateMode()) {
                    this.$saveButton.setText(this.this$0.getString(R.string.EditNotificationProfileFragment__create));
                    this.$title.setText(R.string.EditNotificationProfileFragment__name_your_profile);
                } else {
                    this.$saveButton.setText(this.this$0.getString(R.string.EditNotificationProfileFragment__save));
                    this.$title.setText(R.string.EditNotificationProfileFragment__edit_this_profile);
                }
                this.$nameView.setText(initialState.getName());
                EditNotificationProfileFragment.access$onEmojiSelectedInternal(this.this$0, initialState.getEmoji());
                ViewUtil.focusAndMoveCursorToEndAndOpenKeyboard(this.$nameView);
            }
        }, 1, (Object) null));
        this.nameView = editText;
        this.emojiView = imageView;
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m733onViewCreated$lambda0(EditNotificationProfileFragment editNotificationProfileFragment, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileFragment, "this$0");
        ViewUtil.hideKeyboard(editNotificationProfileFragment.requireContext(), editNotificationProfileFragment.requireView());
        editNotificationProfileFragment.requireActivity().onBackPressed();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m734onViewCreated$lambda1(EditNotificationProfileFragment editNotificationProfileFragment, TextView textView, TextInputLayout textInputLayout, Editable editable) {
        Intrinsics.checkNotNullParameter(editNotificationProfileFragment, "this$0");
        Intrinsics.checkNotNullParameter(textView, "$countView");
        Intrinsics.checkNotNullParameter(textInputLayout, "$nameTextWrapper");
        Intrinsics.checkNotNullParameter(editable, "editable");
        editNotificationProfileFragment.presentCount(textView, editable.toString());
        textInputLayout.setError(null);
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m735onViewCreated$lambda2(EditNotificationProfileFragment editNotificationProfileFragment, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileFragment, "this$0");
        ReactWithAnyEmojiBottomSheetDialogFragment.createForAboutSelection().show(editNotificationProfileFragment.getChildFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m736onViewCreated$lambda3(EditText editText, EditNotificationProfileFragment editNotificationProfileFragment, View view) {
        Intrinsics.checkNotNullParameter(editText, "$nameView");
        Intrinsics.checkNotNullParameter(editNotificationProfileFragment, "this$0");
        editText.setText("");
        editNotificationProfileFragment.onEmojiSelectedInternal("");
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m737onViewCreated$lambda6(EditText editText, TextInputLayout textInputLayout, EditNotificationProfileFragment editNotificationProfileFragment, CircularProgressMaterialButton circularProgressMaterialButton, View view) {
        Intrinsics.checkNotNullParameter(editText, "$nameView");
        Intrinsics.checkNotNullParameter(textInputLayout, "$nameTextWrapper");
        Intrinsics.checkNotNullParameter(editNotificationProfileFragment, "this$0");
        Intrinsics.checkNotNullParameter(circularProgressMaterialButton, "$saveButton");
        if (TextUtils.isEmpty(editText.getText())) {
            textInputLayout.setError(editNotificationProfileFragment.getString(R.string.EditNotificationProfileFragment__profile_must_have_a_name));
            return;
        }
        LifecycleDisposable lifecycleDisposable = editNotificationProfileFragment.lifecycleDisposable;
        Single<EditNotificationProfileViewModel.SaveNotificationProfileResult> doAfterTerminate = editNotificationProfileFragment.getViewModel().save(editText.getText().toString()).doOnSubscribe(new io.reactivex.rxjava3.functions.Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                EditNotificationProfileFragment.$r8$lambda$zcLCaj4mgDN0VtJ9r1GuVhZIFKg(CircularProgressMaterialButton.this, (Disposable) obj);
            }
        }).doAfterTerminate(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                EditNotificationProfileFragment.$r8$lambda$y0hvD8TbQT6vynFlePgzRWAikLo(CircularProgressMaterialButton.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(doAfterTerminate, "viewModel.save(nameView.…Button.cancelSpinning() }");
        lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(doAfterTerminate, (Function1) null, new Function1<EditNotificationProfileViewModel.SaveNotificationProfileResult, Unit>(editNotificationProfileFragment, editText, textInputLayout) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$onViewCreated$5$3
            final /* synthetic */ TextInputLayout $nameTextWrapper;
            final /* synthetic */ EditText $nameView;
            final /* synthetic */ EditNotificationProfileFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$nameView = r2;
                this.$nameTextWrapper = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(EditNotificationProfileViewModel.SaveNotificationProfileResult saveNotificationProfileResult) {
                invoke(saveNotificationProfileResult);
                return Unit.INSTANCE;
            }

            public final void invoke(EditNotificationProfileViewModel.SaveNotificationProfileResult saveNotificationProfileResult) {
                if (saveNotificationProfileResult instanceof EditNotificationProfileViewModel.SaveNotificationProfileResult.Success) {
                    ViewUtil.hideKeyboard(this.this$0.requireContext(), this.$nameView);
                    EditNotificationProfileViewModel.SaveNotificationProfileResult.Success success = (EditNotificationProfileViewModel.SaveNotificationProfileResult.Success) saveNotificationProfileResult;
                    if (success.getCreateMode()) {
                        NavController findNavController = FragmentKt.findNavController(this.this$0);
                        EditNotificationProfileFragmentDirections.ActionEditNotificationProfileFragmentToAddAllowedMembersFragment actionEditNotificationProfileFragmentToAddAllowedMembersFragment = EditNotificationProfileFragmentDirections.actionEditNotificationProfileFragmentToAddAllowedMembersFragment(success.getProfile().getId());
                        Intrinsics.checkNotNullExpressionValue(actionEditNotificationProfileFragmentToAddAllowedMembersFragment, "actionEditNotificationPr…nt(saveResult.profile.id)");
                        SafeNavigation.safeNavigate(findNavController, actionEditNotificationProfileFragmentToAddAllowedMembersFragment);
                        return;
                    }
                    FragmentKt.findNavController(this.this$0).navigateUp();
                } else if (Intrinsics.areEqual(saveNotificationProfileResult, EditNotificationProfileViewModel.SaveNotificationProfileResult.DuplicateNameFailure.INSTANCE)) {
                    this.$nameTextWrapper.setError(this.this$0.getString(R.string.EditNotificationProfileFragment__a_profile_with_this_name_already_exists));
                }
            }
        }, 1, (Object) null));
    }

    /* renamed from: onViewCreated$lambda-6$lambda-4 */
    public static final void m738onViewCreated$lambda6$lambda4(CircularProgressMaterialButton circularProgressMaterialButton, Disposable disposable) {
        Intrinsics.checkNotNullParameter(circularProgressMaterialButton, "$saveButton");
        circularProgressMaterialButton.setSpinning();
    }

    /* renamed from: onViewCreated$lambda-6$lambda-5 */
    public static final void m739onViewCreated$lambda6$lambda5(CircularProgressMaterialButton circularProgressMaterialButton) {
        Intrinsics.checkNotNullParameter(circularProgressMaterialButton, "$saveButton");
        circularProgressMaterialButton.cancelSpinning();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        NotificationProfileNamePreset.INSTANCE.register(dSLSettingsAdapter);
        EditNotificationProfileFragment$bindAdapter$onClick$1 editNotificationProfileFragment$bindAdapter$onClick$1 = new Function1<NotificationProfileNamePreset.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileFragment$bindAdapter$onClick$1
            final /* synthetic */ EditNotificationProfileFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(NotificationProfileNamePreset.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(NotificationProfileNamePreset.Model model) {
                Intrinsics.checkNotNullParameter(model, "preset");
                EditText access$getNameView$p = EditNotificationProfileFragment.access$getNameView$p(this.this$0);
                if (access$getNameView$p != null) {
                    access$getNameView$p.setText(model.getBodyResource());
                    access$getNameView$p.setSelection(access$getNameView$p.length(), access$getNameView$p.length());
                }
                EditNotificationProfileFragment.access$onEmojiSelectedInternal(this.this$0, model.getEmoji());
            }
        };
        dSLSettingsAdapter.submitList(CollectionsKt__CollectionsKt.listOf((Object[]) new NotificationProfileNamePreset.Model[]{new NotificationProfileNamePreset.Model("💪", R.string.EditNotificationProfileFragment__work, editNotificationProfileFragment$bindAdapter$onClick$1), new NotificationProfileNamePreset.Model("😴", R.string.EditNotificationProfileFragment__sleep, editNotificationProfileFragment$bindAdapter$onClick$1), new NotificationProfileNamePreset.Model("🚗", R.string.EditNotificationProfileFragment__driving, editNotificationProfileFragment$bindAdapter$onClick$1), new NotificationProfileNamePreset.Model("😊", R.string.EditNotificationProfileFragment__downtime, editNotificationProfileFragment$bindAdapter$onClick$1), new NotificationProfileNamePreset.Model("💡", R.string.EditNotificationProfileFragment__focus, editNotificationProfileFragment$bindAdapter$onClick$1)}));
    }

    @Override // org.thoughtcrime.securesms.reactions.any.ReactWithAnyEmojiBottomSheetDialogFragment.Callback
    public void onReactWithAnyEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        onEmojiSelectedInternal(str);
    }

    private final void presentCount(TextView textView, String str) {
        BreakIteratorCompat instance = BreakIteratorCompat.getInstance();
        instance.setText(str);
        int countBreaks = instance.countBreaks();
        if (countBreaks >= 22) {
            textView.setVisibility(0);
            textView.setText(getResources().getString(R.string.EditNotificationProfileFragment__count, Integer.valueOf(countBreaks), 32));
            return;
        }
        textView.setVisibility(8);
    }

    public final void onEmojiSelectedInternal(String str) {
        Drawable convertToDrawable = EmojiUtil.convertToDrawable(requireContext(), str);
        if (convertToDrawable != null) {
            ImageView imageView = this.emojiView;
            if (imageView != null) {
                imageView.setImageDrawable(convertToDrawable);
            }
            getViewModel().onEmojiSelected(str);
            return;
        }
        ImageView imageView2 = this.emojiView;
        if (imageView2 != null) {
            imageView2.setImageResource(R.drawable.ic_add_emoji);
        }
        getViewModel().onEmojiSelected("");
    }

    /* compiled from: EditNotificationProfileFragment.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileFragment$Companion;", "", "()V", "NOTIFICATION_PROFILE_NAME_LIMIT_DISPLAY_THRESHOLD", "", "NOTIFICATION_PROFILE_NAME_MAX_GLYPHS", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
