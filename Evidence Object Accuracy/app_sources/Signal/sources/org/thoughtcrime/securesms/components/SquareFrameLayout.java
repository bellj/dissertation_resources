package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class SquareFrameLayout extends FrameLayout {
    private final boolean squareHeight;

    public SquareFrameLayout(Context context) {
        this(context, null);
    }

    public SquareFrameLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public SquareFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.SquareFrameLayout, 0, 0);
            this.squareHeight = obtainStyledAttributes.getBoolean(0, false);
            obtainStyledAttributes.recycle();
            return;
        }
        this.squareHeight = false;
    }

    @Override // android.widget.FrameLayout, android.view.View
    protected void onMeasure(int i, int i2) {
        if (this.squareHeight) {
            super.onMeasure(i2, i2);
        } else {
            super.onMeasure(i, i);
        }
    }
}
