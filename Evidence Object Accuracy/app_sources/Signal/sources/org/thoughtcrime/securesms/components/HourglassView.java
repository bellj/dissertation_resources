package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.View;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class HourglassView extends View {
    private final Paint backgroundPaint;
    private Bitmap empty;
    private final Paint foregroundPaint;
    private Bitmap full;
    private int offset;
    private float percentage;
    private final Paint progressPaint;

    public HourglassView(Context context) {
        this(context, null);
    }

    public HourglassView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public HourglassView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2 = 0;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.HourglassView, 0, 0);
            this.empty = BitmapFactory.decodeResource(getResources(), obtainStyledAttributes.getResourceId(0, 0));
            this.full = BitmapFactory.decodeResource(getResources(), obtainStyledAttributes.getResourceId(1, 0));
            this.percentage = (float) obtainStyledAttributes.getInt(3, 50);
            this.offset = obtainStyledAttributes.getInt(2, 0);
            i2 = obtainStyledAttributes.getColor(4, 0);
            obtainStyledAttributes.recycle();
        }
        Paint paint = new Paint();
        this.backgroundPaint = paint;
        Paint paint2 = new Paint();
        this.foregroundPaint = paint2;
        Paint paint3 = new Paint();
        this.progressPaint = paint3;
        paint.setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.MULTIPLY));
        paint2.setColorFilter(new PorterDuffColorFilter(i2, PorterDuff.Mode.MULTIPLY));
        paint3.setColor(getResources().getColor(R.color.black));
        paint3.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        setLayerType(1, null);
    }

    @Override // android.view.View
    public void onDraw(Canvas canvas) {
        float height = ((float) (this.full.getHeight() - (this.offset * 2))) * (this.percentage / 100.0f);
        canvas.drawBitmap(this.full, 0.0f, 0.0f, this.backgroundPaint);
        canvas.drawRect(0.0f, 0.0f, (float) this.full.getWidth(), ((float) this.offset) + height, this.progressPaint);
        canvas.drawBitmap(this.empty, 0.0f, 0.0f, this.foregroundPaint);
    }

    public void setPercentage(float f) {
        this.percentage = f;
        invalidate();
    }

    public void setTint(int i) {
        this.backgroundPaint.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.MULTIPLY));
        this.foregroundPaint.setColorFilter(new PorterDuffColorFilter(i, PorterDuff.Mode.MULTIPLY));
    }
}
