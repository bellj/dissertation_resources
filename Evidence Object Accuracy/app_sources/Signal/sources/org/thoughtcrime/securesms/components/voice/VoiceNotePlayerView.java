package org.thoughtcrime.securesms.components.voice;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.SimpleColorFilter;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.value.LottieValueCallback;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.PlaybackSpeedToggleTextView;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: VoiceNotePlayerView.kt */
@Metadata(d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0002*+B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\u001d\u001a\u00020\u001eH\u0002J\b\u0010\u001f\u001a\u00020\u001eH\u0002J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0002J\u0006\u0010$\u001a\u00020\u001eJ\u000e\u0010%\u001a\u00020\u001e2\u0006\u0010&\u001a\u00020\u000fJ\u0006\u0010'\u001a\u00020\u001eJ\u0010\u0010(\u001a\u00020\u001e2\u0006\u0010)\u001a\u00020\u0007H\u0002R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u000e¢\u0006\u0002\n\u0000R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015R\u000e\u0010\u0016\u001a\u00020\u0007X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0018X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX\u0004¢\u0006\u0002\n\u0000¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "closeButton", "Landroid/view/View;", "durationView", "Landroid/widget/TextView;", "infoView", "lastState", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView$State;", "listener", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView$Listener;", "getListener", "()Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView$Listener;", "setListener", "(Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView$Listener;)V", "lottieDirection", "playPauseToggleView", "Lcom/airbnb/lottie/LottieAnimationView;", "playerVisible", "", "speedView", "Lorg/thoughtcrime/securesms/components/PlaybackSpeedToggleTextView;", "animateToggleToPause", "", "animateToggleToPlay", "formatDuration", "", "duration", "", "hide", "setState", "state", "show", "startLottieAnimation", "direction", "Listener", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNotePlayerView extends ConstraintLayout {
    private final View closeButton;
    private final TextView durationView;
    private final TextView infoView;
    private State lastState;
    private Listener listener;
    private int lottieDirection;
    private final LottieAnimationView playPauseToggleView;
    private boolean playerVisible;
    private final PlaybackSpeedToggleTextView speedView;

    /* compiled from: VoiceNotePlayerView.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J0\u0010\u0006\u001a\u00020\u00032\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\bH&J\u0010\u0010\u000e\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&J \u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0010\u001a\u00020\b2\u0006\u0010\u0011\u001a\u00020\u0012H&J\u0018\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0014\u001a\u00020\u0015H&¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView$Listener;", "", "onCloseRequested", "", "uri", "Landroid/net/Uri;", "onNavigateToMessage", "threadId", "", "threadRecipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "senderId", "messageSentAt", "messagePositionInThread", "onPause", "onPlay", "messageId", "position", "", "onSpeedChangeRequested", "speed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Listener {
        void onCloseRequested(Uri uri);

        void onNavigateToMessage(long j, RecipientId recipientId, RecipientId recipientId2, long j2, long j3);

        void onPause(Uri uri);

        void onPlay(Uri uri, long j, double d);

        void onSpeedChangeRequested(Uri uri, float f);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public VoiceNotePlayerView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public VoiceNotePlayerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ VoiceNotePlayerView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceNotePlayerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        ViewGroup.inflate(context, R.layout.voice_note_player_view, this);
        setLayoutDirection(0);
        View findViewById = findViewById(R.id.voice_note_player_play_pause_toggle);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.voice_…player_play_pause_toggle)");
        LottieAnimationView lottieAnimationView = (LottieAnimationView) findViewById;
        this.playPauseToggleView = lottieAnimationView;
        View findViewById2 = findViewById(R.id.voice_note_player_info);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.voice_note_player_info)");
        TextView textView = (TextView) findViewById2;
        this.infoView = textView;
        View findViewById3 = findViewById(R.id.voice_note_player_duration);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "findViewById(R.id.voice_note_player_duration)");
        this.durationView = (TextView) findViewById3;
        View findViewById4 = findViewById(R.id.voice_note_player_speed);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "findViewById(R.id.voice_note_player_speed)");
        PlaybackSpeedToggleTextView playbackSpeedToggleTextView = (PlaybackSpeedToggleTextView) findViewById4;
        this.speedView = playbackSpeedToggleTextView;
        View findViewById5 = findViewById(R.id.voice_note_player_close);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "findViewById(R.id.voice_note_player_close)");
        this.closeButton = findViewById5;
        textView.setSelected(true);
        View findViewById6 = findViewById(R.id.voice_note_player_speed_touch_target);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "findViewById(R.id.voice_…layer_speed_touch_target)");
        findViewById6.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                VoiceNotePlayerView.m1269_init_$lambda0(VoiceNotePlayerView.this, view);
            }
        });
        playbackSpeedToggleTextView.setPlaybackSpeedListener(new PlaybackSpeedToggleTextView.PlaybackSpeedListener(this) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView.2
            final /* synthetic */ VoiceNotePlayerView this$0;

            {
                this.this$0 = r1;
            }

            @Override // org.thoughtcrime.securesms.components.PlaybackSpeedToggleTextView.PlaybackSpeedListener
            public void onPlaybackSpeedChanged(float f) {
                Listener listener;
                State state = this.this$0.lastState;
                if (state != null && (listener = this.this$0.getListener()) != null) {
                    listener.onSpeedChangeRequested(state.getUri(), f);
                }
            }
        });
        findViewById5.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                VoiceNotePlayerView.m1270_init_$lambda2(VoiceNotePlayerView.this, view);
            }
        });
        lottieAnimationView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                VoiceNotePlayerView.m1271_init_$lambda4(VoiceNotePlayerView.this, view);
            }
        });
        post(new Runnable(context) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView$$ExternalSyntheticLambda3
            public final /* synthetic */ Context f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                VoiceNotePlayerView.m1272_init_$lambda5(VoiceNotePlayerView.this, this.f$1);
            }
        });
        if (getBackground() != null) {
            getBackground().setColorFilter(new SimpleColorFilter(ContextCompat.getColor(context, R.color.voice_note_player_view_background)));
        }
        setContentDescription(context.getString(R.string.VoiceNotePlayerView__navigate_to_voice_message));
        setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                VoiceNotePlayerView.m1273_init_$lambda7(VoiceNotePlayerView.this, view);
            }
        });
    }

    public final Listener getListener() {
        return this.listener;
    }

    public final void setListener(Listener listener) {
        this.listener = listener;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1269_init_$lambda0(VoiceNotePlayerView voiceNotePlayerView, View view) {
        Intrinsics.checkNotNullParameter(voiceNotePlayerView, "this$0");
        voiceNotePlayerView.speedView.performClick();
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m1270_init_$lambda2(VoiceNotePlayerView voiceNotePlayerView, View view) {
        Listener listener;
        Intrinsics.checkNotNullParameter(voiceNotePlayerView, "this$0");
        State state = voiceNotePlayerView.lastState;
        if (state != null && (listener = voiceNotePlayerView.listener) != null) {
            listener.onCloseRequested(state.getUri());
        }
    }

    /* renamed from: _init_$lambda-4 */
    public static final void m1271_init_$lambda4(VoiceNotePlayerView voiceNotePlayerView, View view) {
        Intrinsics.checkNotNullParameter(voiceNotePlayerView, "this$0");
        State state = voiceNotePlayerView.lastState;
        if (state == null) {
            return;
        }
        if (!state.isPaused()) {
            Listener listener = voiceNotePlayerView.listener;
            if (listener != null) {
                listener.onPause(state.getUri());
            }
        } else if (state.getPlaybackPosition() >= state.getPlaybackDuration()) {
            Listener listener2 = voiceNotePlayerView.listener;
            if (listener2 != null) {
                listener2.onPlay(state.getUri(), state.getMessageId(), 0.0d);
            }
        } else {
            Listener listener3 = voiceNotePlayerView.listener;
            if (listener3 != null) {
                Uri uri = state.getUri();
                long messageId = state.getMessageId();
                double playbackPosition = (double) state.getPlaybackPosition();
                double playbackDuration = (double) state.getPlaybackDuration();
                Double.isNaN(playbackPosition);
                Double.isNaN(playbackDuration);
                listener3.onPlay(uri, messageId, playbackPosition / playbackDuration);
            }
        }
    }

    /* renamed from: _init_$lambda-5 */
    public static final void m1272_init_$lambda5(VoiceNotePlayerView voiceNotePlayerView, Context context) {
        Intrinsics.checkNotNullParameter(voiceNotePlayerView, "this$0");
        Intrinsics.checkNotNullParameter(context, "$context");
        voiceNotePlayerView.playPauseToggleView.addValueCallback(new KeyPath("**"), (KeyPath) LottieProperty.COLOR_FILTER, (LottieValueCallback<KeyPath>) new LottieValueCallback(new SimpleColorFilter(ContextCompat.getColor(context, R.color.signal_colorOnSurface))));
    }

    /* renamed from: _init_$lambda-7 */
    public static final void m1273_init_$lambda7(VoiceNotePlayerView voiceNotePlayerView, View view) {
        Listener listener;
        Intrinsics.checkNotNullParameter(voiceNotePlayerView, "this$0");
        State state = voiceNotePlayerView.lastState;
        if (state != null && (listener = voiceNotePlayerView.listener) != null) {
            listener.onNavigateToMessage(state.getThreadId(), state.getThreadRecipientId(), state.getSenderId(), state.getMessageTimestamp(), state.getMessagePositionInThread());
        }
    }

    public final void setState(State state) {
        Intrinsics.checkNotNullParameter(state, "state");
        this.lastState = state;
        if (state.isPaused()) {
            animateToggleToPlay();
        } else {
            animateToggleToPause();
        }
        if (!Intrinsics.areEqual(this.infoView.getText(), state.getName())) {
            this.infoView.setText(state.getName());
        }
        this.durationView.setText(getContext().getString(R.string.VoiceNotePlayerView__dot_s, formatDuration(state.getPlaybackDuration() - state.getPlaybackPosition())));
        this.speedView.setCurrentSpeed(state.getPlaybackSpeed());
    }

    public final void show() {
        if (!this.playerVisible) {
            setVisibility(0);
            Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_from_top);
            loadAnimation.setDuration(150);
            startAnimation(loadAnimation);
        }
        this.playerVisible = true;
    }

    public final void hide() {
        if (this.playerVisible) {
            Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.slide_to_top);
            loadAnimation.setDuration(150);
            loadAnimation.setAnimationListener(new Animation.AnimationListener(this) { // from class: org.thoughtcrime.securesms.components.voice.VoiceNotePlayerView$hide$1
                final /* synthetic */ VoiceNotePlayerView this$0;

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationRepeat(Animation animation) {
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationStart(Animation animation) {
                }

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // android.view.animation.Animation.AnimationListener
                public void onAnimationEnd(Animation animation) {
                    this.this$0.setVisibility(8);
                }
            });
            startAnimation(loadAnimation);
        }
        this.playerVisible = false;
    }

    private final String formatDuration(long j) {
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j);
        long j2 = (long) 60;
        String string = getResources().getString(R.string.AudioView_duration, Long.valueOf(seconds / j2), Long.valueOf(seconds % j2));
        Intrinsics.checkNotNullExpressionValue(string, "resources.getString(R.st…on, secs / 60, secs % 60)");
        return string;
    }

    private final void animateToggleToPlay() {
        startLottieAnimation(-1);
    }

    private final void animateToggleToPause() {
        startLottieAnimation(1);
    }

    private final void startLottieAnimation(int i) {
        String str;
        if (this.lottieDirection != i) {
            this.lottieDirection = i;
            LottieAnimationView lottieAnimationView = this.playPauseToggleView;
            if (i == -1) {
                str = getContext().getString(R.string.VoiceNotePlayerView__play_voice_message);
            } else {
                str = getContext().getString(R.string.VoiceNotePlayerView__pause_voice_message);
            }
            lottieAnimationView.setContentDescription(str);
            this.playPauseToggleView.pauseAnimation();
            this.playPauseToggleView.setSpeed((float) (i * 2));
            this.playPauseToggleView.resumeAnimation();
        }
    }

    /* compiled from: VoiceNotePlayerView.kt */
    @Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\"\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001Be\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\n\u0012\u0006\u0010\f\u001a\u00020\u0005\u0012\u0006\u0010\r\u001a\u00020\u0005\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0005\u0012\u0006\u0010\u0011\u001a\u00020\u0005\u0012\u0006\u0010\u0012\u001a\u00020\u0013¢\u0006\u0002\u0010\u0014J\t\u0010&\u001a\u00020\u0003HÆ\u0003J\t\u0010'\u001a\u00020\u0005HÆ\u0003J\t\u0010(\u001a\u00020\u0005HÆ\u0003J\t\u0010)\u001a\u00020\u0013HÆ\u0003J\t\u0010*\u001a\u00020\u0005HÆ\u0003J\t\u0010+\u001a\u00020\u0005HÆ\u0003J\t\u0010,\u001a\u00020\bHÆ\u0003J\t\u0010-\u001a\u00020\nHÆ\u0003J\t\u0010.\u001a\u00020\nHÆ\u0003J\t\u0010/\u001a\u00020\u0005HÆ\u0003J\t\u00100\u001a\u00020\u0005HÆ\u0003J\t\u00101\u001a\u00020\u000fHÆ\u0003J\u0001\u00102\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\n2\b\b\u0002\u0010\f\u001a\u00020\u00052\b\b\u0002\u0010\r\u001a\u00020\u00052\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00052\b\b\u0002\u0010\u0011\u001a\u00020\u00052\b\b\u0002\u0010\u0012\u001a\u00020\u0013HÆ\u0001J\u0013\u00103\u001a\u00020\b2\b\u00104\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u00105\u001a\u000206HÖ\u0001J\t\u00107\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0015R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\f\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017R\u0011\u0010\r\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0017R\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0011\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0017R\u0011\u0010\u0010\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0017R\u0011\u0010\u0012\u001a\u00020\u0013¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0017R\u0011\u0010\u000b\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b#\u0010!R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%¨\u00068"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayerView$State;", "", "uri", "Landroid/net/Uri;", "messageId", "", "threadId", "isPaused", "", "senderId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "threadRecipientId", "messagePositionInThread", "messageTimestamp", "name", "", "playbackPosition", "playbackDuration", "playbackSpeed", "", "(Landroid/net/Uri;JJZLorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;JJLjava/lang/String;JJF)V", "()Z", "getMessageId", "()J", "getMessagePositionInThread", "getMessageTimestamp", "getName", "()Ljava/lang/String;", "getPlaybackDuration", "getPlaybackPosition", "getPlaybackSpeed", "()F", "getSenderId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getThreadId", "getThreadRecipientId", "getUri", "()Landroid/net/Uri;", "component1", "component10", "component11", "component12", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class State {
        private final boolean isPaused;
        private final long messageId;
        private final long messagePositionInThread;
        private final long messageTimestamp;
        private final String name;
        private final long playbackDuration;
        private final long playbackPosition;
        private final float playbackSpeed;
        private final RecipientId senderId;
        private final long threadId;
        private final RecipientId threadRecipientId;
        private final Uri uri;

        public final Uri component1() {
            return this.uri;
        }

        public final long component10() {
            return this.playbackPosition;
        }

        public final long component11() {
            return this.playbackDuration;
        }

        public final float component12() {
            return this.playbackSpeed;
        }

        public final long component2() {
            return this.messageId;
        }

        public final long component3() {
            return this.threadId;
        }

        public final boolean component4() {
            return this.isPaused;
        }

        public final RecipientId component5() {
            return this.senderId;
        }

        public final RecipientId component6() {
            return this.threadRecipientId;
        }

        public final long component7() {
            return this.messagePositionInThread;
        }

        public final long component8() {
            return this.messageTimestamp;
        }

        public final String component9() {
            return this.name;
        }

        public final State copy(Uri uri, long j, long j2, boolean z, RecipientId recipientId, RecipientId recipientId2, long j3, long j4, String str, long j5, long j6, float f) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(recipientId, "senderId");
            Intrinsics.checkNotNullParameter(recipientId2, "threadRecipientId");
            Intrinsics.checkNotNullParameter(str, "name");
            return new State(uri, j, j2, z, recipientId, recipientId2, j3, j4, str, j5, j6, f);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return Intrinsics.areEqual(this.uri, state.uri) && this.messageId == state.messageId && this.threadId == state.threadId && this.isPaused == state.isPaused && Intrinsics.areEqual(this.senderId, state.senderId) && Intrinsics.areEqual(this.threadRecipientId, state.threadRecipientId) && this.messagePositionInThread == state.messagePositionInThread && this.messageTimestamp == state.messageTimestamp && Intrinsics.areEqual(this.name, state.name) && this.playbackPosition == state.playbackPosition && this.playbackDuration == state.playbackDuration && Intrinsics.areEqual(Float.valueOf(this.playbackSpeed), Float.valueOf(state.playbackSpeed));
        }

        public int hashCode() {
            int hashCode = ((((this.uri.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messageId)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId)) * 31;
            boolean z = this.isPaused;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return ((((((((((((((((hashCode + i) * 31) + this.senderId.hashCode()) * 31) + this.threadRecipientId.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messagePositionInThread)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messageTimestamp)) * 31) + this.name.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.playbackPosition)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.playbackDuration)) * 31) + Float.floatToIntBits(this.playbackSpeed);
        }

        public String toString() {
            return "State(uri=" + this.uri + ", messageId=" + this.messageId + ", threadId=" + this.threadId + ", isPaused=" + this.isPaused + ", senderId=" + this.senderId + ", threadRecipientId=" + this.threadRecipientId + ", messagePositionInThread=" + this.messagePositionInThread + ", messageTimestamp=" + this.messageTimestamp + ", name=" + this.name + ", playbackPosition=" + this.playbackPosition + ", playbackDuration=" + this.playbackDuration + ", playbackSpeed=" + this.playbackSpeed + ')';
        }

        public State(Uri uri, long j, long j2, boolean z, RecipientId recipientId, RecipientId recipientId2, long j3, long j4, String str, long j5, long j6, float f) {
            Intrinsics.checkNotNullParameter(uri, "uri");
            Intrinsics.checkNotNullParameter(recipientId, "senderId");
            Intrinsics.checkNotNullParameter(recipientId2, "threadRecipientId");
            Intrinsics.checkNotNullParameter(str, "name");
            this.uri = uri;
            this.messageId = j;
            this.threadId = j2;
            this.isPaused = z;
            this.senderId = recipientId;
            this.threadRecipientId = recipientId2;
            this.messagePositionInThread = j3;
            this.messageTimestamp = j4;
            this.name = str;
            this.playbackPosition = j5;
            this.playbackDuration = j6;
            this.playbackSpeed = f;
        }

        public final Uri getUri() {
            return this.uri;
        }

        public final long getMessageId() {
            return this.messageId;
        }

        public final long getThreadId() {
            return this.threadId;
        }

        public final boolean isPaused() {
            return this.isPaused;
        }

        public final RecipientId getSenderId() {
            return this.senderId;
        }

        public final RecipientId getThreadRecipientId() {
            return this.threadRecipientId;
        }

        public final long getMessagePositionInThread() {
            return this.messagePositionInThread;
        }

        public final long getMessageTimestamp() {
            return this.messageTimestamp;
        }

        public final String getName() {
            return this.name;
        }

        public final long getPlaybackPosition() {
            return this.playbackPosition;
        }

        public final long getPlaybackDuration() {
            return this.playbackDuration;
        }

        public final float getPlaybackSpeed() {
            return this.playbackSpeed;
        }
    }
}
