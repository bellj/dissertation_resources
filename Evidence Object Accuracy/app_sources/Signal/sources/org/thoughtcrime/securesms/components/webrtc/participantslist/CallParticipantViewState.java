package org.thoughtcrime.securesms.components.webrtc.participantslist;

import android.content.Context;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel;

/* loaded from: classes4.dex */
public final class CallParticipantViewState extends RecipientMappingModel<CallParticipantViewState> {
    private final CallParticipant callParticipant;

    public CallParticipantViewState(CallParticipant callParticipant) {
        this.callParticipant = callParticipant;
    }

    @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel
    public Recipient getRecipient() {
        return this.callParticipant.getRecipient();
    }

    @Override // org.thoughtcrime.securesms.util.viewholders.RecipientMappingModel
    public String getName(Context context) {
        return this.callParticipant.getRecipientDisplayName(context);
    }

    public int getVideoMutedVisibility() {
        return this.callParticipant.isVideoEnabled() ? 8 : 0;
    }

    public int getAudioMutedVisibility() {
        return this.callParticipant.isMicrophoneEnabled() ? 8 : 0;
    }

    public int getScreenSharingVisibility() {
        return this.callParticipant.isScreenSharing() ? 0 : 8;
    }

    public boolean areItemsTheSame(CallParticipantViewState callParticipantViewState) {
        return this.callParticipant.getCallParticipantId().equals(callParticipantViewState.callParticipant.getCallParticipantId());
    }

    public boolean areContentsTheSame(CallParticipantViewState callParticipantViewState) {
        return super.areContentsTheSame(callParticipantViewState) && this.callParticipant.isVideoEnabled() == callParticipantViewState.callParticipant.isVideoEnabled() && this.callParticipant.isMicrophoneEnabled() == callParticipantViewState.callParticipant.isMicrophoneEnabled();
    }
}
