package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState;

/* compiled from: ManageDonationsViewModel.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "transactionState", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ManageDonationsViewModel$refresh$4 extends Lambda implements Function1<ManageDonationsState.TransactionState, Unit> {
    final /* synthetic */ ManageDonationsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ManageDonationsViewModel$refresh$4(ManageDonationsViewModel manageDonationsViewModel) {
        super(1);
        this.this$0 = manageDonationsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(ManageDonationsState.TransactionState transactionState) {
        invoke(transactionState);
        return Unit.INSTANCE;
    }

    public final void invoke(ManageDonationsState.TransactionState transactionState) {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$refresh$4$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel$refresh$4.m979invoke$lambda0(ManageDonationsState.TransactionState.this, (ManageDonationsState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final ManageDonationsState m979invoke$lambda0(ManageDonationsState.TransactionState transactionState, ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "it");
        Intrinsics.checkNotNullExpressionValue(transactionState, "transactionState");
        return ManageDonationsState.copy$default(manageDonationsState, null, transactionState, null, null, 13, null);
    }
}
