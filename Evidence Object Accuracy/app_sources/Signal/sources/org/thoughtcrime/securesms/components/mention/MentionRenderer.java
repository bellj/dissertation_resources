package org.thoughtcrime.securesms.components.mention;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import org.thoughtcrime.securesms.util.LayoutUtil;

/* loaded from: classes4.dex */
public abstract class MentionRenderer {
    protected final int horizontalPadding;
    protected final int verticalPadding;

    public abstract void draw(Canvas canvas, Layout layout, int i, int i2, int i3, int i4);

    public MentionRenderer(int i, int i2) {
        this.horizontalPadding = i;
        this.verticalPadding = i2;
    }

    protected int getLineTop(Layout layout, int i) {
        return LayoutUtil.getLineTopWithoutPadding(layout, i) - this.verticalPadding;
    }

    protected int getLineBottom(Layout layout, int i) {
        return LayoutUtil.getLineBottomWithoutPadding(layout, i) + this.verticalPadding;
    }

    /* loaded from: classes4.dex */
    public static final class SingleLineMentionRenderer extends MentionRenderer {
        private final Drawable drawable;

        public SingleLineMentionRenderer(int i, int i2, Drawable drawable) {
            super(i, i2);
            this.drawable = drawable;
        }

        @Override // org.thoughtcrime.securesms.components.mention.MentionRenderer
        public void draw(Canvas canvas, Layout layout, int i, int i2, int i3, int i4) {
            int lineTop = getLineTop(layout, i);
            int lineBottom = getLineBottom(layout, i);
            this.drawable.setBounds(Math.min(i3, i4), lineTop, Math.max(i3, i4), lineBottom);
            this.drawable.draw(canvas);
        }
    }

    /* loaded from: classes4.dex */
    public static final class MultiLineMentionRenderer extends MentionRenderer {
        private final Drawable drawableLeft;
        private final Drawable drawableMid;
        private final Drawable drawableRight;

        public MultiLineMentionRenderer(int i, int i2, Drawable drawable, Drawable drawable2, Drawable drawable3) {
            super(i, i2);
            this.drawableLeft = drawable;
            this.drawableMid = drawable2;
            this.drawableRight = drawable3;
        }

        @Override // org.thoughtcrime.securesms.components.mention.MentionRenderer
        public void draw(Canvas canvas, Layout layout, int i, int i2, int i3, int i4) {
            float f;
            float f2;
            int paragraphDirection = layout.getParagraphDirection(i);
            if (paragraphDirection == -1) {
                f = layout.getLineLeft(i) - ((float) this.horizontalPadding);
            } else {
                f = layout.getLineRight(i) + ((float) this.horizontalPadding);
            }
            drawStart(canvas, i3, getLineTop(layout, i), (int) f, getLineBottom(layout, i));
            for (int i5 = i + 1; i5 < i2; i5++) {
                int lineLeft = ((int) layout.getLineLeft(i5)) - this.horizontalPadding;
                int lineRight = ((int) layout.getLineRight(i5)) + this.horizontalPadding;
                this.drawableMid.setBounds(lineLeft, getLineTop(layout, i5), lineRight, getLineBottom(layout, i5));
                this.drawableMid.draw(canvas);
            }
            if (paragraphDirection == -1) {
                f2 = layout.getLineRight(i) + ((float) this.horizontalPadding);
            } else {
                f2 = layout.getLineLeft(i) - ((float) this.horizontalPadding);
            }
            int i6 = (int) f2;
            drawEnd(canvas, i6, getLineTop(layout, i2), i4, getLineBottom(layout, i2));
        }

        private void drawStart(Canvas canvas, int i, int i2, int i3, int i4) {
            if (i > i3) {
                this.drawableRight.setBounds(i3, i2, i, i4);
                this.drawableRight.draw(canvas);
                return;
            }
            this.drawableLeft.setBounds(i, i2, i3, i4);
            this.drawableLeft.draw(canvas);
        }

        private void drawEnd(Canvas canvas, int i, int i2, int i3, int i4) {
            if (i > i3) {
                this.drawableLeft.setBounds(i3, i2, i, i4);
                this.drawableLeft.draw(canvas);
                return;
            }
            this.drawableRight.setBounds(i, i2, i3, i4);
            this.drawableRight.draw(canvas);
        }
    }
}
