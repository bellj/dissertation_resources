package org.thoughtcrime.securesms.components.settings.app.subscription.currency;

import java.util.Currency;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: SetCurrencyState.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B\u001f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0003HÖ\u0001R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/currency/SetCurrencyState;", "", "selectedCurrencyCode", "", "currencies", "", "Ljava/util/Currency;", "(Ljava/lang/String;Ljava/util/List;)V", "getCurrencies", "()Ljava/util/List;", "getSelectedCurrencyCode", "()Ljava/lang/String;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SetCurrencyState {
    private final List<Currency> currencies;
    private final String selectedCurrencyCode;

    public SetCurrencyState() {
        this(null, null, 3, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.subscription.currency.SetCurrencyState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SetCurrencyState copy$default(SetCurrencyState setCurrencyState, String str, List list, int i, Object obj) {
        if ((i & 1) != 0) {
            str = setCurrencyState.selectedCurrencyCode;
        }
        if ((i & 2) != 0) {
            list = setCurrencyState.currencies;
        }
        return setCurrencyState.copy(str, list);
    }

    public final String component1() {
        return this.selectedCurrencyCode;
    }

    public final List<Currency> component2() {
        return this.currencies;
    }

    public final SetCurrencyState copy(String str, List<Currency> list) {
        Intrinsics.checkNotNullParameter(str, "selectedCurrencyCode");
        Intrinsics.checkNotNullParameter(list, "currencies");
        return new SetCurrencyState(str, list);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SetCurrencyState)) {
            return false;
        }
        SetCurrencyState setCurrencyState = (SetCurrencyState) obj;
        return Intrinsics.areEqual(this.selectedCurrencyCode, setCurrencyState.selectedCurrencyCode) && Intrinsics.areEqual(this.currencies, setCurrencyState.currencies);
    }

    public int hashCode() {
        return (this.selectedCurrencyCode.hashCode() * 31) + this.currencies.hashCode();
    }

    public String toString() {
        return "SetCurrencyState(selectedCurrencyCode=" + this.selectedCurrencyCode + ", currencies=" + this.currencies + ')';
    }

    public SetCurrencyState(String str, List<Currency> list) {
        Intrinsics.checkNotNullParameter(str, "selectedCurrencyCode");
        Intrinsics.checkNotNullParameter(list, "currencies");
        this.selectedCurrencyCode = str;
        this.currencies = list;
    }

    public final String getSelectedCurrencyCode() {
        return this.selectedCurrencyCode;
    }

    public /* synthetic */ SetCurrencyState(String str, List list, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? "" : str, (i & 2) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list);
    }

    public final List<Currency> getCurrencies() {
        return this.currencies;
    }
}
