package org.thoughtcrime.securesms.components.mention;

import android.text.Annotation;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import org.thoughtcrime.securesms.database.MentionUtil;

/* loaded from: classes4.dex */
public class MentionDeleter implements TextWatcher {
    private Annotation toDelete;

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (i2 > 0 && (charSequence instanceof Spanned)) {
            Spanned spanned = (Spanned) charSequence;
            for (Annotation annotation : MentionAnnotation.getMentionAnnotations(spanned, i, i2 + i)) {
                if (spanned.getSpanStart(annotation) < i && spanned.getSpanEnd(annotation) > i) {
                    this.toDelete = annotation;
                    return;
                }
            }
        }
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        Annotation annotation = this.toDelete;
        if (annotation != null) {
            int spanStart = editable.getSpanStart(annotation);
            int spanEnd = editable.getSpanEnd(this.toDelete);
            editable.removeSpan(this.toDelete);
            this.toDelete = null;
            editable.replace(spanStart, spanEnd, String.valueOf((char) MentionUtil.MENTION_STARTER));
        }
    }
}
