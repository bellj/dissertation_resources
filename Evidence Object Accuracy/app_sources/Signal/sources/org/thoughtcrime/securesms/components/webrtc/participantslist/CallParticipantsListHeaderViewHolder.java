package org.thoughtcrime.securesms.components.webrtc.participantslist;

import android.view.View;
import android.widget.TextView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class CallParticipantsListHeaderViewHolder extends MappingViewHolder<CallParticipantsListHeader> {
    private final TextView headerText = ((TextView) findViewById(R.id.call_participants_list_header));

    public CallParticipantsListHeaderViewHolder(View view) {
        super(view);
    }

    public void bind(CallParticipantsListHeader callParticipantsListHeader) {
        this.headerText.setText(callParticipantsListHeader.getHeader(getContext()));
    }
}
