package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.Interpolator;
import android.view.animation.OvershootInterpolator;
import androidx.appcompat.widget.AppCompatSeekBar;
import java.util.Arrays;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class WaveFormSeekBarView extends AppCompatSeekBar {
    private static final int ANIM_BAR_OFF_SET_DURATION;
    private static final int ANIM_DURATION;
    private int barWidth;
    private float[] data;
    private long dataSetTime;
    private final Interpolator overshoot;
    private final Paint paint;
    private int playedBarColor;
    private Drawable progressDrawable;
    private int unplayedBarColor;
    private boolean waveMode;

    public WaveFormSeekBarView(Context context) {
        super(context);
        this.overshoot = new OvershootInterpolator();
        this.paint = new Paint();
        this.data = new float[0];
        this.playedBarColor = -1;
        this.unplayedBarColor = Integer.MAX_VALUE;
        init();
    }

    public WaveFormSeekBarView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.overshoot = new OvershootInterpolator();
        this.paint = new Paint();
        this.data = new float[0];
        this.playedBarColor = -1;
        this.unplayedBarColor = Integer.MAX_VALUE;
        init();
    }

    public WaveFormSeekBarView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.overshoot = new OvershootInterpolator();
        this.paint = new Paint();
        this.data = new float[0];
        this.playedBarColor = -1;
        this.unplayedBarColor = Integer.MAX_VALUE;
        init();
    }

    private void init() {
        setWillNotDraw(false);
        this.paint.setStrokeCap(Paint.Cap.ROUND);
        this.paint.setAntiAlias(true);
        this.progressDrawable = super.getProgressDrawable();
        if (isInEditMode()) {
            setWaveData(sinusoidalExampleData());
            this.dataSetTime = 0;
        }
        this.barWidth = getResources().getDimensionPixelSize(R.dimen.wave_form_bar_width);
    }

    public void setColors(int i, int i2, int i3) {
        this.playedBarColor = i;
        this.unplayedBarColor = i2;
        getThumb().setColorFilter(i3, PorterDuff.Mode.SRC_IN);
        invalidate();
    }

    @Override // android.widget.ProgressBar
    public void setProgressDrawable(Drawable drawable) {
        this.progressDrawable = drawable;
        if (!this.waveMode) {
            super.setProgressDrawable(drawable);
        }
    }

    @Override // android.widget.ProgressBar
    public Drawable getProgressDrawable() {
        return this.progressDrawable;
    }

    public void setWaveData(float[] fArr) {
        if (!Arrays.equals(fArr, this.data)) {
            this.data = fArr;
            this.dataSetTime = System.currentTimeMillis();
        }
        setWaveMode(fArr.length > 0);
    }

    public void setWaveMode(boolean z) {
        Drawable drawable;
        this.waveMode = z;
        if (z) {
            drawable = null;
        } else {
            drawable = this.progressDrawable;
        }
        super.setProgressDrawable(drawable);
        invalidate();
    }

    @Override // androidx.appcompat.widget.AppCompatSeekBar, android.widget.AbsSeekBar, android.widget.ProgressBar, android.view.View
    public void onDraw(Canvas canvas) {
        if (this.waveMode) {
            drawWave(canvas);
        }
        super.onDraw(canvas);
    }

    private void drawWave(Canvas canvas) {
        this.paint.setStrokeWidth((float) this.barWidth);
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        float f = ((float) height) / 2.0f;
        int i = this.barWidth;
        float f2 = f - ((float) i);
        float[] fArr = this.data;
        float length = ((float) (width - (fArr.length * i))) / ((float) (fArr.length - 1));
        canvas.save();
        canvas.translate((float) getPaddingLeft(), (float) getPaddingTop());
        if (getLayoutDirection() == 1) {
            canvas.scale(-1.0f, 1.0f, ((float) width) / 2.0f, f);
        }
        int i2 = 0;
        boolean z = false;
        while (true) {
            float[] fArr2 = this.data;
            if (i2 >= fArr2.length) {
                break;
            }
            int i3 = this.barWidth;
            float f3 = (((float) i2) * (((float) i3) + length)) + (((float) i3) / 2.0f);
            float f4 = fArr2[i2] * f2;
            this.paint.setColor((f3 / ((float) width)) * ((float) getMax()) < ((float) getProgress()) ? this.playedBarColor : this.unplayedBarColor);
            long currentTimeMillis = (System.currentTimeMillis() - ((long) (i2 * 12))) - this.dataSetTime;
            float interpolation = f4 * this.overshoot.getInterpolation(Math.max(0.0f, Math.min(1.0f, ((float) currentTimeMillis) / 450.0f)));
            canvas.drawLine(f3, f - interpolation, f3, f + interpolation, this.paint);
            if (currentTimeMillis < 450) {
                z = true;
            }
            i2++;
        }
        canvas.restore();
        if (z) {
            invalidate();
        }
    }

    private static float[] sinusoidalExampleData() {
        float[] fArr = new float[21];
        for (int i = 0; i < 21; i++) {
            double d = (double) ((((float) i) / ((float) 20)) * 2.0f);
            Double.isNaN(d);
            fArr[i] = (float) Math.sin(d * 3.141592653589793d);
        }
        return fArr;
    }
}
