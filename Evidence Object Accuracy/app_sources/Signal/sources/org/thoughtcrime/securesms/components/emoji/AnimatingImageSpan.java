package org.thoughtcrime.securesms.components.emoji;

import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;

/* loaded from: classes4.dex */
public class AnimatingImageSpan extends ImageSpan {
    public AnimatingImageSpan(Drawable drawable, Drawable.Callback callback) {
        super(drawable, 0);
        drawable.setCallback(callback);
    }
}
