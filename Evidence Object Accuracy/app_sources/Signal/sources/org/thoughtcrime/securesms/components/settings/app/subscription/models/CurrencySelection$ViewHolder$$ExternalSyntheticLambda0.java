package org.thoughtcrime.securesms.components.settings.app.subscription.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.CurrencySelection;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CurrencySelection$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ CurrencySelection.Model f$0;

    public /* synthetic */ CurrencySelection$ViewHolder$$ExternalSyntheticLambda0(CurrencySelection.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        CurrencySelection.ViewHolder.m988bind$lambda0(this.f$0, view);
    }
}
