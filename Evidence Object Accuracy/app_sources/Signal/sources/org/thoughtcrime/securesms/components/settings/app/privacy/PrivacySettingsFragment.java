package org.thoughtcrime.securesms.components.settings.app.privacy;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.text.SpannableStringBuilder;
import android.text.style.TextAppearanceSpan;
import android.view.View;
import android.widget.TextView;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.StringCompanionObject;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.ClickPreference;
import org.thoughtcrime.securesms.components.settings.ClickPreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.PreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment;
import org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsViewModel;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues;
import org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;

/* compiled from: PrivacySettingsFragment.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0002()B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0003J\u0010\u0010\u001b\u001a\u00020\u00182\u0006\u0010\u001c\u001a\u00020\u001dH\u0003J\u001c\u0010\u001e\u001a\u000e\u0012\u0004\u0012\u00020\u001d\u0012\u0004\u0012\u00020\u00040\u001f2\u0006\u0010 \u001a\u00020!H\u0002J\u000e\u0010\"\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u001aJ\b\u0010#\u001a\u00020\fH\u0016J\u0010\u0010$\u001a\u00020\f2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J \u0010%\u001a\u00020\u00042\u0006\u0010 \u001a\u00020!2\u0006\u0010&\u001a\u00020\u00142\u0006\u0010'\u001a\u00020\u0014H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "incognitoSummary", "", "getIncognitoSummary", "()Ljava/lang/CharSequence;", "incognitoSummary$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsState;", "getScreenLockInactivityTimeoutSummary", "", "timeoutSeconds", "", "getWhoCanFindMeByPhoneNumberSummary", "", "phoneNumberListingMode", "Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberListingMode;", "getWhoCanSeeMyPhoneNumberSummary", "phoneNumberSharingMode", "Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberSharingMode;", "items", "", "context", "Landroid/content/Context;", "onFindMyPhoneNumberClicked", "onResume", "onSeeMyPhoneNumberClicked", "titleAndDescription", "header", "description", "ValueClickPreference", "ValueClickPreferenceViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PrivacySettingsFragment extends DSLSettingsFragment {
    private final Lazy incognitoSummary$delegate = LazyKt__LazyJVMKt.lazy(new PrivacySettingsFragment$incognitoSummary$2(this));
    private PrivacySettingsViewModel viewModel;

    /* compiled from: PrivacySettingsFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;
        public static final /* synthetic */ int[] $EnumSwitchMapping$1;

        static {
            int[] iArr = new int[PhoneNumberPrivacyValues.PhoneNumberSharingMode.values().length];
            iArr[PhoneNumberPrivacyValues.PhoneNumberSharingMode.EVERYONE.ordinal()] = 1;
            iArr[PhoneNumberPrivacyValues.PhoneNumberSharingMode.CONTACTS.ordinal()] = 2;
            iArr[PhoneNumberPrivacyValues.PhoneNumberSharingMode.NOBODY.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
            int[] iArr2 = new int[PhoneNumberPrivacyValues.PhoneNumberListingMode.values().length];
            iArr2[PhoneNumberPrivacyValues.PhoneNumberListingMode.LISTED.ordinal()] = 1;
            iArr2[PhoneNumberPrivacyValues.PhoneNumberListingMode.UNLISTED.ordinal()] = 2;
            $EnumSwitchMapping$1 = iArr2;
        }
    }

    public PrivacySettingsFragment() {
        super(R.string.preferences__privacy, 0, 0, null, 14, null);
    }

    public final CharSequence getIncognitoSummary() {
        Object value = this.incognitoSummary$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-incognitoSummary>(...)");
        return (CharSequence) value;
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        PrivacySettingsViewModel privacySettingsViewModel = this.viewModel;
        if (privacySettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            privacySettingsViewModel = null;
        }
        privacySettingsViewModel.refreshBlockedCount();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        dSLSettingsAdapter.registerFactory(ValueClickPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new PrivacySettingsFragment.ValueClickPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.value_click_preference_item));
        PrivateStoryItem.INSTANCE.register(dSLSettingsAdapter);
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        PrivacySettingsRepository privacySettingsRepository = new PrivacySettingsRepository();
        Intrinsics.checkNotNullExpressionValue(defaultSharedPreferences, "sharedPreferences");
        ViewModel viewModel = new ViewModelProvider(this, new PrivacySettingsViewModel.Factory(defaultSharedPreferences, privacySettingsRepository)).get(PrivacySettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ngsViewModel::class.java]");
        PrivacySettingsViewModel privacySettingsViewModel = (PrivacySettingsViewModel) viewModel;
        this.viewModel = privacySettingsViewModel;
        if (privacySettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            privacySettingsViewModel = null;
        }
        privacySettingsViewModel.m837getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ PrivacySettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                PrivacySettingsFragment.$r8$lambda$XiHYsmmeumVYutL7J2SQ0p4uhnQ(DSLSettingsAdapter.this, this.f$1, (PrivacySettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m822bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, PrivacySettingsFragment privacySettingsFragment, PrivacySettingsState privacySettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(privacySettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(privacySettingsState, "state");
        dSLSettingsAdapter.submitList(privacySettingsFragment.getConfiguration(privacySettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(PrivacySettingsState privacySettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, privacySettingsState) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1
            final /* synthetic */ PrivacySettingsState $state;
            final /* synthetic */ PrivacySettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$state = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0353: INVOKE  
                  (r26v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel : 0x0350: CONSTRUCTOR  (r4v15 org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel A[REMOVE]) = 
                  (r3v28 'distributionListPartialRecord' org.thoughtcrime.securesms.database.model.DistributionListPartialRecord A[REMOVE])
                  (wrap: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1 : 0x034d: CONSTRUCTOR  (r5v8 org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1 A[REMOVE]) = (r2v23 'privacySettingsFragment17' org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment) call: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1.<init>(org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem.PartialModel.<init>(org.thoughtcrime.securesms.database.model.DistributionListPartialRecord, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0350: CONSTRUCTOR  (r4v15 org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem$PartialModel A[REMOVE]) = 
                  (r3v28 'distributionListPartialRecord' org.thoughtcrime.securesms.database.model.DistributionListPartialRecord A[REMOVE])
                  (wrap: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1 : 0x034d: CONSTRUCTOR  (r5v8 org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1 A[REMOVE]) = (r2v23 'privacySettingsFragment17' org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment) call: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1.<init>(org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.stories.settings.story.PrivateStoryItem.PartialModel.<init>(org.thoughtcrime.securesms.database.model.DistributionListPartialRecord, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 28 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x034d: CONSTRUCTOR  (r5v8 org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1 A[REMOVE]) = (r2v23 'privacySettingsFragment17' org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment) call: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1.<init>(org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 34 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1$17$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 40 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r26) {
                /*
                // Method dump skipped, instructions count: 943
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final String getScreenLockInactivityTimeoutSummary(long j) {
        TimeUnit timeUnit = TimeUnit.SECONDS;
        long hours = timeUnit.toHours(j);
        long minutes = timeUnit.toMinutes(j) - (timeUnit.toHours(j) * ((long) 60));
        if (j <= 0) {
            String string = getString(R.string.AppProtectionPreferenceFragment_none);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      getString(R.stri…renceFragment_none)\n    }");
            return string;
        }
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        String format = String.format(Locale.getDefault(), "%02d:%02d:00", Arrays.copyOf(new Object[]{Long.valueOf(hours), Long.valueOf(minutes)}, 2));
        Intrinsics.checkNotNullExpressionValue(format, "format(locale, format, *args)");
        return format;
    }

    public final int getWhoCanSeeMyPhoneNumberSummary(PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode) {
        int i = WhenMappings.$EnumSwitchMapping$0[phoneNumberSharingMode.ordinal()];
        if (i == 1) {
            return R.string.PhoneNumberPrivacy_everyone;
        }
        if (i == 2) {
            return R.string.PhoneNumberPrivacy_my_contacts;
        }
        if (i == 3) {
            return R.string.PhoneNumberPrivacy_nobody;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final int getWhoCanFindMeByPhoneNumberSummary(PhoneNumberPrivacyValues.PhoneNumberListingMode phoneNumberListingMode) {
        int i = WhenMappings.$EnumSwitchMapping$1[phoneNumberListingMode.ordinal()];
        if (i == 1) {
            return R.string.PhoneNumberPrivacy_everyone;
        }
        if (i == 2) {
            return R.string.PhoneNumberPrivacy_nobody;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final void onSeeMyPhoneNumberClicked(PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode) {
        PhoneNumberPrivacyValues.PhoneNumberSharingMode[] phoneNumberSharingModeArr = {phoneNumberSharingMode};
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        Map<PhoneNumberPrivacyValues.PhoneNumberSharingMode, CharSequence> items = items(requireContext);
        ArrayList arrayList = new ArrayList(items.keySet());
        Object[] array = items.values().toArray(new CharSequence[0]);
        if (array != null) {
            int indexOf = arrayList.indexOf(phoneNumberSharingModeArr[0]);
            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
            materialAlertDialogBuilder.setTitle(R.string.preferences_app_protection__see_my_phone_number);
            materialAlertDialogBuilder.setCancelable(true);
            materialAlertDialogBuilder.setSingleChoiceItems((CharSequence[]) array, indexOf, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(phoneNumberSharingModeArr, arrayList) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ PhoneNumberPrivacyValues.PhoneNumberSharingMode[] f$0;
                public final /* synthetic */ List f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    PrivacySettingsFragment.m821$r8$lambda$iFJycCK9ECcujX3I1fM_RCgMrc(this.f$0, this.f$1, dialogInterface, i);
                }
            });
            materialAlertDialogBuilder.setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(phoneNumberSharingModeArr, this) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$$ExternalSyntheticLambda3
                public final /* synthetic */ PhoneNumberPrivacyValues.PhoneNumberSharingMode[] f$0;
                public final /* synthetic */ PrivacySettingsFragment f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // android.content.DialogInterface.OnClickListener
                public final void onClick(DialogInterface dialogInterface, int i) {
                    PrivacySettingsFragment.$r8$lambda$kQtYvdQWehe58te9PPUcSerIuhs(this.f$0, this.f$1, dialogInterface, i);
                }
            });
            materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
            materialAlertDialogBuilder.show();
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
    }

    /* renamed from: onSeeMyPhoneNumberClicked$lambda-3$lambda-1 */
    public static final void m825onSeeMyPhoneNumberClicked$lambda3$lambda1(PhoneNumberPrivacyValues.PhoneNumberSharingMode[] phoneNumberSharingModeArr, List list, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(phoneNumberSharingModeArr, "$value");
        Intrinsics.checkNotNullParameter(list, "$modes");
        phoneNumberSharingModeArr[0] = (PhoneNumberPrivacyValues.PhoneNumberSharingMode) list.get(i);
    }

    /* renamed from: onSeeMyPhoneNumberClicked$lambda-3$lambda-2 */
    public static final void m826onSeeMyPhoneNumberClicked$lambda3$lambda2(PhoneNumberPrivacyValues.PhoneNumberSharingMode[] phoneNumberSharingModeArr, PrivacySettingsFragment privacySettingsFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(phoneNumberSharingModeArr, "$value");
        Intrinsics.checkNotNullParameter(privacySettingsFragment, "this$0");
        PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode = phoneNumberSharingModeArr[0];
        String str = PrivacySettingsFragmentKt.TAG;
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        String format = String.format("PhoneNumberSharingMode changed to %s. Scheduling storage value sync", Arrays.copyOf(new Object[]{phoneNumberSharingMode}, 1));
        Intrinsics.checkNotNullExpressionValue(format, "format(format, *args)");
        Log.i(str, format);
        PrivacySettingsViewModel privacySettingsViewModel = privacySettingsFragment.viewModel;
        if (privacySettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            privacySettingsViewModel = null;
        }
        privacySettingsViewModel.setPhoneNumberSharingMode(phoneNumberSharingModeArr[0]);
    }

    private final Map<PhoneNumberPrivacyValues.PhoneNumberSharingMode, CharSequence> items(Context context) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode = PhoneNumberPrivacyValues.PhoneNumberSharingMode.EVERYONE;
        String string = context.getString(R.string.PhoneNumberPrivacy_everyone);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…neNumberPrivacy_everyone)");
        String string2 = context.getString(R.string.PhoneNumberPrivacy_everyone_see_description);
        Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…everyone_see_description)");
        linkedHashMap.put(phoneNumberSharingMode, titleAndDescription(context, string, string2));
        PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode2 = PhoneNumberPrivacyValues.PhoneNumberSharingMode.NOBODY;
        String string3 = context.getString(R.string.PhoneNumberPrivacy_nobody);
        Intrinsics.checkNotNullExpressionValue(string3, "context.getString(R.stri…honeNumberPrivacy_nobody)");
        linkedHashMap.put(phoneNumberSharingMode2, string3);
        return linkedHashMap;
    }

    private final CharSequence titleAndDescription(Context context, String str, String str2) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append((CharSequence) "\n");
        spannableStringBuilder.append((CharSequence) str);
        spannableStringBuilder.append((CharSequence) "\n");
        spannableStringBuilder.setSpan(new TextAppearanceSpan(context, 16973894), spannableStringBuilder.length(), spannableStringBuilder.length(), 18);
        spannableStringBuilder.append((CharSequence) str2);
        spannableStringBuilder.append((CharSequence) "\n");
        return spannableStringBuilder;
    }

    public final void onFindMyPhoneNumberClicked(PhoneNumberPrivacyValues.PhoneNumberListingMode phoneNumberListingMode) {
        Intrinsics.checkNotNullParameter(phoneNumberListingMode, "phoneNumberListingMode");
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        PhoneNumberPrivacyValues.PhoneNumberListingMode[] phoneNumberListingModeArr = {phoneNumberListingMode};
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(requireActivity());
        materialAlertDialogBuilder.setTitle(R.string.preferences_app_protection__find_me_by_phone_number);
        materialAlertDialogBuilder.setCancelable(true);
        String string = requireContext.getString(R.string.PhoneNumberPrivacy_everyone);
        Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…neNumberPrivacy_everyone)");
        String string2 = requireContext.getString(R.string.PhoneNumberPrivacy_everyone_find_description);
        Intrinsics.checkNotNullExpressionValue(string2, "context.getString(R.stri…veryone_find_description)");
        String string3 = requireContext.getString(R.string.PhoneNumberPrivacy_nobody);
        Intrinsics.checkNotNullExpressionValue(string3, "context.getString(R.stri…honeNumberPrivacy_nobody)");
        materialAlertDialogBuilder.setSingleChoiceItems(new CharSequence[]{titleAndDescription(requireContext, string, string2), string3}, phoneNumberListingModeArr[0].ordinal(), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(phoneNumberListingModeArr) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$$ExternalSyntheticLambda4
            public final /* synthetic */ PhoneNumberPrivacyValues.PhoneNumberListingMode[] f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PrivacySettingsFragment.$r8$lambda$niCYgGpQKK4C9qtxvt36mI2M6cQ(this.f$0, dialogInterface, i);
            }
        });
        materialAlertDialogBuilder.setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(phoneNumberListingModeArr, this) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ PhoneNumberPrivacyValues.PhoneNumberListingMode[] f$0;
            public final /* synthetic */ PrivacySettingsFragment f$1;

            {
                this.f$0 = r1;
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                PrivacySettingsFragment.m820$r8$lambda$Jp2jTRR2xYNf76P67MA0yMrd7w(this.f$0, this.f$1, dialogInterface, i);
            }
        });
        materialAlertDialogBuilder.setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
        materialAlertDialogBuilder.show();
    }

    /* renamed from: onFindMyPhoneNumberClicked$lambda-7$lambda-5 */
    public static final void m823onFindMyPhoneNumberClicked$lambda7$lambda5(PhoneNumberPrivacyValues.PhoneNumberListingMode[] phoneNumberListingModeArr, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(phoneNumberListingModeArr, "$value");
        phoneNumberListingModeArr[0] = PhoneNumberPrivacyValues.PhoneNumberListingMode.values()[i];
    }

    /* renamed from: onFindMyPhoneNumberClicked$lambda-7$lambda-6 */
    public static final void m824onFindMyPhoneNumberClicked$lambda7$lambda6(PhoneNumberPrivacyValues.PhoneNumberListingMode[] phoneNumberListingModeArr, PrivacySettingsFragment privacySettingsFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(phoneNumberListingModeArr, "$value");
        Intrinsics.checkNotNullParameter(privacySettingsFragment, "this$0");
        String str = PrivacySettingsFragmentKt.TAG;
        StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
        String format = String.format("PhoneNumberListingMode changed to %s. Scheduling storage value sync", Arrays.copyOf(new Object[]{phoneNumberListingModeArr[0]}, 1));
        Intrinsics.checkNotNullExpressionValue(format, "format(format, *args)");
        Log.i(str, format);
        PrivacySettingsViewModel privacySettingsViewModel = privacySettingsFragment.viewModel;
        if (privacySettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            privacySettingsViewModel = null;
        }
        privacySettingsViewModel.setPhoneNumberListingMode(phoneNumberListingModeArr[0]);
    }

    /* compiled from: PrivacySettingsFragment.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsFragment$ValueClickPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", DraftDatabase.DRAFT_VALUE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "clickPreference", "Lorg/thoughtcrime/securesms/components/settings/ClickPreference;", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/ClickPreference;)V", "getClickPreference", "()Lorg/thoughtcrime/securesms/components/settings/ClickPreference;", "getValue", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "areContentsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ValueClickPreference extends PreferenceModel<ValueClickPreference> {
        private final ClickPreference clickPreference;
        private final DSLSettingsText value;

        public final DSLSettingsText getValue() {
            return this.value;
        }

        public final ClickPreference getClickPreference() {
            return this.clickPreference;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ValueClickPreference(DSLSettingsText dSLSettingsText, ClickPreference clickPreference) {
            super(clickPreference.getTitle(), clickPreference.getSummary(), clickPreference.getIcon(), null, clickPreference.isEnabled(), 8, null);
            Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.DRAFT_VALUE);
            Intrinsics.checkNotNullParameter(clickPreference, "clickPreference");
            this.value = dSLSettingsText;
            this.clickPreference = clickPreference;
        }

        public boolean areContentsTheSame(ValueClickPreference valueClickPreference) {
            Intrinsics.checkNotNullParameter(valueClickPreference, "newItem");
            return super.areContentsTheSame(valueClickPreference) && Intrinsics.areEqual(this.clickPreference, valueClickPreference.clickPreference) && Intrinsics.areEqual(this.value, valueClickPreference.value);
        }
    }

    /* compiled from: PrivacySettingsFragment.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsFragment$ValueClickPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsFragment$ValueClickPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "clickPreferenceViewHolder", "Lorg/thoughtcrime/securesms/components/settings/ClickPreferenceViewHolder;", "valueText", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ValueClickPreferenceViewHolder extends PreferenceViewHolder<ValueClickPreference> {
        private final ClickPreferenceViewHolder clickPreferenceViewHolder;
        private final TextView valueText;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ValueClickPreferenceViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.clickPreferenceViewHolder = new ClickPreferenceViewHolder(view);
            View findViewById = findViewById(R.id.value_client_preference_value);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.value_client_preference_value)");
            this.valueText = (TextView) findViewById;
        }

        public void bind(ValueClickPreference valueClickPreference) {
            Intrinsics.checkNotNullParameter(valueClickPreference, "model");
            super.bind((ValueClickPreferenceViewHolder) valueClickPreference);
            this.clickPreferenceViewHolder.bind(valueClickPreference.getClickPreference());
            TextView textView = this.valueText;
            DSLSettingsText value = valueClickPreference.getValue();
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setText(value.resolve(context));
        }
    }
}
