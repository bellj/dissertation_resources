package org.thoughtcrime.securesms.components;

import android.view.View;
import org.thoughtcrime.securesms.components.ThreadPhotoRailView;
import org.thoughtcrime.securesms.database.MediaDatabase;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ThreadPhotoRailView$ThreadPhotoRailAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ThreadPhotoRailView.ThreadPhotoRailAdapter f$0;
    public final /* synthetic */ MediaDatabase.MediaRecord f$1;

    public /* synthetic */ ThreadPhotoRailView$ThreadPhotoRailAdapter$$ExternalSyntheticLambda0(ThreadPhotoRailView.ThreadPhotoRailAdapter threadPhotoRailAdapter, MediaDatabase.MediaRecord mediaRecord) {
        this.f$0 = threadPhotoRailAdapter;
        this.f$1 = mediaRecord;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onBindItemViewHolder$0(this.f$1, view);
    }
}
