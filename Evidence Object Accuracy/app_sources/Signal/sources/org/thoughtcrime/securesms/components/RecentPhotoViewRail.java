package org.thoughtcrime.securesms.components;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.signature.MediaStoreSignature;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.CursorRecyclerViewAdapter;
import org.thoughtcrime.securesms.database.loaders.RecentPhotosLoader;
import org.thoughtcrime.securesms.mms.GlideApp;

/* loaded from: classes4.dex */
public class RecentPhotoViewRail extends FrameLayout implements LoaderManager.LoaderCallbacks<Cursor> {
    private OnItemClickedListener listener;
    private final RecyclerView recyclerView;

    /* loaded from: classes4.dex */
    public interface OnItemClickedListener {
        void onItemClicked(Uri uri, String str, String str2, long j, int i, int i2, long j2);
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public /* bridge */ /* synthetic */ void onLoadFinished(Loader loader, Object obj) {
        onLoadFinished((Loader<Cursor>) loader, (Cursor) obj);
    }

    public RecentPhotoViewRail(Context context) {
        this(context, null);
    }

    public RecentPhotoViewRail(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RecentPhotoViewRail(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.recent_photo_view, this);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.photo_list);
        this.recyclerView = recyclerView;
        recyclerView.setLayoutManager(new LinearLayoutManager(context, 0, false));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public void setListener(OnItemClickedListener onItemClickedListener) {
        this.listener = onItemClickedListener;
        if (this.recyclerView.getAdapter() != null) {
            ((RecentPhotoAdapter) this.recyclerView.getAdapter()).setListener(onItemClickedListener);
        }
    }

    @Override // androidx.loader.app.LoaderManager.LoaderCallbacks
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new RecentPhotosLoader(getContext());
    }

    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        this.recyclerView.setAdapter(new RecentPhotoAdapter(getContext(), cursor, RecentPhotosLoader.BASE_URL, this.listener));
    }

    public void onLoaderReset(Loader<Cursor> loader) {
        ((CursorRecyclerViewAdapter) this.recyclerView.getAdapter()).changeCursor(null);
    }

    /* loaded from: classes4.dex */
    public static class RecentPhotoAdapter extends CursorRecyclerViewAdapter<RecentPhotoViewHolder> {
        private static final String TAG = Log.tag(RecentPhotoAdapter.class);
        private final Uri baseUri;
        private OnItemClickedListener clickedListener;

        private String getHeightColumn(int i) {
            return (i == 0 || i == 180) ? "height" : "width";
        }

        private String getWidthColumn(int i) {
            return (i == 0 || i == 180) ? "width" : "height";
        }

        private RecentPhotoAdapter(Context context, Cursor cursor, Uri uri, OnItemClickedListener onItemClickedListener) {
            super(context, cursor);
            this.baseUri = uri;
            this.clickedListener = onItemClickedListener;
        }

        public RecentPhotoViewHolder onCreateItemViewHolder(ViewGroup viewGroup, int i) {
            return new RecentPhotoViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recent_photo_view_item, viewGroup, false));
        }

        public void onBindItemViewHolder(RecentPhotoViewHolder recentPhotoViewHolder, Cursor cursor) {
            recentPhotoViewHolder.imageView.setImageDrawable(null);
            long j = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
            long j2 = cursor.getLong(cursor.getColumnIndexOrThrow("datetaken"));
            long j3 = cursor.getLong(cursor.getColumnIndexOrThrow("date_modified"));
            String string = cursor.getString(cursor.getColumnIndexOrThrow("mime_type"));
            String string2 = cursor.getString(cursor.getColumnIndexOrThrow("bucket_id"));
            int i = cursor.getInt(cursor.getColumnIndexOrThrow("orientation"));
            int i2 = cursor.getInt(cursor.getColumnIndexOrThrow(getWidthColumn(i)));
            int i3 = cursor.getInt(cursor.getColumnIndexOrThrow(getHeightColumn(i)));
            Uri withAppendedId = ContentUris.withAppendedId(RecentPhotosLoader.BASE_URL, j);
            GlideApp.with(getContext().getApplicationContext()).load(withAppendedId).signature((Key) new MediaStoreSignature(string, j3, i)).diskCacheStrategy(DiskCacheStrategy.NONE).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()).into(recentPhotoViewHolder.imageView);
            recentPhotoViewHolder.imageView.setOnClickListener(new RecentPhotoViewRail$RecentPhotoAdapter$$ExternalSyntheticLambda0(this, withAppendedId, string, string2, j2, i2, i3, (long) cursor.getInt(cursor.getColumnIndexOrThrow("_size"))));
        }

        public /* synthetic */ void lambda$onBindItemViewHolder$0(Uri uri, String str, String str2, long j, int i, int i2, long j2, View view) {
            OnItemClickedListener onItemClickedListener = this.clickedListener;
            if (onItemClickedListener != null) {
                onItemClickedListener.onItemClicked(uri, str, str2, j, i, i2, j2);
            }
        }

        public void setListener(OnItemClickedListener onItemClickedListener) {
            this.clickedListener = onItemClickedListener;
        }

        /* loaded from: classes4.dex */
        public static class RecentPhotoViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView;

            RecentPhotoViewHolder(View view) {
                super(view);
                this.imageView = (ImageView) view.findViewById(R.id.thumbnail);
            }
        }
    }
}
