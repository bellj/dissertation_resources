package org.thoughtcrime.securesms.components.settings.app.help;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.help.HelpFragment;

/* loaded from: classes4.dex */
public class HelpSettingsFragmentDirections {
    private HelpSettingsFragmentDirections() {
    }

    public static ActionHelpSettingsFragmentToHelpFragment actionHelpSettingsFragmentToHelpFragment() {
        return new ActionHelpSettingsFragmentToHelpFragment();
    }

    public static NavDirections actionHelpSettingsFragmentToSubmitDebugLogActivity() {
        return new ActionOnlyNavDirections(R.id.action_helpSettingsFragment_to_submitDebugLogActivity);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionHelpSettingsFragmentToHelpFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_helpSettingsFragment_to_helpFragment;
        }

        private ActionHelpSettingsFragmentToHelpFragment() {
            this.arguments = new HashMap();
        }

        public ActionHelpSettingsFragmentToHelpFragment setStartCategoryIndex(int i) {
            this.arguments.put(HelpFragment.START_CATEGORY_INDEX, Integer.valueOf(i));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX)) {
                bundle.putInt(HelpFragment.START_CATEGORY_INDEX, ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue());
            } else {
                bundle.putInt(HelpFragment.START_CATEGORY_INDEX, 0);
            }
            return bundle;
        }

        public int getStartCategoryIndex() {
            return ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionHelpSettingsFragmentToHelpFragment actionHelpSettingsFragmentToHelpFragment = (ActionHelpSettingsFragmentToHelpFragment) obj;
            return this.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX) == actionHelpSettingsFragmentToHelpFragment.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX) && getStartCategoryIndex() == actionHelpSettingsFragmentToHelpFragment.getStartCategoryIndex() && getActionId() == actionHelpSettingsFragmentToHelpFragment.getActionId();
        }

        public int hashCode() {
            return ((getStartCategoryIndex() + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionHelpSettingsFragmentToHelpFragment(actionId=" + getActionId() + "){startCategoryIndex=" + getStartCategoryIndex() + "}";
        }
    }
}
