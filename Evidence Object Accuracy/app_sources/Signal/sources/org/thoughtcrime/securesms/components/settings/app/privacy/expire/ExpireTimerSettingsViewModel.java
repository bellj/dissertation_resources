package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.content.Context;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.ProcessState;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: ExpireTimerSettingsViewModel.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002\u0018\u0019B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0012\u001a\u00020\u0013J\u0006\u0010\u0014\u001a\u00020\u0013J\u000e\u0010\u0015\u001a\u00020\u00132\u0006\u0010\u0016\u001a\u00020\u0017R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "config", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsRepository;)V", "getConfig", "()Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "resetError", "", "save", "select", "time", "", "Config", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsViewModel extends ViewModel {
    private final Config config;
    private final RecipientId recipientId;
    private final ExpireTimerSettingsRepository repository;
    private final LiveData<ExpireTimerSettingsState> state;
    private final Store<ExpireTimerSettingsState> store;

    /* JADX DEBUG: Type inference failed for r10v4. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public ExpireTimerSettingsViewModel(Config config, ExpireTimerSettingsRepository expireTimerSettingsRepository) {
        Intrinsics.checkNotNullParameter(config, "config");
        Intrinsics.checkNotNullParameter(expireTimerSettingsRepository, "repository");
        this.config = config;
        this.repository = expireTimerSettingsRepository;
        Store<ExpireTimerSettingsState> store = new Store<>(new ExpireTimerSettingsState(0, null, null, config.getForResultMode(), false, 23, null));
        this.store = store;
        RecipientId recipientId = config.getRecipientId();
        this.recipientId = recipientId;
        LiveData<ExpireTimerSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        if (recipientId != null) {
            store.update(Recipient.live(recipientId).getLiveData(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda0
                @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
                public final Object apply(Object obj, Object obj2) {
                    return ExpireTimerSettingsViewModel.m873_init_$lambda0((Recipient) obj, (ExpireTimerSettingsState) obj2);
                }
            });
        } else {
            store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ExpireTimerSettingsViewModel.m874_init_$lambda1(ExpireTimerSettingsViewModel.this, (ExpireTimerSettingsState) obj);
                }
            });
        }
    }

    public final Config getConfig() {
        return this.config;
    }

    public final LiveData<ExpireTimerSettingsState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final ExpireTimerSettingsState m873_init_$lambda0(Recipient recipient, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "s");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, recipient.getExpiresInSeconds(), null, null, false, true, 14, null);
    }

    /* renamed from: _init_$lambda-1 */
    public static final ExpireTimerSettingsState m874_init_$lambda1(ExpireTimerSettingsViewModel expireTimerSettingsViewModel, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullParameter(expireTimerSettingsViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        Integer initialValue = expireTimerSettingsViewModel.config.getInitialValue();
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, initialValue != null ? initialValue.intValue() : SignalStore.settings().getUniversalExpireTimer(), null, null, false, false, 30, null);
    }

    /* renamed from: select$lambda-2 */
    public static final ExpireTimerSettingsState m879select$lambda2(int i, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, Integer.valueOf(i), null, false, false, 29, null);
    }

    public final void select(int i) {
        this.store.update(new Function(i) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda6
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ExpireTimerSettingsViewModel.m879select$lambda2(this.f$0, (ExpireTimerSettingsState) obj);
            }
        });
    }

    public final void save() {
        int currentTimer = this.store.getState().getCurrentTimer();
        if (currentTimer == this.store.getState().getInitialTimer()) {
            this.store.update(new Function(currentTimer) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda2
                public final /* synthetic */ int f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ExpireTimerSettingsViewModel.m876save$lambda3(this.f$0, (ExpireTimerSettingsState) obj);
                }
            });
            return;
        }
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ExpireTimerSettingsViewModel.m877save$lambda4((ExpireTimerSettingsState) obj);
            }
        });
        RecipientId recipientId = this.recipientId;
        if (recipientId != null) {
            this.repository.setExpiration(recipientId, currentTimer, new ExpireTimerSettingsViewModel$save$3(this));
        } else if (this.config.getForResultMode()) {
            this.store.update(new Function(currentTimer) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda4
                public final /* synthetic */ int f$0;

                {
                    this.f$0 = r1;
                }

                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return ExpireTimerSettingsViewModel.m878save$lambda5(this.f$0, (ExpireTimerSettingsState) obj);
                }
            });
        } else {
            this.repository.setUniversalExpireTimerSeconds(currentTimer, new ExpireTimerSettingsViewModel$save$5(this, currentTimer));
        }
    }

    /* renamed from: save$lambda-3 */
    public static final ExpireTimerSettingsState m876save$lambda3(int i, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, null, new ProcessState.Success(Integer.valueOf(i)), false, false, 27, null);
    }

    /* renamed from: save$lambda-4 */
    public static final ExpireTimerSettingsState m877save$lambda4(ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, null, new ProcessState.Working(), false, false, 27, null);
    }

    /* renamed from: save$lambda-5 */
    public static final ExpireTimerSettingsState m878save$lambda5(int i, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, null, new ProcessState.Success(Integer.valueOf(i)), false, false, 27, null);
    }

    /* renamed from: resetError$lambda-6 */
    public static final ExpireTimerSettingsState m875resetError$lambda6(ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, null, new ProcessState.Idle(), false, false, 27, null);
    }

    public final void resetError() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ExpireTimerSettingsViewModel.m875resetError$lambda6((ExpireTimerSettingsState) obj);
            }
        });
    }

    /* compiled from: ExpireTimerSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u000b\u001a\u0002H\f\"\b\b\u0000\u0010\f*\u00020\r2\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u0002H\f0\u000fH\u0016¢\u0006\u0002\u0010\u0010R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "context", "Landroid/content/Context;", "config", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;", "(Landroid/content/Context;Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;)V", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsRepository;", "getRepository", "()Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsRepository;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Config config;
        private final ExpireTimerSettingsRepository repository;

        public Factory(Context context, Config config) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(config, "config");
            this.config = config;
            Context applicationContext = context.getApplicationContext();
            Intrinsics.checkNotNullExpressionValue(applicationContext, "context.applicationContext");
            this.repository = new ExpireTimerSettingsRepository(applicationContext);
        }

        public final ExpireTimerSettingsRepository getRepository() {
            return this.repository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new ExpireTimerSettingsViewModel(this.config, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* compiled from: ExpireTimerSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\b\n\u0002\b\u0011\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B'\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\u000b\u0010\u0010\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0005HÆ\u0003J\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0007HÆ\u0003¢\u0006\u0002\u0010\fJ0\u0010\u0013\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001¢\u0006\u0002\u0010\u0014J\u0013\u0010\u0015\u001a\u00020\u00052\b\u0010\u0016\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0017\u001a\u00020\u0007HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0015\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\n\n\u0002\u0010\r\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "forResultMode", "", "initialValue", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;ZLjava/lang/Integer;)V", "getForResultMode", "()Z", "getInitialValue", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "copy", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;ZLjava/lang/Integer;)Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Config {
        private final boolean forResultMode;
        private final Integer initialValue;
        private final RecipientId recipientId;

        public Config() {
            this(null, false, null, 7, null);
        }

        public static /* synthetic */ Config copy$default(Config config, RecipientId recipientId, boolean z, Integer num, int i, Object obj) {
            if ((i & 1) != 0) {
                recipientId = config.recipientId;
            }
            if ((i & 2) != 0) {
                z = config.forResultMode;
            }
            if ((i & 4) != 0) {
                num = config.initialValue;
            }
            return config.copy(recipientId, z, num);
        }

        public final RecipientId component1() {
            return this.recipientId;
        }

        public final boolean component2() {
            return this.forResultMode;
        }

        public final Integer component3() {
            return this.initialValue;
        }

        public final Config copy(RecipientId recipientId, boolean z, Integer num) {
            return new Config(recipientId, z, num);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Config)) {
                return false;
            }
            Config config = (Config) obj;
            return Intrinsics.areEqual(this.recipientId, config.recipientId) && this.forResultMode == config.forResultMode && Intrinsics.areEqual(this.initialValue, config.initialValue);
        }

        public int hashCode() {
            RecipientId recipientId = this.recipientId;
            int i = 0;
            int hashCode = (recipientId == null ? 0 : recipientId.hashCode()) * 31;
            boolean z = this.forResultMode;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = (hashCode + i2) * 31;
            Integer num = this.initialValue;
            if (num != null) {
                i = num.hashCode();
            }
            return i5 + i;
        }

        public String toString() {
            return "Config(recipientId=" + this.recipientId + ", forResultMode=" + this.forResultMode + ", initialValue=" + this.initialValue + ')';
        }

        public Config(RecipientId recipientId, boolean z, Integer num) {
            this.recipientId = recipientId;
            this.forResultMode = z;
            this.initialValue = num;
        }

        public /* synthetic */ Config(RecipientId recipientId, boolean z, Integer num, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : recipientId, (i & 2) != 0 ? false : z, (i & 4) != 0 ? null : num);
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final boolean getForResultMode() {
            return this.forResultMode;
        }

        public final Integer getInitialValue() {
            return this.initialValue;
        }
    }
}
