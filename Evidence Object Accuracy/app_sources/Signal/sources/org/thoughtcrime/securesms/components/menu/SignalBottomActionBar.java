package org.thoughtcrime.securesms.components.menu;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: SignalBottomActionBar.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0012H\u0002J(\u0010\u001a\u001a\u00020\u00162\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u001e\u001a\u00020\u001c2\u0006\u0010\u001f\u001a\u00020\u001cH\u0014J\u0016\u0010 \u001a\u00020\u00162\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120!H\u0002J\u0014\u0010\"\u001a\u00020\u00162\f\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120!R\u001b\u0010\u0007\u001a\u00020\b8FX\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u001b\u0010\r\u001a\u00020\b8FX\u0002¢\u0006\f\n\u0004\b\u000f\u0010\f\u001a\u0004\b\u000e\u0010\nR\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/SignalBottomActionBar;", "Landroid/widget/LinearLayout;", "context", "Landroid/content/Context;", "attributeSet", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "enterAnimation", "Landroid/view/animation/Animation;", "getEnterAnimation", "()Landroid/view/animation/Animation;", "enterAnimation$delegate", "Lkotlin/Lazy;", "exitAnimation", "getExitAnimation", "exitAnimation$delegate", "items", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "getItems", "()Ljava/util/List;", "bindItem", "", "view", "Landroid/view/View;", "item", "onSizeChanged", "w", "", "h", "oldw", "oldh", "present", "", "setItems", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SignalBottomActionBar extends LinearLayout {
    private final Lazy enterAnimation$delegate;
    private final Lazy exitAnimation$delegate;
    private final List<ActionItem> items = new ArrayList();

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SignalBottomActionBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(attributeSet, "attributeSet");
        this.enterAnimation$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Animation>(context) { // from class: org.thoughtcrime.securesms.components.menu.SignalBottomActionBar$enterAnimation$2
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final Animation invoke() {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.$context, R.anim.slide_fade_from_bottom);
                loadAnimation.setDuration(250);
                loadAnimation.setInterpolator(new FastOutSlowInInterpolator());
                return loadAnimation;
            }
        });
        this.exitAnimation$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Animation>(context) { // from class: org.thoughtcrime.securesms.components.menu.SignalBottomActionBar$exitAnimation$2
            final /* synthetic */ Context $context;

            /* access modifiers changed from: package-private */
            {
                this.$context = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final Animation invoke() {
                Animation loadAnimation = AnimationUtils.loadAnimation(this.$context, R.anim.slide_fade_to_bottom);
                loadAnimation.setDuration(250);
                loadAnimation.setInterpolator(new FastOutSlowInInterpolator());
                return loadAnimation;
            }
        });
        setOrientation(0);
        setBackgroundResource(R.drawable.signal_bottom_action_bar_background);
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation(20.0f);
        }
    }

    public final List<ActionItem> getItems() {
        return this.items;
    }

    public final Animation getEnterAnimation() {
        Object value = this.enterAnimation$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-enterAnimation>(...)");
        return (Animation) value;
    }

    public final Animation getExitAnimation() {
        Object value = this.exitAnimation$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-exitAnimation>(...)");
        return (Animation) value;
    }

    public final void setItems(List<ActionItem> list) {
        Intrinsics.checkNotNullParameter(list, "items");
        this.items.clear();
        this.items.addAll(list);
        present(this.items);
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        if (i != i3) {
            present(this.items);
        }
    }

    private final void present(List<ActionItem> list) {
        if (getWidth() != 0) {
            boolean isLayoutRequested = isLayoutRequested();
            int pxToDp = (int) (ViewUtil.pxToDp((float) getWidth()) / ((float) 80));
            int size = list.size() <= pxToDp ? list.size() : pxToDp - 1;
            List<ActionItem> subList = list.subList(0, size);
            List<ActionItem> subList2 = subList.size() < list.size() ? list.subList(size, list.size()) : CollectionsKt__CollectionsKt.emptyList();
            removeAllViews();
            for (ActionItem actionItem : subList) {
                View inflate = LayoutInflater.from(getContext()).inflate(R.layout.signal_bottom_action_bar_item, (ViewGroup) this, false);
                Intrinsics.checkNotNullExpressionValue(inflate, "from(context).inflate(R.…on_bar_item, this, false)");
                addView(inflate);
                bindItem(inflate, actionItem);
            }
            if (!subList2.isEmpty()) {
                View inflate2 = LayoutInflater.from(getContext()).inflate(R.layout.signal_bottom_action_bar_item, (ViewGroup) this, false);
                Intrinsics.checkNotNullExpressionValue(inflate2, "from(context).inflate(R.…on_bar_item, this, false)");
                addView(inflate2);
                String string = getContext().getString(R.string.SignalBottomActionBar_more);
                Intrinsics.checkNotNullExpressionValue(string, "context.getString(R.stri…gnalBottomActionBar_more)");
                bindItem(inflate2, new ActionItem(R.drawable.ic_more_horiz_24, string, 0, new Runnable(inflate2, this, subList2) { // from class: org.thoughtcrime.securesms.components.menu.SignalBottomActionBar$$ExternalSyntheticLambda0
                    public final /* synthetic */ View f$0;
                    public final /* synthetic */ SignalBottomActionBar f$1;
                    public final /* synthetic */ List f$2;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        SignalBottomActionBar.m510$r8$lambda$C9gl1P35tl_slpcEt5jrCwb9CA(this.f$0, this.f$1, this.f$2);
                    }
                }, 4, null));
            }
            if (isLayoutRequested) {
                post(new Runnable() { // from class: org.thoughtcrime.securesms.components.menu.SignalBottomActionBar$$ExternalSyntheticLambda1
                    @Override // java.lang.Runnable
                    public final void run() {
                        SignalBottomActionBar.$r8$lambda$WBmfFud93hn7xPAVpQrn80HTPTE(SignalBottomActionBar.this);
                    }
                });
            }
        }
    }

    /* renamed from: present$lambda-1 */
    public static final void m512present$lambda1(View view, SignalBottomActionBar signalBottomActionBar, List list) {
        Intrinsics.checkNotNullParameter(view, "$view");
        Intrinsics.checkNotNullParameter(signalBottomActionBar, "this$0");
        Intrinsics.checkNotNullParameter(list, "$overflowItems");
        ViewParent parent = signalBottomActionBar.getParent();
        if (parent != null) {
            new SignalContextMenu.Builder(view, (ViewGroup) parent).preferredHorizontalPosition(SignalContextMenu.HorizontalPosition.END).offsetY(ViewUtil.dpToPx(8)).show(list);
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.ViewGroup");
    }

    /* renamed from: present$lambda-2 */
    public static final void m513present$lambda2(SignalBottomActionBar signalBottomActionBar) {
        Intrinsics.checkNotNullParameter(signalBottomActionBar, "this$0");
        signalBottomActionBar.requestLayout();
    }

    private final void bindItem(View view, ActionItem actionItem) {
        View findViewById = view.findViewById(R.id.signal_bottom_action_bar_item_icon);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.s…tom_action_bar_item_icon)");
        View findViewById2 = view.findViewById(R.id.signal_bottom_action_bar_item_title);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.s…om_action_bar_item_title)");
        ((ImageView) findViewById).setImageResource(actionItem.getIconRes());
        ((TextView) findViewById2).setText(actionItem.getTitle());
        view.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.menu.SignalBottomActionBar$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SignalBottomActionBar.$r8$lambda$gFrNARP1PsXAFHNCnuFq4Kumukw(ActionItem.this, view2);
            }
        });
    }

    /* renamed from: bindItem$lambda-3 */
    public static final void m511bindItem$lambda3(ActionItem actionItem, View view) {
        Intrinsics.checkNotNullParameter(actionItem, "$item");
        actionItem.getAction().run();
    }
}
