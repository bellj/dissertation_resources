package org.thoughtcrime.securesms.components.settings.app.wrapped;

import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.preferences.EditProxyFragment;

/* compiled from: WrappedEditProxyFragment.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/wrapped/WrappedEditProxyFragment;", "Lorg/thoughtcrime/securesms/components/settings/app/wrapped/SettingsWrapperFragment;", "()V", "getFragment", "Landroidx/fragment/app/Fragment;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class WrappedEditProxyFragment extends SettingsWrapperFragment {
    @Override // org.thoughtcrime.securesms.components.settings.app.wrapped.SettingsWrapperFragment
    public Fragment getFragment() {
        getToolbar().setTitle(R.string.preferences_use_proxy);
        return new EditProxyFragment();
    }
}
