package org.thoughtcrime.securesms.components.settings.app.data;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: DataAndStorageSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "totalStorageUse", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DataAndStorageSettingsViewModel$refresh$1 extends Lambda implements Function1<Long, Unit> {
    final /* synthetic */ DataAndStorageSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DataAndStorageSettingsViewModel$refresh$1(DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel) {
        super(1);
        this.this$0 = dataAndStorageSettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Long l) {
        invoke(l.longValue());
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final DataAndStorageSettingsState m650invoke$lambda0(DataAndStorageSettingsViewModel dataAndStorageSettingsViewModel, long j, DataAndStorageSettingsState dataAndStorageSettingsState) {
        Intrinsics.checkNotNullParameter(dataAndStorageSettingsViewModel, "this$0");
        return r0.copy((r18 & 1) != 0 ? r0.totalStorageUse : j, (r18 & 2) != 0 ? r0.mobileAutoDownloadValues : null, (r18 & 4) != 0 ? r0.wifiAutoDownloadValues : null, (r18 & 8) != 0 ? r0.roamingAutoDownloadValues : null, (r18 & 16) != 0 ? r0.callBandwidthMode : null, (r18 & 32) != 0 ? r0.isProxyEnabled : false, (r18 & 64) != 0 ? dataAndStorageSettingsViewModel.getState().sentMediaQuality : null);
    }

    public final void invoke(long j) {
        this.this$0.store.update(new Function(j) { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsViewModel$refresh$1$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DataAndStorageSettingsViewModel$refresh$1.m650invoke$lambda0(DataAndStorageSettingsViewModel.this, this.f$1, (DataAndStorageSettingsState) obj);
            }
        });
    }
}
