package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Metadata;

/* compiled from: ConversationSettingsFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"REQUEST_CODE_ADD_CONTACT", "", "REQUEST_CODE_ADD_MEMBERS_TO_GROUP", "REQUEST_CODE_RETURN_FROM_MEDIA", "REQUEST_CODE_VIEW_CONTACT", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationSettingsFragmentKt {
    private static final int REQUEST_CODE_ADD_CONTACT;
    private static final int REQUEST_CODE_ADD_MEMBERS_TO_GROUP;
    private static final int REQUEST_CODE_RETURN_FROM_MEDIA;
    private static final int REQUEST_CODE_VIEW_CONTACT;
}
