package org.thoughtcrime.securesms.components.settings;

import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/ExternalLinkPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/ExternalLinkPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExternalLinkPreferenceViewHolder extends PreferenceViewHolder<ExternalLinkPreference> {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ExternalLinkPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
    }

    public void bind(ExternalLinkPreference externalLinkPreference) {
        Intrinsics.checkNotNullParameter(externalLinkPreference, "model");
        super.bind((ExternalLinkPreferenceViewHolder) externalLinkPreference);
        Drawable drawable = ContextCompat.getDrawable(this.context, R.drawable.ic_open_20);
        if (drawable != null) {
            Intrinsics.checkNotNullExpressionValue(drawable, "requireNotNull(ContextCo…, R.drawable.ic_open_20))");
            drawable.setBounds(0, 0, ViewUtil.dpToPx(20), ViewUtil.dpToPx(20));
            if (ViewUtil.isLtr(this.itemView)) {
                getTitleView().setCompoundDrawables(null, null, drawable, null);
            } else {
                getTitleView().setCompoundDrawables(drawable, null, null, null);
            }
            this.itemView.setOnClickListener(new View.OnClickListener(externalLinkPreference) { // from class: org.thoughtcrime.securesms.components.settings.ExternalLinkPreferenceViewHolder$$ExternalSyntheticLambda0
                public final /* synthetic */ ExternalLinkPreference f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    ExternalLinkPreferenceViewHolder.$r8$lambda$ITAsdn8HMROYIeDxJMbFXQpqLOQ(ExternalLinkPreferenceViewHolder.this, this.f$1, view);
                }
            });
            return;
        }
        throw new IllegalArgumentException("Required value was null.".toString());
    }

    /* renamed from: bind$lambda-0 */
    public static final void m526bind$lambda0(ExternalLinkPreferenceViewHolder externalLinkPreferenceViewHolder, ExternalLinkPreference externalLinkPreference, View view) {
        Intrinsics.checkNotNullParameter(externalLinkPreferenceViewHolder, "this$0");
        Intrinsics.checkNotNullParameter(externalLinkPreference, "$model");
        CommunicationActions.openBrowserLink(externalLinkPreferenceViewHolder.itemView.getContext(), externalLinkPreferenceViewHolder.itemView.getContext().getString(externalLinkPreference.getLinkId()));
    }
}
