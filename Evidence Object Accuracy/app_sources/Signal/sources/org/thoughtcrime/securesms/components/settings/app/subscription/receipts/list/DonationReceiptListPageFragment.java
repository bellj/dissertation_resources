package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import android.os.Bundle;
import android.view.View;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.TextPreference;
import org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListItem;
import org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageViewModel;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: DonationReceiptListPageFragment.kt */
@Metadata(d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 !2\u00020\u0001:\u0001!B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0014\u001a\u0004\u0018\u00010\u00152\u0006\u0010\u0016\u001a\u00020\u00172\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H\u0002J\u001a\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u0016\u0010\u000b\u001a\u0004\u0018\u00010\f8BX\u0004¢\u0006\u0006\u001a\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u00020\u00108BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\n\u001a\u0004\b\u0011\u0010\u0012¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageFragment;", "Landroidx/fragment/app/Fragment;", "()V", "emptyStateGroup", "Landroidx/constraintlayout/widget/Group;", "sharedViewModel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListViewModel;", "getSharedViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListViewModel;", "sharedViewModel$delegate", "Lkotlin/Lazy;", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "getType", "()Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageViewModel;", "viewModel$delegate", "getBadgeForRecord", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "record", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "badges", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptBadge;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListPageFragment extends Fragment {
    private static final String ARG_TYPE;
    public static final Companion Companion = new Companion(null);
    private Group emptyStateGroup;
    private final Lazy sharedViewModel$delegate;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(DonationReceiptListPageViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$viewModel$2
        final /* synthetic */ DonationReceiptListPageFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new DonationReceiptListPageViewModel.Factory(this.this$0.getType(), new DonationReceiptListPageRepository());
        }
    });

    /* compiled from: DonationReceiptListPageFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[DonationReceiptRecord.Type.values().length];
            iArr[DonationReceiptRecord.Type.BOOST.ordinal()] = 1;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public DonationReceiptListPageFragment() {
        super(R.layout.donation_receipt_list_page_fragment);
        DonationReceiptListPageFragment$sharedViewModel$2 donationReceiptListPageFragment$sharedViewModel$2 = new Function0<ViewModelStoreOwner>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$sharedViewModel$2
            final /* synthetic */ DonationReceiptListPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStoreOwner invoke() {
                Fragment requireParentFragment = this.this$0.requireParentFragment();
                Intrinsics.checkNotNullExpressionValue(requireParentFragment, "requireParentFragment()");
                return requireParentFragment;
            }
        };
        this.sharedViewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(DonationReceiptListViewModel.class), new Function0<ViewModelStore>(donationReceiptListPageFragment$sharedViewModel$2) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$special$$inlined$viewModels$1
            final /* synthetic */ Function0 $ownerProducer;

            {
                this.$ownerProducer = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final ViewModelStore invoke() {
                ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
                Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
                return viewModelStore;
            }
        }, DonationReceiptListPageFragment$sharedViewModel$3.INSTANCE);
    }

    private final DonationReceiptListPageViewModel getViewModel() {
        return (DonationReceiptListPageViewModel) this.viewModel$delegate.getValue();
    }

    private final DonationReceiptListViewModel getSharedViewModel() {
        return (DonationReceiptListViewModel) this.sharedViewModel$delegate.getValue();
    }

    public final DonationReceiptRecord.Type getType() {
        String string = requireArguments().getString(ARG_TYPE);
        if (string != null) {
            return DonationReceiptRecord.Type.Companion.fromCode(string);
        }
        return null;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        DonationReceiptListAdapter donationReceiptListAdapter = new DonationReceiptListAdapter(new Function1<DonationReceiptListItem.Model, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$onViewCreated$adapter$1
            final /* synthetic */ DonationReceiptListPageFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DonationReceiptListItem.Model model) {
                invoke(model);
                return Unit.INSTANCE;
            }

            public final void invoke(DonationReceiptListItem.Model model) {
                Intrinsics.checkNotNullParameter(model, "model");
                NavController findNavController = FragmentKt.findNavController(this.this$0);
                DonationReceiptListFragmentDirections.ActionDonationReceiptListFragmentToDonationReceiptDetailFragment actionDonationReceiptListFragmentToDonationReceiptDetailFragment = DonationReceiptListFragmentDirections.actionDonationReceiptListFragmentToDonationReceiptDetailFragment(model.getRecord().getId());
                Intrinsics.checkNotNullExpressionValue(actionDonationReceiptListFragmentToDonationReceiptDetailFragment, "actionDonationReceiptLis…Fragment(model.record.id)");
                SafeNavigation.safeNavigate(findNavController, actionDonationReceiptListFragmentToDonationReceiptDetailFragment);
            }
        });
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setAdapter(donationReceiptListAdapter);
        recyclerView.addItemDecoration(new StickyHeaderDecoration(donationReceiptListAdapter, false, true, 0));
        View findViewById = view.findViewById(R.id.empty_state);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.empty_state)");
        this.emptyStateGroup = (Group) findViewById;
        LiveDataUtil.combineLatest(getViewModel().getState(), getSharedViewModel().getState(), new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return DonationReceiptListPageFragment.m1024onViewCreated$lambda3(DonationReceiptListPageFragment.this, (DonationReceiptListPageState) obj, (List) obj2);
            }
        }).observe(getViewLifecycleOwner(), new Observer(donationReceiptListAdapter) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ DonationReceiptListAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                DonationReceiptListPageFragment.m1025onViewCreated$lambda4(DonationReceiptListPageFragment.this, this.f$1, (Pair) obj);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final Pair m1024onViewCreated$lambda3(DonationReceiptListPageFragment donationReceiptListPageFragment, DonationReceiptListPageState donationReceiptListPageState, List list) {
        Intrinsics.checkNotNullParameter(donationReceiptListPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(donationReceiptListPageState, "state");
        Intrinsics.checkNotNullParameter(list, "badges");
        Boolean valueOf = Boolean.valueOf(donationReceiptListPageState.isLoaded());
        List<DonationReceiptRecord> records = donationReceiptListPageState.getRecords();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(records, 10));
        for (DonationReceiptRecord donationReceiptRecord : records) {
            arrayList.add(new DonationReceiptListItem.Model(donationReceiptRecord, donationReceiptListPageFragment.getBadgeForRecord(donationReceiptRecord, list)));
        }
        return TuplesKt.to(valueOf, arrayList);
    }

    /* renamed from: onViewCreated$lambda-4 */
    public static final void m1025onViewCreated$lambda4(DonationReceiptListPageFragment donationReceiptListPageFragment, DonationReceiptListAdapter donationReceiptListAdapter, Pair pair) {
        Intrinsics.checkNotNullParameter(donationReceiptListPageFragment, "this$0");
        Intrinsics.checkNotNullParameter(donationReceiptListAdapter, "$adapter");
        boolean booleanValue = ((Boolean) pair.component1()).booleanValue();
        List list = (List) pair.component2();
        Group group = null;
        if (!list.isEmpty()) {
            Group group2 = donationReceiptListPageFragment.emptyStateGroup;
            if (group2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("emptyStateGroup");
                group2 = null;
            }
            ViewExtensionsKt.setVisible(group2, false);
            donationReceiptListAdapter.submitList(CollectionsKt___CollectionsKt.plus((Collection<? extends TextPreference>) ((Collection<? extends Object>) list), new TextPreference(null, DSLSettingsText.Companion.from(R.string.DonationReceiptListFragment__if_you_have, new DSLSettingsText.TextAppearanceModifier(R.style.TextAppearance_Signal_Subtitle)))));
            return;
        }
        Group group3 = donationReceiptListPageFragment.emptyStateGroup;
        if (group3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("emptyStateGroup");
        } else {
            group = group3;
        }
        ViewExtensionsKt.setVisible(group, booleanValue);
    }

    private final Badge getBadgeForRecord(DonationReceiptRecord donationReceiptRecord, List<DonationReceiptBadge> list) {
        Object obj;
        boolean z;
        Object obj2;
        boolean z2;
        if (WhenMappings.$EnumSwitchMapping$0[donationReceiptRecord.getType().ordinal()] == 1) {
            Iterator<T> it = list.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj2 = null;
                    break;
                }
                obj2 = it.next();
                if (((DonationReceiptBadge) obj2).getType() == DonationReceiptRecord.Type.BOOST) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            DonationReceiptBadge donationReceiptBadge = (DonationReceiptBadge) obj2;
            if (donationReceiptBadge != null) {
                return donationReceiptBadge.getBadge();
            }
            return null;
        }
        Iterator<T> it2 = list.iterator();
        while (true) {
            if (!it2.hasNext()) {
                obj = null;
                break;
            }
            obj = it2.next();
            if (((DonationReceiptBadge) obj).getLevel() == donationReceiptRecord.getSubscriptionLevel()) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        DonationReceiptBadge donationReceiptBadge2 = (DonationReceiptBadge) obj;
        if (donationReceiptBadge2 != null) {
            return donationReceiptBadge2.getBadge();
        }
        return null;
    }

    /* compiled from: DonationReceiptListPageFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageFragment$Companion;", "", "()V", "ARG_TYPE", "", "create", "Landroidx/fragment/app/Fragment;", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final Fragment create(DonationReceiptRecord.Type type) {
            DonationReceiptListPageFragment donationReceiptListPageFragment = new DonationReceiptListPageFragment();
            Bundle bundle = new Bundle();
            bundle.putString(DonationReceiptListPageFragment.ARG_TYPE, type != null ? type.getCode() : null);
            donationReceiptListPageFragment.setArguments(bundle);
            return donationReceiptListPageFragment;
        }
    }
}
