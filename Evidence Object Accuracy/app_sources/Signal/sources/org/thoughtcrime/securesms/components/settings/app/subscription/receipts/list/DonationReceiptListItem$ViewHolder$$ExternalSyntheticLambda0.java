package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListItem;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DonationReceiptListItem$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ DonationReceiptListItem.ViewHolder f$0;
    public final /* synthetic */ DonationReceiptListItem.Model f$1;

    public /* synthetic */ DonationReceiptListItem$ViewHolder$$ExternalSyntheticLambda0(DonationReceiptListItem.ViewHolder viewHolder, DonationReceiptListItem.Model model) {
        this.f$0 = viewHolder;
        this.f$1 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        DonationReceiptListItem.ViewHolder.m1022bind$lambda0(this.f$0, this.f$1, view);
    }
}
