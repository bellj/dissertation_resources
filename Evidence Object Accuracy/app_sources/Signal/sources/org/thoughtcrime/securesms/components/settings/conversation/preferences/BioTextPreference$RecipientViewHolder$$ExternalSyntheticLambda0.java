package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class BioTextPreference$RecipientViewHolder$$ExternalSyntheticLambda0 implements View.OnLongClickListener {
    public final /* synthetic */ BioTextPreference.RecipientViewHolder f$0;

    public /* synthetic */ BioTextPreference$RecipientViewHolder$$ExternalSyntheticLambda0(BioTextPreference.RecipientViewHolder recipientViewHolder) {
        this.f$0 = recipientViewHolder;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return BioTextPreference.RecipientViewHolder.m1195bind$lambda0(this.f$0, view);
    }
}
