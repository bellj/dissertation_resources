package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Pair;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda8 implements Store.Action {
    public final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel f$0;

    public /* synthetic */ ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda8(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel) {
        this.f$0 = groupSettingsViewModel;
    }

    @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
    public final Object apply(Object obj, Object obj2) {
        return ConversationSettingsViewModel.GroupSettingsViewModel.m1137_init_$lambda3(this.f$0, (Pair) obj, (ConversationSettingsState) obj2);
    }
}
