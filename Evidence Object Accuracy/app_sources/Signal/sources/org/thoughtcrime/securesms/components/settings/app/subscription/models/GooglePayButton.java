package org.thoughtcrime.securesms.components.settings.app.subscription.models;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: GooglePayButton.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/models/GooglePayButton;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GooglePayButton {
    public static final GooglePayButton INSTANCE = new GooglePayButton();

    private GooglePayButton() {
    }

    /* compiled from: GooglePayButton.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0000H\u0016R\u0014\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\bR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/models/GooglePayButton$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "onClick", "Lkotlin/Function0;", "", "isEnabled", "", "(Lkotlin/jvm/functions/Function0;Z)V", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "areItemsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final boolean isEnabled;
        private final Function0<Unit> onClick;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Function0<Unit> function0, boolean z) {
            super(null, null, null, null, z, 15, null);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.onClick = function0;
            this.isEnabled = z;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public boolean isEnabled() {
            return this.isEnabled;
        }
    }

    /* compiled from: GooglePayButton.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/models/GooglePayButton$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/models/GooglePayButton$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "googlePayButton", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final View googlePayButton;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.googlepay_button);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.googlepay_button)");
            this.googlePayButton = findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.googlePayButton.setEnabled(model.isEnabled());
            this.googlePayButton.setOnClickListener(new GooglePayButton$ViewHolder$$ExternalSyntheticLambda0(this, model));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m990bind$lambda0(ViewHolder viewHolder, Model model, View view) {
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            Intrinsics.checkNotNullParameter(model, "$model");
            viewHolder.googlePayButton.setEnabled(false);
            model.getOnClick().invoke();
        }
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m989register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.models.GooglePayButton$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return GooglePayButton.m989register$lambda0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.google_pay_button_pref));
    }
}
