package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import j$.util.function.Function;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.PreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileAddMembers;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: NotificationProfileAddMembers.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileAddMembers;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileAddMembers {
    public static final NotificationProfileAddMembers INSTANCE = new NotificationProfileAddMembers();

    private NotificationProfileAddMembers() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileAddMembers$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new NotificationProfileAddMembers.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.large_icon_preference_item));
    }

    /* compiled from: NotificationProfileAddMembers.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BO\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u001e\u0010\u0006\u001a\u001a\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t\u0012\u0004\u0012\u00020\u000b0\u0007\u0012\u0006\u0010\f\u001a\u00020\b\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0002\u0010\u000eJ\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0000H\u0016R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R)\u0010\u0006\u001a\u001a\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t\u0012\u0004\u0012\u00020\u000b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\f\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileAddMembers$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "onClick", "Lkotlin/Function2;", "", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "", "profileId", "currentSelection", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Lkotlin/jvm/functions/Function2;JLjava/util/Set;)V", "getCurrentSelection", "()Ljava/util/Set;", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "getOnClick", "()Lkotlin/jvm/functions/Function2;", "getProfileId", "()J", "getTitle", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "areContentsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Set<RecipientId> currentSelection;
        private final DSLSettingsIcon icon;
        private final Function2<Long, Set<? extends RecipientId>, Unit> onClick;
        private final long profileId;
        private final DSLSettingsText title;

        public /* synthetic */ Model(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, Function2 function2, long j, Set set, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? DSLSettingsText.Companion.from(R.string.AddAllowedMembers__add_people_or_groups, new DSLSettingsText.Modifier[0]) : dSLSettingsText, (i & 2) != 0 ? DSLSettingsIcon.Companion.from(R.drawable.add_to_a_group, -1) : dSLSettingsIcon, function2, j, set);
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsText getTitle() {
            return this.title;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsIcon getIcon() {
            return this.icon;
        }

        public final Function2<Long, Set<? extends RecipientId>, Unit> getOnClick() {
            return this.onClick;
        }

        public final long getProfileId() {
            return this.profileId;
        }

        public final Set<RecipientId> getCurrentSelection() {
            return this.currentSelection;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: kotlin.jvm.functions.Function2<? super java.lang.Long, ? super java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId>, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r15v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, Function2<? super Long, ? super Set<? extends RecipientId>, Unit> function2, long j, Set<? extends RecipientId> set) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(dSLSettingsIcon, "icon");
            Intrinsics.checkNotNullParameter(function2, "onClick");
            Intrinsics.checkNotNullParameter(set, "currentSelection");
            this.title = dSLSettingsText;
            this.icon = dSLSettingsIcon;
            this.onClick = function2;
            this.profileId = j;
            this.currentSelection = set;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && this.profileId == model.profileId && Intrinsics.areEqual(this.currentSelection, model.currentSelection);
        }
    }

    /* compiled from: NotificationProfileAddMembers.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileAddMembers$ViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileAddMembers$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends PreferenceViewHolder<Model> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            super.bind((ViewHolder) model);
            this.itemView.setOnClickListener(new NotificationProfileAddMembers$ViewHolder$$ExternalSyntheticLambda0(model));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m813bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke(Long.valueOf(model.getProfileId()), model.getCurrentSelection());
        }
    }
}
