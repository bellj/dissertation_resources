package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;

/* loaded from: classes4.dex */
public class BoostFragmentDirections {
    private BoostFragmentDirections() {
    }

    public static ActionBoostFragmentToSetDonationCurrencyFragment actionBoostFragmentToSetDonationCurrencyFragment(boolean z, String[] strArr) {
        return new ActionBoostFragmentToSetDonationCurrencyFragment(z, strArr);
    }

    public static ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog(Badge badge) {
        return new ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog(badge);
    }

    /* loaded from: classes4.dex */
    public static class ActionBoostFragmentToSetDonationCurrencyFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_boostFragment_to_setDonationCurrencyFragment;
        }

        private ActionBoostFragmentToSetDonationCurrencyFragment(boolean z, String[] strArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("isBoost", Boolean.valueOf(z));
            if (strArr != null) {
                hashMap.put("supportedCurrencyCodes", strArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        public ActionBoostFragmentToSetDonationCurrencyFragment setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        public ActionBoostFragmentToSetDonationCurrencyFragment setSupportedCurrencyCodes(String[] strArr) {
            if (strArr != null) {
                this.arguments.put("supportedCurrencyCodes", strArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("isBoost")) {
                bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
            }
            if (this.arguments.containsKey("supportedCurrencyCodes")) {
                bundle.putStringArray("supportedCurrencyCodes", (String[]) this.arguments.get("supportedCurrencyCodes"));
            }
            return bundle;
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }

        public String[] getSupportedCurrencyCodes() {
            return (String[]) this.arguments.get("supportedCurrencyCodes");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionBoostFragmentToSetDonationCurrencyFragment actionBoostFragmentToSetDonationCurrencyFragment = (ActionBoostFragmentToSetDonationCurrencyFragment) obj;
            if (this.arguments.containsKey("isBoost") != actionBoostFragmentToSetDonationCurrencyFragment.arguments.containsKey("isBoost") || getIsBoost() != actionBoostFragmentToSetDonationCurrencyFragment.getIsBoost() || this.arguments.containsKey("supportedCurrencyCodes") != actionBoostFragmentToSetDonationCurrencyFragment.arguments.containsKey("supportedCurrencyCodes")) {
                return false;
            }
            if (getSupportedCurrencyCodes() == null ? actionBoostFragmentToSetDonationCurrencyFragment.getSupportedCurrencyCodes() == null : getSupportedCurrencyCodes().equals(actionBoostFragmentToSetDonationCurrencyFragment.getSupportedCurrencyCodes())) {
                return getActionId() == actionBoostFragmentToSetDonationCurrencyFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getIsBoost() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getSupportedCurrencyCodes())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionBoostFragmentToSetDonationCurrencyFragment(actionId=" + getActionId() + "){isBoost=" + getIsBoost() + ", supportedCurrencyCodes=" + getSupportedCurrencyCodes() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_boostFragment_to_boostThanksForYourSupportBottomSheetDialog;
        }

        private ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog(Badge badge) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (badge != null) {
                hashMap.put("badge", badge);
                return;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog setBadge(Badge badge) {
            if (badge != null) {
                this.arguments.put("badge", badge);
                return this;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("badge")) {
                Badge badge = (Badge) this.arguments.get("badge");
                if (Parcelable.class.isAssignableFrom(Badge.class) || badge == null) {
                    bundle.putParcelable("badge", (Parcelable) Parcelable.class.cast(badge));
                } else if (Serializable.class.isAssignableFrom(Badge.class)) {
                    bundle.putSerializable("badge", (Serializable) Serializable.class.cast(badge));
                } else {
                    throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("isBoost")) {
                bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
            } else {
                bundle.putBoolean("isBoost", false);
            }
            return bundle;
        }

        public Badge getBadge() {
            return (Badge) this.arguments.get("badge");
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog = (ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog) obj;
            if (this.arguments.containsKey("badge") != actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog.arguments.containsKey("badge")) {
                return false;
            }
            if (getBadge() == null ? actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog.getBadge() == null : getBadge().equals(actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog.getBadge())) {
                return this.arguments.containsKey("isBoost") == actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog.arguments.containsKey("isBoost") && getIsBoost() == actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog.getIsBoost() && getActionId() == actionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getBadge() != null ? getBadge().hashCode() : 0) + 31) * 31) + (getIsBoost() ? 1 : 0)) * 31) + getActionId();
        }

        public String toString() {
            return "ActionBoostFragmentToBoostThanksForYourSupportBottomSheetDialog(actionId=" + getActionId() + "){badge=" + getBadge() + ", isBoost=" + getIsBoost() + "}";
        }
    }
}
