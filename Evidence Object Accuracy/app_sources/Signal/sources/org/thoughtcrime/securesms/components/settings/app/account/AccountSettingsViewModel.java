package org.thoughtcrime.securesms.components.settings.app.account;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: AccountSettingsViewModel.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u00020\u0005H\u0002J\u0006\u0010\u000b\u001a\u00020\fR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getCurrentState", "refreshState", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AccountSettingsViewModel extends ViewModel {
    private final LiveData<AccountSettingsState> state;
    private final Store<AccountSettingsState> store;

    public AccountSettingsViewModel() {
        Store<AccountSettingsState> store = new Store<>(getCurrentState());
        this.store = store;
        LiveData<AccountSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<AccountSettingsState> getState() {
        return this.state;
    }

    /* renamed from: refreshState$lambda-0 */
    public static final AccountSettingsState m570refreshState$lambda0(AccountSettingsViewModel accountSettingsViewModel, AccountSettingsState accountSettingsState) {
        Intrinsics.checkNotNullParameter(accountSettingsViewModel, "this$0");
        return accountSettingsViewModel.getCurrentState();
    }

    public final void refreshState() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AccountSettingsViewModel.m570refreshState$lambda0(AccountSettingsViewModel.this, (AccountSettingsState) obj);
            }
        });
    }

    private final AccountSettingsState getCurrentState() {
        return new AccountSettingsState(SignalStore.kbsValues().hasPin() && !SignalStore.kbsValues().hasOptedOut(), SignalStore.pinValues().arePinRemindersEnabled(), SignalStore.kbsValues().isV2RegistrationLockEnabled());
    }
}
