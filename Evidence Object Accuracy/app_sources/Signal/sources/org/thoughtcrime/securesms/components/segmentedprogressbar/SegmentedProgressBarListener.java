package org.thoughtcrime.securesms.components.segmentedprogressbar;

import kotlin.Metadata;

/* compiled from: SegmentedProgressBarListener.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0018\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H&J\u000f\u0010\b\u001a\u0004\u0018\u00010\tH&¢\u0006\u0002\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBarListener;", "", "onFinished", "", "onPage", "oldPageIndex", "", "newPageIndex", "onRequestSegmentProgressPercentage", "", "()Ljava/lang/Float;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public interface SegmentedProgressBarListener {
    void onFinished();

    void onPage(int i, int i2);

    Float onRequestSegmentProgressPercentage();
}
