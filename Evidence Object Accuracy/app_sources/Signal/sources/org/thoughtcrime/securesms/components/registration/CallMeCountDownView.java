package org.thoughtcrime.securesms.components.registration;

import android.content.Context;
import android.util.AttributeSet;
import com.google.android.material.button.MaterialButton;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class CallMeCountDownView extends MaterialButton {
    private long countDownToTime;
    private Listener listener;

    /* loaded from: classes4.dex */
    public interface Listener {
        void onRemaining(CallMeCountDownView callMeCountDownView, int i);
    }

    public CallMeCountDownView(Context context) {
        super(context);
    }

    public CallMeCountDownView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CallMeCountDownView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void startCountDownTo(long j) {
        if (j > 0) {
            this.countDownToTime = j;
            updateCountDown();
        }
    }

    public void setCallEnabled() {
        setText(R.string.RegistrationActivity_call);
        setEnabled(true);
        setAlpha(1.0f);
    }

    public void updateCountDown() {
        long currentTimeMillis = this.countDownToTime - System.currentTimeMillis();
        if (currentTimeMillis > 0) {
            setEnabled(false);
            setAlpha(0.5f);
            int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(currentTimeMillis);
            setText(getResources().getString(R.string.RegistrationActivity_call_me_instead_available_in, Integer.valueOf(seconds / 60), Integer.valueOf(seconds % 60)));
            Listener listener = this.listener;
            if (listener != null) {
                listener.onRemaining(this, seconds);
            }
            postDelayed(new Runnable() { // from class: org.thoughtcrime.securesms.components.registration.CallMeCountDownView$$ExternalSyntheticLambda0
                @Override // java.lang.Runnable
                public final void run() {
                    CallMeCountDownView.$r8$lambda$MTaheCJDtC09EUBEwD93CuBIxiw(CallMeCountDownView.this);
                }
            }, 250);
            return;
        }
        setCallEnabled();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
