package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.FunctionReferenceImpl;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ConversationSettingsRepository.kt */
/* access modifiers changed from: package-private */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public /* synthetic */ class ConversationSettingsRepository$getGroupsInCommon$1$2 extends FunctionReferenceImpl implements Function1<GroupDatabase.GroupRecord, RecipientId> {
    public static final ConversationSettingsRepository$getGroupsInCommon$1$2 INSTANCE = new ConversationSettingsRepository$getGroupsInCommon$1$2();

    ConversationSettingsRepository$getGroupsInCommon$1$2() {
        super(1, GroupDatabase.GroupRecord.class, "getRecipientId", "getRecipientId()Lorg/thoughtcrime/securesms/recipients/RecipientId;", 0);
    }

    public final RecipientId invoke(GroupDatabase.GroupRecord groupRecord) {
        Intrinsics.checkNotNullParameter(groupRecord, "p0");
        return groupRecord.getRecipientId();
    }
}
