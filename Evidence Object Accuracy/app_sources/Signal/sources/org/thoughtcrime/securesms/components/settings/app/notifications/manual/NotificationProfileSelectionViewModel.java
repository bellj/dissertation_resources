package org.thoughtcrime.securesms.components.settings.app.notifications.manual;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import j$.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: NotificationProfileSelectionViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 \u001f2\u00020\u0001:\u0002\u001f B\u000f\u0012\u0006\u0010\r\u001a\u00020\f¢\u0006\u0004\b\u001d\u0010\u001eJ\b\u0010\u0003\u001a\u00020\u0002H\u0014J\u000e\u0010\u0006\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004J\u000e\u0010\b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0004J\u000e\u0010\t\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u0004J\u0016\u0010\u000b\u001a\u00020\u00022\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\nR\u0014\u0010\r\u001a\u00020\f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\r\u0010\u000eR\u001a\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0011\u0010\u0012R\u001d\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00100\u00138\u0006¢\u0006\f\n\u0004\b\u0014\u0010\u0015\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0019\u001a\u00020\u00188\u0006¢\u0006\f\n\u0004\b\u0019\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionViewModel;", "Landroidx/lifecycle/ViewModel;", "", "onCleared", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "notificationProfile", "setExpanded", "profile", "toggleEnabled", "enableForOneHour", "j$/time/LocalDateTime", "enableUntil", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionState;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "Landroidx/lifecycle/LiveData;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "getDisposables", "()Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "<init>", "(Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfileSelectionViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private final CompositeDisposable disposables;
    private final NotificationProfilesRepository repository;
    private final LiveData<NotificationProfileSelectionState> state;
    private final Store<NotificationProfileSelectionState> store;

    public NotificationProfileSelectionViewModel(NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "repository");
        this.repository = notificationProfilesRepository;
        Store<NotificationProfileSelectionState> store = new Store<>(new NotificationProfileSelectionState(null, 0, Companion.getTimeSlotB(), 3, null));
        this.store = store;
        LiveData<NotificationProfileSelectionState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(notificationProfilesRepository.getProfiles(), (Function1) null, (Function0) null, new Function1<List<? extends NotificationProfile>, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel.1
            final /* synthetic */ NotificationProfileSelectionViewModel this$0;

            {
                this.this$0 = r1;
            }

            /* renamed from: invoke$lambda-0 */
            public static final NotificationProfileSelectionState m720invoke$lambda0(List list, NotificationProfileSelectionState notificationProfileSelectionState) {
                Intrinsics.checkNotNullParameter(list, "$profiles");
                Intrinsics.checkNotNullExpressionValue(notificationProfileSelectionState, "it");
                return NotificationProfileSelectionState.copy$default(notificationProfileSelectionState, list, 0, null, 6, null);
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(List<? extends NotificationProfile> list) {
                invoke((List<NotificationProfile>) list);
                return Unit.INSTANCE;
            }

            public final void invoke(List<NotificationProfile> list) {
                Intrinsics.checkNotNullParameter(list, "profiles");
                this.this$0.store.update(new NotificationProfileSelectionViewModel$1$$ExternalSyntheticLambda0(list));
            }
        }, 3, (Object) null));
        Disposable subscribe = Observable.interval(0, 1, TimeUnit.MINUTES).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileSelectionViewModel.m716_init_$lambda0((Long) obj);
            }
        }).distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                NotificationProfileSelectionViewModel.m717_init_$lambda2(NotificationProfileSelectionViewModel.this, (LocalDateTime) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "interval(0, 1, TimeUnit.…otB = calendar) }\n      }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final LiveData<NotificationProfileSelectionState> getState() {
        return this.state;
    }

    public final CompositeDisposable getDisposables() {
        return this.disposables;
    }

    /* renamed from: _init_$lambda-0 */
    public static final LocalDateTime m716_init_$lambda0(Long l) {
        return Companion.getTimeSlotB();
    }

    /* renamed from: _init_$lambda-2 */
    public static final void m717_init_$lambda2(NotificationProfileSelectionViewModel notificationProfileSelectionViewModel, LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(notificationProfileSelectionViewModel, "this$0");
        notificationProfileSelectionViewModel.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationProfileSelectionViewModel.m718lambda2$lambda1(LocalDateTime.this, (NotificationProfileSelectionState) obj);
            }
        });
    }

    /* renamed from: lambda-2$lambda-1 */
    public static final NotificationProfileSelectionState m718lambda2$lambda1(LocalDateTime localDateTime, NotificationProfileSelectionState notificationProfileSelectionState) {
        Intrinsics.checkNotNullExpressionValue(notificationProfileSelectionState, "it");
        Intrinsics.checkNotNullExpressionValue(localDateTime, "calendar");
        return NotificationProfileSelectionState.copy$default(notificationProfileSelectionState, null, 0, localDateTime, 3, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final void setExpanded(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "notificationProfile");
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationProfileSelectionViewModel.m719setExpanded$lambda3(NotificationProfile.this, (NotificationProfileSelectionState) obj);
            }
        });
    }

    /* renamed from: setExpanded$lambda-3 */
    public static final NotificationProfileSelectionState m719setExpanded$lambda3(NotificationProfile notificationProfile, NotificationProfileSelectionState notificationProfileSelectionState) {
        Intrinsics.checkNotNullParameter(notificationProfile, "$notificationProfile");
        Intrinsics.checkNotNullExpressionValue(notificationProfileSelectionState, "it");
        return NotificationProfileSelectionState.copy$default(notificationProfileSelectionState, null, notificationProfileSelectionState.getExpandedId() == notificationProfile.getId() ? -1 : notificationProfile.getId(), null, 5, null);
    }

    public final void toggleEnabled(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = NotificationProfilesRepository.manuallyToggleProfile$default(this.repository, notificationProfile, 0, 2, null).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.manuallyToggl…ofile)\n      .subscribe()");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final void enableForOneHour(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = NotificationProfilesRepository.manuallyEnableProfileForDuration$default(this.repository, notificationProfile.getId(), System.currentTimeMillis() + TimeUnit.HOURS.toMillis(1), 0, 4, null).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.manuallyEnabl…is(1))\n      .subscribe()");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final void enableUntil(NotificationProfile notificationProfile, LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        Intrinsics.checkNotNullParameter(localDateTime, "enableUntil");
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = NotificationProfilesRepository.manuallyEnableProfileForDuration$default(this.repository, notificationProfile.getId(), JavaTimeExtensionsKt.toMillis$default(localDateTime, null, 1, null), 0, 4, null).subscribe();
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.manuallyEnabl…lis())\n      .subscribe()");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* compiled from: NotificationProfileSelectionViewModel.kt */
    @Metadata(bv = {}, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\b\u0003\u0018\u00002\u00020\u0001B\t\b\u0002¢\u0006\u0004\b\u0004\u0010\u0005J\b\u0010\u0003\u001a\u00020\u0002H\u0002¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionViewModel$Companion;", "", "j$/time/LocalDateTime", "getTimeSlotB", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final LocalDateTime getTimeSlotB() {
            LocalDateTime now = LocalDateTime.now();
            LocalDateTime withSecond = now.withHour(18).withMinute(0).withSecond(0);
            LocalDateTime withSecond2 = now.withHour(8).withMinute(0).withSecond(0);
            Intrinsics.checkNotNullExpressionValue(now, "now");
            Intrinsics.checkNotNullExpressionValue(withSecond2, "eightAm");
            Intrinsics.checkNotNullExpressionValue(withSecond, "sixPm");
            if (JavaTimeExtensionsKt.isBetween(now, withSecond2, withSecond)) {
                return withSecond;
            }
            if (now.isBefore(withSecond2)) {
                return withSecond2;
            }
            LocalDateTime plusDays = withSecond2.plusDays(1);
            Intrinsics.checkNotNullExpressionValue(plusDays, "{\n        eightAm.plusDays(1)\n      }");
            return plusDays;
        }
    }

    /* compiled from: NotificationProfileSelectionViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "notificationProfilesRepository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final NotificationProfilesRepository notificationProfilesRepository;

        public Factory(NotificationProfilesRepository notificationProfilesRepository) {
            Intrinsics.checkNotNullParameter(notificationProfilesRepository, "notificationProfilesRepository");
            this.notificationProfilesRepository = notificationProfilesRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new NotificationProfileSelectionViewModel(this.notificationProfilesRepository));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
