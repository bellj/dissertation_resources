package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.timepicker.MaterialTimePicker;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import j$.time.DayOfWeek;
import j$.time.LocalTime;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleViewModel;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: EditNotificationProfileScheduleFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0007¢\u0006\u0004\b\u001f\u0010 J\u0018\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0003\u001a\u00020\u00022\u0006\u0010\u0005\u001a\u00020\u0004H\u0002J\u001a\u0010\f\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\b2\b\u0010\u000b\u001a\u0004\u0018\u00010\nH\u0016R\u001b\u0010\u0012\u001a\u00020\r8BX\u0002¢\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0014\u001a\u00020\u00138\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0014\u0010\u0015R\u001b\u0010\u001a\u001a\u00020\u00168BX\u0002¢\u0006\f\n\u0004\b\u0017\u0010\u000f\u001a\u0004\b\u0018\u0010\u0019R\u001b\u0010\u001e\u001a\u00020\u00028BX\u0002¢\u0006\f\n\u0004\b\u001b\u0010\u000f\u001a\u0004\b\u001c\u0010\u001d¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "", "isStart", "j$/time/LocalTime", "time", "", "showTimeSelector", "Landroid/view/View;", "view", "Landroid/os/Bundle;", "savedInstanceState", "onViewCreated", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleViewModel;", "viewModel", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "", "profileId$delegate", "getProfileId", "()J", "profileId", "createMode$delegate", "getCreateMode", "()Z", "createMode", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EditNotificationProfileScheduleFragment extends LoggingFragment {
    private final Lazy createMode$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Boolean>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$createMode$2
        final /* synthetic */ EditNotificationProfileScheduleFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Boolean invoke() {
            return Boolean.valueOf(EditNotificationProfileScheduleFragmentArgs.fromBundle(this.this$0.requireArguments()).getCreateMode());
        }
    });
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy profileId$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Long>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$profileId$2
        final /* synthetic */ EditNotificationProfileScheduleFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Long invoke() {
            return Long.valueOf(EditNotificationProfileScheduleFragmentArgs.fromBundle(this.this$0.requireArguments()).getProfileId());
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(EditNotificationProfileScheduleViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$viewModel$2
        final /* synthetic */ EditNotificationProfileScheduleFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return new EditNotificationProfileScheduleViewModel.Factory(EditNotificationProfileScheduleFragment.access$getProfileId(this.this$0), EditNotificationProfileScheduleFragment.access$getCreateMode(this.this$0));
        }
    });

    public EditNotificationProfileScheduleFragment() {
        super(R.layout.fragment_edit_notification_profile_schedule);
    }

    private final EditNotificationProfileScheduleViewModel getViewModel() {
        return (EditNotificationProfileScheduleViewModel) this.viewModel$delegate.getValue();
    }

    public final long getProfileId() {
        return ((Number) this.profileId$delegate.getValue()).longValue();
    }

    public final boolean getCreateMode() {
        return ((Boolean) this.createMode$delegate.getValue()).booleanValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileScheduleFragment.$r8$lambda$3MIxVTRHWp7OXuVTbphb56n4cTw(EditNotificationProfileScheduleFragment.this, view2);
            }
        });
        View findViewById2 = view.findViewById(R.id.edit_notification_profile_schedule_title);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.e…n_profile_schedule_title)");
        View findViewById3 = view.findViewById(R.id.edit_notification_profile_schedule_description);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.e…ile_schedule_description)");
        toolbar.setTitle(!getCreateMode() ? getString(R.string.EditNotificationProfileSchedule__schedule) : null);
        ViewExtensionsKt.setVisible(findViewById2, getCreateMode());
        ViewExtensionsKt.setVisible(findViewById3, getCreateMode());
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        View findViewById4 = view.findViewById(R.id.edit_notification_profile_schedule_switch);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.e…_profile_schedule_switch)");
        SwitchMaterial switchMaterial = (SwitchMaterial) findViewById4;
        switchMaterial.setOnClickListener(new View.OnClickListener(switchMaterial) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ SwitchMaterial f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileScheduleFragment.$r8$lambda$a6OdR6Vusu6lPx1dkMS9UPgGxN0(EditNotificationProfileScheduleFragment.this, this.f$1, view2);
            }
        });
        View findViewById5 = view.findViewById(R.id.edit_notification_profile_schedule_start_time);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.e…file_schedule_start_time)");
        TextView textView = (TextView) findViewById5;
        View findViewById6 = view.findViewById(R.id.edit_notification_profile_schedule_end_time);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.e…rofile_schedule_end_time)");
        TextView textView2 = (TextView) findViewById6;
        View findViewById7 = view.findViewById(R.id.edit_notification_profile_schedule__next);
        Intrinsics.checkNotNullExpressionValue(findViewById7, "view.findViewById(R.id.e…n_profile_schedule__next)");
        CircularProgressMaterialButton circularProgressMaterialButton = (CircularProgressMaterialButton) findViewById7;
        circularProgressMaterialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                EditNotificationProfileScheduleFragment.m740$r8$lambda$wSCPbxxPilvwuFVKSDXe4dnLs(EditNotificationProfileScheduleFragment.this, view2);
            }
        });
        View findViewById8 = view.findViewById(R.id.edit_notification_profile_schedule_day_1);
        Intrinsics.checkNotNullExpressionValue(findViewById8, "view.findViewById(R.id.e…n_profile_schedule_day_1)");
        View findViewById9 = view.findViewById(R.id.edit_notification_profile_schedule_day_2);
        Intrinsics.checkNotNullExpressionValue(findViewById9, "view.findViewById(R.id.e…n_profile_schedule_day_2)");
        View findViewById10 = view.findViewById(R.id.edit_notification_profile_schedule_day_3);
        Intrinsics.checkNotNullExpressionValue(findViewById10, "view.findViewById(R.id.e…n_profile_schedule_day_3)");
        View findViewById11 = view.findViewById(R.id.edit_notification_profile_schedule_day_4);
        Intrinsics.checkNotNullExpressionValue(findViewById11, "view.findViewById(R.id.e…n_profile_schedule_day_4)");
        View findViewById12 = view.findViewById(R.id.edit_notification_profile_schedule_day_5);
        Intrinsics.checkNotNullExpressionValue(findViewById12, "view.findViewById(R.id.e…n_profile_schedule_day_5)");
        View findViewById13 = view.findViewById(R.id.edit_notification_profile_schedule_day_6);
        Intrinsics.checkNotNullExpressionValue(findViewById13, "view.findViewById(R.id.e…n_profile_schedule_day_6)");
        View findViewById14 = view.findViewById(R.id.edit_notification_profile_schedule_day_7);
        Intrinsics.checkNotNullExpressionValue(findViewById14, "view.findViewById(R.id.e…n_profile_schedule_day_7)");
        List list = CollectionsKt__CollectionsKt.listOf((Object[]) new CheckedTextView[]{(CheckedTextView) findViewById8, (CheckedTextView) findViewById9, (CheckedTextView) findViewById10, (CheckedTextView) findViewById11, (CheckedTextView) findViewById12, (CheckedTextView) findViewById13, (CheckedTextView) findViewById14});
        Locale locale = Locale.getDefault();
        Intrinsics.checkNotNullExpressionValue(locale, "getDefault()");
        Map map = MapsKt__MapsKt.toMap(CollectionsKt___CollectionsKt.zip(list, JavaTimeExtensionsKt.orderOfDaysInWeek(locale)));
        for (Map.Entry entry : map.entrySet()) {
            CheckedTextView checkedTextView = (CheckedTextView) entry.getKey();
            DayOfWeek dayOfWeek = (DayOfWeek) entry.getValue();
            DrawableCompat.setTintList(checkedTextView.getBackground(), ContextCompat.getColorStateList(checkedTextView.getContext(), R.color.notification_profile_schedule_background_tint));
            checkedTextView.setOnClickListener(new View.OnClickListener(dayOfWeek) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$$ExternalSyntheticLambda3
                public final /* synthetic */ DayOfWeek f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    EditNotificationProfileScheduleFragment.m742$r8$lambda$PvFj2HgLVuMfufWANlZn7kQTCY(EditNotificationProfileScheduleFragment.this, this.f$1, view2);
                }
            });
            Object obj = EditNotificationProfileScheduleFragmentKt.DAY_TO_STARTING_LETTER.get(dayOfWeek);
            Intrinsics.checkNotNull(obj);
            checkedTextView.setText(((Number) obj).intValue());
        }
        this.lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(getViewModel().schedule(), (Function1) null, (Function0) null, new EditNotificationProfileScheduleFragment$onViewCreated$5(switchMaterial, map, textView, view, textView2, this, circularProgressMaterialButton), 3, (Object) null));
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m743onViewCreated$lambda0(EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        FragmentKt.findNavController(editNotificationProfileScheduleFragment).navigateUp();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m744onViewCreated$lambda1(EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, SwitchMaterial switchMaterial, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        Intrinsics.checkNotNullParameter(switchMaterial, "$enableToggle");
        editNotificationProfileScheduleFragment.getViewModel().setEnabled(switchMaterial.isChecked());
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m745onViewCreated$lambda2(EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        editNotificationProfileScheduleFragment.lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(editNotificationProfileScheduleFragment.getViewModel().save(editNotificationProfileScheduleFragment.getCreateMode()), (Function1) null, new Function1<EditNotificationProfileScheduleViewModel.SaveScheduleResult, Unit>(editNotificationProfileScheduleFragment) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$onViewCreated$3$1
            final /* synthetic */ EditNotificationProfileScheduleFragment this$0;

            /* compiled from: EditNotificationProfileScheduleFragment.kt */
            @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
            /* loaded from: classes4.dex */
            public /* synthetic */ class WhenMappings {
                public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                static {
                    int[] iArr = new int[EditNotificationProfileScheduleViewModel.SaveScheduleResult.values().length];
                    iArr[EditNotificationProfileScheduleViewModel.SaveScheduleResult.Success.ordinal()] = 1;
                    iArr[EditNotificationProfileScheduleViewModel.SaveScheduleResult.NoDaysSelected.ordinal()] = 2;
                    $EnumSwitchMapping$0 = iArr;
                }
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(EditNotificationProfileScheduleViewModel.SaveScheduleResult saveScheduleResult) {
                invoke(saveScheduleResult);
                return Unit.INSTANCE;
            }

            public final void invoke(EditNotificationProfileScheduleViewModel.SaveScheduleResult saveScheduleResult) {
                Intrinsics.checkNotNullParameter(saveScheduleResult, MediaSendActivityResult.EXTRA_RESULT);
                int i = WhenMappings.$EnumSwitchMapping$0[saveScheduleResult.ordinal()];
                if (i != 1) {
                    if (i == 2) {
                        Toast.makeText(this.this$0.requireContext(), (int) R.string.EditNotificationProfileSchedule__schedule_must_have_at_least_one_day, 1).show();
                    }
                } else if (EditNotificationProfileScheduleFragment.access$getCreateMode(this.this$0)) {
                    NavController findNavController = FragmentKt.findNavController(this.this$0);
                    EditNotificationProfileScheduleFragmentDirections.ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment = EditNotificationProfileScheduleFragmentDirections.actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment(EditNotificationProfileScheduleFragment.access$getProfileId(this.this$0));
                    Intrinsics.checkNotNullExpressionValue(actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment, "actionEditNotificationPr…reatedFragment(profileId)");
                    SafeNavigation.safeNavigate(findNavController, actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment);
                } else {
                    FragmentKt.findNavController(this.this$0).navigateUp();
                }
            }
        }, 1, (Object) null));
    }

    /* renamed from: onViewCreated$lambda-4$lambda-3 */
    public static final void m746onViewCreated$lambda4$lambda3(EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, DayOfWeek dayOfWeek, View view) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        Intrinsics.checkNotNullParameter(dayOfWeek, "$day");
        editNotificationProfileScheduleFragment.getViewModel().toggleDay(dayOfWeek);
    }

    public final void showTimeSelector(boolean z, LocalTime localTime) {
        MaterialTimePicker build = new MaterialTimePicker.Builder().setTimeFormat(DateFormat.is24HourFormat(requireContext()) ? 1 : 0).setHour(localTime.getHour()).setMinute(localTime.getMinute()).setTitleText(z ? R.string.EditNotificationProfileSchedule__set_start_time : R.string.EditNotificationProfileSchedule__set_end_time).build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder()\n      .setTime…_end_time)\n      .build()");
        build.addOnDismissListener(new DialogInterface.OnDismissListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$$ExternalSyntheticLambda4
            @Override // android.content.DialogInterface.OnDismissListener
            public final void onDismiss(DialogInterface dialogInterface) {
                EditNotificationProfileScheduleFragment.m741$r8$lambda$KP9rVb9AEoEeXy_OvZTpx5v1zc(MaterialTimePicker.this, dialogInterface);
            }
        });
        build.addOnPositiveButtonClickListener(new View.OnClickListener(z, this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragment$$ExternalSyntheticLambda5
            public final /* synthetic */ boolean f$1;
            public final /* synthetic */ EditNotificationProfileScheduleFragment f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                EditNotificationProfileScheduleFragment.$r8$lambda$8nPoPXpiA9d73O6mwnOvumoOFwk(MaterialTimePicker.this, this.f$1, this.f$2, view);
            }
        });
        build.show(getChildFragmentManager(), "TIME_PICKER");
    }

    /* renamed from: showTimeSelector$lambda-5 */
    public static final void m747showTimeSelector$lambda5(MaterialTimePicker materialTimePicker, DialogInterface dialogInterface) {
        Intrinsics.checkNotNullParameter(materialTimePicker, "$timePickerFragment");
        materialTimePicker.clearOnDismissListeners();
        materialTimePicker.clearOnPositiveButtonClickListeners();
    }

    /* renamed from: showTimeSelector$lambda-6 */
    public static final void m748showTimeSelector$lambda6(MaterialTimePicker materialTimePicker, boolean z, EditNotificationProfileScheduleFragment editNotificationProfileScheduleFragment, View view) {
        Intrinsics.checkNotNullParameter(materialTimePicker, "$timePickerFragment");
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleFragment, "this$0");
        int hour = materialTimePicker.getHour();
        int minute = materialTimePicker.getMinute();
        if (z) {
            editNotificationProfileScheduleFragment.getViewModel().setStartTime(hour, minute);
        } else {
            editNotificationProfileScheduleFragment.getViewModel().setEndTime(hour, minute);
        }
    }
}
