package org.thoughtcrime.securesms.components.settings.models;

import io.reactivex.rxjava3.functions.Consumer;
import org.thoughtcrime.securesms.components.settings.models.TextInput;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class TextInput$MultilineViewHolder$$ExternalSyntheticLambda2 implements Consumer {
    public final /* synthetic */ TextInput.MultilineViewHolder f$0;

    public /* synthetic */ TextInput$MultilineViewHolder$$ExternalSyntheticLambda2(TextInput.MultilineViewHolder multilineViewHolder) {
        this.f$0 = multilineViewHolder;
    }

    @Override // io.reactivex.rxjava3.functions.Consumer
    public final void accept(Object obj) {
        TextInput.MultilineViewHolder.m1263onAttachedToWindow$lambda1(this.f$0, (TextInput.TextInputEvent) obj);
    }
}
