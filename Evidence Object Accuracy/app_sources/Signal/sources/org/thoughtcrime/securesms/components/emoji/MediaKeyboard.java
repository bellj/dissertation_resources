package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.InputAwareLayout;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.keyboard.KeyboardPagerFragment;
import org.thoughtcrime.securesms.keyboard.emoji.search.EmojiSearchFragment;
import org.thoughtcrime.securesms.util.ThemedFragment;

/* loaded from: classes4.dex */
public class MediaKeyboard extends FrameLayout implements InputAwareLayout.InputView {
    private static final String EMOJI_SEARCH;
    private static final String TAG = Log.tag(MediaKeyboard.class);
    private FragmentManager fragmentManager;
    private boolean isInitialised;
    private MediaKeyboardListener keyboardListener;
    private KeyboardPagerFragment keyboardPagerFragment;
    private State keyboardState;
    private int latestKeyboardHeight;
    private int mediaKeyboardTheme;

    /* loaded from: classes4.dex */
    public interface MediaKeyboardListener {
        void onHidden();

        void onKeyboardChanged(KeyboardPage keyboardPage);

        void onShown();
    }

    /* loaded from: classes4.dex */
    public enum State {
        NORMAL,
        EMOJI_SEARCH
    }

    public MediaKeyboard(Context context) {
        this(context, null);
    }

    public MediaKeyboard(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.MediaKeyboard);
            this.mediaKeyboardTheme = obtainStyledAttributes.getResourceId(0, -1);
            obtainStyledAttributes.recycle();
        }
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public void setKeyboardListener(MediaKeyboardListener mediaKeyboardListener) {
        this.keyboardListener = mediaKeyboardListener;
    }

    @Override // org.thoughtcrime.securesms.components.InputAwareLayout.InputView
    public boolean isShowing() {
        return getVisibility() == 0;
    }

    @Override // org.thoughtcrime.securesms.components.InputAwareLayout.InputView
    public void show(int i, boolean z) {
        if (!this.isInitialised) {
            initView();
        }
        this.latestKeyboardHeight = i;
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        layoutParams.height = this.keyboardState == State.NORMAL ? this.latestKeyboardHeight : -2;
        String str = TAG;
        Log.i(str, "showing emoji drawer with height " + layoutParams.height);
        setLayoutParams(layoutParams);
        show();
    }

    public boolean isInitialised() {
        return this.isInitialised;
    }

    public void show() {
        if (!this.isInitialised) {
            initView();
        }
        setVisibility(0);
        MediaKeyboardListener mediaKeyboardListener = this.keyboardListener;
        if (mediaKeyboardListener != null) {
            mediaKeyboardListener.onShown();
        }
        this.keyboardPagerFragment.show();
    }

    @Override // org.thoughtcrime.securesms.components.InputAwareLayout.InputView
    public void hide(boolean z) {
        setVisibility(8);
        onCloseEmojiSearchInternal(false);
        MediaKeyboardListener mediaKeyboardListener = this.keyboardListener;
        if (mediaKeyboardListener != null) {
            mediaKeyboardListener.onHidden();
        }
        Log.i(TAG, "hide()");
        this.keyboardPagerFragment.hide();
    }

    public void onCloseEmojiSearch() {
        onCloseEmojiSearchInternal(true);
    }

    private void onCloseEmojiSearchInternal(boolean z) {
        State state = this.keyboardState;
        State state2 = State.NORMAL;
        if (state != state2) {
            this.keyboardState = state2;
            Fragment findFragmentByTag = this.fragmentManager.findFragmentByTag(EMOJI_SEARCH);
            if (findFragmentByTag != null) {
                FragmentTransaction customAnimations = this.fragmentManager.beginTransaction().remove(findFragmentByTag).show(this.keyboardPagerFragment).setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                if (z) {
                    customAnimations.runOnCommit(new Runnable() { // from class: org.thoughtcrime.securesms.components.emoji.MediaKeyboard$$ExternalSyntheticLambda0
                        @Override // java.lang.Runnable
                        public final void run() {
                            MediaKeyboard.this.lambda$onCloseEmojiSearchInternal$0();
                        }
                    });
                }
                customAnimations.commitAllowingStateLoss();
            }
        }
    }

    public /* synthetic */ void lambda$onCloseEmojiSearchInternal$0() {
        show(this.latestKeyboardHeight, false);
    }

    public void onOpenEmojiSearch() {
        State state = this.keyboardState;
        State state2 = State.EMOJI_SEARCH;
        if (state != state2) {
            this.keyboardState = state2;
            EmojiSearchFragment emojiSearchFragment = new EmojiSearchFragment();
            int i = this.mediaKeyboardTheme;
            if (i != -1) {
                ThemedFragment.withTheme(emojiSearchFragment, i);
            }
            this.fragmentManager.beginTransaction().hide(this.keyboardPagerFragment).add(R.id.media_keyboard_fragment_container, emojiSearchFragment, EMOJI_SEARCH).runOnCommit(new Runnable() { // from class: org.thoughtcrime.securesms.components.emoji.MediaKeyboard$$ExternalSyntheticLambda1
                @Override // java.lang.Runnable
                public final void run() {
                    MediaKeyboard.this.lambda$onOpenEmojiSearch$1();
                }
            }).setCustomAnimations(R.anim.fade_in, R.anim.fade_out).commitAllowingStateLoss();
        }
    }

    public /* synthetic */ void lambda$onOpenEmojiSearch$1() {
        show(this.latestKeyboardHeight, true);
    }

    public boolean isEmojiSearchMode() {
        return this.keyboardState == State.EMOJI_SEARCH;
    }

    private void initView() {
        if (!this.isInitialised) {
            LayoutInflater.from(getContext()).inflate(R.layout.media_keyboard, (ViewGroup) this, true);
            if (this.fragmentManager == null) {
                this.fragmentManager = resolveActivity(getContext()).getSupportFragmentManager();
            }
            KeyboardPagerFragment keyboardPagerFragment = new KeyboardPagerFragment();
            this.keyboardPagerFragment = keyboardPagerFragment;
            int i = this.mediaKeyboardTheme;
            if (i != -1) {
                ThemedFragment.withTheme(keyboardPagerFragment, i);
            }
            this.fragmentManager.beginTransaction().replace(R.id.media_keyboard_fragment_container, this.keyboardPagerFragment).commitNowAllowingStateLoss();
            this.keyboardState = State.NORMAL;
            this.latestKeyboardHeight = -1;
            this.isInitialised = true;
        }
    }

    private static FragmentActivity resolveActivity(Context context) {
        if (context instanceof FragmentActivity) {
            return (FragmentActivity) context;
        }
        if (context instanceof ContextThemeWrapper) {
            return resolveActivity(((ContextThemeWrapper) context).getBaseContext());
        }
        throw new IllegalStateException("Could not locate FragmentActivity");
    }
}
