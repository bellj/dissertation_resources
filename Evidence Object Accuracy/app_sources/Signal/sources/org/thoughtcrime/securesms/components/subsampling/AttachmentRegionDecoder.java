package org.thoughtcrime.securesms.components.subsampling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder;
import com.davemorrissey.labs.subscaleview.decoder.SkiaImageRegionDecoder;
import java.io.InputStream;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.mms.PartAuthority;

/* loaded from: classes4.dex */
public class AttachmentRegionDecoder implements ImageRegionDecoder {
    private static final String TAG = Log.tag(AttachmentRegionDecoder.class);
    private BitmapRegionDecoder bitmapRegionDecoder;
    private SkiaImageRegionDecoder passthrough;

    @Override // com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder
    public Point init(Context context, Uri uri) throws Exception {
        Log.d(TAG, "Init!");
        if (!PartAuthority.isLocalUri(uri)) {
            SkiaImageRegionDecoder skiaImageRegionDecoder = new SkiaImageRegionDecoder();
            this.passthrough = skiaImageRegionDecoder;
            return skiaImageRegionDecoder.init(context, uri);
        }
        InputStream attachmentStream = PartAuthority.getAttachmentStream(context, uri);
        this.bitmapRegionDecoder = BitmapRegionDecoder.newInstance(attachmentStream, false);
        attachmentStream.close();
        return new Point(this.bitmapRegionDecoder.getWidth(), this.bitmapRegionDecoder.getHeight());
    }

    @Override // com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder
    public Bitmap decodeRegion(Rect rect, int i) {
        Bitmap decodeRegion;
        String str = TAG;
        Log.d(str, "Decode region: " + rect);
        SkiaImageRegionDecoder skiaImageRegionDecoder = this.passthrough;
        if (skiaImageRegionDecoder != null) {
            return skiaImageRegionDecoder.decodeRegion(rect, i);
        }
        synchronized (this) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = i;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            decodeRegion = this.bitmapRegionDecoder.decodeRegion(rect, options);
            if (decodeRegion == null) {
                throw new RuntimeException("Skia image decoder returned null bitmap - image format may not be supported");
            }
        }
        return decodeRegion;
    }

    @Override // com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder
    public boolean isReady() {
        BitmapRegionDecoder bitmapRegionDecoder;
        Log.d(TAG, "isReady");
        SkiaImageRegionDecoder skiaImageRegionDecoder = this.passthrough;
        return (skiaImageRegionDecoder != null && skiaImageRegionDecoder.isReady()) || ((bitmapRegionDecoder = this.bitmapRegionDecoder) != null && !bitmapRegionDecoder.isRecycled());
    }

    @Override // com.davemorrissey.labs.subscaleview.decoder.ImageRegionDecoder
    public void recycle() {
        SkiaImageRegionDecoder skiaImageRegionDecoder = this.passthrough;
        if (skiaImageRegionDecoder != null) {
            skiaImageRegionDecoder.recycle();
            this.passthrough = null;
            return;
        }
        this.bitmapRegionDecoder.recycle();
    }
}
