package org.thoughtcrime.securesms.components.settings.conversation;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ Recipient f$0;

    public /* synthetic */ InternalConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda1(Recipient recipient) {
        this.f$0 = recipient;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        InternalConversationSettingsFragment$getConfiguration$1.AnonymousClass8.m1173invoke$lambda1(this.f$0, dialogInterface, i);
    }
}
