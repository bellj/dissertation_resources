package org.thoughtcrime.securesms.components.settings.conversation;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ConversationSettingsEvent.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0007\u0003\u0004\u0005\u0006\u0007\b\tB\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0007\n\u000b\f\r\u000e\u000f\u0010¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "", "()V", "AddMembersToGroup", "AddToAGroup", "InitiateGroupMigration", "ShowAddMembersToGroupError", "ShowGroupHardLimitDialog", "ShowGroupInvitesSentDialog", "ShowMembersAdded", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$AddToAGroup;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$AddMembersToGroup;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowGroupHardLimitDialog;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowAddMembersToGroupError;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowGroupInvitesSentDialog;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowMembersAdded;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$InitiateGroupMigration;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ConversationSettingsEvent {
    public /* synthetic */ ConversationSettingsEvent(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    private ConversationSettingsEvent() {
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\u0002\u0010\u0006R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$AddToAGroup;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "groupMembership", "", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Ljava/util/List;)V", "getGroupMembership", "()Ljava/util/List;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class AddToAGroup extends ConversationSettingsEvent {
        private final List<RecipientId> groupMembership;
        private final RecipientId recipientId;

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public final List<RecipientId> getGroupMembership() {
            return this.groupMembership;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public AddToAGroup(RecipientId recipientId, List<? extends RecipientId> list) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(list, "groupMembership");
            this.recipientId = recipientId;
            this.groupMembership = list;
        }
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\n\u0018\u00002\u00020\u0001B3\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0011R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0013¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$AddMembersToGroup;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "selectionWarning", "", "selectionLimit", "isAnnouncementGroup", "", "groupMembersWithoutSelf", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/groups/GroupId;IIZLjava/util/List;)V", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId;", "getGroupMembersWithoutSelf", "()Ljava/util/List;", "()Z", "getSelectionLimit", "()I", "getSelectionWarning", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class AddMembersToGroup extends ConversationSettingsEvent {
        private final GroupId groupId;
        private final List<RecipientId> groupMembersWithoutSelf;
        private final boolean isAnnouncementGroup;
        private final int selectionLimit;
        private final int selectionWarning;

        public final GroupId getGroupId() {
            return this.groupId;
        }

        public final int getSelectionWarning() {
            return this.selectionWarning;
        }

        public final int getSelectionLimit() {
            return this.selectionLimit;
        }

        public final boolean isAnnouncementGroup() {
            return this.isAnnouncementGroup;
        }

        public final List<RecipientId> getGroupMembersWithoutSelf() {
            return this.groupMembersWithoutSelf;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public AddMembersToGroup(GroupId groupId, int i, int i2, boolean z, List<? extends RecipientId> list) {
            super(null);
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(list, "groupMembersWithoutSelf");
            this.groupId = groupId;
            this.selectionWarning = i;
            this.selectionLimit = i2;
            this.isAnnouncementGroup = z;
            this.groupMembersWithoutSelf = list;
        }
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowGroupHardLimitDialog;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ShowGroupHardLimitDialog extends ConversationSettingsEvent {
        public static final ShowGroupHardLimitDialog INSTANCE = new ShowGroupHardLimitDialog();

        private ShowGroupHardLimitDialog() {
            super(null);
        }
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowAddMembersToGroupError;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "failureReason", "Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "(Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;)V", "getFailureReason", "()Lorg/thoughtcrime/securesms/groups/ui/GroupChangeFailureReason;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ShowAddMembersToGroupError extends ConversationSettingsEvent {
        private final GroupChangeFailureReason failureReason;

        public final GroupChangeFailureReason getFailureReason() {
            return this.failureReason;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ShowAddMembersToGroupError(GroupChangeFailureReason groupChangeFailureReason) {
            super(null);
            Intrinsics.checkNotNullParameter(groupChangeFailureReason, "failureReason");
            this.failureReason = groupChangeFailureReason;
        }
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0013\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\u0002\u0010\u0005R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowGroupInvitesSentDialog;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "invitesSentTo", "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Ljava/util/List;)V", "getInvitesSentTo", "()Ljava/util/List;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ShowGroupInvitesSentDialog extends ConversationSettingsEvent {
        private final List<Recipient> invitesSentTo;

        public final List<Recipient> getInvitesSentTo() {
            return this.invitesSentTo;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r2v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ShowGroupInvitesSentDialog(List<? extends Recipient> list) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "invitesSentTo");
            this.invitesSentTo = list;
        }
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowMembersAdded;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "membersAddedCount", "", "(I)V", "getMembersAddedCount", "()I", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ShowMembersAdded extends ConversationSettingsEvent {
        private final int membersAddedCount;

        public final int getMembersAddedCount() {
            return this.membersAddedCount;
        }

        public ShowMembersAdded(int i) {
            super(null);
            this.membersAddedCount = i;
        }
    }

    /* compiled from: ConversationSettingsEvent.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$InitiateGroupMigration;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class InitiateGroupMigration extends ConversationSettingsEvent {
        private final RecipientId recipientId;

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public InitiateGroupMigration(RecipientId recipientId) {
            super(null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }
    }
}
