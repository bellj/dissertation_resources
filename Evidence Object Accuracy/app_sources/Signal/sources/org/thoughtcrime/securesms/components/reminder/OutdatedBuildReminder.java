package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.view.View;
import java.util.concurrent.TimeUnit;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class OutdatedBuildReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    public OutdatedBuildReminder(Context context) {
        super(null, getPluralsText(context));
        setOkListener(new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.OutdatedBuildReminder$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                OutdatedBuildReminder.m519$r8$lambda$Gmzc3tveRuagzOYU6OUtVcfzlo(this.f$0, view);
            }
        });
        addAction(new Reminder.Action(context.getString(R.string.OutdatedBuildReminder_update_now), R.id.reminder_action_update_now));
    }

    private static CharSequence getPluralsText(Context context) {
        int daysUntilExpiry = getDaysUntilExpiry();
        if (daysUntilExpiry == 0) {
            return context.getString(R.string.OutdatedBuildReminder_your_version_of_signal_will_expire_today);
        }
        return context.getResources().getQuantityString(R.plurals.OutdatedBuildReminder_your_version_of_signal_will_expire_in_n_days, daysUntilExpiry, Integer.valueOf(daysUntilExpiry));
    }

    public static boolean isEligible() {
        return getDaysUntilExpiry() <= 10;
    }

    private static int getDaysUntilExpiry() {
        return (int) TimeUnit.MILLISECONDS.toDays(Util.getTimeUntilBuildExpiry());
    }
}
