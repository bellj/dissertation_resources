package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversationlist.model.UnreadPayments;

/* loaded from: classes4.dex */
public class UnreadPaymentsView extends ConstraintLayout {
    private AvatarImageView avatar;
    private Listener listener;
    private TextView title;

    /* loaded from: classes4.dex */
    public interface Listener {
        void onClosePaymentsNotificationClicked();

        void onOpenPaymentsNotificationClicked();
    }

    public UnreadPaymentsView(Context context) {
        super(context);
    }

    public UnreadPaymentsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public UnreadPaymentsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public UnreadPaymentsView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.title = (TextView) findViewById(R.id.payment_notification_title);
        this.avatar = (AvatarImageView) findViewById(R.id.payment_notification_avatar);
        View findViewById = findViewById(R.id.payment_notification_touch_target);
        View findViewById2 = findViewById(R.id.payment_notification_close_touch_target);
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.UnreadPaymentsView$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UnreadPaymentsView.m497$r8$lambda$2yTLrLMjlg1u_16KUWiff5n0Z0(UnreadPaymentsView.this, view);
            }
        });
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.UnreadPaymentsView$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UnreadPaymentsView.$r8$lambda$msLzC7HF1zQvfyHf2GKgUX4d7nw(UnreadPaymentsView.this, view);
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$0(View view) {
        Listener listener = this.listener;
        if (listener != null) {
            listener.onOpenPaymentsNotificationClicked();
        }
    }

    public /* synthetic */ void lambda$onFinishInflate$1(View view) {
        Listener listener = this.listener;
        if (listener != null) {
            listener.onClosePaymentsNotificationClicked();
        }
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setUnreadPayments(UnreadPayments unreadPayments) {
        this.title.setText(unreadPayments.getDescription(getContext()));
        this.avatar.setAvatar(unreadPayments.getRecipient());
        this.avatar.setVisibility(unreadPayments.getRecipient() == null ? 8 : 0);
    }
}
