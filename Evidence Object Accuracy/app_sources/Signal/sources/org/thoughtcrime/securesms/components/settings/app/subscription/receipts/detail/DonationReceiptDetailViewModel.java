package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: DonationReceiptDetailViewModel.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0019B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0015\u001a\u00020\u0016H\u0014J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00100\u0014X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailViewModel;", "Landroidx/lifecycle/ViewModel;", ContactRepository.ID_COLUMN, "", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailRepository;", "(JLorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailRepository;)V", "cachedRecord", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "networkDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "refresh", "retry", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptDetailViewModel extends ViewModel {
    private final Single<DonationReceiptRecord> cachedRecord;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private Disposable networkDisposable;
    private final DonationReceiptDetailRepository repository;
    private final LiveData<DonationReceiptDetailState> state;
    private final Store<DonationReceiptDetailState> store;

    public DonationReceiptDetailViewModel(long j, DonationReceiptDetailRepository donationReceiptDetailRepository) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailRepository, "repository");
        this.repository = donationReceiptDetailRepository;
        Store<DonationReceiptDetailState> store = new Store<>(new DonationReceiptDetailState(null, null, 3, null));
        this.store = store;
        Single<DonationReceiptRecord> cache = donationReceiptDetailRepository.getDonationReceiptRecord(j).cache();
        Intrinsics.checkNotNullExpressionValue(cache, "repository.getDonationReceiptRecord(id).cache()");
        this.cachedRecord = cache;
        LiveData<DonationReceiptDetailState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Disposable subscribe = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationReceiptDetailViewModel.m1010_init_$lambda0(DonationReceiptDetailViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "InternetConnectionObserv…retry()\n        }\n      }");
        this.networkDisposable = subscribe;
        refresh();
    }

    public final LiveData<DonationReceiptDetailState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1010_init_$lambda0(DonationReceiptDetailViewModel donationReceiptDetailViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
        if (bool.booleanValue()) {
            donationReceiptDetailViewModel.retry();
        }
    }

    private final void retry() {
        if (this.store.getState().getSubscriptionName() == null) {
            refresh();
        }
    }

    private final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.cachedRecord.subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationReceiptDetailViewModel.m1011refresh$lambda2(DonationReceiptDetailViewModel.this, (DonationReceiptRecord) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "cachedRecord.subscribe {…tRecord = record) }\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
        CompositeDisposable compositeDisposable2 = this.disposables;
        Disposable subscribe2 = this.cachedRecord.flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationReceiptDetailViewModel.m1013refresh$lambda3(DonationReceiptDetailViewModel.this, (DonationReceiptRecord) obj);
            }
        }).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationReceiptDetailViewModel.m1014refresh$lambda5(DonationReceiptDetailViewModel.this, (String) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe2, "cachedRecord.flatMap {\n …ptionName = name) }\n    }");
        DisposableKt.plusAssign(compositeDisposable2, subscribe2);
    }

    /* renamed from: refresh$lambda-2 */
    public static final void m1011refresh$lambda2(DonationReceiptDetailViewModel donationReceiptDetailViewModel, DonationReceiptRecord donationReceiptRecord) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailViewModel, "this$0");
        donationReceiptDetailViewModel.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DonationReceiptDetailViewModel.m1012refresh$lambda2$lambda1(DonationReceiptRecord.this, (DonationReceiptDetailState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-2$lambda-1 */
    public static final DonationReceiptDetailState m1012refresh$lambda2$lambda1(DonationReceiptRecord donationReceiptRecord, DonationReceiptDetailState donationReceiptDetailState) {
        Intrinsics.checkNotNullExpressionValue(donationReceiptDetailState, "it");
        return DonationReceiptDetailState.copy$default(donationReceiptDetailState, donationReceiptRecord, null, 2, null);
    }

    /* renamed from: refresh$lambda-3 */
    public static final SingleSource m1013refresh$lambda3(DonationReceiptDetailViewModel donationReceiptDetailViewModel, DonationReceiptRecord donationReceiptRecord) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailViewModel, "this$0");
        if (donationReceiptRecord.getSubscriptionLevel() > 0) {
            return donationReceiptDetailViewModel.repository.getSubscriptionLevelName(donationReceiptRecord.getSubscriptionLevel());
        }
        return Single.just("");
    }

    /* renamed from: refresh$lambda-5 */
    public static final void m1014refresh$lambda5(DonationReceiptDetailViewModel donationReceiptDetailViewModel, String str) {
        Intrinsics.checkNotNullParameter(donationReceiptDetailViewModel, "this$0");
        donationReceiptDetailViewModel.store.update(new com.annimon.stream.function.Function(str) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DonationReceiptDetailViewModel.m1015refresh$lambda5$lambda4(this.f$0, (DonationReceiptDetailState) obj);
            }
        });
    }

    /* renamed from: refresh$lambda-5$lambda-4 */
    public static final DonationReceiptDetailState m1015refresh$lambda5$lambda4(String str, DonationReceiptDetailState donationReceiptDetailState) {
        Intrinsics.checkNotNullExpressionValue(donationReceiptDetailState, "it");
        return DonationReceiptDetailState.copy$default(donationReceiptDetailState, null, str, 1, null);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
        this.networkDisposable.dispose();
    }

    /* compiled from: DonationReceiptDetailViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", ContactRepository.ID_COLUMN, "", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailRepository;", "(JLorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long id;
        private final DonationReceiptDetailRepository repository;

        public Factory(long j, DonationReceiptDetailRepository donationReceiptDetailRepository) {
            Intrinsics.checkNotNullParameter(donationReceiptDetailRepository, "repository");
            this.id = j;
            this.repository = donationReceiptDetailRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new DonationReceiptDetailViewModel(this.id, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailViewModel.Factory.create");
        }
    }
}
