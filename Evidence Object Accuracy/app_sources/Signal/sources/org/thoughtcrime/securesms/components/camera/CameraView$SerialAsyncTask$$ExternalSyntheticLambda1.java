package org.thoughtcrime.securesms.components.camera;

import org.thoughtcrime.securesms.components.camera.CameraView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CameraView$SerialAsyncTask$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ CameraView.SerialAsyncTask f$0;
    public final /* synthetic */ Object f$1;

    public /* synthetic */ CameraView$SerialAsyncTask$$ExternalSyntheticLambda1(CameraView.SerialAsyncTask serialAsyncTask, Object obj) {
        this.f$0 = serialAsyncTask;
        this.f$1 = obj;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.lambda$run$0(this.f$1);
    }
}
