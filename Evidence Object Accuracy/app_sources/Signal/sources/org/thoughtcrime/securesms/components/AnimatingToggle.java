package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class AnimatingToggle extends FrameLayout {
    private View current;
    private final Animation inAnimation;
    private final Animation outAnimation;

    public AnimatingToggle(Context context) {
        this(context, null);
    }

    public AnimatingToggle(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AnimatingToggle(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Animation loadAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.animation_toggle_out);
        this.outAnimation = loadAnimation;
        Animation loadAnimation2 = AnimationUtils.loadAnimation(getContext(), R.anim.animation_toggle_in);
        this.inAnimation = loadAnimation2;
        loadAnimation.setInterpolator(new FastOutSlowInInterpolator());
        loadAnimation2.setInterpolator(new FastOutSlowInInterpolator());
    }

    @Override // android.view.ViewGroup
    public void addView(View view, int i, ViewGroup.LayoutParams layoutParams) {
        super.addView(view, i, layoutParams);
        if (!isInEditMode()) {
            if (getChildCount() == 1) {
                this.current = view;
                view.setVisibility(0);
            } else {
                view.setVisibility(8);
            }
            view.setClickable(false);
        }
    }

    public void display(View view) {
        View view2 = this.current;
        if (view != view2) {
            if (view2 != null) {
                ViewUtil.animateOut(view2, this.outAnimation, 8);
            }
            if (view != null) {
                ViewUtil.animateIn(view, this.inAnimation);
            }
            this.current = view;
        }
    }

    public void displayQuick(View view) {
        View view2 = this.current;
        if (view != view2) {
            if (view2 != null) {
                view2.setVisibility(8);
            }
            if (view != null) {
                view.setVisibility(0);
            }
            this.current = view;
        }
    }
}
