package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;

/* compiled from: BubbleOptOutReminder.kt */
@Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/reminder/BubbleOptOutReminder;", "Lorg/thoughtcrime/securesms/components/reminder/Reminder;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "isDismissable", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BubbleOptOutReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BubbleOptOutReminder(Context context) {
        super(null, context.getString(R.string.BubbleOptOutTooltip__description));
        Intrinsics.checkNotNullParameter(context, "context");
        addAction(new Reminder.Action(context.getString(R.string.BubbleOptOutTooltip__turn_off), R.id.reminder_action_turn_off));
        addAction(new Reminder.Action(context.getString(R.string.BubbleOptOutTooltip__not_now), R.id.reminder_action_not_now));
    }
}
