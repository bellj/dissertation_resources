package org.thoughtcrime.securesms.components;

import android.net.Uri;
import android.view.View;
import org.thoughtcrime.securesms.components.RecentPhotoViewRail;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class RecentPhotoViewRail$RecentPhotoAdapter$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ RecentPhotoViewRail.RecentPhotoAdapter f$0;
    public final /* synthetic */ Uri f$1;
    public final /* synthetic */ String f$2;
    public final /* synthetic */ String f$3;
    public final /* synthetic */ long f$4;
    public final /* synthetic */ int f$5;
    public final /* synthetic */ int f$6;
    public final /* synthetic */ long f$7;

    public /* synthetic */ RecentPhotoViewRail$RecentPhotoAdapter$$ExternalSyntheticLambda0(RecentPhotoViewRail.RecentPhotoAdapter recentPhotoAdapter, Uri uri, String str, String str2, long j, int i, int i2, long j2) {
        this.f$0 = recentPhotoAdapter;
        this.f$1 = uri;
        this.f$2 = str;
        this.f$3 = str2;
        this.f$4 = j;
        this.f$5 = i;
        this.f$6 = i2;
        this.f$7 = j2;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$onBindItemViewHolder$0(this.f$1, this.f$2, this.f$3, this.f$4, this.f$5, this.f$6, this.f$7, view);
    }
}
