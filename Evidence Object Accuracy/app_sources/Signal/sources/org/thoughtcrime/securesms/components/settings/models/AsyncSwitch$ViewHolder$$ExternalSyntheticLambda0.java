package org.thoughtcrime.securesms.components.settings.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.models.AsyncSwitch;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AsyncSwitch$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AsyncSwitch.Model f$0;
    public final /* synthetic */ AsyncSwitch.ViewHolder f$1;

    public /* synthetic */ AsyncSwitch$ViewHolder$$ExternalSyntheticLambda0(AsyncSwitch.Model model, AsyncSwitch.ViewHolder viewHolder) {
        this.f$0 = model;
        this.f$1 = viewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AsyncSwitch.ViewHolder.m1247bind$lambda0(this.f$0, this.f$1, view);
    }
}
