package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import android.content.Context;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import java.util.LinkedHashMap;
import java.util.Map;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt__MapsJVMKt;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.RangesKt___RangesKt;
import org.signal.core.util.logging.Log;
import org.signal.donations.StripeDeclineCode;
import org.signal.donations.StripeError;
import org.thoughtcrime.securesms.database.PushDatabase;

/* compiled from: DonationError.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u000b2\u00060\u0001j\u0002`\u0002:\u0007\n\u000b\f\r\u000e\u000f\u0010B\u0017\b\u0004\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t\u0001\u0007\u0011\u0012\u0013\u0014\u0015\u0016\u0017¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", "Ljava/lang/Exception;", "Lkotlin/Exception;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "getSource", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "BadgeRedemptionError", "Companion", "GiftRecipientVerificationError", "GooglePayError", "OneTimeDonationError", "PaymentProcessingError", "PaymentSetupError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentProcessingError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentProcessingError$GenericError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DonationError extends Exception {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(DonationError.class);
    private static final Map<DonationErrorSource, Subject<DonationError>> donationErrorSubjectSourceMap;
    private final DonationErrorSource source;

    public /* synthetic */ DonationError(DonationErrorSource donationErrorSource, Throwable th, DefaultConstructorMarker defaultConstructorMarker) {
        this(donationErrorSource, th);
    }

    @JvmStatic
    public static final DonationError badgeCredentialVerificationFailure(DonationErrorSource donationErrorSource) {
        return Companion.badgeCredentialVerificationFailure(donationErrorSource);
    }

    @JvmStatic
    public static final DonationError genericBadgeRedemptionFailure(DonationErrorSource donationErrorSource) {
        return Companion.genericBadgeRedemptionFailure(donationErrorSource);
    }

    @JvmStatic
    public static final DonationError genericPaymentFailure(DonationErrorSource donationErrorSource) {
        return Companion.genericPaymentFailure(donationErrorSource);
    }

    @JvmStatic
    public static final Observable<DonationError> getErrorsForSource(DonationErrorSource donationErrorSource) {
        return Companion.getErrorsForSource(donationErrorSource);
    }

    @JvmStatic
    public static final DonationError getGooglePayRequestTokenError(DonationErrorSource donationErrorSource, Throwable th) {
        return Companion.getGooglePayRequestTokenError(donationErrorSource, th);
    }

    @JvmStatic
    public static final DonationError getPaymentSetupError(DonationErrorSource donationErrorSource, Throwable th) {
        return Companion.getPaymentSetupError(donationErrorSource, th);
    }

    @JvmStatic
    public static final DonationError invalidCurrencyForOneTimeDonation(DonationErrorSource donationErrorSource) {
        return Companion.invalidCurrencyForOneTimeDonation(donationErrorSource);
    }

    @JvmStatic
    public static final DonationError oneTimeDonationAmountTooLarge(DonationErrorSource donationErrorSource) {
        return Companion.oneTimeDonationAmountTooLarge(donationErrorSource);
    }

    @JvmStatic
    public static final DonationError oneTimeDonationAmountTooSmall(DonationErrorSource donationErrorSource) {
        return Companion.oneTimeDonationAmountTooSmall(donationErrorSource);
    }

    @JvmStatic
    public static final void routeDonationError(Context context, DonationError donationError) {
        Companion.routeDonationError(context, donationError);
    }

    @JvmStatic
    public static final DonationError timeoutWaitingForToken(DonationErrorSource donationErrorSource) {
        return Companion.timeoutWaitingForToken(donationErrorSource);
    }

    private DonationError(DonationErrorSource donationErrorSource, Throwable th) {
        super(th);
        this.source = donationErrorSource;
    }

    public final DonationErrorSource getSource() {
        return this.source;
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006\u0001\u0002\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "NotAvailableError", "RequestTokenError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError$NotAvailableError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError$RequestTokenError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class GooglePayError extends DonationError {
        public /* synthetic */ GooglePayError(DonationErrorSource donationErrorSource, Throwable th, DefaultConstructorMarker defaultConstructorMarker) {
            this(donationErrorSource, th);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError$NotAvailableError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class NotAvailableError extends GooglePayError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public NotAvailableError(DonationErrorSource donationErrorSource, Throwable th) {
                super(donationErrorSource, th, null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
                Intrinsics.checkNotNullParameter(th, "cause");
            }
        }

        private GooglePayError(DonationErrorSource donationErrorSource, Throwable th) {
            super(donationErrorSource, th, null);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError$RequestTokenError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GooglePayError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class RequestTokenError extends GooglePayError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public RequestTokenError(DonationErrorSource donationErrorSource, Throwable th) {
                super(donationErrorSource, th, null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
                Intrinsics.checkNotNullParameter(th, "cause");
            }
        }
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0005\u0006\u0007B\u000f\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004\u0001\u0003\b\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", "cause", "", "(Ljava/lang/Throwable;)V", "FailedToFetchProfile", "SelectedRecipientDoesNotSupportGifts", "SelectedRecipientIsInvalid", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError$SelectedRecipientIsInvalid;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError$SelectedRecipientDoesNotSupportGifts;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError$FailedToFetchProfile;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class GiftRecipientVerificationError extends DonationError {
        public /* synthetic */ GiftRecipientVerificationError(Throwable th, DefaultConstructorMarker defaultConstructorMarker) {
            this(th);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError$SelectedRecipientIsInvalid;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SelectedRecipientIsInvalid extends GiftRecipientVerificationError {
            public static final SelectedRecipientIsInvalid INSTANCE = new SelectedRecipientIsInvalid();

            private SelectedRecipientIsInvalid() {
                super(new Exception("Selected recipient is invalid."), null);
            }
        }

        private GiftRecipientVerificationError(Throwable th) {
            super(DonationErrorSource.GIFT, th, null);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError$SelectedRecipientDoesNotSupportGifts;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class SelectedRecipientDoesNotSupportGifts extends GiftRecipientVerificationError {
            public static final SelectedRecipientDoesNotSupportGifts INSTANCE = new SelectedRecipientDoesNotSupportGifts();

            private SelectedRecipientDoesNotSupportGifts() {
                super(new Exception("Selected recipient does not support gifts."), null);
            }
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError$FailedToFetchProfile;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$GiftRecipientVerificationError;", "cause", "", "(Ljava/lang/Throwable;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class FailedToFetchProfile extends GiftRecipientVerificationError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public FailedToFetchProfile(Throwable th) {
                super(new Exception("Failed to fetch recipient profile.", th), null);
                Intrinsics.checkNotNullParameter(th, "cause");
            }
        }
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006\u0001\u0003\n\u000b\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "message", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/String;)V", "AmountTooLargeError", "AmountTooSmallError", "InvalidCurrencyError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError$AmountTooSmallError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError$AmountTooLargeError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError$InvalidCurrencyError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class OneTimeDonationError extends DonationError {
        public /* synthetic */ OneTimeDonationError(DonationErrorSource donationErrorSource, String str, DefaultConstructorMarker defaultConstructorMarker) {
            this(donationErrorSource, str);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError$AmountTooSmallError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class AmountTooSmallError extends OneTimeDonationError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public AmountTooSmallError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, "Amount is too small", null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }

        private OneTimeDonationError(DonationErrorSource donationErrorSource, String str) {
            super(donationErrorSource, new Exception(str), null);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError$AmountTooLargeError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class AmountTooLargeError extends OneTimeDonationError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public AmountTooLargeError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, "Amount is too large", null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError$InvalidCurrencyError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$OneTimeDonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class InvalidCurrencyError extends OneTimeDonationError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public InvalidCurrencyError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, "Currency is not supported", null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006\u0001\u0003\n\u000b\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "CodedError", "DeclinedError", "GenericError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$GenericError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$CodedError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$DeclinedError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class PaymentSetupError extends DonationError {
        public /* synthetic */ PaymentSetupError(DonationErrorSource donationErrorSource, Throwable th, DefaultConstructorMarker defaultConstructorMarker) {
            this(donationErrorSource, th);
        }

        private PaymentSetupError(DonationErrorSource donationErrorSource, Throwable th) {
            super(donationErrorSource, th, null);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$GenericError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class GenericError extends PaymentSetupError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public GenericError(DonationErrorSource donationErrorSource, Throwable th) {
                super(donationErrorSource, th, null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
                Intrinsics.checkNotNullParameter(th, "cause");
            }
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$CodedError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "errorCode", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;Ljava/lang/String;)V", "getErrorCode", "()Ljava/lang/String;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class CodedError extends PaymentSetupError {
            private final String errorCode;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public CodedError(DonationErrorSource donationErrorSource, Throwable th, String str) {
                super(donationErrorSource, th, null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
                Intrinsics.checkNotNullParameter(th, "cause");
                Intrinsics.checkNotNullParameter(str, "errorCode");
                this.errorCode = str;
            }

            public final String getErrorCode() {
                return this.errorCode;
            }
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError$DeclinedError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentSetupError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "declineCode", "Lorg/signal/donations/StripeDeclineCode;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;Lorg/signal/donations/StripeDeclineCode;)V", "getDeclineCode", "()Lorg/signal/donations/StripeDeclineCode;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DeclinedError extends PaymentSetupError {
            private final StripeDeclineCode declineCode;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public DeclinedError(DonationErrorSource donationErrorSource, Throwable th, StripeDeclineCode stripeDeclineCode) {
                super(donationErrorSource, th, null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
                Intrinsics.checkNotNullParameter(th, "cause");
                Intrinsics.checkNotNullParameter(stripeDeclineCode, "declineCode");
                this.declineCode = stripeDeclineCode;
            }

            public final StripeDeclineCode getDeclineCode() {
                return this.declineCode;
            }
        }
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0003\b6\u0018\u00002\u00020\u0001:\u0001\u0007B\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentProcessingError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "GenericError", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class PaymentProcessingError extends DonationError {
        public /* synthetic */ PaymentProcessingError(DonationErrorSource donationErrorSource, Throwable th, DefaultConstructorMarker defaultConstructorMarker) {
            this(donationErrorSource, th);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$PaymentProcessingError$GenericError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class GenericError extends DonationError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public GenericError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, new Exception("Generic Payment Error"), null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }

        private PaymentProcessingError(DonationErrorSource donationErrorSource, Throwable th) {
            super(donationErrorSource, th, null);
        }
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006\u0001\u0003\n\u000b\f¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "cause", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;Ljava/lang/Throwable;)V", "FailedToValidateCredentialError", "GenericError", "TimeoutWaitingForTokenError", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$TimeoutWaitingForTokenError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$FailedToValidateCredentialError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$GenericError;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class BadgeRedemptionError extends DonationError {
        public /* synthetic */ BadgeRedemptionError(DonationErrorSource donationErrorSource, Throwable th, DefaultConstructorMarker defaultConstructorMarker) {
            this(donationErrorSource, th);
        }

        private BadgeRedemptionError(DonationErrorSource donationErrorSource, Throwable th) {
            super(donationErrorSource, th, null);
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$TimeoutWaitingForTokenError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class TimeoutWaitingForTokenError extends BadgeRedemptionError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public TimeoutWaitingForTokenError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, new Exception("Timed out waiting for badge redemption to complete."), null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$FailedToValidateCredentialError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class FailedToValidateCredentialError extends BadgeRedemptionError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public FailedToValidateCredentialError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, new Exception("Failed to validate credential from server."), null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }

        /* compiled from: DonationError.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError$GenericError;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$BadgeRedemptionError;", PushDatabase.SOURCE_E164, "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class GenericError extends BadgeRedemptionError {
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public GenericError(DonationErrorSource donationErrorSource) {
                super(donationErrorSource, new Exception("Failed to add badge to account."), null);
                Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            }
        }
    }

    /* compiled from: DonationError.kt */
    @Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0003\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007J\u0010\u0010\r\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007J\u0010\u0010\u000e\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007J\u0016\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\n0\u00102\u0006\u0010\u0011\u001a\u00020\bH\u0007J\u0018\u0010\u0012\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u0018\u0010\u0015\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u0010\u0010\u0016\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007J\u0010\u0010\u0017\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007J\u0010\u0010\u0018\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007J\u0018\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\nH\u0007J\u0010\u0010\u001e\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\bH\u0007R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000R \u0010\u0006\u001a\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "donationErrorSubjectSourceMap", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationError;", "badgeCredentialVerificationFailure", PushDatabase.SOURCE_E164, "genericBadgeRedemptionFailure", "genericPaymentFailure", "getErrorsForSource", "Lio/reactivex/rxjava3/core/Observable;", "donationErrorSource", "getGooglePayRequestTokenError", "throwable", "", "getPaymentSetupError", "invalidCurrencyForOneTimeDonation", "oneTimeDonationAmountTooLarge", "oneTimeDonationAmountTooSmall", "routeDonationError", "", "context", "Landroid/content/Context;", "error", "timeoutWaitingForToken", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Observable<DonationError> getErrorsForSource(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, "donationErrorSource");
            Object obj = DonationError.donationErrorSubjectSourceMap.get(donationErrorSource);
            Intrinsics.checkNotNull(obj);
            return (Observable) obj;
        }

        @JvmStatic
        public final void routeDonationError(Context context, DonationError donationError) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(donationError, "error");
            Object obj = DonationError.donationErrorSubjectSourceMap.get(donationError.getSource());
            Intrinsics.checkNotNull(obj);
            Subject subject = (Subject) obj;
            if (subject.hasObservers()) {
                String str = DonationError.TAG;
                Log.i(str, "Routing donation error to subject " + donationError.getSource() + " dialog", donationError);
                subject.onNext(donationError);
                return;
            }
            String str2 = DonationError.TAG;
            Log.i(str2, "Routing donation error to subject " + donationError.getSource() + " notification", donationError);
            DonationErrorNotifications.INSTANCE.displayErrorNotification(context, donationError);
        }

        @JvmStatic
        public final DonationError getGooglePayRequestTokenError(DonationErrorSource donationErrorSource, Throwable th) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            Intrinsics.checkNotNullParameter(th, "throwable");
            return new GooglePayError.RequestTokenError(donationErrorSource, th);
        }

        @JvmStatic
        public final DonationError getPaymentSetupError(DonationErrorSource donationErrorSource, Throwable th) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            Intrinsics.checkNotNullParameter(th, "throwable");
            if (!(th instanceof StripeError.PostError)) {
                return new PaymentSetupError.GenericError(donationErrorSource, th);
            }
            StripeError.PostError postError = (StripeError.PostError) th;
            StripeDeclineCode declineCode = postError.getDeclineCode();
            String errorCode = postError.getErrorCode();
            if (declineCode != null) {
                return new PaymentSetupError.DeclinedError(donationErrorSource, th, declineCode);
            }
            if (errorCode != null) {
                return new PaymentSetupError.CodedError(donationErrorSource, th, errorCode);
            }
            return new PaymentSetupError.GenericError(donationErrorSource, th);
        }

        @JvmStatic
        public final DonationError oneTimeDonationAmountTooSmall(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new OneTimeDonationError.AmountTooSmallError(donationErrorSource);
        }

        @JvmStatic
        public final DonationError oneTimeDonationAmountTooLarge(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new OneTimeDonationError.AmountTooLargeError(donationErrorSource);
        }

        @JvmStatic
        public final DonationError invalidCurrencyForOneTimeDonation(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new OneTimeDonationError.InvalidCurrencyError(donationErrorSource);
        }

        @JvmStatic
        public final DonationError timeoutWaitingForToken(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new BadgeRedemptionError.TimeoutWaitingForTokenError(donationErrorSource);
        }

        @JvmStatic
        public final DonationError genericBadgeRedemptionFailure(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new BadgeRedemptionError.GenericError(donationErrorSource);
        }

        @JvmStatic
        public final DonationError badgeCredentialVerificationFailure(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new BadgeRedemptionError.FailedToValidateCredentialError(donationErrorSource);
        }

        @JvmStatic
        public final DonationError genericPaymentFailure(DonationErrorSource donationErrorSource) {
            Intrinsics.checkNotNullParameter(donationErrorSource, PushDatabase.SOURCE_E164);
            return new PaymentProcessingError.GenericError(donationErrorSource);
        }
    }

    static {
        Companion = new Companion(null);
        TAG = Log.tag(DonationError.class);
        DonationErrorSource[] values = DonationErrorSource.values();
        LinkedHashMap linkedHashMap = new LinkedHashMap(RangesKt___RangesKt.coerceAtLeast(MapsKt__MapsJVMKt.mapCapacity(values.length), 16));
        for (DonationErrorSource donationErrorSource : values) {
            Pair pair = TuplesKt.to(donationErrorSource, PublishSubject.create());
            linkedHashMap.put(pair.getFirst(), pair.getSecond());
        }
        donationErrorSubjectSourceMap = linkedHashMap;
    }
}
