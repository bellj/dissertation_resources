package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.widget.TextView;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.emoji.EmojiProvider;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiDrawInfo;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiParser;
import org.thoughtcrime.securesms.emoji.EmojiPageCache;
import org.thoughtcrime.securesms.emoji.EmojiSource;
import org.thoughtcrime.securesms.emoji.JumboEmoji;
import org.thoughtcrime.securesms.util.DeviceProperties;
import org.thoughtcrime.securesms.util.FutureTaskListener;

/* loaded from: classes4.dex */
public class EmojiProvider {
    private static final Paint PAINT = new Paint(3);
    private static final String TAG = Log.tag(EmojiProvider.class);

    public static EmojiParser.CandidateList getCandidates(CharSequence charSequence) {
        if (charSequence == null) {
            return null;
        }
        return new EmojiParser(EmojiSource.getLatest().getEmojiTree()).findCandidates(charSequence);
    }

    public static Spannable emojify(CharSequence charSequence, TextView textView, boolean z) {
        if (textView.isInEditMode()) {
            return null;
        }
        return emojify(getCandidates(charSequence), charSequence, textView, z);
    }

    public static Spannable emojify(EmojiParser.CandidateList candidateList, CharSequence charSequence, TextView textView, boolean z) {
        if (candidateList == null || charSequence == null || textView.isInEditMode()) {
            return null;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Iterator<EmojiParser.Candidate> it = candidateList.iterator();
        while (it.hasNext()) {
            EmojiParser.Candidate next = it.next();
            Drawable emojiDrawable = getEmojiDrawable(textView.getContext(), next.getDrawInfo(), new Runnable(textView) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiProvider$$ExternalSyntheticLambda2
                public final /* synthetic */ TextView f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    this.f$0.requestLayout();
                }
            }, z);
            if (emojiDrawable != null) {
                spannableStringBuilder.setSpan(new EmojiSpan(emojiDrawable, textView), next.getStartIndex(), next.getEndIndex(), 33);
            }
        }
        return spannableStringBuilder;
    }

    public static Spannable emojify(Context context, EmojiParser.CandidateList candidateList, CharSequence charSequence, Paint paint, boolean z, boolean z2) {
        Drawable drawable;
        if (candidateList == null || charSequence == null) {
            return null;
        }
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(charSequence);
        Iterator<EmojiParser.Candidate> it = candidateList.iterator();
        while (it.hasNext()) {
            EmojiParser.Candidate next = it.next();
            if (z) {
                drawable = getEmojiDrawableSync(context, next.getDrawInfo(), z2);
            } else {
                drawable = getEmojiDrawable(context, next.getDrawInfo(), null, z2);
            }
            if (drawable != null) {
                spannableStringBuilder.setSpan(new EmojiSpan(context, drawable, paint), next.getStartIndex(), next.getEndIndex(), 33);
            }
        }
        return spannableStringBuilder;
    }

    public static Drawable getEmojiDrawable(Context context, CharSequence charSequence) {
        return getEmojiDrawable(context, charSequence, false);
    }

    public static Drawable getEmojiDrawable(Context context, CharSequence charSequence, boolean z) {
        if (TextUtils.isEmpty(charSequence)) {
            return null;
        }
        return getEmojiDrawable(context, EmojiSource.getLatest().getEmojiTree().getEmoji(charSequence, 0, charSequence.length()), null, z);
    }

    private static Drawable getEmojiDrawable(Context context, EmojiDrawInfo emojiDrawInfo, final Runnable runnable, boolean z) {
        if (emojiDrawInfo == null) {
            return null;
        }
        int i = DeviceProperties.isLowMemoryDevice(context) ? 2 : 1;
        final EmojiDrawable emojiDrawable = new EmojiDrawable(EmojiSource.getLatest(), emojiDrawInfo, i);
        final AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        EmojiPageCache.LoadResult load = EmojiPageCache.INSTANCE.load(context, emojiDrawInfo.getPage(), i);
        if (load instanceof EmojiPageCache.LoadResult.Immediate) {
            ThreadUtil.runOnMain(new Runnable(load) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiProvider$$ExternalSyntheticLambda0
                public final /* synthetic */ EmojiPageCache.LoadResult f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    EmojiProvider.lambda$getEmojiDrawable$0(EmojiProvider.EmojiDrawable.this, this.f$1);
                }
            });
        } else if (load instanceof EmojiPageCache.LoadResult.Async) {
            ((EmojiPageCache.LoadResult.Async) load).getTask().addListener(new FutureTaskListener<Bitmap>() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiProvider.1
                public void onSuccess(Bitmap bitmap) {
                    ThreadUtil.runOnMain(new EmojiProvider$1$$ExternalSyntheticLambda0(atomicBoolean, emojiDrawable, bitmap, runnable));
                }

                public static /* synthetic */ void lambda$onSuccess$0(AtomicBoolean atomicBoolean2, EmojiDrawable emojiDrawable2, Bitmap bitmap, Runnable runnable2) {
                    if (!atomicBoolean2.get()) {
                        emojiDrawable2.setBitmap(bitmap);
                        if (runnable2 != null) {
                            runnable2.run();
                        }
                    }
                }

                @Override // org.thoughtcrime.securesms.util.FutureTaskListener
                public void onFailure(ExecutionException executionException) {
                    Log.d(EmojiProvider.TAG, "Failed to load emoji bitmap resource", executionException);
                }
            });
        } else {
            throw new IllegalStateException("Unexpected subclass " + load.getClass());
        }
        if (z && emojiDrawInfo.getJumboSheet() != null) {
            JumboEmoji.LoadResult loadJumboEmoji = JumboEmoji.loadJumboEmoji(context, emojiDrawInfo);
            if (loadJumboEmoji instanceof JumboEmoji.LoadResult.Immediate) {
                ThreadUtil.runOnMain(new Runnable(atomicBoolean, emojiDrawable, loadJumboEmoji) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiProvider$$ExternalSyntheticLambda1
                    public final /* synthetic */ AtomicBoolean f$0;
                    public final /* synthetic */ EmojiProvider.EmojiDrawable f$1;
                    public final /* synthetic */ JumboEmoji.LoadResult f$2;

                    {
                        this.f$0 = r1;
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        EmojiProvider.lambda$getEmojiDrawable$1(this.f$0, this.f$1, this.f$2);
                    }
                });
            } else if (loadJumboEmoji instanceof JumboEmoji.LoadResult.Async) {
                ((JumboEmoji.LoadResult.Async) loadJumboEmoji).getTask().addListener(new FutureTaskListener<Bitmap>() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiProvider.2
                    public void onSuccess(Bitmap bitmap) {
                        ThreadUtil.runOnMain(new EmojiProvider$2$$ExternalSyntheticLambda0(atomicBoolean, emojiDrawable, bitmap, runnable));
                    }

                    public static /* synthetic */ void lambda$onSuccess$0(AtomicBoolean atomicBoolean2, EmojiDrawable emojiDrawable2, Bitmap bitmap, Runnable runnable2) {
                        atomicBoolean2.set(true);
                        emojiDrawable2.setSingleBitmap(bitmap);
                        if (runnable2 != null) {
                            runnable2.run();
                        }
                    }

                    @Override // org.thoughtcrime.securesms.util.FutureTaskListener
                    public void onFailure(ExecutionException executionException) {
                        if (executionException.getCause() instanceof JumboEmoji.CannotAutoDownload) {
                            Log.i(EmojiProvider.TAG, "Download restrictions are preventing jumbomoji use");
                        } else {
                            Log.d(EmojiProvider.TAG, "Failed to load jumbo emoji bitmap resource", executionException);
                        }
                    }
                });
            }
        }
        return emojiDrawable;
    }

    public static /* synthetic */ void lambda$getEmojiDrawable$0(EmojiDrawable emojiDrawable, EmojiPageCache.LoadResult loadResult) {
        emojiDrawable.setBitmap(((EmojiPageCache.LoadResult.Immediate) loadResult).getBitmap());
    }

    public static /* synthetic */ void lambda$getEmojiDrawable$1(AtomicBoolean atomicBoolean, EmojiDrawable emojiDrawable, JumboEmoji.LoadResult loadResult) {
        atomicBoolean.set(true);
        emojiDrawable.setSingleBitmap(((JumboEmoji.LoadResult.Immediate) loadResult).getBitmap());
    }

    private static Drawable getEmojiDrawableSync(Context context, EmojiDrawInfo emojiDrawInfo, boolean z) {
        ThreadUtil.assertNotMainThread();
        Bitmap bitmap = null;
        if (emojiDrawInfo == null) {
            return null;
        }
        int i = DeviceProperties.isLowMemoryDevice(context) ? 2 : 1;
        EmojiDrawable emojiDrawable = new EmojiDrawable(EmojiSource.getLatest(), emojiDrawInfo, i);
        if (z && emojiDrawInfo.getJumboSheet() != null) {
            JumboEmoji.LoadResult loadJumboEmoji = JumboEmoji.loadJumboEmoji(context, emojiDrawInfo);
            if (loadJumboEmoji instanceof JumboEmoji.LoadResult.Immediate) {
                bitmap = ((JumboEmoji.LoadResult.Immediate) loadJumboEmoji).getBitmap();
            } else if (loadJumboEmoji instanceof JumboEmoji.LoadResult.Async) {
                try {
                    bitmap = ((JumboEmoji.LoadResult.Async) loadJumboEmoji).getTask().get(10, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException | TimeoutException e) {
                    if (e.getCause() instanceof JumboEmoji.CannotAutoDownload) {
                        Log.i(TAG, "Download restrictions are preventing jumbomoji use");
                    } else {
                        Log.d(TAG, "Failed to load jumbo emoji bitmap resource", e);
                    }
                }
            }
            if (bitmap != null) {
                emojiDrawable.setSingleBitmap(bitmap);
            }
        }
        if (!z || bitmap == null) {
            EmojiPageCache.LoadResult load = EmojiPageCache.INSTANCE.load(context, emojiDrawInfo.getPage(), i);
            if (load instanceof EmojiPageCache.LoadResult.Immediate) {
                String str = TAG;
                Log.d(str, "Cached emoji page: " + emojiDrawInfo.getPage().getUri().toString());
                bitmap = ((EmojiPageCache.LoadResult.Immediate) load).getBitmap();
            } else if (load instanceof EmojiPageCache.LoadResult.Async) {
                String str2 = TAG;
                Log.d(str2, "Loading emoji page: " + emojiDrawInfo.getPage().getUri().toString());
                try {
                    bitmap = ((EmojiPageCache.LoadResult.Async) load).getTask().get(2, TimeUnit.SECONDS);
                } catch (InterruptedException | ExecutionException | TimeoutException e2) {
                    Log.d(TAG, "Failed to load emoji bitmap resource", e2);
                }
            } else {
                throw new IllegalStateException("Unexpected subclass " + load.getClass());
            }
            emojiDrawable.setBitmap(bitmap);
        }
        return emojiDrawable;
    }

    /* loaded from: classes4.dex */
    public static final class EmojiDrawable extends Drawable {
        private Bitmap bmp;
        private final Rect emojiBounds;
        private final float intrinsicHeight;
        private final float intrinsicWidth;
        private boolean isSingleBitmap;

        @Override // android.graphics.drawable.Drawable
        public int getOpacity() {
            return -3;
        }

        @Override // android.graphics.drawable.Drawable
        public void setAlpha(int i) {
        }

        @Override // android.graphics.drawable.Drawable
        public void setColorFilter(ColorFilter colorFilter) {
        }

        @Override // android.graphics.drawable.Drawable
        public int getIntrinsicWidth() {
            return (int) this.intrinsicWidth;
        }

        @Override // android.graphics.drawable.Drawable
        public int getIntrinsicHeight() {
            return (int) this.intrinsicHeight;
        }

        EmojiDrawable(EmojiSource emojiSource, EmojiDrawInfo emojiDrawInfo, int i) {
            float f = (float) i;
            float rawWidth = (((float) emojiSource.getMetrics().getRawWidth()) * emojiSource.getDecodeScale()) / f;
            this.intrinsicWidth = rawWidth;
            float rawHeight = (((float) emojiSource.getMetrics().getRawHeight()) * emojiSource.getDecodeScale()) / f;
            this.intrinsicHeight = rawHeight;
            int i2 = (int) rawWidth;
            int i3 = (int) rawHeight;
            int index = emojiDrawInfo.getIndex();
            int perRow = emojiSource.getMetrics().getPerRow();
            int i4 = (index % perRow) * i2;
            int i5 = (index / perRow) * i3;
            this.emojiBounds = new Rect(i4 + 1, i5 + 1, (i4 + i2) - 1, (i5 + i3) - 1);
        }

        @Override // android.graphics.drawable.Drawable
        public void draw(Canvas canvas) {
            Bitmap bitmap = this.bmp;
            if (bitmap != null) {
                canvas.drawBitmap(bitmap, this.isSingleBitmap ? null : this.emojiBounds, getBounds(), EmojiProvider.PAINT);
            }
        }

        public void setBitmap(Bitmap bitmap) {
            setBitmap(bitmap, false);
        }

        public void setSingleBitmap(Bitmap bitmap) {
            setBitmap(bitmap, true);
        }

        private void setBitmap(Bitmap bitmap, boolean z) {
            this.isSingleBitmap = z;
            Bitmap bitmap2 = this.bmp;
            if (bitmap2 == null || !bitmap2.sameAs(bitmap)) {
                this.bmp = bitmap;
                invalidateSelf();
            }
        }
    }
}
