package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import androidx.core.content.ContextCompat;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.views.LearnMoreTextView;

/* compiled from: LegacyGroupPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "State", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class LegacyGroupPreference {
    public static final LegacyGroupPreference INSTANCE = new LegacyGroupPreference();

    /* compiled from: LegacyGroupPreference.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0007\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;", "", "(Ljava/lang/String;I)V", "LEARN_MORE", "UPGRADE", "TOO_LARGE", "MMS_WARNING", "NONE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum State {
        LEARN_MORE,
        UPGRADE,
        TOO_LARGE,
        MMS_WARNING,
        NONE
    }

    private LegacyGroupPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new LegacyGroupPreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_legacy_group_preference));
    }

    /* compiled from: LegacyGroupPreference.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B7\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\tJ\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0000H\u0016R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000bR\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;", "onLearnMoreClick", "Lkotlin/Function0;", "", "onUpgradeClick", "onMmsWarningClick", "(Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getOnLearnMoreClick", "()Lkotlin/jvm/functions/Function0;", "getOnMmsWarningClick", "getOnUpgradeClick", "getState", "()Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Function0<Unit> onLearnMoreClick;
        private final Function0<Unit> onMmsWarningClick;
        private final Function0<Unit> onUpgradeClick;
        private final State state;

        public final State getState() {
            return this.state;
        }

        public final Function0<Unit> getOnLearnMoreClick() {
            return this.onLearnMoreClick;
        }

        public final Function0<Unit> getOnUpgradeClick() {
            return this.onUpgradeClick;
        }

        public final Function0<Unit> getOnMmsWarningClick() {
            return this.onMmsWarningClick;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(State state, Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(state, "state");
            Intrinsics.checkNotNullParameter(function0, "onLearnMoreClick");
            Intrinsics.checkNotNullParameter(function02, "onUpgradeClick");
            Intrinsics.checkNotNullParameter(function03, "onMmsWarningClick");
            this.state = state;
            this.onLearnMoreClick = function0;
            this.onUpgradeClick = function02;
            this.onMmsWarningClick = function03;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return this.state == model.state;
        }
    }

    /* compiled from: LegacyGroupPreference.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "groupInfoText", "Lorg/thoughtcrime/securesms/util/views/LearnMoreTextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final LearnMoreTextView groupInfoText;

        /* compiled from: LegacyGroupPreference.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[State.values().length];
                iArr[State.LEARN_MORE.ordinal()] = 1;
                iArr[State.UPGRADE.ordinal()] = 2;
                iArr[State.TOO_LARGE.ordinal()] = 3;
                iArr[State.MMS_WARNING.ordinal()] = 4;
                iArr[State.NONE.ordinal()] = 5;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.manage_group_info_text);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.manage_group_info_text)");
            this.groupInfoText = (LearnMoreTextView) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.itemView.setVisibility(0);
            this.groupInfoText.setLinkColor(ContextCompat.getColor(this.context, R.color.signal_text_primary));
            int i = WhenMappings.$EnumSwitchMapping$0[model.getState().ordinal()];
            if (i == 1) {
                this.groupInfoText.setText(R.string.ManageGroupActivity_legacy_group_learn_more);
                this.groupInfoText.setOnLinkClickListener(new LegacyGroupPreference$ViewHolder$$ExternalSyntheticLambda0(model));
                this.groupInfoText.setLearnMoreVisible(true);
            } else if (i == 2) {
                this.groupInfoText.setText(R.string.ManageGroupActivity_legacy_group_upgrade);
                this.groupInfoText.setOnLinkClickListener(new LegacyGroupPreference$ViewHolder$$ExternalSyntheticLambda1(model));
                this.groupInfoText.setLearnMoreVisible(true, R.string.ManageGroupActivity_upgrade_this_group);
            } else if (i == 3) {
                this.groupInfoText.setText(this.context.getString(R.string.ManageGroupActivity_legacy_group_too_large, Integer.valueOf(FeatureFlags.groupLimits().getHardLimit() - 1)));
                this.groupInfoText.setLearnMoreVisible(false);
            } else if (i == 4) {
                this.groupInfoText.setText(R.string.ManageGroupActivity_this_is_an_insecure_mms_group);
                this.groupInfoText.setOnLinkClickListener(new LegacyGroupPreference$ViewHolder$$ExternalSyntheticLambda2(model));
                this.groupInfoText.setLearnMoreVisible(true, R.string.ManageGroupActivity_invite_now);
            } else if (i == 5) {
                this.itemView.setVisibility(8);
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1210bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnLearnMoreClick().invoke();
        }

        /* renamed from: bind$lambda-1 */
        public static final void m1211bind$lambda1(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnUpgradeClick().invoke();
        }

        /* renamed from: bind$lambda-2 */
        public static final void m1212bind$lambda2(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnMmsWarningClick().invoke();
        }
    }
}
