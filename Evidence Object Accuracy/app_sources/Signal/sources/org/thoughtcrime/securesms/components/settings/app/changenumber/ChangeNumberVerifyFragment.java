package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.RequestVerificationCodeResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberVerifyFragment.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\u001a\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u000b\u001a\u0004\u0018\u00010\fH\u0016J\b\u0010\u0010\u001a\u00020\nH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX.¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberVerifyFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "requestingCaptcha", "", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "Landroid/view/View;", "requestCode", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberVerifyFragment extends LoggingFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private boolean requestingCaptcha;
    private ChangeNumberViewModel viewModel;

    public ChangeNumberVerifyFragment() {
        super(R.layout.fragment_change_phone_number_verify);
    }

    @Override // org.thoughtcrime.securesms.LoggingFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        this.viewModel = ChangeNumberUtil.getViewModel(this);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        toolbar.setTitle(R.string.ChangeNumberVerifyFragment__change_number);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberVerifyFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberVerifyFragment.$r8$lambda$xHvQx0XzvMX_DP9n3aJrmAY6G9k(ChangeNumberVerifyFragment.this, view2);
            }
        });
        View findViewById2 = view.findViewById(R.id.change_phone_number_verify_status);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.c…one_number_verify_status)");
        TextView textView = (TextView) findViewById2;
        Object[] objArr = new Object[1];
        ChangeNumberViewModel changeNumberViewModel = this.viewModel;
        ChangeNumberViewModel changeNumberViewModel2 = null;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        objArr[0] = changeNumberViewModel.getNumber().getFullFormattedNumber();
        textView.setText(getString(R.string.ChangeNumberVerifyFragment__verifying_s, objArr));
        if (this.requestingCaptcha) {
            ChangeNumberViewModel changeNumberViewModel3 = this.viewModel;
            if (changeNumberViewModel3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            } else {
                changeNumberViewModel2 = changeNumberViewModel3;
            }
            if (!changeNumberViewModel2.hasCaptchaToken()) {
                Toast.makeText(requireContext(), (int) R.string.ChangeNumberVerifyFragment__captcha_required, 0).show();
                FragmentKt.findNavController(this).navigateUp();
                return;
            }
        }
        requestCode();
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m617onViewCreated$lambda0(ChangeNumberVerifyFragment changeNumberVerifyFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberVerifyFragment, "this$0");
        FragmentKt.findNavController(changeNumberVerifyFragment).navigateUp();
    }

    private final void requestCode() {
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        ChangeNumberViewModel changeNumberViewModel = this.viewModel;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        Disposable subscribe = changeNumberViewModel.requestVerificationCode(VerifyAccountRepository.Mode.SMS_WITHOUT_LISTENER).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberVerifyFragment$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChangeNumberVerifyFragment.m616$r8$lambda$N_NlUVfxiSeDMvb_TUOCkyuGl4(ChangeNumberVerifyFragment.this, (RequestVerificationCodeResponseProcessor) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.requestVerific…p()\n          }\n        }");
        lifecycleDisposable.add(subscribe);
    }

    /* renamed from: requestCode$lambda-1 */
    public static final void m618requestCode$lambda1(ChangeNumberVerifyFragment changeNumberVerifyFragment, RequestVerificationCodeResponseProcessor requestVerificationCodeResponseProcessor) {
        Intrinsics.checkNotNullParameter(changeNumberVerifyFragment, "this$0");
        if (requestVerificationCodeResponseProcessor.hasResult()) {
            SafeNavigation.safeNavigate(FragmentKt.findNavController(changeNumberVerifyFragment), (int) R.id.action_changePhoneNumberVerifyFragment_to_changeNumberEnterCodeFragment);
        } else if (requestVerificationCodeResponseProcessor.localRateLimit()) {
            Log.i(ChangeNumberVerifyFragmentKt.TAG, "Unable to request sms code due to local rate limit");
            SafeNavigation.safeNavigate(FragmentKt.findNavController(changeNumberVerifyFragment), (int) R.id.action_changePhoneNumberVerifyFragment_to_changeNumberEnterCodeFragment);
        } else if (requestVerificationCodeResponseProcessor.captchaRequired()) {
            Log.i(ChangeNumberVerifyFragmentKt.TAG, "Unable to request sms code due to captcha required");
            SafeNavigation.safeNavigate(FragmentKt.findNavController(changeNumberVerifyFragment), (int) R.id.action_changePhoneNumberVerifyFragment_to_captchaFragment, ChangeNumberUtil.INSTANCE.getCaptchaArguments());
            changeNumberVerifyFragment.requestingCaptcha = true;
        } else if (requestVerificationCodeResponseProcessor.rateLimit()) {
            Log.i(ChangeNumberVerifyFragmentKt.TAG, "Unable to request sms code due to rate limit");
            Toast.makeText(changeNumberVerifyFragment.requireContext(), (int) R.string.RegistrationActivity_rate_limited_to_service, 1).show();
            FragmentKt.findNavController(changeNumberVerifyFragment).navigateUp();
        } else {
            Log.w(ChangeNumberVerifyFragmentKt.TAG, "Unable to request sms code", requestVerificationCodeResponseProcessor.getError());
            Toast.makeText(changeNumberVerifyFragment.requireContext(), (int) R.string.RegistrationActivity_unable_to_connect_to_service, 1).show();
            FragmentKt.findNavController(changeNumberVerifyFragment).navigateUp();
        }
    }
}
