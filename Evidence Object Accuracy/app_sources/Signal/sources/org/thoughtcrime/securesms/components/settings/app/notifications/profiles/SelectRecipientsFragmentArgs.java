package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class SelectRecipientsFragmentArgs {
    private final HashMap arguments;

    private SelectRecipientsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private SelectRecipientsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static SelectRecipientsFragmentArgs fromBundle(Bundle bundle) {
        SelectRecipientsFragmentArgs selectRecipientsFragmentArgs = new SelectRecipientsFragmentArgs();
        bundle.setClassLoader(SelectRecipientsFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("profileId")) {
            selectRecipientsFragmentArgs.arguments.put("profileId", Long.valueOf(bundle.getLong("profileId")));
            RecipientId[] recipientIdArr = null;
            if (bundle.containsKey("currentSelection")) {
                Parcelable[] parcelableArray = bundle.getParcelableArray("currentSelection");
                if (parcelableArray != null) {
                    recipientIdArr = new RecipientId[parcelableArray.length];
                    System.arraycopy(parcelableArray, 0, recipientIdArr, 0, parcelableArray.length);
                }
                selectRecipientsFragmentArgs.arguments.put("currentSelection", recipientIdArr);
            } else {
                selectRecipientsFragmentArgs.arguments.put("currentSelection", null);
            }
            return selectRecipientsFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"profileId\" is missing and does not have an android:defaultValue");
    }

    public long getProfileId() {
        return ((Long) this.arguments.get("profileId")).longValue();
    }

    public RecipientId[] getCurrentSelection() {
        return (RecipientId[]) this.arguments.get("currentSelection");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("profileId")) {
            bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
        }
        if (this.arguments.containsKey("currentSelection")) {
            bundle.putParcelableArray("currentSelection", (RecipientId[]) this.arguments.get("currentSelection"));
        } else {
            bundle.putParcelableArray("currentSelection", null);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SelectRecipientsFragmentArgs selectRecipientsFragmentArgs = (SelectRecipientsFragmentArgs) obj;
        if (this.arguments.containsKey("profileId") == selectRecipientsFragmentArgs.arguments.containsKey("profileId") && getProfileId() == selectRecipientsFragmentArgs.getProfileId() && this.arguments.containsKey("currentSelection") == selectRecipientsFragmentArgs.arguments.containsKey("currentSelection")) {
            return getCurrentSelection() == null ? selectRecipientsFragmentArgs.getCurrentSelection() == null : getCurrentSelection().equals(selectRecipientsFragmentArgs.getCurrentSelection());
        }
        return false;
    }

    public int hashCode() {
        return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + Arrays.hashCode(getCurrentSelection());
    }

    public String toString() {
        return "SelectRecipientsFragmentArgs{profileId=" + getProfileId() + ", currentSelection=" + getCurrentSelection() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(SelectRecipientsFragmentArgs selectRecipientsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(selectRecipientsFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public SelectRecipientsFragmentArgs build() {
            return new SelectRecipientsFragmentArgs(this.arguments);
        }

        public Builder setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public Builder setCurrentSelection(RecipientId[] recipientIdArr) {
            this.arguments.put("currentSelection", recipientIdArr);
            return this;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public RecipientId[] getCurrentSelection() {
            return (RecipientId[]) this.arguments.get("currentSelection");
        }
    }
}
