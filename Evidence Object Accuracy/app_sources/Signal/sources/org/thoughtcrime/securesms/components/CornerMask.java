package org.thoughtcrime.securesms.components;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.view.View;

/* loaded from: classes4.dex */
public class CornerMask {
    private final RectF bounds;
    private final Paint clearPaint;
    private final Path corners;
    private final Path outline;
    private final float[] radii = new float[8];

    public CornerMask(View view) {
        Paint paint = new Paint();
        this.clearPaint = paint;
        this.outline = new Path();
        this.corners = new Path();
        this.bounds = new RectF();
        view.setLayerType(2, null);
        paint.setColor(-16777216);
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
    }

    public void mask(Canvas canvas) {
        this.bounds.set(canvas.getClipBounds());
        this.corners.reset();
        this.corners.addRoundRect(this.bounds, this.radii, Path.Direction.CW);
        this.outline.reset();
        this.outline.addRect(this.bounds, Path.Direction.CW);
        this.outline.op(this.corners, Path.Op.DIFFERENCE);
        canvas.drawPath(this.outline, this.clearPaint);
    }

    public void setRadius(int i) {
        setRadii(i, i, i, i);
    }

    public void setRadii(int i, int i2, int i3, int i4) {
        float[] fArr = this.radii;
        float f = (float) i;
        fArr[1] = f;
        fArr[0] = f;
        float f2 = (float) i2;
        fArr[3] = f2;
        fArr[2] = f2;
        float f3 = (float) i3;
        fArr[5] = f3;
        fArr[4] = f3;
        float f4 = (float) i4;
        fArr[7] = f4;
        fArr[6] = f4;
    }

    public void setRadii(float f, float f2, float f3, float f4) {
        float[] fArr = this.radii;
        fArr[1] = f;
        fArr[0] = f;
        fArr[3] = f2;
        fArr[2] = f2;
        fArr[5] = f3;
        fArr[4] = f3;
        fArr[7] = f4;
        fArr[6] = f4;
    }

    public void setTopLeftRadius(int i) {
        float[] fArr = this.radii;
        float f = (float) i;
        fArr[1] = f;
        fArr[0] = f;
    }

    public void setTopRightRadius(int i) {
        float[] fArr = this.radii;
        float f = (float) i;
        fArr[3] = f;
        fArr[2] = f;
    }

    public void setBottomRightRadius(int i) {
        float[] fArr = this.radii;
        float f = (float) i;
        fArr[5] = f;
        fArr[4] = f;
    }

    public void setBottomLeftRadius(int i) {
        float[] fArr = this.radii;
        float f = (float) i;
        fArr[7] = f;
        fArr[6] = f;
    }

    public float[] getRadii() {
        return this.radii;
    }
}
