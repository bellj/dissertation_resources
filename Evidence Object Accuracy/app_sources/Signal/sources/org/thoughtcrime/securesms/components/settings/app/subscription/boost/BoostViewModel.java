package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.content.Intent;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function3;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.PublishSubject;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.StringUtil;
import org.signal.core.util.logging.Log;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState;
import org.thoughtcrime.securesms.database.EmojiSearchDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;
import org.thoughtcrime.securesms.util.PlatformCurrencyUtil;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: BoostViewModel.kt */
@Metadata(d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u0000 12\u00020\u0001:\u0003012B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001f0\u001eJ \u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020\u00072\u0006\u0010#\u001a\u00020\u00072\b\u0010$\u001a\u0004\u0018\u00010%J\b\u0010&\u001a\u00020!H\u0014J\u0006\u0010'\u001a\u00020!J\u000e\u0010(\u001a\u00020!2\u0006\u0010)\u001a\u00020\u001fJ\u0006\u0010*\u001a\u00020!J\u000e\u0010+\u001a\u00020!2\u0006\u0010,\u001a\u00020\u001fJ\u0006\u0010-\u001a\u00020!J\u000e\u0010.\u001a\u00020!2\u0006\u0010/\u001a\u00020\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000eX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0011¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00180\u0017¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00180\u001cX\u0004¢\u0006\u0002\n\u0000¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel;", "Landroidx/lifecycle/ViewModel;", "boostRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostRepository;", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "fetchTokenRequestCode", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;I)V", "boostToPurchase", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "eventPublisher", "Lio/reactivex/rxjava3/subjects/PublishSubject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "events", "Lio/reactivex/rxjava3/core/Observable;", "getEvents", "()Lio/reactivex/rxjava3/core/Observable;", "networkDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getSupportedCurrencyCodes", "", "", "onActivityResult", "", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCleared", "refresh", "requestTokenFromGooglePay", EmojiSearchDatabase.LABEL, "retry", "setCustomAmount", "rawAmount", "setCustomAmountFocused", "setSelectedBoost", "boost", "BoostInfo", "Companion", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostViewModel extends ViewModel {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(BoostViewModel.class);
    private final BoostRepository boostRepository;
    private Boost boostToPurchase;
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final DonationPaymentRepository donationPaymentRepository;
    private final PublishSubject<DonationEvent> eventPublisher;
    private final Observable<DonationEvent> events;
    private final int fetchTokenRequestCode;
    private final Disposable networkDisposable;
    private final LiveData<BoostState> state;
    private final Store<BoostState> store;

    public BoostViewModel(BoostRepository boostRepository, DonationPaymentRepository donationPaymentRepository, int i) {
        Intrinsics.checkNotNullParameter(boostRepository, "boostRepository");
        Intrinsics.checkNotNullParameter(donationPaymentRepository, "donationPaymentRepository");
        this.boostRepository = boostRepository;
        this.donationPaymentRepository = donationPaymentRepository;
        this.fetchTokenRequestCode = i;
        Store<BoostState> store = new Store<>(new BoostState(null, SignalStore.donationsValues().getOneTimeCurrency(), false, null, null, null, false, null, null, 509, null));
        this.store = store;
        PublishSubject<DonationEvent> create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.eventPublisher = create;
        LiveData<BoostState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Observable<DonationEvent> observeOn = create.observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "eventPublisher.observeOn…dSchedulers.mainThread())");
        this.events = observeOn;
        Disposable subscribe = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                BoostViewModel.m938_init_$lambda0(BoostViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "InternetConnectionObserv…retry()\n        }\n      }");
        this.networkDisposable = subscribe;
    }

    public final LiveData<BoostState> getState() {
        return this.state;
    }

    public final Observable<DonationEvent> getEvents() {
        return this.events;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m938_init_$lambda0(BoostViewModel boostViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(boostViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
        if (bool.booleanValue()) {
            boostViewModel.retry();
        }
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.networkDisposable.dispose();
        this.disposables.dispose();
    }

    public final List<String> getSupportedCurrencyCodes() {
        return this.store.getState().getSupportedCurrencyCodes();
    }

    public final void retry() {
        if (!this.disposables.isDisposed() && this.store.getState().getStage() == BoostState.Stage.FAILURE) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda1
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return BoostViewModel.m941retry$lambda1((BoostState) obj);
                }
            });
            refresh();
        }
    }

    /* renamed from: retry$lambda-1 */
    public static final BoostState m941retry$lambda1(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.INIT, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    public final void refresh() {
        this.disposables.clear();
        Observable<Currency> observableOneTimeCurrency = SignalStore.donationsValues().getObservableOneTimeCurrency();
        Single<Map<Currency, List<Boost>>> boosts = this.boostRepository.getBoosts();
        Single<Badge> boostBadge = this.boostRepository.getBoostBadge();
        CompositeDisposable compositeDisposable = this.disposables;
        Observable combineLatest = Observable.combineLatest(observableOneTimeCurrency, boosts.toObservable(), boostBadge.toObservable(), new Function3() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function3
            public final Object apply(Object obj, Object obj2, Object obj3) {
                return BoostViewModel.m939refresh$lambda2((Currency) obj, (Map) obj2, (Badge) obj3);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(currencyOb…dge, boostMap.keys)\n    }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(combineLatest, new BoostViewModel$refresh$2(this), (Function0) null, new BoostViewModel$refresh$3(this), 2, (Object) null));
        DisposableKt.plusAssign(this.disposables, SubscribersKt.subscribeBy$default(observableOneTimeCurrency, (Function1) null, (Function0) null, new BoostViewModel$refresh$4(this), 3, (Object) null));
    }

    /* renamed from: refresh$lambda-2 */
    public static final BoostInfo m939refresh$lambda2(Currency currency, Map map, Badge badge) {
        List list;
        Intrinsics.checkNotNullExpressionValue(map, "boostMap");
        if (map.containsKey(currency)) {
            Object obj = map.get(currency);
            Intrinsics.checkNotNull(obj);
            list = (List) obj;
        } else {
            SignalStore.donationsValues().setOneTimeCurrency(PlatformCurrencyUtil.INSTANCE.getUSD());
            list = CollectionsKt__CollectionsKt.emptyList();
        }
        Intrinsics.checkNotNullExpressionValue(badge, "badge");
        return new BoostInfo(list, (Boost) list.get(2), badge, map.keySet());
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        Boost boost = this.boostToPurchase;
        this.boostToPurchase = null;
        this.donationPaymentRepository.onActivityResult(i, i2, intent, this.fetchTokenRequestCode, new BoostViewModel$onActivityResult$1(boost, this));
    }

    public final void requestTokenFromGooglePay(String str) {
        Boost boost;
        Intrinsics.checkNotNullParameter(str, EmojiSearchDatabase.LABEL);
        BoostState state = this.store.getState();
        Intrinsics.checkNotNullExpressionValue(state, "store.state");
        BoostState boostState = state;
        if (boostState.getSelectedBoost() != null) {
            this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda4
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return BoostViewModel.m940requestTokenFromGooglePay$lambda3((BoostState) obj);
                }
            });
            if (boostState.isCustomAmountFocused()) {
                String str2 = TAG;
                Log.d(str2, "Boosting with custom amount " + boostState.getCustomAmount());
                boost = new Boost(boostState.getCustomAmount());
            } else {
                String str3 = TAG;
                Log.d(str3, "Boosting with preset amount " + boostState.getSelectedBoost().getPrice());
                boost = boostState.getSelectedBoost();
            }
            this.boostToPurchase = boost;
            this.donationPaymentRepository.requestTokenFromGooglePay(boost.getPrice(), str, this.fetchTokenRequestCode);
        }
    }

    /* renamed from: requestTokenFromGooglePay$lambda-3 */
    public static final BoostState m940requestTokenFromGooglePay$lambda3(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.TOKEN_REQUEST, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    public final void setSelectedBoost(Boost boost) {
        Intrinsics.checkNotNullParameter(boost, "boost");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel.m944setSelectedBoost$lambda4(Boost.this, (BoostState) obj);
            }
        });
    }

    /* renamed from: setSelectedBoost$lambda-4 */
    public static final BoostState m944setSelectedBoost$lambda4(Boost boost, BoostState boostState) {
        Intrinsics.checkNotNullParameter(boost, "$boost");
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : boost, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : null, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    public final void setCustomAmount(String str) {
        BigDecimal bigDecimal;
        Number parse;
        Intrinsics.checkNotNullParameter(str, "rawAmount");
        String stripBidiIndicator = StringUtil.stripBidiIndicator(str);
        Intrinsics.checkNotNullExpressionValue(stripBidiIndicator, "stripBidiIndicator(rawAmount)");
        if ((stripBidiIndicator.length() == 0) || Intrinsics.areEqual(stripBidiIndicator, String.valueOf(DecimalFormatSymbols.getInstance().getDecimalSeparator()))) {
            bigDecimal = BigDecimal.ZERO;
            Intrinsics.checkNotNullExpressionValue(bigDecimal, "{\n      BigDecimal.ZERO\n    }");
        } else {
            NumberFormat instance = DecimalFormat.getInstance();
            if (instance != null) {
                DecimalFormat decimalFormat = (DecimalFormat) instance;
                decimalFormat.setParseBigDecimal(true);
                try {
                    parse = decimalFormat.parse(stripBidiIndicator);
                } catch (NumberFormatException unused) {
                    bigDecimal = BigDecimal.ZERO;
                }
                if (parse != null) {
                    bigDecimal = (BigDecimal) parse;
                    Intrinsics.checkNotNullExpressionValue(bigDecimal, "{\n      val decimalForma…ecimal.ZERO\n      }\n    }");
                } else {
                    throw new NullPointerException("null cannot be cast to non-null type java.math.BigDecimal");
                }
            } else {
                throw new NullPointerException("null cannot be cast to non-null type java.text.DecimalFormat");
            }
        }
        this.store.update(new Function(bigDecimal) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda3
            public final /* synthetic */ BigDecimal f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel.m942setCustomAmount$lambda5(this.f$0, (BoostState) obj);
            }
        });
    }

    /* renamed from: setCustomAmount$lambda-5 */
    public static final BoostState m942setCustomAmount$lambda5(BigDecimal bigDecimal, BoostState boostState) {
        Intrinsics.checkNotNullParameter(bigDecimal, "$bigDecimalAmount");
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : new FiatMoney(bigDecimal, boostState.getCustomAmount().getCurrency()), (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : null, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    /* renamed from: setCustomAmountFocused$lambda-6 */
    public static final BoostState m943setCustomAmountFocused$lambda6(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : true, (r20 & 128) != 0 ? boostState.stage : null, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    public final void setCustomAmountFocused() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel.m943setCustomAmountFocused$lambda6((BoostState) obj);
            }
        });
    }

    /* compiled from: BoostViewModel.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u000f\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B3\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\u0002\u0010\u000bJ\u000f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003HÆ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0004HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0007HÆ\u0003J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0003J?\u0010\u0018\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00042\b\b\u0002\u0010\u0006\u001a\u00020\u00072\u000e\b\u0002\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\tHÆ\u0001J\u0013\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001c\u001a\u00020\u001dHÖ\u0001J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006 "}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel$BoostInfo;", "", "boosts", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "defaultBoost", "boostBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "supportedCurrencies", "", "Ljava/util/Currency;", "(Ljava/util/List;Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;Lorg/thoughtcrime/securesms/badges/models/Badge;Ljava/util/Set;)V", "getBoostBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getBoosts", "()Ljava/util/List;", "getDefaultBoost", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "getSupportedCurrencies", "()Ljava/util/Set;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class BoostInfo {
        private final Badge boostBadge;
        private final List<Boost> boosts;
        private final Boost defaultBoost;
        private final Set<Currency> supportedCurrencies;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$BoostInfo */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ BoostInfo copy$default(BoostInfo boostInfo, List list, Boost boost, Badge badge, Set set, int i, Object obj) {
            if ((i & 1) != 0) {
                list = boostInfo.boosts;
            }
            if ((i & 2) != 0) {
                boost = boostInfo.defaultBoost;
            }
            if ((i & 4) != 0) {
                badge = boostInfo.boostBadge;
            }
            if ((i & 8) != 0) {
                set = boostInfo.supportedCurrencies;
            }
            return boostInfo.copy(list, boost, badge, set);
        }

        public final List<Boost> component1() {
            return this.boosts;
        }

        public final Boost component2() {
            return this.defaultBoost;
        }

        public final Badge component3() {
            return this.boostBadge;
        }

        public final Set<Currency> component4() {
            return this.supportedCurrencies;
        }

        public final BoostInfo copy(List<Boost> list, Boost boost, Badge badge, Set<Currency> set) {
            Intrinsics.checkNotNullParameter(list, "boosts");
            Intrinsics.checkNotNullParameter(badge, "boostBadge");
            Intrinsics.checkNotNullParameter(set, "supportedCurrencies");
            return new BoostInfo(list, boost, badge, set);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof BoostInfo)) {
                return false;
            }
            BoostInfo boostInfo = (BoostInfo) obj;
            return Intrinsics.areEqual(this.boosts, boostInfo.boosts) && Intrinsics.areEqual(this.defaultBoost, boostInfo.defaultBoost) && Intrinsics.areEqual(this.boostBadge, boostInfo.boostBadge) && Intrinsics.areEqual(this.supportedCurrencies, boostInfo.supportedCurrencies);
        }

        public int hashCode() {
            int hashCode = this.boosts.hashCode() * 31;
            Boost boost = this.defaultBoost;
            return ((((hashCode + (boost == null ? 0 : boost.hashCode())) * 31) + this.boostBadge.hashCode()) * 31) + this.supportedCurrencies.hashCode();
        }

        public String toString() {
            return "BoostInfo(boosts=" + this.boosts + ", defaultBoost=" + this.defaultBoost + ", boostBadge=" + this.boostBadge + ", supportedCurrencies=" + this.supportedCurrencies + ')';
        }

        public BoostInfo(List<Boost> list, Boost boost, Badge badge, Set<Currency> set) {
            Intrinsics.checkNotNullParameter(list, "boosts");
            Intrinsics.checkNotNullParameter(badge, "boostBadge");
            Intrinsics.checkNotNullParameter(set, "supportedCurrencies");
            this.boosts = list;
            this.defaultBoost = boost;
            this.boostBadge = badge;
            this.supportedCurrencies = set;
        }

        public final Badge getBoostBadge() {
            return this.boostBadge;
        }

        public final List<Boost> getBoosts() {
            return this.boosts;
        }

        public final Boost getDefaultBoost() {
            return this.defaultBoost;
        }

        public final Set<Currency> getSupportedCurrencies() {
            return this.supportedCurrencies;
        }
    }

    /* compiled from: BoostViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "boostRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostRepository;", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "fetchTokenRequestCode", "", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostRepository;Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;I)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final BoostRepository boostRepository;
        private final DonationPaymentRepository donationPaymentRepository;
        private final int fetchTokenRequestCode;

        public Factory(BoostRepository boostRepository, DonationPaymentRepository donationPaymentRepository, int i) {
            Intrinsics.checkNotNullParameter(boostRepository, "boostRepository");
            Intrinsics.checkNotNullParameter(donationPaymentRepository, "donationPaymentRepository");
            this.boostRepository = boostRepository;
            this.donationPaymentRepository = donationPaymentRepository;
            this.fetchTokenRequestCode = i;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new BoostViewModel(this.boostRepository, this.donationPaymentRepository, this.fetchTokenRequestCode));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }

    /* compiled from: BoostViewModel.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
