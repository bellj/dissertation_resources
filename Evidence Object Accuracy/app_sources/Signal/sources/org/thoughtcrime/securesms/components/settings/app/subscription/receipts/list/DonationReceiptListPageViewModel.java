package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: DonationReceiptListPageViewModel.kt */
@Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0012B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\u0010\u001a\u00020\u0011H\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageViewModel;", "Landroidx/lifecycle/ViewModel;", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageRepository;", "(Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListPageViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final LiveData<DonationReceiptListPageState> state;
    private final Store<DonationReceiptListPageState> store;

    public DonationReceiptListPageViewModel(DonationReceiptRecord.Type type, DonationReceiptListPageRepository donationReceiptListPageRepository) {
        Intrinsics.checkNotNullParameter(donationReceiptListPageRepository, "repository");
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        Store<DonationReceiptListPageState> store = new Store<>(new DonationReceiptListPageState(null, false, 3, null));
        this.store = store;
        LiveData<DonationReceiptListPageState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Disposable subscribe = donationReceiptListPageRepository.getRecords(type).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationReceiptListPageViewModel.m1027_init_$lambda1(DonationReceiptListPageViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getRecords(ty…      )\n        }\n      }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    public final LiveData<DonationReceiptListPageState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m1027_init_$lambda1(DonationReceiptListPageViewModel donationReceiptListPageViewModel, List list) {
        Intrinsics.checkNotNullParameter(donationReceiptListPageViewModel, "this$0");
        donationReceiptListPageViewModel.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return DonationReceiptListPageViewModel.m1028lambda1$lambda0(this.f$0, (DonationReceiptListPageState) obj);
            }
        });
    }

    /* renamed from: lambda-1$lambda-0 */
    public static final DonationReceiptListPageState m1028lambda1$lambda0(List list, DonationReceiptListPageState donationReceiptListPageState) {
        Intrinsics.checkNotNullExpressionValue(list, "records");
        return donationReceiptListPageState.copy(list, true);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    /* compiled from: DonationReceiptListPageViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageRepository;", "(Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DonationReceiptListPageRepository repository;
        private final DonationReceiptRecord.Type type;

        public Factory(DonationReceiptRecord.Type type, DonationReceiptListPageRepository donationReceiptListPageRepository) {
            Intrinsics.checkNotNullParameter(donationReceiptListPageRepository, "repository");
            this.type = type;
            this.repository = donationReceiptListPageRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new DonationReceiptListPageViewModel(this.type, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageViewModel.Factory.create");
        }
    }
}
