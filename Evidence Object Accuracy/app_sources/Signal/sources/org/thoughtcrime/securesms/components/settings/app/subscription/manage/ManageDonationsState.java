package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import java.util.List;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: ManageDonationsState.kt */
@Metadata(d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u00002\u00020\u0001:\u0002\"#B5\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\u000f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\b0\u0007HÆ\u0003J\t\u0010\u0015\u001a\u00020\nHÂ\u0003J9\u0010\u0016\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u00072\b\b\u0002\u0010\t\u001a\u00020\nHÆ\u0001J\u0013\u0010\u0017\u001a\u00020\u00182\b\u0010\u0019\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\u0006\u0010\u001a\u001a\u00020\nJ\u0010\u0010\u001b\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001c\u001a\u00020\u001dJ\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState;", "", "featuredBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "transactionState", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "availableSubscriptions", "", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "subscriptionRedemptionState", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;Ljava/util/List;Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;)V", "getAvailableSubscriptions", "()Ljava/util/List;", "getFeaturedBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "getTransactionState", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "getRedemptionState", "getStateFromActiveSubscription", "activeSubscription", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "hashCode", "", "toString", "", "SubscriptionRedemptionState", "TransactionState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ManageDonationsState {
    private final List<Subscription> availableSubscriptions;
    private final Badge featuredBadge;
    private final SubscriptionRedemptionState subscriptionRedemptionState;
    private final TransactionState transactionState;

    /* compiled from: ManageDonationsState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;", "", "(Ljava/lang/String;I)V", "NONE", "IN_PROGRESS", "FAILED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum SubscriptionRedemptionState {
        NONE,
        IN_PROGRESS,
        FAILED
    }

    public ManageDonationsState() {
        this(null, null, null, null, 15, null);
    }

    private final SubscriptionRedemptionState component4() {
        return this.subscriptionRedemptionState;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ManageDonationsState copy$default(ManageDonationsState manageDonationsState, Badge badge, TransactionState transactionState, List list, SubscriptionRedemptionState subscriptionRedemptionState, int i, Object obj) {
        if ((i & 1) != 0) {
            badge = manageDonationsState.featuredBadge;
        }
        if ((i & 2) != 0) {
            transactionState = manageDonationsState.transactionState;
        }
        if ((i & 4) != 0) {
            list = manageDonationsState.availableSubscriptions;
        }
        if ((i & 8) != 0) {
            subscriptionRedemptionState = manageDonationsState.subscriptionRedemptionState;
        }
        return manageDonationsState.copy(badge, transactionState, list, subscriptionRedemptionState);
    }

    public final Badge component1() {
        return this.featuredBadge;
    }

    public final TransactionState component2() {
        return this.transactionState;
    }

    public final List<Subscription> component3() {
        return this.availableSubscriptions;
    }

    public final ManageDonationsState copy(Badge badge, TransactionState transactionState, List<Subscription> list, SubscriptionRedemptionState subscriptionRedemptionState) {
        Intrinsics.checkNotNullParameter(transactionState, "transactionState");
        Intrinsics.checkNotNullParameter(list, "availableSubscriptions");
        Intrinsics.checkNotNullParameter(subscriptionRedemptionState, "subscriptionRedemptionState");
        return new ManageDonationsState(badge, transactionState, list, subscriptionRedemptionState);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ManageDonationsState)) {
            return false;
        }
        ManageDonationsState manageDonationsState = (ManageDonationsState) obj;
        return Intrinsics.areEqual(this.featuredBadge, manageDonationsState.featuredBadge) && Intrinsics.areEqual(this.transactionState, manageDonationsState.transactionState) && Intrinsics.areEqual(this.availableSubscriptions, manageDonationsState.availableSubscriptions) && this.subscriptionRedemptionState == manageDonationsState.subscriptionRedemptionState;
    }

    public int hashCode() {
        Badge badge = this.featuredBadge;
        return ((((((badge == null ? 0 : badge.hashCode()) * 31) + this.transactionState.hashCode()) * 31) + this.availableSubscriptions.hashCode()) * 31) + this.subscriptionRedemptionState.hashCode();
    }

    public String toString() {
        return "ManageDonationsState(featuredBadge=" + this.featuredBadge + ", transactionState=" + this.transactionState + ", availableSubscriptions=" + this.availableSubscriptions + ", subscriptionRedemptionState=" + this.subscriptionRedemptionState + ')';
    }

    public ManageDonationsState(Badge badge, TransactionState transactionState, List<Subscription> list, SubscriptionRedemptionState subscriptionRedemptionState) {
        Intrinsics.checkNotNullParameter(transactionState, "transactionState");
        Intrinsics.checkNotNullParameter(list, "availableSubscriptions");
        Intrinsics.checkNotNullParameter(subscriptionRedemptionState, "subscriptionRedemptionState");
        this.featuredBadge = badge;
        this.transactionState = transactionState;
        this.availableSubscriptions = list;
        this.subscriptionRedemptionState = subscriptionRedemptionState;
    }

    public final Badge getFeaturedBadge() {
        return this.featuredBadge;
    }

    public /* synthetic */ ManageDonationsState(Badge badge, TransactionState transactionState, List list, SubscriptionRedemptionState subscriptionRedemptionState, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : badge, (i & 2) != 0 ? TransactionState.Init.INSTANCE : transactionState, (i & 4) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 8) != 0 ? SubscriptionRedemptionState.NONE : subscriptionRedemptionState);
    }

    public final TransactionState getTransactionState() {
        return this.transactionState;
    }

    public final List<Subscription> getAvailableSubscriptions() {
        return this.availableSubscriptions;
    }

    public final SubscriptionRedemptionState getRedemptionState() {
        TransactionState transactionState = this.transactionState;
        if (Intrinsics.areEqual(transactionState, TransactionState.Init.INSTANCE)) {
            return this.subscriptionRedemptionState;
        }
        if (Intrinsics.areEqual(transactionState, TransactionState.NetworkFailure.INSTANCE)) {
            return this.subscriptionRedemptionState;
        }
        if (Intrinsics.areEqual(transactionState, TransactionState.InTransaction.INSTANCE)) {
            return SubscriptionRedemptionState.IN_PROGRESS;
        }
        if (transactionState instanceof TransactionState.NotInTransaction) {
            SubscriptionRedemptionState stateFromActiveSubscription = getStateFromActiveSubscription(((TransactionState.NotInTransaction) this.transactionState).getActiveSubscription());
            return stateFromActiveSubscription == null ? this.subscriptionRedemptionState : stateFromActiveSubscription;
        }
        throw new NoWhenBranchMatchedException();
    }

    public final SubscriptionRedemptionState getStateFromActiveSubscription(ActiveSubscription activeSubscription) {
        Intrinsics.checkNotNullParameter(activeSubscription, "activeSubscription");
        if (activeSubscription.isFailedPayment()) {
            return SubscriptionRedemptionState.FAILED;
        }
        if (activeSubscription.isInProgress()) {
            return SubscriptionRedemptionState.IN_PROGRESS;
        }
        return null;
    }

    /* compiled from: ManageDonationsState.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0004\u0007\b\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "", "()V", "InTransaction", "Init", "NetworkFailure", "NotInTransaction", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$Init;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$NetworkFailure;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$InTransaction;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$NotInTransaction;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class TransactionState {
        public /* synthetic */ TransactionState(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: ManageDonationsState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$Init;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Init extends TransactionState {
            public static final Init INSTANCE = new Init();

            private Init() {
                super(null);
            }
        }

        private TransactionState() {
        }

        /* compiled from: ManageDonationsState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$NetworkFailure;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class NetworkFailure extends TransactionState {
            public static final NetworkFailure INSTANCE = new NetworkFailure();

            private NetworkFailure() {
                super(null);
            }
        }

        /* compiled from: ManageDonationsState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$InTransaction;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class InTransaction extends TransactionState {
            public static final InTransaction INSTANCE = new InTransaction();

            private InTransaction() {
                super(null);
            }
        }

        /* compiled from: ManageDonationsState.kt */
        @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState$NotInTransaction;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$TransactionState;", "activeSubscription", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "(Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;)V", "getActiveSubscription", "()Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class NotInTransaction extends TransactionState {
            private final ActiveSubscription activeSubscription;

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public NotInTransaction(ActiveSubscription activeSubscription) {
                super(null);
                Intrinsics.checkNotNullParameter(activeSubscription, "activeSubscription");
                this.activeSubscription = activeSubscription;
            }

            public final ActiveSubscription getActiveSubscription() {
                return this.activeSubscription;
            }
        }
    }
}
