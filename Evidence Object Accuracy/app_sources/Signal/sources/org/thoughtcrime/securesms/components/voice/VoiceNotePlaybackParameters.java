package org.thoughtcrime.securesms.components.voice;

import android.os.Bundle;
import android.support.v4.media.session.MediaSessionCompat;
import com.google.android.exoplayer2.PlaybackParameters;

/* loaded from: classes4.dex */
public final class VoiceNotePlaybackParameters {
    private final MediaSessionCompat mediaSessionCompat;

    public VoiceNotePlaybackParameters(MediaSessionCompat mediaSessionCompat) {
        this.mediaSessionCompat = mediaSessionCompat;
    }

    public PlaybackParameters getParameters() {
        return new PlaybackParameters(getSpeed());
    }

    public void setSpeed(float f) {
        Bundle bundle = new Bundle();
        bundle.putFloat(VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED, f);
        this.mediaSessionCompat.setExtras(bundle);
    }

    private float getSpeed() {
        Bundle extras = this.mediaSessionCompat.getController().getExtras();
        if (extras == null) {
            return 1.0f;
        }
        return extras.getFloat(VoiceNotePlaybackService.ACTION_NEXT_PLAYBACK_SPEED, 1.0f);
    }
}
