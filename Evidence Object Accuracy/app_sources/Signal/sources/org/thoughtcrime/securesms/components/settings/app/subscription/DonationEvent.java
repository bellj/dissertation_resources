package org.thoughtcrime.securesms.components.settings.app.subscription;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: DonationEvent.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0004\u0003\u0004\u0005\u0006B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0004\u0007\b\t\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "", "()V", "PaymentConfirmationSuccess", "RequestTokenSuccess", "SubscriptionCancellationFailed", "SubscriptionCancelled", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$RequestTokenSuccess;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$PaymentConfirmationSuccess;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$SubscriptionCancellationFailed;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$SubscriptionCancelled;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DonationEvent {
    public /* synthetic */ DonationEvent(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    /* compiled from: DonationEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$RequestTokenSuccess;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RequestTokenSuccess extends DonationEvent {
        public static final RequestTokenSuccess INSTANCE = new RequestTokenSuccess();

        private RequestTokenSuccess() {
            super(null);
        }
    }

    private DonationEvent() {
    }

    /* compiled from: DonationEvent.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$PaymentConfirmationSuccess;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "badge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PaymentConfirmationSuccess extends DonationEvent {
        private final Badge badge;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PaymentConfirmationSuccess(Badge badge) {
            super(null);
            Intrinsics.checkNotNullParameter(badge, "badge");
            this.badge = badge;
        }

        public final Badge getBadge() {
            return this.badge;
        }
    }

    /* compiled from: DonationEvent.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$SubscriptionCancellationFailed;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "throwable", "", "(Ljava/lang/Throwable;)V", "getThrowable", "()Ljava/lang/Throwable;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SubscriptionCancellationFailed extends DonationEvent {
        private final Throwable throwable;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SubscriptionCancellationFailed(Throwable th) {
            super(null);
            Intrinsics.checkNotNullParameter(th, "throwable");
            this.throwable = th;
        }

        public final Throwable getThrowable() {
            return this.throwable;
        }
    }

    /* compiled from: DonationEvent.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent$SubscriptionCancelled;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationEvent;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SubscriptionCancelled extends DonationEvent {
        public static final SubscriptionCancelled INSTANCE = new SubscriptionCancelled();

        private SubscriptionCancelled() {
            super(null);
        }
    }
}
