package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class RemovableEditableMediaView extends FrameLayout {
    private View current;
    private final ImageView edit;
    private final int editSize;
    private final ImageView remove;
    private final int removeSize;

    public RemovableEditableMediaView(Context context) {
        this(context, null);
    }

    public RemovableEditableMediaView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public RemovableEditableMediaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        ImageView imageView = (ImageView) LayoutInflater.from(context).inflate(R.layout.media_view_remove_button, (ViewGroup) this, false);
        this.remove = imageView;
        ImageView imageView2 = (ImageView) LayoutInflater.from(context).inflate(R.layout.media_view_edit_button, (ViewGroup) this, false);
        this.edit = imageView2;
        this.removeSize = getResources().getDimensionPixelSize(R.dimen.media_bubble_remove_button_size);
        this.editSize = getResources().getDimensionPixelSize(R.dimen.media_bubble_edit_button_size);
        imageView.setVisibility(8);
        imageView2.setVisibility(8);
    }

    @Override // android.view.View
    public void onFinishInflate() {
        super.onFinishInflate();
        addView(this.remove);
        addView(this.edit);
    }

    public void display(View view, boolean z) {
        this.edit.setVisibility(z ? 0 : 8);
        View view2 = this.current;
        if (view != view2) {
            if (view2 != null) {
                view2.setVisibility(8);
            }
            if (view != null) {
                int paddingLeft = view.getPaddingLeft();
                int i = this.removeSize;
                view.setPadding(paddingLeft, i / 2, i / 2, view.getPaddingRight());
                this.edit.setPadding(0, 0, this.removeSize / 2, 0);
                view.setVisibility(0);
                this.remove.setVisibility(0);
            } else {
                this.remove.setVisibility(8);
                this.edit.setVisibility(8);
            }
            this.current = view;
        }
    }

    public void setRemoveClickListener(View.OnClickListener onClickListener) {
        this.remove.setOnClickListener(onClickListener);
    }

    public void setEditClickListener(View.OnClickListener onClickListener) {
        this.edit.setOnClickListener(onClickListener);
    }
}
