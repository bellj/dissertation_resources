package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import com.annimon.stream.function.Function;
import j$.util.Optional;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState;
import org.thoughtcrime.securesms.jobmanager.JobTracker;

/* compiled from: ManageDonationsViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\u0010\u0006\u001a\u00020\u00032\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00010\u0000H\n¢\u0006\u0004\b\u0004\u0010\u0005"}, d2 = {"j$/util/Optional", "Lorg/thoughtcrime/securesms/jobmanager/JobTracker$JobState;", "jobStateOptional", "", "invoke", "(Lj$/util/Optional;)V", "<anonymous>"}, k = 3, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class ManageDonationsViewModel$refresh$1 extends Lambda implements Function1<Optional<JobTracker.JobState>, Unit> {
    final /* synthetic */ ManageDonationsViewModel this$0;

    /* compiled from: ManageDonationsViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[JobTracker.JobState.values().length];
            iArr[JobTracker.JobState.PENDING.ordinal()] = 1;
            iArr[JobTracker.JobState.RUNNING.ordinal()] = 2;
            iArr[JobTracker.JobState.SUCCESS.ordinal()] = 3;
            iArr[JobTracker.JobState.FAILURE.ordinal()] = 4;
            iArr[JobTracker.JobState.IGNORED.ordinal()] = 5;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ManageDonationsViewModel$refresh$1(ManageDonationsViewModel manageDonationsViewModel) {
        super(1);
        this.this$0 = manageDonationsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Optional<JobTracker.JobState> optional) {
        invoke(optional);
        return Unit.INSTANCE;
    }

    public final void invoke(Optional<JobTracker.JobState> optional) {
        Intrinsics.checkNotNullParameter(optional, "jobStateOptional");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$refresh$1$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel$refresh$1.m975invoke$lambda1(Optional.this, (ManageDonationsState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-1 */
    public static final ManageDonationsState m975invoke$lambda1(Optional optional, ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullParameter(optional, "$jobStateOptional");
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "manageDonationsState");
        Object orElse = optional.map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$refresh$1$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel$refresh$1.m976invoke$lambda1$lambda0((JobTracker.JobState) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).orElse(ManageDonationsState.SubscriptionRedemptionState.NONE);
        Intrinsics.checkNotNullExpressionValue(orElse, "jobStateOptional.map { j…tionRedemptionState.NONE)");
        return ManageDonationsState.copy$default(manageDonationsState, null, null, null, (ManageDonationsState.SubscriptionRedemptionState) orElse, 7, null);
    }

    /* renamed from: invoke$lambda-1$lambda-0 */
    public static final ManageDonationsState.SubscriptionRedemptionState m976invoke$lambda1$lambda0(JobTracker.JobState jobState) {
        int i = jobState == null ? -1 : WhenMappings.$EnumSwitchMapping$0[jobState.ordinal()];
        if (i == 1) {
            return ManageDonationsState.SubscriptionRedemptionState.IN_PROGRESS;
        }
        if (i == 2) {
            return ManageDonationsState.SubscriptionRedemptionState.IN_PROGRESS;
        }
        if (i == 3) {
            return ManageDonationsState.SubscriptionRedemptionState.NONE;
        }
        if (i == 4) {
            return ManageDonationsState.SubscriptionRedemptionState.FAILED;
        }
        if (i == 5) {
            return ManageDonationsState.SubscriptionRedemptionState.NONE;
        }
        throw new NoWhenBranchMatchedException();
    }
}
