package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class CircleColorImageView extends AppCompatImageView {
    public CircleColorImageView(Context context) {
        this(context, null);
    }

    public CircleColorImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public CircleColorImageView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        int i2 = -1;
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.CircleColorImageView, 0, 0);
            i2 = obtainStyledAttributes.getColor(0, -1);
            obtainStyledAttributes.recycle();
        }
        Drawable drawable = context.getResources().getDrawable(R.drawable.circle_tintable);
        drawable.setColorFilter(i2, PorterDuff.Mode.SRC_IN);
        setBackgroundDrawable(drawable);
    }
}
