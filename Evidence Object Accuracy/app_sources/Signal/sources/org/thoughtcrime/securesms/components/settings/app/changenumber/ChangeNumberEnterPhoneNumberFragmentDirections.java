package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* loaded from: classes4.dex */
public class ChangeNumberEnterPhoneNumberFragmentDirections {
    private ChangeNumberEnterPhoneNumberFragmentDirections() {
    }

    public static NavDirections actionEnterPhoneNumberChangeFragmentToChangePhoneNumberConfirmFragment() {
        return new ActionOnlyNavDirections(R.id.action_enterPhoneNumberChangeFragment_to_changePhoneNumberConfirmFragment);
    }

    public static ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment actionEnterPhoneNumberChangeFragmentToCountryPickerFragment() {
        return new ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment();
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }

    /* loaded from: classes4.dex */
    public static class ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_enterPhoneNumberChangeFragment_to_countryPickerFragment;
        }

        private ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment() {
            this.arguments = new HashMap();
        }

        public ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment setResultKey(String str) {
            this.arguments.put(MultiselectForwardFragment.RESULT_KEY, str);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(MultiselectForwardFragment.RESULT_KEY)) {
                bundle.putString(MultiselectForwardFragment.RESULT_KEY, (String) this.arguments.get(MultiselectForwardFragment.RESULT_KEY));
            } else {
                bundle.putString(MultiselectForwardFragment.RESULT_KEY, null);
            }
            return bundle;
        }

        public String getResultKey() {
            return (String) this.arguments.get(MultiselectForwardFragment.RESULT_KEY);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment actionEnterPhoneNumberChangeFragmentToCountryPickerFragment = (ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment) obj;
            if (this.arguments.containsKey(MultiselectForwardFragment.RESULT_KEY) != actionEnterPhoneNumberChangeFragmentToCountryPickerFragment.arguments.containsKey(MultiselectForwardFragment.RESULT_KEY)) {
                return false;
            }
            if (getResultKey() == null ? actionEnterPhoneNumberChangeFragmentToCountryPickerFragment.getResultKey() == null : getResultKey().equals(actionEnterPhoneNumberChangeFragmentToCountryPickerFragment.getResultKey())) {
                return getActionId() == actionEnterPhoneNumberChangeFragmentToCountryPickerFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getResultKey() != null ? getResultKey().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionEnterPhoneNumberChangeFragmentToCountryPickerFragment(actionId=" + getActionId() + "){resultKey=" + getResultKey() + "}";
        }
    }
}
