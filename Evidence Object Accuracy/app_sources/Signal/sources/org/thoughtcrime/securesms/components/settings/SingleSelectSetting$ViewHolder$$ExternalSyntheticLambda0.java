package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.SingleSelectSetting;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SingleSelectSetting$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ SingleSelectSetting.ViewHolder f$0;
    public final /* synthetic */ SingleSelectSetting.Item f$1;

    public /* synthetic */ SingleSelectSetting$ViewHolder$$ExternalSyntheticLambda0(SingleSelectSetting.ViewHolder viewHolder, SingleSelectSetting.Item item) {
        this.f$0 = viewHolder;
        this.f$1 = item;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
