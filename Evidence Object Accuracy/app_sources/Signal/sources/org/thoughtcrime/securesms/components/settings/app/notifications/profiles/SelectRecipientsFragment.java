package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentOnAttachListener;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import j$.util.Optional;
import java.util.ArrayList;
import java.util.Set;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__MutableCollectionsKt;
import kotlin.collections.SetsKt__SetsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.ContactSelectionListFragment;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ContactFilterView;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsViewModel;
import org.thoughtcrime.securesms.groups.SelectionLimits;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.CircularProgressMaterialButton;

/* compiled from: SelectRecipientsFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000h\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u00012\u00020\u0002B\u0007¢\u0006\u0004\b+\u0010,J\b\u0010\u0004\u001a\u00020\u0003H\u0002J\b\u0010\u0006\u001a\u00020\u0005H\u0002J\b\u0010\b\u001a\u00020\u0007H\u0002J&\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\n\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016J\u001a\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\rH\u0016J\b\u0010\u0013\u001a\u00020\u0007H\u0016J.\u0010\u001c\u001a\u00020\u00072\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\b\u0010\u0018\u001a\u0004\u0018\u00010\u00172\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001a0\u0019H\u0016J \u0010\u001d\u001a\u00020\u00072\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00150\u00142\b\u0010\u0018\u001a\u0004\u0018\u00010\u0017H\u0016J\b\u0010\u001e\u001a\u00020\u0007H\u0016R\u001b\u0010$\u001a\u00020\u001f8BX\u0002¢\u0006\f\n\u0004\b \u0010!\u001a\u0004\b\"\u0010#R\u0014\u0010&\u001a\u00020%8\u0002X\u0004¢\u0006\u0006\n\u0004\b&\u0010'R\u0018\u0010)\u001a\u0004\u0018\u00010(8\u0002@\u0002X\u000e¢\u0006\u0006\n\u0004\b)\u0010*¨\u0006-"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/SelectRecipientsFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "Lorg/thoughtcrime/securesms/ContactSelectionListFragment$OnContactSelectedListener;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "createFactory", "", "getDefaultDisplayMode", "", "updateAddToProfile", "Landroid/view/LayoutInflater;", "inflater", "Landroid/view/ViewGroup;", "container", "Landroid/os/Bundle;", "savedInstanceState", "Landroid/view/View;", "onCreateView", "view", "onViewCreated", "onDestroyView", "j$/util/Optional", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "recipientId", "", "number", "j$/util/function/Consumer", "", "callback", "onBeforeContactSelected", "onContactDeselected", "onSelectionChanged", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/SelectRecipientsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/SelectRecipientsViewModel;", "viewModel", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton;", "addToProfile", "Lorg/thoughtcrime/securesms/util/views/CircularProgressMaterialButton;", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class SelectRecipientsFragment extends LoggingFragment implements ContactSelectionListFragment.OnContactSelectedListener {
    private CircularProgressMaterialButton addToProfile;
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SelectRecipientsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$viewModel$2
        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            return SelectRecipientsFragment.access$createFactory((SelectRecipientsFragment) this.receiver);
        }
    });

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onSelectionChanged() {
    }

    private final SelectRecipientsViewModel getViewModel() {
        return (SelectRecipientsViewModel) this.viewModel$delegate.getValue();
    }

    public final ViewModelProvider.Factory createFactory() {
        Set set;
        SelectRecipientsFragmentArgs fromBundle = SelectRecipientsFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        long profileId = fromBundle.getProfileId();
        RecipientId[] currentSelection = fromBundle.getCurrentSelection();
        if (currentSelection == null || (set = ArraysKt___ArraysKt.toSet(currentSelection)) == null) {
            set = SetsKt__SetsKt.emptySet();
        }
        return new SelectRecipientsViewModel.Factory(profileId, set);
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        Intrinsics.checkNotNullParameter(layoutInflater, "inflater");
        RecipientId[] currentSelection = SelectRecipientsFragmentArgs.fromBundle(requireArguments()).getCurrentSelection();
        ArrayList arrayList = new ArrayList();
        if (currentSelection != null) {
            boolean unused = CollectionsKt__MutableCollectionsKt.addAll(arrayList, currentSelection);
        }
        getChildFragmentManager().addFragmentOnAttachListener(new FragmentOnAttachListener(arrayList) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ ArrayList f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.fragment.app.FragmentOnAttachListener
            public final void onAttachFragment(FragmentManager fragmentManager, Fragment fragment) {
                SelectRecipientsFragment.$r8$lambda$v0uYB725pFkxqcB7KxIQqfdr7jI(SelectRecipientsFragment.this, this.f$1, fragmentManager, fragment);
            }
        });
        return layoutInflater.inflate(R.layout.fragment_select_recipients_fragment, viewGroup, false);
    }

    /* renamed from: onCreateView$lambda-1 */
    public static final void m803onCreateView$lambda1(SelectRecipientsFragment selectRecipientsFragment, ArrayList arrayList, FragmentManager fragmentManager, Fragment fragment) {
        Intrinsics.checkNotNullParameter(selectRecipientsFragment, "this$0");
        Intrinsics.checkNotNullParameter(arrayList, "$selectionList");
        Intrinsics.checkNotNullParameter(fragmentManager, "<anonymous parameter 0>");
        Intrinsics.checkNotNullParameter(fragment, "fragment");
        Bundle bundle = new Bundle();
        bundle.putInt("display_mode", selectRecipientsFragment.getDefaultDisplayMode());
        bundle.putBoolean("refreshable", false);
        bundle.putBoolean("recents", true);
        bundle.putParcelable("selection_limits", SelectionLimits.NO_LIMITS);
        bundle.putParcelableArrayList("current_selection", arrayList);
        bundle.putBoolean("hide_count", true);
        bundle.putBoolean("display_chips", true);
        bundle.putBoolean("can_select_self", false);
        bundle.putBoolean("recycler_view_clipping", false);
        bundle.putInt("recycler_view_padding_bottom", ViewUtil.dpToPx(60));
        fragment.setArguments(bundle);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        toolbar.setTitle(R.string.AddAllowedMembers__allowed_notifications);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                SelectRecipientsFragment.$r8$lambda$dMKsnZNCYEVt2YF7tenx4KiitUE(SelectRecipientsFragment.this, view2);
            }
        });
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        View findViewById2 = view.findViewById(R.id.contact_filter_edit_text);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.contact_filter_edit_text)");
        ContactFilterView contactFilterView = (ContactFilterView) findViewById2;
        Fragment findFragmentById = getChildFragmentManager().findFragmentById(R.id.contact_selection_list_fragment);
        if (findFragmentById != null) {
            contactFilterView.setOnFilterChangedListener(new ContactFilterView.OnFilterChangedListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$$ExternalSyntheticLambda1
                @Override // org.thoughtcrime.securesms.components.ContactFilterView.OnFilterChangedListener
                public final void onFilterChanged(String str) {
                    SelectRecipientsFragment.m801$r8$lambda$ECW6nh2pybjj3zYTdy200pJORE(ContactSelectionListFragment.this, str);
                }
            });
            CircularProgressMaterialButton circularProgressMaterialButton = (CircularProgressMaterialButton) view.findViewById(R.id.select_recipients_add);
            this.addToProfile = circularProgressMaterialButton;
            if (circularProgressMaterialButton != null) {
                circularProgressMaterialButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$$ExternalSyntheticLambda2
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view2) {
                        SelectRecipientsFragment.$r8$lambda$LHcyscwfD5mKlVoVjplyKYlBIcQ(SelectRecipientsFragment.this, view2);
                    }
                });
            }
            updateAddToProfile();
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.ContactSelectionListFragment");
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m804onViewCreated$lambda2(SelectRecipientsFragment selectRecipientsFragment, View view) {
        Intrinsics.checkNotNullParameter(selectRecipientsFragment, "this$0");
        FragmentKt.findNavController(selectRecipientsFragment).navigateUp();
    }

    /* renamed from: onViewCreated$lambda-3 */
    public static final void m805onViewCreated$lambda3(ContactSelectionListFragment contactSelectionListFragment, String str) {
        Intrinsics.checkNotNullParameter(contactSelectionListFragment, "$selectionFragment");
        if (str == null || str.length() == 0) {
            contactSelectionListFragment.resetQueryFilter();
        } else {
            contactSelectionListFragment.setQueryFilter(str);
        }
    }

    /* renamed from: onViewCreated$lambda-6 */
    public static final void m806onViewCreated$lambda6(SelectRecipientsFragment selectRecipientsFragment, View view) {
        Intrinsics.checkNotNullParameter(selectRecipientsFragment, "this$0");
        LifecycleDisposable lifecycleDisposable = selectRecipientsFragment.lifecycleDisposable;
        Single<NotificationProfile> doOnTerminate = selectRecipientsFragment.getViewModel().updateAllowedMembers().doOnSubscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                SelectRecipientsFragment.m802$r8$lambda$Limxj7QEQCFFSYQv8IwBEc3U0I(SelectRecipientsFragment.this, (Disposable) obj);
            }
        }).doOnTerminate(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                SelectRecipientsFragment.$r8$lambda$pTvzGi5LBEkPbTkxmmeNOaatiW4(SelectRecipientsFragment.this);
            }
        });
        Intrinsics.checkNotNullExpressionValue(doOnTerminate, "viewModel.updateAllowedM…ofile?.cancelSpinning() }");
        lifecycleDisposable.plusAssign(SubscribersKt.subscribeBy$default(doOnTerminate, (Function1) null, new Function1<NotificationProfile, Unit>(selectRecipientsFragment) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.SelectRecipientsFragment$onViewCreated$3$3
            final /* synthetic */ SelectRecipientsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(NotificationProfile notificationProfile) {
                invoke(notificationProfile);
                return Unit.INSTANCE;
            }

            public final void invoke(NotificationProfile notificationProfile) {
                FragmentKt.findNavController(this.this$0).navigateUp();
            }
        }, 1, (Object) null));
    }

    /* renamed from: onViewCreated$lambda-6$lambda-4 */
    public static final void m807onViewCreated$lambda6$lambda4(SelectRecipientsFragment selectRecipientsFragment, Disposable disposable) {
        Intrinsics.checkNotNullParameter(selectRecipientsFragment, "this$0");
        CircularProgressMaterialButton circularProgressMaterialButton = selectRecipientsFragment.addToProfile;
        if (circularProgressMaterialButton != null) {
            circularProgressMaterialButton.setSpinning();
        }
    }

    /* renamed from: onViewCreated$lambda-6$lambda-5 */
    public static final void m808onViewCreated$lambda6$lambda5(SelectRecipientsFragment selectRecipientsFragment) {
        Intrinsics.checkNotNullParameter(selectRecipientsFragment, "this$0");
        CircularProgressMaterialButton circularProgressMaterialButton = selectRecipientsFragment.addToProfile;
        if (circularProgressMaterialButton != null) {
            circularProgressMaterialButton.cancelSpinning();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.addToProfile = null;
    }

    private final int getDefaultDisplayMode() {
        return (Util.isDefaultSmsProvider(requireContext()) ? 455 : 453) | 32;
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onBeforeContactSelected(Optional<RecipientId> optional, String str, j$.util.function.Consumer<Boolean> consumer) {
        Intrinsics.checkNotNullParameter(optional, "recipientId");
        Intrinsics.checkNotNullParameter(consumer, "callback");
        if (optional.isPresent()) {
            SelectRecipientsViewModel viewModel = getViewModel();
            RecipientId recipientId = optional.get();
            Intrinsics.checkNotNullExpressionValue(recipientId, "recipientId.get()");
            viewModel.select(recipientId);
            consumer.accept(Boolean.TRUE);
            updateAddToProfile();
            return;
        }
        consumer.accept(Boolean.FALSE);
    }

    @Override // org.thoughtcrime.securesms.ContactSelectionListFragment.OnContactSelectedListener
    public void onContactDeselected(Optional<RecipientId> optional, String str) {
        Intrinsics.checkNotNullParameter(optional, "recipientId");
        if (optional.isPresent()) {
            SelectRecipientsViewModel viewModel = getViewModel();
            RecipientId recipientId = optional.get();
            Intrinsics.checkNotNullExpressionValue(recipientId, "recipientId.get()");
            viewModel.deselect(recipientId);
            updateAddToProfile();
        }
    }

    private final void updateAddToProfile() {
        boolean z = !getViewModel().getRecipients().isEmpty();
        CircularProgressMaterialButton circularProgressMaterialButton = this.addToProfile;
        if (circularProgressMaterialButton != null) {
            circularProgressMaterialButton.setEnabled(z);
        }
        CircularProgressMaterialButton circularProgressMaterialButton2 = this.addToProfile;
        if (circularProgressMaterialButton2 != null) {
            circularProgressMaterialButton2.setAlpha(z ? 1.0f : 0.5f);
        }
    }
}
