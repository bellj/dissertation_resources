package org.thoughtcrime.securesms.components.settings.conversation;

import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ InternalConversationSettingsFragment.InternalViewModel f$0;

    public /* synthetic */ InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda0(InternalConversationSettingsFragment.InternalViewModel internalViewModel) {
        this.f$0 = internalViewModel;
    }

    @Override // java.lang.Runnable
    public final void run() {
        InternalConversationSettingsFragment.InternalViewModel.m1163_init_$lambda2(this.f$0);
    }
}
