package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.view.View;
import androidx.appcompat.widget.AppCompatEditText;
import com.google.android.material.button.MaterialButton;
import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.MatchResult;
import kotlin.text.Regex;
import kotlin.text.StringsKt__StringNumberConversionsJVMKt;
import kotlin.text.StringsKt__StringsJVMKt;
import kotlin.text.StringsKt__StringsKt;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;
import org.signal.core.util.StringUtil;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.PushDatabase;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: Boost.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\b\u0018\u0000 \u00102\u00020\u0001:\b\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\f\u001a\u00020\rHÖ\u0001J\t\u0010\u000e\u001a\u00020\u000fHÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "", "price", "Lorg/signal/core/util/money/FiatMoney;", "(Lorg/signal/core/util/money/FiatMoney;)V", "getPrice", "()Lorg/signal/core/util/money/FiatMoney;", "component1", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Companion", "HeadingModel", "HeadingViewHolder", "LoadingModel", "LoadingViewHolder", "MoneyFilter", "SelectionModel", "SelectionViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Boost {
    public static final Companion Companion = new Companion(null);
    private final FiatMoney price;

    public static /* synthetic */ Boost copy$default(Boost boost, FiatMoney fiatMoney, int i, Object obj) {
        if ((i & 1) != 0) {
            fiatMoney = boost.price;
        }
        return boost.copy(fiatMoney);
    }

    public final FiatMoney component1() {
        return this.price;
    }

    public final Boost copy(FiatMoney fiatMoney) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        return new Boost(fiatMoney);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof Boost) && Intrinsics.areEqual(this.price, ((Boost) obj).price);
    }

    public int hashCode() {
        return this.price.hashCode();
    }

    public String toString() {
        return "Boost(price=" + this.price + ')';
    }

    public Boost(FiatMoney fiatMoney) {
        Intrinsics.checkNotNullParameter(fiatMoney, "price");
        this.price = fiatMoney;
    }

    public final FiatMoney getPrice() {
        return this.price;
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016J\u0010\u0010\n\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u0000H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$HeadingModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "boostBadge", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/badges/models/Badge;)V", "getBoostBadge", "()Lorg/thoughtcrime/securesms/badges/models/Badge;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class HeadingModel extends PreferenceModel<HeadingModel> {
        private final Badge boostBadge;

        public boolean areItemsTheSame(HeadingModel headingModel) {
            Intrinsics.checkNotNullParameter(headingModel, "newItem");
            return true;
        }

        public final Badge getBoostBadge() {
            return this.boostBadge;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public HeadingModel(Badge badge) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(badge, "boostBadge");
            this.boostBadge = badge;
        }

        public boolean areContentsTheSame(HeadingModel headingModel) {
            Intrinsics.checkNotNullParameter(headingModel, "newItem");
            return super.areContentsTheSame(headingModel) && Intrinsics.areEqual(headingModel.boostBadge, this.boostBadge);
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0000H\u0016¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$LoadingModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "()V", "areItemsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LoadingModel extends PreferenceModel<LoadingModel> {
        public boolean areItemsTheSame(LoadingModel loadingModel) {
            Intrinsics.checkNotNullParameter(loadingModel, "newItem");
            return true;
        }

        public LoadingModel() {
            super(null, null, null, null, false, 31, null);
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016J\b\u0010\u000b\u001a\u00020\tH\u0016J\b\u0010\f\u001a\u00020\tH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$LoadingViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$LoadingModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "animator", "Landroid/animation/Animator;", "bind", "", "model", "onAttachedToWindow", "onDetachedFromWindow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LoadingViewHolder extends MappingViewHolder<LoadingModel> {
        private final Animator animator;

        public void bind(LoadingModel loadingModel) {
            Intrinsics.checkNotNullParameter(loadingModel, "model");
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public LoadingViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            AnimatorSet animatorSet = new AnimatorSet();
            ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", 0.8f, 0.25f);
            ofFloat.setDuration(1000L);
            ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(view, "alpha", 0.25f, 0.8f);
            ofFloat2.setDuration(300L);
            animatorSet.playSequentially(ofFloat, ofFloat2);
            animatorSet.addListener(new Boost$LoadingViewHolder$animator$lambda3$$inlined$doOnEnd$1(view, animatorSet));
            this.animator = animatorSet;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onAttachedToWindow() {
            if (this.animator.isStarted()) {
                this.animator.resume();
            } else {
                this.animator.start();
            }
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onDetachedFromWindow() {
            this.animator.pause();
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001Bw\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0018\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\r0\u000b\u0012\u0006\u0010\u000e\u001a\u00020\t\u0012\u0012\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u0010\u0012\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\r0\u0010¢\u0006\u0002\u0010\u0013J\u0010\u0010 \u001a\u00020\t2\u0006\u0010!\u001a\u00020\u0000H\u0016J\u0010\u0010\"\u001a\u00020\t2\u0006\u0010!\u001a\u00020\u0000H\u0016R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\u000e\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0018R\u0014\u0010\b\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0018R#\u0010\n\u001a\u0014\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0004\u0012\u0004\u0012\u00020\r0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010\u000f\u001a\u000e\u0012\u0004\u0012\u00020\u0011\u0012\u0004\u0012\u00020\r0\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u001d\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\r0\u0010¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001cR\u0013\u0010\u0005\u001a\u0004\u0018\u00010\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001f¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$SelectionModel;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "boosts", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "selectedBoost", "currency", "Ljava/util/Currency;", "isEnabled", "", "onBoostClick", "Lkotlin/Function2;", "Landroid/view/View;", "", "isCustomAmountFocused", "onCustomAmountChanged", "Lkotlin/Function1;", "", "onCustomAmountFocusChanged", "(Ljava/util/List;Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;Ljava/util/Currency;ZLkotlin/jvm/functions/Function2;ZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "getBoosts", "()Ljava/util/List;", "getCurrency", "()Ljava/util/Currency;", "()Z", "getOnBoostClick", "()Lkotlin/jvm/functions/Function2;", "getOnCustomAmountChanged", "()Lkotlin/jvm/functions/Function1;", "getOnCustomAmountFocusChanged", "getSelectedBoost", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost;", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SelectionModel extends PreferenceModel<SelectionModel> {
        private final List<Boost> boosts;
        private final Currency currency;
        private final boolean isCustomAmountFocused;
        private final boolean isEnabled;
        private final Function2<View, Boost, Unit> onBoostClick;
        private final Function1<String, Unit> onCustomAmountChanged;
        private final Function1<Boolean, Unit> onCustomAmountFocusChanged;
        private final Boost selectedBoost;

        public boolean areItemsTheSame(SelectionModel selectionModel) {
            Intrinsics.checkNotNullParameter(selectionModel, "newItem");
            return true;
        }

        public final List<Boost> getBoosts() {
            return this.boosts;
        }

        public final Boost getSelectedBoost() {
            return this.selectedBoost;
        }

        public final Currency getCurrency() {
            return this.currency;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public boolean isEnabled() {
            return this.isEnabled;
        }

        public final Function2<View, Boost, Unit> getOnBoostClick() {
            return this.onBoostClick;
        }

        public final boolean isCustomAmountFocused() {
            return this.isCustomAmountFocused;
        }

        public final Function1<String, Unit> getOnCustomAmountChanged() {
            return this.onCustomAmountChanged;
        }

        public final Function1<Boolean, Unit> getOnCustomAmountFocusChanged() {
            return this.onCustomAmountFocusChanged;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r19v0, resolved type: kotlin.jvm.functions.Function2<? super android.view.View, ? super org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r21v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r22v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SelectionModel(List<Boost> list, Boost boost, Currency currency, boolean z, Function2<? super View, ? super Boost, Unit> function2, boolean z2, Function1<? super String, Unit> function1, Function1<? super Boolean, Unit> function12) {
            super(null, null, null, null, z, 15, null);
            Intrinsics.checkNotNullParameter(list, "boosts");
            Intrinsics.checkNotNullParameter(currency, "currency");
            Intrinsics.checkNotNullParameter(function2, "onBoostClick");
            Intrinsics.checkNotNullParameter(function1, "onCustomAmountChanged");
            Intrinsics.checkNotNullParameter(function12, "onCustomAmountFocusChanged");
            this.boosts = list;
            this.selectedBoost = boost;
            this.currency = currency;
            this.isEnabled = z;
            this.onBoostClick = function2;
            this.isCustomAmountFocused = z2;
            this.onCustomAmountChanged = function1;
            this.onCustomAmountFocusChanged = function12;
        }

        public boolean areContentsTheSame(SelectionModel selectionModel) {
            Intrinsics.checkNotNullParameter(selectionModel, "newItem");
            return super.areContentsTheSame(selectionModel) && Intrinsics.areEqual(selectionModel.boosts, this.boosts) && Intrinsics.areEqual(selectionModel.selectedBoost, this.selectedBoost) && Intrinsics.areEqual(selectionModel.currency, this.currency) && selectionModel.isCustomAmountFocused == this.isCustomAmountFocused;
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010 \n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00070\u000e8BX\u0004¢\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u000e¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$SelectionViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$SelectionModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "boost1", "Lcom/google/android/material/button/MaterialButton;", "boost2", "boost3", "boost4", "boost5", "boost6", "boostButtons", "", "getBoostButtons", "()Ljava/util/List;", "custom", "Landroidx/appcompat/widget/AppCompatEditText;", "filter", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$MoneyFilter;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SelectionViewHolder extends MappingViewHolder<SelectionModel> {
        private final MaterialButton boost1;
        private final MaterialButton boost2;
        private final MaterialButton boost3;
        private final MaterialButton boost4;
        private final MaterialButton boost5;
        private final MaterialButton boost6;
        private final AppCompatEditText custom;
        private MoneyFilter filter;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SelectionViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.boost_1);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.boost_1)");
            this.boost1 = (MaterialButton) findViewById;
            View findViewById2 = view.findViewById(R.id.boost_2);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.boost_2)");
            this.boost2 = (MaterialButton) findViewById2;
            View findViewById3 = view.findViewById(R.id.boost_3);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.boost_3)");
            this.boost3 = (MaterialButton) findViewById3;
            View findViewById4 = view.findViewById(R.id.boost_4);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.id.boost_4)");
            this.boost4 = (MaterialButton) findViewById4;
            View findViewById5 = view.findViewById(R.id.boost_5);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.boost_5)");
            this.boost5 = (MaterialButton) findViewById5;
            View findViewById6 = view.findViewById(R.id.boost_6);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "itemView.findViewById(R.id.boost_6)");
            this.boost6 = (MaterialButton) findViewById6;
            View findViewById7 = view.findViewById(R.id.boost_custom);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "itemView.findViewById(R.id.boost_custom)");
            AppCompatEditText appCompatEditText = (AppCompatEditText) findViewById7;
            this.custom = appCompatEditText;
            appCompatEditText.setFilters(new InputFilter[0]);
        }

        private final List<MaterialButton> getBoostButtons() {
            if (ViewUtil.isLtr(this.context)) {
                return CollectionsKt__CollectionsKt.listOf((Object[]) new MaterialButton[]{this.boost1, this.boost2, this.boost3, this.boost4, this.boost5, this.boost6});
            }
            return CollectionsKt__CollectionsKt.listOf((Object[]) new MaterialButton[]{this.boost3, this.boost2, this.boost1, this.boost6, this.boost5, this.boost4});
        }

        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0082, code lost:
            if (kotlin.jvm.internal.Intrinsics.areEqual(r0 != null ? r0.getCurrency() : null, r7.getCurrency()) == false) goto L_0x0084;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void bind(org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost.SelectionModel r7) {
            /*
                r6 = this;
                java.lang.String r0 = "model"
                kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r7, r0)
                android.view.View r0 = r6.itemView
                boolean r1 = r7.isEnabled()
                r0.setEnabled(r1)
                java.util.List r0 = r7.getBoosts()
                java.util.List r1 = r6.getBoostButtons()
                java.util.List r0 = kotlin.collections.CollectionsKt.zip(r0, r1)
                java.util.Iterator r0 = r0.iterator()
            L_0x001e:
                boolean r1 = r0.hasNext()
                if (r1 == 0) goto L_0x006e
                java.lang.Object r1 = r0.next()
                kotlin.Pair r1 = (kotlin.Pair) r1
                java.lang.Object r2 = r1.component1()
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost r2 = (org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost) r2
                java.lang.Object r1 = r1.component2()
                com.google.android.material.button.MaterialButton r1 = (com.google.android.material.button.MaterialButton) r1
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost r3 = r7.getSelectedBoost()
                boolean r3 = kotlin.jvm.internal.Intrinsics.areEqual(r2, r3)
                if (r3 == 0) goto L_0x0048
                boolean r3 = r7.isCustomAmountFocused()
                if (r3 != 0) goto L_0x0048
                r3 = 1
                goto L_0x0049
            L_0x0048:
                r3 = 0
            L_0x0049:
                r1.setSelected(r3)
                android.content.Context r3 = r6.context
                android.content.res.Resources r3 = r3.getResources()
                org.signal.core.util.money.FiatMoney r4 = r2.getPrice()
                org.thoughtcrime.securesms.payments.FiatMoneyUtil$FormatOptions r5 = org.thoughtcrime.securesms.payments.FiatMoneyUtil.formatOptions()
                org.thoughtcrime.securesms.payments.FiatMoneyUtil$FormatOptions r5 = r5.trimZerosAfterDecimal()
                java.lang.String r3 = org.thoughtcrime.securesms.payments.FiatMoneyUtil.format(r3, r4, r5)
                r1.setText(r3)
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionViewHolder$$ExternalSyntheticLambda0 r3 = new org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionViewHolder$$ExternalSyntheticLambda0
                r3.<init>(r7, r2, r6)
                r1.setOnClickListener(r3)
                goto L_0x001e
            L_0x006e:
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$MoneyFilter r0 = r6.filter
                if (r0 == 0) goto L_0x0084
                if (r0 == 0) goto L_0x0079
                java.util.Currency r0 = r0.getCurrency()
                goto L_0x007a
            L_0x0079:
                r0 = 0
            L_0x007a:
                java.util.Currency r1 = r7.getCurrency()
                boolean r0 = kotlin.jvm.internal.Intrinsics.areEqual(r0, r1)
                if (r0 != 0) goto L_0x00b0
            L_0x0084:
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$MoneyFilter r1 = r6.filter
                r0.removeTextChangedListener(r1)
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$MoneyFilter r0 = new org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$MoneyFilter
                java.util.Currency r1 = r7.getCurrency()
                androidx.appcompat.widget.AppCompatEditText r2 = r6.custom
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionViewHolder$bind$2 r3 = new org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionViewHolder$bind$2
                r3.<init>(r7)
                r0.<init>(r1, r2, r3)
                r6.filter = r0
                androidx.appcompat.widget.AppCompatEditText r1 = r6.custom
                r1.setKeyListener(r0)
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$MoneyFilter r1 = r6.filter
                r0.addTextChangedListener(r1)
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                java.lang.String r1 = ""
                r0.setText(r1)
            L_0x00b0:
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                boolean r1 = r7.isCustomAmountFocused()
                r0.setSelected(r1)
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionViewHolder$$ExternalSyntheticLambda1 r1 = new org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionViewHolder$$ExternalSyntheticLambda1
                r1.<init>(r7)
                r0.setOnFocusChangeListener(r1)
                boolean r0 = r7.isCustomAmountFocused()
                if (r0 == 0) goto L_0x00d7
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                boolean r0 = r0.hasFocus()
                if (r0 != 0) goto L_0x00d7
                androidx.appcompat.widget.AppCompatEditText r7 = r6.custom
                org.thoughtcrime.securesms.util.ViewUtil.focusAndShowKeyboard(r7)
                goto L_0x00f1
            L_0x00d7:
                boolean r7 = r7.isCustomAmountFocused()
                if (r7 != 0) goto L_0x00f1
                androidx.appcompat.widget.AppCompatEditText r7 = r6.custom
                boolean r7 = r7.hasFocus()
                if (r7 == 0) goto L_0x00f1
                android.content.Context r7 = r6.context
                androidx.appcompat.widget.AppCompatEditText r0 = r6.custom
                org.thoughtcrime.securesms.util.ViewUtil.hideKeyboard(r7, r0)
                androidx.appcompat.widget.AppCompatEditText r7 = r6.custom
                r7.clearFocus()
            L_0x00f1:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost.SelectionViewHolder.bind(org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost$SelectionModel):void");
        }

        /* renamed from: bind$lambda-1$lambda-0 */
        public static final void m920bind$lambda1$lambda0(SelectionModel selectionModel, Boost boost, SelectionViewHolder selectionViewHolder, View view) {
            Intrinsics.checkNotNullParameter(selectionModel, "$model");
            Intrinsics.checkNotNullParameter(boost, "$boost");
            Intrinsics.checkNotNullParameter(selectionViewHolder, "this$0");
            Function2<View, Boost, Unit> onBoostClick = selectionModel.getOnBoostClick();
            Intrinsics.checkNotNullExpressionValue(view, "it");
            onBoostClick.invoke(view, boost);
            selectionViewHolder.custom.clearFocus();
        }

        /* renamed from: bind$lambda-2 */
        public static final void m921bind$lambda2(SelectionModel selectionModel, View view, boolean z) {
            Intrinsics.checkNotNullParameter(selectionModel, "$model");
            selectionModel.getOnCustomAmountFocusChanged().invoke(Boolean.valueOf(z));
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$HeadingViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$HeadingModel;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "badgeImageView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class HeadingViewHolder extends MappingViewHolder<HeadingModel> {
        private final BadgeImageView badgeImageView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public HeadingViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.badgeImageView = (BadgeImageView) view;
        }

        public void bind(HeadingModel headingModel) {
            Intrinsics.checkNotNullParameter(headingModel, "model");
            this.badgeImageView.setBadge(headingModel.getBoostBadge());
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\f\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u00012\u00020\u0002B/\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0014\b\u0002\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\b¢\u0006\u0002\u0010\u000bJ\u0012\u0010'\u001a\u00020\n2\b\u0010(\u001a\u0004\u0018\u00010)H\u0016J*\u0010*\u001a\u00020\n2\b\u0010(\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020\u001e2\u0006\u0010-\u001a\u00020\u001e2\u0006\u0010.\u001a\u00020\u001eH\u0016J:\u0010/\u001a\u0004\u0018\u00010+2\u0006\u00100\u001a\u00020+2\u0006\u0010,\u001a\u00020\u001e2\u0006\u00101\u001a\u00020\u001e2\u0006\u00102\u001a\u0002032\u0006\u00104\u001a\u00020\u001e2\u0006\u00105\u001a\u00020\u001eH\u0016J*\u00106\u001a\u00020\n2\b\u0010(\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020\u001e2\u0006\u00107\u001a\u00020\u001e2\u0006\u0010-\u001a\u00020\u001eH\u0016R\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\u00020\tXD¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0011\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bX\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0015\u001a\u00020\u0016¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u0019\u001a\u00020\u001a¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u001d\u001a\u00020\u001e¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0011\u0010!\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0010R\u0011\u0010#\u001a\u00020\u0012¢\u0006\b\n\u0000\u001a\u0004\b$\u0010\u0014R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010%\u001a\u00020\tXD¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0010¨\u00068"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$MoneyFilter;", "Landroid/text/method/DigitsKeyListener;", "Landroid/text/TextWatcher;", "currency", "Ljava/util/Currency;", DraftDatabase.Draft.TEXT, "Landroidx/appcompat/widget/AppCompatEditText;", "onCustomAmountChanged", "Lkotlin/Function1;", "", "", "(Ljava/util/Currency;Landroidx/appcompat/widget/AppCompatEditText;Lkotlin/jvm/functions/Function1;)V", "getCurrency", "()Ljava/util/Currency;", "digitsGroup", "getDigitsGroup", "()Ljava/lang/String;", "leadingZeroesPattern", "Lkotlin/text/Regex;", "getLeadingZeroesPattern", "()Lkotlin/text/Regex;", "pattern", "Ljava/util/regex/Pattern;", "getPattern", "()Ljava/util/regex/Pattern;", "separator", "", "getSeparator", "()C", "separatorCount", "", "getSeparatorCount", "()I", "symbol", "getSymbol", "symbolPattern", "getSymbolPattern", "zeros", "getZeros", "afterTextChanged", "s", "Landroid/text/Editable;", "beforeTextChanged", "", NotificationProfileDatabase.NotificationProfileScheduleTable.START, NewHtcHomeBadger.COUNT, "after", "filter", PushDatabase.SOURCE_E164, NotificationProfileDatabase.NotificationProfileScheduleTable.END, "dest", "Landroid/text/Spanned;", "dstart", "dend", "onTextChanged", "before", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MoneyFilter extends DigitsKeyListener implements TextWatcher {
        private final Currency currency;
        private final String digitsGroup;
        private final Regex leadingZeroesPattern;
        private final Function1<String, Unit> onCustomAmountChanged;
        private final Pattern pattern;
        private final char separator;
        private final int separatorCount;
        private final String symbol;
        private final Regex symbolPattern;
        private final AppCompatEditText text;
        private final String zeros;

        @Override // android.text.TextWatcher
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override // android.text.TextWatcher
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        /* JADX DEBUG: Multi-variable search result rejected for r9v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.String, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public MoneyFilter(Currency currency, AppCompatEditText appCompatEditText, Function1<? super String, Unit> function1) {
            super(false, true);
            Intrinsics.checkNotNullParameter(currency, "currency");
            Intrinsics.checkNotNullParameter(function1, "onCustomAmountChanged");
            this.currency = currency;
            this.text = appCompatEditText;
            this.onCustomAmountChanged = function1;
            char decimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
            this.separator = decimalSeparator;
            int min = Math.min(1, currency.getDefaultFractionDigits());
            this.separatorCount = min;
            String symbol = currency.getSymbol(Locale.getDefault());
            Intrinsics.checkNotNullExpressionValue(symbol, "currency.getSymbol(Locale.getDefault())");
            this.symbol = symbol;
            this.digitsGroup = "[\\u0030-\\u0039]|[\\u0660-\\u0669]|[\\u06F0-\\u06F9]|[\\u0966-\\u096F]|[\\uFF10-\\uFF19]";
            this.zeros = "\\u0030|\\u0660|\\u06F0|\\u0966|\\uFF10";
            Pattern compile = Pattern.compile("([\\u0030-\\u0039]|[\\u0660-\\u0669]|[\\u06F0-\\u06F9]|[\\u0966-\\u096F]|[\\uFF10-\\uFF19])*([" + decimalSeparator + "]){0," + min + "}([\\u0030-\\u0039]|[\\u0660-\\u0669]|[\\u06F0-\\u06F9]|[\\u0966-\\u096F]|[\\uFF10-\\uFF19]){0," + currency.getDefaultFractionDigits() + '}', 0);
            Intrinsics.checkNotNullExpressionValue(compile, "compile(this, flags)");
            this.pattern = compile;
            StringBuilder sb = new StringBuilder();
            sb.append("\\s*");
            sb.append(Regex.Companion.escape(symbol));
            sb.append("\\s*");
            this.symbolPattern = new Regex(sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("^(");
            sb2.append("\\u0030|\\u0660|\\u06F0|\\u0966|\\uFF10");
            sb2.append(")*");
            this.leadingZeroesPattern = new Regex(sb2.toString());
        }

        public /* synthetic */ MoneyFilter(Currency currency, AppCompatEditText appCompatEditText, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(currency, (i & 2) != 0 ? null : appCompatEditText, (i & 4) != 0 ? AnonymousClass1.INSTANCE : function1);
        }

        public final Currency getCurrency() {
            return this.currency;
        }

        public final char getSeparator() {
            return this.separator;
        }

        public final int getSeparatorCount() {
            return this.separatorCount;
        }

        public final String getSymbol() {
            return this.symbol;
        }

        public final String getDigitsGroup() {
            return this.digitsGroup;
        }

        public final String getZeros() {
            return this.zeros;
        }

        public final Pattern getPattern() {
            return this.pattern;
        }

        public final Regex getSymbolPattern() {
            return this.symbolPattern;
        }

        public final Regex getLeadingZeroesPattern() {
            return this.leadingZeroesPattern;
        }

        /* JADX DEBUG: TODO: convert one arg to string using `String.valueOf()`, args: [(wrap: java.lang.Object : ?: CAST (java.lang.Object) (wrap: java.lang.CharSequence : 0x0010: INVOKE  (r3v2 java.lang.CharSequence A[REMOVE]) = (r4v0 android.text.Spanned), (0 int), (r5v0 int) type: INTERFACE call: android.text.Spanned.subSequence(int, int):java.lang.CharSequence)), (wrap: java.lang.Object : ?: CAST (java.lang.Object) (r1v0 java.lang.CharSequence)), (wrap: java.lang.Object : ?: CAST (java.lang.Object) (wrap: java.lang.CharSequence : 0x001e: INVOKE  (r1v2 java.lang.CharSequence A[REMOVE]) = 
          (r4v0 android.text.Spanned)
          (r6v0 int)
          (wrap: int : 0x001a: INVOKE  (r1v1 int A[REMOVE]) = (r4v0 android.text.Spanned) type: INTERFACE call: android.text.Spanned.length():int)
         type: INTERFACE call: android.text.Spanned.subSequence(int, int):java.lang.CharSequence))] */
        @Override // android.text.method.DigitsKeyListener, android.text.method.NumberKeyListener, android.text.InputFilter
        public CharSequence filter(CharSequence charSequence, int i, int i2, Spanned spanned, int i3, int i4) {
            Intrinsics.checkNotNullParameter(charSequence, PushDatabase.SOURCE_E164);
            Intrinsics.checkNotNullParameter(spanned, "dest");
            StringBuilder sb = new StringBuilder();
            sb.append((Object) spanned.subSequence(0, i3));
            sb.append((Object) charSequence);
            sb.append((Object) spanned.subSequence(i4, spanned.length()));
            String stripBidiIndicator = StringUtil.stripBidiIndicator(StringsKt__StringsKt.trim(StringsKt__StringsKt.removeSuffix(StringsKt__StringsKt.removePrefix(sb.toString(), (CharSequence) this.symbol), (CharSequence) this.symbol)).toString());
            Intrinsics.checkNotNullExpressionValue(stripBidiIndicator, "stripBidiIndicator(resul…oveSuffix(symbol).trim())");
            if (stripBidiIndicator.length() == 1 && !TextUtils.isDigitsOnly(stripBidiIndicator) && !Intrinsics.areEqual(stripBidiIndicator, String.valueOf(this.separator))) {
                return spanned.subSequence(i3, i4);
            }
            if (!this.pattern.matcher(stripBidiIndicator).matches()) {
                return spanned.subSequence(i3, i4);
            }
            return null;
        }

        @Override // android.text.TextWatcher
        public void afterTextChanged(Editable editable) {
            CharSequence charSequence;
            MatchResult find$default;
            AppCompatEditText appCompatEditText;
            if (!(editable == null || editable.length() == 0)) {
                boolean z = (StringsKt__StringsKt.startsWith$default((CharSequence) editable, (CharSequence) this.symbol, false, 2, (Object) null)) || (StringsKt__StringsKt.endsWith$default((CharSequence) editable, (CharSequence) this.symbol, false, 2, (Object) null));
                if (z && this.symbolPattern.matchEntire(editable.toString()) != null) {
                    editable.clear();
                } else if (!z) {
                    NumberFormat currencyInstance = NumberFormat.getCurrencyInstance();
                    currencyInstance.setCurrency(this.currency);
                    if (StringsKt__StringsKt.contains$default((CharSequence) editable, this.separator, false, 2, (Object) null)) {
                        currencyInstance.setMinimumFractionDigits(((String) CollectionsKt___CollectionsKt.last((List) ((List<? extends Object>) StringsKt__StringsKt.split$default((CharSequence) editable, new char[]{this.separator}, false, 0, 6, (Object) null)))).length());
                    } else {
                        currencyInstance.setMinimumFractionDigits(0);
                    }
                    currencyInstance.setMaximumFractionDigits(this.currency.getDefaultFractionDigits());
                    Double d = StringsKt__StringNumberConversionsJVMKt.toDoubleOrNull(editable.toString());
                    if (d != null) {
                        String format = currencyInstance.format(d.doubleValue());
                        AppCompatEditText appCompatEditText2 = this.text;
                        if (appCompatEditText2 != null) {
                            appCompatEditText2.removeTextChangedListener(this);
                        }
                        editable.replace(0, editable.length(), format);
                        Intrinsics.checkNotNullExpressionValue(format, "formatted");
                        if ((StringsKt__StringsJVMKt.endsWith$default(format, this.symbol, false, 2, null)) && (find$default = Regex.find$default(this.symbolPattern, format, 0, 2, null)) != null && find$default.getRange().getFirst() < editable.length() && (appCompatEditText = this.text) != null) {
                            appCompatEditText.setSelection(find$default.getRange().getFirst());
                        }
                        AppCompatEditText appCompatEditText3 = this.text;
                        if (appCompatEditText3 != null) {
                            appCompatEditText3.addTextChangedListener(this);
                        }
                    }
                }
                String obj = StringsKt__StringsKt.trim(StringsKt__StringsKt.removeSuffix(StringsKt__StringsKt.removePrefix(editable, this.symbol), this.symbol)).toString();
                try {
                    StringBuilder sb = new StringBuilder();
                    NumberFormat instance = NumberFormat.getInstance();
                    instance.setGroupingUsed(false);
                    if (StringsKt__StringsKt.contains$default((CharSequence) editable, this.separator, false, 2, (Object) null)) {
                        instance.setMinimumFractionDigits(((String) CollectionsKt___CollectionsKt.last((List) ((List<? extends Object>) StringsKt__StringsKt.split$default((CharSequence) editable, new char[]{this.separator}, false, 0, 6, (Object) null)))).length());
                    }
                    sb.append(instance.format(new BigDecimal(obj)));
                    sb.append(StringsKt__StringsKt.endsWith$default((CharSequence) obj, this.separator, false, 2, (Object) null) ? Character.valueOf(this.separator) : "");
                    charSequence = sb.toString();
                } catch (NumberFormatException unused) {
                    charSequence = obj;
                }
                if (!Intrinsics.areEqual(obj, charSequence)) {
                    AppCompatEditText appCompatEditText4 = this.text;
                    if (appCompatEditText4 != null) {
                        appCompatEditText4.removeTextChangedListener(this);
                    }
                    int i = StringsKt__StringsKt.indexOf$default((CharSequence) editable, obj, 0, false, 6, (Object) null);
                    editable.replace(i, obj.length() + i, charSequence);
                    AppCompatEditText appCompatEditText5 = this.text;
                    if (appCompatEditText5 != null) {
                        appCompatEditText5.addTextChangedListener(this);
                    }
                }
                this.onCustomAmountChanged.invoke(StringsKt__StringsKt.trim(StringsKt__StringsKt.removeSuffix(StringsKt__StringsKt.removePrefix(editable, this.symbol), this.symbol)).toString());
            }
        }
    }

    /* compiled from: Boost.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/Boost$Companion;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        /* renamed from: register$lambda-0 */
        public static final MappingViewHolder m917register$lambda0(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new SelectionViewHolder(view);
        }

        public final void register(MappingAdapter mappingAdapter) {
            Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
            mappingAdapter.registerFactory(SelectionModel.class, new LayoutFactory(new Boost$Companion$$ExternalSyntheticLambda0(), R.layout.boost_preference));
            mappingAdapter.registerFactory(HeadingModel.class, new LayoutFactory(new Boost$Companion$$ExternalSyntheticLambda1(), R.layout.boost_preview_preference));
            mappingAdapter.registerFactory(LoadingModel.class, new LayoutFactory(new Boost$Companion$$ExternalSyntheticLambda2(), R.layout.boost_loading_preference));
        }

        /* renamed from: register$lambda-1 */
        public static final MappingViewHolder m918register$lambda1(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new HeadingViewHolder(view);
        }

        /* renamed from: register$lambda-2 */
        public static final MappingViewHolder m919register$lambda2(View view) {
            Intrinsics.checkNotNullExpressionValue(view, "it");
            return new LoadingViewHolder(view);
        }
    }
}
