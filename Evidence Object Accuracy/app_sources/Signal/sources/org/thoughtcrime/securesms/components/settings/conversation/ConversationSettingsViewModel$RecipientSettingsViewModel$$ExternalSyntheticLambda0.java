package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda0 implements Function {
    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.RecipientSettingsViewModel.m1154revealAllMembers$lambda3((ConversationSettingsState) obj);
    }
}
