package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class FirstInviteReminder extends Reminder {
    public FirstInviteReminder(Context context, Recipient recipient, int i) {
        super(context.getString(R.string.FirstInviteReminder__title), context.getString(R.string.FirstInviteReminder__description, Integer.valueOf(i)));
        addAction(new Reminder.Action(context.getString(R.string.InsightsReminder__invite), R.id.reminder_action_invite));
        addAction(new Reminder.Action(context.getString(R.string.InsightsReminder__view_insights), R.id.reminder_action_view_insights));
    }
}
