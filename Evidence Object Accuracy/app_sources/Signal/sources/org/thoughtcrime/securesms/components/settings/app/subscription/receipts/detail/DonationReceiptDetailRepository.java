package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.Locale;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;
import org.whispersystems.signalservice.internal.ServiceResponse;

/* compiled from: DonationReceiptDetailRepository.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\t0\u00042\u0006\u0010\n\u001a\u00020\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/detail/DonationReceiptDetailRepository;", "", "()V", "getDonationReceiptRecord", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", ContactRepository.ID_COLUMN, "", "getSubscriptionLevelName", "", "subscriptionLevel", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptDetailRepository {
    public final Single<String> getSubscriptionLevelName(int i) {
        Single<String> subscribeOn = ApplicationDependencies.getDonationsService().getSubscriptionLevels(Locale.getDefault()).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailRepository$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationReceiptDetailRepository.m1001$r8$lambda$gy_ig8S3IkToZ7P3LTlNhho3cU((ServiceResponse) obj);
            }
        }).map(new Function(i) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationReceiptDetailRepository.m999$r8$lambda$ZDMhQM3qHgkG_2G1uXm_VI8FLk(this.f$0, (SubscriptionLevels) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailRepository$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return DonationReceiptDetailRepository.m1000$r8$lambda$bNjRL6Ea6lRw5Q_D9Az69dB7FQ((SubscriptionLevels.Level) obj);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "getDonationsService()\n  …scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getSubscriptionLevelName$lambda-0 */
    public static final SingleSource m1004getSubscriptionLevelName$lambda0(ServiceResponse serviceResponse) {
        return serviceResponse.flattenResult();
    }

    /* renamed from: getSubscriptionLevelName$lambda-1 */
    public static final SubscriptionLevels.Level m1005getSubscriptionLevelName$lambda1(int i, SubscriptionLevels subscriptionLevels) {
        SubscriptionLevels.Level level = subscriptionLevels.getLevels().get(String.valueOf(i));
        if (level != null) {
            return level;
        }
        throw new Exception("Subscription level " + i + " not found");
    }

    /* renamed from: getSubscriptionLevelName$lambda-2 */
    public static final String m1006getSubscriptionLevelName$lambda2(SubscriptionLevels.Level level) {
        return level.getName();
    }

    public final Single<DonationReceiptRecord> getDonationReceiptRecord(long j) {
        Single<DonationReceiptRecord> subscribeOn = Single.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.detail.DonationReceiptDetailRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$0;

            {
                this.f$0 = r1;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationReceiptDetailRepository.m1002$r8$lambda$yHMdZJx9cHd1n26fUKpombR3w(this.f$0);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable<DonationRec…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getDonationReceiptRecord$lambda-3 */
    public static final DonationReceiptRecord m1003getDonationReceiptRecord$lambda3(long j) {
        return SignalDatabase.Companion.donationReceipts().getReceipt(j);
    }
}
