package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import androidx.core.content.ContextCompat;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class OutlinedThumbnailView extends ThumbnailView {
    private CornerMask cornerMask;
    private Outliner outliner;

    public OutlinedThumbnailView(Context context) {
        super(context);
        init(null);
    }

    public OutlinedThumbnailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        int i;
        this.cornerMask = new CornerMask(this);
        this.outliner = new Outliner();
        int color = ContextCompat.getColor(getContext(), R.color.signal_inverse_transparent_20);
        this.outliner.setColor(color);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.OutlinedThumbnailView, 0, 0);
            i = obtainStyledAttributes.getDimensionPixelOffset(0, 0);
            this.outliner.setStrokeWidth((float) obtainStyledAttributes.getDimensionPixelSize(2, 1));
            this.outliner.setColor(obtainStyledAttributes.getColor(1, color));
        } else {
            i = 0;
        }
        setRadius(i);
        setCorners(i, i, i, i);
        setWillNotDraw(false);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        this.cornerMask.mask(canvas);
        this.outliner.draw(canvas);
    }

    public void setCorners(int i, int i2, int i3, int i4) {
        this.cornerMask.setRadii(i, i2, i3, i4);
        this.outliner.setRadii(i, i2, i3, i4);
        postInvalidate();
    }
}
