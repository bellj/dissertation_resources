package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import android.view.View;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: AdvancedPrivacySettingsFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\r\n\u0002\b\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class AdvancedPrivacySettingsFragment$sealedSenderSummary$2 extends Lambda implements Function0<CharSequence> {
    final /* synthetic */ AdvancedPrivacySettingsFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public AdvancedPrivacySettingsFragment$sealedSenderSummary$2(AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment) {
        super(0);
        this.this$0 = advancedPrivacySettingsFragment;
    }

    @Override // kotlin.jvm.functions.Function0
    public final CharSequence invoke() {
        return SpanUtil.learnMore(this.this$0.requireContext(), ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_text_primary), new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$sealedSenderSummary$2$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                AdvancedPrivacySettingsFragment$sealedSenderSummary$2.m845invoke$lambda0(AdvancedPrivacySettingsFragment.this, view);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m845invoke$lambda0(AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment, View view) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsFragment, "this$0");
        CommunicationActions.openBrowserLink(advancedPrivacySettingsFragment.requireContext(), advancedPrivacySettingsFragment.getString(R.string.AdvancedPrivacySettingsFragment__sealed_sender_link));
    }
}
