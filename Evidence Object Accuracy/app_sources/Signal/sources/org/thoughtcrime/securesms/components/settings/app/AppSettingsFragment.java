package org.thoughtcrime.securesms.components.settings.app;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.FragmentKt;
import j$.util.function.Function;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.PreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.PlayServicesUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: AppSettingsFragment.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001:\u0006\u0014\u0015\u0016\u0017\u0018\u0019B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\b\u0010\r\u001a\u00020\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\b\u0010\u0013\u001a\u00020\nH\u0016R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "copySubscriberIdToClipboard", "", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsState;", "onResume", "BioPreference", "BioPreferenceViewHolder", "PaymentsPreference", "PaymentsPreferenceViewHolder", "SubscriptionPreference", "SubscriptionPreferenceViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppSettingsFragment extends DSLSettingsFragment {
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(AppSettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public AppSettingsFragment() {
        super(R.string.text_secure_normal__menu_settings, 0, 0, null, 14, null);
    }

    private final AppSettingsViewModel getViewModel() {
        return (AppSettingsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        dSLSettingsAdapter.registerFactory(BioPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new AppSettingsFragment.BioPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.bio_preference_item));
        dSLSettingsAdapter.registerFactory(PaymentsPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new AppSettingsFragment.PaymentsPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_payments_preference));
        dSLSettingsAdapter.registerFactory(SubscriptionPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new AppSettingsFragment.SubscriptionPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$$ExternalSyntheticLambda3
            public final /* synthetic */ AppSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AppSettingsFragment.m547$r8$lambda$s8kQ6wBNQFS1zjVdBbsObOlEXU(DSLSettingsAdapter.this, this.f$1, (AppSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m548bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, AppSettingsFragment appSettingsFragment, AppSettingsState appSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(appSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(appSettingsState, "state");
        dSLSettingsAdapter.submitList(appSettingsFragment.getConfiguration(appSettingsState).toMappingModelList());
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refreshExpiredGiftBadge();
    }

    private final DSLConfiguration getConfiguration(AppSettingsState appSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(appSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1
            final /* synthetic */ AppSettingsState $state;
            final /* synthetic */ AppSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Object obj;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                Recipient self = this.$state.getSelf();
                final AppSettingsFragment appSettingsFragment = this.this$0;
                dSLConfiguration.customPref(new AppSettingsFragment.BioPreference(self, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment), (int) R.id.action_appSettingsFragment_to_manageProfileActivity);
                    }
                }));
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.AccountSettingsFragment__account, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon.Companion companion2 = DSLSettingsIcon.Companion;
                DSLSettingsIcon from$default = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_profile_circle_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment2 = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment2), (int) R.id.action_appSettingsFragment_to_accountSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from2 = companion.from(R.string.preferences__linked_devices, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default2 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_linked_devices_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment3 = this.this$0;
                dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default2, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment3), (int) R.id.action_appSettingsFragment_to_deviceActivity);
                    }
                }, (r18 & 64) != 0 ? null : null);
                if (!FeatureFlags.donorBadges() || PlayServicesUtil.getPlayServicesStatus(this.this$0.requireContext()) != PlayServicesUtil.PlayServicesStatus.SUCCESS) {
                    obj = null;
                    dSLConfiguration.externalLinkPref(companion.from(R.string.preferences__donate_to_signal, new DSLSettingsText.Modifier[0]), DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_heart_24, 0, 2, null), R.string.donate_url);
                } else {
                    DSLSettingsText from3 = companion.from(R.string.preferences__donate_to_signal, new DSLSettingsText.Modifier[0]);
                    DSLSettingsIcon from$default3 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_heart_24, 0, 2, null);
                    DSLSettingsIcon from4 = this.$state.getHasExpiredGiftBadge() ? companion2.from(R.drawable.ic_info_solid_24, R.color.signal_accent_primary) : null;
                    final AppSettingsFragment appSettingsFragment4 = this.this$0;
                    dSLConfiguration.clickPref(from3, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default3, (r18 & 8) != 0 ? null : from4, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.4
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            NavController findNavController = FragmentKt.findNavController(appSettingsFragment4);
                            NavDirections actionAppSettingsFragmentToManageDonationsFragment = AppSettingsFragmentDirections.actionAppSettingsFragmentToManageDonationsFragment();
                            Intrinsics.checkNotNullExpressionValue(actionAppSettingsFragmentToManageDonationsFragment, "actionAppSettingsFragmen…ManageDonationsFragment()");
                            SafeNavigation.safeNavigate(findNavController, actionAppSettingsFragmentToManageDonationsFragment);
                        }
                    }, (r18 & 64) != 0 ? null : new Function0<Boolean>(this.this$0) { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.5
                        @Override // kotlin.jvm.functions.Function0
                        public final Boolean invoke() {
                            return Boolean.valueOf(AppSettingsFragment.access$copySubscriberIdToClipboard((AppSettingsFragment) this.receiver));
                        }
                    });
                    obj = null;
                }
                dSLConfiguration.dividerPref();
                DSLSettingsText from5 = companion.from(R.string.preferences__appearance, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default4 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_appearance_24, 0, 2, obj);
                final AppSettingsFragment appSettingsFragment5 = this.this$0;
                dSLConfiguration.clickPref(from5, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default4, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.6
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment5), (int) R.id.action_appSettingsFragment_to_appearanceSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from6 = companion.from(R.string.preferences_chats__chats, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default5 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_chat_message_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment6 = this.this$0;
                dSLConfiguration.clickPref(from6, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default5, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.7
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment6), (int) R.id.action_appSettingsFragment_to_chatsSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from7 = companion.from(R.string.preferences__notifications, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default6 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_bell_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment7 = this.this$0;
                dSLConfiguration.clickPref(from7, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default6, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.8
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment7), (int) R.id.action_appSettingsFragment_to_notificationsSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from8 = companion.from(R.string.preferences__privacy, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default7 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_lock_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment8 = this.this$0;
                dSLConfiguration.clickPref(from8, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default7, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.9
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment8), (int) R.id.action_appSettingsFragment_to_privacySettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from9 = companion.from(R.string.preferences__data_and_storage, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default8 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_archive_24dp, 0, 2, null);
                final AppSettingsFragment appSettingsFragment9 = this.this$0;
                dSLConfiguration.clickPref(from9, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default8, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.10
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment9), (int) R.id.action_appSettingsFragment_to_dataAndStorageSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                dSLConfiguration.dividerPref();
                if (SignalStore.paymentsValues().getPaymentsAvailability().showPaymentsMenu()) {
                    int unreadPaymentsCount = this.$state.getUnreadPaymentsCount();
                    final AppSettingsFragment appSettingsFragment10 = this.this$0;
                    dSLConfiguration.customPref(new AppSettingsFragment.PaymentsPreference(unreadPaymentsCount, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.11
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment10), (int) R.id.action_appSettingsFragment_to_paymentsActivity);
                        }
                    }));
                    dSLConfiguration.dividerPref();
                }
                DSLSettingsText from10 = companion.from(R.string.preferences__help, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default9 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_help_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment11 = this.this$0;
                dSLConfiguration.clickPref(from10, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default9, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.12
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment11), (int) R.id.action_appSettingsFragment_to_helpSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from11 = companion.from(R.string.AppSettingsFragment__invite_your_friends, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default10 = DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_invite_24, 0, 2, null);
                final AppSettingsFragment appSettingsFragment12 = this.this$0;
                dSLConfiguration.clickPref(from11, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default10, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.13
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment12), (int) R.id.action_appSettingsFragment_to_inviteActivity);
                    }
                }, (r18 & 64) != 0 ? null : null);
                if (FeatureFlags.internalUser()) {
                    dSLConfiguration.dividerPref();
                    DSLSettingsText from12 = companion.from(R.string.preferences__internal_preferences, new DSLSettingsText.Modifier[0]);
                    final AppSettingsFragment appSettingsFragment13 = this.this$0;
                    dSLConfiguration.clickPref(from12, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment$getConfiguration$1.14
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            SafeNavigation.safeNavigate(FragmentKt.findNavController(appSettingsFragment13), (int) R.id.action_appSettingsFragment_to_internalSettingsFragment);
                        }
                    }, (r18 & 64) != 0 ? null : null);
                }
            }
        });
    }

    public final boolean copySubscriberIdToClipboard() {
        Subscriber subscriber = SignalStore.donationsValues().getSubscriber();
        if (subscriber == null) {
            return false;
        }
        Toast.makeText(requireContext(), (int) R.string.AppSettingsFragment__copied_subscriber_id_to_clipboard, 1).show();
        Util.copyToClipboard(requireContext(), subscriber.getSubscriberId().serialize());
        return true;
    }

    /* compiled from: AppSettingsFragment.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000f\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B[\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\b\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u000e¢\u0006\u0002\u0010\u000fJ\u0010\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u0000H\u0016J\u0010\u0010\u001c\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u0000H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0012R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0012R\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0018¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$SubscriptionPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "isActive", "onClick", "Lkotlin/Function1;", "", "onLongClick", "Lkotlin/Function0;", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZZLkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "getOnLongClick", "()Lkotlin/jvm/functions/Function0;", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getTitle", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SubscriptionPreference extends PreferenceModel<SubscriptionPreference> {
        private final DSLSettingsIcon icon;
        private final boolean isActive;
        private final boolean isEnabled;
        private final Function1<Boolean, Unit> onClick;
        private final Function0<Boolean> onLongClick;
        private final DSLSettingsText summary;
        private final DSLSettingsText title;

        public boolean areItemsTheSame(SubscriptionPreference subscriptionPreference) {
            Intrinsics.checkNotNullParameter(subscriptionPreference, "newItem");
            return true;
        }

        public /* synthetic */ SubscriptionPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, boolean z, boolean z2, Function1 function1, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(dSLSettingsText, (i & 2) != 0 ? null : dSLSettingsText2, (i & 4) != 0 ? null : dSLSettingsIcon, (i & 8) != 0 ? true : z, (i & 16) != 0 ? false : z2, function1, function0);
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsText getTitle() {
            return this.title;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsText getSummary() {
            return this.summary;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public DSLSettingsIcon getIcon() {
            return this.icon;
        }

        @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
        public boolean isEnabled() {
            return this.isEnabled;
        }

        public final boolean isActive() {
            return this.isActive;
        }

        public final Function1<Boolean, Unit> getOnClick() {
            return this.onClick;
        }

        public final Function0<Boolean> getOnLongClick() {
            return this.onLongClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r18v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SubscriptionPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, boolean z, boolean z2, Function1<? super Boolean, Unit> function1, Function0<Boolean> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
            Intrinsics.checkNotNullParameter(function1, "onClick");
            Intrinsics.checkNotNullParameter(function0, "onLongClick");
            this.title = dSLSettingsText;
            this.summary = dSLSettingsText2;
            this.icon = dSLSettingsIcon;
            this.isEnabled = z;
            this.isActive = z2;
            this.onClick = function1;
            this.onLongClick = function0;
        }

        public boolean areContentsTheSame(SubscriptionPreference subscriptionPreference) {
            Intrinsics.checkNotNullParameter(subscriptionPreference, "newItem");
            return super.areContentsTheSame(subscriptionPreference) && this.isActive == subscriptionPreference.isActive;
        }
    }

    /* compiled from: AppSettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$SubscriptionPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$SubscriptionPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SubscriptionPreferenceViewHolder extends PreferenceViewHolder<SubscriptionPreference> {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public SubscriptionPreferenceViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
        }

        public void bind(SubscriptionPreference subscriptionPreference) {
            Intrinsics.checkNotNullParameter(subscriptionPreference, "model");
            super.bind((SubscriptionPreferenceViewHolder) subscriptionPreference);
            this.itemView.setOnClickListener(new AppSettingsFragment$SubscriptionPreferenceViewHolder$$ExternalSyntheticLambda0(subscriptionPreference));
            this.itemView.setOnLongClickListener(new AppSettingsFragment$SubscriptionPreferenceViewHolder$$ExternalSyntheticLambda1(subscriptionPreference));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m552bind$lambda0(SubscriptionPreference subscriptionPreference, View view) {
            Intrinsics.checkNotNullParameter(subscriptionPreference, "$model");
            subscriptionPreference.getOnClick().invoke(Boolean.valueOf(subscriptionPreference.isActive()));
        }

        /* renamed from: bind$lambda-1 */
        public static final boolean m553bind$lambda1(SubscriptionPreference subscriptionPreference, View view) {
            Intrinsics.checkNotNullParameter(subscriptionPreference, "$model");
            return subscriptionPreference.getOnLongClick().invoke().booleanValue();
        }
    }

    /* compiled from: AppSettingsFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$BioPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class BioPreference extends PreferenceModel<BioPreference> {
        private final Function0<Unit> onClick;
        private final Recipient recipient;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BioPreference(Recipient recipient, Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.recipient = recipient;
            this.onClick = function0;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public boolean areContentsTheSame(BioPreference bioPreference) {
            Intrinsics.checkNotNullParameter(bioPreference, "newItem");
            return super.areContentsTheSame(bioPreference) && this.recipient.hasSameContent(bioPreference.recipient);
        }

        public boolean areItemsTheSame(BioPreference bioPreference) {
            Intrinsics.checkNotNullParameter(bioPreference, "newItem");
            return Intrinsics.areEqual(this.recipient, bioPreference.recipient);
        }
    }

    /* compiled from: AppSettingsFragment.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$BioPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$BioPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "aboutView", "Landroid/widget/TextView;", "avatarView", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "badgeView", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class BioPreferenceViewHolder extends PreferenceViewHolder<BioPreference> {
        private final TextView aboutView;
        private final AvatarImageView avatarView;
        private final BadgeImageView badgeView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public BioPreferenceViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.icon);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.icon)");
            this.avatarView = (AvatarImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.about);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.about)");
            this.aboutView = (TextView) findViewById2;
            View findViewById3 = view.findViewById(R.id.badge);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.badge)");
            this.badgeView = (BadgeImageView) findViewById3;
        }

        public void bind(BioPreference bioPreference) {
            Intrinsics.checkNotNullParameter(bioPreference, "model");
            super.bind((BioPreferenceViewHolder) bioPreference);
            this.itemView.setOnClickListener(new AppSettingsFragment$BioPreferenceViewHolder$$ExternalSyntheticLambda0(bioPreference));
            getTitleView().setText(bioPreference.getRecipient().getProfileName().toString());
            getSummaryView().setText(PhoneNumberFormatter.prettyPrint(bioPreference.getRecipient().requireE164()));
            this.avatarView.setRecipient(Recipient.self());
            this.badgeView.setBadgeFromRecipient(Recipient.self());
            getTitleView().setVisibility(0);
            getSummaryView().setVisibility(0);
            this.avatarView.setVisibility(0);
            if (bioPreference.getRecipient().getCombinedAboutAndEmoji() != null) {
                this.aboutView.setText(bioPreference.getRecipient().getCombinedAboutAndEmoji());
                this.aboutView.setVisibility(0);
                return;
            }
            this.aboutView.setVisibility(8);
        }

        /* renamed from: bind$lambda-0 */
        public static final void m549bind$lambda0(BioPreference bioPreference, View view) {
            Intrinsics.checkNotNullParameter(bioPreference, "$model");
            bioPreference.getOnClick().invoke();
        }
    }

    /* compiled from: AppSettingsFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016J\u0010\u0010\u000f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u0000H\u0016R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$PaymentsPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "unreadCount", "", "onClick", "Lkotlin/Function0;", "", "(ILkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getUnreadCount", "()I", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PaymentsPreference extends PreferenceModel<PaymentsPreference> {
        private final Function0<Unit> onClick;
        private final int unreadCount;

        public boolean areItemsTheSame(PaymentsPreference paymentsPreference) {
            Intrinsics.checkNotNullParameter(paymentsPreference, "newItem");
            return true;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PaymentsPreference(int i, Function0<Unit> function0) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.unreadCount = i;
            this.onClick = function0;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        public final int getUnreadCount() {
            return this.unreadCount;
        }

        public boolean areContentsTheSame(PaymentsPreference paymentsPreference) {
            Intrinsics.checkNotNullParameter(paymentsPreference, "newItem");
            return super.areContentsTheSame(paymentsPreference) && this.unreadCount == paymentsPreference.unreadCount;
        }
    }

    /* compiled from: AppSettingsFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$PaymentsPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsFragment$PaymentsPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "unreadCountView", "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class PaymentsPreferenceViewHolder extends MappingViewHolder<PaymentsPreference> {
        private final TextView unreadCountView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public PaymentsPreferenceViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.unread_indicator);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.unread_indicator)");
            this.unreadCountView = (TextView) findViewById;
        }

        public void bind(PaymentsPreference paymentsPreference) {
            Intrinsics.checkNotNullParameter(paymentsPreference, "model");
            this.unreadCountView.setText(String.valueOf(paymentsPreference.getUnreadCount()));
            this.unreadCountView.setVisibility(paymentsPreference.getUnreadCount() > 0 ? 0 : 8);
            this.itemView.setOnClickListener(new AppSettingsFragment$PaymentsPreferenceViewHolder$$ExternalSyntheticLambda0(paymentsPreference));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m550bind$lambda0(PaymentsPreference paymentsPreference, View view) {
            Intrinsics.checkNotNullParameter(paymentsPreference, "$model");
            paymentsPreference.getOnClick().invoke();
        }
    }
}
