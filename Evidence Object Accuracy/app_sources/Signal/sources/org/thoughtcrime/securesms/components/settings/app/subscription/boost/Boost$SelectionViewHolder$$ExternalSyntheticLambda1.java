package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Boost$SelectionViewHolder$$ExternalSyntheticLambda1 implements View.OnFocusChangeListener {
    public final /* synthetic */ Boost.SelectionModel f$0;

    public /* synthetic */ Boost$SelectionViewHolder$$ExternalSyntheticLambda1(Boost.SelectionModel selectionModel) {
        this.f$0 = selectionModel;
    }

    @Override // android.view.View.OnFocusChangeListener
    public final void onFocusChange(View view, boolean z) {
        Boost.SelectionViewHolder.m921bind$lambda2(this.f$0, view, z);
    }
}
