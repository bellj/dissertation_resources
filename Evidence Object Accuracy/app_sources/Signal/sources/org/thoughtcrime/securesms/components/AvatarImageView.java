package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import java.util.ArrayList;
import java.util.Objects;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsActivity;
import org.thoughtcrime.securesms.contacts.avatars.ContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.ResourceContactPhoto;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.conversation.colors.ChatColors;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;
import org.thoughtcrime.securesms.util.AvatarUtil;
import org.thoughtcrime.securesms.util.BlurTransformation;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class AvatarImageView extends AppCompatImageView {
    private static final int SIZE_LARGE;
    private static final int SIZE_SMALL;
    private static final String TAG = Log.tag(AvatarImageView.class);
    private boolean blurred;
    private ChatColors chatColors;
    private Recipient.FallbackPhotoProvider fallbackPhotoProvider;
    private FixedSizeTarget fixedSizeTarget;
    private boolean inverted;
    private View.OnClickListener listener;
    private RecipientContactPhoto recipientContactPhoto;
    private int size;
    private Drawable unknownRecipientDrawable;

    public AvatarImageView(Context context) {
        super(context);
        initialize(context, null);
    }

    public AvatarImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(context, attributeSet);
    }

    public void initialize(Context context, AttributeSet attributeSet) {
        setScaleType(ImageView.ScaleType.CENTER_CROP);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.AvatarImageView, 0, 0);
            this.inverted = obtainStyledAttributes.getBoolean(1, false);
            this.size = obtainStyledAttributes.getInt(0, 1);
            obtainStyledAttributes.recycle();
        }
        this.unknownRecipientDrawable = new ResourceContactPhoto(R.drawable.ic_profile_outline_40, R.drawable.ic_profile_outline_20).asDrawable(context, AvatarColor.UNKNOWN, this.inverted);
        this.blurred = false;
        this.chatColors = null;
    }

    @Override // android.view.View
    public void setClipBounds(Rect rect) {
        super.setClipBounds(rect);
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.listener = onClickListener;
        super.setOnClickListener(onClickListener);
    }

    public void setFallbackPhotoProvider(Recipient.FallbackPhotoProvider fallbackPhotoProvider) {
        this.fallbackPhotoProvider = fallbackPhotoProvider;
    }

    public void setRecipient(Recipient recipient) {
        setRecipient(recipient, false);
    }

    public void setRecipient(Recipient recipient, boolean z) {
        if (recipient.isSelf()) {
            setAvatar(GlideApp.with(this), (Recipient) null, z);
            AvatarUtil.loadIconIntoImageView(recipient, this);
            return;
        }
        setAvatar(GlideApp.with(this), recipient, z);
    }

    public AvatarOptions.Builder buildOptions() {
        return new AvatarOptions.Builder(this);
    }

    public void setAvatar(Recipient recipient) {
        setAvatar(GlideApp.with(this), recipient, false);
    }

    public void setAvatarUsingProfile(Recipient recipient) {
        setAvatar(GlideApp.with(this), recipient, false, true);
    }

    public void setAvatar(GlideRequests glideRequests, Recipient recipient, boolean z) {
        setAvatar(glideRequests, recipient, z, false);
    }

    public void setAvatar(GlideRequests glideRequests, Recipient recipient, boolean z, boolean z2) {
        setAvatar(glideRequests, recipient, new AvatarOptions.Builder(this).withUseSelfProfileAvatar(z2).withQuickContactEnabled(z).build());
    }

    public void setAvatar(Recipient recipient, AvatarOptions avatarOptions) {
        setAvatar(GlideApp.with(this), recipient, avatarOptions);
    }

    private void setAvatar(GlideRequests glideRequests, Recipient recipient, AvatarOptions avatarOptions) {
        RecipientContactPhoto recipientContactPhoto;
        Drawable drawable;
        if (recipient != null) {
            if (!recipient.isSelf() || !avatarOptions.useSelfProfileAvatar) {
                recipientContactPhoto = new RecipientContactPhoto(recipient);
            } else {
                recipientContactPhoto = new RecipientContactPhoto(recipient, new ProfileContactPhoto(Recipient.self()));
            }
            boolean shouldBlurAvatar = recipient.shouldBlurAvatar();
            ChatColors chatColors = recipient.getChatColors();
            if (!recipientContactPhoto.equals(this.recipientContactPhoto) || shouldBlurAvatar != this.blurred || !Objects.equals(chatColors, this.chatColors)) {
                glideRequests.clear(this);
                this.chatColors = chatColors;
                this.recipientContactPhoto = recipientContactPhoto;
                if (this.size == 2) {
                    drawable = recipientContactPhoto.recipient.getSmallFallbackContactPhotoDrawable(getContext(), this.inverted, this.fallbackPhotoProvider, ViewUtil.getWidth(this));
                } else {
                    drawable = recipientContactPhoto.recipient.getFallbackContactPhotoDrawable(getContext(), this.inverted, this.fallbackPhotoProvider, ViewUtil.getWidth(this));
                }
                FixedSizeTarget fixedSizeTarget = this.fixedSizeTarget;
                if (fixedSizeTarget != null) {
                    glideRequests.clear(fixedSizeTarget);
                }
                if (recipientContactPhoto.contactPhoto != null) {
                    ArrayList arrayList = new ArrayList();
                    if (shouldBlurAvatar) {
                        arrayList.add(new BlurTransformation(ApplicationDependencies.getApplication(), 0.25f, 25.0f));
                    }
                    arrayList.add(new CircleCrop());
                    this.blurred = shouldBlurAvatar;
                    GlideRequest<Drawable> transform = glideRequests.load((Object) recipientContactPhoto.contactPhoto).dontAnimate().fallback(drawable).error(drawable).diskCacheStrategy(DiskCacheStrategy.ALL).downsample(DownsampleStrategy.CENTER_INSIDE).transform((Transformation<Bitmap>) new MultiTransformation(arrayList));
                    if (avatarOptions.fixedSize > 0) {
                        FixedSizeTarget fixedSizeTarget2 = new FixedSizeTarget(avatarOptions.fixedSize);
                        this.fixedSizeTarget = fixedSizeTarget2;
                        transform.into((GlideRequest<Drawable>) fixedSizeTarget2);
                    } else {
                        transform.into(this);
                    }
                } else {
                    setImageDrawable(drawable);
                }
            }
            setAvatarClickHandler(recipient, avatarOptions.quickContactEnabled);
            return;
        }
        this.recipientContactPhoto = null;
        glideRequests.clear(this);
        Recipient.FallbackPhotoProvider fallbackPhotoProvider = this.fallbackPhotoProvider;
        if (fallbackPhotoProvider != null) {
            setImageDrawable(fallbackPhotoProvider.getPhotoForRecipientWithoutName().asDrawable(getContext(), AvatarColor.UNKNOWN, this.inverted));
        } else {
            setImageDrawable(this.unknownRecipientDrawable);
        }
        disableQuickContact();
    }

    private void setAvatarClickHandler(Recipient recipient, boolean z) {
        if (z) {
            super.setOnClickListener(new View.OnClickListener(recipient) { // from class: org.thoughtcrime.securesms.components.AvatarImageView$$ExternalSyntheticLambda0
                public final /* synthetic */ Recipient f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AvatarImageView.this.lambda$setAvatarClickHandler$0(this.f$1, view);
                }
            });
        } else {
            disableQuickContact();
        }
    }

    public /* synthetic */ void lambda$setAvatarClickHandler$0(Recipient recipient, View view) {
        Context context = getContext();
        if (recipient.isPushGroup()) {
            context.startActivity(ConversationSettingsActivity.forGroup(context, recipient.requireGroupId().requirePush()), ConversationSettingsActivity.createTransitionBundle(context, this));
        } else if (context instanceof FragmentActivity) {
            RecipientBottomSheetDialogFragment.create(recipient.getId(), null).show(((FragmentActivity) context).getSupportFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        } else {
            context.startActivity(ConversationSettingsActivity.forRecipient(context, recipient.getId()), ConversationSettingsActivity.createTransitionBundle(context, this));
        }
    }

    public void setImageBytesForGroup(byte[] bArr, Recipient.FallbackPhotoProvider fallbackPhotoProvider, AvatarColor avatarColor) {
        Drawable asDrawable = ((Recipient.FallbackPhotoProvider) Util.firstNonNull(fallbackPhotoProvider, Recipient.DEFAULT_FALLBACK_PHOTO_PROVIDER)).getPhotoForGroup().asDrawable(getContext(), avatarColor);
        GlideApp.with(this).load(bArr).dontAnimate().fallback(asDrawable).error(asDrawable).diskCacheStrategy(DiskCacheStrategy.ALL).circleCrop().into(this);
    }

    public void setNonAvatarImageResource(int i) {
        this.recipientContactPhoto = null;
        setImageResource(i);
    }

    public void disableQuickContact() {
        super.setOnClickListener(this.listener);
        setClickable(this.listener != null);
    }

    /* loaded from: classes4.dex */
    public static class RecipientContactPhoto {
        private final ContactPhoto contactPhoto;
        private final boolean ready;
        private final Recipient recipient;

        RecipientContactPhoto(Recipient recipient) {
            this(recipient, recipient.getContactPhoto());
        }

        RecipientContactPhoto(Recipient recipient, ContactPhoto contactPhoto) {
            this.recipient = recipient;
            this.ready = !recipient.isResolving();
            this.contactPhoto = contactPhoto;
        }

        public boolean equals(RecipientContactPhoto recipientContactPhoto) {
            if (recipientContactPhoto != null && recipientContactPhoto.recipient.equals(this.recipient) && recipientContactPhoto.recipient.getChatColors().equals(this.recipient.getChatColors()) && recipientContactPhoto.ready == this.ready && Objects.equals(recipientContactPhoto.contactPhoto, this.contactPhoto)) {
                return true;
            }
            return false;
        }
    }

    /* loaded from: classes4.dex */
    public final class FixedSizeTarget extends SimpleTarget<Drawable> {
        @Override // com.bumptech.glide.request.target.Target
        public /* bridge */ /* synthetic */ void onResourceReady(Object obj, Transition transition) {
            onResourceReady((Drawable) obj, (Transition<? super Drawable>) transition);
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        FixedSizeTarget(int i) {
            super(i, i);
            AvatarImageView.this = r1;
        }

        public void onResourceReady(Drawable drawable, Transition<? super Drawable> transition) {
            AvatarImageView.this.setImageDrawable(drawable);
        }
    }

    /* loaded from: classes4.dex */
    public static final class AvatarOptions {
        private final int fixedSize;
        private final boolean quickContactEnabled;
        private final boolean useSelfProfileAvatar;

        private AvatarOptions(Builder builder) {
            this.quickContactEnabled = builder.quickContactEnabled;
            this.useSelfProfileAvatar = builder.useSelfProfileAvatar;
            this.fixedSize = builder.fixedSize;
        }

        /* loaded from: classes4.dex */
        public static final class Builder {
            private final AvatarImageView avatarImageView;
            private int fixedSize;
            private boolean quickContactEnabled;
            private boolean useSelfProfileAvatar;

            private Builder(AvatarImageView avatarImageView) {
                this.quickContactEnabled = false;
                this.useSelfProfileAvatar = false;
                this.fixedSize = -1;
                this.avatarImageView = avatarImageView;
            }

            public Builder withQuickContactEnabled(boolean z) {
                this.quickContactEnabled = z;
                return this;
            }

            public Builder withUseSelfProfileAvatar(boolean z) {
                this.useSelfProfileAvatar = z;
                return this;
            }

            public Builder withFixedSize(int i) {
                this.fixedSize = i;
                return this;
            }

            public AvatarOptions build() {
                return new AvatarOptions(this);
            }

            public void load(Recipient recipient) {
                this.avatarImageView.setAvatar(recipient, build());
            }
        }
    }
}
