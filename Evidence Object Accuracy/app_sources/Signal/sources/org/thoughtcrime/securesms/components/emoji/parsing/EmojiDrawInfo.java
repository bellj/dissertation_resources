package org.thoughtcrime.securesms.components.emoji.parsing;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.emoji.EmojiPage;

/* compiled from: EmojiDrawInfo.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0011\n\u0002\u0010\u000b\n\u0002\b\u0004\b\b\u0018\u00002\u00020\u0001B1\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\nJ\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0007HÂ\u0003J\u000b\u0010\u0015\u001a\u0004\u0018\u00010\u0007HÆ\u0003J\u000b\u0010\u0016\u001a\u0004\u0018\u00010\u0007HÆ\u0003J?\u0010\u0017\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007HÆ\u0001J\u0013\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001b\u001a\u00020\u0005HÖ\u0001J\t\u0010\u001c\u001a\u00020\u0007HÖ\u0001R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\t\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0013\u0010\b\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000e¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/components/emoji/parsing/EmojiDrawInfo;", "", "page", "Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "index", "", "emoji", "", "rawEmoji", "jumboSheet", "(Lorg/thoughtcrime/securesms/emoji/EmojiPage;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getIndex", "()I", "getJumboSheet", "()Ljava/lang/String;", "getPage", "()Lorg/thoughtcrime/securesms/emoji/EmojiPage;", "getRawEmoji", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiDrawInfo {
    private final String emoji;
    private final int index;
    private final String jumboSheet;
    private final EmojiPage page;
    private final String rawEmoji;

    private final String component3() {
        return this.emoji;
    }

    public static /* synthetic */ EmojiDrawInfo copy$default(EmojiDrawInfo emojiDrawInfo, EmojiPage emojiPage, int i, String str, String str2, String str3, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            emojiPage = emojiDrawInfo.page;
        }
        if ((i2 & 2) != 0) {
            i = emojiDrawInfo.index;
        }
        if ((i2 & 4) != 0) {
            str = emojiDrawInfo.emoji;
        }
        if ((i2 & 8) != 0) {
            str2 = emojiDrawInfo.rawEmoji;
        }
        if ((i2 & 16) != 0) {
            str3 = emojiDrawInfo.jumboSheet;
        }
        return emojiDrawInfo.copy(emojiPage, i, str, str2, str3);
    }

    public final EmojiPage component1() {
        return this.page;
    }

    public final int component2() {
        return this.index;
    }

    public final String component4() {
        return this.rawEmoji;
    }

    public final String component5() {
        return this.jumboSheet;
    }

    public final EmojiDrawInfo copy(EmojiPage emojiPage, int i, String str, String str2, String str3) {
        Intrinsics.checkNotNullParameter(emojiPage, "page");
        Intrinsics.checkNotNullParameter(str, "emoji");
        return new EmojiDrawInfo(emojiPage, i, str, str2, str3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof EmojiDrawInfo)) {
            return false;
        }
        EmojiDrawInfo emojiDrawInfo = (EmojiDrawInfo) obj;
        return Intrinsics.areEqual(this.page, emojiDrawInfo.page) && this.index == emojiDrawInfo.index && Intrinsics.areEqual(this.emoji, emojiDrawInfo.emoji) && Intrinsics.areEqual(this.rawEmoji, emojiDrawInfo.rawEmoji) && Intrinsics.areEqual(this.jumboSheet, emojiDrawInfo.jumboSheet);
    }

    public int hashCode() {
        int hashCode = ((((this.page.hashCode() * 31) + this.index) * 31) + this.emoji.hashCode()) * 31;
        String str = this.rawEmoji;
        int i = 0;
        int hashCode2 = (hashCode + (str == null ? 0 : str.hashCode())) * 31;
        String str2 = this.jumboSheet;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "EmojiDrawInfo(page=" + this.page + ", index=" + this.index + ", emoji=" + this.emoji + ", rawEmoji=" + this.rawEmoji + ", jumboSheet=" + this.jumboSheet + ')';
    }

    public EmojiDrawInfo(EmojiPage emojiPage, int i, String str, String str2, String str3) {
        Intrinsics.checkNotNullParameter(emojiPage, "page");
        Intrinsics.checkNotNullParameter(str, "emoji");
        this.page = emojiPage;
        this.index = i;
        this.emoji = str;
        this.rawEmoji = str2;
        this.jumboSheet = str3;
    }

    public final int getIndex() {
        return this.index;
    }

    public final String getJumboSheet() {
        return this.jumboSheet;
    }

    public final EmojiPage getPage() {
        return this.page;
    }

    public final String getRawEmoji() {
        return this.rawEmoji;
    }
}
