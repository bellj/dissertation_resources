package org.thoughtcrime.securesms.components.settings.conversation;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class ConversationSettingsFragmentDirections {
    private ConversationSettingsFragmentDirections() {
    }

    public static ActionConversationSettingsFragmentToAppSettingsExpireTimer actionConversationSettingsFragmentToAppSettingsExpireTimer() {
        return new ActionConversationSettingsFragmentToAppSettingsExpireTimer();
    }

    public static ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment(RecipientId recipientId) {
        return new ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment(recipientId);
    }

    public static ActionConversationSettingsFragmentToPermissionsSettingsFragment actionConversationSettingsFragmentToPermissionsSettingsFragment(Parcelable parcelable) {
        return new ActionConversationSettingsFragmentToPermissionsSettingsFragment(parcelable);
    }

    public static ActionConversationSettingsFragmentToInternalDetailsSettingsFragment actionConversationSettingsFragmentToInternalDetailsSettingsFragment(RecipientId recipientId) {
        return new ActionConversationSettingsFragmentToInternalDetailsSettingsFragment(recipientId);
    }

    public static ActionConversationSettingsFragmentToShareableGroupLinkFragment actionConversationSettingsFragmentToShareableGroupLinkFragment(String str) {
        return new ActionConversationSettingsFragmentToShareableGroupLinkFragment(str);
    }

    /* loaded from: classes4.dex */
    public static class ActionConversationSettingsFragmentToAppSettingsExpireTimer implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_conversationSettingsFragment_to_app_settings_expire_timer;
        }

        private ActionConversationSettingsFragmentToAppSettingsExpireTimer() {
            this.arguments = new HashMap();
        }

        public ActionConversationSettingsFragmentToAppSettingsExpireTimer setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        public ActionConversationSettingsFragmentToAppSettingsExpireTimer setForResultMode(boolean z) {
            this.arguments.put("for_result_mode", Boolean.valueOf(z));
            return this;
        }

        public ActionConversationSettingsFragmentToAppSettingsExpireTimer setInitialValue(Integer num) {
            this.arguments.put("initial_value", num);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("recipient_id")) {
                RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
                if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                    bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
                } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                    bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
                } else {
                    throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            } else {
                bundle.putSerializable("recipient_id", null);
            }
            if (this.arguments.containsKey("for_result_mode")) {
                bundle.putBoolean("for_result_mode", ((Boolean) this.arguments.get("for_result_mode")).booleanValue());
            } else {
                bundle.putBoolean("for_result_mode", false);
            }
            if (this.arguments.containsKey("initial_value")) {
                Integer num = (Integer) this.arguments.get("initial_value");
                if (Parcelable.class.isAssignableFrom(Integer.class) || num == null) {
                    bundle.putParcelable("initial_value", (Parcelable) Parcelable.class.cast(num));
                } else if (Serializable.class.isAssignableFrom(Integer.class)) {
                    bundle.putSerializable("initial_value", (Serializable) Serializable.class.cast(num));
                } else {
                    throw new UnsupportedOperationException(Integer.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            } else {
                bundle.putSerializable("initial_value", null);
            }
            return bundle;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public boolean getForResultMode() {
            return ((Boolean) this.arguments.get("for_result_mode")).booleanValue();
        }

        public Integer getInitialValue() {
            return (Integer) this.arguments.get("initial_value");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionConversationSettingsFragmentToAppSettingsExpireTimer actionConversationSettingsFragmentToAppSettingsExpireTimer = (ActionConversationSettingsFragmentToAppSettingsExpireTimer) obj;
            if (this.arguments.containsKey("recipient_id") != actionConversationSettingsFragmentToAppSettingsExpireTimer.arguments.containsKey("recipient_id")) {
                return false;
            }
            if (getRecipientId() == null ? actionConversationSettingsFragmentToAppSettingsExpireTimer.getRecipientId() != null : !getRecipientId().equals(actionConversationSettingsFragmentToAppSettingsExpireTimer.getRecipientId())) {
                return false;
            }
            if (this.arguments.containsKey("for_result_mode") != actionConversationSettingsFragmentToAppSettingsExpireTimer.arguments.containsKey("for_result_mode") || getForResultMode() != actionConversationSettingsFragmentToAppSettingsExpireTimer.getForResultMode() || this.arguments.containsKey("initial_value") != actionConversationSettingsFragmentToAppSettingsExpireTimer.arguments.containsKey("initial_value")) {
                return false;
            }
            if (getInitialValue() == null ? actionConversationSettingsFragmentToAppSettingsExpireTimer.getInitialValue() == null : getInitialValue().equals(actionConversationSettingsFragmentToAppSettingsExpireTimer.getInitialValue())) {
                return getActionId() == actionConversationSettingsFragmentToAppSettingsExpireTimer.getActionId();
            }
            return false;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = ((((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + (getForResultMode() ? 1 : 0)) * 31;
            if (getInitialValue() != null) {
                i = getInitialValue().hashCode();
            }
            return ((hashCode + i) * 31) + getActionId();
        }

        public String toString() {
            return "ActionConversationSettingsFragmentToAppSettingsExpireTimer(actionId=" + getActionId() + "){recipientId=" + getRecipientId() + ", forResultMode=" + getForResultMode() + ", initialValue=" + getInitialValue() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_conversationSettingsFragment_to_soundsAndNotificationsSettingsFragment;
        }

        private ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment(RecipientId recipientId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientId != null) {
                hashMap.put("recipient_id", recipientId);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        public ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment setRecipientId(RecipientId recipientId) {
            if (recipientId != null) {
                this.arguments.put("recipient_id", recipientId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("recipient_id")) {
                RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
                if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                    bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
                } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                    bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
                } else {
                    throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment = (ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment) obj;
            if (this.arguments.containsKey("recipient_id") != actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment.arguments.containsKey("recipient_id")) {
                return false;
            }
            if (getRecipientId() == null ? actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment.getRecipientId() == null : getRecipientId().equals(actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment.getRecipientId())) {
                return getActionId() == actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment(actionId=" + getActionId() + "){recipientId=" + getRecipientId() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionConversationSettingsFragmentToPermissionsSettingsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_conversationSettingsFragment_to_permissionsSettingsFragment;
        }

        private ActionConversationSettingsFragmentToPermissionsSettingsFragment(Parcelable parcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("group_id", parcelable);
        }

        public ActionConversationSettingsFragmentToPermissionsSettingsFragment setGroupId(Parcelable parcelable) {
            this.arguments.put("group_id", parcelable);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("group_id")) {
                Parcelable parcelable = (Parcelable) this.arguments.get("group_id");
                if (Parcelable.class.isAssignableFrom(Parcelable.class) || parcelable == null) {
                    bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelable));
                } else if (Serializable.class.isAssignableFrom(Parcelable.class)) {
                    bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelable));
                } else {
                    throw new UnsupportedOperationException(Parcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public Parcelable getGroupId() {
            return (Parcelable) this.arguments.get("group_id");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionConversationSettingsFragmentToPermissionsSettingsFragment actionConversationSettingsFragmentToPermissionsSettingsFragment = (ActionConversationSettingsFragmentToPermissionsSettingsFragment) obj;
            if (this.arguments.containsKey("group_id") != actionConversationSettingsFragmentToPermissionsSettingsFragment.arguments.containsKey("group_id")) {
                return false;
            }
            if (getGroupId() == null ? actionConversationSettingsFragmentToPermissionsSettingsFragment.getGroupId() == null : getGroupId().equals(actionConversationSettingsFragmentToPermissionsSettingsFragment.getGroupId())) {
                return getActionId() == actionConversationSettingsFragmentToPermissionsSettingsFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getGroupId() != null ? getGroupId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionConversationSettingsFragmentToPermissionsSettingsFragment(actionId=" + getActionId() + "){groupId=" + getGroupId() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionConversationSettingsFragmentToInternalDetailsSettingsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_conversationSettingsFragment_to_internalDetailsSettingsFragment;
        }

        private ActionConversationSettingsFragmentToInternalDetailsSettingsFragment(RecipientId recipientId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientId != null) {
                hashMap.put("recipient_id", recipientId);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        public ActionConversationSettingsFragmentToInternalDetailsSettingsFragment setRecipientId(RecipientId recipientId) {
            if (recipientId != null) {
                this.arguments.put("recipient_id", recipientId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("recipient_id")) {
                RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
                if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                    bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
                } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                    bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
                } else {
                    throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionConversationSettingsFragmentToInternalDetailsSettingsFragment actionConversationSettingsFragmentToInternalDetailsSettingsFragment = (ActionConversationSettingsFragmentToInternalDetailsSettingsFragment) obj;
            if (this.arguments.containsKey("recipient_id") != actionConversationSettingsFragmentToInternalDetailsSettingsFragment.arguments.containsKey("recipient_id")) {
                return false;
            }
            if (getRecipientId() == null ? actionConversationSettingsFragmentToInternalDetailsSettingsFragment.getRecipientId() == null : getRecipientId().equals(actionConversationSettingsFragmentToInternalDetailsSettingsFragment.getRecipientId())) {
                return getActionId() == actionConversationSettingsFragmentToInternalDetailsSettingsFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionConversationSettingsFragmentToInternalDetailsSettingsFragment(actionId=" + getActionId() + "){recipientId=" + getRecipientId() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionConversationSettingsFragmentToShareableGroupLinkFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_conversationSettingsFragment_to_shareableGroupLinkFragment;
        }

        private ActionConversationSettingsFragmentToShareableGroupLinkFragment(String str) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (str != null) {
                hashMap.put("group_id", str);
                return;
            }
            throw new IllegalArgumentException("Argument \"group_id\" is marked as non-null but was passed a null value.");
        }

        public ActionConversationSettingsFragmentToShareableGroupLinkFragment setGroupId(String str) {
            if (str != null) {
                this.arguments.put("group_id", str);
                return this;
            }
            throw new IllegalArgumentException("Argument \"group_id\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("group_id")) {
                bundle.putString("group_id", (String) this.arguments.get("group_id"));
            }
            return bundle;
        }

        public String getGroupId() {
            return (String) this.arguments.get("group_id");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionConversationSettingsFragmentToShareableGroupLinkFragment actionConversationSettingsFragmentToShareableGroupLinkFragment = (ActionConversationSettingsFragmentToShareableGroupLinkFragment) obj;
            if (this.arguments.containsKey("group_id") != actionConversationSettingsFragmentToShareableGroupLinkFragment.arguments.containsKey("group_id")) {
                return false;
            }
            if (getGroupId() == null ? actionConversationSettingsFragmentToShareableGroupLinkFragment.getGroupId() == null : getGroupId().equals(actionConversationSettingsFragmentToShareableGroupLinkFragment.getGroupId())) {
                return getActionId() == actionConversationSettingsFragmentToShareableGroupLinkFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getGroupId() != null ? getGroupId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionConversationSettingsFragmentToShareableGroupLinkFragment(actionId=" + getActionId() + "){groupId=" + getGroupId() + "}";
        }
    }
}
