package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import org.thoughtcrime.securesms.MuteDialog;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda0 implements MuteDialog.MuteSelectionListener {
    public final /* synthetic */ SoundsAndNotificationsSettingsViewModel f$0;

    public /* synthetic */ SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda0(SoundsAndNotificationsSettingsViewModel soundsAndNotificationsSettingsViewModel) {
        this.f$0 = soundsAndNotificationsSettingsViewModel;
    }

    @Override // org.thoughtcrime.securesms.MuteDialog.MuteSelectionListener
    public final void onMuted(long j) {
        this.f$0.setMuteUntil(j);
    }
}
