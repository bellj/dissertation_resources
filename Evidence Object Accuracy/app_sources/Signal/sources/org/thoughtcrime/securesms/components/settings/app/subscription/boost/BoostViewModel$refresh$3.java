package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import com.annimon.stream.function.Function;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel;

/* compiled from: BoostViewModel.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u000e\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "info", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel$BoostInfo;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostViewModel$refresh$3 extends Lambda implements Function1<BoostViewModel.BoostInfo, Unit> {
    final /* synthetic */ BoostViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BoostViewModel$refresh$3(BoostViewModel boostViewModel) {
        super(1);
        this.this$0 = boostViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(BoostViewModel.BoostInfo boostInfo) {
        invoke(boostInfo);
        return Unit.INSTANCE;
    }

    public final void invoke(BoostViewModel.BoostInfo boostInfo) {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$refresh$3$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$refresh$3.m955invoke$lambda0(BoostViewModel.BoostInfo.this, (BoostState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final BoostState m955invoke$lambda0(BoostViewModel.BoostInfo boostInfo, BoostState boostState) {
        List<Boost> boosts = boostInfo.getBoosts();
        Boost selectedBoost = CollectionsKt___CollectionsKt.contains(boostInfo.getBoosts(), boostState.getSelectedBoost()) ? boostState.getSelectedBoost() : boostInfo.getDefaultBoost();
        Badge boostBadge = boostState.getBoostBadge();
        if (boostBadge == null) {
            boostBadge = boostInfo.getBoostBadge();
        }
        BoostState.Stage stage = (boostState.getStage() == BoostState.Stage.INIT || boostState.getStage() == BoostState.Stage.FAILURE) ? BoostState.Stage.READY : boostState.getStage();
        Set<Currency> supportedCurrencies = boostInfo.getSupportedCurrencies();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(supportedCurrencies, 10));
        for (Currency currency : supportedCurrencies) {
            arrayList.add(currency.getCurrencyCode());
        }
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : boostBadge, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : boosts, (r20 & 16) != 0 ? boostState.selectedBoost : selectedBoost, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : stage, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : arrayList);
    }
}
