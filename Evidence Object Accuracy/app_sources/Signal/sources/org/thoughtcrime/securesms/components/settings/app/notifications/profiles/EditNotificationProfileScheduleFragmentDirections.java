package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class EditNotificationProfileScheduleFragmentDirections {
    private EditNotificationProfileScheduleFragmentDirections() {
    }

    public static ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment(long j) {
        return new ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment(j);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_editNotificationProfileScheduleFragment_to_notificationProfileCreatedFragment;
        }

        private ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment = (ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment) obj;
            return this.arguments.containsKey("profileId") == actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment.arguments.containsKey("profileId") && getProfileId() == actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment.getProfileId() && getActionId() == actionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment.getActionId();
        }

        public int hashCode() {
            return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionEditNotificationProfileScheduleFragmentToNotificationProfileCreatedFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + "}";
        }
    }
}
