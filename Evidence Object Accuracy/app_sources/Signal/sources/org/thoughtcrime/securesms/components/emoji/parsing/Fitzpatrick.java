package org.thoughtcrime.securesms.components.emoji.parsing;

/* loaded from: classes4.dex */
public enum Fitzpatrick {
    TYPE_1_2("🏻"),
    TYPE_3("🏼"),
    TYPE_4("🏽"),
    TYPE_5("🏾"),
    TYPE_6("🏿");
    
    public final String unicode;

    Fitzpatrick(String str) {
        this.unicode = str;
    }

    public static Fitzpatrick fitzpatrickFromUnicode(CharSequence charSequence, int i) {
        Fitzpatrick[] values = values();
        for (Fitzpatrick fitzpatrick : values) {
            boolean z = true;
            for (int i2 = 0; i2 < fitzpatrick.unicode.toCharArray().length; i2++) {
                if (fitzpatrick.unicode.toCharArray()[i2] != charSequence.charAt(i + i2)) {
                    z = false;
                }
            }
            if (z) {
                return fitzpatrick;
            }
        }
        return null;
    }

    public static Fitzpatrick fitzpatrickFromType(String str) {
        try {
            return valueOf(str.toUpperCase());
        } catch (IllegalArgumentException unused) {
            return null;
        }
    }
}
