package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.text.SpannableStringBuilder;
import android.view.View;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: BoostFragment.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\n \u0002*\u0004\u0018\u00010\u00010\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Landroid/text/SpannableStringBuilder;", "kotlin.jvm.PlatformType", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class BoostFragment$sayThanks$2 extends Lambda implements Function0<SpannableStringBuilder> {
    final /* synthetic */ BoostFragment this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BoostFragment$sayThanks$2(BoostFragment boostFragment) {
        super(0);
        this.this$0 = boostFragment;
    }

    @Override // kotlin.jvm.functions.Function0
    public final SpannableStringBuilder invoke() {
        return new SpannableStringBuilder(this.this$0.requireContext().getString(R.string.BoostFragment__make_a_one_time, 30)).append((CharSequence) " ").append(SpanUtil.learnMore(this.this$0.requireContext(), ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_accent_primary), new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostFragment$sayThanks$2$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                BoostFragment$sayThanks$2.m935invoke$lambda0(BoostFragment.this, view);
            }
        }));
    }

    /* renamed from: invoke$lambda-0 */
    public static final void m935invoke$lambda0(BoostFragment boostFragment, View view) {
        Intrinsics.checkNotNullParameter(boostFragment, "this$0");
        CommunicationActions.openBrowserLink(boostFragment.requireContext(), boostFragment.getString(R.string.sustainer_boost_and_badges));
    }
}
