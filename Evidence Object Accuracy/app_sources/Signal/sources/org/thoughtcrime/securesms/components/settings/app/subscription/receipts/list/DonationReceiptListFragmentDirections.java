package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contacts.ContactRepository;

/* loaded from: classes4.dex */
public class DonationReceiptListFragmentDirections {
    private DonationReceiptListFragmentDirections() {
    }

    public static ActionDonationReceiptListFragmentToDonationReceiptDetailFragment actionDonationReceiptListFragmentToDonationReceiptDetailFragment(long j) {
        return new ActionDonationReceiptListFragmentToDonationReceiptDetailFragment(j);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionDonationReceiptListFragmentToDonationReceiptDetailFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_donationReceiptListFragment_to_donationReceiptDetailFragment;
        }

        private ActionDonationReceiptListFragmentToDonationReceiptDetailFragment(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put(ContactRepository.ID_COLUMN, Long.valueOf(j));
        }

        public ActionDonationReceiptListFragmentToDonationReceiptDetailFragment setId(long j) {
            this.arguments.put(ContactRepository.ID_COLUMN, Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey(ContactRepository.ID_COLUMN)) {
                bundle.putLong(ContactRepository.ID_COLUMN, ((Long) this.arguments.get(ContactRepository.ID_COLUMN)).longValue());
            }
            return bundle;
        }

        public long getId() {
            return ((Long) this.arguments.get(ContactRepository.ID_COLUMN)).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionDonationReceiptListFragmentToDonationReceiptDetailFragment actionDonationReceiptListFragmentToDonationReceiptDetailFragment = (ActionDonationReceiptListFragmentToDonationReceiptDetailFragment) obj;
            return this.arguments.containsKey(ContactRepository.ID_COLUMN) == actionDonationReceiptListFragmentToDonationReceiptDetailFragment.arguments.containsKey(ContactRepository.ID_COLUMN) && getId() == actionDonationReceiptListFragmentToDonationReceiptDetailFragment.getId() && getActionId() == actionDonationReceiptListFragmentToDonationReceiptDetailFragment.getActionId();
        }

        public int hashCode() {
            return ((((int) (getId() ^ (getId() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionDonationReceiptListFragmentToDonationReceiptDetailFragment(actionId=" + getActionId() + "){id=" + getId() + "}";
        }
    }
}
