package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NoNotificationProfiles;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LargeIconClickPreference;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: NotificationProfilesFragment.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0016\u0010\u0011\u001a\u00020\u00122\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014H\u0002J\b\u0010\u0016\u001a\u00020\u000eH\u0016J\u001a\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u000e¢\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0002¢\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\n¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "profiles", "", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "onDestroyView", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfilesFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private Toolbar toolbar;
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(NotificationProfilesViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, NotificationProfilesFragment$viewModel$2.INSTANCE);

    public NotificationProfilesFragment() {
        super(0, 0, 0, null, 15, null);
    }

    private final NotificationProfilesViewModel getViewModel() {
        return (NotificationProfilesViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        this.toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.toolbar = null;
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        NoNotificationProfiles.INSTANCE.register(dSLSettingsAdapter);
        LargeIconClickPreference.INSTANCE.register(dSLSettingsAdapter);
        NotificationProfilePreference.INSTANCE.register(dSLSettingsAdapter);
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getProfiles().subscribe(new Consumer(dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DSLSettingsAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                NotificationProfilesFragment.$r8$lambda$1qX6BoXzQRq5AYn3jRLcJO2KjSg(NotificationProfilesFragment.this, this.f$1, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.getProfiles()\n…ppingModelList())\n      }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m774bindAdapter$lambda0(NotificationProfilesFragment notificationProfilesFragment, DSLSettingsAdapter dSLSettingsAdapter, List list) {
        Intrinsics.checkNotNullParameter(notificationProfilesFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        if (list.isEmpty()) {
            Toolbar toolbar = notificationProfilesFragment.toolbar;
            if (toolbar != null) {
                toolbar.setTitle("");
            }
        } else {
            Toolbar toolbar2 = notificationProfilesFragment.toolbar;
            if (toolbar2 != null) {
                toolbar2.setTitle(R.string.NotificationsSettingsFragment__notification_profiles);
            }
        }
        Intrinsics.checkNotNullExpressionValue(list, "profiles");
        dSLSettingsAdapter.submitList(notificationProfilesFragment.getConfiguration(list).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(List<NotificationProfile> list) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(list, this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1
            final /* synthetic */ List<NotificationProfile> $profiles;
            final /* synthetic */ NotificationProfilesFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$profiles = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00f7: INVOKE  
                  (r23v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference$Model : 0x00f4: CONSTRUCTOR  (r5v6 org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference$Model A[REMOVE]) = 
                  (r7v2 'from3' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (r14v0 'dSLSettingsText' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (r15v1 'dSLSettingsIcon' org.thoughtcrime.securesms.components.settings.DSLSettingsIcon)
                  (wrap: org.thoughtcrime.securesms.conversation.colors.AvatarColor : 0x00dd: INVOKE  (r16v1 org.thoughtcrime.securesms.conversation.colors.AvatarColor A[REMOVE]) = (r5v5 'notificationProfile' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile) type: VIRTUAL call: org.thoughtcrime.securesms.notifications.profiles.NotificationProfile.getColor():org.thoughtcrime.securesms.conversation.colors.AvatarColor)
                  false
                  false
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2 : 0x00e7: CONSTRUCTOR  (r6v5 org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2 A[REMOVE]) = 
                  (r4v2 'notificationProfilesFragment3' org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment)
                  (r5v5 'notificationProfile' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2.<init>(org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment, org.thoughtcrime.securesms.notifications.profiles.NotificationProfile):void type: CONSTRUCTOR)
                  (48 int)
                  (null kotlin.jvm.internal.DefaultConstructorMarker)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference.Model.<init>(org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsIcon, org.thoughtcrime.securesms.conversation.colors.AvatarColor, boolean, boolean, kotlin.jvm.functions.Function0, int, kotlin.jvm.internal.DefaultConstructorMarker):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:151)
                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:137)
                	at jadx.core.dex.regions.conditions.IfRegion.generate(IfRegion.java:137)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00f4: CONSTRUCTOR  (r5v6 org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference$Model A[REMOVE]) = 
                  (r7v2 'from3' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (r14v0 'dSLSettingsText' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (r15v1 'dSLSettingsIcon' org.thoughtcrime.securesms.components.settings.DSLSettingsIcon)
                  (wrap: org.thoughtcrime.securesms.conversation.colors.AvatarColor : 0x00dd: INVOKE  (r16v1 org.thoughtcrime.securesms.conversation.colors.AvatarColor A[REMOVE]) = (r5v5 'notificationProfile' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile) type: VIRTUAL call: org.thoughtcrime.securesms.notifications.profiles.NotificationProfile.getColor():org.thoughtcrime.securesms.conversation.colors.AvatarColor)
                  false
                  false
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2 : 0x00e7: CONSTRUCTOR  (r6v5 org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2 A[REMOVE]) = 
                  (r4v2 'notificationProfilesFragment3' org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment)
                  (r5v5 'notificationProfile' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2.<init>(org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment, org.thoughtcrime.securesms.notifications.profiles.NotificationProfile):void type: CONSTRUCTOR)
                  (48 int)
                  (null kotlin.jvm.internal.DefaultConstructorMarker)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfilePreference.Model.<init>(org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsIcon, org.thoughtcrime.securesms.conversation.colors.AvatarColor, boolean, boolean, kotlin.jvm.functions.Function0, int, kotlin.jvm.internal.DefaultConstructorMarker):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 32 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00e7: CONSTRUCTOR  (r6v5 org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2 A[REMOVE]) = 
                  (r4v2 'notificationProfilesFragment3' org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment)
                  (r5v5 'notificationProfile' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2.<init>(org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment, org.thoughtcrime.securesms.notifications.profiles.NotificationProfile):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 38 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1$3$2, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 44 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r23) {
                /*
                // Method dump skipped, instructions count: 253
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }
}
