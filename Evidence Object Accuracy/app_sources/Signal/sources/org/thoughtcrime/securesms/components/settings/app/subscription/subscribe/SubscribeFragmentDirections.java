package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;

/* loaded from: classes4.dex */
public class SubscribeFragmentDirections {
    private SubscribeFragmentDirections() {
    }

    public static ActionSubscribeFragmentToSetDonationCurrencyFragment actionSubscribeFragmentToSetDonationCurrencyFragment(boolean z, String[] strArr) {
        return new ActionSubscribeFragmentToSetDonationCurrencyFragment(z, strArr);
    }

    public static NavDirections actionSubscribeFragmentToSubscribeLearnMoreBottomSheetDialog() {
        return new ActionOnlyNavDirections(R.id.action_subscribeFragment_to_subscribeLearnMoreBottomSheetDialog);
    }

    public static ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog(Badge badge) {
        return new ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog(badge);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionSubscribeFragmentToSetDonationCurrencyFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_subscribeFragment_to_setDonationCurrencyFragment;
        }

        private ActionSubscribeFragmentToSetDonationCurrencyFragment(boolean z, String[] strArr) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("isBoost", Boolean.valueOf(z));
            if (strArr != null) {
                hashMap.put("supportedCurrencyCodes", strArr);
                return;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        public ActionSubscribeFragmentToSetDonationCurrencyFragment setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        public ActionSubscribeFragmentToSetDonationCurrencyFragment setSupportedCurrencyCodes(String[] strArr) {
            if (strArr != null) {
                this.arguments.put("supportedCurrencyCodes", strArr);
                return this;
            }
            throw new IllegalArgumentException("Argument \"supportedCurrencyCodes\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("isBoost")) {
                bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
            }
            if (this.arguments.containsKey("supportedCurrencyCodes")) {
                bundle.putStringArray("supportedCurrencyCodes", (String[]) this.arguments.get("supportedCurrencyCodes"));
            }
            return bundle;
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }

        public String[] getSupportedCurrencyCodes() {
            return (String[]) this.arguments.get("supportedCurrencyCodes");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionSubscribeFragmentToSetDonationCurrencyFragment actionSubscribeFragmentToSetDonationCurrencyFragment = (ActionSubscribeFragmentToSetDonationCurrencyFragment) obj;
            if (this.arguments.containsKey("isBoost") != actionSubscribeFragmentToSetDonationCurrencyFragment.arguments.containsKey("isBoost") || getIsBoost() != actionSubscribeFragmentToSetDonationCurrencyFragment.getIsBoost() || this.arguments.containsKey("supportedCurrencyCodes") != actionSubscribeFragmentToSetDonationCurrencyFragment.arguments.containsKey("supportedCurrencyCodes")) {
                return false;
            }
            if (getSupportedCurrencyCodes() == null ? actionSubscribeFragmentToSetDonationCurrencyFragment.getSupportedCurrencyCodes() == null : getSupportedCurrencyCodes().equals(actionSubscribeFragmentToSetDonationCurrencyFragment.getSupportedCurrencyCodes())) {
                return getActionId() == actionSubscribeFragmentToSetDonationCurrencyFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getIsBoost() ? 1 : 0) + 31) * 31) + Arrays.hashCode(getSupportedCurrencyCodes())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionSubscribeFragmentToSetDonationCurrencyFragment(actionId=" + getActionId() + "){isBoost=" + getIsBoost() + ", supportedCurrencyCodes=" + getSupportedCurrencyCodes() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_subscribeFragment_to_subscribeThanksForYourSupportBottomSheetDialog;
        }

        private ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog(Badge badge) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (badge != null) {
                hashMap.put("badge", badge);
                return;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog setBadge(Badge badge) {
            if (badge != null) {
                this.arguments.put("badge", badge);
                return this;
            }
            throw new IllegalArgumentException("Argument \"badge\" is marked as non-null but was passed a null value.");
        }

        public ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog setIsBoost(boolean z) {
            this.arguments.put("isBoost", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("badge")) {
                Badge badge = (Badge) this.arguments.get("badge");
                if (Parcelable.class.isAssignableFrom(Badge.class) || badge == null) {
                    bundle.putParcelable("badge", (Parcelable) Parcelable.class.cast(badge));
                } else if (Serializable.class.isAssignableFrom(Badge.class)) {
                    bundle.putSerializable("badge", (Serializable) Serializable.class.cast(badge));
                } else {
                    throw new UnsupportedOperationException(Badge.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            if (this.arguments.containsKey("isBoost")) {
                bundle.putBoolean("isBoost", ((Boolean) this.arguments.get("isBoost")).booleanValue());
            } else {
                bundle.putBoolean("isBoost", false);
            }
            return bundle;
        }

        public Badge getBadge() {
            return (Badge) this.arguments.get("badge");
        }

        public boolean getIsBoost() {
            return ((Boolean) this.arguments.get("isBoost")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog = (ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog) obj;
            if (this.arguments.containsKey("badge") != actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog.arguments.containsKey("badge")) {
                return false;
            }
            if (getBadge() == null ? actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog.getBadge() == null : getBadge().equals(actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog.getBadge())) {
                return this.arguments.containsKey("isBoost") == actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog.arguments.containsKey("isBoost") && getIsBoost() == actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog.getIsBoost() && getActionId() == actionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((((getBadge() != null ? getBadge().hashCode() : 0) + 31) * 31) + (getIsBoost() ? 1 : 0)) * 31) + getActionId();
        }

        public String toString() {
            return "ActionSubscribeFragmentToSubscribeThanksForYourSupportBottomSheetDialog(actionId=" + getActionId() + "){badge=" + getBadge() + ", isBoost=" + getIsBoost() + "}";
        }
    }
}
