package org.thoughtcrime.securesms.components.webrtc;

import android.view.View;
import android.view.ViewGroup;
import com.google.android.flexbox.FlexboxLayout;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.webrtc.RendererCommon;

/* compiled from: CallParticipantsLayoutStrategies.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\b\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0006H\u0007¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsLayoutStrategies;", "", "()V", "getStrategy", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsLayout$LayoutStrategy;", "isPortrait", "", "isLandscapeEnabled", "Landscape", "Portrait", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CallParticipantsLayoutStrategies {
    public static final CallParticipantsLayoutStrategies INSTANCE = new CallParticipantsLayoutStrategies();

    private CallParticipantsLayoutStrategies() {
    }

    /* compiled from: CallParticipantsLayoutStrategies.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004H\u0016J(\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u0004H\u0016¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsLayoutStrategies$Portrait;", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsLayout$LayoutStrategy;", "()V", "getFlexDirection", "", "setChildLayoutParams", "", "child", "Landroid/view/View;", "childPosition", "childCount", "setChildScaling", "callParticipant", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "callParticipantView", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantView;", "isPortrait", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Portrait implements CallParticipantsLayout.LayoutStrategy {
        public static final Portrait INSTANCE = new Portrait();

        @Override // org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout.LayoutStrategy
        public int getFlexDirection() {
            return 0;
        }

        private Portrait() {
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout.LayoutStrategy
        public void setChildScaling(CallParticipant callParticipant, CallParticipantView callParticipantView, boolean z, int i) {
            Intrinsics.checkNotNullParameter(callParticipant, "callParticipant");
            Intrinsics.checkNotNullParameter(callParticipantView, "callParticipantView");
            if (callParticipant.isScreenSharing()) {
                callParticipantView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
            } else {
                callParticipantView.setScalingType((z || i < 3) ? RendererCommon.ScalingType.SCALE_ASPECT_FILL : RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
            }
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout.LayoutStrategy
        public void setChildLayoutParams(View view, int i, int i2) {
            Intrinsics.checkNotNullParameter(view, "child");
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams != null) {
                FlexboxLayout.LayoutParams layoutParams2 = (FlexboxLayout.LayoutParams) layoutParams;
                if (i2 < 3) {
                    layoutParams2.setFlexBasisPercent(1.0f);
                } else if (i2 % 2 == 0 || i != i2 - 1) {
                    layoutParams2.setFlexBasisPercent(0.5f);
                } else {
                    layoutParams2.setFlexBasisPercent(1.0f);
                }
                view.setLayoutParams(layoutParams2);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type com.google.android.flexbox.FlexboxLayout.LayoutParams");
        }
    }

    /* compiled from: CallParticipantsLayoutStrategies.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bÂ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016J \u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004H\u0016J(\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\n\u001a\u00020\u0004H\u0016¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsLayoutStrategies$Landscape;", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantsLayout$LayoutStrategy;", "()V", "getFlexDirection", "", "setChildLayoutParams", "", "child", "Landroid/view/View;", "childPosition", "childCount", "setChildScaling", "callParticipant", "Lorg/thoughtcrime/securesms/events/CallParticipant;", "callParticipantView", "Lorg/thoughtcrime/securesms/components/webrtc/CallParticipantView;", "isPortrait", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Landscape implements CallParticipantsLayout.LayoutStrategy {
        public static final Landscape INSTANCE = new Landscape();

        @Override // org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout.LayoutStrategy
        public int getFlexDirection() {
            return 2;
        }

        private Landscape() {
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout.LayoutStrategy
        public void setChildScaling(CallParticipant callParticipant, CallParticipantView callParticipantView, boolean z, int i) {
            Intrinsics.checkNotNullParameter(callParticipant, "callParticipant");
            Intrinsics.checkNotNullParameter(callParticipantView, "callParticipantView");
            if (callParticipant.isScreenSharing()) {
                callParticipantView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FIT);
            } else {
                callParticipantView.setScalingType(RendererCommon.ScalingType.SCALE_ASPECT_FILL, RendererCommon.ScalingType.SCALE_ASPECT_BALANCED);
            }
        }

        @Override // org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout.LayoutStrategy
        public void setChildLayoutParams(View view, int i, int i2) {
            Intrinsics.checkNotNullParameter(view, "child");
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams != null) {
                FlexboxLayout.LayoutParams layoutParams2 = (FlexboxLayout.LayoutParams) layoutParams;
                if (i2 < 4) {
                    layoutParams2.setFlexBasisPercent(1.0f);
                } else if (i2 % 2 == 0 || i != i2 - 1) {
                    layoutParams2.setFlexBasisPercent(0.5f);
                } else {
                    layoutParams2.setFlexBasisPercent(1.0f);
                }
                view.setLayoutParams(layoutParams2);
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type com.google.android.flexbox.FlexboxLayout.LayoutParams");
        }
    }

    @JvmStatic
    public static final CallParticipantsLayout.LayoutStrategy getStrategy(boolean z, boolean z2) {
        if (z || !z2) {
            return Portrait.INSTANCE;
        }
        return Landscape.INSTANCE;
    }
}
