package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;

/* loaded from: classes4.dex */
public final class PendingGroupJoinRequestsReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return true;
    }

    private PendingGroupJoinRequestsReminder(CharSequence charSequence, CharSequence charSequence2) {
        super(charSequence, charSequence2);
    }

    public static Reminder create(Context context, int i) {
        PendingGroupJoinRequestsReminder pendingGroupJoinRequestsReminder = new PendingGroupJoinRequestsReminder(null, context.getResources().getQuantityString(R.plurals.PendingGroupJoinRequestsReminder_d_pending_member_requests, i, Integer.valueOf(i)));
        pendingGroupJoinRequestsReminder.addAction(new Reminder.Action(context.getString(R.string.PendingGroupJoinRequestsReminder_view), R.id.reminder_action_review_join_requests));
        return pendingGroupJoinRequestsReminder;
    }

    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public Reminder.Importance getImportance() {
        return Reminder.Importance.NORMAL;
    }
}
