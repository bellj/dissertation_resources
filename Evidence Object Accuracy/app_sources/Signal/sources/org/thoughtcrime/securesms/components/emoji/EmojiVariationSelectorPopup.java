package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import androidx.core.content.ContextCompat;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class EmojiVariationSelectorPopup extends PopupWindow {
    private final Context context;
    private final ViewGroup list = ((ViewGroup) getContentView().findViewById(R.id.emoji_variation_container));
    private final EmojiEventListener listener;

    public EmojiVariationSelectorPopup(Context context, EmojiEventListener emojiEventListener) {
        super(LayoutInflater.from(context).inflate(R.layout.emoji_variation_selector, (ViewGroup) null), -2, -2);
        this.context = context;
        this.listener = emojiEventListener;
        setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.emoji_variation_selector_background));
        setOutsideTouchable(true);
        if (Build.VERSION.SDK_INT >= 21) {
            setElevation(20.0f);
        }
    }

    public void setVariations(List<String> list) {
        this.list.removeAllViews();
        for (String str : list) {
            ImageView imageView = (ImageView) LayoutInflater.from(this.context).inflate(R.layout.emoji_variation_selector_item, this.list, false);
            imageView.setImageDrawable(EmojiProvider.getEmojiDrawable(this.context, str));
            imageView.setOnClickListener(new View.OnClickListener(str) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiVariationSelectorPopup$$ExternalSyntheticLambda0
                public final /* synthetic */ String f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    EmojiVariationSelectorPopup.this.lambda$setVariations$0(this.f$1, view);
                }
            });
            this.list.addView(imageView);
        }
    }

    public /* synthetic */ void lambda$setVariations$0(String str, View view) {
        this.listener.onEmojiSelected(str);
        dismiss();
    }
}
