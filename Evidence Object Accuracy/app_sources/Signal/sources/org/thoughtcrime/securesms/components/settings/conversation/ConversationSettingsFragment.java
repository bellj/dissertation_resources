package org.thoughtcrime.securesms.components.settings.conversation;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.view.OneShotPreDrawListener;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.functions.Function3;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Ref$BooleanRef;
import kotlin.jvm.internal.Reflection;
import kotlin.jvm.internal.StringCompanionObject;
import org.signal.core.util.DimensionUnit;
import org.thoughtcrime.securesms.AvatarPreviewActivity;
import org.thoughtcrime.securesms.BlockUnblockDialog;
import org.thoughtcrime.securesms.InviteActivity;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.MuteDialog;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.view.AvatarView;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.view.ViewBadgeBottomSheetDialogFragment;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsEvent;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragmentDirections;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.AvatarPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.BioTextPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.GroupDescriptionPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.InternalPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LargeIconClickPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.SharedMediaPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.Utils;
import org.thoughtcrime.securesms.conversation.ConversationIntents;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.groups.ui.GroupErrors;
import org.thoughtcrime.securesms.groups.ui.GroupLimitDialog;
import org.thoughtcrime.securesms.groups.ui.GroupMemberEntry;
import org.thoughtcrime.securesms.groups.ui.LeaveGroupDialog;
import org.thoughtcrime.securesms.groups.ui.addmembers.AddMembersActivity;
import org.thoughtcrime.securesms.groups.ui.addtogroup.AddToGroupsActivity;
import org.thoughtcrime.securesms.groups.ui.invitesandrequests.ManagePendingAndRequestingMembersActivity;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupDescriptionDialog;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupInviteSentDialog;
import org.thoughtcrime.securesms.groups.ui.managegroup.dialogs.GroupsLearnMoreBottomSheetDialogFragment;
import org.thoughtcrime.securesms.groups.ui.migration.GroupsV1MigrationInitiationBottomSheetDialogFragment;
import org.thoughtcrime.securesms.mediaoverview.MediaOverviewActivity;
import org.thoughtcrime.securesms.profiles.edit.EditProfileActivity;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientExporter;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.ui.bottomsheet.RecipientBottomSheetDialogFragment;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.stories.StoryViewerArgs;
import org.thoughtcrime.securesms.stories.dialogs.StoryDialogs;
import org.thoughtcrime.securesms.stories.viewer.StoryViewerActivity;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.ExpirationUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.thoughtcrime.securesms.util.views.SimpleProgressDialog;
import org.thoughtcrime.securesms.verify.VerifyIdentityActivity;
import org.thoughtcrime.securesms.wallpaper.ChatWallpaperActivity;

/* compiled from: ConversationSettingsFragment.kt */
@Metadata(d1 = {"\u0000Ê\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002UVB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.H\u0016J\u0010\u0010/\u001a\u0002002\u0006\u00101\u001a\u00020\u0004H\u0002J\u0010\u00102\u001a\u0002032\u0006\u00104\u001a\u000205H\u0002J\u0012\u00106\u001a\u0002072\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0016J\u0010\u00108\u001a\u00020,2\u0006\u00109\u001a\u00020:H\u0002J\u0010\u0010;\u001a\u00020,2\u0006\u0010<\u001a\u00020=H\u0002J\"\u0010>\u001a\u00020,2\u0006\u0010?\u001a\u00020\u00042\u0006\u0010@\u001a\u00020\u00042\b\u0010A\u001a\u0004\u0018\u00010BH\u0016J\u0010\u0010C\u001a\u00020,2\u0006\u0010D\u001a\u00020EH\u0016J\u0010\u0010F\u001a\u00020G2\u0006\u0010H\u001a\u00020IH\u0016J\u001a\u0010J\u001a\u00020,2\u0006\u0010K\u001a\u00020\u001e2\b\u0010L\u001a\u0004\u0018\u00010MH\u0016J\u0010\u0010N\u001a\u00020,2\u0006\u0010N\u001a\u00020OH\u0002J\b\u0010P\u001a\u00020,H\u0002J\u0010\u0010Q\u001a\u00020,2\u0006\u0010Q\u001a\u00020RH\u0002J\u0010\u0010S\u001a\u00020,2\u0006\u0010S\u001a\u00020TH\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006R\u001b\u0010\t\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\r\u0010\b\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u000e\u001a\u00020\u000fX.¢\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b\u0012\u0010\b\u001a\u0004\b\u0011\u0010\fR\u0014\u0010\u0013\u001a\u00020\u00148BX\u0004¢\u0006\u0006\u001a\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0018X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u001aX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u001cX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u001eX.¢\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020 X.¢\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\"X.¢\u0006\u0002\n\u0000R\u001b\u0010#\u001a\u00020\n8BX\u0002¢\u0006\f\n\u0004\b%\u0010\b\u001a\u0004\b$\u0010\fR\u001b\u0010&\u001a\u00020'8BX\u0002¢\u0006\f\n\u0004\b*\u0010\b\u001a\u0004\b(\u0010)¨\u0006W"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "alertTint", "", "getAlertTint", "()I", "alertTint$delegate", "Lkotlin/Lazy;", "blockIcon", "Landroid/graphics/drawable/Drawable;", "getBlockIcon", "()Landroid/graphics/drawable/Drawable;", "blockIcon$delegate", "callback", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsFragment$Callback;", "leaveIcon", "getLeaveIcon", "leaveIcon$delegate", "navController", "Landroidx/navigation/NavController;", "getNavController", "()Landroidx/navigation/NavController;", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "toolbarAvatar", "Lorg/thoughtcrime/securesms/components/AvatarImageView;", "toolbarAvatarContainer", "Landroid/widget/FrameLayout;", "toolbarBackground", "Landroid/view/View;", "toolbarBadge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "toolbarTitle", "Landroid/widget/TextView;", "unblockIcon", "getUnblockIcon", "unblockIcon$delegate", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "formatDisappearingMessagesLifespan", "", "disappearingMessagesLifespan", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsState;", "getMaterial3OnScrollHelper", "Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper;", "handleAddMembersToGroup", "addMembersToGroup", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$AddMembersToGroup;", "handleAddToAGroup", "addToAGroup", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$AddToAGroup;", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onAttach", "context", "Landroid/content/Context;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onViewCreated", "view", "savedInstanceState", "Landroid/os/Bundle;", "showAddMembersToGroupError", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowAddMembersToGroupError;", "showGroupHardLimitDialog", "showGroupInvitesSentDialog", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowGroupInvitesSentDialog;", "showMembersAdded", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent$ShowMembersAdded;", "Callback", "ConversationSettingsOnUserScrolledAnimationHelper", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationSettingsFragment extends DSLSettingsFragment {
    private final Lazy alertTint$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Integer>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$alertTint$2
        final /* synthetic */ ConversationSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Integer invoke() {
            return Integer.valueOf(ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_alert_primary));
        }
    });
    private final Lazy blockIcon$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Drawable>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$blockIcon$2
        final /* synthetic */ ConversationSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Drawable invoke() {
            Drawable requireDrawable = ContextUtil.requireDrawable(this.this$0.requireContext(), R.drawable.ic_block_tinted_24);
            requireDrawable.setColorFilter(new PorterDuffColorFilter(this.this$0.getAlertTint(), PorterDuff.Mode.SRC_IN));
            return requireDrawable;
        }
    });
    private Callback callback;
    private final Lazy leaveIcon$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Drawable>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$leaveIcon$2
        final /* synthetic */ ConversationSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Drawable invoke() {
            Drawable requireDrawable = ContextUtil.requireDrawable(this.this$0.requireContext(), R.drawable.ic_leave_tinted_24);
            requireDrawable.setColorFilter(new PorterDuffColorFilter(this.this$0.getAlertTint(), PorterDuff.Mode.SRC_IN));
            return requireDrawable;
        }
    });
    private Toolbar toolbar;
    private AvatarImageView toolbarAvatar;
    private FrameLayout toolbarAvatarContainer;
    private View toolbarBackground;
    private BadgeImageView toolbarBadge;
    private TextView toolbarTitle;
    private final Lazy unblockIcon$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Drawable>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$unblockIcon$2
        final /* synthetic */ ConversationSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Drawable invoke() {
            return ContextUtil.requireDrawable(this.this$0.requireContext(), R.drawable.ic_block_tinted_24);
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ConversationSettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$viewModel$2
        final /* synthetic */ ConversationSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            ConversationSettingsFragmentArgs fromBundle = ConversationSettingsFragmentArgs.fromBundle(this.this$0.requireArguments());
            Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
            Parcelable groupId = fromBundle.getGroupId();
            ParcelableGroupId parcelableGroupId = groupId instanceof ParcelableGroupId ? (ParcelableGroupId) groupId : null;
            RecipientId recipientId = fromBundle.getRecipientId();
            GroupId groupId2 = ParcelableGroupId.get(parcelableGroupId);
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new ConversationSettingsViewModel.Factory(recipientId, groupId2, new ConversationSettingsRepository(requireContext, null, 2, null));
        }
    });

    /* compiled from: ConversationSettingsFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsFragment$Callback;", "", "onContentWillRender", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Callback {
        void onContentWillRender();
    }

    public ConversationSettingsFragment() {
        super(0, R.menu.conversation_settings, R.layout.conversation_settings_fragment, null, 9, null);
    }

    public final int getAlertTint() {
        return ((Number) this.alertTint$delegate.getValue()).intValue();
    }

    public final Drawable getBlockIcon() {
        return (Drawable) this.blockIcon$delegate.getValue();
    }

    public final Drawable getUnblockIcon() {
        return (Drawable) this.unblockIcon$delegate.getValue();
    }

    public final Drawable getLeaveIcon() {
        return (Drawable) this.leaveIcon$delegate.getValue();
    }

    public final ConversationSettingsViewModel getViewModel() {
        return (ConversationSettingsViewModel) this.viewModel$delegate.getValue();
    }

    public final NavController getNavController() {
        NavController findNavController = Navigation.findNavController(requireView());
        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
        return findNavController;
    }

    @Override // androidx.fragment.app.Fragment
    public void onAttach(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        super.onAttach(context);
        this.callback = (Callback) context;
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        this.toolbar = (Toolbar) findViewById;
        View findViewById2 = view.findViewById(R.id.toolbar_avatar_container);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.toolbar_avatar_container)");
        this.toolbarAvatarContainer = (FrameLayout) findViewById2;
        View findViewById3 = view.findViewById(R.id.toolbar_avatar);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.toolbar_avatar)");
        this.toolbarAvatar = (AvatarImageView) findViewById3;
        View findViewById4 = view.findViewById(R.id.toolbar_badge);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.toolbar_badge)");
        this.toolbarBadge = (BadgeImageView) findViewById4;
        View findViewById5 = view.findViewById(R.id.toolbar_title);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.toolbar_title)");
        this.toolbarTitle = (TextView) findViewById5;
        View findViewById6 = view.findViewById(R.id.toolbar_background);
        Intrinsics.checkNotNullExpressionValue(findViewById6, "view.findViewById(R.id.toolbar_background)");
        this.toolbarBackground = findViewById6;
        ConversationSettingsFragmentArgs fromBundle = ConversationSettingsFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        if (fromBundle.getRecipientId() != null) {
            setLayoutManagerProducer(new Function1<Context, RecyclerView.LayoutManager>(Badges.INSTANCE) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$onViewCreated$1
                public final RecyclerView.LayoutManager invoke(Context context) {
                    Intrinsics.checkNotNullParameter(context, "p0");
                    return ((Badges) this.receiver).createLayoutManagerForGridWithBadges(context);
                }
            });
        }
        super.onViewCreated(view, bundle);
        RecyclerView recyclerView = getRecyclerView();
        if (recyclerView != null) {
            FrameLayout frameLayout = this.toolbarAvatarContainer;
            View view2 = null;
            if (frameLayout == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbarAvatarContainer");
                frameLayout = null;
            }
            TextView textView = this.toolbarTitle;
            if (textView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbarTitle");
                textView = null;
            }
            View view3 = this.toolbarBackground;
            if (view3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbarBackground");
            } else {
                view2 = view3;
            }
            recyclerView.addOnScrollListener(new ConversationSettingsOnUserScrolledAnimationHelper(frameLayout, textView, view2));
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 1) {
            getViewModel().refreshRecipient();
        } else if (i == 2) {
            getViewModel().refreshRecipient();
        } else if (i != 3) {
            if (i == 4) {
                getViewModel().refreshSharedMedia();
            }
        } else if (intent != null) {
            ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra(PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
            if (parcelableArrayListExtra != null) {
                Intrinsics.checkNotNullExpressionValue(parcelableArrayListExtra, "requireNotNull(data.getP…KEY_SELECTED_RECIPIENTS))");
                SimpleProgressDialog.DismissibleDialog showDelayed = SimpleProgressDialog.showDelayed(requireContext());
                Intrinsics.checkNotNullExpressionValue(showDelayed, "showDelayed(requireContext())");
                getViewModel().onAddToGroupComplete(parcelableArrayListExtra, new Function0<Unit>(showDelayed) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$onActivityResult$1
                    final /* synthetic */ SimpleProgressDialog.DismissibleDialog $progress;

                    /* access modifiers changed from: package-private */
                    {
                        this.$progress = r1;
                    }

                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        this.$progress.dismiss();
                    }
                });
                return;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    @Override // androidx.fragment.app.Fragment
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(menuItem, "item");
        if (menuItem.getItemId() != R.id.action_edit) {
            return super.onOptionsItemSelected(menuItem);
        }
        ConversationSettingsFragmentArgs fromBundle = ConversationSettingsFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        Parcelable groupId = fromBundle.getGroupId();
        if (groupId != null) {
            FragmentActivity requireActivity = requireActivity();
            GroupId groupId2 = ParcelableGroupId.get((ParcelableGroupId) groupId);
            if (groupId2 != null) {
                startActivity(EditProfileActivity.getIntentForGroupProfile(requireActivity, groupId2));
                return true;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.groups.ParcelableGroupId");
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public Material3OnScrollHelper getMaterial3OnScrollHelper(Toolbar toolbar) {
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNull(toolbar);
        return new Material3OnScrollHelper(requireActivity, toolbar) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getMaterial3OnScrollHelper$1
            private final Material3OnScrollHelper.ColorSet inactiveColorSet = new Material3OnScrollHelper.ColorSet(R.color.signal_colorBackground_0, R.color.signal_colorBackground);

            /* access modifiers changed from: package-private */
            {
                Intrinsics.checkNotNullExpressionValue(r2, "requireActivity()");
            }

            @Override // org.thoughtcrime.securesms.util.Material3OnScrollHelper
            public Material3OnScrollHelper.ColorSet getInactiveColorSet() {
                return this.inactiveColorSet;
            }
        };
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ConversationSettingsFragmentArgs fromBundle = ConversationSettingsFragmentArgs.fromBundle(requireArguments());
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(requireArguments())");
        BioTextPreference.INSTANCE.register(dSLSettingsAdapter);
        AvatarPreference.INSTANCE.register(dSLSettingsAdapter);
        ButtonStripPreference.INSTANCE.register(dSLSettingsAdapter);
        LargeIconClickPreference.INSTANCE.register(dSLSettingsAdapter);
        SharedMediaPreference.INSTANCE.register(dSLSettingsAdapter);
        RecipientPreference.INSTANCE.register(dSLSettingsAdapter);
        InternalPreference.INSTANCE.register(dSLSettingsAdapter);
        GroupDescriptionPreference.INSTANCE.register(dSLSettingsAdapter);
        LegacyGroupPreference.INSTANCE.register(dSLSettingsAdapter);
        RecipientId recipientId = fromBundle.getRecipientId();
        if (recipientId != null) {
            Badge.Companion.register(dSLSettingsAdapter, new Function3<Badge, Boolean, Boolean, Unit>(this, recipientId) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$bindAdapter$1
                final /* synthetic */ RecipientId $recipientId;
                final /* synthetic */ ConversationSettingsFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$recipientId = r2;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                @Override // kotlin.jvm.functions.Function3
                public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2, Object obj3) {
                    invoke((Badge) obj, ((Boolean) obj2).booleanValue(), ((Boolean) obj3).booleanValue());
                    return Unit.INSTANCE;
                }

                public final void invoke(Badge badge, boolean z, boolean z2) {
                    Intrinsics.checkNotNullParameter(badge, "badge");
                    ViewBadgeBottomSheetDialogFragment.Companion companion = ViewBadgeBottomSheetDialogFragment.Companion;
                    FragmentManager parentFragmentManager = this.this$0.getParentFragmentManager();
                    Intrinsics.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                    companion.show(parentFragmentManager, this.$recipientId, badge);
                }
            });
        }
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(dSLSettingsAdapter) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DSLSettingsAdapter f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationSettingsFragment.m1082bindAdapter$lambda2(ConversationSettingsFragment.this, this.f$1, (ConversationSettingsState) obj);
            }
        });
        getViewModel().getEvents().observe(getViewLifecycleOwner(), new Observer() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ConversationSettingsFragment.m1084bindAdapter$lambda3(ConversationSettingsFragment.this, (ConversationSettingsEvent) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-2 */
    public static final void m1082bindAdapter$lambda2(ConversationSettingsFragment conversationSettingsFragment, DSLSettingsAdapter dSLSettingsAdapter, ConversationSettingsState conversationSettingsState) {
        Intrinsics.checkNotNullParameter(conversationSettingsFragment, "this$0");
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        if (!Intrinsics.areEqual(conversationSettingsState.getRecipient(), Recipient.UNKNOWN)) {
            AvatarImageView avatarImageView = conversationSettingsFragment.toolbarAvatar;
            BadgeImageView badgeImageView = null;
            if (avatarImageView == null) {
                Intrinsics.throwUninitializedPropertyAccessException("toolbarAvatar");
                avatarImageView = null;
            }
            avatarImageView.buildOptions().withQuickContactEnabled(false).withUseSelfProfileAvatar(false).withFixedSize(ViewUtil.dpToPx(80)).load(conversationSettingsState.getRecipient());
            if (FeatureFlags.displayDonorBadges() && !conversationSettingsState.getRecipient().isSelf()) {
                BadgeImageView badgeImageView2 = conversationSettingsFragment.toolbarBadge;
                if (badgeImageView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("toolbarBadge");
                } else {
                    badgeImageView = badgeImageView2;
                }
                badgeImageView.setBadgeFromRecipient(conversationSettingsState.getRecipient());
            }
            conversationSettingsState.withRecipientSettingsState(new Function1<SpecificSettingsState.RecipientSettingsState, Unit>(conversationSettingsFragment, conversationSettingsState) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$bindAdapter$2$1
                final /* synthetic */ ConversationSettingsState $state;
                final /* synthetic */ ConversationSettingsFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                    this.$state = r2;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                    invoke(recipientSettingsState);
                    return Unit.INSTANCE;
                }

                public final void invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                    Intrinsics.checkNotNullParameter(recipientSettingsState, "it");
                    TextView textView = this.this$0.toolbarTitle;
                    if (textView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("toolbarTitle");
                        textView = null;
                    }
                    textView.setText(this.$state.getRecipient().isSelf() ? this.this$0.getString(R.string.note_to_self) : this.$state.getRecipient().getDisplayName(this.this$0.requireContext()));
                }
            });
            conversationSettingsState.withGroupSettingsState(new Function1<SpecificSettingsState.GroupSettingsState, Unit>(conversationSettingsFragment) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$bindAdapter$2$2
                final /* synthetic */ ConversationSettingsFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                /* Return type fixed from 'java.lang.Object' to match base method */
                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                    invoke(groupSettingsState);
                    return Unit.INSTANCE;
                }

                public final void invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                    Intrinsics.checkNotNullParameter(groupSettingsState, "it");
                    TextView textView = this.this$0.toolbarTitle;
                    Toolbar toolbar = null;
                    if (textView == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("toolbarTitle");
                        textView = null;
                    }
                    textView.setText(groupSettingsState.getGroupTitle());
                    Toolbar toolbar2 = this.this$0.toolbar;
                    if (toolbar2 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("toolbar");
                    } else {
                        toolbar = toolbar2;
                    }
                    toolbar.getMenu().findItem(R.id.action_edit).setVisible(groupSettingsState.getCanEditGroupAttributes());
                }
            });
        }
        Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
        dSLSettingsAdapter.submitList(conversationSettingsFragment.getConfiguration(conversationSettingsState).toMappingModelList(), new Runnable(conversationSettingsFragment) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$$ExternalSyntheticLambda2
            public final /* synthetic */ ConversationSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                ConversationSettingsFragment.m1083bindAdapter$lambda2$lambda1(ConversationSettingsState.this, this.f$1);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-2$lambda-1 */
    public static final void m1083bindAdapter$lambda2$lambda1(ConversationSettingsState conversationSettingsState, ConversationSettingsFragment conversationSettingsFragment) {
        Intrinsics.checkNotNullParameter(conversationSettingsFragment, "this$0");
        if (conversationSettingsState.isLoaded()) {
            View view = conversationSettingsFragment.getView();
            ViewGroup viewGroup = null;
            ViewParent parent = view != null ? view.getParent() : null;
            if (parent instanceof ViewGroup) {
                viewGroup = (ViewGroup) parent;
            }
            if (viewGroup != null) {
                Intrinsics.checkNotNullExpressionValue(OneShotPreDrawListener.add(viewGroup, new Runnable(viewGroup, conversationSettingsFragment) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$bindAdapter$lambda-2$lambda-1$$inlined$doOnPreDraw$1
                    final /* synthetic */ View $this_doOnPreDraw;
                    final /* synthetic */ ConversationSettingsFragment this$0;

                    {
                        this.$this_doOnPreDraw = r1;
                        this.this$0 = r2;
                    }

                    @Override // java.lang.Runnable
                    public final void run() {
                        ConversationSettingsFragment.Callback callback = this.this$0.callback;
                        if (callback == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("callback");
                            callback = null;
                        }
                        callback.onContentWillRender();
                    }
                }), "OneShotPreDrawListener.add(this) { action(this) }");
            }
        }
    }

    /* renamed from: bindAdapter$lambda-3 */
    public static final void m1084bindAdapter$lambda3(ConversationSettingsFragment conversationSettingsFragment, ConversationSettingsEvent conversationSettingsEvent) {
        Intrinsics.checkNotNullParameter(conversationSettingsFragment, "this$0");
        if (conversationSettingsEvent instanceof ConversationSettingsEvent.AddToAGroup) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsEvent, "event");
            conversationSettingsFragment.handleAddToAGroup((ConversationSettingsEvent.AddToAGroup) conversationSettingsEvent);
        } else if (conversationSettingsEvent instanceof ConversationSettingsEvent.AddMembersToGroup) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsEvent, "event");
            conversationSettingsFragment.handleAddMembersToGroup((ConversationSettingsEvent.AddMembersToGroup) conversationSettingsEvent);
        } else if (Intrinsics.areEqual(conversationSettingsEvent, ConversationSettingsEvent.ShowGroupHardLimitDialog.INSTANCE)) {
            conversationSettingsFragment.showGroupHardLimitDialog();
        } else if (conversationSettingsEvent instanceof ConversationSettingsEvent.ShowAddMembersToGroupError) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsEvent, "event");
            conversationSettingsFragment.showAddMembersToGroupError((ConversationSettingsEvent.ShowAddMembersToGroupError) conversationSettingsEvent);
        } else if (conversationSettingsEvent instanceof ConversationSettingsEvent.ShowGroupInvitesSentDialog) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsEvent, "event");
            conversationSettingsFragment.showGroupInvitesSentDialog((ConversationSettingsEvent.ShowGroupInvitesSentDialog) conversationSettingsEvent);
        } else if (conversationSettingsEvent instanceof ConversationSettingsEvent.ShowMembersAdded) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsEvent, "event");
            conversationSettingsFragment.showMembersAdded((ConversationSettingsEvent.ShowMembersAdded) conversationSettingsEvent);
        } else if (conversationSettingsEvent instanceof ConversationSettingsEvent.InitiateGroupMigration) {
            GroupsV1MigrationInitiationBottomSheetDialogFragment.showForInitiation(conversationSettingsFragment.getParentFragmentManager(), ((ConversationSettingsEvent.InitiateGroupMigration) conversationSettingsEvent).getRecipientId());
        }
    }

    private final DSLConfiguration getConfiguration(ConversationSettingsState conversationSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(conversationSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1
            final /* synthetic */ ConversationSettingsState $state;
            final /* synthetic */ ConversationSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(final DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                if (!Intrinsics.areEqual(this.$state.getRecipient(), Recipient.UNKNOWN)) {
                    Recipient recipient = this.$state.getRecipient();
                    StoryViewState storyViewState = this.$state.getStoryViewState();
                    final ConversationSettingsFragment conversationSettingsFragment = this.this$0;
                    final ConversationSettingsState conversationSettingsState2 = this.$state;
                    AnonymousClass1 r4 = new Function1<AvatarView, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.1
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(AvatarView avatarView) {
                            invoke(avatarView);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(AvatarView avatarView) {
                            Intrinsics.checkNotNullParameter(avatarView, "avatar");
                            final Intent intentFromRecipientId = AvatarPreviewActivity.intentFromRecipientId(conversationSettingsFragment.requireContext(), conversationSettingsState2.getRecipient().getId());
                            Intrinsics.checkNotNullExpressionValue(intentFromRecipientId, "intentFromRecipientId(re…xt(), state.recipient.id)");
                            final Bundle createTransitionBundle = AvatarPreviewActivity.createTransitionBundle(conversationSettingsFragment.requireActivity(), avatarView);
                            if (Stories.isFeatureEnabled() && avatarView.hasStory()) {
                                StoryViewerActivity.Companion companion = StoryViewerActivity.Companion;
                                Context requireContext = conversationSettingsFragment.requireContext();
                                Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                                RecipientId id = conversationSettingsState2.getRecipient().getId();
                                Intrinsics.checkNotNullExpressionValue(id, "state.recipient.id");
                                final Intent createIntent = companion.createIntent(requireContext, new StoryViewerArgs(id, conversationSettingsState2.getRecipient().shouldHideStory(), 0, null, null, null, null, false, 0, false, false, false, false, 8188, null));
                                StoryDialogs storyDialogs = StoryDialogs.INSTANCE;
                                Context requireContext2 = conversationSettingsFragment.requireContext();
                                Intrinsics.checkNotNullExpressionValue(requireContext2, "requireContext()");
                                final ConversationSettingsFragment conversationSettingsFragment2 = conversationSettingsFragment;
                                AnonymousClass1 r5 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.1.1
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment2.startActivity(createIntent);
                                    }
                                };
                                final ConversationSettingsFragment conversationSettingsFragment3 = conversationSettingsFragment;
                                storyDialogs.displayStoryOrProfileImage(requireContext2, r5, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.1.2
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment3.startActivity(intentFromRecipientId, createTransitionBundle);
                                    }
                                });
                            } else if (!conversationSettingsState2.getRecipient().isSelf()) {
                                conversationSettingsFragment.startActivity(intentFromRecipientId, createTransitionBundle);
                            }
                        }
                    };
                    final ConversationSettingsFragment conversationSettingsFragment2 = this.this$0;
                    final ConversationSettingsState conversationSettingsState3 = this.$state;
                    dSLConfiguration.customPref(new AvatarPreference.Model(recipient, storyViewState, r4, new Function1<Badge, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.2
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Badge badge) {
                            invoke(badge);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(Badge badge) {
                            Intrinsics.checkNotNullParameter(badge, "badge");
                            ViewBadgeBottomSheetDialogFragment.Companion companion = ViewBadgeBottomSheetDialogFragment.Companion;
                            FragmentManager parentFragmentManager = conversationSettingsFragment2.getParentFragmentManager();
                            Intrinsics.checkNotNullExpressionValue(parentFragmentManager, "parentFragmentManager");
                            RecipientId id = conversationSettingsState3.getRecipient().getId();
                            Intrinsics.checkNotNullExpressionValue(id, "state.recipient.id");
                            companion.show(parentFragmentManager, id, badge);
                        }
                    }));
                    final ConversationSettingsState conversationSettingsState4 = this.$state;
                    conversationSettingsState4.withRecipientSettingsState(new Function1<SpecificSettingsState.RecipientSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.3
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                            invoke(recipientSettingsState);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                            Intrinsics.checkNotNullParameter(recipientSettingsState, "it");
                            dSLConfiguration.customPref(new BioTextPreference.RecipientModel(conversationSettingsState4.getRecipient()));
                        }
                    });
                    ConversationSettingsState conversationSettingsState5 = this.$state;
                    final ConversationSettingsFragment conversationSettingsFragment3 = this.this$0;
                    conversationSettingsState5.withGroupSettingsState(new Function1<SpecificSettingsState.GroupSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.4
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                            invoke(groupSettingsState);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(final SpecificSettingsState.GroupSettingsState groupSettingsState) {
                            String str;
                            Intrinsics.checkNotNullParameter(groupSettingsState, "groupState");
                            boolean z = true;
                            if (groupSettingsState.getGroupId().isV1()) {
                                StringCompanionObject stringCompanionObject = StringCompanionObject.INSTANCE;
                                str = String.format("%s · %s", Arrays.copyOf(new Object[]{groupSettingsState.getMembershipCountDescription(), conversationSettingsFragment3.getString(R.string.ManageGroupActivity_legacy_group)}, 2));
                                Intrinsics.checkNotNullExpressionValue(str, "format(format, *args)");
                            } else {
                                if (!groupSettingsState.getCanEditGroupAttributes()) {
                                    String groupDescription = groupSettingsState.getGroupDescription();
                                    if (!(groupDescription == null || groupDescription.length() == 0)) {
                                        z = false;
                                    }
                                    if (z) {
                                        str = groupSettingsState.getMembershipCountDescription();
                                    }
                                }
                                str = null;
                            }
                            dSLConfiguration.customPref(new BioTextPreference.GroupModel(groupSettingsState.getGroupTitle(), str));
                            if (groupSettingsState.getGroupId().isV2()) {
                                DSLConfiguration dSLConfiguration2 = dSLConfiguration;
                                GroupId groupId = groupSettingsState.getGroupId();
                                String groupDescription2 = groupSettingsState.getGroupDescription();
                                boolean groupDescriptionShouldLinkify = groupSettingsState.getGroupDescriptionShouldLinkify();
                                boolean canEditGroupAttributes = groupSettingsState.getCanEditGroupAttributes();
                                final ConversationSettingsFragment conversationSettingsFragment4 = conversationSettingsFragment3;
                                AnonymousClass1 r6 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.4.1
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        ConversationSettingsFragment conversationSettingsFragment5 = conversationSettingsFragment4;
                                        conversationSettingsFragment5.startActivity(EditProfileActivity.getIntentForGroupProfile(conversationSettingsFragment5.requireActivity(), groupSettingsState.getGroupId()));
                                    }
                                };
                                final ConversationSettingsFragment conversationSettingsFragment5 = conversationSettingsFragment3;
                                dSLConfiguration2.customPref(new GroupDescriptionPreference.Model(groupId, groupDescription2, groupDescriptionShouldLinkify, canEditGroupAttributes, r6, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.4.2
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        GroupDescriptionDialog.show(conversationSettingsFragment5.getChildFragmentManager(), groupSettingsState.getGroupId(), (String) null, groupSettingsState.getGroupDescriptionShouldLinkify());
                                    }
                                }));
                            } else if (groupSettingsState.getLegacyGroupState() != LegacyGroupPreference.State.NONE) {
                                DSLConfiguration dSLConfiguration3 = dSLConfiguration;
                                LegacyGroupPreference.State legacyGroupState = groupSettingsState.getLegacyGroupState();
                                final ConversationSettingsFragment conversationSettingsFragment6 = conversationSettingsFragment3;
                                AnonymousClass3 r2 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.4.3
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        GroupsLearnMoreBottomSheetDialogFragment.show(conversationSettingsFragment6.getParentFragmentManager());
                                    }
                                };
                                final ConversationSettingsFragment conversationSettingsFragment7 = conversationSettingsFragment3;
                                AnonymousClass4 r3 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.4.4
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment7.getViewModel().initiateGroupUpgrade();
                                    }
                                };
                                final ConversationSettingsFragment conversationSettingsFragment8 = conversationSettingsFragment3;
                                dSLConfiguration3.customPref(new LegacyGroupPreference.Model(legacyGroupState, r2, r3, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.4.5
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment8.startActivity(new Intent(conversationSettingsFragment8.requireContext(), InviteActivity.class));
                                    }
                                }));
                            }
                        }
                    });
                    if (this.$state.getDisplayInternalRecipientDetails()) {
                        Recipient recipient2 = this.$state.getRecipient();
                        final ConversationSettingsState conversationSettingsState6 = this.$state;
                        final ConversationSettingsFragment conversationSettingsFragment4 = this.this$0;
                        dSLConfiguration.customPref(new InternalPreference.Model(recipient2, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.5
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                ConversationSettingsFragmentDirections.ActionConversationSettingsFragmentToInternalDetailsSettingsFragment actionConversationSettingsFragmentToInternalDetailsSettingsFragment = ConversationSettingsFragmentDirections.actionConversationSettingsFragmentToInternalDetailsSettingsFragment(conversationSettingsState6.getRecipient().getId());
                                Intrinsics.checkNotNullExpressionValue(actionConversationSettingsFragmentToInternalDetailsSettingsFragment, "actionConversationSettin…gment(state.recipient.id)");
                                SafeNavigation.safeNavigate(conversationSettingsFragment4.getNavController(), actionConversationSettingsFragmentToInternalDetailsSettingsFragment);
                            }
                        }));
                    }
                    ButtonStripPreference.State buttonStripState = this.$state.getButtonStripState();
                    final ConversationSettingsState conversationSettingsState7 = this.$state;
                    final ConversationSettingsFragment conversationSettingsFragment5 = this.this$0;
                    AnonymousClass6 r2 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.6
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            if (!conversationSettingsState7.getRecipient().isPushV2Group() || !conversationSettingsState7.requireGroupSettingsState().isAnnouncementGroup() || conversationSettingsState7.requireGroupSettingsState().isSelfAdmin()) {
                                CommunicationActions.startVideoCall(conversationSettingsFragment5.requireActivity(), conversationSettingsState7.getRecipient());
                            } else {
                                new MaterialAlertDialogBuilder(conversationSettingsFragment5.requireContext()).setTitle(R.string.ConversationActivity_cant_start_group_call).setMessage(R.string.ConversationActivity_only_admins_of_this_group_can_start_a_call).setPositiveButton(17039370, (DialogInterface.OnClickListener) new ConversationSettingsFragment$getConfiguration$1$6$$ExternalSyntheticLambda0()).show();
                            }
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-0  reason: not valid java name */
                        public static final void m1087invoke$lambda0(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    };
                    final ConversationSettingsFragment conversationSettingsFragment6 = this.this$0;
                    final ConversationSettingsState conversationSettingsState8 = this.$state;
                    AnonymousClass7 r3 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.7
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            CommunicationActions.startVoiceCall(conversationSettingsFragment6.requireActivity(), conversationSettingsState8.getRecipient());
                        }
                    };
                    final ConversationSettingsState conversationSettingsState9 = this.$state;
                    final ConversationSettingsFragment conversationSettingsFragment7 = this.this$0;
                    AnonymousClass8 r42 = new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.8
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            if (!conversationSettingsState9.getButtonStripState().isMuted()) {
                                MuteDialog.show(conversationSettingsFragment7.requireContext(), new ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda0(conversationSettingsFragment7.getViewModel()));
                                return;
                            }
                            MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(conversationSettingsFragment7.requireContext());
                            Utils utils = Utils.INSTANCE;
                            long muteUntil = conversationSettingsState9.getRecipient().getMuteUntil();
                            Context requireContext = conversationSettingsFragment7.requireContext();
                            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                            materialAlertDialogBuilder.setMessage((CharSequence) utils.formatMutedUntil(muteUntil, requireContext)).setPositiveButton(R.string.ConversationSettingsFragment__unmute, (DialogInterface.OnClickListener) new ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda1(conversationSettingsFragment7)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda2()).show();
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-0  reason: not valid java name */
                        public static final void m1088invoke$lambda0(ConversationSettingsFragment conversationSettingsFragment8, DialogInterface dialogInterface, int i) {
                            Intrinsics.checkNotNullParameter(conversationSettingsFragment8, "this$0");
                            conversationSettingsFragment8.getViewModel().unmute();
                            dialogInterface.dismiss();
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-1  reason: not valid java name */
                        public static final void m1089invoke$lambda1(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    };
                    final ConversationSettingsFragment conversationSettingsFragment8 = this.this$0;
                    final ConversationSettingsState conversationSettingsState10 = this.$state;
                    dSLConfiguration.customPref(new ButtonStripPreference.Model(buttonStripState, null, null, r2, r3, r42, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.9
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            Intent build = ConversationIntents.createBuilder(conversationSettingsFragment8.requireContext(), conversationSettingsState10.getRecipient().getId(), conversationSettingsState10.getThreadId()).withSearchOpen(true).build();
                            Intrinsics.checkNotNullExpressionValue(build, "createBuilder(requireCon…e)\n              .build()");
                            conversationSettingsFragment8.startActivity(build);
                            conversationSettingsFragment8.requireActivity().finish();
                        }
                    }, 6, null));
                    dSLConfiguration.dividerPref();
                    DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                    DSLSettingsText from = companion.from(this.this$0.formatDisappearingMessagesLifespan(this.$state.getDisappearingMessagesLifespan()), new DSLSettingsText.Modifier[0]);
                    int i = (this.$state.getDisappearingMessagesLifespan() <= 0 || this.$state.getRecipient().isBlocked()) ? R.drawable.ic_update_timer_disabled_16 : R.drawable.ic_update_timer_16;
                    final Ref$BooleanRef ref$BooleanRef = new Ref$BooleanRef();
                    ref$BooleanRef.element = !this.$state.getRecipient().isBlocked();
                    final ConversationSettingsState conversationSettingsState11 = this.$state;
                    conversationSettingsState11.withGroupSettingsState(new Function1<SpecificSettingsState.GroupSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.10
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                            invoke(groupSettingsState);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                            Intrinsics.checkNotNullParameter(groupSettingsState, "it");
                            ref$BooleanRef.element = groupSettingsState.getCanEditGroupAttributes() && !conversationSettingsState11.getRecipient().isBlocked();
                        }
                    });
                    Integer num = null;
                    if (!this.$state.getRecipient().isReleaseNotes() && !this.$state.getRecipient().isBlocked()) {
                        DSLSettingsText from2 = companion.from(R.string.ConversationSettingsFragment__disappearing_messages, new DSLSettingsText.Modifier[0]);
                        DSLSettingsIcon from$default = DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, i, 0, 2, null);
                        boolean z = ref$BooleanRef.element;
                        final ConversationSettingsState conversationSettingsState12 = this.$state;
                        final ConversationSettingsFragment conversationSettingsFragment9 = this.this$0;
                        dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : from, (r18 & 4) != 0 ? null : from$default, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : z, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.11
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                ConversationSettingsFragmentDirections.ActionConversationSettingsFragmentToAppSettingsExpireTimer forResultMode = ConversationSettingsFragmentDirections.actionConversationSettingsFragmentToAppSettingsExpireTimer().setInitialValue(Integer.valueOf(conversationSettingsState12.getDisappearingMessagesLifespan())).setRecipientId(conversationSettingsState12.getRecipient().getId()).setForResultMode(false);
                                Intrinsics.checkNotNullExpressionValue(forResultMode, "actionConversationSettin… .setForResultMode(false)");
                                SafeNavigation.safeNavigate(conversationSettingsFragment9.getNavController(), forResultMode);
                            }
                        }, (r18 & 64) != 0 ? null : null);
                    }
                    if (!this.$state.getRecipient().isReleaseNotes()) {
                        DSLSettingsText from3 = companion.from(R.string.preferences__chat_color_and_wallpaper, new DSLSettingsText.Modifier[0]);
                        DSLSettingsIcon from$default2 = DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, R.drawable.ic_color_24, 0, 2, null);
                        final ConversationSettingsFragment conversationSettingsFragment10 = this.this$0;
                        final ConversationSettingsState conversationSettingsState13 = this.$state;
                        dSLConfiguration.clickPref(from3, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default2, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.12
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                ConversationSettingsFragment conversationSettingsFragment11 = conversationSettingsFragment10;
                                conversationSettingsFragment11.startActivity(ChatWallpaperActivity.createIntent(conversationSettingsFragment11.requireContext(), conversationSettingsState13.getRecipient().getId()));
                            }
                        }, (r18 & 64) != 0 ? null : null);
                    }
                    if (!this.$state.getRecipient().isSelf()) {
                        DSLSettingsText from4 = companion.from(R.string.ConversationSettingsFragment__sounds_and_notifications, new DSLSettingsText.Modifier[0]);
                        DSLSettingsIcon from$default3 = DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, R.drawable.ic_speaker_24, 0, 2, null);
                        final ConversationSettingsState conversationSettingsState14 = this.$state;
                        final ConversationSettingsFragment conversationSettingsFragment11 = this.this$0;
                        dSLConfiguration.clickPref(from4, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default3, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.13
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                ConversationSettingsFragmentDirections.ActionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment = ConversationSettingsFragmentDirections.actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment(conversationSettingsState14.getRecipient().getId());
                                Intrinsics.checkNotNullExpressionValue(actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment, "actionConversationSettin…gment(state.recipient.id)");
                                SafeNavigation.safeNavigate(conversationSettingsFragment11.getNavController(), actionConversationSettingsFragmentToSoundsAndNotificationsSettingsFragment);
                            }
                        }, (r18 & 64) != 0 ? null : null);
                    }
                    final ConversationSettingsState conversationSettingsState15 = this.$state;
                    final ConversationSettingsFragment conversationSettingsFragment12 = this.this$0;
                    conversationSettingsState15.withRecipientSettingsState(new Function1<SpecificSettingsState.RecipientSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.14

                        /* compiled from: ConversationSettingsFragment.kt */
                        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
                        /* renamed from: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1$14$WhenMappings */
                        /* loaded from: classes4.dex */
                        public /* synthetic */ class WhenMappings {
                            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

                            static {
                                int[] iArr = new int[ContactLinkState.values().length];
                                iArr[ContactLinkState.OPEN.ordinal()] = 1;
                                iArr[ContactLinkState.ADD.ordinal()] = 2;
                                iArr[ContactLinkState.NONE.ordinal()] = 3;
                                $EnumSwitchMapping$0 = iArr;
                            }
                        }

                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                            invoke(recipientSettingsState);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(final SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                            DSLSettingsText from5;
                            Intrinsics.checkNotNullParameter(recipientSettingsState, "recipientState");
                            int i2 = WhenMappings.$EnumSwitchMapping$0[recipientSettingsState.getContactLinkState().ordinal()];
                            if (i2 == 1) {
                                DSLConfiguration dSLConfiguration2 = dSLConfiguration;
                                from5 = DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__contact_details, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon from$default4 = DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, R.drawable.ic_profile_circle_24, 0, 2, null);
                                final ConversationSettingsFragment conversationSettingsFragment13 = conversationSettingsFragment12;
                                final ConversationSettingsState conversationSettingsState16 = conversationSettingsState15;
                                dSLConfiguration2.clickPref(from5, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default4, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.14.1
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment13.startActivityForResult(new Intent("android.intent.action.VIEW", conversationSettingsState16.getRecipient().getContactUri()), 1);
                                    }
                                }, (r18 & 64) != 0 ? null : null);
                            } else if (i2 == 2) {
                                DSLConfiguration dSLConfiguration3 = dSLConfiguration;
                                DSLSettingsText from6 = DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__add_as_a_contact, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon from$default5 = DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, R.drawable.ic_plus_24, 0, 2, null);
                                final ConversationSettingsFragment conversationSettingsFragment14 = conversationSettingsFragment12;
                                final ConversationSettingsState conversationSettingsState17 = conversationSettingsState15;
                                dSLConfiguration3.clickPref(from6, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default5, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.14.2
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment14.startActivityForResult(RecipientExporter.export(conversationSettingsState17.getRecipient()).asAddContactIntent(), 2);
                                    }
                                }, (r18 & 64) != 0 ? null : null);
                            }
                            if (recipientSettingsState.getIdentityRecord() != null) {
                                DSLConfiguration dSLConfiguration4 = dSLConfiguration;
                                DSLSettingsText from7 = DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__view_safety_number, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon from$default6 = DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, R.drawable.ic_safety_number_24, 0, 2, null);
                                final ConversationSettingsFragment conversationSettingsFragment15 = conversationSettingsFragment12;
                                dSLConfiguration4.clickPref(from7, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default6, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.14.3
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        ConversationSettingsFragment conversationSettingsFragment16 = conversationSettingsFragment15;
                                        conversationSettingsFragment16.startActivity(VerifyIdentityActivity.newIntent(conversationSettingsFragment16.requireActivity(), recipientSettingsState.getIdentityRecord()));
                                    }
                                }, (r18 & 64) != 0 ? null : null);
                            }
                        }
                    });
                    if (this.$state.getSharedMedia() != null && this.$state.getSharedMedia().getCount() > 0) {
                        dSLConfiguration.dividerPref();
                        dSLConfiguration.sectionHeaderPref(R.string.recipient_preference_activity__shared_media);
                        Cursor sharedMedia = this.$state.getSharedMedia();
                        List<Long> sharedMediaIds = this.$state.getSharedMediaIds();
                        final ConversationSettingsFragment conversationSettingsFragment13 = this.this$0;
                        dSLConfiguration.customPref(new SharedMediaPreference.Model(sharedMedia, sharedMediaIds, new Function2<MediaDatabase.MediaRecord, Boolean, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.15
                            /* Return type fixed from 'java.lang.Object' to match base method */
                            @Override // kotlin.jvm.functions.Function2
                            public /* bridge */ /* synthetic */ Unit invoke(Object obj, Object obj2) {
                                invoke((MediaDatabase.MediaRecord) obj, ((Boolean) obj2).booleanValue());
                                return Unit.INSTANCE;
                            }

                            public final void invoke(MediaDatabase.MediaRecord mediaRecord, boolean z2) {
                                Intrinsics.checkNotNullParameter(mediaRecord, "mediaRecord");
                                ConversationSettingsFragment conversationSettingsFragment14 = conversationSettingsFragment13;
                                conversationSettingsFragment14.startActivityForResult(MediaPreviewActivity.intentFromMediaRecord(conversationSettingsFragment14.requireContext(), mediaRecord, z2), 4);
                            }
                        }));
                        DSLSettingsText from5 = companion.from(R.string.ConversationSettingsFragment__see_all, new DSLSettingsText.Modifier[0]);
                        final ConversationSettingsFragment conversationSettingsFragment14 = this.this$0;
                        final ConversationSettingsState conversationSettingsState16 = this.$state;
                        dSLConfiguration.clickPref(from5, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.16
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                ConversationSettingsFragment conversationSettingsFragment15 = conversationSettingsFragment14;
                                conversationSettingsFragment15.startActivity(MediaOverviewActivity.forThread(conversationSettingsFragment15.requireContext(), conversationSettingsState16.getThreadId()));
                            }
                        }, (r18 & 64) != 0 ? null : null);
                    }
                    final ConversationSettingsState conversationSettingsState17 = this.$state;
                    final ConversationSettingsFragment conversationSettingsFragment15 = this.this$0;
                    conversationSettingsState17.withRecipientSettingsState(new Function1<SpecificSettingsState.RecipientSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.17
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                            invoke(recipientSettingsState);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                            String str;
                            Intrinsics.checkNotNullParameter(recipientSettingsState, "recipientSettingsState");
                            List<Badge> badges = conversationSettingsState17.getRecipient().getBadges();
                            Intrinsics.checkNotNullExpressionValue(badges, "state.recipient.badges");
                            if (!badges.isEmpty()) {
                                dSLConfiguration.dividerPref();
                                dSLConfiguration.sectionHeaderPref(R.string.ManageProfileFragment_badges);
                                Badges badges2 = Badges.INSTANCE;
                                DSLConfiguration dSLConfiguration2 = dSLConfiguration;
                                Context requireContext = conversationSettingsFragment15.requireContext();
                                Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                                List<Badge> badges3 = conversationSettingsState17.getRecipient().getBadges();
                                Intrinsics.checkNotNullExpressionValue(badges3, "state.recipient.badges");
                                badges2.displayBadges(dSLConfiguration2, requireContext, badges3, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0 ? null : null);
                                DSLConfiguration.textPref$default(dSLConfiguration, null, DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__get_badges, new DSLSettingsText.Modifier[0]), 1, null);
                            }
                            if (recipientSettingsState.getSelfHasGroups() && !conversationSettingsState17.getRecipient().isReleaseNotes()) {
                                dSLConfiguration.dividerPref();
                                int size = recipientSettingsState.getAllGroupsInCommon().size();
                                DSLConfiguration dSLConfiguration3 = dSLConfiguration;
                                DSLSettingsText.Companion companion2 = DSLSettingsText.Companion;
                                if (size == 0) {
                                    str = conversationSettingsFragment15.getString(R.string.ManageRecipientActivity_no_groups_in_common);
                                } else {
                                    str = conversationSettingsFragment15.getResources().getQuantityString(R.plurals.ManageRecipientActivity_d_groups_in_common, size, Integer.valueOf(size));
                                }
                                Intrinsics.checkNotNullExpressionValue(str, "if (groupsInCommonCount …        )\n              }");
                                dSLConfiguration3.sectionHeaderPref(companion2.from(str, new DSLSettingsText.Modifier[0]));
                                if (!conversationSettingsState17.getRecipient().isBlocked()) {
                                    DSLConfiguration dSLConfiguration4 = dSLConfiguration;
                                    DSLSettingsText from6 = companion2.from(R.string.ConversationSettingsFragment__add_to_a_group, new DSLSettingsText.Modifier[0]);
                                    DSLSettingsIcon from7 = DSLSettingsIcon.Companion.from(R.drawable.add_to_a_group, -1);
                                    final ConversationSettingsFragment conversationSettingsFragment16 = conversationSettingsFragment15;
                                    dSLConfiguration4.customPref(new LargeIconClickPreference.Model(from6, from7, null, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.17.1
                                        @Override // kotlin.jvm.functions.Function0
                                        public final void invoke() {
                                            conversationSettingsFragment16.getViewModel().onAddToGroup();
                                        }
                                    }, 4, null));
                                }
                                for (final Recipient recipient3 : recipientSettingsState.getGroupsInCommon()) {
                                    DSLConfiguration dSLConfiguration5 = dSLConfiguration;
                                    final ConversationSettingsFragment conversationSettingsFragment17 = conversationSettingsFragment15;
                                    dSLConfiguration5.customPref(new RecipientPreference.Model(recipient3, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.17.2
                                        @Override // kotlin.jvm.functions.Function0
                                        public final void invoke() {
                                            CommunicationActions.startConversation(conversationSettingsFragment17.requireActivity(), recipient3, null);
                                            conversationSettingsFragment17.requireActivity().finish();
                                        }
                                    }, 2, null));
                                }
                                if (recipientSettingsState.getCanShowMoreGroupsInCommon()) {
                                    DSLConfiguration dSLConfiguration6 = dSLConfiguration;
                                    DSLSettingsText from8 = DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__see_all, new DSLSettingsText.Modifier[0]);
                                    DSLSettingsIcon from9 = DSLSettingsIcon.Companion.from(R.drawable.show_more, -1);
                                    final ConversationSettingsFragment conversationSettingsFragment18 = conversationSettingsFragment15;
                                    dSLConfiguration6.customPref(new LargeIconClickPreference.Model(from8, from9, null, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.17.3
                                        @Override // kotlin.jvm.functions.Function0
                                        public final void invoke() {
                                            conversationSettingsFragment18.getViewModel().revealAllMembers();
                                        }
                                    }, 4, null));
                                }
                            }
                        }
                    });
                    final ConversationSettingsState conversationSettingsState18 = this.$state;
                    final ConversationSettingsFragment conversationSettingsFragment16 = this.this$0;
                    conversationSettingsState18.withGroupSettingsState(new Function1<SpecificSettingsState.GroupSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.18
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                            invoke(groupSettingsState);
                            return Unit.INSTANCE;
                        }

                        public final void invoke(final SpecificSettingsState.GroupSettingsState groupSettingsState) {
                            Intrinsics.checkNotNullParameter(groupSettingsState, "groupState");
                            int size = groupSettingsState.getAllMembers().size();
                            if (groupSettingsState.getCanAddToGroup() || size > 0) {
                                dSLConfiguration.dividerPref();
                                DSLConfiguration dSLConfiguration2 = dSLConfiguration;
                                DSLSettingsText.Companion companion2 = DSLSettingsText.Companion;
                                String quantityString = conversationSettingsFragment16.getResources().getQuantityString(R.plurals.ContactSelectionListFragment_d_members, size, Integer.valueOf(size));
                                Intrinsics.checkNotNullExpressionValue(quantityString, "resources.getQuantityStr…memberCount, memberCount)");
                                dSLConfiguration2.sectionHeaderPref(companion2.from(quantityString, new DSLSettingsText.Modifier[0]));
                            }
                            if (groupSettingsState.getCanAddToGroup()) {
                                DSLConfiguration dSLConfiguration3 = dSLConfiguration;
                                DSLSettingsText from6 = DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__add_members, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon from7 = DSLSettingsIcon.Companion.from(R.drawable.add_to_a_group, -1);
                                final ConversationSettingsFragment conversationSettingsFragment17 = conversationSettingsFragment16;
                                dSLConfiguration3.customPref(new LargeIconClickPreference.Model(from6, from7, null, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.1
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment17.getViewModel().onAddToGroup();
                                    }
                                }, 4, null));
                            }
                            for (final GroupMemberEntry.FullMember fullMember : groupSettingsState.getMembers()) {
                                DSLConfiguration dSLConfiguration4 = dSLConfiguration;
                                Recipient member = fullMember.getMember();
                                Intrinsics.checkNotNullExpressionValue(member, "member.member");
                                boolean isAdmin = fullMember.isAdmin();
                                final ConversationSettingsFragment conversationSettingsFragment18 = conversationSettingsFragment16;
                                dSLConfiguration4.customPref(new RecipientPreference.Model(member, isAdmin, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.2
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        RecipientBottomSheetDialogFragment.create(fullMember.getMember().getId(), groupSettingsState.getGroupId()).show(conversationSettingsFragment18.getParentFragmentManager(), BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
                                    }
                                }));
                            }
                            if (groupSettingsState.getCanShowMoreGroupMembers()) {
                                DSLConfiguration dSLConfiguration5 = dSLConfiguration;
                                DSLSettingsText from8 = DSLSettingsText.Companion.from(R.string.ConversationSettingsFragment__see_all, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon from9 = DSLSettingsIcon.Companion.from(R.drawable.show_more, -1);
                                final ConversationSettingsFragment conversationSettingsFragment19 = conversationSettingsFragment16;
                                dSLConfiguration5.customPref(new LargeIconClickPreference.Model(from8, from9, null, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.3
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        conversationSettingsFragment19.getViewModel().revealAllMembers();
                                    }
                                }, 4, null));
                            }
                            if (conversationSettingsState18.getRecipient().isPushV2Group()) {
                                dSLConfiguration.dividerPref();
                                DSLConfiguration dSLConfiguration6 = dSLConfiguration;
                                DSLSettingsText.Companion companion3 = DSLSettingsText.Companion;
                                DSLSettingsText from10 = companion3.from(R.string.ConversationSettingsFragment__group_link, new DSLSettingsText.Modifier[0]);
                                DSLSettingsText from11 = companion3.from(groupSettingsState.getGroupLinkEnabled() ? R.string.preferences_on : R.string.preferences_off, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon.Companion companion4 = DSLSettingsIcon.Companion;
                                DSLSettingsIcon from$default4 = DSLSettingsIcon.Companion.from$default(companion4, R.drawable.ic_link_16, 0, 2, null);
                                final ConversationSettingsFragment conversationSettingsFragment20 = conversationSettingsFragment16;
                                dSLConfiguration6.clickPref(from10, (r18 & 2) != 0 ? null : from11, (r18 & 4) != 0 ? null : from$default4, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.4
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        NavController navController = conversationSettingsFragment20.getNavController();
                                        ConversationSettingsFragmentDirections.ActionConversationSettingsFragmentToShareableGroupLinkFragment actionConversationSettingsFragmentToShareableGroupLinkFragment = ConversationSettingsFragmentDirections.actionConversationSettingsFragmentToShareableGroupLinkFragment(groupSettingsState.getGroupId().requireV2().toString());
                                        Intrinsics.checkNotNullExpressionValue(actionConversationSettingsFragmentToShareableGroupLinkFragment, "actionConversationSettin…d.requireV2().toString())");
                                        SafeNavigation.safeNavigate(navController, actionConversationSettingsFragmentToShareableGroupLinkFragment);
                                    }
                                }, (r18 & 64) != 0 ? null : null);
                                DSLConfiguration dSLConfiguration7 = dSLConfiguration;
                                DSLSettingsText from12 = companion3.from(R.string.ConversationSettingsFragment__requests_and_invites, new DSLSettingsText.Modifier[0]);
                                DSLSettingsIcon from$default5 = DSLSettingsIcon.Companion.from$default(companion4, R.drawable.ic_update_group_add_16, 0, 2, null);
                                final ConversationSettingsFragment conversationSettingsFragment21 = conversationSettingsFragment16;
                                dSLConfiguration7.clickPref(from12, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default5, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.5
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        ConversationSettingsFragment conversationSettingsFragment22 = conversationSettingsFragment21;
                                        conversationSettingsFragment22.startActivity(ManagePendingAndRequestingMembersActivity.newIntent(conversationSettingsFragment22.requireContext(), groupSettingsState.getGroupId().requireV2()));
                                    }
                                }, (r18 & 64) != 0 ? null : null);
                                if (groupSettingsState.isSelfAdmin()) {
                                    DSLConfiguration dSLConfiguration8 = dSLConfiguration;
                                    DSLSettingsText from13 = companion3.from(R.string.ConversationSettingsFragment__permissions, new DSLSettingsText.Modifier[0]);
                                    DSLSettingsIcon from$default6 = DSLSettingsIcon.Companion.from$default(companion4, R.drawable.ic_lock_24, 0, 2, null);
                                    final ConversationSettingsFragment conversationSettingsFragment22 = conversationSettingsFragment16;
                                    dSLConfiguration8.clickPref(from13, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from$default6, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.6
                                        @Override // kotlin.jvm.functions.Function0
                                        public final void invoke() {
                                            ConversationSettingsFragmentDirections.ActionConversationSettingsFragmentToPermissionsSettingsFragment actionConversationSettingsFragmentToPermissionsSettingsFragment = ConversationSettingsFragmentDirections.actionConversationSettingsFragmentToPermissionsSettingsFragment(ParcelableGroupId.from(groupSettingsState.getGroupId()));
                                            Intrinsics.checkNotNullExpressionValue(actionConversationSettingsFragmentToPermissionsSettingsFragment, "actionConversationSettin…from(groupState.groupId))");
                                            SafeNavigation.safeNavigate(conversationSettingsFragment22.getNavController(), actionConversationSettingsFragmentToPermissionsSettingsFragment);
                                        }
                                    }, (r18 & 64) != 0 ? null : null);
                                }
                            }
                            if (groupSettingsState.getCanLeave()) {
                                dSLConfiguration.dividerPref();
                                DSLConfiguration dSLConfiguration9 = dSLConfiguration;
                                DSLSettingsText from14 = DSLSettingsText.Companion.from(R.string.conversation__menu_leave_group, conversationSettingsFragment16.getAlertTint());
                                DSLSettingsIcon.Companion companion5 = DSLSettingsIcon.Companion;
                                Drawable drawable = conversationSettingsFragment16.getLeaveIcon();
                                Intrinsics.checkNotNullExpressionValue(drawable, "leaveIcon");
                                DSLSettingsIcon from15 = companion5.from(drawable);
                                final ConversationSettingsFragment conversationSettingsFragment23 = conversationSettingsFragment16;
                                dSLConfiguration9.clickPref(from14, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from15, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.getConfiguration.1.18.7
                                    @Override // kotlin.jvm.functions.Function0
                                    public final void invoke() {
                                        LeaveGroupDialog.handleLeavePushGroup(conversationSettingsFragment23.requireActivity(), groupSettingsState.getGroupId().requirePush(), null);
                                    }
                                }, (r18 & 64) != 0 ? null : null);
                            }
                        }
                    });
                    if (this.$state.getCanModifyBlockedState()) {
                        this.$state.withRecipientSettingsState(new Function1<SpecificSettingsState.RecipientSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.19
                            /* Return type fixed from 'java.lang.Object' to match base method */
                            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                            @Override // kotlin.jvm.functions.Function1
                            public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                                invoke(recipientSettingsState);
                                return Unit.INSTANCE;
                            }

                            public final void invoke(SpecificSettingsState.RecipientSettingsState recipientSettingsState) {
                                Intrinsics.checkNotNullParameter(recipientSettingsState, "it");
                                dSLConfiguration.dividerPref();
                            }
                        });
                        this.$state.withGroupSettingsState(new Function1<SpecificSettingsState.GroupSettingsState, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.20
                            /* Return type fixed from 'java.lang.Object' to match base method */
                            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                            @Override // kotlin.jvm.functions.Function1
                            public /* bridge */ /* synthetic */ Unit invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                                invoke(groupSettingsState);
                                return Unit.INSTANCE;
                            }

                            public final void invoke(SpecificSettingsState.GroupSettingsState groupSettingsState) {
                                Intrinsics.checkNotNullParameter(groupSettingsState, "it");
                                if (!groupSettingsState.getCanLeave()) {
                                    dSLConfiguration.dividerPref();
                                }
                            }
                        });
                        boolean isBlocked = this.$state.getRecipient().isBlocked();
                        boolean isPushGroup = this.$state.getRecipient().isPushGroup();
                        int i2 = (!isBlocked || !isPushGroup) ? isBlocked ? R.string.ConversationSettingsFragment__unblock : isPushGroup ? R.string.ConversationSettingsFragment__block_group : R.string.ConversationSettingsFragment__block : R.string.ConversationSettingsFragment__unblock_group;
                        if (!isBlocked) {
                            num = Integer.valueOf(this.this$0.getAlertTint());
                        }
                        Drawable drawable = isBlocked ? this.this$0.getUnblockIcon() : this.this$0.getBlockIcon();
                        Intrinsics.checkNotNullExpressionValue(drawable, "if (isBlocked) unblockIcon else blockIcon");
                        DSLSettingsText from6 = num != null ? companion.from(i2, num.intValue()) : companion.from(i2, new DSLSettingsText.Modifier[0]);
                        DSLSettingsIcon from7 = DSLSettingsIcon.Companion.from(drawable);
                        final ConversationSettingsState conversationSettingsState19 = this.$state;
                        final ConversationSettingsFragment conversationSettingsFragment17 = this.this$0;
                        dSLConfiguration.clickPref(from6, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : from7, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1.21
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                if (conversationSettingsState19.getRecipient().isBlocked()) {
                                    BlockUnblockDialog.showUnblockFor(conversationSettingsFragment17.requireContext(), conversationSettingsFragment17.getViewLifecycleOwner().getLifecycle(), conversationSettingsState19.getRecipient(), new ConversationSettingsFragment$getConfiguration$1$21$$ExternalSyntheticLambda0(conversationSettingsFragment17));
                                } else {
                                    BlockUnblockDialog.showBlockFor(conversationSettingsFragment17.requireContext(), conversationSettingsFragment17.getViewLifecycleOwner().getLifecycle(), conversationSettingsState19.getRecipient(), new ConversationSettingsFragment$getConfiguration$1$21$$ExternalSyntheticLambda1(conversationSettingsFragment17));
                                }
                            }

                            /* access modifiers changed from: private */
                            /* renamed from: invoke$lambda-0  reason: not valid java name */
                            public static final void m1085invoke$lambda0(ConversationSettingsFragment conversationSettingsFragment18) {
                                Intrinsics.checkNotNullParameter(conversationSettingsFragment18, "this$0");
                                conversationSettingsFragment18.getViewModel().unblock();
                            }

                            /* access modifiers changed from: private */
                            /* renamed from: invoke$lambda-1  reason: not valid java name */
                            public static final void m1086invoke$lambda1(ConversationSettingsFragment conversationSettingsFragment18) {
                                Intrinsics.checkNotNullParameter(conversationSettingsFragment18, "this$0");
                                conversationSettingsFragment18.getViewModel().block();
                            }
                        }, (r18 & 64) != 0 ? null : null);
                    }
                }
            }
        });
    }

    public final String formatDisappearingMessagesLifespan(int i) {
        if (i <= 0) {
            String string = getString(R.string.preferences_off);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      getString(R.string.preferences_off)\n    }");
            return string;
        }
        String expirationDisplayValue = ExpirationUtil.getExpirationDisplayValue(requireContext(), i);
        Intrinsics.checkNotNullExpressionValue(expirationDisplayValue, "{\n      ExpirationUtil.g…ngMessagesLifespan)\n    }");
        return expirationDisplayValue;
    }

    private final void handleAddToAGroup(ConversationSettingsEvent.AddToAGroup addToAGroup) {
        startActivity(AddToGroupsActivity.newIntent(requireContext(), addToAGroup.getRecipientId(), addToAGroup.getGroupMembership()));
    }

    private final void handleAddMembersToGroup(ConversationSettingsEvent.AddMembersToGroup addMembersToGroup) {
        startActivityForResult(AddMembersActivity.createIntent(requireContext(), addMembersToGroup.getGroupId(), 1, addMembersToGroup.getSelectionWarning(), addMembersToGroup.getSelectionLimit(), addMembersToGroup.isAnnouncementGroup(), addMembersToGroup.getGroupMembersWithoutSelf()), 3);
    }

    private final void showGroupHardLimitDialog() {
        GroupLimitDialog.showHardLimitMessage(requireContext());
    }

    private final void showAddMembersToGroupError(ConversationSettingsEvent.ShowAddMembersToGroupError showAddMembersToGroupError) {
        Toast.makeText(requireContext(), GroupErrors.getUserDisplayMessage(showAddMembersToGroupError.getFailureReason()), 1).show();
    }

    private final void showGroupInvitesSentDialog(ConversationSettingsEvent.ShowGroupInvitesSentDialog showGroupInvitesSentDialog) {
        GroupInviteSentDialog.showInvitesSent(requireContext(), getViewLifecycleOwner(), showGroupInvitesSentDialog.getInvitesSentTo());
    }

    private final void showMembersAdded(ConversationSettingsEvent.ShowMembersAdded showMembersAdded) {
        String quantityString = getResources().getQuantityString(R.plurals.ManageGroupActivity_added, showMembersAdded.getMembersAddedCount(), Integer.valueOf(showMembersAdded.getMembersAddedCount()));
        Intrinsics.checkNotNullExpressionValue(quantityString, "resources.getQuantityStr…d.membersAddedCount\n    )");
        Snackbar.make(requireView(), quantityString, -1).show();
    }

    /* compiled from: ConversationSettingsFragment.kt */
    @Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\nH\u0014J\u0010\u0010\u0015\u001a\u00020\u00142\u0006\u0010\t\u001a\u00020\nH\u0014R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nXD¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsFragment$ConversationSettingsOnUserScrolledAnimationHelper;", "Lorg/thoughtcrime/securesms/components/recyclerview/OnScrollAnimationHelper;", "toolbarAvatar", "Landroid/view/View;", "toolbarTitle", "toolbarBackground", "(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V", "actionBarSize", "", "duration", "", "getDuration", "()J", "rect", "Landroid/graphics/Rect;", "getAnimationState", "Lorg/thoughtcrime/securesms/components/recyclerview/OnScrollAnimationHelper$AnimationState;", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "hide", "", "show", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    private static final class ConversationSettingsOnUserScrolledAnimationHelper extends OnScrollAnimationHelper {
        private final float actionBarSize = DimensionUnit.DP.toPixels(64.0f);
        private final long duration = 200;
        private final Rect rect = new Rect();
        private final View toolbarAvatar;
        private final View toolbarBackground;
        private final View toolbarTitle;

        public ConversationSettingsOnUserScrolledAnimationHelper(View view, View view2, View view3) {
            Intrinsics.checkNotNullParameter(view, "toolbarAvatar");
            Intrinsics.checkNotNullParameter(view2, "toolbarTitle");
            Intrinsics.checkNotNullParameter(view3, "toolbarBackground");
            this.toolbarAvatar = view;
            this.toolbarTitle = view2;
            this.toolbarBackground = view3;
        }

        @Override // org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper
        protected long getDuration() {
            return this.duration;
        }

        @Override // org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper
        protected OnScrollAnimationHelper.AnimationState getAnimationState(RecyclerView recyclerView) {
            int i;
            Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
            RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
            Intrinsics.checkNotNull(layoutManager);
            if (layoutManager instanceof FlexboxLayoutManager) {
                i = ((FlexboxLayoutManager) layoutManager).findFirstVisibleItemPosition();
            } else {
                i = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
            }
            if (i != 0) {
                return OnScrollAnimationHelper.AnimationState.SHOW;
            }
            View childAt = layoutManager.getChildAt(0);
            if (childAt != null) {
                Intrinsics.checkNotNullExpressionValue(childAt, "requireNotNull(layoutManager.getChildAt(0))");
                childAt.getLocalVisibleRect(this.rect);
                if (((float) this.rect.height()) <= this.actionBarSize) {
                    return OnScrollAnimationHelper.AnimationState.SHOW;
                }
                return OnScrollAnimationHelper.AnimationState.HIDE;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }

        @Override // org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper
        protected void show(long j) {
            this.toolbarAvatar.animate().setDuration(j).translationY(0.0f).alpha(1.0f);
            this.toolbarTitle.animate().setDuration(j).translationY(0.0f).alpha(1.0f);
            this.toolbarBackground.animate().setDuration(j).alpha(1.0f);
        }

        @Override // org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper
        protected void hide(long j) {
            this.toolbarAvatar.animate().setDuration(j).translationY((float) ViewUtil.dpToPx(56)).alpha(0.0f);
            this.toolbarTitle.animate().setDuration(j).translationY((float) ViewUtil.dpToPx(56)).alpha(0.0f);
            this.toolbarBackground.animate().setDuration(j).alpha(0.0f);
        }
    }
}
