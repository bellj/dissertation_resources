package org.thoughtcrime.securesms.components.emoji;

import kotlin.Metadata;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: EmojiItemDecoration.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"EDGE_LENGTH", "", "EMOJI_VERTICAL_INSET", "HEADER_VERTICAL_INSET", "HORIZONTAL_INSET", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiItemDecorationKt {
    private static final int EDGE_LENGTH = ViewUtil.dpToPx(6);
    private static final int EMOJI_VERTICAL_INSET = ViewUtil.dpToPx(5);
    private static final int HEADER_VERTICAL_INSET = ViewUtil.dpToPx(8);
    private static final int HORIZONTAL_INSET = ViewUtil.dpToPx(6);
}
