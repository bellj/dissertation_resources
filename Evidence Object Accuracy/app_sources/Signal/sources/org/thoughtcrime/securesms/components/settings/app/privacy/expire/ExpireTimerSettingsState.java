package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.livedata.ProcessState;

/* compiled from: ExpireTimerSettingsState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B?\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\b¢\u0006\u0002\u0010\nJ\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0003HÆ\u0003¢\u0006\u0002\u0010\u0013J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006HÆ\u0003J\t\u0010\u0018\u001a\u00020\bHÆ\u0003J\t\u0010\u0019\u001a\u00020\bHÆ\u0003JH\u0010\u001a\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u00062\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\bHÆ\u0001¢\u0006\u0002\u0010\u001bJ\u0013\u0010\u001c\u001a\u00020\b2\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001e\u001a\u00020\u0003HÖ\u0001J\t\u0010\u001f\u001a\u00020 HÖ\u0001R\u0011\u0010\u000b\u001a\u00020\u00038F¢\u0006\u0006\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u000fR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000fR\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00030\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0003¢\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0012\u0010\u0013¨\u0006!"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsState;", "", "initialTimer", "", "userSetTimer", "saveState", "Lorg/thoughtcrime/securesms/util/livedata/ProcessState;", "isGroupCreate", "", "isForRecipient", "(ILjava/lang/Integer;Lorg/thoughtcrime/securesms/util/livedata/ProcessState;ZZ)V", "currentTimer", "getCurrentTimer", "()I", "getInitialTimer", "()Z", "getSaveState", "()Lorg/thoughtcrime/securesms/util/livedata/ProcessState;", "getUserSetTimer", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "component1", "component2", "component3", "component4", "component5", "copy", "(ILjava/lang/Integer;Lorg/thoughtcrime/securesms/util/livedata/ProcessState;ZZ)Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsState;", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsState {
    private final int initialTimer;
    private final boolean isForRecipient;
    private final boolean isGroupCreate;
    private final ProcessState<Integer> saveState;
    private final Integer userSetTimer;

    public ExpireTimerSettingsState() {
        this(0, null, null, false, false, 31, null);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ ExpireTimerSettingsState copy$default(ExpireTimerSettingsState expireTimerSettingsState, int i, Integer num, ProcessState processState, boolean z, boolean z2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = expireTimerSettingsState.initialTimer;
        }
        if ((i2 & 2) != 0) {
            num = expireTimerSettingsState.userSetTimer;
        }
        if ((i2 & 4) != 0) {
            processState = expireTimerSettingsState.saveState;
        }
        if ((i2 & 8) != 0) {
            z = expireTimerSettingsState.isGroupCreate;
        }
        if ((i2 & 16) != 0) {
            z2 = expireTimerSettingsState.isForRecipient;
        }
        return expireTimerSettingsState.copy(i, num, processState, z, z2);
    }

    public final int component1() {
        return this.initialTimer;
    }

    public final Integer component2() {
        return this.userSetTimer;
    }

    public final ProcessState<Integer> component3() {
        return this.saveState;
    }

    public final boolean component4() {
        return this.isGroupCreate;
    }

    public final boolean component5() {
        return this.isForRecipient;
    }

    public final ExpireTimerSettingsState copy(int i, Integer num, ProcessState<Integer> processState, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(processState, "saveState");
        return new ExpireTimerSettingsState(i, num, processState, z, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ExpireTimerSettingsState)) {
            return false;
        }
        ExpireTimerSettingsState expireTimerSettingsState = (ExpireTimerSettingsState) obj;
        return this.initialTimer == expireTimerSettingsState.initialTimer && Intrinsics.areEqual(this.userSetTimer, expireTimerSettingsState.userSetTimer) && Intrinsics.areEqual(this.saveState, expireTimerSettingsState.saveState) && this.isGroupCreate == expireTimerSettingsState.isGroupCreate && this.isForRecipient == expireTimerSettingsState.isForRecipient;
    }

    public int hashCode() {
        int i = this.initialTimer * 31;
        Integer num = this.userSetTimer;
        int hashCode = (((i + (num == null ? 0 : num.hashCode())) * 31) + this.saveState.hashCode()) * 31;
        boolean z = this.isGroupCreate;
        int i2 = 1;
        if (z) {
            z = true;
        }
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = (hashCode + i3) * 31;
        boolean z2 = this.isForRecipient;
        if (!z2) {
            i2 = z2 ? 1 : 0;
        }
        return i6 + i2;
    }

    public String toString() {
        return "ExpireTimerSettingsState(initialTimer=" + this.initialTimer + ", userSetTimer=" + this.userSetTimer + ", saveState=" + this.saveState + ", isGroupCreate=" + this.isGroupCreate + ", isForRecipient=" + this.isForRecipient + ')';
    }

    public ExpireTimerSettingsState(int i, Integer num, ProcessState<Integer> processState, boolean z, boolean z2) {
        Intrinsics.checkNotNullParameter(processState, "saveState");
        this.initialTimer = i;
        this.userSetTimer = num;
        this.saveState = processState;
        this.isGroupCreate = z;
        this.isForRecipient = z2;
    }

    public final int getInitialTimer() {
        return this.initialTimer;
    }

    public final Integer getUserSetTimer() {
        return this.userSetTimer;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ ExpireTimerSettingsState(int r4, java.lang.Integer r5, org.thoughtcrime.securesms.util.livedata.ProcessState r6, boolean r7, boolean r8, int r9, kotlin.jvm.internal.DefaultConstructorMarker r10) {
        /*
            r3 = this;
            r10 = r9 & 1
            r0 = 0
            if (r10 == 0) goto L_0x0007
            r10 = 0
            goto L_0x0008
        L_0x0007:
            r10 = r4
        L_0x0008:
            r4 = r9 & 2
            if (r4 == 0) goto L_0x000d
            r5 = 0
        L_0x000d:
            r1 = r5
            r4 = r9 & 4
            if (r4 == 0) goto L_0x0017
            org.thoughtcrime.securesms.util.livedata.ProcessState$Idle r6 = new org.thoughtcrime.securesms.util.livedata.ProcessState$Idle
            r6.<init>()
        L_0x0017:
            r2 = r6
            r4 = r9 & 8
            if (r4 == 0) goto L_0x001d
            goto L_0x001e
        L_0x001d:
            r0 = r7
        L_0x001e:
            r4 = r9 & 16
            if (r4 == 0) goto L_0x0024
            r9 = r0
            goto L_0x0025
        L_0x0024:
            r9 = r8
        L_0x0025:
            r4 = r3
            r5 = r10
            r6 = r1
            r7 = r2
            r8 = r0
            r4.<init>(r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsState.<init>(int, java.lang.Integer, org.thoughtcrime.securesms.util.livedata.ProcessState, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final ProcessState<Integer> getSaveState() {
        return this.saveState;
    }

    public final boolean isGroupCreate() {
        return this.isGroupCreate;
    }

    public final boolean isForRecipient() {
        return this.isForRecipient;
    }

    public final int getCurrentTimer() {
        Integer num = this.userSetTimer;
        return num != null ? num.intValue() : this.initialTimer;
    }
}
