package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.Insets;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.WindowInsets;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class InsetAwareConstraintLayout extends ConstraintLayout {
    public InsetAwareConstraintLayout(Context context) {
        super(context);
    }

    public InsetAwareConstraintLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public InsetAwareConstraintLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    @Override // android.view.View
    public WindowInsets onApplyWindowInsets(WindowInsets windowInsets) {
        if (Build.VERSION.SDK_INT < 30) {
            return super.onApplyWindowInsets(windowInsets);
        }
        Insets insets = windowInsets.getInsets(WindowInsets.Type.systemBars() | WindowInsets.Type.ime() | WindowInsets.Type.displayCutout());
        applyInsets(new Rect(insets.left, insets.top, insets.right, insets.bottom));
        return super.onApplyWindowInsets(windowInsets);
    }

    @Override // android.view.View
    protected boolean fitSystemWindows(Rect rect) {
        if (Build.VERSION.SDK_INT >= 30) {
            return true;
        }
        applyInsets(rect);
        return true;
    }

    private void applyInsets(Rect rect) {
        Guideline guideline = (Guideline) findViewById(R.id.status_bar_guideline);
        Guideline guideline2 = (Guideline) findViewById(R.id.navigation_bar_guideline);
        Guideline guideline3 = (Guideline) findViewById(R.id.parent_start_guideline);
        Guideline guideline4 = (Guideline) findViewById(R.id.parent_end_guideline);
        if (guideline != null) {
            guideline.setGuidelineBegin(rect.top);
        }
        if (guideline2 != null) {
            guideline2.setGuidelineEnd(rect.bottom);
        }
        if (guideline3 != null) {
            if (ViewUtil.isLtr(this)) {
                guideline3.setGuidelineBegin(rect.left);
            } else {
                guideline3.setGuidelineBegin(rect.right);
            }
        }
        if (guideline4 == null) {
            return;
        }
        if (ViewUtil.isLtr(this)) {
            guideline4.setGuidelineEnd(rect.right);
        } else {
            guideline4.setGuidelineEnd(rect.left);
        }
    }
}
