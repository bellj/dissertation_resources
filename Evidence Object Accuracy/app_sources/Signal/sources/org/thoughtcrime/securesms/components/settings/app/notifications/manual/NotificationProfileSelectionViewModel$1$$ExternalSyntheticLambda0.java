package org.thoughtcrime.securesms.components.settings.app.notifications.manual;

import com.annimon.stream.function.Function;
import java.util.List;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileSelectionViewModel$1$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ List f$0;

    public /* synthetic */ NotificationProfileSelectionViewModel$1$$ExternalSyntheticLambda0(List list) {
        this.f$0 = list;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return NotificationProfileSelectionViewModel.AnonymousClass1.m720invoke$lambda0(this.f$0, (NotificationProfileSelectionState) obj);
    }
}
