package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.google.android.material.tabs.TabLayout;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.collections.IntIterator;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;
import kotlin.ranges.RangesKt___RangesKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.ControllableTabLayout;

/* compiled from: BoldSelectionTabItem.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u0000 \u00102\u00020\u0001:\u0003\u0010\u0011\u0012B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\b\u0010\f\u001a\u00020\rH\u0014J\u0006\u0010\u000e\u001a\u00020\rJ\u0006\u0010\u000f\u001a\u00020\rR\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX.¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/BoldSelectionTabItem;", "Landroid/widget/FrameLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "selectedTextView", "Landroid/widget/TextView;", "unselectedTextView", "onFinishInflate", "", "select", "unselect", "Companion", "NewTabListener", "OnTabSelectedListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoldSelectionTabItem extends FrameLayout {
    public static final Companion Companion = new Companion(null);
    private TextView selectedTextView;
    private TextView unselectedTextView;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public BoldSelectionTabItem(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public BoldSelectionTabItem(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    @JvmStatic
    public static final void registerListeners(ControllableTabLayout controllableTabLayout) {
        Companion.registerListeners(controllableTabLayout);
    }

    public /* synthetic */ BoldSelectionTabItem(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public BoldSelectionTabItem(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        View findViewById = findViewById(16908308);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(android.R.id.text1)");
        this.unselectedTextView = (TextView) findViewById;
        View findViewById2 = findViewById(R.id.text1_bold);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.text1_bold)");
        this.selectedTextView = (TextView) findViewById2;
        TextView textView = this.unselectedTextView;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("unselectedTextView");
            textView = null;
        }
        textView.addTextChangedListener(new TextWatcher(this) { // from class: org.thoughtcrime.securesms.components.BoldSelectionTabItem$onFinishInflate$$inlined$doAfterTextChanged$1
            final /* synthetic */ BoldSelectionTabItem this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.this$0 = r1;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                TextView textView2 = this.this$0.selectedTextView;
                if (textView2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("selectedTextView");
                    textView2 = null;
                }
                textView2.setText(editable);
            }
        });
    }

    public final void select() {
        TextView textView = this.unselectedTextView;
        TextView textView2 = null;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("unselectedTextView");
            textView = null;
        }
        textView.setAlpha(0.0f);
        TextView textView3 = this.selectedTextView;
        if (textView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("selectedTextView");
        } else {
            textView2 = textView3;
        }
        textView2.setAlpha(1.0f);
    }

    public final void unselect() {
        TextView textView = this.unselectedTextView;
        TextView textView2 = null;
        if (textView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("unselectedTextView");
            textView = null;
        }
        textView.setAlpha(1.0f);
        TextView textView3 = this.selectedTextView;
        if (textView3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("selectedTextView");
        } else {
            textView2 = textView3;
        }
        textView2.setAlpha(0.0f);
    }

    /* compiled from: BoldSelectionTabItem.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/BoldSelectionTabItem$Companion;", "", "()V", "registerListeners", "", "tabLayout", "Lorg/thoughtcrime/securesms/components/ControllableTabLayout;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void registerListeners(ControllableTabLayout controllableTabLayout) {
            Intrinsics.checkNotNullParameter(controllableTabLayout, "tabLayout");
            NewTabListener newTabListener = new NewTabListener();
            OnTabSelectedListener onTabSelectedListener = new OnTabSelectedListener();
            IntRange intRange = RangesKt___RangesKt.until(0, controllableTabLayout.getTabCount());
            ArrayList<TabLayout.Tab> arrayList = new ArrayList();
            Iterator<Integer> it = intRange.iterator();
            while (it.hasNext()) {
                TabLayout.Tab tabAt = controllableTabLayout.getTabAt(((IntIterator) it).nextInt());
                if (tabAt != null) {
                    arrayList.add(tabAt);
                }
            }
            for (TabLayout.Tab tab : arrayList) {
                Intrinsics.checkNotNullExpressionValue(tab, "it");
                newTabListener.onNewTab(tab);
                if (tab.isSelected()) {
                    onTabSelectedListener.onTabSelected(tab);
                } else {
                    onTabSelectedListener.onTabUnselected(tab);
                }
            }
            controllableTabLayout.setNewTabListener(newTabListener);
            controllableTabLayout.addOnTabSelectedListener((TabLayout.OnTabSelectedListener) onTabSelectedListener);
        }
    }

    /* compiled from: BoldSelectionTabItem.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/BoldSelectionTabItem$NewTabListener;", "Lorg/thoughtcrime/securesms/components/ControllableTabLayout$NewTabListener;", "()V", "onNewTab", "", "tab", "Lcom/google/android/material/tabs/TabLayout$Tab;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NewTabListener implements ControllableTabLayout.NewTabListener {
        @Override // org.thoughtcrime.securesms.components.ControllableTabLayout.NewTabListener
        public void onNewTab(TabLayout.Tab tab) {
            Intrinsics.checkNotNullParameter(tab, "tab");
            if (tab.getCustomView() == null) {
                tab.setCustomView(R.layout.bold_selection_tab_item);
            }
        }
    }

    /* compiled from: BoldSelectionTabItem.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/BoldSelectionTabItem$OnTabSelectedListener;", "Lcom/google/android/material/tabs/TabLayout$OnTabSelectedListener;", "()V", "onTabReselected", "", "tab", "Lcom/google/android/material/tabs/TabLayout$Tab;", "onTabSelected", "onTabUnselected", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class OnTabSelectedListener implements TabLayout.OnTabSelectedListener {
        @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
        public void onTabReselected(TabLayout.Tab tab) {
            Intrinsics.checkNotNullParameter(tab, "tab");
        }

        @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
        public void onTabSelected(TabLayout.Tab tab) {
            Intrinsics.checkNotNullParameter(tab, "tab");
            View customView = tab.getCustomView();
            Objects.requireNonNull(customView);
            ((BoldSelectionTabItem) customView).select();
        }

        @Override // com.google.android.material.tabs.TabLayout.BaseOnTabSelectedListener
        public void onTabUnselected(TabLayout.Tab tab) {
            Intrinsics.checkNotNullParameter(tab, "tab");
            View customView = tab.getCustomView();
            Objects.requireNonNull(customView);
            ((BoldSelectionTabItem) customView).unselect();
        }
    }
}
