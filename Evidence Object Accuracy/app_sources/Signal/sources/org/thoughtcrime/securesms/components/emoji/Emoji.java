package org.thoughtcrime.securesms.components.emoji;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* loaded from: classes4.dex */
public class Emoji {
    private final List<String> rawVariations;
    private final List<String> variations;

    public Emoji(String... strArr) {
        this(Arrays.asList(strArr), Collections.emptyList());
    }

    public Emoji(List<String> list) {
        this(list, Collections.emptyList());
    }

    public Emoji(List<String> list, List<String> list2) {
        this.variations = list;
        this.rawVariations = list2;
    }

    public String getValue() {
        return this.variations.get(0);
    }

    public List<String> getVariations() {
        return this.variations;
    }

    public boolean hasMultipleVariations() {
        return this.variations.size() > 1;
    }

    public String getRawVariation(int i) {
        List<String> list = this.rawVariations;
        if (list == null || i < 0 || i >= list.size()) {
            return null;
        }
        return this.rawVariations.get(i);
    }
}
