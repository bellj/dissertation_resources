package org.thoughtcrime.securesms.components.webrtc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.VibrateUtil;

/* compiled from: WifiToCellularPopupWindow.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u0000 \u00072\u00020\u0001:\u0001\u0007B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/WifiToCellularPopupWindow;", "Landroid/widget/PopupWindow;", "parent", "Landroid/view/ViewGroup;", "(Landroid/view/ViewGroup;)V", "show", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class WifiToCellularPopupWindow extends PopupWindow {
    public static final Companion Companion = new Companion(null);
    private static final long DISPLAY_DURATION_MS = TimeUnit.SECONDS.toMillis(4);
    private static final int VIBRATE_DURATION_MS;
    private final ViewGroup parent;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public WifiToCellularPopupWindow(ViewGroup viewGroup) {
        super(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wifi_to_cellular_popup, viewGroup, false), -1, -2);
        Intrinsics.checkNotNullParameter(viewGroup, "parent");
        this.parent = viewGroup;
        setAnimationStyle(R.style.PopupAnimation);
    }

    public final void show() {
        showAtLocation(this.parent, 8388659, 0, 0);
        VibrateUtil.vibrate(this.parent.getContext(), 50);
        View contentView = getContentView();
        Intrinsics.checkNotNullExpressionValue(contentView, "contentView");
        contentView.postDelayed(new Runnable(this) { // from class: org.thoughtcrime.securesms.components.webrtc.WifiToCellularPopupWindow$show$$inlined$postDelayed$1
            final /* synthetic */ WifiToCellularPopupWindow this$0;

            {
                this.this$0 = r1;
            }

            @Override // java.lang.Runnable
            public final void run() {
                this.this$0.dismiss();
            }
        }, DISPLAY_DURATION_MS);
    }

    /* compiled from: WifiToCellularPopupWindow.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006XT¢\u0006\u0002\n\u0000¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/webrtc/WifiToCellularPopupWindow$Companion;", "", "()V", "DISPLAY_DURATION_MS", "", "VIBRATE_DURATION_MS", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}
