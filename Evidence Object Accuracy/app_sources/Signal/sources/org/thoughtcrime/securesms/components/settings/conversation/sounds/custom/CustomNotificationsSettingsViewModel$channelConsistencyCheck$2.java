package org.thoughtcrime.securesms.components.settings.conversation.sounds.custom;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.notifications.NotificationChannels;

/* compiled from: CustomNotificationsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomNotificationsSettingsViewModel$channelConsistencyCheck$2 extends Lambda implements Function0<Unit> {
    final /* synthetic */ CustomNotificationsSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public CustomNotificationsSettingsViewModel$channelConsistencyCheck$2(CustomNotificationsSettingsViewModel customNotificationsSettingsViewModel) {
        super(0);
        this.this$0 = customNotificationsSettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsViewModel$channelConsistencyCheck$2$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return CustomNotificationsSettingsViewModel$channelConsistencyCheck$2.m1246invoke$lambda0((CustomNotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: invoke$lambda-0 */
    public static final CustomNotificationsSettingsState m1246invoke$lambda0(CustomNotificationsSettingsState customNotificationsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(customNotificationsSettingsState, "it");
        return customNotificationsSettingsState.copy((r22 & 1) != 0 ? customNotificationsSettingsState.isInitialLoadComplete : true, (r22 & 2) != 0 ? customNotificationsSettingsState.recipient : null, (r22 & 4) != 0 ? customNotificationsSettingsState.hasCustomNotifications : false, (r22 & 8) != 0 ? customNotificationsSettingsState.controlsEnabled : !NotificationChannels.supported() || customNotificationsSettingsState.getHasCustomNotifications(), (r22 & 16) != 0 ? customNotificationsSettingsState.messageVibrateState : null, (r22 & 32) != 0 ? customNotificationsSettingsState.messageVibrateEnabled : false, (r22 & 64) != 0 ? customNotificationsSettingsState.messageSound : null, (r22 & 128) != 0 ? customNotificationsSettingsState.callVibrateState : null, (r22 & 256) != 0 ? customNotificationsSettingsState.callSound : null, (r22 & 512) != 0 ? customNotificationsSettingsState.showCallingOptions : false);
    }
}
