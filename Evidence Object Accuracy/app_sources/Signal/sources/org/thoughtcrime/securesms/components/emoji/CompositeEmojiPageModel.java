package org.thoughtcrime.securesms.components.emoji;

import android.net.Uri;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class CompositeEmojiPageModel implements EmojiPageModel {
    private final int iconAttr;
    private final List<EmojiPageModel> models;

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public Uri getSpriteUri() {
        return null;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public boolean isDynamic() {
        return false;
    }

    public CompositeEmojiPageModel(int i, List<EmojiPageModel> list) {
        this.iconAttr = i;
        this.models = list;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public String getKey() {
        return Util.hasItems(this.models) ? this.models.get(0).getKey() : "";
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public int getIconAttr() {
        return this.iconAttr;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<String> getEmoji() {
        LinkedList linkedList = new LinkedList();
        for (EmojiPageModel emojiPageModel : this.models) {
            linkedList.addAll(emojiPageModel.getEmoji());
        }
        return linkedList;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<Emoji> getDisplayEmoji() {
        LinkedList linkedList = new LinkedList();
        for (EmojiPageModel emojiPageModel : this.models) {
            linkedList.addAll(emojiPageModel.getDisplayEmoji());
        }
        return linkedList;
    }
}
