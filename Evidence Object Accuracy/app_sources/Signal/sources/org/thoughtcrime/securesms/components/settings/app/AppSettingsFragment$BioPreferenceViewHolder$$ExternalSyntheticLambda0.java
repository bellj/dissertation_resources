package org.thoughtcrime.securesms.components.settings.app;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AppSettingsFragment$BioPreferenceViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AppSettingsFragment.BioPreference f$0;

    public /* synthetic */ AppSettingsFragment$BioPreferenceViewHolder$$ExternalSyntheticLambda0(AppSettingsFragment.BioPreference bioPreference) {
        this.f$0 = bioPreference;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AppSettingsFragment.BioPreferenceViewHolder.m549bind$lambda0(this.f$0, view);
    }
}
