package org.thoughtcrime.securesms.components.settings.app.privacy;

import android.content.DialogInterface;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PrivacySettingsFragment$getConfiguration$1$7$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ PrivacySettingsFragment f$0;
    public final /* synthetic */ MaterialAlertDialogBuilder f$1;

    public /* synthetic */ PrivacySettingsFragment$getConfiguration$1$7$$ExternalSyntheticLambda0(PrivacySettingsFragment privacySettingsFragment, MaterialAlertDialogBuilder materialAlertDialogBuilder) {
        this.f$0 = privacySettingsFragment;
        this.f$1 = materialAlertDialogBuilder;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        PrivacySettingsFragment$getConfiguration$1.AnonymousClass7.m829invoke$lambda1$lambda0(this.f$0, this.f$1, dialogInterface, i);
    }
}
