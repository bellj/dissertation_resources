package org.thoughtcrime.securesms.components.settings.app.chats.sms;

import android.content.Intent;
import android.os.Build;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.SmsUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: SmsSettingsFragment.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\"\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000f2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012H\u0016J\b\u0010\u0013\u001a\u00020\u0006H\u0016J\b\u0010\u0014\u001a\u00020\u0006H\u0003R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/sms/SmsSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/chats/sms/SmsSettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/chats/sms/SmsSettingsState;", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onResume", "startDefaultAppSelectionIntent", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SmsSettingsFragment extends DSLSettingsFragment {
    private SmsSettingsViewModel viewModel;

    public SmsSettingsFragment() {
        super(R.string.preferences__sms_mms, 0, 0, null, 14, null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        SmsSettingsViewModel smsSettingsViewModel = this.viewModel;
        if (smsSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            smsSettingsViewModel = null;
        }
        smsSettingsViewModel.checkSmsEnabled();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ViewModel viewModel = new ViewModelProvider(this).get(SmsSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this)[…ngsViewModel::class.java]");
        SmsSettingsViewModel smsSettingsViewModel = (SmsSettingsViewModel) viewModel;
        this.viewModel = smsSettingsViewModel;
        if (smsSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            smsSettingsViewModel = null;
        }
        smsSettingsViewModel.getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ SmsSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SmsSettingsFragment.$r8$lambda$p8HPa5TRadl7wjUqU6O5BN08HZo(DSLSettingsAdapter.this, this.f$1, (SmsSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m640bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, SmsSettingsFragment smsSettingsFragment, SmsSettingsState smsSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(smsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(smsSettingsState, "it");
        dSLSettingsAdapter.submitList(smsSettingsFragment.getConfiguration(smsSettingsState).toMappingModelList());
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        SignalStore.settings().setDefaultSms(Util.isDefaultSmsProvider(requireContext()));
    }

    private final DSLConfiguration getConfiguration(SmsSettingsState smsSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(smsSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsFragment$getConfiguration$1
            final /* synthetic */ SmsSettingsState $state;
            final /* synthetic */ SmsSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.SmsSettingsFragment__use_as_default_sms_app, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from2 = companion.from(this.$state.getUseAsDefaultSmsApp() ? R.string.arrays__enabled : R.string.arrays__disabled, new DSLSettingsText.Modifier[0]);
                final SmsSettingsState smsSettingsState2 = this.$state;
                final SmsSettingsFragment smsSettingsFragment = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : from2, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        if (smsSettingsState2.getUseAsDefaultSmsApp()) {
                            SmsSettingsFragment.access$startDefaultAppSelectionIntent(smsSettingsFragment);
                            return;
                        }
                        SmsSettingsFragment smsSettingsFragment2 = smsSettingsFragment;
                        smsSettingsFragment2.startActivityForResult(SmsUtil.getSmsRoleIntent(smsSettingsFragment2.requireContext()), 1234);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from3 = companion.from(R.string.preferences__sms_delivery_reports, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from4 = companion.from(R.string.preferences__request_a_delivery_report_for_each_sms_message_you_send, new DSLSettingsText.Modifier[0]);
                boolean smsDeliveryReportsEnabled = this.$state.getSmsDeliveryReportsEnabled();
                final SmsSettingsFragment smsSettingsFragment2 = this.this$0;
                final SmsSettingsState smsSettingsState3 = this.$state;
                dSLConfiguration.switchPref(from3, (r16 & 2) != 0 ? null : from4, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, smsDeliveryReportsEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SmsSettingsViewModel access$getViewModel$p = SmsSettingsFragment.access$getViewModel$p(smsSettingsFragment2);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setSmsDeliveryReportsEnabled(!smsSettingsState3.getSmsDeliveryReportsEnabled());
                    }
                });
                DSLSettingsText from5 = companion.from(R.string.preferences__support_wifi_calling, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from6 = companion.from(R.string.preferences__enable_if_your_device_supports_sms_mms_delivery_over_wifi, new DSLSettingsText.Modifier[0]);
                boolean wifiCallingCompatibilityEnabled = this.$state.getWifiCallingCompatibilityEnabled();
                final SmsSettingsFragment smsSettingsFragment3 = this.this$0;
                final SmsSettingsState smsSettingsState4 = this.$state;
                dSLConfiguration.switchPref(from5, (r16 & 2) != 0 ? null : from6, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, wifiCallingCompatibilityEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SmsSettingsViewModel access$getViewModel$p = SmsSettingsFragment.access$getViewModel$p(smsSettingsFragment3);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setWifiCallingCompatibilityEnabled(!smsSettingsState4.getWifiCallingCompatibilityEnabled());
                    }
                });
                if (Build.VERSION.SDK_INT < 21) {
                    DSLSettingsText from7 = companion.from(R.string.preferences__advanced_mms_access_point_names, new DSLSettingsText.Modifier[0]);
                    final SmsSettingsFragment smsSettingsFragment4 = this.this$0;
                    dSLConfiguration.clickPref(from7, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsFragment$getConfiguration$1.4
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            NavController findNavController = Navigation.findNavController(smsSettingsFragment4.requireView());
                            Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                            SafeNavigation.safeNavigate(findNavController, (int) R.id.action_smsSettingsFragment_to_mmsPreferencesFragment);
                        }
                    }, (r18 & 64) != 0 ? null : null);
                }
            }
        });
    }

    public final void startDefaultAppSelectionIntent() {
        Intent intent;
        int i = Build.VERSION.SDK_INT;
        if (i < 23) {
            intent = new Intent("android.settings.WIRELESS_SETTINGS");
        } else if (i < 24) {
            intent = new Intent("android.settings.SETTINGS");
        } else {
            intent = new Intent("android.settings.MANAGE_DEFAULT_APPS_SETTINGS");
        }
        startActivityForResult(intent, 1234);
    }
}
