package org.thoughtcrime.securesms.components.settings.app.data;

import android.app.Application;
import android.content.Context;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.MediaDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;

/* compiled from: DataAndStorageSettingsRepository.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/data/DataAndStorageSettingsRepository;", "", "()V", "context", "Landroid/content/Context;", "getTotalStorageUse", "", "consumer", "Lkotlin/Function1;", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DataAndStorageSettingsRepository {
    private final Context context;

    public DataAndStorageSettingsRepository() {
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        this.context = application;
    }

    public final void getTotalStorageUse(Function1<? super Long, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.data.DataAndStorageSettingsRepository$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                DataAndStorageSettingsRepository.m647getTotalStorageUse$lambda0(Function1.this);
            }
        });
    }

    /* renamed from: getTotalStorageUse$lambda-0 */
    public static final void m647getTotalStorageUse$lambda0(Function1 function1) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        MediaDatabase.StorageBreakdown storageBreakdown = SignalDatabase.Companion.media().getStorageBreakdown();
        function1.invoke(Long.valueOf(CollectionsKt___CollectionsKt.sumOfLong(CollectionsKt__CollectionsKt.listOf((Object[]) new Long[]{Long.valueOf(storageBreakdown.getAudioSize()), Long.valueOf(storageBreakdown.getDocumentSize()), Long.valueOf(storageBreakdown.getPhotoSize()), Long.valueOf(storageBreakdown.getVideoSize())}))));
    }
}
