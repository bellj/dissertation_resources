package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import android.view.ViewParent;
import androidx.core.view.ViewCompat;
import j$.util.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.avatar.view.AvatarView;
import org.thoughtcrime.securesms.badges.BadgeImageView;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.AvatarPreference;
import org.thoughtcrime.securesms.contacts.avatars.FallbackContactPhoto;
import org.thoughtcrime.securesms.contacts.avatars.FallbackPhoto;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: AvatarPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/AvatarPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "AvatarPreferenceFallbackPhotoProvider", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AvatarPreference {
    public static final AvatarPreference INSTANCE = new AvatarPreference();

    private AvatarPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.AvatarPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new AvatarPreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_avatar_preference_item));
    }

    /* compiled from: AvatarPreference.kt */
    @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0002\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0000H\u0016J\u0010\u0010\u0017\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0000H\u0016R\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/AvatarPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "storyViewState", "Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "onAvatarClick", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/avatar/view/AvatarView;", "", "onBadgeClick", "Lorg/thoughtcrime/securesms/badges/models/Badge;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Lorg/thoughtcrime/securesms/database/model/StoryViewState;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "getOnAvatarClick", "()Lkotlin/jvm/functions/Function1;", "getOnBadgeClick", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getStoryViewState", "()Lorg/thoughtcrime/securesms/database/model/StoryViewState;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Function1<AvatarView, Unit> onAvatarClick;
        private final Function1<Badge, Unit> onBadgeClick;
        private final Recipient recipient;
        private final StoryViewState storyViewState;

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final StoryViewState getStoryViewState() {
            return this.storyViewState;
        }

        public final Function1<AvatarView, Unit> getOnAvatarClick() {
            return this.onAvatarClick;
        }

        public final Function1<Badge, Unit> getOnBadgeClick() {
            return this.onBadgeClick;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r12v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.avatar.view.AvatarView, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r13v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.badges.models.Badge, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(Recipient recipient, StoryViewState storyViewState, Function1<? super AvatarView, Unit> function1, Function1<? super Badge, Unit> function12) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            Intrinsics.checkNotNullParameter(storyViewState, "storyViewState");
            Intrinsics.checkNotNullParameter(function1, "onAvatarClick");
            Intrinsics.checkNotNullParameter(function12, "onBadgeClick");
            this.recipient = recipient;
            this.storyViewState = storyViewState;
            this.onAvatarClick = function1;
            this.onBadgeClick = function12;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.recipient, model.recipient);
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && this.recipient.hasSameContent(model.recipient);
        }
    }

    /* compiled from: AvatarPreference.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/AvatarPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/AvatarPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "avatar", "Lorg/thoughtcrime/securesms/avatar/view/AvatarView;", "badge", "Lorg/thoughtcrime/securesms/badges/BadgeImageView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final AvatarView avatar;
        private final BadgeImageView badge;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.bio_preference_avatar);
            AvatarView avatarView = (AvatarView) findViewById;
            avatarView.setFallbackPhotoProvider(new AvatarPreferenceFallbackPhotoProvider());
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById<Av…ackPhotoProvider())\n    }");
            this.avatar = avatarView;
            View findViewById2 = view.findViewById(R.id.bio_preference_badge);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.id.bio_preference_badge)");
            this.badge = (BadgeImageView) findViewById2;
            ViewParent parent = avatarView.getParent();
            if (parent != null) {
                ViewCompat.setTransitionName((View) parent, "avatar");
                return;
            }
            throw new NullPointerException("null cannot be cast to non-null type android.view.View");
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            if (model.getRecipient().isSelf()) {
                this.badge.setBadge(null);
                this.badge.setOnClickListener(null);
            } else {
                this.badge.setBadgeFromRecipient(model.getRecipient());
                this.badge.setOnClickListener(new AvatarPreference$ViewHolder$$ExternalSyntheticLambda0(model));
            }
            this.avatar.setStoryRingFromState(model.getStoryViewState());
            this.avatar.displayChatAvatar(model.getRecipient());
            this.avatar.disableQuickContact();
            this.avatar.setOnClickListener(new AvatarPreference$ViewHolder$$ExternalSyntheticLambda1(model, this));
        }

        /* renamed from: bind$lambda-1 */
        public static final void m1193bind$lambda1(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            List<Badge> badges = model.getRecipient().getBadges();
            Intrinsics.checkNotNullExpressionValue(badges, "model.recipient.badges");
            Badge badge = (Badge) CollectionsKt___CollectionsKt.firstOrNull((List) ((List<? extends Object>) badges));
            if (badge != null) {
                model.getOnBadgeClick().invoke(badge);
            }
        }

        /* renamed from: bind$lambda-2 */
        public static final void m1194bind$lambda2(Model model, ViewHolder viewHolder, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            Intrinsics.checkNotNullParameter(viewHolder, "this$0");
            model.getOnAvatarClick().invoke(viewHolder.avatar);
        }
    }

    /* compiled from: AvatarPreference.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0016¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/AvatarPreference$AvatarPreferenceFallbackPhotoProvider;", "Lorg/thoughtcrime/securesms/recipients/Recipient$FallbackPhotoProvider;", "()V", "getPhotoForGroup", "Lorg/thoughtcrime/securesms/contacts/avatars/FallbackContactPhoto;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class AvatarPreferenceFallbackPhotoProvider extends Recipient.FallbackPhotoProvider {
        @Override // org.thoughtcrime.securesms.recipients.Recipient.FallbackPhotoProvider
        public FallbackContactPhoto getPhotoForGroup() {
            return new FallbackPhoto(R.drawable.ic_group_outline_40, ViewUtil.dpToPx(8));
        }
    }
}
