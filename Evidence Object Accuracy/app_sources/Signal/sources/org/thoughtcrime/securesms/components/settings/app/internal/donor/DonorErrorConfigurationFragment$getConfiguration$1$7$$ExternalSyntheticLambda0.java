package org.thoughtcrime.securesms.components.settings.app.internal.donor;

import io.reactivex.rxjava3.functions.Action;
import org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DonorErrorConfigurationFragment$getConfiguration$1$7$$ExternalSyntheticLambda0 implements Action {
    public final /* synthetic */ DonorErrorConfigurationFragment f$0;

    public /* synthetic */ DonorErrorConfigurationFragment$getConfiguration$1$7$$ExternalSyntheticLambda0(DonorErrorConfigurationFragment donorErrorConfigurationFragment) {
        this.f$0 = donorErrorConfigurationFragment;
    }

    @Override // io.reactivex.rxjava3.functions.Action
    public final void run() {
        DonorErrorConfigurationFragment$getConfiguration$1.AnonymousClass7.m674invoke$lambda0(this.f$0);
    }
}
