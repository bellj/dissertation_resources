package org.thoughtcrime.securesms.components.settings.app.subscription;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Currency;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.comparisons.ComparisonsKt__ComparisonsKt;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.badges.Badges;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowRepository$$ExternalSyntheticLambda2;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.PlatformCurrencyUtil;
import org.whispersystems.signalservice.api.profiles.SignalServiceProfile;
import org.whispersystems.signalservice.api.services.DonationsService;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* compiled from: SubscriptionsRepository.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006J\u0012\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u0006R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/SubscriptionsRepository;", "", "donationsService", "Lorg/whispersystems/signalservice/api/services/DonationsService;", "(Lorg/whispersystems/signalservice/api/services/DonationsService;)V", "getActiveSubscription", "Lio/reactivex/rxjava3/core/Single;", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "getSubscriptions", "", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscriptionsRepository {
    private final DonationsService donationsService;

    public SubscriptionsRepository(DonationsService donationsService) {
        Intrinsics.checkNotNullParameter(donationsService, "donationsService");
        this.donationsService = donationsService;
    }

    /* JADX DEBUG: Type inference failed for r0v7. Raw type applied. Possible types: io.reactivex.rxjava3.core.Single<R>, java.lang.Object, io.reactivex.rxjava3.core.Single<org.whispersystems.signalservice.api.subscriptions.ActiveSubscription> */
    public final Single<ActiveSubscription> getActiveSubscription() {
        Subscriber subscriber = SignalStore.donationsValues().getSubscriber();
        if (subscriber != null) {
            Single flatMap = this.donationsService.getSubscription(subscriber.getSubscriberId()).subscribeOn(Schedulers.io()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2());
            Intrinsics.checkNotNullExpressionValue(flatMap, "{\n      donationsService…on>::flattenResult)\n    }");
            return flatMap;
        }
        Single<ActiveSubscription> just = Single.just(ActiveSubscription.EMPTY);
        Intrinsics.checkNotNullExpressionValue(just, "{\n      Single.just(Acti…Subscription.EMPTY)\n    }");
        return just;
    }

    public final Single<List<Subscription>> getSubscriptions() {
        Single<List<Subscription>> map = this.donationsService.getSubscriptionLevels(Locale.getDefault()).subscribeOn(Schedulers.io()).flatMap(new GiftFlowRepository$$ExternalSyntheticLambda2()).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return SubscriptionsRepository.m915getSubscriptions$lambda4((SubscriptionLevels) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "donationsService.getSubs…   it.level\n      }\n    }");
        return map;
    }

    /* renamed from: getSubscriptions$lambda-4 */
    public static final List m915getSubscriptions$lambda4(SubscriptionLevels subscriptionLevels) {
        Map<String, SubscriptionLevels.Level> levels = subscriptionLevels.getLevels();
        Intrinsics.checkNotNullExpressionValue(levels, "subscriptionLevels.levels");
        ArrayList arrayList = new ArrayList(levels.size());
        for (Map.Entry<String, SubscriptionLevels.Level> entry : levels.entrySet()) {
            String key = entry.getKey();
            SubscriptionLevels.Level value = entry.getValue();
            Intrinsics.checkNotNullExpressionValue(key, "code");
            String name = value.getName();
            Intrinsics.checkNotNullExpressionValue(name, "level.name");
            SignalServiceProfile.Badge badge = value.getBadge();
            Intrinsics.checkNotNullExpressionValue(badge, "level.badge");
            Badge fromServiceBadge = Badges.fromServiceBadge(badge);
            Map<String, BigDecimal> currencies = value.getCurrencies();
            Intrinsics.checkNotNullExpressionValue(currencies, "level.currencies");
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (Map.Entry<String, BigDecimal> entry2 : currencies.entrySet()) {
                if (PlatformCurrencyUtil.INSTANCE.getAvailableCurrencyCodes().contains(entry2.getKey())) {
                    linkedHashMap.put(entry2.getKey(), entry2.getValue());
                }
            }
            ArrayList arrayList2 = new ArrayList(linkedHashMap.size());
            for (Map.Entry entry3 : linkedHashMap.entrySet()) {
                arrayList2.add(new FiatMoney((BigDecimal) entry3.getValue(), Currency.getInstance((String) entry3.getKey())));
            }
            arrayList.add(new Subscription(key, name, fromServiceBadge, CollectionsKt___CollectionsKt.toSet(arrayList2), Integer.parseInt(key)));
        }
        return CollectionsKt___CollectionsKt.sortedWith(arrayList, new Comparator() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.SubscriptionsRepository$getSubscriptions$lambda-4$$inlined$sortedBy$1
            @Override // java.util.Comparator
            public final int compare(T t, T t2) {
                return ComparisonsKt__ComparisonsKt.compareValues(Integer.valueOf(((Subscription) t).getLevel()), Integer.valueOf(((Subscription) t2).getLevel()));
            }
        });
    }
}
