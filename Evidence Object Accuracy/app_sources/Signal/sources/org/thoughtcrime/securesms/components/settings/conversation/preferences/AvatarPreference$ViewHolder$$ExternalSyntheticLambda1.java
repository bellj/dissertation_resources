package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.AvatarPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AvatarPreference$ViewHolder$$ExternalSyntheticLambda1 implements View.OnClickListener {
    public final /* synthetic */ AvatarPreference.Model f$0;
    public final /* synthetic */ AvatarPreference.ViewHolder f$1;

    public /* synthetic */ AvatarPreference$ViewHolder$$ExternalSyntheticLambda1(AvatarPreference.Model model, AvatarPreference.ViewHolder viewHolder) {
        this.f$0 = model;
        this.f$1 = viewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AvatarPreference.ViewHolder.m1194bind$lambda2(this.f$0, this.f$1, view);
    }
}
