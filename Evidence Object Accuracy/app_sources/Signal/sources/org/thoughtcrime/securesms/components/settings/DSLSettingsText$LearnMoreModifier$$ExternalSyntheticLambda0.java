package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class DSLSettingsText$LearnMoreModifier$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ DSLSettingsText.LearnMoreModifier f$0;

    public /* synthetic */ DSLSettingsText$LearnMoreModifier$$ExternalSyntheticLambda0(DSLSettingsText.LearnMoreModifier learnMoreModifier) {
        this.f$0 = learnMoreModifier;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        DSLSettingsText.LearnMoreModifier.m525modify$lambda0(this.f$0, view);
    }
}
