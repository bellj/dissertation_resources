package org.thoughtcrime.securesms.components.settings;

import java.util.Arrays;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0013\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B_\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00100\u000f\u0012\b\b\u0002\u0010\u0011\u001a\u00020\u0007¢\u0006\u0002\u0010\u0012J\u0010\u0010!\u001a\u00020\u00072\u0006\u0010\"\u001a\u00020\u0000H\u0016R\u0011\u0010\u0011\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0014R\u0019\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\n\n\u0002\u0010\u001b\u001a\u0004\b\u0019\u0010\u001aR\u001d\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00100\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0016¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/RadioListPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "dialogTitle", "listItems", "", "", "selected", "", "onSelected", "Lkotlin/Function1;", "", "confirmAction", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZLorg/thoughtcrime/securesms/components/settings/DSLSettingsText;[Ljava/lang/String;ILkotlin/jvm/functions/Function1;Z)V", "getConfirmAction", "()Z", "getDialogTitle", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "getListItems", "()[Ljava/lang/String;", "[Ljava/lang/String;", "getOnSelected", "()Lkotlin/jvm/functions/Function1;", "getSelected", "()I", "getTitle", "areContentsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class RadioListPreference extends PreferenceModel<RadioListPreference> {
    private final boolean confirmAction;
    private final DSLSettingsText dialogTitle;
    private final DSLSettingsIcon icon;
    private final boolean isEnabled;
    private final String[] listItems;
    private final Function1<Integer, Unit> onSelected;
    private final int selected;
    private final DSLSettingsText title;

    public /* synthetic */ RadioListPreference(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, DSLSettingsText dSLSettingsText2, String[] strArr, int i, Function1 function1, boolean z2, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(dSLSettingsText, (i2 & 2) != 0 ? null : dSLSettingsIcon, z, (i2 & 8) != 0 ? dSLSettingsText : dSLSettingsText2, strArr, i, function1, (i2 & 128) != 0 ? false : z2);
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsIcon getIcon() {
        return this.icon;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public final DSLSettingsText getDialogTitle() {
        return this.dialogTitle;
    }

    public final String[] getListItems() {
        return this.listItems;
    }

    public final int getSelected() {
        return this.selected;
    }

    public final Function1<Integer, Unit> getOnSelected() {
        return this.onSelected;
    }

    public final boolean getConfirmAction() {
        return this.confirmAction;
    }

    /* JADX DEBUG: Multi-variable search result rejected for r20v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.Integer, kotlin.Unit> */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public RadioListPreference(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, boolean z, DSLSettingsText dSLSettingsText2, String[] strArr, int i, Function1<? super Integer, Unit> function1, boolean z2) {
        super(null, null, null, null, false, 31, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(dSLSettingsText2, "dialogTitle");
        Intrinsics.checkNotNullParameter(strArr, "listItems");
        Intrinsics.checkNotNullParameter(function1, "onSelected");
        this.title = dSLSettingsText;
        this.icon = dSLSettingsIcon;
        this.isEnabled = z;
        this.dialogTitle = dSLSettingsText2;
        this.listItems = strArr;
        this.selected = i;
        this.onSelected = function1;
        this.confirmAction = z2;
    }

    public boolean areContentsTheSame(RadioListPreference radioListPreference) {
        Intrinsics.checkNotNullParameter(radioListPreference, "newItem");
        return super.areContentsTheSame(radioListPreference) && Arrays.equals(this.listItems, radioListPreference.listItems) && this.selected == radioListPreference.selected;
    }
}
