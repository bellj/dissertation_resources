package org.thoughtcrime.securesms.components.settings.app.internal;

import android.content.Context;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.attachments.DatabaseAttachment;
import org.thoughtcrime.securesms.database.MessageDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DatabaseProtoExtensionsKt;
import org.thoughtcrime.securesms.database.model.databaseprotos.BodyRangeList;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.emoji.EmojiFiles;
import org.thoughtcrime.securesms.jobs.AttachmentDownloadJob;
import org.thoughtcrime.securesms.jobs.CreateReleaseChannelJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.v2.ConversationId;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.releasechannel.ReleaseChannel;

/* compiled from: InternalSettingsRepository.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0006\u001a\u00020\u0007J\u001c\u0010\b\u001a\u00020\u00072\u0014\u0010\t\u001a\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u000b\u0012\u0004\u0012\u00020\u00070\nR\u0016\u0010\u0002\u001a\n \u0005*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "kotlin.jvm.PlatformType", "addSampleReleaseNote", "", "getEmojiVersionInfo", "consumer", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternalSettingsRepository {
    private final Context context;

    public InternalSettingsRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context.getApplicationContext();
    }

    public final void getEmojiVersionInfo(Function1<? super EmojiFiles.Version, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ InternalSettingsRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                InternalSettingsRepository.m667getEmojiVersionInfo$lambda0(Function1.this, this.f$1);
            }
        });
    }

    /* renamed from: getEmojiVersionInfo$lambda-0 */
    public static final void m667getEmojiVersionInfo$lambda0(Function1 function1, InternalSettingsRepository internalSettingsRepository) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(internalSettingsRepository, "this$0");
        EmojiFiles.Version.Companion companion = EmojiFiles.Version.Companion;
        Context context = internalSettingsRepository.context;
        Intrinsics.checkNotNullExpressionValue(context, "context");
        function1.invoke(EmojiFiles.Version.Companion.readVersion$default(companion, context, false, 2, null));
    }

    public final void addSampleReleaseNote() {
        SignalExecutors.UNBOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsRepository$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                InternalSettingsRepository.m666addSampleReleaseNote$lambda2(InternalSettingsRepository.this);
            }
        });
    }

    /* renamed from: addSampleReleaseNote$lambda-2 */
    public static final void m666addSampleReleaseNote$lambda2(InternalSettingsRepository internalSettingsRepository) {
        Intrinsics.checkNotNullParameter(internalSettingsRepository, "this$0");
        ApplicationDependencies.getJobManager().runSynchronously(CreateReleaseChannelJob.Companion.create(), 5000);
        BodyRangeList.Builder newBuilder = BodyRangeList.newBuilder();
        Intrinsics.checkNotNullExpressionValue(newBuilder, "newBuilder()");
        BodyRangeList.Builder addStyle = DatabaseProtoExtensionsKt.addStyle(newBuilder, BodyRangeList.BodyRange.Style.BOLD, 0, 18);
        RecipientId releaseChannelRecipientId = SignalStore.releaseChannelValues().getReleaseChannelRecipientId();
        Intrinsics.checkNotNull(releaseChannelRecipientId);
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        long orCreateThreadIdFor = companion.threads().getOrCreateThreadIdFor(Recipient.resolved(releaseChannelRecipientId));
        MessageDatabase.InsertResult insertReleaseChannelMessage$default = ReleaseChannel.insertReleaseChannelMessage$default(ReleaseChannel.INSTANCE, releaseChannelRecipientId, "Release Note Title\n\nRelease note body. Aren't I awesome?", orCreateThreadIdFor, "/static/release-notes/signal.png", 1800, 720, null, addStyle.build(), null, 320, null);
        companion.sms().insertBoostRequestMessage(releaseChannelRecipientId, orCreateThreadIdFor);
        if (insertReleaseChannelMessage$default != null) {
            List<DatabaseAttachment> attachmentsForMessage = companion.attachments().getAttachmentsForMessage(insertReleaseChannelMessage$default.getMessageId());
            Intrinsics.checkNotNullExpressionValue(attachmentsForMessage, "SignalDatabase.attachmen…e(insertResult.messageId)");
            for (DatabaseAttachment databaseAttachment : attachmentsForMessage) {
                ApplicationDependencies.getJobManager().add(new AttachmentDownloadJob(insertReleaseChannelMessage$default.getMessageId(), databaseAttachment.getAttachmentId(), false));
            }
            ApplicationDependencies.getMessageNotifier().updateNotification(internalSettingsRepository.context, ConversationId.Companion.forConversation(insertReleaseChannelMessage$default.getThreadId()));
        }
    }
}
