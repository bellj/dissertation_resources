package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: DonationErrorNotifications.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "Landroid/app/PendingIntent;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationErrorNotifications$NotificationCallback$createAction$1 extends Lambda implements Function0<PendingIntent> {
    final /* synthetic */ Intent $actionIntent;
    final /* synthetic */ Context $context;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DonationErrorNotifications$NotificationCallback$createAction$1(Context context, Intent intent) {
        super(0);
        this.$context = context;
        this.$actionIntent = intent;
    }

    @Override // kotlin.jvm.functions.Function0
    public final PendingIntent invoke() {
        PendingIntent activity = PendingIntent.getActivity(this.$context, 0, this.$actionIntent, Build.VERSION.SDK_INT >= 23 ? 1073741824 : 0);
        Intrinsics.checkNotNullExpressionValue(activity, "getActivity(\n           …E_SHOT else 0\n          )");
        return activity;
    }
}
