package org.thoughtcrime.securesms.components.settings.app.notifications.manual;

import j$.time.LocalDateTime;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;

/* compiled from: NotificationProfileSelectionState.kt */
@Metadata(bv = {}, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\b\b\u0018\u00002\u00020\u0001B)\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\b\b\u0002\u0010\n\u001a\u00020\u0005\u0012\u0006\u0010\u000b\u001a\u00020\u0007¢\u0006\u0004\b\u001d\u0010\u001eJ\u000f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00030\u0002HÆ\u0003J\t\u0010\u0006\u001a\u00020\u0005HÆ\u0003J\t\u0010\b\u001a\u00020\u0007HÆ\u0003J-\u0010\f\u001a\u00020\u00002\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00022\b\b\u0002\u0010\n\u001a\u00020\u00052\b\b\u0002\u0010\u000b\u001a\u00020\u0007HÆ\u0001J\t\u0010\u000e\u001a\u00020\rHÖ\u0001J\t\u0010\u0010\u001a\u00020\u000fHÖ\u0001J\u0013\u0010\u0013\u001a\u00020\u00122\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003R\u001d\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00030\u00028\u0006¢\u0006\f\n\u0004\b\t\u0010\u0014\u001a\u0004\b\u0015\u0010\u0016R\u0017\u0010\n\u001a\u00020\u00058\u0006¢\u0006\f\n\u0004\b\n\u0010\u0017\u001a\u0004\b\u0018\u0010\u0019R\u0017\u0010\u000b\u001a\u00020\u00078\u0006¢\u0006\f\n\u0004\b\u000b\u0010\u001a\u001a\u0004\b\u001b\u0010\u001c¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionState;", "", "", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "component1", "", "component2", "j$/time/LocalDateTime", "component3", "notificationProfiles", "expandedId", "timeSlotB", "copy", "", "toString", "", "hashCode", "other", "", "equals", "Ljava/util/List;", "getNotificationProfiles", "()Ljava/util/List;", "J", "getExpandedId", "()J", "Lj$/time/LocalDateTime;", "getTimeSlotB", "()Lj$/time/LocalDateTime;", "<init>", "(Ljava/util/List;JLj$/time/LocalDateTime;)V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class NotificationProfileSelectionState {
    private final long expandedId;
    private final List<NotificationProfile> notificationProfiles;
    private final LocalDateTime timeSlotB;

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ NotificationProfileSelectionState copy$default(NotificationProfileSelectionState notificationProfileSelectionState, List list, long j, LocalDateTime localDateTime, int i, Object obj) {
        if ((i & 1) != 0) {
            list = notificationProfileSelectionState.notificationProfiles;
        }
        if ((i & 2) != 0) {
            j = notificationProfileSelectionState.expandedId;
        }
        if ((i & 4) != 0) {
            localDateTime = notificationProfileSelectionState.timeSlotB;
        }
        return notificationProfileSelectionState.copy(list, j, localDateTime);
    }

    public final List<NotificationProfile> component1() {
        return this.notificationProfiles;
    }

    public final long component2() {
        return this.expandedId;
    }

    public final LocalDateTime component3() {
        return this.timeSlotB;
    }

    public final NotificationProfileSelectionState copy(List<NotificationProfile> list, long j, LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(list, "notificationProfiles");
        Intrinsics.checkNotNullParameter(localDateTime, "timeSlotB");
        return new NotificationProfileSelectionState(list, j, localDateTime);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof NotificationProfileSelectionState)) {
            return false;
        }
        NotificationProfileSelectionState notificationProfileSelectionState = (NotificationProfileSelectionState) obj;
        return Intrinsics.areEqual(this.notificationProfiles, notificationProfileSelectionState.notificationProfiles) && this.expandedId == notificationProfileSelectionState.expandedId && Intrinsics.areEqual(this.timeSlotB, notificationProfileSelectionState.timeSlotB);
    }

    public int hashCode() {
        return (((this.notificationProfiles.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.expandedId)) * 31) + this.timeSlotB.hashCode();
    }

    public String toString() {
        return "NotificationProfileSelectionState(notificationProfiles=" + this.notificationProfiles + ", expandedId=" + this.expandedId + ", timeSlotB=" + this.timeSlotB + ')';
    }

    public NotificationProfileSelectionState(List<NotificationProfile> list, long j, LocalDateTime localDateTime) {
        Intrinsics.checkNotNullParameter(list, "notificationProfiles");
        Intrinsics.checkNotNullParameter(localDateTime, "timeSlotB");
        this.notificationProfiles = list;
        this.expandedId = j;
        this.timeSlotB = localDateTime;
    }

    public /* synthetic */ NotificationProfileSelectionState(List list, long j, LocalDateTime localDateTime, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? CollectionsKt__CollectionsKt.emptyList() : list, (i & 2) != 0 ? -1 : j, localDateTime);
    }

    public final List<NotificationProfile> getNotificationProfiles() {
        return this.notificationProfiles;
    }

    public final long getExpandedId() {
        return this.expandedId;
    }

    public final LocalDateTime getTimeSlotB() {
        return this.timeSlotB;
    }
}
