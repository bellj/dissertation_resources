package org.thoughtcrime.securesms.components.settings.app.notifications.manual;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.util.BottomSheetUtil;

/* compiled from: NotificationProfileSelectionFragment.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsBottomSheetFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionState;", "navigateToSettings", "notificationProfile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileSelectionFragment extends DSLSettingsBottomSheetFragment {
    public static final Companion Companion = new Companion(null);
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(NotificationProfileSelectionViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, NotificationProfileSelectionFragment$viewModel$2.INSTANCE);

    @JvmStatic
    public static final void show(FragmentManager fragmentManager) {
        Companion.show(fragmentManager);
    }

    public NotificationProfileSelectionFragment() {
        super(0, null, 0.0f, 7, null);
    }

    public final NotificationProfileSelectionViewModel getViewModel() {
        return (NotificationProfileSelectionViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsBottomSheetFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        NotificationProfileSelection.INSTANCE.register(dSLSettingsAdapter);
        getRecyclerView().setItemAnimator(null);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ NotificationProfileSelectionFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                NotificationProfileSelectionFragment.$r8$lambda$am2by6IDv5R7To5M_2L7acc3N_Q(DSLSettingsAdapter.this, this.f$1, (NotificationProfileSelectionState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m713bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, NotificationProfileSelectionFragment notificationProfileSelectionFragment, NotificationProfileSelectionState notificationProfileSelectionState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(notificationProfileSelectionFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(notificationProfileSelectionState, "it");
        dSLSettingsAdapter.submitList(notificationProfileSelectionFragment.getConfiguration(notificationProfileSelectionState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(NotificationProfileSelectionState notificationProfileSelectionState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(notificationProfileSelectionState, NotificationProfiles.getActiveProfile$default(notificationProfileSelectionState.getNotificationProfiles(), 0, null, 6, null), this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1
            final /* synthetic */ NotificationProfile $activeProfile;
            final /* synthetic */ NotificationProfileSelectionState $state;
            final /* synthetic */ NotificationProfileSelectionFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.$activeProfile = r2;
                this.this$0 = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00b7: INVOKE  
                  (r21v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$Entry : 0x00b2: CONSTRUCTOR  (r7v7 org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$Entry A[REMOVE]) = 
                  (r14v0 'areEqual' boolean)
                  (r7v1 'dSLSettingsText' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (r6v2 'notificationProfile2' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile)
                  (wrap: boolean : ?: TERNARY(r11v0 boolean A[REMOVE]) = ((wrap: long : 0x0064: INVOKE  (r6v2 'notificationProfile2' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile) type: VIRTUAL call: org.thoughtcrime.securesms.notifications.profiles.NotificationProfile.getId():long) == (wrap: long : 0x0068: INVOKE  
                  (r5v0 'notificationProfileSelectionState2' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState.getExpandedId():long)) ? true : false)
                  (wrap: j$.time.LocalDateTime : 0x0074: INVOKE  (r12v1 j$.time.LocalDateTime A[REMOVE]) = 
                  (r5v0 'notificationProfileSelectionState2' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState.getTimeSlotB():j$.time.LocalDateTime)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1 : 0x007e: CONSTRUCTOR  (r13v0 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x007a: INVOKE  (r7v3 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2 : 0x0087: CONSTRUCTOR  (r15v1 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x0083: INVOKE  (r7v4 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3 : 0x0090: CONSTRUCTOR  (r10v1 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x008c: INVOKE  (r7v5 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5 : 0x00a2: CONSTRUCTOR  (r2v10 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5 A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5.<init>(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4 : 0x0099: CONSTRUCTOR  (r8v0 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x0095: INVOKE  (r7v6 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4.<init>(java.lang.Object):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection.Entry.<init>(boolean, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, boolean, j$.time.LocalDateTime, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.customPref(org.thoughtcrime.securesms.util.adapter.mapping.MappingModel):void in method: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:209)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x00b2: CONSTRUCTOR  (r7v7 org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$Entry A[REMOVE]) = 
                  (r14v0 'areEqual' boolean)
                  (r7v1 'dSLSettingsText' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (r6v2 'notificationProfile2' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile)
                  (wrap: boolean : ?: TERNARY(r11v0 boolean A[REMOVE]) = ((wrap: long : 0x0064: INVOKE  (r6v2 'notificationProfile2' org.thoughtcrime.securesms.notifications.profiles.NotificationProfile) type: VIRTUAL call: org.thoughtcrime.securesms.notifications.profiles.NotificationProfile.getId():long) == (wrap: long : 0x0068: INVOKE  
                  (r5v0 'notificationProfileSelectionState2' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState.getExpandedId():long)) ? true : false)
                  (wrap: j$.time.LocalDateTime : 0x0074: INVOKE  (r12v1 j$.time.LocalDateTime A[REMOVE]) = 
                  (r5v0 'notificationProfileSelectionState2' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState)
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState.getTimeSlotB():j$.time.LocalDateTime)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1 : 0x007e: CONSTRUCTOR  (r13v0 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x007a: INVOKE  (r7v3 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2 : 0x0087: CONSTRUCTOR  (r15v1 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x0083: INVOKE  (r7v4 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3 : 0x0090: CONSTRUCTOR  (r10v1 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x008c: INVOKE  (r7v5 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3.<init>(java.lang.Object):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5 : 0x00a2: CONSTRUCTOR  (r2v10 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5 A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5.<init>(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):void type: CONSTRUCTOR)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4 : 0x0099: CONSTRUCTOR  (r8v0 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x0095: INVOKE  (r7v6 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4.<init>(java.lang.Object):void type: CONSTRUCTOR)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection.Entry.<init>(boolean, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.notifications.profiles.NotificationProfile, boolean, j$.time.LocalDateTime, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function2, kotlin.jvm.functions.Function1, kotlin.jvm.functions.Function1):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 16 more
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x007e: CONSTRUCTOR  (r13v0 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1 A[REMOVE]) = 
                  (wrap: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel : 0x007a: INVOKE  (r7v3 org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel A[REMOVE]) = 
                  (r4v0 'notificationProfileSelectionFragment' org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment):org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel)
                 call: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1.<init>(java.lang.Object):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:708)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 22 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 28 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r21) {
                /*
                    r20 = this;
                    r0 = r20
                    r1 = r21
                    java.lang.String r2 = "$this$configure"
                    kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r1, r2)
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState r2 = r0.$state
                    java.util.List r2 = r2.getNotificationProfiles()
                    java.util.List r2 = kotlin.collections.CollectionsKt.sortedDescending(r2)
                    org.thoughtcrime.securesms.notifications.profiles.NotificationProfile r3 = r0.$activeProfile
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment r4 = r0.this$0
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionState r5 = r0.$state
                    java.util.Iterator r2 = r2.iterator()
                L_0x001d:
                    boolean r6 = r2.hasNext()
                    if (r6 == 0) goto L_0x00ca
                    java.lang.Object r6 = r2.next()
                    org.thoughtcrime.securesms.notifications.profiles.NotificationProfile r6 = (org.thoughtcrime.securesms.notifications.profiles.NotificationProfile) r6
                    boolean r14 = kotlin.jvm.internal.Intrinsics.areEqual(r6, r3)
                    boolean r7 = kotlin.jvm.internal.Intrinsics.areEqual(r6, r3)
                    r15 = 0
                    if (r7 == 0) goto L_0x0058
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r13 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles r7 = org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles.INSTANCE
                    android.content.Context r8 = r4.requireContext()
                    java.lang.String r9 = "requireContext()"
                    kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r8, r9)
                    r10 = 0
                    r12 = 4
                    r16 = 0
                    r9 = r6
                    r18 = r13
                    r13 = r16
                    java.lang.String r7 = org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles.getActiveProfileDescription$default(r7, r8, r9, r10, r12, r13)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r8 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r15]
                    r9 = r18
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r7 = r9.from(r7, r8)
                    goto L_0x0063
                L_0x0058:
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion r7 = org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion
                    r8 = 2131953298(0x7f130692, float:1.9543063E38)
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] r9 = new org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier[r15]
                    org.thoughtcrime.securesms.components.settings.DSLSettingsText r7 = r7.from(r8, r9)
                L_0x0063:
                    r9 = r7
                    long r7 = r6.getId()
                    long r10 = r5.getExpandedId()
                    int r12 = (r7 > r10 ? 1 : (r7 == r10 ? 0 : -1))
                    if (r12 != 0) goto L_0x0073
                    r7 = 1
                    r11 = 1
                    goto L_0x0074
                L_0x0073:
                    r11 = 0
                L_0x0074:
                    j$.time.LocalDateTime r12 = r5.getTimeSlotB()
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1 r13 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$1
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel r7 = org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(r4)
                    r13.<init>(r7)
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2 r15 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$2
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel r7 = org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(r4)
                    r15.<init>(r7)
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3 r10 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$3
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel r7 = org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(r4)
                    r10.<init>(r7)
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4 r8 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$4
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionViewModel r7 = org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment.access$getViewModel(r4)
                    r8.<init>(r7)
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$Entry r7 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$Entry
                    r18 = r2
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5 r2 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$1$5
                    r2.<init>(r4)
                    r19 = r7
                    r17 = r8
                    r8 = r14
                    r16 = r10
                    r10 = r6
                    r14 = r15
                    r15 = r16
                    r16 = r2
                    r7.<init>(r8, r9, r10, r11, r12, r13, r14, r15, r16, r17)
                    r2 = r19
                    r1.customPref(r2)
                    org.signal.core.util.DimensionUnit r2 = org.signal.core.util.DimensionUnit.DP
                    r6 = 1098907648(0x41800000, float:16.0)
                    float r2 = r2.toPixels(r6)
                    int r2 = (int) r2
                    r1.space(r2)
                    r2 = r18
                    goto L_0x001d
                L_0x00ca:
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$New r2 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection$New
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$2 r3 = new org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1$2
                    org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment r4 = r0.this$0
                    r3.<init>(r4)
                    r2.<init>(r3)
                    r1.customPref(r2)
                    org.signal.core.util.DimensionUnit r2 = org.signal.core.util.DimensionUnit.DP
                    r3 = 1101004800(0x41a00000, float:20.0)
                    float r2 = r2.toPixels(r3)
                    int r2 = (int) r2
                    r1.space(r2)
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.notifications.manual.NotificationProfileSelectionFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final void navigateToSettings(NotificationProfile notificationProfile) {
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        startActivity(companion.notificationProfileDetails(requireContext, notificationProfile.getId()));
        dismissAllowingStateLoss();
    }

    /* compiled from: NotificationProfileSelectionFragment.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/manual/NotificationProfileSelectionFragment$Companion;", "", "()V", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            new NotificationProfileSelectionFragment().show(fragmentManager, BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG);
        }
    }
}
