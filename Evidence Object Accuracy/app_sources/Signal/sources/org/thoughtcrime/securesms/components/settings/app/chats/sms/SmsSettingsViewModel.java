package org.thoughtcrime.securesms.components.settings.app.chats.sms;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: SmsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eJ\u000e\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\r\u001a\u00020\u000eR\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/sms/SmsSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/chats/sms/SmsSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "checkSmsEnabled", "", "setSmsDeliveryReportsEnabled", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "setWifiCallingCompatibilityEnabled", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SmsSettingsViewModel extends ViewModel {
    private final LiveData<SmsSettingsState> state;
    private final Store<SmsSettingsState> store;

    public SmsSettingsViewModel() {
        Store<SmsSettingsState> store = new Store<>(new SmsSettingsState(Util.isDefaultSmsProvider(ApplicationDependencies.getApplication()), SignalStore.settings().isSmsDeliveryReportsEnabled(), SignalStore.settings().isWifiCallingCompatibilityModeEnabled()));
        this.store = store;
        LiveData<SmsSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<SmsSettingsState> getState() {
        return this.state;
    }

    /* renamed from: setSmsDeliveryReportsEnabled$lambda-0 */
    public static final SmsSettingsState m643setSmsDeliveryReportsEnabled$lambda0(boolean z, SmsSettingsState smsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(smsSettingsState, "it");
        return SmsSettingsState.copy$default(smsSettingsState, false, z, false, 5, null);
    }

    public final void setSmsDeliveryReportsEnabled(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SmsSettingsViewModel.m643setSmsDeliveryReportsEnabled$lambda0(this.f$0, (SmsSettingsState) obj);
            }
        });
        SignalStore.settings().setSmsDeliveryReportsEnabled(z);
    }

    /* renamed from: setWifiCallingCompatibilityEnabled$lambda-1 */
    public static final SmsSettingsState m644setWifiCallingCompatibilityEnabled$lambda1(boolean z, SmsSettingsState smsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(smsSettingsState, "it");
        return SmsSettingsState.copy$default(smsSettingsState, false, false, z, 3, null);
    }

    public final void setWifiCallingCompatibilityEnabled(boolean z) {
        this.store.update(new Function(z) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsViewModel$$ExternalSyntheticLambda1
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SmsSettingsViewModel.m644setWifiCallingCompatibilityEnabled$lambda1(this.f$0, (SmsSettingsState) obj);
            }
        });
        SignalStore.settings().setWifiCallingCompatibilityModeEnabled(z);
    }

    /* renamed from: checkSmsEnabled$lambda-2 */
    public static final SmsSettingsState m642checkSmsEnabled$lambda2(SmsSettingsState smsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(smsSettingsState, "it");
        return SmsSettingsState.copy$default(smsSettingsState, Util.isDefaultSmsProvider(ApplicationDependencies.getApplication()), false, false, 6, null);
    }

    public final void checkSmsEnabled() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.sms.SmsSettingsViewModel$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SmsSettingsViewModel.m642checkSmsEnabled$lambda2((SmsSettingsState) obj);
            }
        });
    }
}
