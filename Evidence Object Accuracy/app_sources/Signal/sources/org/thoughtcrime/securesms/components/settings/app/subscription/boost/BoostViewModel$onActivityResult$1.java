package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.app.Application;
import com.annimon.stream.function.Function;
import com.google.android.gms.wallet.PaymentData;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.money.FiatMoney;
import org.signal.donations.GooglePayApi;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostState;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationError;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.DonationErrorSource;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.whispersystems.signalservice.api.subscriptions.SubscriptionLevels;

/* compiled from: BoostViewModel.kt */
@Metadata(d1 = {"\u0000!\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\u0016J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H\u0016J\u0010\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tH\u0016¨\u0006\n"}, d2 = {"org/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostViewModel$onActivityResult$1", "Lorg/signal/donations/GooglePayApi$PaymentRequestCallback;", "onCancelled", "", "onError", "googlePayException", "Lorg/signal/donations/GooglePayApi$GooglePayException;", "onSuccess", "paymentData", "Lcom/google/android/gms/wallet/PaymentData;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostViewModel$onActivityResult$1 implements GooglePayApi.PaymentRequestCallback {
    final /* synthetic */ Boost $boost;
    final /* synthetic */ BoostViewModel this$0;

    public BoostViewModel$onActivityResult$1(Boost boost, BoostViewModel boostViewModel) {
        this.$boost = boost;
        this.this$0 = boostViewModel;
    }

    @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
    public void onSuccess(PaymentData paymentData) {
        Intrinsics.checkNotNullParameter(paymentData, "paymentData");
        if (this.$boost != null) {
            this.this$0.eventPublisher.onNext(DonationEvent.RequestTokenSuccess.INSTANCE);
            this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$onActivityResult$1$$ExternalSyntheticLambda0
                @Override // com.annimon.stream.function.Function
                public final Object apply(Object obj) {
                    return BoostViewModel$onActivityResult$1.m949onSuccess$lambda0((BoostState) obj);
                }
            });
            DonationPaymentRepository donationPaymentRepository = this.this$0.donationPaymentRepository;
            FiatMoney price = this.$boost.getPrice();
            RecipientId id = Recipient.self().getId();
            Intrinsics.checkNotNullExpressionValue(id, "self().id");
            SubscribersKt.subscribeBy(donationPaymentRepository.continuePayment(price, paymentData, id, null, Long.parseLong(SubscriptionLevels.BOOST_LEVEL)), new BoostViewModel$onActivityResult$1$onSuccess$2(this.this$0), new BoostViewModel$onActivityResult$1$onSuccess$3(this.this$0));
            return;
        }
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$onActivityResult$1$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$onActivityResult$1.m950onSuccess$lambda1((BoostState) obj);
            }
        });
    }

    /* renamed from: onSuccess$lambda-0 */
    public static final BoostState m949onSuccess$lambda0(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.PAYMENT_PIPELINE, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    /* renamed from: onSuccess$lambda-1 */
    public static final BoostState m950onSuccess$lambda1(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.READY, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    /* renamed from: onError$lambda-2 */
    public static final BoostState m948onError$lambda2(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.READY, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
    public void onError(GooglePayApi.GooglePayException googlePayException) {
        Intrinsics.checkNotNullParameter(googlePayException, "googlePayException");
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$onActivityResult$1$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$onActivityResult$1.m948onError$lambda2((BoostState) obj);
            }
        });
        DonationError.Companion companion = DonationError.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        companion.routeDonationError(application, companion.getGooglePayRequestTokenError(DonationErrorSource.BOOST, googlePayException));
    }

    /* renamed from: onCancelled$lambda-3 */
    public static final BoostState m947onCancelled$lambda3(BoostState boostState) {
        Intrinsics.checkNotNullExpressionValue(boostState, "it");
        return boostState.copy((r20 & 1) != 0 ? boostState.boostBadge : null, (r20 & 2) != 0 ? boostState.currencySelection : null, (r20 & 4) != 0 ? boostState.isGooglePayAvailable : false, (r20 & 8) != 0 ? boostState.boosts : null, (r20 & 16) != 0 ? boostState.selectedBoost : null, (r20 & 32) != 0 ? boostState.customAmount : null, (r20 & 64) != 0 ? boostState.isCustomAmountFocused : false, (r20 & 128) != 0 ? boostState.stage : BoostState.Stage.READY, (r20 & 256) != 0 ? boostState.supportedCurrencyCodes : null);
    }

    @Override // org.signal.donations.GooglePayApi.PaymentRequestCallback
    public void onCancelled() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostViewModel$onActivityResult$1$$ExternalSyntheticLambda2
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return BoostViewModel$onActivityResult$1.m947onCancelled$lambda3((BoostState) obj);
            }
        });
    }
}
