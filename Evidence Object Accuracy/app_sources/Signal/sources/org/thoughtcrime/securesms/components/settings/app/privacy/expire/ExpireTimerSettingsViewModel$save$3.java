package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Result;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.mediasend.MediaSendActivityResult;
import org.thoughtcrime.securesms.util.livedata.ProcessState;

/* compiled from: ExpireTimerSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u0014\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\n¢\u0006\u0004\b\u0005\u0010\u0006"}, d2 = {"<anonymous>", "", MediaSendActivityResult.EXTRA_RESULT, "Lkotlin/Result;", "", "invoke", "(Ljava/lang/Object;)V"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsViewModel$save$3 extends Lambda implements Function1<Result<? extends Integer>, Unit> {
    final /* synthetic */ ExpireTimerSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ExpireTimerSettingsViewModel$save$3(ExpireTimerSettingsViewModel expireTimerSettingsViewModel) {
        super(1);
        this.this$0 = expireTimerSettingsViewModel;
    }

    /* renamed from: invoke$lambda-0 */
    public static final ExpireTimerSettingsState m880invoke$lambda0(Object obj, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullExpressionValue(expireTimerSettingsState, "it");
        return ExpireTimerSettingsState.copy$default(expireTimerSettingsState, 0, null, ProcessState.Companion.fromResult(obj), false, false, 27, null);
    }

    @Override // kotlin.jvm.functions.Function1
    public final void invoke(Result<? extends Integer> result) {
        this.this$0.store.update(new Function(result) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel$save$3$$ExternalSyntheticLambda0
            public final /* synthetic */ Object f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ExpireTimerSettingsViewModel$save$3.m880invoke$lambda0(this.f$0, (ExpireTimerSettingsState) obj);
            }
        });
    }
}
