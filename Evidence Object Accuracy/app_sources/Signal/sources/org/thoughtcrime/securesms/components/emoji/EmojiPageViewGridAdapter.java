package org.thoughtcrime.securesms.components.emoji;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import j$.util.function.Function;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public class EmojiPageViewGridAdapter extends MappingAdapter implements PopupWindow.OnDismissListener {
    private final VariationSelectorListener variationSelectorListener;

    /* loaded from: classes4.dex */
    public static class EmojiNoResultsModel implements MappingModel<EmojiNoResultsModel> {
        public boolean areContentsTheSame(EmojiNoResultsModel emojiNoResultsModel) {
            return true;
        }

        public boolean areItemsTheSame(EmojiNoResultsModel emojiNoResultsModel) {
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(EmojiNoResultsModel emojiNoResultsModel) {
            return MappingModel.CC.$default$getChangePayload(this, emojiNoResultsModel);
        }
    }

    /* loaded from: classes4.dex */
    public interface HasKey {
        String getKey();
    }

    /* loaded from: classes4.dex */
    public interface VariationSelectorListener {
        void onVariationSelectorStateChanged(boolean z);
    }

    public EmojiPageViewGridAdapter(EmojiVariationSelectorPopup emojiVariationSelectorPopup, EmojiEventListener emojiEventListener, VariationSelectorListener variationSelectorListener, boolean z, int i, int i2) {
        this.variationSelectorListener = variationSelectorListener;
        emojiVariationSelectorPopup.setOnDismissListener(this);
        registerFactory(EmojiHeader.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new EmojiPageViewGridAdapter.EmojiHeaderViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.emoji_grid_header));
        registerFactory(EmojiModel.class, new LayoutFactory(new Function(variationSelectorListener, emojiVariationSelectorPopup, z) { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter$$ExternalSyntheticLambda1
            public final /* synthetic */ EmojiPageViewGridAdapter.VariationSelectorListener f$1;
            public final /* synthetic */ EmojiVariationSelectorPopup f$2;
            public final /* synthetic */ boolean f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiPageViewGridAdapter.lambda$new$0(EmojiEventListener.this, this.f$1, this.f$2, this.f$3, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, i));
        registerFactory(EmojiTextModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return EmojiPageViewGridAdapter.lambda$new$1(EmojiEventListener.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, i2));
        registerFactory(EmojiNoResultsModel.class, new LayoutFactory(new EmojiPageViewGridAdapter$$ExternalSyntheticLambda3(), R.layout.emoji_grid_no_results));
    }

    public static /* synthetic */ MappingViewHolder lambda$new$0(EmojiEventListener emojiEventListener, VariationSelectorListener variationSelectorListener, EmojiVariationSelectorPopup emojiVariationSelectorPopup, boolean z, View view) {
        return new EmojiViewHolder(view, emojiEventListener, variationSelectorListener, emojiVariationSelectorPopup, z);
    }

    public static /* synthetic */ MappingViewHolder lambda$new$1(EmojiEventListener emojiEventListener, View view) {
        return new EmojiTextViewHolder(view, emojiEventListener);
    }

    @Override // android.widget.PopupWindow.OnDismissListener
    public void onDismiss() {
        this.variationSelectorListener.onVariationSelectorStateChanged(false);
    }

    /* loaded from: classes4.dex */
    public static class EmojiHeader implements MappingModel<EmojiHeader>, HasKey {
        private final String key;
        private final int title;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(EmojiHeader emojiHeader) {
            return MappingModel.CC.$default$getChangePayload(this, emojiHeader);
        }

        public EmojiHeader(String str, int i) {
            this.key = str;
            this.title = i;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.HasKey
        public String getKey() {
            return this.key;
        }

        public boolean areItemsTheSame(EmojiHeader emojiHeader) {
            return this.title == emojiHeader.title;
        }

        public boolean areContentsTheSame(EmojiHeader emojiHeader) {
            return areItemsTheSame(emojiHeader);
        }
    }

    /* loaded from: classes4.dex */
    static class EmojiHeaderViewHolder extends MappingViewHolder<EmojiHeader> {
        private final TextView title = ((TextView) findViewById(R.id.emoji_grid_header_title));

        public EmojiHeaderViewHolder(View view) {
            super(view);
        }

        public void bind(EmojiHeader emojiHeader) {
            this.title.setText(emojiHeader.title);
        }
    }

    /* loaded from: classes4.dex */
    public static class EmojiModel implements MappingModel<EmojiModel>, HasKey {
        private final Emoji emoji;
        private final String key;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(EmojiModel emojiModel) {
            return MappingModel.CC.$default$getChangePayload(this, emojiModel);
        }

        public EmojiModel(String str, Emoji emoji) {
            this.key = str;
            this.emoji = emoji;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.HasKey
        public String getKey() {
            return this.key;
        }

        public Emoji getEmoji() {
            return this.emoji;
        }

        public boolean areItemsTheSame(EmojiModel emojiModel) {
            return emojiModel.emoji.getValue().equals(this.emoji.getValue());
        }

        public boolean areContentsTheSame(EmojiModel emojiModel) {
            return areItemsTheSame(emojiModel);
        }
    }

    /* loaded from: classes4.dex */
    public static class EmojiViewHolder extends MappingViewHolder<EmojiModel> {
        private final boolean allowVariations;
        private final EmojiEventListener emojiEventListener;
        private final ImageView imageView;
        private final EmojiVariationSelectorPopup popup;
        private final VariationSelectorListener variationSelectorListener;

        public EmojiViewHolder(View view, EmojiEventListener emojiEventListener, VariationSelectorListener variationSelectorListener, EmojiVariationSelectorPopup emojiVariationSelectorPopup, boolean z) {
            super(view);
            this.popup = emojiVariationSelectorPopup;
            this.variationSelectorListener = variationSelectorListener;
            this.emojiEventListener = emojiEventListener;
            this.allowVariations = z;
            this.imageView = (ImageView) view.findViewById(R.id.emoji_image);
        }

        public void bind(EmojiModel emojiModel) {
            Drawable emojiDrawable = EmojiProvider.getEmojiDrawable(this.imageView.getContext(), emojiModel.emoji.getValue());
            if (emojiDrawable != null) {
                this.imageView.setVisibility(0);
                this.imageView.setImageDrawable(emojiDrawable);
            }
            this.itemView.setOnClickListener(new EmojiPageViewGridAdapter$EmojiViewHolder$$ExternalSyntheticLambda0(this, emojiModel));
            if (!this.allowVariations || !emojiModel.emoji.hasMultipleVariations()) {
                this.itemView.setOnLongClickListener(null);
            } else {
                this.itemView.setOnLongClickListener(new EmojiPageViewGridAdapter$EmojiViewHolder$$ExternalSyntheticLambda1(this, emojiModel));
            }
        }

        public /* synthetic */ void lambda$bind$0(EmojiModel emojiModel, View view) {
            this.emojiEventListener.onEmojiSelected(emojiModel.emoji.getValue());
        }

        public /* synthetic */ boolean lambda$bind$1(EmojiModel emojiModel, View view) {
            this.popup.dismiss();
            this.popup.setVariations(emojiModel.emoji.getVariations());
            EmojiVariationSelectorPopup emojiVariationSelectorPopup = this.popup;
            View view2 = this.itemView;
            emojiVariationSelectorPopup.showAsDropDown(view2, 0, -(view2.getHeight() * 2));
            this.variationSelectorListener.onVariationSelectorStateChanged(true);
            return true;
        }
    }

    /* loaded from: classes4.dex */
    public static class EmojiTextModel implements MappingModel<EmojiTextModel>, HasKey {
        private final Emoji emoji;
        private final String key;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(EmojiTextModel emojiTextModel) {
            return MappingModel.CC.$default$getChangePayload(this, emojiTextModel);
        }

        public EmojiTextModel(String str, Emoji emoji) {
            this.key = str;
            this.emoji = emoji;
        }

        @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter.HasKey
        public String getKey() {
            return this.key;
        }

        public Emoji getEmoji() {
            return this.emoji;
        }

        public boolean areItemsTheSame(EmojiTextModel emojiTextModel) {
            return emojiTextModel.emoji.getValue().equals(this.emoji.getValue());
        }

        public boolean areContentsTheSame(EmojiTextModel emojiTextModel) {
            return areItemsTheSame(emojiTextModel);
        }
    }

    /* loaded from: classes4.dex */
    public static class EmojiTextViewHolder extends MappingViewHolder<EmojiTextModel> {
        private final EmojiEventListener emojiEventListener;
        private final AsciiEmojiView textView;

        public EmojiTextViewHolder(View view, EmojiEventListener emojiEventListener) {
            super(view);
            this.emojiEventListener = emojiEventListener;
            this.textView = (AsciiEmojiView) view.findViewById(R.id.emoji_text);
        }

        public void bind(EmojiTextModel emojiTextModel) {
            this.textView.setEmoji(emojiTextModel.emoji.getValue());
            this.itemView.setOnClickListener(new EmojiPageViewGridAdapter$EmojiTextViewHolder$$ExternalSyntheticLambda0(this, emojiTextModel));
        }

        public /* synthetic */ void lambda$bind$0(EmojiTextModel emojiTextModel, View view) {
            this.emojiEventListener.onEmojiSelected(emojiTextModel.emoji.getValue());
        }
    }
}
