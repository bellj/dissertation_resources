package org.thoughtcrime.securesms.components.webrtc;

import android.view.View;
import android.view.ViewGroup;
import org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper;

/* loaded from: classes4.dex */
public final class PictureInPictureExpansionHelper {
    private State state = State.IS_SHRUNKEN;

    /* loaded from: classes4.dex */
    public interface Callback {
        void onAnimationHasFinished();

        void onAnimationWillStart();

        void onPictureInPictureExpanded();

        void onPictureInPictureNotVisible();
    }

    /* loaded from: classes4.dex */
    public enum State {
        IS_EXPANDING,
        IS_EXPANDED,
        IS_SHRINKING,
        IS_SHRUNKEN
    }

    public boolean isExpandedOrExpanding() {
        State state = this.state;
        return state == State.IS_EXPANDED || state == State.IS_EXPANDING;
    }

    public boolean isShrunkenOrShrinking() {
        State state = this.state;
        return state == State.IS_SHRUNKEN || state == State.IS_SHRINKING;
    }

    public void expand(View view, final Callback callback) {
        if (!isExpandedOrExpanding()) {
            performExpandAnimation(view, new Callback() { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.1
                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onAnimationWillStart() {
                    PictureInPictureExpansionHelper.this.state = State.IS_EXPANDING;
                    callback.onAnimationWillStart();
                }

                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onPictureInPictureExpanded() {
                    callback.onPictureInPictureExpanded();
                }

                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onPictureInPictureNotVisible() {
                    callback.onPictureInPictureNotVisible();
                }

                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onAnimationHasFinished() {
                    PictureInPictureExpansionHelper.this.state = State.IS_EXPANDED;
                    callback.onAnimationHasFinished();
                }
            });
        }
    }

    public void shrink(View view, final Callback callback) {
        if (!isShrunkenOrShrinking()) {
            performShrinkAnimation(view, new Callback() { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.2
                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onAnimationWillStart() {
                    PictureInPictureExpansionHelper.this.state = State.IS_SHRINKING;
                    callback.onAnimationWillStart();
                }

                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onPictureInPictureExpanded() {
                    callback.onPictureInPictureExpanded();
                }

                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onPictureInPictureNotVisible() {
                    callback.onPictureInPictureNotVisible();
                }

                @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
                public void onAnimationHasFinished() {
                    PictureInPictureExpansionHelper.this.state = State.IS_SHRUNKEN;
                    callback.onAnimationHasFinished();
                }
            });
        }
    }

    private void performExpandAnimation(View view, Callback callback) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        float x = view.getX();
        float y = view.getY();
        float max = Math.max(((float) viewGroup.getMeasuredWidth()) / ((float) view.getMeasuredWidth()), ((float) viewGroup.getMeasuredHeight()) / ((float) view.getMeasuredHeight()));
        callback.onAnimationWillStart();
        view.animate().setDuration(200).x(((float) (viewGroup.getMeasuredWidth() - view.getMeasuredWidth())) / 2.0f).y(((float) (viewGroup.getMeasuredHeight() - view.getMeasuredHeight())) / 2.0f).scaleX(max).scaleY(max).withEndAction(new Runnable(view, x, y) { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper$$ExternalSyntheticLambda0
            public final /* synthetic */ View f$1;
            public final /* synthetic */ float f$2;
            public final /* synthetic */ float f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PictureInPictureExpansionHelper.$r8$lambda$v739hNGx8f_1b3Erw5ArbdKgCyc(PictureInPictureExpansionHelper.Callback.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public static /* synthetic */ void lambda$performExpandAnimation$1(Callback callback, View view, float f, float f2) {
        callback.onPictureInPictureExpanded();
        view.animate().setDuration(100).alpha(0.0f).withEndAction(new Runnable(view, f, f2) { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper$$ExternalSyntheticLambda3
            public final /* synthetic */ View f$1;
            public final /* synthetic */ float f$2;
            public final /* synthetic */ float f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PictureInPictureExpansionHelper.m1279$r8$lambda$6eXNMtniI8GByXYGjxac5tlO7s(PictureInPictureExpansionHelper.Callback.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public static /* synthetic */ void lambda$performExpandAnimation$0(Callback callback, View view, float f, float f2) {
        callback.onPictureInPictureNotVisible();
        view.setX(f);
        view.setY(f2);
        view.setScaleX(0.0f);
        view.setScaleY(0.0f);
        view.setAlpha(1.0f);
        view.animate().setDuration(200).scaleX(1.0f).scaleY(1.0f).withEndAction(new PictureInPictureExpansionHelper$$ExternalSyntheticLambda1(callback));
    }

    private void performShrinkAnimation(View view, Callback callback) {
        ViewGroup viewGroup = (ViewGroup) view.getParent();
        float x = view.getX();
        float y = view.getY();
        float max = Math.max(((float) viewGroup.getMeasuredWidth()) / ((float) view.getMeasuredWidth()), ((float) viewGroup.getMeasuredHeight()) / ((float) view.getMeasuredHeight()));
        callback.onAnimationWillStart();
        view.animate().setDuration(200).scaleX(0.0f).scaleY(0.0f).withEndAction(new Runnable(view, viewGroup, max, callback, x, y) { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper$$ExternalSyntheticLambda2
            public final /* synthetic */ View f$0;
            public final /* synthetic */ ViewGroup f$1;
            public final /* synthetic */ float f$2;
            public final /* synthetic */ PictureInPictureExpansionHelper.Callback f$3;
            public final /* synthetic */ float f$4;
            public final /* synthetic */ float f$5;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
                this.f$4 = r5;
                this.f$5 = r6;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PictureInPictureExpansionHelper.$r8$lambda$KyPdWYAe6ijz6ED1zWZK_ZN4wRA(this.f$0, this.f$1, this.f$2, this.f$3, this.f$4, this.f$5);
            }
        });
    }

    public static /* synthetic */ void lambda$performShrinkAnimation$3(View view, ViewGroup viewGroup, float f, Callback callback, float f2, float f3) {
        view.setX(((float) (viewGroup.getMeasuredWidth() - view.getMeasuredWidth())) / 2.0f);
        view.setY(((float) (viewGroup.getMeasuredHeight() - view.getMeasuredHeight())) / 2.0f);
        view.setAlpha(0.0f);
        view.setScaleX(f);
        view.setScaleY(f);
        callback.onPictureInPictureNotVisible();
        view.animate().setDuration(100).alpha(1.0f).withEndAction(new Runnable(view, f2, f3) { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper$$ExternalSyntheticLambda4
            public final /* synthetic */ View f$1;
            public final /* synthetic */ float f$2;
            public final /* synthetic */ float f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PictureInPictureExpansionHelper.$r8$lambda$PMcwItxpi1oHmpsVwPflgE09kas(PictureInPictureExpansionHelper.Callback.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    public static /* synthetic */ void lambda$performShrinkAnimation$2(Callback callback, View view, float f, float f2) {
        callback.onPictureInPictureExpanded();
        view.animate().scaleX(1.0f).scaleY(1.0f).x(f).y(f2).withEndAction(new PictureInPictureExpansionHelper$$ExternalSyntheticLambda1(callback));
    }
}
