package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AdvancedPrivacySettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ AdvancedPrivacySettingsFragment f$0;

    public /* synthetic */ AdvancedPrivacySettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda0(AdvancedPrivacySettingsFragment advancedPrivacySettingsFragment) {
        this.f$0 = advancedPrivacySettingsFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        AdvancedPrivacySettingsFragment$getConfiguration$1.AnonymousClass1.m843invoke$lambda1$lambda0(this.f$0, dialogInterface, i);
    }
}
