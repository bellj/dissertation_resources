package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class EmojiSpan extends AnimatingImageSpan {
    private final float SHIFT_FACTOR = 1.5f;
    private Paint.FontMetricsInt fontMetrics;
    private int size;

    public EmojiSpan(Drawable drawable, TextView textView) {
        super(drawable, textView);
        int i;
        Paint.FontMetricsInt fontMetricsInt = textView.getPaint().getFontMetricsInt();
        this.fontMetrics = fontMetricsInt;
        if (fontMetricsInt != null) {
            i = Math.abs(fontMetricsInt.descent) + Math.abs(this.fontMetrics.ascent);
        } else {
            i = textView.getResources().getDimensionPixelSize(R.dimen.conversation_item_body_text_size);
        }
        this.size = i;
        Drawable drawable2 = getDrawable();
        int i2 = this.size;
        drawable2.setBounds(0, 0, i2, i2);
    }

    public EmojiSpan(Context context, Drawable drawable, Paint paint) {
        super(drawable, null);
        int i;
        Paint.FontMetricsInt fontMetricsInt = paint.getFontMetricsInt();
        this.fontMetrics = fontMetricsInt;
        if (fontMetricsInt != null) {
            i = Math.abs(fontMetricsInt.descent) + Math.abs(this.fontMetrics.ascent);
        } else {
            i = context.getResources().getDimensionPixelSize(R.dimen.conversation_item_body_text_size);
        }
        this.size = i;
        Drawable drawable2 = getDrawable();
        int i2 = this.size;
        drawable2.setBounds(0, 0, i2, i2);
    }

    @Override // android.text.style.DynamicDrawableSpan, android.text.style.ReplacementSpan
    public int getSize(Paint paint, CharSequence charSequence, int i, int i2, Paint.FontMetricsInt fontMetricsInt) {
        Paint.FontMetricsInt fontMetricsInt2;
        if (fontMetricsInt == null || (fontMetricsInt2 = this.fontMetrics) == null) {
            Paint.FontMetricsInt fontMetricsInt3 = paint.getFontMetricsInt();
            this.fontMetrics = fontMetricsInt3;
            this.size = Math.abs(fontMetricsInt3.descent) + Math.abs(this.fontMetrics.ascent);
            Drawable drawable = getDrawable();
            int i3 = this.size;
            drawable.setBounds(0, 0, i3, i3);
        } else {
            fontMetricsInt.ascent = fontMetricsInt2.ascent;
            fontMetricsInt.descent = fontMetricsInt2.descent;
            fontMetricsInt.top = fontMetricsInt2.top;
            fontMetricsInt.bottom = fontMetricsInt2.bottom;
            fontMetricsInt.leading = fontMetricsInt2.leading;
        }
        return this.size;
    }

    @Override // android.text.style.DynamicDrawableSpan, android.text.style.ReplacementSpan
    public void draw(Canvas canvas, CharSequence charSequence, int i, int i2, float f, int i3, int i4, int i5, Paint paint) {
        super.draw(canvas, charSequence, i, i2, f, i3, i4, i5 - ((int) (((float) (((i5 - i3) - this.size) / 2)) * 1.5f)), paint);
    }
}
