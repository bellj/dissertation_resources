package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public final class SecondInviteReminder extends Reminder {
    private final int progress;

    public SecondInviteReminder(Context context, Recipient recipient, int i) {
        super(context.getString(R.string.SecondInviteReminder__title), context.getString(R.string.SecondInviteReminder__description, recipient.getDisplayName(context)));
        this.progress = i;
        addAction(new Reminder.Action(context.getString(R.string.InsightsReminder__invite), R.id.reminder_action_invite));
        addAction(new Reminder.Action(context.getString(R.string.InsightsReminder__view_insights), R.id.reminder_action_view_insights));
    }

    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public int getProgress() {
        return this.progress;
    }
}
