package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import kotlin.Metadata;

/* compiled from: AdvancedPrivacySettingsState.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/CensorshipCircumventionState;", "", "available", "", "(Ljava/lang/String;IZ)V", "getAvailable", "()Z", "UNAVAILABLE_CONNECTED", "UNAVAILABLE_NO_INTERNET", "AVAILABLE_MANUALLY_DISABLED", "AVAILABLE_AUTOMATICALLY_ENABLED", "AVAILABLE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum CensorshipCircumventionState {
    UNAVAILABLE_CONNECTED(false),
    UNAVAILABLE_NO_INTERNET(false),
    AVAILABLE_MANUALLY_DISABLED(true),
    AVAILABLE_AUTOMATICALLY_ENABLED(true),
    AVAILABLE(true);
    
    private final boolean available;

    CensorshipCircumventionState(boolean z) {
        this.available = z;
    }

    public final boolean getAvailable() {
        return this.available;
    }
}
