package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import java.util.List;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.util.InternetConnectionObserver;

/* compiled from: DonationReceiptListViewModel.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\b\u0010\u0011\u001a\u00020\u0012H\u0014J\b\u0010\u0013\u001a\u00020\u0012H\u0002J\b\u0010\u0014\u001a\u00020\u0012H\u0002R\u000e\u0010\u0005\u001a\u00020\u0006X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "internalState", "Landroidx/lifecycle/MutableLiveData;", "", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptBadge;", "networkDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "state", "Landroidx/lifecycle/LiveData;", "getState", "()Landroidx/lifecycle/LiveData;", "onCleared", "", "refresh", "retry", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListViewModel extends ViewModel {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private final MutableLiveData<List<DonationReceiptBadge>> internalState;
    private Disposable networkDisposable;
    private final DonationReceiptListRepository repository;
    private final LiveData<List<DonationReceiptBadge>> state;

    public DonationReceiptListViewModel(DonationReceiptListRepository donationReceiptListRepository) {
        Intrinsics.checkNotNullParameter(donationReceiptListRepository, "repository");
        this.repository = donationReceiptListRepository;
        MutableLiveData<List<DonationReceiptBadge>> mutableLiveData = new MutableLiveData<>(CollectionsKt__CollectionsKt.emptyList());
        this.internalState = mutableLiveData;
        this.state = mutableLiveData;
        Disposable subscribe = InternetConnectionObserver.INSTANCE.observe().distinctUntilChanged().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationReceiptListViewModel.m1032_init_$lambda0(DonationReceiptListViewModel.this, (Boolean) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "InternetConnectionObserv…retry()\n        }\n      }");
        this.networkDisposable = subscribe;
        refresh();
    }

    public final LiveData<List<DonationReceiptBadge>> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m1032_init_$lambda0(DonationReceiptListViewModel donationReceiptListViewModel, Boolean bool) {
        Intrinsics.checkNotNullParameter(donationReceiptListViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(bool, "isConnected");
        if (bool.booleanValue()) {
            donationReceiptListViewModel.retry();
        }
    }

    private final void retry() {
        List<DonationReceiptBadge> value = this.internalState.getValue();
        boolean z = true;
        if (value == null || !value.isEmpty()) {
            z = false;
        }
        if (z) {
            refresh();
        }
    }

    private final void refresh() {
        this.disposables.clear();
        CompositeDisposable compositeDisposable = this.disposables;
        Disposable subscribe = this.repository.getBadges().subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonationReceiptListViewModel.m1033refresh$lambda1(DonationReceiptListViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getBadges().s…e.postValue(badges)\n    }");
        DisposableKt.plusAssign(compositeDisposable, subscribe);
    }

    /* renamed from: refresh$lambda-1 */
    public static final void m1033refresh$lambda1(DonationReceiptListViewModel donationReceiptListViewModel, List list) {
        Intrinsics.checkNotNullParameter(donationReceiptListViewModel, "this$0");
        donationReceiptListViewModel.internalState.postValue(list);
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
        this.networkDisposable.dispose();
    }

    /* compiled from: DonationReceiptListViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListRepository;", "(Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final DonationReceiptListRepository repository;

        public Factory(DonationReceiptListRepository donationReceiptListRepository) {
            Intrinsics.checkNotNullParameter(donationReceiptListRepository, "repository");
            this.repository = donationReceiptListRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new DonationReceiptListViewModel(this.repository));
            if (cast != null) {
                return cast;
            }
            throw new NullPointerException("null cannot be cast to non-null type T of org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListViewModel.Factory.create");
        }
    }
}
