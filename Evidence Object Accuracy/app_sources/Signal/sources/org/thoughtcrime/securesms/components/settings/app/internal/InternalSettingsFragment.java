package org.thoughtcrime.securesms.components.settings.app.internal;

import android.app.Application;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import j$.util.Optional;
import java.util.concurrent.TimeUnit;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.AppUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsViewModel;
import org.thoughtcrime.securesms.database.LocalMetricsDatabase;
import org.thoughtcrime.securesms.database.LogDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.JobTracker;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.jobs.RemoteConfigRefreshJob;
import org.thoughtcrime.securesms.jobs.RotateProfileKeyJob;
import org.thoughtcrime.securesms.jobs.StorageForcePushJob;
import org.thoughtcrime.securesms.jobs.SubscriptionKeepAliveJob;
import org.thoughtcrime.securesms.jobs.SubscriptionReceiptRequestResponseJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.payments.DataExportUtil;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.ConversationUtil;

/* compiled from: InternalSettingsFragment.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\b\u0010\t\u001a\u00020\u0006H\u0002J\b\u0010\n\u001a\u00020\u0006H\u0002J\b\u0010\u000b\u001a\u00020\u0006H\u0002J\b\u0010\f\u001a\u00020\u0006H\u0002J\b\u0010\r\u001a\u00020\u0006H\u0002J\b\u0010\u000e\u001a\u00020\u0006H\u0002J\b\u0010\u000f\u001a\u00020\u0006H\u0002J\b\u0010\u0010\u001a\u00020\u0006H\u0002J\b\u0010\u0011\u001a\u00020\u0006H\u0002J\b\u0010\u0012\u001a\u00020\u0006H\u0002J\b\u0010\u0013\u001a\u00020\u0006H\u0002J\b\u0010\u0014\u001a\u00020\u0006H\u0002J\b\u0010\u0015\u001a\u00020\u0006H\u0002J\u0010\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u0006H\u0002J\b\u0010\u001b\u001a\u00020\u0006H\u0002J\b\u0010\u001c\u001a\u00020\u0006H\u0002J\b\u0010\u001d\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006\u001e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "clearAllLocalMetricsState", "clearAllProfileKeys", "clearAllSenderKeySharedState", "clearAllSenderKeyState", "clearAllServiceIds", "clearCdsHistory", "clearKeepLongerLogs", "copyPaymentsDataToClipboard", "deleteAllDynamicShortcuts", "enqueueStorageServiceForcePush", "enqueueStorageServiceSync", "enqueueSubscriptionKeepAlive", "enqueueSubscriptionRedemption", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsState;", "refreshAttributes", "refreshProfile", "refreshRemoteValues", "rotateProfileKey", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternalSettingsFragment extends DSLSettingsFragment {
    private InternalSettingsViewModel viewModel;

    public InternalSettingsFragment() {
        super(R.string.preferences__internal_preferences, 0, 0, null, 14, null);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        ViewModel viewModel = new ViewModelProvider(this, new InternalSettingsViewModel.Factory(new InternalSettingsRepository(requireContext))).get(InternalSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ngsViewModel::class.java]");
        InternalSettingsViewModel internalSettingsViewModel = (InternalSettingsViewModel) viewModel;
        this.viewModel = internalSettingsViewModel;
        if (internalSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            internalSettingsViewModel = null;
        }
        internalSettingsViewModel.m670getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda8
            public final /* synthetic */ InternalSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                InternalSettingsFragment.$r8$lambda$cwiQVrGFrm7AM9LHMBVKM08I0qc(DSLSettingsAdapter.this, this.f$1, (InternalSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m655bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, InternalSettingsFragment internalSettingsFragment, InternalSettingsState internalSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(internalSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(internalSettingsState, "it");
        dSLSettingsAdapter.submitList(internalSettingsFragment.getConfiguration(internalSettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(InternalSettingsState internalSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(internalSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1
            final /* synthetic */ InternalSettingsState $state;
            final /* synthetic */ InternalSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /*  JADX ERROR: Method code generation error
                jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x050b: INVOKE  
                  (r16v0 'dSLConfiguration' org.thoughtcrime.securesms.components.settings.DSLConfiguration)
                  (r2v89 'from52' org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText : 0x04f5: INVOKE  (r3v73 org.thoughtcrime.securesms.components.settings.DSLSettingsText A[REMOVE]) = 
                  (r3v72 'companion2' org.thoughtcrime.securesms.components.settings.DSLSettingsText$Companion)
                  (r1v209 'str2' java.lang.String)
                  (wrap: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] : 0x04f3: NEW_ARRAY  (r4v33 org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[] A[REMOVE]) = (0 int) type: org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[])
                 type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.DSLSettingsText.Companion.from(java.lang.CharSequence, org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier[]):org.thoughtcrime.securesms.components.settings.DSLSettingsText)
                  false
                  (wrap: boolean : 0x04fe: INVOKE  (r5v22 boolean A[REMOVE]) = 
                  (wrap: java.lang.String : 0x04fa: INVOKE  (r5v21 java.lang.String A[REMOVE]) = (r10v4 'internalSettingsState15' org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsState) type: VIRTUAL call: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsState.getCallingServer():java.lang.String)
                  (r1v209 'str2' java.lang.String)
                 type: STATIC call: kotlin.jvm.internal.Intrinsics.areEqual(java.lang.Object, java.lang.Object):boolean)
                  (wrap: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1$29$1 : 0x0504: CONSTRUCTOR  (r6v33 org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1$29$1 A[REMOVE]) = 
                  (r9v13 'internalSettingsFragment27' org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment)
                  (r1v209 'str2' java.lang.String)
                 call: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1$29$1.<init>(org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment, java.lang.String):void type: CONSTRUCTOR)
                  (4 int)
                  (null java.lang.Object)
                 type: STATIC call: org.thoughtcrime.securesms.components.settings.DSLConfiguration.radioPref$default(org.thoughtcrime.securesms.components.settings.DSLConfiguration, org.thoughtcrime.securesms.components.settings.DSLSettingsText, org.thoughtcrime.securesms.components.settings.DSLSettingsText, boolean, boolean, kotlin.jvm.functions.Function0, int, java.lang.Object):void in method: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:245)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:105)
                	at jadx.core.dex.nodes.IBlock.generate(IBlock.java:15)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:94)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:221)
                	at jadx.core.dex.regions.loops.LoopRegion.generate(LoopRegion.java:173)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.dex.regions.Region.generate(Region.java:35)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:65)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:261)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:254)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:349)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:302)
                Caused by: jadx.core.utils.exceptions.CodegenException: Error generate insn: 0x0504: CONSTRUCTOR  (r6v33 org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1$29$1 A[REMOVE]) = 
                  (r9v13 'internalSettingsFragment27' org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment)
                  (r1v209 'str2' java.lang.String)
                 call: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1$29$1.<init>(org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment, java.lang.String):void type: CONSTRUCTOR in method: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void, file: classes4.dex
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:282)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:138)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:116)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:973)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:798)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:394)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:275)
                	... 16 more
                Caused by: jadx.core.utils.exceptions.JadxRuntimeException: Expected class to be processed at this point, class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1$29$1, state: NOT_LOADED
                	at jadx.core.dex.nodes.ClassNode.ensureProcessed(ClassNode.java:259)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:672)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:390)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:258)
                	... 22 more
                */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r16) {
                /*
                // Method dump skipped, instructions count: 1865
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final void copyPaymentsDataToClipboard() {
        new MaterialAlertDialogBuilder(requireContext()).setMessage((CharSequence) "Local payments history will be copied to the clipboard.\nIt may therefore compromise privacy.\nHowever, no private keys will be copied.").setPositiveButton((CharSequence) "Copy", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda6
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InternalSettingsFragment.$r8$lambda$O8hCuZNpKctf4jMJ5kto_amNyck(dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).show();
    }

    /* renamed from: copyPaymentsDataToClipboard$lambda-3 */
    public static final void m662copyPaymentsDataToClipboard$lambda3(DialogInterface dialogInterface, int i) {
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        Object systemService = application.getSystemService("clipboard");
        if (systemService != null) {
            SimpleTask.run(SignalExecutors.UNBOUNDED, new SimpleTask.BackgroundTask(application, (ClipboardManager) systemService) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ Context f$0;
                public final /* synthetic */ ClipboardManager f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
                public final Object run() {
                    return InternalSettingsFragment.$r8$lambda$YQxnG4n43b35ONTKrwhuuGDNiGQ(this.f$0, this.f$1);
                }
            }, new SimpleTask.ForegroundTask(application) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ Context f$0;

                {
                    this.f$0 = r1;
                }

                @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
                public final void run(Object obj) {
                    InternalSettingsFragment.$r8$lambda$OGfYdlmPRTvRNVflHRc2_QwC7Go(this.f$0, obj);
                }
            });
            return;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.content.ClipboardManager");
    }

    /* renamed from: copyPaymentsDataToClipboard$lambda-3$lambda-1 */
    public static final Object m663copyPaymentsDataToClipboard$lambda3$lambda1(Context context, ClipboardManager clipboardManager) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Intrinsics.checkNotNullParameter(clipboardManager, "$clipboard");
        String createTsv = DataExportUtil.createTsv();
        Intrinsics.checkNotNullExpressionValue(createTsv, "createTsv()");
        clipboardManager.setPrimaryClip(ClipData.newPlainText(context.getString(R.string.app_name), createTsv));
        return null;
    }

    /* renamed from: copyPaymentsDataToClipboard$lambda-3$lambda-2 */
    public static final void m664copyPaymentsDataToClipboard$lambda3$lambda2(Context context, Object obj) {
        Intrinsics.checkNotNullParameter(context, "$context");
        Toast.makeText(context, "Payments have been copied", 0).show();
    }

    public final void refreshAttributes() {
        ApplicationDependencies.getJobManager().startChain(new RefreshAttributesJob()).then(new RefreshOwnProfileJob()).enqueue();
        Toast.makeText(getContext(), "Scheduled attribute refresh", 0).show();
    }

    public final void refreshProfile() {
        ApplicationDependencies.getJobManager().add(new RefreshOwnProfileJob());
        Toast.makeText(getContext(), "Scheduled profile refresh", 0).show();
    }

    public final void rotateProfileKey() {
        ApplicationDependencies.getJobManager().add(new RotateProfileKeyJob());
        Toast.makeText(getContext(), "Scheduled profile key rotation", 0).show();
    }

    public final void refreshRemoteValues() {
        Toast.makeText(getContext(), "Running remote config refresh, app will restart after completion.", 1).show();
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda7
            @Override // java.lang.Runnable
            public final void run() {
                InternalSettingsFragment.$r8$lambda$FKk5SPyZpVVLyR_a1zKukR2B_Go(InternalSettingsFragment.this);
            }
        });
    }

    /* renamed from: refreshRemoteValues$lambda-4 */
    public static final void m665refreshRemoteValues$lambda4(InternalSettingsFragment internalSettingsFragment) {
        Intrinsics.checkNotNullParameter(internalSettingsFragment, "this$0");
        Optional<JobTracker.JobState> runSynchronously = ApplicationDependencies.getJobManager().runSynchronously(new RemoteConfigRefreshJob(), TimeUnit.SECONDS.toMillis(10));
        Intrinsics.checkNotNullExpressionValue(runSynchronously, "getJobManager().runSynch…nit.SECONDS.toMillis(10))");
        if (!runSynchronously.isPresent() || runSynchronously.get() != JobTracker.JobState.SUCCESS) {
            Toast.makeText(internalSettingsFragment.getContext(), "Failed to refresh config remote config.", 0).show();
        } else {
            AppUtil.restart(internalSettingsFragment.requireContext());
        }
    }

    public final void enqueueStorageServiceSync() {
        StorageSyncHelper.scheduleSyncForDataChange();
        Toast.makeText(getContext(), "Scheduled routine storage sync", 0).show();
    }

    public final void enqueueStorageServiceForcePush() {
        ApplicationDependencies.getJobManager().add(new StorageForcePushJob());
        Toast.makeText(getContext(), "Scheduled storage force push", 0).show();
    }

    public final void deleteAllDynamicShortcuts() {
        ConversationUtil.clearAllShortcuts(requireContext());
        Toast.makeText(getContext(), "Deleted all dynamic shortcuts.", 0).show();
    }

    public final void clearAllSenderKeyState() {
        SignalDatabase.Companion companion = SignalDatabase.Companion;
        companion.senderKeys().deleteAll();
        companion.senderKeyShared().deleteAll();
        Toast.makeText(getContext(), "Deleted all sender key state.", 0).show();
    }

    public final void clearAllSenderKeySharedState() {
        SignalDatabase.Companion.senderKeyShared().deleteAll();
        Toast.makeText(getContext(), "Deleted all sender key shared state.", 0).show();
    }

    public final void clearAllLocalMetricsState() {
        LocalMetricsDatabase.Companion companion = LocalMetricsDatabase.Companion;
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        companion.getInstance(application).clear();
        Toast.makeText(getContext(), "Cleared all local metrics state.", 0).show();
    }

    public final void enqueueSubscriptionRedemption() {
        SubscriptionReceiptRequestResponseJob.createSubscriptionContinuationJobChain().enqueue();
    }

    public final void enqueueSubscriptionKeepAlive() {
        SubscriptionKeepAliveJob.enqueueAndTrackTime(System.currentTimeMillis());
    }

    public final void clearCdsHistory() {
        SignalDatabase.Companion.cds().clearAll();
        SignalStore.misc().setCdsToken(null);
        Toast.makeText(getContext(), "Cleared all CDS history.", 0).show();
    }

    public final void clearAllServiceIds() {
        new MaterialAlertDialogBuilder(requireContext()).setTitle((CharSequence) "Clear all serviceIds?").setMessage((CharSequence) "Are you sure? Never do this on a non-test device.").setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda9
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InternalSettingsFragment.m651$r8$lambda$8zgbZ1fMWyWtVgLFaKNHCAjUNw(InternalSettingsFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda10
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InternalSettingsFragment.$r8$lambda$E11a3gyyLt_3GfzZufNJ_JTihNs(dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: clearAllServiceIds$lambda-5 */
    public static final void m658clearAllServiceIds$lambda5(InternalSettingsFragment internalSettingsFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(internalSettingsFragment, "this$0");
        RecipientDatabase.debugClearServiceIds$default(SignalDatabase.Companion.recipients(), null, 1, null);
        Toast.makeText(internalSettingsFragment.getContext(), "Cleared all service IDs.", 0).show();
    }

    /* renamed from: clearAllServiceIds$lambda-6 */
    public static final void m659clearAllServiceIds$lambda6(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    public final void clearAllProfileKeys() {
        new MaterialAlertDialogBuilder(requireContext()).setTitle((CharSequence) "Clear all profile keys?").setMessage((CharSequence) "Are you sure? Never do this on a non-test device.").setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InternalSettingsFragment.m654$r8$lambda$uu2Jb_dzue60F6V2P5yKJ7Axo(InternalSettingsFragment.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda3
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                InternalSettingsFragment.m653$r8$lambda$umLHyMTkzTjtOXLDZriUB2a36w(dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: clearAllProfileKeys$lambda-7 */
    public static final void m656clearAllProfileKeys$lambda7(InternalSettingsFragment internalSettingsFragment, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(internalSettingsFragment, "this$0");
        RecipientDatabase.debugClearProfileData$default(SignalDatabase.Companion.recipients(), null, 1, null);
        Toast.makeText(internalSettingsFragment.getContext(), "Cleared all profile keys.", 0).show();
    }

    /* renamed from: clearAllProfileKeys$lambda-8 */
    public static final void m657clearAllProfileKeys$lambda8(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    public final void clearKeepLongerLogs() {
        SimpleTask.run(new SimpleTask.BackgroundTask() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda4
            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return InternalSettingsFragment.$r8$lambda$m8qVg_0ssqDyPeJ1uJnJh2ABpqw(InternalSettingsFragment.this);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsFragment$$ExternalSyntheticLambda5
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                InternalSettingsFragment.m652$r8$lambda$SzZynNFIIo9s2s9V99shmx0MU(InternalSettingsFragment.this, (Unit) obj);
            }
        });
    }

    /* renamed from: clearKeepLongerLogs$lambda-9 */
    public static final Unit m661clearKeepLongerLogs$lambda9(InternalSettingsFragment internalSettingsFragment) {
        Intrinsics.checkNotNullParameter(internalSettingsFragment, "this$0");
        LogDatabase.Companion companion = LogDatabase.Companion;
        Application application = internalSettingsFragment.requireActivity().getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "requireActivity().application");
        companion.getInstance(application).clearKeepLonger();
        return Unit.INSTANCE;
    }

    /* renamed from: clearKeepLongerLogs$lambda-10 */
    public static final void m660clearKeepLongerLogs$lambda10(InternalSettingsFragment internalSettingsFragment, Unit unit) {
        Intrinsics.checkNotNullParameter(internalSettingsFragment, "this$0");
        Toast.makeText(internalSettingsFragment.requireContext(), "Cleared keep longer logs", 0).show();
    }
}
