package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ChangeNumberEnterSmsCodeFragmentDirections {
    private ChangeNumberEnterSmsCodeFragmentDirections() {
    }

    public static NavDirections actionChangeNumberEnterCodeFragmentToCaptchaFragment() {
        return new ActionOnlyNavDirections(R.id.action_changeNumberEnterCodeFragment_to_captchaFragment);
    }

    public static ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock(long j) {
        return new ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock(j);
    }

    public static NavDirections actionChangeNumberEnterCodeFragmentToChangeNumberAccountLocked() {
        return new ActionOnlyNavDirections(R.id.action_changeNumberEnterCodeFragment_to_changeNumberAccountLocked);
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }

    /* loaded from: classes4.dex */
    public static class ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_changeNumberEnterCodeFragment_to_changeNumberRegistrationLock;
        }

        private ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("timeRemaining", Long.valueOf(j));
        }

        public ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock setTimeRemaining(long j) {
            this.arguments.put("timeRemaining", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("timeRemaining")) {
                bundle.putLong("timeRemaining", ((Long) this.arguments.get("timeRemaining")).longValue());
            }
            return bundle;
        }

        public long getTimeRemaining() {
            return ((Long) this.arguments.get("timeRemaining")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock = (ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock) obj;
            return this.arguments.containsKey("timeRemaining") == actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock.arguments.containsKey("timeRemaining") && getTimeRemaining() == actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock.getTimeRemaining() && getActionId() == actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock.getActionId();
        }

        public int hashCode() {
            return ((((int) (getTimeRemaining() ^ (getTimeRemaining() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock(actionId=" + getActionId() + "){timeRemaining=" + getTimeRemaining() + "}";
        }
    }
}
