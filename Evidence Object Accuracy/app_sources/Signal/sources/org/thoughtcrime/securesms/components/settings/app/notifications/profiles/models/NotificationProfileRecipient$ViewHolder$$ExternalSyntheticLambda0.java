package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileRecipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileRecipient$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfileRecipient.Model f$0;

    public /* synthetic */ NotificationProfileRecipient$ViewHolder$$ExternalSyntheticLambda0(NotificationProfileRecipient.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfileRecipient.ViewHolder.m819bind$lambda0(this.f$0, view);
    }
}
