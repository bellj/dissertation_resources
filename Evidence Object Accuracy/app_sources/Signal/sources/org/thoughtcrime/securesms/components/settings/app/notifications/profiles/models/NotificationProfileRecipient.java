package org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: NotificationProfileRecipient.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileRecipient;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileRecipient {
    public static final NotificationProfileRecipient INSTANCE = new NotificationProfileRecipient();

    private NotificationProfileRecipient() {
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m818register$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ViewHolder(view);
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.models.NotificationProfileRecipient$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return NotificationProfileRecipient.m818register$lambda0((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.notification_profile_recipient_list_item));
    }

    /* compiled from: NotificationProfileRecipient.kt */
    @Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\u0002\u0010\bJ\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0000H\u0016J\u0010\u0010\u0010\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0000H\u0016R\u001d\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileRecipient$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "recipientModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$Model;", "onRemoveClick", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "", "(Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$Model;Lkotlin/jvm/functions/Function1;)V", "getOnRemoveClick", "()Lkotlin/jvm/functions/Function1;", "getRecipientModel", "()Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$Model;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final Function1<RecipientId, Unit> onRemoveClick;
        private final RecipientPreference.Model recipientModel;

        /* JADX DEBUG: Multi-variable search result rejected for r11v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.recipients.RecipientId, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(RecipientPreference.Model model, Function1<? super RecipientId, Unit> function1) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(model, "recipientModel");
            Intrinsics.checkNotNullParameter(function1, "onRemoveClick");
            this.recipientModel = model;
            this.onRemoveClick = function1;
        }

        public final Function1<RecipientId, Unit> getOnRemoveClick() {
            return this.onRemoveClick;
        }

        public final RecipientPreference.Model getRecipientModel() {
            return this.recipientModel;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.recipientModel.getRecipient().getId(), model.recipientModel.getRecipient().getId());
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && this.recipientModel.areContentsTheSame(model.recipientModel);
        }
    }

    /* compiled from: NotificationProfileRecipient.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileRecipient$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/models/NotificationProfileRecipient$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "recipientViewHolder", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/RecipientPreference$ViewHolder;", "remove", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final RecipientPreference.ViewHolder recipientViewHolder;
        private final View remove;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.recipientViewHolder = new RecipientPreference.ViewHolder(view);
            View findViewById = findViewById(R.id.recipient_remove);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.recipient_remove)");
            this.remove = findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.recipientViewHolder.bind(model.getRecipientModel());
            this.remove.setOnClickListener(new NotificationProfileRecipient$ViewHolder$$ExternalSyntheticLambda0(model));
        }

        /* renamed from: bind$lambda-0 */
        public static final void m819bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            Function1<RecipientId, Unit> onRemoveClick = model.getOnRemoveClick();
            RecipientId id = model.getRecipientModel().getRecipient().getId();
            Intrinsics.checkNotNullExpressionValue(id, "model.recipientModel.recipient.id");
            onRemoveClick.invoke(id);
        }
    }
}
