package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.view.View;
import j$.util.function.Function;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Boost$Companion$$ExternalSyntheticLambda0 implements Function {
    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return Boost.Companion.m917register$lambda0((View) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
