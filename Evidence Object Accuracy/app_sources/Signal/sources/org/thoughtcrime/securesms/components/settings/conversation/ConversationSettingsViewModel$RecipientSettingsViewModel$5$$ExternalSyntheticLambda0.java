package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$5$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ boolean f$0;

    public /* synthetic */ ConversationSettingsViewModel$RecipientSettingsViewModel$5$$ExternalSyntheticLambda0(boolean z) {
        this.f$0 = z;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.RecipientSettingsViewModel.AnonymousClass5.m1158invoke$lambda0(this.f$0, (ConversationSettingsState) obj);
    }
}
