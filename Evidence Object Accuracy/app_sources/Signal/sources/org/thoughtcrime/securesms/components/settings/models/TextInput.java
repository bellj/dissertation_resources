package org.thoughtcrime.securesms.components.settings.models;

import android.content.Context;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import com.google.android.material.textfield.TextInputLayout;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.disposables.Disposable;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.EditTextUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.stories.Stories;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;
import org.thoughtcrime.securesms.util.text.AfterTextChanged;

/* compiled from: TextInput.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003\n\u000b\fB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/TextInput;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "events", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent;", "MultilineModel", "MultilineViewHolder", "TextInputEvent", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class TextInput {
    public static final TextInput INSTANCE = new TextInput();

    private TextInput() {
    }

    /* compiled from: TextInput.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent;", "", "()V", "OnEmojiEvent", "OnKeyEvent", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent$OnKeyEvent;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent$OnEmojiEvent;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class TextInputEvent {
        public /* synthetic */ TextInputEvent(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: TextInput.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent$OnKeyEvent;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent;", "keyEvent", "Landroid/view/KeyEvent;", "(Landroid/view/KeyEvent;)V", "getKeyEvent", "()Landroid/view/KeyEvent;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class OnKeyEvent extends TextInputEvent {
            private final KeyEvent keyEvent;

            public static /* synthetic */ OnKeyEvent copy$default(OnKeyEvent onKeyEvent, KeyEvent keyEvent, int i, Object obj) {
                if ((i & 1) != 0) {
                    keyEvent = onKeyEvent.keyEvent;
                }
                return onKeyEvent.copy(keyEvent);
            }

            public final KeyEvent component1() {
                return this.keyEvent;
            }

            public final OnKeyEvent copy(KeyEvent keyEvent) {
                Intrinsics.checkNotNullParameter(keyEvent, "keyEvent");
                return new OnKeyEvent(keyEvent);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof OnKeyEvent) && Intrinsics.areEqual(this.keyEvent, ((OnKeyEvent) obj).keyEvent);
            }

            public int hashCode() {
                return this.keyEvent.hashCode();
            }

            public String toString() {
                return "OnKeyEvent(keyEvent=" + this.keyEvent + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public OnKeyEvent(KeyEvent keyEvent) {
                super(null);
                Intrinsics.checkNotNullParameter(keyEvent, "keyEvent");
                this.keyEvent = keyEvent;
            }

            public final KeyEvent getKeyEvent() {
                return this.keyEvent;
            }
        }

        private TextInputEvent() {
        }

        /* compiled from: TextInput.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\t\u0010\u0007\u001a\u00020\u0003HÆ\u0003J\u0013\u0010\b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\t\u001a\u00020\n2\b\u0010\u000b\u001a\u0004\u0018\u00010\fHÖ\u0003J\t\u0010\r\u001a\u00020\u000eHÖ\u0001J\t\u0010\u000f\u001a\u00020\u0010HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent$OnEmojiEvent;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent;", "emoji", "", "(Ljava/lang/CharSequence;)V", "getEmoji", "()Ljava/lang/CharSequence;", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class OnEmojiEvent extends TextInputEvent {
            private final CharSequence emoji;

            public static /* synthetic */ OnEmojiEvent copy$default(OnEmojiEvent onEmojiEvent, CharSequence charSequence, int i, Object obj) {
                if ((i & 1) != 0) {
                    charSequence = onEmojiEvent.emoji;
                }
                return onEmojiEvent.copy(charSequence);
            }

            public final CharSequence component1() {
                return this.emoji;
            }

            public final OnEmojiEvent copy(CharSequence charSequence) {
                Intrinsics.checkNotNullParameter(charSequence, "emoji");
                return new OnEmojiEvent(charSequence);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                return (obj instanceof OnEmojiEvent) && Intrinsics.areEqual(this.emoji, ((OnEmojiEvent) obj).emoji);
            }

            public int hashCode() {
                return this.emoji.hashCode();
            }

            public String toString() {
                return "OnEmojiEvent(emoji=" + ((Object) this.emoji) + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public OnEmojiEvent(CharSequence charSequence) {
                super(null);
                Intrinsics.checkNotNullParameter(charSequence, "emoji");
                this.emoji = charSequence;
            }

            public final CharSequence getEmoji() {
                return this.emoji;
            }
        }
    }

    /* renamed from: register$lambda-0 */
    public static final MappingViewHolder m1259register$lambda0(Observable observable, View view) {
        Intrinsics.checkNotNullParameter(observable, "$events");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new MultilineViewHolder(view, observable);
    }

    public final void register(MappingAdapter mappingAdapter, Observable<TextInputEvent> observable) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        Intrinsics.checkNotNullParameter(observable, "events");
        mappingAdapter.registerFactory(MultilineModel.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.models.TextInput$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return TextInput.m1259register$lambda0(Observable.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_multiline_text_input));
    }

    /* compiled from: TextInput.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BC\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007\u0012\u0012\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0002\u0010\u000bJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0000H\u0016J\u0010\u0010\u0016\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0000H\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u001d\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001d\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/TextInput$MultilineModel;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", DraftDatabase.Draft.TEXT, "", "hint", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "onEmojiToggleClicked", "Lkotlin/Function1;", "Landroid/widget/EditText;", "", "onTextChanged", "(Ljava/lang/CharSequence;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;)V", "getHint", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getOnEmojiToggleClicked", "()Lkotlin/jvm/functions/Function1;", "getOnTextChanged", "getText", "()Ljava/lang/CharSequence;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MultilineModel implements MappingModel<MultilineModel> {
        private final DSLSettingsText hint;
        private final Function1<EditText, Unit> onEmojiToggleClicked;
        private final Function1<CharSequence, Unit> onTextChanged;
        private final CharSequence text;

        public boolean areItemsTheSame(MultilineModel multilineModel) {
            Intrinsics.checkNotNullParameter(multilineModel, "newItem");
            return true;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(MultilineModel multilineModel) {
            return MappingModel.CC.$default$getChangePayload(this, multilineModel);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: kotlin.jvm.functions.Function1<? super android.widget.EditText, kotlin.Unit> */
        /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: kotlin.jvm.functions.Function1<? super java.lang.CharSequence, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        public MultilineModel(CharSequence charSequence, DSLSettingsText dSLSettingsText, Function1<? super EditText, Unit> function1, Function1<? super CharSequence, Unit> function12) {
            Intrinsics.checkNotNullParameter(function1, "onEmojiToggleClicked");
            Intrinsics.checkNotNullParameter(function12, "onTextChanged");
            this.text = charSequence;
            this.hint = dSLSettingsText;
            this.onEmojiToggleClicked = function1;
            this.onTextChanged = function12;
        }

        public /* synthetic */ MultilineModel(CharSequence charSequence, DSLSettingsText dSLSettingsText, Function1 function1, Function1 function12, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(charSequence, (i & 2) != 0 ? null : dSLSettingsText, function1, function12);
        }

        public final CharSequence getText() {
            return this.text;
        }

        public final DSLSettingsText getHint() {
            return this.hint;
        }

        public final Function1<EditText, Unit> getOnEmojiToggleClicked() {
            return this.onEmojiToggleClicked;
        }

        public final Function1<CharSequence, Unit> getOnTextChanged() {
            return this.onTextChanged;
        }

        public boolean areContentsTheSame(MultilineModel multilineModel) {
            Intrinsics.checkNotNullParameter(multilineModel, "newItem");
            return Intrinsics.areEqual(this.text, multilineModel.text);
        }
    }

    /* compiled from: TextInput.kt */
    @Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0002H\u0016J\b\u0010\u0016\u001a\u00020\u0014H\u0016J\b\u0010\u0017\u001a\u00020\u0014H\u0016R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0012X\u000e¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/TextInput$MultilineViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$MultilineModel;", "itemView", "Landroid/view/View;", "events", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/components/settings/models/TextInput$TextInputEvent;", "(Landroid/view/View;Lio/reactivex/rxjava3/core/Observable;)V", "emojiToggle", "Landroid/widget/ImageView;", "eventDisposable", "Lio/reactivex/rxjava3/disposables/Disposable;", "input", "Landroid/widget/EditText;", "inputLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "textChangedListener", "Lorg/thoughtcrime/securesms/util/text/AfterTextChanged;", "bind", "", "model", "onAttachedToWindow", "onDetachedFromWindow", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MultilineViewHolder extends MappingViewHolder<MultilineModel> {
        private final ImageView emojiToggle;
        private Disposable eventDisposable;
        private final Observable<TextInputEvent> events;
        private final EditText input;
        private final TextInputLayout inputLayout;
        private AfterTextChanged textChangedListener;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public MultilineViewHolder(View view, Observable<TextInputEvent> observable) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(observable, "events");
            this.events = observable;
            View findViewById = view.findViewById(R.id.input_layout);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.input_layout)");
            this.inputLayout = (TextInputLayout) findViewById;
            View findViewById2 = view.findViewById(R.id.input);
            EditText editText = (EditText) findViewById2;
            EditTextUtil.addGraphemeClusterLimitFilter(editText, Stories.MAX_BODY_SIZE);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById<Ed…itFilter(this, 700)\n    }");
            this.input = editText;
            View findViewById3 = view.findViewById(R.id.emoji_toggle);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.emoji_toggle)");
            this.emojiToggle = (ImageView) findViewById3;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onAttachedToWindow() {
            this.eventDisposable = this.events.subscribe(new TextInput$MultilineViewHolder$$ExternalSyntheticLambda2(this));
        }

        /* renamed from: onAttachedToWindow$lambda-1 */
        public static final void m1263onAttachedToWindow$lambda1(MultilineViewHolder multilineViewHolder, TextInputEvent textInputEvent) {
            Intrinsics.checkNotNullParameter(multilineViewHolder, "this$0");
            if (textInputEvent instanceof TextInputEvent.OnEmojiEvent) {
                multilineViewHolder.input.append(((TextInputEvent.OnEmojiEvent) textInputEvent).getEmoji());
            } else if (textInputEvent instanceof TextInputEvent.OnKeyEvent) {
                multilineViewHolder.input.dispatchKeyEvent(((TextInputEvent.OnKeyEvent) textInputEvent).getKeyEvent());
            }
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder
        public void onDetachedFromWindow() {
            Disposable disposable = this.eventDisposable;
            if (disposable != null) {
                disposable.dispose();
            }
        }

        public void bind(MultilineModel multilineModel) {
            CharSequence charSequence;
            Intrinsics.checkNotNullParameter(multilineModel, "model");
            TextInputLayout textInputLayout = this.inputLayout;
            DSLSettingsText hint = multilineModel.getHint();
            if (hint != null) {
                Context context = this.context;
                Intrinsics.checkNotNullExpressionValue(context, "context");
                charSequence = hint.resolve(context);
            } else {
                charSequence = null;
            }
            textInputLayout.setHint(charSequence);
            AfterTextChanged afterTextChanged = this.textChangedListener;
            if (afterTextChanged != null) {
                this.input.removeTextChangedListener(afterTextChanged);
            }
            if (!Intrinsics.areEqual(String.valueOf(multilineModel.getText()), this.input.getText().toString())) {
                this.input.setText(multilineModel.getText());
            }
            AfterTextChanged afterTextChanged2 = new AfterTextChanged(new TextInput$MultilineViewHolder$$ExternalSyntheticLambda0(multilineModel));
            this.textChangedListener = afterTextChanged2;
            this.input.addTextChangedListener(afterTextChanged2);
            this.emojiToggle.setOnClickListener(new TextInput$MultilineViewHolder$$ExternalSyntheticLambda1(multilineModel, this));
        }

        /* renamed from: bind$lambda-2 */
        public static final void m1261bind$lambda2(MultilineModel multilineModel, Editable editable) {
            Intrinsics.checkNotNullParameter(multilineModel, "$model");
            multilineModel.getOnTextChanged().invoke(editable.toString());
        }

        /* renamed from: bind$lambda-3 */
        public static final void m1262bind$lambda3(MultilineModel multilineModel, MultilineViewHolder multilineViewHolder, View view) {
            Intrinsics.checkNotNullParameter(multilineModel, "$model");
            Intrinsics.checkNotNullParameter(multilineViewHolder, "this$0");
            multilineModel.getOnEmojiToggleClicked().invoke(multilineViewHolder.input);
        }
    }
}
