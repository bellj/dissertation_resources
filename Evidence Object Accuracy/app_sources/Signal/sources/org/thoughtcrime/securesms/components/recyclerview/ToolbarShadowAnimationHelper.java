package org.thoughtcrime.securesms.components.recyclerview;

import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: ToolbarShadowAnimationHelper.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0014J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/recyclerview/ToolbarShadowAnimationHelper;", "Lorg/thoughtcrime/securesms/components/recyclerview/OnScrollAnimationHelper;", "toolbarShadow", "Landroid/view/View;", "(Landroid/view/View;)V", "hide", "", "duration", "", "show", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class ToolbarShadowAnimationHelper extends OnScrollAnimationHelper {
    private final View toolbarShadow;

    public ToolbarShadowAnimationHelper(View view) {
        Intrinsics.checkNotNullParameter(view, "toolbarShadow");
        this.toolbarShadow = view;
    }

    @Override // org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper
    protected void show(long j) {
        this.toolbarShadow.animate().setDuration(j).alpha(1.0f);
    }

    @Override // org.thoughtcrime.securesms.components.recyclerview.OnScrollAnimationHelper
    protected void hide(long j) {
        this.toolbarShadow.animate().setDuration(j).alpha(0.0f);
    }
}
