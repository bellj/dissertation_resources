package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import java.util.Currency;
import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: SubscribeState.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0019\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001:\u0001+BS\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u000b\u0012\b\b\u0002\u0010\f\u001a\u00020\r\u0012\b\b\u0002\u0010\u000e\u001a\u00020\u000b¢\u0006\u0002\u0010\u000fJ\t\u0010\u001c\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u0006HÆ\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\tHÆ\u0003J\t\u0010 \u001a\u00020\u000bHÆ\u0003J\t\u0010!\u001a\u00020\rHÆ\u0003J\t\u0010\"\u001a\u00020\u000bHÆ\u0003JY\u0010#\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000bHÆ\u0001J\u0013\u0010$\u001a\u00020\u000b2\b\u0010%\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010&\u001a\u00020'HÖ\u0001J\u0006\u0010(\u001a\u00020\u000bJ\t\u0010)\u001a\u00020*HÖ\u0001R\u0013\u0010\b\u001a\u0004\u0018\u00010\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u000e\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\n\u001a\u00020\u000b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u0015R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001b¨\u0006,"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState;", "", "currencySelection", "Ljava/util/Currency;", "subscriptions", "", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "selectedSubscription", "activeSubscription", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "isGooglePayAvailable", "", "stage", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState$Stage;", "hasInProgressSubscriptionTransaction", "(Ljava/util/Currency;Ljava/util/List;Lorg/thoughtcrime/securesms/subscription/Subscription;Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;ZLorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState$Stage;Z)V", "getActiveSubscription", "()Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription;", "getCurrencySelection", "()Ljava/util/Currency;", "getHasInProgressSubscriptionTransaction", "()Z", "getSelectedSubscription", "()Lorg/thoughtcrime/securesms/subscription/Subscription;", "getStage", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState$Stage;", "getSubscriptions", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "isSubscriptionExpiring", "toString", "", "Stage", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeState {
    private final ActiveSubscription activeSubscription;
    private final Currency currencySelection;
    private final boolean hasInProgressSubscriptionTransaction;
    private final boolean isGooglePayAvailable;
    private final Subscription selectedSubscription;
    private final Stage stage;
    private final List<Subscription> subscriptions;

    /* compiled from: SubscribeState.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\b\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/subscribe/SubscribeState$Stage;", "", "(Ljava/lang/String;I)V", "INIT", "READY", "TOKEN_REQUEST", "PAYMENT_PIPELINE", "CANCELLING", "FAILURE", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Stage {
        INIT,
        READY,
        TOKEN_REQUEST,
        PAYMENT_PIPELINE,
        CANCELLING,
        FAILURE
    }

    /* JADX DEBUG: Multi-variable search result rejected for r5v0, resolved type: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState */
    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ SubscribeState copy$default(SubscribeState subscribeState, Currency currency, List list, Subscription subscription, ActiveSubscription activeSubscription, boolean z, Stage stage, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            currency = subscribeState.currencySelection;
        }
        if ((i & 2) != 0) {
            list = subscribeState.subscriptions;
        }
        if ((i & 4) != 0) {
            subscription = subscribeState.selectedSubscription;
        }
        if ((i & 8) != 0) {
            activeSubscription = subscribeState.activeSubscription;
        }
        if ((i & 16) != 0) {
            z = subscribeState.isGooglePayAvailable;
        }
        if ((i & 32) != 0) {
            stage = subscribeState.stage;
        }
        if ((i & 64) != 0) {
            z2 = subscribeState.hasInProgressSubscriptionTransaction;
        }
        return subscribeState.copy(currency, list, subscription, activeSubscription, z, stage, z2);
    }

    public final Currency component1() {
        return this.currencySelection;
    }

    public final List<Subscription> component2() {
        return this.subscriptions;
    }

    public final Subscription component3() {
        return this.selectedSubscription;
    }

    public final ActiveSubscription component4() {
        return this.activeSubscription;
    }

    public final boolean component5() {
        return this.isGooglePayAvailable;
    }

    public final Stage component6() {
        return this.stage;
    }

    public final boolean component7() {
        return this.hasInProgressSubscriptionTransaction;
    }

    public final SubscribeState copy(Currency currency, List<Subscription> list, Subscription subscription, ActiveSubscription activeSubscription, boolean z, Stage stage, boolean z2) {
        Intrinsics.checkNotNullParameter(currency, "currencySelection");
        Intrinsics.checkNotNullParameter(list, "subscriptions");
        Intrinsics.checkNotNullParameter(stage, "stage");
        return new SubscribeState(currency, list, subscription, activeSubscription, z, stage, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SubscribeState)) {
            return false;
        }
        SubscribeState subscribeState = (SubscribeState) obj;
        return Intrinsics.areEqual(this.currencySelection, subscribeState.currencySelection) && Intrinsics.areEqual(this.subscriptions, subscribeState.subscriptions) && Intrinsics.areEqual(this.selectedSubscription, subscribeState.selectedSubscription) && Intrinsics.areEqual(this.activeSubscription, subscribeState.activeSubscription) && this.isGooglePayAvailable == subscribeState.isGooglePayAvailable && this.stage == subscribeState.stage && this.hasInProgressSubscriptionTransaction == subscribeState.hasInProgressSubscriptionTransaction;
    }

    public int hashCode() {
        int hashCode = ((this.currencySelection.hashCode() * 31) + this.subscriptions.hashCode()) * 31;
        Subscription subscription = this.selectedSubscription;
        int i = 0;
        int hashCode2 = (hashCode + (subscription == null ? 0 : subscription.hashCode())) * 31;
        ActiveSubscription activeSubscription = this.activeSubscription;
        if (activeSubscription != null) {
            i = activeSubscription.hashCode();
        }
        int i2 = (hashCode2 + i) * 31;
        boolean z = this.isGooglePayAvailable;
        int i3 = 1;
        if (z) {
            z = true;
        }
        int i4 = z ? 1 : 0;
        int i5 = z ? 1 : 0;
        int i6 = z ? 1 : 0;
        int hashCode3 = (((i2 + i4) * 31) + this.stage.hashCode()) * 31;
        boolean z2 = this.hasInProgressSubscriptionTransaction;
        if (!z2) {
            i3 = z2 ? 1 : 0;
        }
        return hashCode3 + i3;
    }

    public String toString() {
        return "SubscribeState(currencySelection=" + this.currencySelection + ", subscriptions=" + this.subscriptions + ", selectedSubscription=" + this.selectedSubscription + ", activeSubscription=" + this.activeSubscription + ", isGooglePayAvailable=" + this.isGooglePayAvailable + ", stage=" + this.stage + ", hasInProgressSubscriptionTransaction=" + this.hasInProgressSubscriptionTransaction + ')';
    }

    public SubscribeState(Currency currency, List<Subscription> list, Subscription subscription, ActiveSubscription activeSubscription, boolean z, Stage stage, boolean z2) {
        Intrinsics.checkNotNullParameter(currency, "currencySelection");
        Intrinsics.checkNotNullParameter(list, "subscriptions");
        Intrinsics.checkNotNullParameter(stage, "stage");
        this.currencySelection = currency;
        this.subscriptions = list;
        this.selectedSubscription = subscription;
        this.activeSubscription = activeSubscription;
        this.isGooglePayAvailable = z;
        this.stage = stage;
        this.hasInProgressSubscriptionTransaction = z2;
    }

    public final Currency getCurrencySelection() {
        return this.currencySelection;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ SubscribeState(java.util.Currency r8, java.util.List r9, org.thoughtcrime.securesms.subscription.Subscription r10, org.whispersystems.signalservice.api.subscriptions.ActiveSubscription r11, boolean r12, org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState.Stage r13, boolean r14, int r15, kotlin.jvm.internal.DefaultConstructorMarker r16) {
        /*
            r7 = this;
            r0 = r15 & 2
            if (r0 == 0) goto L_0x0009
            java.util.List r0 = kotlin.collections.CollectionsKt.emptyList()
            goto L_0x000a
        L_0x0009:
            r0 = r9
        L_0x000a:
            r1 = r15 & 4
            r2 = 0
            if (r1 == 0) goto L_0x0011
            r1 = r2
            goto L_0x0012
        L_0x0011:
            r1 = r10
        L_0x0012:
            r3 = r15 & 8
            if (r3 == 0) goto L_0x0017
            goto L_0x0018
        L_0x0017:
            r2 = r11
        L_0x0018:
            r3 = r15 & 16
            r4 = 0
            if (r3 == 0) goto L_0x001f
            r3 = 0
            goto L_0x0020
        L_0x001f:
            r3 = r12
        L_0x0020:
            r5 = r15 & 32
            if (r5 == 0) goto L_0x0027
            org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState$Stage r5 = org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState.Stage.INIT
            goto L_0x0028
        L_0x0027:
            r5 = r13
        L_0x0028:
            r6 = r15 & 64
            if (r6 == 0) goto L_0x002d
            goto L_0x002e
        L_0x002d:
            r4 = r14
        L_0x002e:
            r9 = r7
            r10 = r8
            r11 = r0
            r12 = r1
            r13 = r2
            r14 = r3
            r15 = r5
            r16 = r4
            r9.<init>(r10, r11, r12, r13, r14, r15, r16)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState.<init>(java.util.Currency, java.util.List, org.thoughtcrime.securesms.subscription.Subscription, org.whispersystems.signalservice.api.subscriptions.ActiveSubscription, boolean, org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState$Stage, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final List<Subscription> getSubscriptions() {
        return this.subscriptions;
    }

    public final Subscription getSelectedSubscription() {
        return this.selectedSubscription;
    }

    public final ActiveSubscription getActiveSubscription() {
        return this.activeSubscription;
    }

    public final boolean isGooglePayAvailable() {
        return this.isGooglePayAvailable;
    }

    public final Stage getStage() {
        return this.stage;
    }

    public final boolean getHasInProgressSubscriptionTransaction() {
        return this.hasInProgressSubscriptionTransaction;
    }

    public final boolean isSubscriptionExpiring() {
        ActiveSubscription activeSubscription = this.activeSubscription;
        return (activeSubscription != null && activeSubscription.isActive()) && this.activeSubscription.getActiveSubscription().willCancelAtPeriodEnd();
    }
}
