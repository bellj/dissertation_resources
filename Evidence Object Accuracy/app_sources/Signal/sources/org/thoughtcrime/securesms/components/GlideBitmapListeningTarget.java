package org.thoughtcrime.securesms.components;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class GlideBitmapListeningTarget extends BitmapImageViewTarget {
    private final SettableFuture<Boolean> loaded;

    public GlideBitmapListeningTarget(ImageView imageView, SettableFuture<Boolean> settableFuture) {
        super(imageView);
        this.loaded = settableFuture;
    }

    @Override // com.bumptech.glide.request.target.BitmapImageViewTarget
    public void setResource(Bitmap bitmap) {
        super.setResource(bitmap);
        this.loaded.set(Boolean.TRUE);
    }

    @Override // com.bumptech.glide.request.target.ImageViewTarget, com.bumptech.glide.request.target.BaseTarget, com.bumptech.glide.request.target.Target
    public void onLoadFailed(Drawable drawable) {
        super.onLoadFailed(drawable);
        this.loaded.set(Boolean.TRUE);
    }
}
