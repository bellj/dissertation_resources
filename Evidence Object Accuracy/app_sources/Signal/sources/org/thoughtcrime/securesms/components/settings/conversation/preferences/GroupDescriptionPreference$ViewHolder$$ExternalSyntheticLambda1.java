package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import org.thoughtcrime.securesms.components.settings.conversation.preferences.GroupDescriptionPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupDescriptionPreference$ViewHolder$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ GroupDescriptionPreference.Model f$0;

    public /* synthetic */ GroupDescriptionPreference$ViewHolder$$ExternalSyntheticLambda1(GroupDescriptionPreference.Model model) {
        this.f$0 = model;
    }

    @Override // java.lang.Runnable
    public final void run() {
        GroupDescriptionPreference.ViewHolder.m1205bind$lambda1(this.f$0);
    }
}
