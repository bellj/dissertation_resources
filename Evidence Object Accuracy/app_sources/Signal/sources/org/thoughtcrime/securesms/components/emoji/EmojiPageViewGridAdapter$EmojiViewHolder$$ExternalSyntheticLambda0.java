package org.thoughtcrime.securesms.components.emoji;

import android.view.View;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EmojiPageViewGridAdapter$EmojiViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ EmojiPageViewGridAdapter.EmojiViewHolder f$0;
    public final /* synthetic */ EmojiPageViewGridAdapter.EmojiModel f$1;

    public /* synthetic */ EmojiPageViewGridAdapter$EmojiViewHolder$$ExternalSyntheticLambda0(EmojiPageViewGridAdapter.EmojiViewHolder emojiViewHolder, EmojiPageViewGridAdapter.EmojiModel emojiModel) {
        this.f$0 = emojiViewHolder;
        this.f$1 = emojiModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
