package org.thoughtcrime.securesms.components.settings.app.internal.donor;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

/* compiled from: DonorErrorConfigurationViewModel.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0003"}, d2 = {"<anonymous>", "Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationState;", "it", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonorErrorConfigurationViewModel$clear$1$2 extends Lambda implements Function1<DonorErrorConfigurationState, DonorErrorConfigurationState> {
    public static final DonorErrorConfigurationViewModel$clear$1$2 INSTANCE = new DonorErrorConfigurationViewModel$clear$1$2();

    DonorErrorConfigurationViewModel$clear$1$2() {
        super(1);
    }

    public final DonorErrorConfigurationState invoke(DonorErrorConfigurationState donorErrorConfigurationState) {
        Intrinsics.checkNotNullParameter(donorErrorConfigurationState, "it");
        return DonorErrorConfigurationState.copy$default(donorErrorConfigurationState, null, null, null, null, 1, null);
    }
}
