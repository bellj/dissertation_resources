package org.thoughtcrime.securesms.components.settings.app.subscription.subscribe;

import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.subjects.PublishSubject;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationEvent;
import org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeState;
import org.thoughtcrime.securesms.subscription.Subscription;

/* compiled from: SubscribeViewModel.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u0002\n\u0000\u0010\u0000\u001a\u00020\u0001H\n¢\u0006\u0002\b\u0002"}, d2 = {"<anonymous>", "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SubscribeViewModel$updateSubscription$3 extends Lambda implements Function0<Unit> {
    final /* synthetic */ SubscribeViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SubscribeViewModel$updateSubscription$3(SubscribeViewModel subscribeViewModel) {
        super(0);
        this.this$0 = subscribeViewModel;
    }

    /* renamed from: invoke$lambda-0 */
    public static final SubscribeState m1076invoke$lambda0(SubscribeState subscribeState) {
        Intrinsics.checkNotNullExpressionValue(subscribeState, "it");
        return SubscribeState.copy$default(subscribeState, null, null, null, null, false, SubscribeState.Stage.READY, false, 95, null);
    }

    @Override // kotlin.jvm.functions.Function0
    public final void invoke() {
        this.this$0.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.subscribe.SubscribeViewModel$updateSubscription$3$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SubscribeViewModel$updateSubscription$3.m1076invoke$lambda0((SubscribeState) obj);
            }
        });
        PublishSubject publishSubject = this.this$0.eventPublisher;
        Subscription selectedSubscription = ((SubscribeState) this.this$0.store.getState()).getSelectedSubscription();
        Intrinsics.checkNotNull(selectedSubscription);
        publishSubject.onNext(new DonationEvent.PaymentConfirmationSuccess(selectedSubscription.getBadge()));
    }
}
