package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ActiveSubscriptionPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ActiveSubscriptionPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ActiveSubscriptionPreference.Model f$0;

    public /* synthetic */ ActiveSubscriptionPreference$ViewHolder$$ExternalSyntheticLambda0(ActiveSubscriptionPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ActiveSubscriptionPreference.ViewHolder.m965presentRedemptionFailureState$lambda1(this.f$0, view);
    }
}
