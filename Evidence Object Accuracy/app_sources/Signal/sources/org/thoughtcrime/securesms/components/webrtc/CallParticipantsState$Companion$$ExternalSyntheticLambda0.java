package org.thoughtcrime.securesms.components.webrtc;

import j$.util.function.Function;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsState;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.service.webrtc.state.WebRtcEphemeralState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class CallParticipantsState$Companion$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ WebRtcEphemeralState f$0;

    public /* synthetic */ CallParticipantsState$Companion$$ExternalSyntheticLambda0(WebRtcEphemeralState webRtcEphemeralState) {
        this.f$0 = webRtcEphemeralState;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return CallParticipantsState.Companion.m1278update$lambda0(this.f$0, (CallParticipant) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
