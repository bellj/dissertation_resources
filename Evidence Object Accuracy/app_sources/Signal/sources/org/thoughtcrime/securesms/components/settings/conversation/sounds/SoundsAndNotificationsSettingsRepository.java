package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import android.content.Context;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: SoundsAndNotificationsSettingsRepository.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0014\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\u00060\bJ\"\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u000e\u0012\u0004\u0012\u00020\u00060\rJ\u0016\u0010\u000f\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011J\u0016\u0010\u0012\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\u0014R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "ensureCustomChannelConsistency", "", "complete", "Lkotlin/Function0;", "hasCustomNotificationSettings", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "consumer", "Lkotlin/Function1;", "", "setMentionSetting", "mentionSetting", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "setMuteUntil", "muteUntil", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SoundsAndNotificationsSettingsRepository {
    private final Context context;

    public SoundsAndNotificationsSettingsRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final void ensureCustomChannelConsistency(Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(function0, "complete");
        SignalExecutors.BOUNDED.execute(new Runnable(function0) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Function0 f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SoundsAndNotificationsSettingsRepository.$r8$lambda$pJiGtg9zgOUfVIrqHZI7dFfVQT0(SoundsAndNotificationsSettingsRepository.this, this.f$1);
            }
        });
    }

    /* renamed from: ensureCustomChannelConsistency$lambda-0 */
    public static final void m1223ensureCustomChannelConsistency$lambda0(SoundsAndNotificationsSettingsRepository soundsAndNotificationsSettingsRepository, Function0 function0) {
        Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(function0, "$complete");
        if (NotificationChannels.supported()) {
            NotificationChannels.ensureCustomChannelConsistency(soundsAndNotificationsSettingsRepository.context);
        }
        function0.invoke();
    }

    public final void setMuteUntil(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        SignalExecutors.BOUNDED.execute(new Runnable(j) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SoundsAndNotificationsSettingsRepository.m1222$r8$lambda$ws94LRs0IuiHIL4jYPqqNtzEtU(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: setMuteUntil$lambda-1 */
    public static final void m1226setMuteUntil$lambda1(RecipientId recipientId, long j) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        SignalDatabase.Companion.recipients().setMuted(recipientId, j);
    }

    public final void setMentionSetting(RecipientId recipientId, RecipientDatabase.MentionSetting mentionSetting) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        SignalExecutors.BOUNDED.execute(new Runnable(mentionSetting) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ RecipientDatabase.MentionSetting f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SoundsAndNotificationsSettingsRepository.m1221$r8$lambda$hXIhC_vStLS4y9ybxKLtQEDFrg(RecipientId.this, this.f$1);
            }
        });
    }

    /* renamed from: setMentionSetting$lambda-2 */
    public static final void m1225setMentionSetting$lambda2(RecipientId recipientId, RecipientDatabase.MentionSetting mentionSetting) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(mentionSetting, "$mentionSetting");
        SignalDatabase.Companion.recipients().setMentionSetting(recipientId, mentionSetting);
    }

    public final void hasCustomNotificationSettings(RecipientId recipientId, Function1<? super Boolean, Unit> function1) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable(function1, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ Function1 f$1;
            public final /* synthetic */ SoundsAndNotificationsSettingsRepository f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SoundsAndNotificationsSettingsRepository.m1220$r8$lambda$Q58duzo9dMKNNfZ9YqolyQnUw(RecipientId.this, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: hasCustomNotificationSettings$lambda-3 */
    public static final void m1224hasCustomNotificationSettings$lambda3(RecipientId recipientId, Function1 function1, SoundsAndNotificationsSettingsRepository soundsAndNotificationsSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsRepository, "this$0");
        Recipient resolved = Recipient.resolved(recipientId);
        Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
        function1.invoke(Boolean.valueOf((resolved.getNotificationChannel() != null || !NotificationChannels.supported()) ? true : NotificationChannels.updateWithShortcutBasedChannel(soundsAndNotificationsSettingsRepository.context, resolved)));
    }
}
