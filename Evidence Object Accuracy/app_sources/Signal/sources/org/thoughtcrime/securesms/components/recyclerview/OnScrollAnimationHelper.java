package org.thoughtcrime.securesms.components.recyclerview;

import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: OnScrollAnimationHelper.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\b&\u0018\u00002\u00020\u0001:\u0001\u0014B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\u000bH\u0014J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0004H$J \u0010\u000e\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0010H\u0016J\u000e\u0010\u0012\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000bJ\u0010\u0010\u0013\u001a\u00020\r2\u0006\u0010\u0003\u001a\u00020\u0004H$R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/recyclerview/OnScrollAnimationHelper;", "Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;", "()V", "duration", "", "getDuration", "()J", "lastAnimationState", "Lorg/thoughtcrime/securesms/components/recyclerview/OnScrollAnimationHelper$AnimationState;", "getAnimationState", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "hide", "", "onScrolled", "dx", "", "dy", "setImmediateState", "show", "AnimationState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class OnScrollAnimationHelper extends RecyclerView.OnScrollListener {
    private final long duration = 250;
    private AnimationState lastAnimationState = AnimationState.NONE;

    /* compiled from: OnScrollAnimationHelper.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/recyclerview/OnScrollAnimationHelper$AnimationState;", "", "(Ljava/lang/String;I)V", "NONE", "HIDE", "SHOW", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum AnimationState {
        NONE,
        HIDE,
        SHOW
    }

    /* compiled from: OnScrollAnimationHelper.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[AnimationState.values().length];
            iArr[AnimationState.NONE.ordinal()] = 1;
            iArr[AnimationState.HIDE.ordinal()] = 2;
            iArr[AnimationState.SHOW.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    protected abstract void hide(long j);

    protected abstract void show(long j);

    protected long getDuration() {
        return this.duration;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.OnScrollListener
    public void onScrolled(RecyclerView recyclerView, int i, int i2) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        AnimationState animationState = getAnimationState(recyclerView);
        AnimationState animationState2 = this.lastAnimationState;
        if (animationState != animationState2) {
            if (animationState2 == AnimationState.NONE) {
                setImmediateState(recyclerView);
                return;
            }
            int i3 = WhenMappings.$EnumSwitchMapping$0[animationState.ordinal()];
            if (i3 != 1) {
                if (i3 == 2) {
                    hide(getDuration());
                } else if (i3 == 3) {
                    show(getDuration());
                }
                this.lastAnimationState = animationState;
                return;
            }
            throw new AssertionError();
        }
    }

    public final void setImmediateState(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        AnimationState animationState = getAnimationState(recyclerView);
        int i = WhenMappings.$EnumSwitchMapping$0[animationState.ordinal()];
        if (i != 1) {
            if (i == 2) {
                hide(0);
            } else if (i == 3) {
                show(0);
            }
            this.lastAnimationState = animationState;
            return;
        }
        throw new AssertionError();
    }

    protected AnimationState getAnimationState(RecyclerView recyclerView) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        return recyclerView.canScrollVertically(-1) ? AnimationState.SHOW : AnimationState.HIDE;
    }
}
