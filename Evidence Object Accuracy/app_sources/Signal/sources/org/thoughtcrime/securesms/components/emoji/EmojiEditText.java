package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatEditText;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiProvider;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class EmojiEditText extends AppCompatEditText {
    private static final String TAG = Log.tag(EmojiEditText.class);

    public EmojiEditText(Context context) {
        this(context, null);
    }

    public EmojiEditText(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.editTextStyle);
    }

    public EmojiEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.EmojiTextView, 0, 0);
        boolean z = obtainStyledAttributes.getBoolean(0, false);
        boolean z2 = obtainStyledAttributes.getBoolean(1, false);
        obtainStyledAttributes.recycle();
        if (isInEditMode()) {
            return;
        }
        if (z || !SignalStore.settings().isPreferSystemEmoji()) {
            setFilters(appendEmojiFilter(getFilters(), z2));
        }
    }

    public void insertEmoji(String str) {
        int selectionStart = getSelectionStart();
        int selectionEnd = getSelectionEnd();
        getText().replace(Math.min(selectionStart, selectionEnd), Math.max(selectionStart, selectionEnd), str);
        setSelection(selectionStart + str.length());
    }

    @Override // android.widget.TextView, android.graphics.drawable.Drawable.Callback, android.view.View
    public void invalidateDrawable(Drawable drawable) {
        if (drawable instanceof EmojiProvider.EmojiDrawable) {
            invalidate();
        } else {
            super.invalidateDrawable(drawable);
        }
    }

    private InputFilter[] appendEmojiFilter(InputFilter[] inputFilterArr, boolean z) {
        InputFilter[] inputFilterArr2;
        if (inputFilterArr != null) {
            inputFilterArr2 = new InputFilter[inputFilterArr.length + 1];
            System.arraycopy(inputFilterArr, 0, inputFilterArr2, 1, inputFilterArr.length);
        } else {
            inputFilterArr2 = new InputFilter[1];
        }
        inputFilterArr2[0] = new EmojiFilter(this, z);
        return inputFilterArr2;
    }
}
