package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.Boost;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Boost$SelectionViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Boost.SelectionModel f$0;
    public final /* synthetic */ Boost f$1;
    public final /* synthetic */ Boost.SelectionViewHolder f$2;

    public /* synthetic */ Boost$SelectionViewHolder$$ExternalSyntheticLambda0(Boost.SelectionModel selectionModel, Boost boost, Boost.SelectionViewHolder selectionViewHolder) {
        this.f$0 = selectionModel;
        this.f$1 = boost;
        this.f$2 = selectionViewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        Boost.SelectionViewHolder.m920bind$lambda1$lambda0(this.f$0, this.f$1, this.f$2, view);
    }
}
