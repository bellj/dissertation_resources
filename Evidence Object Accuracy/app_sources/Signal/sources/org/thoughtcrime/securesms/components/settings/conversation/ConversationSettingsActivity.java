package org.thoughtcrime.securesms.components.settings.conversation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsActivity;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragmentArgs;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.ParcelableGroupId;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.DynamicConversationSettingsTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* compiled from: ConversationSettingsActivity.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u0000 \u00102\u00020\u00012\u00020\u0002:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\b\u001a\u00020\tH\u0016J\b\u0010\n\u001a\u00020\tH\u0016J\u001a\u0010\u000b\u001a\u00020\t2\b\u0010\f\u001a\u0004\u0018\u00010\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0014R\u0014\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsActivity;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsActivity;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsFragment$Callback;", "()V", "dynamicTheme", "Lorg/thoughtcrime/securesms/util/DynamicTheme;", "getDynamicTheme", "()Lorg/thoughtcrime/securesms/util/DynamicTheme;", "finish", "", "onContentWillRender", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ConversationSettingsActivity extends DSLSettingsActivity implements ConversationSettingsFragment.Callback {
    public static final Companion Companion = new Companion(null);
    private final DynamicTheme dynamicTheme = new DynamicConversationSettingsTheme();

    @JvmStatic
    public static final Bundle createTransitionBundle(Context context, View view) {
        return Companion.createTransitionBundle(context, view);
    }

    @JvmStatic
    public static final Bundle createTransitionBundle(Context context, View view, View view2) {
        return Companion.createTransitionBundle(context, view, view2);
    }

    @JvmStatic
    public static final Intent forGroup(Context context, GroupId groupId) {
        return Companion.forGroup(context, groupId);
    }

    @JvmStatic
    public static final Intent forRecipient(Context context, RecipientId recipientId) {
        return Companion.forRecipient(context, recipientId);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsActivity
    protected DynamicTheme getDynamicTheme() {
        return this.dynamicTheme;
    }

    /* access modifiers changed from: protected */
    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        ActivityCompat.postponeEnterTransition(this);
        super.onCreate(bundle, z);
    }

    @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment.Callback
    public void onContentWillRender() {
        ActivityCompat.startPostponedEnterTransition(this);
    }

    @Override // android.app.Activity
    public void finish() {
        super.finish();
        overridePendingTransition(0, R.anim.slide_fade_to_bottom);
    }

    /* compiled from: ConversationSettingsActivity.kt */
    @Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\"\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\bH\u0007J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\rH\u0007J\u0018\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000f\u001a\u00020\u0010H\u0007J\u0010\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0005\u001a\u00020\u0006H\u0002¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsActivity$Companion;", "", "()V", "createTransitionBundle", "Landroid/os/Bundle;", "context", "Landroid/content/Context;", "avatar", "Landroid/view/View;", "windowContent", "forGroup", "Landroid/content/Intent;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "forRecipient", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getIntent", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final Bundle createTransitionBundle(Context context, View view, View view2) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(view, "avatar");
            Intrinsics.checkNotNullParameter(view2, "windowContent");
            if (context instanceof Activity) {
                return ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, Pair.create(view, "avatar"), Pair.create(view2, "window_content")).toBundle();
            }
            return null;
        }

        @JvmStatic
        public final Bundle createTransitionBundle(Context context, View view) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(view, "avatar");
            if (context instanceof Activity) {
                return ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context, view, "avatar").toBundle();
            }
            return null;
        }

        @JvmStatic
        public final Intent forGroup(Context context, GroupId groupId) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Bundle bundle = new ConversationSettingsFragmentArgs.Builder(null, ParcelableGroupId.from(groupId)).build().toBundle();
            Intrinsics.checkNotNullExpressionValue(bundle, "Builder(null, Parcelable…ild()\n        .toBundle()");
            Intent putExtra = getIntent(context).putExtra(DSLSettingsActivity.ARG_START_BUNDLE, bundle);
            Intrinsics.checkNotNullExpressionValue(putExtra, "getIntent(context)\n     …TART_BUNDLE, startBundle)");
            return putExtra;
        }

        @JvmStatic
        public final Intent forRecipient(Context context, RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Bundle bundle = new ConversationSettingsFragmentArgs.Builder(recipientId, null).build().toBundle();
            Intrinsics.checkNotNullExpressionValue(bundle, "Builder(recipientId, nul…ild()\n        .toBundle()");
            Intent putExtra = getIntent(context).putExtra(DSLSettingsActivity.ARG_START_BUNDLE, bundle);
            Intrinsics.checkNotNullExpressionValue(putExtra, "getIntent(context)\n     …TART_BUNDLE, startBundle)");
            return putExtra;
        }

        private final Intent getIntent(Context context) {
            Intent putExtra = new Intent(context, ConversationSettingsActivity.class).putExtra(DSLSettingsActivity.ARG_NAV_GRAPH, R.navigation.conversation_settings);
            Intrinsics.checkNotNullExpressionValue(putExtra, "Intent(context, Conversa…on.conversation_settings)");
            return putExtra;
        }
    }
}
