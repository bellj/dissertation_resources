package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import android.content.SharedPreferences;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Consumer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobmanager.impl.NetworkConstraint;
import org.thoughtcrime.securesms.jobs.RefreshAttributesJob;
import org.thoughtcrime.securesms.jobs.RefreshOwnProfileJob;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.TextSecurePreferences;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.websocket.WebSocketConnectionState;

/* compiled from: AdvancedPrivacySettingsViewModel.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001:\u0002$%B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0017\u001a\u00020\u0018J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u0012\u001a\u00020\u001bH\u0002J\b\u0010\u001c\u001a\u00020\u001bH\u0002J\b\u0010\u0014\u001a\u00020\u0013H\u0002J\b\u0010\u001d\u001a\u00020\u0018H\u0014J\u0006\u0010\u001e\u001a\u00020\u0018J\u000e\u0010\u001f\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u001aJ\u000e\u0010!\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u001aJ\u000e\u0010\"\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u001aJ\u000e\u0010#\u001a\u00020\u00182\u0006\u0010 \u001a\u00020\u001aR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\f¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\r0\u0011X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\f¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000fR\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00130\u0016X\u0004¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "sharedPreferences", "Landroid/content/SharedPreferences;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository;", "(Landroid/content/SharedPreferences;Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "getDisposables", "()Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "events", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsViewModel$Event;", "getEvents", "()Landroidx/lifecycle/LiveData;", "singleEvents", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsState;", "getState", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "disablePushMessages", "", "getCensorshipCircumventionEnabled", "", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/CensorshipCircumventionState;", "getCensorshipCircumventionState", "onCleared", "refresh", "setAllowSealedSenderFromAnyone", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "setAlwaysRelayCalls", "setCensorshipCircumventionEnabled", "setShowStatusIconForSealedSender", "Event", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AdvancedPrivacySettingsViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final LiveData<Event> events;
    private final AdvancedPrivacySettingsRepository repository;
    private final SharedPreferences sharedPreferences;
    private final SingleLiveEvent<Event> singleEvents;
    private final LiveData<AdvancedPrivacySettingsState> state;
    private final Store<AdvancedPrivacySettingsState> store;

    /* compiled from: AdvancedPrivacySettingsViewModel.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0003\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsViewModel$Event;", "", "(Ljava/lang/String;I)V", "DISABLE_PUSH_FAILED", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum Event {
        DISABLE_PUSH_FAILED
    }

    /* compiled from: AdvancedPrivacySettingsViewModel.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[CensorshipCircumventionState.values().length];
            iArr[CensorshipCircumventionState.UNAVAILABLE_CONNECTED.ordinal()] = 1;
            iArr[CensorshipCircumventionState.UNAVAILABLE_NO_INTERNET.ordinal()] = 2;
            iArr[CensorshipCircumventionState.AVAILABLE_MANUALLY_DISABLED.ordinal()] = 3;
            iArr[CensorshipCircumventionState.AVAILABLE_AUTOMATICALLY_ENABLED.ordinal()] = 4;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public AdvancedPrivacySettingsViewModel(SharedPreferences sharedPreferences, AdvancedPrivacySettingsRepository advancedPrivacySettingsRepository) {
        Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsRepository, "repository");
        this.sharedPreferences = sharedPreferences;
        this.repository = advancedPrivacySettingsRepository;
        Store<AdvancedPrivacySettingsState> store = new Store<>(getState());
        this.store = store;
        SingleLiveEvent<Event> singleLiveEvent = new SingleLiveEvent<>();
        this.singleEvents = singleLiveEvent;
        LiveData<AdvancedPrivacySettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        this.events = singleLiveEvent;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        compositeDisposable.add(ApplicationDependencies.getSignalWebSocket().getWebSocketState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                AdvancedPrivacySettingsViewModel.m851_init_$lambda0(AdvancedPrivacySettingsViewModel.this, (WebSocketConnectionState) obj);
            }
        }));
    }

    /* renamed from: getState */
    public final LiveData<AdvancedPrivacySettingsState> m854getState() {
        return this.state;
    }

    public final LiveData<Event> getEvents() {
        return this.events;
    }

    public final CompositeDisposable getDisposables() {
        return this.disposables;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m851_init_$lambda0(AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel, WebSocketConnectionState webSocketConnectionState) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsViewModel, "this$0");
        advancedPrivacySettingsViewModel.refresh();
    }

    /* renamed from: disablePushMessages$lambda-1 */
    public static final AdvancedPrivacySettingsState m852disablePushMessages$lambda1(AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel, AdvancedPrivacySettingsState advancedPrivacySettingsState) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsViewModel, "this$0");
        return AdvancedPrivacySettingsState.copy$default(advancedPrivacySettingsViewModel.getState(), false, false, null, false, false, false, true, 63, null);
    }

    public final void disablePushMessages() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AdvancedPrivacySettingsViewModel.m852disablePushMessages$lambda1(AdvancedPrivacySettingsViewModel.this, (AdvancedPrivacySettingsState) obj);
            }
        });
        this.repository.disablePushMessages(new AdvancedPrivacySettingsViewModel$disablePushMessages$2(this));
    }

    public final void setAlwaysRelayCalls(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.ALWAYS_RELAY_CALLS_PREF, z).apply();
        refresh();
    }

    public final void setShowStatusIconForSealedSender(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.SHOW_UNIDENTIFIED_DELIVERY_INDICATORS, z).apply();
        this.repository.syncShowSealedSenderIconState();
        refresh();
    }

    public final void setAllowSealedSenderFromAnyone(boolean z) {
        this.sharedPreferences.edit().putBoolean(TextSecurePreferences.UNIVERSAL_UNIDENTIFIED_ACCESS, z).apply();
        ApplicationDependencies.getJobManager().startChain(new RefreshAttributesJob()).then(new RefreshOwnProfileJob()).enqueue();
        refresh();
    }

    public final void setCensorshipCircumventionEnabled(boolean z) {
        SignalStore.settings().setCensorshipCircumventionEnabled(z);
        SignalStore.misc().setServiceReachableWithoutCircumvention(false);
        ApplicationDependencies.resetNetworkConnectionsAfterProxyChange();
        refresh();
    }

    /* renamed from: refresh$lambda-2 */
    public static final AdvancedPrivacySettingsState m853refresh$lambda2(AdvancedPrivacySettingsViewModel advancedPrivacySettingsViewModel, AdvancedPrivacySettingsState advancedPrivacySettingsState) {
        Intrinsics.checkNotNullParameter(advancedPrivacySettingsViewModel, "this$0");
        return AdvancedPrivacySettingsState.copy$default(advancedPrivacySettingsViewModel.getState(), false, false, null, false, false, false, advancedPrivacySettingsState.getShowProgressSpinner(), 63, null);
    }

    public final void refresh() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.advanced.AdvancedPrivacySettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AdvancedPrivacySettingsViewModel.m853refresh$lambda2(AdvancedPrivacySettingsViewModel.this, (AdvancedPrivacySettingsState) obj);
            }
        });
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.dispose();
    }

    public final AdvancedPrivacySettingsState getState() {
        CensorshipCircumventionState censorshipCircumventionState = getCensorshipCircumventionState();
        return new AdvancedPrivacySettingsState(SignalStore.account().isRegistered(), TextSecurePreferences.isTurnOnly(ApplicationDependencies.getApplication()), censorshipCircumventionState, getCensorshipCircumventionEnabled(censorshipCircumventionState), TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(ApplicationDependencies.getApplication()), TextSecurePreferences.isUniversalUnidentifiedAccess(ApplicationDependencies.getApplication()), false);
    }

    private final CensorshipCircumventionState getCensorshipCircumventionState() {
        boolean isCountryCodeCensoredByDefault = ApplicationDependencies.getSignalServiceNetworkAccess().isCountryCodeCensoredByDefault(PhoneNumberFormatter.getLocalCountryCode());
        SettingsValues.CensorshipCircumventionEnabled censorshipCircumventionEnabled = SignalStore.settings().getCensorshipCircumventionEnabled();
        Intrinsics.checkNotNullExpressionValue(censorshipCircumventionEnabled, "settings().censorshipCircumventionEnabled");
        boolean isMet = NetworkConstraint.isMet(ApplicationDependencies.getApplication());
        boolean z = ApplicationDependencies.getSignalWebSocket().getWebSocketState().firstOrError().blockingGet() == WebSocketConnectionState.CONNECTED;
        if (SignalStore.internalValues().allowChangingCensorshipSetting()) {
            return CensorshipCircumventionState.AVAILABLE;
        }
        if (isCountryCodeCensoredByDefault && censorshipCircumventionEnabled == SettingsValues.CensorshipCircumventionEnabled.DISABLED) {
            return CensorshipCircumventionState.AVAILABLE_MANUALLY_DISABLED;
        }
        if (isCountryCodeCensoredByDefault) {
            return CensorshipCircumventionState.AVAILABLE_AUTOMATICALLY_ENABLED;
        }
        if (!isMet && censorshipCircumventionEnabled != SettingsValues.CensorshipCircumventionEnabled.ENABLED) {
            return CensorshipCircumventionState.UNAVAILABLE_NO_INTERNET;
        }
        if (!z || censorshipCircumventionEnabled == SettingsValues.CensorshipCircumventionEnabled.ENABLED) {
            return CensorshipCircumventionState.AVAILABLE;
        }
        return CensorshipCircumventionState.UNAVAILABLE_CONNECTED;
    }

    private final boolean getCensorshipCircumventionEnabled(CensorshipCircumventionState censorshipCircumventionState) {
        int i = WhenMappings.$EnumSwitchMapping$0[censorshipCircumventionState.ordinal()];
        if (i == 1 || i == 2 || i == 3) {
            return false;
        }
        if (i == 4 || SignalStore.settings().getCensorshipCircumventionEnabled() == SettingsValues.CensorshipCircumventionEnabled.ENABLED) {
            return true;
        }
        return false;
    }

    /* compiled from: AdvancedPrivacySettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "sharedPreferences", "Landroid/content/SharedPreferences;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository;", "(Landroid/content/SharedPreferences;Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final AdvancedPrivacySettingsRepository repository;
        private final SharedPreferences sharedPreferences;

        public Factory(SharedPreferences sharedPreferences, AdvancedPrivacySettingsRepository advancedPrivacySettingsRepository) {
            Intrinsics.checkNotNullParameter(sharedPreferences, "sharedPreferences");
            Intrinsics.checkNotNullParameter(advancedPrivacySettingsRepository, "repository");
            this.sharedPreferences = sharedPreferences;
            this.repository = advancedPrivacySettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new AdvancedPrivacySettingsViewModel(this.sharedPreferences, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
