package org.thoughtcrime.securesms.components.camera;

import android.content.Context;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/* loaded from: classes4.dex */
public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback {
    private boolean ready;

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
    }

    public CameraSurfaceView(Context context) {
        super(context);
        getHolder().setType(3);
        getHolder().addCallback(this);
    }

    public boolean isReady() {
        return this.ready;
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.ready = true;
    }

    @Override // android.view.SurfaceHolder.Callback
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        this.ready = false;
    }
}
