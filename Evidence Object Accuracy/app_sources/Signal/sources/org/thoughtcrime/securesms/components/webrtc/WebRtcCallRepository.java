package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import androidx.core.util.Consumer;
import java.util.Collections;
import java.util.List;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.identity.IdentityRecordList;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.recipients.Recipient;

/* loaded from: classes4.dex */
public class WebRtcCallRepository {
    private final Context context;

    public WebRtcCallRepository(Context context) {
        this.context = context;
    }

    public void getIdentityRecords(Recipient recipient, Consumer<IdentityRecordList> consumer) {
        SignalExecutors.BOUNDED.execute(new Runnable(consumer) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ Consumer f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallRepository.$r8$lambda$SsE2kD9eLX1l0zvOFY8VsMOKxuE(Recipient.this, this.f$1);
            }
        });
    }

    public static /* synthetic */ void lambda$getIdentityRecords$0(Recipient recipient, Consumer consumer) {
        List<Recipient> list;
        if (recipient.isGroup()) {
            list = SignalDatabase.groups().getGroupMembers(recipient.requireGroupId(), GroupDatabase.MemberSet.FULL_MEMBERS_EXCLUDING_SELF);
        } else {
            list = Collections.singletonList(recipient);
        }
        consumer.accept(ApplicationDependencies.getProtocolStore().aci().identities().getIdentityRecords(list));
    }
}
