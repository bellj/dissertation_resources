package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.lifecycle.Lifecycle;
import androidx.navigation.NavController;
import androidx.navigation.fragment.FragmentKt;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileCreatedFragmentDirections;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.util.LifecycleDisposable;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: NotificationProfileCreatedFragment.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileCreatedFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "profileId", "", "getProfileId", "()J", "profileId$delegate", "Lkotlin/Lazy;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileCreatedFragment extends LoggingFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy profileId$delegate = LazyKt__LazyJVMKt.lazy(new Function0<Long>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileCreatedFragment$profileId$2
        final /* synthetic */ NotificationProfileCreatedFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Long invoke() {
            return Long.valueOf(NotificationProfileCreatedFragmentArgs.fromBundle(this.this$0.requireArguments()).getProfileId());
        }
    });

    public NotificationProfileCreatedFragment() {
        super(R.layout.fragment_notification_profile_created);
    }

    private final long getProfileId() {
        return ((Number) this.profileId$delegate.getValue()).longValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.notification_profile_created_top_image);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.n…rofile_created_top_image)");
        View findViewById2 = view.findViewById(R.id.notification_profile_created_top_text);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.n…profile_created_top_text)");
        View findViewById3 = view.findViewById(R.id.notification_profile_created_bottom_image);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.n…ile_created_bottom_image)");
        View findViewById4 = view.findViewById(R.id.notification_profile_created_bottom_text);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.n…file_created_bottom_text)");
        view.findViewById(R.id.notification_profile_created_done).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileCreatedFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                NotificationProfileCreatedFragment.$r8$lambda$m5h7rRmXq2ChKosdDStBVPsgRVk(NotificationProfileCreatedFragment.this, view2);
            }
        });
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Lifecycle lifecycle = getViewLifecycleOwner().getLifecycle();
        Intrinsics.checkNotNullExpressionValue(lifecycle, "viewLifecycleOwner.lifecycle");
        lifecycleDisposable.bindTo(lifecycle);
        NotificationProfilesRepository notificationProfilesRepository = new NotificationProfilesRepository();
        LifecycleDisposable lifecycleDisposable2 = this.lifecycleDisposable;
        Observable<NotificationProfile> observeOn = notificationProfilesRepository.getProfile(getProfileId()).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.getProfile(pr…dSchedulers.mainThread())");
        lifecycleDisposable2.plusAssign(SubscribersKt.subscribeBy$default(observeOn, (Function1) null, (Function0) null, new Function1<NotificationProfile, Unit>((ImageView) findViewById, (TextView) findViewById2, (ImageView) findViewById3, (TextView) findViewById4) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileCreatedFragment$onViewCreated$2
            final /* synthetic */ ImageView $bottomIcon;
            final /* synthetic */ TextView $bottomText;
            final /* synthetic */ ImageView $topIcon;
            final /* synthetic */ TextView $topText;

            /* access modifiers changed from: package-private */
            {
                this.$topIcon = r1;
                this.$topText = r2;
                this.$bottomIcon = r3;
                this.$bottomText = r4;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(NotificationProfile notificationProfile) {
                invoke(notificationProfile);
                return Unit.INSTANCE;
            }

            public final void invoke(NotificationProfile notificationProfile) {
                if (notificationProfile.getSchedule().getEnabled()) {
                    this.$topIcon.setImageResource(R.drawable.ic_recent_20);
                    this.$topText.setText(R.string.NotificationProfileCreated__your_profile_will_turn_on_and_off_automatically_according_to_your_schedule);
                    this.$bottomIcon.setImageResource(R.drawable.ic_more_vert_24);
                    this.$bottomText.setText(R.string.NotificationProfileCreated__you_can_turn_your_profile_on_or_off_manually_via_the_menu_on_the_chat_list);
                    return;
                }
                this.$topIcon.setImageResource(R.drawable.ic_more_vert_24);
                this.$topText.setText(R.string.NotificationProfileCreated__you_can_turn_your_profile_on_or_off_manually_via_the_menu_on_the_chat_list);
                this.$bottomIcon.setImageResource(R.drawable.ic_recent_20);
                this.$bottomText.setText(R.string.NotificationProfileCreated__add_a_schedule_in_settings_to_automate_your_profile);
            }
        }, 3, (Object) null));
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m756onViewCreated$lambda0(NotificationProfileCreatedFragment notificationProfileCreatedFragment, View view) {
        Intrinsics.checkNotNullParameter(notificationProfileCreatedFragment, "this$0");
        NavController findNavController = FragmentKt.findNavController(notificationProfileCreatedFragment);
        NotificationProfileCreatedFragmentDirections.ActionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment = NotificationProfileCreatedFragmentDirections.actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment(notificationProfileCreatedFragment.getProfileId());
        Intrinsics.checkNotNullExpressionValue(actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment, "actionNotificationProfil…etailsFragment(profileId)");
        SafeNavigation.safeNavigate(findNavController, actionNotificationProfileCreatedFragmentToNotificationProfileDetailsFragment);
    }
}
