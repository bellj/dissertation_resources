package org.thoughtcrime.securesms.components.menu;

import android.os.Build;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.util.BottomSheetUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: ContextMenuList.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0003\u0010\u0011\u0012B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0014\u0010\n\u001a\u00020\u00062\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fJ\u0018\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\f*\b\u0012\u0004\u0012\u00020\r0\fH\u0002R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/ContextMenuList;", "", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "onItemClick", "Lkotlin/Function0;", "", "(Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function0;)V", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "setItems", "items", "", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "toAdapterItems", "Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayItem;", "DisplayItem", "DisplayType", "ItemViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ContextMenuList {
    private final MappingAdapter mappingAdapter;

    /* compiled from: ContextMenuList.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0006\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005j\u0002\b\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayType;", "", "(Ljava/lang/String;I)V", "TOP", BottomSheetUtil.STANDARD_BOTTOM_SHEET_FRAGMENT_TAG, "MIDDLE", "ONLY", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum DisplayType {
        TOP,
        BOTTOM,
        MIDDLE,
        ONLY
    }

    public ContextMenuList(RecyclerView recyclerView, Function0<Unit> function0) {
        Intrinsics.checkNotNullParameter(recyclerView, "recyclerView");
        Intrinsics.checkNotNullParameter(function0, "onItemClick");
        MappingAdapter mappingAdapter = new MappingAdapter();
        mappingAdapter.registerFactory(DisplayItem.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.menu.ContextMenuList$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ContextMenuList.$r8$lambda$4YyKRXuB9uAZJrdui8PRPDed6G4(Function0.this, (View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.signal_context_menu_item));
        this.mappingAdapter = mappingAdapter;
        recyclerView.setAdapter(mappingAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setItemAnimator(null);
    }

    /* renamed from: mappingAdapter$lambda-1$lambda-0 */
    public static final MappingViewHolder m508mappingAdapter$lambda1$lambda0(Function0 function0, View view) {
        Intrinsics.checkNotNullParameter(function0, "$onItemClick");
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new ItemViewHolder(view, function0);
    }

    public final void setItems(List<ActionItem> list) {
        Intrinsics.checkNotNullParameter(list, "items");
        this.mappingAdapter.submitList(toAdapterItems(list));
    }

    /* compiled from: ContextMenuList.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\u0010\u0010\u000e\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0000H\u0016J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\u0011\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0012\u001a\u00020\f2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001J\t\u0010\u0017\u001a\u00020\u0018HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayItem;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "item", "Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "displayType", "Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayType;", "(Lorg/thoughtcrime/securesms/components/menu/ActionItem;Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayType;)V", "getDisplayType", "()Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayType;", "getItem", "()Lorg/thoughtcrime/securesms/components/menu/ActionItem;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "component1", "component2", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DisplayItem implements MappingModel<DisplayItem> {
        private final DisplayType displayType;
        private final ActionItem item;

        public static /* synthetic */ DisplayItem copy$default(DisplayItem displayItem, ActionItem actionItem, DisplayType displayType, int i, Object obj) {
            if ((i & 1) != 0) {
                actionItem = displayItem.item;
            }
            if ((i & 2) != 0) {
                displayType = displayItem.displayType;
            }
            return displayItem.copy(actionItem, displayType);
        }

        public final ActionItem component1() {
            return this.item;
        }

        public final DisplayType component2() {
            return this.displayType;
        }

        public final DisplayItem copy(ActionItem actionItem, DisplayType displayType) {
            Intrinsics.checkNotNullParameter(actionItem, "item");
            Intrinsics.checkNotNullParameter(displayType, "displayType");
            return new DisplayItem(actionItem, displayType);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof DisplayItem)) {
                return false;
            }
            DisplayItem displayItem = (DisplayItem) obj;
            return Intrinsics.areEqual(this.item, displayItem.item) && this.displayType == displayItem.displayType;
        }

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(DisplayItem displayItem) {
            return MappingModel.CC.$default$getChangePayload(this, displayItem);
        }

        public int hashCode() {
            return (this.item.hashCode() * 31) + this.displayType.hashCode();
        }

        public String toString() {
            return "DisplayItem(item=" + this.item + ", displayType=" + this.displayType + ')';
        }

        public DisplayItem(ActionItem actionItem, DisplayType displayType) {
            Intrinsics.checkNotNullParameter(actionItem, "item");
            Intrinsics.checkNotNullParameter(displayType, "displayType");
            this.item = actionItem;
            this.displayType = displayType;
        }

        public final ActionItem getItem() {
            return this.item;
        }

        public final DisplayType getDisplayType() {
            return this.displayType;
        }

        public boolean areItemsTheSame(DisplayItem displayItem) {
            Intrinsics.checkNotNullParameter(displayItem, "newItem");
            return Intrinsics.areEqual(this, displayItem);
        }

        public boolean areContentsTheSame(DisplayItem displayItem) {
            Intrinsics.checkNotNullParameter(displayItem, "newItem");
            return Intrinsics.areEqual(this, displayItem);
        }
    }

    /* compiled from: ContextMenuList.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001b\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006¢\u0006\u0002\u0010\bJ\u0010\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0002H\u0016R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006X\u0004¢\u0006\u0002\n\u0000R\u0011\u0010\r\u001a\u00020\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$ItemViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/menu/ContextMenuList$DisplayItem;", "itemView", "Landroid/view/View;", "onItemClick", "Lkotlin/Function0;", "", "(Landroid/view/View;Lkotlin/jvm/functions/Function0;)V", "icon", "Landroid/widget/ImageView;", "getIcon", "()Landroid/widget/ImageView;", MultiselectForwardFragment.DIALOG_TITLE, "Landroid/widget/TextView;", "getTitle", "()Landroid/widget/TextView;", "bind", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ItemViewHolder extends MappingViewHolder<DisplayItem> {
        private final ImageView icon;
        private final Function0<Unit> onItemClick;
        private final TextView title;

        /* compiled from: ContextMenuList.kt */
        @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public /* synthetic */ class WhenMappings {
            public static final /* synthetic */ int[] $EnumSwitchMapping$0;

            static {
                int[] iArr = new int[DisplayType.values().length];
                iArr[DisplayType.TOP.ordinal()] = 1;
                iArr[DisplayType.BOTTOM.ordinal()] = 2;
                iArr[DisplayType.MIDDLE.ordinal()] = 3;
                iArr[DisplayType.ONLY.ordinal()] = 4;
                $EnumSwitchMapping$0 = iArr;
            }
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ItemViewHolder(View view, Function0<Unit> function0) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            Intrinsics.checkNotNullParameter(function0, "onItemClick");
            this.onItemClick = function0;
            View findViewById = view.findViewById(R.id.signal_context_menu_item_icon);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.…l_context_menu_item_icon)");
            this.icon = (ImageView) findViewById;
            View findViewById2 = view.findViewById(R.id.signal_context_menu_item_title);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…_context_menu_item_title)");
            this.title = (TextView) findViewById2;
        }

        public final ImageView getIcon() {
            return this.icon;
        }

        public final TextView getTitle() {
            return this.title;
        }

        public void bind(DisplayItem displayItem) {
            Intrinsics.checkNotNullParameter(displayItem, "model");
            this.icon.setImageResource(displayItem.getItem().getIconRes());
            this.title.setText(displayItem.getItem().getTitle());
            this.itemView.setOnClickListener(new ContextMenuList$ItemViewHolder$$ExternalSyntheticLambda0(displayItem, this));
            int color = ContextCompat.getColor(this.context, displayItem.getItem().getTintRes());
            this.icon.setColorFilter(color);
            this.title.setTextColor(color);
            if (Build.VERSION.SDK_INT >= 21) {
                int i = WhenMappings.$EnumSwitchMapping$0[displayItem.getDisplayType().ordinal()];
                if (i == 1) {
                    this.itemView.setBackgroundResource(R.drawable.signal_context_menu_item_background_top);
                } else if (i == 2) {
                    this.itemView.setBackgroundResource(R.drawable.signal_context_menu_item_background_bottom);
                } else if (i == 3) {
                    this.itemView.setBackgroundResource(R.drawable.signal_context_menu_item_background_middle);
                } else if (i == 4) {
                    this.itemView.setBackgroundResource(R.drawable.signal_context_menu_item_background_only);
                }
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m509bind$lambda0(DisplayItem displayItem, ItemViewHolder itemViewHolder, View view) {
            Intrinsics.checkNotNullParameter(displayItem, "$model");
            Intrinsics.checkNotNullParameter(itemViewHolder, "this$0");
            displayItem.getItem().getAction().run();
            itemViewHolder.onItemClick.invoke();
        }
    }

    private final List<DisplayItem> toAdapterItems(List<ActionItem> list) {
        DisplayType displayType;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        int i = 0;
        for (Object obj : list) {
            int i2 = i + 1;
            if (i < 0) {
                CollectionsKt__CollectionsKt.throwIndexOverflow();
            }
            ActionItem actionItem = (ActionItem) obj;
            if (list.size() == 1) {
                displayType = DisplayType.ONLY;
            } else if (i == 0) {
                displayType = DisplayType.TOP;
            } else if (i == list.size() - 1) {
                displayType = DisplayType.BOTTOM;
            } else {
                displayType = DisplayType.MIDDLE;
            }
            arrayList.add(new DisplayItem(actionItem, displayType));
            i = i2;
        }
        return arrayList;
    }
}
