package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.FunctionReferenceImpl;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: ConversationSettingsRepository.kt */
/* access modifiers changed from: package-private */
@Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public /* synthetic */ class ConversationSettingsRepository$getGroupsInCommon$1$3 extends FunctionReferenceImpl implements Function1<RecipientId, Recipient> {
    public static final ConversationSettingsRepository$getGroupsInCommon$1$3 INSTANCE = new ConversationSettingsRepository$getGroupsInCommon$1$3();

    ConversationSettingsRepository$getGroupsInCommon$1$3() {
        super(1, Recipient.class, "resolved", "resolved(Lorg/thoughtcrime/securesms/recipients/RecipientId;)Lorg/thoughtcrime/securesms/recipients/Recipient;", 0);
    }

    public final Recipient invoke(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "p0");
        return Recipient.resolved(recipientId);
    }
}
