package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import j$.time.ZoneId;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.SectionHeaderPreference;
import org.thoughtcrime.securesms.components.settings.SectionHeaderPreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.TextPreference;
import org.thoughtcrime.securesms.components.settings.TextPreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListItem;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;
import org.thoughtcrime.securesms.util.StickyHeaderDecoration;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: DonationReceiptListAdapter.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\b\u0012\u0004\u0012\u00020\u00030\u0002B\u0019\u0012\u0012\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005¢\u0006\u0002\u0010\bJ\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\"\u0010\r\u001a\u00020\u00072\b\u0010\u000e\u001a\u0004\u0018\u00010\u00032\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\fH\u0016J\"\u0010\u0010\u001a\u00020\u00032\b\u0010\u0011\u001a\u0004\u0018\u00010\u00122\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\fH\u0016¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Lorg/thoughtcrime/securesms/util/StickyHeaderDecoration$StickyHeaderAdapter;", "Lorg/thoughtcrime/securesms/components/settings/SectionHeaderPreferenceViewHolder;", "onModelClick", "Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListItem$Model;", "", "(Lkotlin/jvm/functions/Function1;)V", "getHeaderId", "", "position", "", "onBindHeaderViewHolder", "viewHolder", "type", "onCreateHeaderViewHolder", "parent", "Landroid/view/ViewGroup;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListAdapter extends MappingAdapter implements StickyHeaderDecoration.StickyHeaderAdapter<SectionHeaderPreferenceViewHolder> {
    public DonationReceiptListAdapter(Function1<? super DonationReceiptListItem.Model, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "onModelClick");
        registerFactory(TextPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return DonationReceiptListAdapter.$r8$lambda$Wlw93mhGw4xIkMrPrGJD6SGUBrM((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        DonationReceiptListItem.INSTANCE.register(this, function1);
    }

    /* renamed from: _init_$lambda-0 */
    public static final MappingViewHolder m1016_init_$lambda0(View view) {
        Intrinsics.checkNotNullExpressionValue(view, "it");
        return new TextPreferenceViewHolder(view);
    }

    @Override // org.thoughtcrime.securesms.util.StickyHeaderDecoration.StickyHeaderAdapter
    public long getHeaderId(int i) {
        MappingModel<?> item = getItem(i);
        if (item instanceof DonationReceiptListItem.Model) {
            return (long) JavaTimeExtensionsKt.toLocalDateTime$default(((DonationReceiptListItem.Model) item).getRecord().getTimestamp(), (ZoneId) null, 1, (Object) null).getYear();
        }
        return -1;
    }

    public SectionHeaderPreferenceViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup, int i, int i2) {
        Intrinsics.checkNotNull(viewGroup);
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.dsl_section_header, viewGroup, false);
        Intrinsics.checkNotNullExpressionValue(inflate, "from(parent!!.context).i…on_header, parent, false)");
        return new SectionHeaderPreferenceViewHolder(inflate);
    }

    public void onBindHeaderViewHolder(SectionHeaderPreferenceViewHolder sectionHeaderPreferenceViewHolder, int i, int i2) {
        if (sectionHeaderPreferenceViewHolder != null) {
            sectionHeaderPreferenceViewHolder.bind(new SectionHeaderPreference(DSLSettingsText.Companion.from(String.valueOf(getHeaderId(i)), new DSLSettingsText.Modifier[0])));
        }
    }
}
