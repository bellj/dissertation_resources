package org.thoughtcrime.securesms.components.settings.app;

import kotlin.Metadata;

/* compiled from: AppSettingsActivity.kt */
@Metadata(d1 = {"\u0000\n\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0002\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0003\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0004\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u000e\u0010\u0005\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"EXTRA_PERFORM_ACTION_ON_CREATE", "", "NOTIFICATION_CATEGORY", "START_ARGUMENTS", "START_LOCATION", "STATE_WAS_CONFIGURATION_UPDATED", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppSettingsActivityKt {
    private static final String EXTRA_PERFORM_ACTION_ON_CREATE;
    private static final String NOTIFICATION_CATEGORY;
    private static final String START_ARGUMENTS;
    private static final String START_LOCATION;
    private static final String STATE_WAS_CONFIGURATION_UPDATED;
}
