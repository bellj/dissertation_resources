package org.thoughtcrime.securesms.components.settings.conversation;

import android.content.DialogInterface;
import android.graphics.Color;
import android.text.TextUtils;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.Objects;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import kotlin.text.StringsKt__IndentKt;
import org.signal.core.util.Hex;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.GroupDatabase;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SessionDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.subscription.Subscriber;
import org.thoughtcrime.securesms.util.Base64;
import org.thoughtcrime.securesms.util.SpanUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.livedata.Store;
import org.whispersystems.signalservice.api.push.ACI;
import org.whispersystems.signalservice.api.push.PNI;

/* compiled from: InternalConversationSettingsFragment.kt */
@Metadata(d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0003\u001c\u001d\u001eB\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0018\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0010\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0017\u001a\u00020\u0013H\u0002J\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bH\u0002R\u001b\u0010\u0003\u001a\u00020\u00048BX\u0002¢\u0006\f\n\u0004\b\u0007\u0010\b\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u001f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "buildCapabilitySpan", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "colorize", "name", "", "support", "Lorg/thoughtcrime/securesms/recipients/Recipient$Capability;", "copyToClipboard", DraftDatabase.Draft.TEXT, "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalState;", "InternalState", "InternalViewModel", "MyViewModelFactory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternalConversationSettingsFragment extends DSLSettingsFragment {
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(InternalViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$viewModel$2
        final /* synthetic */ InternalConversationSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            RecipientId recipientId = InternalConversationSettingsFragmentArgs.fromBundle(this.this$0.requireArguments()).getRecipientId();
            Intrinsics.checkNotNullExpressionValue(recipientId, "fromBundle(requireArguments()).recipientId");
            return new InternalConversationSettingsFragment.MyViewModelFactory(recipientId);
        }
    });

    /* compiled from: InternalConversationSettingsFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[Recipient.Capability.values().length];
            iArr[Recipient.Capability.SUPPORTED.ordinal()] = 1;
            iArr[Recipient.Capability.NOT_SUPPORTED.ordinal()] = 2;
            iArr[Recipient.Capability.UNKNOWN.ordinal()] = 3;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public InternalConversationSettingsFragment() {
        super(R.string.ConversationSettingsFragment__internal_details, 0, 0, null, 14, null);
    }

    private final InternalViewModel getViewModel() {
        return (InternalViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ InternalConversationSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                InternalConversationSettingsFragment.m1160$r8$lambda$1cnVWIUK9mQc0e7467fmVK9XfQ(DSLSettingsAdapter.this, this.f$1, (InternalConversationSettingsFragment.InternalState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m1161bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, InternalConversationSettingsFragment internalConversationSettingsFragment, InternalState internalState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(internalConversationSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(internalState, "state");
        dSLSettingsAdapter.submitList(internalConversationSettingsFragment.getConfiguration(internalState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(InternalState internalState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(internalState.getRecipient(), internalState, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1
            final /* synthetic */ Recipient $recipient;
            final /* synthetic */ InternalConversationSettingsFragment.InternalState $state;
            final /* synthetic */ InternalConversationSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$recipient = r1;
                this.$state = r2;
                this.this$0 = r3;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                final String str;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                dSLConfiguration.sectionHeaderPref(companion.from("Data", new DSLSettingsText.Modifier[0]));
                DSLSettingsText from = companion.from("RecipientId", new DSLSettingsText.Modifier[0]);
                String serialize = this.$recipient.getId().serialize();
                Intrinsics.checkNotNullExpressionValue(serialize, "recipient.id.serialize()");
                dSLConfiguration.textPref(from, companion.from(serialize, new DSLSettingsText.Modifier[0]));
                if (!this.$recipient.isGroup()) {
                    final String str2 = "null";
                    if (this.$recipient.isSelf()) {
                        ACI aci = SignalStore.account().getAci();
                        final String serviceId = aci != null ? aci.toString() : null;
                        if (serviceId == null) {
                            serviceId = str2;
                        }
                        DSLSettingsText from2 = companion.from("ACI", new DSLSettingsText.Modifier[0]);
                        DSLSettingsText from3 = companion.from(serviceId, new DSLSettingsText.Modifier[0]);
                        final InternalConversationSettingsFragment internalConversationSettingsFragment = this.this$0;
                        dSLConfiguration.longClickPref(from2, (r13 & 2) != 0 ? null : from3, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.1
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment, serviceId);
                            }
                        });
                        PNI pni = SignalStore.account().getPni();
                        String serviceId2 = pni != null ? pni.toString() : null;
                        if (serviceId2 != null) {
                            str2 = serviceId2;
                        }
                        DSLSettingsText from4 = companion.from("PNI", new DSLSettingsText.Modifier[0]);
                        DSLSettingsText from5 = companion.from(str2, new DSLSettingsText.Modifier[0]);
                        final InternalConversationSettingsFragment internalConversationSettingsFragment2 = this.this$0;
                        dSLConfiguration.longClickPref(from4, (r13 & 2) != 0 ? null : from5, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.2
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment2, str2);
                            }
                        });
                    } else {
                        Object orElse = this.$recipient.getServiceId().map(new InternalConversationSettingsFragment$getConfiguration$1$$ExternalSyntheticLambda0()).orElse(str2);
                        Intrinsics.checkNotNullExpressionValue(orElse, "recipient.serviceId.map(…:toString).orElse(\"null\")");
                        final String str3 = (String) orElse;
                        DSLSettingsText from6 = companion.from("ServiceId", new DSLSettingsText.Modifier[0]);
                        DSLSettingsText from7 = companion.from(str3, new DSLSettingsText.Modifier[0]);
                        final InternalConversationSettingsFragment internalConversationSettingsFragment3 = this.this$0;
                        dSLConfiguration.longClickPref(from6, (r13 & 2) != 0 ? null : from7, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.3
                            @Override // kotlin.jvm.functions.Function0
                            public final void invoke() {
                                InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment3, str3);
                            }
                        });
                    }
                }
                if (this.$state.getGroupId() != null) {
                    final String groupId = this.$state.getGroupId().toString();
                    Intrinsics.checkNotNullExpressionValue(groupId, "state.groupId.toString()");
                    DSLSettingsText from8 = companion.from("GroupId", new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from9 = companion.from(groupId, new DSLSettingsText.Modifier[0]);
                    final InternalConversationSettingsFragment internalConversationSettingsFragment4 = this.this$0;
                    dSLConfiguration.longClickPref(from8, (r13 & 2) != 0 ? null : from9, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.4
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment4, groupId);
                        }
                    });
                }
                final String l = this.$state.getThreadId() != null ? this.$state.getThreadId().toString() : "N/A";
                DSLSettingsText from10 = companion.from("ThreadId", new DSLSettingsText.Modifier[0]);
                DSLSettingsText from11 = companion.from(l, new DSLSettingsText.Modifier[0]);
                final InternalConversationSettingsFragment internalConversationSettingsFragment5 = this.this$0;
                dSLConfiguration.longClickPref(from10, (r13 & 2) != 0 ? null : from11, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.5
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment5, l);
                    }
                });
                String str4 = "None";
                if (!this.$recipient.isGroup()) {
                    DSLSettingsText from12 = companion.from("Profile Name", new DSLSettingsText.Modifier[0]);
                    dSLConfiguration.textPref(from12, companion.from('[' + this.$recipient.getProfileName().getGivenName() + "] [" + this.$state.getRecipient().getProfileName().getFamilyName() + ']', new DSLSettingsText.Modifier[0]));
                    byte[] profileKey = this.$recipient.getProfileKey();
                    if (profileKey == null || (str = Base64.encodeBytes(profileKey)) == null) {
                        str = str4;
                    }
                    DSLSettingsText from13 = companion.from("Profile Key (Base64)", new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from14 = companion.from(str, new DSLSettingsText.Modifier[0]);
                    final InternalConversationSettingsFragment internalConversationSettingsFragment6 = this.this$0;
                    dSLConfiguration.longClickPref(from13, (r13 & 2) != 0 ? null : from14, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.6
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment6, str);
                        }
                    });
                    byte[] profileKey2 = this.$recipient.getProfileKey();
                    final String stringCondensed = profileKey2 != null ? Hex.toStringCondensed(profileKey2) : null;
                    if (stringCondensed == null) {
                        stringCondensed = "";
                    }
                    DSLSettingsText from15 = companion.from("Profile Key (Hex)", new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from16 = companion.from(stringCondensed, new DSLSettingsText.Modifier[0]);
                    final InternalConversationSettingsFragment internalConversationSettingsFragment7 = this.this$0;
                    dSLConfiguration.longClickPref(from15, (r13 & 2) != 0 ? null : from16, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.7
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment7, stringCondensed);
                        }
                    });
                    dSLConfiguration.textPref(companion.from("Sealed Sender Mode", new DSLSettingsText.Modifier[0]), companion.from(this.$recipient.getUnidentifiedAccessMode().toString(), new DSLSettingsText.Modifier[0]));
                }
                dSLConfiguration.textPref(companion.from("Profile Sharing (AKA \"Whitelisted\")", new DSLSettingsText.Modifier[0]), companion.from(String.valueOf(this.$recipient.isProfileSharing()), new DSLSettingsText.Modifier[0]));
                if (!this.$recipient.isGroup()) {
                    dSLConfiguration.textPref(companion.from("Capabilities", new DSLSettingsText.Modifier[0]), companion.from(InternalConversationSettingsFragment.access$buildCapabilitySpan(this.this$0, this.$recipient), new DSLSettingsText.Modifier[0]));
                }
                if (!this.$recipient.isGroup()) {
                    dSLConfiguration.sectionHeaderPref(companion.from("Actions", new DSLSettingsText.Modifier[0]));
                    DSLSettingsText from17 = companion.from("Disable Profile Sharing", new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from18 = companion.from("Clears profile sharing/whitelisted status, which should cause the Message Request UI to show.", new DSLSettingsText.Modifier[0]);
                    final InternalConversationSettingsFragment internalConversationSettingsFragment8 = this.this$0;
                    final Recipient recipient = this.$recipient;
                    dSLConfiguration.clickPref(from17, (r18 & 2) != 0 ? null : from18, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.8
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            new MaterialAlertDialogBuilder(internalConversationSettingsFragment8.requireContext()).setTitle((CharSequence) "Are you sure?").setNegativeButton(17039360, (DialogInterface.OnClickListener) new InternalConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda0()).setPositiveButton(17039370, (DialogInterface.OnClickListener) new InternalConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda1(recipient)).show();
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-0  reason: not valid java name */
                        public static final void m1172invoke$lambda0(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-1  reason: not valid java name */
                        public static final void m1173invoke$lambda1(Recipient recipient2, DialogInterface dialogInterface, int i) {
                            Intrinsics.checkNotNullParameter(recipient2, "$recipient");
                            RecipientDatabase recipients = SignalDatabase.Companion.recipients();
                            RecipientId id = recipient2.getId();
                            Intrinsics.checkNotNullExpressionValue(id, "recipient.id");
                            recipients.setProfileSharing(id, false);
                        }
                    }, (r18 & 64) != 0 ? null : null);
                    DSLSettingsText from19 = companion.from("Delete Session", new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from20 = companion.from("Deletes the session, essentially guaranteeing an encryption error if they send you a message.", new DSLSettingsText.Modifier[0]);
                    final InternalConversationSettingsFragment internalConversationSettingsFragment9 = this.this$0;
                    final Recipient recipient2 = this.$recipient;
                    dSLConfiguration.clickPref(from19, (r18 & 2) != 0 ? null : from20, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.9
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            new MaterialAlertDialogBuilder(internalConversationSettingsFragment9.requireContext()).setTitle((CharSequence) "Are you sure?").setNegativeButton(17039360, (DialogInterface.OnClickListener) new InternalConversationSettingsFragment$getConfiguration$1$9$$ExternalSyntheticLambda0()).setPositiveButton(17039370, (DialogInterface.OnClickListener) new InternalConversationSettingsFragment$getConfiguration$1$9$$ExternalSyntheticLambda1(recipient2)).show();
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-0  reason: not valid java name */
                        public static final void m1175invoke$lambda0(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }

                        /* access modifiers changed from: private */
                        /* renamed from: invoke$lambda-1  reason: not valid java name */
                        public static final void m1176invoke$lambda1(Recipient recipient3, DialogInterface dialogInterface, int i) {
                            Intrinsics.checkNotNullParameter(recipient3, "$recipient");
                            if (recipient3.hasServiceId()) {
                                SessionDatabase sessions = SignalDatabase.Companion.sessions();
                                ACI requireAci = SignalStore.account().requireAci();
                                String serviceId3 = recipient3.requireServiceId().toString();
                                Intrinsics.checkNotNullExpressionValue(serviceId3, "recipient.requireServiceId().toString()");
                                sessions.deleteAllFor(requireAci, serviceId3);
                            }
                        }
                    }, (r18 & 64) != 0 ? null : null);
                }
                DSLSettingsText from21 = companion.from("Clear recipient data", new DSLSettingsText.Modifier[0]);
                DSLSettingsText from22 = companion.from("Clears service id, profile data, sessions, identities, and thread.", new DSLSettingsText.Modifier[0]);
                final InternalConversationSettingsFragment internalConversationSettingsFragment10 = this.this$0;
                final Recipient recipient3 = this.$recipient;
                dSLConfiguration.clickPref(from21, (r18 & 2) != 0 ? null : from22, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.10
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        new MaterialAlertDialogBuilder(internalConversationSettingsFragment10.requireContext()).setTitle((CharSequence) "Are you sure?").setNegativeButton(17039360, (DialogInterface.OnClickListener) new InternalConversationSettingsFragment$getConfiguration$1$10$$ExternalSyntheticLambda0()).setPositiveButton(17039370, (DialogInterface.OnClickListener) new InternalConversationSettingsFragment$getConfiguration$1$10$$ExternalSyntheticLambda1(recipient3, internalConversationSettingsFragment10)).show();
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-0  reason: not valid java name */
                    public static final void m1168invoke$lambda0(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-1  reason: not valid java name */
                    public static final void m1169invoke$lambda1(Recipient recipient4, InternalConversationSettingsFragment internalConversationSettingsFragment11, DialogInterface dialogInterface, int i) {
                        Intrinsics.checkNotNullParameter(recipient4, "$recipient");
                        Intrinsics.checkNotNullParameter(internalConversationSettingsFragment11, "this$0");
                        if (recipient4.hasServiceId()) {
                            SignalDatabase.Companion companion2 = SignalDatabase.Companion;
                            companion2.recipients().debugClearServiceIds(recipient4.getId());
                            companion2.recipients().debugClearProfileData(recipient4.getId());
                            SessionDatabase sessions = companion2.sessions();
                            ACI requireAci = SignalStore.account().requireAci();
                            String serviceId3 = recipient4.requireServiceId().toString();
                            Intrinsics.checkNotNullExpressionValue(serviceId3, "recipient.requireServiceId().toString()");
                            sessions.deleteAllFor(requireAci, serviceId3);
                            ApplicationDependencies.getProtocolStore().aci().identities().delete(recipient4.requireServiceId().toString());
                            ApplicationDependencies.getProtocolStore().pni().identities().delete(recipient4.requireServiceId().toString());
                            companion2.threads().deleteConversation(companion2.threads().getThreadIdIfExistsFor(recipient4.getId()));
                        }
                        internalConversationSettingsFragment11.startActivity(MainActivity.clearTop(internalConversationSettingsFragment11.requireContext()));
                    }
                }, (r18 & 64) != 0 ? null : null);
                if (this.$recipient.isSelf()) {
                    dSLConfiguration.sectionHeaderPref(companion.from("Donations", new DSLSettingsText.Modifier[0]));
                    final Subscriber subscriber = SignalStore.donationsValues().getSubscriber();
                    if (subscriber != null) {
                        str4 = StringsKt__IndentKt.trimMargin$default("currency code: " + subscriber.getCurrencyCode() + "\n            |subscriber id: " + subscriber.getSubscriberId().serialize() + "\n          ", null, 1, null);
                    }
                    DSLSettingsText from23 = companion.from("Subscriber ID", new DSLSettingsText.Modifier[0]);
                    DSLSettingsText from24 = companion.from(str4, new DSLSettingsText.Modifier[0]);
                    final InternalConversationSettingsFragment internalConversationSettingsFragment11 = this.this$0;
                    dSLConfiguration.longClickPref(from23, (r13 & 2) != 0 ? null : from24, (r13 & 4) != 0 ? null : null, (r13 & 8) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.InternalConversationSettingsFragment$getConfiguration$1.11
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            Subscriber subscriber2 = subscriber;
                            if (subscriber2 != null) {
                                InternalConversationSettingsFragment internalConversationSettingsFragment12 = internalConversationSettingsFragment11;
                                String serialize2 = subscriber2.getSubscriberId().serialize();
                                Intrinsics.checkNotNullExpressionValue(serialize2, "subscriber.subscriberId.serialize()");
                                InternalConversationSettingsFragment.access$copyToClipboard(internalConversationSettingsFragment12, serialize2);
                            }
                        }
                    });
                }
            }
        });
    }

    public final void copyToClipboard(String str) {
        Util.copyToClipboard(requireContext(), str);
        Toast.makeText(requireContext(), "Copied to clipboard", 0).show();
    }

    public final CharSequence buildCapabilitySpan(Recipient recipient) {
        Recipient.Capability groupsV1MigrationCapability = recipient.getGroupsV1MigrationCapability();
        Intrinsics.checkNotNullExpressionValue(groupsV1MigrationCapability, "recipient.groupsV1MigrationCapability");
        Recipient.Capability announcementGroupCapability = recipient.getAnnouncementGroupCapability();
        Intrinsics.checkNotNullExpressionValue(announcementGroupCapability, "recipient.announcementGroupCapability");
        Recipient.Capability senderKeyCapability = recipient.getSenderKeyCapability();
        Intrinsics.checkNotNullExpressionValue(senderKeyCapability, "recipient.senderKeyCapability");
        Recipient.Capability changeNumberCapability = recipient.getChangeNumberCapability();
        Intrinsics.checkNotNullExpressionValue(changeNumberCapability, "recipient.changeNumberCapability");
        Recipient.Capability storiesCapability = recipient.getStoriesCapability();
        Intrinsics.checkNotNullExpressionValue(storiesCapability, "recipient.storiesCapability");
        CharSequence concat = TextUtils.concat(colorize("GV1Migration", groupsV1MigrationCapability), ", ", colorize("AnnouncementGroup", announcementGroupCapability), ", ", colorize("SenderKey", senderKeyCapability), ", ", colorize("ChangeNumber", changeNumberCapability), ", ", colorize("Stories", storiesCapability));
        Intrinsics.checkNotNullExpressionValue(concat, "concat(\n      colorize(\"…storiesCapability),\n    )");
        return concat;
    }

    private final CharSequence colorize(String str, Recipient.Capability capability) {
        int i = WhenMappings.$EnumSwitchMapping$0[capability.ordinal()];
        if (i == 1) {
            CharSequence color = SpanUtil.color(Color.rgb(0, 150, 0), str);
            Intrinsics.checkNotNullExpressionValue(color, "color(Color.rgb(0, 150, 0), name)");
            return color;
        } else if (i == 2) {
            CharSequence color2 = SpanUtil.color(-65536, str);
            Intrinsics.checkNotNullExpressionValue(color2, "color(Color.RED, name)");
            return color2;
        } else if (i == 3) {
            CharSequence italic = SpanUtil.italic(str);
            Intrinsics.checkNotNullExpressionValue(italic, "italic(name)");
            return italic;
        } else {
            throw new NoWhenBranchMatchedException();
        }
    }

    /* compiled from: InternalConversationSettingsFragment.kt */
    @Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\b\u0010\u0014\u001a\u00020\u0015H\u0014J\u0010\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0017\u001a\u00020\u0018H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0003\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u001f\u0010\f\u001a\u0010\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u000e0\u000e0\r¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0013X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalViewModel;", "Landroidx/lifecycle/ViewModel;", "Lorg/thoughtcrime/securesms/recipients/RecipientForeverObserver;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "liveRecipient", "Lorg/thoughtcrime/securesms/recipients/LiveRecipient;", "getLiveRecipient", "()Lorg/thoughtcrime/securesms/recipients/LiveRecipient;", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalState;", "kotlin.jvm.PlatformType", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "onCleared", "", "onRecipientChanged", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class InternalViewModel extends ViewModel implements RecipientForeverObserver {
        private final LiveRecipient liveRecipient;
        private final RecipientId recipientId;
        private final LiveData<InternalState> state;
        private final Store<InternalState> store;

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        public InternalViewModel(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
            Recipient resolved = Recipient.resolved(recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(recipientId)");
            Store<InternalState> store = new Store<>(new InternalState(resolved, null, null));
            this.store = store;
            LiveData<InternalState> stateLiveData = store.getStateLiveData();
            Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
            this.state = stateLiveData;
            LiveRecipient live = Recipient.live(recipientId);
            Intrinsics.checkNotNullExpressionValue(live, "live(recipientId)");
            this.liveRecipient = live;
            live.observeForever(this);
            SignalExecutors.BOUNDED.execute(new InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda0(this));
        }

        public final LiveData<InternalState> getState() {
            return this.state;
        }

        public final LiveRecipient getLiveRecipient() {
            return this.liveRecipient;
        }

        /* renamed from: _init_$lambda-2 */
        public static final void m1163_init_$lambda2(InternalViewModel internalViewModel) {
            Intrinsics.checkNotNullParameter(internalViewModel, "this$0");
            SignalDatabase.Companion companion = SignalDatabase.Companion;
            internalViewModel.store.update(new InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda2(companion.threads().getThreadIdFor(internalViewModel.recipientId), (GroupId) companion.groups().getGroup(internalViewModel.recipientId).map(new InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda1()).orElse(null)));
        }

        /* renamed from: lambda-2$lambda-0 */
        public static final GroupId m1164lambda2$lambda0(GroupDatabase.GroupRecord groupRecord) {
            return groupRecord.getId();
        }

        /* renamed from: lambda-2$lambda-1 */
        public static final InternalState m1165lambda2$lambda1(Long l, GroupId groupId, InternalState internalState) {
            Intrinsics.checkNotNullExpressionValue(internalState, "state");
            return InternalState.copy$default(internalState, null, l, groupId, 1, null);
        }

        /* renamed from: onRecipientChanged$lambda-3 */
        public static final InternalState m1166onRecipientChanged$lambda3(Recipient recipient, InternalState internalState) {
            Intrinsics.checkNotNullParameter(recipient, "$recipient");
            Intrinsics.checkNotNullExpressionValue(internalState, "state");
            return InternalState.copy$default(internalState, recipient, null, null, 6, null);
        }

        @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
        public void onRecipientChanged(Recipient recipient) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.store.update(new InternalConversationSettingsFragment$InternalViewModel$$ExternalSyntheticLambda3(recipient));
        }

        @Override // androidx.lifecycle.ViewModel
        public void onCleared() {
            this.liveRecipient.lambda$asObservable$6(this);
        }
    }

    /* compiled from: InternalConversationSettingsFragment.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$MyViewModelFactory;", "Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;)V", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class MyViewModelFactory extends ViewModelProvider.NewInstanceFactory {
        private final RecipientId recipientId;

        public MyViewModelFactory(RecipientId recipientId) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            this.recipientId = recipientId;
        }

        public final RecipientId getRecipientId() {
            return this.recipientId;
        }

        @Override // androidx.lifecycle.ViewModelProvider.NewInstanceFactory, androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new InternalViewModel(this.recipientId));
            Objects.requireNonNull(cast);
            Intrinsics.checkNotNullExpressionValue(cast, "requireNonNull(modelClas…lViewModel(recipientId)))");
            return cast;
        }
    }

    /* compiled from: InternalConversationSettingsFragment.kt */
    @Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0005HÆ\u0003¢\u0006\u0002\u0010\u000eJ\u000b\u0010\u0012\u001a\u0004\u0018\u00010\u0007HÆ\u0003J0\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007HÆ\u0001¢\u0006\u0002\u0010\u0014J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u001bHÖ\u0001R\u0013\u0010\u0006\u001a\u0004\u0018\u00010\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\n\n\u0002\u0010\u000f\u001a\u0004\b\r\u0010\u000e¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalState;", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "threadId", "", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Ljava/lang/Long;Lorg/thoughtcrime/securesms/groups/GroupId;)V", "getGroupId", "()Lorg/thoughtcrime/securesms/groups/GroupId;", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getThreadId", "()Ljava/lang/Long;", "Ljava/lang/Long;", "component1", "component2", "component3", "copy", "(Lorg/thoughtcrime/securesms/recipients/Recipient;Ljava/lang/Long;Lorg/thoughtcrime/securesms/groups/GroupId;)Lorg/thoughtcrime/securesms/components/settings/conversation/InternalConversationSettingsFragment$InternalState;", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class InternalState {
        private final GroupId groupId;
        private final Recipient recipient;
        private final Long threadId;

        public static /* synthetic */ InternalState copy$default(InternalState internalState, Recipient recipient, Long l, GroupId groupId, int i, Object obj) {
            if ((i & 1) != 0) {
                recipient = internalState.recipient;
            }
            if ((i & 2) != 0) {
                l = internalState.threadId;
            }
            if ((i & 4) != 0) {
                groupId = internalState.groupId;
            }
            return internalState.copy(recipient, l, groupId);
        }

        public final Recipient component1() {
            return this.recipient;
        }

        public final Long component2() {
            return this.threadId;
        }

        public final GroupId component3() {
            return this.groupId;
        }

        public final InternalState copy(Recipient recipient, Long l, GroupId groupId) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            return new InternalState(recipient, l, groupId);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InternalState)) {
                return false;
            }
            InternalState internalState = (InternalState) obj;
            return Intrinsics.areEqual(this.recipient, internalState.recipient) && Intrinsics.areEqual(this.threadId, internalState.threadId) && Intrinsics.areEqual(this.groupId, internalState.groupId);
        }

        public int hashCode() {
            int hashCode = this.recipient.hashCode() * 31;
            Long l = this.threadId;
            int i = 0;
            int hashCode2 = (hashCode + (l == null ? 0 : l.hashCode())) * 31;
            GroupId groupId = this.groupId;
            if (groupId != null) {
                i = groupId.hashCode();
            }
            return hashCode2 + i;
        }

        public String toString() {
            return "InternalState(recipient=" + this.recipient + ", threadId=" + this.threadId + ", groupId=" + this.groupId + ')';
        }

        public InternalState(Recipient recipient, Long l, GroupId groupId) {
            Intrinsics.checkNotNullParameter(recipient, RecipientDatabase.TABLE_NAME);
            this.recipient = recipient;
            this.threadId = l;
            this.groupId = groupId;
        }

        public final Recipient getRecipient() {
            return this.recipient;
        }

        public final Long getThreadId() {
            return this.threadId;
        }

        public final GroupId getGroupId() {
            return this.groupId;
        }
    }
}
