package org.thoughtcrime.securesms.components.webrtc;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.AccessibilityUtil;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class WebRtcAnswerDeclineButton extends LinearLayout implements AccessibilityManager.TouchExplorationStateChangeListener {
    private static final int ANSWER_THRESHOLD;
    private static final int DECLINE_THRESHOLD;
    private static final int DOWN_TIME;
    private static final int FADE_IN_TIME;
    private static final int FADE_OUT_TIME;
    private static final int SHAKE_TIME;
    private static final int SHIMMER_TOTAL;
    private static final String TAG = Log.tag(WebRtcAnswerDeclineButton.class);
    private static final int TOTAL_TIME;
    private static final int UP_TIME;
    private AccessibilityManager accessibilityManager;
    private DragToAnswer dragToAnswerListener;
    private AnswerDeclineListener listener;
    private boolean ringAnimation;

    /* loaded from: classes4.dex */
    public interface AnswerDeclineListener {
        void onAnswered();

        void onDeclined();
    }

    public WebRtcAnswerDeclineButton(Context context) {
        super(context);
        initialize();
    }

    public WebRtcAnswerDeclineButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public WebRtcAnswerDeclineButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    private void initialize() {
        setOrientation(1);
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        AccessibilityManager accessibilityManager = ServiceUtil.getAccessibilityManager(getContext());
        this.accessibilityManager = accessibilityManager;
        createView(accessibilityManager.isTouchExplorationEnabled());
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.accessibilityManager.addTouchExplorationStateChangeListener(this);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        this.accessibilityManager.removeTouchExplorationStateChangeListener(this);
        super.onDetachedFromWindow();
    }

    private void createView(boolean z) {
        if (z) {
            LinearLayout.inflate(getContext(), R.layout.webrtc_answer_decline_button_accessible, this);
            findViewById(R.id.answer).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcAnswerDeclineButton$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WebRtcAnswerDeclineButton.$r8$lambda$EX3DaAner3rpTkSgDp7jy9aUAe8(WebRtcAnswerDeclineButton.this, view);
                }
            });
            findViewById(R.id.reject).setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcAnswerDeclineButton$$ExternalSyntheticLambda1
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    WebRtcAnswerDeclineButton.$r8$lambda$mP0VZ5qeqiBvIexaOxbrN49LE2o(WebRtcAnswerDeclineButton.this, view);
                }
            });
            return;
        }
        LinearLayout.inflate(getContext(), R.layout.webrtc_answer_decline_button, this);
        ImageView imageView = (ImageView) findViewById(R.id.answer);
        DragToAnswer dragToAnswer = new DragToAnswer(imageView, this);
        this.dragToAnswerListener = dragToAnswer;
        imageView.setOnTouchListener(dragToAnswer);
        if (this.ringAnimation) {
            startRingingAnimation();
        }
    }

    public /* synthetic */ void lambda$createView$0(View view) {
        this.listener.onAnswered();
    }

    public /* synthetic */ void lambda$createView$1(View view) {
        this.listener.onDeclined();
    }

    public void setAnswerDeclineListener(AnswerDeclineListener answerDeclineListener) {
        this.listener = answerDeclineListener;
    }

    public void startRingingAnimation() {
        this.ringAnimation = true;
        DragToAnswer dragToAnswer = this.dragToAnswerListener;
        if (dragToAnswer != null) {
            dragToAnswer.startRingingAnimation();
        }
    }

    public void stopRingingAnimation() {
        this.ringAnimation = false;
        DragToAnswer dragToAnswer = this.dragToAnswerListener;
        if (dragToAnswer != null) {
            dragToAnswer.stopRingingAnimation();
        }
    }

    @Override // android.view.accessibility.AccessibilityManager.TouchExplorationStateChangeListener
    public void onTouchExplorationStateChanged(boolean z) {
        removeAllViews();
        createView(z);
    }

    /* loaded from: classes4.dex */
    public class DragToAnswer implements View.OnTouchListener {
        private boolean animating;
        private AnimatorSet animatorSet;
        private final ImageView answer;
        private final ImageView arrowFour;
        private final ImageView arrowOne;
        private final ImageView arrowThree;
        private final ImageView arrowTwo;
        private boolean complete;
        private float lastY;
        private final TextView swipeDownText;
        private final TextView swipeUpText;

        private DragToAnswer(ImageView imageView, WebRtcAnswerDeclineButton webRtcAnswerDeclineButton) {
            WebRtcAnswerDeclineButton.this = r1;
            this.animating = false;
            this.complete = false;
            this.swipeUpText = (TextView) webRtcAnswerDeclineButton.findViewById(R.id.swipe_up_text);
            this.answer = imageView;
            this.swipeDownText = (TextView) webRtcAnswerDeclineButton.findViewById(R.id.swipe_down_text);
            this.arrowOne = (ImageView) webRtcAnswerDeclineButton.findViewById(R.id.arrow_one);
            this.arrowTwo = (ImageView) webRtcAnswerDeclineButton.findViewById(R.id.arrow_two);
            this.arrowThree = (ImageView) webRtcAnswerDeclineButton.findViewById(R.id.arrow_three);
            this.arrowFour = (ImageView) webRtcAnswerDeclineButton.findViewById(R.id.arrow_four);
        }

        public void startRingingAnimation() {
            if (!this.animating) {
                this.animating = true;
                animateElements(0);
            }
        }

        public void stopRingingAnimation() {
            if (this.animating) {
                this.animating = false;
                resetElements();
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0016, code lost:
            if (r14 != 3) goto L_0x01af;
         */
        @Override // android.view.View.OnTouchListener
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean onTouch(android.view.View r14, android.view.MotionEvent r15) {
            /*
            // Method dump skipped, instructions count: 432
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.webrtc.WebRtcAnswerDeclineButton.DragToAnswer.onTouch(android.view.View, android.view.MotionEvent):boolean");
        }

        public void animateElements(int i) {
            if (!AccessibilityUtil.areAnimationsDisabled(WebRtcAnswerDeclineButton.this.getContext())) {
                ObjectAnimator upAnimation = WebRtcAnswerDeclineButton.getUpAnimation(this.answer);
                ObjectAnimator downAnimation = WebRtcAnswerDeclineButton.getDownAnimation(this.answer);
                ObjectAnimator shakeAnimation = WebRtcAnswerDeclineButton.getShakeAnimation(this.answer);
                AnimatorSet animatorSet = new AnimatorSet();
                this.animatorSet = animatorSet;
                animatorSet.play(upAnimation).with(WebRtcAnswerDeclineButton.getUpAnimation(this.swipeUpText));
                this.animatorSet.play(shakeAnimation).after(upAnimation);
                this.animatorSet.play(downAnimation).with(WebRtcAnswerDeclineButton.getDownAnimation(this.swipeUpText)).after(shakeAnimation);
                this.animatorSet.play(WebRtcAnswerDeclineButton.getFadeOut(this.swipeDownText)).with(upAnimation);
                this.animatorSet.play(WebRtcAnswerDeclineButton.getFadeIn(this.swipeDownText)).after(downAnimation);
                this.animatorSet.play(WebRtcAnswerDeclineButton.getShimmer(this.arrowFour, this.arrowThree, this.arrowTwo, this.arrowOne));
                this.animatorSet.addListener(new Animator.AnimatorListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcAnswerDeclineButton.DragToAnswer.1
                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationCancel(Animator animator) {
                    }

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationRepeat(Animator animator) {
                    }

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationStart(Animator animator) {
                    }

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        if (DragToAnswer.this.animating) {
                            DragToAnswer.this.animateElements(1000);
                        }
                    }
                });
                this.animatorSet.setStartDelay((long) i);
                this.animatorSet.start();
            }
        }

        private void resetElements() {
            this.animating = false;
            this.complete = false;
            AnimatorSet animatorSet = this.animatorSet;
            if (animatorSet != null) {
                animatorSet.cancel();
            }
            this.swipeUpText.setTranslationY(0.0f);
            this.answer.setTranslationY(0.0f);
            this.swipeDownText.setAlpha(1.0f);
        }
    }

    public static Animator getShimmer(View... viewArr) {
        AnimatorSet animatorSet = new AnimatorSet();
        int length = SHIMMER_TOTAL / viewArr.length;
        for (int i = 0; i < viewArr.length; i++) {
            animatorSet.play(getShimmer(viewArr[i], (length - 75) + length)).after((long) (75 * i));
        }
        return animatorSet;
    }

    private static ObjectAnimator getShimmer(View view, int i) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", 0.0f, 1.0f, 0.0f);
        ofFloat.setDuration((long) i);
        return ofFloat;
    }

    public static ObjectAnimator getShakeAnimation(View view) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationX", 0.0f, 25.0f, -25.0f, 25.0f, -25.0f, 15.0f, -15.0f, 6.0f, -6.0f, 0.0f);
        ofFloat.setDuration(200L);
        return ofFloat;
    }

    public static ObjectAnimator getUpAnimation(View view) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationY", 0.0f, (float) (ViewUtil.dpToPx(view.getContext(), 32) * -1));
        ofFloat.setInterpolator(new AccelerateInterpolator());
        ofFloat.setDuration(400L);
        return ofFloat;
    }

    public static ObjectAnimator getDownAnimation(View view) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "translationY", 0.0f);
        ofFloat.setInterpolator(new DecelerateInterpolator());
        ofFloat.setDuration(400L);
        return ofFloat;
    }

    public static ObjectAnimator getFadeOut(View view) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", 1.0f, 0.0f);
        ofFloat.setDuration(300L);
        return ofFloat;
    }

    public static ObjectAnimator getFadeIn(View view) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, "alpha", 0.0f, 1.0f);
        ofFloat.setDuration(100L);
        return ofFloat;
    }
}
