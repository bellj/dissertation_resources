package org.thoughtcrime.securesms.components.mention;

import android.text.Annotation;
import android.text.Editable;
import android.text.Spanned;
import android.text.TextWatcher;
import java.util.List;

/* loaded from: classes4.dex */
public class MentionValidatorWatcher implements TextWatcher {
    private List<Annotation> invalidMentionAnnotations;
    private MentionValidator mentionValidator;

    /* loaded from: classes4.dex */
    public interface MentionValidator {
        List<Annotation> getInvalidMentionAnnotations(List<Annotation> list);
    }

    @Override // android.text.TextWatcher
    public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
    }

    @Override // android.text.TextWatcher
    public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        if (i3 > 1 && this.mentionValidator != null && (charSequence instanceof Spanned)) {
            List<Annotation> mentionAnnotations = MentionAnnotation.getMentionAnnotations((Spanned) charSequence, i, i3 + i);
            if (mentionAnnotations.size() > 0) {
                this.invalidMentionAnnotations = this.mentionValidator.getInvalidMentionAnnotations(mentionAnnotations);
            }
        }
    }

    @Override // android.text.TextWatcher
    public void afterTextChanged(Editable editable) {
        List<Annotation> list = this.invalidMentionAnnotations;
        if (list != null) {
            this.invalidMentionAnnotations = null;
            for (Annotation annotation : list) {
                editable.removeSpan(annotation);
            }
        }
    }

    public void setMentionValidator(MentionValidator mentionValidator) {
        this.mentionValidator = mentionValidator;
    }
}
