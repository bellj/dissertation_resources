package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CompoundButton;
import androidx.appcompat.widget.AppCompatToggleButton;

/* loaded from: classes4.dex */
public class AccessibleToggleButton extends AppCompatToggleButton {
    private CompoundButton.OnCheckedChangeListener listener;

    public AccessibleToggleButton(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public AccessibleToggleButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public AccessibleToggleButton(Context context) {
        super(context);
    }

    @Override // android.widget.CompoundButton
    public void setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        super.setOnCheckedChangeListener(onCheckedChangeListener);
        this.listener = onCheckedChangeListener;
    }

    public void setChecked(boolean z, boolean z2) {
        if (!z2) {
            super.setOnCheckedChangeListener(null);
        }
        super.setChecked(z);
        if (!z2) {
            super.setOnCheckedChangeListener(this.listener);
        }
    }

    public CompoundButton.OnCheckedChangeListener getOnCheckedChangeListener() {
        return this.listener;
    }
}
