package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Consumer;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.contactshare.Contact;
import org.thoughtcrime.securesms.contactshare.ContactUtil;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class SharedContactView extends LinearLayout implements RecipientForeverObserver {
    private TextView actionButtonView;
    private final Map<RecipientId, LiveRecipient> activeRecipients = new HashMap();
    private ImageView avatarView;
    private int bigCornerRadius;
    private Contact contact;
    private CornerMask cornerMask;
    private EventListener eventListener;
    private ConversationItemFooter footer;
    private GlideRequests glideRequests;
    private Locale locale;
    private TextView nameView;
    private TextView numberView;
    private int smallCornerRadius;

    /* loaded from: classes4.dex */
    public interface EventListener {
        void onAddToContactsClicked(Contact contact);

        void onInviteClicked(List<Recipient> list);

        void onMessageClicked(List<Recipient> list);
    }

    public SharedContactView(Context context) {
        super(context);
        initialize(null);
    }

    public SharedContactView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(attributeSet);
    }

    public SharedContactView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(attributeSet);
    }

    public SharedContactView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        initialize(attributeSet);
    }

    private void initialize(AttributeSet attributeSet) {
        LinearLayout.inflate(getContext(), R.layout.shared_contact_view, this);
        this.avatarView = (ImageView) findViewById(R.id.contact_avatar);
        this.nameView = (TextView) findViewById(R.id.contact_name);
        this.numberView = (TextView) findViewById(R.id.contact_number);
        this.actionButtonView = (TextView) findViewById(R.id.contact_action_button);
        this.footer = (ConversationItemFooter) findViewById(R.id.contact_footer);
        this.cornerMask = new CornerMask(this);
        this.bigCornerRadius = getResources().getDimensionPixelOffset(R.dimen.message_corner_radius);
        this.smallCornerRadius = getResources().getDimensionPixelOffset(R.dimen.message_corner_collapse_radius);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.SharedContactView, 0, 0);
            int i = obtainStyledAttributes.getInt(3, -16777216);
            int i2 = obtainStyledAttributes.getInt(0, -16777216);
            int i3 = obtainStyledAttributes.getInt(2, -16777216);
            float f = obtainStyledAttributes.getFloat(1, 1.0f);
            obtainStyledAttributes.recycle();
            this.nameView.setTextColor(i);
            this.numberView.setTextColor(i2);
            this.footer.setTextColor(i2);
            this.footer.setIconColor(i3);
            this.footer.setAlpha(f);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        this.cornerMask.mask(canvas);
    }

    public void setContact(Contact contact, GlideRequests glideRequests, Locale locale) {
        this.glideRequests = glideRequests;
        this.locale = locale;
        this.contact = contact;
        Stream.of(this.activeRecipients.values()).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.components.SharedContactView$$ExternalSyntheticLambda3
            @Override // com.annimon.stream.function.Consumer
            public final void accept(Object obj) {
                SharedContactView.$r8$lambda$mWhHFJDvFWX4Bk6vCxLOnTOXTMQ(SharedContactView.this, (LiveRecipient) obj);
            }
        });
        this.activeRecipients.clear();
        presentContact(contact);
        presentAvatar(contact.getAvatarAttachment() != null ? contact.getAvatarAttachment().getUri() : null);
        presentActionButtons(ContactUtil.getRecipients(getContext(), contact));
        for (LiveRecipient liveRecipient : this.activeRecipients.values()) {
            liveRecipient.observeForever(this);
        }
    }

    public /* synthetic */ void lambda$setContact$0(LiveRecipient liveRecipient) {
        liveRecipient.lambda$asObservable$6(this);
    }

    public void setSingularStyle() {
        this.cornerMask.setBottomLeftRadius(this.bigCornerRadius);
        this.cornerMask.setBottomRightRadius(this.bigCornerRadius);
    }

    public void setClusteredIncomingStyle() {
        this.cornerMask.setBottomLeftRadius(this.smallCornerRadius);
        this.cornerMask.setBottomRightRadius(this.bigCornerRadius);
    }

    public void setClusteredOutgoingStyle() {
        this.cornerMask.setBottomLeftRadius(this.bigCornerRadius);
        this.cornerMask.setBottomRightRadius(this.smallCornerRadius);
    }

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public View getAvatarView() {
        return this.avatarView;
    }

    public ConversationItemFooter getFooter() {
        return this.footer;
    }

    @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
    public void onRecipientChanged(Recipient recipient) {
        presentActionButtons(Collections.singletonList(recipient.getId()));
    }

    private void presentContact(Contact contact) {
        if (contact != null) {
            this.nameView.setText(ContactUtil.getDisplayName(contact));
            this.numberView.setText(ContactUtil.getDisplayNumber(contact, this.locale));
            return;
        }
        this.nameView.setText("");
        this.numberView.setText("");
    }

    private void presentAvatar(Uri uri) {
        if (uri != null) {
            this.glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).fallback((int) R.drawable.ic_contact_picture).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).dontAnimate().into(this.avatarView);
        } else {
            this.glideRequests.load(Integer.valueOf((int) R.drawable.ic_contact_picture)).circleCrop().diskCacheStrategy(DiskCacheStrategy.ALL).into(this.avatarView);
        }
    }

    private void presentActionButtons(List<RecipientId> list) {
        for (RecipientId recipientId : list) {
            this.activeRecipients.put(recipientId, Recipient.live(recipientId));
        }
        ArrayList arrayList = new ArrayList(list.size());
        ArrayList arrayList2 = new ArrayList(list.size());
        for (LiveRecipient liveRecipient : this.activeRecipients.values()) {
            if (liveRecipient.get().getRegistered() == RecipientDatabase.RegisteredState.REGISTERED) {
                arrayList.add(liveRecipient.get());
            } else if (liveRecipient.get().isSystemContact()) {
                arrayList2.add(liveRecipient.get());
            }
        }
        if (!arrayList.isEmpty()) {
            this.actionButtonView.setText(R.string.SharedContactView_message);
            this.actionButtonView.setOnClickListener(new View.OnClickListener(arrayList) { // from class: org.thoughtcrime.securesms.components.SharedContactView$$ExternalSyntheticLambda0
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactView.$r8$lambda$Guigv4Jd3GZqMIR_Q5UZ1g0QbOA(SharedContactView.this, this.f$1, view);
                }
            });
        } else if (!arrayList2.isEmpty()) {
            this.actionButtonView.setText(R.string.SharedContactView_invite_to_signal);
            this.actionButtonView.setOnClickListener(new View.OnClickListener(arrayList2) { // from class: org.thoughtcrime.securesms.components.SharedContactView$$ExternalSyntheticLambda1
                public final /* synthetic */ List f$1;

                {
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactView.m490$r8$lambda$P1Rr1mwunckAchCPbeYZlYV9vI(SharedContactView.this, this.f$1, view);
                }
            });
        } else {
            this.actionButtonView.setText(R.string.SharedContactView_add_to_contacts);
            this.actionButtonView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.SharedContactView$$ExternalSyntheticLambda2
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    SharedContactView.$r8$lambda$HlKy4TGmgDe1V0JNfDXRX5JLwyw(SharedContactView.this, view);
                }
            });
        }
    }

    public /* synthetic */ void lambda$presentActionButtons$1(List list, View view) {
        EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onMessageClicked(list);
        }
    }

    public /* synthetic */ void lambda$presentActionButtons$2(List list, View view) {
        EventListener eventListener = this.eventListener;
        if (eventListener != null) {
            eventListener.onInviteClicked(list);
        }
    }

    public /* synthetic */ void lambda$presentActionButtons$3(View view) {
        Contact contact;
        EventListener eventListener = this.eventListener;
        if (eventListener != null && (contact = this.contact) != null) {
            eventListener.onAddToContactsClicked(contact);
        }
    }
}
