package org.thoughtcrime.securesms.components.webrtc;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsLayout;
import org.thoughtcrime.securesms.events.CallParticipant;

/* loaded from: classes4.dex */
public class WebRtcCallParticipantsPage {
    private final List<CallParticipant> callParticipants;
    private final CallParticipant focusedParticipant;
    private final boolean isIncomingRing;
    private final boolean isLandscapeEnabled;
    private final boolean isPortrait;
    private final boolean isRenderInPip;
    private final boolean isSpeaker;
    private final int navBarBottomInset;

    public static WebRtcCallParticipantsPage forMultipleParticipants(List<CallParticipant> list, CallParticipant callParticipant, boolean z, boolean z2, boolean z3, boolean z4, int i) {
        return new WebRtcCallParticipantsPage(list, callParticipant, false, z, z2, z3, z4, i);
    }

    public static WebRtcCallParticipantsPage forSingleParticipant(CallParticipant callParticipant, boolean z, boolean z2, boolean z3) {
        return new WebRtcCallParticipantsPage(Collections.singletonList(callParticipant), callParticipant, true, z, z2, z3, false, 0);
    }

    private WebRtcCallParticipantsPage(List<CallParticipant> list, CallParticipant callParticipant, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, int i) {
        this.callParticipants = list;
        this.focusedParticipant = callParticipant;
        this.isSpeaker = z;
        this.isRenderInPip = z2;
        this.isPortrait = z3;
        this.isLandscapeEnabled = z4;
        this.isIncomingRing = z5;
        this.navBarBottomInset = i;
    }

    public List<CallParticipant> getCallParticipants() {
        return this.callParticipants;
    }

    public CallParticipant getFocusedParticipant() {
        return this.focusedParticipant;
    }

    public boolean isRenderInPip() {
        return this.isRenderInPip;
    }

    public boolean isSpeaker() {
        return this.isSpeaker;
    }

    public boolean isPortrait() {
        return this.isPortrait;
    }

    public boolean isIncomingRing() {
        return this.isIncomingRing;
    }

    public int getNavBarBottomInset() {
        return this.navBarBottomInset;
    }

    public CallParticipantsLayout.LayoutStrategy getLayoutStrategy() {
        return CallParticipantsLayoutStrategies.getStrategy(this.isPortrait, this.isLandscapeEnabled);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        WebRtcCallParticipantsPage webRtcCallParticipantsPage = (WebRtcCallParticipantsPage) obj;
        if (this.isSpeaker == webRtcCallParticipantsPage.isSpeaker && this.isRenderInPip == webRtcCallParticipantsPage.isRenderInPip && this.isPortrait == webRtcCallParticipantsPage.isPortrait && this.isLandscapeEnabled == webRtcCallParticipantsPage.isLandscapeEnabled && this.isIncomingRing == webRtcCallParticipantsPage.isIncomingRing && this.callParticipants.equals(webRtcCallParticipantsPage.callParticipants) && this.focusedParticipant.equals(webRtcCallParticipantsPage.focusedParticipant) && this.navBarBottomInset == webRtcCallParticipantsPage.navBarBottomInset) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Objects.hash(this.callParticipants, this.focusedParticipant, Boolean.valueOf(this.isSpeaker), Boolean.valueOf(this.isRenderInPip), Boolean.valueOf(this.isPortrait), Boolean.valueOf(this.isLandscapeEnabled), Boolean.valueOf(this.isIncomingRing), Integer.valueOf(this.navBarBottomInset));
    }
}
