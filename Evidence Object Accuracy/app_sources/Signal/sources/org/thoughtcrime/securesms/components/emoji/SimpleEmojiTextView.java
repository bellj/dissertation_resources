package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import j$.util.Optional;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.parsing.EmojiParser;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.ThrottledDebouncer;

/* compiled from: SimpleEmojiTextView.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\r\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B%\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ(\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00072\u0006\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u00072\u0006\u0010\u0012\u001a\u00020\u0007H\u0014J\u001c\u0010\u0013\u001a\u00020\u000e2\b\u0010\u0014\u001a\u0004\u0018\u00010\u00152\b\u0010\u0016\u001a\u0004\u0018\u00010\nH\u0016R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u000e¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/emoji/SimpleEmojiTextView;", "Landroidx/appcompat/widget/AppCompatTextView;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "bufferType", "Landroid/widget/TextView$BufferType;", "sizeChangeDebouncer", "Lorg/thoughtcrime/securesms/util/ThrottledDebouncer;", "onSizeChanged", "", "width", "height", "oldWidth", "oldHeight", "setText", DraftDatabase.Draft.TEXT, "", "type", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class SimpleEmojiTextView extends AppCompatTextView {
    private TextView.BufferType bufferType;
    private final ThrottledDebouncer sizeChangeDebouncer;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public SimpleEmojiTextView(Context context) {
        this(context, null, 0, 6, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public SimpleEmojiTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0, 4, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ SimpleEmojiTextView(Context context, AttributeSet attributeSet, int i, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i2 & 2) != 0 ? null : attributeSet, (i2 & 4) != 0 ? 0 : i);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SimpleEmojiTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Intrinsics.checkNotNullParameter(context, "context");
        this.sizeChangeDebouncer = new ThrottledDebouncer(200);
    }

    @Override // android.widget.TextView
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        this.bufferType = bufferType;
        EmojiParser.CandidateList candidateList = null;
        EmojiParser.CandidateList candidates = isInEditMode() ? null : EmojiProvider.getCandidates(charSequence);
        if (SignalStore.settings().isPreferSystemEmoji() || candidates == null || candidates.size() == 0) {
            super.setText((CharSequence) Optional.ofNullable(charSequence).orElse(""), bufferType);
            return;
        }
        Drawable drawable = getCompoundDrawables()[0];
        int intrinsicWidth = drawable != null ? drawable.getIntrinsicWidth() + getCompoundDrawablePadding() : 0;
        Drawable drawable2 = getCompoundDrawables()[1];
        int width = (getWidth() - intrinsicWidth) - (drawable2 != null ? drawable2.getIntrinsicWidth() + getCompoundDrawablePadding() : 0);
        if (!isInEditMode()) {
            candidateList = EmojiProvider.getCandidates(charSequence);
        }
        if (!(candidateList == null || candidateList.size() == 0)) {
            charSequence = EmojiProvider.emojify(candidateList, charSequence, this, false);
        }
        CharSequence charSequence2 = charSequence;
        if (!(getWidth() == 0 || getMaxLines() == -1)) {
            charSequence2 = TextUtils.ellipsize(charSequence2, getPaint(), (float) (width * getMaxLines()), TextUtils.TruncateAt.END, false, null);
        }
        this.bufferType = TextView.BufferType.SPANNABLE;
        super.setText(charSequence2, bufferType);
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.sizeChangeDebouncer.publish(new Runnable(i, i3, this) { // from class: org.thoughtcrime.securesms.components.emoji.SimpleEmojiTextView$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;
            public final /* synthetic */ int f$1;
            public final /* synthetic */ SimpleEmojiTextView f$2;

            {
                this.f$0 = r1;
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                SimpleEmojiTextView.m505onSizeChanged$lambda2(this.f$0, this.f$1, this.f$2);
            }
        });
    }

    /* renamed from: onSizeChanged$lambda-2 */
    public static final void m505onSizeChanged$lambda2(int i, int i2, SimpleEmojiTextView simpleEmojiTextView) {
        Intrinsics.checkNotNullParameter(simpleEmojiTextView, "this$0");
        if (i > 0 && i2 != i) {
            CharSequence text = simpleEmojiTextView.getText();
            TextView.BufferType bufferType = simpleEmojiTextView.bufferType;
            if (bufferType == null) {
                bufferType = TextView.BufferType.SPANNABLE;
            }
            simpleEmojiTextView.setText(text, bufferType);
        }
    }
}
