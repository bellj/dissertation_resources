package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.WindowInsets;
import android.view.animation.Animation;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Guideline;
import androidx.core.util.Consumer;
import androidx.core.view.ViewKt;
import androidx.core.view.WindowInsetsCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;
import androidx.transition.TransitionSet;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;
import com.annimon.stream.function.LongFunction;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.google.android.material.button.MaterialButton;
import com.google.common.collect.Sets;
import j$.util.function.Consumer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.SetUtil;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.ResizeAnimation;
import org.thoughtcrime.securesms.components.AccessibleToggleButton;
import org.thoughtcrime.securesms.components.AvatarImageView;
import org.thoughtcrime.securesms.components.webrtc.CallParticipantsState;
import org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;
import org.thoughtcrime.securesms.contacts.avatars.ProfileContactPhoto;
import org.thoughtcrime.securesms.events.CallParticipant;
import org.thoughtcrime.securesms.events.WebRtcViewModel;
import org.thoughtcrime.securesms.mediasend.SimpleAnimationListener;
import org.thoughtcrime.securesms.mms.GlideApp;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.ringrtc.CameraState;
import org.thoughtcrime.securesms.util.BlurTransformation;
import org.thoughtcrime.securesms.util.ThrottledDebouncer;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.Stub;
import org.thoughtcrime.securesms.webrtc.CallParticipantsViewState;
import org.webrtc.EglBase;
import org.webrtc.RendererCommon;
import org.whispersystems.signalservice.api.messages.calls.HangupMessage;

/* loaded from: classes4.dex */
public class WebRtcCallView extends ConstraintLayout {
    public static final int CONTROLS_HEIGHT;
    public static final int FADE_OUT_DELAY;
    private static final int LARGE_ONGOING_CALL_BUTTON_MARGIN_DP;
    public static final int PIP_RESIZE_DURATION;
    private static final int SMALL_ONGOING_CALL_BUTTON_MARGIN_DP;
    private static final long TRANSITION_DURATION_MILLIS;
    private final Set<View> adjustableMarginsSet;
    private final Set<View> allTimeVisibleViews;
    private ImageView answer;
    private View answerWithoutVideo;
    private View answerWithoutVideoLabel;
    private WebRtcAudioOutputToggleButton audioToggle;
    private TextView audioToggleLabel;
    private ViewPager2 callParticipantsPager;
    private RecyclerView callParticipantsRecycler;
    private Guideline callScreenTopFoldGuideline;
    private ImageView cameraDirectionToggle;
    private TextView cameraDirectionToggleLabel;
    private WebRtcControls controls;
    private ControlsListener controlsListener;
    private boolean controlsVisible;
    private View errorButton;
    private final Runnable fadeOutRunnable;
    private TextView foldParticipantCount;
    private View foldParticipantCountWrapper;
    private View footerGradient;
    private View fullScreenShade;
    private Stub<View> groupCallFullStub;
    private Stub<FrameLayout> groupCallSpeakerHint;
    private ImageView hangup;
    private TextView hangupLabel;
    private final Set<View> incomingCallViews;
    private TextView incomingRingStatus;
    private AvatarImageView largeHeaderAvatar;
    private ConstraintSet largeHeaderConstraints;
    private TextureViewRenderer largeLocalRender;
    private View largeLocalRenderFrame;
    private View largeLocalRenderNoVideo;
    private ImageView largeLocalRenderNoVideoAvatar;
    private CallParticipantsViewState lastState;
    private AccessibleToggleButton micToggle;
    private TextView micToggleLabel;
    private int navBarBottomInset;
    private WebRtcCallParticipantsPagerAdapter pagerAdapter;
    private int pagerBottomMarginDp;
    private ConstraintLayout parent;
    private ConstraintLayout participantsParent;
    private PictureInPictureExpansionHelper pictureInPictureExpansionHelper;
    private PictureInPictureGestureHelper pictureInPictureGestureHelper;
    private RecipientId recipientId;
    private TextView recipientName;
    private WebRtcCallParticipantsRecyclerAdapter recyclerAdapter;
    private AccessibleToggleButton ringToggle;
    private TextView ringToggleLabel;
    private final Set<View> rotatableControls;
    private Guideline showParticipantsGuideline;
    private ConstraintSet smallHeaderConstraints;
    private CallParticipantView smallLocalRender;
    private ViewGroup smallLocalRenderFrame;
    private MaterialButton startCall;
    private View startCallControls;
    private TextView status;
    private Guideline statusBarGuideline;
    private final ThrottledDebouncer throttledDebouncer;
    private ConstraintLayout toolbar;
    private Guideline topFoldGuideline;
    private View topGradient;
    private final Set<View> topViews;
    private AccessibleToggleButton videoToggle;
    private TextView videoToggleLabel;
    private final Set<View> visibleViewSet;

    /* loaded from: classes4.dex */
    public interface ControlsListener {
        void hideSystemUI();

        void onAcceptCallPressed();

        void onAcceptCallWithVoiceOnlyPressed();

        void onAudioOutputChanged(WebRtcAudioOutput webRtcAudioOutput);

        void onCameraDirectionChanged();

        void onCancelStartCall();

        void onControlsFadeOut();

        void onDenyCallPressed();

        void onEndCallPressed();

        void onLocalPictureInPictureClicked();

        void onMicChanged(boolean z);

        void onPageChanged(CallParticipantsState.SelectedPage selectedPage);

        void onRingGroupChanged(boolean z, boolean z2);

        void onShowParticipantsList();

        void onStartCall(boolean z);

        void onVideoChanged(boolean z);

        void showSystemUI();
    }

    public /* synthetic */ void lambda$new$0() {
        if (isAttachedToWindow() && this.controls.isFadeOutEnabled()) {
            fadeOutControls();
        }
    }

    public WebRtcCallView(Context context) {
        this(context, null);
    }

    public WebRtcCallView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.controlsVisible = true;
        this.incomingCallViews = new HashSet();
        this.topViews = new HashSet();
        this.visibleViewSet = new HashSet();
        this.allTimeVisibleViews = new HashSet();
        this.adjustableMarginsSet = new HashSet();
        this.rotatableControls = new HashSet();
        this.throttledDebouncer = new ThrottledDebouncer(TRANSITION_DURATION_MILLIS);
        this.controls = WebRtcControls.NONE;
        this.fadeOutRunnable = new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda14
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallView.this.lambda$new$0();
            }
        };
        ViewGroup.inflate(context, R.layout.webrtc_call_view, this);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.audioToggle = (WebRtcAudioOutputToggleButton) findViewById(R.id.call_screen_speaker_toggle);
        this.audioToggleLabel = (TextView) findViewById(R.id.call_screen_speaker_toggle_label);
        this.videoToggle = (AccessibleToggleButton) findViewById(R.id.call_screen_video_toggle);
        this.videoToggleLabel = (TextView) findViewById(R.id.call_screen_video_toggle_label);
        this.micToggle = (AccessibleToggleButton) findViewById(R.id.call_screen_audio_mic_toggle);
        this.micToggleLabel = (TextView) findViewById(R.id.call_screen_audio_mic_toggle_label);
        this.smallLocalRenderFrame = (ViewGroup) findViewById(R.id.call_screen_pip);
        this.smallLocalRender = (CallParticipantView) findViewById(R.id.call_screen_small_local_renderer);
        this.largeLocalRenderFrame = findViewById(R.id.call_screen_large_local_renderer_frame);
        this.largeLocalRender = (TextureViewRenderer) findViewById(R.id.call_screen_large_local_renderer);
        this.largeLocalRenderNoVideo = findViewById(R.id.call_screen_large_local_video_off);
        this.largeLocalRenderNoVideoAvatar = (ImageView) findViewById(R.id.call_screen_large_local_video_off_avatar);
        this.recipientName = (TextView) findViewById(R.id.call_screen_recipient_name);
        this.status = (TextView) findViewById(R.id.call_screen_status);
        this.incomingRingStatus = (TextView) findViewById(R.id.call_screen_incoming_ring_status);
        this.parent = (ConstraintLayout) findViewById(R.id.call_screen);
        this.participantsParent = (ConstraintLayout) findViewById(R.id.call_screen_participants_parent);
        this.answer = (ImageView) findViewById(R.id.call_screen_answer_call);
        this.cameraDirectionToggle = (ImageView) findViewById(R.id.call_screen_camera_direction_toggle);
        this.cameraDirectionToggleLabel = (TextView) findViewById(R.id.call_screen_camera_direction_toggle_label);
        this.ringToggle = (AccessibleToggleButton) findViewById(R.id.call_screen_audio_ring_toggle);
        this.ringToggleLabel = (TextView) findViewById(R.id.call_screen_audio_ring_toggle_label);
        this.hangup = (ImageView) findViewById(R.id.call_screen_end_call);
        this.hangupLabel = (TextView) findViewById(R.id.call_screen_end_call_label);
        this.answerWithoutVideo = findViewById(R.id.call_screen_answer_without_video);
        this.answerWithoutVideoLabel = findViewById(R.id.call_screen_answer_without_video_label);
        this.topGradient = findViewById(R.id.call_screen_header_gradient);
        this.footerGradient = findViewById(R.id.call_screen_footer_gradient);
        this.startCallControls = findViewById(R.id.call_screen_start_call_controls);
        this.callParticipantsPager = (ViewPager2) findViewById(R.id.call_screen_participants_pager);
        this.callParticipantsRecycler = (RecyclerView) findViewById(R.id.call_screen_participants_recycler);
        this.toolbar = (ConstraintLayout) findViewById(R.id.call_screen_header);
        this.startCall = (MaterialButton) findViewById(R.id.call_screen_start_call_start_call);
        this.errorButton = findViewById(R.id.call_screen_error_cancel);
        this.groupCallSpeakerHint = new Stub<>((ViewStub) findViewById(R.id.call_screen_group_call_speaker_hint));
        this.groupCallFullStub = new Stub<>((ViewStub) findViewById(R.id.group_call_call_full_view));
        this.showParticipantsGuideline = (Guideline) findViewById(R.id.call_screen_show_participants_guideline);
        this.topFoldGuideline = (Guideline) findViewById(R.id.fold_top_guideline);
        this.callScreenTopFoldGuideline = (Guideline) findViewById(R.id.fold_top_call_screen_guideline);
        this.foldParticipantCountWrapper = findViewById(R.id.fold_show_participants_menu_counter_wrapper);
        this.foldParticipantCount = (TextView) findViewById(R.id.fold_show_participants_menu_counter);
        this.largeHeaderAvatar = (AvatarImageView) findViewById(R.id.call_screen_header_avatar);
        this.statusBarGuideline = (Guideline) findViewById(R.id.call_screen_status_bar_guideline);
        this.fullScreenShade = findViewById(R.id.call_screen_full_shade);
        View findViewById = findViewById(R.id.call_screen_decline_call);
        View findViewById2 = findViewById(R.id.call_screen_answer_call_label);
        View findViewById3 = findViewById(R.id.call_screen_decline_call_label);
        this.callParticipantsPager.setPageTransformer(new MarginPageTransformer(ViewUtil.dpToPx(4)));
        this.pagerAdapter = new WebRtcCallParticipantsPagerAdapter(new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallView.this.toggleControls();
            }
        });
        this.recyclerAdapter = new WebRtcCallParticipantsRecyclerAdapter();
        this.callParticipantsPager.setAdapter(this.pagerAdapter);
        this.callParticipantsRecycler.setAdapter(this.recyclerAdapter);
        DefaultItemAnimator defaultItemAnimator = new DefaultItemAnimator();
        defaultItemAnimator.setSupportsChangeAnimations(false);
        this.callParticipantsRecycler.setItemAnimator(defaultItemAnimator);
        this.callParticipantsPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.1
            public static /* synthetic */ void lambda$onPageSelected$0(int i, ControlsListener controlsListener) {
                controlsListener.onPageChanged(i == 0 ? CallParticipantsState.SelectedPage.GRID : CallParticipantsState.SelectedPage.FOCUSED);
            }

            @Override // androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
            public void onPageSelected(int i) {
                WebRtcCallView.runIfNonNull(WebRtcCallView.this.controlsListener, new WebRtcCallView$1$$ExternalSyntheticLambda0(i));
            }
        });
        this.topViews.add(this.toolbar);
        this.topViews.add(this.topGradient);
        this.incomingCallViews.add(this.answer);
        this.incomingCallViews.add(findViewById2);
        this.incomingCallViews.add(findViewById);
        this.incomingCallViews.add(findViewById3);
        this.incomingCallViews.add(this.footerGradient);
        this.incomingCallViews.add(this.incomingRingStatus);
        this.adjustableMarginsSet.add(this.micToggle);
        this.adjustableMarginsSet.add(this.cameraDirectionToggle);
        this.adjustableMarginsSet.add(this.videoToggle);
        this.adjustableMarginsSet.add(this.audioToggle);
        this.audioToggle.setOnAudioOutputChangedListener(new OnAudioOutputChangedListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.components.webrtc.OnAudioOutputChangedListener
            public final void audioOutputChanged(WebRtcAudioOutput webRtcAudioOutput) {
                WebRtcCallView.this.lambda$onFinishInflate$2(webRtcAudioOutput);
            }
        });
        this.videoToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda6
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                WebRtcCallView.this.lambda$onFinishInflate$4(compoundButton, z);
            }
        });
        this.micToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda7
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                WebRtcCallView.this.lambda$onFinishInflate$6(compoundButton, z);
            }
        });
        this.ringToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda8
            @Override // android.widget.CompoundButton.OnCheckedChangeListener
            public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                WebRtcCallView.this.lambda$onFinishInflate$8(compoundButton, z);
            }
        });
        this.cameraDirectionToggle.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda9
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$9(view);
            }
        });
        this.hangup.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda10
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$10(view);
            }
        });
        findViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda11
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$11(view);
            }
        });
        this.answer.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda12
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$12(view);
            }
        });
        this.answerWithoutVideo.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda13
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$13(view);
            }
        });
        this.pictureInPictureGestureHelper = PictureInPictureGestureHelper.applyTo(this.smallLocalRenderFrame);
        this.pictureInPictureExpansionHelper = new PictureInPictureExpansionHelper();
        this.smallLocalRenderFrame.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$14(view);
            }
        });
        View findViewById4 = this.smallLocalRender.findViewById(R.id.call_participant_audio_indicator);
        int pixels = (int) DimensionUnit.DP.toPixels(8.0f);
        ViewUtil.setLeftMargin(findViewById4, pixels);
        ViewUtil.setBottomMargin(findViewById4, pixels);
        this.startCall.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda3
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$15(view);
            }
        });
        ColorMatrix colorMatrix = new ColorMatrix();
        colorMatrix.setSaturation(0.0f);
        this.largeLocalRenderNoVideoAvatar.setAlpha(0.6f);
        this.largeLocalRenderNoVideoAvatar.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        this.errorButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda4
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                WebRtcCallView.this.lambda$onFinishInflate$16(view);
            }
        });
        this.rotatableControls.add(this.hangup);
        this.rotatableControls.add(this.answer);
        this.rotatableControls.add(this.answerWithoutVideo);
        this.rotatableControls.add(this.audioToggle);
        this.rotatableControls.add(this.micToggle);
        this.rotatableControls.add(this.videoToggle);
        this.rotatableControls.add(this.cameraDirectionToggle);
        this.rotatableControls.add(findViewById);
        this.rotatableControls.add(findViewById4);
        this.rotatableControls.add(this.ringToggle);
        ConstraintSet constraintSet = new ConstraintSet();
        this.largeHeaderConstraints = constraintSet;
        constraintSet.clone(getContext(), R.layout.webrtc_call_view_header_large);
        ConstraintSet constraintSet2 = new ConstraintSet();
        this.smallHeaderConstraints = constraintSet2;
        constraintSet2.clone(getContext(), R.layout.webrtc_call_view_header_small);
    }

    public /* synthetic */ void lambda$onFinishInflate$2(WebRtcAudioOutput webRtcAudioOutput) {
        runIfNonNull(this.controlsListener, new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onAudioOutputChanged(WebRtcAudioOutput.this);
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$4(CompoundButton compoundButton, boolean z) {
        runIfNonNull(this.controlsListener, new Consumer(z) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda20
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onVideoChanged(this.f$0);
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$6(CompoundButton compoundButton, boolean z) {
        runIfNonNull(this.controlsListener, new Consumer(z) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda24
            public final /* synthetic */ boolean f$0;

            {
                this.f$0 = r1;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onMicChanged(this.f$0);
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$7(boolean z, ControlsListener controlsListener) {
        controlsListener.onRingGroupChanged(z, this.ringToggle.isActivated());
    }

    public /* synthetic */ void lambda$onFinishInflate$8(CompoundButton compoundButton, boolean z) {
        runIfNonNull(this.controlsListener, new Consumer(z) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda22
            public final /* synthetic */ boolean f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                WebRtcCallView.this.lambda$onFinishInflate$7(this.f$1, (WebRtcCallView.ControlsListener) obj);
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$9(View view) {
        runIfNonNull(this.controlsListener, new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda25
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onCameraDirectionChanged();
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$10(View view) {
        runIfNonNull(this.controlsListener, new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda18
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onEndCallPressed();
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$11(View view) {
        runIfNonNull(this.controlsListener, new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda19
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onDenyCallPressed();
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$12(View view) {
        runIfNonNull(this.controlsListener, new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda16
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onAcceptCallPressed();
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$13(View view) {
        runIfNonNull(this.controlsListener, new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda17
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                ((WebRtcCallView.ControlsListener) obj).onAcceptCallWithVoiceOnlyPressed();
            }
        });
    }

    public /* synthetic */ void lambda$onFinishInflate$14(View view) {
        ControlsListener controlsListener = this.controlsListener;
        if (controlsListener != null) {
            controlsListener.onLocalPictureInPictureClicked();
        }
    }

    public /* synthetic */ void lambda$onFinishInflate$15(View view) {
        if (this.controlsListener != null) {
            this.startCall.setEnabled(false);
            this.controlsListener.onStartCall(this.videoToggle.isChecked());
        }
    }

    public /* synthetic */ void lambda$onFinishInflate$16(View view) {
        ControlsListener controlsListener = this.controlsListener;
        if (controlsListener != null) {
            controlsListener.onCancelStartCall();
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.controls.isFadeOutEnabled()) {
            scheduleFadeOut();
        }
    }

    @Override // android.view.View
    protected boolean fitSystemWindows(Rect rect) {
        this.statusBarGuideline.setGuidelineBegin(rect.top);
        ((Guideline) findViewById(R.id.call_screen_navigation_bar_guideline)).setGuidelineEnd(rect.bottom);
        return true;
    }

    @Override // android.view.View
    public WindowInsets onApplyWindowInsets(WindowInsets windowInsets) {
        if (Build.VERSION.SDK_INT >= 20) {
            this.navBarBottomInset = WindowInsetsCompat.toWindowInsetsCompat(windowInsets).getInsets(WindowInsetsCompat.Type.navigationBars()).bottom;
            CallParticipantsViewState callParticipantsViewState = this.lastState;
            if (callParticipantsViewState != null) {
                updateCallParticipants(callParticipantsViewState);
            }
        }
        return super.onApplyWindowInsets(windowInsets);
    }

    @Override // android.view.View
    public void onWindowSystemUiVisibilityChanged(int i) {
        if ((i & 2) != 0) {
            this.pictureInPictureGestureHelper.clearVerticalBoundaries();
        } else if (this.controls.adjustForFold()) {
            this.pictureInPictureGestureHelper.clearVerticalBoundaries();
            this.pictureInPictureGestureHelper.setTopVerticalBoundary(this.toolbar.getTop());
        } else {
            this.pictureInPictureGestureHelper.setTopVerticalBoundary(this.toolbar.getBottom());
            this.pictureInPictureGestureHelper.setBottomVerticalBoundary(this.videoToggle.getTop());
        }
        this.pictureInPictureGestureHelper.adjustPip();
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        cancelFadeOut();
    }

    public void rotateControls(int i) {
        for (View view : this.rotatableControls) {
            view.animate().rotation((float) i);
        }
    }

    public void setControlsListener(ControlsListener controlsListener) {
        this.controlsListener = controlsListener;
    }

    public void setMicEnabled(boolean z) {
        this.micToggle.setChecked(z, false);
    }

    public void updateCallParticipants(CallParticipantsViewState callParticipantsViewState) {
        this.lastState = callParticipantsViewState;
        CallParticipantsState callParticipantsState = callParticipantsViewState.getCallParticipantsState();
        boolean isPortrait = callParticipantsViewState.isPortrait();
        boolean isLandscapeEnabled = callParticipantsViewState.isLandscapeEnabled();
        ArrayList arrayList = new ArrayList(2);
        if (!callParticipantsState.getGridParticipants().isEmpty()) {
            arrayList.add(WebRtcCallParticipantsPage.forMultipleParticipants(callParticipantsState.getGridParticipants(), callParticipantsState.getFocusedParticipant(), callParticipantsState.isInPipMode(), isPortrait, isLandscapeEnabled, callParticipantsState.isIncomingRing(), this.navBarBottomInset));
        }
        boolean z = true;
        if (callParticipantsState.getFocusedParticipant() != CallParticipant.EMPTY && callParticipantsState.getAllRemoteParticipants().size() > 1) {
            arrayList.add(WebRtcCallParticipantsPage.forSingleParticipant(callParticipantsState.getFocusedParticipant(), callParticipantsState.isInPipMode(), isPortrait, isLandscapeEnabled));
        }
        if (callParticipantsState.getGroupCallState().isNotIdle()) {
            if (callParticipantsState.getCallState() == WebRtcViewModel.State.CALL_PRE_JOIN) {
                this.status.setText(callParticipantsState.getPreJoinGroupDescription(getContext()));
            } else if (callParticipantsState.getCallState() == WebRtcViewModel.State.CALL_CONNECTED && callParticipantsState.isInOutgoingRingingMode()) {
                this.status.setText(callParticipantsState.getOutgoingRingingGroupDescription(getContext()));
            } else if (callParticipantsState.getGroupCallState().isRinging()) {
                this.status.setText(callParticipantsState.getIncomingRingingGroupDescription(getContext()));
            }
        }
        if (callParticipantsState.getGroupCallState().isNotIdle()) {
            boolean isPresent = callParticipantsState.getParticipantCount().isPresent();
            this.foldParticipantCount.setText((String) callParticipantsState.getParticipantCount().mapToObj(new LongFunction() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda26
                @Override // com.annimon.stream.function.LongFunction
                public final Object apply(long j) {
                    return String.valueOf(j);
                }
            }).orElse("—"));
            this.foldParticipantCount.setEnabled(isPresent);
        }
        this.pagerAdapter.submitList(arrayList);
        this.recyclerAdapter.submitList(callParticipantsState.getListParticipants());
        if (isPortrait || !isLandscapeEnabled) {
            z = false;
        }
        updateLocalCallParticipant(callParticipantsState.getLocalRenderState(), callParticipantsState.getLocalParticipant(), callParticipantsState.getFocusedParticipant(), z);
        if (!callParticipantsState.isLargeVideoGroup() || callParticipantsState.isInPipMode() || callParticipantsState.isFolded()) {
            layoutParticipantsForSmallCount();
        } else {
            layoutParticipantsForLargeCount();
        }
    }

    public void updateLocalCallParticipant(WebRtcLocalRenderState webRtcLocalRenderState, CallParticipant callParticipant, CallParticipant callParticipant2, boolean z) {
        TextureViewRenderer textureViewRenderer = this.largeLocalRender;
        CameraState.Direction cameraDirection = callParticipant.getCameraDirection();
        CameraState.Direction direction = CameraState.Direction.FRONT;
        textureViewRenderer.setMirror(cameraDirection == direction);
        CallParticipantView callParticipantView = this.smallLocalRender;
        RendererCommon.ScalingType scalingType = RendererCommon.ScalingType.SCALE_ASPECT_FILL;
        callParticipantView.setScalingType(scalingType);
        this.largeLocalRender.setScalingType(scalingType);
        callParticipant.getVideoSink().getLockableEglBase().performWithValidEglBase(new j$.util.function.Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda27
            @Override // j$.util.function.Consumer
            public final void accept(Object obj) {
                WebRtcCallView.this.lambda$updateLocalCallParticipant$17((EglBase) obj);
            }

            @Override // j$.util.function.Consumer
            public /* synthetic */ j$.util.function.Consumer andThen(j$.util.function.Consumer consumer) {
                return Consumer.CC.$default$andThen(this, consumer);
            }
        });
        this.videoToggle.setChecked(callParticipant.isVideoEnabled(), false);
        this.smallLocalRender.setRenderInPip(true);
        if (webRtcLocalRenderState == WebRtcLocalRenderState.EXPANDED) {
            expandPip(callParticipant, callParticipant2);
            this.smallLocalRender.setCallParticipant(callParticipant2);
        } else if ((webRtcLocalRenderState == WebRtcLocalRenderState.SMALL_RECTANGLE || webRtcLocalRenderState == WebRtcLocalRenderState.GONE) && this.pictureInPictureExpansionHelper.isExpandedOrExpanding()) {
            shrinkPip(callParticipant);
        } else {
            this.smallLocalRender.setCallParticipant(callParticipant);
            this.smallLocalRender.setMirror(callParticipant.getCameraDirection() == direction);
            int i = AnonymousClass6.$SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState[webRtcLocalRenderState.ordinal()];
            if (i == 1) {
                this.largeLocalRender.attachBroadcastVideoSink(null);
                this.largeLocalRenderFrame.setVisibility(8);
                this.smallLocalRenderFrame.setVisibility(8);
            } else if (i == 2) {
                this.smallLocalRenderFrame.setVisibility(0);
                animatePipToLargeRectangle(z);
                this.largeLocalRender.attachBroadcastVideoSink(null);
                this.largeLocalRenderFrame.setVisibility(8);
            } else if (i == 3) {
                this.smallLocalRenderFrame.setVisibility(0);
                animatePipToSmallRectangle();
                this.largeLocalRender.attachBroadcastVideoSink(null);
                this.largeLocalRenderFrame.setVisibility(8);
            } else if (i == 4) {
                this.largeLocalRender.attachBroadcastVideoSink(callParticipant.getVideoSink());
                this.largeLocalRenderFrame.setVisibility(0);
                this.largeLocalRenderNoVideo.setVisibility(8);
                this.largeLocalRenderNoVideoAvatar.setVisibility(8);
                this.smallLocalRenderFrame.setVisibility(8);
            } else if (i == 5) {
                this.largeLocalRender.attachBroadcastVideoSink(null);
                this.largeLocalRenderFrame.setVisibility(0);
                this.largeLocalRenderNoVideo.setVisibility(0);
                this.largeLocalRenderNoVideoAvatar.setVisibility(0);
                GlideApp.with(getContext().getApplicationContext()).load((Object) new ProfileContactPhoto(callParticipant.getRecipient())).transform(new CenterCrop(), new BlurTransformation(getContext(), 0.25f, 25.0f)).diskCacheStrategy(DiskCacheStrategy.ALL).into(this.largeLocalRenderNoVideoAvatar);
                this.smallLocalRenderFrame.setVisibility(8);
            }
        }
    }

    public /* synthetic */ void lambda$updateLocalCallParticipant$17(EglBase eglBase) {
        this.largeLocalRender.init(eglBase);
    }

    public void setRecipient(Recipient recipient) {
        if (recipient.getId() != this.recipientId) {
            this.recipientId = recipient.getId();
            this.largeHeaderAvatar.setRecipient(recipient, false);
            if (recipient.isGroup()) {
                this.foldParticipantCountWrapper.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda15
                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        WebRtcCallView.this.lambda$setRecipient$18(view);
                    }
                });
            }
            this.recipientName.setText(recipient.getDisplayName(getContext()));
        }
    }

    public /* synthetic */ void lambda$setRecipient$18(View view) {
        showParticipantsList();
    }

    public void setStatus(String str) {
        this.status.setText(str);
    }

    public void setStatusFromHangupType(HangupMessage.Type type) {
        int i = AnonymousClass6.$SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[type.ordinal()];
        if (i == 1 || i == 2) {
            this.status.setText(R.string.RedPhone_ending_call);
        } else if (i == 3) {
            this.status.setText(R.string.WebRtcCallActivity__answered_on_a_linked_device);
        } else if (i == 4) {
            this.status.setText(R.string.WebRtcCallActivity__declined_on_a_linked_device);
        } else if (i == 5) {
            this.status.setText(R.string.WebRtcCallActivity__busy_on_a_linked_device);
        } else {
            throw new IllegalStateException("Unknown hangup type: " + type);
        }
    }

    /* renamed from: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$6 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass6 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState;
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState;
        static final /* synthetic */ int[] $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type;

        static {
            int[] iArr = new int[WebRtcViewModel.GroupCallState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState = iArr;
            try {
                iArr[WebRtcViewModel.GroupCallState.DISCONNECTED.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.RECONNECTING.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINING.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTING.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTED_AND_JOINED.ordinal()] = 5;
            } catch (NoSuchFieldError unused5) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[WebRtcViewModel.GroupCallState.CONNECTED.ordinal()] = 6;
            } catch (NoSuchFieldError unused6) {
            }
            int[] iArr2 = new int[HangupMessage.Type.values().length];
            $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type = iArr2;
            try {
                iArr2[HangupMessage.Type.NORMAL.ordinal()] = 1;
            } catch (NoSuchFieldError unused7) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.NEED_PERMISSION.ordinal()] = 2;
            } catch (NoSuchFieldError unused8) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.ACCEPTED.ordinal()] = 3;
            } catch (NoSuchFieldError unused9) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.DECLINED.ordinal()] = 4;
            } catch (NoSuchFieldError unused10) {
            }
            try {
                $SwitchMap$org$whispersystems$signalservice$api$messages$calls$HangupMessage$Type[HangupMessage.Type.BUSY.ordinal()] = 5;
            } catch (NoSuchFieldError unused11) {
            }
            int[] iArr3 = new int[WebRtcLocalRenderState.values().length];
            $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState = iArr3;
            try {
                iArr3[WebRtcLocalRenderState.GONE.ordinal()] = 1;
            } catch (NoSuchFieldError unused12) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState[WebRtcLocalRenderState.SMALL_RECTANGLE.ordinal()] = 2;
            } catch (NoSuchFieldError unused13) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState[WebRtcLocalRenderState.SMALLER_RECTANGLE.ordinal()] = 3;
            } catch (NoSuchFieldError unused14) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState[WebRtcLocalRenderState.LARGE.ordinal()] = 4;
            } catch (NoSuchFieldError unused15) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$components$webrtc$WebRtcLocalRenderState[WebRtcLocalRenderState.LARGE_NO_VIDEO.ordinal()] = 5;
            } catch (NoSuchFieldError unused16) {
            }
        }
    }

    public void setStatusFromGroupCallState(WebRtcViewModel.GroupCallState groupCallState) {
        switch (AnonymousClass6.$SwitchMap$org$thoughtcrime$securesms$events$WebRtcViewModel$GroupCallState[groupCallState.ordinal()]) {
            case 1:
                this.status.setText(R.string.WebRtcCallView__disconnected);
                return;
            case 2:
                this.status.setText(R.string.WebRtcCallView__reconnecting);
                return;
            case 3:
                this.status.setText(R.string.WebRtcCallView__joining);
                return;
            case 4:
            case 5:
            case 6:
                this.status.setText("");
                return;
            default:
                return;
        }
    }

    public void setWebRtcControls(WebRtcControls webRtcControls) {
        HashSet hashSet = new HashSet(this.visibleViewSet);
        this.visibleViewSet.clear();
        boolean z = false;
        if (webRtcControls.adjustForFold()) {
            this.showParticipantsGuideline.setGuidelineBegin(-1);
            this.showParticipantsGuideline.setGuidelineEnd(webRtcControls.getFold());
            this.topFoldGuideline.setGuidelineEnd(webRtcControls.getFold());
            this.callScreenTopFoldGuideline.setGuidelineEnd(webRtcControls.getFold());
        } else {
            this.showParticipantsGuideline.setGuidelineBegin(((ConstraintLayout.LayoutParams) this.statusBarGuideline.getLayoutParams()).guideBegin);
            this.showParticipantsGuideline.setGuidelineEnd(-1);
            this.topFoldGuideline.setGuidelineEnd(0);
            this.callScreenTopFoldGuideline.setGuidelineEnd(0);
        }
        if (webRtcControls.displayGroupMembersButton()) {
            this.visibleViewSet.add(this.foldParticipantCountWrapper);
            this.foldParticipantCount.setClickable(webRtcControls.adjustForFold());
        }
        if (webRtcControls.displayStartCallControls()) {
            this.visibleViewSet.add(this.footerGradient);
            this.visibleViewSet.add(this.startCallControls);
            this.startCall.setText(webRtcControls.getStartCallButtonText());
            this.startCall.setEnabled(webRtcControls.isStartCallEnabled());
        }
        if (webRtcControls.displayErrorControls()) {
            this.visibleViewSet.add(this.footerGradient);
            this.visibleViewSet.add(this.errorButton);
        }
        if (webRtcControls.displayGroupCallFull()) {
            this.groupCallFullStub.get().setVisibility(0);
            ((TextView) this.groupCallFullStub.get().findViewById(R.id.group_call_call_full_message)).setText(webRtcControls.getGroupCallFullMessage(getContext()));
        } else if (this.groupCallFullStub.resolved()) {
            this.groupCallFullStub.get().setVisibility(8);
        }
        if (webRtcControls.displayTopViews()) {
            this.visibleViewSet.addAll(this.topViews);
        }
        if (webRtcControls.displayIncomingCallButtons()) {
            this.visibleViewSet.addAll(this.incomingCallViews);
            this.incomingRingStatus.setText(webRtcControls.displayAnswerWithoutVideo() ? R.string.WebRtcCallView__signal_video_call : R.string.WebRtcCallView__signal_call);
            this.answer.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.webrtc_call_screen_answer));
        }
        if (webRtcControls.displayAnswerWithoutVideo()) {
            this.visibleViewSet.add(this.answerWithoutVideo);
            this.visibleViewSet.add(this.answerWithoutVideoLabel);
            this.answer.setImageDrawable(AppCompatResources.getDrawable(getContext(), R.drawable.webrtc_call_screen_answer_with_video));
        }
        if (!webRtcControls.displayIncomingCallButtons()) {
            this.incomingRingStatus.setVisibility(8);
        }
        if (webRtcControls.displayAudioToggle()) {
            this.visibleViewSet.add(this.audioToggle);
            this.visibleViewSet.add(this.audioToggleLabel);
            this.audioToggle.setControlAvailability(webRtcControls.enableHandsetInAudioToggle(), webRtcControls.enableHeadsetInAudioToggle());
            this.audioToggle.setAudioOutput(webRtcControls.getAudioOutput(), false);
        }
        if (webRtcControls.displayCameraToggle()) {
            this.visibleViewSet.add(this.cameraDirectionToggle);
            this.visibleViewSet.add(this.cameraDirectionToggleLabel);
        }
        if (webRtcControls.displayEndCall()) {
            this.visibleViewSet.add(this.hangup);
            this.visibleViewSet.add(this.hangupLabel);
            this.visibleViewSet.add(this.footerGradient);
        }
        if (webRtcControls.displayMuteAudio()) {
            this.visibleViewSet.add(this.micToggle);
            this.visibleViewSet.add(this.micToggleLabel);
        }
        if (webRtcControls.displayVideoToggle()) {
            this.visibleViewSet.add(this.videoToggle);
            this.visibleViewSet.add(this.videoToggleLabel);
        }
        if (webRtcControls.displaySmallOngoingCallButtons()) {
            updateButtonStateForSmallButtons();
        } else if (webRtcControls.displayLargeOngoingCallButtons()) {
            updateButtonStateForLargeButtons();
        }
        if (webRtcControls.displayRemoteVideoRecycler()) {
            this.callParticipantsRecycler.setVisibility(0);
        } else {
            this.callParticipantsRecycler.setVisibility(8);
        }
        if (webRtcControls.showFullScreenShade()) {
            this.fullScreenShade.setVisibility(0);
            this.visibleViewSet.remove(this.topGradient);
            this.visibleViewSet.remove(this.footerGradient);
        } else {
            this.fullScreenShade.setVisibility(8);
        }
        if (webRtcControls.displayRingToggle()) {
            this.visibleViewSet.add(this.ringToggle);
            this.visibleViewSet.add(this.ringToggleLabel);
        }
        if (!webRtcControls.isFadeOutEnabled()) {
            cancelFadeOut();
            ControlsListener controlsListener = this.controlsListener;
            if (controlsListener != null) {
                controlsListener.showSystemUI();
            }
        } else if (!this.controls.isFadeOutEnabled()) {
            scheduleFadeOut();
        }
        if (webRtcControls.adjustForFold() && webRtcControls.isFadeOutEnabled() && !this.controls.adjustForFold()) {
            scheduleFadeOut();
        }
        if (webRtcControls.adjustForFold() && !this.controls.adjustForFold()) {
            z = true;
        }
        this.controls = webRtcControls;
        if (!webRtcControls.isFadeOutEnabled()) {
            this.controlsVisible = true;
        }
        this.allTimeVisibleViews.addAll(this.visibleViewSet);
        if (!this.visibleViewSet.equals(hashSet) || !this.controls.isFadeOutEnabled() || ((webRtcControls.showSmallHeader() && this.largeHeaderAvatar.getVisibility() == 0) || ((!webRtcControls.showSmallHeader() && this.largeHeaderAvatar.getVisibility() == 8) || z))) {
            ControlsListener controlsListener2 = this.controlsListener;
            if (controlsListener2 != null) {
                controlsListener2.showSystemUI();
            }
            this.throttledDebouncer.publish(new Runnable(webRtcControls) { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda21
                public final /* synthetic */ WebRtcControls f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    WebRtcCallView.this.lambda$setWebRtcControls$19(this.f$1);
                }
            });
        }
        onWindowSystemUiVisibilityChanged(getWindowSystemUiVisibility());
    }

    public /* synthetic */ void lambda$setWebRtcControls$19(WebRtcControls webRtcControls) {
        fadeInNewUiState(webRtcControls.displaySmallOngoingCallButtons(), webRtcControls.showSmallHeader());
    }

    public View getVideoTooltipTarget() {
        return this.videoToggle;
    }

    public void showSpeakerViewHint() {
        this.groupCallSpeakerHint.get().setVisibility(0);
    }

    public void hideSpeakerViewHint() {
        if (this.groupCallSpeakerHint.resolved()) {
            this.groupCallSpeakerHint.get().setVisibility(8);
        }
    }

    private void expandPip(final CallParticipant callParticipant, final CallParticipant callParticipant2) {
        this.pictureInPictureExpansionHelper.expand(this.smallLocalRenderFrame, new PictureInPictureExpansionHelper.Callback() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.2
            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onAnimationWillStart() {
                WebRtcCallView.this.largeLocalRender.attachBroadcastVideoSink(callParticipant.getVideoSink());
            }

            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onPictureInPictureExpanded() {
                WebRtcCallView.this.largeLocalRenderFrame.setVisibility(0);
                WebRtcCallView.this.largeLocalRenderNoVideo.setVisibility(8);
                WebRtcCallView.this.largeLocalRenderNoVideoAvatar.setVisibility(8);
            }

            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onPictureInPictureNotVisible() {
                WebRtcCallView.this.smallLocalRender.setCallParticipant(callParticipant2);
                WebRtcCallView.this.smallLocalRender.setMirror(false);
            }

            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onAnimationHasFinished() {
                WebRtcCallView.this.pictureInPictureGestureHelper.adjustPip();
            }
        });
    }

    private void shrinkPip(final CallParticipant callParticipant) {
        this.pictureInPictureExpansionHelper.shrink(this.smallLocalRenderFrame, new PictureInPictureExpansionHelper.Callback() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.3
            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onAnimationWillStart() {
            }

            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onPictureInPictureExpanded() {
                WebRtcCallView.this.largeLocalRenderFrame.setVisibility(8);
                WebRtcCallView.this.largeLocalRender.attachBroadcastVideoSink(null);
            }

            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onPictureInPictureNotVisible() {
                WebRtcCallView.this.smallLocalRender.setCallParticipant(callParticipant);
                WebRtcCallView.this.smallLocalRender.setMirror(callParticipant.getCameraDirection() == CameraState.Direction.FRONT);
                if (!callParticipant.isVideoEnabled()) {
                    WebRtcCallView.this.smallLocalRenderFrame.setVisibility(8);
                }
            }

            @Override // org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper.Callback
            public void onAnimationHasFinished() {
                WebRtcCallView.this.pictureInPictureGestureHelper.adjustPip();
            }
        });
    }

    private void animatePipToLargeRectangle(boolean z) {
        Point point;
        if (z) {
            point = new Point(ViewUtil.dpToPx(160), ViewUtil.dpToPx(90));
        } else {
            point = new Point(ViewUtil.dpToPx(90), ViewUtil.dpToPx(160));
        }
        AnonymousClass4 r0 = new SimpleAnimationListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.4
            @Override // org.thoughtcrime.securesms.mediasend.SimpleAnimationListener, android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                WebRtcCallView.this.pictureInPictureGestureHelper.enableCorners();
                WebRtcCallView.this.pictureInPictureGestureHelper.adjustPip();
            }
        };
        ViewGroup.LayoutParams layoutParams = this.smallLocalRenderFrame.getLayoutParams();
        if (layoutParams.width == point.x && layoutParams.height == point.y) {
            r0.onAnimationEnd(null);
            return;
        }
        ResizeAnimation resizeAnimation = new ResizeAnimation(this.smallLocalRenderFrame, point.x, point.y);
        resizeAnimation.setDuration(300);
        resizeAnimation.setAnimationListener(r0);
        this.smallLocalRenderFrame.startAnimation(resizeAnimation);
    }

    private void animatePipToSmallRectangle() {
        this.pictureInPictureGestureHelper.lockToBottomEnd();
        this.pictureInPictureGestureHelper.performAfterFling(new Runnable() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView$$ExternalSyntheticLambda23
            @Override // java.lang.Runnable
            public final void run() {
                WebRtcCallView.this.lambda$animatePipToSmallRectangle$20();
            }
        });
    }

    public /* synthetic */ void lambda$animatePipToSmallRectangle$20() {
        ResizeAnimation resizeAnimation = new ResizeAnimation(this.smallLocalRenderFrame, ViewUtil.dpToPx(54), ViewUtil.dpToPx(72));
        resizeAnimation.setDuration(300);
        resizeAnimation.setAnimationListener(new SimpleAnimationListener() { // from class: org.thoughtcrime.securesms.components.webrtc.WebRtcCallView.5
            @Override // org.thoughtcrime.securesms.mediasend.SimpleAnimationListener, android.view.animation.Animation.AnimationListener
            public void onAnimationEnd(Animation animation) {
                WebRtcCallView.this.pictureInPictureGestureHelper.adjustPip();
            }
        });
        this.smallLocalRenderFrame.startAnimation(resizeAnimation);
    }

    public void toggleControls() {
        if (!this.controls.isFadeOutEnabled() || this.toolbar.getVisibility() != 0) {
            fadeInControls();
        } else {
            fadeOutControls();
        }
    }

    private void fadeOutControls() {
        fadeControls(8);
        this.controlsListener.onControlsFadeOut();
    }

    private void fadeInControls() {
        fadeControls(0);
        scheduleFadeOut();
    }

    private void layoutParticipantsForSmallCount() {
        this.pagerBottomMarginDp = 0;
        layoutParticipants();
    }

    private void layoutParticipantsForLargeCount() {
        this.pagerBottomMarginDp = 104;
        layoutParticipants();
    }

    private int withControlsHeight(int i) {
        if (i == 0) {
            return 0;
        }
        return (this.controlsVisible || this.controls.adjustForFold()) ? i + 98 : i;
    }

    private void layoutParticipants() {
        int dpToPx = ViewUtil.dpToPx(withControlsHeight(this.pagerBottomMarginDp));
        if (ViewKt.getMarginBottom(this.callParticipantsPager) != dpToPx) {
            TransitionManager.beginDelayedTransition(this.participantsParent, new AutoTransition().setDuration(TRANSITION_DURATION_MILLIS));
            ConstraintSet constraintSet = new ConstraintSet();
            constraintSet.clone(this.participantsParent);
            constraintSet.setMargin(R.id.call_screen_participants_pager, 4, dpToPx);
            constraintSet.applyTo(this.participantsParent);
        }
    }

    private void fadeControls(int i) {
        this.controlsVisible = i == 0;
        TransitionSet duration = new AutoTransition().setOrdering(0).setDuration(TRANSITION_DURATION_MILLIS);
        TransitionManager.endTransitions(this.parent);
        ControlsListener controlsListener = this.controlsListener;
        if (controlsListener != null) {
            if (this.controlsVisible) {
                controlsListener.showSystemUI();
            } else {
                controlsListener.hideSystemUI();
            }
        }
        TransitionManager.beginDelayedTransition(this.parent, duration);
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this.parent);
        for (View view : controlsToFade()) {
            constraintSet.setVisibility(view.getId(), i);
        }
        adjustParticipantsRecycler(constraintSet);
        constraintSet.applyTo(this.parent);
        layoutParticipants();
    }

    private Set<View> controlsToFade() {
        if (this.controls.adjustForFold()) {
            return Sets.intersection(this.topViews, this.visibleViewSet);
        }
        return this.visibleViewSet;
    }

    private void fadeInNewUiState(boolean z, boolean z2) {
        int i;
        TransitionManager.beginDelayedTransition(this.parent, new AutoTransition().setDuration(TRANSITION_DURATION_MILLIS));
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this.parent);
        Iterator it = SetUtil.difference(this.allTimeVisibleViews, this.visibleViewSet).iterator();
        while (true) {
            i = 8;
            if (!it.hasNext()) {
                break;
            }
            constraintSet.setVisibility(((View) it.next()).getId(), 8);
        }
        for (View view : this.visibleViewSet) {
            constraintSet.setVisibility(view.getId(), 0);
            if (this.adjustableMarginsSet.contains(view)) {
                constraintSet.setMargin(view.getId(), 7, ViewUtil.dpToPx(z ? 8 : 16));
            }
        }
        adjustParticipantsRecycler(constraintSet);
        constraintSet.applyTo(this.parent);
        if (z2) {
            ConstraintSet constraintSet2 = this.smallHeaderConstraints;
            int id = this.incomingRingStatus.getId();
            if (this.visibleViewSet.contains(this.incomingRingStatus)) {
                i = 0;
            }
            constraintSet2.setVisibility(id, i);
            this.smallHeaderConstraints.applyTo(this.toolbar);
            return;
        }
        ConstraintSet constraintSet3 = this.largeHeaderConstraints;
        int id2 = this.incomingRingStatus.getId();
        if (this.visibleViewSet.contains(this.incomingRingStatus)) {
            i = 0;
        }
        constraintSet3.setVisibility(id2, i);
        this.largeHeaderConstraints.applyTo(this.toolbar);
    }

    private void adjustParticipantsRecycler(ConstraintSet constraintSet) {
        if (this.controlsVisible || this.controls.adjustForFold()) {
            constraintSet.connect(R.id.call_screen_participants_recycler, 4, R.id.call_screen_video_toggle, 3);
        } else {
            constraintSet.connect(R.id.call_screen_participants_recycler, 4, 0, 4);
        }
        constraintSet.setHorizontalBias(R.id.call_screen_participants_recycler, this.controls.adjustForFold() ? 0.5f : 1.0f);
    }

    private void scheduleFadeOut() {
        cancelFadeOut();
        if (getHandler() != null) {
            getHandler().postDelayed(this.fadeOutRunnable, 5000);
        }
    }

    private void cancelFadeOut() {
        if (getHandler() != null) {
            getHandler().removeCallbacks(this.fadeOutRunnable);
        }
    }

    public static <T> void runIfNonNull(T t, androidx.core.util.Consumer<T> consumer) {
        if (t != null) {
            consumer.accept(t);
        }
    }

    private void updateButtonStateForLargeButtons() {
        this.cameraDirectionToggle.setImageResource(R.drawable.webrtc_call_screen_camera_toggle);
        this.hangup.setImageResource(R.drawable.webrtc_call_screen_hangup);
        this.micToggle.setBackgroundResource(R.drawable.webrtc_call_screen_mic_toggle);
        this.videoToggle.setBackgroundResource(R.drawable.webrtc_call_screen_video_toggle);
        this.audioToggle.setImageResource(R.drawable.webrtc_call_screen_speaker_toggle);
        this.ringToggle.setBackgroundResource(R.drawable.webrtc_call_screen_ring_toggle);
    }

    private void updateButtonStateForSmallButtons() {
        this.cameraDirectionToggle.setImageResource(R.drawable.webrtc_call_screen_camera_toggle_small);
        this.hangup.setImageResource(R.drawable.webrtc_call_screen_hangup_small);
        this.micToggle.setBackgroundResource(R.drawable.webrtc_call_screen_mic_toggle_small);
        this.videoToggle.setBackgroundResource(R.drawable.webrtc_call_screen_video_toggle_small);
        this.audioToggle.setImageResource(R.drawable.webrtc_call_screen_speaker_toggle_small);
        this.ringToggle.setBackgroundResource(R.drawable.webrtc_call_screen_ring_toggle_small);
    }

    private boolean showParticipantsList() {
        this.controlsListener.onShowParticipantsList();
        return true;
    }

    public void switchToSpeakerView() {
        if (this.pagerAdapter.getItemCount() > 0) {
            this.callParticipantsPager.setCurrentItem(this.pagerAdapter.getItemCount() - 1, false);
        }
    }

    public void setRingGroup(boolean z) {
        this.ringToggle.setChecked(z, false);
    }

    public void enableRingGroup(boolean z) {
        this.ringToggle.setActivated(z);
    }
}
