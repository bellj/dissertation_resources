package org.thoughtcrime.securesms.components.segmentedprogressbar;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.TypedValue;
import java.util.ArrayList;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.segmentedprogressbar.Segment;

/* compiled from: Utils.kt */
@Metadata(d1 = {"\u0000,\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a2\u0010\u0000\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u0002\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00040\u00020\u0001*\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t\u001a\u0012\u0010\n\u001a\u00020\t*\u00020\u000b2\u0006\u0010\f\u001a\u00020\t¨\u0006\r"}, d2 = {"getDrawingComponents", "Lkotlin/Pair;", "", "Landroid/graphics/RectF;", "Landroid/graphics/Paint;", "Lorg/thoughtcrime/securesms/components/segmentedprogressbar/SegmentedProgressBar;", "segment", "Lorg/thoughtcrime/securesms/components/segmentedprogressbar/Segment;", "segmentIndex", "", "getThemeColor", "Landroid/content/Context;", "attributeColor", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class UtilsKt {
    public static final int getThemeColor(Context context, int i) {
        Intrinsics.checkNotNullParameter(context, "<this>");
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(i, typedValue, true);
        return typedValue.data;
    }

    public static final Pair<List<RectF>, List<Paint>> getDrawingComponents(SegmentedProgressBar segmentedProgressBar, Segment segment, int i) {
        Intrinsics.checkNotNullParameter(segmentedProgressBar, "<this>");
        Intrinsics.checkNotNullParameter(segment, "segment");
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        float segmentWidth = segmentedProgressBar.getSegmentWidth();
        float margin = (((float) i) * segmentWidth) + ((float) (i * segmentedProgressBar.getMargin()));
        float f = margin + segmentWidth;
        float segmentStrokeWidth = !segmentedProgressBar.getStrokeApplicable() ? 0.0f : (float) segmentedProgressBar.getSegmentStrokeWidth();
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(segmentedProgressBar.getSegmentBackgroundColor());
        Paint paint2 = new Paint();
        paint2.setStyle(Paint.Style.FILL);
        paint2.setColor(segmentedProgressBar.getSegmentSelectedBackgroundColor());
        Paint paint3 = new Paint();
        paint3.setColor(segment.getAnimationState() == Segment.AnimationState.IDLE ? segmentedProgressBar.getSegmentStrokeColor() : segmentedProgressBar.getSegmentSelectedStrokeColor());
        paint3.setStyle(Paint.Style.STROKE);
        paint3.setStrokeWidth(segmentStrokeWidth);
        if (segment.getAnimationState() == Segment.AnimationState.ANIMATED) {
            arrayList.add(new RectF(margin + segmentStrokeWidth, ((float) segmentedProgressBar.getHeight()) - segmentStrokeWidth, f - segmentStrokeWidth, segmentStrokeWidth));
            arrayList2.add(paint2);
        } else {
            arrayList.add(new RectF(margin + segmentStrokeWidth, ((float) segmentedProgressBar.getHeight()) - segmentStrokeWidth, f - segmentStrokeWidth, segmentStrokeWidth));
            arrayList2.add(paint);
        }
        if (segment.getAnimationState() == Segment.AnimationState.ANIMATING) {
            arrayList.add(new RectF(margin + segmentStrokeWidth, ((float) segmentedProgressBar.getHeight()) - segmentStrokeWidth, (segment.getAnimationProgressPercentage() * segmentWidth) + margin, segmentStrokeWidth));
            arrayList2.add(paint2);
        }
        if (segmentStrokeWidth > 0.0f) {
            arrayList.add(new RectF(margin + segmentStrokeWidth, ((float) segmentedProgressBar.getHeight()) - segmentStrokeWidth, f - segmentStrokeWidth, segmentStrokeWidth));
            arrayList2.add(paint3);
        }
        return new Pair<>(arrayList, arrayList2);
    }
}
