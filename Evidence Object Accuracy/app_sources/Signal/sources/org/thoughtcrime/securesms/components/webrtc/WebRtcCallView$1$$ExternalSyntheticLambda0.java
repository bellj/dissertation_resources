package org.thoughtcrime.securesms.components.webrtc;

import androidx.core.util.Consumer;
import org.thoughtcrime.securesms.components.webrtc.WebRtcCallView;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class WebRtcCallView$1$$ExternalSyntheticLambda0 implements Consumer {
    public final /* synthetic */ int f$0;

    public /* synthetic */ WebRtcCallView$1$$ExternalSyntheticLambda0(int i) {
        this.f$0 = i;
    }

    @Override // androidx.core.util.Consumer
    public final void accept(Object obj) {
        WebRtcCallView.AnonymousClass1.lambda$onPageSelected$0(this.f$0, (WebRtcCallView.ControlsListener) obj);
    }
}
