package org.thoughtcrime.securesms.components.identity;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.IdentityRecord;

/* loaded from: classes4.dex */
public class UnverifiedBannerView extends LinearLayout {
    private static final String TAG = Log.tag(UnverifiedBannerView.class);
    private ImageView closeButton;
    private View container;
    private TextView text;

    /* loaded from: classes4.dex */
    public interface ClickListener {
        void onClicked(List<IdentityRecord> list);
    }

    /* loaded from: classes4.dex */
    public interface DismissListener {
        void onDismissed(List<IdentityRecord> list);
    }

    public UnverifiedBannerView(Context context) {
        super(context);
        initialize();
    }

    public UnverifiedBannerView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public UnverifiedBannerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    public UnverifiedBannerView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        initialize();
    }

    private void initialize() {
        LayoutInflater.from(getContext()).inflate(R.layout.unverified_banner_view, (ViewGroup) this, true);
        this.container = findViewById(R.id.container);
        this.text = (TextView) findViewById(R.id.unverified_text);
        this.closeButton = (ImageView) findViewById(R.id.cancel);
    }

    public void display(String str, final List<IdentityRecord> list, final ClickListener clickListener, final DismissListener dismissListener) {
        this.text.setText(str);
        setVisibility(0);
        this.container.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.identity.UnverifiedBannerView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                Log.i(UnverifiedBannerView.TAG, "onClick()");
                clickListener.onClicked(list);
            }
        });
        this.closeButton.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.identity.UnverifiedBannerView.2
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                UnverifiedBannerView.this.hide();
                dismissListener.onDismissed(list);
            }
        });
    }

    public void hide() {
        setVisibility(8);
    }
}
