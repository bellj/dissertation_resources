package org.thoughtcrime.securesms.components.settings.conversation;

import j$.util.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda15 implements Function {
    public final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel f$0;

    public /* synthetic */ ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda15(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel) {
        this.f$0 = groupSettingsViewModel;
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function andThen(Function function) {
        return Function.CC.$default$andThen(this, function);
    }

    @Override // j$.util.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.GroupSettingsViewModel.m1130_init_$lambda10(this.f$0, (Recipient) obj);
    }

    @Override // j$.util.function.Function
    public /* synthetic */ Function compose(Function function) {
        return Function.CC.$default$compose(this, function);
    }
}
