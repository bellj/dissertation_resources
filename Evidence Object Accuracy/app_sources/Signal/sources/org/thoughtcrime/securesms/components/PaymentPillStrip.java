package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class PaymentPillStrip extends ConstraintLayout {
    private FrameLayout buttonEnd;
    private FrameLayout buttonStart;

    public PaymentPillStrip(Context context) {
        super(context);
    }

    public PaymentPillStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PaymentPillStrip(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public PaymentPillStrip(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }

    @Override // android.view.View
    protected void onFinishInflate() {
        super.onFinishInflate();
        this.buttonStart = (FrameLayout) findViewById(R.id.button_start_frame);
        this.buttonEnd = (FrameLayout) findViewById(R.id.button_end_frame);
    }

    @Override // androidx.constraintlayout.widget.ConstraintLayout, android.view.View
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
        if (this.buttonStart.getMeasuredWidth() > this.buttonEnd.getMinimumWidth()) {
            this.buttonEnd.setMinimumWidth(this.buttonStart.getMeasuredWidth());
        }
        if (this.buttonEnd.getMeasuredWidth() > this.buttonStart.getMinimumWidth()) {
            this.buttonStart.setMinimumWidth(this.buttonEnd.getMeasuredWidth());
        }
        if (this.buttonStart.getMeasuredHeight() > this.buttonEnd.getMinimumHeight()) {
            this.buttonEnd.setMinimumHeight(this.buttonStart.getMeasuredHeight());
        }
        if (this.buttonEnd.getMeasuredHeight() > this.buttonStart.getMinimumHeight()) {
            this.buttonStart.setMinimumHeight(this.buttonEnd.getMeasuredHeight());
        }
        super.onMeasure(i, i2);
    }
}
