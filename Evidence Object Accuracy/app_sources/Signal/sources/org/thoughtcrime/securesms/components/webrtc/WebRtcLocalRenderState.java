package org.thoughtcrime.securesms.components.webrtc;

/* loaded from: classes4.dex */
public enum WebRtcLocalRenderState {
    GONE,
    SMALL_RECTANGLE,
    SMALLER_RECTANGLE,
    LARGE,
    LARGE_NO_VIDEO,
    EXPANDED
}
