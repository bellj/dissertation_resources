package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatImageButton;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.MediaKeyboard;
import org.thoughtcrime.securesms.keyboard.KeyboardPage;
import org.thoughtcrime.securesms.util.ContextUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class EmojiToggle extends AppCompatImageButton implements MediaKeyboard.MediaKeyboardListener {
    private Drawable emojiToggle;
    private Drawable gifToggle;
    private Drawable imeToggle;
    private Drawable mediaToggle;
    private Drawable stickerToggle;

    public EmojiToggle(Context context) {
        super(context);
        initialize();
    }

    public EmojiToggle(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    public EmojiToggle(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize();
    }

    public void setToMedia() {
        setImageDrawable(this.mediaToggle);
    }

    public void setToIme() {
        setImageDrawable(this.imeToggle);
    }

    private void initialize() {
        this.emojiToggle = ContextUtil.requireDrawable(getContext(), R.drawable.ic_emoji);
        this.stickerToggle = ContextUtil.requireDrawable(getContext(), R.drawable.ic_sticker_24);
        this.gifToggle = ContextUtil.requireDrawable(getContext(), R.drawable.ic_gif_24);
        this.imeToggle = ContextUtil.requireDrawable(getContext(), R.drawable.ic_keyboard_24);
        this.mediaToggle = this.emojiToggle;
        setToMedia();
    }

    public void attach(MediaKeyboard mediaKeyboard) {
        mediaKeyboard.setKeyboardListener(this);
    }

    /* renamed from: org.thoughtcrime.securesms.components.emoji.EmojiToggle$1 */
    /* loaded from: classes4.dex */
    public static /* synthetic */ class AnonymousClass1 {
        static final /* synthetic */ int[] $SwitchMap$org$thoughtcrime$securesms$keyboard$KeyboardPage;

        static {
            int[] iArr = new int[KeyboardPage.values().length];
            $SwitchMap$org$thoughtcrime$securesms$keyboard$KeyboardPage = iArr;
            try {
                iArr[KeyboardPage.EMOJI.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyboard$KeyboardPage[KeyboardPage.STICKER.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                $SwitchMap$org$thoughtcrime$securesms$keyboard$KeyboardPage[KeyboardPage.GIF.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
        }
    }

    public void setStickerMode(KeyboardPage keyboardPage) {
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyboard$KeyboardPage[keyboardPage.ordinal()];
        if (i == 1) {
            this.mediaToggle = this.emojiToggle;
        } else if (i == 2) {
            this.mediaToggle = this.stickerToggle;
        } else if (i == 3) {
            this.mediaToggle = this.gifToggle;
        }
        if (getDrawable() != this.imeToggle) {
            setToMedia();
        }
    }

    public boolean isStickerMode() {
        return this.mediaToggle == this.stickerToggle;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onShown() {
        setToIme();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onHidden() {
        setToMedia();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.MediaKeyboard.MediaKeyboardListener
    public void onKeyboardChanged(KeyboardPage keyboardPage) {
        setStickerMode(keyboardPage);
        int i = AnonymousClass1.$SwitchMap$org$thoughtcrime$securesms$keyboard$KeyboardPage[keyboardPage.ordinal()];
        if (i == 1) {
            TextSecurePreferences.setMediaKeyboardMode(getContext(), TextSecurePreferences.MediaKeyboardMode.EMOJI);
        } else if (i == 2) {
            TextSecurePreferences.setMediaKeyboardMode(getContext(), TextSecurePreferences.MediaKeyboardMode.STICKER);
        } else if (i == 3) {
            TextSecurePreferences.setMediaKeyboardMode(getContext(), TextSecurePreferences.MediaKeyboardMode.GIF);
        }
    }
}
