package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.shape.ShapeAppearanceModel;
import j$.util.Collection$EL;
import j$.util.function.Predicate;
import java.io.IOException;
import java.util.List;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.components.emoji.EmojiImageView;
import org.thoughtcrime.securesms.components.mention.MentionAnnotation;
import org.thoughtcrime.securesms.components.quotes.QuoteViewColorTheme;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.QuoteModel;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideDeck;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientForeverObserver;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class QuoteView extends FrameLayout implements RecipientForeverObserver {
    private static final String TAG = Log.tag(QuoteView.class);
    private ViewGroup attachmentContainerView;
    private TextView attachmentNameView;
    private View attachmentVideoOverlayView;
    private SlideDeck attachments;
    private LiveRecipient author;
    private TextView authorView;
    private View background;
    private CharSequence body;
    private TextView bodyView;
    private CornerMask cornerMask;
    private ImageView dismissView;
    private ViewGroup footerView;
    private long id;
    private boolean isWallpaperEnabled;
    private int largeCornerRadius;
    private ViewGroup mainView;
    private TextView mediaDescriptionText;
    private MessageType messageType;
    private TextView missingLinkText;
    private EmojiImageView missingStoryReaction;
    private View quoteBarView;
    private QuoteModel.Type quoteType;
    private int smallCornerRadius;
    private EmojiImageView storyReactionEmoji;
    private int thumbHeight;
    private int thumbWidth;
    private ShapeableImageView thumbnailView;

    /* loaded from: classes4.dex */
    public enum MessageType {
        PREVIEW(0),
        OUTGOING(1),
        INCOMING(2),
        STORY_REPLY_OUTGOING(3),
        STORY_REPLY_INCOMING(4),
        STORY_REPLY_PREVIEW(5);
        
        private final int code;

        MessageType(int i) {
            this.code = i;
        }

        public static MessageType fromCode(int i) {
            MessageType[] values = values();
            for (MessageType messageType : values) {
                if (messageType.code == i) {
                    return messageType;
                }
            }
            throw new IllegalArgumentException("Unsupported code " + i);
        }
    }

    public QuoteView(Context context) {
        super(context);
        initialize(null);
    }

    public QuoteView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(attributeSet);
    }

    public QuoteView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(attributeSet);
    }

    public QuoteView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        initialize(attributeSet);
    }

    private void initialize(AttributeSet attributeSet) {
        FrameLayout.inflate(getContext(), R.layout.quote_view, this);
        this.background = findViewById(R.id.quote_background);
        this.mainView = (ViewGroup) findViewById(R.id.quote_main);
        this.footerView = (ViewGroup) findViewById(R.id.quote_missing_footer);
        this.authorView = (TextView) findViewById(R.id.quote_author);
        this.bodyView = (TextView) findViewById(R.id.quote_text);
        this.quoteBarView = findViewById(R.id.quote_bar);
        this.thumbnailView = (ShapeableImageView) findViewById(R.id.quote_thumbnail);
        this.attachmentVideoOverlayView = findViewById(R.id.quote_video_overlay);
        this.attachmentContainerView = (ViewGroup) findViewById(R.id.quote_attachment_container);
        this.attachmentNameView = (TextView) findViewById(R.id.quote_attachment_name);
        this.dismissView = (ImageView) findViewById(R.id.quote_dismiss);
        this.mediaDescriptionText = (TextView) findViewById(R.id.media_type);
        this.missingLinkText = (TextView) findViewById(R.id.quote_missing_text);
        this.missingStoryReaction = (EmojiImageView) findViewById(R.id.quote_missing_story_reaction_emoji);
        this.storyReactionEmoji = (EmojiImageView) findViewById(R.id.quote_story_reaction_emoji);
        this.largeCornerRadius = getResources().getDimensionPixelSize(R.dimen.quote_corner_radius_large);
        this.smallCornerRadius = getResources().getDimensionPixelSize(R.dimen.quote_corner_radius_bottom);
        this.cornerMask = new CornerMask(this);
        if (attributeSet != null) {
            int i = 0;
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.QuoteView, 0, 0);
            this.messageType = MessageType.fromCode(obtainStyledAttributes.getInt(0, 0));
            obtainStyledAttributes.recycle();
            ImageView imageView = this.dismissView;
            if (this.messageType != MessageType.PREVIEW) {
                i = 8;
            }
            imageView.setVisibility(i);
        }
        setMessageType(this.messageType);
        this.dismissView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.QuoteView$$ExternalSyntheticLambda7
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                QuoteView.this.lambda$initialize$0(view);
            }
        });
    }

    public /* synthetic */ void lambda$initialize$0(View view) {
        setVisibility(8);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        this.cornerMask.mask(canvas);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LiveRecipient liveRecipient = this.author;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this);
        }
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
        CornerMask cornerMask = this.cornerMask;
        int i = this.largeCornerRadius;
        int i2 = this.smallCornerRadius;
        cornerMask.setRadii(i, i, i2, i2);
        int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.quote_thumb_size);
        this.thumbHeight = dimensionPixelSize;
        this.thumbWidth = dimensionPixelSize;
        if (messageType == MessageType.PREVIEW) {
            int dimensionPixelOffset = getResources().getDimensionPixelOffset(R.dimen.quote_corner_radius_preview);
            this.cornerMask.setTopLeftRadius(dimensionPixelOffset);
            this.cornerMask.setTopRightRadius(dimensionPixelOffset);
        } else if (isStoryReply()) {
            this.thumbWidth = getResources().getDimensionPixelOffset(R.dimen.quote_story_thumb_width);
            this.thumbHeight = getResources().getDimensionPixelOffset(R.dimen.quote_story_thumb_height);
        }
        ViewGroup.LayoutParams layoutParams = this.thumbnailView.getLayoutParams();
        layoutParams.height = this.thumbHeight;
        layoutParams.width = this.thumbWidth;
        this.thumbnailView.setLayoutParams(layoutParams);
    }

    public void setQuote(GlideRequests glideRequests, long j, Recipient recipient, CharSequence charSequence, boolean z, SlideDeck slideDeck, String str, QuoteModel.Type type) {
        LiveRecipient liveRecipient = this.author;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this);
        }
        this.id = j;
        LiveRecipient live = recipient.live();
        this.author = live;
        this.body = charSequence;
        this.attachments = slideDeck;
        this.quoteType = type;
        live.observeForever(this);
        setQuoteAuthor(recipient);
        setQuoteText(resolveBody(charSequence, type), slideDeck, z, str);
        setQuoteAttachment(glideRequests, charSequence, slideDeck, z);
        setQuoteMissingFooter(z);
        applyColorTheme();
    }

    private CharSequence resolveBody(CharSequence charSequence, QuoteModel.Type type) {
        return type == QuoteModel.Type.GIFT_BADGE ? getContext().getString(R.string.QuoteView__gift) : charSequence;
    }

    public void setTopCornerSizes(boolean z, boolean z2) {
        this.cornerMask.setTopLeftRadius(z ? this.largeCornerRadius : this.smallCornerRadius);
        this.cornerMask.setTopRightRadius(z2 ? this.largeCornerRadius : this.smallCornerRadius);
    }

    public void dismiss() {
        LiveRecipient liveRecipient = this.author;
        if (liveRecipient != null) {
            liveRecipient.lambda$asObservable$6(this);
        }
        this.id = 0;
        this.author = null;
        this.body = null;
        setVisibility(8);
    }

    public void setWallpaperEnabled(boolean z) {
        this.isWallpaperEnabled = z;
        applyColorTheme();
    }

    @Override // org.thoughtcrime.securesms.recipients.RecipientForeverObserver
    public void onRecipientChanged(Recipient recipient) {
        setQuoteAuthor(recipient);
    }

    public Projection getProjection(ViewGroup viewGroup) {
        return Projection.relativeToParent(viewGroup, this, getCorners());
    }

    public Projection.Corners getCorners() {
        return new Projection.Corners(this.cornerMask.getRadii());
    }

    private void setQuoteAuthor(Recipient recipient) {
        String str;
        String str2;
        if (isStoryReply()) {
            TextView textView = this.authorView;
            if (recipient.isSelf()) {
                str2 = getContext().getString(R.string.QuoteView_your_story);
            } else {
                str2 = getContext().getString(R.string.QuoteView_s_story, recipient.getDisplayName(getContext()));
            }
            textView.setText(str2);
            return;
        }
        TextView textView2 = this.authorView;
        if (recipient.isSelf()) {
            str = getContext().getString(R.string.QuoteView_you);
        } else {
            str = recipient.getDisplayName(getContext());
        }
        textView2.setText(str);
    }

    private boolean isStoryReply() {
        MessageType messageType = this.messageType;
        return messageType == MessageType.STORY_REPLY_OUTGOING || messageType == MessageType.STORY_REPLY_INCOMING || messageType == MessageType.STORY_REPLY_PREVIEW;
    }

    private void setQuoteText(CharSequence charSequence, SlideDeck slideDeck, boolean z, String str) {
        if (!z || !isStoryReply()) {
            if (str != null) {
                this.storyReactionEmoji.setImageEmoji(str);
                this.storyReactionEmoji.setVisibility(0);
                this.missingStoryReaction.setVisibility(4);
            } else {
                this.storyReactionEmoji.setVisibility(8);
                this.missingStoryReaction.setVisibility(8);
            }
            boolean z2 = !slideDeck.containsMediaSlide() && isStoryReply();
            if (!TextUtils.isEmpty(charSequence) || !slideDeck.containsMediaSlide()) {
                if (!z2 || charSequence == null) {
                    TextView textView = this.bodyView;
                    if (charSequence == null) {
                        charSequence = "";
                    }
                    textView.setText(charSequence);
                } else {
                    try {
                        this.bodyView.setText(getStoryTextPost(charSequence).getText());
                    } catch (Exception e) {
                        Log.w(TAG, "Could not parse body of text post.", e);
                        this.bodyView.setText("");
                    }
                }
                this.bodyView.setVisibility(0);
                this.mediaDescriptionText.setVisibility(8);
                return;
            }
            this.bodyView.setVisibility(8);
            this.mediaDescriptionText.setVisibility(0);
            Slide slide = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.QuoteView$$ExternalSyntheticLambda0
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ((Slide) obj).hasAudio();
                }
            }).findFirst().orElse(null);
            Slide slide2 = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new QuoteView$$ExternalSyntheticLambda1()).findFirst().orElse(null);
            Slide slide3 = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.QuoteView$$ExternalSyntheticLambda2
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ((Slide) obj).hasImage();
                }
            }).findFirst().orElse(null);
            Slide slide4 = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.QuoteView$$ExternalSyntheticLambda3
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ((Slide) obj).hasVideo();
                }
            }).findFirst().orElse(null);
            Slide slide5 = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.QuoteView$$ExternalSyntheticLambda4
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return ((Slide) obj).hasSticker();
                }
            }).findFirst().orElse(null);
            if (((Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new QuoteView$$ExternalSyntheticLambda5()).findFirst().orElse(null)) != null) {
                this.mediaDescriptionText.setText(R.string.QuoteView_view_once_media);
            } else if (slide != null) {
                this.mediaDescriptionText.setText(R.string.QuoteView_audio);
            } else if (slide2 != null) {
                this.mediaDescriptionText.setVisibility(8);
            } else if (slide4 != null) {
                if (slide4.isVideoGif()) {
                    this.mediaDescriptionText.setText(R.string.QuoteView_gif);
                } else {
                    this.mediaDescriptionText.setText(R.string.QuoteView_video);
                }
            } else if (slide5 != null) {
                this.mediaDescriptionText.setText(R.string.QuoteView_sticker);
            } else if (slide3 == null) {
            } else {
                if (MediaUtil.isGif(slide3.getContentType())) {
                    this.mediaDescriptionText.setText(R.string.QuoteView_gif);
                } else {
                    this.mediaDescriptionText.setText(R.string.QuoteView_photo);
                }
            }
        } else {
            this.bodyView.setVisibility(8);
            this.storyReactionEmoji.setVisibility(8);
            this.mediaDescriptionText.setVisibility(0);
            this.mediaDescriptionText.setText(R.string.QuoteView_no_longer_available);
            if (str != null) {
                this.missingStoryReaction.setVisibility(0);
                this.missingStoryReaction.setImageEmoji(str);
                return;
            }
            this.missingStoryReaction.setVisibility(8);
        }
    }

    private void setQuoteAttachment(GlideRequests glideRequests, CharSequence charSequence, SlideDeck slideDeck, boolean z) {
        MessageType messageType = this.messageType;
        boolean z2 = true;
        boolean z3 = (messageType == MessageType.INCOMING || messageType == MessageType.STORY_REPLY_INCOMING) ? false : true;
        if (!(messageType == MessageType.PREVIEW || messageType == MessageType.STORY_REPLY_PREVIEW)) {
            z2 = false;
        }
        this.mainView.setMinimumHeight((!isStoryReply() || !z) ? this.thumbHeight : 0);
        this.thumbnailView.setPadding(0, 0, 0, 0);
        if (!this.attachments.containsMediaSlide() && isStoryReply()) {
            StoryTextPostModel storyTextPost = getStoryTextPost(charSequence);
            this.attachmentVideoOverlayView.setVisibility(8);
            this.attachmentContainerView.setVisibility(8);
            this.thumbnailView.setVisibility(0);
            glideRequests.load((Object) storyTextPost).centerCrop().override(this.thumbWidth, this.thumbHeight).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(this.thumbnailView);
        } else if (this.quoteType == QuoteModel.Type.GIFT_BADGE) {
            if (z3 && !z2) {
                int pixels = (int) DimensionUnit.DP.toPixels(1.0f);
                this.thumbnailView.setPadding(pixels, pixels, pixels, pixels);
                this.thumbnailView.setShapeAppearanceModel(buildShapeAppearanceForLayoutDirection());
            }
            this.attachmentVideoOverlayView.setVisibility(8);
            this.attachmentContainerView.setVisibility(8);
            this.thumbnailView.setVisibility(0);
            glideRequests.load(Integer.valueOf((int) R.drawable.ic_gift_thumbnail)).centerCrop().override(this.thumbWidth, this.thumbHeight).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(this.thumbnailView);
        } else {
            Slide slide = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.QuoteView$$ExternalSyntheticLambda6
                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate and(Predicate predicate) {
                    return Predicate.CC.$default$and(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate negate() {
                    return Predicate.CC.$default$negate(this);
                }

                @Override // j$.util.function.Predicate
                public /* synthetic */ Predicate or(Predicate predicate) {
                    return Predicate.CC.$default$or(this, predicate);
                }

                @Override // j$.util.function.Predicate
                public final boolean test(Object obj) {
                    return QuoteView.lambda$setQuoteAttachment$1((Slide) obj);
                }
            }).findFirst().orElse(null);
            Slide slide2 = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new QuoteView$$ExternalSyntheticLambda1()).findFirst().orElse(null);
            Slide slide3 = (Slide) Collection$EL.stream(slideDeck.getSlides()).filter(new QuoteView$$ExternalSyntheticLambda5()).findFirst().orElse(null);
            this.attachmentVideoOverlayView.setVisibility(8);
            if (slide3 != null) {
                this.thumbnailView.setVisibility(8);
                this.attachmentContainerView.setVisibility(8);
            } else if (slide != null && slide.getUri() != null) {
                this.thumbnailView.setVisibility(0);
                this.attachmentContainerView.setVisibility(8);
                this.dismissView.setBackgroundResource(R.drawable.dismiss_background);
                if (slide.hasVideo() && !slide.isVideoGif()) {
                    this.attachmentVideoOverlayView.setVisibility(0);
                }
                glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(slide.getUri())).centerCrop().override(this.thumbWidth, this.thumbHeight).diskCacheStrategy(DiskCacheStrategy.RESOURCE).into(this.thumbnailView);
            } else if (slide2 != null) {
                this.thumbnailView.setVisibility(8);
                this.attachmentContainerView.setVisibility(0);
                this.attachmentNameView.setText(slide2.getFileName().orElse(""));
            } else {
                this.thumbnailView.setVisibility(8);
                this.attachmentContainerView.setVisibility(8);
                this.dismissView.setBackgroundDrawable(null);
            }
        }
    }

    public static /* synthetic */ boolean lambda$setQuoteAttachment$1(Slide slide) {
        return slide.hasImage() || slide.hasVideo() || slide.hasSticker();
    }

    private void setQuoteMissingFooter(boolean z) {
        this.footerView.setVisibility((!z || isStoryReply()) ? 8 : 0);
    }

    private StoryTextPostModel getStoryTextPost(CharSequence charSequence) {
        if (Util.isEmpty(charSequence)) {
            return null;
        }
        try {
            return StoryTextPostModel.parseFrom(charSequence.toString(), this.id, this.author.getId());
        } catch (IOException unused) {
            return null;
        }
    }

    public void setTextSize(int i, float f) {
        this.bodyView.setTextSize(i, f);
    }

    public long getQuoteId() {
        return this.id;
    }

    public Recipient getAuthor() {
        return this.author.get();
    }

    public CharSequence getBody() {
        return this.body;
    }

    public List<Attachment> getAttachments() {
        return this.attachments.asAttachments();
    }

    public QuoteModel.Type getQuoteType() {
        return this.quoteType;
    }

    public List<Mention> getMentions() {
        return MentionAnnotation.getMentionsFromAnnotations(this.body);
    }

    private ShapeAppearanceModel buildShapeAppearanceForLayoutDirection() {
        int pixels = (int) DimensionUnit.DP.toPixels(4.0f);
        if (getLayoutDirection() == 0) {
            float f = (float) pixels;
            return ShapeAppearanceModel.builder().setTopRightCorner(0, f).setBottomRightCorner(0, f).build();
        }
        float f2 = (float) pixels;
        return ShapeAppearanceModel.builder().setTopLeftCorner(0, f2).setBottomLeftCorner(0, f2).build();
    }

    private void applyColorTheme() {
        MessageType messageType = this.messageType;
        boolean z = true;
        boolean z2 = (messageType == MessageType.INCOMING || messageType == MessageType.STORY_REPLY_INCOMING) ? false : true;
        if (!(messageType == MessageType.PREVIEW || messageType == MessageType.STORY_REPLY_PREVIEW)) {
            z = false;
        }
        QuoteViewColorTheme resolveTheme = QuoteViewColorTheme.resolveTheme(z2, z, this.isWallpaperEnabled);
        this.quoteBarView.setBackgroundColor(resolveTheme.getBarColor(getContext()));
        this.background.setBackgroundColor(resolveTheme.getBackgroundColor(getContext()));
        this.authorView.setTextColor(resolveTheme.getForegroundColor(getContext()));
        this.bodyView.setTextColor(resolveTheme.getForegroundColor(getContext()));
        this.attachmentNameView.setTextColor(resolveTheme.getForegroundColor(getContext()));
        this.mediaDescriptionText.setTextColor(resolveTheme.getForegroundColor(getContext()));
        this.missingLinkText.setTextColor(resolveTheme.getForegroundColor(getContext()));
        this.footerView.setBackgroundColor(resolveTheme.getBackgroundColor(getContext()));
    }
}
