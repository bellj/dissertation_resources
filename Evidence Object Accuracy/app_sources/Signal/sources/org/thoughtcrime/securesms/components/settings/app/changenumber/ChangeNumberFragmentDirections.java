package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ChangeNumberFragmentDirections {
    private ChangeNumberFragmentDirections() {
    }

    public static NavDirections actionChangePhoneNumberFragmentToEnterPhoneNumberChangeFragment() {
        return new ActionOnlyNavDirections(R.id.action_changePhoneNumberFragment_to_enterPhoneNumberChangeFragment);
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }
}
