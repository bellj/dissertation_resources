package org.thoughtcrime.securesms.components.settings.models;

import android.content.Context;
import android.view.View;
import android.widget.TextView;
import com.google.android.material.switchmaterial.SwitchMaterial;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.models.OutlinedSwitch;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: OutlinedSwitch.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/OutlinedSwitch;", "", "()V", "register", "", "mappingAdapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class OutlinedSwitch {
    public static final OutlinedSwitch INSTANCE = new OutlinedSwitch();

    private OutlinedSwitch() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "mappingAdapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.models.OutlinedSwitch$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new OutlinedSwitch.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.outlined_switch));
    }

    /* compiled from: OutlinedSwitch.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B;\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\fJ\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u0000H\u0016J\u0010\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u0000H\u0016R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\rR\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\rR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u001d\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u0000\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/OutlinedSwitch$Model;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", "key", "", DraftDatabase.Draft.TEXT, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "isChecked", "", "isEnabled", "onClick", "Lkotlin/Function1;", "", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;ZZLkotlin/jvm/functions/Function1;)V", "()Z", "getKey", "()Ljava/lang/String;", "getOnClick", "()Lkotlin/jvm/functions/Function1;", "getText", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model implements MappingModel<Model> {
        private final boolean isChecked;
        private final boolean isEnabled;
        private final String key;
        private final Function1<Model, Unit> onClick;
        private final DSLSettingsText text;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Model model) {
            return MappingModel.CC.$default$getChangePayload(this, model);
        }

        /* JADX DEBUG: Multi-variable search result rejected for r6v0, resolved type: kotlin.jvm.functions.Function1<? super org.thoughtcrime.securesms.components.settings.models.OutlinedSwitch$Model, kotlin.Unit> */
        /* JADX WARN: Multi-variable type inference failed */
        public Model(String str, DSLSettingsText dSLSettingsText, boolean z, boolean z2, Function1<? super Model, Unit> function1) {
            Intrinsics.checkNotNullParameter(str, "key");
            Intrinsics.checkNotNullParameter(dSLSettingsText, DraftDatabase.Draft.TEXT);
            Intrinsics.checkNotNullParameter(function1, "onClick");
            this.key = str;
            this.text = dSLSettingsText;
            this.isChecked = z;
            this.isEnabled = z2;
            this.onClick = function1;
        }

        public /* synthetic */ Model(String str, DSLSettingsText dSLSettingsText, boolean z, boolean z2, Function1 function1, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? "OutlinedSwitch" : str, dSLSettingsText, z, z2, function1);
        }

        public final String getKey() {
            return this.key;
        }

        public final DSLSettingsText getText() {
            return this.text;
        }

        public final boolean isChecked() {
            return this.isChecked;
        }

        public final boolean isEnabled() {
            return this.isEnabled;
        }

        public final Function1<Model, Unit> getOnClick() {
            return this.onClick;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(model.key, this.key);
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return areItemsTheSame(model) && Intrinsics.areEqual(this.text, model.text) && this.isChecked == model.isChecked && this.isEnabled == model.isEnabled;
        }
    }

    /* compiled from: OutlinedSwitch.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/models/OutlinedSwitch$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/models/OutlinedSwitch$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "switch", "Lcom/google/android/material/switchmaterial/SwitchMaterial;", DraftDatabase.Draft.TEXT, "Landroid/widget/TextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {

        /* renamed from: switch */
        private final SwitchMaterial f2switch;
        private final TextView text;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.outlined_switch_control_text);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.outlined_switch_control_text)");
            this.text = (TextView) findViewById;
            View findViewById2 = findViewById(R.id.outlined_switch_switch);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "findViewById(R.id.outlined_switch_switch)");
            this.f2switch = (SwitchMaterial) findViewById2;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            TextView textView = this.text;
            DSLSettingsText text = model.getText();
            Context context = this.context;
            Intrinsics.checkNotNullExpressionValue(context, "context");
            textView.setText(text.resolve(context));
            this.f2switch.setChecked(model.isChecked());
            this.f2switch.setOnClickListener(new OutlinedSwitch$ViewHolder$$ExternalSyntheticLambda0(model));
            this.itemView.setEnabled(model.isEnabled());
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1254bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnClick().invoke(model);
        }
    }
}
