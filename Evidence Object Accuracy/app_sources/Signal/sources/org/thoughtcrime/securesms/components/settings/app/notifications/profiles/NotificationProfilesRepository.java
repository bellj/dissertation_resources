package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.core.ObservableOnSubscribe;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Action;
import io.reactivex.rxjava3.functions.Cancellable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import j$.time.ZoneId;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository;
import org.thoughtcrime.securesms.conversation.colors.AvatarColor;
import org.thoughtcrime.securesms.database.DatabaseObserver;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.NotificationProfileValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;

/* compiled from: NotificationProfilesRepository.kt */
@Metadata(d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\"\n\u0002\b\u0004\u0018\u00002\u00020\u0001:\u0001%B\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ\u001c\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u00062\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fJ\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\tJ\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00070\u00142\u0006\u0010\b\u001a\u00020\tJ\u0012\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00160\u0014J \u0010\u0017\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u0018\u001a\u00020\t2\b\b\u0002\u0010\u0019\u001a\u00020\tJ \u0010\u001a\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001c2\b\b\u0002\u0010\u0019\u001a\u00020\tJ \u0010\u001d\u001a\u00020\u00122\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u001b\u001a\u00020\u001c2\b\b\u0002\u0010\u0019\u001a\u00020\tJ\u0018\u0010\u001d\u001a\u00020\u00122\u0006\u0010\u001e\u001a\u00020\u00072\b\b\u0002\u0010\u0019\u001a\u00020\tJ\u001c\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ\"\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\t2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u000b0\"J$\u0010#\u001a\b\u0012\u0004\u0012\u00020\r0\u00062\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u000fJ\u0014\u0010#\u001a\b\u0012\u0004\u0012\u00020\r0\u00062\u0006\u0010\u001e\u001a\u00020\u0007J\u000e\u0010$\u001a\u00020\u00122\u0006\u0010\u001b\u001a\u00020\u001cR\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "", "()V", "database", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase;", "addMember", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "profileId", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "createProfile", "Lorg/thoughtcrime/securesms/database/NotificationProfileDatabase$NotificationProfileChangeResult;", "name", "", "selectedEmoji", "deleteProfile", "Lio/reactivex/rxjava3/core/Completable;", "getProfile", "Lio/reactivex/rxjava3/core/Observable;", "getProfiles", "", "manuallyEnableProfileForDuration", "enableUntil", "now", "manuallyEnableProfileForSchedule", "schedule", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "manuallyToggleProfile", "profile", "removeMember", "updateAllowedMembers", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "updateProfile", "updateSchedule", "NotificationProfileNotFoundException", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfilesRepository {
    private final NotificationProfileDatabase database = SignalDatabase.Companion.notificationProfiles();

    /* compiled from: NotificationProfilesRepository.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository$NotificationProfileNotFoundException;", "", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotificationProfileNotFoundException extends Throwable {
    }

    public final Observable<List<NotificationProfile>> getProfiles() {
        Observable<List<NotificationProfile>> subscribeOn = Observable.create(new ObservableOnSubscribe() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda18
            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                NotificationProfilesRepository.m787getProfiles$lambda2(NotificationProfilesRepository.this, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create { emitter: Observ…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getProfiles$lambda-2 */
    public static final void m787getProfiles$lambda2(NotificationProfilesRepository notificationProfilesRepository, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(observableEmitter, "emitter");
        DatabaseObserver databaseObserver = ApplicationDependencies.getDatabaseObserver();
        Intrinsics.checkNotNullExpressionValue(databaseObserver, "getDatabaseObserver()");
        NotificationProfilesRepository$$ExternalSyntheticLambda3 notificationProfilesRepository$$ExternalSyntheticLambda3 = new DatabaseObserver.Observer(notificationProfilesRepository) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda3
            public final /* synthetic */ NotificationProfilesRepository f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                NotificationProfilesRepository.m788getProfiles$lambda2$lambda0(ObservableEmitter.this, this.f$1);
            }
        };
        databaseObserver.registerNotificationProfileObserver(notificationProfilesRepository$$ExternalSyntheticLambda3);
        observableEmitter.setCancellable(new Cancellable(notificationProfilesRepository$$ExternalSyntheticLambda3) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda4
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                NotificationProfilesRepository.m789getProfiles$lambda2$lambda1(DatabaseObserver.this, this.f$1);
            }
        });
        observableEmitter.onNext(notificationProfilesRepository.database.getProfiles());
    }

    /* renamed from: getProfiles$lambda-2$lambda-0 */
    public static final void m788getProfiles$lambda2$lambda0(ObservableEmitter observableEmitter, NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(observableEmitter, "$emitter");
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        observableEmitter.onNext(notificationProfilesRepository.database.getProfiles());
    }

    /* renamed from: getProfiles$lambda-2$lambda-1 */
    public static final void m789getProfiles$lambda2$lambda1(DatabaseObserver databaseObserver, DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(databaseObserver, "$databaseObserver");
        Intrinsics.checkNotNullParameter(observer, "$profileObserver");
        databaseObserver.unregisterObserver(observer);
    }

    public final Observable<NotificationProfile> getProfile(long j) {
        Observable<NotificationProfile> subscribeOn = Observable.create(new ObservableOnSubscribe(j) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda16
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.core.ObservableOnSubscribe
            public final void subscribe(ObservableEmitter observableEmitter) {
                NotificationProfilesRepository.m784getProfile$lambda5(NotificationProfilesRepository.this, this.f$1, observableEmitter);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "create { emitter: Observ…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getProfile$lambda-5 */
    public static final void m784getProfile$lambda5(NotificationProfilesRepository notificationProfilesRepository, long j, ObservableEmitter observableEmitter) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(observableEmitter, "emitter");
        NotificationProfilesRepository$getProfile$1$emitProfile$1 notificationProfilesRepository$getProfile$1$emitProfile$1 = new Function0<Unit>(notificationProfilesRepository, j, observableEmitter) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$getProfile$1$emitProfile$1
            final /* synthetic */ ObservableEmitter<NotificationProfile> $emitter;
            final /* synthetic */ long $profileId;
            final /* synthetic */ NotificationProfilesRepository this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$profileId = r2;
                this.$emitter = r4;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                NotificationProfile profile = this.this$0.database.getProfile(this.$profileId);
                if (profile != null) {
                    this.$emitter.onNext(profile);
                } else {
                    this.$emitter.onError(new NotificationProfilesRepository.NotificationProfileNotFoundException());
                }
            }
        };
        DatabaseObserver databaseObserver = ApplicationDependencies.getDatabaseObserver();
        Intrinsics.checkNotNullExpressionValue(databaseObserver, "getDatabaseObserver()");
        NotificationProfilesRepository$$ExternalSyntheticLambda5 notificationProfilesRepository$$ExternalSyntheticLambda5 = new DatabaseObserver.Observer() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda5
            @Override // org.thoughtcrime.securesms.database.DatabaseObserver.Observer
            public final void onChanged() {
                NotificationProfilesRepository.m785getProfile$lambda5$lambda3(Function0.this);
            }
        };
        databaseObserver.registerNotificationProfileObserver(notificationProfilesRepository$$ExternalSyntheticLambda5);
        observableEmitter.setCancellable(new Cancellable(notificationProfilesRepository$$ExternalSyntheticLambda5) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda6
            public final /* synthetic */ DatabaseObserver.Observer f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Cancellable
            public final void cancel() {
                NotificationProfilesRepository.m786getProfile$lambda5$lambda4(DatabaseObserver.this, this.f$1);
            }
        });
        notificationProfilesRepository$getProfile$1$emitProfile$1.invoke();
    }

    /* renamed from: getProfile$lambda-5$lambda-3 */
    public static final void m785getProfile$lambda5$lambda3(Function0 function0) {
        Intrinsics.checkNotNullParameter(function0, "$emitProfile");
        function0.invoke();
    }

    /* renamed from: getProfile$lambda-5$lambda-4 */
    public static final void m786getProfile$lambda5$lambda4(DatabaseObserver databaseObserver, DatabaseObserver.Observer observer) {
        Intrinsics.checkNotNullParameter(databaseObserver, "$databaseObserver");
        Intrinsics.checkNotNullParameter(observer, "$profileObserver");
        databaseObserver.unregisterObserver(observer);
    }

    /* renamed from: createProfile$lambda-6 */
    public static final NotificationProfileDatabase.NotificationProfileChangeResult m782createProfile$lambda6(NotificationProfilesRepository notificationProfilesRepository, String str, String str2) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(str, "$name");
        Intrinsics.checkNotNullParameter(str2, "$selectedEmoji");
        NotificationProfileDatabase notificationProfileDatabase = notificationProfilesRepository.database;
        AvatarColor random = AvatarColor.random();
        Intrinsics.checkNotNullExpressionValue(random, "random()");
        return notificationProfileDatabase.createProfile(str, str2, random, System.currentTimeMillis());
    }

    public final Single<NotificationProfileDatabase.NotificationProfileChangeResult> createProfile(String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(str2, "selectedEmoji");
        Single<NotificationProfileDatabase.NotificationProfileChangeResult> subscribeOn = Single.fromCallable(new Callable(str, str2) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda17
            public final /* synthetic */ String f$1;
            public final /* synthetic */ String f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m782createProfile$lambda6(NotificationProfilesRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: updateProfile$lambda-7 */
    public static final NotificationProfileDatabase.NotificationProfileChangeResult m798updateProfile$lambda7(NotificationProfilesRepository notificationProfilesRepository, long j, String str, String str2) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(str, "$name");
        Intrinsics.checkNotNullParameter(str2, "$selectedEmoji");
        return notificationProfilesRepository.database.updateProfile(j, str, str2);
    }

    public final Single<NotificationProfileDatabase.NotificationProfileChangeResult> updateProfile(long j, String str, String str2) {
        Intrinsics.checkNotNullParameter(str, "name");
        Intrinsics.checkNotNullParameter(str2, "selectedEmoji");
        Single<NotificationProfileDatabase.NotificationProfileChangeResult> subscribeOn = Single.fromCallable(new Callable(j, str, str2) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda15
            public final /* synthetic */ long f$1;
            public final /* synthetic */ String f$2;
            public final /* synthetic */ String f$3;

            {
                this.f$1 = r2;
                this.f$2 = r4;
                this.f$3 = r5;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m798updateProfile$lambda7(NotificationProfilesRepository.this, this.f$1, this.f$2, this.f$3);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: updateProfile$lambda-8 */
    public static final NotificationProfileDatabase.NotificationProfileChangeResult m799updateProfile$lambda8(NotificationProfilesRepository notificationProfilesRepository, NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(notificationProfile, "$profile");
        return notificationProfilesRepository.database.updateProfile(notificationProfile);
    }

    public final Single<NotificationProfileDatabase.NotificationProfileChangeResult> updateProfile(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        Single<NotificationProfileDatabase.NotificationProfileChangeResult> subscribeOn = Single.fromCallable(new Callable(notificationProfile) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda10
            public final /* synthetic */ NotificationProfile f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m799updateProfile$lambda8(NotificationProfilesRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: updateAllowedMembers$lambda-9 */
    public static final NotificationProfile m797updateAllowedMembers$lambda9(NotificationProfilesRepository notificationProfilesRepository, long j, Set set) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(set, "$recipients");
        return notificationProfilesRepository.database.setAllowedRecipients(j, set);
    }

    public final Single<NotificationProfile> updateAllowedMembers(long j, Set<? extends RecipientId> set) {
        Intrinsics.checkNotNullParameter(set, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
        Single<NotificationProfile> subscribeOn = Single.fromCallable(new Callable(j, set) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda7
            public final /* synthetic */ long f$1;
            public final /* synthetic */ Set f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m797updateAllowedMembers$lambda9(NotificationProfilesRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: removeMember$lambda-10 */
    public static final NotificationProfile m796removeMember$lambda10(NotificationProfilesRepository notificationProfilesRepository, long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        return notificationProfilesRepository.database.removeAllowedRecipient(j, recipientId);
    }

    public final Single<NotificationProfile> removeMember(long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Single<NotificationProfile> subscribeOn = Single.fromCallable(new Callable(j, recipientId) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda9
            public final /* synthetic */ long f$1;
            public final /* synthetic */ RecipientId f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m796removeMember$lambda10(NotificationProfilesRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: addMember$lambda-11 */
    public static final NotificationProfile m781addMember$lambda11(NotificationProfilesRepository notificationProfilesRepository, long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(recipientId, "$recipientId");
        return notificationProfilesRepository.database.addAllowedRecipient(j, recipientId);
    }

    public final Single<NotificationProfile> addMember(long j, RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Single<NotificationProfile> subscribeOn = Single.fromCallable(new Callable(j, recipientId) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;
            public final /* synthetic */ RecipientId f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m781addMember$lambda11(NotificationProfilesRepository.this, this.f$1, this.f$2);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: deleteProfile$lambda-12 */
    public static final Unit m783deleteProfile$lambda12(NotificationProfilesRepository notificationProfilesRepository, long j) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        notificationProfilesRepository.database.deleteProfile(j);
        return Unit.INSTANCE;
    }

    public final Completable deleteProfile(long j) {
        Completable subscribeOn = Completable.fromCallable(new Callable(j) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda8
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m783deleteProfile$lambda12(NotificationProfilesRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: updateSchedule$lambda-13 */
    public static final Unit m800updateSchedule$lambda13(NotificationProfilesRepository notificationProfilesRepository, NotificationProfileSchedule notificationProfileSchedule) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "$schedule");
        NotificationProfileDatabase.updateSchedule$default(notificationProfilesRepository.database, notificationProfileSchedule, false, 2, null);
        return Unit.INSTANCE;
    }

    public final Completable updateSchedule(NotificationProfileSchedule notificationProfileSchedule) {
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        Completable subscribeOn = Completable.fromCallable(new Callable(notificationProfileSchedule) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda19
            public final /* synthetic */ NotificationProfileSchedule f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.util.concurrent.Callable
            public final Object call() {
                return NotificationProfilesRepository.m800updateSchedule$lambda13(NotificationProfilesRepository.this, this.f$1);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable { database.…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    public static /* synthetic */ Completable manuallyToggleProfile$default(NotificationProfilesRepository notificationProfilesRepository, NotificationProfile notificationProfile, long j, int i, Object obj) {
        if ((i & 2) != 0) {
            j = System.currentTimeMillis();
        }
        return notificationProfilesRepository.manuallyToggleProfile(notificationProfile, j);
    }

    public final Completable manuallyToggleProfile(NotificationProfile notificationProfile, long j) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        return manuallyToggleProfile(notificationProfile.getId(), notificationProfile.getSchedule(), j);
    }

    public static /* synthetic */ Completable manuallyToggleProfile$default(NotificationProfilesRepository notificationProfilesRepository, long j, NotificationProfileSchedule notificationProfileSchedule, long j2, int i, Object obj) {
        if ((i & 4) != 0) {
            j2 = System.currentTimeMillis();
        }
        return notificationProfilesRepository.manuallyToggleProfile(j, notificationProfileSchedule, j2);
    }

    public final Completable manuallyToggleProfile(long j, NotificationProfileSchedule notificationProfileSchedule, long j2) {
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        Completable subscribeOn = Completable.fromAction(new Action(j2, j, notificationProfileSchedule) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda13
            public final /* synthetic */ long f$1;
            public final /* synthetic */ long f$2;
            public final /* synthetic */ NotificationProfileSchedule f$3;

            {
                this.f$1 = r2;
                this.f$2 = r4;
                this.f$3 = r6;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                NotificationProfilesRepository.m794manuallyToggleProfile$lambda14(NotificationProfilesRepository.this, this.f$1, this.f$2, this.f$3);
            }
        }).doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda14
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                NotificationProfilesRepository.m795manuallyToggleProfile$lambda15();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      val p…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: manuallyToggleProfile$lambda-14 */
    public static final void m794manuallyToggleProfile$lambda14(NotificationProfilesRepository notificationProfilesRepository, long j, long j2, NotificationProfileSchedule notificationProfileSchedule) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "this$0");
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "$schedule");
        NotificationProfile activeProfile$default = NotificationProfiles.getActiveProfile$default(notificationProfilesRepository.database.getProfiles(), j, null, 4, null);
        boolean z = false;
        if (activeProfile$default != null && j2 == activeProfile$default.getId()) {
            z = true;
        }
        if (z) {
            SignalStore.notificationProfileValues().setManuallyEnabledProfile(0);
            SignalStore.notificationProfileValues().setManuallyEnabledUntil(0);
            SignalStore.notificationProfileValues().setManuallyDisabledAt(j);
            SignalStore.notificationProfileValues().setLastProfilePopup(0);
            SignalStore.notificationProfileValues().setLastProfilePopupTime(0);
            return;
        }
        boolean isCurrentlyActive$default = NotificationProfileSchedule.isCurrentlyActive$default(notificationProfileSchedule, j, null, 2, null);
        SignalStore.notificationProfileValues().setManuallyEnabledProfile(j2);
        SignalStore.notificationProfileValues().setManuallyEnabledUntil(isCurrentlyActive$default ? JavaTimeExtensionsKt.toMillis$default(notificationProfileSchedule.endDateTime(JavaTimeExtensionsKt.toLocalDateTime$default(j, (ZoneId) null, 1, (Object) null)), null, 1, null) : Long.MAX_VALUE);
        SignalStore.notificationProfileValues().setManuallyDisabledAt(j);
    }

    /* renamed from: manuallyToggleProfile$lambda-15 */
    public static final void m795manuallyToggleProfile$lambda15() {
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
    }

    public static /* synthetic */ Completable manuallyEnableProfileForDuration$default(NotificationProfilesRepository notificationProfilesRepository, long j, long j2, long j3, int i, Object obj) {
        if ((i & 4) != 0) {
            j3 = System.currentTimeMillis();
        }
        return notificationProfilesRepository.manuallyEnableProfileForDuration(j, j2, j3);
    }

    public final Completable manuallyEnableProfileForDuration(long j, long j2, long j3) {
        Completable subscribeOn = Completable.fromAction(new Action(j, j2, j3) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ long f$0;
            public final /* synthetic */ long f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$0 = r1;
                this.f$1 = r3;
                this.f$2 = r5;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                NotificationProfilesRepository.m790manuallyEnableProfileForDuration$lambda16(this.f$0, this.f$1, this.f$2);
            }
        }).doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                NotificationProfilesRepository.m791manuallyEnableProfileForDuration$lambda17();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      Signa…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: manuallyEnableProfileForDuration$lambda-16 */
    public static final void m790manuallyEnableProfileForDuration$lambda16(long j, long j2, long j3) {
        SignalStore.notificationProfileValues().setManuallyEnabledProfile(j);
        SignalStore.notificationProfileValues().setManuallyEnabledUntil(j2);
        SignalStore.notificationProfileValues().setManuallyDisabledAt(j3);
    }

    /* renamed from: manuallyEnableProfileForDuration$lambda-17 */
    public static final void m791manuallyEnableProfileForDuration$lambda17() {
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
    }

    public static /* synthetic */ Completable manuallyEnableProfileForSchedule$default(NotificationProfilesRepository notificationProfilesRepository, long j, NotificationProfileSchedule notificationProfileSchedule, long j2, int i, Object obj) {
        if ((i & 4) != 0) {
            j2 = System.currentTimeMillis();
        }
        return notificationProfilesRepository.manuallyEnableProfileForSchedule(j, notificationProfileSchedule, j2);
    }

    public final Completable manuallyEnableProfileForSchedule(long j, NotificationProfileSchedule notificationProfileSchedule, long j2) {
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "schedule");
        Completable subscribeOn = Completable.fromAction(new Action(j2, j) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda11
            public final /* synthetic */ long f$1;
            public final /* synthetic */ long f$2;

            {
                this.f$1 = r2;
                this.f$2 = r4;
            }

            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                NotificationProfilesRepository.m792manuallyEnableProfileForSchedule$lambda18(NotificationProfileSchedule.this, this.f$1, this.f$2);
            }
        }).doOnComplete(new Action() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfilesRepository$$ExternalSyntheticLambda12
            @Override // io.reactivex.rxjava3.functions.Action
            public final void run() {
                NotificationProfilesRepository.m793manuallyEnableProfileForSchedule$lambda19();
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromAction {\n      val i…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: manuallyEnableProfileForSchedule$lambda-18 */
    public static final void m792manuallyEnableProfileForSchedule$lambda18(NotificationProfileSchedule notificationProfileSchedule, long j, long j2) {
        Intrinsics.checkNotNullParameter(notificationProfileSchedule, "$schedule");
        boolean isCurrentlyActive$default = NotificationProfileSchedule.isCurrentlyActive$default(notificationProfileSchedule, j, null, 2, null);
        NotificationProfileValues notificationProfileValues = SignalStore.notificationProfileValues();
        if (!isCurrentlyActive$default) {
            j2 = 0;
        }
        notificationProfileValues.setManuallyEnabledProfile(j2);
        SignalStore.notificationProfileValues().setManuallyEnabledUntil(isCurrentlyActive$default ? JavaTimeExtensionsKt.toMillis$default(notificationProfileSchedule.endDateTime(JavaTimeExtensionsKt.toLocalDateTime$default(j, (ZoneId) null, 1, (Object) null)), null, 1, null) : Long.MAX_VALUE);
        NotificationProfileValues notificationProfileValues2 = SignalStore.notificationProfileValues();
        if (!isCurrentlyActive$default) {
            j = 0;
        }
        notificationProfileValues2.setManuallyDisabledAt(j);
    }

    /* renamed from: manuallyEnableProfileForSchedule$lambda-19 */
    public static final void m793manuallyEnableProfileForSchedule$lambda19() {
        ApplicationDependencies.getDatabaseObserver().notifyNotificationProfileObservers();
    }
}
