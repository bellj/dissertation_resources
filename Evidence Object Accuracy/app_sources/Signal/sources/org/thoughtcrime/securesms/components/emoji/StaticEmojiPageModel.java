package org.thoughtcrime.securesms.components.emoji;

import android.net.Uri;
import j$.util.DesugarArrays;
import j$.util.function.Function;
import j$.util.stream.Collectors;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.thoughtcrime.securesms.emoji.EmojiCategory;

/* loaded from: classes4.dex */
public class StaticEmojiPageModel implements EmojiPageModel {
    private final EmojiCategory category;
    private final List<Emoji> emoji;
    private final Uri sprite;

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public boolean isDynamic() {
        return false;
    }

    public StaticEmojiPageModel(EmojiCategory emojiCategory, String[] strArr, Uri uri) {
        this(emojiCategory, (List) DesugarArrays.stream(strArr).map(new Function() { // from class: org.thoughtcrime.securesms.components.emoji.StaticEmojiPageModel$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return StaticEmojiPageModel.$r8$lambda$JF4b3_hSAjv4mgyJdDQqQKS7TPI((String) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }).collect(Collectors.toList()), uri);
    }

    public static /* synthetic */ Emoji lambda$new$0(String str) {
        return new Emoji(Collections.singletonList(str));
    }

    public StaticEmojiPageModel(EmojiCategory emojiCategory, List<Emoji> list, Uri uri) {
        this.category = emojiCategory;
        this.emoji = Collections.unmodifiableList(list);
        this.sprite = uri;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public String getKey() {
        return this.category.getKey();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public int getIconAttr() {
        return this.category.getIcon();
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<String> getEmoji() {
        LinkedList linkedList = new LinkedList();
        for (Emoji emoji : this.emoji) {
            linkedList.addAll(emoji.getVariations());
        }
        return linkedList;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public List<Emoji> getDisplayEmoji() {
        return this.emoji;
    }

    @Override // org.thoughtcrime.securesms.components.emoji.EmojiPageModel
    public Uri getSpriteUri() {
        return this.sprite;
    }
}
