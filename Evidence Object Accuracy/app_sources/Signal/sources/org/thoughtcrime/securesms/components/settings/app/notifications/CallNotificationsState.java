package org.thoughtcrime.securesms.components.settings.app.notifications;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: NotificationsSettingsState.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00032\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0016HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\t¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/CallNotificationsState;", "", "notificationsEnabled", "", "ringtone", "Landroid/net/Uri;", "vibrateEnabled", "(ZLandroid/net/Uri;Z)V", "getNotificationsEnabled", "()Z", "getRingtone", "()Landroid/net/Uri;", "getVibrateEnabled", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CallNotificationsState {
    private final boolean notificationsEnabled;
    private final Uri ringtone;
    private final boolean vibrateEnabled;

    public static /* synthetic */ CallNotificationsState copy$default(CallNotificationsState callNotificationsState, boolean z, Uri uri, boolean z2, int i, Object obj) {
        if ((i & 1) != 0) {
            z = callNotificationsState.notificationsEnabled;
        }
        if ((i & 2) != 0) {
            uri = callNotificationsState.ringtone;
        }
        if ((i & 4) != 0) {
            z2 = callNotificationsState.vibrateEnabled;
        }
        return callNotificationsState.copy(z, uri, z2);
    }

    public final boolean component1() {
        return this.notificationsEnabled;
    }

    public final Uri component2() {
        return this.ringtone;
    }

    public final boolean component3() {
        return this.vibrateEnabled;
    }

    public final CallNotificationsState copy(boolean z, Uri uri, boolean z2) {
        Intrinsics.checkNotNullParameter(uri, "ringtone");
        return new CallNotificationsState(z, uri, z2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CallNotificationsState)) {
            return false;
        }
        CallNotificationsState callNotificationsState = (CallNotificationsState) obj;
        return this.notificationsEnabled == callNotificationsState.notificationsEnabled && Intrinsics.areEqual(this.ringtone, callNotificationsState.ringtone) && this.vibrateEnabled == callNotificationsState.vibrateEnabled;
    }

    public int hashCode() {
        boolean z = this.notificationsEnabled;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode = ((i2 * 31) + this.ringtone.hashCode()) * 31;
        boolean z2 = this.vibrateEnabled;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return hashCode + i;
    }

    public String toString() {
        return "CallNotificationsState(notificationsEnabled=" + this.notificationsEnabled + ", ringtone=" + this.ringtone + ", vibrateEnabled=" + this.vibrateEnabled + ')';
    }

    public CallNotificationsState(boolean z, Uri uri, boolean z2) {
        Intrinsics.checkNotNullParameter(uri, "ringtone");
        this.notificationsEnabled = z;
        this.ringtone = uri;
        this.vibrateEnabled = z2;
    }

    public final boolean getNotificationsEnabled() {
        return this.notificationsEnabled;
    }

    public final Uri getRingtone() {
        return this.ringtone;
    }

    public final boolean getVibrateEnabled() {
        return this.vibrateEnabled;
    }
}
