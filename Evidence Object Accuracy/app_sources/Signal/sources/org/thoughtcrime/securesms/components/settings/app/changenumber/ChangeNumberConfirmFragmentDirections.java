package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ChangeNumberConfirmFragmentDirections {
    private ChangeNumberConfirmFragmentDirections() {
    }

    public static NavDirections actionChangePhoneNumberConfirmFragmentToChangePhoneNumberVerifyFragment() {
        return new ActionOnlyNavDirections(R.id.action_changePhoneNumberConfirmFragment_to_changePhoneNumberVerifyFragment);
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }
}
