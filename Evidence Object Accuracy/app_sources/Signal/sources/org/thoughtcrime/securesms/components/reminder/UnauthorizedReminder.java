package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.view.View;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.registration.RegistrationNavigationActivity;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class UnauthorizedReminder extends Reminder {
    @Override // org.thoughtcrime.securesms.components.reminder.Reminder
    public boolean isDismissable() {
        return false;
    }

    public UnauthorizedReminder(Context context) {
        super(context.getString(R.string.UnauthorizedReminder_device_no_longer_registered), context.getString(R.string.UnauthorizedReminder_this_is_likely_because_you_registered_your_phone_number_with_Signal_on_a_different_device));
        setOkListener(new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.UnauthorizedReminder$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                UnauthorizedReminder.$r8$lambda$a4UjpBITBbtOTe4F8G01HKMqxqA(this.f$0, view);
            }
        });
    }

    public static /* synthetic */ void lambda$new$0(Context context, View view) {
        context.startActivity(RegistrationNavigationActivity.newIntentForReRegistration(context));
    }

    public static boolean isEligible(Context context) {
        return TextSecurePreferences.isUnauthorizedRecieved(context);
    }
}
