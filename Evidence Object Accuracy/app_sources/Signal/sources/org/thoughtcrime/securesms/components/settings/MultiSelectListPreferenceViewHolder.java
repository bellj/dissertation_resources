package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import java.util.ArrayList;
import java.util.Arrays;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/MultiSelectListPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/MultiSelectListPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MultiSelectListPreferenceViewHolder extends PreferenceViewHolder<MultiSelectListPreference> {
    /* renamed from: bind$lambda-4$lambda-1 */
    public static final void m534bind$lambda4$lambda1(DialogInterface dialogInterface, int i, boolean z) {
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public MultiSelectListPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
    }

    public void bind(MultiSelectListPreference multiSelectListPreference) {
        Intrinsics.checkNotNullParameter(multiSelectListPreference, "model");
        super.bind((MultiSelectListPreferenceViewHolder) multiSelectListPreference);
        boolean z = false;
        getSummaryView().setVisibility(0);
        boolean[] selected = multiSelectListPreference.getSelected();
        ArrayList arrayList = new ArrayList(selected.length);
        int i = 0;
        for (boolean z2 : selected) {
            i++;
            arrayList.add(z2 ? multiSelectListPreference.getListItems()[i] : null);
        }
        String str = CollectionsKt___CollectionsKt.joinToString$default(CollectionsKt___CollectionsKt.filterNotNull(arrayList), ", ", null, null, 0, null, null, 62, null);
        if (str.length() == 0) {
            z = true;
        }
        if (z) {
            getSummaryView().setText(R.string.preferences__none);
        } else {
            getSummaryView().setText(str);
        }
        boolean[] selected2 = multiSelectListPreference.getSelected();
        boolean[] copyOf = Arrays.copyOf(selected2, selected2.length);
        Intrinsics.checkNotNullExpressionValue(copyOf, "copyOf(this, size)");
        this.itemView.setOnClickListener(new View.OnClickListener(multiSelectListPreference, copyOf) { // from class: org.thoughtcrime.securesms.components.settings.MultiSelectListPreferenceViewHolder$$ExternalSyntheticLambda0
            public final /* synthetic */ MultiSelectListPreference f$1;
            public final /* synthetic */ boolean[] f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                MultiSelectListPreferenceViewHolder.$r8$lambda$2ieC72JekKdn4jWNEzR9JNmlaEs(MultiSelectListPreferenceViewHolder.this, this.f$1, this.f$2, view);
            }
        });
    }

    /* renamed from: bind$lambda-4 */
    public static final void m533bind$lambda4(MultiSelectListPreferenceViewHolder multiSelectListPreferenceViewHolder, MultiSelectListPreference multiSelectListPreference, boolean[] zArr, View view) {
        Intrinsics.checkNotNullParameter(multiSelectListPreferenceViewHolder, "this$0");
        Intrinsics.checkNotNullParameter(multiSelectListPreference, "$model");
        Intrinsics.checkNotNullParameter(zArr, "$selected");
        MaterialAlertDialogBuilder materialAlertDialogBuilder = new MaterialAlertDialogBuilder(multiSelectListPreferenceViewHolder.context);
        DSLSettingsText title = multiSelectListPreference.getTitle();
        Context context = multiSelectListPreferenceViewHolder.context;
        Intrinsics.checkNotNullExpressionValue(context, "context");
        materialAlertDialogBuilder.setTitle(title.resolve(context)).setMultiChoiceItems((CharSequence[]) multiSelectListPreference.getListItems(), zArr, (DialogInterface.OnMultiChoiceClickListener) new DialogInterface.OnMultiChoiceClickListener() { // from class: org.thoughtcrime.securesms.components.settings.MultiSelectListPreferenceViewHolder$$ExternalSyntheticLambda1
            @Override // android.content.DialogInterface.OnMultiChoiceClickListener
            public final void onClick(DialogInterface dialogInterface, int i, boolean z) {
                MultiSelectListPreferenceViewHolder.$r8$lambda$xoQW4IIQ6QOnsfDzWXcXlS0PFrc(dialogInterface, i, z);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.MultiSelectListPreferenceViewHolder$$ExternalSyntheticLambda2
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MultiSelectListPreferenceViewHolder.$r8$lambda$caAQxmOmcDD8UFi6cJlHvyzeE5A(dialogInterface, i);
            }
        }).setPositiveButton(17039370, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(zArr) { // from class: org.thoughtcrime.securesms.components.settings.MultiSelectListPreferenceViewHolder$$ExternalSyntheticLambda3
            public final /* synthetic */ boolean[] f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                MultiSelectListPreferenceViewHolder.m532$r8$lambda$dEtfPV6vmVnfOKBTbnWxKMdJq8(MultiSelectListPreference.this, this.f$1, dialogInterface, i);
            }
        }).show();
    }

    /* renamed from: bind$lambda-4$lambda-2 */
    public static final void m535bind$lambda4$lambda2(DialogInterface dialogInterface, int i) {
        dialogInterface.dismiss();
    }

    /* renamed from: bind$lambda-4$lambda-3 */
    public static final void m536bind$lambda4$lambda3(MultiSelectListPreference multiSelectListPreference, boolean[] zArr, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(multiSelectListPreference, "$model");
        Intrinsics.checkNotNullParameter(zArr, "$selected");
        multiSelectListPreference.getOnSelected().invoke(zArr);
        dialogInterface.dismiss();
    }
}
