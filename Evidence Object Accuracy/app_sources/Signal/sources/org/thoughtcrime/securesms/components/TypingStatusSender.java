package org.thoughtcrime.securesms.components;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.ThreadUtil;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.TypingSendJob;
import org.thoughtcrime.securesms.util.Util;

/* loaded from: classes4.dex */
public class TypingStatusSender {
    private static final long PAUSE_TYPING_TIMEOUT;
    private static final long REFRESH_TYPING_TIMEOUT;
    private static final String TAG = Log.tag(TypingStatusSender.class);
    private final Map<Long, TimerPair> selfTypingTimers = new HashMap();

    static {
        TAG = Log.tag(TypingStatusSender.class);
        TimeUnit timeUnit = TimeUnit.SECONDS;
        REFRESH_TYPING_TIMEOUT = timeUnit.toMillis(10);
        PAUSE_TYPING_TIMEOUT = timeUnit.toMillis(3);
    }

    public synchronized void onTypingStarted(long j) {
        TimerPair timerPair = (TimerPair) Util.getOrDefault(this.selfTypingTimers, Long.valueOf(j), new TimerPair());
        this.selfTypingTimers.put(Long.valueOf(j), timerPair);
        if (timerPair.getStart() == null) {
            sendTyping(j, true);
            StartRunnable startRunnable = new StartRunnable(j);
            ThreadUtil.runOnMainDelayed(startRunnable, REFRESH_TYPING_TIMEOUT);
            timerPair.setStart(startRunnable);
        }
        if (timerPair.getStop() != null) {
            ThreadUtil.cancelRunnableOnMain(timerPair.getStop());
        }
        TypingStatusSender$$ExternalSyntheticLambda0 typingStatusSender$$ExternalSyntheticLambda0 = new Runnable(j) { // from class: org.thoughtcrime.securesms.components.TypingStatusSender$$ExternalSyntheticLambda0
            public final /* synthetic */ long f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                TypingStatusSender.$r8$lambda$NoIqGUksrcCSTVAN4IPIP4x4wqs(TypingStatusSender.this, this.f$1);
            }
        };
        ThreadUtil.runOnMainDelayed(typingStatusSender$$ExternalSyntheticLambda0, PAUSE_TYPING_TIMEOUT);
        timerPair.setStop(typingStatusSender$$ExternalSyntheticLambda0);
    }

    public /* synthetic */ void lambda$onTypingStarted$0(long j) {
        onTypingStopped(j, true);
    }

    public synchronized void onTypingStopped(long j) {
        onTypingStopped(j, false);
    }

    public synchronized void onTypingStoppedWithNotify(long j) {
        onTypingStopped(j, true);
    }

    private synchronized void onTypingStopped(long j, boolean z) {
        TimerPair timerPair = (TimerPair) Util.getOrDefault(this.selfTypingTimers, Long.valueOf(j), new TimerPair());
        this.selfTypingTimers.put(Long.valueOf(j), timerPair);
        if (timerPair.getStart() != null) {
            ThreadUtil.cancelRunnableOnMain(timerPair.getStart());
            if (z) {
                sendTyping(j, false);
            }
        }
        if (timerPair.getStop() != null) {
            ThreadUtil.cancelRunnableOnMain(timerPair.getStop());
        }
        timerPair.setStart(null);
        timerPair.setStop(null);
    }

    public void sendTyping(long j, boolean z) {
        ApplicationDependencies.getJobManager().add(new TypingSendJob(j, z));
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class StartRunnable implements Runnable {
        private final long threadId;

        private StartRunnable(long j) {
            TypingStatusSender.this = r1;
            this.threadId = j;
        }

        @Override // java.lang.Runnable
        public void run() {
            TypingStatusSender.this.sendTyping(this.threadId, true);
            ThreadUtil.runOnMainDelayed(this, TypingStatusSender.REFRESH_TYPING_TIMEOUT);
        }
    }

    /* loaded from: classes4.dex */
    public static class TimerPair {
        private Runnable start;
        private Runnable stop;

        private TimerPair() {
        }

        public Runnable getStart() {
            return this.start;
        }

        public void setStart(Runnable runnable) {
            this.start = runnable;
        }

        public Runnable getStop() {
            return this.stop;
        }

        public void setStop(Runnable runnable) {
            this.stop = runnable;
        }
    }
}
