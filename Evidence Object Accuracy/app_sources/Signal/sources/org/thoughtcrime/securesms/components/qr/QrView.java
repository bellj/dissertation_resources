package org.thoughtcrime.securesms.components.qr;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.SquareImageView;
import org.thoughtcrime.securesms.qr.QrCode;

/* loaded from: classes4.dex */
public class QrView extends SquareImageView {
    private static final int DEFAULT_BACKGROUND_COLOR;
    private static final int DEFAULT_FOREGROUND_COLOR;
    private int backgroundColor;
    private int foregroundColor;
    private Bitmap qrBitmap;

    public QrView(Context context) {
        super(context);
        init(null);
    }

    public QrView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public QrView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.QrView, 0, 0);
            this.foregroundColor = obtainStyledAttributes.getColor(1, DEFAULT_FOREGROUND_COLOR);
            this.backgroundColor = obtainStyledAttributes.getColor(0, 0);
            obtainStyledAttributes.recycle();
        } else {
            this.foregroundColor = DEFAULT_FOREGROUND_COLOR;
            this.backgroundColor = 0;
        }
        if (isInEditMode()) {
            setQrText("https://signal.org");
        }
    }

    public void setQrText(String str) {
        setQrBitmap(QrCode.create(str, this.foregroundColor, this.backgroundColor));
    }

    private void setQrBitmap(Bitmap bitmap) {
        Bitmap bitmap2 = this.qrBitmap;
        if (bitmap2 != bitmap) {
            if (bitmap2 != null) {
                bitmap2.recycle();
            }
            this.qrBitmap = bitmap;
            setImageBitmap(bitmap);
        }
    }

    public Bitmap getQrBitmap() {
        return this.qrBitmap;
    }
}
