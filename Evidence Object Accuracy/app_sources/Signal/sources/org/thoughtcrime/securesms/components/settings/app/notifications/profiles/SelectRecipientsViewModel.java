package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: SelectRecipientsViewModel.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010#\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u0015B#\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b¢\u0006\u0002\u0010\tJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0006J\u000e\u0010\u0011\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0006J\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u00060\u000b¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/SelectRecipientsViewModel;", "Landroidx/lifecycle/ViewModel;", "profileId", "", "currentSelection", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "(JLjava/util/Set;Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "getRecipients", "()Ljava/util/Set;", "deselect", "", "recipientId", "select", "updateAllowedMembers", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SelectRecipientsViewModel extends ViewModel {
    private final long profileId;
    private final Set<RecipientId> recipients;
    private final NotificationProfilesRepository repository;

    public SelectRecipientsViewModel(long j, Set<? extends RecipientId> set, NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(set, "currentSelection");
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "repository");
        this.profileId = j;
        this.repository = notificationProfilesRepository;
        this.recipients = CollectionsKt___CollectionsKt.toMutableSet(set);
    }

    public final Set<RecipientId> getRecipients() {
        return this.recipients;
    }

    public final void select(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.recipients.add(recipientId);
    }

    public final void deselect(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        this.recipients.remove(recipientId);
    }

    public final Single<NotificationProfile> updateAllowedMembers() {
        Single<NotificationProfile> observeOn = this.repository.updateAllowedMembers(this.profileId, this.recipients).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.updateAllowed…dSchedulers.mainThread())");
        return observeOn;
    }

    /* compiled from: SelectRecipientsViewModel.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\"\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J%\u0010\n\u001a\u0002H\u000b\"\b\b\u0000\u0010\u000b*\u00020\f2\f\u0010\r\u001a\b\u0012\u0004\u0012\u0002H\u000b0\u000eH\u0016¢\u0006\u0002\u0010\u000fR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/SelectRecipientsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "profileId", "", "currentSelection", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "(JLjava/util/Set;)V", "getCurrentSelection", "()Ljava/util/Set;", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final Set<RecipientId> currentSelection;
        private final long profileId;

        /* JADX DEBUG: Multi-variable search result rejected for r4v0, resolved type: java.util.Set<? extends org.thoughtcrime.securesms.recipients.RecipientId> */
        /* JADX WARN: Multi-variable type inference failed */
        public Factory(long j, Set<? extends RecipientId> set) {
            Intrinsics.checkNotNullParameter(set, "currentSelection");
            this.profileId = j;
            this.currentSelection = set;
        }

        public final Set<RecipientId> getCurrentSelection() {
            return this.currentSelection;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new SelectRecipientsViewModel(this.profileId, this.currentSelection, new NotificationProfilesRepository()));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
