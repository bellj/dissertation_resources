package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: SoundsAndNotificationsSettingsState.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BA\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0007\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\t\u0012\b\b\u0002\u0010\u000b\u001a\u00020\t¢\u0006\u0002\u0010\fJ\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0007HÆ\u0003J\t\u0010\u001a\u001a\u00020\tHÆ\u0003J\t\u0010\u001b\u001a\u00020\tHÆ\u0003J\t\u0010\u001c\u001a\u00020\tHÆ\u0003JE\u0010\u001d\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\tHÆ\u0001J\u0013\u0010\u001e\u001a\u00020\t2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010 \u001a\u00020!HÖ\u0001J\t\u0010\"\u001a\u00020#HÖ\u0001R\u0011\u0010\u000b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u000eR\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsState;", "", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "muteUntil", "", "mentionSetting", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "hasCustomNotificationSettings", "", "hasMentionsSupport", "channelConsistencyCheckComplete", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;JLorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;ZZZ)V", "getChannelConsistencyCheckComplete", "()Z", "getHasCustomNotificationSettings", "getHasMentionsSupport", "getMentionSetting", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "getMuteUntil", "()J", "getRecipientId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SoundsAndNotificationsSettingsState {
    private final boolean channelConsistencyCheckComplete;
    private final boolean hasCustomNotificationSettings;
    private final boolean hasMentionsSupport;
    private final RecipientDatabase.MentionSetting mentionSetting;
    private final long muteUntil;
    private final RecipientId recipientId;

    public SoundsAndNotificationsSettingsState() {
        this(null, 0, null, false, false, false, 63, null);
    }

    public static /* synthetic */ SoundsAndNotificationsSettingsState copy$default(SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState, RecipientId recipientId, long j, RecipientDatabase.MentionSetting mentionSetting, boolean z, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            recipientId = soundsAndNotificationsSettingsState.recipientId;
        }
        if ((i & 2) != 0) {
            j = soundsAndNotificationsSettingsState.muteUntil;
        }
        if ((i & 4) != 0) {
            mentionSetting = soundsAndNotificationsSettingsState.mentionSetting;
        }
        if ((i & 8) != 0) {
            z = soundsAndNotificationsSettingsState.hasCustomNotificationSettings;
        }
        if ((i & 16) != 0) {
            z2 = soundsAndNotificationsSettingsState.hasMentionsSupport;
        }
        if ((i & 32) != 0) {
            z3 = soundsAndNotificationsSettingsState.channelConsistencyCheckComplete;
        }
        return soundsAndNotificationsSettingsState.copy(recipientId, j, mentionSetting, z, z2, z3);
    }

    public final RecipientId component1() {
        return this.recipientId;
    }

    public final long component2() {
        return this.muteUntil;
    }

    public final RecipientDatabase.MentionSetting component3() {
        return this.mentionSetting;
    }

    public final boolean component4() {
        return this.hasCustomNotificationSettings;
    }

    public final boolean component5() {
        return this.hasMentionsSupport;
    }

    public final boolean component6() {
        return this.channelConsistencyCheckComplete;
    }

    public final SoundsAndNotificationsSettingsState copy(RecipientId recipientId, long j, RecipientDatabase.MentionSetting mentionSetting, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        return new SoundsAndNotificationsSettingsState(recipientId, j, mentionSetting, z, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SoundsAndNotificationsSettingsState)) {
            return false;
        }
        SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState = (SoundsAndNotificationsSettingsState) obj;
        return Intrinsics.areEqual(this.recipientId, soundsAndNotificationsSettingsState.recipientId) && this.muteUntil == soundsAndNotificationsSettingsState.muteUntil && this.mentionSetting == soundsAndNotificationsSettingsState.mentionSetting && this.hasCustomNotificationSettings == soundsAndNotificationsSettingsState.hasCustomNotificationSettings && this.hasMentionsSupport == soundsAndNotificationsSettingsState.hasMentionsSupport && this.channelConsistencyCheckComplete == soundsAndNotificationsSettingsState.channelConsistencyCheckComplete;
    }

    public int hashCode() {
        int hashCode = ((((this.recipientId.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.muteUntil)) * 31) + this.mentionSetting.hashCode()) * 31;
        boolean z = this.hasCustomNotificationSettings;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.hasMentionsSupport;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.channelConsistencyCheckComplete;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i9 + i;
    }

    public String toString() {
        return "SoundsAndNotificationsSettingsState(recipientId=" + this.recipientId + ", muteUntil=" + this.muteUntil + ", mentionSetting=" + this.mentionSetting + ", hasCustomNotificationSettings=" + this.hasCustomNotificationSettings + ", hasMentionsSupport=" + this.hasMentionsSupport + ", channelConsistencyCheckComplete=" + this.channelConsistencyCheckComplete + ')';
    }

    public SoundsAndNotificationsSettingsState(RecipientId recipientId, long j, RecipientDatabase.MentionSetting mentionSetting, boolean z, boolean z2, boolean z3) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        this.recipientId = recipientId;
        this.muteUntil = j;
        this.mentionSetting = mentionSetting;
        this.hasCustomNotificationSettings = z;
        this.hasMentionsSupport = z2;
        this.channelConsistencyCheckComplete = z3;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ SoundsAndNotificationsSettingsState(org.thoughtcrime.securesms.recipients.RecipientId r9, long r10, org.thoughtcrime.securesms.database.RecipientDatabase.MentionSetting r12, boolean r13, boolean r14, boolean r15, int r16, kotlin.jvm.internal.DefaultConstructorMarker r17) {
        /*
            r8 = this;
            r0 = r16 & 1
            if (r0 == 0) goto L_0x0010
            org.thoughtcrime.securesms.recipients.Recipient r0 = org.thoughtcrime.securesms.recipients.Recipient.UNKNOWN
            org.thoughtcrime.securesms.recipients.RecipientId r0 = r0.getId()
            java.lang.String r1 = "UNKNOWN.id"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r0, r1)
            goto L_0x0011
        L_0x0010:
            r0 = r9
        L_0x0011:
            r1 = r16 & 2
            if (r1 == 0) goto L_0x0018
            r1 = 0
            goto L_0x0019
        L_0x0018:
            r1 = r10
        L_0x0019:
            r3 = r16 & 4
            if (r3 == 0) goto L_0x0020
            org.thoughtcrime.securesms.database.RecipientDatabase$MentionSetting r3 = org.thoughtcrime.securesms.database.RecipientDatabase.MentionSetting.DO_NOT_NOTIFY
            goto L_0x0021
        L_0x0020:
            r3 = r12
        L_0x0021:
            r4 = r16 & 8
            r5 = 0
            if (r4 == 0) goto L_0x0028
            r4 = 0
            goto L_0x0029
        L_0x0028:
            r4 = r13
        L_0x0029:
            r6 = r16 & 16
            if (r6 == 0) goto L_0x002f
            r6 = 0
            goto L_0x0030
        L_0x002f:
            r6 = r14
        L_0x0030:
            r7 = r16 & 32
            if (r7 == 0) goto L_0x0035
            goto L_0x0036
        L_0x0035:
            r5 = r15
        L_0x0036:
            r9 = r8
            r10 = r0
            r11 = r1
            r13 = r3
            r14 = r4
            r15 = r6
            r16 = r5
            r9.<init>(r10, r11, r13, r14, r15, r16)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsState.<init>(org.thoughtcrime.securesms.recipients.RecipientId, long, org.thoughtcrime.securesms.database.RecipientDatabase$MentionSetting, boolean, boolean, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final RecipientId getRecipientId() {
        return this.recipientId;
    }

    public final long getMuteUntil() {
        return this.muteUntil;
    }

    public final RecipientDatabase.MentionSetting getMentionSetting() {
        return this.mentionSetting;
    }

    public final boolean getHasCustomNotificationSettings() {
        return this.hasCustomNotificationSettings;
    }

    public final boolean getHasMentionsSupport() {
        return this.hasMentionsSupport;
    }

    public final boolean getChannelConsistencyCheckComplete() {
        return this.channelConsistencyCheckComplete;
    }
}
