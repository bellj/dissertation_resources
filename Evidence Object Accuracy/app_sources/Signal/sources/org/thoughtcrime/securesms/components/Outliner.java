package org.thoughtcrime.securesms.components;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class Outliner {
    private final RectF bounds = new RectF();
    private final Path corners = new Path();
    private final Paint outlinePaint;
    private final float[] radii = new float[8];

    public Outliner() {
        Paint paint = new Paint();
        this.outlinePaint = paint;
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth((float) ViewUtil.dpToPx(1));
        paint.setAntiAlias(true);
    }

    public void setColor(int i) {
        this.outlinePaint.setColor(i);
    }

    public void setStrokeWidth(float f) {
        this.outlinePaint.setStrokeWidth(f);
    }

    public void setAlpha(int i) {
        this.outlinePaint.setAlpha(i);
    }

    public void draw(Canvas canvas) {
        draw(canvas, 0, canvas.getWidth(), canvas.getHeight(), 0);
    }

    public void draw(Canvas canvas, int i, int i2, int i3, int i4) {
        float strokeWidth = this.outlinePaint.getStrokeWidth() / 2.0f;
        RectF rectF = this.bounds;
        rectF.left = ((float) i4) + strokeWidth;
        rectF.top = ((float) i) + strokeWidth;
        rectF.right = ((float) i2) - strokeWidth;
        rectF.bottom = ((float) i3) - strokeWidth;
        this.corners.reset();
        this.corners.addRoundRect(this.bounds, this.radii, Path.Direction.CW);
        canvas.drawPath(this.corners, this.outlinePaint);
    }

    public void setRadius(int i) {
        setRadii(i, i, i, i);
    }

    public void setRadii(int i, int i2, int i3, int i4) {
        float[] fArr = this.radii;
        float f = (float) i;
        fArr[1] = f;
        fArr[0] = f;
        float f2 = (float) i2;
        fArr[3] = f2;
        fArr[2] = f2;
        float f3 = (float) i3;
        fArr[5] = f3;
        fArr[4] = f3;
        float f4 = (float) i4;
        fArr[7] = f4;
        fArr[6] = f4;
    }
}
