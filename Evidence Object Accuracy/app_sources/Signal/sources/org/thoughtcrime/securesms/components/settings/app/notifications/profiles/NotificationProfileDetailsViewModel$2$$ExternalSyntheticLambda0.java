package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileDetailsViewModel$2$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ NotificationProfileDetailsViewModel.State f$0;

    public /* synthetic */ NotificationProfileDetailsViewModel$2$$ExternalSyntheticLambda0(NotificationProfileDetailsViewModel.State state) {
        this.f$0 = state;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return NotificationProfileDetailsViewModel.AnonymousClass2.m773invoke$lambda0(this.f$0, (NotificationProfileDetailsViewModel.State) obj);
    }
}
