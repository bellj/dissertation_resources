package org.thoughtcrime.securesms.components.webrtc;

import org.thoughtcrime.securesms.components.webrtc.PictureInPictureExpansionHelper;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class PictureInPictureExpansionHelper$$ExternalSyntheticLambda1 implements Runnable {
    public final /* synthetic */ PictureInPictureExpansionHelper.Callback f$0;

    public /* synthetic */ PictureInPictureExpansionHelper$$ExternalSyntheticLambda1(PictureInPictureExpansionHelper.Callback callback) {
        this.f$0 = callback;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.onAnimationHasFinished();
    }
}
