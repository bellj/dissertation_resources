package org.thoughtcrime.securesms.components.camera;

import android.app.Activity;
import android.hardware.Camera;
import android.util.DisplayMetrics;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import org.signal.core.util.logging.Log;

/* loaded from: classes4.dex */
public class CameraUtils {
    private static final String TAG = Log.tag(CameraUtils.class);

    public static Camera.Size getPreferredPreviewSize(int i, int i2, int i3, Camera.Parameters parameters) {
        int i4 = i % SubsamplingScaleImageView.ORIENTATION_180;
        int i5 = i4 == 90 ? i3 : i2;
        int i6 = i4 == 90 ? i2 : i3;
        double d = (double) i5;
        double d2 = (double) i6;
        Double.isNaN(d);
        Double.isNaN(d2);
        double d3 = d / d2;
        char c = 0;
        char c2 = 1;
        char c3 = 2;
        int i7 = 3;
        Log.d(TAG, String.format(Locale.US, "getPreferredPreviewSize(%d, %d, %d) -> target %dx%d, AR %.02f", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i5), Integer.valueOf(i6), Double.valueOf(d3)));
        List<Camera.Size> supportedPreviewSizes = parameters.getSupportedPreviewSizes();
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        for (Camera.Size size : supportedPreviewSizes) {
            String str = TAG;
            Locale locale = Locale.US;
            Object[] objArr = new Object[i7];
            objArr[c] = Integer.valueOf(size.width);
            objArr[c2] = Integer.valueOf(size.height);
            objArr[c3] = Float.valueOf(((float) size.width) / ((float) size.height));
            Log.d(str, String.format(locale, "  %dx%d (%.02f)", objArr));
            int i8 = size.height;
            int i9 = size.width;
            double d4 = (double) i9;
            Double.isNaN(d4);
            if (((double) i8) == d4 * d3 && i8 >= i6 && i9 >= i5) {
                linkedList.add(size);
                Log.d(str, "    (ideal ratio)");
            } else if (i9 >= i5 && i8 >= i6) {
                linkedList2.add(size);
                Log.d(str, "    (good size, suboptimal ratio)");
            }
            c = 0;
            c2 = 1;
            c3 = 2;
            i7 = 3;
        }
        if (!linkedList.isEmpty()) {
            return (Camera.Size) Collections.min(linkedList, new AreaComparator());
        }
        if (!linkedList2.isEmpty()) {
            return (Camera.Size) Collections.min(linkedList2, new AspectRatioComparator(d3));
        }
        return (Camera.Size) Collections.max(supportedPreviewSizes, new AreaComparator());
    }

    public static int getCameraDisplayOrientation(Activity activity, Camera.CameraInfo cameraInfo) {
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        activity.getWindowManager().getDefaultDisplay().getMetrics(new DisplayMetrics());
        int i = 0;
        if (rotation != 0) {
            if (rotation == 1) {
                i = 90;
            } else if (rotation == 2) {
                i = SubsamplingScaleImageView.ORIENTATION_180;
            } else if (rotation == 3) {
                i = SubsamplingScaleImageView.ORIENTATION_270;
            }
        }
        if (cameraInfo.facing == 1) {
            return (360 - ((cameraInfo.orientation + i) % 360)) % 360;
        }
        return ((cameraInfo.orientation - i) + 360) % 360;
    }

    /* loaded from: classes4.dex */
    public static class AreaComparator implements Comparator<Camera.Size> {
        private AreaComparator() {
        }

        public int compare(Camera.Size size, Camera.Size size2) {
            return Long.signum((long) ((size.width * size.height) - (size2.width * size2.height)));
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class AspectRatioComparator extends AreaComparator {
        private final double target;

        public AspectRatioComparator(double d) {
            super();
            this.target = d;
        }

        @Override // org.thoughtcrime.securesms.components.camera.CameraUtils.AreaComparator
        public int compare(Camera.Size size, Camera.Size size2) {
            double d = this.target;
            double d2 = (double) size.width;
            double d3 = (double) size.height;
            Double.isNaN(d2);
            Double.isNaN(d3);
            double abs = Math.abs(d - (d2 / d3));
            double d4 = this.target;
            double d5 = (double) size2.width;
            double d6 = (double) size2.height;
            Double.isNaN(d5);
            Double.isNaN(d6);
            double abs2 = Math.abs(d4 - (d5 / d6));
            if (abs < abs2) {
                return -1;
            }
            if (abs > abs2) {
                return 1;
            }
            return super.compare(size, size2);
        }
    }
}
