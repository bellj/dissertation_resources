package org.thoughtcrime.securesms.components.settings.app.privacy;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.database.model.DistributionListId;

/* loaded from: classes4.dex */
public class PrivacySettingsFragmentDirections {
    private PrivacySettingsFragmentDirections() {
    }

    public static NavDirections actionPrivacySettingsFragmentToBlockedUsersActivity() {
        return new ActionOnlyNavDirections(R.id.action_privacySettingsFragment_to_blockedUsersActivity);
    }

    public static NavDirections actionPrivacySettingsFragmentToAdvancedPrivacySettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_privacySettingsFragment_to_advancedPrivacySettingsFragment);
    }

    public static NavDirections actionPrivacySettingsFragmentToDisappearingMessagesTimerSelectFragment() {
        return new ActionOnlyNavDirections(R.id.action_privacySettingsFragment_to_disappearingMessagesTimerSelectFragment);
    }

    public static NavDirections actionPrivacySettingsToMyStorySettings() {
        return new ActionOnlyNavDirections(R.id.action_privacySettings_to_myStorySettings);
    }

    public static NavDirections actionPrivacySettingsToNewPrivateStory() {
        return new ActionOnlyNavDirections(R.id.action_privacySettings_to_newPrivateStory);
    }

    public static ActionPrivacySettingsToPrivateStorySettings actionPrivacySettingsToPrivateStorySettings(DistributionListId distributionListId) {
        return new ActionPrivacySettingsToPrivateStorySettings(distributionListId);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionPrivacySettingsToPrivateStorySettings implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_privacySettings_to_privateStorySettings;
        }

        private ActionPrivacySettingsToPrivateStorySettings(DistributionListId distributionListId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (distributionListId != null) {
                hashMap.put("private_story", distributionListId);
                return;
            }
            throw new IllegalArgumentException("Argument \"private_story\" is marked as non-null but was passed a null value.");
        }

        public ActionPrivacySettingsToPrivateStorySettings setPrivateStory(DistributionListId distributionListId) {
            if (distributionListId != null) {
                this.arguments.put("private_story", distributionListId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"private_story\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("private_story")) {
                DistributionListId distributionListId = (DistributionListId) this.arguments.get("private_story");
                if (Parcelable.class.isAssignableFrom(DistributionListId.class) || distributionListId == null) {
                    bundle.putParcelable("private_story", (Parcelable) Parcelable.class.cast(distributionListId));
                } else if (Serializable.class.isAssignableFrom(DistributionListId.class)) {
                    bundle.putSerializable("private_story", (Serializable) Serializable.class.cast(distributionListId));
                } else {
                    throw new UnsupportedOperationException(DistributionListId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public DistributionListId getPrivateStory() {
            return (DistributionListId) this.arguments.get("private_story");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionPrivacySettingsToPrivateStorySettings actionPrivacySettingsToPrivateStorySettings = (ActionPrivacySettingsToPrivateStorySettings) obj;
            if (this.arguments.containsKey("private_story") != actionPrivacySettingsToPrivateStorySettings.arguments.containsKey("private_story")) {
                return false;
            }
            if (getPrivateStory() == null ? actionPrivacySettingsToPrivateStorySettings.getPrivateStory() == null : getPrivateStory().equals(actionPrivacySettingsToPrivateStorySettings.getPrivateStory())) {
                return getActionId() == actionPrivacySettingsToPrivateStorySettings.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getPrivateStory() != null ? getPrivateStory().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionPrivacySettingsToPrivateStorySettings(actionId=" + getActionId() + "){privateStory=" + getPrivateStory() + "}";
        }
    }
}
