package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import j$.time.DayOfWeek;
import j$.time.LocalTime;
import j$.time.format.DateTimeFormatter;
import java.util.Map;
import kotlin.Metadata;
import kotlin.TuplesKt;
import kotlin.collections.MapsKt__MapsKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.JavaTimeExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: EditNotificationProfileScheduleFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000\u001c\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\u001a\u0014\u0010\u0004\u001a\u00020\u0003*\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0001H\u0002\" \u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00058\u0002X\u0004¢\u0006\u0006\n\u0004\b\b\u0010\t¨\u0006\n"}, d2 = {"j$/time/LocalTime", "Landroid/content/Context;", "context", "Landroid/text/SpannableString;", "formatTime", "", "j$/time/DayOfWeek", "", "DAY_TO_STARTING_LETTER", "Ljava/util/Map;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EditNotificationProfileScheduleFragmentKt {
    private static final Map<DayOfWeek, Integer> DAY_TO_STARTING_LETTER = MapsKt__MapsKt.mapOf(TuplesKt.to(DayOfWeek.SUNDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__sunday_first_letter)), TuplesKt.to(DayOfWeek.MONDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__monday_first_letter)), TuplesKt.to(DayOfWeek.TUESDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__tuesday_first_letter)), TuplesKt.to(DayOfWeek.WEDNESDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__wednesday_first_letter)), TuplesKt.to(DayOfWeek.THURSDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__thursday_first_letter)), TuplesKt.to(DayOfWeek.FRIDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__friday_first_letter)), TuplesKt.to(DayOfWeek.SATURDAY, Integer.valueOf((int) R.string.EditNotificationProfileSchedule__saturday_first_letter)));

    public static final SpannableString formatTime(LocalTime localTime, Context context) {
        String format = DateTimeFormatter.ofPattern("a").format(localTime);
        String formatHours = JavaTimeExtensionsKt.formatHours(localTime, context);
        SpannableString spannableString = new SpannableString(formatHours);
        Intrinsics.checkNotNullExpressionValue(format, "amPm");
        int i = StringsKt__StringsKt.indexOf$default((CharSequence) formatHours, format, 0, true, 2, (Object) null);
        if (i != -1) {
            spannableString.setSpan(new AbsoluteSizeSpan(ViewUtil.spToPx(20.0f)), i, format.length() + i, 33);
        }
        return spannableString;
    }
}
