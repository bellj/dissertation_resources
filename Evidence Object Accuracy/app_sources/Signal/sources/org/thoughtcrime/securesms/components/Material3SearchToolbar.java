package org.thoughtcrime.securesms.components;

import android.animation.Animator;
import android.content.Context;
import android.graphics.PointF;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.constraintlayout.widget.ConstraintLayout;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import kotlin.text.StringsKt__StringsJVMKt;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.Material3SearchToolbar;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: Material3SearchToolbar.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u0007\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0017B\u001b\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u0011\u001a\u00020\u0012J\u0016\u0010\u0013\u001a\u00020\u00122\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0015R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/Material3SearchToolbar;", "Landroidx/constraintlayout/widget/ConstraintLayout;", "context", "Landroid/content/Context;", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "circularRevealPoint", "Landroid/graphics/PointF;", "input", "Landroid/widget/EditText;", "listener", "Lorg/thoughtcrime/securesms/components/Material3SearchToolbar$Listener;", "getListener", "()Lorg/thoughtcrime/securesms/components/Material3SearchToolbar$Listener;", "setListener", "(Lorg/thoughtcrime/securesms/components/Material3SearchToolbar$Listener;)V", "collapse", "", "display", "x", "", "y", "Listener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class Material3SearchToolbar extends ConstraintLayout {
    private final PointF circularRevealPoint;
    private final EditText input;
    private Listener listener;

    /* compiled from: Material3SearchToolbar.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&J\u0010\u0010\u0004\u001a\u00020\u00032\u0006\u0010\u0005\u001a\u00020\u0006H&¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/Material3SearchToolbar$Listener;", "", "onSearchClosed", "", "onSearchTextChange", DraftDatabase.Draft.TEXT, "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Listener {
        void onSearchClosed();

        void onSearchTextChange(String str);
    }

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public Material3SearchToolbar(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    public /* synthetic */ Material3SearchToolbar(Context context, AttributeSet attributeSet, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(context, (i & 2) != 0 ? null : attributeSet);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public Material3SearchToolbar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        this.circularRevealPoint = new PointF();
        ViewGroup.inflate(context, R.layout.material3_serarch_toolbar, this);
        View findViewById = findViewById(R.id.search_input);
        Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.search_input)");
        EditText editText = (EditText) findViewById;
        this.input = editText;
        View findViewById2 = findViewById(R.id.search_close);
        View findViewById3 = findViewById(R.id.search_clear);
        findViewById2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.Material3SearchToolbar$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Material3SearchToolbar.$r8$lambda$UwIQRx8FYust8BhIXYkCLg_Ekys(Material3SearchToolbar.this, view);
            }
        });
        findViewById3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.Material3SearchToolbar$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                Material3SearchToolbar.$r8$lambda$duLJbDWh3OlHjU4eHqDVEt7526w(Material3SearchToolbar.this, view);
            }
        });
        editText.addTextChangedListener(new TextWatcher(findViewById3, this) { // from class: org.thoughtcrime.securesms.components.Material3SearchToolbar$special$$inlined$addTextChangedListener$default$1
            final /* synthetic */ View $clear$inlined;
            final /* synthetic */ Material3SearchToolbar this$0;

            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            {
                this.$clear$inlined = r1;
                this.this$0 = r2;
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                String str;
                Intrinsics.checkNotNullExpressionValue(this.$clear$inlined, "clear");
                ViewExtensionsKt.setVisible(this.$clear$inlined, true ^ (editable == null || (StringsKt__StringsJVMKt.isBlank(editable))));
                Material3SearchToolbar.Listener listener = this.this$0.getListener();
                if (listener != null) {
                    if (editable == null || (str = editable.toString()) == null) {
                        str = "";
                    }
                    listener.onSearchTextChange(str);
                }
            }
        });
    }

    public final Listener getListener() {
        return this.listener;
    }

    public final void setListener(Listener listener) {
        this.listener = listener;
    }

    /* renamed from: _init_$lambda-0 */
    public static final void m483_init_$lambda0(Material3SearchToolbar material3SearchToolbar, View view) {
        Intrinsics.checkNotNullParameter(material3SearchToolbar, "this$0");
        material3SearchToolbar.collapse();
    }

    /* renamed from: _init_$lambda-1 */
    public static final void m484_init_$lambda1(Material3SearchToolbar material3SearchToolbar, View view) {
        Intrinsics.checkNotNullParameter(material3SearchToolbar, "this$0");
        material3SearchToolbar.input.setText("");
    }

    public final void display(float f, float f2) {
        if (Build.VERSION.SDK_INT < 21) {
            setVisibility(0);
            ViewUtil.focusAndShowKeyboard(this.input);
        } else if (!ViewExtensionsKt.getVisible(this)) {
            this.circularRevealPoint.set(f, f2);
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this, (int) f, (int) f2, 0.0f, (float) getWidth());
            createCircularReveal.setDuration(400);
            setVisibility(0);
            ViewUtil.focusAndShowKeyboard(this.input);
            createCircularReveal.start();
        }
    }

    public final void collapse() {
        if (getVisibility() == 0) {
            Listener listener = this.listener;
            if (listener != null) {
                listener.onSearchClosed();
            }
            ViewUtil.hideKeyboard(getContext(), this.input);
            if (Build.VERSION.SDK_INT >= 21) {
                PointF pointF = this.circularRevealPoint;
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this, (int) pointF.x, (int) pointF.y, (float) getWidth(), 0.0f);
                createCircularReveal.setDuration(400);
                Intrinsics.checkNotNullExpressionValue(createCircularReveal, "animator");
                createCircularReveal.addListener(new Animator.AnimatorListener(this) { // from class: org.thoughtcrime.securesms.components.Material3SearchToolbar$collapse$$inlined$addListener$default$1
                    final /* synthetic */ Material3SearchToolbar this$0;

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationCancel(Animator animator) {
                        Intrinsics.checkNotNullParameter(animator, "animator");
                    }

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationRepeat(Animator animator) {
                        Intrinsics.checkNotNullParameter(animator, "animator");
                    }

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationStart(Animator animator) {
                        Intrinsics.checkNotNullParameter(animator, "animator");
                    }

                    {
                        this.this$0 = r1;
                    }

                    @Override // android.animation.Animator.AnimatorListener
                    public void onAnimationEnd(Animator animator) {
                        Intrinsics.checkNotNullParameter(animator, "animator");
                        this.this$0.setVisibility(4);
                    }
                });
                createCircularReveal.start();
                return;
            }
            setVisibility(4);
        }
    }
}
