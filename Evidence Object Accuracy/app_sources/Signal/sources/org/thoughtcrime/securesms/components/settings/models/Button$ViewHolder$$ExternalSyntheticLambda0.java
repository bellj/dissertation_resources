package org.thoughtcrime.securesms.components.settings.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.models.Button;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class Button$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ Button.Model f$0;

    public /* synthetic */ Button$ViewHolder$$ExternalSyntheticLambda0(Button.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        Button.ViewHolder.m1252bind$lambda0(this.f$0, view);
    }
}
