package org.thoughtcrime.securesms.components.voice;

import kotlin.Metadata;
import org.signal.core.util.logging.Log;

/* compiled from: VoiceNoteProximityWakeLockManager.kt */
@Metadata(d1 = {"\u0000\u0010\n\u0000\n\u0002\u0010\u0007\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\"\u000e\u0010\u0000\u001a\u00020\u0001XT¢\u0006\u0002\n\u0000\"\u0016\u0010\u0002\u001a\n \u0004*\u0004\u0018\u00010\u00030\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"PROXIMITY_THRESHOLD", "", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNoteProximityWakeLockManagerKt {
    private static final float PROXIMITY_THRESHOLD;
    private static final String TAG = Log.tag(VoiceNoteProximityWakeLockManager.class);
}
