package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/SectionHeaderPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;)V", "getTitle", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SectionHeaderPreference extends PreferenceModel<SectionHeaderPreference> {
    private final DSLSettingsText title;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SectionHeaderPreference(DSLSettingsText dSLSettingsText) {
        super(null, null, null, null, false, 31, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        this.title = dSLSettingsText;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }
}
