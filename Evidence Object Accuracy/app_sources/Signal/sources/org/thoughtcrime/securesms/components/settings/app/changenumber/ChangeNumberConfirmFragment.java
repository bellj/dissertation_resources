package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.LoggingFragment;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberConfirmFragment.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberConfirmFragment;", "Lorg/thoughtcrime/securesms/LoggingFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel;", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberConfirmFragment extends LoggingFragment {
    private ChangeNumberViewModel viewModel;

    public ChangeNumberConfirmFragment() {
        super(R.layout.fragment_change_number_confirm);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        this.viewModel = ChangeNumberUtil.getViewModel(this);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        toolbar.setTitle(R.string.ChangeNumberEnterPhoneNumberFragment__change_number);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberConfirmFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberConfirmFragment.$r8$lambda$nb3Rav2ryVpaxTD9wbVZLZyRAgQ(ChangeNumberConfirmFragment.this, view2);
            }
        });
        View findViewById2 = view.findViewById(R.id.change_number_confirm_new_number_message);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.c…nfirm_new_number_message)");
        TextView textView = (TextView) findViewById2;
        Object[] objArr = new Object[2];
        ChangeNumberViewModel changeNumberViewModel = this.viewModel;
        ChangeNumberViewModel changeNumberViewModel2 = null;
        if (changeNumberViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel = null;
        }
        objArr[0] = changeNumberViewModel.getOldNumberState().getFullFormattedNumber();
        ChangeNumberViewModel changeNumberViewModel3 = this.viewModel;
        if (changeNumberViewModel3 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            changeNumberViewModel3 = null;
        }
        objArr[1] = changeNumberViewModel3.getNumber().getFullFormattedNumber();
        textView.setText(getString(R.string.ChangeNumberConfirmFragment__you_are_about_to_change_your_phone_number_from_s_to_s, objArr));
        View findViewById3 = view.findViewById(R.id.change_number_confirm_new_number);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.c…umber_confirm_new_number)");
        TextView textView2 = (TextView) findViewById3;
        ChangeNumberViewModel changeNumberViewModel4 = this.viewModel;
        if (changeNumberViewModel4 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        } else {
            changeNumberViewModel2 = changeNumberViewModel4;
        }
        textView2.setText(changeNumberViewModel2.getNumber().getFullFormattedNumber());
        View findViewById4 = view.findViewById(R.id.change_number_confirm_edit_number);
        Intrinsics.checkNotNullExpressionValue(findViewById4, "view.findViewById(R.id.c…mber_confirm_edit_number)");
        findViewById4.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberConfirmFragment$$ExternalSyntheticLambda1
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberConfirmFragment.$r8$lambda$tPzOgHlWzqv6_SqeH3a3hyJeiNs(ChangeNumberConfirmFragment.this, view2);
            }
        });
        View findViewById5 = view.findViewById(R.id.change_number_confirm_change_number);
        Intrinsics.checkNotNullExpressionValue(findViewById5, "view.findViewById(R.id.c…er_confirm_change_number)");
        findViewById5.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberConfirmFragment$$ExternalSyntheticLambda2
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberConfirmFragment.$r8$lambda$pBcpFpQxv9nVv9iquVGBA6xd61c(ChangeNumberConfirmFragment.this, view2);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m579onViewCreated$lambda0(ChangeNumberConfirmFragment changeNumberConfirmFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberConfirmFragment, "this$0");
        FragmentKt.findNavController(changeNumberConfirmFragment).navigateUp();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m580onViewCreated$lambda1(ChangeNumberConfirmFragment changeNumberConfirmFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberConfirmFragment, "this$0");
        FragmentKt.findNavController(changeNumberConfirmFragment).navigateUp();
    }

    /* renamed from: onViewCreated$lambda-2 */
    public static final void m581onViewCreated$lambda2(ChangeNumberConfirmFragment changeNumberConfirmFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberConfirmFragment, "this$0");
        SafeNavigation.safeNavigate(FragmentKt.findNavController(changeNumberConfirmFragment), (int) R.id.action_changePhoneNumberConfirmFragment_to_changePhoneNumberVerifyFragment);
    }
}
