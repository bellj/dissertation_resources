package org.thoughtcrime.securesms.components.settings.app.notifications.manual.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileSelection$NewViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfileSelection.New f$0;

    public /* synthetic */ NotificationProfileSelection$NewViewHolder$$ExternalSyntheticLambda0(NotificationProfileSelection.New r1) {
        this.f$0 = r1;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfileSelection.NewViewHolder.m726bind$lambda0(this.f$0, view);
    }
}
