package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import io.reactivex.rxjava3.subjects.BehaviorSubject;
import j$.time.DayOfWeek;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleViewModel;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfileSchedule;

/* compiled from: EditNotificationProfileScheduleViewModel.kt */
@Metadata(bv = {}, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u00002\u00020\u0001:\u0002'(B\u001f\u0012\u0006\u0010\u0017\u001a\u00020\u0016\u0012\u0006\u0010\u0012\u001a\u00020\u000f\u0012\u0006\u0010\u001b\u001a\u00020\u001a¢\u0006\u0004\b%\u0010&J\b\u0010\u0003\u001a\u00020\u0002H\u0014J\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004J\u000e\u0010\t\u001a\u00020\u00022\u0006\u0010\b\u001a\u00020\u0007J\u0016\u0010\r\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\nJ\u0016\u0010\u000e\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\n2\u0006\u0010\f\u001a\u00020\nJ\u000e\u0010\u0011\u001a\u00020\u00022\u0006\u0010\u0010\u001a\u00020\u000fJ\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\u0006\u0010\u0012\u001a\u00020\u000fR\u0014\u0010\u0017\u001a\u00020\u00168\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0017\u0010\u0018R\u0014\u0010\u0012\u001a\u00020\u000f8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u0012\u0010\u0019R\u0014\u0010\u001b\u001a\u00020\u001a8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001b\u0010\u001cR\u0014\u0010\u001e\u001a\u00020\u001d8\u0002X\u0004¢\u0006\u0006\n\u0004\b\u001e\u0010\u001fR\u001a\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00050 8\u0002X\u0004¢\u0006\u0006\n\u0004\b!\u0010\"R\u0014\u0010\u0006\u001a\u00020\u00058BX\u0004¢\u0006\u0006\u001a\u0004\b#\u0010$¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleViewModel;", "Landroidx/lifecycle/ViewModel;", "", "onCleared", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "schedule", "j$/time/DayOfWeek", "day", "toggleDay", "", "hour", "minute", "setStartTime", "setEndTime", "", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "setEnabled", "createMode", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleViewModel$SaveScheduleResult;", "save", "", "profileId", "J", "Z", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "Lio/reactivex/rxjava3/subjects/BehaviorSubject;", "scheduleSubject", "Lio/reactivex/rxjava3/subjects/BehaviorSubject;", "getSchedule", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfileSchedule;", "<init>", "(JZLorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", "Factory", "SaveScheduleResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class EditNotificationProfileScheduleViewModel extends ViewModel {
    private final boolean createMode;
    private final CompositeDisposable disposables;
    private final long profileId;
    private final NotificationProfilesRepository repository;
    private final BehaviorSubject<NotificationProfileSchedule> scheduleSubject;

    /* compiled from: EditNotificationProfileScheduleViewModel.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleViewModel$SaveScheduleResult;", "", "(Ljava/lang/String;I)V", "NoDaysSelected", "Success", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum SaveScheduleResult {
        NoDaysSelected,
        Success
    }

    public EditNotificationProfileScheduleViewModel(long j, boolean z, NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "repository");
        this.profileId = j;
        this.createMode = z;
        this.repository = notificationProfilesRepository;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        BehaviorSubject<NotificationProfileSchedule> create = BehaviorSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.scheduleSubject = create;
        Single singleOrError = notificationProfilesRepository.getProfile(j).take(1).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return EditNotificationProfileScheduleViewModel.m751_init_$lambda0((NotificationProfile) obj);
            }
        }).singleOrError();
        Intrinsics.checkNotNullExpressionValue(singleOrError, "repository.getProfile(pr… }\n      .singleOrError()");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(singleOrError, (Function1) null, new Function1<NotificationProfileSchedule, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleViewModel.2
            final /* synthetic */ EditNotificationProfileScheduleViewModel this$0;

            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(NotificationProfileSchedule notificationProfileSchedule) {
                invoke(notificationProfileSchedule);
                return Unit.INSTANCE;
            }

            public final void invoke(NotificationProfileSchedule notificationProfileSchedule) {
                this.this$0.scheduleSubject.onNext(notificationProfileSchedule);
            }
        }, 1, (Object) null));
    }

    private final NotificationProfileSchedule getSchedule() {
        NotificationProfileSchedule value = this.scheduleSubject.getValue();
        Intrinsics.checkNotNull(value);
        return value;
    }

    /* renamed from: _init_$lambda-0 */
    public static final NotificationProfileSchedule m751_init_$lambda0(NotificationProfile notificationProfile) {
        return notificationProfile.getSchedule();
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        super.onCleared();
        this.disposables.dispose();
    }

    public final Observable<NotificationProfileSchedule> schedule() {
        Observable<NotificationProfileSchedule> observeOn = this.scheduleSubject.subscribeOn(AndroidSchedulers.mainThread()).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "scheduleSubject.subscrib…dSchedulers.mainThread())");
        return observeOn;
    }

    public final void toggleDay(DayOfWeek dayOfWeek) {
        Intrinsics.checkNotNullParameter(dayOfWeek, "day");
        Set set = CollectionsKt___CollectionsKt.toMutableSet(getSchedule().getDaysEnabled());
        if (set.contains(dayOfWeek)) {
            set.remove(dayOfWeek);
        } else {
            set.add(dayOfWeek);
        }
        this.scheduleSubject.onNext(NotificationProfileSchedule.copy$default(getSchedule(), 0, false, 0, 0, set, 15, null));
    }

    public final void setStartTime(int i, int i2) {
        this.scheduleSubject.onNext(NotificationProfileSchedule.copy$default(getSchedule(), 0, false, (i * 100) + i2, 0, null, 27, null));
    }

    public final void setEndTime(int i, int i2) {
        if (i == 0) {
            i = 24;
        }
        this.scheduleSubject.onNext(NotificationProfileSchedule.copy$default(getSchedule(), 0, false, 0, (i * 100) + i2, null, 23, null));
    }

    public final void setEnabled(boolean z) {
        this.scheduleSubject.onNext(NotificationProfileSchedule.copy$default(getSchedule(), 0, z, 0, 0, null, 29, null));
    }

    public final Single<SaveScheduleResult> save(boolean z) {
        Single single;
        if (getSchedule().getEnabled() && getSchedule().getDaysEnabled().isEmpty()) {
            single = Single.just(SaveScheduleResult.NoDaysSelected);
            Intrinsics.checkNotNullExpressionValue(single, "{\n      Single.just(Save…ult.NoDaysSelected)\n    }");
        } else if (!z || getSchedule().getEnabled()) {
            single = this.repository.updateSchedule(getSchedule()).toSingleDefault(SaveScheduleResult.Success).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleViewModel$$ExternalSyntheticLambda1
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return EditNotificationProfileScheduleViewModel.m752save$lambda1(EditNotificationProfileScheduleViewModel.this, (EditNotificationProfileScheduleViewModel.SaveScheduleResult) obj);
                }
            });
            Intrinsics.checkNotNullExpressionValue(single, "{\n      repository.updat…        }\n        }\n    }");
        } else {
            single = Single.just(SaveScheduleResult.Success);
            Intrinsics.checkNotNullExpressionValue(single, "{\n      Single.just(Save…duleResult.Success)\n    }");
        }
        Single<SaveScheduleResult> observeOn = single.observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "result.observeOn(AndroidSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: save$lambda-1 */
    public static final SingleSource m752save$lambda1(EditNotificationProfileScheduleViewModel editNotificationProfileScheduleViewModel, SaveScheduleResult saveScheduleResult) {
        Intrinsics.checkNotNullParameter(editNotificationProfileScheduleViewModel, "this$0");
        if (!editNotificationProfileScheduleViewModel.getSchedule().getEnabled() || !NotificationProfileSchedule.coversTime$default(editNotificationProfileScheduleViewModel.getSchedule(), System.currentTimeMillis(), null, 2, null)) {
            return Single.just(saveScheduleResult);
        }
        return NotificationProfilesRepository.manuallyEnableProfileForSchedule$default(editNotificationProfileScheduleViewModel.repository, editNotificationProfileScheduleViewModel.profileId, editNotificationProfileScheduleViewModel.getSchedule(), 0, 4, null).toSingleDefault(saveScheduleResult);
    }

    /* compiled from: EditNotificationProfileScheduleViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileScheduleViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "profileId", "", "createMode", "", "(JZ)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final boolean createMode;
        private final long profileId;

        public Factory(long j, boolean z) {
            this.profileId = j;
            this.createMode = z;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new EditNotificationProfileScheduleViewModel(this.profileId, this.createMode, new NotificationProfilesRepository()));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
