package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import androidx.navigation.NavDirections;
import java.util.Arrays;
import java.util.HashMap;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class NotificationProfileDetailsFragmentDirections {
    private NotificationProfileDetailsFragmentDirections() {
    }

    public static ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment actionNotificationProfileDetailsFragmentToSelectRecipientsFragment(long j) {
        return new ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment(j);
    }

    public static ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment actionNotificationProfileDetailsFragmentToEditNotificationProfileFragment() {
        return new ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment();
    }

    public static ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment(long j, boolean z) {
        return new ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment(j, z);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }

    /* loaded from: classes4.dex */
    public static class ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_notificationProfileDetailsFragment_to_selectRecipientsFragment;
        }

        private ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment setCurrentSelection(RecipientId[] recipientIdArr) {
            this.arguments.put("currentSelection", recipientIdArr);
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            if (this.arguments.containsKey("currentSelection")) {
                bundle.putParcelableArray("currentSelection", (RecipientId[]) this.arguments.get("currentSelection"));
            } else {
                bundle.putParcelableArray("currentSelection", null);
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public RecipientId[] getCurrentSelection() {
            return (RecipientId[]) this.arguments.get("currentSelection");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment actionNotificationProfileDetailsFragmentToSelectRecipientsFragment = (ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment) obj;
            if (this.arguments.containsKey("profileId") != actionNotificationProfileDetailsFragmentToSelectRecipientsFragment.arguments.containsKey("profileId") || getProfileId() != actionNotificationProfileDetailsFragmentToSelectRecipientsFragment.getProfileId() || this.arguments.containsKey("currentSelection") != actionNotificationProfileDetailsFragmentToSelectRecipientsFragment.arguments.containsKey("currentSelection")) {
                return false;
            }
            if (getCurrentSelection() == null ? actionNotificationProfileDetailsFragmentToSelectRecipientsFragment.getCurrentSelection() == null : getCurrentSelection().equals(actionNotificationProfileDetailsFragmentToSelectRecipientsFragment.getCurrentSelection())) {
                return getActionId() == actionNotificationProfileDetailsFragmentToSelectRecipientsFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return ((((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + Arrays.hashCode(getCurrentSelection())) * 31) + getActionId();
        }

        public String toString() {
            return "ActionNotificationProfileDetailsFragmentToSelectRecipientsFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + ", currentSelection=" + getCurrentSelection() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_notificationProfileDetailsFragment_to_editNotificationProfileFragment;
        }

        private ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment() {
            this.arguments = new HashMap();
        }

        public ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            } else {
                bundle.putLong("profileId", -1);
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment actionNotificationProfileDetailsFragmentToEditNotificationProfileFragment = (ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment) obj;
            return this.arguments.containsKey("profileId") == actionNotificationProfileDetailsFragmentToEditNotificationProfileFragment.arguments.containsKey("profileId") && getProfileId() == actionNotificationProfileDetailsFragmentToEditNotificationProfileFragment.getProfileId() && getActionId() == actionNotificationProfileDetailsFragmentToEditNotificationProfileFragment.getActionId();
        }

        public int hashCode() {
            return ((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionNotificationProfileDetailsFragmentToEditNotificationProfileFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + "}";
        }
    }

    /* loaded from: classes4.dex */
    public static class ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_notificationProfileDetailsFragment_to_editNotificationProfileScheduleFragment;
        }

        private ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment(long j, boolean z) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
            hashMap.put("createMode", Boolean.valueOf(z));
        }

        public ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment setCreateMode(boolean z) {
            this.arguments.put("createMode", Boolean.valueOf(z));
            return this;
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("profileId")) {
                bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
            }
            if (this.arguments.containsKey("createMode")) {
                bundle.putBoolean("createMode", ((Boolean) this.arguments.get("createMode")).booleanValue());
            }
            return bundle;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }

        public boolean getCreateMode() {
            return ((Boolean) this.arguments.get("createMode")).booleanValue();
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment = (ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment) obj;
            return this.arguments.containsKey("profileId") == actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment.arguments.containsKey("profileId") && getProfileId() == actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment.getProfileId() && this.arguments.containsKey("createMode") == actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment.arguments.containsKey("createMode") && getCreateMode() == actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment.getCreateMode() && getActionId() == actionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment.getActionId();
        }

        public int hashCode() {
            return ((((((int) (getProfileId() ^ (getProfileId() >>> 32))) + 31) * 31) + (getCreateMode() ? 1 : 0)) * 31) + getActionId();
        }

        public String toString() {
            return "ActionNotificationProfileDetailsFragmentToEditNotificationProfileScheduleFragment(actionId=" + getActionId() + "){profileId=" + getProfileId() + ", createMode=" + getCreateMode() + "}";
        }
    }
}
