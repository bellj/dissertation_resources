package org.thoughtcrime.securesms.components.settings.app.privacy.advanced;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: AdvancedPrivacySettingsState.kt */
@Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003¢\u0006\u0002\u0010\u000bJ\t\u0010\u0014\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0006HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0019\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003JO\u0010\u001b\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001c\u001a\u00020\u00032\b\u0010\u001d\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001R\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\rR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\rR\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\rR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\r¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/AdvancedPrivacySettingsState;", "", "isPushEnabled", "", "alwaysRelayCalls", "censorshipCircumventionState", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/CensorshipCircumventionState;", "censorshipCircumventionEnabled", "showSealedSenderStatusIcon", "allowSealedSenderFromAnyone", "showProgressSpinner", "(ZZLorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/CensorshipCircumventionState;ZZZZ)V", "getAllowSealedSenderFromAnyone", "()Z", "getAlwaysRelayCalls", "getCensorshipCircumventionEnabled", "getCensorshipCircumventionState", "()Lorg/thoughtcrime/securesms/components/settings/app/privacy/advanced/CensorshipCircumventionState;", "getShowProgressSpinner", "getShowSealedSenderStatusIcon", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AdvancedPrivacySettingsState {
    private final boolean allowSealedSenderFromAnyone;
    private final boolean alwaysRelayCalls;
    private final boolean censorshipCircumventionEnabled;
    private final CensorshipCircumventionState censorshipCircumventionState;
    private final boolean isPushEnabled;
    private final boolean showProgressSpinner;
    private final boolean showSealedSenderStatusIcon;

    public static /* synthetic */ AdvancedPrivacySettingsState copy$default(AdvancedPrivacySettingsState advancedPrivacySettingsState, boolean z, boolean z2, CensorshipCircumventionState censorshipCircumventionState, boolean z3, boolean z4, boolean z5, boolean z6, int i, Object obj) {
        if ((i & 1) != 0) {
            z = advancedPrivacySettingsState.isPushEnabled;
        }
        if ((i & 2) != 0) {
            z2 = advancedPrivacySettingsState.alwaysRelayCalls;
        }
        if ((i & 4) != 0) {
            censorshipCircumventionState = advancedPrivacySettingsState.censorshipCircumventionState;
        }
        if ((i & 8) != 0) {
            z3 = advancedPrivacySettingsState.censorshipCircumventionEnabled;
        }
        if ((i & 16) != 0) {
            z4 = advancedPrivacySettingsState.showSealedSenderStatusIcon;
        }
        if ((i & 32) != 0) {
            z5 = advancedPrivacySettingsState.allowSealedSenderFromAnyone;
        }
        if ((i & 64) != 0) {
            z6 = advancedPrivacySettingsState.showProgressSpinner;
        }
        return advancedPrivacySettingsState.copy(z, z2, censorshipCircumventionState, z3, z4, z5, z6);
    }

    public final boolean component1() {
        return this.isPushEnabled;
    }

    public final boolean component2() {
        return this.alwaysRelayCalls;
    }

    public final CensorshipCircumventionState component3() {
        return this.censorshipCircumventionState;
    }

    public final boolean component4() {
        return this.censorshipCircumventionEnabled;
    }

    public final boolean component5() {
        return this.showSealedSenderStatusIcon;
    }

    public final boolean component6() {
        return this.allowSealedSenderFromAnyone;
    }

    public final boolean component7() {
        return this.showProgressSpinner;
    }

    public final AdvancedPrivacySettingsState copy(boolean z, boolean z2, CensorshipCircumventionState censorshipCircumventionState, boolean z3, boolean z4, boolean z5, boolean z6) {
        Intrinsics.checkNotNullParameter(censorshipCircumventionState, "censorshipCircumventionState");
        return new AdvancedPrivacySettingsState(z, z2, censorshipCircumventionState, z3, z4, z5, z6);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AdvancedPrivacySettingsState)) {
            return false;
        }
        AdvancedPrivacySettingsState advancedPrivacySettingsState = (AdvancedPrivacySettingsState) obj;
        return this.isPushEnabled == advancedPrivacySettingsState.isPushEnabled && this.alwaysRelayCalls == advancedPrivacySettingsState.alwaysRelayCalls && this.censorshipCircumventionState == advancedPrivacySettingsState.censorshipCircumventionState && this.censorshipCircumventionEnabled == advancedPrivacySettingsState.censorshipCircumventionEnabled && this.showSealedSenderStatusIcon == advancedPrivacySettingsState.showSealedSenderStatusIcon && this.allowSealedSenderFromAnyone == advancedPrivacySettingsState.allowSealedSenderFromAnyone && this.showProgressSpinner == advancedPrivacySettingsState.showProgressSpinner;
    }

    public int hashCode() {
        boolean z = this.isPushEnabled;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.alwaysRelayCalls;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int hashCode = (((i5 + i6) * 31) + this.censorshipCircumventionState.hashCode()) * 31;
        boolean z3 = this.censorshipCircumventionEnabled;
        if (z3) {
            z3 = true;
        }
        int i9 = z3 ? 1 : 0;
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = (hashCode + i9) * 31;
        boolean z4 = this.showSealedSenderStatusIcon;
        if (z4) {
            z4 = true;
        }
        int i13 = z4 ? 1 : 0;
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = (i12 + i13) * 31;
        boolean z5 = this.allowSealedSenderFromAnyone;
        if (z5) {
            z5 = true;
        }
        int i17 = z5 ? 1 : 0;
        int i18 = z5 ? 1 : 0;
        int i19 = z5 ? 1 : 0;
        int i20 = (i16 + i17) * 31;
        boolean z6 = this.showProgressSpinner;
        if (!z6) {
            i = z6 ? 1 : 0;
        }
        return i20 + i;
    }

    public String toString() {
        return "AdvancedPrivacySettingsState(isPushEnabled=" + this.isPushEnabled + ", alwaysRelayCalls=" + this.alwaysRelayCalls + ", censorshipCircumventionState=" + this.censorshipCircumventionState + ", censorshipCircumventionEnabled=" + this.censorshipCircumventionEnabled + ", showSealedSenderStatusIcon=" + this.showSealedSenderStatusIcon + ", allowSealedSenderFromAnyone=" + this.allowSealedSenderFromAnyone + ", showProgressSpinner=" + this.showProgressSpinner + ')';
    }

    public AdvancedPrivacySettingsState(boolean z, boolean z2, CensorshipCircumventionState censorshipCircumventionState, boolean z3, boolean z4, boolean z5, boolean z6) {
        Intrinsics.checkNotNullParameter(censorshipCircumventionState, "censorshipCircumventionState");
        this.isPushEnabled = z;
        this.alwaysRelayCalls = z2;
        this.censorshipCircumventionState = censorshipCircumventionState;
        this.censorshipCircumventionEnabled = z3;
        this.showSealedSenderStatusIcon = z4;
        this.allowSealedSenderFromAnyone = z5;
        this.showProgressSpinner = z6;
    }

    public final boolean isPushEnabled() {
        return this.isPushEnabled;
    }

    public final boolean getAlwaysRelayCalls() {
        return this.alwaysRelayCalls;
    }

    public final CensorshipCircumventionState getCensorshipCircumventionState() {
        return this.censorshipCircumventionState;
    }

    public final boolean getCensorshipCircumventionEnabled() {
        return this.censorshipCircumventionEnabled;
    }

    public final boolean getShowSealedSenderStatusIcon() {
        return this.showSealedSenderStatusIcon;
    }

    public final boolean getAllowSealedSenderFromAnyone() {
        return this.allowSealedSenderFromAnyone;
    }

    public final boolean getShowProgressSpinner() {
        return this.showProgressSpinner;
    }
}
