package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class AddAllowedMembersFragmentArgs {
    private final HashMap arguments;

    private AddAllowedMembersFragmentArgs() {
        this.arguments = new HashMap();
    }

    private AddAllowedMembersFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static AddAllowedMembersFragmentArgs fromBundle(Bundle bundle) {
        AddAllowedMembersFragmentArgs addAllowedMembersFragmentArgs = new AddAllowedMembersFragmentArgs();
        bundle.setClassLoader(AddAllowedMembersFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("profileId")) {
            addAllowedMembersFragmentArgs.arguments.put("profileId", Long.valueOf(bundle.getLong("profileId")));
            return addAllowedMembersFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"profileId\" is missing and does not have an android:defaultValue");
    }

    public long getProfileId() {
        return ((Long) this.arguments.get("profileId")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("profileId")) {
            bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AddAllowedMembersFragmentArgs addAllowedMembersFragmentArgs = (AddAllowedMembersFragmentArgs) obj;
        return this.arguments.containsKey("profileId") == addAllowedMembersFragmentArgs.arguments.containsKey("profileId") && getProfileId() == addAllowedMembersFragmentArgs.getProfileId();
    }

    public int hashCode() {
        return 31 + ((int) (getProfileId() ^ (getProfileId() >>> 32)));
    }

    public String toString() {
        return "AddAllowedMembersFragmentArgs{profileId=" + getProfileId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(AddAllowedMembersFragmentArgs addAllowedMembersFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(addAllowedMembersFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("profileId", Long.valueOf(j));
        }

        public AddAllowedMembersFragmentArgs build() {
            return new AddAllowedMembersFragmentArgs(this.arguments);
        }

        public Builder setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }
    }
}
