package org.thoughtcrime.securesms.components;

import android.view.View;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AlbumThumbnailView$$ExternalSyntheticLambda0 implements SlideClickListener {
    public final /* synthetic */ AlbumThumbnailView f$0;

    public /* synthetic */ AlbumThumbnailView$$ExternalSyntheticLambda0(AlbumThumbnailView albumThumbnailView) {
        this.f$0 = albumThumbnailView;
    }

    @Override // org.thoughtcrime.securesms.mms.SlideClickListener
    public final void onClick(View view, Slide slide) {
        this.f$0.lambda$new$0(view, slide);
    }
}
