package org.thoughtcrime.securesms.components.sensors;

/* loaded from: classes4.dex */
public final class SensorUtil {
    private SensorUtil() {
    }

    public static void getRotationMatrixWithoutMagneticSensorData(float[] fArr, float[] fArr2) {
        double d = (double) (fArr2[0] / 9.81f);
        double d2 = (double) (fArr2[1] / 9.81f);
        double d3 = (double) (fArr2[2] / 9.81f);
        Double.isNaN(d);
        Double.isNaN(d);
        Double.isNaN(d3);
        Double.isNaN(d3);
        double d4 = d3 * d3;
        double sqrt = Math.sqrt((d * d) + d4);
        Double.isNaN(d2);
        Double.isNaN(d2);
        Double.isNaN(d2);
        double sqrt2 = Math.sqrt((d2 * d2) + d4);
        Double.isNaN(d);
        System.arraycopy(getRotationMatrixForOrientation(new float[]{0.0f, (float) (-Math.atan(d2 / sqrt)), (float) (-Math.atan(d / sqrt2))}), 0, fArr, 0, fArr.length);
    }

    private static float[] getRotationMatrixForOrientation(float[] fArr) {
        float sin = (float) Math.sin((double) fArr[1]);
        float cos = (float) Math.cos((double) fArr[1]);
        float sin2 = (float) Math.sin((double) fArr[2]);
        float cos2 = (float) Math.cos((double) fArr[2]);
        float sin3 = (float) Math.sin((double) fArr[0]);
        float cos3 = (float) Math.cos((double) fArr[0]);
        return matrixMultiplication(new float[]{cos3, sin3, 0.0f, -sin3, cos3, 0.0f, 0.0f, 0.0f, 1.0f}, matrixMultiplication(new float[]{1.0f, 0.0f, 0.0f, 0.0f, cos, sin, 0.0f, -sin, cos}, new float[]{cos2, 0.0f, sin2, 0.0f, 1.0f, 0.0f, -sin2, 0.0f, cos2}));
    }

    private static float[] matrixMultiplication(float[] fArr, float[] fArr2) {
        float f = fArr[1];
        float f2 = fArr2[3];
        float f3 = fArr[2];
        float f4 = fArr2[6];
        float f5 = fArr[0];
        float f6 = fArr2[4];
        float f7 = fArr2[7];
        float f8 = f5 * fArr2[2];
        float f9 = fArr[1];
        float f10 = fArr2[5];
        float f11 = fArr2[8];
        float f12 = fArr[3];
        float f13 = fArr2[0];
        float f14 = fArr[4];
        float f15 = (f12 * f13) + (f2 * f14);
        float f16 = fArr[5];
        float f17 = fArr[3];
        float f18 = fArr2[1];
        float f19 = fArr2[2];
        float f20 = fArr[6] * f13;
        float f21 = fArr[7];
        float f22 = f20 + (fArr2[3] * f21);
        float f23 = fArr[8];
        float f24 = fArr[6];
        return new float[]{(fArr[0] * fArr2[0]) + (f * f2) + (f3 * f4), (fArr2[1] * f5) + (f * f6) + (f3 * f7), f8 + (f9 * f10) + (f3 * f11), f15 + (f16 * f4), (f17 * f18) + (f14 * f6) + (f16 * f7), (f17 * f19) + (fArr[4] * f10) + (f16 * f11), f22 + (f4 * f23), (f18 * f24) + (f21 * fArr2[4]) + (f7 * f23), (f24 * f19) + (fArr[7] * fArr2[5]) + (f23 * f11)};
    }
}
