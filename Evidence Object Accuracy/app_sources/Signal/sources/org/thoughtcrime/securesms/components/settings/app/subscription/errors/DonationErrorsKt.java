package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.donations.StripeDeclineCode;
import org.thoughtcrime.securesms.R;

/* compiled from: DonationErrors.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u00020\u0002H\u0007\u001a\n\u0010\u0003\u001a\u00020\u0004*\u00020\u0002¨\u0006\u0005"}, d2 = {"mapToErrorStringResource", "", "Lorg/signal/donations/StripeDeclineCode;", "shouldRouteToGooglePay", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationErrorsKt {

    /* compiled from: DonationErrors.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StripeDeclineCode.Code.values().length];
            iArr[StripeDeclineCode.Code.APPROVE_WITH_ID.ordinal()] = 1;
            iArr[StripeDeclineCode.Code.CALL_ISSUER.ordinal()] = 2;
            iArr[StripeDeclineCode.Code.CARD_NOT_SUPPORTED.ordinal()] = 3;
            iArr[StripeDeclineCode.Code.EXPIRED_CARD.ordinal()] = 4;
            iArr[StripeDeclineCode.Code.INCORRECT_NUMBER.ordinal()] = 5;
            iArr[StripeDeclineCode.Code.INCORRECT_CVC.ordinal()] = 6;
            iArr[StripeDeclineCode.Code.INSUFFICIENT_FUNDS.ordinal()] = 7;
            iArr[StripeDeclineCode.Code.INVALID_CVC.ordinal()] = 8;
            iArr[StripeDeclineCode.Code.INVALID_EXPIRY_MONTH.ordinal()] = 9;
            iArr[StripeDeclineCode.Code.INVALID_EXPIRY_YEAR.ordinal()] = 10;
            iArr[StripeDeclineCode.Code.INVALID_NUMBER.ordinal()] = 11;
            iArr[StripeDeclineCode.Code.ISSUER_NOT_AVAILABLE.ordinal()] = 12;
            iArr[StripeDeclineCode.Code.PROCESSING_ERROR.ordinal()] = 13;
            iArr[StripeDeclineCode.Code.REENTER_TRANSACTION.ordinal()] = 14;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public static final int mapToErrorStringResource(StripeDeclineCode stripeDeclineCode) {
        Intrinsics.checkNotNullParameter(stripeDeclineCode, "<this>");
        if (stripeDeclineCode instanceof StripeDeclineCode.Known) {
            switch (WhenMappings.$EnumSwitchMapping$0[((StripeDeclineCode.Known) stripeDeclineCode).getCode().ordinal()]) {
                case 1:
                    return R.string.DeclineCode__verify_your_payment_method_is_up_to_date_in_google_pay_and_try_again;
                case 2:
                    return R.string.DeclineCode__verify_your_payment_method_is_up_to_date_in_google_pay_and_try_again_if_the_problem;
                case 3:
                    return R.string.DeclineCode__your_card_does_not_support_this_type_of_purchase;
                case 4:
                    return R.string.DeclineCode__your_card_has_expired;
                case 5:
                case 11:
                    return R.string.DeclineCode__your_card_number_is_incorrect;
                case 6:
                case 8:
                    return R.string.DeclineCode__your_cards_cvc_number_is_incorrect;
                case 7:
                    return R.string.DeclineCode__your_card_does_not_have_sufficient_funds;
                case 9:
                    return R.string.DeclineCode__the_expiration_month;
                case 10:
                    return R.string.DeclineCode__the_expiration_year;
                case 12:
                    return R.string.DeclineCode__try_completing_the_payment_again;
                case 13:
                case 14:
                    return R.string.DeclineCode__try_again;
            }
        }
        return R.string.DeclineCode__try_another_payment_method_or_contact_your_bank;
    }

    public static final boolean shouldRouteToGooglePay(StripeDeclineCode stripeDeclineCode) {
        Intrinsics.checkNotNullParameter(stripeDeclineCode, "<this>");
        if (!(stripeDeclineCode instanceof StripeDeclineCode.Known)) {
            return false;
        }
        switch (WhenMappings.$EnumSwitchMapping$0[((StripeDeclineCode.Known) stripeDeclineCode).getCode().ordinal()]) {
            case 1:
            case 2:
            case 4:
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 11:
                return true;
            case 3:
            case 7:
            case 12:
            case 13:
            case 14:
            default:
                return false;
        }
    }
}
