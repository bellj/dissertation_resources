package org.thoughtcrime.securesms.components.settings.app.notifications;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: NotificationsSettingsState.kt */
@Metadata(d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\u001f\b\b\u0018\u00002\u00020\u0001BM\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\u0006\u0010\r\u001a\u00020\b\u0012\u0006\u0010\u000e\u001a\u00020\f¢\u0006\u0002\u0010\u000fJ\t\u0010\u001d\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\t\u0010 \u001a\u00020\bHÆ\u0003J\t\u0010!\u001a\u00020\bHÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\t\u0010#\u001a\u00020\fHÆ\u0003J\t\u0010$\u001a\u00020\bHÆ\u0003J\t\u0010%\u001a\u00020\fHÆ\u0003Jc\u0010&\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\b2\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\f2\b\b\u0002\u0010\r\u001a\u00020\b2\b\b\u0002\u0010\u000e\u001a\u00020\fHÆ\u0001J\u0013\u0010'\u001a\u00020\u00032\b\u0010(\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010)\u001a\u00020\fHÖ\u0001J\t\u0010*\u001a\u00020\bHÖ\u0001R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0013R\u0011\u0010\r\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0011R\u0011\u0010\u000e\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0011\u0010\u000b\u001a\u00020\f¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0018R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011¨\u0006+"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/MessageNotificationsState;", "", "notificationsEnabled", "", "sound", "Landroid/net/Uri;", "vibrateEnabled", "ledColor", "", "ledBlink", "inChatSoundsEnabled", "repeatAlerts", "", "messagePrivacy", "priority", "(ZLandroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;ZILjava/lang/String;I)V", "getInChatSoundsEnabled", "()Z", "getLedBlink", "()Ljava/lang/String;", "getLedColor", "getMessagePrivacy", "getNotificationsEnabled", "getPriority", "()I", "getRepeatAlerts", "getSound", "()Landroid/net/Uri;", "getVibrateEnabled", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class MessageNotificationsState {
    private final boolean inChatSoundsEnabled;
    private final String ledBlink;
    private final String ledColor;
    private final String messagePrivacy;
    private final boolean notificationsEnabled;
    private final int priority;
    private final int repeatAlerts;
    private final Uri sound;
    private final boolean vibrateEnabled;

    public final boolean component1() {
        return this.notificationsEnabled;
    }

    public final Uri component2() {
        return this.sound;
    }

    public final boolean component3() {
        return this.vibrateEnabled;
    }

    public final String component4() {
        return this.ledColor;
    }

    public final String component5() {
        return this.ledBlink;
    }

    public final boolean component6() {
        return this.inChatSoundsEnabled;
    }

    public final int component7() {
        return this.repeatAlerts;
    }

    public final String component8() {
        return this.messagePrivacy;
    }

    public final int component9() {
        return this.priority;
    }

    public final MessageNotificationsState copy(boolean z, Uri uri, boolean z2, String str, String str2, boolean z3, int i, String str3, int i2) {
        Intrinsics.checkNotNullParameter(uri, "sound");
        Intrinsics.checkNotNullParameter(str, "ledColor");
        Intrinsics.checkNotNullParameter(str2, "ledBlink");
        Intrinsics.checkNotNullParameter(str3, "messagePrivacy");
        return new MessageNotificationsState(z, uri, z2, str, str2, z3, i, str3, i2);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MessageNotificationsState)) {
            return false;
        }
        MessageNotificationsState messageNotificationsState = (MessageNotificationsState) obj;
        return this.notificationsEnabled == messageNotificationsState.notificationsEnabled && Intrinsics.areEqual(this.sound, messageNotificationsState.sound) && this.vibrateEnabled == messageNotificationsState.vibrateEnabled && Intrinsics.areEqual(this.ledColor, messageNotificationsState.ledColor) && Intrinsics.areEqual(this.ledBlink, messageNotificationsState.ledBlink) && this.inChatSoundsEnabled == messageNotificationsState.inChatSoundsEnabled && this.repeatAlerts == messageNotificationsState.repeatAlerts && Intrinsics.areEqual(this.messagePrivacy, messageNotificationsState.messagePrivacy) && this.priority == messageNotificationsState.priority;
    }

    public int hashCode() {
        boolean z = this.notificationsEnabled;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int hashCode = ((i2 * 31) + this.sound.hashCode()) * 31;
        boolean z2 = this.vibrateEnabled;
        if (z2) {
            z2 = true;
        }
        int i5 = z2 ? 1 : 0;
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int hashCode2 = (((((hashCode + i5) * 31) + this.ledColor.hashCode()) * 31) + this.ledBlink.hashCode()) * 31;
        boolean z3 = this.inChatSoundsEnabled;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return ((((((hashCode2 + i) * 31) + this.repeatAlerts) * 31) + this.messagePrivacy.hashCode()) * 31) + this.priority;
    }

    public String toString() {
        return "MessageNotificationsState(notificationsEnabled=" + this.notificationsEnabled + ", sound=" + this.sound + ", vibrateEnabled=" + this.vibrateEnabled + ", ledColor=" + this.ledColor + ", ledBlink=" + this.ledBlink + ", inChatSoundsEnabled=" + this.inChatSoundsEnabled + ", repeatAlerts=" + this.repeatAlerts + ", messagePrivacy=" + this.messagePrivacy + ", priority=" + this.priority + ')';
    }

    public MessageNotificationsState(boolean z, Uri uri, boolean z2, String str, String str2, boolean z3, int i, String str3, int i2) {
        Intrinsics.checkNotNullParameter(uri, "sound");
        Intrinsics.checkNotNullParameter(str, "ledColor");
        Intrinsics.checkNotNullParameter(str2, "ledBlink");
        Intrinsics.checkNotNullParameter(str3, "messagePrivacy");
        this.notificationsEnabled = z;
        this.sound = uri;
        this.vibrateEnabled = z2;
        this.ledColor = str;
        this.ledBlink = str2;
        this.inChatSoundsEnabled = z3;
        this.repeatAlerts = i;
        this.messagePrivacy = str3;
        this.priority = i2;
    }

    public final boolean getNotificationsEnabled() {
        return this.notificationsEnabled;
    }

    public final Uri getSound() {
        return this.sound;
    }

    public final boolean getVibrateEnabled() {
        return this.vibrateEnabled;
    }

    public final String getLedColor() {
        return this.ledColor;
    }

    public final String getLedBlink() {
        return this.ledBlink;
    }

    public final boolean getInChatSoundsEnabled() {
        return this.inChatSoundsEnabled;
    }

    public final int getRepeatAlerts() {
        return this.repeatAlerts;
    }

    public final String getMessagePrivacy() {
        return this.messagePrivacy;
    }

    public final int getPriority() {
        return this.priority;
    }
}
