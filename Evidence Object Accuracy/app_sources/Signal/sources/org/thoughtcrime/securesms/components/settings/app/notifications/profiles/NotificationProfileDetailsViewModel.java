package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleSource;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.functions.Function;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import io.reactivex.rxjava3.kotlin.SubscribersKt;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfiles;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: NotificationProfileDetailsViewModel.kt */
@Metadata(d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002#$B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u00120\u00112\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u0015\u001a\u00020\u0016J\b\u0010\u0017\u001a\u00020\u0018H\u0014J\u0014\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00112\u0006\u0010\u0013\u001a\u00020\u0014J\u0006\u0010\u001b\u001a\u00020\u0018J\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00120\u0011J\u000e\u0010\u001e\u001a\u00020\u00162\u0006\u0010\u001f\u001a\u00020\u0012J\u0010\u0010 \u001a\u00020\u00182\u0006\u0010!\u001a\u00020\"H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000b0\u000fX\u0004¢\u0006\u0002\n\u0000¨\u0006%"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel;", "Landroidx/lifecycle/ViewModel;", "profileId", "", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "(JLorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", "disposables", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "addMember", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "deleteProfile", "Lio/reactivex/rxjava3/core/Completable;", "onCleared", "", "removeMember", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "showAllMembers", "toggleAllowAllCalls", "toggleAllowAllMentions", "toggleEnabled", "profile", "updateWithValidState", "newState", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$Valid;", "Factory", "State", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationProfileDetailsViewModel extends ViewModel {
    private final CompositeDisposable disposables;
    private final long profileId;
    private final NotificationProfilesRepository repository;
    private final LiveData<State> state;
    private final Store<State> store;

    public NotificationProfileDetailsViewModel(long j, NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "repository");
        this.profileId = j;
        this.repository = notificationProfilesRepository;
        Store<State> store = new Store<>(State.NotLoaded.INSTANCE);
        this.store = store;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        this.disposables = compositeDisposable;
        LiveData<State> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        Observable<R> map = notificationProfilesRepository.getProfiles().map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m765_init_$lambda2(NotificationProfileDetailsViewModel.this, (List) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(map, "repository.getProfiles()…      )\n        }\n      }");
        DisposableKt.plusAssign(compositeDisposable, SubscribersKt.subscribeBy$default(map, (Function1) null, (Function0) null, new Function1<State, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel.2
            final /* synthetic */ NotificationProfileDetailsViewModel this$0;

            {
                this.this$0 = r1;
            }

            /* renamed from: invoke$lambda-0 */
            public static final State m773invoke$lambda0(State state, State state2) {
                return state;
            }

            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(State state) {
                invoke(state);
                return Unit.INSTANCE;
            }

            public final void invoke(State state) {
                if (Intrinsics.areEqual(state, State.NotLoaded.INSTANCE)) {
                    return;
                }
                if (Intrinsics.areEqual(state, State.Invalid.INSTANCE)) {
                    this.this$0.store.update(new NotificationProfileDetailsViewModel$2$$ExternalSyntheticLambda0(state));
                } else if (state instanceof State.Valid) {
                    NotificationProfileDetailsViewModel notificationProfileDetailsViewModel = this.this$0;
                    Intrinsics.checkNotNullExpressionValue(state, "newState");
                    notificationProfileDetailsViewModel.updateWithValidState((State.Valid) state);
                }
            }
        }, 3, (Object) null));
    }

    public final LiveData<State> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-2 */
    public static final State m765_init_$lambda2(NotificationProfileDetailsViewModel notificationProfileDetailsViewModel, List list) {
        Object obj;
        boolean z;
        Intrinsics.checkNotNullParameter(notificationProfileDetailsViewModel, "this$0");
        Intrinsics.checkNotNullExpressionValue(list, "profiles");
        Iterator it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((NotificationProfile) obj).getId() == notificationProfileDetailsViewModel.profileId) {
                z = true;
                continue;
            } else {
                z = false;
                continue;
            }
            if (z) {
                break;
            }
        }
        NotificationProfile notificationProfile = (NotificationProfile) obj;
        if (notificationProfile == null) {
            return State.Invalid.INSTANCE;
        }
        Set<RecipientId> allowedMembers = notificationProfile.getAllowedMembers();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(allowedMembers, 10));
        for (RecipientId recipientId : allowedMembers) {
            Recipient resolved = Recipient.resolved(recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(it)");
            arrayList.add(resolved);
        }
        return new State.Valid(notificationProfile, arrayList, Intrinsics.areEqual(NotificationProfiles.getActiveProfile$default(list, 0, null, 6, null), notificationProfile), false, 8, null);
    }

    public final void updateWithValidState(State.Valid valid) {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda5
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m772updateWithValidState$lambda3(NotificationProfileDetailsViewModel.State.Valid.this, (NotificationProfileDetailsViewModel.State) obj);
            }
        });
    }

    /* renamed from: updateWithValidState$lambda-3 */
    public static final State m772updateWithValidState$lambda3(State.Valid valid, State state) {
        Intrinsics.checkNotNullParameter(valid, "$newState");
        Intrinsics.checkNotNullParameter(state, "oldState");
        return state instanceof State.Valid ? State.Valid.copy$default((State.Valid) state, valid.getProfile(), valid.getRecipients(), valid.isOn(), false, 8, null) : valid;
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.disposables.clear();
    }

    public final Single<NotificationProfile> addMember(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Single<NotificationProfile> observeOn = this.repository.addMember(this.profileId, recipientId).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.addMember(pro…dSchedulers.mainThread())");
        return observeOn;
    }

    public final Single<Recipient> removeMember(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Single<Recipient> observeOn = this.repository.removeMember(this.profileId, recipientId).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m766removeMember$lambda4(RecipientId.this, (NotificationProfile) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.removeMember(…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: removeMember$lambda-4 */
    public static final Recipient m766removeMember$lambda4(RecipientId recipientId, NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(recipientId, "$id");
        return Recipient.resolved(recipientId);
    }

    public final Completable deleteProfile() {
        Completable observeOn = this.repository.deleteProfile(this.profileId).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.deleteProfile…dSchedulers.mainThread())");
        return observeOn;
    }

    public final Completable toggleEnabled(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfile, "profile");
        Completable observeOn = NotificationProfilesRepository.manuallyToggleProfile$default(this.repository, notificationProfile, 0, 2, null).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.manuallyToggl…dSchedulers.mainThread())");
        return observeOn;
    }

    public final Single<NotificationProfile> toggleAllowAllMentions() {
        Single<NotificationProfile> observeOn = this.repository.getProfile(this.profileId).take(1).singleOrError().flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m770toggleAllowAllMentions$lambda5(NotificationProfileDetailsViewModel.this, (NotificationProfile) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m771toggleAllowAllMentions$lambda6((NotificationProfileDatabase.NotificationProfileChangeResult) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.getProfile(pr…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: toggleAllowAllMentions$lambda-5 */
    public static final SingleSource m770toggleAllowAllMentions$lambda5(NotificationProfileDetailsViewModel notificationProfileDetailsViewModel, NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfileDetailsViewModel, "this$0");
        NotificationProfilesRepository notificationProfilesRepository = notificationProfileDetailsViewModel.repository;
        Intrinsics.checkNotNullExpressionValue(notificationProfile, "it");
        return notificationProfilesRepository.updateProfile(notificationProfile.copy((r24 & 1) != 0 ? notificationProfile.id : 0, (r24 & 2) != 0 ? notificationProfile.name : null, (r24 & 4) != 0 ? notificationProfile.emoji : null, (r24 & 8) != 0 ? notificationProfile.color : null, (r24 & 16) != 0 ? notificationProfile.createdAt : 0, (r24 & 32) != 0 ? notificationProfile.allowAllCalls : false, (r24 & 64) != 0 ? notificationProfile.allowAllMentions : !notificationProfile.getAllowAllMentions(), (r24 & 128) != 0 ? notificationProfile.schedule : null, (r24 & 256) != 0 ? notificationProfile.allowedMembers : null));
    }

    /* renamed from: toggleAllowAllMentions$lambda-6 */
    public static final NotificationProfile m771toggleAllowAllMentions$lambda6(NotificationProfileDatabase.NotificationProfileChangeResult notificationProfileChangeResult) {
        if (notificationProfileChangeResult != null) {
            return ((NotificationProfileDatabase.NotificationProfileChangeResult.Success) notificationProfileChangeResult).getNotificationProfile();
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.NotificationProfileDatabase.NotificationProfileChangeResult.Success");
    }

    public final Single<NotificationProfile> toggleAllowAllCalls() {
        Single<NotificationProfile> observeOn = this.repository.getProfile(this.profileId).take(1).singleOrError().flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m768toggleAllowAllCalls$lambda7(NotificationProfileDetailsViewModel.this, (NotificationProfile) obj);
            }
        }).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m769toggleAllowAllCalls$lambda8((NotificationProfileDatabase.NotificationProfileChangeResult) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.getProfile(pr…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: toggleAllowAllCalls$lambda-7 */
    public static final SingleSource m768toggleAllowAllCalls$lambda7(NotificationProfileDetailsViewModel notificationProfileDetailsViewModel, NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(notificationProfileDetailsViewModel, "this$0");
        NotificationProfilesRepository notificationProfilesRepository = notificationProfileDetailsViewModel.repository;
        Intrinsics.checkNotNullExpressionValue(notificationProfile, "it");
        return notificationProfilesRepository.updateProfile(notificationProfile.copy((r24 & 1) != 0 ? notificationProfile.id : 0, (r24 & 2) != 0 ? notificationProfile.name : null, (r24 & 4) != 0 ? notificationProfile.emoji : null, (r24 & 8) != 0 ? notificationProfile.color : null, (r24 & 16) != 0 ? notificationProfile.createdAt : 0, (r24 & 32) != 0 ? notificationProfile.allowAllCalls : !notificationProfile.getAllowAllCalls(), (r24 & 64) != 0 ? notificationProfile.allowAllMentions : false, (r24 & 128) != 0 ? notificationProfile.schedule : null, (r24 & 256) != 0 ? notificationProfile.allowedMembers : null));
    }

    /* renamed from: toggleAllowAllCalls$lambda-8 */
    public static final NotificationProfile m769toggleAllowAllCalls$lambda8(NotificationProfileDatabase.NotificationProfileChangeResult notificationProfileChangeResult) {
        if (notificationProfileChangeResult != null) {
            return ((NotificationProfileDatabase.NotificationProfileChangeResult.Success) notificationProfileChangeResult).getNotificationProfile();
        }
        throw new NullPointerException("null cannot be cast to non-null type org.thoughtcrime.securesms.database.NotificationProfileDatabase.NotificationProfileChangeResult.Success");
    }

    public final void showAllMembers() {
        this.store.update(new com.annimon.stream.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return NotificationProfileDetailsViewModel.m767showAllMembers$lambda9((NotificationProfileDetailsViewModel.State) obj);
            }
        });
    }

    /* renamed from: showAllMembers$lambda-9 */
    public static final State m767showAllMembers$lambda9(State state) {
        return state instanceof State.Valid ? State.Valid.copy$default((State.Valid) state, null, null, false, true, 7, null) : state;
    }

    /* compiled from: NotificationProfileDetailsViewModel.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State;", "", "()V", "Invalid", "NotLoaded", "Valid", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$Valid;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$Invalid;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$NotLoaded;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class State {
        public /* synthetic */ State(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: NotificationProfileDetailsViewModel.kt */
        @Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\b\b\u0002\u0010\t\u001a\u00020\b¢\u0006\u0002\u0010\nJ\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J\t\u0010\u0013\u001a\u00020\bHÆ\u0003J\t\u0010\u0014\u001a\u00020\bHÆ\u0003J7\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\bHÆ\u0001J\u0013\u0010\u0016\u001a\u00020\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018HÖ\u0003J\t\u0010\u0019\u001a\u00020\u001aHÖ\u0001J\t\u0010\u001b\u001a\u00020\u001cHÖ\u0001R\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\fR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$Valid;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State;", "profile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "isOn", "", "expanded", "(Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;Ljava/util/List;ZZ)V", "getExpanded", "()Z", "getProfile", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "getRecipients", "()Ljava/util/List;", "component1", "component2", "component3", "component4", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Valid extends State {
            private final boolean expanded;
            private final boolean isOn;
            private final NotificationProfile profile;
            private final List<Recipient> recipients;

            /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsViewModel$State$Valid */
            /* JADX WARN: Multi-variable type inference failed */
            public static /* synthetic */ Valid copy$default(Valid valid, NotificationProfile notificationProfile, List list, boolean z, boolean z2, int i, Object obj) {
                if ((i & 1) != 0) {
                    notificationProfile = valid.profile;
                }
                if ((i & 2) != 0) {
                    list = valid.recipients;
                }
                if ((i & 4) != 0) {
                    z = valid.isOn;
                }
                if ((i & 8) != 0) {
                    z2 = valid.expanded;
                }
                return valid.copy(notificationProfile, list, z, z2);
            }

            public final NotificationProfile component1() {
                return this.profile;
            }

            public final List<Recipient> component2() {
                return this.recipients;
            }

            public final boolean component3() {
                return this.isOn;
            }

            public final boolean component4() {
                return this.expanded;
            }

            public final Valid copy(NotificationProfile notificationProfile, List<? extends Recipient> list, boolean z, boolean z2) {
                Intrinsics.checkNotNullParameter(notificationProfile, "profile");
                Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
                return new Valid(notificationProfile, list, z, z2);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Valid)) {
                    return false;
                }
                Valid valid = (Valid) obj;
                return Intrinsics.areEqual(this.profile, valid.profile) && Intrinsics.areEqual(this.recipients, valid.recipients) && this.isOn == valid.isOn && this.expanded == valid.expanded;
            }

            public int hashCode() {
                int hashCode = ((this.profile.hashCode() * 31) + this.recipients.hashCode()) * 31;
                boolean z = this.isOn;
                int i = 1;
                if (z) {
                    z = true;
                }
                int i2 = z ? 1 : 0;
                int i3 = z ? 1 : 0;
                int i4 = z ? 1 : 0;
                int i5 = (hashCode + i2) * 31;
                boolean z2 = this.expanded;
                if (!z2) {
                    i = z2 ? 1 : 0;
                }
                return i5 + i;
            }

            public String toString() {
                return "Valid(profile=" + this.profile + ", recipients=" + this.recipients + ", isOn=" + this.isOn + ", expanded=" + this.expanded + ')';
            }

            public /* synthetic */ Valid(NotificationProfile notificationProfile, List list, boolean z, boolean z2, int i, DefaultConstructorMarker defaultConstructorMarker) {
                this(notificationProfile, list, z, (i & 8) != 0 ? false : z2);
            }

            public final NotificationProfile getProfile() {
                return this.profile;
            }

            public final List<Recipient> getRecipients() {
                return this.recipients;
            }

            public final boolean isOn() {
                return this.isOn;
            }

            public final boolean getExpanded() {
                return this.expanded;
            }

            /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
            /* JADX WARN: Multi-variable type inference failed */
            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Valid(NotificationProfile notificationProfile, List<? extends Recipient> list, boolean z, boolean z2) {
                super(null);
                Intrinsics.checkNotNullParameter(notificationProfile, "profile");
                Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
                this.profile = notificationProfile;
                this.recipients = list;
                this.isOn = z;
                this.expanded = z2;
            }
        }

        private State() {
        }

        /* compiled from: NotificationProfileDetailsViewModel.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$Invalid;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Invalid extends State {
            public static final Invalid INSTANCE = new Invalid();

            private Invalid() {
                super(null);
            }
        }

        /* compiled from: NotificationProfileDetailsViewModel.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State$NotLoaded;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$State;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class NotLoaded extends State {
            public static final NotLoaded INSTANCE = new NotLoaded();

            private NotLoaded() {
                super(null);
            }
        }
    }

    /* compiled from: NotificationProfileDetailsViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfileDetailsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "profileId", "", "(J)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long profileId;

        public Factory(long j) {
            this.profileId = j;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new NotificationProfileDetailsViewModel(this.profileId, new NotificationProfilesRepository()));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
