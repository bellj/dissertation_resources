package org.thoughtcrime.securesms.components.settings.app.account;

import kotlin.Metadata;

/* compiled from: AccountSettingsState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00032\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsState;", "", "hasPin", "", "pinRemindersEnabled", "registrationLockEnabled", "(ZZZ)V", "getHasPin", "()Z", "getPinRemindersEnabled", "getRegistrationLockEnabled", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AccountSettingsState {
    private final boolean hasPin;
    private final boolean pinRemindersEnabled;
    private final boolean registrationLockEnabled;

    public static /* synthetic */ AccountSettingsState copy$default(AccountSettingsState accountSettingsState, boolean z, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z = accountSettingsState.hasPin;
        }
        if ((i & 2) != 0) {
            z2 = accountSettingsState.pinRemindersEnabled;
        }
        if ((i & 4) != 0) {
            z3 = accountSettingsState.registrationLockEnabled;
        }
        return accountSettingsState.copy(z, z2, z3);
    }

    public final boolean component1() {
        return this.hasPin;
    }

    public final boolean component2() {
        return this.pinRemindersEnabled;
    }

    public final boolean component3() {
        return this.registrationLockEnabled;
    }

    public final AccountSettingsState copy(boolean z, boolean z2, boolean z3) {
        return new AccountSettingsState(z, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AccountSettingsState)) {
            return false;
        }
        AccountSettingsState accountSettingsState = (AccountSettingsState) obj;
        return this.hasPin == accountSettingsState.hasPin && this.pinRemindersEnabled == accountSettingsState.pinRemindersEnabled && this.registrationLockEnabled == accountSettingsState.registrationLockEnabled;
    }

    public int hashCode() {
        boolean z = this.hasPin;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.pinRemindersEnabled;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.registrationLockEnabled;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i9 + i;
    }

    public String toString() {
        return "AccountSettingsState(hasPin=" + this.hasPin + ", pinRemindersEnabled=" + this.pinRemindersEnabled + ", registrationLockEnabled=" + this.registrationLockEnabled + ')';
    }

    public AccountSettingsState(boolean z, boolean z2, boolean z3) {
        this.hasPin = z;
        this.pinRemindersEnabled = z2;
        this.registrationLockEnabled = z3;
    }

    public final boolean getHasPin() {
        return this.hasPin;
    }

    public final boolean getPinRemindersEnabled() {
        return this.pinRemindersEnabled;
    }

    public final boolean getRegistrationLockEnabled() {
        return this.registrationLockEnabled;
    }
}
