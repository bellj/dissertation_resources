package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;

/* loaded from: classes4.dex */
public class ChangeNumberAccountLockedFragmentDirections {
    private ChangeNumberAccountLockedFragmentDirections() {
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }
}
