package org.thoughtcrime.securesms.components.settings.app.internal.donor;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import java.util.ArrayList;
import java.util.List;
import kotlin.Lazy;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.donations.StripeDeclineCode;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.subscription.errors.UnexpectedSubscriptionCancellation;
import org.thoughtcrime.securesms.util.LifecycleDisposable;

/* compiled from: DonorErrorConfigurationFragment.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "lifecycleDisposable", "Lorg/thoughtcrime/securesms/util/LifecycleDisposable;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/internal/donor/DonorErrorConfigurationState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonorErrorConfigurationFragment extends DSLSettingsFragment {
    private final LifecycleDisposable lifecycleDisposable = new LifecycleDisposable();
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(DonorErrorConfigurationViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, null);

    public DonorErrorConfigurationFragment() {
        super(0, 0, 0, null, 15, null);
    }

    public final DonorErrorConfigurationViewModel getViewModel() {
        return (DonorErrorConfigurationViewModel) this.viewModel$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        LifecycleDisposable lifecycleDisposable = this.lifecycleDisposable;
        Disposable subscribe = getViewModel().getState().observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ DonorErrorConfigurationFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                DonorErrorConfigurationFragment.$r8$lambda$RXpDfPy1a0CjNKlB28KE6HU5mks(DSLSettingsAdapter.this, this.f$1, (DonorErrorConfigurationState) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.state.observeO…MappingModelList())\n    }");
        lifecycleDisposable.plusAssign(subscribe);
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m672bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, DonorErrorConfigurationFragment donorErrorConfigurationFragment, DonorErrorConfigurationState donorErrorConfigurationState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(donorErrorConfigurationFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(donorErrorConfigurationState, "state");
        dSLSettingsAdapter.submitList(donorErrorConfigurationFragment.getConfiguration(donorErrorConfigurationState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(DonorErrorConfigurationState donorErrorConfigurationState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(donorErrorConfigurationState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1
            final /* synthetic */ DonorErrorConfigurationState $state;
            final /* synthetic */ DonorErrorConfigurationFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText from = DSLSettingsText.Companion.from(R.string.preferences__internal_donor_error_expired_badge, new DSLSettingsText.Modifier[0]);
                int i = CollectionsKt___CollectionsKt.indexOf((List<? extends Badge>) ((List<? extends Object>) this.$state.getBadges()), this.$state.getSelectedBadge());
                List<Badge> badges = this.$state.getBadges();
                ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(badges, 10));
                for (Badge badge : badges) {
                    arrayList.add(badge.getName());
                }
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    String[] strArr = (String[]) array;
                    final DonorErrorConfigurationFragment donorErrorConfigurationFragment = this.this$0;
                    dSLConfiguration.radioListPref(from, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from : null, (r19 & 8) != 0, strArr, i, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1.2
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                            invoke(num.intValue());
                            return Unit.INSTANCE;
                        }

                        public final void invoke(int i2) {
                            DonorErrorConfigurationFragment.access$getViewModel(donorErrorConfigurationFragment).setSelectedBadge(i2);
                        }
                    });
                    DSLSettingsText from2 = DSLSettingsText.Companion.from(R.string.preferences__internal_donor_error_cancelation_reason, new DSLSettingsText.Modifier[0]);
                    int i2 = ArraysKt___ArraysKt.indexOf(UnexpectedSubscriptionCancellation.values(), this.$state.getSelectedUnexpectedSubscriptionCancellation());
                    UnexpectedSubscriptionCancellation[] values = UnexpectedSubscriptionCancellation.values();
                    ArrayList arrayList2 = new ArrayList(values.length);
                    for (UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation : values) {
                        arrayList2.add(unexpectedSubscriptionCancellation.getStatus());
                    }
                    Object[] array2 = arrayList2.toArray(new String[0]);
                    if (array2 != null) {
                        String[] strArr2 = (String[]) array2;
                        boolean z = this.$state.getSelectedBadge() == null || this.$state.getSelectedBadge().isSubscription();
                        final DonorErrorConfigurationFragment donorErrorConfigurationFragment2 = this.this$0;
                        dSLConfiguration.radioListPref(from2, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from2 : null, (r19 & 8) != 0 ? true : z, strArr2, i2, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1.4
                            /* Return type fixed from 'java.lang.Object' to match base method */
                            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                            @Override // kotlin.jvm.functions.Function1
                            public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                                invoke(num.intValue());
                                return Unit.INSTANCE;
                            }

                            public final void invoke(int i3) {
                                DonorErrorConfigurationFragment.access$getViewModel(donorErrorConfigurationFragment2).setSelectedUnexpectedSubscriptionCancellation(i3);
                            }
                        });
                        DSLSettingsText from3 = DSLSettingsText.Companion.from(R.string.preferences__internal_donor_error_charge_failure, new DSLSettingsText.Modifier[0]);
                        int i3 = ArraysKt___ArraysKt.indexOf(StripeDeclineCode.Code.values(), this.$state.getSelectedStripeDeclineCode());
                        StripeDeclineCode.Code[] values2 = StripeDeclineCode.Code.values();
                        ArrayList arrayList3 = new ArrayList(values2.length);
                        for (StripeDeclineCode.Code code : values2) {
                            arrayList3.add(code.getCode());
                        }
                        Object[] array3 = arrayList3.toArray(new String[0]);
                        if (array3 != null) {
                            String[] strArr3 = (String[]) array3;
                            boolean z2 = this.$state.getSelectedBadge() == null || this.$state.getSelectedBadge().isSubscription();
                            final DonorErrorConfigurationFragment donorErrorConfigurationFragment3 = this.this$0;
                            dSLConfiguration.radioListPref(from3, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from3 : null, (r19 & 8) != 0 ? true : z2, strArr3, i3, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1.6
                                /* Return type fixed from 'java.lang.Object' to match base method */
                                /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                                @Override // kotlin.jvm.functions.Function1
                                public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                                    invoke(num.intValue());
                                    return Unit.INSTANCE;
                                }

                                public final void invoke(int i4) {
                                    DonorErrorConfigurationFragment.access$getViewModel(donorErrorConfigurationFragment3).setStripeDeclineCode(i4);
                                }
                            });
                            DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                            DSLSettingsText from4 = companion.from(R.string.preferences__internal_donor_error_save_and_finish, new DSLSettingsText.Modifier[0]);
                            final DonorErrorConfigurationFragment donorErrorConfigurationFragment4 = this.this$0;
                            DSLConfiguration.primaryButton$default(dSLConfiguration, from4, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1.7
                                /* access modifiers changed from: private */
                                /* renamed from: invoke$lambda-0  reason: not valid java name */
                                public static final void m674invoke$lambda0(DonorErrorConfigurationFragment donorErrorConfigurationFragment5) {
                                    Intrinsics.checkNotNullParameter(donorErrorConfigurationFragment5, "this$0");
                                    donorErrorConfigurationFragment5.requireActivity().finish();
                                }

                                @Override // kotlin.jvm.functions.Function0
                                public final void invoke() {
                                    LifecycleDisposable access$getLifecycleDisposable$p = DonorErrorConfigurationFragment.access$getLifecycleDisposable$p(donorErrorConfigurationFragment4);
                                    Disposable subscribe = DonorErrorConfigurationFragment.access$getViewModel(donorErrorConfigurationFragment4).save().subscribe(new DonorErrorConfigurationFragment$getConfiguration$1$7$$ExternalSyntheticLambda0(donorErrorConfigurationFragment4));
                                    Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.save().subscri…uireActivity().finish() }");
                                    access$getLifecycleDisposable$p.plusAssign(subscribe);
                                }
                            }, 2, null);
                            DSLSettingsText from5 = companion.from(R.string.preferences__internal_donor_error_clear, new DSLSettingsText.Modifier[0]);
                            final DonorErrorConfigurationFragment donorErrorConfigurationFragment5 = this.this$0;
                            DSLConfiguration.secondaryButtonNoOutline$default(dSLConfiguration, from5, null, false, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.internal.donor.DonorErrorConfigurationFragment$getConfiguration$1.8
                                @Override // kotlin.jvm.functions.Function0
                                public final void invoke() {
                                    LifecycleDisposable access$getLifecycleDisposable$p = DonorErrorConfigurationFragment.access$getLifecycleDisposable$p(donorErrorConfigurationFragment5);
                                    Disposable subscribe = DonorErrorConfigurationFragment.access$getViewModel(donorErrorConfigurationFragment5).clear().subscribe();
                                    Intrinsics.checkNotNullExpressionValue(subscribe, "viewModel.clear().subscribe()");
                                    access$getLifecycleDisposable$p.plusAssign(subscribe);
                                }
                            }, 6, null);
                            return;
                        }
                        throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                    }
                    throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
                }
                throw new NullPointerException("null cannot be cast to non-null type kotlin.Array<T of kotlin.collections.ArraysKt__ArraysJVMKt.toTypedArray>");
            }
        });
    }
}
