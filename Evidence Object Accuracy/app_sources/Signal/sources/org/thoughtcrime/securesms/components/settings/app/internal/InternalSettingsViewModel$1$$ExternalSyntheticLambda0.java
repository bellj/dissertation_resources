package org.thoughtcrime.securesms.components.settings.app.internal;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.app.internal.InternalSettingsViewModel;
import org.thoughtcrime.securesms.emoji.EmojiFiles;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class InternalSettingsViewModel$1$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ EmojiFiles.Version f$0;

    public /* synthetic */ InternalSettingsViewModel$1$$ExternalSyntheticLambda0(EmojiFiles.Version version) {
        this.f$0 = version;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return InternalSettingsViewModel.AnonymousClass1.m671invoke$lambda0(this.f$0, (InternalSettingsState) obj);
    }
}
