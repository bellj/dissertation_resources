package org.thoughtcrime.securesms.components.settings.app.privacy;

import java.util.List;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.database.model.DistributionListPartialRecord;
import org.thoughtcrime.securesms.keyvalue.PhoneNumberPrivacyValues;

/* compiled from: PrivacySettingsState.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b(\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\t\u0012\u0006\u0010\u000b\u001a\u00020\t\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\t\u0012\u0006\u0010\u000f\u001a\u00020\t\u0012\u0006\u0010\u0010\u001a\u00020\t\u0012\u0006\u0010\u0011\u001a\u00020\t\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015\u0012\u0006\u0010\u0017\u001a\u00020\t¢\u0006\u0002\u0010\u0018J\t\u0010+\u001a\u00020\u0003HÆ\u0003J\t\u0010,\u001a\u00020\tHÆ\u0003J\t\u0010-\u001a\u00020\tHÆ\u0003J\t\u0010.\u001a\u00020\u0003HÆ\u0003J\t\u0010/\u001a\u00020\u0003HÆ\u0003J\u000f\u00100\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015HÆ\u0003J\t\u00101\u001a\u00020\tHÆ\u0003J\t\u00102\u001a\u00020\u0005HÆ\u0003J\t\u00103\u001a\u00020\u0007HÆ\u0003J\t\u00104\u001a\u00020\tHÆ\u0003J\t\u00105\u001a\u00020\tHÆ\u0003J\t\u00106\u001a\u00020\tHÆ\u0003J\t\u00107\u001a\u00020\rHÆ\u0003J\t\u00108\u001a\u00020\tHÆ\u0003J\t\u00109\u001a\u00020\tHÆ\u0003J¥\u0001\u0010:\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\t2\b\b\u0002\u0010\u000b\u001a\u00020\t2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\t2\b\b\u0002\u0010\u000f\u001a\u00020\t2\b\b\u0002\u0010\u0010\u001a\u00020\t2\b\b\u0002\u0010\u0011\u001a\u00020\t2\b\b\u0002\u0010\u0012\u001a\u00020\u00032\b\b\u0002\u0010\u0013\u001a\u00020\u00032\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u00152\b\b\u0002\u0010\u0017\u001a\u00020\tHÆ\u0001J\u0013\u0010;\u001a\u00020\t2\b\u0010<\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010=\u001a\u00020\u0003HÖ\u0001J\t\u0010>\u001a\u00020?HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u000f\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u0010\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u001eR\u0011\u0010\u0011\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u001eR\u0011\u0010\u0017\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u001eR\u0011\u0010\u0012\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u001aR\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001eR\u0011\u0010\u000b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001eR\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\u000e\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001eR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b'\u0010(R\u0011\u0010\n\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001eR\u0011\u0010\u0013\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001a¨\u0006@"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsState;", "", "blockedCount", "", "seeMyPhoneNumber", "Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberSharingMode;", "findMeByPhoneNumber", "Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberListingMode;", "readReceipts", "", "typingIndicators", "screenLock", "screenLockActivityTimeout", "", "screenSecurity", "incognitoKeyboard", "isObsoletePasswordEnabled", "isObsoletePasswordTimeoutEnabled", "obsoletePasswordTimeout", "universalExpireTimer", "privateStories", "", "Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "isStoriesEnabled", "(ILorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberSharingMode;Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberListingMode;ZZZJZZZZIILjava/util/List;Z)V", "getBlockedCount", "()I", "getFindMeByPhoneNumber", "()Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberListingMode;", "getIncognitoKeyboard", "()Z", "getObsoletePasswordTimeout", "getPrivateStories", "()Ljava/util/List;", "getReadReceipts", "getScreenLock", "getScreenLockActivityTimeout", "()J", "getScreenSecurity", "getSeeMyPhoneNumber", "()Lorg/thoughtcrime/securesms/keyvalue/PhoneNumberPrivacyValues$PhoneNumberSharingMode;", "getTypingIndicators", "getUniversalExpireTimer", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PrivacySettingsState {
    private final int blockedCount;
    private final PhoneNumberPrivacyValues.PhoneNumberListingMode findMeByPhoneNumber;
    private final boolean incognitoKeyboard;
    private final boolean isObsoletePasswordEnabled;
    private final boolean isObsoletePasswordTimeoutEnabled;
    private final boolean isStoriesEnabled;
    private final int obsoletePasswordTimeout;
    private final List<DistributionListPartialRecord> privateStories;
    private final boolean readReceipts;
    private final boolean screenLock;
    private final long screenLockActivityTimeout;
    private final boolean screenSecurity;
    private final PhoneNumberPrivacyValues.PhoneNumberSharingMode seeMyPhoneNumber;
    private final boolean typingIndicators;
    private final int universalExpireTimer;

    public final int component1() {
        return this.blockedCount;
    }

    public final boolean component10() {
        return this.isObsoletePasswordEnabled;
    }

    public final boolean component11() {
        return this.isObsoletePasswordTimeoutEnabled;
    }

    public final int component12() {
        return this.obsoletePasswordTimeout;
    }

    public final int component13() {
        return this.universalExpireTimer;
    }

    public final List<DistributionListPartialRecord> component14() {
        return this.privateStories;
    }

    public final boolean component15() {
        return this.isStoriesEnabled;
    }

    public final PhoneNumberPrivacyValues.PhoneNumberSharingMode component2() {
        return this.seeMyPhoneNumber;
    }

    public final PhoneNumberPrivacyValues.PhoneNumberListingMode component3() {
        return this.findMeByPhoneNumber;
    }

    public final boolean component4() {
        return this.readReceipts;
    }

    public final boolean component5() {
        return this.typingIndicators;
    }

    public final boolean component6() {
        return this.screenLock;
    }

    public final long component7() {
        return this.screenLockActivityTimeout;
    }

    public final boolean component8() {
        return this.screenSecurity;
    }

    public final boolean component9() {
        return this.incognitoKeyboard;
    }

    public final PrivacySettingsState copy(int i, PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode, PhoneNumberPrivacyValues.PhoneNumberListingMode phoneNumberListingMode, boolean z, boolean z2, boolean z3, long j, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3, List<DistributionListPartialRecord> list, boolean z8) {
        Intrinsics.checkNotNullParameter(phoneNumberSharingMode, "seeMyPhoneNumber");
        Intrinsics.checkNotNullParameter(phoneNumberListingMode, "findMeByPhoneNumber");
        Intrinsics.checkNotNullParameter(list, "privateStories");
        return new PrivacySettingsState(i, phoneNumberSharingMode, phoneNumberListingMode, z, z2, z3, j, z4, z5, z6, z7, i2, i3, list, z8);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PrivacySettingsState)) {
            return false;
        }
        PrivacySettingsState privacySettingsState = (PrivacySettingsState) obj;
        return this.blockedCount == privacySettingsState.blockedCount && this.seeMyPhoneNumber == privacySettingsState.seeMyPhoneNumber && this.findMeByPhoneNumber == privacySettingsState.findMeByPhoneNumber && this.readReceipts == privacySettingsState.readReceipts && this.typingIndicators == privacySettingsState.typingIndicators && this.screenLock == privacySettingsState.screenLock && this.screenLockActivityTimeout == privacySettingsState.screenLockActivityTimeout && this.screenSecurity == privacySettingsState.screenSecurity && this.incognitoKeyboard == privacySettingsState.incognitoKeyboard && this.isObsoletePasswordEnabled == privacySettingsState.isObsoletePasswordEnabled && this.isObsoletePasswordTimeoutEnabled == privacySettingsState.isObsoletePasswordTimeoutEnabled && this.obsoletePasswordTimeout == privacySettingsState.obsoletePasswordTimeout && this.universalExpireTimer == privacySettingsState.universalExpireTimer && Intrinsics.areEqual(this.privateStories, privacySettingsState.privateStories) && this.isStoriesEnabled == privacySettingsState.isStoriesEnabled;
    }

    public int hashCode() {
        int hashCode = ((((this.blockedCount * 31) + this.seeMyPhoneNumber.hashCode()) * 31) + this.findMeByPhoneNumber.hashCode()) * 31;
        boolean z = this.readReceipts;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = (hashCode + i2) * 31;
        boolean z2 = this.typingIndicators;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.screenLock;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int m = (((i9 + i10) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.screenLockActivityTimeout)) * 31;
        boolean z4 = this.screenSecurity;
        if (z4) {
            z4 = true;
        }
        int i13 = z4 ? 1 : 0;
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = (m + i13) * 31;
        boolean z5 = this.incognitoKeyboard;
        if (z5) {
            z5 = true;
        }
        int i17 = z5 ? 1 : 0;
        int i18 = z5 ? 1 : 0;
        int i19 = z5 ? 1 : 0;
        int i20 = (i16 + i17) * 31;
        boolean z6 = this.isObsoletePasswordEnabled;
        if (z6) {
            z6 = true;
        }
        int i21 = z6 ? 1 : 0;
        int i22 = z6 ? 1 : 0;
        int i23 = z6 ? 1 : 0;
        int i24 = (i20 + i21) * 31;
        boolean z7 = this.isObsoletePasswordTimeoutEnabled;
        if (z7) {
            z7 = true;
        }
        int i25 = z7 ? 1 : 0;
        int i26 = z7 ? 1 : 0;
        int i27 = z7 ? 1 : 0;
        int hashCode2 = (((((((i24 + i25) * 31) + this.obsoletePasswordTimeout) * 31) + this.universalExpireTimer) * 31) + this.privateStories.hashCode()) * 31;
        boolean z8 = this.isStoriesEnabled;
        if (!z8) {
            i = z8 ? 1 : 0;
        }
        return hashCode2 + i;
    }

    public String toString() {
        return "PrivacySettingsState(blockedCount=" + this.blockedCount + ", seeMyPhoneNumber=" + this.seeMyPhoneNumber + ", findMeByPhoneNumber=" + this.findMeByPhoneNumber + ", readReceipts=" + this.readReceipts + ", typingIndicators=" + this.typingIndicators + ", screenLock=" + this.screenLock + ", screenLockActivityTimeout=" + this.screenLockActivityTimeout + ", screenSecurity=" + this.screenSecurity + ", incognitoKeyboard=" + this.incognitoKeyboard + ", isObsoletePasswordEnabled=" + this.isObsoletePasswordEnabled + ", isObsoletePasswordTimeoutEnabled=" + this.isObsoletePasswordTimeoutEnabled + ", obsoletePasswordTimeout=" + this.obsoletePasswordTimeout + ", universalExpireTimer=" + this.universalExpireTimer + ", privateStories=" + this.privateStories + ", isStoriesEnabled=" + this.isStoriesEnabled + ')';
    }

    public PrivacySettingsState(int i, PhoneNumberPrivacyValues.PhoneNumberSharingMode phoneNumberSharingMode, PhoneNumberPrivacyValues.PhoneNumberListingMode phoneNumberListingMode, boolean z, boolean z2, boolean z3, long j, boolean z4, boolean z5, boolean z6, boolean z7, int i2, int i3, List<DistributionListPartialRecord> list, boolean z8) {
        Intrinsics.checkNotNullParameter(phoneNumberSharingMode, "seeMyPhoneNumber");
        Intrinsics.checkNotNullParameter(phoneNumberListingMode, "findMeByPhoneNumber");
        Intrinsics.checkNotNullParameter(list, "privateStories");
        this.blockedCount = i;
        this.seeMyPhoneNumber = phoneNumberSharingMode;
        this.findMeByPhoneNumber = phoneNumberListingMode;
        this.readReceipts = z;
        this.typingIndicators = z2;
        this.screenLock = z3;
        this.screenLockActivityTimeout = j;
        this.screenSecurity = z4;
        this.incognitoKeyboard = z5;
        this.isObsoletePasswordEnabled = z6;
        this.isObsoletePasswordTimeoutEnabled = z7;
        this.obsoletePasswordTimeout = i2;
        this.universalExpireTimer = i3;
        this.privateStories = list;
        this.isStoriesEnabled = z8;
    }

    public final int getBlockedCount() {
        return this.blockedCount;
    }

    public final PhoneNumberPrivacyValues.PhoneNumberSharingMode getSeeMyPhoneNumber() {
        return this.seeMyPhoneNumber;
    }

    public final PhoneNumberPrivacyValues.PhoneNumberListingMode getFindMeByPhoneNumber() {
        return this.findMeByPhoneNumber;
    }

    public final boolean getReadReceipts() {
        return this.readReceipts;
    }

    public final boolean getTypingIndicators() {
        return this.typingIndicators;
    }

    public final boolean getScreenLock() {
        return this.screenLock;
    }

    public final long getScreenLockActivityTimeout() {
        return this.screenLockActivityTimeout;
    }

    public final boolean getScreenSecurity() {
        return this.screenSecurity;
    }

    public final boolean getIncognitoKeyboard() {
        return this.incognitoKeyboard;
    }

    public final boolean isObsoletePasswordEnabled() {
        return this.isObsoletePasswordEnabled;
    }

    public final boolean isObsoletePasswordTimeoutEnabled() {
        return this.isObsoletePasswordTimeoutEnabled;
    }

    public final int getObsoletePasswordTimeout() {
        return this.obsoletePasswordTimeout;
    }

    public final int getUniversalExpireTimer() {
        return this.universalExpireTimer;
    }

    public final List<DistributionListPartialRecord> getPrivateStories() {
        return this.privateStories;
    }

    public final boolean isStoriesEnabled() {
        return this.isStoriesEnabled;
    }
}
