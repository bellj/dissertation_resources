package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.FragmentKt;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.BoldSelectionTabItem;
import org.thoughtcrime.securesms.components.ControllableTabLayout;

/* compiled from: DonationReceiptListFragment.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListFragment;", "Landroidx/fragment/app/Fragment;", "()V", "onViewCreated", "", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListFragment extends Fragment {
    public DonationReceiptListFragment() {
        super(R.layout.donation_receipt_list_fragment);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        View findViewById = view.findViewById(R.id.pager);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.pager)");
        ViewPager2 viewPager2 = (ViewPager2) findViewById;
        View findViewById2 = view.findViewById(R.id.tabs);
        Intrinsics.checkNotNullExpressionValue(findViewById2, "view.findViewById(R.id.tabs)");
        ControllableTabLayout controllableTabLayout = (ControllableTabLayout) findViewById2;
        View findViewById3 = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById3, "view.findViewById(R.id.toolbar)");
        ((Toolbar) findViewById3).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                DonationReceiptListFragment.m1017$r8$lambda$Kh3L2XwvKRI3LRF9An75UTvOdY(DonationReceiptListFragment.this, view2);
            }
        });
        viewPager2.setAdapter(new DonationReceiptListPageAdapter(this));
        BoldSelectionTabItem.Companion.registerListeners(controllableTabLayout);
        new TabLayoutMediator(controllableTabLayout, viewPager2, new TabLayoutMediator.TabConfigurationStrategy() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListFragment$$ExternalSyntheticLambda1
            @Override // com.google.android.material.tabs.TabLayoutMediator.TabConfigurationStrategy
            public final void onConfigureTab(TabLayout.Tab tab, int i) {
                DonationReceiptListFragment.m1018$r8$lambda$mfwuqyjH9KghAh0FQ5xaT_5qwA(tab, i);
            }
        }).attach();
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m1019onViewCreated$lambda0(DonationReceiptListFragment donationReceiptListFragment, View view) {
        Intrinsics.checkNotNullParameter(donationReceiptListFragment, "this$0");
        FragmentKt.findNavController(donationReceiptListFragment).popBackStack();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final void m1020onViewCreated$lambda1(TabLayout.Tab tab, int i) {
        int i2;
        Intrinsics.checkNotNullParameter(tab, "tab");
        if (i == 0) {
            i2 = R.string.DonationReceiptListFragment__all;
        } else if (i == 1) {
            i2 = R.string.DonationReceiptListFragment__recurring;
        } else if (i == 2) {
            i2 = R.string.DonationReceiptListFragment__one_time;
        } else if (i == 3) {
            i2 = R.string.DonationReceiptListFragment__gift;
        } else {
            throw new IllegalStateException(("Unsupported index " + i).toString());
        }
        tab.setText(i2);
    }
}
