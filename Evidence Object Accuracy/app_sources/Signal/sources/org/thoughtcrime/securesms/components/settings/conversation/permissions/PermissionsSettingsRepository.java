package org.thoughtcrime.securesms.components.settings.conversation.permissions;

import android.content.Context;
import java.io.IOException;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.groups.GroupAccessControl;
import org.thoughtcrime.securesms.groups.GroupChangeException;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.GroupManager;
import org.thoughtcrime.securesms.groups.ui.GroupChangeErrorCallback;
import org.thoughtcrime.securesms.groups.ui.GroupChangeFailureReason;

/* compiled from: PermissionsSettingsRepository.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u001e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u001e\u0010\r\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\fJ\u001e\u0010\u0010\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u000b\u001a\u00020\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/permissions/PermissionsSettingsRepository;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "applyAnnouncementGroupChange", "", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "isAnnouncementGroup", "", "error", "Lorg/thoughtcrime/securesms/groups/ui/GroupChangeErrorCallback;", "applyAttributesRightsChange", "newRights", "Lorg/thoughtcrime/securesms/groups/GroupAccessControl;", "applyMembershipRightsChange", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PermissionsSettingsRepository {
    private final Context context;

    public PermissionsSettingsRepository(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        this.context = context;
    }

    public final void applyMembershipRightsChange(GroupId groupId, GroupAccessControl groupAccessControl, GroupChangeErrorCallback groupChangeErrorCallback) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(groupAccessControl, "newRights");
        Intrinsics.checkNotNullParameter(groupChangeErrorCallback, "error");
        SignalExecutors.UNBOUNDED.execute(new Runnable(groupId, groupAccessControl, groupChangeErrorCallback) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsRepository$$ExternalSyntheticLambda1
            public final /* synthetic */ GroupId f$1;
            public final /* synthetic */ GroupAccessControl f$2;
            public final /* synthetic */ GroupChangeErrorCallback f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PermissionsSettingsRepository.$r8$lambda$ce72Yb3PY34mHYllC2FsehXyYS8(PermissionsSettingsRepository.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: applyMembershipRightsChange$lambda-0 */
    public static final void m1184applyMembershipRightsChange$lambda0(PermissionsSettingsRepository permissionsSettingsRepository, GroupId groupId, GroupAccessControl groupAccessControl, GroupChangeErrorCallback groupChangeErrorCallback) {
        Intrinsics.checkNotNullParameter(permissionsSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(groupAccessControl, "$newRights");
        Intrinsics.checkNotNullParameter(groupChangeErrorCallback, "$error");
        try {
            GroupManager.applyMembershipAdditionRightsChange(permissionsSettingsRepository.context, groupId.requireV2(), groupAccessControl);
        } catch (IOException e) {
            Log.w(PermissionsSettingsRepositoryKt.TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
        } catch (GroupChangeException e2) {
            Log.w(PermissionsSettingsRepositoryKt.TAG, e2);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e2));
        }
    }

    public final void applyAttributesRightsChange(GroupId groupId, GroupAccessControl groupAccessControl, GroupChangeErrorCallback groupChangeErrorCallback) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(groupAccessControl, "newRights");
        Intrinsics.checkNotNullParameter(groupChangeErrorCallback, "error");
        SignalExecutors.UNBOUNDED.execute(new Runnable(groupId, groupAccessControl, groupChangeErrorCallback) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsRepository$$ExternalSyntheticLambda0
            public final /* synthetic */ GroupId f$1;
            public final /* synthetic */ GroupAccessControl f$2;
            public final /* synthetic */ GroupChangeErrorCallback f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PermissionsSettingsRepository.m1180$r8$lambda$2udCPQkOXHaijCHw0VvrLlRv4g(PermissionsSettingsRepository.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: applyAttributesRightsChange$lambda-1 */
    public static final void m1183applyAttributesRightsChange$lambda1(PermissionsSettingsRepository permissionsSettingsRepository, GroupId groupId, GroupAccessControl groupAccessControl, GroupChangeErrorCallback groupChangeErrorCallback) {
        Intrinsics.checkNotNullParameter(permissionsSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(groupAccessControl, "$newRights");
        Intrinsics.checkNotNullParameter(groupChangeErrorCallback, "$error");
        try {
            GroupManager.applyAttributesRightsChange(permissionsSettingsRepository.context, groupId.requireV2(), groupAccessControl);
        } catch (IOException e) {
            Log.w(PermissionsSettingsRepositoryKt.TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
        } catch (GroupChangeException e2) {
            Log.w(PermissionsSettingsRepositoryKt.TAG, e2);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e2));
        }
    }

    public final void applyAnnouncementGroupChange(GroupId groupId, boolean z, GroupChangeErrorCallback groupChangeErrorCallback) {
        Intrinsics.checkNotNullParameter(groupId, "groupId");
        Intrinsics.checkNotNullParameter(groupChangeErrorCallback, "error");
        SignalExecutors.UNBOUNDED.execute(new Runnable(groupId, z, groupChangeErrorCallback) { // from class: org.thoughtcrime.securesms.components.settings.conversation.permissions.PermissionsSettingsRepository$$ExternalSyntheticLambda2
            public final /* synthetic */ GroupId f$1;
            public final /* synthetic */ boolean f$2;
            public final /* synthetic */ GroupChangeErrorCallback f$3;

            {
                this.f$1 = r2;
                this.f$2 = r3;
                this.f$3 = r4;
            }

            @Override // java.lang.Runnable
            public final void run() {
                PermissionsSettingsRepository.m1181$r8$lambda$dDvJpQxb0oELbd7rOSvKKhbh7s(PermissionsSettingsRepository.this, this.f$1, this.f$2, this.f$3);
            }
        });
    }

    /* renamed from: applyAnnouncementGroupChange$lambda-2 */
    public static final void m1182applyAnnouncementGroupChange$lambda2(PermissionsSettingsRepository permissionsSettingsRepository, GroupId groupId, boolean z, GroupChangeErrorCallback groupChangeErrorCallback) {
        Intrinsics.checkNotNullParameter(permissionsSettingsRepository, "this$0");
        Intrinsics.checkNotNullParameter(groupId, "$groupId");
        Intrinsics.checkNotNullParameter(groupChangeErrorCallback, "$error");
        try {
            GroupManager.applyAnnouncementGroupChange(permissionsSettingsRepository.context, groupId.requireV2(), z);
        } catch (IOException e) {
            Log.w(PermissionsSettingsRepositoryKt.TAG, e);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e));
        } catch (GroupChangeException e2) {
            Log.w(PermissionsSettingsRepositoryKt.TAG, e2);
            groupChangeErrorCallback.onError(GroupChangeFailureReason.fromException(e2));
        }
    }
}
