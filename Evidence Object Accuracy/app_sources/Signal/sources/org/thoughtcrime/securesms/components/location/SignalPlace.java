package org.thoughtcrime.securesms.components.location;

import android.net.Uri;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;
import java.io.IOException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.maps.AddressData;
import org.thoughtcrime.securesms.util.JsonUtils;

/* loaded from: classes.dex */
public class SignalPlace {
    private static final String TAG = Log.tag(SignalPlace.class);
    private static final String URL;
    @JsonProperty
    private CharSequence address;
    @JsonProperty
    private double latitude;
    @JsonProperty
    private double longitude;
    @JsonProperty
    private CharSequence name;

    public SignalPlace(AddressData addressData) {
        this.name = "";
        this.address = addressData.getAddress();
        this.latitude = addressData.getLatitude();
        this.longitude = addressData.getLongitude();
    }

    @JsonCreator
    public SignalPlace() {
    }

    @JsonIgnore
    public LatLng getLatLong() {
        return new LatLng(this.latitude, this.longitude);
    }

    @JsonIgnore
    public String getDescription() {
        String str = "";
        if (!TextUtils.isEmpty(this.name)) {
            str = str + ((Object) this.name) + "\n";
        }
        if (!TextUtils.isEmpty(this.address)) {
            str = str + ((Object) this.address) + "\n";
        }
        return str + Uri.parse(URL).buildUpon().appendQueryParameter("q", String.format("%s,%s", Double.valueOf(this.latitude), Double.valueOf(this.longitude))).build().toString();
    }

    public String serialize() {
        try {
            return JsonUtils.toJson(this);
        } catch (IOException e) {
            Log.w(TAG, e);
            return null;
        }
    }

    public static SignalPlace deserialize(String str) throws IOException {
        return (SignalPlace) JsonUtils.fromJson(str, SignalPlace.class);
    }
}
