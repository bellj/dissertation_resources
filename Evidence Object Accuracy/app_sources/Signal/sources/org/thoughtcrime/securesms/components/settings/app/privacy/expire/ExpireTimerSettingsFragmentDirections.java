package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ExpireTimerSettingsFragmentDirections {
    private ExpireTimerSettingsFragmentDirections() {
    }

    public static NavDirections actionExpireTimerSettingsFragmentToCustomExpireTimerSelectDialog() {
        return new ActionOnlyNavDirections(R.id.action_expireTimerSettingsFragment_to_customExpireTimerSelectDialog);
    }
}
