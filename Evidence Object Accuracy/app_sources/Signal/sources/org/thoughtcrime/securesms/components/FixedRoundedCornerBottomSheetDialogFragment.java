package org.thoughtcrime.securesms.components;

import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.View;
import androidx.core.view.ViewCompat;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ThemeUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: FixedRoundedCornerBottomSheetDialogFragment.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b&\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016J\u0012\u0010\u0015\u001a\u00020\u00162\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014H\u0016R\u001e\u0010\u0003\u001a\u00020\u00048\u0004@\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX.¢\u0006\u0002\n\u0000R\u0014\u0010\u000b\u001a\u00020\fXD¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0016\u0010\u000f\u001a\u00020\u00048\u0014XD¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/FixedRoundedCornerBottomSheetDialogFragment;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "()V", "backgroundColor", "", "getBackgroundColor", "()I", "setBackgroundColor", "(I)V", "dialogBackground", "Lcom/google/android/material/shape/MaterialShapeDrawable;", "peekHeightPercentage", "", "getPeekHeightPercentage", "()F", "themeResId", "getThemeResId", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateDialog", "Landroid/app/Dialog;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class FixedRoundedCornerBottomSheetDialogFragment extends BottomSheetDialogFragment {
    private int backgroundColor;
    private MaterialShapeDrawable dialogBackground;
    private final float peekHeightPercentage = 0.5f;
    private final int themeResId = R.style.Widget_Signal_FixedRoundedCorners;

    protected float getPeekHeightPercentage() {
        return this.peekHeightPercentage;
    }

    public int getThemeResId() {
        return this.themeResId;
    }

    public final int getBackgroundColor() {
        return this.backgroundColor;
    }

    protected final void setBackgroundColor(int i) {
        this.backgroundColor = i;
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(0, getThemeResId());
    }

    @Override // com.google.android.material.bottomsheet.BottomSheetDialogFragment, androidx.appcompat.app.AppCompatDialogFragment, androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(bundle);
        bottomSheetDialog.getBehavior().setPeekHeight((int) (((float) getResources().getDisplayMetrics().heightPixels) * getPeekHeightPercentage()));
        ShapeAppearanceModel build = ShapeAppearanceModel.builder().setTopLeftCorner(0, (float) ViewUtil.dpToPx(requireContext(), 18)).setTopRightCorner(0, (float) ViewUtil.dpToPx(requireContext(), 18)).build();
        Intrinsics.checkNotNullExpressionValue(build, "builder()\n      .setTopL…toFloat())\n      .build()");
        this.dialogBackground = new MaterialShapeDrawable(build);
        this.backgroundColor = ThemeUtil.getThemedColor(new ContextThemeWrapper(requireContext(), ThemeUtil.getThemedResourceId(new ContextThemeWrapper(requireContext(), getThemeResId()), R.attr.bottomSheetStyle)), R.attr.backgroundTint);
        MaterialShapeDrawable materialShapeDrawable = this.dialogBackground;
        if (materialShapeDrawable == null) {
            Intrinsics.throwUninitializedPropertyAccessException("dialogBackground");
            materialShapeDrawable = null;
        }
        materialShapeDrawable.setFillColor(ColorStateList.valueOf(this.backgroundColor));
        bottomSheetDialog.getBehavior().addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback(this) { // from class: org.thoughtcrime.securesms.components.FixedRoundedCornerBottomSheetDialogFragment$onCreateDialog$1
            final /* synthetic */ FixedRoundedCornerBottomSheetDialogFragment this$0;

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onSlide(View view, float f) {
                Intrinsics.checkNotNullParameter(view, "bottomSheet");
            }

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
            public void onStateChanged(View view, int i) {
                Intrinsics.checkNotNullParameter(view, "bottomSheet");
                Drawable background = view.getBackground();
                MaterialShapeDrawable materialShapeDrawable2 = this.this$0.dialogBackground;
                MaterialShapeDrawable materialShapeDrawable3 = null;
                if (materialShapeDrawable2 == null) {
                    Intrinsics.throwUninitializedPropertyAccessException("dialogBackground");
                    materialShapeDrawable2 = null;
                }
                if (background != materialShapeDrawable2) {
                    MaterialShapeDrawable materialShapeDrawable4 = this.this$0.dialogBackground;
                    if (materialShapeDrawable4 == null) {
                        Intrinsics.throwUninitializedPropertyAccessException("dialogBackground");
                    } else {
                        materialShapeDrawable3 = materialShapeDrawable4;
                    }
                    ViewCompat.setBackground(view, materialShapeDrawable3);
                }
            }
        });
        return bottomSheetDialog;
    }
}
