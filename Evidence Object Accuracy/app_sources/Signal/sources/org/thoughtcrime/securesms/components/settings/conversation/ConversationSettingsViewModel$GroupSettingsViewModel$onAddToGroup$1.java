package org.thoughtcrime.securesms.components.settings.conversation;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsEvent;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;

/* compiled from: ConversationSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", "capacityResult", "Lorg/thoughtcrime/securesms/components/settings/conversation/GroupCapacityResult;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
final class ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroup$1 extends Lambda implements Function1<GroupCapacityResult, Unit> {
    final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroup$1(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel) {
        super(1);
        this.this$0 = groupSettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(GroupCapacityResult groupCapacityResult) {
        invoke(groupCapacityResult);
        return Unit.INSTANCE;
    }

    public final void invoke(GroupCapacityResult groupCapacityResult) {
        Intrinsics.checkNotNullParameter(groupCapacityResult, "capacityResult");
        if (groupCapacityResult.getRemainingCapacity() > 0) {
            this.this$0.getInternalEvents().postValue(new ConversationSettingsEvent.AddMembersToGroup(this.this$0.groupId, groupCapacityResult.getSelectionWarning(), groupCapacityResult.getSelectionLimit(), groupCapacityResult.isAnnouncementGroup(), groupCapacityResult.getMembersWithoutSelf()));
        } else {
            this.this$0.getInternalEvents().postValue(ConversationSettingsEvent.ShowGroupHardLimitDialog.INSTANCE);
        }
    }
}
