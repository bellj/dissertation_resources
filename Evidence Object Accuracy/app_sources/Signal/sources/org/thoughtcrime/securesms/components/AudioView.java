package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.core.util.Consumer;
import androidx.lifecycle.Observer;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieProperty;
import com.airbnb.lottie.SimpleColorFilter;
import com.airbnb.lottie.model.KeyPath;
import com.airbnb.lottie.value.LottieValueCallback;
import com.pnikosis.materialishprogress.ProgressWheel;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.audio.AudioWaveForm;
import org.thoughtcrime.securesms.components.voice.VoiceNotePlaybackState;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.mms.AudioSlide;
import org.thoughtcrime.securesms.mms.SlideClickListener;

/* loaded from: classes.dex */
public final class AudioView extends FrameLayout {
    private static final int FORWARDS;
    private static final int MODE_DRAFT;
    private static final int MODE_NORMAL;
    private static final int MODE_SMALL;
    private static final int REVERSE;
    private static final String TAG = Log.tag(AudioView.class);
    private AudioSlide audioSlide;
    private final boolean autoRewind;
    private int backwardsCounter;
    private Callbacks callbacks;
    private final ProgressWheel circleProgress;
    private final AnimatingToggle controlToggle;
    private final ImageView downloadButton;
    private SlideClickListener downloadListener;
    private final TextView duration;
    private long durationMillis;
    private boolean isPlaying;
    private int lottieDirection;
    private final LottieAnimationView playPauseButton;
    private final Observer<VoiceNotePlaybackState> playbackStateObserver;
    private final View progressAndPlay;
    private final SeekBar seekBar;
    private final boolean smallView;
    private final int waveFormPlayedBarsColor;
    private final int waveFormThumbTint;
    private final int waveFormUnplayedBarsColor;

    /* loaded from: classes4.dex */
    public interface Callbacks {
        void onPause(Uri uri);

        void onPlay(Uri uri, double d);

        void onProgressUpdated(long j, long j2);

        void onSeekTo(Uri uri, double d);

        void onSpeedChanged(float f, boolean z);

        void onStopAndReset(Uri uri);
    }

    public AudioView(Context context) {
        this(context, null);
    }

    public AudioView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public AudioView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        Throwable th;
        TypedArray obtainStyledAttributes;
        this.playbackStateObserver = new Observer() { // from class: org.thoughtcrime.securesms.components.AudioView$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AudioView.m472$r8$lambda$XJEhzUKmcVbwHnyUunBdUzuDWE(AudioView.this, (VoiceNotePlaybackState) obj);
            }
        };
        setLayoutDirection(0);
        TypedArray typedArray = null;
        try {
            obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.AudioView, 0, 0);
        } catch (Throwable th2) {
            th = th2;
        }
        try {
            int integer = obtainStyledAttributes.getInteger(0, 0);
            this.smallView = integer == 1;
            this.autoRewind = obtainStyledAttributes.getBoolean(2, false);
            if (integer == 0) {
                FrameLayout.inflate(context, R.layout.audio_view, this);
            } else if (integer == 1) {
                FrameLayout.inflate(context, R.layout.audio_view_small, this);
            } else if (integer == 2) {
                FrameLayout.inflate(context, R.layout.audio_view_draft, this);
            } else {
                throw new IllegalStateException("Unsupported mode: " + integer);
            }
            this.controlToggle = (AnimatingToggle) findViewById(R.id.control_toggle);
            LottieAnimationView lottieAnimationView = (LottieAnimationView) findViewById(R.id.play);
            this.playPauseButton = lottieAnimationView;
            this.progressAndPlay = findViewById(R.id.progress_and_play);
            this.downloadButton = (ImageView) findViewById(R.id.download);
            this.circleProgress = (ProgressWheel) findViewById(R.id.circle_progress);
            SeekBar seekBar = (SeekBar) findViewById(R.id.seek);
            this.seekBar = seekBar;
            this.duration = (TextView) findViewById(R.id.duration);
            this.lottieDirection = -1;
            lottieAnimationView.setOnClickListener(new PlayPauseClickedListener());
            lottieAnimationView.setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.components.AudioView$$ExternalSyntheticLambda1
                @Override // android.view.View.OnLongClickListener
                public final boolean onLongClick(View view) {
                    return AudioView.m471$r8$lambda$Uf4jjIArTB7PyPL0FhyTtsPYK0(AudioView.this, view);
                }
            });
            seekBar.setOnSeekBarChangeListener(new SeekBarModifiedListener());
            setTint(obtainStyledAttributes.getColor(4, -1));
            this.waveFormPlayedBarsColor = obtainStyledAttributes.getColor(6, -1);
            this.waveFormUnplayedBarsColor = obtainStyledAttributes.getColor(8, -1);
            this.waveFormThumbTint = obtainStyledAttributes.getColor(7, -1);
            setProgressAndPlayBackgroundTint(obtainStyledAttributes.getColor(5, -16777216));
            obtainStyledAttributes.recycle();
        } catch (Throwable th3) {
            th = th3;
            typedArray = obtainStyledAttributes;
            if (typedArray != null) {
                typedArray.recycle();
            }
            throw th;
        }
    }

    public /* synthetic */ boolean lambda$new$0(View view) {
        return performLongClick();
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    public void setProgressAndPlayBackgroundTint(int i) {
        this.progressAndPlay.getBackground().setColorFilter(i, PorterDuff.Mode.SRC_IN);
    }

    public Observer<VoiceNotePlaybackState> getPlaybackStateObserver() {
        return this.playbackStateObserver;
    }

    public void setAudio(AudioSlide audioSlide, Callbacks callbacks, boolean z, boolean z2) {
        TextView textView;
        AudioSlide audioSlide2;
        this.callbacks = callbacks;
        TextView textView2 = this.duration;
        if (textView2 != null) {
            textView2.setVisibility(0);
        }
        if ((this.seekBar instanceof WaveFormSeekBarView) && (audioSlide2 = this.audioSlide) != null && !Objects.equals(audioSlide2.getUri(), audioSlide.getUri())) {
            ((WaveFormSeekBarView) this.seekBar).setWaveMode(false);
            this.seekBar.setProgress(0);
            this.durationMillis = 0;
        }
        if (z && audioSlide.isPendingDownload()) {
            this.controlToggle.displayQuick(this.downloadButton);
            this.seekBar.setEnabled(false);
            this.downloadButton.setOnClickListener(new DownloadClickedListener(audioSlide));
            ProgressWheel progressWheel = this.circleProgress;
            if (progressWheel != null) {
                if (progressWheel.isSpinning()) {
                    this.circleProgress.stopSpinning();
                }
                this.circleProgress.setVisibility(8);
            }
        } else if (!z || audioSlide.getTransferState() != 1) {
            this.seekBar.setEnabled(true);
            ProgressWheel progressWheel2 = this.circleProgress;
            if (progressWheel2 != null && progressWheel2.isSpinning()) {
                this.circleProgress.stopSpinning();
            }
            showPlayButton();
        } else {
            this.controlToggle.displayQuick(this.progressAndPlay);
            this.seekBar.setEnabled(false);
            ProgressWheel progressWheel3 = this.circleProgress;
            if (progressWheel3 != null) {
                progressWheel3.setVisibility(0);
                this.circleProgress.spin();
            }
        }
        this.audioSlide = audioSlide;
        SeekBar seekBar = this.seekBar;
        if (seekBar instanceof WaveFormSeekBarView) {
            WaveFormSeekBarView waveFormSeekBarView = (WaveFormSeekBarView) seekBar;
            waveFormSeekBarView.setColors(this.waveFormPlayedBarsColor, this.waveFormUnplayedBarsColor, this.waveFormThumbTint);
            if (Build.VERSION.SDK_INT >= 23) {
                new AudioWaveForm(getContext(), audioSlide).getWaveForm(new Consumer(z2, waveFormSeekBarView) { // from class: org.thoughtcrime.securesms.components.AudioView$$ExternalSyntheticLambda2
                    public final /* synthetic */ boolean f$1;
                    public final /* synthetic */ WaveFormSeekBarView f$2;

                    {
                        this.f$1 = r2;
                        this.f$2 = r3;
                    }

                    @Override // androidx.core.util.Consumer
                    public final void accept(Object obj) {
                        AudioView.$r8$lambda$lHgFLnxGv9e7YKpR__7FUfEVe0o(AudioView.this, this.f$1, this.f$2, (AudioWaveForm.AudioFileInfo) obj);
                    }
                }, new Runnable() { // from class: org.thoughtcrime.securesms.components.AudioView$$ExternalSyntheticLambda3
                    @Override // java.lang.Runnable
                    public final void run() {
                        AudioView.$r8$lambda$48Z69pY97rHaNns738BMYPOctYE(WaveFormSeekBarView.this);
                    }
                });
            } else {
                waveFormSeekBarView.setWaveMode(false);
                TextView textView3 = this.duration;
                if (textView3 != null) {
                    textView3.setVisibility(8);
                }
            }
        }
        if (z2 && (textView = this.duration) != null) {
            textView.setVisibility(8);
        }
    }

    public /* synthetic */ void lambda$setAudio$1(boolean z, WaveFormSeekBarView waveFormSeekBarView, AudioWaveForm.AudioFileInfo audioFileInfo) {
        TextView textView;
        this.durationMillis = audioFileInfo.getDuration(TimeUnit.MILLISECONDS);
        updateProgress(0.0f, 0);
        if (!z && (textView = this.duration) != null) {
            textView.setVisibility(0);
        }
        waveFormSeekBarView.setWaveData(audioFileInfo.getWaveForm());
    }

    public void setDownloadClickListener(SlideClickListener slideClickListener) {
        this.downloadListener = slideClickListener;
    }

    public Uri getAudioSlideUri() {
        AudioSlide audioSlide = this.audioSlide;
        if (audioSlide != null) {
            return audioSlide.getUri();
        }
        return null;
    }

    public void onPlaybackState(VoiceNotePlaybackState voiceNotePlaybackState) {
        onDuration(voiceNotePlaybackState.getUri(), voiceNotePlaybackState.getTrackDuration());
        Uri uri = voiceNotePlaybackState.getUri();
        double playheadPositionMillis = (double) voiceNotePlaybackState.getPlayheadPositionMillis();
        double trackDuration = (double) voiceNotePlaybackState.getTrackDuration();
        Double.isNaN(playheadPositionMillis);
        Double.isNaN(trackDuration);
        onProgress(uri, playheadPositionMillis / trackDuration, voiceNotePlaybackState.getPlayheadPositionMillis());
        onSpeedChanged(voiceNotePlaybackState.getUri(), voiceNotePlaybackState.getSpeed());
        onStart(voiceNotePlaybackState.getUri(), voiceNotePlaybackState.isPlaying(), voiceNotePlaybackState.isAutoReset());
    }

    private void onDuration(Uri uri, long j) {
        if (isTarget(uri)) {
            this.durationMillis = j;
        }
    }

    private void onStart(Uri uri, boolean z, boolean z2) {
        if (!isTarget(uri) || !z) {
            if (hasAudioUri()) {
                onStop(this.audioSlide.getUri(), z2);
            }
        } else if (!this.isPlaying) {
            this.isPlaying = true;
            togglePlayToPause();
        }
    }

    private void onStop(Uri uri, boolean z) {
        if (isTarget(uri) && this.isPlaying) {
            this.isPlaying = false;
            togglePauseToPlay();
            if (z || this.autoRewind || this.seekBar.getProgress() + 5 >= this.seekBar.getMax()) {
                this.backwardsCounter = 4;
                rewind();
            }
        }
    }

    private void onProgress(Uri uri, double d, long j) {
        int i;
        if (isTarget(uri)) {
            double max = (double) this.seekBar.getMax();
            Double.isNaN(max);
            int floor = (int) Math.floor(max * d);
            if (floor > this.seekBar.getProgress() || (i = this.backwardsCounter) > 3) {
                this.backwardsCounter = 0;
                this.seekBar.setProgress(floor);
                updateProgress((float) d, j);
                return;
            }
            this.backwardsCounter = i + 1;
        }
    }

    private void onSpeedChanged(Uri uri, float f) {
        Callbacks callbacks = this.callbacks;
        if (callbacks != null) {
            callbacks.onSpeedChanged(f, isTarget(uri));
        }
    }

    private boolean isTarget(Uri uri) {
        return hasAudioUri() && Objects.equals(uri, this.audioSlide.getUri());
    }

    private boolean hasAudioUri() {
        AudioSlide audioSlide = this.audioSlide;
        return (audioSlide == null || audioSlide.getUri() == null) ? false : true;
    }

    @Override // android.view.View
    public void setFocusable(boolean z) {
        super.setFocusable(z);
        this.playPauseButton.setFocusable(z);
        this.seekBar.setFocusable(z);
        this.seekBar.setFocusableInTouchMode(z);
        this.downloadButton.setFocusable(z);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        super.setClickable(z);
        this.playPauseButton.setClickable(z);
        this.seekBar.setClickable(z);
        SeekBar seekBar = this.seekBar;
        TouchIgnoringListener touchIgnoringListener = null;
        if (!z) {
            touchIgnoringListener = new TouchIgnoringListener();
        }
        seekBar.setOnTouchListener(touchIgnoringListener);
        this.downloadButton.setClickable(z);
    }

    @Override // android.view.View
    public void setEnabled(boolean z) {
        super.setEnabled(z);
        this.playPauseButton.setEnabled(z);
        this.seekBar.setEnabled(z);
        this.downloadButton.setEnabled(z);
    }

    private void updateProgress(float f, long j) {
        ProgressWheel progressWheel;
        Callbacks callbacks = this.callbacks;
        if (callbacks != null) {
            callbacks.onProgressUpdated(this.durationMillis, j);
        }
        if (this.duration != null) {
            long j2 = this.durationMillis;
            if (j2 > 0) {
                long max = Math.max(0L, TimeUnit.MILLISECONDS.toSeconds(j2 - j));
                this.duration.setText(getResources().getString(R.string.AudioView_duration, Long.valueOf(max / 60), Long.valueOf(max % 60)));
            }
        }
        if (this.smallView && (progressWheel = this.circleProgress) != null) {
            if (this.seekBar.getProgress() == 0) {
                f = 1.0f;
            }
            progressWheel.setInstantProgress(f);
        }
    }

    public /* synthetic */ void lambda$setTint$3(int i) {
        this.playPauseButton.addValueCallback(new KeyPath("**"), (KeyPath) LottieProperty.COLOR_FILTER, (LottieValueCallback<KeyPath>) new LottieValueCallback(new SimpleColorFilter(i)));
    }

    public void setTint(int i) {
        post(new Runnable(i) { // from class: org.thoughtcrime.securesms.components.AudioView$$ExternalSyntheticLambda4
            public final /* synthetic */ int f$1;

            {
                this.f$1 = r2;
            }

            @Override // java.lang.Runnable
            public final void run() {
                AudioView.m473$r8$lambda$k9G08Y05mY7W501ZZPymgbsZSc(AudioView.this, this.f$1);
            }
        });
        this.downloadButton.setColorFilter(i, PorterDuff.Mode.SRC_IN);
        ProgressWheel progressWheel = this.circleProgress;
        if (progressWheel != null) {
            progressWheel.setBarColor(i);
        }
        TextView textView = this.duration;
        if (textView != null) {
            textView.setTextColor(i);
        }
        this.seekBar.getProgressDrawable().setColorFilter(i, PorterDuff.Mode.SRC_IN);
        this.seekBar.getThumb().setColorFilter(i, PorterDuff.Mode.SRC_IN);
    }

    public void getSeekBarGlobalVisibleRect(Rect rect) {
        this.seekBar.getGlobalVisibleRect(rect);
    }

    public double getProgress() {
        if (this.seekBar.getProgress() <= 0 || this.seekBar.getMax() <= 0) {
            return 0.0d;
        }
        double progress = (double) this.seekBar.getProgress();
        double max = (double) this.seekBar.getMax();
        Double.isNaN(progress);
        Double.isNaN(max);
        return progress / max;
    }

    private void togglePlayToPause() {
        startLottieAnimation(1);
    }

    private void togglePauseToPlay() {
        startLottieAnimation(-1);
    }

    private void startLottieAnimation(int i) {
        showPlayButton();
        if (this.lottieDirection != i) {
            this.lottieDirection = i;
            this.playPauseButton.pauseAnimation();
            this.playPauseButton.setSpeed((float) (i * 2));
            this.playPauseButton.resumeAnimation();
        }
    }

    private void showPlayButton() {
        ProgressWheel progressWheel = this.circleProgress;
        if (progressWheel != null) {
            if (!this.smallView) {
                progressWheel.setVisibility(8);
            } else if (this.seekBar.getProgress() == 0) {
                this.circleProgress.setInstantProgress(1.0f);
            }
        }
        this.playPauseButton.setVisibility(0);
        this.controlToggle.displayQuick(this.progressAndPlay);
    }

    public void stopPlaybackAndReset() {
        Callbacks callbacks;
        AudioSlide audioSlide = this.audioSlide;
        if (audioSlide != null && audioSlide.getUri() != null && (callbacks = this.callbacks) != null) {
            callbacks.onStopAndReset(this.audioSlide.getUri());
            rewind();
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class PlayPauseClickedListener implements View.OnClickListener {
        private PlayPauseClickedListener() {
            AudioView.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (AudioView.this.audioSlide != null && AudioView.this.audioSlide.getUri() != null && AudioView.this.callbacks != null) {
                if (AudioView.this.lottieDirection == -1) {
                    AudioView.this.callbacks.onPlay(AudioView.this.audioSlide.getUri(), AudioView.this.getProgress());
                } else {
                    AudioView.this.callbacks.onPause(AudioView.this.audioSlide.getUri());
                }
            }
        }
    }

    private void rewind() {
        this.seekBar.setProgress(0);
        updateProgress(0.0f, 0);
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class DownloadClickedListener implements View.OnClickListener {
        private final AudioSlide slide;

        private DownloadClickedListener(AudioSlide audioSlide) {
            AudioView.this = r1;
            this.slide = audioSlide;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (AudioView.this.downloadListener != null) {
                AudioView.this.downloadListener.onClick(view, this.slide);
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class SeekBarModifiedListener implements SeekBar.OnSeekBarChangeListener {
        private boolean wasPlaying;

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
        }

        private SeekBarModifiedListener() {
            AudioView.this = r1;
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public synchronized void onStartTrackingTouch(SeekBar seekBar) {
            if (!(AudioView.this.audioSlide == null || AudioView.this.audioSlide.getUri() == null)) {
                this.wasPlaying = AudioView.this.isPlaying;
                if (AudioView.this.isPlaying && AudioView.this.callbacks != null) {
                    AudioView.this.callbacks.onPause(AudioView.this.audioSlide.getUri());
                }
            }
        }

        @Override // android.widget.SeekBar.OnSeekBarChangeListener
        public synchronized void onStopTrackingTouch(SeekBar seekBar) {
            if (!(AudioView.this.audioSlide == null || AudioView.this.audioSlide.getUri() == null)) {
                if (AudioView.this.callbacks != null) {
                    if (this.wasPlaying) {
                        AudioView.this.callbacks.onSeekTo(AudioView.this.audioSlide.getUri(), AudioView.this.getProgress());
                    } else {
                        Callbacks callbacks = AudioView.this.callbacks;
                        long j = AudioView.this.durationMillis;
                        double d = (double) AudioView.this.durationMillis;
                        double progress = AudioView.this.getProgress();
                        Double.isNaN(d);
                        callbacks.onProgressUpdated(j, Math.round(d * progress));
                    }
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public static class TouchIgnoringListener implements View.OnTouchListener {
        @Override // android.view.View.OnTouchListener
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return true;
        }

        private TouchIgnoringListener() {
        }
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventAsync(PartProgressEvent partProgressEvent) {
        AudioSlide audioSlide = this.audioSlide;
        if (audioSlide != null && this.circleProgress != null && partProgressEvent.attachment.equals(audioSlide.asAttachment())) {
            this.circleProgress.setInstantProgress(((float) partProgressEvent.progress) / ((float) partProgressEvent.total));
        }
    }
}
