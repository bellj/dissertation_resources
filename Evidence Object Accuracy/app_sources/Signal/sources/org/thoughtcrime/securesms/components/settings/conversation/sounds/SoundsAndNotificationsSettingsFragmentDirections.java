package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.navigation.NavDirections;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class SoundsAndNotificationsSettingsFragmentDirections {
    private SoundsAndNotificationsSettingsFragmentDirections() {
    }

    public static ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment(RecipientId recipientId) {
        return new ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment(recipientId);
    }

    /* loaded from: classes4.dex */
    public static class ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment implements NavDirections {
        private final HashMap arguments;

        @Override // androidx.navigation.NavDirections
        public int getActionId() {
            return R.id.action_soundsAndNotificationsSettingsFragment_to_customNotificationsSettingsFragment;
        }

        private ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment(RecipientId recipientId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientId != null) {
                hashMap.put("recipient_id", recipientId);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        public ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment setRecipientId(RecipientId recipientId) {
            if (recipientId != null) {
                this.arguments.put("recipient_id", recipientId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        @Override // androidx.navigation.NavDirections
        public Bundle getArguments() {
            Bundle bundle = new Bundle();
            if (this.arguments.containsKey("recipient_id")) {
                RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
                if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                    bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
                } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                    bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
                } else {
                    throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
                }
            }
            return bundle;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null || getClass() != obj.getClass()) {
                return false;
            }
            ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment = (ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment) obj;
            if (this.arguments.containsKey("recipient_id") != actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment.arguments.containsKey("recipient_id")) {
                return false;
            }
            if (getRecipientId() == null ? actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment.getRecipientId() == null : getRecipientId().equals(actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment.getRecipientId())) {
                return getActionId() == actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment.getActionId();
            }
            return false;
        }

        public int hashCode() {
            return (((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31) + getActionId();
        }

        public String toString() {
            return "ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment(actionId=" + getActionId() + "){recipientId=" + getRecipientId() + "}";
        }
    }
}
