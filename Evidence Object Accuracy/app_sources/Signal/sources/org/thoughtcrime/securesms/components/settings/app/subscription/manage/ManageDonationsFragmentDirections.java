package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ManageDonationsFragmentDirections {
    private ManageDonationsFragmentDirections() {
    }

    public static NavDirections actionManageDonationsFragmentToSubscribeFragment() {
        return new ActionOnlyNavDirections(R.id.action_manageDonationsFragment_to_subscribeFragment);
    }

    public static NavDirections actionManageDonationsFragmentToManageBadges() {
        return new ActionOnlyNavDirections(R.id.action_manageDonationsFragment_to_manage_badges);
    }

    public static NavDirections actionManageDonationsFragmentToBoosts() {
        return new ActionOnlyNavDirections(R.id.action_manageDonationsFragment_to_boosts);
    }

    public static NavDirections actionManageDonationsFragmentToDonationReceiptListFragment() {
        return new ActionOnlyNavDirections(R.id.action_manageDonationsFragment_to_donationReceiptListFragment);
    }

    public static NavDirections actionManageDonationsFragmentToSubscribeLearnMoreBottomSheetDialog() {
        return new ActionOnlyNavDirections(R.id.action_manageDonationsFragment_to_subscribeLearnMoreBottomSheetDialog);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }
}
