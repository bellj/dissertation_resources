package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda1 implements Function {
    public final /* synthetic */ StoryViewState f$0;

    public /* synthetic */ ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda1(StoryViewState storyViewState) {
        this.f$0 = storyViewState;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.RecipientSettingsViewModel.m1153lambda1$lambda0(this.f$0, (ConversationSettingsState) obj);
    }
}
