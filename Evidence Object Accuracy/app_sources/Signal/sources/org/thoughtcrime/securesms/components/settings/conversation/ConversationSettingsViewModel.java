package org.thoughtcrime.securesms.components.settings.conversation;

import android.database.Cursor;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.rxjava3.disposables.CompositeDisposable;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.kotlin.DisposableKt;
import j$.util.Optional;
import j$.util.function.Function;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import kotlin.Metadata;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.collections.CollectionsKt___CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.CursorUtil;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.components.settings.conversation.SpecificSettingsState;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.database.model.StoryViewState;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.LiveGroup;
import org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus;
import org.thoughtcrime.securesms.recipients.LiveRecipient;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.recipients.RecipientUtil;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.SingleLiveEvent;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: ConversationSettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\t\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u000489:;B\u0017\b\u0004\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010%\u001a\u00020\u001cH&J\b\u0010&\u001a\u00020\u001cH\u0016J\b\u0010'\u001a\u00020\u001cH&J$\u0010(\u001a\u00020\u001c2\f\u0010)\u001a\b\u0012\u0004\u0012\u00020+0*2\f\u0010,\u001a\b\u0012\u0004\u0012\u00020\u001c0-H&J\b\u0010.\u001a\u00020\u001cH\u0014J\b\u0010/\u001a\u00020\u001cH\u0016J\u0006\u00100\u001a\u00020\u001cJ\b\u00101\u001a\u00020\u001cH&J\u0010\u00102\u001a\u00020\u001c2\u0006\u00103\u001a\u000204H&J\b\u00105\u001a\u00020\u001cH&J\b\u00106\u001a\u00020\u001cH&J\u000e\u00107\u001a\u00020\u001c*\u0004\u0018\u00010\u0018H\u0002R\u000e\u0010\u0007\u001a\u00020\bX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\t\u001a\u00020\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000f0\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0013X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u001e\u0010\u0016\u001a\u0012\u0012\u0004\u0012\u00020\u00180\u0017j\b\u0012\u0004\u0012\u00020\u0018`\u0019X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001c\u0010\u001a\u001a\u0010\u0012\f\u0012\n \u001d*\u0004\u0018\u00010\u001c0\u001c0\u001bX\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001f0\u000e¢\u0006\b\n\u0000\u001a\u0004\b \u0010\u0011R\u001a\u0010!\u001a\b\u0012\u0004\u0012\u00020\u001f0\"X\u0004¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$\u0001\u0002<=¨\u0006>"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;", "specificSettingsState", "Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;", "(Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;Lorg/thoughtcrime/securesms/components/settings/conversation/SpecificSettingsState;)V", "cleared", "", "disposable", "Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "getDisposable", "()Lio/reactivex/rxjava3/disposables/CompositeDisposable;", "events", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsEvent;", "getEvents", "()Landroidx/lifecycle/LiveData;", "internalEvents", "Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "getInternalEvents", "()Lorg/thoughtcrime/securesms/util/SingleLiveEvent;", "openedMediaCursors", "Ljava/util/HashSet;", "Landroid/database/Cursor;", "Lkotlin/collections/HashSet;", "sharedMediaUpdateTrigger", "Landroidx/lifecycle/MutableLiveData;", "", "kotlin.jvm.PlatformType", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsState;", "getState", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "getStore", "()Lorg/thoughtcrime/securesms/util/livedata/Store;", "block", "initiateGroupUpgrade", "onAddToGroup", "onAddToGroupComplete", "selected", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onComplete", "Lkotlin/Function0;", "onCleared", "refreshRecipient", "refreshSharedMedia", "revealAllMembers", "setMuteUntil", "muteUntil", "", "unblock", "unmute", "ensureClosed", "DescriptionState", "Factory", "GroupSettingsViewModel", "RecipientSettingsViewModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel$RecipientSettingsViewModel;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel$GroupSettingsViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class ConversationSettingsViewModel extends ViewModel {
    private volatile boolean cleared;
    private final CompositeDisposable disposable;
    private final LiveData<ConversationSettingsEvent> events;
    private final SingleLiveEvent<ConversationSettingsEvent> internalEvents;
    private final HashSet<Cursor> openedMediaCursors;
    private final ConversationSettingsRepository repository;
    private final MutableLiveData<Unit> sharedMediaUpdateTrigger;
    private final LiveData<ConversationSettingsState> state;
    private final Store<ConversationSettingsState> store;

    public /* synthetic */ ConversationSettingsViewModel(ConversationSettingsRepository conversationSettingsRepository, SpecificSettingsState specificSettingsState, DefaultConstructorMarker defaultConstructorMarker) {
        this(conversationSettingsRepository, specificSettingsState);
    }

    public abstract void block();

    public abstract void onAddToGroup();

    public abstract void onAddToGroupComplete(List<? extends RecipientId> list, Function0<Unit> function0);

    public abstract void revealAllMembers();

    public abstract void setMuteUntil(long j);

    public abstract void unblock();

    public abstract void unmute();

    private ConversationSettingsViewModel(ConversationSettingsRepository conversationSettingsRepository, SpecificSettingsState specificSettingsState) {
        this.repository = conversationSettingsRepository;
        this.openedMediaCursors = new HashSet<>();
        Store<ConversationSettingsState> store = new Store<>(new ConversationSettingsState(0, null, null, null, 0, false, null, null, false, false, specificSettingsState, 1023, null));
        this.store = store;
        SingleLiveEvent<ConversationSettingsEvent> singleLiveEvent = new SingleLiveEvent<>();
        this.internalEvents = singleLiveEvent;
        MutableLiveData<Unit> mutableLiveData = new MutableLiveData<>(Unit.INSTANCE);
        this.sharedMediaUpdateTrigger = mutableLiveData;
        LiveData<ConversationSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        this.events = singleLiveEvent;
        this.disposable = new CompositeDisposable();
        LiveData distinctUntilChanged = Transformations.distinctUntilChanged(Transformations.map(stateLiveData, new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel$$ExternalSyntheticLambda0
            @Override // androidx.arch.core.util.Function
            public final Object apply(Object obj) {
                return ConversationSettingsViewModel.m1120_init_$lambda0((ConversationSettingsState) obj);
            }
        }));
        Intrinsics.checkNotNullExpressionValue(distinctUntilChanged, "distinctUntilChanged(Tra…p(state) { it.threadId })");
        LiveData combineLatest = LiveDataUtil.combineLatest(distinctUntilChanged, mutableLiveData, new LiveDataUtil.Combine() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
            public final Object apply(Object obj, Object obj2) {
                return ConversationSettingsViewModel.m1121_init_$lambda1(((Long) obj).longValue(), (Unit) obj2);
            }
        });
        Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(threadId, …rigger) { tId, _ -> tId }");
        LiveData<Input> mapAsync = LiveDataUtil.mapAsync(SignalExecutors.BOUNDED, combineLatest, new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return ConversationSettingsViewModel.m1122_init_$lambda2(ConversationSettingsViewModel.this, (Long) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                return Function.CC.$default$compose(this, function);
            }
        });
        Intrinsics.checkNotNullExpressionValue(mapAsync, "mapAsync(SignalExecutors…getThreadMedia(tId)\n    }");
        store.update(mapAsync, new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel$$ExternalSyntheticLambda3
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return ConversationSettingsViewModel.m1123_init_$lambda4(ConversationSettingsViewModel.this, (Optional) obj, (ConversationSettingsState) obj2);
            }
        });
    }

    protected final Store<ConversationSettingsState> getStore() {
        return this.store;
    }

    public final SingleLiveEvent<ConversationSettingsEvent> getInternalEvents() {
        return this.internalEvents;
    }

    public final LiveData<ConversationSettingsState> getState() {
        return this.state;
    }

    public final LiveData<ConversationSettingsEvent> getEvents() {
        return this.events;
    }

    protected final CompositeDisposable getDisposable() {
        return this.disposable;
    }

    /* renamed from: _init_$lambda-0 */
    public static final Long m1120_init_$lambda0(ConversationSettingsState conversationSettingsState) {
        return Long.valueOf(conversationSettingsState.getThreadId());
    }

    /* renamed from: _init_$lambda-1 */
    public static final Long m1121_init_$lambda1(long j, Unit unit) {
        Intrinsics.checkNotNullParameter(unit, "<anonymous parameter 1>");
        return Long.valueOf(j);
    }

    /* renamed from: _init_$lambda-2 */
    public static final Optional m1122_init_$lambda2(ConversationSettingsViewModel conversationSettingsViewModel, Long l) {
        Intrinsics.checkNotNullParameter(conversationSettingsViewModel, "this$0");
        ConversationSettingsRepository conversationSettingsRepository = conversationSettingsViewModel.repository;
        Intrinsics.checkNotNullExpressionValue(l, "tId");
        return conversationSettingsRepository.getThreadMedia(l.longValue());
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v10, resolved type: java.util.HashSet<android.database.Cursor> */
    /* JADX WARN: Multi-variable type inference failed */
    /* renamed from: _init_$lambda-4 */
    public static final ConversationSettingsState m1123_init_$lambda4(ConversationSettingsViewModel conversationSettingsViewModel, Optional optional, ConversationSettingsState conversationSettingsState) {
        Intrinsics.checkNotNullParameter(conversationSettingsViewModel, "this$0");
        if (!conversationSettingsViewModel.cleared) {
            if (optional.isPresent()) {
                conversationSettingsViewModel.openedMediaCursors.add(optional.get());
            }
            Object orElse = optional.map(new j$.util.function.Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel$$ExternalSyntheticLambda4
                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function andThen(j$.util.function.Function function) {
                    return Function.CC.$default$andThen(this, function);
                }

                @Override // j$.util.function.Function
                public final Object apply(Object obj) {
                    return ConversationSettingsViewModel.m1124lambda4$lambda3((Cursor) obj);
                }

                @Override // j$.util.function.Function
                public /* synthetic */ j$.util.function.Function compose(j$.util.function.Function function) {
                    return Function.CC.$default$compose(this, function);
                }
            }).orElse(CollectionsKt__CollectionsKt.emptyList());
            Intrinsics.checkNotNullExpressionValue(orElse, "cursor.map<List<Long>> {…       }.orElse(listOf())");
            Cursor cursor = (Cursor) optional.orElse(null);
            boolean isInternalRecipientDetailsEnabled = conversationSettingsViewModel.repository.isInternalRecipientDetailsEnabled();
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : cursor, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : (List) orElse, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : isInternalRecipientDetailsEnabled, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : true, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
        }
        conversationSettingsViewModel.ensureClosed((Cursor) optional.orElse(null));
        Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
        return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
    }

    /* renamed from: lambda-4$lambda-3 */
    public static final List m1124lambda4$lambda3(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            arrayList.add(Long.valueOf(CursorUtil.requireLong(cursor, "_id")));
        }
        return arrayList;
    }

    public final void refreshSharedMedia() {
        this.sharedMediaUpdateTrigger.postValue(Unit.INSTANCE);
    }

    public void refreshRecipient() {
        throw new IllegalStateException("This ViewModel does not support this interaction".toString());
    }

    @Override // androidx.lifecycle.ViewModel
    public void onCleared() {
        this.cleared = true;
        for (Cursor cursor : this.openedMediaCursors) {
            ensureClosed(cursor);
        }
        this.store.clear();
        this.disposable.clear();
    }

    private final void ensureClosed(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    public void initiateGroupUpgrade() {
        throw new IllegalStateException("This ViewModel does not support this interaction".toString());
    }

    /* compiled from: ConversationSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\nH\u0016J\b\u0010\u000b\u001a\u00020\nH\u0016J$\u0010\f\u001a\u00020\n2\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00030\u000e2\f\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\n0\u0010H\u0016J\b\u0010\u0011\u001a\u00020\nH\u0016J\b\u0010\u0012\u001a\u00020\nH\u0016J\u0010\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\u0015H\u0016J\b\u0010\u0016\u001a\u00020\nH\u0016J\b\u0010\u0017\u001a\u00020\nH\u0016R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel$RecipientSettingsViewModel;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;)V", "liveRecipient", "Lorg/thoughtcrime/securesms/recipients/LiveRecipient;", "block", "", "onAddToGroup", "onAddToGroupComplete", "selected", "", "onComplete", "Lkotlin/Function0;", "refreshRecipient", "revealAllMembers", "setMuteUntil", "muteUntil", "", "unblock", "unmute", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class RecipientSettingsViewModel extends ConversationSettingsViewModel {
        private final LiveRecipient liveRecipient;
        private final RecipientId recipientId;
        private final ConversationSettingsRepository repository;

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void onAddToGroupComplete(List<? extends RecipientId> list, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(list, "selected");
            Intrinsics.checkNotNullParameter(function0, "onComplete");
        }

        /* JADX DEBUG: Type inference failed for r0v4. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public RecipientSettingsViewModel(RecipientId recipientId, ConversationSettingsRepository conversationSettingsRepository) {
            super(conversationSettingsRepository, new SpecificSettingsState.RecipientSettingsState(null, null, null, false, false, false, null, 127, null), null);
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(conversationSettingsRepository, "repository");
            this.recipientId = recipientId;
            this.repository = conversationSettingsRepository;
            LiveRecipient live = Recipient.live(recipientId);
            Intrinsics.checkNotNullExpressionValue(live, "live(recipientId)");
            this.liveRecipient = live;
            CompositeDisposable disposable = getDisposable();
            Disposable subscribe = StoryViewState.Companion.getForRecipientId(recipientId).subscribe(new ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda2(this));
            Intrinsics.checkNotNullExpressionValue(subscribe, "StoryViewState.getForRec…storyViewState) }\n      }");
            DisposableKt.plusAssign(disposable, subscribe);
            getStore().update(live.getLiveData(), new ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda3());
            conversationSettingsRepository.getThreadId(recipientId, new Function1<Long, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel.RecipientSettingsViewModel.3
                final /* synthetic */ RecipientSettingsViewModel this$0;

                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Long l) {
                    invoke(l.longValue());
                    return Unit.INSTANCE;
                }

                public final void invoke(long j) {
                    this.this$0.getStore().update(new ConversationSettingsViewModel$RecipientSettingsViewModel$3$$ExternalSyntheticLambda0(j));
                }

                /* renamed from: invoke$lambda-0 */
                public static final ConversationSettingsState m1156invoke$lambda0(long j, ConversationSettingsState conversationSettingsState) {
                    Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
                    return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : j, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
                }
            });
            if (!Intrinsics.areEqual(recipientId, Recipient.self().getId())) {
                conversationSettingsRepository.getGroupsInCommon(recipientId, new Function1<List<? extends Recipient>, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel.RecipientSettingsViewModel.4
                    final /* synthetic */ RecipientSettingsViewModel this$0;

                    {
                        this.this$0 = r1;
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(List<? extends Recipient> list) {
                        invoke(list);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(List<? extends Recipient> list) {
                        Intrinsics.checkNotNullParameter(list, "groupsInCommon");
                        this.this$0.getStore().update(new ConversationSettingsViewModel$RecipientSettingsViewModel$4$$ExternalSyntheticLambda0(list));
                    }

                    /* renamed from: invoke$lambda-0 */
                    public static final ConversationSettingsState m1157invoke$lambda0(List list, ConversationSettingsState conversationSettingsState) {
                        List list2;
                        Intrinsics.checkNotNullParameter(list, "$groupsInCommon");
                        SpecificSettingsState.RecipientSettingsState requireRecipientSettingsState = conversationSettingsState.requireRecipientSettingsState();
                        boolean z = !requireRecipientSettingsState.getGroupsInCommonExpanded() && list.size() > 6;
                        Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
                        if (!z) {
                            list2 = list;
                        } else {
                            list2 = CollectionsKt___CollectionsKt.take(list, 5);
                        }
                        return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : SpecificSettingsState.RecipientSettingsState.copy$default(requireRecipientSettingsState, null, list, list2, false, z, false, null, 105, null));
                    }
                });
                conversationSettingsRepository.hasGroups(new Function1<Boolean, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel.RecipientSettingsViewModel.5
                    final /* synthetic */ RecipientSettingsViewModel this$0;

                    {
                        this.this$0 = r1;
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Boolean bool) {
                        invoke(bool.booleanValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(boolean z) {
                        this.this$0.getStore().update(new ConversationSettingsViewModel$RecipientSettingsViewModel$5$$ExternalSyntheticLambda0(z));
                    }

                    /* renamed from: invoke$lambda-0 */
                    public static final ConversationSettingsState m1158invoke$lambda0(boolean z, ConversationSettingsState conversationSettingsState) {
                        SpecificSettingsState.RecipientSettingsState requireRecipientSettingsState = conversationSettingsState.requireRecipientSettingsState();
                        Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
                        return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : SpecificSettingsState.RecipientSettingsState.copy$default(requireRecipientSettingsState, null, null, null, z, false, false, null, 119, null));
                    }
                });
                conversationSettingsRepository.getIdentity(recipientId, new Function1<IdentityRecord, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel.RecipientSettingsViewModel.6
                    final /* synthetic */ RecipientSettingsViewModel this$0;

                    {
                        this.this$0 = r1;
                    }

                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(IdentityRecord identityRecord) {
                        invoke(identityRecord);
                        return Unit.INSTANCE;
                    }

                    public final void invoke(IdentityRecord identityRecord) {
                        this.this$0.getStore().update(new ConversationSettingsViewModel$RecipientSettingsViewModel$6$$ExternalSyntheticLambda0(identityRecord));
                    }

                    /* renamed from: invoke$lambda-0 */
                    public static final ConversationSettingsState m1159invoke$lambda0(IdentityRecord identityRecord, ConversationSettingsState conversationSettingsState) {
                        Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
                        return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : SpecificSettingsState.RecipientSettingsState.copy$default(conversationSettingsState.requireRecipientSettingsState(), identityRecord, null, null, false, false, false, null, 126, null));
                    }
                });
            }
        }

        /* renamed from: _init_$lambda-1 */
        public static final void m1151_init_$lambda1(RecipientSettingsViewModel recipientSettingsViewModel, StoryViewState storyViewState) {
            Intrinsics.checkNotNullParameter(recipientSettingsViewModel, "this$0");
            recipientSettingsViewModel.getStore().update(new ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda1(storyViewState));
        }

        /* renamed from: lambda-1$lambda-0 */
        public static final ConversationSettingsState m1153lambda1$lambda0(StoryViewState storyViewState, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "it");
            Intrinsics.checkNotNullExpressionValue(storyViewState, "storyViewState");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : storyViewState, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
        }

        /* renamed from: _init_$lambda-2 */
        public static final ConversationSettingsState m1152_init_$lambda2(Recipient recipient, ConversationSettingsState conversationSettingsState) {
            ContactLinkState contactLinkState;
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            Intrinsics.checkNotNullExpressionValue(recipient, RecipientDatabase.TABLE_NAME);
            RecipientDatabase.RegisteredState registered = recipient.getRegistered();
            RecipientDatabase.RegisteredState registeredState = RecipientDatabase.RegisteredState.REGISTERED;
            ButtonStripPreference.State state = new ButtonStripPreference.State(false, registered == registeredState && !recipient.isSelf() && !recipient.isBlocked() && !recipient.isReleaseNotes(), !recipient.isGroup() && !recipient.isSelf() && !recipient.isBlocked() && !recipient.isReleaseNotes(), !recipient.isSelf(), true, recipient.getRegistered() == registeredState, recipient.isMuted(), 1, null);
            int expiresInSeconds = recipient.getExpiresInSeconds();
            boolean z = !recipient.isSelf() && RecipientUtil.isBlockable(recipient);
            SpecificSettingsState.RecipientSettingsState requireRecipientSettingsState = conversationSettingsState.requireRecipientSettingsState();
            if (recipient.isSelf() || recipient.isReleaseNotes() || recipient.isBlocked()) {
                contactLinkState = ContactLinkState.NONE;
            } else if (recipient.isSystemContact()) {
                contactLinkState = ContactLinkState.OPEN;
            } else {
                contactLinkState = ContactLinkState.ADD;
            }
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : recipient, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : state, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : expiresInSeconds, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : z, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : SpecificSettingsState.RecipientSettingsState.copy$default(requireRecipientSettingsState, null, null, null, false, false, false, contactLinkState, 63, null));
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void onAddToGroup() {
            this.repository.getGroupMembership(this.recipientId, new ConversationSettingsViewModel$RecipientSettingsViewModel$onAddToGroup$1(this));
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void revealAllMembers() {
            getStore().update(new ConversationSettingsViewModel$RecipientSettingsViewModel$$ExternalSyntheticLambda0());
        }

        /* renamed from: revealAllMembers$lambda-3 */
        public static final ConversationSettingsState m1154revealAllMembers$lambda3(ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : SpecificSettingsState.RecipientSettingsState.copy$default(conversationSettingsState.requireRecipientSettingsState(), null, null, conversationSettingsState.requireRecipientSettingsState().getAllGroupsInCommon(), false, false, true, null, 75, null));
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void refreshRecipient() {
            this.repository.refreshRecipient(this.recipientId);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void setMuteUntil(long j) {
            this.repository.setMuteUntil(this.recipientId, j);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void unmute() {
            this.repository.setMuteUntil(this.recipientId, 0);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void block() {
            this.repository.block(this.recipientId);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void unblock() {
            this.repository.unblock(this.recipientId);
        }
    }

    /* compiled from: ConversationSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\t\n\u0002\b\u0003\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\b\u0010\t\u001a\u00020\nH\u0016J\u0010\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\nH\u0016J\b\u0010\u0010\u001a\u00020\nH\u0016J$\u0010\u0011\u001a\u00020\n2\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u00132\f\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\n0\u0016H\u0016J\b\u0010\u0017\u001a\u00020\nH\u0016J\u0010\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\b\u0010\u001b\u001a\u00020\nH\u0016J\b\u0010\u001c\u001a\u00020\nH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel$GroupSettingsViewModel;", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;", "(Lorg/thoughtcrime/securesms/groups/GroupId;Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;)V", "liveGroup", "Lorg/thoughtcrime/securesms/groups/LiveGroup;", "block", "", "getLegacyGroupState", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/LegacyGroupPreference$State;", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "initiateGroupUpgrade", "onAddToGroup", "onAddToGroupComplete", "selected", "", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "onComplete", "Lkotlin/Function0;", "revealAllMembers", "setMuteUntil", "muteUntil", "", "unblock", "unmute", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class GroupSettingsViewModel extends ConversationSettingsViewModel {
        private final GroupId groupId;
        private final LiveGroup liveGroup;
        private final ConversationSettingsRepository repository;

        /* JADX DEBUG: Type inference failed for r4v5. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v6. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v7. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Integer>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v8. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v9. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.util.List<org.thoughtcrime.securesms.groups.ui.GroupMemberEntry$FullMember>>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v10. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v16. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.Boolean>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v17. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.String>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r4v18. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.groups.v2.GroupLinkUrlAndStatus>, androidx.lifecycle.LiveData<Input> */
        /* JADX DEBUG: Type inference failed for r2v2. Raw type applied. Possible types: androidx.lifecycle.LiveData<java.lang.String>, androidx.lifecycle.LiveData<Input> */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public GroupSettingsViewModel(GroupId groupId, ConversationSettingsRepository conversationSettingsRepository) {
            super(conversationSettingsRepository, new SpecificSettingsState.GroupSettingsState(groupId, null, null, false, false, false, false, false, false, null, false, null, false, false, false, null, null, false, 262142, null), null);
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(conversationSettingsRepository, "repository");
            this.groupId = groupId;
            this.repository = conversationSettingsRepository;
            LiveGroup liveGroup = new LiveGroup(groupId);
            this.liveGroup = liveGroup;
            CompositeDisposable disposable = getDisposable();
            Disposable subscribe = conversationSettingsRepository.getStoryViewState(groupId).subscribe(new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda0(this));
            Intrinsics.checkNotNullExpressionValue(subscribe, "repository.getStoryViewS…storyViewState) }\n      }");
            DisposableKt.plusAssign(disposable, subscribe);
            getStore().update(LiveDataUtil.combineLatest(liveGroup.getGroupRecipient(), liveGroup.isActive(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda7()), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda8(this));
            conversationSettingsRepository.getThreadId(groupId, new Function1<Long, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel.GroupSettingsViewModel.3
                final /* synthetic */ GroupSettingsViewModel this$0;

                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function1
                public /* bridge */ /* synthetic */ Unit invoke(Long l) {
                    invoke(l.longValue());
                    return Unit.INSTANCE;
                }

                public final void invoke(long j) {
                    this.this$0.getStore().update(new ConversationSettingsViewModel$GroupSettingsViewModel$3$$ExternalSyntheticLambda0(j));
                }

                /* renamed from: invoke$lambda-0 */
                public static final ConversationSettingsState m1146invoke$lambda0(long j, ConversationSettingsState conversationSettingsState) {
                    Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
                    return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : j, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
                }
            });
            getStore().update(liveGroup.selfCanEditGroupAttributes(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda9());
            getStore().update(liveGroup.isSelfAdmin(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda10());
            getStore().update(liveGroup.getExpireMessages(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda11());
            getStore().update(liveGroup.selfCanAddMembers(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda12());
            getStore().update(liveGroup.getFullMembers(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda13());
            getStore().update(liveGroup.isAnnouncementGroup(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda14());
            LiveData mapAsync = LiveDataUtil.mapAsync(liveGroup.getGroupRecipient(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda15(this));
            Intrinsics.checkNotNullExpressionValue(mapAsync, "mapAsync(liveGroup.group…ssageRequestAccepted(r) }");
            LiveData<Input> combineLatest = LiveDataUtil.combineLatest(liveGroup.getDescription(), mapAsync, new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda1());
            Intrinsics.checkNotNullExpressionValue(combineLatest, "combineLatest(liveGroup.…pted, ::DescriptionState)");
            getStore().update(combineLatest, new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda2());
            getStore().update(liveGroup.isActive(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda3(this));
            getStore().update(liveGroup.getTitle(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda4());
            getStore().update(liveGroup.getGroupLink(), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda5());
            getStore().update(conversationSettingsRepository.getMembershipCountDescription(liveGroup), new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda6());
        }

        /* renamed from: _init_$lambda-1 */
        public static final void m1129_init_$lambda1(GroupSettingsViewModel groupSettingsViewModel, StoryViewState storyViewState) {
            Intrinsics.checkNotNullParameter(groupSettingsViewModel, "this$0");
            groupSettingsViewModel.getStore().update(new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda17(storyViewState));
        }

        /* renamed from: lambda-1$lambda-0 */
        public static final ConversationSettingsState m1144lambda1$lambda0(StoryViewState storyViewState, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "it");
            Intrinsics.checkNotNullExpressionValue(storyViewState, "storyViewState");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : storyViewState, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
        }

        /* renamed from: _init_$lambda-2 */
        public static final Pair m1136_init_$lambda2(Recipient recipient, boolean z) {
            Intrinsics.checkNotNullParameter(recipient, "r");
            return TuplesKt.to(recipient, Boolean.valueOf(z));
        }

        /* renamed from: _init_$lambda-3 */
        public static final ConversationSettingsState m1137_init_$lambda3(GroupSettingsViewModel groupSettingsViewModel, Pair pair, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullParameter(groupSettingsViewModel, "this$0");
            Recipient recipient = (Recipient) pair.component1();
            boolean booleanValue = ((Boolean) pair.component2()).booleanValue();
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : recipient, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : new ButtonStripPreference.State(false, recipient.isPushV2Group() && !recipient.isBlocked() && booleanValue, false, true, true, recipient.isPushV2Group(), recipient.isMuted(), 1, null), (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : RecipientUtil.isBlockable(recipient), (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : r14.copy((r36 & 1) != 0 ? r14.groupId : null, (r36 & 2) != 0 ? r14.allMembers : null, (r36 & 4) != 0 ? r14.members : null, (r36 & 8) != 0 ? r14.isSelfAdmin : false, (r36 & 16) != 0 ? r14.canAddToGroup : false, (r36 & 32) != 0 ? r14.canEditGroupAttributes : false, (r36 & 64) != 0 ? r14.canLeave : false, (r36 & 128) != 0 ? r14.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? r14.groupMembersExpanded : false, (r36 & 512) != 0 ? r14.groupTitle : null, (r36 & 1024) != 0 ? r14.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r14.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r14.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? r14.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? r14.groupLinkEnabled : false, (r36 & 32768) != 0 ? r14.membershipCountDescription : null, (r36 & 65536) != 0 ? r14.legacyGroupState : groupSettingsViewModel.getLegacyGroupState(recipient), (r36 & 131072) != 0 ? conversationSettingsState.requireGroupSettingsState().isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-4 */
        public static final ConversationSettingsState m1138_init_$lambda4(Boolean bool, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(bool, "selfCanEditGroupAttributes");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : bool.booleanValue(), (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-5 */
        public static final ConversationSettingsState m1139_init_$lambda5(Boolean bool, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(bool, "isSelfAdmin");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : bool.booleanValue(), (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-6 */
        public static final ConversationSettingsState m1140_init_$lambda6(Integer num, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            Intrinsics.checkNotNullExpressionValue(num, "expireMessages");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : num.intValue(), (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : null);
        }

        /* renamed from: _init_$lambda-7 */
        public static final ConversationSettingsState m1141_init_$lambda7(Boolean bool, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(bool, "canAddMembers");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : bool.booleanValue(), (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-8 */
        public static final ConversationSettingsState m1142_init_$lambda8(List list, ConversationSettingsState conversationSettingsState) {
            List list2;
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            boolean z = !requireGroupSettingsState.getGroupMembersExpanded() && list.size() > 6;
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            Intrinsics.checkNotNullExpressionValue(list, "fullMembers");
            if (!z) {
                list2 = list;
            } else {
                list2 = CollectionsKt___CollectionsKt.take(list, 5);
            }
            Intrinsics.checkNotNullExpressionValue(list2, "if (!canShowMore) fullMe… else fullMembers.take(5)");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : list, (r36 & 4) != 0 ? requireGroupSettingsState.members : list2, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : z, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-9 */
        public static final ConversationSettingsState m1143_init_$lambda9(Boolean bool, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(bool, "announcementGroup");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : bool.booleanValue()));
        }

        /* renamed from: _init_$lambda-10 */
        public static final Boolean m1130_init_$lambda10(GroupSettingsViewModel groupSettingsViewModel, Recipient recipient) {
            Intrinsics.checkNotNullParameter(groupSettingsViewModel, "this$0");
            ConversationSettingsRepository conversationSettingsRepository = groupSettingsViewModel.repository;
            Intrinsics.checkNotNullExpressionValue(recipient, "r");
            return Boolean.valueOf(conversationSettingsRepository.isMessageRequestAccepted(recipient));
        }

        /* renamed from: _init_$lambda-11 */
        public static final ConversationSettingsState m1131_init_$lambda11(DescriptionState descriptionState, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : r2.copy((r36 & 1) != 0 ? r2.groupId : null, (r36 & 2) != 0 ? r2.allMembers : null, (r36 & 4) != 0 ? r2.members : null, (r36 & 8) != 0 ? r2.isSelfAdmin : false, (r36 & 16) != 0 ? r2.canAddToGroup : false, (r36 & 32) != 0 ? r2.canEditGroupAttributes : false, (r36 & 64) != 0 ? r2.canLeave : false, (r36 & 128) != 0 ? r2.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? r2.groupMembersExpanded : false, (r36 & 512) != 0 ? r2.groupTitle : null, (r36 & 1024) != 0 ? r2.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r2.groupDescription : descriptionState.getDescription(), (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r2.groupDescriptionShouldLinkify : descriptionState.getCanLinkify(), (r36 & 8192) != 0 ? r2.groupDescriptionLoaded : true, (r36 & 16384) != 0 ? r2.groupLinkEnabled : false, (r36 & 32768) != 0 ? r2.membershipCountDescription : null, (r36 & 65536) != 0 ? r2.legacyGroupState : null, (r36 & 131072) != 0 ? conversationSettingsState.requireGroupSettingsState().isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-12 */
        public static final ConversationSettingsState m1132_init_$lambda12(GroupSettingsViewModel groupSettingsViewModel, Boolean bool, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullParameter(groupSettingsViewModel, "this$0");
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(bool, "isActive");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : bool.booleanValue() && groupSettingsViewModel.groupId.isPush(), (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-13 */
        public static final ConversationSettingsState m1133_init_$lambda13(String str, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(str, MultiselectForwardFragment.DIALOG_TITLE);
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : str, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : true, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : null, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-14 */
        public static final ConversationSettingsState m1134_init_$lambda14(GroupLinkUrlAndStatus groupLinkUrlAndStatus, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : r2.copy((r36 & 1) != 0 ? r2.groupId : null, (r36 & 2) != 0 ? r2.allMembers : null, (r36 & 4) != 0 ? r2.members : null, (r36 & 8) != 0 ? r2.isSelfAdmin : false, (r36 & 16) != 0 ? r2.canAddToGroup : false, (r36 & 32) != 0 ? r2.canEditGroupAttributes : false, (r36 & 64) != 0 ? r2.canLeave : false, (r36 & 128) != 0 ? r2.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? r2.groupMembersExpanded : false, (r36 & 512) != 0 ? r2.groupTitle : null, (r36 & 1024) != 0 ? r2.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r2.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r2.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? r2.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? r2.groupLinkEnabled : groupLinkUrlAndStatus.isEnabled(), (r36 & 32768) != 0 ? r2.membershipCountDescription : null, (r36 & 65536) != 0 ? r2.legacyGroupState : null, (r36 & 131072) != 0 ? conversationSettingsState.requireGroupSettingsState().isAnnouncementGroup : false));
        }

        /* renamed from: _init_$lambda-15 */
        public static final ConversationSettingsState m1135_init_$lambda15(String str, ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            SpecificSettingsState.GroupSettingsState requireGroupSettingsState = conversationSettingsState.requireGroupSettingsState();
            Intrinsics.checkNotNullExpressionValue(str, "description");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : requireGroupSettingsState.copy((r36 & 1) != 0 ? requireGroupSettingsState.groupId : null, (r36 & 2) != 0 ? requireGroupSettingsState.allMembers : null, (r36 & 4) != 0 ? requireGroupSettingsState.members : null, (r36 & 8) != 0 ? requireGroupSettingsState.isSelfAdmin : false, (r36 & 16) != 0 ? requireGroupSettingsState.canAddToGroup : false, (r36 & 32) != 0 ? requireGroupSettingsState.canEditGroupAttributes : false, (r36 & 64) != 0 ? requireGroupSettingsState.canLeave : false, (r36 & 128) != 0 ? requireGroupSettingsState.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? requireGroupSettingsState.groupMembersExpanded : false, (r36 & 512) != 0 ? requireGroupSettingsState.groupTitle : null, (r36 & 1024) != 0 ? requireGroupSettingsState.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? requireGroupSettingsState.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? requireGroupSettingsState.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? requireGroupSettingsState.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? requireGroupSettingsState.groupLinkEnabled : false, (r36 & 32768) != 0 ? requireGroupSettingsState.membershipCountDescription : str, (r36 & 65536) != 0 ? requireGroupSettingsState.legacyGroupState : null, (r36 & 131072) != 0 ? requireGroupSettingsState.isAnnouncementGroup : false));
        }

        private final LegacyGroupPreference.State getLegacyGroupState(Recipient recipient) {
            boolean isV1 = recipient.requireGroupId().isV1();
            if (isV1 && recipient.getParticipantIds().size() > FeatureFlags.groupLimits().getHardLimit()) {
                return LegacyGroupPreference.State.TOO_LARGE;
            }
            if (isV1) {
                return LegacyGroupPreference.State.UPGRADE;
            }
            if (this.groupId.isMms()) {
                return LegacyGroupPreference.State.MMS_WARNING;
            }
            return LegacyGroupPreference.State.NONE;
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void onAddToGroup() {
            this.repository.getGroupCapacity(this.groupId, new ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroup$1(this));
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void onAddToGroupComplete(List<? extends RecipientId> list, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(list, "selected");
            Intrinsics.checkNotNullParameter(function0, "onComplete");
            this.repository.addMembers(this.groupId, list, new ConversationSettingsViewModel$GroupSettingsViewModel$onAddToGroupComplete$1(this, function0));
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void revealAllMembers() {
            getStore().update(new ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda16());
        }

        /* renamed from: revealAllMembers$lambda-16 */
        public static final ConversationSettingsState m1145revealAllMembers$lambda16(ConversationSettingsState conversationSettingsState) {
            Intrinsics.checkNotNullExpressionValue(conversationSettingsState, "state");
            return conversationSettingsState.copy((r26 & 1) != 0 ? conversationSettingsState.threadId : 0, (r26 & 2) != 0 ? conversationSettingsState.storyViewState : null, (r26 & 4) != 0 ? conversationSettingsState.recipient : null, (r26 & 8) != 0 ? conversationSettingsState.buttonStripState : null, (r26 & 16) != 0 ? conversationSettingsState.disappearingMessagesLifespan : 0, (r26 & 32) != 0 ? conversationSettingsState.canModifyBlockedState : false, (r26 & 64) != 0 ? conversationSettingsState.sharedMedia : null, (r26 & 128) != 0 ? conversationSettingsState.sharedMediaIds : null, (r26 & 256) != 0 ? conversationSettingsState.displayInternalRecipientDetails : false, (r26 & 512) != 0 ? conversationSettingsState.sharedMediaLoaded : false, (r26 & 1024) != 0 ? conversationSettingsState.specificSettingsState : r2.copy((r36 & 1) != 0 ? r2.groupId : null, (r36 & 2) != 0 ? r2.allMembers : null, (r36 & 4) != 0 ? r2.members : conversationSettingsState.requireGroupSettingsState().getAllMembers(), (r36 & 8) != 0 ? r2.isSelfAdmin : false, (r36 & 16) != 0 ? r2.canAddToGroup : false, (r36 & 32) != 0 ? r2.canEditGroupAttributes : false, (r36 & 64) != 0 ? r2.canLeave : false, (r36 & 128) != 0 ? r2.canShowMoreGroupMembers : false, (r36 & 256) != 0 ? r2.groupMembersExpanded : true, (r36 & 512) != 0 ? r2.groupTitle : null, (r36 & 1024) != 0 ? r2.groupTitleLoaded : false, (r36 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? r2.groupDescription : null, (r36 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? r2.groupDescriptionShouldLinkify : false, (r36 & 8192) != 0 ? r2.groupDescriptionLoaded : false, (r36 & 16384) != 0 ? r2.groupLinkEnabled : false, (r36 & 32768) != 0 ? r2.membershipCountDescription : null, (r36 & 65536) != 0 ? r2.legacyGroupState : null, (r36 & 131072) != 0 ? conversationSettingsState.requireGroupSettingsState().isAnnouncementGroup : false));
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void setMuteUntil(long j) {
            this.repository.setMuteUntil(this.groupId, j);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void unmute() {
            this.repository.setMuteUntil(this.groupId, 0);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void block() {
            this.repository.block(this.groupId);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void unblock() {
            this.repository.unblock(this.groupId);
        }

        @Override // org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel
        public void initiateGroupUpgrade() {
            this.repository.getExternalPossiblyMigratedGroupRecipientId(this.groupId, new ConversationSettingsViewModel$GroupSettingsViewModel$initiateGroupUpgrade$1(this));
        }
    }

    /* compiled from: ConversationSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B%\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ%\u0010\t\u001a\u0002H\n\"\b\b\u0000\u0010\n*\u00020\u000b2\f\u0010\f\u001a\b\u0012\u0004\u0012\u0002H\n0\rH\u0016¢\u0006\u0002\u0010\u000eR\u0010\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/groups/GroupId;Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final GroupId groupId;
        private final RecipientId recipientId;
        private final ConversationSettingsRepository repository;

        public Factory(RecipientId recipientId, GroupId groupId, ConversationSettingsRepository conversationSettingsRepository) {
            Intrinsics.checkNotNullParameter(conversationSettingsRepository, "repository");
            this.recipientId = recipientId;
            this.groupId = groupId;
            this.repository = conversationSettingsRepository;
        }

        public /* synthetic */ Factory(RecipientId recipientId, GroupId groupId, ConversationSettingsRepository conversationSettingsRepository, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? null : recipientId, (i & 2) != 0 ? null : groupId, conversationSettingsRepository);
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Object obj;
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            RecipientId recipientId = this.recipientId;
            if (recipientId != null) {
                obj = new RecipientSettingsViewModel(recipientId, this.repository);
            } else {
                GroupId groupId = this.groupId;
                if (groupId != null) {
                    obj = new GroupSettingsViewModel(groupId, this.repository);
                } else {
                    throw new IllegalStateException("One of RecipientId or GroupId required.".toString());
                }
            }
            T cast = cls.cast(obj);
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }

    /* compiled from: ConversationSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0006\b\u0002\u0018\u00002\u00020\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/ConversationSettingsViewModel$DescriptionState;", "", "description", "", "canLinkify", "", "(Ljava/lang/String;Z)V", "getCanLinkify", "()Z", "getDescription", "()Ljava/lang/String;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class DescriptionState {
        private final boolean canLinkify;
        private final String description;

        public DescriptionState(String str, boolean z) {
            this.description = str;
            this.canLinkify = z;
        }

        public final String getDescription() {
            return this.description;
        }

        public final boolean getCanLinkify() {
            return this.canLinkify;
        }
    }
}
