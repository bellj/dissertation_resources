package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.RecipientPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class RecipientPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ RecipientPreference.Model f$0;

    public /* synthetic */ RecipientPreference$ViewHolder$$ExternalSyntheticLambda0(RecipientPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        RecipientPreference.ViewHolder.m1213bind$lambda0(this.f$0, view);
    }
}
