package org.thoughtcrime.securesms.components;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import org.thoughtcrime.securesms.PassphraseRequiredActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.DynamicNoActionBarTheme;
import org.thoughtcrime.securesms.util.DynamicTheme;

/* compiled from: FragmentWrapperActivity.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH&J\u001a\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0014J\b\u0010\u0013\u001a\u00020\u000eH\u0014R\u0014\u0010\u0003\u001a\u00020\u0004XD¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0014"}, d2 = {"Lorg/thoughtcrime/securesms/components/FragmentWrapperActivity;", "Lorg/thoughtcrime/securesms/PassphraseRequiredActivity;", "()V", "contentViewId", "", "getContentViewId", "()I", "dynamicTheme", "Lorg/thoughtcrime/securesms/util/DynamicTheme;", "getDynamicTheme", "()Lorg/thoughtcrime/securesms/util/DynamicTheme;", "getFragment", "Landroidx/fragment/app/Fragment;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "ready", "", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class FragmentWrapperActivity extends PassphraseRequiredActivity {
    private final int contentViewId = R.layout.fragment_container;
    private final DynamicTheme dynamicTheme = new DynamicNoActionBarTheme();

    public abstract Fragment getFragment();

    protected DynamicTheme getDynamicTheme() {
        return this.dynamicTheme;
    }

    protected int getContentViewId() {
        return this.contentViewId;
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        super.onCreate(bundle, z);
        setContentView(getContentViewId());
        getDynamicTheme().onCreate(this);
        if (bundle == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, getFragment()).commit();
        }
    }

    @Override // org.thoughtcrime.securesms.PassphraseRequiredActivity, org.thoughtcrime.securesms.BaseActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onResume() {
        super.onResume();
        getDynamicTheme().onResume(this);
    }
}
