package org.thoughtcrime.securesms.components.reminder;

import android.view.View;
import java.util.LinkedList;
import java.util.List;

/* loaded from: classes4.dex */
public abstract class Reminder {
    private final List<Action> actions = new LinkedList();
    private View.OnClickListener dismissListener;
    private View.OnClickListener okListener;
    private CharSequence text;
    private CharSequence title;

    /* loaded from: classes4.dex */
    public enum Importance {
        NORMAL,
        ERROR,
        TERMINAL
    }

    public int getProgress() {
        return -1;
    }

    public boolean isDismissable() {
        return true;
    }

    public Reminder(CharSequence charSequence, CharSequence charSequence2) {
        this.title = charSequence;
        this.text = charSequence2;
    }

    public CharSequence getTitle() {
        return this.title;
    }

    public CharSequence getText() {
        return this.text;
    }

    public View.OnClickListener getOkListener() {
        return this.okListener;
    }

    public View.OnClickListener getDismissListener() {
        return this.dismissListener;
    }

    public void setOkListener(View.OnClickListener onClickListener) {
        this.okListener = onClickListener;
    }

    public void setDismissListener(View.OnClickListener onClickListener) {
        this.dismissListener = onClickListener;
    }

    public Importance getImportance() {
        return Importance.NORMAL;
    }

    public void addAction(Action action) {
        this.actions.add(action);
    }

    public List<Action> getActions() {
        return this.actions;
    }

    /* loaded from: classes4.dex */
    public static final class Action {
        private final int actionId;
        private final CharSequence title;

        public Action(CharSequence charSequence, int i) {
            this.title = charSequence;
            this.actionId = i;
        }

        public CharSequence getTitle() {
            return this.title;
        }

        public int getActionId() {
            return this.actionId;
        }
    }
}
