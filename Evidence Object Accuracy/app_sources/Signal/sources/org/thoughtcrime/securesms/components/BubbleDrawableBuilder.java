package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class BubbleDrawableBuilder {
    private int color;
    private boolean[] corners = {true, true, true, true};
    private boolean hasShadow = true;
    private int shadowColor;

    protected BubbleDrawableBuilder() {
    }

    public BubbleDrawableBuilder setColor(int i) {
        this.color = i;
        return this;
    }

    public BubbleDrawableBuilder setShadowColor(int i) {
        this.shadowColor = i;
        return this;
    }

    public BubbleDrawableBuilder setHasShadow(boolean z) {
        this.hasShadow = z;
        return this;
    }

    public BubbleDrawableBuilder setCorners(boolean[] zArr) {
        this.corners = zArr;
        return this;
    }

    public Drawable create(Context context) {
        GradientDrawable gradientDrawable = new GradientDrawable();
        float[] cornerBooleansToRadii = cornerBooleansToRadii(this.corners, context.getResources().getDimensionPixelSize(R.dimen.message_bubble_corner_radius));
        gradientDrawable.setColor(this.color);
        gradientDrawable.setCornerRadii(cornerBooleansToRadii);
        if (!this.hasShadow) {
            return gradientDrawable;
        }
        GradientDrawable gradientDrawable2 = new GradientDrawable();
        int dimensionPixelSize = context.getResources().getDimensionPixelSize(R.dimen.message_bubble_shadow_distance);
        gradientDrawable2.setColor(this.shadowColor);
        gradientDrawable2.setCornerRadii(cornerBooleansToRadii);
        LayerDrawable layerDrawable = new LayerDrawable(new Drawable[]{gradientDrawable2, gradientDrawable});
        layerDrawable.setLayerInset(1, 0, 0, 0, dimensionPixelSize);
        return layerDrawable;
    }

    private float[] cornerBooleansToRadii(boolean[] zArr, int i) {
        if (zArr == null || zArr.length != 4) {
            throw new AssertionError("there are four corners in a rectangle, silly");
        }
        float[] fArr = new float[8];
        int i2 = 0;
        for (boolean z : zArr) {
            int i3 = i2 + 1;
            float f = z ? (float) i : 0.0f;
            fArr[i3] = f;
            fArr[i2] = f;
            i2 += 2;
        }
        return fArr;
    }
}
