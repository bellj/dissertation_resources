package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import com.google.android.material.switchmaterial.SwitchMaterial;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/SwitchPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/SwitchPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "switchWidget", "Lcom/google/android/material/switchmaterial/SwitchMaterial;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SwitchPreferenceViewHolder extends PreferenceViewHolder<SwitchPreference> {
    private final SwitchMaterial switchWidget;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
        View findViewById = view.findViewById(R.id.switch_widget);
        Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.switch_widget)");
        this.switchWidget = (SwitchMaterial) findViewById;
    }

    public void bind(SwitchPreference switchPreference) {
        Intrinsics.checkNotNullParameter(switchPreference, "model");
        super.bind((SwitchPreferenceViewHolder) switchPreference);
        this.switchWidget.setEnabled(switchPreference.isEnabled());
        this.switchWidget.setChecked(switchPreference.isChecked());
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.SwitchPreferenceViewHolder$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                SwitchPreferenceViewHolder.$r8$lambda$kuo2xXdmEiCbokKZVgsAn59knkM(SwitchPreference.this, view);
            }
        });
    }

    /* renamed from: bind$lambda-0 */
    public static final void m545bind$lambda0(SwitchPreference switchPreference, View view) {
        Intrinsics.checkNotNullParameter(switchPreference, "$model");
        switchPreference.getOnClick().invoke();
    }
}
