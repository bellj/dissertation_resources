package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.widget.TextView;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiFilter;
import org.thoughtcrime.securesms.keyvalue.SignalStore;

/* loaded from: classes4.dex */
public class SearchView extends androidx.appcompat.widget.SearchView {
    public SearchView(Context context) {
        this(context, null);
    }

    public SearchView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.search_view_style);
    }

    public SearchView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initEmojiFilter();
    }

    private void initEmojiFilter() {
        TextView textView;
        if (!isInEditMode() && !SignalStore.settings().isPreferSystemEmoji() && (textView = (TextView) findViewById(R.id.search_src_text)) != null) {
            textView.setFilters(appendEmojiFilter(textView));
        }
    }

    private InputFilter[] appendEmojiFilter(TextView textView) {
        InputFilter[] inputFilterArr;
        InputFilter[] filters = textView.getFilters();
        if (filters != null) {
            inputFilterArr = new InputFilter[filters.length + 1];
            System.arraycopy(filters, 0, inputFilterArr, 1, filters.length);
        } else {
            inputFilterArr = new InputFilter[1];
        }
        inputFilterArr[0] = new EmojiFilter(textView, false);
        return inputFilterArr;
    }
}
