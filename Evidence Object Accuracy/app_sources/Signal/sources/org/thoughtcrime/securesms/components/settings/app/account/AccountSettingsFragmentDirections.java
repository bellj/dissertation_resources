package org.thoughtcrime.securesms.components.settings.app.account;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class AccountSettingsFragmentDirections {
    private AccountSettingsFragmentDirections() {
    }

    public static NavDirections actionAccountSettingsFragmentToChangePhoneNumberFragment() {
        return new ActionOnlyNavDirections(R.id.action_accountSettingsFragment_to_changePhoneNumberFragment);
    }

    public static NavDirections actionAccountSettingsFragmentToDeleteAccountFragment() {
        return new ActionOnlyNavDirections(R.id.action_accountSettingsFragment_to_deleteAccountFragment);
    }

    public static NavDirections actionAccountSettingsFragmentToAdvancedPinSettingsActivity() {
        return new ActionOnlyNavDirections(R.id.action_accountSettingsFragment_to_advancedPinSettingsActivity);
    }

    public static NavDirections actionAccountSettingsFragmentToOldDeviceTransferActivity() {
        return new ActionOnlyNavDirections(R.id.action_accountSettingsFragment_to_oldDeviceTransferActivity);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }
}
