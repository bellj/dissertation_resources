package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\b\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bR\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/ExternalLinkPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "linkId", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;I)V", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "getLinkId", "()I", "getTitle", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExternalLinkPreference extends PreferenceModel<ExternalLinkPreference> {
    private final DSLSettingsIcon icon;
    private final int linkId;
    private final DSLSettingsText title;

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsIcon getIcon() {
        return this.icon;
    }

    public final int getLinkId() {
        return this.linkId;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ExternalLinkPreference(DSLSettingsText dSLSettingsText, DSLSettingsIcon dSLSettingsIcon, int i) {
        super(null, null, null, null, false, 31, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        this.title = dSLSettingsText;
        this.icon = dSLSettingsIcon;
        this.linkId = i;
    }
}
