package org.thoughtcrime.securesms.components.location;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class SignalMapView extends LinearLayout {
    private ImageView imageView;
    private MapView mapView;
    private TextView textView;

    public SignalMapView(Context context) {
        this(context, null);
    }

    public SignalMapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize(context);
    }

    public SignalMapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        initialize(context);
    }

    private void initialize(Context context) {
        setOrientation(1);
        LayoutInflater.from(context).inflate(R.layout.signal_map_view, (ViewGroup) this, true);
        this.mapView = (MapView) findViewById(R.id.map_view);
        this.imageView = (ImageView) findViewById(R.id.image_view);
        this.textView = (TextView) findViewById(R.id.address_view);
    }

    public ListenableFuture<Bitmap> display(final SignalPlace signalPlace) {
        final SettableFuture settableFuture = new SettableFuture();
        this.mapView.onCreate(null);
        this.mapView.onResume();
        this.mapView.setVisibility(0);
        this.imageView.setVisibility(8);
        this.mapView.getMapAsync(new OnMapReadyCallback() { // from class: org.thoughtcrime.securesms.components.location.SignalMapView.1
            @Override // com.google.android.gms.maps.OnMapReadyCallback
            public void onMapReady(final GoogleMap googleMap) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(signalPlace.getLatLong(), 13.0f));
                googleMap.addMarker(new MarkerOptions().position(signalPlace.getLatLong()));
                googleMap.setBuildingsEnabled(true);
                googleMap.setMapType(1);
                googleMap.getUiSettings().setAllGesturesEnabled(false);
                googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() { // from class: org.thoughtcrime.securesms.components.location.SignalMapView.1.1
                    @Override // com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback
                    public void onMapLoaded() {
                        googleMap.snapshot(new GoogleMap.SnapshotReadyCallback() { // from class: org.thoughtcrime.securesms.components.location.SignalMapView.1.1.1
                            @Override // com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback
                            public void onSnapshotReady(Bitmap bitmap) {
                                settableFuture.set(bitmap);
                                SignalMapView.this.imageView.setImageBitmap(bitmap);
                                SignalMapView.this.imageView.setVisibility(0);
                                SignalMapView.this.mapView.setVisibility(8);
                                SignalMapView.this.mapView.onPause();
                                SignalMapView.this.mapView.onDestroy();
                            }
                        });
                    }
                });
            }
        });
        this.textView.setText(signalPlace.getDescription());
        return settableFuture;
    }
}
