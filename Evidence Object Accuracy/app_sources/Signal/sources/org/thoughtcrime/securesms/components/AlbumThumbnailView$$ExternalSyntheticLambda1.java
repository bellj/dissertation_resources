package org.thoughtcrime.securesms.components;

import android.view.View;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AlbumThumbnailView$$ExternalSyntheticLambda1 implements View.OnLongClickListener {
    public final /* synthetic */ AlbumThumbnailView f$0;

    public /* synthetic */ AlbumThumbnailView$$ExternalSyntheticLambda1(AlbumThumbnailView albumThumbnailView) {
        this.f$0 = albumThumbnailView;
    }

    @Override // android.view.View.OnLongClickListener
    public final boolean onLongClick(View view) {
        return this.f$0.lambda$new$1(view);
    }
}
