package org.thoughtcrime.securesms.components.settings.app;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: AppSettingsState.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000f\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007¢\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0007HÆ\u0003J'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007HÆ\u0001J\u0013\u0010\u0013\u001a\u00020\u00072\b\u0010\u0014\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0015\u001a\u00020\u0005HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsState;", "", "self", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "unreadPaymentsCount", "", "hasExpiredGiftBadge", "", "(Lorg/thoughtcrime/securesms/recipients/Recipient;IZ)V", "getHasExpiredGiftBadge", "()Z", "getSelf", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getUnreadPaymentsCount", "()I", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppSettingsState {
    private final boolean hasExpiredGiftBadge;
    private final Recipient self;
    private final int unreadPaymentsCount;

    public static /* synthetic */ AppSettingsState copy$default(AppSettingsState appSettingsState, Recipient recipient, int i, boolean z, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            recipient = appSettingsState.self;
        }
        if ((i2 & 2) != 0) {
            i = appSettingsState.unreadPaymentsCount;
        }
        if ((i2 & 4) != 0) {
            z = appSettingsState.hasExpiredGiftBadge;
        }
        return appSettingsState.copy(recipient, i, z);
    }

    public final Recipient component1() {
        return this.self;
    }

    public final int component2() {
        return this.unreadPaymentsCount;
    }

    public final boolean component3() {
        return this.hasExpiredGiftBadge;
    }

    public final AppSettingsState copy(Recipient recipient, int i, boolean z) {
        Intrinsics.checkNotNullParameter(recipient, "self");
        return new AppSettingsState(recipient, i, z);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof AppSettingsState)) {
            return false;
        }
        AppSettingsState appSettingsState = (AppSettingsState) obj;
        return Intrinsics.areEqual(this.self, appSettingsState.self) && this.unreadPaymentsCount == appSettingsState.unreadPaymentsCount && this.hasExpiredGiftBadge == appSettingsState.hasExpiredGiftBadge;
    }

    public int hashCode() {
        int hashCode = ((this.self.hashCode() * 31) + this.unreadPaymentsCount) * 31;
        boolean z = this.hasExpiredGiftBadge;
        if (z) {
            z = true;
        }
        int i = z ? 1 : 0;
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        return hashCode + i;
    }

    public String toString() {
        return "AppSettingsState(self=" + this.self + ", unreadPaymentsCount=" + this.unreadPaymentsCount + ", hasExpiredGiftBadge=" + this.hasExpiredGiftBadge + ')';
    }

    public AppSettingsState(Recipient recipient, int i, boolean z) {
        Intrinsics.checkNotNullParameter(recipient, "self");
        this.self = recipient;
        this.unreadPaymentsCount = i;
        this.hasExpiredGiftBadge = z;
    }

    public final Recipient getSelf() {
        return this.self;
    }

    public final int getUnreadPaymentsCount() {
        return this.unreadPaymentsCount;
    }

    public final boolean getHasExpiredGiftBadge() {
        return this.hasExpiredGiftBadge;
    }
}
