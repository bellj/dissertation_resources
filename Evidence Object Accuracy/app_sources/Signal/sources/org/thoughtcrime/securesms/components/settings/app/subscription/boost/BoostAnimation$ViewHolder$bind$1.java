package org.thoughtcrime.securesms.components.settings.app.subscription.boost;

import android.animation.Animator;
import kotlin.Metadata;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.components.settings.app.subscription.boost.BoostAnimation;

/* compiled from: BoostAnimation.kt */
@Metadata(d1 = {"\u0000\u0017\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000*\u0001\u0000\b\n\u0018\u00002\u00020\u0001J\u0012\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005H\u0016¨\u0006\u0006"}, d2 = {"org/thoughtcrime/securesms/components/settings/app/subscription/boost/BoostAnimation$ViewHolder$bind$1", "Lorg/thoughtcrime/securesms/animation/AnimationCompleteListener;", "onAnimationEnd", "", "animation", "Landroid/animation/Animator;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class BoostAnimation$ViewHolder$bind$1 extends AnimationCompleteListener {
    final /* synthetic */ BoostAnimation.ViewHolder this$0;

    public BoostAnimation$ViewHolder$bind$1(BoostAnimation.ViewHolder viewHolder) {
        this.this$0 = viewHolder;
    }

    @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
    public void onAnimationEnd(Animator animator) {
        this.this$0.lottie.removeAnimatorListener(this);
        this.this$0.lottie.setMinAndMaxFrame(30, 91);
        this.this$0.lottie.setRepeatMode(1);
        this.this$0.lottie.setRepeatCount(-1);
        this.this$0.lottie.setFrame(30);
        this.this$0.lottie.playAnimation();
    }
}
