package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: SoundsAndNotificationsSettingsViewModel.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0001\u0017B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000e\u001a\u00020\u000fJ\u000e\u0010\u0010\u001a\u00020\u000f2\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u000f2\u0006\u0010\u0014\u001a\u00020\u0015J\u0006\u0010\u0016\u001a\u00020\u000fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\t0\rX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsRepository;)V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "channelConsistencyCheck", "", "setMentionSetting", "mentionSetting", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$MentionSetting;", "setMuteUntil", "muteUntil", "", "unmute", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SoundsAndNotificationsSettingsViewModel extends ViewModel {
    private final RecipientId recipientId;
    private final SoundsAndNotificationsSettingsRepository repository;
    private final LiveData<SoundsAndNotificationsSettingsState> state;
    private final Store<SoundsAndNotificationsSettingsState> store;

    /* JADX DEBUG: Type inference failed for r12v2. Raw type applied. Possible types: androidx.lifecycle.LiveData<org.thoughtcrime.securesms.recipients.Recipient>, androidx.lifecycle.LiveData<Input> */
    public SoundsAndNotificationsSettingsViewModel(RecipientId recipientId, SoundsAndNotificationsSettingsRepository soundsAndNotificationsSettingsRepository) {
        Intrinsics.checkNotNullParameter(recipientId, "recipientId");
        Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsRepository, "repository");
        this.recipientId = recipientId;
        this.repository = soundsAndNotificationsSettingsRepository;
        Store<SoundsAndNotificationsSettingsState> store = new Store<>(new SoundsAndNotificationsSettingsState(null, 0, null, false, false, false, 63, null));
        this.store = store;
        LiveData<SoundsAndNotificationsSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
        store.update(Recipient.live(recipientId).getLiveData(), new Store.Action() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsViewModel$$ExternalSyntheticLambda1
            @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
            public final Object apply(Object obj, Object obj2) {
                return SoundsAndNotificationsSettingsViewModel.m1227_init_$lambda0(SoundsAndNotificationsSettingsViewModel.this, (Recipient) obj, (SoundsAndNotificationsSettingsState) obj2);
            }
        });
    }

    public final LiveData<SoundsAndNotificationsSettingsState> getState() {
        return this.state;
    }

    /* renamed from: _init_$lambda-0 */
    public static final SoundsAndNotificationsSettingsState m1227_init_$lambda0(SoundsAndNotificationsSettingsViewModel soundsAndNotificationsSettingsViewModel, Recipient recipient, SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState) {
        Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsViewModel, "this$0");
        RecipientId recipientId = soundsAndNotificationsSettingsViewModel.recipientId;
        long muteUntil = recipient.isMuted() ? recipient.getMuteUntil() : 0;
        RecipientDatabase.MentionSetting mentionSetting = recipient.getMentionSetting();
        boolean isPushV2Group = recipient.isPushV2Group();
        boolean z = recipient.getNotificationChannel() != null || !NotificationChannels.supported();
        Intrinsics.checkNotNullExpressionValue(soundsAndNotificationsSettingsState, "state");
        Intrinsics.checkNotNullExpressionValue(mentionSetting, "mentionSetting");
        return SoundsAndNotificationsSettingsState.copy$default(soundsAndNotificationsSettingsState, recipientId, muteUntil, mentionSetting, z, isPushV2Group, false, 32, null);
    }

    public final void setMuteUntil(long j) {
        this.repository.setMuteUntil(this.recipientId, j);
    }

    public final void unmute() {
        this.repository.setMuteUntil(this.recipientId, 0);
    }

    public final void setMentionSetting(RecipientDatabase.MentionSetting mentionSetting) {
        Intrinsics.checkNotNullParameter(mentionSetting, "mentionSetting");
        this.repository.setMentionSetting(this.recipientId, mentionSetting);
    }

    /* renamed from: channelConsistencyCheck$lambda-1 */
    public static final SoundsAndNotificationsSettingsState m1228channelConsistencyCheck$lambda1(SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState) {
        Intrinsics.checkNotNullExpressionValue(soundsAndNotificationsSettingsState, "s");
        return SoundsAndNotificationsSettingsState.copy$default(soundsAndNotificationsSettingsState, null, 0, null, false, false, false, 31, null);
    }

    public final void channelConsistencyCheck() {
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsViewModel$$ExternalSyntheticLambda0
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return SoundsAndNotificationsSettingsViewModel.m1228channelConsistencyCheck$lambda1((SoundsAndNotificationsSettingsState) obj);
            }
        });
        this.repository.ensureCustomChannelConsistency(new SoundsAndNotificationsSettingsViewModel$channelConsistencyCheck$2(this));
    }

    /* compiled from: SoundsAndNotificationsSettingsViewModel.kt */
    @Metadata(d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J%\u0010\u0007\u001a\u0002H\b\"\b\b\u0000\u0010\b*\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\b0\u000bH\u0016¢\u0006\u0002\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "recipientId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "repository", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsRepository;", "(Lorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsRepository;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final RecipientId recipientId;
        private final SoundsAndNotificationsSettingsRepository repository;

        public Factory(RecipientId recipientId, SoundsAndNotificationsSettingsRepository soundsAndNotificationsSettingsRepository) {
            Intrinsics.checkNotNullParameter(recipientId, "recipientId");
            Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsRepository, "repository");
            this.recipientId = recipientId;
            this.repository = soundsAndNotificationsSettingsRepository;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new SoundsAndNotificationsSettingsViewModel(this.recipientId, this.repository));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
