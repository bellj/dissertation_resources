package org.thoughtcrime.securesms.components.settings.app.changenumber;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;

/* compiled from: ChangeNumberLockActivity.kt */
@Metadata(d1 = {"\u0000\b\n\u0000\n\u0002\u0010\u000e\n\u0000\"\u000e\u0010\u0000\u001a\u00020\u0001X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0002"}, d2 = {"TAG", "", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberLockActivityKt {
    private static final String TAG;

    static {
        String tag = Log.tag(ChangeNumberLockActivity.class);
        Intrinsics.checkNotNullExpressionValue(tag, "tag(ChangeNumberLockActivity::class.java)");
        TAG = tag;
    }
}
