package org.thoughtcrime.securesms.components.settings.app.appearance;

import android.app.Activity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.jobs.EmojiSearchIndexDownloadJob;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.SplashScreenUtil;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: AppearanceSettingsViewModel.kt */
@Metadata(d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010J\u0018\u0010\u0011\u001a\u00020\u000b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0015R\u0017\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u0007R\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00050\tX\u0004¢\u0006\u0002\n\u0000¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/appearance/AppearanceSettingsViewModel;", "Landroidx/lifecycle/ViewModel;", "()V", "state", "Landroidx/lifecycle/LiveData;", "Lorg/thoughtcrime/securesms/components/settings/app/appearance/AppearanceSettingsState;", "getState", "()Landroidx/lifecycle/LiveData;", "store", "Lorg/thoughtcrime/securesms/util/livedata/Store;", "setLanguage", "", "language", "", "setMessageFontSize", MediaPreviewActivity.SIZE_EXTRA, "", "setTheme", "activity", "Landroid/app/Activity;", "theme", "Lorg/thoughtcrime/securesms/keyvalue/SettingsValues$Theme;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppearanceSettingsViewModel extends ViewModel {
    private final LiveData<AppearanceSettingsState> state;
    private final Store<AppearanceSettingsState> store;

    public AppearanceSettingsViewModel() {
        SettingsValues.Theme theme = SignalStore.settings().getTheme();
        Intrinsics.checkNotNullExpressionValue(theme, "settings().theme");
        int messageFontSize = SignalStore.settings().getMessageFontSize();
        String language = SignalStore.settings().getLanguage();
        Intrinsics.checkNotNullExpressionValue(language, "settings().language");
        Store<AppearanceSettingsState> store = new Store<>(new AppearanceSettingsState(theme, messageFontSize, language));
        this.store = store;
        LiveData<AppearanceSettingsState> stateLiveData = store.getStateLiveData();
        Intrinsics.checkNotNullExpressionValue(stateLiveData, "store.stateLiveData");
        this.state = stateLiveData;
    }

    public final LiveData<AppearanceSettingsState> getState() {
        return this.state;
    }

    /* renamed from: setTheme$lambda-0 */
    public static final AppearanceSettingsState m577setTheme$lambda0(SettingsValues.Theme theme, AppearanceSettingsState appearanceSettingsState) {
        Intrinsics.checkNotNullParameter(theme, "$theme");
        Intrinsics.checkNotNullExpressionValue(appearanceSettingsState, "it");
        return AppearanceSettingsState.copy$default(appearanceSettingsState, theme, 0, null, 6, null);
    }

    public final void setTheme(Activity activity, SettingsValues.Theme theme) {
        Intrinsics.checkNotNullParameter(theme, "theme");
        this.store.update(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsViewModel$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AppearanceSettingsViewModel.m577setTheme$lambda0(SettingsValues.Theme.this, (AppearanceSettingsState) obj);
            }
        });
        SignalStore.settings().setTheme(theme);
        SplashScreenUtil.setSplashScreenThemeIfNecessary(activity, theme);
    }

    /* renamed from: setLanguage$lambda-1 */
    public static final AppearanceSettingsState m575setLanguage$lambda1(String str, AppearanceSettingsState appearanceSettingsState) {
        Intrinsics.checkNotNullParameter(str, "$language");
        Intrinsics.checkNotNullExpressionValue(appearanceSettingsState, "it");
        return AppearanceSettingsState.copy$default(appearanceSettingsState, null, 0, str, 3, null);
    }

    public final void setLanguage(String str) {
        Intrinsics.checkNotNullParameter(str, "language");
        this.store.update(new Function(str) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsViewModel$$ExternalSyntheticLambda2
            public final /* synthetic */ String f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AppearanceSettingsViewModel.m575setLanguage$lambda1(this.f$0, (AppearanceSettingsState) obj);
            }
        });
        SignalStore.settings().setLanguage(str);
        EmojiSearchIndexDownloadJob.scheduleImmediately();
    }

    /* renamed from: setMessageFontSize$lambda-2 */
    public static final AppearanceSettingsState m576setMessageFontSize$lambda2(int i, AppearanceSettingsState appearanceSettingsState) {
        Intrinsics.checkNotNullExpressionValue(appearanceSettingsState, "it");
        return AppearanceSettingsState.copy$default(appearanceSettingsState, null, i, null, 5, null);
    }

    public final void setMessageFontSize(int i) {
        this.store.update(new Function(i) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsViewModel$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return AppearanceSettingsViewModel.m576setMessageFontSize$lambda2(this.f$0, (AppearanceSettingsState) obj);
            }
        });
        SignalStore.settings().setMessageFontSize(i);
    }
}
