package org.thoughtcrime.securesms.components.settings.conversation;

import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.livedata.LiveDataUtil;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda7 implements LiveDataUtil.Combine {
    @Override // org.thoughtcrime.securesms.util.livedata.LiveDataUtil.Combine
    public final Object apply(Object obj, Object obj2) {
        return ConversationSettingsViewModel.GroupSettingsViewModel.m1136_init_$lambda2((Recipient) obj, ((Boolean) obj2).booleanValue());
    }
}
