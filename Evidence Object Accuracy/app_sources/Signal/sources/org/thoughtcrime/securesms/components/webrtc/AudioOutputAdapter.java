package org.thoughtcrime.securesms.components.webrtc;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class AudioOutputAdapter extends RecyclerView.Adapter<ViewHolder> {
    private final List<WebRtcAudioOutput> audioOutputs;
    private final OnAudioOutputChangedListener onAudioOutputChangedListener;
    private WebRtcAudioOutput selected;

    public AudioOutputAdapter(OnAudioOutputChangedListener onAudioOutputChangedListener, List<WebRtcAudioOutput> list) {
        this.audioOutputs = list;
        this.onAudioOutputChangedListener = onAudioOutputChangedListener;
    }

    public void setSelectedOutput(WebRtcAudioOutput webRtcAudioOutput) {
        this.selected = webRtcAudioOutput;
        notifyDataSetChanged();
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.audio_output_adapter_radio_item, viewGroup, false), new Consumer() { // from class: org.thoughtcrime.securesms.components.webrtc.AudioOutputAdapter$$ExternalSyntheticLambda0
            @Override // androidx.core.util.Consumer
            public final void accept(Object obj) {
                AudioOutputAdapter.$r8$lambda$npH9OdyKDwIRZCJmbT3rFXqqYSE(AudioOutputAdapter.this, ((Integer) obj).intValue());
            }
        });
    }

    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.bind(this.audioOutputs.get(i), this.selected);
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.audioOutputs.size();
    }

    public void handlePositionSelected(int i) {
        WebRtcAudioOutput webRtcAudioOutput = this.audioOutputs.get(i);
        if (webRtcAudioOutput != this.selected) {
            setSelectedOutput(webRtcAudioOutput);
            this.onAudioOutputChangedListener.audioOutputChanged(this.selected);
        }
    }

    /* loaded from: classes4.dex */
    public static class ViewHolder extends RecyclerView.ViewHolder implements CompoundButton.OnCheckedChangeListener {
        private final Consumer<Integer> onPressed;
        private final RadioButton radioButton;

        public ViewHolder(View view, Consumer<Integer> consumer) {
            super(view);
            this.radioButton = (RadioButton) view.findViewById(R.id.radio);
            this.onPressed = consumer;
        }

        void bind(WebRtcAudioOutput webRtcAudioOutput, WebRtcAudioOutput webRtcAudioOutput2) {
            this.radioButton.setText(webRtcAudioOutput.getLabelRes());
            boolean z = false;
            this.radioButton.setCompoundDrawablesRelativeWithIntrinsicBounds(webRtcAudioOutput.getIconRes(), 0, 0, 0);
            this.radioButton.setOnCheckedChangeListener(null);
            RadioButton radioButton = this.radioButton;
            if (webRtcAudioOutput == webRtcAudioOutput2) {
                z = true;
            }
            radioButton.setChecked(z);
            this.radioButton.setOnCheckedChangeListener(this);
        }

        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            int adapterPosition = getAdapterPosition();
            if (adapterPosition != -1) {
                this.onPressed.accept(Integer.valueOf(adapterPosition));
            }
        }
    }
}
