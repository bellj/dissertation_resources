package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.PinHashing;
import org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.util.CommunicationActions;
import org.thoughtcrime.securesms.util.SupportEmailUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberRegistrationLockFragment.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0014J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0014J\b\u0010\t\u001a\u00020\u0006H\u0014J\b\u0010\n\u001a\u00020\u0006H\u0002J\u001a\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0016J\b\u0010\u0010\u001a\u00020\u0006H\u0014¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberRegistrationLockFragment;", "Lorg/thoughtcrime/securesms/registration/fragments/BaseRegistrationLockFragment;", "()V", "getViewModel", "Lorg/thoughtcrime/securesms/registration/viewmodel/BaseRegistrationViewModel;", "handleSuccessfulPinEntry", "", "pin", "", "navigateToAccountLocked", "navigateUp", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "sendEmailToSupport", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberRegistrationLockFragment extends BaseRegistrationLockFragment {
    public ChangeNumberRegistrationLockFragment() {
        super(R.layout.fragment_change_number_registration_lock);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        ((Toolbar) findViewById).setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberRegistrationLockFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberRegistrationLockFragment.$r8$lambda$J4NTOB5oHWA7DqFCI56O152f7_Q(ChangeNumberRegistrationLockFragment.this, view2);
            }
        });
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberRegistrationLockFragment$onViewCreated$2
            final /* synthetic */ ChangeNumberRegistrationLockFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                ChangeNumberRegistrationLockFragment.access$navigateUp(this.this$0);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m609onViewCreated$lambda0(ChangeNumberRegistrationLockFragment changeNumberRegistrationLockFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberRegistrationLockFragment, "this$0");
        changeNumberRegistrationLockFragment.navigateUp();
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected BaseRegistrationViewModel getViewModel() {
        return ChangeNumberUtil.getViewModel(this);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected void navigateToAccountLocked() {
        NavController findNavController = FragmentKt.findNavController(this);
        NavDirections actionChangeNumberRegistrationLockToChangeNumberAccountLocked = ChangeNumberRegistrationLockFragmentDirections.actionChangeNumberRegistrationLockToChangeNumberAccountLocked();
        Intrinsics.checkNotNullExpressionValue(actionChangeNumberRegistrationLockToChangeNumberAccountLocked, "actionChangeNumberRegist…angeNumberAccountLocked()");
        SafeNavigation.safeNavigate(findNavController, actionChangeNumberRegistrationLockToChangeNumberAccountLocked);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected void handleSuccessfulPinEntry(String str) {
        Intrinsics.checkNotNullParameter(str, "pin");
        String localPinHash = SignalStore.kbsValues().getLocalPinHash();
        boolean z = localPinHash != null ? !PinHashing.verifyLocalPinHash(localPinHash, str) : false;
        this.pinButton.cancelSpinning();
        if (z) {
            NavController findNavController = FragmentKt.findNavController(this);
            NavDirections actionChangeNumberRegistrationLockToChangeNumberPinDiffers = ChangeNumberRegistrationLockFragmentDirections.actionChangeNumberRegistrationLockToChangeNumberPinDiffers();
            Intrinsics.checkNotNullExpressionValue(actionChangeNumberRegistrationLockToChangeNumberPinDiffers, "actionChangeNumberRegist…oChangeNumberPinDiffers()");
            SafeNavigation.safeNavigate(findNavController, actionChangeNumberRegistrationLockToChangeNumberPinDiffers);
            return;
        }
        ChangeNumberUtil.INSTANCE.changeNumberSuccess(this);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseRegistrationLockFragment
    protected void sendEmailToSupport() {
        String generateSupportEmailBody = SupportEmailUtil.generateSupportEmailBody(requireContext(), R.string.ChangeNumberRegistrationLockFragment__signal_change_number_need_help_with_pin_for_android_v2_pin, null, null);
        Intrinsics.checkNotNullExpressionValue(generateSupportEmailBody, "generateSupportEmailBody…   null,\n      null\n    )");
        CommunicationActions.openEmail(requireContext(), SupportEmailUtil.getSupportEmailAddress(requireContext()), getString(R.string.ChangeNumberRegistrationLockFragment__signal_change_number_need_help_with_pin_for_android_v2_pin), generateSupportEmailBody);
    }

    public final void navigateUp() {
        if (SignalStore.misc().isChangeNumberLocked()) {
            ChangeNumberLockActivity.Companion companion = ChangeNumberLockActivity.Companion;
            Context requireContext = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            startActivity(companion.createIntent(requireContext));
            return;
        }
        FragmentKt.findNavController(this).navigateUp();
    }
}
