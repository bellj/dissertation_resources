package org.thoughtcrime.securesms.components.voice;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.contacts.SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: VoiceNotePlaybackState.kt */
@Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\b\u0018\u0000 )2\u00020\u0001:\u0002()B=\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\b\u0012\u0006\u0010\f\u001a\u00020\r¢\u0006\u0002\u0010\u000eJ\u0006\u0010\u0019\u001a\u00020\u0000J\t\u0010\u001a\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001b\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001c\u001a\u00020\u0005HÆ\u0003J\t\u0010\u001d\u001a\u00020\bHÆ\u0003J\t\u0010\u001e\u001a\u00020\nHÆ\u0003J\t\u0010\u001f\u001a\u00020\bHÆ\u0003J\t\u0010 \u001a\u00020\rHÆ\u0003JO\u0010!\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\b2\b\b\u0002\u0010\f\u001a\u00020\rHÆ\u0001J\u0013\u0010\"\u001a\u00020\b2\b\u0010#\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010$\u001a\u00020%HÖ\u0001J\t\u0010&\u001a\u00020'HÖ\u0001R\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0011\u0010\u0007\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0011R\u0011\u0010\u000b\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\u0011R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\t\u001a\u00020\n¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015R\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0013R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018¨\u0006*"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState;", "", "uri", "Landroid/net/Uri;", "playheadPositionMillis", "", "trackDuration", "isAutoReset", "", "speed", "", "isPlaying", "clipType", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;", "(Landroid/net/Uri;JJZFZLorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;)V", "getClipType", "()Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;", "()Z", "getPlayheadPositionMillis", "()J", "getSpeed", "()F", "getTrackDuration", "getUri", "()Landroid/net/Uri;", "asPaused", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "toString", "", "ClipType", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNotePlaybackState {
    public static final Companion Companion = new Companion(null);
    public static final VoiceNotePlaybackState NONE;
    private final ClipType clipType;
    private final boolean isAutoReset;
    private final boolean isPlaying;
    private final long playheadPositionMillis;
    private final float speed;
    private final long trackDuration;
    private final Uri uri;

    public static /* synthetic */ VoiceNotePlaybackState copy$default(VoiceNotePlaybackState voiceNotePlaybackState, Uri uri, long j, long j2, boolean z, float f, boolean z2, ClipType clipType, int i, Object obj) {
        return voiceNotePlaybackState.copy((i & 1) != 0 ? voiceNotePlaybackState.uri : uri, (i & 2) != 0 ? voiceNotePlaybackState.playheadPositionMillis : j, (i & 4) != 0 ? voiceNotePlaybackState.trackDuration : j2, (i & 8) != 0 ? voiceNotePlaybackState.isAutoReset : z, (i & 16) != 0 ? voiceNotePlaybackState.speed : f, (i & 32) != 0 ? voiceNotePlaybackState.isPlaying : z2, (i & 64) != 0 ? voiceNotePlaybackState.clipType : clipType);
    }

    public final Uri component1() {
        return this.uri;
    }

    public final long component2() {
        return this.playheadPositionMillis;
    }

    public final long component3() {
        return this.trackDuration;
    }

    public final boolean component4() {
        return this.isAutoReset;
    }

    public final float component5() {
        return this.speed;
    }

    public final boolean component6() {
        return this.isPlaying;
    }

    public final ClipType component7() {
        return this.clipType;
    }

    public final VoiceNotePlaybackState copy(Uri uri, long j, long j2, boolean z, float f, boolean z2, ClipType clipType) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        Intrinsics.checkNotNullParameter(clipType, "clipType");
        return new VoiceNotePlaybackState(uri, j, j2, z, f, z2, clipType);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof VoiceNotePlaybackState)) {
            return false;
        }
        VoiceNotePlaybackState voiceNotePlaybackState = (VoiceNotePlaybackState) obj;
        return Intrinsics.areEqual(this.uri, voiceNotePlaybackState.uri) && this.playheadPositionMillis == voiceNotePlaybackState.playheadPositionMillis && this.trackDuration == voiceNotePlaybackState.trackDuration && this.isAutoReset == voiceNotePlaybackState.isAutoReset && Intrinsics.areEqual(Float.valueOf(this.speed), Float.valueOf(voiceNotePlaybackState.speed)) && this.isPlaying == voiceNotePlaybackState.isPlaying && Intrinsics.areEqual(this.clipType, voiceNotePlaybackState.clipType);
    }

    public int hashCode() {
        int hashCode = ((((this.uri.hashCode() * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.playheadPositionMillis)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.trackDuration)) * 31;
        boolean z = this.isAutoReset;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int floatToIntBits = (((hashCode + i2) * 31) + Float.floatToIntBits(this.speed)) * 31;
        boolean z2 = this.isPlaying;
        if (!z2) {
            i = z2 ? 1 : 0;
        }
        return ((floatToIntBits + i) * 31) + this.clipType.hashCode();
    }

    public String toString() {
        return "VoiceNotePlaybackState(uri=" + this.uri + ", playheadPositionMillis=" + this.playheadPositionMillis + ", trackDuration=" + this.trackDuration + ", isAutoReset=" + this.isAutoReset + ", speed=" + this.speed + ", isPlaying=" + this.isPlaying + ", clipType=" + this.clipType + ')';
    }

    public VoiceNotePlaybackState(Uri uri, long j, long j2, boolean z, float f, boolean z2, ClipType clipType) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        Intrinsics.checkNotNullParameter(clipType, "clipType");
        this.uri = uri;
        this.playheadPositionMillis = j;
        this.trackDuration = j2;
        this.isAutoReset = z;
        this.speed = f;
        this.isPlaying = z2;
        this.clipType = clipType;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final long getPlayheadPositionMillis() {
        return this.playheadPositionMillis;
    }

    public final long getTrackDuration() {
        return this.trackDuration;
    }

    public final boolean isAutoReset() {
        return this.isAutoReset;
    }

    public final float getSpeed() {
        return this.speed;
    }

    public final boolean isPlaying() {
        return this.isPlaying;
    }

    public final ClipType getClipType() {
        return this.clipType;
    }

    /* compiled from: VoiceNotePlaybackState.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0010\u0010\u0003\u001a\u00020\u00048\u0006X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0005"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$Companion;", "", "()V", "NONE", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    static {
        Companion = new Companion(null);
        Uri uri = Uri.EMPTY;
        Intrinsics.checkNotNullExpressionValue(uri, "EMPTY");
        NONE = new VoiceNotePlaybackState(uri, 0, 0, false, 1.0f, false, ClipType.Idle.INSTANCE);
    }

    public final VoiceNotePlaybackState asPaused() {
        return copy$default(this, null, 0, 0, false, 0.0f, false, null, 95, null);
    }

    /* compiled from: VoiceNotePlaybackState.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0003\u0003\u0004\u0005B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0003\u0006\u0007\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;", "", "()V", "Draft", "Idle", "Message", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType$Message;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType$Draft;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType$Idle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class ClipType {
        public /* synthetic */ ClipType(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private ClipType() {
        }

        /* compiled from: VoiceNotePlaybackState.kt */
        @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003¢\u0006\u0002\u0010\nJ\t\u0010\u0013\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0014\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0015\u001a\u00020\u0005HÆ\u0003J\t\u0010\u0016\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0017\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0018\u001a\u00020\u0003HÆ\u0003JE\u0010\u0019\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00052\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dHÖ\u0003J\t\u0010\u001e\u001a\u00020\u001fHÖ\u0001J\t\u0010 \u001a\u00020!HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u000fR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\f¨\u0006\""}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType$Message;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;", "messageId", "", "senderId", "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "threadRecipientId", "messagePosition", "threadId", "timestamp", "(JLorg/thoughtcrime/securesms/recipients/RecipientId;Lorg/thoughtcrime/securesms/recipients/RecipientId;JJJ)V", "getMessageId", "()J", "getMessagePosition", "getSenderId", "()Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getThreadId", "getThreadRecipientId", "getTimestamp", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Message extends ClipType {
            private final long messageId;
            private final long messagePosition;
            private final RecipientId senderId;
            private final long threadId;
            private final RecipientId threadRecipientId;
            private final long timestamp;

            public final long component1() {
                return this.messageId;
            }

            public final RecipientId component2() {
                return this.senderId;
            }

            public final RecipientId component3() {
                return this.threadRecipientId;
            }

            public final long component4() {
                return this.messagePosition;
            }

            public final long component5() {
                return this.threadId;
            }

            public final long component6() {
                return this.timestamp;
            }

            public final Message copy(long j, RecipientId recipientId, RecipientId recipientId2, long j2, long j3, long j4) {
                Intrinsics.checkNotNullParameter(recipientId, "senderId");
                Intrinsics.checkNotNullParameter(recipientId2, "threadRecipientId");
                return new Message(j, recipientId, recipientId2, j2, j3, j4);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Message)) {
                    return false;
                }
                Message message = (Message) obj;
                return this.messageId == message.messageId && Intrinsics.areEqual(this.senderId, message.senderId) && Intrinsics.areEqual(this.threadRecipientId, message.threadRecipientId) && this.messagePosition == message.messagePosition && this.threadId == message.threadId && this.timestamp == message.timestamp;
            }

            public int hashCode() {
                return (((((((((SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messageId) * 31) + this.senderId.hashCode()) * 31) + this.threadRecipientId.hashCode()) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.messagePosition)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.threadId)) * 31) + SystemContactsRepository$LinkedContactDetails$$ExternalSyntheticBackport0.m(this.timestamp);
            }

            public String toString() {
                return "Message(messageId=" + this.messageId + ", senderId=" + this.senderId + ", threadRecipientId=" + this.threadRecipientId + ", messagePosition=" + this.messagePosition + ", threadId=" + this.threadId + ", timestamp=" + this.timestamp + ')';
            }

            public final long getMessageId() {
                return this.messageId;
            }

            public final RecipientId getSenderId() {
                return this.senderId;
            }

            public final RecipientId getThreadRecipientId() {
                return this.threadRecipientId;
            }

            public final long getMessagePosition() {
                return this.messagePosition;
            }

            public final long getThreadId() {
                return this.threadId;
            }

            public final long getTimestamp() {
                return this.timestamp;
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Message(long j, RecipientId recipientId, RecipientId recipientId2, long j2, long j3, long j4) {
                super(null);
                Intrinsics.checkNotNullParameter(recipientId, "senderId");
                Intrinsics.checkNotNullParameter(recipientId2, "threadRecipientId");
                this.messageId = j;
                this.senderId = recipientId;
                this.threadRecipientId = recipientId2;
                this.messagePosition = j2;
                this.threadId = j3;
                this.timestamp = j4;
            }
        }

        /* compiled from: VoiceNotePlaybackState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType$Draft;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Draft extends ClipType {
            public static final Draft INSTANCE = new Draft();

            private Draft() {
                super(null);
            }
        }

        /* compiled from: VoiceNotePlaybackState.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType$Idle;", "Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlaybackState$ClipType;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Idle extends ClipType {
            public static final Idle INSTANCE = new Idle();

            private Idle() {
                super(null);
            }
        }
    }
}
