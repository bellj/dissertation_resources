package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.app.Application;
import androidx.lifecycle.AbstractSavedStateViewModelFactory;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;
import androidx.savedstate.SavedStateRegistryOwner;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.functions.Consumer;
import io.reactivex.rxjava3.functions.Function;
import java.util.Objects;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.pin.KbsRepository;
import org.thoughtcrime.securesms.pin.TokenData;
import org.thoughtcrime.securesms.registration.VerifyAccountRepository;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyAccountResponseWithoutKbs;
import org.thoughtcrime.securesms.registration.VerifyCodeWithRegistrationLockResponseProcessor;
import org.thoughtcrime.securesms.registration.VerifyProcessor;
import org.thoughtcrime.securesms.registration.fragments.CountryPickerFragment;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;
import org.thoughtcrime.securesms.registration.viewmodel.NumberViewState;
import org.thoughtcrime.securesms.util.DefaultValueLiveData;
import org.whispersystems.signalservice.api.push.PNI;
import org.whispersystems.signalservice.internal.ServiceResponse;
import org.whispersystems.signalservice.internal.push.VerifyAccountResponse;
import org.whispersystems.signalservice.internal.push.WhoAmIResponse;

/* compiled from: ChangeNumberViewModel.kt */
@Metadata(d1 = {"\u0000\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u00029:B5\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f¢\u0006\u0002\u0010\rJ%\u0010\u0016\u001a\b\u0012\u0004\u0012\u0002H\u00180\u0017\"\b\b\u0000\u0010\u0018*\u00020\u00192\u0006\u0010\u001a\u001a\u0002H\u0018H\u0002¢\u0006\u0002\u0010\u001bJ\u0006\u0010\u001c\u001a\u00020\u001dJ\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00100\u001fJ\f\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00100\u001fJ\u0016\u0010!\u001a\b\u0012\u0004\u0012\u00020\"0\u00172\u0006\u0010\u001a\u001a\u00020\"H\u0015J\u001e\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\u00172\u0006\u0010\u001a\u001a\u00020$2\u0006\u0010%\u001a\u00020\u0003H\u0014J\u001a\u0010&\u001a\u00020'2\u0006\u0010(\u001a\u00020)2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0003J\u000e\u0010+\u001a\u00020'2\u0006\u0010,\u001a\u00020\u0003J\u001a\u0010-\u001a\u00020'2\u0006\u0010(\u001a\u00020)2\n\b\u0002\u0010*\u001a\u0004\u0018\u00010\u0003J\u000e\u0010.\u001a\u00020'2\u0006\u0010,\u001a\u00020\u0003J$\u0010/\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u000201000\u00172\u0006\u0010%\u001a\u00020\u00032\u0006\u00102\u001a\u000203H\u0014J\u0014\u00104\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u000205000\u0017H\u0014J\u0016\u00106\u001a\b\u0012\u0004\u0012\u00020$0\u00172\u0006\u0010%\u001a\u00020\u0003H\u0016J\u0016\u00107\u001a\b\u0012\u0004\u0012\u00020\"0\u00172\u0006\u00108\u001a\u00020\u0003H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0004¢\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001e\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0012\u001a\u00020\u0010@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015¨\u0006;"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel;", "Lorg/thoughtcrime/securesms/registration/viewmodel/BaseRegistrationViewModel;", "localNumber", "", "changeNumberRepository", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberRepository;", "savedState", "Landroidx/lifecycle/SavedStateHandle;", "password", "verifyAccountRepository", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository;", "kbsRepository", "Lorg/thoughtcrime/securesms/pin/KbsRepository;", "(Ljava/lang/String;Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberRepository;Landroidx/lifecycle/SavedStateHandle;Ljava/lang/String;Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository;Lorg/thoughtcrime/securesms/pin/KbsRepository;)V", "liveNewNumberState", "Lorg/thoughtcrime/securesms/util/DefaultValueLiveData;", "Lorg/thoughtcrime/securesms/registration/viewmodel/NumberViewState;", "liveOldNumberState", "<set-?>", "oldNumberState", "getOldNumberState", "()Lorg/thoughtcrime/securesms/registration/viewmodel/NumberViewState;", "attemptToUnlockChangeNumber", "Lio/reactivex/rxjava3/core/Single;", "T", "Lorg/thoughtcrime/securesms/registration/VerifyProcessor;", "processor", "(Lorg/thoughtcrime/securesms/registration/VerifyProcessor;)Lio/reactivex/rxjava3/core/Single;", "canContinue", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel$ContinueStatus;", "getLiveNewNumber", "Landroidx/lifecycle/LiveData;", "getLiveOldNumber", "onVerifySuccess", "Lorg/thoughtcrime/securesms/registration/VerifyAccountResponseProcessor;", "onVerifySuccessWithRegistrationLock", "Lorg/thoughtcrime/securesms/registration/VerifyCodeWithRegistrationLockResponseProcessor;", "pin", "setNewCountry", "", "countryCode", "", CountryPickerFragment.KEY_COUNTRY, "setNewNationalNumber", "number", "setOldCountry", "setOldNationalNumber", "verifyAccountWithRegistrationLock", "Lorg/whispersystems/signalservice/internal/ServiceResponse;", "Lorg/thoughtcrime/securesms/registration/VerifyAccountRepository$VerifyAccountWithRegistrationLockResponse;", "kbsTokenData", "Lorg/thoughtcrime/securesms/pin/TokenData;", "verifyAccountWithoutRegistrationLock", "Lorg/whispersystems/signalservice/internal/push/VerifyAccountResponse;", "verifyCodeAndRegisterAccountWithRegistrationLock", "verifyCodeWithoutRegistrationLock", "code", "ContinueStatus", "Factory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberViewModel extends BaseRegistrationViewModel {
    private final ChangeNumberRepository changeNumberRepository;
    private final DefaultValueLiveData<NumberViewState> liveNewNumberState = new DefaultValueLiveData<>(getNumber());
    private final DefaultValueLiveData<NumberViewState> liveOldNumberState = new DefaultValueLiveData<>(this.oldNumberState);
    private final String localNumber;
    private NumberViewState oldNumberState;

    /* compiled from: ChangeNumberViewModel.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0005\b\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004j\u0002\b\u0005¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel$ContinueStatus;", "", "(Ljava/lang/String;I)V", "CAN_CONTINUE", "INVALID_NUMBER", "OLD_NUMBER_DOESNT_MATCH", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum ContinueStatus {
        CAN_CONTINUE,
        INVALID_NUMBER,
        OLD_NUMBER_DOESNT_MATCH
    }

    /* renamed from: attemptToUnlockChangeNumber$lambda-3 */
    public static final VerifyProcessor m623attemptToUnlockChangeNumber$lambda3(VerifyProcessor verifyProcessor, Throwable th) {
        Intrinsics.checkNotNullParameter(verifyProcessor, "$processor");
        return verifyProcessor;
    }

    /* renamed from: onVerifySuccess$lambda-4 */
    public static final VerifyAccountResponseProcessor m624onVerifySuccess$lambda4(VerifyAccountResponseProcessor verifyAccountResponseProcessor, Unit unit) {
        Intrinsics.checkNotNullParameter(verifyAccountResponseProcessor, "$processor");
        return verifyAccountResponseProcessor;
    }

    /* renamed from: onVerifySuccessWithRegistrationLock$lambda-6 */
    public static final VerifyCodeWithRegistrationLockResponseProcessor m626onVerifySuccessWithRegistrationLock$lambda6(VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor, Unit unit) {
        Intrinsics.checkNotNullParameter(verifyCodeWithRegistrationLockResponseProcessor, "$processor");
        return verifyCodeWithRegistrationLockResponseProcessor;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ChangeNumberViewModel(String str, ChangeNumberRepository changeNumberRepository, SavedStateHandle savedStateHandle, String str2, VerifyAccountRepository verifyAccountRepository, KbsRepository kbsRepository) {
        super(savedStateHandle, verifyAccountRepository, kbsRepository, str2);
        Intrinsics.checkNotNullParameter(str, "localNumber");
        Intrinsics.checkNotNullParameter(changeNumberRepository, "changeNumberRepository");
        Intrinsics.checkNotNullParameter(savedStateHandle, "savedState");
        Intrinsics.checkNotNullParameter(str2, "password");
        Intrinsics.checkNotNullParameter(verifyAccountRepository, "verifyAccountRepository");
        Intrinsics.checkNotNullParameter(kbsRepository, "kbsRepository");
        this.localNumber = str;
        this.changeNumberRepository = changeNumberRepository;
        NumberViewState build = new NumberViewState.Builder().build();
        Intrinsics.checkNotNullExpressionValue(build, "Builder().build()");
        this.oldNumberState = build;
        try {
            int countryCode = PhoneNumberUtil.getInstance().parse(str, null).getCountryCode();
            setOldCountry$default(this, countryCode, null, 2, null);
            setNewCountry$default(this, countryCode, null, 2, null);
        } catch (NumberParseException unused) {
            Log.i(ChangeNumberViewModelKt.TAG, "Unable to parse number for default country code");
        }
    }

    public final NumberViewState getOldNumberState() {
        return this.oldNumberState;
    }

    public final LiveData<NumberViewState> getLiveOldNumber() {
        return this.liveOldNumberState;
    }

    public final LiveData<NumberViewState> getLiveNewNumber() {
        return this.liveNewNumberState;
    }

    public final void setOldNationalNumber(String str) {
        Intrinsics.checkNotNullParameter(str, "number");
        NumberViewState build = this.oldNumberState.toBuilder().nationalNumber(str).build();
        Intrinsics.checkNotNullExpressionValue(build, "oldNumberState.toBuilder…er(number)\n      .build()");
        this.oldNumberState = build;
        this.liveOldNumberState.setValue(build);
    }

    public static /* synthetic */ void setOldCountry$default(ChangeNumberViewModel changeNumberViewModel, int i, String str, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            str = null;
        }
        changeNumberViewModel.setOldCountry(i, str);
    }

    public final void setOldCountry(int i, String str) {
        NumberViewState build = this.oldNumberState.toBuilder().selectedCountryDisplayName(str).countryCode(i).build();
        Intrinsics.checkNotNullExpressionValue(build, "oldNumberState.toBuilder…untryCode)\n      .build()");
        this.oldNumberState = build;
        this.liveOldNumberState.setValue(build);
    }

    public final void setNewNationalNumber(String str) {
        Intrinsics.checkNotNullParameter(str, "number");
        setNationalNumber(str);
        this.liveNewNumberState.setValue(getNumber());
    }

    public static /* synthetic */ void setNewCountry$default(ChangeNumberViewModel changeNumberViewModel, int i, String str, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            str = null;
        }
        changeNumberViewModel.setNewCountry(i, str);
    }

    public final void setNewCountry(int i, String str) {
        onCountrySelected(str, i);
        this.liveNewNumberState.setValue(getNumber());
    }

    public final ContinueStatus canContinue() {
        if (!Intrinsics.areEqual(this.oldNumberState.getE164Number(), this.localNumber)) {
            return ContinueStatus.OLD_NUMBER_DOESNT_MATCH;
        }
        if (getNumber().isValid()) {
            return ContinueStatus.CAN_CONTINUE;
        }
        return ContinueStatus.INVALID_NUMBER;
    }

    /* JADX DEBUG: Type inference failed for r2v3. Raw type applied. Possible types: io.reactivex.rxjava3.core.Single<R>, java.lang.Object, io.reactivex.rxjava3.core.Single<org.thoughtcrime.securesms.registration.VerifyAccountResponseProcessor> */
    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    public Single<VerifyAccountResponseProcessor> verifyCodeWithoutRegistrationLock(String str) {
        Intrinsics.checkNotNullParameter(str, "code");
        Single flatMap = super.verifyCodeWithoutRegistrationLock(str).doOnSubscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda4
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChangeNumberViewModel.m629verifyCodeWithoutRegistrationLock$lambda0((Disposable) obj);
            }
        }).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda5
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.this.attemptToUnlockChangeNumber((VerifyAccountResponseProcessor) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "super.verifyCodeWithoutR…emptToUnlockChangeNumber)");
        return flatMap;
    }

    /* renamed from: verifyCodeWithoutRegistrationLock$lambda-0 */
    public static final void m629verifyCodeWithoutRegistrationLock$lambda0(Disposable disposable) {
        SignalStore.misc().lockChangeNumber();
    }

    /* JADX DEBUG: Type inference failed for r2v3. Raw type applied. Possible types: io.reactivex.rxjava3.core.Single<R>, java.lang.Object, io.reactivex.rxjava3.core.Single<org.thoughtcrime.securesms.registration.VerifyCodeWithRegistrationLockResponseProcessor> */
    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    public Single<VerifyCodeWithRegistrationLockResponseProcessor> verifyCodeAndRegisterAccountWithRegistrationLock(String str) {
        Intrinsics.checkNotNullParameter(str, "pin");
        Single flatMap = super.verifyCodeAndRegisterAccountWithRegistrationLock(str).doOnSubscribe(new Consumer() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda2
            @Override // io.reactivex.rxjava3.functions.Consumer
            public final void accept(Object obj) {
                ChangeNumberViewModel.m628verifyCodeAndRegisterAccountWithRegistrationLock$lambda1((Disposable) obj);
            }
        }).flatMap(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda3
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.this.attemptToUnlockChangeNumber((VerifyCodeWithRegistrationLockResponseProcessor) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(flatMap, "super.verifyCodeAndRegis…emptToUnlockChangeNumber)");
        return flatMap;
    }

    /* renamed from: verifyCodeAndRegisterAccountWithRegistrationLock$lambda-1 */
    public static final void m628verifyCodeAndRegisterAccountWithRegistrationLock$lambda1(Disposable disposable) {
        SignalStore.misc().lockChangeNumber();
    }

    public final <T extends VerifyProcessor> Single<T> attemptToUnlockChangeNumber(T t) {
        if (t.hasResult() || t.isServerSentError()) {
            SignalStore.misc().unlockChangeNumber();
            Single<T> just = Single.just(t);
            Intrinsics.checkNotNullExpressionValue(just, "{\n      SignalStore.misc…gle.just(processor)\n    }");
            return just;
        }
        Single<T> onErrorReturn = this.changeNumberRepository.whoAmI().map(new Function(t) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda8
            public final /* synthetic */ VerifyProcessor f$1;

            {
                this.f$1 = r2;
            }

            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.m622attemptToUnlockChangeNumber$lambda2(ChangeNumberViewModel.this, this.f$1, (WhoAmIResponse) obj);
            }
        }).onErrorReturn(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda9
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.m623attemptToUnlockChangeNumber$lambda3(VerifyProcessor.this, (Throwable) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(onErrorReturn, "{\n      changeNumberRepo…eturn { processor }\n    }");
        return onErrorReturn;
    }

    /* renamed from: attemptToUnlockChangeNumber$lambda-2 */
    public static final VerifyProcessor m622attemptToUnlockChangeNumber$lambda2(ChangeNumberViewModel changeNumberViewModel, VerifyProcessor verifyProcessor, WhoAmIResponse whoAmIResponse) {
        Intrinsics.checkNotNullParameter(changeNumberViewModel, "this$0");
        Intrinsics.checkNotNullParameter(verifyProcessor, "$processor");
        if (Objects.equals(whoAmIResponse.getNumber(), changeNumberViewModel.localNumber)) {
            Log.i(ChangeNumberViewModelKt.TAG, "Local and remote numbers match, we can unlock.");
            SignalStore.misc().unlockChangeNumber();
        }
        return verifyProcessor;
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<ServiceResponse<VerifyAccountResponse>> verifyAccountWithoutRegistrationLock() {
        ChangeNumberRepository changeNumberRepository = this.changeNumberRepository;
        String textCodeEntered = getTextCodeEntered();
        Intrinsics.checkNotNullExpressionValue(textCodeEntered, "textCodeEntered");
        String e164Number = getNumber().getE164Number();
        Intrinsics.checkNotNullExpressionValue(e164Number, "number.e164Number");
        return changeNumberRepository.changeNumber(textCodeEntered, e164Number);
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<ServiceResponse<VerifyAccountRepository.VerifyAccountWithRegistrationLockResponse>> verifyAccountWithRegistrationLock(String str, TokenData tokenData) {
        Intrinsics.checkNotNullParameter(str, "pin");
        Intrinsics.checkNotNullParameter(tokenData, "kbsTokenData");
        ChangeNumberRepository changeNumberRepository = this.changeNumberRepository;
        String textCodeEntered = getTextCodeEntered();
        Intrinsics.checkNotNullExpressionValue(textCodeEntered, "textCodeEntered");
        String e164Number = getNumber().getE164Number();
        Intrinsics.checkNotNullExpressionValue(e164Number, "number.e164Number");
        return changeNumberRepository.changeNumber(textCodeEntered, e164Number, str, tokenData);
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<VerifyAccountResponseProcessor> onVerifySuccess(VerifyAccountResponseProcessor verifyAccountResponseProcessor) {
        Intrinsics.checkNotNullParameter(verifyAccountResponseProcessor, "processor");
        ChangeNumberRepository changeNumberRepository = this.changeNumberRepository;
        String e164Number = getNumber().getE164Number();
        Intrinsics.checkNotNullExpressionValue(e164Number, "number.e164Number");
        PNI parseOrThrow = PNI.parseOrThrow(verifyAccountResponseProcessor.getResult().getPni());
        Intrinsics.checkNotNullExpressionValue(parseOrThrow, "parseOrThrow(processor.result.pni)");
        Single<VerifyAccountResponseProcessor> onErrorReturn = changeNumberRepository.changeLocalNumber(e164Number, parseOrThrow).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda6
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.m624onVerifySuccess$lambda4(VerifyAccountResponseProcessor.this, (Unit) obj);
            }
        }).onErrorReturn(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda7
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.m625onVerifySuccess$lambda5((Throwable) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(onErrorReturn, "changeNumberRepository.c…rUnknownError(t))\n      }");
        return onErrorReturn;
    }

    /* renamed from: onVerifySuccess$lambda-5 */
    public static final VerifyAccountResponseProcessor m625onVerifySuccess$lambda5(Throwable th) {
        Log.w(ChangeNumberViewModelKt.TAG, "Error attempting to change local number", th);
        ServiceResponse forUnknownError = ServiceResponse.forUnknownError(th);
        Intrinsics.checkNotNullExpressionValue(forUnknownError, "forUnknownError(t)");
        return new VerifyAccountResponseWithoutKbs(forUnknownError);
    }

    @Override // org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel
    protected Single<VerifyCodeWithRegistrationLockResponseProcessor> onVerifySuccessWithRegistrationLock(VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor, String str) {
        Intrinsics.checkNotNullParameter(verifyCodeWithRegistrationLockResponseProcessor, "processor");
        Intrinsics.checkNotNullParameter(str, "pin");
        ChangeNumberRepository changeNumberRepository = this.changeNumberRepository;
        String e164Number = getNumber().getE164Number();
        Intrinsics.checkNotNullExpressionValue(e164Number, "number.e164Number");
        PNI parseOrThrow = PNI.parseOrThrow(verifyCodeWithRegistrationLockResponseProcessor.getResult().getVerifyAccountResponse().getPni());
        Intrinsics.checkNotNullExpressionValue(parseOrThrow, "parseOrThrow(processor.r…erifyAccountResponse.pni)");
        Single<VerifyCodeWithRegistrationLockResponseProcessor> onErrorReturn = changeNumberRepository.changeLocalNumber(e164Number, parseOrThrow).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.m626onVerifySuccessWithRegistrationLock$lambda6(VerifyCodeWithRegistrationLockResponseProcessor.this, (Unit) obj);
            }
        }).onErrorReturn(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return ChangeNumberViewModel.m627onVerifySuccessWithRegistrationLock$lambda7(VerifyCodeWithRegistrationLockResponseProcessor.this, (Throwable) obj);
            }
        });
        Intrinsics.checkNotNullExpressionValue(onErrorReturn, "changeNumberRepository.c… processor.token)\n      }");
        return onErrorReturn;
    }

    /* renamed from: onVerifySuccessWithRegistrationLock$lambda-7 */
    public static final VerifyCodeWithRegistrationLockResponseProcessor m627onVerifySuccessWithRegistrationLock$lambda7(VerifyCodeWithRegistrationLockResponseProcessor verifyCodeWithRegistrationLockResponseProcessor, Throwable th) {
        Intrinsics.checkNotNullParameter(verifyCodeWithRegistrationLockResponseProcessor, "$processor");
        Log.w(ChangeNumberViewModelKt.TAG, "Error attempting to change local number", th);
        ServiceResponse forUnknownError = ServiceResponse.forUnknownError(th);
        Intrinsics.checkNotNullExpressionValue(forUnknownError, "forUnknownError(t)");
        return new VerifyCodeWithRegistrationLockResponseProcessor(forUnknownError, verifyCodeWithRegistrationLockResponseProcessor.getToken());
    }

    /* compiled from: ChangeNumberViewModel.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J5\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\f\u0010\n\u001a\b\u0012\u0004\u0012\u0002H\u00060\u000b2\u0006\u0010\f\u001a\u00020\rH\u0014¢\u0006\u0002\u0010\u000e¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel$Factory;", "Landroidx/lifecycle/AbstractSavedStateViewModelFactory;", "owner", "Landroidx/savedstate/SavedStateRegistryOwner;", "(Landroidx/savedstate/SavedStateRegistryOwner;)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "key", "", "modelClass", "Ljava/lang/Class;", "handle", "Landroidx/lifecycle/SavedStateHandle;", "(Ljava/lang/String;Ljava/lang/Class;Landroidx/lifecycle/SavedStateHandle;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory extends AbstractSavedStateViewModelFactory {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Factory(SavedStateRegistryOwner savedStateRegistryOwner) {
            super(savedStateRegistryOwner, null);
            Intrinsics.checkNotNullParameter(savedStateRegistryOwner, "owner");
        }

        @Override // androidx.lifecycle.AbstractSavedStateViewModelFactory
        protected <T extends ViewModel> T create(String str, Class<T> cls, SavedStateHandle savedStateHandle) {
            Intrinsics.checkNotNullParameter(str, "key");
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            Intrinsics.checkNotNullParameter(savedStateHandle, "handle");
            Application application = ApplicationDependencies.getApplication();
            Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
            String e164 = SignalStore.account().getE164();
            Intrinsics.checkNotNull(e164);
            String servicePassword = SignalStore.account().getServicePassword();
            Intrinsics.checkNotNull(servicePassword);
            T cast = cls.cast(new ChangeNumberViewModel(e164, new ChangeNumberRepository(application), savedStateHandle, servicePassword, new VerifyAccountRepository(application), new KbsRepository()));
            if (cast != null) {
                return cast;
            }
            throw new IllegalArgumentException("Required value was null.".toString());
        }
    }
}
