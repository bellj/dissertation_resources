package org.thoughtcrime.securesms.components.settings.conversation;

import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda4 implements Store.Action {
    @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
    public final Object apply(Object obj, Object obj2) {
        return ConversationSettingsViewModel.GroupSettingsViewModel.m1133_init_$lambda13((String) obj, (ConversationSettingsState) obj2);
    }
}
