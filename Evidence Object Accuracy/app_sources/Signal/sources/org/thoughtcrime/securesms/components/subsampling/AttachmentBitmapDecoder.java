package org.thoughtcrime.securesms.components.subsampling;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import com.davemorrissey.labs.subscaleview.decoder.ImageDecoder;
import com.davemorrissey.labs.subscaleview.decoder.SkiaImageDecoder;
import java.io.InputStream;
import org.thoughtcrime.securesms.mms.PartAuthority;

/* loaded from: classes4.dex */
public class AttachmentBitmapDecoder implements ImageDecoder {
    @Override // com.davemorrissey.labs.subscaleview.decoder.ImageDecoder
    public Bitmap decode(Context context, Uri uri) throws Exception {
        if (!PartAuthority.isLocalUri(uri)) {
            return new SkiaImageDecoder().decode(context, uri);
        }
        InputStream attachmentStream = PartAuthority.getAttachmentStream(context, uri);
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            Bitmap decodeStream = BitmapFactory.decodeStream(attachmentStream, null, options);
            if (decodeStream != null) {
                return decodeStream;
            }
            throw new RuntimeException("Skia image region decoder returned null bitmap - image format may not be supported");
        } finally {
            if (attachmentStream != null) {
                attachmentStream.close();
            }
        }
    }
}
