package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.core.widget.TextViewCompat;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.ServiceUtil;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public final class ContactFilterView extends FrameLayout {
    private final ImageView clearToggle;
    private final ImageView dialpadToggle;
    private final ImageView keyboardToggle;
    private OnFilterChangedListener listener;
    private final EditText searchText;
    private final AnimatingToggle toggle;
    private final LinearLayout toggleContainer;

    /* loaded from: classes4.dex */
    public interface OnFilterChangedListener {
        void onFilterChanged(String str);
    }

    public ContactFilterView(Context context) {
        this(context, null);
    }

    public ContactFilterView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, R.attr.toolbarStyle);
    }

    public ContactFilterView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.contact_filter_view, this);
        EditText editText = (EditText) findViewById(R.id.search_view);
        this.searchText = editText;
        this.toggle = (AnimatingToggle) findViewById(R.id.button_toggle);
        ImageView imageView = (ImageView) findViewById(R.id.search_keyboard);
        this.keyboardToggle = imageView;
        ImageView imageView2 = (ImageView) findViewById(R.id.search_dialpad);
        this.dialpadToggle = imageView2;
        ImageView imageView3 = (ImageView) findViewById(R.id.search_clear);
        this.clearToggle = imageView3;
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.toggle_container);
        this.toggleContainer = linearLayout;
        imageView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ContactFilterView.1
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ContactFilterView.this.searchText.setInputType(97);
                ServiceUtil.getInputMethodManager(ContactFilterView.this.getContext()).showSoftInput(ContactFilterView.this.searchText, 0);
                ContactFilterView contactFilterView = ContactFilterView.this;
                contactFilterView.displayTogglingView(contactFilterView.dialpadToggle);
            }
        });
        imageView2.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ContactFilterView.2
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ContactFilterView.this.searchText.setInputType(3);
                ServiceUtil.getInputMethodManager(ContactFilterView.this.getContext()).showSoftInput(ContactFilterView.this.searchText, 0);
                ContactFilterView contactFilterView = ContactFilterView.this;
                contactFilterView.displayTogglingView(contactFilterView.keyboardToggle);
            }
        });
        imageView3.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.ContactFilterView.3
            @Override // android.view.View.OnClickListener
            public void onClick(View view) {
                ContactFilterView.this.searchText.setText("");
                if (SearchUtil.isTextInput(ContactFilterView.this.searchText)) {
                    ContactFilterView contactFilterView = ContactFilterView.this;
                    contactFilterView.displayTogglingView(contactFilterView.dialpadToggle);
                    return;
                }
                ContactFilterView contactFilterView2 = ContactFilterView.this;
                contactFilterView2.displayTogglingView(contactFilterView2.keyboardToggle);
            }
        });
        editText.addTextChangedListener(new TextWatcher() { // from class: org.thoughtcrime.securesms.components.ContactFilterView.4
            @Override // android.text.TextWatcher
            public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            @Override // android.text.TextWatcher
            public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            }

            @Override // android.text.TextWatcher
            public void afterTextChanged(Editable editable) {
                if (!SearchUtil.isEmpty(ContactFilterView.this.searchText)) {
                    ContactFilterView contactFilterView = ContactFilterView.this;
                    contactFilterView.displayTogglingView(contactFilterView.clearToggle);
                } else if (SearchUtil.isTextInput(ContactFilterView.this.searchText)) {
                    ContactFilterView contactFilterView2 = ContactFilterView.this;
                    contactFilterView2.displayTogglingView(contactFilterView2.dialpadToggle);
                } else if (SearchUtil.isPhoneInput(ContactFilterView.this.searchText)) {
                    ContactFilterView contactFilterView3 = ContactFilterView.this;
                    contactFilterView3.displayTogglingView(contactFilterView3.keyboardToggle);
                }
                ContactFilterView.this.notifyListener();
            }
        });
        expandTapArea(linearLayout, imageView2);
        applyAttributes(editText, context, attributeSet, i);
    }

    private void applyAttributes(EditText editText, Context context, AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.ContactFilterToolbar, i, 0);
        int resourceId = obtainStyledAttributes.getResourceId(2, -1);
        if (resourceId != -1) {
            TextViewCompat.setTextAppearance(editText, resourceId);
        }
        if (!obtainStyledAttributes.getBoolean(3, true)) {
            this.dialpadToggle.setVisibility(8);
        }
        if (obtainStyledAttributes.getBoolean(0, true)) {
            editText.requestFocus();
        }
        int resourceId2 = obtainStyledAttributes.getResourceId(1, -1);
        if (resourceId2 != -1) {
            findViewById(R.id.background_holder).setBackgroundResource(resourceId2);
        }
        obtainStyledAttributes.recycle();
    }

    public void focusAndShowKeyboard() {
        ViewUtil.focusAndShowKeyboard(this.searchText);
    }

    public void clear() {
        this.searchText.setText("");
        notifyListener();
    }

    public void setOnFilterChangedListener(OnFilterChangedListener onFilterChangedListener) {
        this.listener = onFilterChangedListener;
    }

    public void setOnSearchInputFocusChangedListener(View.OnFocusChangeListener onFocusChangeListener) {
        this.searchText.setOnFocusChangeListener(onFocusChangeListener);
    }

    public void setHint(int i) {
        this.searchText.setHint(i);
    }

    public void notifyListener() {
        OnFilterChangedListener onFilterChangedListener = this.listener;
        if (onFilterChangedListener != null) {
            onFilterChangedListener.onFilterChanged(this.searchText.getText().toString());
        }
    }

    public void displayTogglingView(View view) {
        this.toggle.display(view);
        expandTapArea(this.toggleContainer, view);
    }

    private void expandTapArea(final View view, final View view2) {
        final int dimensionPixelSize = getResources().getDimensionPixelSize(R.dimen.contact_selection_actions_tap_area);
        view.post(new Runnable() { // from class: org.thoughtcrime.securesms.components.ContactFilterView.5
            @Override // java.lang.Runnable
            public void run() {
                Rect rect = new Rect();
                view2.getHitRect(rect);
                int i = rect.top;
                int i2 = dimensionPixelSize;
                rect.top = i - i2;
                rect.left -= i2;
                rect.right += i2;
                rect.bottom += i2;
                view.setTouchDelegate(new TouchDelegate(rect, view2));
            }
        });
    }

    /* loaded from: classes4.dex */
    private static class SearchUtil {
        private SearchUtil() {
        }

        static boolean isTextInput(EditText editText) {
            return (editText.getInputType() & 15) == 1;
        }

        static boolean isPhoneInput(EditText editText) {
            return (editText.getInputType() & 15) == 3;
        }

        public static boolean isEmpty(EditText editText) {
            return editText.getText().length() <= 0;
        }
    }
}
