package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import java.util.concurrent.TimeUnit;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.PlayStoreUtil;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class RatingManager {
    private static final int DAYS_SINCE_INSTALL_THRESHOLD;
    private static final int DAYS_UNTIL_REPROMPT_THRESHOLD;
    private static final String TAG = Log.tag(RatingManager.class);

    public static void showRatingDialogIfNecessary(Context context) {
        TextSecurePreferences.isRatingEnabled(context);
    }

    private static void showRatingDialog(final Context context) {
        new AlertDialog.Builder(context).setTitle(R.string.RatingManager_rate_this_app).setMessage(R.string.RatingManager_if_you_enjoy_using_this_app_please_take_a_moment).setPositiveButton(R.string.RatingManager_rate_now, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.RatingManager.3
            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                TextSecurePreferences.setRatingEnabled(context, false);
                PlayStoreUtil.openPlayStoreOrOurApkDownloadPage(context);
            }
        }).setNegativeButton(R.string.RatingManager_no_thanks, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.RatingManager.2
            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                TextSecurePreferences.setRatingEnabled(context, false);
            }
        }).setNeutralButton(R.string.RatingManager_later, new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.RatingManager.1
            @Override // android.content.DialogInterface.OnClickListener
            public void onClick(DialogInterface dialogInterface, int i) {
                TextSecurePreferences.setRatingLaterTimestamp(context, System.currentTimeMillis() + TimeUnit.DAYS.toMillis(4));
            }
        }).show();
    }
}
