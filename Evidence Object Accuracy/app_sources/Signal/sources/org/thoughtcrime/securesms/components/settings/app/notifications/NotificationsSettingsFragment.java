package org.thoughtcrime.securesms.components.settings.app.notifications;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.media.Ringtone;
import android.net.Uri;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import j$.util.function.Function;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.PreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.RadioListPreference;
import org.thoughtcrime.securesms.components.settings.RadioListPreferenceViewHolder;
import org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment;
import org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsViewModel;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.notifications.NotificationChannels;
import org.thoughtcrime.securesms.util.RingtoneUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;

/* compiled from: NotificationsSettingsFragment.kt */
@Metadata(d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0002>?B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+H\u0016J\u0010\u0010,\u001a\u00020-2\u0006\u0010.\u001a\u00020/H\u0002J\u0010\u00100\u001a\u00020\u00052\u0006\u00101\u001a\u000202H\u0002J\b\u00103\u001a\u00020)H\u0002J\b\u00104\u001a\u00020)H\u0002J\b\u00105\u001a\u00020)H\u0003J\"\u00106\u001a\u00020)2\u0006\u00107\u001a\u0002082\u0006\u00109\u001a\u0002082\b\u0010:\u001a\u0004\u0018\u00010;H\u0016J\u0018\u0010<\u001a\u00020)2\u0006\u0010=\u001a\u00020;2\u0006\u00107\u001a\u000208H\u0002R?\u0010\u0003\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR?\u0010\u000b\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\f\u0010\bR?\u0010\u000e\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0010\u0010\n\u001a\u0004\b\u000f\u0010\bR?\u0010\u0011\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0013\u0010\n\u001a\u0004\b\u0012\u0010\bR?\u0010\u0014\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0016\u0010\n\u001a\u0004\b\u0015\u0010\bR?\u0010\u0017\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0019\u0010\n\u001a\u0004\b\u0018\u0010\bR?\u0010\u001a\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u001c\u0010\n\u001a\u0004\b\u001b\u0010\bR?\u0010\u001d\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u001f\u0010\n\u001a\u0004\b\u001e\u0010\bR?\u0010 \u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\"\u0010\n\u001a\u0004\b!\u0010\bR?\u0010#\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b%\u0010\n\u001a\u0004\b$\u0010\bR\u000e\u0010&\u001a\u00020'X.¢\u0006\u0002\n\u0000¨\u0006@"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "ledBlinkLabels", "", "", "kotlin.jvm.PlatformType", "getLedBlinkLabels", "()[Ljava/lang/String;", "ledBlinkLabels$delegate", "Lkotlin/Lazy;", "ledBlinkValues", "getLedBlinkValues", "ledBlinkValues$delegate", "ledColorLabels", "getLedColorLabels", "ledColorLabels$delegate", "ledColorValues", "getLedColorValues", "ledColorValues$delegate", "notificationPriorityLabels", "getNotificationPriorityLabels", "notificationPriorityLabels$delegate", "notificationPriorityValues", "getNotificationPriorityValues", "notificationPriorityValues$delegate", "notificationPrivacyLabels", "getNotificationPrivacyLabels", "notificationPrivacyLabels$delegate", "notificationPrivacyValues", "getNotificationPrivacyValues", "notificationPrivacyValues$delegate", "repeatAlertsLabels", "getRepeatAlertsLabels", "repeatAlertsLabels$delegate", "repeatAlertsValues", "getRepeatAlertsValues", "repeatAlertsValues$delegate", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsState;", "getRingtoneSummary", "uri", "Landroid/net/Uri;", "launchCallRingtoneSelectionIntent", "launchMessageSoundSelectionIntent", "launchNotificationPriorityIntent", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "openRingtonePicker", "intent", "LedColorPreference", "LedColorPreferenceViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class NotificationsSettingsFragment extends DSLSettingsFragment {
    private final Lazy ledBlinkLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$ledBlinkLabels$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_led_blink_pattern_entries);
        }
    });
    private final Lazy ledBlinkValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$ledBlinkValues$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_led_blink_pattern_values);
        }
    });
    private final Lazy ledColorLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$ledColorLabels$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_led_color_entries);
        }
    });
    private final Lazy ledColorValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$ledColorValues$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_led_color_values);
        }
    });
    private final Lazy notificationPriorityLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$notificationPriorityLabels$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_notification_priority_entries);
        }
    });
    private final Lazy notificationPriorityValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$notificationPriorityValues$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_notification_priority_values);
        }
    });
    private final Lazy notificationPrivacyLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$notificationPrivacyLabels$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_notification_privacy_entries);
        }
    });
    private final Lazy notificationPrivacyValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$notificationPrivacyValues$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_notification_privacy_values);
        }
    });
    private final Lazy repeatAlertsLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$repeatAlertsLabels$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_repeat_alerts_entries);
        }
    });
    private final Lazy repeatAlertsValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$repeatAlertsValues$2
        final /* synthetic */ NotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_repeat_alerts_values);
        }
    });
    private NotificationsSettingsViewModel viewModel;

    public NotificationsSettingsFragment() {
        super(R.string.preferences__notifications, 0, 0, null, 14, null);
    }

    public final String[] getRepeatAlertsValues() {
        return (String[]) this.repeatAlertsValues$delegate.getValue();
    }

    public final String[] getRepeatAlertsLabels() {
        return (String[]) this.repeatAlertsLabels$delegate.getValue();
    }

    public final String[] getNotificationPrivacyValues() {
        return (String[]) this.notificationPrivacyValues$delegate.getValue();
    }

    public final String[] getNotificationPrivacyLabels() {
        return (String[]) this.notificationPrivacyLabels$delegate.getValue();
    }

    public final String[] getNotificationPriorityValues() {
        return (String[]) this.notificationPriorityValues$delegate.getValue();
    }

    public final String[] getNotificationPriorityLabels() {
        return (String[]) this.notificationPriorityLabels$delegate.getValue();
    }

    public final String[] getLedColorValues() {
        return (String[]) this.ledColorValues$delegate.getValue();
    }

    public final String[] getLedColorLabels() {
        return (String[]) this.ledColorLabels$delegate.getValue();
    }

    public final String[] getLedBlinkValues() {
        return (String[]) this.ledBlinkValues$delegate.getValue();
    }

    public final String[] getLedBlinkLabels() {
        return (String[]) this.ledBlinkLabels$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        NotificationsSettingsViewModel notificationsSettingsViewModel = null;
        if (i == 1 && i2 == -1 && intent != null) {
            Uri uri = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
            NotificationsSettingsViewModel notificationsSettingsViewModel2 = this.viewModel;
            if (notificationsSettingsViewModel2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            } else {
                notificationsSettingsViewModel = notificationsSettingsViewModel2;
            }
            notificationsSettingsViewModel.setMessageNotificationsSound(uri);
        } else if (i == 2 && i2 == -1 && intent != null) {
            Uri uri2 = (Uri) intent.getParcelableExtra("android.intent.extra.ringtone.PICKED_URI");
            NotificationsSettingsViewModel notificationsSettingsViewModel3 = this.viewModel;
            if (notificationsSettingsViewModel3 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            } else {
                notificationsSettingsViewModel = notificationsSettingsViewModel3;
            }
            notificationsSettingsViewModel.setCallRingtone(uri2);
        }
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        dSLSettingsAdapter.registerFactory(LedColorPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new NotificationsSettingsFragment.LedColorPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        Intrinsics.checkNotNullExpressionValue(defaultSharedPreferences, "sharedPreferences");
        ViewModel viewModel = new ViewModelProvider(this, new NotificationsSettingsViewModel.Factory(defaultSharedPreferences)).get(NotificationsSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ngsViewModel::class.java]");
        NotificationsSettingsViewModel notificationsSettingsViewModel = (NotificationsSettingsViewModel) viewModel;
        this.viewModel = notificationsSettingsViewModel;
        if (notificationsSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            notificationsSettingsViewModel = null;
        }
        notificationsSettingsViewModel.m712getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$$ExternalSyntheticLambda1
            public final /* synthetic */ NotificationsSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                NotificationsSettingsFragment.m691$r8$lambda$3uDAj2C5KhVbfCXxQIzVva6f60(DSLSettingsAdapter.this, this.f$1, (NotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m692bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, NotificationsSettingsFragment notificationsSettingsFragment, NotificationsSettingsState notificationsSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(notificationsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(notificationsSettingsState, "it");
        dSLSettingsAdapter.submitList(notificationsSettingsFragment.getConfiguration(notificationsSettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(NotificationsSettingsState notificationsSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(notificationsSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$getConfiguration$1
            final /* synthetic */ NotificationsSettingsState $state;
            final /* synthetic */ NotificationsSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration r30) {
                /*
                // Method dump skipped, instructions count: 968
                */
                throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.app.notifications.NotificationsSettingsFragment$getConfiguration$1.invoke(org.thoughtcrime.securesms.components.settings.DSLConfiguration):void");
            }
        });
    }

    public final String getRingtoneSummary(Uri uri) {
        if (TextUtils.isEmpty(uri.toString())) {
            String string = getString(R.string.preferences__silent);
            Intrinsics.checkNotNullExpressionValue(string, "{\n      getString(R.stri…references__silent)\n    }");
            return string;
        }
        Ringtone ringtone = RingtoneUtil.getRingtone(requireContext(), uri);
        if (ringtone != null) {
            try {
                String title = ringtone.getTitle(requireContext());
                if (title != null) {
                    return title;
                }
                String string2 = getString(R.string.NotificationsSettingsFragment__unknown_ringtone);
                Intrinsics.checkNotNullExpressionValue(string2, "getString(R.string.Notif…agment__unknown_ringtone)");
                return string2;
            } catch (SecurityException e) {
                Log.w(NotificationsSettingsFragmentKt.TAG, "Unable to get title for ringtone", e);
                String string3 = getString(R.string.NotificationsSettingsFragment__unknown_ringtone);
                Intrinsics.checkNotNullExpressionValue(string3, "getString(R.string.Notif…agment__unknown_ringtone)");
                return string3;
            }
        } else {
            String string4 = getString(R.string.preferences__default);
            Intrinsics.checkNotNullExpressionValue(string4, "{\n        getString(R.st…erences__default)\n      }");
            return string4;
        }
    }

    public final void launchMessageSoundSelectionIntent() {
        Uri messageNotificationSound = SignalStore.settings().getMessageNotificationSound();
        Intrinsics.checkNotNullExpressionValue(messageNotificationSound, "settings().messageNotificationSound");
        Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
        intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", true);
        intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", true);
        intent.putExtra("android.intent.extra.ringtone.TYPE", 2);
        intent.putExtra("android.intent.extra.ringtone.DEFAULT_URI", Settings.System.DEFAULT_NOTIFICATION_URI);
        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", messageNotificationSound);
        openRingtonePicker(intent, 1);
    }

    public final void launchNotificationPriorityIntent() {
        Intent intent = new Intent("android.settings.CHANNEL_NOTIFICATION_SETTINGS");
        intent.putExtra("android.provider.extra.CHANNEL_ID", NotificationChannels.getMessagesChannel(requireContext()));
        intent.putExtra("android.provider.extra.APP_PACKAGE", requireContext().getPackageName());
        startActivity(intent);
    }

    public final void launchCallRingtoneSelectionIntent() {
        Uri callRingtone = SignalStore.settings().getCallRingtone();
        Intrinsics.checkNotNullExpressionValue(callRingtone, "settings().callRingtone");
        Intent intent = new Intent("android.intent.action.RINGTONE_PICKER");
        intent.putExtra("android.intent.extra.ringtone.SHOW_DEFAULT", true);
        intent.putExtra("android.intent.extra.ringtone.SHOW_SILENT", true);
        intent.putExtra("android.intent.extra.ringtone.TYPE", 1);
        intent.putExtra("android.intent.extra.ringtone.DEFAULT_URI", Settings.System.DEFAULT_RINGTONE_URI);
        intent.putExtra("android.intent.extra.ringtone.EXISTING_URI", callRingtone);
        openRingtonePicker(intent, 2);
    }

    private final void openRingtonePicker(Intent intent, int i) {
        try {
            startActivityForResult(intent, i);
        } catch (ActivityNotFoundException unused) {
            Toast.makeText(requireContext(), (int) R.string.NotificationSettingsFragment__failed_to_open_picker, 1).show();
        }
    }

    /* compiled from: NotificationsSettingsFragment.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u001b\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006¢\u0006\u0002\u0010\u0007J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0000H\u0016R\u0019\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003¢\u0006\n\n\u0002\u0010\n\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0005\u001a\u00020\u0006¢\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsFragment$LedColorPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "colorValues", "", "", "radioListPreference", "Lorg/thoughtcrime/securesms/components/settings/RadioListPreference;", "([Ljava/lang/String;Lorg/thoughtcrime/securesms/components/settings/RadioListPreference;)V", "getColorValues", "()[Ljava/lang/String;", "[Ljava/lang/String;", "getRadioListPreference", "()Lorg/thoughtcrime/securesms/components/settings/RadioListPreference;", "areContentsTheSame", "", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LedColorPreference extends PreferenceModel<LedColorPreference> {
        private final String[] colorValues;
        private final RadioListPreference radioListPreference;

        public final String[] getColorValues() {
            return this.colorValues;
        }

        public final RadioListPreference getRadioListPreference() {
            return this.radioListPreference;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public LedColorPreference(String[] strArr, RadioListPreference radioListPreference) {
            super(radioListPreference.getTitle(), radioListPreference.getSummary(), radioListPreference.getIcon(), null, false, 24, null);
            Intrinsics.checkNotNullParameter(strArr, "colorValues");
            Intrinsics.checkNotNullParameter(radioListPreference, "radioListPreference");
            this.colorValues = strArr;
            this.radioListPreference = radioListPreference;
        }

        public boolean areContentsTheSame(LedColorPreference ledColorPreference) {
            Intrinsics.checkNotNullParameter(ledColorPreference, "newItem");
            return super.areContentsTheSame(ledColorPreference) && this.radioListPreference.areContentsTheSame(ledColorPreference.radioListPreference);
        }
    }

    /* compiled from: NotificationsSettingsFragment.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0000\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\u0002H\u0016J\f\u0010\r\u001a\u00020\u000e*\u00020\u000fH\u0002R\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsFragment$LedColorPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/NotificationsSettingsFragment$LedColorPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "radioListPreferenceViewHolder", "Lorg/thoughtcrime/securesms/components/settings/RadioListPreferenceViewHolder;", "getRadioListPreferenceViewHolder", "()Lorg/thoughtcrime/securesms/components/settings/RadioListPreferenceViewHolder;", "bind", "", "model", "toColorFilter", "Landroid/graphics/ColorFilter;", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LedColorPreferenceViewHolder extends PreferenceViewHolder<LedColorPreference> {
        private final RadioListPreferenceViewHolder radioListPreferenceViewHolder;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public LedColorPreferenceViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            this.radioListPreferenceViewHolder = new RadioListPreferenceViewHolder(view);
        }

        public final RadioListPreferenceViewHolder getRadioListPreferenceViewHolder() {
            return this.radioListPreferenceViewHolder;
        }

        public void bind(LedColorPreference ledColorPreference) {
            Intrinsics.checkNotNullParameter(ledColorPreference, "model");
            super.bind((LedColorPreferenceViewHolder) ledColorPreference);
            this.radioListPreferenceViewHolder.bind(ledColorPreference.getRadioListPreference());
            getSummaryView().setVisibility(8);
            Drawable drawable = ContextCompat.getDrawable(this.context, R.drawable.circle_tintable);
            if (drawable != null) {
                Intrinsics.checkNotNullExpressionValue(drawable, "requireNotNull(ContextCo…rawable.circle_tintable))");
                drawable.setBounds(0, 0, ViewUtil.dpToPx(20), ViewUtil.dpToPx(20));
                drawable.setColorFilter(toColorFilter(ledColorPreference.getColorValues()[ledColorPreference.getRadioListPreference().getSelected()]));
                if (ViewUtil.isLtr(this.itemView)) {
                    getTitleView().setCompoundDrawables(null, null, drawable, null);
                } else {
                    getTitleView().setCompoundDrawables(drawable, null, null, null);
                }
            } else {
                throw new IllegalArgumentException("Required value was null.".toString());
            }
        }

        /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
        private final ColorFilter toColorFilter(String str) {
            int i;
            switch (str.hashCode()) {
                case -734239628:
                    if (str.equals("yellow")) {
                        i = ContextCompat.getColor(this.context, R.color.yellow_500);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                case 112785:
                    if (str.equals("red")) {
                        i = ContextCompat.getColor(this.context, R.color.red_500);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                case 3027034:
                    if (str.equals("blue")) {
                        i = ContextCompat.getColor(this.context, R.color.blue_500);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                case 3068707:
                    if (str.equals("cyan")) {
                        i = ContextCompat.getColor(this.context, R.color.cyan_500);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                case 98619139:
                    if (str.equals("green")) {
                        i = ContextCompat.getColor(this.context, R.color.green_500);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                case 113101865:
                    if (str.equals("white")) {
                        i = ContextCompat.getColor(this.context, R.color.white);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                case 828922025:
                    if (str.equals("magenta")) {
                        i = ContextCompat.getColor(this.context, R.color.pink_500);
                        break;
                    }
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
                default:
                    i = ContextCompat.getColor(this.context, R.color.transparent);
                    break;
            }
            return new PorterDuffColorFilter(i, PorterDuff.Mode.SRC_IN);
        }
    }
}
