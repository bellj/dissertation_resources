package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

/* loaded from: classes4.dex */
public class PercentFrameLayout extends ViewGroup {
    private int heightPercent = 100;
    private boolean hidden = false;
    private boolean square = false;
    private int widthPercent = 100;
    private int xPercent = 0;
    private int yPercent = 0;

    @Override // android.view.ViewGroup
    public boolean shouldDelayChildPressedState() {
        return false;
    }

    public PercentFrameLayout(Context context) {
        super(context);
    }

    public PercentFrameLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public PercentFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public void setSquare(boolean z) {
        this.square = z;
    }

    public void setHidden(boolean z) {
        this.hidden = z;
    }

    public boolean isHidden() {
        return this.hidden;
    }

    public void setPosition(int i, int i2, int i3, int i4) {
        this.xPercent = i;
        this.yPercent = i2;
        this.widthPercent = i3;
        this.heightPercent = i4;
    }

    @Override // android.view.View
    protected void onMeasure(int i, int i2) {
        int defaultSize = ViewGroup.getDefaultSize(Integer.MAX_VALUE, i);
        int defaultSize2 = ViewGroup.getDefaultSize(Integer.MAX_VALUE, i2);
        setMeasuredDimension(View.MeasureSpec.makeMeasureSpec(defaultSize, 1073741824), View.MeasureSpec.makeMeasureSpec(defaultSize2, 1073741824));
        int i3 = (this.widthPercent * defaultSize) / 100;
        int i4 = (this.heightPercent * defaultSize2) / 100;
        if (this.square) {
            if (defaultSize > defaultSize2) {
                i3 = i4;
            } else {
                i4 = i3;
            }
        }
        if (this.hidden) {
            i3 = 1;
            i4 = 1;
        }
        int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i3, Integer.MIN_VALUE);
        int makeMeasureSpec2 = View.MeasureSpec.makeMeasureSpec(i4, Integer.MIN_VALUE);
        for (int i5 = 0; i5 < getChildCount(); i5++) {
            View childAt = getChildAt(i5);
            if (childAt.getVisibility() != 8) {
                childAt.measure(makeMeasureSpec, makeMeasureSpec2);
            }
        }
    }

    @Override // android.view.ViewGroup, android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5 = i3 - i;
        int i6 = i4 - i2;
        int i7 = (this.widthPercent * i5) / 100;
        int i8 = (this.heightPercent * i6) / 100;
        int i9 = i + ((i5 * this.xPercent) / 100);
        int i10 = i2 + ((i6 * this.yPercent) / 100);
        for (int i11 = 0; i11 < getChildCount(); i11++) {
            View childAt = getChildAt(i11);
            if (childAt.getVisibility() != 8) {
                int measuredWidth = childAt.getMeasuredWidth();
                int measuredHeight = childAt.getMeasuredHeight();
                int i12 = ((i7 - measuredWidth) / 2) + i9;
                int i13 = ((i8 - measuredHeight) / 2) + i10;
                if (this.hidden) {
                    i12 = 0;
                    i13 = 0;
                }
                childAt.layout(i12, i13, measuredWidth + i12, measuredHeight + i13);
            }
        }
    }
}
