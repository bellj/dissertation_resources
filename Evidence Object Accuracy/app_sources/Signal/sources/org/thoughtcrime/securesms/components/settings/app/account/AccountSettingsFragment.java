package org.thoughtcrime.securesms.components.settings.app.account;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.DialogCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.snackbar.Snackbar;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.contactshare.SimpleTextWatcher;
import org.thoughtcrime.securesms.database.DraftDatabase;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.lock.PinHashing;
import org.thoughtcrime.securesms.lock.v2.CreateKbsPinActivity;
import org.thoughtcrime.securesms.lock.v2.PinKeyboardType;
import org.thoughtcrime.securesms.pin.RegistrationLockV2Dialog;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: AccountSettingsFragment.kt */
@Metadata(d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u0010\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\"\u0010\u0011\u001a\u00020\n2\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00132\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0016J\b\u0010\u0017\u001a\u00020\nH\u0016J\u0010\u0010\u0018\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002J\u0010\u0010\u001b\u001a\u00020\n2\u0006\u0010\u0019\u001a\u00020\u001aH\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsViewModel;", "setViewModel", "(Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsViewModel;)V", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/account/AccountSettingsState;", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onResume", "setPinRemindersEnabled", NotificationProfileDatabase.NotificationProfileScheduleTable.ENABLED, "", "setRegistrationLockEnabled", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AccountSettingsFragment extends DSLSettingsFragment {
    public AccountSettingsViewModel viewModel;

    /* compiled from: AccountSettingsFragment.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[PinKeyboardType.values().length];
            iArr[PinKeyboardType.NUMERIC.ordinal()] = 1;
            iArr[PinKeyboardType.ALPHA_NUMERIC.ordinal()] = 2;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    public AccountSettingsFragment() {
        super(R.string.AccountSettingsFragment__account, 0, 0, null, 14, null);
    }

    public final AccountSettingsViewModel getViewModel() {
        AccountSettingsViewModel accountSettingsViewModel = this.viewModel;
        if (accountSettingsViewModel != null) {
            return accountSettingsViewModel;
        }
        Intrinsics.throwUninitializedPropertyAccessException("viewModel");
        return null;
    }

    public final void setViewModel(AccountSettingsViewModel accountSettingsViewModel) {
        Intrinsics.checkNotNullParameter(accountSettingsViewModel, "<set-?>");
        this.viewModel = accountSettingsViewModel;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityResult(int i, int i2, Intent intent) {
        if (i == 27698 && i2 == -1) {
            Snackbar.make(requireView(), (int) R.string.ConfirmKbsPinFragment__pin_created, 0).show();
        }
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refreshState();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ViewModel viewModel = new ViewModelProvider(this).get(AccountSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this)[…ngsViewModel::class.java]");
        setViewModel((AccountSettingsViewModel) viewModel);
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda6
            public final /* synthetic */ AccountSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AccountSettingsFragment.$r8$lambda$ySwEEwGfsFJbWaa5wzObqsvuutk(DSLSettingsAdapter.this, this.f$1, (AccountSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m562bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, AccountSettingsFragment accountSettingsFragment, AccountSettingsState accountSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(accountSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(accountSettingsState, "state");
        dSLSettingsAdapter.submitList(accountSettingsFragment.getConfiguration(accountSettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(AccountSettingsState accountSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(accountSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1
            final /* synthetic */ AccountSettingsState $state;
            final /* synthetic */ AccountSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.sectionHeaderPref(R.string.preferences_app_protection__signal_pin);
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(this.$state.getHasPin() ? R.string.preferences_app_protection__change_your_pin : R.string.preferences_app_protection__create_a_pin, new DSLSettingsText.Modifier[0]);
                final AccountSettingsState accountSettingsState2 = this.$state;
                final AccountSettingsFragment accountSettingsFragment = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        if (accountSettingsState2.getHasPin()) {
                            AccountSettingsFragment accountSettingsFragment2 = accountSettingsFragment;
                            accountSettingsFragment2.startActivityForResult(CreateKbsPinActivity.getIntentForPinChangeFromSettings(accountSettingsFragment2.requireContext()), 27698);
                            return;
                        }
                        AccountSettingsFragment accountSettingsFragment3 = accountSettingsFragment;
                        accountSettingsFragment3.startActivityForResult(CreateKbsPinActivity.getIntentForPinCreate(accountSettingsFragment3.requireContext()), 27698);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from2 = companion.from(R.string.preferences_app_protection__pin_reminders, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from3 = companion.from(R.string.AccountSettingsFragment__youll_be_asked_less_frequently, new DSLSettingsText.Modifier[0]);
                boolean z = this.$state.getHasPin() && this.$state.getPinRemindersEnabled();
                boolean hasPin = this.$state.getHasPin();
                final AccountSettingsFragment accountSettingsFragment2 = this.this$0;
                final AccountSettingsState accountSettingsState3 = this.$state;
                dSLConfiguration.switchPref(from2, (r16 & 2) != 0 ? null : from3, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : hasPin, z, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        AccountSettingsFragment.access$setPinRemindersEnabled(accountSettingsFragment2, !accountSettingsState3.getPinRemindersEnabled());
                    }
                });
                DSLSettingsText from4 = companion.from(R.string.preferences_app_protection__registration_lock, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from5 = companion.from(R.string.AccountSettingsFragment__require_your_signal_pin, new DSLSettingsText.Modifier[0]);
                boolean registrationLockEnabled = this.$state.getRegistrationLockEnabled();
                boolean hasPin2 = this.$state.getHasPin();
                final AccountSettingsFragment accountSettingsFragment3 = this.this$0;
                final AccountSettingsState accountSettingsState4 = this.$state;
                dSLConfiguration.switchPref(from4, (r16 & 2) != 0 ? null : from5, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0 ? true : hasPin2, registrationLockEnabled, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        AccountSettingsFragment.access$setRegistrationLockEnabled(accountSettingsFragment3, !accountSettingsState4.getRegistrationLockEnabled());
                    }
                });
                DSLSettingsText from6 = companion.from(R.string.preferences__advanced_pin_settings, new DSLSettingsText.Modifier[0]);
                final AccountSettingsFragment accountSettingsFragment4 = this.this$0;
                dSLConfiguration.clickPref(from6, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.4
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(accountSettingsFragment4.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_accountSettingsFragment_to_advancedPinSettingsActivity);
                    }
                }, (r18 & 64) != 0 ? null : null);
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.AccountSettingsFragment__account);
                if (Recipient.self().getChangeNumberCapability() == Recipient.Capability.SUPPORTED && SignalStore.account().isRegistered()) {
                    DSLSettingsText from7 = companion.from(R.string.AccountSettingsFragment__change_phone_number, new DSLSettingsText.Modifier[0]);
                    final AccountSettingsFragment accountSettingsFragment5 = this.this$0;
                    dSLConfiguration.clickPref(from7, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.5
                        @Override // kotlin.jvm.functions.Function0
                        public final void invoke() {
                            NavController findNavController = Navigation.findNavController(accountSettingsFragment5.requireView());
                            Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                            SafeNavigation.safeNavigate(findNavController, (int) R.id.action_accountSettingsFragment_to_changePhoneNumberFragment);
                        }
                    }, (r18 & 64) != 0 ? null : null);
                }
                DSLSettingsText from8 = companion.from(R.string.preferences_chats__transfer_account, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from9 = companion.from(R.string.preferences_chats__transfer_account_to_a_new_android_device, new DSLSettingsText.Modifier[0]);
                final AccountSettingsFragment accountSettingsFragment6 = this.this$0;
                dSLConfiguration.clickPref(from8, (r18 & 2) != 0 ? null : from9, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.6
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(accountSettingsFragment6.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_accountSettingsFragment_to_oldDeviceTransferActivity);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from10 = companion.from(R.string.preferences__delete_account, ContextCompat.getColor(this.this$0.requireContext(), R.color.signal_alert_primary));
                final AccountSettingsFragment accountSettingsFragment7 = this.this$0;
                dSLConfiguration.clickPref(from10, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$getConfiguration$1.7
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(accountSettingsFragment7.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_accountSettingsFragment_to_deleteAccountFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
            }
        });
    }

    public final void setRegistrationLockEnabled(boolean z) {
        if (z) {
            RegistrationLockV2Dialog.showEnableDialog(requireContext(), new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda4
                @Override // java.lang.Runnable
                public final void run() {
                    AccountSettingsFragment.$r8$lambda$HB6g0mink5YRq5raC_0dmMJWz1g(AccountSettingsFragment.this);
                }
            });
        } else {
            RegistrationLockV2Dialog.showDisableDialog(requireContext(), new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda5
                @Override // java.lang.Runnable
                public final void run() {
                    AccountSettingsFragment.m561$r8$lambda$hEgCfllCuGjnr3Saxq7V8nng6w(AccountSettingsFragment.this);
                }
            });
        }
    }

    /* renamed from: setRegistrationLockEnabled$lambda-1 */
    public static final void m567setRegistrationLockEnabled$lambda1(AccountSettingsFragment accountSettingsFragment) {
        Intrinsics.checkNotNullParameter(accountSettingsFragment, "this$0");
        accountSettingsFragment.getViewModel().refreshState();
    }

    /* renamed from: setRegistrationLockEnabled$lambda-2 */
    public static final void m568setRegistrationLockEnabled$lambda2(AccountSettingsFragment accountSettingsFragment) {
        Intrinsics.checkNotNullParameter(accountSettingsFragment, "this$0");
        accountSettingsFragment.getViewModel().refreshState();
    }

    public final void setPinRemindersEnabled(boolean z) {
        if (!z) {
            Context requireContext = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            Intrinsics.checkNotNullExpressionValue(displayMetrics, "resources.displayMetrics");
            AlertDialog create = new MaterialAlertDialogBuilder(requireContext).setView(R.layout.pin_disable_reminders_dialog).create();
            Intrinsics.checkNotNullExpressionValue(create, "MaterialAlertDialogBuild…dialog)\n        .create()");
            create.show();
            Window window = create.getWindow();
            Intrinsics.checkNotNull(window);
            double d = (double) displayMetrics.widthPixels;
            Double.isNaN(d);
            window.setLayout((int) (d * 0.8d), -2);
            EditText editText = (EditText) DialogCompat.requireViewById(create, R.id.reminder_disable_pin);
            TextView textView = (TextView) DialogCompat.requireViewById(create, R.id.reminder_disable_status);
            View requireViewById = DialogCompat.requireViewById(create, R.id.reminder_disable_cancel);
            Intrinsics.checkNotNullExpressionValue(requireViewById, "requireViewById(dialog, ….reminder_disable_cancel)");
            View requireViewById2 = DialogCompat.requireViewById(create, R.id.reminder_disable_turn_off);
            Intrinsics.checkNotNullExpressionValue(requireViewById2, "requireViewById(dialog, …eminder_disable_turn_off)");
            Button button = (Button) DialogCompat.requireViewById(create, R.id.reminder_change_keyboard);
            button.setOnClickListener(new View.OnClickListener(editText, button) { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda0
                public final /* synthetic */ EditText f$0;
                public final /* synthetic */ Button f$1;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AccountSettingsFragment.$r8$lambda$KskZMH9PlNbsPFg3DdIdYyrkjJY(this.f$0, this.f$1, view);
                }
            });
            editText.post(new Runnable(editText) { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda1
                public final /* synthetic */ EditText f$0;

                {
                    this.f$0 = r1;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    AccountSettingsFragment.$r8$lambda$r0Ytzxj1MTxX5KIXdmRx50qQgbg(this.f$0);
                }
            });
            ViewCompat.setAutofillHints(editText, "password");
            int i = WhenMappings.$EnumSwitchMapping$0[SignalStore.pinValues().getKeyboardType().ordinal()];
            if (i == 1) {
                editText.setInputType(18);
                button.setText(R.string.PinRestoreEntryFragment_enter_alphanumeric_pin);
            } else if (i == 2) {
                editText.setInputType(129);
                button.setText(R.string.PinRestoreEntryFragment_enter_numeric_pin);
            }
            editText.addTextChangedListener(new SimpleTextWatcher(requireViewById2) { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$setPinRemindersEnabled$3
                final /* synthetic */ View $turnOffButton;

                /* access modifiers changed from: package-private */
                {
                    this.$turnOffButton = r1;
                }

                @Override // org.thoughtcrime.securesms.contactshare.SimpleTextWatcher
                public void onTextChanged(String str) {
                    Intrinsics.checkNotNullParameter(str, DraftDatabase.Draft.TEXT);
                    this.$turnOffButton.setEnabled(str.length() >= 4);
                }
            });
            editText.setTypeface(Typeface.DEFAULT);
            requireViewById2.setOnClickListener(new View.OnClickListener(editText, this, create, textView) { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda2
                public final /* synthetic */ EditText f$0;
                public final /* synthetic */ AccountSettingsFragment f$1;
                public final /* synthetic */ AlertDialog f$2;
                public final /* synthetic */ TextView f$3;

                {
                    this.f$0 = r1;
                    this.f$1 = r2;
                    this.f$2 = r3;
                    this.f$3 = r4;
                }

                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AccountSettingsFragment.$r8$lambda$UEZ1MUeBt7kujJaVwl2VK4DRbE8(this.f$0, this.f$1, this.f$2, this.f$3, view);
                }
            });
            requireViewById.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.account.AccountSettingsFragment$$ExternalSyntheticLambda3
                @Override // android.view.View.OnClickListener
                public final void onClick(View view) {
                    AccountSettingsFragment.m560$r8$lambda$d2_zXNdrgs0ak3C1mYBxu3XNZs(AlertDialog.this, view);
                }
            });
            return;
        }
        SignalStore.pinValues().setPinRemindersEnabled(true);
        getViewModel().refreshState();
    }

    /* renamed from: setPinRemindersEnabled$lambda-3 */
    public static final void m563setPinRemindersEnabled$lambda3(EditText editText, Button button, View view) {
        Intrinsics.checkNotNullParameter(editText, "$pinEditText");
        Intrinsics.checkNotNullParameter(button, "$changeKeyboard");
        if ((editText.getInputType() & 2) == 0) {
            editText.setInputType(18);
            button.setText(R.string.PinRestoreEntryFragment_enter_alphanumeric_pin);
        } else {
            editText.setInputType(129);
            button.setText(R.string.PinRestoreEntryFragment_enter_numeric_pin);
        }
        editText.setTypeface(Typeface.DEFAULT);
    }

    /* renamed from: setPinRemindersEnabled$lambda-4 */
    public static final void m564setPinRemindersEnabled$lambda4(EditText editText) {
        Intrinsics.checkNotNullParameter(editText, "$pinEditText");
        ViewUtil.focusAndShowKeyboard(editText);
    }

    /* renamed from: setPinRemindersEnabled$lambda-5 */
    public static final void m565setPinRemindersEnabled$lambda5(EditText editText, AccountSettingsFragment accountSettingsFragment, AlertDialog alertDialog, TextView textView, View view) {
        Intrinsics.checkNotNullParameter(editText, "$pinEditText");
        Intrinsics.checkNotNullParameter(accountSettingsFragment, "this$0");
        Intrinsics.checkNotNullParameter(alertDialog, "$dialog");
        Intrinsics.checkNotNullParameter(textView, "$statusText");
        String obj = editText.getText().toString();
        String localPinHash = SignalStore.kbsValues().getLocalPinHash();
        Intrinsics.checkNotNull(localPinHash);
        if (PinHashing.verifyLocalPinHash(localPinHash, obj)) {
            SignalStore.pinValues().setPinRemindersEnabled(false);
            accountSettingsFragment.getViewModel().refreshState();
            alertDialog.dismiss();
            return;
        }
        textView.setText(R.string.preferences_app_protection__incorrect_pin_try_again);
    }

    /* renamed from: setPinRemindersEnabled$lambda-6 */
    public static final void m566setPinRemindersEnabled$lambda6(AlertDialog alertDialog, View view) {
        Intrinsics.checkNotNullParameter(alertDialog, "$dialog");
        alertDialog.dismiss();
    }
}
