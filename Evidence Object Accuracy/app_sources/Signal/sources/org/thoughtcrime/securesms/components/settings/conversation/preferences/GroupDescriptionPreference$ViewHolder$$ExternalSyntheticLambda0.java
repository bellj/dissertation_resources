package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.GroupDescriptionPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class GroupDescriptionPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ GroupDescriptionPreference.Model f$0;

    public /* synthetic */ GroupDescriptionPreference$ViewHolder$$ExternalSyntheticLambda0(GroupDescriptionPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        GroupDescriptionPreference.ViewHolder.m1204bind$lambda0(this.f$0, view);
    }
}
