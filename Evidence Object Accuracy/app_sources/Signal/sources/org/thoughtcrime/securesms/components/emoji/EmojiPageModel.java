package org.thoughtcrime.securesms.components.emoji;

import android.net.Uri;
import java.util.List;

/* loaded from: classes4.dex */
public interface EmojiPageModel {
    List<Emoji> getDisplayEmoji();

    List<String> getEmoji();

    int getIconAttr();

    String getKey();

    Uri getSpriteUri();

    boolean isDynamic();
}
