package org.thoughtcrime.securesms.components.settings.app;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AppSettingsFragment$PaymentsPreferenceViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AppSettingsFragment.PaymentsPreference f$0;

    public /* synthetic */ AppSettingsFragment$PaymentsPreferenceViewHolder$$ExternalSyntheticLambda0(AppSettingsFragment.PaymentsPreference paymentsPreference) {
        this.f$0 = paymentsPreference;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AppSettingsFragment.PaymentsPreferenceViewHolder.m550bind$lambda0(this.f$0, view);
    }
}
