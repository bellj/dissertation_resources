package org.thoughtcrime.securesms.components.settings.app.chats;

import android.app.Application;
import android.content.Context;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceConfigurationUpdateJob;
import org.thoughtcrime.securesms.jobs.MultiDeviceContactUpdateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: ChatsSettingsRepository.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0005\u001a\u00020\u0006J\u0006\u0010\u0007\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsRepository;", "", "()V", "context", "Landroid/content/Context;", "syncLinkPreviewsState", "", "syncPreferSystemContactPhotos", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatsSettingsRepository {
    private final Context context;

    public ChatsSettingsRepository() {
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        this.context = application;
    }

    public final void syncLinkPreviewsState() {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsRepository$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                ChatsSettingsRepository.m631syncLinkPreviewsState$lambda0(ChatsSettingsRepository.this);
            }
        });
    }

    /* renamed from: syncLinkPreviewsState$lambda-0 */
    public static final void m631syncLinkPreviewsState$lambda0(ChatsSettingsRepository chatsSettingsRepository) {
        Intrinsics.checkNotNullParameter(chatsSettingsRepository, "this$0");
        boolean isLinkPreviewsEnabled = SignalStore.settings().isLinkPreviewsEnabled();
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
        ApplicationDependencies.getJobManager().add(new MultiDeviceConfigurationUpdateJob(TextSecurePreferences.isReadReceiptsEnabled(chatsSettingsRepository.context), TextSecurePreferences.isTypingIndicatorsEnabled(chatsSettingsRepository.context), TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(chatsSettingsRepository.context), isLinkPreviewsEnabled));
    }

    public final void syncPreferSystemContactPhotos() {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                ChatsSettingsRepository.m632syncPreferSystemContactPhotos$lambda1();
            }
        });
    }

    /* renamed from: syncPreferSystemContactPhotos$lambda-1 */
    public static final void m632syncPreferSystemContactPhotos$lambda1() {
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        ApplicationDependencies.getJobManager().add(new MultiDeviceContactUpdateJob(true));
        StorageSyncHelper.scheduleSyncForDataChange();
    }
}
