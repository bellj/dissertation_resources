package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.emoji.EmojiTextView;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.GroupDescriptionPreference;
import org.thoughtcrime.securesms.groups.GroupId;
import org.thoughtcrime.securesms.groups.v2.GroupDescriptionUtil;
import org.thoughtcrime.securesms.util.LongClickMovementMethod;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: GroupDescriptionPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\bÆ\u0002\u0018\u00002\u00020\u0001:\u0002\u0007\bB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/GroupDescriptionPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class GroupDescriptionPreference {
    public static final GroupDescriptionPreference INSTANCE = new GroupDescriptionPreference();

    private GroupDescriptionPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.GroupDescriptionPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new GroupDescriptionPreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_group_description_preference));
    }

    /* compiled from: GroupDescriptionPreference.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u000e\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0007\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\u0002\u0010\rJ\u0010\u0010\u0016\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0000H\u0016J\u0010\u0010\u0018\u001a\u00020\u00072\u0006\u0010\u0017\u001a\u00020\u0000H\u0016R\u0011\u0010\b\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0006\u001a\u00020\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000b0\n¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/GroupDescriptionPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "groupId", "Lorg/thoughtcrime/securesms/groups/GroupId;", "groupDescription", "", "descriptionShouldLinkify", "", "canEditGroupAttributes", "onEditGroupDescription", "Lkotlin/Function0;", "", "onViewGroupDescription", "(Lorg/thoughtcrime/securesms/groups/GroupId;Ljava/lang/String;ZZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getCanEditGroupAttributes", "()Z", "getDescriptionShouldLinkify", "getGroupDescription", "()Ljava/lang/String;", "getOnEditGroupDescription", "()Lkotlin/jvm/functions/Function0;", "getOnViewGroupDescription", "areContentsTheSame", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final boolean canEditGroupAttributes;
        private final boolean descriptionShouldLinkify;
        private final String groupDescription;
        private final GroupId groupId;
        private final Function0<Unit> onEditGroupDescription;
        private final Function0<Unit> onViewGroupDescription;

        public final String getGroupDescription() {
            return this.groupDescription;
        }

        public final boolean getDescriptionShouldLinkify() {
            return this.descriptionShouldLinkify;
        }

        public final boolean getCanEditGroupAttributes() {
            return this.canEditGroupAttributes;
        }

        public final Function0<Unit> getOnEditGroupDescription() {
            return this.onEditGroupDescription;
        }

        public final Function0<Unit> getOnViewGroupDescription() {
            return this.onViewGroupDescription;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(GroupId groupId, String str, boolean z, boolean z2, Function0<Unit> function0, Function0<Unit> function02) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(groupId, "groupId");
            Intrinsics.checkNotNullParameter(function0, "onEditGroupDescription");
            Intrinsics.checkNotNullParameter(function02, "onViewGroupDescription");
            this.groupId = groupId;
            this.groupDescription = str;
            this.descriptionShouldLinkify = z;
            this.canEditGroupAttributes = z2;
            this.onEditGroupDescription = function0;
            this.onViewGroupDescription = function02;
        }

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return Intrinsics.areEqual(this.groupId, model.groupId);
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(this.groupDescription, model.groupDescription) && this.descriptionShouldLinkify == model.descriptionShouldLinkify && this.canEditGroupAttributes == model.canEditGroupAttributes;
        }
    }

    /* compiled from: GroupDescriptionPreference.kt */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/GroupDescriptionPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/GroupDescriptionPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "groupDescriptionTextView", "Lorg/thoughtcrime/securesms/components/emoji/EmojiTextView;", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final EmojiTextView groupDescriptionTextView;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = findViewById(R.id.manage_group_description);
            Intrinsics.checkNotNullExpressionValue(findViewById, "findViewById(R.id.manage_group_description)");
            this.groupDescriptionTextView = (EmojiTextView) findViewById;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            this.groupDescriptionTextView.setMovementMethod(LongClickMovementMethod.getInstance(this.context));
            String groupDescription = model.getGroupDescription();
            if (!(groupDescription == null || groupDescription.length() == 0)) {
                this.groupDescriptionTextView.setOnClickListener(null);
                GroupDescriptionUtil.setText(this.context, this.groupDescriptionTextView, model.getGroupDescription(), model.getDescriptionShouldLinkify(), new GroupDescriptionPreference$ViewHolder$$ExternalSyntheticLambda1(model));
            } else if (model.getCanEditGroupAttributes()) {
                this.groupDescriptionTextView.setOverflowText(null);
                this.groupDescriptionTextView.setText(R.string.ManageGroupActivity_add_group_description);
                this.groupDescriptionTextView.setOnClickListener(new GroupDescriptionPreference$ViewHolder$$ExternalSyntheticLambda0(model));
            }
        }

        /* renamed from: bind$lambda-0 */
        public static final void m1204bind$lambda0(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnEditGroupDescription().invoke();
        }

        /* renamed from: bind$lambda-1 */
        public static final void m1205bind$lambda1(Model model) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnViewGroupDescription().invoke();
        }
    }
}
