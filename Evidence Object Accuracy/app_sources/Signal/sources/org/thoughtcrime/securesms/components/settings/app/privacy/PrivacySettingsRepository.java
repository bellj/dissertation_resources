package org.thoughtcrime.securesms.components.settings.app.privacy;

import android.app.Application;
import android.content.Context;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.concurrent.SignalExecutors;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DistributionListPartialRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.thoughtcrime.securesms.jobs.MultiDeviceConfigurationUpdateJob;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.storage.StorageSyncHelper;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* compiled from: PrivacySettingsRepository.kt */
@Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001a\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\u00060\bJ \u0010\n\u001a\u00020\u00062\u0018\u0010\u0007\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0004\u0012\u00020\u00060\bJ\u0006\u0010\r\u001a\u00020\u0006J\u0006\u0010\u000e\u001a\u00020\u0006R\u000e\u0010\u0003\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/PrivacySettingsRepository;", "", "()V", "context", "Landroid/content/Context;", "getBlockedCount", "", "consumer", "Lkotlin/Function1;", "", "getPrivateStories", "", "Lorg/thoughtcrime/securesms/database/model/DistributionListPartialRecord;", "syncReadReceiptState", "syncTypingIndicatorsState", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PrivacySettingsRepository {
    private final Context context;

    public PrivacySettingsRepository() {
        Application application = ApplicationDependencies.getApplication();
        Intrinsics.checkNotNullExpressionValue(application, "getApplication()");
        this.context = application;
    }

    public final void getBlockedCount(Function1<? super Integer, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsRepository$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                PrivacySettingsRepository.m833getBlockedCount$lambda0(Function1.this);
            }
        });
    }

    /* renamed from: getBlockedCount$lambda-0 */
    public static final void m833getBlockedCount$lambda0(Function1 function1) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        function1.invoke(Integer.valueOf(SignalDatabase.Companion.recipients().getBlocked().getCount()));
    }

    public final void getPrivateStories(Function1<? super List<DistributionListPartialRecord>, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "consumer");
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsRepository$$ExternalSyntheticLambda2
            @Override // java.lang.Runnable
            public final void run() {
                PrivacySettingsRepository.m834getPrivateStories$lambda1(Function1.this);
            }
        });
    }

    /* renamed from: getPrivateStories$lambda-1 */
    public static final void m834getPrivateStories$lambda1(Function1 function1) {
        Intrinsics.checkNotNullParameter(function1, "$consumer");
        function1.invoke(SignalDatabase.Companion.distributionLists().getCustomListsForUi());
    }

    public final void syncReadReceiptState() {
        SignalExecutors.BOUNDED.execute(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsRepository$$ExternalSyntheticLambda0
            @Override // java.lang.Runnable
            public final void run() {
                PrivacySettingsRepository.m835syncReadReceiptState$lambda2(PrivacySettingsRepository.this);
            }
        });
    }

    /* renamed from: syncReadReceiptState$lambda-2 */
    public static final void m835syncReadReceiptState$lambda2(PrivacySettingsRepository privacySettingsRepository) {
        Intrinsics.checkNotNullParameter(privacySettingsRepository, "this$0");
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
        ApplicationDependencies.getJobManager().add(new MultiDeviceConfigurationUpdateJob(TextSecurePreferences.isReadReceiptsEnabled(privacySettingsRepository.context), TextSecurePreferences.isTypingIndicatorsEnabled(privacySettingsRepository.context), TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(privacySettingsRepository.context), SignalStore.settings().isLinkPreviewsEnabled()));
    }

    public final void syncTypingIndicatorsState() {
        boolean isTypingIndicatorsEnabled = TextSecurePreferences.isTypingIndicatorsEnabled(this.context);
        RecipientDatabase recipients = SignalDatabase.Companion.recipients();
        RecipientId id = Recipient.self().getId();
        Intrinsics.checkNotNullExpressionValue(id, "self().id");
        recipients.markNeedsSync(id);
        StorageSyncHelper.scheduleSyncForDataChange();
        ApplicationDependencies.getJobManager().add(new MultiDeviceConfigurationUpdateJob(TextSecurePreferences.isReadReceiptsEnabled(this.context), isTypingIndicatorsEnabled, TextSecurePreferences.isShowUnidentifiedDeliveryIndicatorsEnabled(this.context), SignalStore.settings().isLinkPreviewsEnabled()));
        if (!isTypingIndicatorsEnabled) {
            ApplicationDependencies.getTypingStatusRepository().clear();
        }
    }
}
