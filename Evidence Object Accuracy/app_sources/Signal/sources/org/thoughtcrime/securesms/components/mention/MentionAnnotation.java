package org.thoughtcrime.securesms.components.mention;

import android.text.Annotation;
import android.text.Spannable;
import android.text.Spanned;
import com.annimon.stream.Stream;
import com.annimon.stream.function.Function;
import com.annimon.stream.function.Predicate;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.database.model.Mention;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public final class MentionAnnotation {
    public static final String MENTION_ANNOTATION;

    private MentionAnnotation() {
    }

    public static Annotation mentionAnnotationForRecipientId(RecipientId recipientId) {
        return new Annotation("mention", idToMentionAnnotationValue(recipientId));
    }

    public static String idToMentionAnnotationValue(RecipientId recipientId) {
        return String.valueOf(recipientId.toLong());
    }

    public static boolean isMentionAnnotation(Annotation annotation) {
        return "mention".equals(annotation.getKey());
    }

    public static void setMentionAnnotations(Spannable spannable, List<Mention> list) {
        for (Mention mention : list) {
            spannable.setSpan(mentionAnnotationForRecipientId(mention.getRecipientId()), mention.getStart(), mention.getStart() + mention.getLength(), 33);
        }
    }

    public static List<Mention> getMentionsFromAnnotations(CharSequence charSequence) {
        if (!(charSequence instanceof Spanned)) {
            return Collections.emptyList();
        }
        Spanned spanned = (Spanned) charSequence;
        return Stream.of(getMentionAnnotations(spanned)).map(new Function(spanned) { // from class: org.thoughtcrime.securesms.components.mention.MentionAnnotation$$ExternalSyntheticLambda0
            public final /* synthetic */ Spanned f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return MentionAnnotation.lambda$getMentionsFromAnnotations$0(this.f$0, (Annotation) obj);
            }
        }).toList();
    }

    public static /* synthetic */ Mention lambda$getMentionsFromAnnotations$0(Spanned spanned, Annotation annotation) {
        int spanStart = spanned.getSpanStart(annotation);
        return new Mention(RecipientId.from(annotation.getValue()), spanStart, spanned.getSpanEnd(annotation) - spanStart);
    }

    public static List<Annotation> getMentionAnnotations(Spanned spanned) {
        return getMentionAnnotations(spanned, 0, spanned.length());
    }

    public static List<Annotation> getMentionAnnotations(Spanned spanned, int i, int i2) {
        return Stream.of((Annotation[]) spanned.getSpans(i, i2, Annotation.class)).filter(new Predicate() { // from class: org.thoughtcrime.securesms.components.mention.MentionAnnotation$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.Predicate
            public final boolean test(Object obj) {
                return MentionAnnotation.isMentionAnnotation((Annotation) obj);
            }
        }).toList();
    }
}
