package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.view.View;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.util.TextSecurePreferences;

/* loaded from: classes4.dex */
public class DozeReminder extends Reminder {
    public DozeReminder(Context context) {
        super(context.getString(R.string.DozeReminder_optimize_for_missing_play_services), context.getString(R.string.DozeReminder_this_device_does_not_support_play_services_tap_to_disable_system_battery));
        setOkListener(new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.DozeReminder$$ExternalSyntheticLambda0
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                DozeReminder.m518$r8$lambda$scfgOBXKer2L_IkEsFDBsXQo1U(this.f$0, view);
            }
        });
        setDismissListener(new View.OnClickListener(context) { // from class: org.thoughtcrime.securesms.components.reminder.DozeReminder$$ExternalSyntheticLambda1
            public final /* synthetic */ Context f$0;

            {
                this.f$0 = r1;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                DozeReminder.m517$r8$lambda$rsYWJOI4iXoGVSRK2I3N73lTfE(this.f$0, view);
            }
        });
    }

    public static /* synthetic */ void lambda$new$0(Context context, View view) {
        TextSecurePreferences.setPromptedOptimizeDoze(context, true);
        context.startActivity(new Intent("android.settings.REQUEST_IGNORE_BATTERY_OPTIMIZATIONS", Uri.parse("package:" + context.getPackageName())));
    }

    public static boolean isEligible(Context context) {
        return !SignalStore.account().isFcmEnabled() && !TextSecurePreferences.hasPromptedOptimizeDoze(context) && Build.VERSION.SDK_INT >= 23 && !((PowerManager) context.getSystemService("power")).isIgnoringBatteryOptimizations(context.getPackageName());
    }
}
