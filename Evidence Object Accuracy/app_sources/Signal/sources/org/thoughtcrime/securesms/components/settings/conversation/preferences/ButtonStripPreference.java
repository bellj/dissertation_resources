package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.content.res.AppCompatResources;
import j$.util.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.CollectionsKt__CollectionsKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference;
import org.thoughtcrime.securesms.util.ViewExtensionsKt;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* compiled from: ButtonStripPreference.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\bÆ\u0002\u0018\u00002\u00020\u0001:\u0003\u0007\b\tB\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006¨\u0006\n"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference;", "", "()V", "register", "", "adapter", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "Model", "State", "ViewHolder", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ButtonStripPreference {
    public static final ButtonStripPreference INSTANCE = new ButtonStripPreference();

    private ButtonStripPreference() {
    }

    public final void register(MappingAdapter mappingAdapter) {
        Intrinsics.checkNotNullParameter(mappingAdapter, "adapter");
        mappingAdapter.registerFactory(Model.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ButtonStripPreference.ViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.conversation_settings_button_strip));
    }

    /* compiled from: ButtonStripPreference.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001Bi\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u000e\b\u0002\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u000e\b\u0002\u0010\n\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u0007\u0012\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\u0002\u0010\rJ\u0010\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0000H\u0016J\u0010\u0010\u001b\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0000H\u0016R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0011R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0011R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\b0\u0007¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0011R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017¨\u0006\u001c"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$Model;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;", "background", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "onMessageClick", "Lkotlin/Function0;", "", "onVideoClick", "onAudioClick", "onMuteClick", "onSearchClick", "(Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getBackground", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "getOnAudioClick", "()Lkotlin/jvm/functions/Function0;", "getOnMessageClick", "getOnMuteClick", "getOnSearchClick", "getOnVideoClick", "getState", "()Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;", "areContentsTheSame", "", "newItem", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Model extends PreferenceModel<Model> {
        private final DSLSettingsIcon background;
        private final Function0<Unit> onAudioClick;
        private final Function0<Unit> onMessageClick;
        private final Function0<Unit> onMuteClick;
        private final Function0<Unit> onSearchClick;
        private final Function0<Unit> onVideoClick;
        private final State state;

        public boolean areItemsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return true;
        }

        public final State getState() {
            return this.state;
        }

        public final DSLSettingsIcon getBackground() {
            return this.background;
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public /* synthetic */ Model(org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.State r7, org.thoughtcrime.securesms.components.settings.DSLSettingsIcon r8, kotlin.jvm.functions.Function0 r9, kotlin.jvm.functions.Function0 r10, kotlin.jvm.functions.Function0 r11, kotlin.jvm.functions.Function0 r12, kotlin.jvm.functions.Function0 r13, int r14, kotlin.jvm.internal.DefaultConstructorMarker r15) {
            /*
                r6 = this;
                r0 = r14 & 2
                if (r0 == 0) goto L_0x0006
                r0 = 0
                goto L_0x0007
            L_0x0006:
                r0 = r8
            L_0x0007:
                r1 = r14 & 4
                if (r1 == 0) goto L_0x000e
                org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$Model$1 r1 = org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.Model.AnonymousClass1.INSTANCE
                goto L_0x000f
            L_0x000e:
                r1 = r9
            L_0x000f:
                r2 = r14 & 8
                if (r2 == 0) goto L_0x0016
                org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$Model$2 r2 = org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.Model.AnonymousClass2.INSTANCE
                goto L_0x0017
            L_0x0016:
                r2 = r10
            L_0x0017:
                r3 = r14 & 16
                if (r3 == 0) goto L_0x001e
                org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$Model$3 r3 = org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.Model.AnonymousClass3.INSTANCE
                goto L_0x001f
            L_0x001e:
                r3 = r11
            L_0x001f:
                r4 = r14 & 32
                if (r4 == 0) goto L_0x0026
                org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$Model$4 r4 = org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.Model.AnonymousClass4.INSTANCE
                goto L_0x0027
            L_0x0026:
                r4 = r12
            L_0x0027:
                r5 = r14 & 64
                if (r5 == 0) goto L_0x002e
                org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$Model$5 r5 = org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.Model.AnonymousClass5.INSTANCE
                goto L_0x002f
            L_0x002e:
                r5 = r13
            L_0x002f:
                r8 = r6
                r9 = r7
                r10 = r0
                r11 = r1
                r12 = r2
                r13 = r3
                r14 = r4
                r15 = r5
                r8.<init>(r9, r10, r11, r12, r13, r14, r15)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference.Model.<init>(org.thoughtcrime.securesms.components.settings.conversation.preferences.ButtonStripPreference$State, org.thoughtcrime.securesms.components.settings.DSLSettingsIcon, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, kotlin.jvm.functions.Function0, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
        }

        public final Function0<Unit> getOnMessageClick() {
            return this.onMessageClick;
        }

        public final Function0<Unit> getOnVideoClick() {
            return this.onVideoClick;
        }

        public final Function0<Unit> getOnAudioClick() {
            return this.onAudioClick;
        }

        public final Function0<Unit> getOnMuteClick() {
            return this.onMuteClick;
        }

        public final Function0<Unit> getOnSearchClick() {
            return this.onSearchClick;
        }

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public Model(State state, DSLSettingsIcon dSLSettingsIcon, Function0<Unit> function0, Function0<Unit> function02, Function0<Unit> function03, Function0<Unit> function04, Function0<Unit> function05) {
            super(null, null, null, null, false, 31, null);
            Intrinsics.checkNotNullParameter(state, "state");
            Intrinsics.checkNotNullParameter(function0, "onMessageClick");
            Intrinsics.checkNotNullParameter(function02, "onVideoClick");
            Intrinsics.checkNotNullParameter(function03, "onAudioClick");
            Intrinsics.checkNotNullParameter(function04, "onMuteClick");
            Intrinsics.checkNotNullParameter(function05, "onSearchClick");
            this.state = state;
            this.background = dSLSettingsIcon;
            this.onMessageClick = function0;
            this.onVideoClick = function02;
            this.onAudioClick = function03;
            this.onMuteClick = function04;
            this.onSearchClick = function05;
        }

        public boolean areContentsTheSame(Model model) {
            Intrinsics.checkNotNullParameter(model, "newItem");
            return super.areContentsTheSame(model) && Intrinsics.areEqual(this.state, model.state);
        }
    }

    /* compiled from: ButtonStripPreference.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0002H\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0007X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\nX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$ViewHolder;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$Model;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "audioCall", "Landroid/widget/ImageView;", "audioContainer", "audioLabel", "Landroid/widget/TextView;", "message", "messageContainer", "mute", "muteContainer", "muteLabel", "search", "searchContainer", "videoCall", "videoContainer", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Model> {
        private final ImageView audioCall;
        private final View audioContainer;
        private final TextView audioLabel;
        private final View message;
        private final View messageContainer;
        private final ImageView mute;
        private final View muteContainer;
        private final TextView muteLabel;
        private final View search;
        private final View searchContainer;
        private final View videoCall;
        private final View videoContainer;

        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public ViewHolder(View view) {
            super(view);
            Intrinsics.checkNotNullParameter(view, "itemView");
            View findViewById = view.findViewById(R.id.message);
            Intrinsics.checkNotNullExpressionValue(findViewById, "itemView.findViewById(R.id.message)");
            this.message = findViewById;
            View findViewById2 = view.findViewById(R.id.button_strip_message_container);
            Intrinsics.checkNotNullExpressionValue(findViewById2, "itemView.findViewById(R.…_strip_message_container)");
            this.messageContainer = findViewById2;
            View findViewById3 = view.findViewById(R.id.start_video);
            Intrinsics.checkNotNullExpressionValue(findViewById3, "itemView.findViewById(R.id.start_video)");
            this.videoCall = findViewById3;
            View findViewById4 = view.findViewById(R.id.button_strip_video_container);
            Intrinsics.checkNotNullExpressionValue(findViewById4, "itemView.findViewById(R.…on_strip_video_container)");
            this.videoContainer = findViewById4;
            View findViewById5 = view.findViewById(R.id.start_audio);
            Intrinsics.checkNotNullExpressionValue(findViewById5, "itemView.findViewById(R.id.start_audio)");
            this.audioCall = (ImageView) findViewById5;
            View findViewById6 = view.findViewById(R.id.start_audio_label);
            Intrinsics.checkNotNullExpressionValue(findViewById6, "itemView.findViewById(R.id.start_audio_label)");
            this.audioLabel = (TextView) findViewById6;
            View findViewById7 = view.findViewById(R.id.button_strip_audio_container);
            Intrinsics.checkNotNullExpressionValue(findViewById7, "itemView.findViewById(R.…on_strip_audio_container)");
            this.audioContainer = findViewById7;
            View findViewById8 = view.findViewById(R.id.mute);
            Intrinsics.checkNotNullExpressionValue(findViewById8, "itemView.findViewById(R.id.mute)");
            this.mute = (ImageView) findViewById8;
            View findViewById9 = view.findViewById(R.id.mute_label);
            Intrinsics.checkNotNullExpressionValue(findViewById9, "itemView.findViewById(R.id.mute_label)");
            this.muteLabel = (TextView) findViewById9;
            View findViewById10 = view.findViewById(R.id.button_strip_mute_container);
            Intrinsics.checkNotNullExpressionValue(findViewById10, "itemView.findViewById(R.…ton_strip_mute_container)");
            this.muteContainer = findViewById10;
            View findViewById11 = view.findViewById(R.id.search);
            Intrinsics.checkNotNullExpressionValue(findViewById11, "itemView.findViewById(R.id.search)");
            this.search = findViewById11;
            View findViewById12 = view.findViewById(R.id.button_strip_search_container);
            Intrinsics.checkNotNullExpressionValue(findViewById12, "itemView.findViewById(R.…n_strip_search_container)");
            this.searchContainer = findViewById12;
        }

        public void bind(Model model) {
            Intrinsics.checkNotNullParameter(model, "model");
            ViewExtensionsKt.setVisible(this.messageContainer, model.getState().isMessageAvailable());
            ViewExtensionsKt.setVisible(this.videoContainer, model.getState().isVideoAvailable());
            ViewExtensionsKt.setVisible(this.audioContainer, model.getState().isAudioAvailable());
            ViewExtensionsKt.setVisible(this.muteContainer, model.getState().isMuteAvailable());
            ViewExtensionsKt.setVisible(this.searchContainer, model.getState().isSearchAvailable());
            if (model.getState().isAudioSecure()) {
                this.audioLabel.setText(R.string.ConversationSettingsFragment__audio);
                this.audioCall.setImageDrawable(AppCompatResources.getDrawable(this.context, R.drawable.ic_phone_right_24));
            } else {
                this.audioLabel.setText(R.string.ConversationSettingsFragment__call);
                this.audioCall.setImageDrawable(AppCompatResources.getDrawable(this.context, R.drawable.ic_phone_right_unlock_primary_accent_24));
            }
            if (model.getState().isMuted()) {
                this.mute.setImageDrawable(AppCompatResources.getDrawable(this.context, R.drawable.ic_bell_disabled_24));
                this.muteLabel.setText(R.string.ConversationSettingsFragment__muted);
            } else {
                this.mute.setImageDrawable(AppCompatResources.getDrawable(this.context, R.drawable.ic_bell_24));
                this.muteLabel.setText(R.string.ConversationSettingsFragment__mute);
            }
            if (model.getBackground() != null) {
                for (View view : CollectionsKt__CollectionsKt.listOf((Object[]) new View[]{this.message, this.videoCall, this.audioCall, this.mute, this.search})) {
                    DSLSettingsIcon background = model.getBackground();
                    Context context = this.context;
                    Intrinsics.checkNotNullExpressionValue(context, "context");
                    view.setBackground(background.resolve(context));
                }
            }
            this.message.setOnClickListener(new ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda0(model));
            this.videoCall.setOnClickListener(new ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda1(model));
            this.audioCall.setOnClickListener(new ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda2(model));
            this.mute.setOnClickListener(new ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda3(model));
            this.search.setOnClickListener(new ButtonStripPreference$ViewHolder$$ExternalSyntheticLambda4(model));
        }

        /* renamed from: bind$lambda-1 */
        public static final void m1199bind$lambda1(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnMessageClick().invoke();
        }

        /* renamed from: bind$lambda-2 */
        public static final void m1200bind$lambda2(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnVideoClick().invoke();
        }

        /* renamed from: bind$lambda-3 */
        public static final void m1201bind$lambda3(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnAudioClick().invoke();
        }

        /* renamed from: bind$lambda-4 */
        public static final void m1202bind$lambda4(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnMuteClick().invoke();
        }

        /* renamed from: bind$lambda-5 */
        public static final void m1203bind$lambda5(Model model, View view) {
            Intrinsics.checkNotNullParameter(model, "$model");
            model.getOnSearchClick().invoke();
        }
    }

    /* compiled from: ButtonStripPreference.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0013\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001BK\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0005\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\u0003\u0012\b\b\u0002\u0010\t\u001a\u00020\u0003¢\u0006\u0002\u0010\nJ\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0010\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0003HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0003HÆ\u0003JO\u0010\u0013\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u0014\u001a\u00020\u00032\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u000bR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u000bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u000bR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0006\u0010\u000bR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0004\u0010\u000b¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/preferences/ButtonStripPreference$State;", "", "isMessageAvailable", "", "isVideoAvailable", "isAudioAvailable", "isMuteAvailable", "isSearchAvailable", "isAudioSecure", "isMuted", "(ZZZZZZZ)V", "()Z", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class State {
        private final boolean isAudioAvailable;
        private final boolean isAudioSecure;
        private final boolean isMessageAvailable;
        private final boolean isMuteAvailable;
        private final boolean isMuted;
        private final boolean isSearchAvailable;
        private final boolean isVideoAvailable;

        public State() {
            this(false, false, false, false, false, false, false, 127, null);
        }

        public static /* synthetic */ State copy$default(State state, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i, Object obj) {
            if ((i & 1) != 0) {
                z = state.isMessageAvailable;
            }
            if ((i & 2) != 0) {
                z2 = state.isVideoAvailable;
            }
            if ((i & 4) != 0) {
                z3 = state.isAudioAvailable;
            }
            if ((i & 8) != 0) {
                z4 = state.isMuteAvailable;
            }
            if ((i & 16) != 0) {
                z5 = state.isSearchAvailable;
            }
            if ((i & 32) != 0) {
                z6 = state.isAudioSecure;
            }
            if ((i & 64) != 0) {
                z7 = state.isMuted;
            }
            return state.copy(z, z2, z3, z4, z5, z6, z7);
        }

        public final boolean component1() {
            return this.isMessageAvailable;
        }

        public final boolean component2() {
            return this.isVideoAvailable;
        }

        public final boolean component3() {
            return this.isAudioAvailable;
        }

        public final boolean component4() {
            return this.isMuteAvailable;
        }

        public final boolean component5() {
            return this.isSearchAvailable;
        }

        public final boolean component6() {
            return this.isAudioSecure;
        }

        public final boolean component7() {
            return this.isMuted;
        }

        public final State copy(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            return new State(z, z2, z3, z4, z5, z6, z7);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof State)) {
                return false;
            }
            State state = (State) obj;
            return this.isMessageAvailable == state.isMessageAvailable && this.isVideoAvailable == state.isVideoAvailable && this.isAudioAvailable == state.isAudioAvailable && this.isMuteAvailable == state.isMuteAvailable && this.isSearchAvailable == state.isSearchAvailable && this.isAudioSecure == state.isAudioSecure && this.isMuted == state.isMuted;
        }

        public int hashCode() {
            boolean z = this.isMessageAvailable;
            int i = 1;
            if (z) {
                z = true;
            }
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            int i4 = z ? 1 : 0;
            int i5 = i2 * 31;
            boolean z2 = this.isVideoAvailable;
            if (z2) {
                z2 = true;
            }
            int i6 = z2 ? 1 : 0;
            int i7 = z2 ? 1 : 0;
            int i8 = z2 ? 1 : 0;
            int i9 = (i5 + i6) * 31;
            boolean z3 = this.isAudioAvailable;
            if (z3) {
                z3 = true;
            }
            int i10 = z3 ? 1 : 0;
            int i11 = z3 ? 1 : 0;
            int i12 = z3 ? 1 : 0;
            int i13 = (i9 + i10) * 31;
            boolean z4 = this.isMuteAvailable;
            if (z4) {
                z4 = true;
            }
            int i14 = z4 ? 1 : 0;
            int i15 = z4 ? 1 : 0;
            int i16 = z4 ? 1 : 0;
            int i17 = (i13 + i14) * 31;
            boolean z5 = this.isSearchAvailable;
            if (z5) {
                z5 = true;
            }
            int i18 = z5 ? 1 : 0;
            int i19 = z5 ? 1 : 0;
            int i20 = z5 ? 1 : 0;
            int i21 = (i17 + i18) * 31;
            boolean z6 = this.isAudioSecure;
            if (z6) {
                z6 = true;
            }
            int i22 = z6 ? 1 : 0;
            int i23 = z6 ? 1 : 0;
            int i24 = z6 ? 1 : 0;
            int i25 = (i21 + i22) * 31;
            boolean z7 = this.isMuted;
            if (!z7) {
                i = z7 ? 1 : 0;
            }
            return i25 + i;
        }

        public String toString() {
            return "State(isMessageAvailable=" + this.isMessageAvailable + ", isVideoAvailable=" + this.isVideoAvailable + ", isAudioAvailable=" + this.isAudioAvailable + ", isMuteAvailable=" + this.isMuteAvailable + ", isSearchAvailable=" + this.isSearchAvailable + ", isAudioSecure=" + this.isAudioSecure + ", isMuted=" + this.isMuted + ')';
        }

        public State(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
            this.isMessageAvailable = z;
            this.isVideoAvailable = z2;
            this.isAudioAvailable = z3;
            this.isMuteAvailable = z4;
            this.isSearchAvailable = z5;
            this.isAudioSecure = z6;
            this.isMuted = z7;
        }

        public /* synthetic */ State(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this((i & 1) != 0 ? false : z, (i & 2) != 0 ? false : z2, (i & 4) != 0 ? false : z3, (i & 8) != 0 ? false : z4, (i & 16) != 0 ? false : z5, (i & 32) != 0 ? false : z6, (i & 64) != 0 ? false : z7);
        }

        public final boolean isMessageAvailable() {
            return this.isMessageAvailable;
        }

        public final boolean isVideoAvailable() {
            return this.isVideoAvailable;
        }

        public final boolean isAudioAvailable() {
            return this.isAudioAvailable;
        }

        public final boolean isMuteAvailable() {
            return this.isMuteAvailable;
        }

        public final boolean isSearchAvailable() {
            return this.isSearchAvailable;
        }

        public final boolean isAudioSecure() {
            return this.isAudioSecure;
        }

        public final boolean isMuted() {
            return this.isMuted;
        }
    }
}
