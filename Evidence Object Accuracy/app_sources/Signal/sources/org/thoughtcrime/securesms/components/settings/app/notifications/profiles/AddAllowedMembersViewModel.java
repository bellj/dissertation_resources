package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Function;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.PushContactSelectionActivity;
import org.thoughtcrime.securesms.contacts.ContactRepository;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* compiled from: AddAllowedMembersViewModel.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bJ\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\rJ\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00100\b2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0013"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersViewModel;", "Landroidx/lifecycle/ViewModel;", "profileId", "", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "(JLorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", "addMember", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", ContactRepository.ID_COLUMN, "Lorg/thoughtcrime/securesms/recipients/RecipientId;", "getProfile", "Lio/reactivex/rxjava3/core/Observable;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersViewModel$NotificationProfileAndRecipients;", "removeMember", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "Factory", "NotificationProfileAndRecipients", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AddAllowedMembersViewModel extends ViewModel {
    private final long profileId;
    private final NotificationProfilesRepository repository;

    public AddAllowedMembersViewModel(long j, NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "repository");
        this.profileId = j;
        this.repository = notificationProfilesRepository;
    }

    public final Observable<NotificationProfileAndRecipients> getProfile() {
        Observable<NotificationProfileAndRecipients> observeOn = this.repository.getProfile(this.profileId).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersViewModel$$ExternalSyntheticLambda0
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return AddAllowedMembersViewModel.m730getProfile$lambda1((NotificationProfile) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.getProfile(pr…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: getProfile$lambda-1 */
    public static final NotificationProfileAndRecipients m730getProfile$lambda1(NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullExpressionValue(notificationProfile, "profile");
        Set<RecipientId> allowedMembers = notificationProfile.getAllowedMembers();
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(allowedMembers, 10));
        for (RecipientId recipientId : allowedMembers) {
            Recipient resolved = Recipient.resolved(recipientId);
            Intrinsics.checkNotNullExpressionValue(resolved, "resolved(it)");
            arrayList.add(resolved);
        }
        return new NotificationProfileAndRecipients(notificationProfile, arrayList);
    }

    public final Single<NotificationProfile> addMember(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Single<NotificationProfile> observeOn = this.repository.addMember(this.profileId, recipientId).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.addMember(pro…dSchedulers.mainThread())");
        return observeOn;
    }

    public final Single<Recipient> removeMember(RecipientId recipientId) {
        Intrinsics.checkNotNullParameter(recipientId, ContactRepository.ID_COLUMN);
        Single<Recipient> observeOn = this.repository.removeMember(this.profileId, recipientId).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return AddAllowedMembersViewModel.m731removeMember$lambda2(RecipientId.this, (NotificationProfile) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "repository.removeMember(…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: removeMember$lambda-2 */
    public static final Recipient m731removeMember$lambda2(RecipientId recipientId, NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(recipientId, "$id");
        return Recipient.resolved(recipientId);
    }

    /* compiled from: AddAllowedMembersViewModel.kt */
    @Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\u000f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0003J#\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0012\u001a\u00020\u0013HÖ\u0001J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersViewModel$NotificationProfileAndRecipients;", "", "profile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS, "", "Lorg/thoughtcrime/securesms/recipients/Recipient;", "(Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;Ljava/util/List;)V", "getProfile", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "getRecipients", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class NotificationProfileAndRecipients {
        private final NotificationProfile profile;
        private final List<Recipient> recipients;

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.AddAllowedMembersViewModel$NotificationProfileAndRecipients */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ NotificationProfileAndRecipients copy$default(NotificationProfileAndRecipients notificationProfileAndRecipients, NotificationProfile notificationProfile, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                notificationProfile = notificationProfileAndRecipients.profile;
            }
            if ((i & 2) != 0) {
                list = notificationProfileAndRecipients.recipients;
            }
            return notificationProfileAndRecipients.copy(notificationProfile, list);
        }

        public final NotificationProfile component1() {
            return this.profile;
        }

        public final List<Recipient> component2() {
            return this.recipients;
        }

        public final NotificationProfileAndRecipients copy(NotificationProfile notificationProfile, List<? extends Recipient> list) {
            Intrinsics.checkNotNullParameter(notificationProfile, "profile");
            Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
            return new NotificationProfileAndRecipients(notificationProfile, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof NotificationProfileAndRecipients)) {
                return false;
            }
            NotificationProfileAndRecipients notificationProfileAndRecipients = (NotificationProfileAndRecipients) obj;
            return Intrinsics.areEqual(this.profile, notificationProfileAndRecipients.profile) && Intrinsics.areEqual(this.recipients, notificationProfileAndRecipients.recipients);
        }

        public int hashCode() {
            return (this.profile.hashCode() * 31) + this.recipients.hashCode();
        }

        public String toString() {
            return "NotificationProfileAndRecipients(profile=" + this.profile + ", recipients=" + this.recipients + ')';
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.recipients.Recipient> */
        /* JADX WARN: Multi-variable type inference failed */
        public NotificationProfileAndRecipients(NotificationProfile notificationProfile, List<? extends Recipient> list) {
            Intrinsics.checkNotNullParameter(notificationProfile, "profile");
            Intrinsics.checkNotNullParameter(list, PushContactSelectionActivity.KEY_SELECTED_RECIPIENTS);
            this.profile = notificationProfile;
            this.recipients = list;
        }

        public final NotificationProfile getProfile() {
            return this.profile;
        }

        public final List<Recipient> getRecipients() {
            return this.recipients;
        }
    }

    /* compiled from: AddAllowedMembersViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/AddAllowedMembersViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "profileId", "", "(J)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long profileId;

        public Factory(long j) {
            this.profileId = j;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new AddAllowedMembersViewModel(this.profileId, new NotificationProfilesRepository()));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }
}
