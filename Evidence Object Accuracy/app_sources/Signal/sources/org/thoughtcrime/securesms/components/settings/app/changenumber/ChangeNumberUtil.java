package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavBackStackEntry;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberViewModel;
import org.thoughtcrime.securesms.registration.fragments.CaptchaFragment;
import org.thoughtcrime.securesms.registration.viewmodel.BaseRegistrationViewModel;

/* compiled from: ChangeNumberUtil.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\n\u0010\t\u001a\u00020\n*\u00020\b¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberUtil;", "", "()V", "getCaptchaArguments", "Landroid/os/Bundle;", "getViewModel", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel;", "fragment", "Landroidx/fragment/app/Fragment;", "changeNumberSuccess", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberUtil {
    public static final ChangeNumberUtil INSTANCE = new ChangeNumberUtil();

    private ChangeNumberUtil() {
    }

    @JvmStatic
    public static final ChangeNumberViewModel getViewModel(Fragment fragment) {
        Intrinsics.checkNotNullParameter(fragment, "fragment");
        NavController findNavController = NavHostFragment.findNavController(fragment);
        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(fragment)");
        ViewModelStoreOwner viewModelStoreOwner = findNavController.getViewModelStoreOwner(R.id.app_settings_change_number);
        NavBackStackEntry backStackEntry = findNavController.getBackStackEntry(R.id.app_settings_change_number);
        Intrinsics.checkNotNullExpressionValue(backStackEntry, "navController.getBackSta…p_settings_change_number)");
        ViewModel viewModel = new ViewModelProvider(viewModelStoreOwner, new ChangeNumberViewModel.Factory(backStackEntry)).get(ChangeNumberViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(\n     …berViewModel::class.java)");
        return (ChangeNumberViewModel) viewModel;
    }

    public final Bundle getCaptchaArguments() {
        Bundle bundle = new Bundle();
        bundle.putSerializable(CaptchaFragment.EXTRA_VIEW_MODEL_PROVIDER, new CaptchaFragment.CaptchaViewModelProvider() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberUtil$getCaptchaArguments$1$1
            @Override // org.thoughtcrime.securesms.registration.fragments.CaptchaFragment.CaptchaViewModelProvider
            public BaseRegistrationViewModel get(CaptchaFragment captchaFragment) {
                Intrinsics.checkNotNullParameter(captchaFragment, "fragment");
                return ChangeNumberUtil.getViewModel(captchaFragment);
            }
        });
        return bundle;
    }

    public final void changeNumberSuccess(Fragment fragment) {
        Intrinsics.checkNotNullParameter(fragment, "<this>");
        fragment.requireActivity().finish();
        FragmentActivity requireActivity = fragment.requireActivity();
        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
        Context requireContext = fragment.requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        requireActivity.startActivity(companion.home(requireContext, AppSettingsActivity.ACTION_CHANGE_NUMBER_SUCCESS));
    }
}
