package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CenterInside;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.mms.SlidesClickedListener;

/* loaded from: classes4.dex */
public class BorderlessImageView extends FrameLayout {
    private ThumbnailView image;
    private View missingShade;

    public BorderlessImageView(Context context) {
        super(context);
        init();
    }

    public BorderlessImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init();
    }

    private void init() {
        FrameLayout.inflate(getContext(), R.layout.sticker_view, this);
        this.image = (ThumbnailView) findViewById(R.id.sticker_thumbnail);
        this.missingShade = findViewById(R.id.sticker_missing_shade);
    }

    @Override // android.view.View
    public void setFocusable(boolean z) {
        this.image.setFocusable(z);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        this.image.setClickable(z);
    }

    @Override // android.view.View
    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.image.setOnLongClickListener(onLongClickListener);
    }

    public void setSlide(GlideRequests glideRequests, Slide slide) {
        int i = 0;
        boolean z = slide.asAttachment().getUri() == null;
        if (slide.hasSticker()) {
            this.image.setFit(new CenterInside());
            this.image.setImageResource(glideRequests, slide, z, false);
        } else {
            this.image.setFit(new CenterCrop());
            this.image.setImageResource(glideRequests, slide, z, false, slide.asAttachment().getWidth(), slide.asAttachment().getHeight());
        }
        View view = this.missingShade;
        if (!z) {
            i = 8;
        }
        view.setVisibility(i);
    }

    public void setThumbnailClickListener(SlideClickListener slideClickListener) {
        this.image.setThumbnailClickListener(slideClickListener);
    }

    public void setDownloadClickListener(SlidesClickedListener slidesClickedListener) {
        this.image.setDownloadClickListener(slidesClickedListener);
    }
}
