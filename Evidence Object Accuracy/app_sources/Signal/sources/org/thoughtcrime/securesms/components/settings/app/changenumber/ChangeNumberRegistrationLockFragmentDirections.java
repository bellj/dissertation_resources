package org.thoughtcrime.securesms.components.settings.app.changenumber;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsChangeNumberDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class ChangeNumberRegistrationLockFragmentDirections {
    private ChangeNumberRegistrationLockFragmentDirections() {
    }

    public static NavDirections actionChangeNumberRegistrationLockToChangeNumberAccountLocked() {
        return new ActionOnlyNavDirections(R.id.action_changeNumberRegistrationLock_to_changeNumberAccountLocked);
    }

    public static NavDirections actionChangeNumberRegistrationLockToChangeNumberPinDiffers() {
        return new ActionOnlyNavDirections(R.id.action_changeNumberRegistrationLock_to_changeNumberPinDiffers);
    }

    public static NavDirections actionPopAppSettingsChangeNumber() {
        return AppSettingsChangeNumberDirections.actionPopAppSettingsChangeNumber();
    }
}
