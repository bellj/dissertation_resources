package org.thoughtcrime.securesms.components.settings.app.wrapped;

import android.os.Bundle;
import java.util.HashMap;
import org.thoughtcrime.securesms.help.HelpFragment;

/* loaded from: classes4.dex */
public class WrappedHelpFragmentArgs {
    private final HashMap arguments;

    private WrappedHelpFragmentArgs() {
        this.arguments = new HashMap();
    }

    private WrappedHelpFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static WrappedHelpFragmentArgs fromBundle(Bundle bundle) {
        WrappedHelpFragmentArgs wrappedHelpFragmentArgs = new WrappedHelpFragmentArgs();
        bundle.setClassLoader(WrappedHelpFragmentArgs.class.getClassLoader());
        if (bundle.containsKey(HelpFragment.START_CATEGORY_INDEX)) {
            wrappedHelpFragmentArgs.arguments.put(HelpFragment.START_CATEGORY_INDEX, Integer.valueOf(bundle.getInt(HelpFragment.START_CATEGORY_INDEX)));
        } else {
            wrappedHelpFragmentArgs.arguments.put(HelpFragment.START_CATEGORY_INDEX, 0);
        }
        return wrappedHelpFragmentArgs;
    }

    public int getStartCategoryIndex() {
        return ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX)) {
            bundle.putInt(HelpFragment.START_CATEGORY_INDEX, ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue());
        } else {
            bundle.putInt(HelpFragment.START_CATEGORY_INDEX, 0);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        WrappedHelpFragmentArgs wrappedHelpFragmentArgs = (WrappedHelpFragmentArgs) obj;
        return this.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX) == wrappedHelpFragmentArgs.arguments.containsKey(HelpFragment.START_CATEGORY_INDEX) && getStartCategoryIndex() == wrappedHelpFragmentArgs.getStartCategoryIndex();
    }

    public int hashCode() {
        return 31 + getStartCategoryIndex();
    }

    public String toString() {
        return "WrappedHelpFragmentArgs{startCategoryIndex=" + getStartCategoryIndex() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(WrappedHelpFragmentArgs wrappedHelpFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(wrappedHelpFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public WrappedHelpFragmentArgs build() {
            return new WrappedHelpFragmentArgs(this.arguments);
        }

        public Builder setStartCategoryIndex(int i) {
            this.arguments.put(HelpFragment.START_CATEGORY_INDEX, Integer.valueOf(i));
            return this;
        }

        public int getStartCategoryIndex() {
            return ((Integer) this.arguments.get(HelpFragment.START_CATEGORY_INDEX)).intValue();
        }
    }
}
