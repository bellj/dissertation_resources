package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import kotlin.Metadata;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004¢\u0006\u0002\u0010\u0005J\u0010\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\u0002H\u0016¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/ClickPreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceViewHolder;", "Lorg/thoughtcrime/securesms/components/settings/ClickPreference;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "bind", "", "model", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ClickPreferenceViewHolder extends PreferenceViewHolder<ClickPreference> {
    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ClickPreferenceViewHolder(View view) {
        super(view);
        Intrinsics.checkNotNullParameter(view, "itemView");
    }

    public void bind(ClickPreference clickPreference) {
        Intrinsics.checkNotNullParameter(clickPreference, "model");
        super.bind((ClickPreferenceViewHolder) clickPreference);
        this.itemView.setOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.ClickPreferenceViewHolder$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ClickPreferenceViewHolder.$r8$lambda$EGCUelMBVfmkZDKN7yne31HGfoM(ClickPreference.this, view);
            }
        });
        this.itemView.setOnLongClickListener(new View.OnLongClickListener() { // from class: org.thoughtcrime.securesms.components.settings.ClickPreferenceViewHolder$$ExternalSyntheticLambda1
            @Override // android.view.View.OnLongClickListener
            public final boolean onLongClick(View view) {
                return ClickPreferenceViewHolder.$r8$lambda$dJR_HR0VsX9JpsDOwi2BYzbBXgU(ClickPreference.this, view);
            }
        });
    }

    /* renamed from: bind$lambda-0 */
    public static final void m520bind$lambda0(ClickPreference clickPreference, View view) {
        Intrinsics.checkNotNullParameter(clickPreference, "$model");
        clickPreference.getOnClick().invoke();
    }

    /* renamed from: bind$lambda-1 */
    public static final boolean m521bind$lambda1(ClickPreference clickPreference, View view) {
        Intrinsics.checkNotNullParameter(clickPreference, "$model");
        Function0<Boolean> onLongClick = clickPreference.getOnLongClick();
        if (onLongClick != null) {
            return onLongClick.invoke().booleanValue();
        }
        return false;
    }
}
