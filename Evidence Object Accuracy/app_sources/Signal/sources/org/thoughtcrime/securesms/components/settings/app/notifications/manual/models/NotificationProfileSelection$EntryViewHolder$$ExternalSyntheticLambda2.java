package org.thoughtcrime.securesms.components.settings.app.notifications.manual.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.notifications.manual.models.NotificationProfileSelection;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda2 implements View.OnClickListener {
    public final /* synthetic */ NotificationProfileSelection.Entry f$0;

    public /* synthetic */ NotificationProfileSelection$EntryViewHolder$$ExternalSyntheticLambda2(NotificationProfileSelection.Entry entry) {
        this.f$0 = entry;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        NotificationProfileSelection.EntryViewHolder.m723bind$lambda2(this.f$0, view);
    }
}
