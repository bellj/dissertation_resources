package org.thoughtcrime.securesms.components.settings.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.lifecycle.Observer;
import androidx.navigation.NavDirections;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import io.reactivex.rxjava3.subjects.PublishSubject;
import io.reactivex.rxjava3.subjects.Subject;
import java.util.Set;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MainActivity;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.components.settings.DSLSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileScheduleFragmentArgs;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent;
import org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentRepository;
import org.thoughtcrime.securesms.help.HelpFragment;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.phonenumbers.PhoneNumberFormatter;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.service.KeyCachingService;
import org.thoughtcrime.securesms.util.CachedInflater;
import org.thoughtcrime.securesms.util.DynamicTheme;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: AppSettingsActivity.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 !2\u00020\u00012\u00020\u0002:\u0002!\"B\u0005¢\u0006\u0002\u0010\u0003J\"\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\u001a\u0010\u0018\u001a\u00020\u00122\b\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u0010H\u0014J\u0012\u0010\u001c\u001a\u00020\u00122\b\u0010\u001d\u001a\u0004\u0018\u00010\u0017H\u0014J\u0010\u0010\u001e\u001a\u00020\u00122\u0006\u0010\u001f\u001a\u00020\u001aH\u0014J\b\u0010 \u001a\u00020\u0012H\u0014R\u001b\u0010\u0004\u001a\u00020\u00058VX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001a\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000¨\u0006#"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsActivity;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent;", "()V", "donationPaymentRepository", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "getDonationPaymentRepository", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentRepository;", "donationPaymentRepository$delegate", "Lkotlin/Lazy;", "googlePayResultPublisher", "Lio/reactivex/rxjava3/subjects/Subject;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/DonationPaymentComponent$GooglePayResult;", "getGooglePayResultPublisher", "()Lio/reactivex/rxjava3/subjects/Subject;", "wasConfigurationUpdated", "", "onActivityResult", "", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "ready", "onNewIntent", "intent", "onSaveInstanceState", "outState", "onWillFinish", "Companion", "StartLocation", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppSettingsActivity extends DSLSettingsActivity implements DonationPaymentComponent {
    public static final String ACTION_CHANGE_NUMBER_SUCCESS;
    public static final Companion Companion = new Companion(null);
    private final Lazy donationPaymentRepository$delegate = LazyKt__LazyJVMKt.lazy(new Function0<DonationPaymentRepository>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity$donationPaymentRepository$2
        final /* synthetic */ AppSettingsActivity this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final DonationPaymentRepository invoke() {
            return new DonationPaymentRepository(this.this$0);
        }
    });
    private final Subject<DonationPaymentComponent.GooglePayResult> googlePayResultPublisher;
    private boolean wasConfigurationUpdated;

    /* compiled from: AppSettingsActivity.kt */
    @Metadata(k = 3, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public /* synthetic */ class WhenMappings {
        public static final /* synthetic */ int[] $EnumSwitchMapping$0;

        static {
            int[] iArr = new int[StartLocation.values().length];
            iArr[StartLocation.HOME.ordinal()] = 1;
            iArr[StartLocation.BACKUPS.ordinal()] = 2;
            iArr[StartLocation.HELP.ordinal()] = 3;
            iArr[StartLocation.PROXY.ordinal()] = 4;
            iArr[StartLocation.NOTIFICATIONS.ordinal()] = 5;
            iArr[StartLocation.CHANGE_NUMBER.ordinal()] = 6;
            iArr[StartLocation.SUBSCRIPTIONS.ordinal()] = 7;
            iArr[StartLocation.BOOST.ordinal()] = 8;
            iArr[StartLocation.MANAGE_SUBSCRIPTIONS.ordinal()] = 9;
            iArr[StartLocation.NOTIFICATION_PROFILES.ordinal()] = 10;
            iArr[StartLocation.CREATE_NOTIFICATION_PROFILE.ordinal()] = 11;
            iArr[StartLocation.NOTIFICATION_PROFILE_DETAILS.ordinal()] = 12;
            iArr[StartLocation.PRIVACY.ordinal()] = 13;
            $EnumSwitchMapping$0 = iArr;
        }
    }

    @JvmStatic
    public static final Intent backups(Context context) {
        return Companion.backups(context);
    }

    @JvmStatic
    public static final Intent boost(Context context) {
        return Companion.boost(context);
    }

    @JvmStatic
    public static final Intent changeNumber(Context context) {
        return Companion.changeNumber(context);
    }

    @JvmStatic
    public static final Intent createNotificationProfile(Context context) {
        return Companion.createNotificationProfile(context);
    }

    @JvmStatic
    public static final Intent help(Context context, int i) {
        return Companion.help(context, i);
    }

    @JvmStatic
    public static final Intent home(Context context) {
        return Companion.home(context);
    }

    @JvmStatic
    public static final Intent home(Context context, String str) {
        return Companion.home(context, str);
    }

    @JvmStatic
    public static final Intent manageSubscriptions(Context context) {
        return Companion.manageSubscriptions(context);
    }

    @JvmStatic
    public static final Intent notificationProfileDetails(Context context, long j) {
        return Companion.notificationProfileDetails(context, j);
    }

    @JvmStatic
    public static final Intent notificationProfiles(Context context) {
        return Companion.notificationProfiles(context);
    }

    @JvmStatic
    public static final Intent notifications(Context context) {
        return Companion.notifications(context);
    }

    @JvmStatic
    public static final Intent privacy(Context context) {
        return Companion.privacy(context);
    }

    @JvmStatic
    public static final Intent proxy(Context context) {
        return Companion.proxy(context);
    }

    @JvmStatic
    public static final Intent subscriptions(Context context) {
        return Companion.subscriptions(context);
    }

    public AppSettingsActivity() {
        PublishSubject create = PublishSubject.create();
        Intrinsics.checkNotNullExpressionValue(create, "create()");
        this.googlePayResultPublisher = create;
    }

    @Override // org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent
    public DonationPaymentRepository getDonationPaymentRepository() {
        return (DonationPaymentRepository) this.donationPaymentRepository$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.app.subscription.DonationPaymentComponent
    public Subject<DonationPaymentComponent.GooglePayResult> getGooglePayResultPublisher() {
        return this.googlePayResultPublisher;
    }

    /* access modifiers changed from: protected */
    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsActivity, org.thoughtcrime.securesms.PassphraseRequiredActivity
    public void onCreate(Bundle bundle, boolean z) {
        NavDirections navDirections;
        String stringExtra;
        Set<String> categories;
        Intent intent;
        Intent intent2 = getIntent();
        if (!(intent2 != null && intent2.hasExtra(DSLSettingsActivity.ARG_NAV_GRAPH)) && (intent = getIntent()) != null) {
            intent.putExtra(DSLSettingsActivity.ARG_NAV_GRAPH, R.navigation.app_settings);
        }
        super.onCreate(bundle, z);
        Intent intent3 = getIntent();
        if ((intent3 == null || (categories = intent3.getCategories()) == null || !categories.contains("android.intent.category.NOTIFICATION_PREFERENCES")) ? false : true) {
            navDirections = AppSettingsFragmentDirections.actionDirectToNotificationsSettingsFragment();
        } else {
            StartLocation.Companion companion = StartLocation.Companion;
            Intent intent4 = getIntent();
            switch (WhenMappings.$EnumSwitchMapping$0[companion.fromCode(intent4 != null ? Integer.valueOf(intent4.getIntExtra("app.settings.start.location", StartLocation.HOME.getCode())) : null).ordinal()]) {
                case 1:
                    navDirections = null;
                    break;
                case 2:
                    navDirections = AppSettingsFragmentDirections.actionDirectToBackupsPreferenceFragment();
                    break;
                case 3:
                    navDirections = AppSettingsFragmentDirections.actionDirectToHelpFragment().setStartCategoryIndex(getIntent().getIntExtra(HelpFragment.START_CATEGORY_INDEX, 0));
                    break;
                case 4:
                    navDirections = AppSettingsFragmentDirections.actionDirectToEditProxyFragment();
                    break;
                case 5:
                    navDirections = AppSettingsFragmentDirections.actionDirectToNotificationsSettingsFragment();
                    break;
                case 6:
                    navDirections = AppSettingsFragmentDirections.actionDirectToChangeNumberFragment();
                    break;
                case 7:
                    navDirections = AppSettingsFragmentDirections.actionDirectToSubscriptions();
                    break;
                case 8:
                    navDirections = AppSettingsFragmentDirections.actionAppSettingsFragmentToBoostsFragment();
                    break;
                case 9:
                    navDirections = AppSettingsFragmentDirections.actionDirectToManageDonations();
                    break;
                case 10:
                    navDirections = AppSettingsFragmentDirections.actionDirectToNotificationProfiles();
                    break;
                case 11:
                    navDirections = AppSettingsFragmentDirections.actionDirectToCreateNotificationProfiles();
                    break;
                case 12:
                    Bundle bundleExtra = getIntent().getBundleExtra("app.settings.start.arguments");
                    Intrinsics.checkNotNull(bundleExtra);
                    navDirections = AppSettingsFragmentDirections.actionDirectToNotificationProfileDetails(EditNotificationProfileScheduleFragmentArgs.fromBundle(bundleExtra).getProfileId());
                    break;
                case 13:
                    navDirections = AppSettingsFragmentDirections.actionDirectToPrivacy();
                    break;
                default:
                    throw new NoWhenBranchMatchedException();
            }
        }
        setIntent(getIntent().putExtra("app.settings.start.location", StartLocation.HOME));
        if (navDirections == null && bundle != null) {
            this.wasConfigurationUpdated = bundle.getBoolean("app.settings.state.configuration.updated");
        }
        if (navDirections != null) {
            SafeNavigation.safeNavigate(getNavController(), navDirections);
        }
        SignalStore.settings().getOnConfigurationSettingChanged().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity$$ExternalSyntheticLambda0
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AppSettingsActivity.m546onCreate$lambda1(AppSettingsActivity.this, (String) obj);
            }
        });
        if (bundle == null && (stringExtra = getIntent().getStringExtra("extra_perform_action_on_create")) != null && stringExtra.hashCode() == -1946189773 && stringExtra.equals(ACTION_CHANGE_NUMBER_SUCCESS)) {
            new MaterialAlertDialogBuilder(this).setMessage((CharSequence) getString(R.string.ChangeNumber__your_phone_number_has_changed_to_s, new Object[]{PhoneNumberFormatter.prettyPrint(Recipient.self().requireE164())})).setPositiveButton(R.string.ChangeNumber__okay, (DialogInterface.OnClickListener) null).show();
        }
    }

    /* renamed from: onCreate$lambda-1 */
    public static final void m546onCreate$lambda1(AppSettingsActivity appSettingsActivity, String str) {
        Intrinsics.checkNotNullParameter(appSettingsActivity, "this$0");
        if (Intrinsics.areEqual(str, SettingsValues.THEME)) {
            DynamicTheme.setDefaultDayNightMode(appSettingsActivity);
            appSettingsActivity.recreate();
        } else if (Intrinsics.areEqual(str, SettingsValues.LANGUAGE)) {
            CachedInflater.from(appSettingsActivity).clear();
            appSettingsActivity.wasConfigurationUpdated = true;
            appSettingsActivity.recreate();
            Intent intent = new Intent(appSettingsActivity, KeyCachingService.class);
            intent.setAction(KeyCachingService.LOCALE_CHANGE_EVENT);
            appSettingsActivity.startService(intent);
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        finish();
        startActivity(intent);
    }

    @Override // androidx.appcompat.app.AppCompatActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onSaveInstanceState(Bundle bundle) {
        Intrinsics.checkNotNullParameter(bundle, "outState");
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("app.settings.state.configuration.updated", this.wasConfigurationUpdated);
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsActivity
    protected void onWillFinish() {
        if (this.wasConfigurationUpdated) {
            setResult(MainActivity.RESULT_CONFIG_CHANGED);
        } else {
            setResult(-1);
        }
    }

    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, android.app.Activity
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        getGooglePayResultPublisher().onNext(new DonationPaymentComponent.GooglePayResult(i, i2, intent));
    }

    /* compiled from: AppSettingsActivity.kt */
    @Metadata(d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0006\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\n\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0018\u0010\f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\u000eH\u0002J\u001a\u0010\u000f\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\u0010\u001a\u00020\u0011H\u0007J\u001c\u0010\u0012\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\n\b\u0002\u0010\u0013\u001a\u0004\u0018\u00010\u0004H\u0007J\u0010\u0010\u0014\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0018\u0010\u0015\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0016\u001a\u00020\u0017H\u0007J\u0010\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\u0019\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\u001a\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007J\u0010\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\u001d"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity$Companion;", "", "()V", "ACTION_CHANGE_NUMBER_SUCCESS", "", "backups", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "boost", "changeNumber", "createNotificationProfile", "getIntentForStartLocation", "startLocation", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity$StartLocation;", "help", "startCategoryIndex", "", "home", "action", "manageSubscriptions", "notificationProfileDetails", "profileId", "", "notificationProfiles", "notifications", "privacy", "proxy", "subscriptions", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        @JvmStatic
        public final Intent home(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return home$default(this, context, null, 2, null);
        }

        private Companion() {
        }

        public static /* synthetic */ Intent home$default(Companion companion, Context context, String str, int i, Object obj) {
            if ((i & 2) != 0) {
                str = null;
            }
            return companion.home(context, str);
        }

        @JvmStatic
        public final Intent home(Context context, String str) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intent putExtra = getIntentForStartLocation(context, StartLocation.HOME).putExtra("extra_perform_action_on_create", str);
            Intrinsics.checkNotNullExpressionValue(putExtra, "getIntentForStartLocatio…ACTION_ON_CREATE, action)");
            return putExtra;
        }

        @JvmStatic
        public final Intent backups(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.BACKUPS);
        }

        public static /* synthetic */ Intent help$default(Companion companion, Context context, int i, int i2, Object obj) {
            if ((i2 & 2) != 0) {
                i = 0;
            }
            return companion.help(context, i);
        }

        @JvmStatic
        public final Intent help(Context context, int i) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intent putExtra = getIntentForStartLocation(context, StartLocation.HELP).putExtra(HelpFragment.START_CATEGORY_INDEX, i);
            Intrinsics.checkNotNullExpressionValue(putExtra, "getIntentForStartLocatio…NDEX, startCategoryIndex)");
            return putExtra;
        }

        @JvmStatic
        public final Intent proxy(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.PROXY);
        }

        @JvmStatic
        public final Intent notifications(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.NOTIFICATIONS);
        }

        @JvmStatic
        public final Intent changeNumber(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.CHANGE_NUMBER);
        }

        @JvmStatic
        public final Intent subscriptions(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.SUBSCRIPTIONS);
        }

        @JvmStatic
        public final Intent boost(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.BOOST);
        }

        @JvmStatic
        public final Intent manageSubscriptions(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.MANAGE_SUBSCRIPTIONS);
        }

        @JvmStatic
        public final Intent notificationProfiles(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.NOTIFICATION_PROFILES);
        }

        @JvmStatic
        public final Intent createNotificationProfile(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.CREATE_NOTIFICATION_PROFILE);
        }

        @JvmStatic
        public final Intent privacy(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return getIntentForStartLocation(context, StartLocation.PRIVACY);
        }

        @JvmStatic
        public final Intent notificationProfileDetails(Context context, long j) {
            Intrinsics.checkNotNullParameter(context, "context");
            Bundle bundle = new EditNotificationProfileScheduleFragmentArgs.Builder(j, false).build().toBundle();
            Intrinsics.checkNotNullExpressionValue(bundle, "Builder(profileId, false…ild()\n        .toBundle()");
            Intent putExtra = getIntentForStartLocation(context, StartLocation.NOTIFICATION_PROFILE_DETAILS).putExtra("app.settings.start.arguments", bundle);
            Intrinsics.checkNotNullExpressionValue(putExtra, "getIntentForStartLocatio…ART_ARGUMENTS, arguments)");
            return putExtra;
        }

        private final Intent getIntentForStartLocation(Context context, StartLocation startLocation) {
            Intent putExtra = new Intent(context, AppSettingsActivity.class).putExtra(DSLSettingsActivity.ARG_NAV_GRAPH, R.navigation.app_settings).putExtra("app.settings.start.location", startLocation.getCode());
            Intrinsics.checkNotNullExpressionValue(putExtra, "Intent(context, AppSetti…TION, startLocation.code)");
            return putExtra;
        }
    }

    /* compiled from: AppSettingsActivity.kt */
    @Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0012\b\u0001\u0018\u0000 \u00142\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0014B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000bj\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000fj\u0002\b\u0010j\u0002\b\u0011j\u0002\b\u0012j\u0002\b\u0013¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity$StartLocation;", "", "code", "", "(Ljava/lang/String;II)V", "getCode", "()I", "HOME", "BACKUPS", "HELP", "PROXY", "NOTIFICATIONS", "CHANGE_NUMBER", "SUBSCRIPTIONS", Badge.BOOST_BADGE_ID, "MANAGE_SUBSCRIPTIONS", "NOTIFICATION_PROFILES", "CREATE_NOTIFICATION_PROFILE", "NOTIFICATION_PROFILE_DETAILS", "PRIVACY", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public enum StartLocation {
        HOME(0),
        BACKUPS(1),
        HELP(2),
        PROXY(3),
        NOTIFICATIONS(4),
        CHANGE_NUMBER(5),
        SUBSCRIPTIONS(6),
        BOOST(7),
        MANAGE_SUBSCRIPTIONS(8),
        NOTIFICATION_PROFILES(9),
        CREATE_NOTIFICATION_PROFILE(10),
        NOTIFICATION_PROFILE_DETAILS(11),
        PRIVACY(12);
        
        public static final Companion Companion = new Companion(null);
        private final int code;

        StartLocation(int i) {
            this.code = i;
        }

        public final int getCode() {
            return this.code;
        }

        /* compiled from: AppSettingsActivity.kt */
        @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity$StartLocation$Companion;", "", "()V", "fromCode", "Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity$StartLocation;", "code", "", "(Ljava/lang/Integer;)Lorg/thoughtcrime/securesms/components/settings/app/AppSettingsActivity$StartLocation;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Companion {
            public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
                this();
            }

            private Companion() {
            }

            public final StartLocation fromCode(Integer num) {
                StartLocation startLocation;
                StartLocation[] values = StartLocation.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        startLocation = null;
                        break;
                    }
                    startLocation = values[i];
                    if (num != null && num.intValue() == startLocation.getCode()) {
                        break;
                    }
                    i++;
                }
                return startLocation == null ? StartLocation.HOME : startLocation;
            }
        }
    }
}
