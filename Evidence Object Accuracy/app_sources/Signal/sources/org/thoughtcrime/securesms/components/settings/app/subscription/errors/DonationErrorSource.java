package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.badges.models.Badge;

/* compiled from: DonationErrorSource.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\b\u0001\u0018\u0000 \f2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\fB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0006\u0010\u0005\u001a\u00020\u0003R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000j\u0002\b\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\nj\u0002\b\u000b¨\u0006\r"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "", "code", "", "(Ljava/lang/String;ILjava/lang/String;)V", "serialize", Badge.BOOST_BADGE_ID, "SUBSCRIPTION", Badge.GIFT_BADGE_ID, "GIFT_REDEMPTION", "KEEP_ALIVE", "UNKNOWN", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum DonationErrorSource {
    BOOST("boost"),
    SUBSCRIPTION("subscription"),
    GIFT("gift"),
    GIFT_REDEMPTION("gift-redemption"),
    KEEP_ALIVE("keep-alive"),
    UNKNOWN("unknown");
    
    public static final Companion Companion = new Companion(null);
    private final String code;

    @JvmStatic
    public static final DonationErrorSource deserialize(String str) {
        return Companion.deserialize(str);
    }

    DonationErrorSource(String str) {
        this.code = str;
    }

    public final String serialize() {
        return this.code;
    }

    /* compiled from: DonationErrorSource.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource$Companion;", "", "()V", "deserialize", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/DonationErrorSource;", "code", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final DonationErrorSource deserialize(String str) {
            DonationErrorSource donationErrorSource;
            Intrinsics.checkNotNullParameter(str, "code");
            DonationErrorSource[] values = DonationErrorSource.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    donationErrorSource = null;
                    break;
                }
                donationErrorSource = values[i];
                if (Intrinsics.areEqual(donationErrorSource.code, str)) {
                    break;
                }
                i++;
            }
            return donationErrorSource == null ? DonationErrorSource.UNKNOWN : donationErrorSource;
        }
    }
}
