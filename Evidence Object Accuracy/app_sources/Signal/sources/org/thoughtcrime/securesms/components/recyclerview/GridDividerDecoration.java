package org.thoughtcrime.securesms.components.recyclerview;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: GridDividerDecoration.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0016\u0018\u00002\u00020\u0001B\u0017\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0003¢\u0006\u0002\u0010\u0005J(\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J \u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\tH\u0004J\u001c\u0010\u0012\u001a\u00020\u0007*\u00020\t2\u0006\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u001c\u0010\u0016\u001a\u00020\u0007*\u00020\t2\u0006\u0010\u0017\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u0015H\u0002R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/recyclerview/GridDividerDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "spanCount", "", "space", "(II)V", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "setItemOffsets", "position", "setEnd", NotificationProfileDatabase.NotificationProfileScheduleTable.END, "isRtl", "", "setStart", NotificationProfileDatabase.NotificationProfileScheduleTable.START, "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public class GridDividerDecoration extends RecyclerView.ItemDecoration {
    private final int space;
    private final int spanCount;

    public GridDividerDecoration(int i, int i2) {
        this.spanCount = i;
        this.space = i2;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void getItemOffsets(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(rect, "outRect");
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        setItemOffsets(recyclerView.getChildAdapterPosition(view), view, rect);
    }

    public final void setItemOffsets(int i, View view, Rect rect) {
        Intrinsics.checkNotNullParameter(view, "view");
        Intrinsics.checkNotNullParameter(rect, "outRect");
        int i2 = i % this.spanCount;
        boolean isRtl = ViewUtil.isRtl(view);
        int i3 = this.spanCount;
        int i4 = (i3 - 1) - i2;
        int i5 = this.space;
        setStart(rect, (int) ((((float) i2) / ((float) i3)) * ((float) i5)), isRtl);
        setEnd(rect, (int) ((((float) i4) / ((float) i3)) * ((float) i5)), isRtl);
        rect.bottom = this.space;
    }

    private final void setEnd(Rect rect, int i, boolean z) {
        if (z) {
            rect.left = i;
        } else {
            rect.right = i;
        }
    }

    private final void setStart(Rect rect, int i, boolean z) {
        if (z) {
            rect.right = i;
        } else {
            rect.left = i;
        }
    }
}
