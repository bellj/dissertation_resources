package org.thoughtcrime.securesms.components.webrtc;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.SurfaceTexture;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.View;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.webrtc.EglBase;
import org.webrtc.EglRenderer;
import org.webrtc.GlRectDrawer;
import org.webrtc.RendererCommon;
import org.webrtc.ThreadUtils;
import org.webrtc.VideoFrame;
import org.webrtc.VideoSink;

/* loaded from: classes4.dex */
public class TextureViewRenderer extends TextureView implements TextureView.SurfaceTextureListener, VideoSink, RendererCommon.RendererEvents {
    private static final String TAG = Log.tag(TextureViewRenderer.class);
    private BroadcastVideoSink attachedVideoSink;
    private final SurfaceTextureEglRenderer eglRenderer = new SurfaceTextureEglRenderer(getResourceName());
    private boolean enableFixedSize;
    private boolean isInitialized;
    private Lifecycle lifecycle;
    private RendererCommon.RendererEvents rendererEvents;
    private int rotatedFrameHeight;
    private int rotatedFrameWidth;
    private int surfaceHeight;
    private int surfaceWidth;
    private final RendererCommon.VideoLayoutMeasure videoLayoutMeasure = new RendererCommon.VideoLayoutMeasure();

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public TextureViewRenderer(Context context) {
        super(context);
        setSurfaceTextureListener(this);
    }

    public TextureViewRenderer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setSurfaceTextureListener(this);
    }

    public void init(EglBase eglBase) {
        if (!this.isInitialized) {
            this.isInitialized = true;
            init(eglBase.getEglBaseContext(), null, EglBase.CONFIG_PLAIN, new GlRectDrawer());
        }
    }

    public void init(EglBase.Context context, RendererCommon.RendererEvents rendererEvents, int[] iArr, RendererCommon.GlDrawer glDrawer) {
        ThreadUtils.checkIsOnMainThread();
        this.rendererEvents = rendererEvents;
        this.rotatedFrameWidth = 0;
        this.rotatedFrameHeight = 0;
        this.eglRenderer.init(context, this, iArr, glDrawer);
        Lifecycle activityLifecycle = ViewUtil.getActivityLifecycle(this);
        this.lifecycle = activityLifecycle;
        if (activityLifecycle != null) {
            activityLifecycle.addObserver(new DefaultLifecycleObserver() { // from class: org.thoughtcrime.securesms.components.webrtc.TextureViewRenderer.1
                @Override // androidx.lifecycle.FullLifecycleObserver
                public /* bridge */ /* synthetic */ void onCreate(LifecycleOwner lifecycleOwner) {
                    DefaultLifecycleObserver.CC.$default$onCreate(this, lifecycleOwner);
                }

                @Override // androidx.lifecycle.FullLifecycleObserver
                public /* bridge */ /* synthetic */ void onPause(LifecycleOwner lifecycleOwner) {
                    DefaultLifecycleObserver.CC.$default$onPause(this, lifecycleOwner);
                }

                @Override // androidx.lifecycle.FullLifecycleObserver
                public /* bridge */ /* synthetic */ void onResume(LifecycleOwner lifecycleOwner) {
                    DefaultLifecycleObserver.CC.$default$onResume(this, lifecycleOwner);
                }

                @Override // androidx.lifecycle.FullLifecycleObserver
                public /* bridge */ /* synthetic */ void onStart(LifecycleOwner lifecycleOwner) {
                    DefaultLifecycleObserver.CC.$default$onStart(this, lifecycleOwner);
                }

                @Override // androidx.lifecycle.FullLifecycleObserver
                public /* bridge */ /* synthetic */ void onStop(LifecycleOwner lifecycleOwner) {
                    DefaultLifecycleObserver.CC.$default$onStop(this, lifecycleOwner);
                }

                @Override // androidx.lifecycle.FullLifecycleObserver
                public void onDestroy(LifecycleOwner lifecycleOwner) {
                    TextureViewRenderer.this.release();
                }
            });
        }
    }

    public void attachBroadcastVideoSink(BroadcastVideoSink broadcastVideoSink) {
        if (this.attachedVideoSink != broadcastVideoSink) {
            this.eglRenderer.clearImage();
            BroadcastVideoSink broadcastVideoSink2 = this.attachedVideoSink;
            if (broadcastVideoSink2 != null) {
                broadcastVideoSink2.removeSink(this);
                this.attachedVideoSink.removeRequestingSize(this);
            }
            if (broadcastVideoSink != null) {
                broadcastVideoSink.addSink(this);
                broadcastVideoSink.putRequestingSize(this, new Point(getWidth(), getHeight()));
            } else {
                clearImage();
            }
            this.attachedVideoSink = broadcastVideoSink;
        }
    }

    @Override // android.view.View
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Lifecycle lifecycle = this.lifecycle;
        if (lifecycle == null || lifecycle.getCurrentState() == Lifecycle.State.DESTROYED) {
            release();
        }
    }

    public void release() {
        this.eglRenderer.release();
        BroadcastVideoSink broadcastVideoSink = this.attachedVideoSink;
        if (broadcastVideoSink != null) {
            broadcastVideoSink.removeSink(this);
            this.attachedVideoSink.removeRequestingSize(this);
        }
    }

    public void addFrameListener(EglRenderer.FrameListener frameListener, float f, RendererCommon.GlDrawer glDrawer) {
        this.eglRenderer.addFrameListener(frameListener, f, glDrawer);
    }

    public void addFrameListener(EglRenderer.FrameListener frameListener, float f) {
        this.eglRenderer.addFrameListener(frameListener, f);
    }

    public void removeFrameListener(EglRenderer.FrameListener frameListener) {
        this.eglRenderer.removeFrameListener(frameListener);
    }

    public void setEnableHardwareScaler(boolean z) {
        ThreadUtils.checkIsOnMainThread();
        this.enableFixedSize = z;
        updateSurfaceSize();
    }

    public void setMirror(boolean z) {
        this.eglRenderer.setMirror(z);
    }

    public void setScalingType(RendererCommon.ScalingType scalingType) {
        ThreadUtils.checkIsOnMainThread();
        this.videoLayoutMeasure.setScalingType(scalingType);
        requestLayout();
    }

    public void setScalingType(RendererCommon.ScalingType scalingType, RendererCommon.ScalingType scalingType2) {
        ThreadUtils.checkIsOnMainThread();
        this.videoLayoutMeasure.setScalingType(scalingType, scalingType2);
        requestLayout();
    }

    public void setFpsReduction(float f) {
        this.eglRenderer.setFpsReduction(f);
    }

    public void disableFpsReduction() {
        this.eglRenderer.disableFpsReduction();
    }

    public void pauseVideo() {
        this.eglRenderer.pauseVideo();
    }

    @Override // android.view.View
    protected void onMeasure(int i, int i2) {
        ThreadUtils.checkIsOnMainThread();
        Point measure = this.videoLayoutMeasure.measure(View.MeasureSpec.makeMeasureSpec(TextureView.resolveSizeAndState(0, i, 0), Integer.MIN_VALUE), View.MeasureSpec.makeMeasureSpec(TextureView.resolveSizeAndState(0, i2, 0), Integer.MIN_VALUE), this.rotatedFrameWidth, this.rotatedFrameHeight);
        setMeasuredDimension(measure.x, measure.y);
        BroadcastVideoSink broadcastVideoSink = this.attachedVideoSink;
        if (broadcastVideoSink != null) {
            broadcastVideoSink.putRequestingSize(this, measure);
        }
    }

    @Override // android.view.View
    protected void onLayout(boolean z, int i, int i2, int i3, int i4) {
        ThreadUtils.checkIsOnMainThread();
        this.eglRenderer.setLayoutAspectRatio(((float) (i3 - i)) / ((float) (i4 - i2)));
        updateSurfaceSize();
    }

    private void updateSurfaceSize() {
        ThreadUtils.checkIsOnMainThread();
        if (isAvailable()) {
            if (!this.enableFixedSize || this.rotatedFrameWidth == 0 || this.rotatedFrameHeight == 0 || getWidth() == 0 || getHeight() == 0) {
                this.surfaceHeight = 0;
                this.surfaceWidth = 0;
                getSurfaceTexture().setDefaultBufferSize(getMeasuredWidth(), getMeasuredHeight());
                return;
            }
            float width = ((float) getWidth()) / ((float) getHeight());
            int i = this.rotatedFrameWidth;
            int i2 = this.rotatedFrameHeight;
            if (((float) i) / ((float) i2) > width) {
                i = (int) (((float) i2) * width);
            } else {
                i2 = (int) (((float) i) / width);
            }
            int min = Math.min(getWidth(), i);
            int min2 = Math.min(getHeight(), i2);
            String str = TAG;
            Log.d(str, "updateSurfaceSize. Layout size: " + getWidth() + "x" + getHeight() + ", frame size: " + this.rotatedFrameWidth + "x" + this.rotatedFrameHeight + ", requested surface size: " + min + "x" + min2 + ", old surface size: " + this.surfaceWidth + "x" + this.surfaceHeight);
            if (min != this.surfaceWidth || min2 != this.surfaceHeight) {
                this.surfaceWidth = min;
                this.surfaceHeight = min2;
                getSurfaceTexture().setDefaultBufferSize(min, min2);
            }
        }
    }

    @Override // org.webrtc.RendererCommon.RendererEvents
    public void onFirstFrameRendered() {
        RendererCommon.RendererEvents rendererEvents = this.rendererEvents;
        if (rendererEvents != null) {
            rendererEvents.onFirstFrameRendered();
        }
    }

    @Override // org.webrtc.RendererCommon.RendererEvents
    public void onFrameResolutionChanged(int i, int i2, int i3) {
        RendererCommon.RendererEvents rendererEvents = this.rendererEvents;
        if (rendererEvents != null) {
            rendererEvents.onFrameResolutionChanged(i, i2, i3);
        }
        int i4 = (i3 == 0 || i3 == 180) ? i : i2;
        if (i3 == 0 || i3 == 180) {
            i = i2;
        }
        postOrRun(new Runnable(i4, i) { // from class: org.thoughtcrime.securesms.components.webrtc.TextureViewRenderer$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$1;
            public final /* synthetic */ int f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // java.lang.Runnable
            public final void run() {
                TextureViewRenderer.this.lambda$onFrameResolutionChanged$0(this.f$1, this.f$2);
            }
        });
    }

    public /* synthetic */ void lambda$onFrameResolutionChanged$0(int i, int i2) {
        this.rotatedFrameWidth = i;
        this.rotatedFrameHeight = i2;
        updateSurfaceSize();
        requestLayout();
    }

    @Override // org.webrtc.VideoSink
    public void onFrame(VideoFrame videoFrame) {
        if (isAttachedToWindow()) {
            this.eglRenderer.onFrame(videoFrame);
        }
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i2) {
        ThreadUtils.checkIsOnMainThread();
        this.surfaceWidth = 0;
        this.surfaceHeight = 0;
        updateSurfaceSize();
        this.eglRenderer.onSurfaceTextureAvailable(surfaceTexture, i, i2);
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i2) {
        this.eglRenderer.onSurfaceTextureSizeChanged(surfaceTexture, i, i2);
    }

    @Override // android.view.TextureView.SurfaceTextureListener
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return this.eglRenderer.onSurfaceTextureDestroyed(surfaceTexture);
    }

    private String getResourceName() {
        try {
            return getResources().getResourceEntryName(getId());
        } catch (Resources.NotFoundException unused) {
            return "";
        }
    }

    public void clearImage() {
        this.eglRenderer.clearImage();
    }

    private void postOrRun(Runnable runnable) {
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            runnable.run();
        } else {
            post(runnable);
        }
    }
}
