package org.thoughtcrime.securesms.components.settings.conversation;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda1 implements DialogInterface.OnClickListener {
    public final /* synthetic */ ConversationSettingsFragment f$0;

    public /* synthetic */ ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda1(ConversationSettingsFragment conversationSettingsFragment) {
        this.f$0 = conversationSettingsFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        ConversationSettingsFragment$getConfiguration$1.AnonymousClass8.m1088invoke$lambda0(this.f$0, dialogInterface, i);
    }
}
