package org.thoughtcrime.securesms.components.emoji;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;
import org.thoughtcrime.securesms.util.InsetItemDecoration;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: EmojiItemDecoration.kt */
@Metadata(d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001:\u0001\u000fB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0010"}, d2 = {"Lorg/thoughtcrime/securesms/components/emoji/EmojiItemDecoration;", "Lorg/thoughtcrime/securesms/util/InsetItemDecoration;", "allowVariations", "", "variationsDrawable", "Landroid/graphics/drawable/Drawable;", "(ZLandroid/graphics/drawable/Drawable;)V", "onDrawOver", "", "canvas", "Landroid/graphics/Canvas;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "SetInset", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EmojiItemDecoration extends InsetItemDecoration {
    private final boolean allowVariations;
    private final Drawable variationsDrawable;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public EmojiItemDecoration(boolean z, Drawable drawable) {
        super(new SetInset());
        Intrinsics.checkNotNullParameter(drawable, "variationsDrawable");
        this.allowVariations = z;
        this.variationsDrawable = drawable;
    }

    @Override // androidx.recyclerview.widget.RecyclerView.ItemDecoration
    public void onDrawOver(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        Intrinsics.checkNotNullParameter(canvas, "canvas");
        Intrinsics.checkNotNullParameter(recyclerView, "parent");
        Intrinsics.checkNotNullParameter(state, "state");
        super.onDrawOver(canvas, recyclerView, state);
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        EmojiPageViewGridAdapter emojiPageViewGridAdapter = adapter instanceof EmojiPageViewGridAdapter ? (EmojiPageViewGridAdapter) adapter : null;
        if (this.allowVariations && emojiPageViewGridAdapter != null) {
            int childCount = recyclerView.getChildCount();
            for (int i = 0; i < childCount; i++) {
                View childAt = recyclerView.getChildAt(i);
                Intrinsics.checkNotNullExpressionValue(childAt, "parent.getChildAt(i)");
                int childAdapterPosition = recyclerView.getChildAdapterPosition(childAt);
                if (childAdapterPosition >= 0 && childAdapterPosition <= emojiPageViewGridAdapter.getItemCount()) {
                    MappingModel mappingModel = (MappingModel) emojiPageViewGridAdapter.getCurrentList().get(childAdapterPosition);
                    if ((mappingModel instanceof EmojiPageViewGridAdapter.EmojiModel) && ((EmojiPageViewGridAdapter.EmojiModel) mappingModel).getEmoji().hasMultipleVariations()) {
                        this.variationsDrawable.setBounds(childAt.getRight(), childAt.getBottom() - EmojiItemDecorationKt.EDGE_LENGTH, childAt.getRight() + EmojiItemDecorationKt.EDGE_LENGTH, childAt.getBottom());
                        this.variationsDrawable.draw(canvas);
                    }
                }
            }
        }
    }

    /* compiled from: EmojiItemDecoration.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nH\u0016¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/emoji/EmojiItemDecoration$SetInset;", "Lorg/thoughtcrime/securesms/util/InsetItemDecoration$SetInset;", "()V", "setInset", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class SetInset extends InsetItemDecoration.SetInset {
        @Override // org.thoughtcrime.securesms.util.InsetItemDecoration.SetInset
        public void setInset(Rect rect, View view, RecyclerView recyclerView) {
            int i;
            Intrinsics.checkNotNullParameter(rect, "outRect");
            Intrinsics.checkNotNullParameter(view, "view");
            Intrinsics.checkNotNullParameter(recyclerView, "parent");
            boolean areEqual = Intrinsics.areEqual(view.getClass(), AppCompatTextView.class);
            rect.left = EmojiItemDecorationKt.HORIZONTAL_INSET;
            rect.right = EmojiItemDecorationKt.HORIZONTAL_INSET;
            rect.top = areEqual ? EmojiItemDecorationKt.HEADER_VERTICAL_INSET : EmojiItemDecorationKt.EMOJI_VERTICAL_INSET;
            if (areEqual) {
                i = 0;
            } else {
                i = EmojiItemDecorationKt.EMOJI_VERTICAL_INSET;
            }
            rect.bottom = i;
        }
    }
}
