package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.appcompat.widget.AppCompatImageButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import kotlin.Metadata;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.collections.CollectionsKt__IterablesKt;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.components.menu.ActionItem;
import org.thoughtcrime.securesms.components.menu.SignalContextMenu;
import org.thoughtcrime.securesms.conversation.MessageSendType;
import org.thoughtcrime.securesms.util.ViewUtil;

/* compiled from: SendButton.kt */
@Metadata(d1 = {"\u0000`\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010!\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0010\u0018\u0000 12\u00020\u00012\u00020\u0002:\u000212B\u0017\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¢\u0006\u0002\u0010\u0007J\u000e\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u0016J\u000e\u0010\u001f\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u0010J\u0010\u0010!\u001a\u00020\u00122\u0006\u0010\"\u001a\u00020#H\u0016J\u0018\u0010$\u001a\u00020\u001d2\u0006\u0010%\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u0012H\u0002J\u000e\u0010&\u001a\u00020\u001d2\u0006\u0010'\u001a\u00020\u0012J\u0015\u0010(\u001a\u00020\u001d2\b\u0010)\u001a\u0004\u0018\u00010\r¢\u0006\u0002\u0010*J\u000e\u0010+\u001a\u00020\u001d2\u0006\u0010 \u001a\u00020\u0010J\u000e\u0010,\u001a\u00020\u001d2\u0006\u0010-\u001a\u00020\u0018J\u0010\u0010.\u001a\u00020\u001d2\b\u0010/\u001a\u0004\u0018\u00010\tJ\u0006\u00100\u001a\u00020\u001dR\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u000e¢\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\b\u0012\u0004\u0012\u00020\t0\u000bX\u000e¢\u0006\u0002\n\u0000R\u0012\u0010\f\u001a\u0004\u0018\u00010\rX\u000e¢\u0006\u0004\n\u0002\u0010\u000eR\u000e\u0010\u000f\u001a\u00020\u0010X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0011\u001a\u00020\u00128F¢\u0006\u0006\u001a\u0004\b\u0011\u0010\u0013R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00160\u0015X\u0004¢\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u000e¢\u0006\u0002\n\u0000R\u0011\u0010\u0019\u001a\u00020\t8F¢\u0006\u0006\u001a\u0004\b\u001a\u0010\u001b¨\u00063"}, d2 = {"Lorg/thoughtcrime/securesms/components/SendButton;", "Landroidx/appcompat/widget/AppCompatImageButton;", "Landroid/view/View$OnLongClickListener;", "context", "Landroid/content/Context;", "attributeSet", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "activeMessageSendType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "availableSendTypes", "", "defaultSubscriptionId", "", "Ljava/lang/Integer;", "defaultTransportType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType$TransportType;", "isManualSelection", "", "()Z", "listeners", "", "Lorg/thoughtcrime/securesms/components/SendButton$SendTypeChangedListener;", "popupContainer", "Landroid/view/ViewGroup;", "selectedSendType", "getSelectedSendType", "()Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "addOnSelectionChangedListener", "", "listener", "disableTransportType", "type", "onLongClick", "v", "Landroid/view/View;", "onSelectionChanged", "newType", "resetAvailableTransports", "isMediaMessage", "setDefaultSubscriptionId", "subscriptionId", "(Ljava/lang/Integer;)V", "setDefaultTransport", "setPopupContainer", "container", "setSendType", "sendType", "triggerSelectedChangedEvent", "Companion", "SendTypeChangedListener", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SendButton extends AppCompatImageButton implements View.OnLongClickListener {
    public static final Companion Companion = new Companion(null);
    private static final String TAG = Log.tag(SendButton.class);
    private MessageSendType activeMessageSendType;
    private List<? extends MessageSendType> availableSendTypes;
    private Integer defaultSubscriptionId;
    private MessageSendType.TransportType defaultTransportType;
    private final List<SendTypeChangedListener> listeners = new CopyOnWriteArrayList();
    private ViewGroup popupContainer;

    /* compiled from: SendButton.kt */
    @Metadata(d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/SendButton$SendTypeChangedListener;", "", "onSendTypeChanged", "", "newType", "Lorg/thoughtcrime/securesms/conversation/MessageSendType;", "manuallySelected", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface SendTypeChangedListener {
        void onSendTypeChanged(MessageSendType messageSendType, boolean z);
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SendButton(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        Intrinsics.checkNotNullParameter(context, "context");
        this.availableSendTypes = MessageSendType.Companion.getAllAvailable(context, false);
        this.defaultTransportType = MessageSendType.TransportType.SMS;
        setOnLongClickListener(this);
        ViewUtil.mirrorIfRtl(this, getContext());
    }

    /* compiled from: SendButton.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u0016\u0010\u0003\u001a\n \u0005*\u0004\u0018\u00010\u00040\u0004X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0006"}, d2 = {"Lorg/thoughtcrime/securesms/components/SendButton$Companion;", "", "()V", "TAG", "", "kotlin.jvm.PlatformType", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }

    public final boolean isManualSelection() {
        return this.activeMessageSendType != null;
    }

    public final MessageSendType getSelectedSendType() {
        Object obj;
        MessageSendType messageSendType = this.activeMessageSendType;
        if (messageSendType != null) {
            return messageSendType;
        }
        if (this.defaultTransportType == MessageSendType.TransportType.SMS) {
            for (MessageSendType messageSendType2 : this.availableSendTypes) {
                if (messageSendType2.usesSmsTransport() && (this.defaultSubscriptionId == null || Intrinsics.areEqual(messageSendType2.getSimSubscriptionId(), this.defaultSubscriptionId))) {
                    return messageSendType2;
                }
            }
        }
        for (MessageSendType messageSendType3 : this.availableSendTypes) {
            if (messageSendType3.getTransportType() == this.defaultTransportType) {
                return messageSendType3;
            }
        }
        String str = TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("No options of default type! Resetting. DefaultTransportType: ");
        sb.append(this.defaultTransportType);
        sb.append(", AllAvailable: ");
        List<? extends MessageSendType> list = this.availableSendTypes;
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list, 10));
        for (MessageSendType messageSendType4 : list) {
            arrayList.add(messageSendType4.getTransportType());
        }
        sb.append(arrayList);
        Log.w(str, sb.toString());
        Iterator<T> it = this.availableSendTypes.iterator();
        while (true) {
            if (!it.hasNext()) {
                obj = null;
                break;
            }
            obj = it.next();
            if (((MessageSendType) obj).usesSignalTransport()) {
                break;
            }
        }
        MessageSendType messageSendType5 = (MessageSendType) obj;
        if (messageSendType5 != null) {
            String str2 = TAG;
            StringBuilder sb2 = new StringBuilder();
            sb2.append("No options of default type, but Signal type is available. Switching. DefaultTransportType: ");
            sb2.append(this.defaultTransportType);
            sb2.append(", AllAvailable: ");
            List<? extends MessageSendType> list2 = this.availableSendTypes;
            ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list2, 10));
            for (MessageSendType messageSendType6 : list2) {
                arrayList2.add(messageSendType6.getTransportType());
            }
            sb2.append(arrayList2);
            Log.w(str2, sb2.toString());
            this.defaultTransportType = MessageSendType.TransportType.SIGNAL;
            onSelectionChanged(messageSendType5, false);
            return messageSendType5;
        } else if (this.availableSendTypes.isEmpty()) {
            Log.w(TAG, "No send types available at all! Enabling the Signal transport.");
            this.defaultTransportType = MessageSendType.TransportType.SIGNAL;
            MessageSendType.SignalMessageSendType signalMessageSendType = MessageSendType.SignalMessageSendType.INSTANCE;
            this.availableSendTypes = CollectionsKt__CollectionsJVMKt.listOf(signalMessageSendType);
            onSelectionChanged(signalMessageSendType, false);
            return signalMessageSendType;
        } else {
            StringBuilder sb3 = new StringBuilder();
            sb3.append("No options of default type! DefaultTransportType: ");
            sb3.append(this.defaultTransportType);
            sb3.append(", AllAvailable: ");
            List<? extends MessageSendType> list3 = this.availableSendTypes;
            ArrayList arrayList3 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(list3, 10));
            for (MessageSendType messageSendType7 : list3) {
                arrayList3.add(messageSendType7.getTransportType());
            }
            sb3.append(arrayList3);
            throw new AssertionError(sb3.toString());
        }
    }

    public final void addOnSelectionChangedListener(SendTypeChangedListener sendTypeChangedListener) {
        Intrinsics.checkNotNullParameter(sendTypeChangedListener, "listener");
        this.listeners.add(sendTypeChangedListener);
    }

    public final void triggerSelectedChangedEvent() {
        onSelectionChanged(getSelectedSendType(), false);
    }

    public final void resetAvailableTransports(boolean z) {
        MessageSendType.Companion companion = MessageSendType.Companion;
        Context context = getContext();
        Intrinsics.checkNotNullExpressionValue(context, "context");
        this.availableSendTypes = companion.getAllAvailable(context, z);
        this.activeMessageSendType = null;
        this.defaultTransportType = MessageSendType.TransportType.SMS;
        this.defaultSubscriptionId = null;
        onSelectionChanged(getSelectedSendType(), false);
    }

    public final void disableTransportType(MessageSendType.TransportType transportType) {
        Intrinsics.checkNotNullParameter(transportType, "type");
        List<? extends MessageSendType> list = this.availableSendTypes;
        ArrayList arrayList = new ArrayList();
        for (Object obj : list) {
            if (!(((MessageSendType) obj).getTransportType() == transportType)) {
                arrayList.add(obj);
            }
        }
        this.availableSendTypes = arrayList;
    }

    public final void setDefaultTransport(MessageSendType.TransportType transportType) {
        Intrinsics.checkNotNullParameter(transportType, "type");
        if (this.defaultTransportType != transportType) {
            this.defaultTransportType = transportType;
            onSelectionChanged(getSelectedSendType(), false);
        }
    }

    public final void setSendType(MessageSendType messageSendType) {
        if (!Intrinsics.areEqual(this.activeMessageSendType, messageSendType)) {
            this.activeMessageSendType = messageSendType;
            onSelectionChanged(getSelectedSendType(), true);
        }
    }

    public final void setDefaultSubscriptionId(Integer num) {
        if (!Intrinsics.areEqual(this.defaultSubscriptionId, num)) {
            this.defaultSubscriptionId = num;
            onSelectionChanged(getSelectedSendType(), false);
        }
    }

    public final void setPopupContainer(ViewGroup viewGroup) {
        Intrinsics.checkNotNullParameter(viewGroup, "container");
        this.popupContainer = viewGroup;
    }

    private final void onSelectionChanged(MessageSendType messageSendType, boolean z) {
        setImageResource(messageSendType.getButtonDrawableRes());
        setContentDescription(getContext().getString(messageSendType.getTitleRes()));
        for (SendTypeChangedListener sendTypeChangedListener : this.listeners) {
            sendTypeChangedListener.onSendTypeChanged(messageSendType, z);
        }
    }

    @Override // android.view.View.OnLongClickListener
    public boolean onLongClick(View view) {
        Intrinsics.checkNotNullParameter(view, "v");
        if (!isEnabled() || this.availableSendTypes.size() == 1) {
            return false;
        }
        MessageSendType selectedSendType = getSelectedSendType();
        List<? extends MessageSendType> list = this.availableSendTypes;
        ArrayList<MessageSendType> arrayList = new ArrayList();
        for (Object obj : list) {
            if (!Intrinsics.areEqual((MessageSendType) obj, selectedSendType)) {
                arrayList.add(obj);
            }
        }
        ArrayList arrayList2 = new ArrayList(CollectionsKt__IterablesKt.collectionSizeOrDefault(arrayList, 10));
        for (MessageSendType messageSendType : arrayList) {
            int menuDrawableRes = messageSendType.getMenuDrawableRes();
            Context context = getContext();
            Intrinsics.checkNotNullExpressionValue(context, "context");
            arrayList2.add(new ActionItem(menuDrawableRes, messageSendType.getTitle(context), 0, new Runnable(messageSendType) { // from class: org.thoughtcrime.securesms.components.SendButton$$ExternalSyntheticLambda0
                public final /* synthetic */ MessageSendType f$1;

                {
                    this.f$1 = r2;
                }

                @Override // java.lang.Runnable
                public final void run() {
                    SendButton.m489onLongClick$lambda8$lambda7(SendButton.this, this.f$1);
                }
            }, 4, null));
        }
        ViewParent parent = getParent();
        if (parent != null) {
            ViewGroup viewGroup = this.popupContainer;
            Intrinsics.checkNotNull(viewGroup);
            new SignalContextMenu.Builder((View) parent, viewGroup).preferredVerticalPosition(SignalContextMenu.VerticalPosition.ABOVE).offsetY(ViewUtil.dpToPx(8)).show(arrayList2);
            return true;
        }
        throw new NullPointerException("null cannot be cast to non-null type android.view.View");
    }

    /* renamed from: onLongClick$lambda-8$lambda-7 */
    public static final void m489onLongClick$lambda8$lambda7(SendButton sendButton, MessageSendType messageSendType) {
        Intrinsics.checkNotNullParameter(sendButton, "this$0");
        Intrinsics.checkNotNullParameter(messageSendType, "$option");
        sendButton.setSendType(messageSendType);
    }
}
