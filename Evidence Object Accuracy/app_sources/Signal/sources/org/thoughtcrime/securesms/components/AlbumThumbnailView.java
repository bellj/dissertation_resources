package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.TextView;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.mms.SlidesClickedListener;
import org.thoughtcrime.securesms.util.views.Stub;

/* loaded from: classes4.dex */
public class AlbumThumbnailView extends FrameLayout {
    private ViewGroup albumCellContainer;
    private int currentSizeClass;
    private final View.OnLongClickListener defaultLongClickListener = new AlbumThumbnailView$$ExternalSyntheticLambda1(this);
    private final SlideClickListener defaultThumbnailClickListener = new AlbumThumbnailView$$ExternalSyntheticLambda0(this);
    private SlidesClickedListener downloadClickListener;
    private SlideClickListener thumbnailClickListener;
    private Stub<TransferControlView> transferControls;

    public /* synthetic */ void lambda$new$0(View view, Slide slide) {
        SlideClickListener slideClickListener = this.thumbnailClickListener;
        if (slideClickListener != null) {
            slideClickListener.onClick(view, slide);
        }
    }

    public /* synthetic */ boolean lambda$new$1(View view) {
        return performLongClick();
    }

    public AlbumThumbnailView(Context context) {
        super(context);
        initialize();
    }

    public AlbumThumbnailView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        initialize();
    }

    private void initialize() {
        FrameLayout.inflate(getContext(), R.layout.album_thumbnail_view, this);
        this.albumCellContainer = (ViewGroup) findViewById(R.id.album_cell_container);
        this.transferControls = new Stub<>((ViewStub) findViewById(R.id.album_transfer_controls_stub));
    }

    public void setSlides(GlideRequests glideRequests, List<Slide> list, boolean z) {
        if (list.size() >= 2) {
            if (z) {
                this.transferControls.get().setShowDownloadText(true);
                this.transferControls.get().setSlides(list);
                this.transferControls.get().setDownloadClickListener(new View.OnClickListener(list) { // from class: org.thoughtcrime.securesms.components.AlbumThumbnailView$$ExternalSyntheticLambda2
                    public final /* synthetic */ List f$1;

                    {
                        this.f$1 = r2;
                    }

                    @Override // android.view.View.OnClickListener
                    public final void onClick(View view) {
                        AlbumThumbnailView.$r8$lambda$WEREpPIiZsNxUlVDU2boRXeoWJM(AlbumThumbnailView.this, this.f$1, view);
                    }
                });
            } else if (this.transferControls.resolved()) {
                this.transferControls.get().setVisibility(8);
            }
            int sizeClass = sizeClass(list.size());
            if (sizeClass != this.currentSizeClass) {
                inflateLayout(sizeClass);
                this.currentSizeClass = sizeClass;
            }
            showSlides(glideRequests, list);
            return;
        }
        throw new IllegalStateException("Provided less than two slides.");
    }

    public /* synthetic */ void lambda$setSlides$2(List list, View view) {
        SlidesClickedListener slidesClickedListener = this.downloadClickListener;
        if (slidesClickedListener != null) {
            slidesClickedListener.onClick(view, list);
        }
    }

    public void setCellBackgroundColor(int i) {
        ViewGroup viewGroup = (ViewGroup) findViewById(R.id.album_thumbnail_root);
        if (viewGroup != null) {
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                viewGroup.getChildAt(i2).setBackgroundColor(i);
            }
        }
    }

    public void setThumbnailClickListener(SlideClickListener slideClickListener) {
        this.thumbnailClickListener = slideClickListener;
    }

    public void setDownloadClickListener(SlidesClickedListener slidesClickedListener) {
        this.downloadClickListener = slidesClickedListener;
    }

    private void inflateLayout(int i) {
        this.albumCellContainer.removeAllViews();
        if (i == 2) {
            FrameLayout.inflate(getContext(), R.layout.album_thumbnail_2, this.albumCellContainer);
        } else if (i == 3) {
            FrameLayout.inflate(getContext(), R.layout.album_thumbnail_3, this.albumCellContainer);
        } else if (i == 4) {
            FrameLayout.inflate(getContext(), R.layout.album_thumbnail_4, this.albumCellContainer);
        } else if (i != 5) {
            FrameLayout.inflate(getContext(), R.layout.album_thumbnail_many, this.albumCellContainer);
        } else {
            FrameLayout.inflate(getContext(), R.layout.album_thumbnail_5, this.albumCellContainer);
        }
    }

    private void showSlides(GlideRequests glideRequests, List<Slide> list) {
        setSlide(glideRequests, list.get(0), R.id.album_cell_1);
        setSlide(glideRequests, list.get(1), R.id.album_cell_2);
        if (list.size() >= 3) {
            setSlide(glideRequests, list.get(2), R.id.album_cell_3);
        }
        if (list.size() >= 4) {
            setSlide(glideRequests, list.get(3), R.id.album_cell_4);
        }
        if (list.size() >= 5) {
            setSlide(glideRequests, list.get(4), R.id.album_cell_5);
        }
        if (list.size() > 5) {
            ((TextView) findViewById(R.id.album_cell_overflow_text)).setText(getContext().getString(R.string.AlbumThumbnailView_plus, Integer.valueOf(list.size() - 5)));
        }
    }

    private void setSlide(GlideRequests glideRequests, Slide slide, int i) {
        ThumbnailView thumbnailView = (ThumbnailView) findViewById(i);
        thumbnailView.setImageResource(glideRequests, slide, false, false);
        thumbnailView.setThumbnailClickListener(this.defaultThumbnailClickListener);
        thumbnailView.setOnLongClickListener(this.defaultLongClickListener);
    }

    private int sizeClass(int i) {
        return Math.min(i, 6);
    }
}
