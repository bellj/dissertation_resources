package org.thoughtcrime.securesms.components.settings.conversation;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class InternalConversationSettingsFragmentArgs {
    private final HashMap arguments;

    private InternalConversationSettingsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private InternalConversationSettingsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static InternalConversationSettingsFragmentArgs fromBundle(Bundle bundle) {
        InternalConversationSettingsFragmentArgs internalConversationSettingsFragmentArgs = new InternalConversationSettingsFragmentArgs();
        bundle.setClassLoader(InternalConversationSettingsFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("recipient_id")) {
            throw new IllegalArgumentException("Required argument \"recipient_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(RecipientId.class) || Serializable.class.isAssignableFrom(RecipientId.class)) {
            RecipientId recipientId = (RecipientId) bundle.get("recipient_id");
            if (recipientId != null) {
                internalConversationSettingsFragmentArgs.arguments.put("recipient_id", recipientId);
                return internalConversationSettingsFragmentArgs;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        } else {
            throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public RecipientId getRecipientId() {
        return (RecipientId) this.arguments.get("recipient_id");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("recipient_id")) {
            RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
            if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
            } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
            } else {
                throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        InternalConversationSettingsFragmentArgs internalConversationSettingsFragmentArgs = (InternalConversationSettingsFragmentArgs) obj;
        if (this.arguments.containsKey("recipient_id") != internalConversationSettingsFragmentArgs.arguments.containsKey("recipient_id")) {
            return false;
        }
        return getRecipientId() == null ? internalConversationSettingsFragmentArgs.getRecipientId() == null : getRecipientId().equals(internalConversationSettingsFragmentArgs.getRecipientId());
    }

    public int hashCode() {
        return 31 + (getRecipientId() != null ? getRecipientId().hashCode() : 0);
    }

    public String toString() {
        return "InternalConversationSettingsFragmentArgs{recipientId=" + getRecipientId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(InternalConversationSettingsFragmentArgs internalConversationSettingsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(internalConversationSettingsFragmentArgs.arguments);
        }

        public Builder(RecipientId recipientId) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            if (recipientId != null) {
                hashMap.put("recipient_id", recipientId);
                return;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        public InternalConversationSettingsFragmentArgs build() {
            return new InternalConversationSettingsFragmentArgs(this.arguments);
        }

        public Builder setRecipientId(RecipientId recipientId) {
            if (recipientId != null) {
                this.arguments.put("recipient_id", recipientId);
                return this;
            }
            throw new IllegalArgumentException("Argument \"recipient_id\" is marked as non-null but was passed a null value.");
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }
    }
}
