package org.thoughtcrime.securesms.components.settings.app;

import androidx.navigation.ActionOnlyNavDirections;
import androidx.navigation.NavDirections;
import org.thoughtcrime.securesms.AppSettingsDirections;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public class AppSettingsFragmentDirections {
    private AppSettingsFragmentDirections() {
    }

    public static NavDirections actionAppSettingsFragmentToAccountSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_accountSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToDeviceActivity() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_deviceActivity);
    }

    public static NavDirections actionAppSettingsFragmentToPaymentsActivity() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_paymentsActivity);
    }

    public static NavDirections actionAppSettingsFragmentToAppearanceSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_appearanceSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToChatsSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_chatsSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToNotificationsSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_notificationsSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToPrivacySettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_privacySettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToDataAndStorageSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_dataAndStorageSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToManageProfileActivity() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_manageProfileActivity);
    }

    public static NavDirections actionAppSettingsFragmentToHelpSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_helpSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToInviteActivity() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_inviteActivity);
    }

    public static NavDirections actionAppSettingsFragmentToInternalSettingsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_internalSettingsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToManageDonationsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_manageDonationsFragment);
    }

    public static NavDirections actionAppSettingsFragmentToSubscribeFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_subscribeFragment);
    }

    public static NavDirections actionAppSettingsFragmentToBoostsFragment() {
        return new ActionOnlyNavDirections(R.id.action_appSettingsFragment_to_boostsFragment);
    }

    public static NavDirections actionDirectToBackupsPreferenceFragment() {
        return AppSettingsDirections.actionDirectToBackupsPreferenceFragment();
    }

    public static AppSettingsDirections.ActionDirectToHelpFragment actionDirectToHelpFragment() {
        return AppSettingsDirections.actionDirectToHelpFragment();
    }

    public static NavDirections actionDirectToEditProxyFragment() {
        return AppSettingsDirections.actionDirectToEditProxyFragment();
    }

    public static NavDirections actionDirectToNotificationsSettingsFragment() {
        return AppSettingsDirections.actionDirectToNotificationsSettingsFragment();
    }

    public static NavDirections actionDirectToChangeNumberFragment() {
        return AppSettingsDirections.actionDirectToChangeNumberFragment();
    }

    public static NavDirections actionDirectToSubscriptions() {
        return AppSettingsDirections.actionDirectToSubscriptions();
    }

    public static NavDirections actionDirectToManageDonations() {
        return AppSettingsDirections.actionDirectToManageDonations();
    }

    public static NavDirections actionDirectToNotificationProfiles() {
        return AppSettingsDirections.actionDirectToNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToCreateNotificationProfiles actionDirectToCreateNotificationProfiles() {
        return AppSettingsDirections.actionDirectToCreateNotificationProfiles();
    }

    public static AppSettingsDirections.ActionDirectToNotificationProfileDetails actionDirectToNotificationProfileDetails(long j) {
        return AppSettingsDirections.actionDirectToNotificationProfileDetails(j);
    }

    public static NavDirections actionDirectToPrivacy() {
        return AppSettingsDirections.actionDirectToPrivacy();
    }
}
