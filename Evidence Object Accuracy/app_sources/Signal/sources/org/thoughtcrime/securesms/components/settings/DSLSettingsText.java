package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.view.View;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.collections.CollectionsKt__CollectionsJVMKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.SpanUtil;

/* compiled from: DSLSettingsText.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u0000 \u00112\u00020\u0001:\u000b\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017B\u0007\b\u0004¢\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH$J\u000e\u0010\f\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u0018\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X¤\u0004¢\u0006\u0006\u001a\u0004\b\u0006\u0010\u0007\u0001\u0002\u0018\u0019¨\u0006\u001a"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "", "()V", "modifiers", "", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "getModifiers", "()Ljava/util/List;", "getCharSequence", "", "context", "Landroid/content/Context;", "resolve", "Body1BoldModifier", "BoldModifier", "CenterModifier", "ColorModifier", "Companion", "FromCharSequence", "FromResource", "LearnMoreModifier", "Modifier", "TextAppearanceModifier", "TitleLargeModifier", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$FromResource;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$FromCharSequence;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DSLSettingsText {
    public static final Companion Companion = new Companion(null);

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0003H&¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "", "modify", "", "context", "Landroid/content/Context;", "charSequence", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface Modifier {
        CharSequence modify(Context context, CharSequence charSequence);
    }

    public /* synthetic */ DSLSettingsText(DefaultConstructorMarker defaultConstructorMarker) {
        this();
    }

    protected abstract CharSequence getCharSequence(Context context);

    protected abstract List<Modifier> getModifiers();

    private DSLSettingsText() {
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\n\u001a\u00020\u0003HÂ\u0003J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÄ\u0003J#\u0010\f\u001a\u00020\u00002\b\b\u0003\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014H\u0014J\t\u0010\u0015\u001a\u00020\u0003HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$FromResource;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "stringId", "", "modifiers", "", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "(ILjava/util/List;)V", "getModifiers", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "", "getCharSequence", "", "context", "Landroid/content/Context;", "hashCode", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FromResource extends DSLSettingsText {
        private final List<Modifier> modifiers;
        private final int stringId;

        private final int component1() {
            return this.stringId;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.DSLSettingsText$FromResource */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ FromResource copy$default(FromResource fromResource, int i, List list, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                i = fromResource.stringId;
            }
            if ((i2 & 2) != 0) {
                list = fromResource.getModifiers();
            }
            return fromResource.copy(i, list);
        }

        protected final List<Modifier> component2() {
            return getModifiers();
        }

        public final FromResource copy(int i, List<? extends Modifier> list) {
            Intrinsics.checkNotNullParameter(list, "modifiers");
            return new FromResource(i, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FromResource)) {
                return false;
            }
            FromResource fromResource = (FromResource) obj;
            return this.stringId == fromResource.stringId && Intrinsics.areEqual(getModifiers(), fromResource.getModifiers());
        }

        public int hashCode() {
            return (this.stringId * 31) + getModifiers().hashCode();
        }

        public String toString() {
            return "FromResource(stringId=" + this.stringId + ", modifiers=" + getModifiers() + ')';
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText
        protected List<Modifier> getModifiers() {
            return this.modifiers;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public FromResource(int i, List<? extends Modifier> list) {
            super(null);
            Intrinsics.checkNotNullParameter(list, "modifiers");
            this.stringId = i;
            this.modifiers = list;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText
        protected CharSequence getCharSequence(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            String string = context.getString(this.stringId);
            Intrinsics.checkNotNullExpressionValue(string, "context.getString(stringId)");
            return string;
        }
    }

    /* compiled from: DSLSettingsText.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001b\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\n\u001a\u00020\u0003HÂ\u0003J\u000f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÄ\u0003J#\u0010\f\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005HÆ\u0001J\u0013\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\u0010\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0013H\u0014J\t\u0010\u0014\u001a\u00020\u0015HÖ\u0001J\t\u0010\u0016\u001a\u00020\u0017HÖ\u0001R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u001a\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$FromCharSequence;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "charSequence", "", "modifiers", "", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "(Ljava/lang/CharSequence;Ljava/util/List;)V", "getModifiers", "()Ljava/util/List;", "component1", "component2", "copy", "equals", "", "other", "", "getCharSequence", "context", "Landroid/content/Context;", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class FromCharSequence extends DSLSettingsText {
        private final CharSequence charSequence;
        private final List<Modifier> modifiers;

        private final CharSequence component1() {
            return this.charSequence;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.DSLSettingsText$FromCharSequence */
        /* JADX WARN: Multi-variable type inference failed */
        public static /* synthetic */ FromCharSequence copy$default(FromCharSequence fromCharSequence, CharSequence charSequence, List list, int i, Object obj) {
            if ((i & 1) != 0) {
                charSequence = fromCharSequence.charSequence;
            }
            if ((i & 2) != 0) {
                list = fromCharSequence.getModifiers();
            }
            return fromCharSequence.copy(charSequence, list);
        }

        protected final List<Modifier> component2() {
            return getModifiers();
        }

        public final FromCharSequence copy(CharSequence charSequence, List<? extends Modifier> list) {
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            Intrinsics.checkNotNullParameter(list, "modifiers");
            return new FromCharSequence(charSequence, list);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof FromCharSequence)) {
                return false;
            }
            FromCharSequence fromCharSequence = (FromCharSequence) obj;
            return Intrinsics.areEqual(this.charSequence, fromCharSequence.charSequence) && Intrinsics.areEqual(getModifiers(), fromCharSequence.getModifiers());
        }

        public int hashCode() {
            return (this.charSequence.hashCode() * 31) + getModifiers().hashCode();
        }

        public String toString() {
            return "FromCharSequence(charSequence=" + ((Object) this.charSequence) + ", modifiers=" + getModifiers() + ')';
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText
        protected List<Modifier> getModifiers() {
            return this.modifiers;
        }

        /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: java.util.List<? extends org.thoughtcrime.securesms.components.settings.DSLSettingsText$Modifier> */
        /* JADX WARN: Multi-variable type inference failed */
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        public FromCharSequence(CharSequence charSequence, List<? extends Modifier> list) {
            super(null);
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            Intrinsics.checkNotNullParameter(list, "modifiers");
            this.charSequence = charSequence;
            this.modifiers = list;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText
        protected CharSequence getCharSequence(Context context) {
            Intrinsics.checkNotNullParameter(context, "context");
            return this.charSequence;
        }
    }

    public final CharSequence resolve(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        CharSequence charSequence = getCharSequence(context);
        for (Modifier modifier : getModifiers()) {
            charSequence = modifier.modify(context, charSequence);
        }
        return charSequence;
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0000\n\u0002\u0010\u0011\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J'\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0012\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b\"\u00020\t¢\u0006\u0002\u0010\nJ)\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\f2\u0012\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b\"\u00020\t¢\u0006\u0002\u0010\rJ\u001a\u0010\u0003\u001a\u00020\u00042\b\b\u0001\u0010\u000b\u001a\u00020\f2\b\b\u0001\u0010\u000e\u001a\u00020\f¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Companion;", "", "()V", "from", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "charSequence", "", "modifiers", "", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "(Ljava/lang/CharSequence;[Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;)Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "stringId", "", "(I[Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;)Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "textColor", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final DSLSettingsText from(int i, int i2) {
            return new FromResource(i, CollectionsKt__CollectionsJVMKt.listOf(new ColorModifier(i2)));
        }

        public final DSLSettingsText from(int i, Modifier... modifierArr) {
            Intrinsics.checkNotNullParameter(modifierArr, "modifiers");
            return new FromResource(i, ArraysKt___ArraysKt.toList(modifierArr));
        }

        public final DSLSettingsText from(CharSequence charSequence, Modifier... modifierArr) {
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            Intrinsics.checkNotNullParameter(modifierArr, "modifiers");
            return new FromCharSequence(charSequence, ArraysKt___ArraysKt.toList(modifierArr));
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0002J\b\u0010\t\u001a\u00020\u0003H\u0016J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$ColorModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "textColor", "", "(I)V", "equals", "", "other", "", "hashCode", "modify", "", "context", "Landroid/content/Context;", "charSequence", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class ColorModifier implements Modifier {
        private final int textColor;

        public ColorModifier(int i) {
            this.textColor = i;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier
        public CharSequence modify(Context context, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            CharSequence color = SpanUtil.color(this.textColor, charSequence);
            Intrinsics.checkNotNullExpressionValue(color, "color(textColor, charSequence)");
            return color;
        }

        public boolean equals(Object obj) {
            ColorModifier colorModifier = obj instanceof ColorModifier ? (ColorModifier) obj : null;
            return colorModifier != null && this.textColor == colorModifier.textColor;
        }

        public int hashCode() {
            return this.textColor;
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$CenterModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "()V", "modify", "", "context", "Landroid/content/Context;", "charSequence", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class CenterModifier implements Modifier {
        public static final CenterModifier INSTANCE = new CenterModifier();

        private CenterModifier() {
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier
        public CharSequence modify(Context context, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            CharSequence center = SpanUtil.center(charSequence);
            Intrinsics.checkNotNullExpressionValue(center, "center(charSequence)");
            return center;
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$TitleLargeModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$TextAppearanceModifier;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class TitleLargeModifier extends TextAppearanceModifier {
        public static final TitleLargeModifier INSTANCE = new TitleLargeModifier();

        private TitleLargeModifier() {
            super(R.style.Signal_Text_TitleLarge);
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Body1BoldModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$TextAppearanceModifier;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Body1BoldModifier extends TextAppearanceModifier {
        public static final Body1BoldModifier INSTANCE = new Body1BoldModifier();

        private Body1BoldModifier() {
            super(R.style.TextAppearance_Signal_Body1_Bold);
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J\u0013\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0002J\b\u0010\t\u001a\u00020\u0003H\u0016J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$TextAppearanceModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "textAppearance", "", "(I)V", "equals", "", "other", "", "hashCode", "modify", "", "context", "Landroid/content/Context;", "charSequence", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static class TextAppearanceModifier implements Modifier {
        private final int textAppearance;

        public TextAppearanceModifier(int i) {
            this.textAppearance = i;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier
        public CharSequence modify(Context context, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            CharSequence textAppearance = SpanUtil.textAppearance(context, this.textAppearance, charSequence);
            Intrinsics.checkNotNullExpressionValue(textAppearance, "textAppearance(context, …Appearance, charSequence)");
            return textAppearance;
        }

        public boolean equals(Object obj) {
            TextAppearanceModifier textAppearanceModifier = obj instanceof TextAppearanceModifier ? (TextAppearanceModifier) obj : null;
            return textAppearanceModifier != null && this.textAppearance == textAppearanceModifier.textAppearance;
        }

        public int hashCode() {
            return this.textAppearance;
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004H\u0016¨\u0006\b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$BoldModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "()V", "modify", "", "context", "Landroid/content/Context;", "charSequence", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class BoldModifier implements Modifier {
        public static final BoldModifier INSTANCE = new BoldModifier();

        private BoldModifier() {
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier
        public CharSequence modify(Context context, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            CharSequence bold = SpanUtil.bold(charSequence);
            Intrinsics.checkNotNullExpressionValue(bold, "bold(charSequence)");
            return bold;
        }
    }

    /* compiled from: DSLSettingsText.kt */
    @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\r\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u001d\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\u0002\u0010\u0007J\u0018\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000bH\u0016R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\t¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$LearnMoreModifier;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText$Modifier;", "learnMoreColor", "", "onClick", "Lkotlin/Function0;", "", "(ILkotlin/jvm/functions/Function0;)V", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "modify", "", "context", "Landroid/content/Context;", "charSequence", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class LearnMoreModifier implements Modifier {
        private final int learnMoreColor;
        private final Function0<Unit> onClick;

        public LearnMoreModifier(int i, Function0<Unit> function0) {
            Intrinsics.checkNotNullParameter(function0, "onClick");
            this.learnMoreColor = i;
            this.onClick = function0;
        }

        public final Function0<Unit> getOnClick() {
            return this.onClick;
        }

        @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsText.Modifier
        public CharSequence modify(Context context, CharSequence charSequence) {
            Intrinsics.checkNotNullParameter(context, "context");
            Intrinsics.checkNotNullParameter(charSequence, "charSequence");
            SpannableStringBuilder append = new SpannableStringBuilder(charSequence).append((CharSequence) " ").append(SpanUtil.learnMore(context, this.learnMoreColor, new DSLSettingsText$LearnMoreModifier$$ExternalSyntheticLambda0(this)));
            Intrinsics.checkNotNullExpressionValue(append, "SpannableStringBuilder(c…Click()\n        }\n      )");
            return append;
        }

        /* renamed from: modify$lambda-0 */
        public static final void m525modify$lambda0(LearnMoreModifier learnMoreModifier, View view) {
            Intrinsics.checkNotNullParameter(learnMoreModifier, "this$0");
            learnMoreModifier.onClick.invoke();
        }
    }
}
