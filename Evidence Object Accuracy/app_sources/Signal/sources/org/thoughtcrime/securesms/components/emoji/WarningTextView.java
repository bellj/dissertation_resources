package org.thoughtcrime.securesms.components.emoji;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import androidx.appcompat.widget.AppCompatTextView;
import org.thoughtcrime.securesms.R;

/* loaded from: classes4.dex */
public final class WarningTextView extends AppCompatTextView {
    private final int originalTextColor;
    private boolean warning;
    private final int warningTextColor;

    public WarningTextView(Context context) {
        this(context, null);
    }

    public WarningTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public WarningTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R.styleable.WarningTextView, 0, 0);
        this.warningTextColor = obtainStyledAttributes.getColor(0, 0);
        obtainStyledAttributes.recycle();
        TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, new int[]{16842904});
        this.originalTextColor = obtainStyledAttributes2.getColor(0, 0);
        obtainStyledAttributes2.recycle();
    }

    public void setWarning(boolean z) {
        if (this.warning != z) {
            this.warning = z;
            setTextColor(z ? this.warningTextColor : this.originalTextColor);
            invalidate();
        }
    }
}
