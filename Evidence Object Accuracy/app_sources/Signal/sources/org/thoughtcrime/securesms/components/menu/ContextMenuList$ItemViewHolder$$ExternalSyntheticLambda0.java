package org.thoughtcrime.securesms.components.menu;

import android.view.View;
import org.thoughtcrime.securesms.components.menu.ContextMenuList;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ContextMenuList$ItemViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ ContextMenuList.DisplayItem f$0;
    public final /* synthetic */ ContextMenuList.ItemViewHolder f$1;

    public /* synthetic */ ContextMenuList$ItemViewHolder$$ExternalSyntheticLambda0(ContextMenuList.DisplayItem displayItem, ContextMenuList.ItemViewHolder itemViewHolder) {
        this.f$0 = displayItem;
        this.f$1 = itemViewHolder;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        ContextMenuList.ItemViewHolder.m509bind$lambda0(this.f$0, this.f$1, view);
    }
}
