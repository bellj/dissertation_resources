package org.thoughtcrime.securesms.components.webrtc;

import android.animation.Animator;
import android.graphics.Point;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import androidx.core.view.GestureDetectorCompat;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.animation.AnimationCompleteListener;
import org.thoughtcrime.securesms.util.MessageRecordUtil;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.views.TouchInterceptingFrameLayout;

/* loaded from: classes4.dex */
public class PictureInPictureGestureHelper extends GestureDetector.SimpleOnGestureListener {
    private static final Interpolator ADJUST_INTERPOLATOR = new AccelerateDecelerateInterpolator();
    private static final float DECELERATION_RATE;
    private static final Interpolator FLING_INTERPOLATOR = new ViscousFluidInterpolator();
    private int activePointerId = -1;
    private final View child;
    private int extraPaddingBottom;
    private int extraPaddingTop;
    private final int framePadding;
    private Interpolator interpolator;
    private boolean isAnimating;
    private boolean isDragging;
    private boolean isLockedToBottomEnd;
    private float lastTouchX;
    private float lastTouchY;
    private int maximumFlingVelocity;
    private final ViewGroup parent;
    private int pipHeight;
    private int pipWidth;
    private double projectionX;
    private double projectionY;
    private final Queue<Runnable> runAfterFling;
    private VelocityTracker velocityTracker;

    private static float project(float f) {
        return ((f / 1000.0f) * DECELERATION_RATE) / 0.00999999f;
    }

    public static PictureInPictureGestureHelper applyTo(View view) {
        TouchInterceptingFrameLayout touchInterceptingFrameLayout = (TouchInterceptingFrameLayout) view.getParent();
        PictureInPictureGestureHelper pictureInPictureGestureHelper = new PictureInPictureGestureHelper(touchInterceptingFrameLayout, view);
        GestureDetectorCompat gestureDetectorCompat = new GestureDetectorCompat(view.getContext(), pictureInPictureGestureHelper);
        touchInterceptingFrameLayout.setOnInterceptTouchEventListener(new TouchInterceptingFrameLayout.OnInterceptTouchEventListener() { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureGestureHelper$$ExternalSyntheticLambda0
            @Override // org.thoughtcrime.securesms.util.views.TouchInterceptingFrameLayout.OnInterceptTouchEventListener
            public final boolean onInterceptTouchEvent(MotionEvent motionEvent) {
                return PictureInPictureGestureHelper.$r8$lambda$NOsM5JmLrX7sI8hGRswAHyc5Nuw(PictureInPictureGestureHelper.this, motionEvent);
            }
        });
        touchInterceptingFrameLayout.setOnTouchListener(new View.OnTouchListener() { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureGestureHelper$$ExternalSyntheticLambda1
            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                return PictureInPictureGestureHelper.$r8$lambda$nJLno6WPs8xRQZyoFhAJ8a9FWL4(PictureInPictureGestureHelper.this, view2, motionEvent);
            }
        });
        view.setOnTouchListener(new View.OnTouchListener(pictureInPictureGestureHelper) { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureGestureHelper$$ExternalSyntheticLambda2
            public final /* synthetic */ PictureInPictureGestureHelper f$1;

            {
                this.f$1 = r2;
            }

            @Override // android.view.View.OnTouchListener
            public final boolean onTouch(View view2, MotionEvent motionEvent) {
                return PictureInPictureGestureHelper.$r8$lambda$rTIGMFoO9tVqZaubWR_nvCgxUwY(GestureDetectorCompat.this, this.f$1, view2, motionEvent);
            }
        });
        return pictureInPictureGestureHelper;
    }

    public static /* synthetic */ boolean lambda$applyTo$0(PictureInPictureGestureHelper pictureInPictureGestureHelper, MotionEvent motionEvent) {
        if (((motionEvent.getAction() & 65280) >> 8) > 0) {
            return false;
        }
        if (pictureInPictureGestureHelper.velocityTracker == null) {
            pictureInPictureGestureHelper.velocityTracker = VelocityTracker.obtain();
        }
        pictureInPictureGestureHelper.velocityTracker.addMovement(motionEvent);
        return false;
    }

    public static /* synthetic */ boolean lambda$applyTo$1(PictureInPictureGestureHelper pictureInPictureGestureHelper, View view, MotionEvent motionEvent) {
        VelocityTracker velocityTracker = pictureInPictureGestureHelper.velocityTracker;
        if (velocityTracker == null) {
            return false;
        }
        velocityTracker.recycle();
        pictureInPictureGestureHelper.velocityTracker = null;
        return false;
    }

    public static /* synthetic */ boolean lambda$applyTo$2(GestureDetectorCompat gestureDetectorCompat, PictureInPictureGestureHelper pictureInPictureGestureHelper, View view, MotionEvent motionEvent) {
        boolean onTouchEvent = gestureDetectorCompat.onTouchEvent(motionEvent);
        if (motionEvent.getActionMasked() == 1 || motionEvent.getActionMasked() == 3) {
            if (!onTouchEvent) {
                onTouchEvent = pictureInPictureGestureHelper.onGestureFinished(motionEvent);
            }
            VelocityTracker velocityTracker = pictureInPictureGestureHelper.velocityTracker;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                pictureInPictureGestureHelper.velocityTracker = null;
            }
        }
        return onTouchEvent;
    }

    private PictureInPictureGestureHelper(ViewGroup viewGroup, View view) {
        this.parent = viewGroup;
        this.child = view;
        this.framePadding = view.getResources().getDimensionPixelSize(R.dimen.picture_in_picture_gesture_helper_frame_padding);
        this.pipWidth = view.getResources().getDimensionPixelSize(R.dimen.picture_in_picture_gesture_helper_pip_width);
        this.pipHeight = view.getResources().getDimensionPixelSize(R.dimen.picture_in_picture_gesture_helper_pip_height);
        this.maximumFlingVelocity = ViewConfiguration.get(view.getContext()).getScaledMaximumFlingVelocity();
        this.runAfterFling = new LinkedList();
        this.interpolator = ADJUST_INTERPOLATOR;
    }

    public void clearVerticalBoundaries() {
        setTopVerticalBoundary(this.parent.getTop());
        setBottomVerticalBoundary(this.parent.getMeasuredHeight() + this.parent.getTop());
    }

    public void setTopVerticalBoundary(int i) {
        this.extraPaddingTop = i - this.parent.getTop();
    }

    public void setBottomVerticalBoundary(int i) {
        this.extraPaddingBottom = (this.parent.getMeasuredHeight() + this.parent.getTop()) - i;
    }

    private boolean onGestureFinished(MotionEvent motionEvent) {
        if (motionEvent.getActionIndex() != motionEvent.findPointerIndex(this.activePointerId)) {
            return false;
        }
        onFling(motionEvent, motionEvent, 0.0f, 0.0f);
        return true;
    }

    public void adjustPip() {
        this.pipWidth = this.child.getMeasuredWidth();
        this.pipHeight = this.child.getMeasuredHeight();
        if (this.isAnimating) {
            this.interpolator = ADJUST_INTERPOLATOR;
            fling();
        } else if (!this.isDragging) {
            this.interpolator = ADJUST_INTERPOLATOR;
            onFling(null, null, 0.0f, 0.0f);
        }
    }

    public void lockToBottomEnd() {
        this.isLockedToBottomEnd = true;
    }

    public void enableCorners() {
        this.isLockedToBottomEnd = false;
    }

    public void performAfterFling(Runnable runnable) {
        if (this.isAnimating) {
            this.runAfterFling.add(runnable);
        } else {
            runnable.run();
        }
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onDown(MotionEvent motionEvent) {
        this.activePointerId = motionEvent.getPointerId(0);
        this.lastTouchX = motionEvent.getX(0) + this.child.getX();
        this.lastTouchY = motionEvent.getY(0) + this.child.getY();
        this.isDragging = true;
        this.pipWidth = this.child.getMeasuredWidth();
        this.pipHeight = this.child.getMeasuredHeight();
        this.interpolator = FLING_INTERPOLATOR;
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        int findPointerIndex = motionEvent2.findPointerIndex(this.activePointerId);
        if (findPointerIndex == -1) {
            fling();
            return false;
        }
        float x = motionEvent2.getX(findPointerIndex) + this.child.getX();
        float y = motionEvent2.getY(findPointerIndex) + this.child.getY();
        View view = this.child;
        view.setTranslationX(view.getTranslationX() + (x - this.lastTouchX));
        View view2 = this.child;
        view2.setTranslationY(view2.getTranslationY() + (y - this.lastTouchY));
        this.lastTouchX = x;
        this.lastTouchY = y;
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        VelocityTracker velocityTracker = this.velocityTracker;
        if (velocityTracker != null) {
            velocityTracker.computeCurrentVelocity(MessageRecordUtil.MAX_BODY_DISPLAY_LENGTH, (float) this.maximumFlingVelocity);
            this.projectionX = (double) (this.child.getX() + project(this.velocityTracker.getXVelocity()));
            this.projectionY = (double) (this.child.getY() + project(this.velocityTracker.getYVelocity()));
        } else {
            this.projectionX = (double) this.child.getX();
            this.projectionY = (double) this.child.getY();
        }
        fling();
        return true;
    }

    @Override // android.view.GestureDetector.SimpleOnGestureListener, android.view.GestureDetector.OnGestureListener
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        this.isDragging = false;
        this.child.performClick();
        return true;
    }

    private void fling() {
        Point findNearestCornerPosition = findNearestCornerPosition(new Point((int) this.projectionX, (int) this.projectionY));
        this.isAnimating = true;
        this.isDragging = false;
        this.child.animate().translationX(getTranslationXForPoint(findNearestCornerPosition)).translationY(getTranslationYForPoint(findNearestCornerPosition)).setDuration(250).setInterpolator(this.interpolator).setListener(new AnimationCompleteListener() { // from class: org.thoughtcrime.securesms.components.webrtc.PictureInPictureGestureHelper.1
            @Override // org.thoughtcrime.securesms.animation.AnimationCompleteListener, android.animation.Animator.AnimatorListener
            public void onAnimationEnd(Animator animator) {
                PictureInPictureGestureHelper.this.isAnimating = false;
                Iterator it = PictureInPictureGestureHelper.this.runAfterFling.iterator();
                while (it.hasNext()) {
                    ((Runnable) it.next()).run();
                    it.remove();
                }
            }
        }).start();
    }

    private Point findNearestCornerPosition(Point point) {
        if (!this.isLockedToBottomEnd) {
            Point point2 = null;
            double d = Double.MAX_VALUE;
            for (Point point3 : Arrays.asList(calculateTopLeftCoordinates(), calculateTopRightCoordinates(this.parent), calculateBottomLeftCoordinates(this.parent), calculateBottomRightCoordinates(this.parent))) {
                double distance = distance(point3, point);
                if (distance < d) {
                    point2 = point3;
                    d = distance;
                }
            }
            return point2;
        } else if (ViewUtil.isLtr(this.parent)) {
            return calculateBottomRightCoordinates(this.parent);
        } else {
            return calculateBottomLeftCoordinates(this.parent);
        }
    }

    private float getTranslationXForPoint(Point point) {
        return (float) (point.x - this.child.getLeft());
    }

    private float getTranslationYForPoint(Point point) {
        return (float) (point.y - this.child.getTop());
    }

    private Point calculateTopLeftCoordinates() {
        int i = this.framePadding;
        return new Point(i, this.extraPaddingTop + i);
    }

    private Point calculateTopRightCoordinates(ViewGroup viewGroup) {
        int measuredWidth = viewGroup.getMeasuredWidth() - this.pipWidth;
        int i = this.framePadding;
        return new Point(measuredWidth - i, i + this.extraPaddingTop);
    }

    private Point calculateBottomLeftCoordinates(ViewGroup viewGroup) {
        return new Point(this.framePadding, ((viewGroup.getMeasuredHeight() - this.pipHeight) - this.framePadding) - this.extraPaddingBottom);
    }

    private Point calculateBottomRightCoordinates(ViewGroup viewGroup) {
        return new Point((viewGroup.getMeasuredWidth() - this.pipWidth) - this.framePadding, ((viewGroup.getMeasuredHeight() - this.pipHeight) - this.framePadding) - this.extraPaddingBottom);
    }

    private static double distance(Point point, Point point2) {
        return Math.sqrt(Math.pow((double) (point.x - point2.x), 2.0d) + Math.pow((double) (point.y - point2.y), 2.0d));
    }

    /* loaded from: classes4.dex */
    private static class ViscousFluidInterpolator implements Interpolator {
        private static final float VISCOUS_FLUID_NORMALIZE;
        private static final float VISCOUS_FLUID_OFFSET;
        private static final float VISCOUS_FLUID_SCALE;

        private ViscousFluidInterpolator() {
        }

        static {
            float viscousFluid = 1.0f / viscousFluid(1.0f);
            VISCOUS_FLUID_NORMALIZE = viscousFluid;
            VISCOUS_FLUID_OFFSET = 1.0f - (viscousFluid * viscousFluid(1.0f));
        }

        private static float viscousFluid(float f) {
            float f2 = f * VISCOUS_FLUID_SCALE;
            if (f2 < 1.0f) {
                return f2 - (1.0f - ((float) Math.exp((double) (-f2))));
            }
            return ((1.0f - ((float) Math.exp((double) (1.0f - f2)))) * 0.63212055f) + 0.36787945f;
        }

        @Override // android.animation.TimeInterpolator
        public float getInterpolation(float f) {
            float viscousFluid = VISCOUS_FLUID_NORMALIZE * viscousFluid(f);
            return viscousFluid > 0.0f ? viscousFluid + VISCOUS_FLUID_OFFSET : viscousFluid;
        }
    }
}
