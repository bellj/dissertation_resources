package org.thoughtcrime.securesms.components.settings.app.appearance;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.collections.ArraysKt___ArraysKt;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.keyvalue.SettingsValues;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: AppearanceSettingsFragment.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0010\u0015\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020!H\u0016J\u0010\u0010\"\u001a\u00020#2\u0006\u0010$\u001a\u00020%H\u0002R?\u0010\u0003\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR?\u0010\u000b\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\r\u0010\n\u001a\u0004\b\f\u0010\bR?\u0010\u000e\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0010\u0010\n\u001a\u0004\b\u000f\u0010\bR\u001b\u0010\u0011\u001a\u00020\u00128BX\u0002¢\u0006\f\n\u0004\b\u0015\u0010\n\u001a\u0004\b\u0013\u0010\u0014R?\u0010\u0016\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u0018\u0010\n\u001a\u0004\b\u0017\u0010\bR?\u0010\u0019\u001a&\u0012\f\u0012\n \u0006*\u0004\u0018\u00010\u00050\u0005 \u0006*\u0012\u0012\u000e\b\u0001\u0012\n \u0006*\u0004\u0018\u00010\u00050\u00050\u00040\u00048BX\u0002¢\u0006\f\n\u0004\b\u001b\u0010\n\u001a\u0004\b\u001a\u0010\bR\u000e\u0010\u001c\u001a\u00020\u001dX.¢\u0006\u0002\n\u0000¨\u0006&"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/appearance/AppearanceSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "languageLabels", "", "", "kotlin.jvm.PlatformType", "getLanguageLabels", "()[Ljava/lang/String;", "languageLabels$delegate", "Lkotlin/Lazy;", "languageValues", "getLanguageValues", "languageValues$delegate", "messageFontSizeLabels", "getMessageFontSizeLabels", "messageFontSizeLabels$delegate", "messageFontSizeValues", "", "getMessageFontSizeValues", "()[I", "messageFontSizeValues$delegate", "themeLabels", "getThemeLabels", "themeLabels$delegate", "themeValues", "getThemeValues", "themeValues$delegate", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/appearance/AppearanceSettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/appearance/AppearanceSettingsState;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class AppearanceSettingsFragment extends DSLSettingsFragment {
    private final Lazy languageLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$languageLabels$2
        final /* synthetic */ AppearanceSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.language_entries);
        }
    });
    private final Lazy languageValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$languageValues$2
        final /* synthetic */ AppearanceSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.language_values);
        }
    });
    private final Lazy messageFontSizeLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$messageFontSizeLabels$2
        final /* synthetic */ AppearanceSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_message_font_size_entries);
        }
    });
    private final Lazy messageFontSizeValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<int[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$messageFontSizeValues$2
        final /* synthetic */ AppearanceSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final int[] invoke() {
            return this.this$0.getResources().getIntArray(R.array.pref_message_font_size_values);
        }
    });
    private final Lazy themeLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$themeLabels$2
        final /* synthetic */ AppearanceSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_theme_entries);
        }
    });
    private final Lazy themeValues$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$themeValues$2
        final /* synthetic */ AppearanceSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.pref_theme_values);
        }
    });
    private AppearanceSettingsViewModel viewModel;

    public AppearanceSettingsFragment() {
        super(R.string.preferences__appearance, 0, 0, null, 14, null);
    }

    public final String[] getThemeLabels() {
        return (String[]) this.themeLabels$delegate.getValue();
    }

    public final String[] getThemeValues() {
        return (String[]) this.themeValues$delegate.getValue();
    }

    public final String[] getMessageFontSizeLabels() {
        return (String[]) this.messageFontSizeLabels$delegate.getValue();
    }

    public final int[] getMessageFontSizeValues() {
        return (int[]) this.messageFontSizeValues$delegate.getValue();
    }

    public final String[] getLanguageLabels() {
        return (String[]) this.languageLabels$delegate.getValue();
    }

    public final String[] getLanguageValues() {
        return (String[]) this.languageValues$delegate.getValue();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ViewModel viewModel = new ViewModelProvider(this).get(AppearanceSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this)[…ngsViewModel::class.java]");
        AppearanceSettingsViewModel appearanceSettingsViewModel = (AppearanceSettingsViewModel) viewModel;
        this.viewModel = appearanceSettingsViewModel;
        if (appearanceSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            appearanceSettingsViewModel = null;
        }
        appearanceSettingsViewModel.getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ AppearanceSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                AppearanceSettingsFragment.m571$r8$lambda$H_JZfyfBfBygUH7Mox4dogbUjQ(DSLSettingsAdapter.this, this.f$1, (AppearanceSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m572bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, AppearanceSettingsFragment appearanceSettingsFragment, AppearanceSettingsState appearanceSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(appearanceSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(appearanceSettingsState, "state");
        dSLSettingsAdapter.submitList(appearanceSettingsFragment.getConfiguration(appearanceSettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(AppearanceSettingsState appearanceSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(this, appearanceSettingsState) { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$getConfiguration$1
            final /* synthetic */ AppearanceSettingsState $state;
            final /* synthetic */ AppearanceSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
                this.$state = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.preferences__theme, new DSLSettingsText.Modifier[0]);
                String[] access$getThemeLabels = AppearanceSettingsFragment.access$getThemeLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getThemeLabels, "themeLabels");
                String[] access$getThemeValues = AppearanceSettingsFragment.access$getThemeValues(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getThemeValues, "themeValues");
                int i = ArraysKt___ArraysKt.indexOf(access$getThemeValues, this.$state.getTheme().serialize());
                final AppearanceSettingsFragment appearanceSettingsFragment = this.this$0;
                dSLConfiguration.radioListPref(from, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from : null, (r19 & 8) != 0, access$getThemeLabels, i, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$getConfiguration$1.1
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i2) {
                        AppearanceSettingsViewModel access$getViewModel$p = AppearanceSettingsFragment.access$getViewModel$p(appearanceSettingsFragment);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        FragmentActivity activity = appearanceSettingsFragment.getActivity();
                        SettingsValues.Theme deserialize = SettingsValues.Theme.deserialize(AppearanceSettingsFragment.access$getThemeValues(appearanceSettingsFragment)[i2]);
                        Intrinsics.checkNotNullExpressionValue(deserialize, "deserialize(themeValues[it])");
                        access$getViewModel$p.setTheme(activity, deserialize);
                    }
                });
                DSLSettingsText from2 = companion.from(R.string.preferences__chat_color_and_wallpaper, new DSLSettingsText.Modifier[0]);
                final AppearanceSettingsFragment appearanceSettingsFragment2 = this.this$0;
                dSLConfiguration.clickPref(from2, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(appearanceSettingsFragment2.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_appearanceSettings_to_wallpaperActivity);
                    }
                }, (r18 & 64) != 0 ? null : null);
                DSLSettingsText from3 = companion.from(R.string.preferences_chats__message_text_size, new DSLSettingsText.Modifier[0]);
                String[] access$getMessageFontSizeLabels = AppearanceSettingsFragment.access$getMessageFontSizeLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getMessageFontSizeLabels, "messageFontSizeLabels");
                int[] access$getMessageFontSizeValues = AppearanceSettingsFragment.access$getMessageFontSizeValues(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getMessageFontSizeValues, "messageFontSizeValues");
                int i2 = ArraysKt___ArraysKt.indexOf(access$getMessageFontSizeValues, this.$state.getMessageFontSize());
                final AppearanceSettingsFragment appearanceSettingsFragment3 = this.this$0;
                dSLConfiguration.radioListPref(from3, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from3 : null, (r19 & 8) != 0, access$getMessageFontSizeLabels, i2, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$getConfiguration$1.3
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i3) {
                        AppearanceSettingsViewModel access$getViewModel$p = AppearanceSettingsFragment.access$getViewModel$p(appearanceSettingsFragment3);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setMessageFontSize(AppearanceSettingsFragment.access$getMessageFontSizeValues(appearanceSettingsFragment3)[i3]);
                    }
                });
                DSLSettingsText from4 = companion.from(R.string.preferences__language, new DSLSettingsText.Modifier[0]);
                String[] access$getLanguageLabels = AppearanceSettingsFragment.access$getLanguageLabels(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getLanguageLabels, "languageLabels");
                String[] access$getLanguageValues = AppearanceSettingsFragment.access$getLanguageValues(this.this$0);
                Intrinsics.checkNotNullExpressionValue(access$getLanguageValues, "languageValues");
                int i3 = ArraysKt___ArraysKt.indexOf(access$getLanguageValues, this.$state.getLanguage());
                final AppearanceSettingsFragment appearanceSettingsFragment4 = this.this$0;
                dSLConfiguration.radioListPref(from4, (r19 & 2) != 0 ? null : null, (r19 & 4) != 0 ? from4 : null, (r19 & 8) != 0, access$getLanguageLabels, i3, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.appearance.AppearanceSettingsFragment$getConfiguration$1.4
                    /* Return type fixed from 'java.lang.Object' to match base method */
                    /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                    @Override // kotlin.jvm.functions.Function1
                    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                        invoke(num.intValue());
                        return Unit.INSTANCE;
                    }

                    public final void invoke(int i4) {
                        AppearanceSettingsViewModel access$getViewModel$p = AppearanceSettingsFragment.access$getViewModel$p(appearanceSettingsFragment4);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        String str = AppearanceSettingsFragment.access$getLanguageValues(appearanceSettingsFragment4)[i4];
                        Intrinsics.checkNotNullExpressionValue(str, "languageValues[it]");
                        access$getViewModel$p.setLanguage(str);
                    }
                });
            }
        });
    }
}
