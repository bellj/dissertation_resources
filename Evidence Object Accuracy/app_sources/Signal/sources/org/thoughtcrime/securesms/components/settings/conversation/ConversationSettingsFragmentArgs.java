package org.thoughtcrime.securesms.components.settings.conversation;

import android.os.Bundle;
import android.os.Parcelable;
import java.io.Serializable;
import java.util.HashMap;
import org.thoughtcrime.securesms.recipients.RecipientId;

/* loaded from: classes4.dex */
public class ConversationSettingsFragmentArgs {
    private final HashMap arguments;

    private ConversationSettingsFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ConversationSettingsFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ConversationSettingsFragmentArgs fromBundle(Bundle bundle) {
        ConversationSettingsFragmentArgs conversationSettingsFragmentArgs = new ConversationSettingsFragmentArgs();
        bundle.setClassLoader(ConversationSettingsFragmentArgs.class.getClassLoader());
        if (!bundle.containsKey("recipient_id")) {
            throw new IllegalArgumentException("Required argument \"recipient_id\" is missing and does not have an android:defaultValue");
        } else if (Parcelable.class.isAssignableFrom(RecipientId.class) || Serializable.class.isAssignableFrom(RecipientId.class)) {
            conversationSettingsFragmentArgs.arguments.put("recipient_id", (RecipientId) bundle.get("recipient_id"));
            if (!bundle.containsKey("group_id")) {
                throw new IllegalArgumentException("Required argument \"group_id\" is missing and does not have an android:defaultValue");
            } else if (Parcelable.class.isAssignableFrom(Parcelable.class) || Serializable.class.isAssignableFrom(Parcelable.class)) {
                conversationSettingsFragmentArgs.arguments.put("group_id", (Parcelable) bundle.get("group_id"));
                return conversationSettingsFragmentArgs;
            } else {
                throw new UnsupportedOperationException(Parcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        } else {
            throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
        }
    }

    public RecipientId getRecipientId() {
        return (RecipientId) this.arguments.get("recipient_id");
    }

    public Parcelable getGroupId() {
        return (Parcelable) this.arguments.get("group_id");
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("recipient_id")) {
            RecipientId recipientId = (RecipientId) this.arguments.get("recipient_id");
            if (Parcelable.class.isAssignableFrom(RecipientId.class) || recipientId == null) {
                bundle.putParcelable("recipient_id", (Parcelable) Parcelable.class.cast(recipientId));
            } else if (Serializable.class.isAssignableFrom(RecipientId.class)) {
                bundle.putSerializable("recipient_id", (Serializable) Serializable.class.cast(recipientId));
            } else {
                throw new UnsupportedOperationException(RecipientId.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        if (this.arguments.containsKey("group_id")) {
            Parcelable parcelable = (Parcelable) this.arguments.get("group_id");
            if (Parcelable.class.isAssignableFrom(Parcelable.class) || parcelable == null) {
                bundle.putParcelable("group_id", (Parcelable) Parcelable.class.cast(parcelable));
            } else if (Serializable.class.isAssignableFrom(Parcelable.class)) {
                bundle.putSerializable("group_id", (Serializable) Serializable.class.cast(parcelable));
            } else {
                throw new UnsupportedOperationException(Parcelable.class.getName() + " must implement Parcelable or Serializable or must be an Enum.");
            }
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ConversationSettingsFragmentArgs conversationSettingsFragmentArgs = (ConversationSettingsFragmentArgs) obj;
        if (this.arguments.containsKey("recipient_id") != conversationSettingsFragmentArgs.arguments.containsKey("recipient_id")) {
            return false;
        }
        if (getRecipientId() == null ? conversationSettingsFragmentArgs.getRecipientId() != null : !getRecipientId().equals(conversationSettingsFragmentArgs.getRecipientId())) {
            return false;
        }
        if (this.arguments.containsKey("group_id") != conversationSettingsFragmentArgs.arguments.containsKey("group_id")) {
            return false;
        }
        return getGroupId() == null ? conversationSettingsFragmentArgs.getGroupId() == null : getGroupId().equals(conversationSettingsFragmentArgs.getGroupId());
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((getRecipientId() != null ? getRecipientId().hashCode() : 0) + 31) * 31;
        if (getGroupId() != null) {
            i = getGroupId().hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return "ConversationSettingsFragmentArgs{recipientId=" + getRecipientId() + ", groupId=" + getGroupId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ConversationSettingsFragmentArgs conversationSettingsFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(conversationSettingsFragmentArgs.arguments);
        }

        public Builder(RecipientId recipientId, Parcelable parcelable) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("recipient_id", recipientId);
            hashMap.put("group_id", parcelable);
        }

        public ConversationSettingsFragmentArgs build() {
            return new ConversationSettingsFragmentArgs(this.arguments);
        }

        public Builder setRecipientId(RecipientId recipientId) {
            this.arguments.put("recipient_id", recipientId);
            return this;
        }

        public Builder setGroupId(Parcelable parcelable) {
            this.arguments.put("group_id", parcelable);
            return this;
        }

        public RecipientId getRecipientId() {
            return (RecipientId) this.arguments.get("recipient_id");
        }

        public Parcelable getGroupId() {
            return (Parcelable) this.arguments.get("group_id");
        }
    }
}
