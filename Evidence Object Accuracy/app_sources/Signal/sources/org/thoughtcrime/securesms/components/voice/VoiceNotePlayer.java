package org.thoughtcrime.securesms.components.voice;

import android.content.Context;
import com.google.android.exoplayer2.ForwardingPlayer;
import com.google.android.exoplayer2.SimpleExoPlayer;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: VoiceNotePlayer.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\u0018\u00002\u00020\u0001B\u0019\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0018\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000eH\u0016R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b¨\u0006\u000f"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNotePlayer;", "Lcom/google/android/exoplayer2/ForwardingPlayer;", "context", "Landroid/content/Context;", "internalPlayer", "Lcom/google/android/exoplayer2/SimpleExoPlayer;", "(Landroid/content/Context;Lcom/google/android/exoplayer2/SimpleExoPlayer;)V", "getInternalPlayer", "()Lcom/google/android/exoplayer2/SimpleExoPlayer;", "seekTo", "", "windowIndex", "", "positionMs", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNotePlayer extends ForwardingPlayer {
    private final SimpleExoPlayer internalPlayer;

    /* JADX INFO: 'this' call moved to the top of the method (can break code semantics) */
    public VoiceNotePlayer(Context context) {
        this(context, null, 2, null);
        Intrinsics.checkNotNullParameter(context, "context");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ VoiceNotePlayer(android.content.Context r1, com.google.android.exoplayer2.SimpleExoPlayer r2, int r3, kotlin.jvm.internal.DefaultConstructorMarker r4) {
        /*
            r0 = this;
            r3 = r3 & 2
            if (r3 == 0) goto L_0x002f
            com.google.android.exoplayer2.SimpleExoPlayer$Builder r2 = new com.google.android.exoplayer2.SimpleExoPlayer$Builder
            r2.<init>(r1)
            org.thoughtcrime.securesms.video.exo.SignalMediaSourceFactory r3 = new org.thoughtcrime.securesms.video.exo.SignalMediaSourceFactory
            r3.<init>(r1)
            com.google.android.exoplayer2.SimpleExoPlayer$Builder r2 = r2.setMediaSourceFactory(r3)
            com.google.android.exoplayer2.DefaultLoadControl$Builder r3 = new com.google.android.exoplayer2.DefaultLoadControl$Builder
            r3.<init>()
            r4 = 2147483647(0x7fffffff, float:NaN)
            com.google.android.exoplayer2.DefaultLoadControl$Builder r3 = r3.setBufferDurationsMs(r4, r4, r4, r4)
            com.google.android.exoplayer2.DefaultLoadControl r3 = r3.build()
            com.google.android.exoplayer2.SimpleExoPlayer$Builder r2 = r2.setLoadControl(r3)
            com.google.android.exoplayer2.SimpleExoPlayer r2 = r2.build()
            java.lang.String r3 = "Builder(context)\n    .se…   .build()\n    ).build()"
            kotlin.jvm.internal.Intrinsics.checkNotNullExpressionValue(r2, r3)
        L_0x002f:
            r0.<init>(r1, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.voice.VoiceNotePlayer.<init>(android.content.Context, com.google.android.exoplayer2.SimpleExoPlayer, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final SimpleExoPlayer getInternalPlayer() {
        return this.internalPlayer;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public VoiceNotePlayer(Context context, SimpleExoPlayer simpleExoPlayer) {
        super(simpleExoPlayer);
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(simpleExoPlayer, "internalPlayer");
        this.internalPlayer = simpleExoPlayer;
    }

    @Override // com.google.android.exoplayer2.ForwardingPlayer, com.google.android.exoplayer2.Player
    public void seekTo(int i, long j) {
        super.seekTo(i, j);
        boolean z = false;
        boolean z2 = i % 2 == 1;
        if (j == -9223372036854775807L) {
            z = true;
        }
        if (!z2 || !z) {
            super.seekTo(i, j);
            return;
        }
        int i2 = getCurrentWindowIndex() < i ? i + 1 : i - 1;
        if (getMediaItemCount() <= i2) {
            super.seekTo(i, j);
        } else {
            super.seekTo(i2, j);
        }
    }
}
