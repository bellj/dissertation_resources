package org.thoughtcrime.securesms.components.settings.app.subscription.errors;

import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: UnexpectedSubscriptionCancellation.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0001\u0018\u0000 \u000b2\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u000bB\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\u0007j\u0002\b\bj\u0002\b\tj\u0002\b\n¨\u0006\f"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation;", "", "status", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getStatus", "()Ljava/lang/String;", "PAST_DUE", "CANCELED", "UNPAID", "INACTIVE", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum UnexpectedSubscriptionCancellation {
    PAST_DUE("past_due"),
    CANCELED("canceled"),
    UNPAID("unpaid"),
    INACTIVE("user-was-inactive");
    
    public static final Companion Companion = new Companion(null);
    private final String status;

    @JvmStatic
    public static final UnexpectedSubscriptionCancellation fromStatus(String str) {
        return Companion.fromStatus(str);
    }

    UnexpectedSubscriptionCancellation(String str) {
        this.status = str;
    }

    public final String getStatus() {
        return this.status;
    }

    /* compiled from: UnexpectedSubscriptionCancellation.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0014\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation$Companion;", "", "()V", "fromStatus", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/errors/UnexpectedSubscriptionCancellation;", "status", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final UnexpectedSubscriptionCancellation fromStatus(String str) {
            UnexpectedSubscriptionCancellation[] values = UnexpectedSubscriptionCancellation.values();
            for (UnexpectedSubscriptionCancellation unexpectedSubscriptionCancellation : values) {
                if (Intrinsics.areEqual(unexpectedSubscriptionCancellation.getStatus(), str)) {
                    return unexpectedSubscriptionCancellation;
                }
            }
            return null;
        }
    }
}
