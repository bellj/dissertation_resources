package org.thoughtcrime.securesms.components.settings.conversation;

import org.thoughtcrime.securesms.MuteDialog;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda0 implements MuteDialog.MuteSelectionListener {
    public final /* synthetic */ ConversationSettingsViewModel f$0;

    public /* synthetic */ ConversationSettingsFragment$getConfiguration$1$8$$ExternalSyntheticLambda0(ConversationSettingsViewModel conversationSettingsViewModel) {
        this.f$0 = conversationSettingsViewModel;
    }

    @Override // org.thoughtcrime.securesms.MuteDialog.MuteSelectionListener
    public final void onMuted(long j) {
        this.f$0.setMuteUntil(j);
    }
}
