package org.thoughtcrime.securesms.components.settings.conversation;

import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.util.livedata.Store;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda3 implements Store.Action {
    public final /* synthetic */ ConversationSettingsViewModel.GroupSettingsViewModel f$0;

    public /* synthetic */ ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda3(ConversationSettingsViewModel.GroupSettingsViewModel groupSettingsViewModel) {
        this.f$0 = groupSettingsViewModel;
    }

    @Override // org.thoughtcrime.securesms.util.livedata.Store.Action
    public final Object apply(Object obj, Object obj2) {
        return ConversationSettingsViewModel.GroupSettingsViewModel.m1132_init_$lambda12(this.f$0, (Boolean) obj, (ConversationSettingsState) obj2);
    }
}
