package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import com.annimon.stream.function.Function;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import org.thoughtcrime.securesms.subscription.Subscription;

/* compiled from: ManageDonationsViewModel.kt */
@Metadata(d1 = {"\u0000\u0012\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\u0010\u0000\u001a\u00020\u00012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\n¢\u0006\u0002\b\u0005"}, d2 = {"<anonymous>", "", "subs", "", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ManageDonationsViewModel$refresh$6 extends Lambda implements Function1<List<? extends Subscription>, Unit> {
    final /* synthetic */ ManageDonationsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ManageDonationsViewModel$refresh$6(ManageDonationsViewModel manageDonationsViewModel) {
        super(1);
        this.this$0 = manageDonationsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(List<? extends Subscription> list) {
        invoke((List<Subscription>) list);
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final ManageDonationsState m980invoke$lambda0(List list, ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullParameter(list, "$subs");
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "it");
        return ManageDonationsState.copy$default(manageDonationsState, null, null, list, null, 11, null);
    }

    public final void invoke(List<Subscription> list) {
        Intrinsics.checkNotNullParameter(list, "subs");
        this.this$0.store.update(new Function(list) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsViewModel$refresh$6$$ExternalSyntheticLambda0
            public final /* synthetic */ List f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return ManageDonationsViewModel$refresh$6.m980invoke$lambda0(this.f$0, (ManageDonationsState) obj);
            }
        });
    }
}
