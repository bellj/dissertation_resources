package org.thoughtcrime.securesms.components.settings.app;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsFragment;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AppSettingsFragment$SubscriptionPreferenceViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AppSettingsFragment.SubscriptionPreference f$0;

    public /* synthetic */ AppSettingsFragment$SubscriptionPreferenceViewHolder$$ExternalSyntheticLambda0(AppSettingsFragment.SubscriptionPreference subscriptionPreference) {
        this.f$0 = subscriptionPreference;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AppSettingsFragment.SubscriptionPreferenceViewHolder.m552bind$lambda0(this.f$0, view);
    }
}
