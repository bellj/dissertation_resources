package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\r\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B[\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b\u0012\u0010\b\u0002\u0010\r\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000b¢\u0006\u0002\u0010\u000eR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0016\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0014\u0010\b\u001a\u00020\tX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\u0012R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0019\u0010\r\u001a\n\u0012\u0004\u0012\u00020\t\u0018\u00010\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0017¨\u0006\u0019"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/ClickPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "iconEnd", "isEnabled", "", "onClick", "Lkotlin/Function0;", "", "onLongClick", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZLkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "getIconEnd", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getOnLongClick", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getTitle", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ClickPreference extends PreferenceModel<ClickPreference> {
    private final DSLSettingsIcon icon;
    private final DSLSettingsIcon iconEnd;
    private final boolean isEnabled;
    private final Function0<Unit> onClick;
    private final Function0<Boolean> onLongClick;
    private final DSLSettingsText summary;
    private final DSLSettingsText title;

    public /* synthetic */ ClickPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, DSLSettingsIcon dSLSettingsIcon2, boolean z, Function0 function0, Function0 function02, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dSLSettingsText, (i & 2) != 0 ? null : dSLSettingsText2, (i & 4) != 0 ? null : dSLSettingsIcon, (i & 8) != 0 ? null : dSLSettingsIcon2, (i & 16) != 0 ? true : z, function0, (i & 64) != 0 ? null : function02);
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getSummary() {
        return this.summary;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsIcon getIcon() {
        return this.icon;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsIcon getIconEnd() {
        return this.iconEnd;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public final Function0<Unit> getOnClick() {
        return this.onClick;
    }

    public final Function0<Boolean> getOnLongClick() {
        return this.onLongClick;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public ClickPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, DSLSettingsIcon dSLSettingsIcon2, boolean z, Function0<Unit> function0, Function0<Boolean> function02) {
        super(null, null, null, null, false, 31, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.title = dSLSettingsText;
        this.summary = dSLSettingsText2;
        this.icon = dSLSettingsIcon;
        this.iconEnd = dSLSettingsIcon2;
        this.isEnabled = z;
        this.onClick = function0;
        this.onLongClick = function02;
    }
}
