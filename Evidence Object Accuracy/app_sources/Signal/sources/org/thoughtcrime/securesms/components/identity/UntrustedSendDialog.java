package org.thoughtcrime.securesms.components.identity;

import android.content.Context;
import android.content.DialogInterface;
import androidx.appcompat.app.AlertDialog;
import java.util.List;
import org.signal.core.util.concurrent.SimpleTask;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.crypto.ReentrantSessionLock;
import org.thoughtcrime.securesms.crypto.storage.SignalIdentityKeyStore;
import org.thoughtcrime.securesms.database.model.IdentityRecord;
import org.thoughtcrime.securesms.dependencies.ApplicationDependencies;
import org.whispersystems.signalservice.api.SignalSessionLock;

/* loaded from: classes4.dex */
public class UntrustedSendDialog extends AlertDialog.Builder implements DialogInterface.OnClickListener {
    private final ResendListener resendListener;
    private final List<IdentityRecord> untrustedRecords;

    /* loaded from: classes4.dex */
    public interface ResendListener {
        void onResendMessage();
    }

    public UntrustedSendDialog(Context context, String str, List<IdentityRecord> list, ResendListener resendListener) {
        super(context);
        this.untrustedRecords = list;
        this.resendListener = resendListener;
        setTitle(R.string.UntrustedSendDialog_send_message);
        setIcon(R.drawable.ic_warning);
        setMessage(str);
        setPositiveButton(R.string.UntrustedSendDialog_send, this);
        setNegativeButton(17039360, (DialogInterface.OnClickListener) null);
    }

    @Override // android.content.DialogInterface.OnClickListener
    public void onClick(DialogInterface dialogInterface, int i) {
        SimpleTask.run(new SimpleTask.BackgroundTask(ApplicationDependencies.getProtocolStore().aci().identities()) { // from class: org.thoughtcrime.securesms.components.identity.UntrustedSendDialog$$ExternalSyntheticLambda0
            public final /* synthetic */ SignalIdentityKeyStore f$1;

            {
                this.f$1 = r2;
            }

            @Override // org.signal.core.util.concurrent.SimpleTask.BackgroundTask
            public final Object run() {
                return UntrustedSendDialog.$r8$lambda$ezNybcknYdNonDtUy5GLgubLjaw(UntrustedSendDialog.this, this.f$1);
            }
        }, new SimpleTask.ForegroundTask() { // from class: org.thoughtcrime.securesms.components.identity.UntrustedSendDialog$$ExternalSyntheticLambda1
            @Override // org.signal.core.util.concurrent.SimpleTask.ForegroundTask
            public final void run(Object obj) {
                UntrustedSendDialog.m506$r8$lambda$yJGfYvm3ne_s2oYm7ljXvGYGs0(UntrustedSendDialog.this, obj);
            }
        });
    }

    public /* synthetic */ Object lambda$onClick$0(SignalIdentityKeyStore signalIdentityKeyStore) {
        SignalSessionLock.Lock acquire = ReentrantSessionLock.INSTANCE.acquire();
        try {
            for (IdentityRecord identityRecord : this.untrustedRecords) {
                signalIdentityKeyStore.setApproval(identityRecord.getRecipientId(), true);
            }
            if (acquire == null) {
                return null;
            }
            acquire.close();
            return null;
        } catch (Throwable th) {
            if (acquire != null) {
                try {
                    acquire.close();
                } catch (Throwable th2) {
                    th.addSuppressed(th2);
                }
            }
            throw th;
        }
    }

    public /* synthetic */ void lambda$onClick$1(Object obj) {
        this.resendListener.onResendMessage();
    }
}
