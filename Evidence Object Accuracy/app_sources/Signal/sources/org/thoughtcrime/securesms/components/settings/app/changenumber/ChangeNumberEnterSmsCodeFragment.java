package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.FragmentKt;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterSmsCodeFragmentDirections;
import org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberLockActivity;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChangeNumberEnterSmsCodeFragment.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0003J\b\u0010\u0004\u001a\u00020\u0002H\u0014J\b\u0010\u0005\u001a\u00020\u0006H\u0014J\b\u0010\u0007\u001a\u00020\u0006H\u0014J\b\u0010\b\u001a\u00020\u0006H\u0014J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0014J\b\u0010\f\u001a\u00020\u0006H\u0002J\u001a\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0016¨\u0006\u0012"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberEnterSmsCodeFragment;", "Lorg/thoughtcrime/securesms/registration/fragments/BaseEnterSmsCodeFragment;", "Lorg/thoughtcrime/securesms/components/settings/app/changenumber/ChangeNumberViewModel;", "()V", "getViewModel", "handleSuccessfulVerify", "", "navigateToCaptcha", "navigateToKbsAccountLocked", "navigateToRegistrationLock", "timeRemaining", "", "navigateUp", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChangeNumberEnterSmsCodeFragment extends BaseEnterSmsCodeFragment<ChangeNumberViewModel> {
    public ChangeNumberEnterSmsCodeFragment() {
        super(R.layout.fragment_change_number_enter_code);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment, androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        super.onViewCreated(view, bundle);
        View findViewById = view.findViewById(R.id.toolbar);
        Intrinsics.checkNotNullExpressionValue(findViewById, "view.findViewById(R.id.toolbar)");
        Toolbar toolbar = (Toolbar) findViewById;
        toolbar.setTitle(getViewModel().getNumber().getFullFormattedNumber());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterSmsCodeFragment$$ExternalSyntheticLambda0
            @Override // android.view.View.OnClickListener
            public final void onClick(View view2) {
                ChangeNumberEnterSmsCodeFragment.$r8$lambda$4h3UeyZw3OZXJcJy5Gz6iaxS4K4(ChangeNumberEnterSmsCodeFragment.this, view2);
            }
        });
        view.findViewById(R.id.verify_header).setOnClickListener(null);
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterSmsCodeFragment$onViewCreated$2
            final /* synthetic */ ChangeNumberEnterSmsCodeFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                ChangeNumberEnterSmsCodeFragment.access$navigateUp(this.this$0);
            }
        });
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m591onViewCreated$lambda0(ChangeNumberEnterSmsCodeFragment changeNumberEnterSmsCodeFragment, View view) {
        Intrinsics.checkNotNullParameter(changeNumberEnterSmsCodeFragment, "this$0");
        changeNumberEnterSmsCodeFragment.navigateUp();
    }

    public final void navigateUp() {
        if (SignalStore.misc().isChangeNumberLocked()) {
            ChangeNumberLockActivity.Companion companion = ChangeNumberLockActivity.Companion;
            Context requireContext = requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            startActivity(companion.createIntent(requireContext));
            return;
        }
        FragmentKt.findNavController(this).navigateUp();
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    public ChangeNumberViewModel getViewModel() {
        return ChangeNumberUtil.getViewModel(this);
    }

    /* renamed from: handleSuccessfulVerify$lambda-1 */
    public static final void m590handleSuccessfulVerify$lambda1(ChangeNumberEnterSmsCodeFragment changeNumberEnterSmsCodeFragment) {
        Intrinsics.checkNotNullParameter(changeNumberEnterSmsCodeFragment, "this$0");
        ChangeNumberUtil.INSTANCE.changeNumberSuccess(changeNumberEnterSmsCodeFragment);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void handleSuccessfulVerify() {
        displaySuccess(new Runnable() { // from class: org.thoughtcrime.securesms.components.settings.app.changenumber.ChangeNumberEnterSmsCodeFragment$$ExternalSyntheticLambda1
            @Override // java.lang.Runnable
            public final void run() {
                ChangeNumberEnterSmsCodeFragment.$r8$lambda$lnzUgA70R3oNZ4GLv4fx7ByceV0(ChangeNumberEnterSmsCodeFragment.this);
            }
        });
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void navigateToCaptcha() {
        SafeNavigation.safeNavigate(FragmentKt.findNavController(this), (int) R.id.action_changeNumberEnterCodeFragment_to_captchaFragment, ChangeNumberUtil.INSTANCE.getCaptchaArguments());
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void navigateToRegistrationLock(long j) {
        NavController findNavController = FragmentKt.findNavController(this);
        ChangeNumberEnterSmsCodeFragmentDirections.ActionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock = ChangeNumberEnterSmsCodeFragmentDirections.actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock(j);
        Intrinsics.checkNotNullExpressionValue(actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock, "actionChangeNumberEnterC…rationLock(timeRemaining)");
        SafeNavigation.safeNavigate(findNavController, actionChangeNumberEnterCodeFragmentToChangeNumberRegistrationLock);
    }

    @Override // org.thoughtcrime.securesms.registration.fragments.BaseEnterSmsCodeFragment
    protected void navigateToKbsAccountLocked() {
        NavController findNavController = FragmentKt.findNavController(this);
        NavDirections actionChangeNumberEnterCodeFragmentToChangeNumberAccountLocked = ChangeNumberEnterSmsCodeFragmentDirections.actionChangeNumberEnterCodeFragmentToChangeNumberAccountLocked();
        Intrinsics.checkNotNullExpressionValue(actionChangeNumberEnterCodeFragmentToChangeNumberAccountLocked, "actionChangeNumberEnterC…angeNumberAccountLocked()");
        SafeNavigation.safeNavigate(findNavController, actionChangeNumberEnterCodeFragmentToChangeNumberAccountLocked);
    }
}
