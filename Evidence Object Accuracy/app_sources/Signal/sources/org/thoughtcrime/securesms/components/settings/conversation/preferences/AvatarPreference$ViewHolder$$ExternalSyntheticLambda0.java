package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.AvatarPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class AvatarPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ AvatarPreference.Model f$0;

    public /* synthetic */ AvatarPreference$ViewHolder$$ExternalSyntheticLambda0(AvatarPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        AvatarPreference.ViewHolder.m1193bind$lambda1(this.f$0, view);
    }
}
