package org.thoughtcrime.securesms.components.webrtc;

/* loaded from: classes4.dex */
public interface OnAudioOutputChangedListener {
    void audioOutputChanged(WebRtcAudioOutput webRtcAudioOutput);
}
