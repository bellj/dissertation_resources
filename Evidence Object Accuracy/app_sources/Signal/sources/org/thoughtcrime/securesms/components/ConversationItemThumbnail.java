package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.mms.SlidesClickedListener;
import org.thoughtcrime.securesms.util.Projection;
import org.thoughtcrime.securesms.util.ViewUtil;

/* loaded from: classes4.dex */
public class ConversationItemThumbnail extends FrameLayout {
    private AlbumThumbnailView album;
    private boolean borderless;
    private CornerMask cornerMask;
    private ConversationItemFooter footer;
    private int[] gifBounds;
    private int maximumThumbnailHeight;
    private int minimumThumbnailWidth;
    private int[] normalBounds;
    private Outliner pulseOutliner;
    private ImageView shade;
    private ThumbnailView thumbnail;

    public ConversationItemThumbnail(Context context) {
        super(context);
        init(null);
    }

    public ConversationItemThumbnail(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(attributeSet);
    }

    public ConversationItemThumbnail(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        init(attributeSet);
    }

    private void init(AttributeSet attributeSet) {
        FrameLayout.inflate(getContext(), R.layout.conversation_item_thumbnail, this);
        this.thumbnail = (ThumbnailView) findViewById(R.id.conversation_thumbnail_image);
        this.album = (AlbumThumbnailView) findViewById(R.id.conversation_thumbnail_album);
        this.shade = (ImageView) findViewById(R.id.conversation_thumbnail_shade);
        this.footer = (ConversationItemFooter) findViewById(R.id.conversation_thumbnail_footer);
        this.cornerMask = new CornerMask(this);
        int dpToPx = ViewUtil.dpToPx(260);
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R.styleable.ConversationItemThumbnail, 0, 0);
            this.normalBounds = new int[]{obtainStyledAttributes.getDimensionPixelSize(4, 0), obtainStyledAttributes.getDimensionPixelSize(2, 0), obtainStyledAttributes.getDimensionPixelSize(3, 0), obtainStyledAttributes.getDimensionPixelSize(1, 0)};
            dpToPx = obtainStyledAttributes.getDimensionPixelSize(0, dpToPx);
            obtainStyledAttributes.recycle();
        } else {
            this.normalBounds = new int[]{0, 0, 0, 0};
        }
        this.gifBounds = new int[]{dpToPx, dpToPx, 1, Integer.MAX_VALUE};
        this.minimumThumbnailWidth = -1;
        this.maximumThumbnailHeight = -1;
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if (!this.borderless) {
            this.cornerMask.mask(canvas);
        }
        Outliner outliner = this.pulseOutliner;
        if (outliner != null) {
            outliner.draw(canvas);
        }
    }

    public void hideThumbnailView() {
        this.thumbnail.setAlpha(0.0f);
    }

    public void showThumbnailView() {
        this.thumbnail.setAlpha(1.0f);
    }

    public Projection.Corners getCorners() {
        return new Projection.Corners(this.cornerMask.getRadii());
    }

    public void setPulseOutliner(Outliner outliner) {
        this.pulseOutliner = outliner;
    }

    @Override // android.view.View
    public void setFocusable(boolean z) {
        this.thumbnail.setFocusable(z);
        this.album.setFocusable(z);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        this.thumbnail.setClickable(z);
        this.album.setClickable(z);
    }

    @Override // android.view.View
    public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
        this.thumbnail.setOnLongClickListener(onLongClickListener);
        this.album.setOnLongClickListener(onLongClickListener);
    }

    public void showShade(boolean z) {
        this.shade.setVisibility(z ? 0 : 8);
        forceLayout();
    }

    public void setCorners(int i, int i2, int i3, int i4) {
        this.cornerMask.setRadii(i, i2, i3, i4);
    }

    public void setMinimumThumbnailWidth(int i) {
        this.minimumThumbnailWidth = i;
        this.thumbnail.setMinimumThumbnailWidth(i);
    }

    public void setMaximumThumbnailHeight(int i) {
        this.maximumThumbnailHeight = i;
        this.thumbnail.setMaximumThumbnailHeight(i);
    }

    public void setBorderless(boolean z) {
        this.borderless = z;
    }

    public ConversationItemFooter getFooter() {
        return this.footer;
    }

    public void setImageResource(GlideRequests glideRequests, List<Slide> list, boolean z, boolean z2) {
        if (list.size() == 1) {
            if (list.get(0).isVideoGif()) {
                setThumbnailBounds(this.gifBounds);
            } else {
                setThumbnailBounds(this.normalBounds);
                int i = this.minimumThumbnailWidth;
                if (i != -1) {
                    this.thumbnail.setMinimumThumbnailWidth(i);
                }
                int i2 = this.maximumThumbnailHeight;
                if (i2 != -1) {
                    this.thumbnail.setMaximumThumbnailHeight(i2);
                }
            }
            this.thumbnail.setVisibility(0);
            this.album.setVisibility(8);
            Attachment asAttachment = list.get(0).asAttachment();
            this.thumbnail.setImageResource(glideRequests, list.get(0), z, z2, asAttachment.getWidth(), asAttachment.getHeight());
            setTouchDelegate(this.thumbnail.getTouchDelegate());
            return;
        }
        this.thumbnail.setVisibility(8);
        this.album.setVisibility(0);
        this.album.setSlides(glideRequests, list, z);
        setTouchDelegate(this.album.getTouchDelegate());
    }

    public void setConversationColor(int i) {
        if (this.album.getVisibility() == 0) {
            this.album.setCellBackgroundColor(i);
        }
    }

    public void setThumbnailClickListener(SlideClickListener slideClickListener) {
        this.thumbnail.setThumbnailClickListener(slideClickListener);
        this.album.setThumbnailClickListener(slideClickListener);
    }

    public void setDownloadClickListener(SlidesClickedListener slidesClickedListener) {
        this.thumbnail.setDownloadClickListener(slidesClickedListener);
        this.album.setDownloadClickListener(slidesClickedListener);
    }

    private void setThumbnailBounds(int[] iArr) {
        this.thumbnail.setBounds(iArr[0], iArr[1], iArr[2], iArr[3]);
    }
}
