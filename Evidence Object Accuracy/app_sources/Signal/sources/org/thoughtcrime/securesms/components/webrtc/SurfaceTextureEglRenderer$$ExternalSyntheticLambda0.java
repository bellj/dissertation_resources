package org.thoughtcrime.securesms.components.webrtc;

import java.util.concurrent.CountDownLatch;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class SurfaceTextureEglRenderer$$ExternalSyntheticLambda0 implements Runnable {
    public final /* synthetic */ CountDownLatch f$0;

    public /* synthetic */ SurfaceTextureEglRenderer$$ExternalSyntheticLambda0(CountDownLatch countDownLatch) {
        this.f$0 = countDownLatch;
    }

    @Override // java.lang.Runnable
    public final void run() {
        this.f$0.countDown();
    }
}
