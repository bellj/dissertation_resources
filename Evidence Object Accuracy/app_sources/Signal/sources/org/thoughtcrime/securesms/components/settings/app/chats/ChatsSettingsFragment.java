package org.thoughtcrime.securesms.components.settings.app.chats;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsViewModel;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: ChatsSettingsFragment.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\b\u0010\r\u001a\u00020\u0006H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsViewModel;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/app/chats/ChatsSettingsState;", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ChatsSettingsFragment extends DSLSettingsFragment {
    private ChatsSettingsViewModel viewModel;

    public ChatsSettingsFragment() {
        super(R.string.preferences_chats__chats, 0, 0, null, 14, null);
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        ChatsSettingsViewModel chatsSettingsViewModel = this.viewModel;
        if (chatsSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            chatsSettingsViewModel = null;
        }
        chatsSettingsViewModel.refresh();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ViewModel viewModel = new ViewModelProvider(this, new ChatsSettingsViewModel.Factory(new ChatsSettingsRepository())).get(ChatsSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(this, …ngsViewModel::class.java]");
        ChatsSettingsViewModel chatsSettingsViewModel = (ChatsSettingsViewModel) viewModel;
        this.viewModel = chatsSettingsViewModel;
        if (chatsSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            chatsSettingsViewModel = null;
        }
        chatsSettingsViewModel.getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ ChatsSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ChatsSettingsFragment.$r8$lambda$jm01Q7_M9Wy74Cu8581sZpGyZto(DSLSettingsAdapter.this, this.f$1, (ChatsSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m630bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, ChatsSettingsFragment chatsSettingsFragment, ChatsSettingsState chatsSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(chatsSettingsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(chatsSettingsState, "it");
        dSLSettingsAdapter.submitList(chatsSettingsFragment.getConfiguration(chatsSettingsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(ChatsSettingsState chatsSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(chatsSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1
            final /* synthetic */ ChatsSettingsState $state;
            final /* synthetic */ ChatsSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                DSLSettingsText.Companion companion = DSLSettingsText.Companion;
                DSLSettingsText from = companion.from(R.string.preferences__sms_mms, new DSLSettingsText.Modifier[0]);
                final ChatsSettingsFragment chatsSettingsFragment = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(chatsSettingsFragment.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_chatsSettingsFragment_to_smsSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
                dSLConfiguration.dividerPref();
                DSLSettingsText from2 = companion.from(R.string.preferences__generate_link_previews, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from3 = companion.from(R.string.preferences__retrieve_link_previews_from_websites_for_messages, new DSLSettingsText.Modifier[0]);
                boolean generateLinkPreviews = this.$state.getGenerateLinkPreviews();
                final ChatsSettingsFragment chatsSettingsFragment2 = this.this$0;
                final ChatsSettingsState chatsSettingsState2 = this.$state;
                dSLConfiguration.switchPref(from2, (r16 & 2) != 0 ? null : from3, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, generateLinkPreviews, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1.2
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ChatsSettingsViewModel access$getViewModel$p = ChatsSettingsFragment.access$getViewModel$p(chatsSettingsFragment2);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setGenerateLinkPreviewsEnabled(!chatsSettingsState2.getGenerateLinkPreviews());
                    }
                });
                DSLSettingsText from4 = companion.from(R.string.preferences__pref_use_address_book_photos, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from5 = companion.from(R.string.preferences__display_contact_photos_from_your_address_book_if_available, new DSLSettingsText.Modifier[0]);
                boolean useAddressBook = this.$state.getUseAddressBook();
                final ChatsSettingsFragment chatsSettingsFragment3 = this.this$0;
                final ChatsSettingsState chatsSettingsState3 = this.$state;
                dSLConfiguration.switchPref(from4, (r16 & 2) != 0 ? null : from5, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, useAddressBook, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ChatsSettingsViewModel access$getViewModel$p = ChatsSettingsFragment.access$getViewModel$p(chatsSettingsFragment3);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setUseAddressBook(!chatsSettingsState3.getUseAddressBook());
                    }
                });
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.ChatsSettingsFragment__keyboard);
                DSLSettingsText from6 = companion.from(R.string.preferences_advanced__use_system_emoji, new DSLSettingsText.Modifier[0]);
                boolean useSystemEmoji = this.$state.getUseSystemEmoji();
                final ChatsSettingsFragment chatsSettingsFragment4 = this.this$0;
                final ChatsSettingsState chatsSettingsState4 = this.$state;
                dSLConfiguration.switchPref(from6, (r16 & 2) != 0 ? null : null, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, useSystemEmoji, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1.4
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ChatsSettingsViewModel access$getViewModel$p = ChatsSettingsFragment.access$getViewModel$p(chatsSettingsFragment4);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setUseSystemEmoji(!chatsSettingsState4.getUseSystemEmoji());
                    }
                });
                DSLSettingsText from7 = companion.from(R.string.ChatsSettingsFragment__enter_key_sends, new DSLSettingsText.Modifier[0]);
                boolean enterKeySends = this.$state.getEnterKeySends();
                final ChatsSettingsFragment chatsSettingsFragment5 = this.this$0;
                final ChatsSettingsState chatsSettingsState5 = this.$state;
                dSLConfiguration.switchPref(from7, (r16 & 2) != 0 ? null : null, (r16 & 4) != 0 ? null : null, (r16 & 8) != 0, enterKeySends, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1.5
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ChatsSettingsViewModel access$getViewModel$p = ChatsSettingsFragment.access$getViewModel$p(chatsSettingsFragment5);
                        if (access$getViewModel$p == null) {
                            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
                            access$getViewModel$p = null;
                        }
                        access$getViewModel$p.setEnterKeySends(!chatsSettingsState5.getEnterKeySends());
                    }
                });
                dSLConfiguration.dividerPref();
                dSLConfiguration.sectionHeaderPref(R.string.preferences_chats__backups);
                DSLSettingsText from8 = companion.from(R.string.preferences_chats__chat_backups, new DSLSettingsText.Modifier[0]);
                DSLSettingsText from9 = companion.from(this.$state.getChatBackupsEnabled() ? R.string.arrays__enabled : R.string.arrays__disabled, new DSLSettingsText.Modifier[0]);
                final ChatsSettingsFragment chatsSettingsFragment6 = this.this$0;
                dSLConfiguration.clickPref(from8, (r18 & 2) != 0 ? null : from9, (r18 & 4) != 0 ? null : null, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.chats.ChatsSettingsFragment$getConfiguration$1.6
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        NavController findNavController = Navigation.findNavController(chatsSettingsFragment6.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, (int) R.id.action_chatsSettingsFragment_to_backupsPreferenceFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
            }
        });
    }
}
