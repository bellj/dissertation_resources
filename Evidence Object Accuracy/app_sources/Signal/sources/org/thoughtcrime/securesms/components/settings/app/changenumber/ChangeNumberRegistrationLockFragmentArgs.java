package org.thoughtcrime.securesms.components.settings.app.changenumber;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class ChangeNumberRegistrationLockFragmentArgs {
    private final HashMap arguments;

    private ChangeNumberRegistrationLockFragmentArgs() {
        this.arguments = new HashMap();
    }

    private ChangeNumberRegistrationLockFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static ChangeNumberRegistrationLockFragmentArgs fromBundle(Bundle bundle) {
        ChangeNumberRegistrationLockFragmentArgs changeNumberRegistrationLockFragmentArgs = new ChangeNumberRegistrationLockFragmentArgs();
        bundle.setClassLoader(ChangeNumberRegistrationLockFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("timeRemaining")) {
            changeNumberRegistrationLockFragmentArgs.arguments.put("timeRemaining", Long.valueOf(bundle.getLong("timeRemaining")));
            return changeNumberRegistrationLockFragmentArgs;
        }
        throw new IllegalArgumentException("Required argument \"timeRemaining\" is missing and does not have an android:defaultValue");
    }

    public long getTimeRemaining() {
        return ((Long) this.arguments.get("timeRemaining")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("timeRemaining")) {
            bundle.putLong("timeRemaining", ((Long) this.arguments.get("timeRemaining")).longValue());
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        ChangeNumberRegistrationLockFragmentArgs changeNumberRegistrationLockFragmentArgs = (ChangeNumberRegistrationLockFragmentArgs) obj;
        return this.arguments.containsKey("timeRemaining") == changeNumberRegistrationLockFragmentArgs.arguments.containsKey("timeRemaining") && getTimeRemaining() == changeNumberRegistrationLockFragmentArgs.getTimeRemaining();
    }

    public int hashCode() {
        return 31 + ((int) (getTimeRemaining() ^ (getTimeRemaining() >>> 32)));
    }

    public String toString() {
        return "ChangeNumberRegistrationLockFragmentArgs{timeRemaining=" + getTimeRemaining() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(ChangeNumberRegistrationLockFragmentArgs changeNumberRegistrationLockFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(changeNumberRegistrationLockFragmentArgs.arguments);
        }

        public Builder(long j) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.put("timeRemaining", Long.valueOf(j));
        }

        public ChangeNumberRegistrationLockFragmentArgs build() {
            return new ChangeNumberRegistrationLockFragmentArgs(this.arguments);
        }

        public Builder setTimeRemaining(long j) {
            this.arguments.put("timeRemaining", Long.valueOf(j));
            return this;
        }

        public long getTimeRemaining() {
            return ((Long) this.arguments.get("timeRemaining")).longValue();
        }
    }
}
