package org.thoughtcrime.securesms.components;

import android.os.Bundle;
import android.view.View;
import androidx.activity.OnBackPressedCallback;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: WrapperDialogFragment.kt */
@Metadata(d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001:\u0001\u0010B\u0005¢\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&J\u0012\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016J\u0010\u0010\t\u001a\u00020\u00062\u0006\u0010\n\u001a\u00020\u000bH\u0016J\b\u0010\f\u001a\u00020\u0006H\u0016J\u001a\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/WrapperDialogFragment;", "Landroidx/fragment/app/DialogFragment;", "()V", "getWrappedFragment", "Landroidx/fragment/app/Fragment;", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "onDismiss", "dialog", "Landroid/content/DialogInterface;", "onHandleBackPressed", "onViewCreated", "view", "Landroid/view/View;", "WrapperDialogFragmentCallback", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class WrapperDialogFragment extends DialogFragment {

    /* compiled from: WrapperDialogFragment.kt */
    @Metadata(d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H&¨\u0006\u0004"}, d2 = {"Lorg/thoughtcrime/securesms/components/WrapperDialogFragment$WrapperDialogFragmentCallback;", "", "onWrapperDialogFragmentDismissed", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public interface WrapperDialogFragmentCallback {
        void onWrapperDialogFragmentDismissed();
    }

    public abstract Fragment getWrappedFragment();

    /* JADX DEBUG: Failed to insert an additional move for type inference into block B:16:0x0020 */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r2v2, types: [androidx.fragment.app.Fragment] */
    /* JADX WARN: Type inference failed for: r2v5, types: [org.thoughtcrime.securesms.components.WrapperDialogFragment$WrapperDialogFragmentCallback] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARN: Type inference failed for: r2v10 */
    /* JADX WARN: Type inference failed for: r2v11 */
    /* JADX WARNING: Unknown variable types count: 1 */
    @Override // androidx.fragment.app.DialogFragment, android.content.DialogInterface.OnDismissListener
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDismiss(android.content.DialogInterface r2) {
        /*
            r1 = this;
            java.lang.String r0 = "dialog"
            kotlin.jvm.internal.Intrinsics.checkNotNullParameter(r2, r0)
            androidx.fragment.app.Fragment r2 = r1.getParentFragment()
        L_0x0009:
            if (r2 == 0) goto L_0x0015
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback
            if (r0 == 0) goto L_0x0010
            goto L_0x0020
        L_0x0010:
            androidx.fragment.app.Fragment r2 = r2.getParentFragment()
            goto L_0x0009
        L_0x0015:
            androidx.fragment.app.FragmentActivity r2 = r1.requireActivity()
            boolean r0 = r2 instanceof org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback
            if (r0 != 0) goto L_0x001e
            r2 = 0
        L_0x001e:
            org.thoughtcrime.securesms.components.WrapperDialogFragment$WrapperDialogFragmentCallback r2 = (org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback) r2
        L_0x0020:
            org.thoughtcrime.securesms.components.WrapperDialogFragment$WrapperDialogFragmentCallback r2 = (org.thoughtcrime.securesms.components.WrapperDialogFragment.WrapperDialogFragmentCallback) r2
            if (r2 == 0) goto L_0x0027
            r2.onWrapperDialogFragmentDismissed()
        L_0x0027:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.WrapperDialogFragment.onDismiss(android.content.DialogInterface):void");
    }

    public WrapperDialogFragment() {
        super(R.layout.fragment_container);
    }

    @Override // androidx.fragment.app.DialogFragment, androidx.fragment.app.Fragment
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(2, R.style.Signal_DayNight_Dialog_FullScreen);
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), new OnBackPressedCallback(this) { // from class: org.thoughtcrime.securesms.components.WrapperDialogFragment$onViewCreated$1
            final /* synthetic */ WrapperDialogFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // androidx.activity.OnBackPressedCallback
            public void handleOnBackPressed() {
                this.this$0.onHandleBackPressed();
            }
        });
        if (bundle == null) {
            getChildFragmentManager().beginTransaction().replace(R.id.fragment_container, getWrappedFragment()).commitAllowingStateLoss();
        }
    }

    public void onHandleBackPressed() {
        dismissAllowingStateLoss();
    }
}
