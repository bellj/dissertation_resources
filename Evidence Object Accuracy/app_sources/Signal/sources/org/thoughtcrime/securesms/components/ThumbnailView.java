package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.FitCenter;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import j$.util.Optional;
import java.util.Collections;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import org.signal.core.util.logging.Log;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.blurhash.BlurHash;
import org.thoughtcrime.securesms.mms.DecryptableStreamUriLoader;
import org.thoughtcrime.securesms.mms.GlideRequest;
import org.thoughtcrime.securesms.mms.GlideRequests;
import org.thoughtcrime.securesms.mms.Slide;
import org.thoughtcrime.securesms.mms.SlideClickListener;
import org.thoughtcrime.securesms.mms.SlidesClickedListener;
import org.thoughtcrime.securesms.stories.StoryTextPostModel;
import org.thoughtcrime.securesms.util.MediaUtil;
import org.thoughtcrime.securesms.util.Util;
import org.thoughtcrime.securesms.util.ViewUtil;
import org.thoughtcrime.securesms.util.concurrent.ListenableFuture;
import org.thoughtcrime.securesms.util.concurrent.SettableFuture;

/* loaded from: classes4.dex */
public class ThumbnailView extends FrameLayout {
    private static final int HEIGHT;
    private static final int MAX_HEIGHT;
    private static final int MAX_WIDTH;
    private static final int MIN_HEIGHT;
    private static final int MIN_WIDTH;
    private static final String TAG = Log.tag(ThumbnailView.class);
    private static final int WIDTH;
    private ImageView blurhash;
    private final int[] bounds;
    private View captionIcon;
    private final int[] dimens;
    private SlidesClickedListener downloadClickListener;
    private BitmapTransformation fit;
    private ImageView image;
    private final int[] measureDimens;
    private View.OnClickListener parentClickListener;
    private View playOverlay;
    private int radius;
    private Slide slide;
    private SlideClickListener thumbnailClickListener;
    private Optional<TransferControlView> transferControls;

    public ThumbnailView(Context context) {
        this(context, null);
    }

    public ThumbnailView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ThumbnailView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.dimens = new int[2];
        int[] iArr = new int[4];
        this.bounds = iArr;
        this.measureDimens = new int[2];
        this.transferControls = Optional.empty();
        this.thumbnailClickListener = null;
        this.downloadClickListener = null;
        this.slide = null;
        this.fit = new CenterCrop();
        FrameLayout.inflate(context, R.layout.thumbnail_view, this);
        this.image = (ImageView) findViewById(R.id.thumbnail_image);
        this.blurhash = (ImageView) findViewById(R.id.thumbnail_blurhash);
        this.playOverlay = findViewById(R.id.play_overlay);
        this.captionIcon = findViewById(R.id.thumbnail_caption_icon);
        super.setOnClickListener(new ThumbnailClickDispatcher());
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.ThumbnailView, 0, 0);
            iArr[0] = obtainStyledAttributes.getDimensionPixelSize(3, 0);
            iArr[1] = obtainStyledAttributes.getDimensionPixelSize(1, 0);
            iArr[2] = obtainStyledAttributes.getDimensionPixelSize(2, 0);
            iArr[3] = obtainStyledAttributes.getDimensionPixelSize(0, 0);
            this.radius = obtainStyledAttributes.getDimensionPixelSize(5, getResources().getDimensionPixelSize(R.dimen.thumbnail_default_radius));
            this.fit = obtainStyledAttributes.getInt(4, 0) == 1 ? new FitCenter() : new CenterCrop();
            int color = obtainStyledAttributes.getColor(6, -1);
            if (color > 0) {
                this.image.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
            } else {
                this.image.setColorFilter((ColorFilter) null);
            }
            obtainStyledAttributes.recycle();
            return;
        }
        this.radius = getResources().getDimensionPixelSize(R.dimen.message_corner_collapse_radius);
        this.image.setColorFilter((ColorFilter) null);
    }

    @Override // android.widget.FrameLayout, android.view.View
    protected void onMeasure(int i, int i2) {
        fillTargetDimensions(this.measureDimens, this.dimens, this.bounds);
        int[] iArr = this.measureDimens;
        int i3 = iArr[0];
        if (i3 == 0 && iArr[1] == 0) {
            super.onMeasure(i, i2);
            return;
        }
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3 + getPaddingLeft() + getPaddingRight(), 1073741824), View.MeasureSpec.makeMeasureSpec(this.measureDimens[1] + getPaddingTop() + getPaddingBottom(), 1073741824));
    }

    @Override // android.view.View
    protected void onSizeChanged(int i, int i2, int i3, int i4) {
        float f;
        super.onSizeChanged(i, i2, i3, i4);
        float f2 = 1.0f;
        if (this.playOverlay.getLayoutParams().width * 2 > getWidth()) {
            f2 = 0.5f;
            f = 0.0f;
        } else {
            f = 1.0f;
        }
        this.playOverlay.setScaleX(f2);
        this.playOverlay.setScaleY(f2);
        this.captionIcon.setScaleX(f);
        this.captionIcon.setScaleY(f);
    }

    public void setMinimumThumbnailWidth(int i) {
        this.bounds[0] = i;
        invalidate();
    }

    public void setMaximumThumbnailHeight(int i) {
        this.bounds[3] = i;
        invalidate();
    }

    private void fillTargetDimensions(int[] iArr, int[] iArr2, int[] iArr3) {
        double d;
        double d2;
        double d3;
        double d4;
        double d5;
        int nonZeroCount = getNonZeroCount(iArr2);
        int nonZeroCount2 = getNonZeroCount(iArr3);
        boolean z = nonZeroCount > 0 && nonZeroCount < iArr2.length;
        if (z) {
            Log.w(TAG, String.format(Locale.ENGLISH, "Width or height has been specified, but not both. Dimens: %d x %d", Integer.valueOf(iArr2[0]), Integer.valueOf(iArr2[1])));
        }
        if (z || nonZeroCount == 0 || nonZeroCount2 == 0) {
            iArr[0] = 0;
            iArr[1] = 0;
            return;
        }
        double d6 = (double) iArr2[0];
        double d7 = (double) iArr2[1];
        int i = iArr3[0];
        int i2 = iArr3[1];
        int i3 = iArr3[2];
        int i4 = iArr3[3];
        if (nonZeroCount2 <= 0 || nonZeroCount2 >= iArr3.length) {
            double d8 = (double) i;
            boolean z2 = d6 >= d8 && d6 <= ((double) i2);
            double d9 = (double) i3;
            boolean z3 = d7 >= d9 && d7 <= ((double) i4);
            if (!z2 || !z3) {
                Double.isNaN(d6);
                Double.isNaN(d8);
                double d10 = d6 / d8;
                double d11 = (double) i2;
                Double.isNaN(d6);
                Double.isNaN(d11);
                double d12 = d6 / d11;
                Double.isNaN(d7);
                Double.isNaN(d9);
                double d13 = d7 / d9;
                double d14 = (double) i4;
                Double.isNaN(d7);
                Double.isNaN(d14);
                double d15 = d7 / d14;
                if (d12 > 1.0d || d15 > 1.0d) {
                    if (d12 >= d15) {
                        Double.isNaN(d6);
                        d3 = d6 / d12;
                        Double.isNaN(d7);
                        d2 = d7 / d12;
                    } else {
                        Double.isNaN(d6);
                        d3 = d6 / d15;
                        Double.isNaN(d7);
                        d2 = d7 / d15;
                    }
                    d = Math.max(d3, d8);
                    d7 = Math.max(d2, d9);
                } else if (d10 < 1.0d || d13 < 1.0d) {
                    if (d10 <= d13) {
                        Double.isNaN(d6);
                        d5 = d6 / d10;
                        Double.isNaN(d7);
                        d4 = d7 / d10;
                    } else {
                        Double.isNaN(d6);
                        d5 = d6 / d13;
                        Double.isNaN(d7);
                        d4 = d7 / d13;
                    }
                    d = Math.min(d5, d11);
                    d7 = Math.min(d4, d14);
                }
                iArr[0] = (int) d;
                iArr[1] = (int) d7;
                return;
            }
            d = d6;
            iArr[0] = (int) d;
            iArr[1] = (int) d7;
            return;
        }
        throw new IllegalStateException(String.format(Locale.ENGLISH, "One or more min/max dimensions have been specified, but not all. Bounds: [%d, %d, %d, %d]", Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4)));
    }

    private int getNonZeroCount(int[] iArr) {
        int i = 0;
        for (int i2 : iArr) {
            if (i2 > 0) {
                i++;
            }
        }
        return i;
    }

    @Override // android.view.View
    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.parentClickListener = onClickListener;
    }

    @Override // android.view.View
    public void setFocusable(boolean z) {
        super.setFocusable(z);
        if (this.transferControls.isPresent()) {
            this.transferControls.get().setFocusable(z);
        }
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        super.setClickable(z);
        if (this.transferControls.isPresent()) {
            this.transferControls.get().setClickable(z);
        }
    }

    private TransferControlView getTransferControls() {
        if (!this.transferControls.isPresent()) {
            this.transferControls = Optional.of((TransferControlView) ViewUtil.inflateStub(this, R.id.transfer_controls_stub));
        }
        return this.transferControls.get();
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        int[] iArr = this.bounds;
        iArr[0] = i;
        iArr[1] = i2;
        iArr[2] = i3;
        iArr[3] = i4;
        forceLayout();
    }

    public ListenableFuture<Boolean> setImageResource(GlideRequests glideRequests, Slide slide, boolean z, boolean z2) {
        return setImageResource(glideRequests, slide, z, z2, 0, 0);
    }

    public ListenableFuture<Boolean> setImageResource(GlideRequests glideRequests, Slide slide, boolean z, boolean z2, int i, int i2) {
        int i3 = 8;
        if (z) {
            getTransferControls().setSlide(slide);
            getTransferControls().setDownloadClickListener(new DownloadClickDispatcher());
        } else if (this.transferControls.isPresent()) {
            getTransferControls().setVisibility(8);
        }
        boolean z3 = false;
        if (slide.getUri() == null || !slide.hasPlayOverlay() || (slide.getTransferState() != 0 && !z2)) {
            this.playOverlay.setVisibility(8);
        } else {
            this.playOverlay.setVisibility(0);
        }
        if (Util.equals(slide, this.slide)) {
            String str = TAG;
            Log.i(str, "Not re-loading slide " + slide.asAttachment().getUri());
            return new SettableFuture(Boolean.FALSE);
        }
        Slide slide2 = this.slide;
        if (slide2 == null || slide2.getFastPreflightId() == null || ((slide.hasVideo() && !Util.equals(this.slide.getUri(), slide.getUri())) || !Util.equals(this.slide.getFastPreflightId(), slide.getFastPreflightId()))) {
            String str2 = TAG;
            Log.i(str2, "loading part with id " + slide.asAttachment().getUri() + ", progress " + slide.getTransferState() + ", fast preflight id: " + slide.asAttachment().getFastPreflightId());
            Slide slide3 = this.slide;
            BlurHash placeholderBlur = slide3 != null ? slide3.getPlaceholderBlur() : null;
            this.slide = slide;
            View view = this.captionIcon;
            if (slide.getCaption().isPresent()) {
                i3 = 0;
            }
            view.setVisibility(i3);
            int[] iArr = this.dimens;
            iArr[0] = i;
            boolean z4 = true;
            iArr[1] = i2;
            invalidate();
            SettableFuture settableFuture = new SettableFuture();
            if (slide.hasPlaceholder() && (placeholderBlur == null || !Objects.equals(slide.getPlaceholderBlur(), placeholderBlur))) {
                buildPlaceholderGlideRequest(glideRequests, slide).into((RequestBuilder) new GlideBitmapListeningTarget(this.blurhash, settableFuture));
                z3 = true;
            } else if (!slide.hasPlaceholder()) {
                glideRequests.clear(this.blurhash);
                this.blurhash.setImageDrawable(null);
            }
            if (slide.getUri() != null) {
                if (!MediaUtil.isJpegType(slide.getContentType()) && !MediaUtil.isVideoType(slide.getContentType())) {
                    SettableFuture settableFuture2 = new SettableFuture();
                    settableFuture2.deferTo(settableFuture);
                    settableFuture2.addListener(new BlurhashClearListener(glideRequests, this.blurhash));
                }
                buildThumbnailGlideRequest(glideRequests, slide).into((GlideRequest) new GlideDrawableListeningTarget(this.image, settableFuture));
            } else {
                glideRequests.clear(this.image);
                this.image.setImageDrawable(null);
                z4 = z3;
            }
            if (!z4) {
                settableFuture.set(Boolean.FALSE);
            }
            return settableFuture;
        }
        String str3 = TAG;
        Log.i(str3, "Not re-loading slide for fast preflight: " + slide.getFastPreflightId());
        this.slide = slide;
        return new SettableFuture(Boolean.FALSE);
    }

    public ListenableFuture<Boolean> setImageResource(GlideRequests glideRequests, Uri uri) {
        return setImageResource(glideRequests, uri, 0, 0);
    }

    public ListenableFuture<Boolean> setImageResource(GlideRequests glideRequests, Uri uri, int i, int i2) {
        GlideRequest<Drawable> glideRequest;
        SettableFuture settableFuture = new SettableFuture();
        if (this.transferControls.isPresent()) {
            getTransferControls().setVisibility(8);
        }
        GlideRequest<Drawable> transition = glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(uri)).diskCacheStrategy(DiskCacheStrategy.NONE).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade());
        if (i > 0 && i2 > 0) {
            transition = transition.override(i, i2);
        }
        if (this.radius > 0) {
            glideRequest = transition.transforms(new CenterCrop(), new RoundedCorners(this.radius));
        } else {
            glideRequest = transition.transforms(new CenterCrop());
        }
        glideRequest.into((GlideRequest<Drawable>) new GlideDrawableListeningTarget(this.image, settableFuture));
        this.blurhash.setImageDrawable(null);
        return settableFuture;
    }

    public ListenableFuture<Boolean> setImageResource(GlideRequests glideRequests, StoryTextPostModel storyTextPostModel, int i, int i2) {
        GlideRequest<Drawable> glideRequest;
        SettableFuture settableFuture = new SettableFuture();
        if (this.transferControls.isPresent()) {
            getTransferControls().setVisibility(8);
        }
        GlideRequest<Drawable> transition = glideRequests.load((Object) storyTextPostModel).diskCacheStrategy(DiskCacheStrategy.NONE).placeholder(storyTextPostModel.getPlaceholder()).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade());
        if (i > 0 && i2 > 0) {
            transition = transition.override(i, i2);
        }
        if (this.radius > 0) {
            glideRequest = transition.transforms(new CenterCrop(), new RoundedCorners(this.radius));
        } else {
            glideRequest = transition.transforms(new CenterCrop());
        }
        glideRequest.into((GlideRequest<Drawable>) new GlideDrawableListeningTarget(this.image, settableFuture));
        this.blurhash.setImageDrawable(null);
        return settableFuture;
    }

    public void setThumbnailClickListener(SlideClickListener slideClickListener) {
        this.thumbnailClickListener = slideClickListener;
    }

    public void setDownloadClickListener(SlidesClickedListener slidesClickedListener) {
        this.downloadClickListener = slidesClickedListener;
    }

    public void clear(GlideRequests glideRequests) {
        glideRequests.clear(this.image);
        this.image.setImageDrawable(null);
        if (this.transferControls.isPresent()) {
            getTransferControls().clear();
        }
        glideRequests.clear(this.blurhash);
        this.blurhash.setImageDrawable(null);
        this.slide = null;
    }

    public void showDownloadText(boolean z) {
        getTransferControls().setShowDownloadText(z);
    }

    public void showProgressSpinner() {
        getTransferControls().showProgressSpinner();
    }

    public void setFit(BitmapTransformation bitmapTransformation) {
        this.fit = bitmapTransformation;
    }

    public void setRadius(int i) {
        this.radius = i;
    }

    private GlideRequest buildThumbnailGlideRequest(GlideRequests glideRequests, Slide slide) {
        GlideRequest applySizing = applySizing(glideRequests.load((Object) new DecryptableStreamUriLoader.DecryptableUri(slide.getUri())).diskCacheStrategy(DiskCacheStrategy.RESOURCE).transition((TransitionOptions<?, ? super Drawable>) DrawableTransitionOptions.withCrossFade()), this.fit);
        return (slide.isInProgress() || (Build.VERSION.SDK_INT < 23)) ? applySizing : applySizing.apply((BaseRequestOptions<?>) RequestOptions.errorOf((int) R.drawable.ic_missing_thumbnail_picture));
    }

    private RequestBuilder buildPlaceholderGlideRequest(GlideRequests glideRequests, Slide slide) {
        GlideRequest<Bitmap> glideRequest;
        GlideRequest<Bitmap> asBitmap = glideRequests.asBitmap();
        BlurHash placeholderBlur = slide.getPlaceholderBlur();
        if (placeholderBlur != null) {
            glideRequest = asBitmap.load((Object) placeholderBlur);
        } else {
            glideRequest = asBitmap.load(Integer.valueOf(slide.getPlaceholderRes(getContext().getTheme())));
        }
        return applySizing(glideRequest.diskCacheStrategy(DiskCacheStrategy.NONE), new CenterCrop());
    }

    private GlideRequest applySizing(GlideRequest glideRequest, BitmapTransformation bitmapTransformation) {
        int[] iArr = new int[2];
        fillTargetDimensions(iArr, this.dimens, this.bounds);
        if (iArr[0] == 0 && iArr[1] == 0) {
            iArr[0] = getDefaultWidth();
            iArr[1] = getDefaultHeight();
        }
        GlideRequest override = glideRequest.override(iArr[0], iArr[1]);
        if (this.radius > 0) {
            return override.transforms(bitmapTransformation, new RoundedCorners(this.radius));
        }
        return override.transforms(bitmapTransformation);
    }

    private int getDefaultWidth() {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            return Math.max(layoutParams.width, 0);
        }
        return 0;
    }

    private int getDefaultHeight() {
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            return Math.max(layoutParams.height, 0);
        }
        return 0;
    }

    /* access modifiers changed from: private */
    /* loaded from: classes4.dex */
    public class ThumbnailClickDispatcher implements View.OnClickListener {
        private ThumbnailClickDispatcher() {
            ThumbnailView.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            if (ThumbnailView.this.thumbnailClickListener != null && ThumbnailView.this.slide != null && ThumbnailView.this.slide.asAttachment().getUri() != null && ThumbnailView.this.slide.getTransferState() == 0) {
                ThumbnailView.this.thumbnailClickListener.onClick(view, ThumbnailView.this.slide);
            } else if (ThumbnailView.this.parentClickListener != null) {
                ThumbnailView.this.parentClickListener.onClick(view);
            }
        }
    }

    /* loaded from: classes4.dex */
    public class DownloadClickDispatcher implements View.OnClickListener {
        private DownloadClickDispatcher() {
            ThumbnailView.this = r1;
        }

        @Override // android.view.View.OnClickListener
        public void onClick(View view) {
            Log.i(ThumbnailView.TAG, "onClick() for download button");
            if (ThumbnailView.this.downloadClickListener == null || ThumbnailView.this.slide == null) {
                String str = ThumbnailView.TAG;
                Log.w(str, "Received a download button click, but unable to execute it. slide: " + String.valueOf(ThumbnailView.this.slide) + "  downloadClickListener: " + String.valueOf(ThumbnailView.this.downloadClickListener));
                return;
            }
            ThumbnailView.this.downloadClickListener.onClick(view, Collections.singletonList(ThumbnailView.this.slide));
        }
    }

    /* loaded from: classes4.dex */
    public static class BlurhashClearListener implements ListenableFuture.Listener<Boolean> {
        private final ImageView blurhash;
        private final GlideRequests glideRequests;

        private BlurhashClearListener(GlideRequests glideRequests, ImageView imageView) {
            this.glideRequests = glideRequests;
            this.blurhash = imageView;
        }

        public void onSuccess(Boolean bool) {
            this.glideRequests.clear(this.blurhash);
            this.blurhash.setImageDrawable(null);
        }

        @Override // org.thoughtcrime.securesms.util.concurrent.ListenableFuture.Listener
        public void onFailure(ExecutionException executionException) {
            this.glideRequests.clear(this.blurhash);
            this.blurhash.setImageDrawable(null);
        }
    }
}
