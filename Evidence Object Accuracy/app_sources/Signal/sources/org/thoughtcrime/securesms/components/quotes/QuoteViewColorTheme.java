package org.thoughtcrime.securesms.components.quotes;

import android.content.Context;
import androidx.core.content.ContextCompat;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: QuoteViewColorTheme.kt */
@Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\b\u0001\u0018\u0000 \u00102\b\u0012\u0004\u0012\u00020\u00000\u0001:\u0001\u0010B\u001f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\u000e\u0010\u0007\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\n\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tJ\u000e\u0010\u000b\u001a\u00020\u00032\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000j\u0002\b\fj\u0002\b\rj\u0002\b\u000ej\u0002\b\u000f¨\u0006\u0011"}, d2 = {"Lorg/thoughtcrime/securesms/components/quotes/QuoteViewColorTheme;", "", "backgroundColorRes", "", "barColorRes", "foregroundColorRes", "(Ljava/lang/String;IIII)V", "getBackgroundColor", "context", "Landroid/content/Context;", "getBarColor", "getForegroundColor", "INCOMING_WALLPAPER", "INCOMING_NORMAL", "OUTGOING_WALLPAPER", "OUTGOING_NORMAL", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public enum QuoteViewColorTheme {
    INCOMING_WALLPAPER(R.color.quote_view_background_incoming_wallpaper, R.color.quote_view_bar_incoming_wallpaper, R.color.quote_view_foreground_incoming_wallpaper),
    INCOMING_NORMAL(R.color.quote_view_background_incoming_normal, R.color.quote_view_bar_incoming_normal, R.color.quote_view_foreground_incoming_normal),
    OUTGOING_WALLPAPER(R.color.quote_view_background_outgoing_wallpaper, R.color.quote_view_bar_outgoing_wallpaper, R.color.quote_view_foreground_outgoing_wallpaper),
    OUTGOING_NORMAL(R.color.quote_view_background_outgoing_normal, R.color.quote_view_bar_outgoing_normal, R.color.quote_view_foreground_outgoing_normal);
    
    public static final Companion Companion = new Companion(null);
    private final int backgroundColorRes;
    private final int barColorRes;
    private final int foregroundColorRes;

    @JvmStatic
    public static final QuoteViewColorTheme resolveTheme(boolean z, boolean z2, boolean z3) {
        return Companion.resolveTheme(z, z2, z3);
    }

    QuoteViewColorTheme(int i, int i2, int i3) {
        this.backgroundColorRes = i;
        this.barColorRes = i2;
        this.foregroundColorRes = i3;
    }

    public final int getBackgroundColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.backgroundColorRes);
    }

    public final int getBarColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.barColorRes);
    }

    public final int getForegroundColor(Context context) {
        Intrinsics.checkNotNullParameter(context, "context");
        return ContextCompat.getColor(context, this.foregroundColorRes);
    }

    /* compiled from: QuoteViewColorTheme.kt */
    @Metadata(d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J \u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\u0006H\u0007¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/quotes/QuoteViewColorTheme$Companion;", "", "()V", "resolveTheme", "Lorg/thoughtcrime/securesms/components/quotes/QuoteViewColorTheme;", "isOutgoing", "", "isPreview", "hasWallpaper", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final QuoteViewColorTheme resolveTheme(boolean z, boolean z2, boolean z3) {
            if (z2 && z3) {
                return QuoteViewColorTheme.INCOMING_WALLPAPER;
            }
            if (z2 && !z3) {
                return QuoteViewColorTheme.INCOMING_NORMAL;
            }
            if (z && z3) {
                return QuoteViewColorTheme.OUTGOING_WALLPAPER;
            }
            if (!z && z3) {
                return QuoteViewColorTheme.INCOMING_WALLPAPER;
            }
            if (!z || z3) {
                return QuoteViewColorTheme.INCOMING_NORMAL;
            }
            return QuoteViewColorTheme.OUTGOING_NORMAL;
        }
    }
}
