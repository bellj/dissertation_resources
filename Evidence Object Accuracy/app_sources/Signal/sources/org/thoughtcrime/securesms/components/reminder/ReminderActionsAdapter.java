package org.thoughtcrime.securesms.components.reminder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Collections;
import java.util.List;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.reminder.Reminder;
import org.thoughtcrime.securesms.components.reminder.ReminderActionsAdapter;
import org.thoughtcrime.securesms.components.reminder.ReminderView;

/* loaded from: classes4.dex */
public final class ReminderActionsAdapter extends RecyclerView.Adapter<ActionViewHolder> {
    private final ReminderView.OnActionClickListener actionClickListener;
    private final List<Reminder.Action> actions;
    private final Reminder.Importance importance;

    public ReminderActionsAdapter(Reminder.Importance importance, List<Reminder.Action> list, ReminderView.OnActionClickListener onActionClickListener) {
        this.importance = importance;
        this.actions = Collections.unmodifiableList(list);
        this.actionClickListener = onActionClickListener;
    }

    public ActionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.reminder_action_button, viewGroup, false);
        if (this.importance == Reminder.Importance.NORMAL) {
            textView.setTextColor(ContextCompat.getColor(context, R.color.signal_accent_primary));
        }
        return new ActionViewHolder(textView);
    }

    public void onBindViewHolder(ActionViewHolder actionViewHolder, int i) {
        Reminder.Action action = this.actions.get(i);
        ((Button) actionViewHolder.itemView).setText(action.getTitle());
        actionViewHolder.itemView.setOnClickListener(new View.OnClickListener(actionViewHolder, action) { // from class: org.thoughtcrime.securesms.components.reminder.ReminderActionsAdapter$$ExternalSyntheticLambda0
            public final /* synthetic */ ReminderActionsAdapter.ActionViewHolder f$1;
            public final /* synthetic */ Reminder.Action f$2;

            {
                this.f$1 = r2;
                this.f$2 = r3;
            }

            @Override // android.view.View.OnClickListener
            public final void onClick(View view) {
                ReminderActionsAdapter.$r8$lambda$n8YslpRhEu7EATujb7FVWPO7gOI(ReminderActionsAdapter.this, this.f$1, this.f$2, view);
            }
        });
    }

    public /* synthetic */ void lambda$onBindViewHolder$0(ActionViewHolder actionViewHolder, Reminder.Action action, View view) {
        if (actionViewHolder.getAdapterPosition() != -1) {
            this.actionClickListener.onActionClick(action.getActionId());
        }
    }

    @Override // androidx.recyclerview.widget.RecyclerView.Adapter
    public int getItemCount() {
        return this.actions.size();
    }

    /* loaded from: classes4.dex */
    public final class ActionViewHolder extends RecyclerView.ViewHolder {
        /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
        ActionViewHolder(View view) {
            super(view);
            ReminderActionsAdapter.this = r1;
        }
    }
}
