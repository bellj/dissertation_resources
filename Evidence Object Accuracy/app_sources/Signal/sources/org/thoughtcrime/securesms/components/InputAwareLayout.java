package org.thoughtcrime.securesms.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout;
import org.thoughtcrime.securesms.util.ServiceUtil;

/* loaded from: classes4.dex */
public class InputAwareLayout extends KeyboardAwareLinearLayout implements KeyboardAwareLinearLayout.OnKeyboardShownListener {
    private InputView current;

    /* loaded from: classes4.dex */
    public interface InputView {
        void hide(boolean z);

        boolean isShowing();

        void show(int i, boolean z);
    }

    @Override // org.thoughtcrime.securesms.components.KeyboardAwareLinearLayout.OnKeyboardShownListener
    public void onKeyboardShown() {
    }

    public InputAwareLayout(Context context) {
        this(context, null);
    }

    public InputAwareLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public InputAwareLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        addOnKeyboardShownListener(this);
    }

    public void show(EditText editText, final InputView inputView) {
        if (isKeyboardOpen()) {
            hideSoftkey(editText, new Runnable() { // from class: org.thoughtcrime.securesms.components.InputAwareLayout.1
                @Override // java.lang.Runnable
                public void run() {
                    InputAwareLayout.this.hideAttachedInput(true);
                    inputView.show(InputAwareLayout.this.getKeyboardHeight(), true);
                    InputAwareLayout.this.current = inputView;
                }
            });
            return;
        }
        InputView inputView2 = this.current;
        boolean z = true;
        if (inputView2 != null) {
            inputView2.hide(true);
        }
        int keyboardHeight = getKeyboardHeight();
        if (this.current == null) {
            z = false;
        }
        inputView.show(keyboardHeight, z);
        this.current = inputView;
    }

    public InputView getCurrentInput() {
        return this.current;
    }

    public void hideCurrentInput(EditText editText) {
        if (isKeyboardOpen()) {
            hideSoftkey(editText, null);
        } else {
            hideAttachedInput(false);
        }
    }

    public void hideAttachedInput(boolean z) {
        InputView inputView = this.current;
        if (inputView != null) {
            inputView.hide(z);
        }
        this.current = null;
    }

    public boolean isInputOpen() {
        InputView inputView;
        return isKeyboardOpen() || ((inputView = this.current) != null && inputView.isShowing());
    }

    public void showSoftkey(final EditText editText) {
        postOnKeyboardOpen(new Runnable() { // from class: org.thoughtcrime.securesms.components.InputAwareLayout.2
            @Override // java.lang.Runnable
            public void run() {
                InputAwareLayout.this.hideAttachedInput(true);
            }
        });
        editText.post(new Runnable() { // from class: org.thoughtcrime.securesms.components.InputAwareLayout.3
            @Override // java.lang.Runnable
            public void run() {
                editText.requestFocus();
                ServiceUtil.getInputMethodManager(editText.getContext()).showSoftInput(editText, 0);
            }
        });
    }

    public void hideSoftkey(EditText editText, Runnable runnable) {
        if (runnable != null) {
            postOnKeyboardClose(runnable);
        }
        ServiceUtil.getInputMethodManager(editText.getContext()).hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }
}
