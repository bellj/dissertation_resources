package org.thoughtcrime.securesms.components.settings.conversation.sounds;

import android.content.Context;
import android.content.DialogInterface;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.thoughtcrime.securesms.MuteDialog;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.Utils;
import org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragmentDirections;
import org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsViewModel;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.recipients.RecipientId;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;

/* compiled from: SoundsAndNotificationsSettingsFragment.kt */
@Metadata(d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u0010\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0010H\u0016R!\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u00048BX\u0002¢\u0006\f\n\u0004\b\b\u0010\t\u001a\u0004\b\u0006\u0010\u0007R\u001b\u0010\n\u001a\u00020\u000b8BX\u0002¢\u0006\f\n\u0004\b\u000e\u0010\t\u001a\u0004\b\f\u0010\r¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "()V", "mentionLabels", "", "", "getMentionLabels", "()[Ljava/lang/String;", "mentionLabels$delegate", "Lkotlin/Lazy;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsViewModel;", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsViewModel;", "viewModel$delegate", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "state", "Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/SoundsAndNotificationsSettingsState;", "onResume", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SoundsAndNotificationsSettingsFragment extends DSLSettingsFragment {
    private final Lazy mentionLabels$delegate = LazyKt__LazyJVMKt.lazy(new Function0<String[]>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$mentionLabels$2
        final /* synthetic */ SoundsAndNotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final String[] invoke() {
            return this.this$0.getResources().getStringArray(R.array.SoundsAndNotificationsSettingsFragment__mention_labels);
        }
    });
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(SoundsAndNotificationsSettingsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, new Function0<ViewModelProvider.Factory>(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$viewModel$2
        final /* synthetic */ SoundsAndNotificationsSettingsFragment this$0;

        /* access modifiers changed from: package-private */
        {
            this.this$0 = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelProvider.Factory invoke() {
            RecipientId recipientId = SoundsAndNotificationsSettingsFragmentArgs.fromBundle(this.this$0.requireArguments()).getRecipientId();
            Intrinsics.checkNotNullExpressionValue(recipientId, "fromBundle(requireArguments()).recipientId");
            Context requireContext = this.this$0.requireContext();
            Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
            return new SoundsAndNotificationsSettingsViewModel.Factory(recipientId, new SoundsAndNotificationsSettingsRepository(requireContext));
        }
    });

    public SoundsAndNotificationsSettingsFragment() {
        super(R.string.ConversationSettingsFragment__sounds_and_notifications, 0, 0, null, 14, null);
    }

    public final String[] getMentionLabels() {
        Object value = this.mentionLabels$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-mentionLabels>(...)");
        return (String[]) value;
    }

    public final SoundsAndNotificationsSettingsViewModel getViewModel() {
        return (SoundsAndNotificationsSettingsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().channelConsistencyCheck();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ SoundsAndNotificationsSettingsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                SoundsAndNotificationsSettingsFragment.$r8$lambda$iq7BjifUo4rRbgTxiMWBmAvc0Dc(DSLSettingsAdapter.this, this.f$1, (SoundsAndNotificationsSettingsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m1216bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, SoundsAndNotificationsSettingsFragment soundsAndNotificationsSettingsFragment, SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsFragment, "this$0");
        if (soundsAndNotificationsSettingsState.getChannelConsistencyCheckComplete() && !Intrinsics.areEqual(soundsAndNotificationsSettingsState.getRecipientId(), Recipient.UNKNOWN.getId())) {
            Intrinsics.checkNotNullExpressionValue(soundsAndNotificationsSettingsState, "state");
            dSLSettingsAdapter.submitList(soundsAndNotificationsSettingsFragment.getConfiguration(soundsAndNotificationsSettingsState).toMappingModelList());
        }
    }

    private final DSLConfiguration getConfiguration(SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(soundsAndNotificationsSettingsState, this) { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$getConfiguration$1
            final /* synthetic */ SoundsAndNotificationsSettingsState $state;
            final /* synthetic */ SoundsAndNotificationsSettingsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                final String str;
                DSLSettingsIcon.Companion companion;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                if (this.$state.getMuteUntil() > 0) {
                    Utils utils = Utils.INSTANCE;
                    long muteUntil = this.$state.getMuteUntil();
                    Context requireContext = this.this$0.requireContext();
                    Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                    str = utils.formatMutedUntil(muteUntil, requireContext);
                } else {
                    str = this.this$0.getString(R.string.SoundsAndNotificationsSettingsFragment__not_muted);
                    Intrinsics.checkNotNullExpressionValue(str, "{\n        getString(R.st…gment__not_muted)\n      }");
                }
                int i = this.$state.getMuteUntil() > 0 ? R.drawable.ic_bell_disabled_24 : R.drawable.ic_bell_24;
                DSLSettingsText.Companion companion2 = DSLSettingsText.Companion;
                DSLSettingsText from = companion2.from(R.string.SoundsAndNotificationsSettingsFragment__mute_notifications, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon.Companion companion3 = DSLSettingsIcon.Companion;
                DSLSettingsIcon from$default = DSLSettingsIcon.Companion.from$default(companion3, i, 0, 2, null);
                DSLSettingsText from2 = companion2.from(str, new DSLSettingsText.Modifier[0]);
                final SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState2 = this.$state;
                final SoundsAndNotificationsSettingsFragment soundsAndNotificationsSettingsFragment = this.this$0;
                dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : from2, (r18 & 4) != 0 ? null : from$default, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$getConfiguration$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        if (soundsAndNotificationsSettingsState2.getMuteUntil() <= 0) {
                            MuteDialog.show(soundsAndNotificationsSettingsFragment.requireContext(), new SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda0(SoundsAndNotificationsSettingsFragment.access$getViewModel(soundsAndNotificationsSettingsFragment)));
                        } else {
                            new MaterialAlertDialogBuilder(soundsAndNotificationsSettingsFragment.requireContext()).setMessage((CharSequence) str).setPositiveButton(R.string.ConversationSettingsFragment__unmute, (DialogInterface.OnClickListener) new SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda1(soundsAndNotificationsSettingsFragment)).setNegativeButton(17039360, (DialogInterface.OnClickListener) new SoundsAndNotificationsSettingsFragment$getConfiguration$1$1$$ExternalSyntheticLambda2()).show();
                        }
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-0  reason: not valid java name */
                    public static final void m1218invoke$lambda0(SoundsAndNotificationsSettingsFragment soundsAndNotificationsSettingsFragment2, DialogInterface dialogInterface, int i2) {
                        Intrinsics.checkNotNullParameter(soundsAndNotificationsSettingsFragment2, "this$0");
                        SoundsAndNotificationsSettingsFragment.access$getViewModel(soundsAndNotificationsSettingsFragment2).unmute();
                        dialogInterface.dismiss();
                    }

                    /* access modifiers changed from: private */
                    /* renamed from: invoke$lambda-1  reason: not valid java name */
                    public static final void m1219invoke$lambda1(DialogInterface dialogInterface, int i2) {
                        dialogInterface.dismiss();
                    }
                }, (r18 & 64) != 0 ? null : null);
                if (this.$state.getHasMentionsSupport()) {
                    int i2 = this.$state.getMentionSetting() == RecipientDatabase.MentionSetting.ALWAYS_NOTIFY ? 0 : 1;
                    DSLSettingsText from3 = companion2.from(R.string.SoundsAndNotificationsSettingsFragment__mentions, new DSLSettingsText.Modifier[0]);
                    DSLSettingsIcon from$default2 = DSLSettingsIcon.Companion.from$default(companion3, R.drawable.ic_at_24, 0, 2, null);
                    String[] access$getMentionLabels = SoundsAndNotificationsSettingsFragment.access$getMentionLabels(this.this$0);
                    final SoundsAndNotificationsSettingsFragment soundsAndNotificationsSettingsFragment2 = this.this$0;
                    companion = companion3;
                    dSLConfiguration.radioListPref(from3, (r19 & 2) != 0 ? null : from$default2, (r19 & 4) != 0 ? from3 : null, (r19 & 8) != 0, access$getMentionLabels, i2, (r19 & 64) != 0 ? false : false, new Function1<Integer, Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$getConfiguration$1.2
                        /* Return type fixed from 'java.lang.Object' to match base method */
                        /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
                        @Override // kotlin.jvm.functions.Function1
                        public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
                            invoke(num.intValue());
                            return Unit.INSTANCE;
                        }

                        public final void invoke(int i3) {
                            RecipientDatabase.MentionSetting mentionSetting;
                            SoundsAndNotificationsSettingsViewModel access$getViewModel = SoundsAndNotificationsSettingsFragment.access$getViewModel(soundsAndNotificationsSettingsFragment2);
                            if (i3 == 0) {
                                mentionSetting = RecipientDatabase.MentionSetting.ALWAYS_NOTIFY;
                            } else {
                                mentionSetting = RecipientDatabase.MentionSetting.DO_NOT_NOTIFY;
                            }
                            access$getViewModel.setMentionSetting(mentionSetting);
                        }
                    });
                } else {
                    companion = companion3;
                }
                int i3 = this.$state.getHasCustomNotificationSettings() ? R.string.preferences_on : R.string.preferences_off;
                DSLSettingsText from4 = companion2.from(R.string.SoundsAndNotificationsSettingsFragment__custom_notifications, new DSLSettingsText.Modifier[0]);
                DSLSettingsIcon from$default3 = DSLSettingsIcon.Companion.from$default(companion, R.drawable.ic_speaker_24, 0, 2, null);
                DSLSettingsText from5 = companion2.from(i3, new DSLSettingsText.Modifier[0]);
                final SoundsAndNotificationsSettingsState soundsAndNotificationsSettingsState3 = this.$state;
                final SoundsAndNotificationsSettingsFragment soundsAndNotificationsSettingsFragment3 = this.this$0;
                dSLConfiguration.clickPref(from4, (r18 & 2) != 0 ? null : from5, (r18 & 4) != 0 ? null : from$default3, (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.conversation.sounds.SoundsAndNotificationsSettingsFragment$getConfiguration$1.3
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        SoundsAndNotificationsSettingsFragmentDirections.ActionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment = SoundsAndNotificationsSettingsFragmentDirections.actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment(soundsAndNotificationsSettingsState3.getRecipientId());
                        Intrinsics.checkNotNullExpressionValue(actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment, "actionSoundsAndNotificat…agment(state.recipientId)");
                        NavController findNavController = Navigation.findNavController(soundsAndNotificationsSettingsFragment3.requireView());
                        Intrinsics.checkNotNullExpressionValue(findNavController, "findNavController(requireView())");
                        SafeNavigation.safeNavigate(findNavController, actionSoundsAndNotificationsSettingsFragmentToCustomNotificationsSettingsFragment);
                    }
                }, (r18 & 64) != 0 ? null : null);
            }
        });
    }
}
