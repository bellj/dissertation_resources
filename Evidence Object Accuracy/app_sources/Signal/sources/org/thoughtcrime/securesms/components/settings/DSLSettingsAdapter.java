package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import j$.util.function.Function;
import kotlin.Metadata;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.components.settings.models.AsyncSwitch;
import org.thoughtcrime.securesms.components.settings.models.Button;
import org.thoughtcrime.securesms.components.settings.models.Space;
import org.thoughtcrime.securesms.components.settings.models.Text;
import org.thoughtcrime.securesms.util.adapter.mapping.LayoutFactory;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingAdapter;

/* compiled from: DSLSettingsAdapter.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingAdapter;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DSLSettingsAdapter extends MappingAdapter {
    public DSLSettingsAdapter() {
        registerFactory(ClickPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda0
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ClickPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        registerFactory(LongClickPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda2
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new LongClickPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        registerFactory(TextPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda3
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new TextPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        registerFactory(LearnMoreTextPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda4
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new LearnMoreTextPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_learn_more_preference_item));
        registerFactory(RadioListPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda5
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new RadioListPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        registerFactory(MultiSelectListPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda6
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new MultiSelectListPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        registerFactory(ExternalLinkPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda7
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new ExternalLinkPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_preference_item));
        registerFactory(DividerPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda8
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new DividerPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_divider_item));
        registerFactory(SectionHeaderPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda9
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SectionHeaderPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_section_header));
        registerFactory(SwitchPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda10
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new SwitchPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_switch_preference_item));
        registerFactory(RadioPreference.class, new LayoutFactory(new Function() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter$$ExternalSyntheticLambda1
            @Override // j$.util.function.Function
            public /* synthetic */ Function andThen(Function function) {
                return Function.CC.$default$andThen(this, function);
            }

            @Override // j$.util.function.Function
            public final Object apply(Object obj) {
                return new RadioPreferenceViewHolder((View) obj);
            }

            @Override // j$.util.function.Function
            public /* synthetic */ Function compose(Function function) {
                return Function.CC.$default$compose(this, function);
            }
        }, R.layout.dsl_radio_preference_item));
        Text.Companion.register(this);
        Space.Companion.register(this);
        Button.INSTANCE.register(this);
        AsyncSwitch.INSTANCE.register(this);
    }
}
