package org.thoughtcrime.securesms.components.voice;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.JvmStatic;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.MediaPreviewActivity;
import org.thoughtcrime.securesms.database.DraftDatabase;

/* compiled from: VoiceNoteDraft.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\u0006\u0010\u000b\u001a\u00020\fR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNoteDraft;", "", "uri", "Landroid/net/Uri;", MediaPreviewActivity.SIZE_EXTRA, "", "(Landroid/net/Uri;J)V", "getSize", "()J", "getUri", "()Landroid/net/Uri;", "asDraft", "Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class VoiceNoteDraft {
    public static final Companion Companion = new Companion(null);
    private final long size;
    private final Uri uri;

    @JvmStatic
    public static final VoiceNoteDraft fromDraft(DraftDatabase.Draft draft) {
        return Companion.fromDraft(draft);
    }

    public VoiceNoteDraft(Uri uri, long j) {
        Intrinsics.checkNotNullParameter(uri, "uri");
        this.uri = uri;
        this.size = j;
    }

    public final Uri getUri() {
        return this.uri;
    }

    public final long getSize() {
        return this.size;
    }

    /* compiled from: VoiceNoteDraft.kt */
    @Metadata(d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0007¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/voice/VoiceNoteDraft$Companion;", "", "()V", "fromDraft", "Lorg/thoughtcrime/securesms/components/voice/VoiceNoteDraft;", "draft", "Lorg/thoughtcrime/securesms/database/DraftDatabase$Draft;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        @JvmStatic
        public final VoiceNoteDraft fromDraft(DraftDatabase.Draft draft) {
            Intrinsics.checkNotNullParameter(draft, "draft");
            if (Intrinsics.areEqual(draft.getType(), DraftDatabase.Draft.VOICE_NOTE)) {
                Uri parse = Uri.parse(draft.getValue());
                Uri build = parse.buildUpon().clearQuery().build();
                Intrinsics.checkNotNullExpressionValue(build, "draftUri.buildUpon().clearQuery().build()");
                String queryParameter = parse.getQueryParameter(MediaPreviewActivity.SIZE_EXTRA);
                Intrinsics.checkNotNull(queryParameter);
                return new VoiceNoteDraft(build, Long.parseLong(queryParameter));
            }
            throw new IllegalArgumentException();
        }
    }

    public final DraftDatabase.Draft asDraft() {
        return new DraftDatabase.Draft(DraftDatabase.Draft.VOICE_NOTE, this.uri.buildUpon().appendQueryParameter(MediaPreviewActivity.SIZE_EXTRA, String.valueOf(this.size)).build().toString());
    }
}
