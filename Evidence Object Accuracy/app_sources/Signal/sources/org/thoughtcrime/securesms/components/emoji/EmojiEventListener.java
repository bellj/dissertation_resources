package org.thoughtcrime.securesms.components.emoji;

import android.view.KeyEvent;

/* loaded from: classes4.dex */
public interface EmojiEventListener {
    void onEmojiSelected(String str);

    void onKeyEvent(KeyEvent keyEvent);
}
