package org.thoughtcrime.securesms.components.emoji;

import android.view.View;
import org.thoughtcrime.securesms.components.emoji.EmojiPageViewGridAdapter;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class EmojiPageViewGridAdapter$EmojiTextViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ EmojiPageViewGridAdapter.EmojiTextViewHolder f$0;
    public final /* synthetic */ EmojiPageViewGridAdapter.EmojiTextModel f$1;

    public /* synthetic */ EmojiPageViewGridAdapter$EmojiTextViewHolder$$ExternalSyntheticLambda0(EmojiPageViewGridAdapter.EmojiTextViewHolder emojiTextViewHolder, EmojiPageViewGridAdapter.EmojiTextModel emojiTextModel) {
        this.f$0 = emojiTextViewHolder;
        this.f$1 = emojiTextModel;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        this.f$0.lambda$bind$0(this.f$1, view);
    }
}
