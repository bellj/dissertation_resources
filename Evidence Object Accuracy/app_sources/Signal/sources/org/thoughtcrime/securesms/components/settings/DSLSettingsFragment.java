package org.thoughtcrime.securesms.components.settings;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EdgeEffect;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import kotlin.Metadata;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.Material3OnScrollHelper;

/* compiled from: DSLSettingsFragment.kt */
@Metadata(d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001:\u0001#B9\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0004\u001a\u00020\u0003\u0012\b\b\u0003\u0010\u0005\u001a\u00020\u0003\u0012\u0014\b\u0002\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007¢\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H&J\u0014\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J\b\u0010\u001c\u001a\u00020\u0015H\u0016J\b\u0010\u001d\u001a\u00020\u0015H\u0016J\u001a\u0010\u001e\u001a\u00020\u00152\u0006\u0010\u001f\u001a\u00020 2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0017R&\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\t0\u0007X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u000e\u0010\u0004\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\"\u0010\u0011\u001a\u0004\u0018\u00010\u00102\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010@BX\u000e¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006$"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "Landroidx/fragment/app/Fragment;", "titleId", "", "menuId", "layoutId", "layoutManagerProducer", "Lkotlin/Function1;", "Landroid/content/Context;", "Landroidx/recyclerview/widget/RecyclerView$LayoutManager;", "(IIILkotlin/jvm/functions/Function1;)V", "getLayoutManagerProducer", "()Lkotlin/jvm/functions/Function1;", "setLayoutManagerProducer", "(Lkotlin/jvm/functions/Function1;)V", "<set-?>", "Landroidx/recyclerview/widget/RecyclerView;", "recyclerView", "getRecyclerView", "()Landroidx/recyclerview/widget/RecyclerView;", "bindAdapter", "", "adapter", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "getMaterial3OnScrollHelper", "Lorg/thoughtcrime/securesms/util/Material3OnScrollHelper;", "toolbar", "Landroidx/appcompat/widget/Toolbar;", "onDestroyView", "onToolbarNavigationClicked", "onViewCreated", "view", "Landroid/view/View;", "savedInstanceState", "Landroid/os/Bundle;", "EdgeEffectFactory", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class DSLSettingsFragment extends Fragment {
    private Function1<? super Context, ? extends RecyclerView.LayoutManager> layoutManagerProducer;
    private final int menuId;
    private RecyclerView recyclerView;
    private final int titleId;

    public DSLSettingsFragment() {
        this(0, 0, 0, null, 15, null);
    }

    public abstract void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter);

    public /* synthetic */ DSLSettingsFragment(int i, int i2, int i3, Function1 function1, int i4, DefaultConstructorMarker defaultConstructorMarker) {
        this((i4 & 1) != 0 ? -1 : i, (i4 & 2) != 0 ? -1 : i2, (i4 & 4) != 0 ? R.layout.dsl_settings_fragment : i3, (i4 & 8) != 0 ? AnonymousClass1.INSTANCE : function1);
    }

    /* JADX DEBUG: Type inference failed for r0v0. Raw type applied. Possible types: kotlin.jvm.functions.Function1<? super android.content.Context, ? extends androidx.recyclerview.widget.RecyclerView$LayoutManager>, kotlin.jvm.functions.Function1<android.content.Context, androidx.recyclerview.widget.RecyclerView$LayoutManager> */
    protected final Function1<Context, RecyclerView.LayoutManager> getLayoutManagerProducer() {
        return this.layoutManagerProducer;
    }

    public final void setLayoutManagerProducer(Function1<? super Context, ? extends RecyclerView.LayoutManager> function1) {
        Intrinsics.checkNotNullParameter(function1, "<set-?>");
        this.layoutManagerProducer = function1;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public DSLSettingsFragment(int i, int i2, int i3, Function1<? super Context, ? extends RecyclerView.LayoutManager> function1) {
        super(i3);
        Intrinsics.checkNotNullParameter(function1, "layoutManagerProducer");
        this.titleId = i;
        this.menuId = i2;
        this.layoutManagerProducer = function1;
    }

    public final RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    @Override // androidx.fragment.app.Fragment
    public void onViewCreated(View view, Bundle bundle) {
        Intrinsics.checkNotNullParameter(view, "view");
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        int i = this.titleId;
        if (!(i == -1 || toolbar == null)) {
            toolbar.setTitle(i);
        }
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsFragment$$ExternalSyntheticLambda0
                @Override // android.view.View.OnClickListener
                public final void onClick(View view2) {
                    DSLSettingsFragment.m523onViewCreated$lambda0(DSLSettingsFragment.this, view2);
                }
            });
        }
        int i2 = this.menuId;
        if (i2 != -1) {
            if (toolbar != null) {
                toolbar.inflateMenu(i2);
            }
            if (toolbar != null) {
                toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() { // from class: org.thoughtcrime.securesms.components.settings.DSLSettingsFragment$$ExternalSyntheticLambda1
                    @Override // androidx.appcompat.widget.Toolbar.OnMenuItemClickListener
                    public final boolean onMenuItemClick(MenuItem menuItem) {
                        return DSLSettingsFragment.m524onViewCreated$lambda1(DSLSettingsFragment.this, menuItem);
                    }
                });
            }
        }
        DSLSettingsAdapter dSLSettingsAdapter = new DSLSettingsAdapter();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        recyclerView.setEdgeEffectFactory(new EdgeEffectFactory());
        Function1<? super Context, ? extends RecyclerView.LayoutManager> function1 = this.layoutManagerProducer;
        Context requireContext = requireContext();
        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
        recyclerView.setLayoutManager((RecyclerView.LayoutManager) function1.invoke(requireContext));
        recyclerView.setAdapter(dSLSettingsAdapter);
        Material3OnScrollHelper material3OnScrollHelper = getMaterial3OnScrollHelper(toolbar);
        if (material3OnScrollHelper != null) {
            Intrinsics.checkNotNullExpressionValue(recyclerView, "this");
            material3OnScrollHelper.attach(recyclerView);
        }
        this.recyclerView = recyclerView;
        bindAdapter(dSLSettingsAdapter);
    }

    /* renamed from: onViewCreated$lambda-0 */
    public static final void m523onViewCreated$lambda0(DSLSettingsFragment dSLSettingsFragment, View view) {
        Intrinsics.checkNotNullParameter(dSLSettingsFragment, "this$0");
        dSLSettingsFragment.onToolbarNavigationClicked();
    }

    /* renamed from: onViewCreated$lambda-1 */
    public static final boolean m524onViewCreated$lambda1(DSLSettingsFragment dSLSettingsFragment, MenuItem menuItem) {
        Intrinsics.checkNotNullParameter(dSLSettingsFragment, "this$0");
        return dSLSettingsFragment.onOptionsItemSelected(menuItem);
    }

    public Material3OnScrollHelper getMaterial3OnScrollHelper(Toolbar toolbar) {
        if (toolbar == null) {
            return null;
        }
        FragmentActivity requireActivity = requireActivity();
        Intrinsics.checkNotNullExpressionValue(requireActivity, "requireActivity()");
        return new Material3OnScrollHelper(requireActivity, toolbar);
    }

    public void onToolbarNavigationClicked() {
        requireActivity().onBackPressed();
    }

    @Override // androidx.fragment.app.Fragment
    public void onDestroyView() {
        super.onDestroyView();
        this.recyclerView = null;
    }

    /* compiled from: DSLSettingsFragment.kt */
    /* access modifiers changed from: private */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\b\u0002\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0014¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment$EdgeEffectFactory;", "Landroidx/recyclerview/widget/RecyclerView$EdgeEffectFactory;", "()V", "createEdgeEffect", "Landroid/widget/EdgeEffect;", "view", "Landroidx/recyclerview/widget/RecyclerView;", "direction", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class EdgeEffectFactory extends RecyclerView.EdgeEffectFactory {
        @Override // androidx.recyclerview.widget.RecyclerView.EdgeEffectFactory
        public EdgeEffect createEdgeEffect(RecyclerView recyclerView, int i) {
            Intrinsics.checkNotNullParameter(recyclerView, "view");
            EdgeEffect createEdgeEffect = super.createEdgeEffect(recyclerView, i);
            Intrinsics.checkNotNullExpressionValue(createEdgeEffect, "super.createEdgeEffect(view, direction)");
            if (Build.VERSION.SDK_INT > 21) {
                createEdgeEffect.setColor(ContextCompat.getColor(recyclerView.getContext(), R.color.settings_ripple_color));
            }
            return createEdgeEffect;
        }
    }
}
