package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.os.Bundle;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.app.privacy.expire.ExpireTimerSettingsViewModel;

/* compiled from: ExpireTimerSettingsFragment.kt */
@Metadata(d1 = {"\u0000\f\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u001a\u000e\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002H\u0002¨\u0006\u0003"}, d2 = {"toConfig", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel$Config;", "Landroid/os/Bundle;", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class ExpireTimerSettingsFragmentKt {
    public static final ExpireTimerSettingsViewModel.Config toConfig(Bundle bundle) {
        if (bundle == null) {
            return new ExpireTimerSettingsViewModel.Config(null, false, null, 7, null);
        }
        ExpireTimerSettingsFragmentArgs fromBundle = ExpireTimerSettingsFragmentArgs.fromBundle(bundle);
        Intrinsics.checkNotNullExpressionValue(fromBundle, "fromBundle(this)");
        return new ExpireTimerSettingsViewModel.Config(fromBundle.getRecipientId(), fromBundle.getForResultMode(), fromBundle.getInitialValue());
    }
}
