package org.thoughtcrime.securesms.components.settings.conversation.preferences;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.conversation.preferences.LegacyGroupPreference;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class LegacyGroupPreference$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ LegacyGroupPreference.Model f$0;

    public /* synthetic */ LegacyGroupPreference$ViewHolder$$ExternalSyntheticLambda0(LegacyGroupPreference.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        LegacyGroupPreference.ViewHolder.m1210bind$lambda0(this.f$0, view);
    }
}
