package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;
import org.thoughtcrime.securesms.database.model.StoryViewState;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda17 implements Function {
    public final /* synthetic */ StoryViewState f$0;

    public /* synthetic */ ConversationSettingsViewModel$GroupSettingsViewModel$$ExternalSyntheticLambda17(StoryViewState storyViewState) {
        this.f$0 = storyViewState;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.GroupSettingsViewModel.m1144lambda1$lambda0(this.f$0, (ConversationSettingsState) obj);
    }
}
