package org.thoughtcrime.securesms.components.settings;

import android.view.View;
import android.widget.TextView;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingViewHolder;

/* loaded from: classes4.dex */
public final class SettingHeader {

    /* loaded from: classes4.dex */
    public static final class ViewHolder extends MappingViewHolder<Item> {
        private final TextView headerText = ((TextView) findViewById(R.id.base_settings_header_item_text));

        public ViewHolder(View view) {
            super(view);
        }

        public void bind(Item item) {
            if (item.text != null) {
                this.headerText.setText(item.text);
            } else {
                this.headerText.setText(item.textRes);
            }
        }
    }

    /* loaded from: classes4.dex */
    public static final class Item implements MappingModel<Item> {
        private final String text;
        private final int textRes;

        @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
        public /* synthetic */ Object getChangePayload(Item item) {
            return MappingModel.CC.$default$getChangePayload(this, item);
        }

        public Item(String str) {
            this.text = str;
            this.textRes = 0;
        }

        public Item(int i) {
            this.text = null;
            this.textRes = i;
        }

        public boolean areItemsTheSame(Item item) {
            return this.textRes == item.textRes && Objects.equals(this.text, item.text);
        }

        public boolean areContentsTheSame(Item item) {
            return areItemsTheSame(item);
        }
    }
}
