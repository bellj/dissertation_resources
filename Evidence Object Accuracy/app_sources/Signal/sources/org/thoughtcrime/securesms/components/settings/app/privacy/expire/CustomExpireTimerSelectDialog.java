package org.thoughtcrime.securesms.components.settings.app.privacy.expire;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.R;

/* compiled from: CustomExpireTimerSelectDialog.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \r2\u00020\u0001:\u0001\rB\u0005¢\u0006\u0002\u0010\u0002J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016J\u0012\u0010\u000b\u001a\u00020\f2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X.¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X.¢\u0006\u0002\n\u0000¨\u0006\u000e"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectDialog;", "Landroidx/fragment/app/DialogFragment;", "()V", "selector", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectorView;", "viewModel", "Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/ExpireTimerSettingsViewModel;", "onActivityCreated", "", "savedInstanceState", "Landroid/os/Bundle;", "onCreateDialog", "Landroid/app/Dialog;", "Companion", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomExpireTimerSelectDialog extends DialogFragment {
    public static final Companion Companion = new Companion(null);
    private static final String DIALOG_TAG;
    private CustomExpireTimerSelectorView selector;
    private ExpireTimerSettingsViewModel viewModel;

    @Override // androidx.fragment.app.DialogFragment
    public Dialog onCreateDialog(Bundle bundle) {
        View inflate = LayoutInflater.from(getContext()).inflate(R.layout.custom_expire_timer_select_dialog, (ViewGroup) null, false);
        Intrinsics.checkNotNullExpressionValue(inflate, "from(context).inflate(R.…lect_dialog, null, false)");
        View findViewById = inflate.findViewById(R.id.custom_expire_timer_select_dialog_selector);
        Intrinsics.checkNotNullExpressionValue(findViewById, "dialogView.findViewById(…r_select_dialog_selector)");
        this.selector = (CustomExpireTimerSelectorView) findViewById;
        AlertDialog create = new MaterialAlertDialogBuilder(requireContext()).setTitle(R.string.ExpireTimerSettingsFragment__custom_time).setView(inflate).setPositiveButton(R.string.ExpireTimerSettingsFragment__set, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.CustomExpireTimerSelectDialog$$ExternalSyntheticLambda0
            @Override // android.content.DialogInterface.OnClickListener
            public final void onClick(DialogInterface dialogInterface, int i) {
                CustomExpireTimerSelectDialog.$r8$lambda$_8IKrJqjV2oHRjGuBasRSnCYx08(CustomExpireTimerSelectDialog.this, dialogInterface, i);
            }
        }).setNegativeButton(17039360, (DialogInterface.OnClickListener) null).create();
        Intrinsics.checkNotNullExpressionValue(create, "MaterialAlertDialogBuild…el, null)\n      .create()");
        return create;
    }

    /* renamed from: onCreateDialog$lambda-0 */
    public static final void m858onCreateDialog$lambda0(CustomExpireTimerSelectDialog customExpireTimerSelectDialog, DialogInterface dialogInterface, int i) {
        Intrinsics.checkNotNullParameter(customExpireTimerSelectDialog, "this$0");
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel = customExpireTimerSelectDialog.viewModel;
        CustomExpireTimerSelectorView customExpireTimerSelectorView = null;
        if (expireTimerSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            expireTimerSettingsViewModel = null;
        }
        CustomExpireTimerSelectorView customExpireTimerSelectorView2 = customExpireTimerSelectDialog.selector;
        if (customExpireTimerSelectorView2 == null) {
            Intrinsics.throwUninitializedPropertyAccessException("selector");
        } else {
            customExpireTimerSelectorView = customExpireTimerSelectorView2;
        }
        expireTimerSettingsViewModel.select(customExpireTimerSelectorView.getTimer());
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        ViewModel viewModel = new ViewModelProvider(NavHostFragment.findNavController(this).getViewModelStoreOwner(R.id.app_settings_expire_timer)).get(ExpireTimerSettingsViewModel.class);
        Intrinsics.checkNotNullExpressionValue(viewModel, "ViewModelProvider(NavHos…ngsViewModel::class.java)");
        ExpireTimerSettingsViewModel expireTimerSettingsViewModel = (ExpireTimerSettingsViewModel) viewModel;
        this.viewModel = expireTimerSettingsViewModel;
        if (expireTimerSettingsViewModel == null) {
            Intrinsics.throwUninitializedPropertyAccessException("viewModel");
            expireTimerSettingsViewModel = null;
        }
        expireTimerSettingsViewModel.getState().observe(this, new Observer() { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.expire.CustomExpireTimerSelectDialog$$ExternalSyntheticLambda1
            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                CustomExpireTimerSelectDialog.m856$r8$lambda$f6G5o7dGQUoqqQYbzoWDYaFOIs(CustomExpireTimerSelectDialog.this, (ExpireTimerSettingsState) obj);
            }
        });
    }

    /* renamed from: onActivityCreated$lambda-1 */
    public static final void m857onActivityCreated$lambda1(CustomExpireTimerSelectDialog customExpireTimerSelectDialog, ExpireTimerSettingsState expireTimerSettingsState) {
        Intrinsics.checkNotNullParameter(customExpireTimerSelectDialog, "this$0");
        CustomExpireTimerSelectorView customExpireTimerSelectorView = customExpireTimerSelectDialog.selector;
        if (customExpireTimerSelectorView == null) {
            Intrinsics.throwUninitializedPropertyAccessException("selector");
            customExpireTimerSelectorView = null;
        }
        customExpireTimerSelectorView.setTimer(Integer.valueOf(expireTimerSettingsState.getCurrentTimer()));
    }

    /* compiled from: CustomExpireTimerSelectDialog.kt */
    @Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bR\u000e\u0010\u0003\u001a\u00020\u0004XT¢\u0006\u0002\n\u0000¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/privacy/expire/CustomExpireTimerSelectDialog$Companion;", "", "()V", "DIALOG_TAG", "", "show", "", "fragmentManager", "Landroidx/fragment/app/FragmentManager;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }

        public final void show(FragmentManager fragmentManager) {
            Intrinsics.checkNotNullParameter(fragmentManager, "fragmentManager");
            new CustomExpireTimerSelectDialog().show(fragmentManager, CustomExpireTimerSelectDialog.DIALOG_TAG);
        }
    }
}
