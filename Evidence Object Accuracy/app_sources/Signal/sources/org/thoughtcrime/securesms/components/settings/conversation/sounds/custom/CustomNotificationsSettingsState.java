package org.thoughtcrime.securesms.components.settings.conversation.sounds.custom;

import android.net.Uri;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.RecipientDatabase;
import org.thoughtcrime.securesms.recipients.Recipient;

/* compiled from: CustomNotificationsSettingsState.kt */
@Metadata(d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001f\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001Bo\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\u0003\u0012\b\b\u0002\u0010\b\u001a\u00020\t\u0012\b\b\u0002\u0010\n\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f\u0012\b\b\u0002\u0010\r\u001a\u00020\t\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\f\u0012\b\b\u0002\u0010\u000f\u001a\u00020\u0003¢\u0006\u0002\u0010\u0010J\t\u0010\u001e\u001a\u00020\u0003HÆ\u0003J\t\u0010\u001f\u001a\u00020\u0003HÆ\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u0005HÆ\u0003J\t\u0010!\u001a\u00020\u0003HÆ\u0003J\t\u0010\"\u001a\u00020\u0003HÆ\u0003J\t\u0010#\u001a\u00020\tHÆ\u0003J\t\u0010$\u001a\u00020\u0003HÆ\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\fHÆ\u0003J\t\u0010&\u001a\u00020\tHÆ\u0003J\u000b\u0010'\u001a\u0004\u0018\u00010\fHÆ\u0003Js\u0010(\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\r\u001a\u00020\t2\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\f2\b\b\u0002\u0010\u000f\u001a\u00020\u0003HÆ\u0001J\u0013\u0010)\u001a\u00020\u00032\b\u0010*\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010+\u001a\u00020,HÖ\u0001J\t\u0010-\u001a\u00020.HÖ\u0001R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0011\u0010\r\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0016R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u0016R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\f¢\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0012R\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0016R\u0011\u0010\b\u001a\u00020\t¢\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0014R\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\u000f\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0016¨\u0006/"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/conversation/sounds/custom/CustomNotificationsSettingsState;", "", "isInitialLoadComplete", "", RecipientDatabase.TABLE_NAME, "Lorg/thoughtcrime/securesms/recipients/Recipient;", "hasCustomNotifications", "controlsEnabled", "messageVibrateState", "Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "messageVibrateEnabled", "messageSound", "Landroid/net/Uri;", "callVibrateState", "callSound", "showCallingOptions", "(ZLorg/thoughtcrime/securesms/recipients/Recipient;ZZLorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;ZLandroid/net/Uri;Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;Landroid/net/Uri;Z)V", "getCallSound", "()Landroid/net/Uri;", "getCallVibrateState", "()Lorg/thoughtcrime/securesms/database/RecipientDatabase$VibrateState;", "getControlsEnabled", "()Z", "getHasCustomNotifications", "getMessageSound", "getMessageVibrateEnabled", "getMessageVibrateState", "getRecipient", "()Lorg/thoughtcrime/securesms/recipients/Recipient;", "getShowCallingOptions", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class CustomNotificationsSettingsState {
    private final Uri callSound;
    private final RecipientDatabase.VibrateState callVibrateState;
    private final boolean controlsEnabled;
    private final boolean hasCustomNotifications;
    private final boolean isInitialLoadComplete;
    private final Uri messageSound;
    private final boolean messageVibrateEnabled;
    private final RecipientDatabase.VibrateState messageVibrateState;
    private final Recipient recipient;
    private final boolean showCallingOptions;

    public CustomNotificationsSettingsState() {
        this(false, null, false, false, null, false, null, null, null, false, 1023, null);
    }

    public final boolean component1() {
        return this.isInitialLoadComplete;
    }

    public final boolean component10() {
        return this.showCallingOptions;
    }

    public final Recipient component2() {
        return this.recipient;
    }

    public final boolean component3() {
        return this.hasCustomNotifications;
    }

    public final boolean component4() {
        return this.controlsEnabled;
    }

    public final RecipientDatabase.VibrateState component5() {
        return this.messageVibrateState;
    }

    public final boolean component6() {
        return this.messageVibrateEnabled;
    }

    public final Uri component7() {
        return this.messageSound;
    }

    public final RecipientDatabase.VibrateState component8() {
        return this.callVibrateState;
    }

    public final Uri component9() {
        return this.callSound;
    }

    public final CustomNotificationsSettingsState copy(boolean z, Recipient recipient, boolean z2, boolean z3, RecipientDatabase.VibrateState vibrateState, boolean z4, Uri uri, RecipientDatabase.VibrateState vibrateState2, Uri uri2, boolean z5) {
        Intrinsics.checkNotNullParameter(vibrateState, "messageVibrateState");
        Intrinsics.checkNotNullParameter(vibrateState2, "callVibrateState");
        return new CustomNotificationsSettingsState(z, recipient, z2, z3, vibrateState, z4, uri, vibrateState2, uri2, z5);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CustomNotificationsSettingsState)) {
            return false;
        }
        CustomNotificationsSettingsState customNotificationsSettingsState = (CustomNotificationsSettingsState) obj;
        return this.isInitialLoadComplete == customNotificationsSettingsState.isInitialLoadComplete && Intrinsics.areEqual(this.recipient, customNotificationsSettingsState.recipient) && this.hasCustomNotifications == customNotificationsSettingsState.hasCustomNotifications && this.controlsEnabled == customNotificationsSettingsState.controlsEnabled && this.messageVibrateState == customNotificationsSettingsState.messageVibrateState && this.messageVibrateEnabled == customNotificationsSettingsState.messageVibrateEnabled && Intrinsics.areEqual(this.messageSound, customNotificationsSettingsState.messageSound) && this.callVibrateState == customNotificationsSettingsState.callVibrateState && Intrinsics.areEqual(this.callSound, customNotificationsSettingsState.callSound) && this.showCallingOptions == customNotificationsSettingsState.showCallingOptions;
    }

    public int hashCode() {
        boolean z = this.isInitialLoadComplete;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        Recipient recipient = this.recipient;
        int i6 = 0;
        int hashCode = (i5 + (recipient == null ? 0 : recipient.hashCode())) * 31;
        boolean z2 = this.hasCustomNotifications;
        if (z2) {
            z2 = true;
        }
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = z2 ? 1 : 0;
        int i10 = (hashCode + i7) * 31;
        boolean z3 = this.controlsEnabled;
        if (z3) {
            z3 = true;
        }
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = z3 ? 1 : 0;
        int hashCode2 = (((i10 + i11) * 31) + this.messageVibrateState.hashCode()) * 31;
        boolean z4 = this.messageVibrateEnabled;
        if (z4) {
            z4 = true;
        }
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = (hashCode2 + i14) * 31;
        Uri uri = this.messageSound;
        int hashCode3 = (((i17 + (uri == null ? 0 : uri.hashCode())) * 31) + this.callVibrateState.hashCode()) * 31;
        Uri uri2 = this.callSound;
        if (uri2 != null) {
            i6 = uri2.hashCode();
        }
        int i18 = (hashCode3 + i6) * 31;
        boolean z5 = this.showCallingOptions;
        if (!z5) {
            i = z5 ? 1 : 0;
        }
        return i18 + i;
    }

    public String toString() {
        return "CustomNotificationsSettingsState(isInitialLoadComplete=" + this.isInitialLoadComplete + ", recipient=" + this.recipient + ", hasCustomNotifications=" + this.hasCustomNotifications + ", controlsEnabled=" + this.controlsEnabled + ", messageVibrateState=" + this.messageVibrateState + ", messageVibrateEnabled=" + this.messageVibrateEnabled + ", messageSound=" + this.messageSound + ", callVibrateState=" + this.callVibrateState + ", callSound=" + this.callSound + ", showCallingOptions=" + this.showCallingOptions + ')';
    }

    public CustomNotificationsSettingsState(boolean z, Recipient recipient, boolean z2, boolean z3, RecipientDatabase.VibrateState vibrateState, boolean z4, Uri uri, RecipientDatabase.VibrateState vibrateState2, Uri uri2, boolean z5) {
        Intrinsics.checkNotNullParameter(vibrateState, "messageVibrateState");
        Intrinsics.checkNotNullParameter(vibrateState2, "callVibrateState");
        this.isInitialLoadComplete = z;
        this.recipient = recipient;
        this.hasCustomNotifications = z2;
        this.controlsEnabled = z3;
        this.messageVibrateState = vibrateState;
        this.messageVibrateEnabled = z4;
        this.messageSound = uri;
        this.callVibrateState = vibrateState2;
        this.callSound = uri2;
        this.showCallingOptions = z5;
    }

    public final boolean isInitialLoadComplete() {
        return this.isInitialLoadComplete;
    }

    public final Recipient getRecipient() {
        return this.recipient;
    }

    public final boolean getHasCustomNotifications() {
        return this.hasCustomNotifications;
    }

    public final boolean getControlsEnabled() {
        return this.controlsEnabled;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ CustomNotificationsSettingsState(boolean r13, org.thoughtcrime.securesms.recipients.Recipient r14, boolean r15, boolean r16, org.thoughtcrime.securesms.database.RecipientDatabase.VibrateState r17, boolean r18, android.net.Uri r19, org.thoughtcrime.securesms.database.RecipientDatabase.VibrateState r20, android.net.Uri r21, boolean r22, int r23, kotlin.jvm.internal.DefaultConstructorMarker r24) {
        /*
            r12 = this;
            r0 = r23
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = 0
            goto L_0x000a
        L_0x0009:
            r1 = r13
        L_0x000a:
            r3 = r0 & 2
            r4 = 0
            if (r3 == 0) goto L_0x0011
            r3 = r4
            goto L_0x0012
        L_0x0011:
            r3 = r14
        L_0x0012:
            r5 = r0 & 4
            if (r5 == 0) goto L_0x0018
            r5 = 0
            goto L_0x0019
        L_0x0018:
            r5 = r15
        L_0x0019:
            r6 = r0 & 8
            if (r6 == 0) goto L_0x001f
            r6 = 0
            goto L_0x0021
        L_0x001f:
            r6 = r16
        L_0x0021:
            r7 = r0 & 16
            if (r7 == 0) goto L_0x0028
            org.thoughtcrime.securesms.database.RecipientDatabase$VibrateState r7 = org.thoughtcrime.securesms.database.RecipientDatabase.VibrateState.DEFAULT
            goto L_0x002a
        L_0x0028:
            r7 = r17
        L_0x002a:
            r8 = r0 & 32
            if (r8 == 0) goto L_0x0030
            r8 = 0
            goto L_0x0032
        L_0x0030:
            r8 = r18
        L_0x0032:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x0038
            r9 = r4
            goto L_0x003a
        L_0x0038:
            r9 = r19
        L_0x003a:
            r10 = r0 & 128(0x80, float:1.794E-43)
            if (r10 == 0) goto L_0x0041
            org.thoughtcrime.securesms.database.RecipientDatabase$VibrateState r10 = org.thoughtcrime.securesms.database.RecipientDatabase.VibrateState.DEFAULT
            goto L_0x0043
        L_0x0041:
            r10 = r20
        L_0x0043:
            r11 = r0 & 256(0x100, float:3.59E-43)
            if (r11 == 0) goto L_0x0048
            goto L_0x004a
        L_0x0048:
            r4 = r21
        L_0x004a:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto L_0x004f
            goto L_0x0051
        L_0x004f:
            r2 = r22
        L_0x0051:
            r13 = r12
            r14 = r1
            r15 = r3
            r16 = r5
            r17 = r6
            r18 = r7
            r19 = r8
            r20 = r9
            r21 = r10
            r22 = r4
            r23 = r2
            r13.<init>(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.thoughtcrime.securesms.components.settings.conversation.sounds.custom.CustomNotificationsSettingsState.<init>(boolean, org.thoughtcrime.securesms.recipients.Recipient, boolean, boolean, org.thoughtcrime.securesms.database.RecipientDatabase$VibrateState, boolean, android.net.Uri, org.thoughtcrime.securesms.database.RecipientDatabase$VibrateState, android.net.Uri, boolean, int, kotlin.jvm.internal.DefaultConstructorMarker):void");
    }

    public final RecipientDatabase.VibrateState getMessageVibrateState() {
        return this.messageVibrateState;
    }

    public final boolean getMessageVibrateEnabled() {
        return this.messageVibrateEnabled;
    }

    public final Uri getMessageSound() {
        return this.messageSound;
    }

    public final RecipientDatabase.VibrateState getCallVibrateState() {
        return this.callVibrateState;
    }

    public final Uri getCallSound() {
        return this.callSound;
    }

    public final boolean getShowCallingOptions() {
        return this.showCallingOptions;
    }
}
