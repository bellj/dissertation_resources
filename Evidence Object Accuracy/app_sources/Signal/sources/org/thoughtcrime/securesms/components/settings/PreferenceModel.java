package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.components.settings.PreferenceModel;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModel;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\b&\u0018\u0000*\u000e\b\u0000\u0010\u0001*\b\u0012\u0004\u0012\u0002H\u00010\u00002\b\u0012\u0004\u0012\u0002H\u00010\u0002B?\u0012\n\b\u0002\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0004\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\b\b\u0002\u0010\t\u001a\u00020\n¢\u0006\u0002\u0010\u000bJ\u0015\u0010\u0013\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00028\u0000H\u0017¢\u0006\u0002\u0010\u0015J\u0015\u0010\u0016\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00028\u0000H\u0016¢\u0006\u0002\u0010\u0015R\u0016\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0016\u0010\b\u001a\u0004\u0018\u00010\u0007X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0014\u0010\t\u001a\u00020\nX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u000fR\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0016\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0011¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", "T", "Lorg/thoughtcrime/securesms/util/adapter/mapping/MappingModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "iconEnd", "isEnabled", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;Z)V", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "getIconEnd", "()Z", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getTitle", "areContentsTheSame", "newItem", "(Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;)Z", "areItemsTheSame", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public abstract class PreferenceModel<T extends PreferenceModel<T>> implements MappingModel<T> {
    private final DSLSettingsIcon icon;
    private final DSLSettingsIcon iconEnd;
    private final boolean isEnabled;
    private final DSLSettingsText summary;
    private final DSLSettingsText title;

    public PreferenceModel() {
        this(null, null, null, null, false, 31, null);
    }

    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* synthetic */ Object getChangePayload(Object obj) {
        return MappingModel.CC.$default$getChangePayload(this, obj);
    }

    public PreferenceModel(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, DSLSettingsIcon dSLSettingsIcon2, boolean z) {
        this.title = dSLSettingsText;
        this.summary = dSLSettingsText2;
        this.icon = dSLSettingsIcon;
        this.iconEnd = dSLSettingsIcon2;
        this.isEnabled = z;
    }

    public /* synthetic */ PreferenceModel(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, DSLSettingsIcon dSLSettingsIcon2, boolean z, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this((i & 1) != 0 ? null : dSLSettingsText, (i & 2) != 0 ? null : dSLSettingsText2, (i & 4) != 0 ? null : dSLSettingsIcon, (i & 8) == 0 ? dSLSettingsIcon2 : null, (i & 16) != 0 ? true : z);
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.PreferenceModel<T extends org.thoughtcrime.securesms.components.settings.PreferenceModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* bridge */ /* synthetic */ boolean areContentsTheSame(Object obj) {
        return areContentsTheSame((PreferenceModel<T>) ((PreferenceModel) obj));
    }

    /* JADX DEBUG: Multi-variable search result rejected for r0v0, resolved type: org.thoughtcrime.securesms.components.settings.PreferenceModel<T extends org.thoughtcrime.securesms.components.settings.PreferenceModel<T>> */
    /* JADX WARN: Multi-variable type inference failed */
    @Override // org.thoughtcrime.securesms.util.adapter.mapping.MappingModel
    public /* bridge */ /* synthetic */ boolean areItemsTheSame(Object obj) {
        return areItemsTheSame((PreferenceModel<T>) ((PreferenceModel) obj));
    }

    public DSLSettingsText getTitle() {
        return this.title;
    }

    public DSLSettingsText getSummary() {
        return this.summary;
    }

    public DSLSettingsIcon getIcon() {
        return this.icon;
    }

    public DSLSettingsIcon getIconEnd() {
        return this.iconEnd;
    }

    public boolean isEnabled() {
        return this.isEnabled;
    }

    public boolean areItemsTheSame(T t) {
        Intrinsics.checkNotNullParameter(t, "newItem");
        if (getTitle() != null) {
            return Intrinsics.areEqual(getTitle(), t.getTitle());
        }
        if (getSummary() != null) {
            return Intrinsics.areEqual(getSummary(), t.getSummary());
        }
        throw new AssertionError("Could not determine equality of " + t + ". Did you forget to override this method?");
    }

    public boolean areContentsTheSame(T t) {
        Intrinsics.checkNotNullParameter(t, "newItem");
        return areItemsTheSame((PreferenceModel<T>) t) && Intrinsics.areEqual(t.getSummary(), getSummary()) && Intrinsics.areEqual(t.getIcon(), getIcon()) && t.isEnabled() == isEnabled() && Intrinsics.areEqual(t.getIconEnd(), getIconEnd());
    }
}
