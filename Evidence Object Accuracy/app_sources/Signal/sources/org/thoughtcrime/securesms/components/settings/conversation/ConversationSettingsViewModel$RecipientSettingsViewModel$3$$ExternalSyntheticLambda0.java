package org.thoughtcrime.securesms.components.settings.conversation;

import com.annimon.stream.function.Function;
import org.thoughtcrime.securesms.components.settings.conversation.ConversationSettingsViewModel;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class ConversationSettingsViewModel$RecipientSettingsViewModel$3$$ExternalSyntheticLambda0 implements Function {
    public final /* synthetic */ long f$0;

    public /* synthetic */ ConversationSettingsViewModel$RecipientSettingsViewModel$3$$ExternalSyntheticLambda0(long j) {
        this.f$0 = j;
    }

    @Override // com.annimon.stream.function.Function
    public final Object apply(Object obj) {
        return ConversationSettingsViewModel.RecipientSettingsViewModel.AnonymousClass3.m1156invoke$lambda0(this.f$0, (ConversationSettingsState) obj);
    }
}
