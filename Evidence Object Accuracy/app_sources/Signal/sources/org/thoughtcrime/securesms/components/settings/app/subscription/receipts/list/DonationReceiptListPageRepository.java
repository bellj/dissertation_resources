package org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list;

import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.SignalDatabase;
import org.thoughtcrime.securesms.database.model.DonationReceiptRecord;

/* compiled from: DonationReceiptListPageRepository.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00060\u00050\u00042\b\u0010\u0007\u001a\u0004\u0018\u00010\b¨\u0006\t"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/receipts/list/DonationReceiptListPageRepository;", "", "()V", "getRecords", "Lio/reactivex/rxjava3/core/Single;", "", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord;", "type", "Lorg/thoughtcrime/securesms/database/model/DonationReceiptRecord$Type;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class DonationReceiptListPageRepository {
    public final Single<List<DonationReceiptRecord>> getRecords(DonationReceiptRecord.Type type) {
        Single<List<DonationReceiptRecord>> subscribeOn = Single.fromCallable(new Callable() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.receipts.list.DonationReceiptListPageRepository$$ExternalSyntheticLambda0
            @Override // java.util.concurrent.Callable
            public final Object call() {
                return DonationReceiptListPageRepository.$r8$lambda$5L4sXFC6z_Ie3Xvr4Sq1EohseKk(DonationReceiptRecord.Type.this);
            }
        }).subscribeOn(Schedulers.io());
        Intrinsics.checkNotNullExpressionValue(subscribeOn, "fromCallable {\n      Sig…scribeOn(Schedulers.io())");
        return subscribeOn;
    }

    /* renamed from: getRecords$lambda-0 */
    public static final List m1026getRecords$lambda0(DonationReceiptRecord.Type type) {
        return SignalDatabase.Companion.donationReceipts().getReceipts(type);
    }
}
