package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.content.DialogInterface;
import org.thoughtcrime.securesms.components.settings.app.notifications.profiles.NotificationProfileDetailsFragment$getConfiguration$1;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class NotificationProfileDetailsFragment$getConfiguration$1$9$$ExternalSyntheticLambda0 implements DialogInterface.OnClickListener {
    public final /* synthetic */ NotificationProfileDetailsFragment f$0;

    public /* synthetic */ NotificationProfileDetailsFragment$getConfiguration$1$9$$ExternalSyntheticLambda0(NotificationProfileDetailsFragment notificationProfileDetailsFragment) {
        this.f$0 = notificationProfileDetailsFragment;
    }

    @Override // android.content.DialogInterface.OnClickListener
    public final void onClick(DialogInterface dialogInterface, int i) {
        NotificationProfileDetailsFragment$getConfiguration$1.AnonymousClass9.m762invoke$lambda0(this.f$0, dialogInterface, i);
    }
}
