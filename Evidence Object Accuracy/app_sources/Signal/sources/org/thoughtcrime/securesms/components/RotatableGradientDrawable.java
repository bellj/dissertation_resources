package org.thoughtcrime.securesms.components;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Xfermode;
import android.graphics.drawable.Drawable;
import java.util.Arrays;

/* loaded from: classes4.dex */
public final class RotatableGradientDrawable extends Drawable {
    private static final float DEGREE_OFFSET;
    private final int[] colors;
    private final float degrees;
    private final Paint fillPaint = new Paint(5);
    private final Rect fillRect = new Rect();
    private final float[] positions;

    @Override // android.graphics.drawable.Drawable
    public int getOpacity() {
        return -1;
    }

    @Override // android.graphics.drawable.Drawable
    public void setAlpha(int i) {
    }

    @Override // android.graphics.drawable.Drawable
    public void setColorFilter(ColorFilter colorFilter) {
    }

    public RotatableGradientDrawable(float f, int[] iArr, float[] fArr) {
        this.degrees = f + DEGREE_OFFSET;
        this.colors = iArr;
        this.positions = fArr;
    }

    @Override // android.graphics.drawable.Drawable
    public void setBounds(int i, int i2, int i3, int i4) {
        super.setBounds(i, i2, i3, i4);
        Point point = new Point(i, i2);
        Point point2 = new Point(i3, i2);
        Point point3 = new Point(i, i4);
        Point point4 = new Point(i3, i4);
        Point point5 = new Point(getBounds().width() / 2, getBounds().height() / 2);
        Point cornerPrime = cornerPrime(point5, point, this.degrees);
        Point cornerPrime2 = cornerPrime(point5, point2, this.degrees);
        Point cornerPrime3 = cornerPrime(point5, point3, this.degrees);
        Point cornerPrime4 = cornerPrime(point5, point4, this.degrees);
        Rect rect = this.fillRect;
        rect.left = Integer.MAX_VALUE;
        rect.top = Integer.MAX_VALUE;
        rect.right = Integer.MIN_VALUE;
        rect.bottom = Integer.MIN_VALUE;
        for (Point point6 : Arrays.asList(point, point2, point3, point4, cornerPrime, cornerPrime2, cornerPrime3, cornerPrime4)) {
            int i5 = point6.x;
            Rect rect2 = this.fillRect;
            if (i5 < rect2.left) {
                rect2.left = i5;
            }
            if (i5 > rect2.right) {
                rect2.right = i5;
            }
            int i6 = point6.y;
            if (i6 < rect2.top) {
                rect2.top = i6;
            }
            if (i6 > rect2.bottom) {
                rect2.bottom = i6;
            }
        }
        Paint paint = this.fillPaint;
        Rect rect3 = this.fillRect;
        paint.setShader(new LinearGradient((float) rect3.left, (float) rect3.top, (float) rect3.right, (float) rect3.bottom, this.colors, this.positions, Shader.TileMode.CLAMP));
    }

    public void setXfermode(Xfermode xfermode) {
        this.fillPaint.setXfermode(xfermode);
    }

    private static Point cornerPrime(Point point, Point point2, float f) {
        double d = (double) f;
        return new Point(xPrime(point, point2, Math.toRadians(d)), yPrime(point, point2, Math.toRadians(d)));
    }

    private static int xPrime(Point point, Point point2, double d) {
        double d2 = (double) (point2.x - point.x);
        double cos = Math.cos(d);
        Double.isNaN(d2);
        double d3 = d2 * cos;
        double d4 = (double) (point2.y - point.y);
        double sin = Math.sin(d);
        Double.isNaN(d4);
        double d5 = (double) point.x;
        Double.isNaN(d5);
        return (int) Math.ceil((d3 - (d4 * sin)) + d5);
    }

    private static int yPrime(Point point, Point point2, double d) {
        double d2 = (double) (point2.x - point.x);
        double sin = Math.sin(d);
        Double.isNaN(d2);
        double d3 = d2 * sin;
        double d4 = (double) (point2.y - point.y);
        double cos = Math.cos(d);
        Double.isNaN(d4);
        double d5 = (double) point.y;
        Double.isNaN(d5);
        return (int) Math.ceil(d3 + (d4 * cos) + d5);
    }

    @Override // android.graphics.drawable.Drawable
    public void draw(Canvas canvas) {
        int save = canvas.save();
        canvas.rotate(this.degrees, ((float) getBounds().width()) / 2.0f, ((float) getBounds().height()) / 2.0f);
        int height = this.fillRect.height();
        int width = this.fillRect.width();
        Rect rect = this.fillRect;
        canvas.drawRect((float) (rect.left - width), (float) (rect.top - height), (float) (rect.right + width), (float) (rect.bottom + height), this.fillPaint);
        canvas.restoreToCount(save);
    }
}
