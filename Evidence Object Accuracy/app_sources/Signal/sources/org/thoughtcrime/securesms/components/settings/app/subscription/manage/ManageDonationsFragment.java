package org.thoughtcrime.securesms.components.settings.app.subscription.manage;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentViewModelLazyKt;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelStore;
import androidx.lifecycle.ViewModelStoreOwner;
import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.FragmentKt;
import java.util.Currency;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import kotlin.Lazy;
import kotlin.LazyKt__LazyJVMKt;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Reflection;
import org.signal.core.util.DimensionUnit;
import org.signal.core.util.money.FiatMoney;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet;
import org.thoughtcrime.securesms.badges.gifts.flow.GiftFlowActivity;
import org.thoughtcrime.securesms.badges.models.Badge;
import org.thoughtcrime.securesms.badges.models.BadgePreview;
import org.thoughtcrime.securesms.components.settings.DSLConfiguration;
import org.thoughtcrime.securesms.components.settings.DSLSettingsAdapter;
import org.thoughtcrime.securesms.components.settings.DSLSettingsFragment;
import org.thoughtcrime.securesms.components.settings.DSLSettingsIcon;
import org.thoughtcrime.securesms.components.settings.DSLSettingsText;
import org.thoughtcrime.securesms.components.settings.DslKt;
import org.thoughtcrime.securesms.components.settings.app.AppSettingsActivity;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ActiveSubscriptionPreference;
import org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsState;
import org.thoughtcrime.securesms.components.settings.app.subscription.models.NetworkFailure;
import org.thoughtcrime.securesms.components.settings.models.IndeterminateLoadingCircle;
import org.thoughtcrime.securesms.keyvalue.SignalStore;
import org.thoughtcrime.securesms.recipients.Recipient;
import org.thoughtcrime.securesms.subscription.Subscription;
import org.thoughtcrime.securesms.util.FeatureFlags;
import org.thoughtcrime.securesms.util.navigation.SafeNavigation;
import org.whispersystems.signalservice.api.subscriptions.ActiveSubscription;

/* compiled from: ManageDonationsFragment.kt */
@Metadata(bv = {}, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u00002\u00020\u00012\u00020\u0002B\u0007¢\u0006\u0004\b'\u0010(J\u0010\u0010\u0006\u001a\u00020\u00052\u0006\u0010\u0004\u001a\u00020\u0003H\u0002J\u0014\u0010\n\u001a\u00020\t*\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002J\u0014\u0010\u000b\u001a\u00020\t*\u00020\u00052\u0006\u0010\b\u001a\u00020\u0007H\u0002J$\u0010\u0010\u001a\u00020\t*\u00020\u00052\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\u000f\u001a\u00020\u000e2\u0006\u0010\b\u001a\u00020\u0007H\u0002J(\u0010\u0013\u001a\u00020\t*\u00020\u00052\u0006\u0010\b\u001a\u00020\u00072\u0012\u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\t0\u0011H\u0002J\f\u0010\u0014\u001a\u00020\t*\u00020\u0005H\u0002J\f\u0010\u0015\u001a\u00020\t*\u00020\u0005H\u0002J\f\u0010\u0016\u001a\u00020\t*\u00020\u0005H\u0002J\b\u0010\u0017\u001a\u00020\tH\u0016J\u0010\u0010\u001a\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u0018H\u0016J\b\u0010\u001b\u001a\u00020\tH\u0016R\u001b\u0010!\u001a\u00020\u001c8BX\u0002¢\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001f\u0010 R\u001b\u0010&\u001a\u00020\"8BX\u0002¢\u0006\f\n\u0004\b#\u0010\u001e\u001a\u0004\b$\u0010%¨\u0006)"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsFragment;", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsFragment;", "Lorg/thoughtcrime/securesms/badges/gifts/ExpiredGiftSheet$Callback;", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState;", "state", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "getConfiguration", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsState$SubscriptionRedemptionState;", "redemptionState", "", "presentNetworkFailureSettings", "presentSubscriptionSettingsWithNetworkError", "Lorg/whispersystems/signalservice/api/subscriptions/ActiveSubscription$Subscription;", "activeSubscription", "Lorg/thoughtcrime/securesms/subscription/Subscription;", "subscription", "presentSubscriptionSettings", "Lkotlin/Function1;", "subscriptionBlock", "presentSubscriptionSettingsWithState", "presentNoSubscriptionSettings", "presentOtherWaysToGive", "presentDonationReceipts", "onResume", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsAdapter;", "adapter", "bindAdapter", "onMakeAMonthlyDonation", "", "supportTechSummary$delegate", "Lkotlin/Lazy;", "getSupportTechSummary", "()Ljava/lang/CharSequence;", "supportTechSummary", "Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsViewModel;", "viewModel$delegate", "getViewModel", "()Lorg/thoughtcrime/securesms/components/settings/app/subscription/manage/ManageDonationsViewModel;", "viewModel", "<init>", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class ManageDonationsFragment extends DSLSettingsFragment implements ExpiredGiftSheet.Callback {
    private final Lazy supportTechSummary$delegate = LazyKt__LazyJVMKt.lazy(new ManageDonationsFragment$supportTechSummary$2(this));
    private final Lazy viewModel$delegate = FragmentViewModelLazyKt.createViewModelLazy(this, Reflection.getOrCreateKotlinClass(ManageDonationsViewModel.class), new Function0<ViewModelStore>(new Function0<Fragment>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$special$$inlined$viewModels$default$1
        final /* synthetic */ Fragment $this_viewModels;

        {
            this.$this_viewModels = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final Fragment invoke() {
            return this.$this_viewModels;
        }
    }) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$special$$inlined$viewModels$default$2
        final /* synthetic */ Function0 $ownerProducer;

        {
            this.$ownerProducer = r1;
        }

        @Override // kotlin.jvm.functions.Function0
        public final ViewModelStore invoke() {
            ViewModelStore viewModelStore = ((ViewModelStoreOwner) this.$ownerProducer.invoke()).getViewModelStore();
            Intrinsics.checkNotNullExpressionValue(viewModelStore, "ownerProducer().viewModelStore");
            return viewModelStore;
        }
    }, ManageDonationsFragment$viewModel$2.INSTANCE);

    public ManageDonationsFragment() {
        super(0, 0, 0, null, 15, null);
    }

    private final CharSequence getSupportTechSummary() {
        Object value = this.supportTechSummary$delegate.getValue();
        Intrinsics.checkNotNullExpressionValue(value, "<get-supportTechSummary>(...)");
        return (CharSequence) value;
    }

    public final ManageDonationsViewModel getViewModel() {
        return (ManageDonationsViewModel) this.viewModel$delegate.getValue();
    }

    @Override // androidx.fragment.app.Fragment
    public void onResume() {
        super.onResume();
        getViewModel().refresh();
    }

    @Override // org.thoughtcrime.securesms.components.settings.DSLSettingsFragment
    public void bindAdapter(DSLSettingsAdapter dSLSettingsAdapter) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "adapter");
        ActiveSubscriptionPreference.INSTANCE.register(dSLSettingsAdapter);
        IndeterminateLoadingCircle.INSTANCE.register(dSLSettingsAdapter);
        BadgePreview.INSTANCE.register(dSLSettingsAdapter);
        NetworkFailure.INSTANCE.register(dSLSettingsAdapter);
        Badge expiredGiftBadge = SignalStore.donationsValues().getExpiredGiftBadge();
        if (expiredGiftBadge != null) {
            SignalStore.donationsValues().setExpiredGiftBadge(null);
            ExpiredGiftSheet.Companion companion = ExpiredGiftSheet.Companion;
            FragmentManager childFragmentManager = getChildFragmentManager();
            Intrinsics.checkNotNullExpressionValue(childFragmentManager, "childFragmentManager");
            companion.show(childFragmentManager, expiredGiftBadge);
        }
        getViewModel().getState().observe(getViewLifecycleOwner(), new Observer(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$$ExternalSyntheticLambda0
            public final /* synthetic */ ManageDonationsFragment f$1;

            {
                this.f$1 = r2;
            }

            @Override // androidx.lifecycle.Observer
            public final void onChanged(Object obj) {
                ManageDonationsFragment.$r8$lambda$HGUH2GjeE8fdPnFIvOoMPBUj57E(DSLSettingsAdapter.this, this.f$1, (ManageDonationsState) obj);
            }
        });
    }

    /* renamed from: bindAdapter$lambda-0 */
    public static final void m966bindAdapter$lambda0(DSLSettingsAdapter dSLSettingsAdapter, ManageDonationsFragment manageDonationsFragment, ManageDonationsState manageDonationsState) {
        Intrinsics.checkNotNullParameter(dSLSettingsAdapter, "$adapter");
        Intrinsics.checkNotNullParameter(manageDonationsFragment, "this$0");
        Intrinsics.checkNotNullExpressionValue(manageDonationsState, "state");
        dSLSettingsAdapter.submitList(manageDonationsFragment.getConfiguration(manageDonationsState).toMappingModelList());
    }

    private final DSLConfiguration getConfiguration(ManageDonationsState manageDonationsState) {
        return DslKt.configure(new Function1<DSLConfiguration, Unit>(manageDonationsState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$getConfiguration$1
            final /* synthetic */ ManageDonationsState $state;
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$state = r1;
                this.this$0 = r2;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration) {
                invoke(dSLConfiguration);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration) {
                Object obj;
                boolean z;
                Intrinsics.checkNotNullParameter(dSLConfiguration, "$this$configure");
                dSLConfiguration.customPref(new BadgePreview.BadgeModel.FeaturedModel(this.$state.getFeaturedBadge()));
                dSLConfiguration.space((int) DimensionUnit.DP.toPixels(8.0f));
                dSLConfiguration.sectionHeaderPref(DSLSettingsText.Companion.from(R.string.SubscribeFragment__signal_is_powered_by_people_like_you, DSLSettingsText.CenterModifier.INSTANCE, DSLSettingsText.TitleLargeModifier.INSTANCE));
                if (this.$state.getTransactionState() instanceof ManageDonationsState.TransactionState.NotInTransaction) {
                    ActiveSubscription.Subscription activeSubscription = ((ManageDonationsState.TransactionState.NotInTransaction) this.$state.getTransactionState()).getActiveSubscription().getActiveSubscription();
                    if (activeSubscription != null) {
                        Iterator<T> it = this.$state.getAvailableSubscriptions().iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj = null;
                                break;
                            }
                            obj = it.next();
                            if (activeSubscription.getLevel() == ((Subscription) obj).getLevel()) {
                                z = true;
                                continue;
                            } else {
                                z = false;
                                continue;
                            }
                            if (z) {
                                break;
                            }
                        }
                        Subscription subscription = (Subscription) obj;
                        if (subscription != null) {
                            ManageDonationsFragment.access$presentSubscriptionSettings(this.this$0, dSLConfiguration, activeSubscription, subscription, this.$state.getRedemptionState());
                        } else {
                            dSLConfiguration.customPref(IndeterminateLoadingCircle.INSTANCE);
                        }
                    } else {
                        ManageDonationsFragment.access$presentNoSubscriptionSettings(this.this$0, dSLConfiguration);
                    }
                } else if (Intrinsics.areEqual(this.$state.getTransactionState(), ManageDonationsState.TransactionState.NetworkFailure.INSTANCE)) {
                    ManageDonationsFragment.access$presentNetworkFailureSettings(this.this$0, dSLConfiguration, this.$state.getRedemptionState());
                } else {
                    dSLConfiguration.customPref(IndeterminateLoadingCircle.INSTANCE);
                }
            }
        });
    }

    public final void presentNetworkFailureSettings(DSLConfiguration dSLConfiguration, ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState) {
        if (SignalStore.donationsValues().isLikelyASustainer()) {
            presentSubscriptionSettingsWithNetworkError(dSLConfiguration, subscriptionRedemptionState);
        } else {
            presentNoSubscriptionSettings(dSLConfiguration);
        }
    }

    private final void presentSubscriptionSettingsWithNetworkError(DSLConfiguration dSLConfiguration, ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState) {
        presentSubscriptionSettingsWithState(dSLConfiguration, subscriptionRedemptionState, new Function1<DSLConfiguration, Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentSubscriptionSettingsWithNetworkError$1
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration2) {
                invoke(dSLConfiguration2);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration2) {
                Intrinsics.checkNotNullParameter(dSLConfiguration2, "$this$presentSubscriptionSettingsWithState");
                final ManageDonationsFragment manageDonationsFragment = this.this$0;
                dSLConfiguration2.customPref(new NetworkFailure.Model(new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentSubscriptionSettingsWithNetworkError$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        ManageDonationsFragment.access$getViewModel(manageDonationsFragment).retry();
                    }
                }));
            }
        });
    }

    public final void presentSubscriptionSettings(DSLConfiguration dSLConfiguration, ActiveSubscription.Subscription subscription, Subscription subscription2, ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState) {
        presentSubscriptionSettingsWithState(dSLConfiguration, subscriptionRedemptionState, new Function1<DSLConfiguration, Unit>(subscription, subscription2, subscriptionRedemptionState, this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentSubscriptionSettings$1
            final /* synthetic */ ActiveSubscription.Subscription $activeSubscription;
            final /* synthetic */ ManageDonationsState.SubscriptionRedemptionState $redemptionState;
            final /* synthetic */ Subscription $subscription;
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.$activeSubscription = r1;
                this.$subscription = r2;
                this.$redemptionState = r3;
                this.this$0 = r4;
            }

            /* Return type fixed from 'java.lang.Object' to match base method */
            /* JADX DEBUG: Method arguments types fixed to match base method, original types: [java.lang.Object] */
            @Override // kotlin.jvm.functions.Function1
            public /* bridge */ /* synthetic */ Unit invoke(DSLConfiguration dSLConfiguration2) {
                invoke(dSLConfiguration2);
                return Unit.INSTANCE;
            }

            public final void invoke(DSLConfiguration dSLConfiguration2) {
                Intrinsics.checkNotNullParameter(dSLConfiguration2, "$this$presentSubscriptionSettingsWithState");
                Currency instance = Currency.getInstance(this.$activeSubscription.getCurrency());
                FiatMoney fiatMoney = new FiatMoney(this.$activeSubscription.getAmount().movePointLeft(instance.getDefaultFractionDigits()), instance);
                long millis = TimeUnit.SECONDS.toMillis(this.$activeSubscription.getEndOfCurrentPeriod());
                Subscription subscription3 = this.$subscription;
                ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState2 = this.$redemptionState;
                ActiveSubscription.Subscription subscription4 = this.$activeSubscription;
                final ManageDonationsFragment manageDonationsFragment = this.this$0;
                dSLConfiguration2.customPref(new ActiveSubscriptionPreference.Model(fiatMoney, subscription3, millis, subscriptionRedemptionState2, subscription4, new Function0<Unit>() { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentSubscriptionSettings$1.1
                    @Override // kotlin.jvm.functions.Function0
                    public final void invoke() {
                        manageDonationsFragment.requireActivity().finish();
                        FragmentActivity requireActivity = manageDonationsFragment.requireActivity();
                        AppSettingsActivity.Companion companion = AppSettingsActivity.Companion;
                        Context requireContext = manageDonationsFragment.requireContext();
                        Intrinsics.checkNotNullExpressionValue(requireContext, "requireContext()");
                        requireActivity.startActivity(companion.help(requireContext, 7));
                    }
                }));
            }
        });
    }

    private final void presentSubscriptionSettingsWithState(DSLConfiguration dSLConfiguration, ManageDonationsState.SubscriptionRedemptionState subscriptionRedemptionState, Function1<? super DSLConfiguration, Unit> function1) {
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        dSLConfiguration.space((int) dimensionUnit.toPixels(32.0f));
        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
        dSLConfiguration.noPadTextPref(companion.from(R.string.ManageDonationsFragment__my_subscription, DSLSettingsText.Body1BoldModifier.INSTANCE, DSLSettingsText.BoldModifier.INSTANCE));
        dSLConfiguration.space((int) dimensionUnit.toPixels(12.0f));
        function1.invoke(dSLConfiguration);
        DSLSettingsText from = companion.from(R.string.ManageDonationsFragment__manage_subscription, new DSLSettingsText.Modifier[0]);
        DSLSettingsIcon.Companion companion2 = DSLSettingsIcon.Companion;
        dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_person_white_24dp, 0, 2, null), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0 ? true : subscriptionRedemptionState != ManageDonationsState.SubscriptionRedemptionState.IN_PROGRESS, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentSubscriptionSettingsWithState$1
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                NavController findNavController = FragmentKt.findNavController(this.this$0);
                NavDirections actionManageDonationsFragmentToSubscribeFragment = ManageDonationsFragmentDirections.actionManageDonationsFragmentToSubscribeFragment();
                Intrinsics.checkNotNullExpressionValue(actionManageDonationsFragmentToSubscribeFragment, "actionManageDonationsFragmentToSubscribeFragment()");
                SafeNavigation.safeNavigate(findNavController, actionManageDonationsFragmentToSubscribeFragment);
            }
        }, (r18 & 64) != 0 ? null : null);
        dSLConfiguration.clickPref(companion.from(R.string.ManageDonationsFragment__badges, new DSLSettingsText.Modifier[0]), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_badge_24, 0, 2, null), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentSubscriptionSettingsWithState$2
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                NavController findNavController = FragmentKt.findNavController(this.this$0);
                NavDirections actionManageDonationsFragmentToManageBadges = ManageDonationsFragmentDirections.actionManageDonationsFragmentToManageBadges();
                Intrinsics.checkNotNullExpressionValue(actionManageDonationsFragmentToManageBadges, "actionManageDonationsFragmentToManageBadges()");
                SafeNavigation.safeNavigate(findNavController, actionManageDonationsFragmentToManageBadges);
            }
        }, (r18 & 64) != 0 ? null : null);
        presentOtherWaysToGive(dSLConfiguration);
        dSLConfiguration.sectionHeaderPref(R.string.ManageDonationsFragment__more);
        presentDonationReceipts(dSLConfiguration);
        dSLConfiguration.externalLinkPref(companion.from(R.string.ManageDonationsFragment__subscription_faq, new DSLSettingsText.Modifier[0]), DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_help_24, 0, 2, null), R.string.donate_url);
    }

    public final void presentNoSubscriptionSettings(DSLConfiguration dSLConfiguration) {
        DimensionUnit dimensionUnit = DimensionUnit.DP;
        dSLConfiguration.space((int) dimensionUnit.toPixels(16.0f));
        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
        dSLConfiguration.noPadTextPref(companion.from(getSupportTechSummary(), DSLSettingsText.CenterModifier.INSTANCE));
        dSLConfiguration.space((int) dimensionUnit.toPixels(16.0f));
        DSLConfiguration.tonalButton$default(dSLConfiguration, companion.from(R.string.ManageDonationsFragment__make_a_monthly_donation, new DSLSettingsText.Modifier[0]), false, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentNoSubscriptionSettings$1
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                SafeNavigation.safeNavigate(FragmentKt.findNavController(this.this$0), (int) R.id.action_manageDonationsFragment_to_subscribeFragment);
            }
        }, 2, null);
        presentOtherWaysToGive(dSLConfiguration);
        dSLConfiguration.sectionHeaderPref(R.string.ManageDonationsFragment__receipts);
        presentDonationReceipts(dSLConfiguration);
    }

    private final void presentOtherWaysToGive(DSLConfiguration dSLConfiguration) {
        dSLConfiguration.dividerPref();
        dSLConfiguration.sectionHeaderPref(R.string.ManageDonationsFragment__other_ways_to_give);
        DSLSettingsText.Companion companion = DSLSettingsText.Companion;
        DSLSettingsText from = companion.from(R.string.preferences__one_time_donation, new DSLSettingsText.Modifier[0]);
        DSLSettingsIcon.Companion companion2 = DSLSettingsIcon.Companion;
        dSLConfiguration.clickPref(from, (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_boost_24, 0, 2, null), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentOtherWaysToGive$1
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                NavController findNavController = FragmentKt.findNavController(this.this$0);
                NavDirections actionManageDonationsFragmentToBoosts = ManageDonationsFragmentDirections.actionManageDonationsFragmentToBoosts();
                Intrinsics.checkNotNullExpressionValue(actionManageDonationsFragmentToBoosts, "actionManageDonationsFragmentToBoosts()");
                SafeNavigation.safeNavigate(findNavController, actionManageDonationsFragmentToBoosts);
            }
        }, (r18 & 64) != 0 ? null : null);
        if (FeatureFlags.giftBadgeSendSupport() && Recipient.self().getGiftBadgesCapability() == Recipient.Capability.SUPPORTED) {
            dSLConfiguration.clickPref(companion.from(R.string.ManageDonationsFragment__gift_a_badge, new DSLSettingsText.Modifier[0]), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : DSLSettingsIcon.Companion.from$default(companion2, R.drawable.ic_gift_24, 0, 2, null), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentOtherWaysToGive$2
                final /* synthetic */ ManageDonationsFragment this$0;

                /* access modifiers changed from: package-private */
                {
                    this.this$0 = r1;
                }

                @Override // kotlin.jvm.functions.Function0
                public final void invoke() {
                    this.this$0.startActivity(new Intent(this.this$0.requireContext(), GiftFlowActivity.class));
                }
            }, (r18 & 64) != 0 ? null : null);
        }
    }

    private final void presentDonationReceipts(DSLConfiguration dSLConfiguration) {
        dSLConfiguration.clickPref(DSLSettingsText.Companion.from(R.string.ManageDonationsFragment__donation_receipts, new DSLSettingsText.Modifier[0]), (r18 & 2) != 0 ? null : null, (r18 & 4) != 0 ? null : DSLSettingsIcon.Companion.from$default(DSLSettingsIcon.Companion, R.drawable.ic_receipt_24, 0, 2, null), (r18 & 8) != 0 ? null : null, (r18 & 16) != 0, new Function0<Unit>(this) { // from class: org.thoughtcrime.securesms.components.settings.app.subscription.manage.ManageDonationsFragment$presentDonationReceipts$1
            final /* synthetic */ ManageDonationsFragment this$0;

            /* access modifiers changed from: package-private */
            {
                this.this$0 = r1;
            }

            @Override // kotlin.jvm.functions.Function0
            public final void invoke() {
                NavController findNavController = FragmentKt.findNavController(this.this$0);
                NavDirections actionManageDonationsFragmentToDonationReceiptListFragment = ManageDonationsFragmentDirections.actionManageDonationsFragmentToDonationReceiptListFragment();
                Intrinsics.checkNotNullExpressionValue(actionManageDonationsFragmentToDonationReceiptListFragment, "actionManageDonationsFra…tionReceiptListFragment()");
                SafeNavigation.safeNavigate(findNavController, actionManageDonationsFragmentToDonationReceiptListFragment);
            }
        }, (r18 & 64) != 0 ? null : null);
    }

    @Override // org.thoughtcrime.securesms.badges.gifts.ExpiredGiftSheet.Callback
    public void onMakeAMonthlyDonation() {
        NavController findNavController = FragmentKt.findNavController(this);
        NavDirections actionManageDonationsFragmentToSubscribeFragment = ManageDonationsFragmentDirections.actionManageDonationsFragmentToSubscribeFragment();
        Intrinsics.checkNotNullExpressionValue(actionManageDonationsFragmentToSubscribeFragment, "actionManageDonationsFragmentToSubscribeFragment()");
        SafeNavigation.safeNavigate(findNavController, actionManageDonationsFragmentToSubscribeFragment);
    }
}
