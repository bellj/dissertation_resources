package org.thoughtcrime.securesms.components.webrtc;

import androidx.arch.core.util.Function;
import org.thoughtcrime.securesms.recipients.LiveRecipient;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class WebRtcCallViewModel$$ExternalSyntheticLambda5 implements Function {
    @Override // androidx.arch.core.util.Function
    public final Object apply(Object obj) {
        return ((LiveRecipient) obj).getLiveData();
    }
}
