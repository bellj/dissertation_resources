package org.thoughtcrime.securesms.components.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.io.Serializable;
import java.util.Objects;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.util.adapter.mapping.MappingModelList;

/* loaded from: classes4.dex */
public class BaseSettingsFragment extends Fragment {
    private static final String CONFIGURATION_ARGUMENT;
    private RecyclerView recycler;

    public static BaseSettingsFragment create(Configuration configuration) {
        BaseSettingsFragment baseSettingsFragment = new BaseSettingsFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("current_selection", configuration);
        baseSettingsFragment.setArguments(bundle);
        return baseSettingsFragment;
    }

    @Override // androidx.fragment.app.Fragment
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.base_settings_fragment, viewGroup, false);
        RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.base_settings_list);
        this.recycler = recyclerView;
        recyclerView.setItemAnimator(null);
        return inflate;
    }

    @Override // androidx.fragment.app.Fragment
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        BaseSettingsAdapter baseSettingsAdapter = new BaseSettingsAdapter();
        this.recycler.setLayoutManager(new LinearLayoutManager(requireContext()));
        this.recycler.setAdapter(baseSettingsAdapter);
        Serializable serializable = requireArguments().getSerializable("current_selection");
        Objects.requireNonNull(serializable);
        Configuration configuration = (Configuration) serializable;
        configuration.configure(requireActivity(), baseSettingsAdapter);
        configuration.setArguments(getArguments());
        configuration.configureAdapter(baseSettingsAdapter);
        baseSettingsAdapter.submitList(configuration.getSettings());
    }

    /* loaded from: classes4.dex */
    public static abstract class Configuration implements Serializable {
        protected transient FragmentActivity activity;
        protected transient BaseSettingsAdapter adapter;

        public abstract void configureAdapter(BaseSettingsAdapter baseSettingsAdapter);

        public abstract MappingModelList getSettings();

        public void setArguments(Bundle bundle) {
        }

        public void configure(FragmentActivity fragmentActivity, BaseSettingsAdapter baseSettingsAdapter) {
            this.activity = fragmentActivity;
            this.adapter = baseSettingsAdapter;
        }

        public void updateSettingsList() {
            this.adapter.submitList(getSettings());
        }
    }
}
