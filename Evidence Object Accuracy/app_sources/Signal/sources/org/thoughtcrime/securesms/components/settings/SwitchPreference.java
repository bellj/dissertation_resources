package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.conversation.mutiselect.forward.MultiselectForwardFragment;

/* compiled from: dsl.kt */
@Metadata(d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\f\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001BC\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\b\u0012\f\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\u0002\u0010\rJ\u0010\u0010\u0016\u001a\u00020\b2\u0006\u0010\u0017\u001a\u00020\u0000H\u0016R\u0016\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\t\u001a\u00020\b¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\u0010R\u0014\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\u0010R\u0017\u0010\n\u001a\b\u0012\u0004\u0012\u00020\f0\u000b¢\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R\u0016\u0010\u0004\u001a\u0004\u0018\u00010\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0014\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014¨\u0006\u0018"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/SwitchPreference;", "Lorg/thoughtcrime/securesms/components/settings/PreferenceModel;", MultiselectForwardFragment.DIALOG_TITLE, "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "summary", "icon", "Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "isEnabled", "", "isChecked", "onClick", "Lkotlin/Function0;", "", "(Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;ZZLkotlin/jvm/functions/Function0;)V", "getIcon", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsIcon;", "()Z", "getOnClick", "()Lkotlin/jvm/functions/Function0;", "getSummary", "()Lorg/thoughtcrime/securesms/components/settings/DSLSettingsText;", "getTitle", "areContentsTheSame", "newItem", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SwitchPreference extends PreferenceModel<SwitchPreference> {
    private final DSLSettingsIcon icon;
    private final boolean isChecked;
    private final boolean isEnabled;
    private final Function0<Unit> onClick;
    private final DSLSettingsText summary;
    private final DSLSettingsText title;

    public /* synthetic */ SwitchPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, boolean z, boolean z2, Function0 function0, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(dSLSettingsText, (i & 2) != 0 ? null : dSLSettingsText2, (i & 4) != 0 ? null : dSLSettingsIcon, z, z2, function0);
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getTitle() {
        return this.title;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsText getSummary() {
        return this.summary;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public DSLSettingsIcon getIcon() {
        return this.icon;
    }

    @Override // org.thoughtcrime.securesms.components.settings.PreferenceModel
    public boolean isEnabled() {
        return this.isEnabled;
    }

    public final boolean isChecked() {
        return this.isChecked;
    }

    public final Function0<Unit> getOnClick() {
        return this.onClick;
    }

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public SwitchPreference(DSLSettingsText dSLSettingsText, DSLSettingsText dSLSettingsText2, DSLSettingsIcon dSLSettingsIcon, boolean z, boolean z2, Function0<Unit> function0) {
        super(null, null, null, null, false, 31, null);
        Intrinsics.checkNotNullParameter(dSLSettingsText, MultiselectForwardFragment.DIALOG_TITLE);
        Intrinsics.checkNotNullParameter(function0, "onClick");
        this.title = dSLSettingsText;
        this.summary = dSLSettingsText2;
        this.icon = dSLSettingsIcon;
        this.isEnabled = z;
        this.isChecked = z2;
        this.onClick = function0;
    }

    public boolean areContentsTheSame(SwitchPreference switchPreference) {
        Intrinsics.checkNotNullParameter(switchPreference, "newItem");
        return super.areContentsTheSame(switchPreference) && this.isChecked == switchPreference.isChecked;
    }
}
