package org.thoughtcrime.securesms.components.settings.app.internal;

import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.signal.ringrtc.CallManager;
import org.thoughtcrime.securesms.emoji.EmojiFiles;

/* compiled from: InternalSettingsState.kt */
@Metadata(d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\t\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b7\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B§\u0001\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\u0006\u0010\u0007\u001a\u00020\u0003\u0012\u0006\u0010\b\u001a\u00020\u0003\u0012\u0006\u0010\t\u001a\u00020\u0003\u0012\u0006\u0010\n\u001a\u00020\u0003\u0012\u0006\u0010\u000b\u001a\u00020\u0003\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000f\u0012\u0006\u0010\u0010\u001a\u00020\u0011\u0012\u0006\u0010\u0012\u001a\u00020\u0003\u0012\u0006\u0010\u0013\u001a\u00020\u0003\u0012\b\u0010\u0014\u001a\u0004\u0018\u00010\u0015\u0012\u0006\u0010\u0016\u001a\u00020\u0003\u0012\u0006\u0010\u0017\u001a\u00020\u0003\u0012\u0006\u0010\u0018\u001a\u00020\u0003\u0012\u0006\u0010\u0019\u001a\u00020\u0003\u0012\u0006\u0010\u001a\u001a\u00020\u0003¢\u0006\u0002\u0010\u001bJ\t\u00105\u001a\u00020\u0003HÆ\u0003J\t\u00106\u001a\u00020\rHÆ\u0003J\t\u00107\u001a\u00020\u000fHÆ\u0003J\t\u00108\u001a\u00020\u0011HÆ\u0003J\t\u00109\u001a\u00020\u0003HÆ\u0003J\t\u0010:\u001a\u00020\u0003HÆ\u0003J\u000b\u0010;\u001a\u0004\u0018\u00010\u0015HÆ\u0003J\t\u0010<\u001a\u00020\u0003HÆ\u0003J\t\u0010=\u001a\u00020\u0003HÆ\u0003J\t\u0010>\u001a\u00020\u0003HÆ\u0003J\t\u0010?\u001a\u00020\u0003HÆ\u0003J\t\u0010@\u001a\u00020\u0003HÆ\u0003J\t\u0010A\u001a\u00020\u0003HÆ\u0003J\t\u0010B\u001a\u00020\u0003HÆ\u0003J\t\u0010C\u001a\u00020\u0003HÆ\u0003J\t\u0010D\u001a\u00020\u0003HÆ\u0003J\t\u0010E\u001a\u00020\u0003HÆ\u0003J\t\u0010F\u001a\u00020\u0003HÆ\u0003J\t\u0010G\u001a\u00020\u0003HÆ\u0003J\t\u0010H\u001a\u00020\u0003HÆ\u0003JÓ\u0001\u0010I\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u00032\b\b\u0002\u0010\u0007\u001a\u00020\u00032\b\b\u0002\u0010\b\u001a\u00020\u00032\b\b\u0002\u0010\t\u001a\u00020\u00032\b\b\u0002\u0010\n\u001a\u00020\u00032\b\b\u0002\u0010\u000b\u001a\u00020\u00032\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000f2\b\b\u0002\u0010\u0010\u001a\u00020\u00112\b\b\u0002\u0010\u0012\u001a\u00020\u00032\b\b\u0002\u0010\u0013\u001a\u00020\u00032\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00152\b\b\u0002\u0010\u0016\u001a\u00020\u00032\b\b\u0002\u0010\u0017\u001a\u00020\u00032\b\b\u0002\u0010\u0018\u001a\u00020\u00032\b\b\u0002\u0010\u0019\u001a\u00020\u00032\b\b\u0002\u0010\u001a\u001a\u00020\u0003HÆ\u0001J\u0013\u0010J\u001a\u00020\u00032\b\u0010K\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010L\u001a\u00020MHÖ\u0001J\t\u0010N\u001a\u00020\rHÖ\u0001R\u0011\u0010\u000b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u000e\u001a\u00020\u000f¢\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\u0010\u001a\u00020\u0011¢\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u0012\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001dR\u0011\u0010\f\u001a\u00020\r¢\u0006\b\n\u0000\u001a\u0004\b#\u0010$R\u0011\u0010\u001a\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001dR\u0011\u0010\u0017\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001dR\u0011\u0010\t\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b'\u0010\u001dR\u0011\u0010\n\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b(\u0010\u001dR\u0011\u0010\u0018\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b)\u0010\u001dR\u0011\u0010\u0019\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b*\u0010\u001dR\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0015¢\u0006\b\n\u0000\u001a\u0004\b+\u0010,R\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b-\u0010\u001dR\u0011\u0010\u0006\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b.\u0010\u001dR\u0011\u0010\b\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b/\u0010\u001dR\u0011\u0010\u0007\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b0\u0010\u001dR\u0011\u0010\u0016\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b1\u0010\u001dR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b2\u0010\u001dR\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b3\u0010\u001dR\u0011\u0010\u0013\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b4\u0010\u001d¨\u0006O"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/internal/InternalSettingsState;", "", "seeMoreUserDetails", "", "shakeToReport", "gv2doNotCreateGv2Groups", "gv2forceInvites", "gv2ignoreServerChanges", "gv2ignoreP2PChanges", "disableAutoMigrationInitiation", "disableAutoMigrationNotification", "allowCensorshipSetting", "callingServer", "", "callingAudioProcessingMethod", "Lorg/signal/ringrtc/CallManager$AudioProcessingMethod;", "callingBandwidthMode", "Lorg/signal/ringrtc/CallManager$BandwidthMode;", "callingDisableTelecom", "useBuiltInEmojiSet", "emojiVersion", "Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "removeSenderKeyMinimium", "delayResends", "disableStorageService", "disableStories", "canClearOnboardingState", "(ZZZZZZZZZLjava/lang/String;Lorg/signal/ringrtc/CallManager$AudioProcessingMethod;Lorg/signal/ringrtc/CallManager$BandwidthMode;ZZLorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;ZZZZZ)V", "getAllowCensorshipSetting", "()Z", "getCallingAudioProcessingMethod", "()Lorg/signal/ringrtc/CallManager$AudioProcessingMethod;", "getCallingBandwidthMode", "()Lorg/signal/ringrtc/CallManager$BandwidthMode;", "getCallingDisableTelecom", "getCallingServer", "()Ljava/lang/String;", "getCanClearOnboardingState", "getDelayResends", "getDisableAutoMigrationInitiation", "getDisableAutoMigrationNotification", "getDisableStorageService", "getDisableStories", "getEmojiVersion", "()Lorg/thoughtcrime/securesms/emoji/EmojiFiles$Version;", "getGv2doNotCreateGv2Groups", "getGv2forceInvites", "getGv2ignoreP2PChanges", "getGv2ignoreServerChanges", "getRemoveSenderKeyMinimium", "getSeeMoreUserDetails", "getShakeToReport", "getUseBuiltInEmojiSet", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component19", "component2", "component20", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class InternalSettingsState {
    private final boolean allowCensorshipSetting;
    private final CallManager.AudioProcessingMethod callingAudioProcessingMethod;
    private final CallManager.BandwidthMode callingBandwidthMode;
    private final boolean callingDisableTelecom;
    private final String callingServer;
    private final boolean canClearOnboardingState;
    private final boolean delayResends;
    private final boolean disableAutoMigrationInitiation;
    private final boolean disableAutoMigrationNotification;
    private final boolean disableStorageService;
    private final boolean disableStories;
    private final EmojiFiles.Version emojiVersion;
    private final boolean gv2doNotCreateGv2Groups;
    private final boolean gv2forceInvites;
    private final boolean gv2ignoreP2PChanges;
    private final boolean gv2ignoreServerChanges;
    private final boolean removeSenderKeyMinimium;
    private final boolean seeMoreUserDetails;
    private final boolean shakeToReport;
    private final boolean useBuiltInEmojiSet;

    public final boolean component1() {
        return this.seeMoreUserDetails;
    }

    public final String component10() {
        return this.callingServer;
    }

    public final CallManager.AudioProcessingMethod component11() {
        return this.callingAudioProcessingMethod;
    }

    public final CallManager.BandwidthMode component12() {
        return this.callingBandwidthMode;
    }

    public final boolean component13() {
        return this.callingDisableTelecom;
    }

    public final boolean component14() {
        return this.useBuiltInEmojiSet;
    }

    public final EmojiFiles.Version component15() {
        return this.emojiVersion;
    }

    public final boolean component16() {
        return this.removeSenderKeyMinimium;
    }

    public final boolean component17() {
        return this.delayResends;
    }

    public final boolean component18() {
        return this.disableStorageService;
    }

    public final boolean component19() {
        return this.disableStories;
    }

    public final boolean component2() {
        return this.shakeToReport;
    }

    public final boolean component20() {
        return this.canClearOnboardingState;
    }

    public final boolean component3() {
        return this.gv2doNotCreateGv2Groups;
    }

    public final boolean component4() {
        return this.gv2forceInvites;
    }

    public final boolean component5() {
        return this.gv2ignoreServerChanges;
    }

    public final boolean component6() {
        return this.gv2ignoreP2PChanges;
    }

    public final boolean component7() {
        return this.disableAutoMigrationInitiation;
    }

    public final boolean component8() {
        return this.disableAutoMigrationNotification;
    }

    public final boolean component9() {
        return this.allowCensorshipSetting;
    }

    public final InternalSettingsState copy(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, String str, CallManager.AudioProcessingMethod audioProcessingMethod, CallManager.BandwidthMode bandwidthMode, boolean z10, boolean z11, EmojiFiles.Version version, boolean z12, boolean z13, boolean z14, boolean z15, boolean z16) {
        Intrinsics.checkNotNullParameter(str, "callingServer");
        Intrinsics.checkNotNullParameter(audioProcessingMethod, "callingAudioProcessingMethod");
        Intrinsics.checkNotNullParameter(bandwidthMode, "callingBandwidthMode");
        return new InternalSettingsState(z, z2, z3, z4, z5, z6, z7, z8, z9, str, audioProcessingMethod, bandwidthMode, z10, z11, version, z12, z13, z14, z15, z16);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof InternalSettingsState)) {
            return false;
        }
        InternalSettingsState internalSettingsState = (InternalSettingsState) obj;
        return this.seeMoreUserDetails == internalSettingsState.seeMoreUserDetails && this.shakeToReport == internalSettingsState.shakeToReport && this.gv2doNotCreateGv2Groups == internalSettingsState.gv2doNotCreateGv2Groups && this.gv2forceInvites == internalSettingsState.gv2forceInvites && this.gv2ignoreServerChanges == internalSettingsState.gv2ignoreServerChanges && this.gv2ignoreP2PChanges == internalSettingsState.gv2ignoreP2PChanges && this.disableAutoMigrationInitiation == internalSettingsState.disableAutoMigrationInitiation && this.disableAutoMigrationNotification == internalSettingsState.disableAutoMigrationNotification && this.allowCensorshipSetting == internalSettingsState.allowCensorshipSetting && Intrinsics.areEqual(this.callingServer, internalSettingsState.callingServer) && this.callingAudioProcessingMethod == internalSettingsState.callingAudioProcessingMethod && this.callingBandwidthMode == internalSettingsState.callingBandwidthMode && this.callingDisableTelecom == internalSettingsState.callingDisableTelecom && this.useBuiltInEmojiSet == internalSettingsState.useBuiltInEmojiSet && Intrinsics.areEqual(this.emojiVersion, internalSettingsState.emojiVersion) && this.removeSenderKeyMinimium == internalSettingsState.removeSenderKeyMinimium && this.delayResends == internalSettingsState.delayResends && this.disableStorageService == internalSettingsState.disableStorageService && this.disableStories == internalSettingsState.disableStories && this.canClearOnboardingState == internalSettingsState.canClearOnboardingState;
    }

    public int hashCode() {
        boolean z = this.seeMoreUserDetails;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.shakeToReport;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.gv2doNotCreateGv2Groups;
        if (z3) {
            z3 = true;
        }
        int i10 = z3 ? 1 : 0;
        int i11 = z3 ? 1 : 0;
        int i12 = z3 ? 1 : 0;
        int i13 = (i9 + i10) * 31;
        boolean z4 = this.gv2forceInvites;
        if (z4) {
            z4 = true;
        }
        int i14 = z4 ? 1 : 0;
        int i15 = z4 ? 1 : 0;
        int i16 = z4 ? 1 : 0;
        int i17 = (i13 + i14) * 31;
        boolean z5 = this.gv2ignoreServerChanges;
        if (z5) {
            z5 = true;
        }
        int i18 = z5 ? 1 : 0;
        int i19 = z5 ? 1 : 0;
        int i20 = z5 ? 1 : 0;
        int i21 = (i17 + i18) * 31;
        boolean z6 = this.gv2ignoreP2PChanges;
        if (z6) {
            z6 = true;
        }
        int i22 = z6 ? 1 : 0;
        int i23 = z6 ? 1 : 0;
        int i24 = z6 ? 1 : 0;
        int i25 = (i21 + i22) * 31;
        boolean z7 = this.disableAutoMigrationInitiation;
        if (z7) {
            z7 = true;
        }
        int i26 = z7 ? 1 : 0;
        int i27 = z7 ? 1 : 0;
        int i28 = z7 ? 1 : 0;
        int i29 = (i25 + i26) * 31;
        boolean z8 = this.disableAutoMigrationNotification;
        if (z8) {
            z8 = true;
        }
        int i30 = z8 ? 1 : 0;
        int i31 = z8 ? 1 : 0;
        int i32 = z8 ? 1 : 0;
        int i33 = (i29 + i30) * 31;
        boolean z9 = this.allowCensorshipSetting;
        if (z9) {
            z9 = true;
        }
        int i34 = z9 ? 1 : 0;
        int i35 = z9 ? 1 : 0;
        int i36 = z9 ? 1 : 0;
        int hashCode = (((((((i33 + i34) * 31) + this.callingServer.hashCode()) * 31) + this.callingAudioProcessingMethod.hashCode()) * 31) + this.callingBandwidthMode.hashCode()) * 31;
        boolean z10 = this.callingDisableTelecom;
        if (z10) {
            z10 = true;
        }
        int i37 = z10 ? 1 : 0;
        int i38 = z10 ? 1 : 0;
        int i39 = z10 ? 1 : 0;
        int i40 = (hashCode + i37) * 31;
        boolean z11 = this.useBuiltInEmojiSet;
        if (z11) {
            z11 = true;
        }
        int i41 = z11 ? 1 : 0;
        int i42 = z11 ? 1 : 0;
        int i43 = z11 ? 1 : 0;
        int i44 = (i40 + i41) * 31;
        EmojiFiles.Version version = this.emojiVersion;
        int hashCode2 = (i44 + (version == null ? 0 : version.hashCode())) * 31;
        boolean z12 = this.removeSenderKeyMinimium;
        if (z12) {
            z12 = true;
        }
        int i45 = z12 ? 1 : 0;
        int i46 = z12 ? 1 : 0;
        int i47 = z12 ? 1 : 0;
        int i48 = (hashCode2 + i45) * 31;
        boolean z13 = this.delayResends;
        if (z13) {
            z13 = true;
        }
        int i49 = z13 ? 1 : 0;
        int i50 = z13 ? 1 : 0;
        int i51 = z13 ? 1 : 0;
        int i52 = (i48 + i49) * 31;
        boolean z14 = this.disableStorageService;
        if (z14) {
            z14 = true;
        }
        int i53 = z14 ? 1 : 0;
        int i54 = z14 ? 1 : 0;
        int i55 = z14 ? 1 : 0;
        int i56 = (i52 + i53) * 31;
        boolean z15 = this.disableStories;
        if (z15) {
            z15 = true;
        }
        int i57 = z15 ? 1 : 0;
        int i58 = z15 ? 1 : 0;
        int i59 = z15 ? 1 : 0;
        int i60 = (i56 + i57) * 31;
        boolean z16 = this.canClearOnboardingState;
        if (!z16) {
            i = z16 ? 1 : 0;
        }
        return i60 + i;
    }

    public String toString() {
        return "InternalSettingsState(seeMoreUserDetails=" + this.seeMoreUserDetails + ", shakeToReport=" + this.shakeToReport + ", gv2doNotCreateGv2Groups=" + this.gv2doNotCreateGv2Groups + ", gv2forceInvites=" + this.gv2forceInvites + ", gv2ignoreServerChanges=" + this.gv2ignoreServerChanges + ", gv2ignoreP2PChanges=" + this.gv2ignoreP2PChanges + ", disableAutoMigrationInitiation=" + this.disableAutoMigrationInitiation + ", disableAutoMigrationNotification=" + this.disableAutoMigrationNotification + ", allowCensorshipSetting=" + this.allowCensorshipSetting + ", callingServer=" + this.callingServer + ", callingAudioProcessingMethod=" + this.callingAudioProcessingMethod + ", callingBandwidthMode=" + this.callingBandwidthMode + ", callingDisableTelecom=" + this.callingDisableTelecom + ", useBuiltInEmojiSet=" + this.useBuiltInEmojiSet + ", emojiVersion=" + this.emojiVersion + ", removeSenderKeyMinimium=" + this.removeSenderKeyMinimium + ", delayResends=" + this.delayResends + ", disableStorageService=" + this.disableStorageService + ", disableStories=" + this.disableStories + ", canClearOnboardingState=" + this.canClearOnboardingState + ')';
    }

    public InternalSettingsState(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, boolean z8, boolean z9, String str, CallManager.AudioProcessingMethod audioProcessingMethod, CallManager.BandwidthMode bandwidthMode, boolean z10, boolean z11, EmojiFiles.Version version, boolean z12, boolean z13, boolean z14, boolean z15, boolean z16) {
        Intrinsics.checkNotNullParameter(str, "callingServer");
        Intrinsics.checkNotNullParameter(audioProcessingMethod, "callingAudioProcessingMethod");
        Intrinsics.checkNotNullParameter(bandwidthMode, "callingBandwidthMode");
        this.seeMoreUserDetails = z;
        this.shakeToReport = z2;
        this.gv2doNotCreateGv2Groups = z3;
        this.gv2forceInvites = z4;
        this.gv2ignoreServerChanges = z5;
        this.gv2ignoreP2PChanges = z6;
        this.disableAutoMigrationInitiation = z7;
        this.disableAutoMigrationNotification = z8;
        this.allowCensorshipSetting = z9;
        this.callingServer = str;
        this.callingAudioProcessingMethod = audioProcessingMethod;
        this.callingBandwidthMode = bandwidthMode;
        this.callingDisableTelecom = z10;
        this.useBuiltInEmojiSet = z11;
        this.emojiVersion = version;
        this.removeSenderKeyMinimium = z12;
        this.delayResends = z13;
        this.disableStorageService = z14;
        this.disableStories = z15;
        this.canClearOnboardingState = z16;
    }

    public final boolean getSeeMoreUserDetails() {
        return this.seeMoreUserDetails;
    }

    public final boolean getShakeToReport() {
        return this.shakeToReport;
    }

    public final boolean getGv2doNotCreateGv2Groups() {
        return this.gv2doNotCreateGv2Groups;
    }

    public final boolean getGv2forceInvites() {
        return this.gv2forceInvites;
    }

    public final boolean getGv2ignoreServerChanges() {
        return this.gv2ignoreServerChanges;
    }

    public final boolean getGv2ignoreP2PChanges() {
        return this.gv2ignoreP2PChanges;
    }

    public final boolean getDisableAutoMigrationInitiation() {
        return this.disableAutoMigrationInitiation;
    }

    public final boolean getDisableAutoMigrationNotification() {
        return this.disableAutoMigrationNotification;
    }

    public final boolean getAllowCensorshipSetting() {
        return this.allowCensorshipSetting;
    }

    public final String getCallingServer() {
        return this.callingServer;
    }

    public final CallManager.AudioProcessingMethod getCallingAudioProcessingMethod() {
        return this.callingAudioProcessingMethod;
    }

    public final CallManager.BandwidthMode getCallingBandwidthMode() {
        return this.callingBandwidthMode;
    }

    public final boolean getCallingDisableTelecom() {
        return this.callingDisableTelecom;
    }

    public final boolean getUseBuiltInEmojiSet() {
        return this.useBuiltInEmojiSet;
    }

    public final EmojiFiles.Version getEmojiVersion() {
        return this.emojiVersion;
    }

    public final boolean getRemoveSenderKeyMinimium() {
        return this.removeSenderKeyMinimium;
    }

    public final boolean getDelayResends() {
        return this.delayResends;
    }

    public final boolean getDisableStorageService() {
        return this.disableStorageService;
    }

    public final boolean getDisableStories() {
        return this.disableStories;
    }

    public final boolean getCanClearOnboardingState() {
        return this.canClearOnboardingState;
    }
}
