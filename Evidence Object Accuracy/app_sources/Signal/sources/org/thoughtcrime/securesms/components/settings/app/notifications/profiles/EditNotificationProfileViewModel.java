package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.functions.Function;
import kotlin.Metadata;
import kotlin.NoWhenBranchMatchedException;
import kotlin.jvm.internal.DefaultConstructorMarker;
import kotlin.jvm.internal.Intrinsics;
import org.thoughtcrime.securesms.database.NotificationProfileDatabase;
import org.thoughtcrime.securesms.notifications.profiles.NotificationProfile;

/* compiled from: EditNotificationProfileViewModel.kt */
@Metadata(d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0003\u0014\u0015\u0016B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\r0\fJ\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\nJ\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\f2\u0006\u0010\u0013\u001a\u00020\nR\u000e\u0010\u0007\u001a\u00020\bX\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0004¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u000e¢\u0006\u0002\n\u0000¨\u0006\u0017"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel;", "Landroidx/lifecycle/ViewModel;", "profileId", "", "repository", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;", "(JLorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/NotificationProfilesRepository;)V", "createMode", "", "selectedEmoji", "", "getInitialState", "Lio/reactivex/rxjava3/core/Single;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$InitialState;", "onEmojiSelected", "", "emoji", "save", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult;", "name", "Factory", "InitialState", "SaveNotificationProfileResult", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class EditNotificationProfileViewModel extends ViewModel {
    private final boolean createMode;
    private final long profileId;
    private final NotificationProfilesRepository repository;
    private String selectedEmoji;

    public EditNotificationProfileViewModel(long j, NotificationProfilesRepository notificationProfilesRepository) {
        Intrinsics.checkNotNullParameter(notificationProfilesRepository, "repository");
        this.profileId = j;
        this.repository = notificationProfilesRepository;
        this.createMode = j == -1;
        this.selectedEmoji = "";
    }

    public final Single<InitialState> getInitialState() {
        Single single;
        boolean z = this.createMode;
        if (z) {
            single = Single.just(new InitialState(z, null, null, 6, null));
        } else {
            single = this.repository.getProfile(this.profileId).take(1).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileViewModel$$ExternalSyntheticLambda0
                @Override // io.reactivex.rxjava3.functions.Function
                public final Object apply(Object obj) {
                    return EditNotificationProfileViewModel.m754getInitialState$lambda0(EditNotificationProfileViewModel.this, (NotificationProfile) obj);
                }
            }).singleOrError();
        }
        Single<InitialState> observeOn = single.observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "initialState.observeOn(A…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: getInitialState$lambda-0 */
    public static final InitialState m754getInitialState$lambda0(EditNotificationProfileViewModel editNotificationProfileViewModel, NotificationProfile notificationProfile) {
        Intrinsics.checkNotNullParameter(editNotificationProfileViewModel, "this$0");
        return new InitialState(editNotificationProfileViewModel.createMode, notificationProfile.getName(), notificationProfile.getEmoji());
    }

    public final void onEmojiSelected(String str) {
        Intrinsics.checkNotNullParameter(str, "emoji");
        this.selectedEmoji = str;
    }

    public final Single<SaveNotificationProfileResult> save(String str) {
        Intrinsics.checkNotNullParameter(str, "name");
        Single<SaveNotificationProfileResult> observeOn = (this.createMode ? this.repository.createProfile(str, this.selectedEmoji) : this.repository.updateProfile(this.profileId, str, this.selectedEmoji)).map(new Function() { // from class: org.thoughtcrime.securesms.components.settings.app.notifications.profiles.EditNotificationProfileViewModel$$ExternalSyntheticLambda1
            @Override // io.reactivex.rxjava3.functions.Function
            public final Object apply(Object obj) {
                return EditNotificationProfileViewModel.m755save$lambda1(EditNotificationProfileViewModel.this, (NotificationProfileDatabase.NotificationProfileChangeResult) obj);
            }
        }).observeOn(AndroidSchedulers.mainThread());
        Intrinsics.checkNotNullExpressionValue(observeOn, "save.map { r ->\n      wh…dSchedulers.mainThread())");
        return observeOn;
    }

    /* renamed from: save$lambda-1 */
    public static final SaveNotificationProfileResult m755save$lambda1(EditNotificationProfileViewModel editNotificationProfileViewModel, NotificationProfileDatabase.NotificationProfileChangeResult notificationProfileChangeResult) {
        Intrinsics.checkNotNullParameter(editNotificationProfileViewModel, "this$0");
        if (notificationProfileChangeResult instanceof NotificationProfileDatabase.NotificationProfileChangeResult.Success) {
            return new SaveNotificationProfileResult.Success(((NotificationProfileDatabase.NotificationProfileChangeResult.Success) notificationProfileChangeResult).getNotificationProfile(), editNotificationProfileViewModel.createMode);
        }
        if (Intrinsics.areEqual(notificationProfileChangeResult, NotificationProfileDatabase.NotificationProfileChangeResult.DuplicateName.INSTANCE)) {
            return SaveNotificationProfileResult.DuplicateNameFailure.INSTANCE;
        }
        throw new NoWhenBranchMatchedException();
    }

    /* compiled from: EditNotificationProfileViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003¢\u0006\u0002\u0010\u0004J%\u0010\u0005\u001a\u0002H\u0006\"\b\b\u0000\u0010\u0006*\u00020\u00072\f\u0010\b\u001a\b\u0012\u0004\u0012\u0002H\u00060\tH\u0016¢\u0006\u0002\u0010\nR\u000e\u0010\u0002\u001a\u00020\u0003X\u0004¢\u0006\u0002\n\u0000¨\u0006\u000b"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$Factory;", "Landroidx/lifecycle/ViewModelProvider$Factory;", "profileId", "", "(J)V", "create", "T", "Landroidx/lifecycle/ViewModel;", "modelClass", "Ljava/lang/Class;", "(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class Factory implements ViewModelProvider.Factory {
        private final long profileId;

        public Factory(long j) {
            this.profileId = j;
        }

        @Override // androidx.lifecycle.ViewModelProvider.Factory
        public <T extends ViewModel> T create(Class<T> cls) {
            Intrinsics.checkNotNullParameter(cls, "modelClass");
            T cast = cls.cast(new EditNotificationProfileViewModel(this.profileId, new NotificationProfilesRepository()));
            Intrinsics.checkNotNull(cast);
            return cast;
        }
    }

    /* compiled from: EditNotificationProfileViewModel.kt */
    @Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u000e\n\u0002\u0010\b\n\u0002\b\u0002\b\b\u0018\u00002\u00020\u0001B!\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0005\u0012\b\b\u0002\u0010\u0006\u001a\u00020\u0005¢\u0006\u0002\u0010\u0007J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J\t\u0010\u000e\u001a\u00020\u0005HÆ\u0003J\t\u0010\u000f\u001a\u00020\u0005HÆ\u0003J'\u0010\u0010\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u0011\u001a\u00020\u00032\b\u0010\u0012\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001J\t\u0010\u0015\u001a\u00020\u0005HÖ\u0001R\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u000b¨\u0006\u0016"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$InitialState;", "", "createMode", "", "name", "", "emoji", "(ZLjava/lang/String;Ljava/lang/String;)V", "getCreateMode", "()Z", "getEmoji", "()Ljava/lang/String;", "getName", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static final class InitialState {
        private final boolean createMode;
        private final String emoji;
        private final String name;

        public static /* synthetic */ InitialState copy$default(InitialState initialState, boolean z, String str, String str2, int i, Object obj) {
            if ((i & 1) != 0) {
                z = initialState.createMode;
            }
            if ((i & 2) != 0) {
                str = initialState.name;
            }
            if ((i & 4) != 0) {
                str2 = initialState.emoji;
            }
            return initialState.copy(z, str, str2);
        }

        public final boolean component1() {
            return this.createMode;
        }

        public final String component2() {
            return this.name;
        }

        public final String component3() {
            return this.emoji;
        }

        public final InitialState copy(boolean z, String str, String str2) {
            Intrinsics.checkNotNullParameter(str, "name");
            Intrinsics.checkNotNullParameter(str2, "emoji");
            return new InitialState(z, str, str2);
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof InitialState)) {
                return false;
            }
            InitialState initialState = (InitialState) obj;
            return this.createMode == initialState.createMode && Intrinsics.areEqual(this.name, initialState.name) && Intrinsics.areEqual(this.emoji, initialState.emoji);
        }

        public int hashCode() {
            boolean z = this.createMode;
            if (z) {
                z = true;
            }
            int i = z ? 1 : 0;
            int i2 = z ? 1 : 0;
            int i3 = z ? 1 : 0;
            return (((i * 31) + this.name.hashCode()) * 31) + this.emoji.hashCode();
        }

        public String toString() {
            return "InitialState(createMode=" + this.createMode + ", name=" + this.name + ", emoji=" + this.emoji + ')';
        }

        public InitialState(boolean z, String str, String str2) {
            Intrinsics.checkNotNullParameter(str, "name");
            Intrinsics.checkNotNullParameter(str2, "emoji");
            this.createMode = z;
            this.name = str;
            this.emoji = str2;
        }

        public /* synthetic */ InitialState(boolean z, String str, String str2, int i, DefaultConstructorMarker defaultConstructorMarker) {
            this(z, (i & 2) != 0 ? "" : str, (i & 4) != 0 ? "" : str2);
        }

        public final boolean getCreateMode() {
            return this.createMode;
        }

        public final String getName() {
            return this.name;
        }

        public final String getEmoji() {
            return this.emoji;
        }
    }

    /* compiled from: EditNotificationProfileViewModel.kt */
    @Metadata(d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001:\u0002\u0003\u0004B\u0007\b\u0004¢\u0006\u0002\u0010\u0002\u0001\u0002\u0005\u0006¨\u0006\u0007"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult;", "", "()V", "DuplicateNameFailure", "Success", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult$Success;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult$DuplicateNameFailure;", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
    /* loaded from: classes4.dex */
    public static abstract class SaveNotificationProfileResult {
        public /* synthetic */ SaveNotificationProfileResult(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        /* compiled from: EditNotificationProfileViewModel.kt */
        @Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\n\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0005HÆ\u0003J\u001d\u0010\r\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u0005HÆ\u0001J\u0013\u0010\u000e\u001a\u00020\u00052\b\u0010\u000f\u001a\u0004\u0018\u00010\u0010HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0005¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult$Success;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult;", "profile", "Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "createMode", "", "(Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;Z)V", "getCreateMode", "()Z", "getProfile", "()Lorg/thoughtcrime/securesms/notifications/profiles/NotificationProfile;", "component1", "component2", "copy", "equals", "other", "", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class Success extends SaveNotificationProfileResult {
            private final boolean createMode;
            private final NotificationProfile profile;

            public static /* synthetic */ Success copy$default(Success success, NotificationProfile notificationProfile, boolean z, int i, Object obj) {
                if ((i & 1) != 0) {
                    notificationProfile = success.profile;
                }
                if ((i & 2) != 0) {
                    z = success.createMode;
                }
                return success.copy(notificationProfile, z);
            }

            public final NotificationProfile component1() {
                return this.profile;
            }

            public final boolean component2() {
                return this.createMode;
            }

            public final Success copy(NotificationProfile notificationProfile, boolean z) {
                Intrinsics.checkNotNullParameter(notificationProfile, "profile");
                return new Success(notificationProfile, z);
            }

            public boolean equals(Object obj) {
                if (this == obj) {
                    return true;
                }
                if (!(obj instanceof Success)) {
                    return false;
                }
                Success success = (Success) obj;
                return Intrinsics.areEqual(this.profile, success.profile) && this.createMode == success.createMode;
            }

            public int hashCode() {
                int hashCode = this.profile.hashCode() * 31;
                boolean z = this.createMode;
                if (z) {
                    z = true;
                }
                int i = z ? 1 : 0;
                int i2 = z ? 1 : 0;
                int i3 = z ? 1 : 0;
                return hashCode + i;
            }

            public String toString() {
                return "Success(profile=" + this.profile + ", createMode=" + this.createMode + ')';
            }

            /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
            public Success(NotificationProfile notificationProfile, boolean z) {
                super(null);
                Intrinsics.checkNotNullParameter(notificationProfile, "profile");
                this.profile = notificationProfile;
                this.createMode = z;
            }

            public final boolean getCreateMode() {
                return this.createMode;
            }

            public final NotificationProfile getProfile() {
                return this.profile;
            }
        }

        private SaveNotificationProfileResult() {
        }

        /* compiled from: EditNotificationProfileViewModel.kt */
        @Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002¨\u0006\u0003"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult$DuplicateNameFailure;", "Lorg/thoughtcrime/securesms/components/settings/app/notifications/profiles/EditNotificationProfileViewModel$SaveNotificationProfileResult;", "()V", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
        /* loaded from: classes4.dex */
        public static final class DuplicateNameFailure extends SaveNotificationProfileResult {
            public static final DuplicateNameFailure INSTANCE = new DuplicateNameFailure();

            private DuplicateNameFailure() {
                super(null);
            }
        }
    }
}
