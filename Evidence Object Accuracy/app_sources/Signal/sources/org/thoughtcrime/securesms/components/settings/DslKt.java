package org.thoughtcrime.securesms.components.settings;

import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: dsl.kt */
@Metadata(bv = {}, d1 = {"\u0000\u0010\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\u001a\u001a\u0010\u0004\u001a\u00020\u00012\u0012\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0001\u0012\u0004\u0012\u00020\u00020\u0000¨\u0006\u0005"}, d2 = {"Lkotlin/Function1;", "Lorg/thoughtcrime/securesms/components/settings/DSLConfiguration;", "", "init", "configure", "Signal-Android_websiteProdRelease"}, k = 2, mv = {1, 6, 0})
/* loaded from: classes4.dex */
public final class DslKt {
    public static final DSLConfiguration configure(Function1<? super DSLConfiguration, Unit> function1) {
        Intrinsics.checkNotNullParameter(function1, "init");
        DSLConfiguration dSLConfiguration = new DSLConfiguration();
        function1.invoke(dSLConfiguration);
        return dSLConfiguration;
    }
}
