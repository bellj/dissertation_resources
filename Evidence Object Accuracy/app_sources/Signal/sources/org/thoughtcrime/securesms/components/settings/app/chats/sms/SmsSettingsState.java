package org.thoughtcrime.securesms.components.settings.app.chats.sms;

import kotlin.Metadata;

/* compiled from: SmsSettingsState.kt */
@Metadata(d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000e\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003¢\u0006\u0002\u0010\u0006J\t\u0010\u000b\u001a\u00020\u0003HÆ\u0003J\t\u0010\f\u001a\u00020\u0003HÆ\u0003J\t\u0010\r\u001a\u00020\u0003HÆ\u0003J'\u0010\u000e\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u0003HÆ\u0001J\u0013\u0010\u000f\u001a\u00020\u00032\b\u0010\u0010\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0011\u001a\u00020\u0012HÖ\u0001J\t\u0010\u0013\u001a\u00020\u0014HÖ\u0001R\u0011\u0010\u0004\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\u0002\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\t\u0010\bR\u0011\u0010\u0005\u001a\u00020\u0003¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\b¨\u0006\u0015"}, d2 = {"Lorg/thoughtcrime/securesms/components/settings/app/chats/sms/SmsSettingsState;", "", "useAsDefaultSmsApp", "", "smsDeliveryReportsEnabled", "wifiCallingCompatibilityEnabled", "(ZZZ)V", "getSmsDeliveryReportsEnabled", "()Z", "getUseAsDefaultSmsApp", "getWifiCallingCompatibilityEnabled", "component1", "component2", "component3", "copy", "equals", "other", "hashCode", "", "toString", "", "Signal-Android_websiteProdRelease"}, k = 1, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class SmsSettingsState {
    private final boolean smsDeliveryReportsEnabled;
    private final boolean useAsDefaultSmsApp;
    private final boolean wifiCallingCompatibilityEnabled;

    public static /* synthetic */ SmsSettingsState copy$default(SmsSettingsState smsSettingsState, boolean z, boolean z2, boolean z3, int i, Object obj) {
        if ((i & 1) != 0) {
            z = smsSettingsState.useAsDefaultSmsApp;
        }
        if ((i & 2) != 0) {
            z2 = smsSettingsState.smsDeliveryReportsEnabled;
        }
        if ((i & 4) != 0) {
            z3 = smsSettingsState.wifiCallingCompatibilityEnabled;
        }
        return smsSettingsState.copy(z, z2, z3);
    }

    public final boolean component1() {
        return this.useAsDefaultSmsApp;
    }

    public final boolean component2() {
        return this.smsDeliveryReportsEnabled;
    }

    public final boolean component3() {
        return this.wifiCallingCompatibilityEnabled;
    }

    public final SmsSettingsState copy(boolean z, boolean z2, boolean z3) {
        return new SmsSettingsState(z, z2, z3);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SmsSettingsState)) {
            return false;
        }
        SmsSettingsState smsSettingsState = (SmsSettingsState) obj;
        return this.useAsDefaultSmsApp == smsSettingsState.useAsDefaultSmsApp && this.smsDeliveryReportsEnabled == smsSettingsState.smsDeliveryReportsEnabled && this.wifiCallingCompatibilityEnabled == smsSettingsState.wifiCallingCompatibilityEnabled;
    }

    public int hashCode() {
        boolean z = this.useAsDefaultSmsApp;
        int i = 1;
        if (z) {
            z = true;
        }
        int i2 = z ? 1 : 0;
        int i3 = z ? 1 : 0;
        int i4 = z ? 1 : 0;
        int i5 = i2 * 31;
        boolean z2 = this.smsDeliveryReportsEnabled;
        if (z2) {
            z2 = true;
        }
        int i6 = z2 ? 1 : 0;
        int i7 = z2 ? 1 : 0;
        int i8 = z2 ? 1 : 0;
        int i9 = (i5 + i6) * 31;
        boolean z3 = this.wifiCallingCompatibilityEnabled;
        if (!z3) {
            i = z3 ? 1 : 0;
        }
        return i9 + i;
    }

    public String toString() {
        return "SmsSettingsState(useAsDefaultSmsApp=" + this.useAsDefaultSmsApp + ", smsDeliveryReportsEnabled=" + this.smsDeliveryReportsEnabled + ", wifiCallingCompatibilityEnabled=" + this.wifiCallingCompatibilityEnabled + ')';
    }

    public SmsSettingsState(boolean z, boolean z2, boolean z3) {
        this.useAsDefaultSmsApp = z;
        this.smsDeliveryReportsEnabled = z2;
        this.wifiCallingCompatibilityEnabled = z3;
    }

    public final boolean getUseAsDefaultSmsApp() {
        return this.useAsDefaultSmsApp;
    }

    public final boolean getSmsDeliveryReportsEnabled() {
        return this.smsDeliveryReportsEnabled;
    }

    public final boolean getWifiCallingCompatibilityEnabled() {
        return this.wifiCallingCompatibilityEnabled;
    }
}
