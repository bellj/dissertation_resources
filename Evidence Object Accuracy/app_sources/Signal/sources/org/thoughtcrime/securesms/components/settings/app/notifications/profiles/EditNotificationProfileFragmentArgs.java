package org.thoughtcrime.securesms.components.settings.app.notifications.profiles;

import android.os.Bundle;
import java.util.HashMap;

/* loaded from: classes4.dex */
public class EditNotificationProfileFragmentArgs {
    private final HashMap arguments;

    private EditNotificationProfileFragmentArgs() {
        this.arguments = new HashMap();
    }

    private EditNotificationProfileFragmentArgs(HashMap hashMap) {
        HashMap hashMap2 = new HashMap();
        this.arguments = hashMap2;
        hashMap2.putAll(hashMap);
    }

    public static EditNotificationProfileFragmentArgs fromBundle(Bundle bundle) {
        EditNotificationProfileFragmentArgs editNotificationProfileFragmentArgs = new EditNotificationProfileFragmentArgs();
        bundle.setClassLoader(EditNotificationProfileFragmentArgs.class.getClassLoader());
        if (bundle.containsKey("profileId")) {
            editNotificationProfileFragmentArgs.arguments.put("profileId", Long.valueOf(bundle.getLong("profileId")));
        } else {
            editNotificationProfileFragmentArgs.arguments.put("profileId", -1L);
        }
        return editNotificationProfileFragmentArgs;
    }

    public long getProfileId() {
        return ((Long) this.arguments.get("profileId")).longValue();
    }

    public Bundle toBundle() {
        Bundle bundle = new Bundle();
        if (this.arguments.containsKey("profileId")) {
            bundle.putLong("profileId", ((Long) this.arguments.get("profileId")).longValue());
        } else {
            bundle.putLong("profileId", -1);
        }
        return bundle;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        EditNotificationProfileFragmentArgs editNotificationProfileFragmentArgs = (EditNotificationProfileFragmentArgs) obj;
        return this.arguments.containsKey("profileId") == editNotificationProfileFragmentArgs.arguments.containsKey("profileId") && getProfileId() == editNotificationProfileFragmentArgs.getProfileId();
    }

    public int hashCode() {
        return 31 + ((int) (getProfileId() ^ (getProfileId() >>> 32)));
    }

    public String toString() {
        return "EditNotificationProfileFragmentArgs{profileId=" + getProfileId() + "}";
    }

    /* loaded from: classes4.dex */
    public static class Builder {
        private final HashMap arguments;

        public Builder(EditNotificationProfileFragmentArgs editNotificationProfileFragmentArgs) {
            HashMap hashMap = new HashMap();
            this.arguments = hashMap;
            hashMap.putAll(editNotificationProfileFragmentArgs.arguments);
        }

        public Builder() {
            this.arguments = new HashMap();
        }

        public EditNotificationProfileFragmentArgs build() {
            return new EditNotificationProfileFragmentArgs(this.arguments);
        }

        public Builder setProfileId(long j) {
            this.arguments.put("profileId", Long.valueOf(j));
            return this;
        }

        public long getProfileId() {
            return ((Long) this.arguments.get("profileId")).longValue();
        }
    }
}
