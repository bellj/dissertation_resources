package org.thoughtcrime.securesms.components.settings.app.privacy;

import androidx.recyclerview.widget.RecyclerView;
import com.annimon.stream.function.Function;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;
import me.leolin.shortcutbadger.impl.NewHtcHomeBadger;

/* compiled from: PrivacySettingsViewModel.kt */
@Metadata(d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u0003H\n¢\u0006\u0002\b\u0004"}, d2 = {"<anonymous>", "", NewHtcHomeBadger.COUNT, "", "invoke"}, k = 3, mv = {1, 6, 0}, xi = 48)
/* loaded from: classes4.dex */
public final class PrivacySettingsViewModel$refreshBlockedCount$1 extends Lambda implements Function1<Integer, Unit> {
    final /* synthetic */ PrivacySettingsViewModel this$0;

    /* JADX INFO: 'super' call moved to the top of the method (can break code semantics) */
    public PrivacySettingsViewModel$refreshBlockedCount$1(PrivacySettingsViewModel privacySettingsViewModel) {
        super(1);
        this.this$0 = privacySettingsViewModel;
    }

    @Override // kotlin.jvm.functions.Function1
    public /* bridge */ /* synthetic */ Unit invoke(Integer num) {
        invoke(num.intValue());
        return Unit.INSTANCE;
    }

    /* renamed from: invoke$lambda-0 */
    public static final PrivacySettingsState m838invoke$lambda0(int i, PrivacySettingsState privacySettingsState) {
        Intrinsics.checkNotNullExpressionValue(privacySettingsState, "it");
        return privacySettingsState.copy((r33 & 1) != 0 ? privacySettingsState.blockedCount : i, (r33 & 2) != 0 ? privacySettingsState.seeMyPhoneNumber : null, (r33 & 4) != 0 ? privacySettingsState.findMeByPhoneNumber : null, (r33 & 8) != 0 ? privacySettingsState.readReceipts : false, (r33 & 16) != 0 ? privacySettingsState.typingIndicators : false, (r33 & 32) != 0 ? privacySettingsState.screenLock : false, (r33 & 64) != 0 ? privacySettingsState.screenLockActivityTimeout : 0, (r33 & 128) != 0 ? privacySettingsState.screenSecurity : false, (r33 & 256) != 0 ? privacySettingsState.incognitoKeyboard : false, (r33 & 512) != 0 ? privacySettingsState.isObsoletePasswordEnabled : false, (r33 & 1024) != 0 ? privacySettingsState.isObsoletePasswordTimeoutEnabled : false, (r33 & RecyclerView.ItemAnimator.FLAG_MOVED) != 0 ? privacySettingsState.obsoletePasswordTimeout : 0, (r33 & RecyclerView.ItemAnimator.FLAG_APPEARED_IN_PRE_LAYOUT) != 0 ? privacySettingsState.universalExpireTimer : 0, (r33 & 8192) != 0 ? privacySettingsState.privateStories : null, (r33 & 16384) != 0 ? privacySettingsState.isStoriesEnabled : false);
    }

    public final void invoke(int i) {
        this.this$0.store.update(new Function(i) { // from class: org.thoughtcrime.securesms.components.settings.app.privacy.PrivacySettingsViewModel$refreshBlockedCount$1$$ExternalSyntheticLambda0
            public final /* synthetic */ int f$0;

            {
                this.f$0 = r1;
            }

            @Override // com.annimon.stream.function.Function
            public final Object apply(Object obj) {
                return PrivacySettingsViewModel$refreshBlockedCount$1.m838invoke$lambda0(this.f$0, (PrivacySettingsState) obj);
            }
        });
        this.this$0.refresh();
    }
}
