package org.thoughtcrime.securesms.components;

import android.animation.LayoutTransition;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.annimon.stream.Stream;
import com.annimon.stream.function.BiFunction;
import com.annimon.stream.function.Consumer;
import com.pnikosis.materialishprogress.ProgressWheel;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.thoughtcrime.securesms.BuildConfig;
import org.thoughtcrime.securesms.R;
import org.thoughtcrime.securesms.attachments.Attachment;
import org.thoughtcrime.securesms.events.PartProgressEvent;
import org.thoughtcrime.securesms.mms.Slide;

/* loaded from: classes.dex */
public final class TransferControlView extends FrameLayout {
    private static final int COMPRESSION_TASK_WEIGHT;
    private static final int UPLOAD_TASK_WEIGHT;
    private final Map<Attachment, Float> compresssionProgress;
    private View current;
    private final View downloadDetails;
    private final TextView downloadDetailsText;
    private final Map<Attachment, Float> networkProgress;
    private final ProgressWheel progressWheel;
    private List<Slide> slides;

    public TransferControlView(Context context) {
        this(context, null);
    }

    public TransferControlView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public TransferControlView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        FrameLayout.inflate(context, R.layout.transfer_controls_view, this);
        setLongClickable(false);
        setBackground(ContextCompat.getDrawable(context, R.drawable.transfer_controls_background));
        setVisibility(8);
        setLayoutTransition(new LayoutTransition());
        this.networkProgress = new HashMap();
        this.compresssionProgress = new HashMap();
        this.progressWheel = (ProgressWheel) findViewById(R.id.progress_wheel);
        this.downloadDetails = findViewById(R.id.download_details);
        this.downloadDetailsText = (TextView) findViewById(R.id.download_details_text);
    }

    @Override // android.view.View
    public void setFocusable(boolean z) {
        super.setFocusable(z);
        this.downloadDetails.setFocusable(z);
    }

    @Override // android.view.View
    public void setClickable(boolean z) {
        super.setClickable(z);
        this.downloadDetails.setClickable(z);
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override // android.view.View, android.view.ViewGroup
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    public void setSlide(Slide slide) {
        setSlides(Collections.singletonList(slide));
    }

    public void setSlides(List<Slide> list) {
        if (!list.isEmpty()) {
            this.slides = list;
            if (!isUpdateToExistingSet(list)) {
                this.networkProgress.clear();
                this.compresssionProgress.clear();
                Stream.of(list).forEach(new Consumer() { // from class: org.thoughtcrime.securesms.components.TransferControlView$$ExternalSyntheticLambda0
                    @Override // com.annimon.stream.function.Consumer
                    public final void accept(Object obj) {
                        TransferControlView.this.lambda$setSlides$0((Slide) obj);
                    }
                });
            }
            for (Slide slide : list) {
                if (slide.asAttachment().getTransferState() == 0) {
                    this.networkProgress.put(slide.asAttachment(), Float.valueOf(1.0f));
                }
            }
            int transferState = getTransferState(list);
            if (transferState == 1) {
                showProgressSpinner(calculateProgress(this.networkProgress, this.compresssionProgress));
            } else if (transferState == 2 || transferState == 3) {
                this.downloadDetailsText.setText(getDownloadText(this.slides));
                display(this.downloadDetails);
            } else {
                display(null);
            }
        } else {
            throw new IllegalArgumentException("Must provide at least one slide.");
        }
    }

    public /* synthetic */ void lambda$setSlides$0(Slide slide) {
        this.networkProgress.put(slide.asAttachment(), Float.valueOf(0.0f));
    }

    public void showProgressSpinner() {
        showProgressSpinner(calculateProgress(this.networkProgress, this.compresssionProgress));
    }

    public void showProgressSpinner(float f) {
        if (f == 0.0f) {
            this.progressWheel.spin();
        } else {
            this.progressWheel.setInstantProgress(f);
        }
        display(this.progressWheel);
    }

    public void setDownloadClickListener(View.OnClickListener onClickListener) {
        this.downloadDetails.setOnClickListener(onClickListener);
    }

    public void clear() {
        clearAnimation();
        setVisibility(8);
        View view = this.current;
        if (view != null) {
            view.clearAnimation();
            this.current.setVisibility(8);
        }
        this.current = null;
        this.slides = null;
    }

    public void setShowDownloadText(boolean z) {
        this.downloadDetailsText.setVisibility(z ? 0 : 8);
        forceLayout();
    }

    private boolean isUpdateToExistingSet(List<Slide> list) {
        if (list.size() != this.networkProgress.size()) {
            return false;
        }
        for (Slide slide : list) {
            if (!this.networkProgress.containsKey(slide.asAttachment())) {
                return false;
            }
        }
        return true;
    }

    private int getTransferState(List<Slide> list) {
        int i = 0;
        for (Slide slide : list) {
            if (slide.getTransferState() == 2 && i == 0) {
                i = slide.getTransferState();
            } else {
                i = Math.max(i, slide.getTransferState());
            }
        }
        return i;
    }

    private String getDownloadText(List<Slide> list) {
        if (list.size() == 1) {
            return list.get(0).getContentDescription();
        }
        int intValue = ((Integer) Stream.of(list).reduce(0, new BiFunction() { // from class: org.thoughtcrime.securesms.components.TransferControlView$$ExternalSyntheticLambda1
            @Override // com.annimon.stream.function.BiFunction
            public final Object apply(Object obj, Object obj2) {
                return TransferControlView.lambda$getDownloadText$1((Integer) obj, (Slide) obj2);
            }
        })).intValue();
        return getContext().getResources().getQuantityString(R.plurals.TransferControlView_n_items, intValue, Integer.valueOf(intValue));
    }

    public static /* synthetic */ Integer lambda$getDownloadText$1(Integer num, Slide slide) {
        int transferState = slide.getTransferState();
        int intValue = num.intValue();
        if (transferState != 0) {
            intValue++;
        }
        return Integer.valueOf(intValue);
    }

    private void display(View view) {
        View view2 = this.current;
        if (view2 != view) {
            if (view2 != null) {
                view2.setVisibility(8);
            }
            if (view != null) {
                view.setVisibility(0);
                setVisibility(0);
            } else {
                setVisibility(8);
            }
            this.current = view;
        }
    }

    private static float calculateProgress(Map<Attachment, Float> map, Map<Attachment, Float> map2) {
        float f = 0.0f;
        float f2 = 0.0f;
        for (Float f3 : map.values()) {
            f2 += f3.floatValue();
        }
        for (Float f4 : map2.values()) {
            f += f4.floatValue();
        }
        return ((f2 * 1.0f) + (f * 3.0f)) / ((float) ((map.size() * 1) + (map2.size() * 3)));
    }

    @Subscribe(sticky = BuildConfig.PLAY_STORE_DISABLED, threadMode = ThreadMode.MAIN)
    public void onEventAsync(PartProgressEvent partProgressEvent) {
        if (this.networkProgress.containsKey(partProgressEvent.attachment)) {
            float f = ((float) partProgressEvent.progress) / ((float) partProgressEvent.total);
            if (partProgressEvent.type == PartProgressEvent.Type.COMPRESSION) {
                this.compresssionProgress.put(partProgressEvent.attachment, Float.valueOf(f));
            } else {
                this.networkProgress.put(partProgressEvent.attachment, Float.valueOf(f));
            }
            this.progressWheel.setInstantProgress(calculateProgress(this.networkProgress, this.compresssionProgress));
        }
    }
}
