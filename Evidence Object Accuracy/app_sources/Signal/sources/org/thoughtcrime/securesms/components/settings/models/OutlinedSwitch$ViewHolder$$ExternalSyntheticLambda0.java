package org.thoughtcrime.securesms.components.settings.models;

import android.view.View;
import org.thoughtcrime.securesms.components.settings.models.OutlinedSwitch;

/* compiled from: R8$$SyntheticClass */
/* loaded from: classes4.dex */
public final /* synthetic */ class OutlinedSwitch$ViewHolder$$ExternalSyntheticLambda0 implements View.OnClickListener {
    public final /* synthetic */ OutlinedSwitch.Model f$0;

    public /* synthetic */ OutlinedSwitch$ViewHolder$$ExternalSyntheticLambda0(OutlinedSwitch.Model model) {
        this.f$0 = model;
    }

    @Override // android.view.View.OnClickListener
    public final void onClick(View view) {
        OutlinedSwitch.ViewHolder.m1254bind$lambda0(this.f$0, view);
    }
}
